; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "LES"
#define MyAppFilePath "D:\Project\BR_2012\BIN\LES.exe"
#define MyMajorVer
#define MyMinorVer
#define MyRevVer
#define MyBuildDate GetStringFileInfo(MyAppFilePath, "PrivateBuild")
#define MyAppVersion ParseVersion(MyAppFilePath, MyMajorVer, MyMinorVer, MyRevVer, NULL);
#define MyAppPublisher "LSIS Co. Ltd."
#define MyAppURL "http://www.lsis.com"
#define MyAppExeName "LES.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{F6EDF2CC-EA34-4C15-AD71-F8100BE68B30}
AppName={#MyAppName}
AppVersion={#MyMajorVer}.{#MyMinorVer}.{#MyRevVer}.{#MyBuildDate}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\LSIS
DisableDirPage=yes
DefaultGroupName=LSIS
LicenseFile=D:\Project\BR_2012\Release\license.txt
DisableProgramGroupPage=yes
;OutputDir=Output_13STN
OutputBaseFilename={#MyAppName}_setup_13STN_{#MyMajorVer}.{#MyMinorVer}.{#MyRevVer}.{#MyBuildDate}
Compression=lzma
SolidCompression=yes
AlwaysRestart=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Types]
Name: "custom"; Description: "Custom installation"; Flags: iscustom

[Components]
Name: "system"; Description: "System Files"; Types: custom; Flags: fixed
Name: "program"; Description: "Program Files"; Types: custom; Flags: fixed
Name: "db"; Description: "Database Files"; Types: custom; Flags: fixed
Name: "db\ALNR"; Description: "Aualianagar(ALNR)"; Flags: exclusive
Name: "db\BGPX"; Description: "Bhawalgazipur(BGPX)"; Flags: exclusive
Name: "db\DHV"; Description: "Dhola(DHV)"; Flags: exclusive
Name: "db\FMN"; Description: "Fatemanagar(FMN)"; Flags: exclusive
Name: "db\GFN"; Description: "Gafargaon(GFN)"; Flags: exclusive
Name: "db\IZP"; Description: "Izzatpur(IZP)"; Flags: exclusive
Name: "db\KWY"; Description: "Kawraid(KWY)"; Flags: exclusive
Name: "db\MHY"; Description: "Moshakhali(MHY)"; Flags: exclusive
Name: "db\MYN"; Description: "Mymensingh(MYN)"; Flags: exclusive
Name: "db\RAP"; Description: "Rajendrapur(RAP)"; Flags: exclusive
Name: "db\SPU"; Description: "Sripur(SPU)"; Flags: exclusive
Name: "db\SSK"; Description: "Satkhamair(SSK)"; Flags: exclusive
Name: "db\SUY"; Description: "Sutiakhali(SUY)"; Flags: exclusive

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Dirs]
Name: "{app}\ALNR"; Components: db/ALNR
Name: "{app}\BGPX"; Components: db/BGPX
Name: "{app}\DHV"; Components: db/DHV
Name: "{app}\FMN"; Components: db/FMN
Name: "{app}\GFN"; Components: db/GFN
Name: "{app}\IZP"; Components: db/IZP
Name: "{app}\KWY"; Components: db/KWY
Name: "{app}\MHY"; Components: db/MHY
Name: "{app}\MYN"; Components: db/MYN
Name: "{app}\RAP"; Components: db/RAP
Name: "{app}\SPU"; Components: db/SPU
Name: "{app}\SSK"; Components: db/SSK
Name: "{app}\SUY"; Components: db/SUY

[Files]
Source: "D:\Project\BR_2012\Release\Setup Files\DLL\*"; Excludes: "*.OCX"; DestDir: "{sys}"; Components: system
;Source: "D:\Project\BR_2012\Release\Setup Files\DLL\Crystl32.OCX"; DestDir: "{sys}"; Components: system; Flags: regserver
Source: "D:\Project\BR_2012\BIN\HookTool.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\HookTool.ini"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\HookTool.lib"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\HookTool.log"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\LES.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\EventMgr.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\LISMSGKR_ORIGINAL.TXT"; DestName: "LISMSGKR.TXT"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\LSM.lic"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\LSM.ocx"; DestDir: "{app}"; Components: program; Flags: ignoreversion regserver
Source: "D:\Project\BR_2012\BIN\Station.ini"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\GPSConf_13STN.ini"; DestName: "GPSConf.ini"; DestDir: "{app}"; Components: db/MYN; Check: IsMMCR; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\GPSSvr.exe"; DestDir: "{app}"; Components: db/MYN; Check: IsMMCR; Flags: ignoreversion
Source: "D:\Project\BR_2012\BIN\ALNR\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\ALNR"; Components: db/ALNR; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\BGPX\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\BGPX"; Components: db/BGPX; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\DHV\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\DHV"; Components: db/DHV; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\FMN\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\FMN"; Components: db/FMN; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\GFN\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\GFN"; Components: db/GFN; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\IZP\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\IZP"; Components: db/IZP; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\KWY\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\KWY"; Components: db/KWY; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\MHY\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\MHY"; Components: db/MHY; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\MYN\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\MYN"; Components: db/MYN; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\RAP\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\RAP"; Components: db/RAP; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\SPU\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\SPU"; Components: db/SPU; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\SSK\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\SSK"; Components: db/SSK; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Project\BR_2012\BIN\SUY\*"; Excludes: "*.*~,*.bak,.*"; DestDir: "{app}\SUY"; Components: db/SUY; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[INI]
Filename: "{app}\Station.ini"; Section: "SYSTEM"
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "ALNR"; Components: db/ALNR
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "BGPX"; Components: db/BGPX
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "DHV"; Components: db/DHV
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "FMN"; Components: db/FMN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "GFN"; Components: db/GFN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "IZP"; Components: db/IZP
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "KWY"; Components: db/KWY
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "MHY"; Components: db/MHY
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "MYN"; Components: db/MYN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "RAP"; Components: db/RAP
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "SPU"; Components: db/SPU
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "SSK"; Components: db/SSK
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_FILENAME"; String: "SUY"; Components: db/SUY
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "ALNR"; Components: db/ALNR
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "BGPX"; Components: db/BGPX
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "DHV"; Components: db/DHV
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "FMN"; Components: db/FMN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "GFN"; Components: db/GFN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "IZP"; Components: db/IZP
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "KWY"; Components: db/KWY
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "MHY"; Components: db/MHY
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "MYN"; Components: db/MYN
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "RAP"; Components: db/RAP
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "SPU"; Components: db/SPU
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "SSK"; Components: db/SSK
Filename: "{app}\Station.ini"; Section: "SYSTEM"; Key: "STATION_REALNAME"; String: "SUY"; Components: db/SUY
Filename: "{app}\Station.ini"; Section: "WINDOW"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LSM_HEIGHT";      String: "1600";       Components: db/MYN
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LSM_HEIGHT";      String: "1080";       Components: not db/MYN
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LSM_RESOLUTION";  String: "2560-1600";  Components: db/MYN
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LSM_RESOLUTION";  String: "1920-1080";  Components: not db/MYN
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_STARTX";  String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_STARTY";  String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_SIZEX";   String: "2560";       Components: db/MYN
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_SIZEX";   String: "1920";       Components: not db/MYN 
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_SIZEY";   String: "1600";       Components: db/MYN 
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MAINWND_SIZEY";   String: "1080";       Components: not db/MYN 
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MENUWND_STARTX";  String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MENUWND_STARTY";  String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MENUWND_SIZEX";   String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "MENUWND_SIZEY";   String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LOGWND_STARTX";   String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LOGWND_STARTY";   String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LOGWND_SIZEX";    String: "0"
Filename: "{app}\Station.ini"; Section: "WINDOW"; Key: "LOGWND_SIZEY";    String: "0"
Filename: "{app}\Station.ini"; Section: "OPTION"
;Filename: "{app}\Station.ini"; Section: "OPTION"; Key: "TESTMODE"; String: "FALSE"; Check: not IsMMCR
Filename: "{app}\Station.ini"; Section: "OPTION"; Key: "TESTMODE"; String: "TRUE"
Filename: "{app}\Station.ini"; Section: "COM"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "MODE"; String: "SERIAL"; Check: not IsMMCR
Filename: "{app}\Station.ini"; Section: "COM"; Key: "MODE"; String: "LAN"; Check: IsMMCR
Filename: "{app}\Station.ini"; Section: "COM"; Key: "SHAREDMEMORY"; String: "NO"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "DISPLAY"; String: "YES"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "COMPORT1"; String: "1"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "COMPORT2"; String: "2"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "TRCID"; String: "0"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "TRCIP"; String: "0.0.0.0"
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.15"; Components: db/BGPX
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.25"; Components: db/RAP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.35"; Components: db/IZP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.45"; Components: db/SPU
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.55"; Components: db/SSK
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.65"; Components: db/KWY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.75"; Components: db/MHY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.85"; Components: db/GFN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.95"; Components: db/DHV
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.105"; Components: db/ALNR
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.115"; Components: db/FMN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.125"; Components: db/SUY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_LOCAL"; String: "192.6.14.135"; Components: db/MYN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.11"; Components: db/BGPX
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.21"; Components: db/RAP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.31"; Components: db/IZP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.41"; Components: db/SPU
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.51"; Components: db/SSK
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.61"; Components: db/KWY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.71"; Components: db/MHY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.81"; Components: db/GFN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.91"; Components: db/DHV
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.101"; Components: db/ALNR
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.111"; Components: db/FMN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.121"; Components: db/SUY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_PRI_CBI"; String: "192.6.14.131"; Components: db/MYN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.12"; Components: db/BGPX
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.22"; Components: db/RAP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.32"; Components: db/IZP
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.42"; Components: db/SPU
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.52"; Components: db/SSK
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.62"; Components: db/KWY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.72"; Components: db/MHY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.82"; Components: db/GFN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.92"; Components: db/DHV
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.102"; Components: db/ALNR
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.112"; Components: db/FMN
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.122"; Components: db/SUY
Filename: "{app}\Station.ini"; Section: "COM"; Key: "NET1_SEC_CBI"; String: "192.6.14.132"; Components: db/MYN

[Registry]
Root: HKCU; Subkey: "Software\LSIS"; Flags: uninsdeletekeyifempty
Root: HKCU; Subkey: "Software\LSIS\LES"; Flags: uninsdeletekeyifempty
Root: HKCU; Subkey: "Software\LSIS\LES\Common"; Flags: uninsdeletekeyifempty
Root: HKCU; Subkey: "Software\LSIS\LES\Common"; ValueType: dword; ValueName: "SYSTYPE"; ValueData: "$00000000"
Root: HKCU; Subkey: "Software\LSIS\LES\Common"; ValueType: dword; ValueName: "SYSNO"; ValueData: "$00000001"
Root: HKCU; Subkey: "Software\LSIS\LES\Common"; ValueType: dword; ValueName: "DATE"; ValueData: "$18b8c039"
Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; Flags: uninsdeletekey
;Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "Callon";      ValueData: "$0001869f"
Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "Callon";       ValueData: "$00000000"
Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "RouteRelease"; ValueData: "$00000000"
;Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "B2AXLreset";  ValueData: "$0001869f"
Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "B2AXLreset";   ValueData: "$00000000"
Root: HKCU; Subkey: "Software\LSIS\LES\Counter"; ValueType: dword; ValueName: "B4AXLreset";   ValueData: "$00000000"
Root: HKCU; Subkey: "Software\LSIS\LES\Operator"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\LSIS\LES\Operator"; ValueType: string; ValueName: "strCurrentOperator"; ValueData: "ADMIN"
Root: HKCU; Subkey: "Software\LSIS\LES\Operator"; ValueType: dword; ValueName: "bCtrlLock"; ValueData: "$00000000"
Root: HKCU; Subkey: "Software\LSIS\LES\Operator"; ValueType: string; ValueName: "strLockOperator"; ValueData: ""
Root: HKCU; Subkey: "Software\LSIS\LES\USER_ID"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\LSIS\LES\USER_ID"; ValueType: string; ValueName: "ID1"; ValueData: "ADMIN"
Root: HKCU; Subkey: "Software\LSIS\LES\USER_ID"; ValueType: dword; ValueName: "NUMBER"; ValueData: "$00000001"
Root: HKCU; Subkey: "Software\LSIS\LES\USER_PASS"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\LSIS\LES\USER_PASS"; ValueType: string; ValueName: "ADMIN"; ValueData: "ADMIN"
Root: HKLM; Subkey: "Software\Microsoft\PCHealth\ErrorReporting"; ValueType: dword; ValueName: "DoReport"; ValueData: "$00000000"
Root: HKLM; Subkey: "Software\Microsoft\PCHealth\ErrorReporting"; ValueType: dword; ValueName: "ShowUI"; ValueData: "$00000000" 

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{group}\LSIS GPS Clock Server"; Filename: "{app}\GPSSvr.exe"; Check: IsMMCR; Components: db/MYN 
Name: "{commonstartup}\LSIS GPS Clock Server"; Filename: "{app}\GPSSvr.exe"; Check: IsMMCR; Components: db/MYN
Name: "{group}\Uninstall"; Filename: "{uninstallexe}"

[Run]
Filename: "{sys}\Control.exe"; StatusMsg: "Setup TimeZone..."; Flags: runhidden; Parameters: "TIMEDATE.CPL,,/Z Central Asia Standard Time"
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.15 255.255.255.0"; Check: IsMMCR; Components: db/BGPX
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.25 255.255.255.0"; Check: IsMMCR;  Components: db/RAP
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.35 255.255.255.0"; Check: IsMMCR;  Components: db/IZP
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.45 255.255.255.0"; Check: IsMMCR;  Components: db/SPU
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.55 255.255.255.0"; Check: IsMMCR;  Components: db/SSK
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.65 255.255.255.0"; Check: IsMMCR;  Components: db/KWY
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.75 255.255.255.0"; Check: IsMMCR;  Components: db/MHY
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.85 255.255.255.0"; Check: IsMMCR;  Components: db/GFN
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.95 255.255.255.0"; Check: IsMMCR;  Components: db/DHV
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.105 255.255.255.0"; Check: IsMMCR;  Components: db/ALNR
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.115 255.255.255.0"; Check: IsMMCR;  Components: db/FMN
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.125 255.255.255.0"; Check: IsMMCR;  Components: db/SUY
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup IP Address..."; Flags: runhidden; Parameters: "interface ip set address name=""Local Area Connection"" static 192.6.14.135 255.255.255.0"; Check: IsMMCR;  Components: db/MYN
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup Firewall..."; Flags: runhidden; Parameters: "firewall set opmode enable"
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup Firewall..."; Flags: runhidden; Parameters: "firewall add allowedprogram program=""C:\Program Files\LSIS\LES.exe"" name=LES mode=ENABLE"; Check:IsMMCR
Filename: "{sys}\netsh.exe"; StatusMsg: "Setup Firewall..."; Flags: runhidden; Parameters: "firewall add allowedprogram program=""C:\Program Files\LSIS\GPSSvr.exe"" name=GPSSvr mode=ENABLE"; Check:IsMMCR; Components: db/MYN
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Code]
var
  UsagePage: TInputOptionWizardPage;

procedure InitializeWizard;
begin
  { Create the pages }

  UsagePage := CreateInputOptionPage(wpLicense,
    'Choose Target', 'Select target where Setup will install this program.',
    'Please select target console where you would like to install this program, then click Next.',
    True, False);
  UsagePage.Add('1st MCCR (for Station Master)');
  UsagePage.Add('2nd MCCR (for Station Master)');
  UsagePage.Add('MMCR (for Maintenance)');

  { Set default values, using settings that were stored last time if possible }
  case GetPreviousData('SysMode', '') of
    'MCCR1': UsagePage.SelectedValueIndex := 0;
    'MCCR2': UsagePage.SelectedValueIndex := 1;
    'MMCR': UsagePage.SelectedValueIndex := 2;
  else
    UsagePage.SelectedValueIndex := 0;
  end;
end;

procedure RegisterPreviousData(PreviousDataKey: Integer);
var
  SysMode: String;
begin
  { Return a user value }
  { Could also be split into separate GetUserName and GetUserCompany functions }
  case UsagePage.SelectedValueIndex of
    0: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSTYPE', 0);
    1: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSTYPE', 0);
    2: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSTYPE', 1);
  end;
  case UsagePage.SelectedValueIndex of
    0: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSNO', 1);
    1: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSNO', 2);
    2: RegWriteDWordValue(HKEY_CURRENT_USER, 'Software\LSIS\LES\Common', 'SYSNO', 3);
  end;
  case UsagePage.SelectedValueIndex of
    0: SysMode := 'MCCR1';
    1: SysMode := 'MCCR2';
    2: SysMode := 'MMCR';
  end;
  SetPreviousData(PreviousDataKey, 'SysMode', SysMode);
end;

function IsMMCR(): Boolean;
var
  CheckResult: Boolean;
begin
  case UsagePage.SelectedValueIndex of
    0: CheckResult := false;
    1: CheckResult := false;
    2: CheckResult := true;
  end;
  Result := CheckResult;
end;

