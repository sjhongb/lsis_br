// GPSComm.h: interface for the CGPSComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPSCOMM_H__52E08B53_1F74_4056_B627_62D37D91CB52__INCLUDED_)
#define AFX_GPSCOMM_H__52E08B53_1F74_4056_B627_62D37D91CB52__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SerialComm.h"

class CGPSComm : public CSerialComm  
{
	DECLARE_DYNCREATE(CGPSComm)
public:
	BOOL GetGPSStatus();
	SYSTEMTIME GetLocalTime();
	SYSTEMTIME GetUTCTime();
	CGPSComm();
	virtual ~CGPSComm();
	virtual void ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen);
	virtual BOOL SendMsg(BYTE *pBuffer, USHORT nMsgLen);

private:
	BOOL m_bGPSOK;
	CRITICAL_SECTION m_CSTime;
	SYSTEMTIME m_GPSTime;
	BYTE m_pRcvBuffer[MAX_BUFFER_LEN];
	USHORT m_nRcvLen;
};

#endif // !defined(AFX_GPSCOMM_H__52E08B53_1F74_4056_B627_62D37D91CB52__INCLUDED_)
