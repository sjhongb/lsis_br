//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GPSSvr.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDC_CUSTOM_GRIDCTRL             100
#define IDS_ABOUTBOX                    101
#define IDC_CUSTOM_GRIDCTRL2            101
#define IDD_GPSSVR_DIALOG               102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDR_MENU_TRAY                   130
#define IDB_MATRIXLARGE                 264
#define IDB_MATRIXSMALL                 265
#define IDB_MATRIXTINY                  266
#define IDC_STATIC_GPSTIME              1000
#define IDC_COMBO_PORT                  1001
#define IDC_COMBO_BAUD                  1002
#define IDC_COMBO_DATA                  1003
#define IDC_COMBO_PARITY                1004
#define IDC_BUTTON_START                1005
#define IDC_STATIC_LOCALTIME            1006
#define IDC_COMBO_STOP                  1007
#define IDC_COMBO_FLOW                  1008
#define IDC_BUTTON_STOP                 1009
#define IDC_STATIC_STATUS               1010
#define IDC_EDIT_INTERVAL               1011
#define IDC_RADIO_INTERVAL              1012
#define IDC_RADIO_ATTIME                1013
#define IDC_DATETIME_ATTIME             1014
#define ID_TRAYMENU_SHOW                32771
#define ID_TRAYMENU_EXIT                32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
