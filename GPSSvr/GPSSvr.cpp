// GPSSvr.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GPSSvr.h"
#include "GPSSvrDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrApp

BEGIN_MESSAGE_MAP(CGPSSvrApp, CWinApp)
	//{{AFX_MSG_MAP(CGPSSvrApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrApp construction

CGPSSvrApp::CGPSSvrApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGPSSvrApp object

CGPSSvrApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrApp initialization

BOOL CGPSSvrApp::InitInstance()
{
	m_hMutex = CreateMutex(NULL, TRUE, "GPSSvr");
	if(ERROR_ALREADY_EXISTS == GetLastError())
	{
		return FALSE;
	}

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	m_pszProfileName = _tcsdup(_T("./GPSConf.ini"));

	LoadINIParam();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CGPSSvrDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void CGPSSvrApp::LoadINIParam()
{
	CString str;
	
	//SYSTEM
	str = GetProfileString(_T("SYSTEM"), _T("DEFAULTPORT"), "");
	if (str != "") {
		m_strDefaultPort = str;
	}
}

int CGPSSvrApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	ReleaseMutex(m_hMutex);
	
	return CWinApp::ExitInstance();
}
