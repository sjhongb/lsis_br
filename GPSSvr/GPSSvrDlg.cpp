// GPSSvrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GPSSvr.h"
#include "GPSSvrDlg.h"
#include "GPSComm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrDlg dialog

#define TICK_TIMER_ID		100
#define AUTOSTART_TIMER_ID	200
#define REFRESH_TIMER_ID	300

CGPSSvrDlg::CGPSSvrDlg(CWnd* pParent /*=NULL*/)
	: CTrayDialog(CGPSSvrDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGPSSvrDlg)
	m_nSyncInterval = 60;
	m_nSyncMode = 0;
	m_timeTarget = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pGPSComm = NULL;

	memset(&m_LastSentTime, 0, sizeof(m_LastSentTime));
	memset(&m_PrevTime, 0, sizeof(m_PrevTime));
	m_nGPSAliveCheckCnt = 0;
}

void CGPSSvrDlg::DoDataExchange(CDataExchange* pDX)
{
	CTrayDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGPSSvrDlg)
	DDX_Control(pDX, IDC_COMBO_PORT, m_ctrlComboPort);
	DDX_Control(pDX, IDC_COMBO_STOP, m_ctrlComboStop);
	DDX_Control(pDX, IDC_COMBO_PARITY, m_ctrlComboParity);
	DDX_Control(pDX, IDC_COMBO_FLOW, m_ctrlComboFlow);
	DDX_Control(pDX, IDC_COMBO_DATA, m_ctrlComboData);
	DDX_Control(pDX, IDC_COMBO_BAUD, m_ctrlComboBaud);
	DDX_Control(pDX, IDC_STATIC_GPSTIME, m_ctrlUTCTime);
	DDX_Control(pDX, IDC_STATIC_LOCALTIME, m_ctrlLocalTime);
	DDX_Text(pDX, IDC_EDIT_INTERVAL, m_nSyncInterval);
	DDV_MinMaxInt(pDX, m_nSyncInterval, 1, 1440);
	DDX_Radio(pDX, IDC_RADIO_INTERVAL, m_nSyncMode);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_ATTIME, m_timeTarget);
	//}}AFX_DATA_MAP

	DDX_GridControl(pDX, IDC_CUSTOM_GRIDCTRL, m_ctrlCBIGrid);
}

BEGIN_MESSAGE_MAP(CGPSSvrDlg, CTrayDialog)
	//{{AFX_MSG_MAP(CGPSSvrDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RADIO_INTERVAL, OnRadioInterval)
	ON_BN_CLICKED(IDC_RADIO_ATTIME, OnRadioAttime)
	ON_WM_CLOSE()
	ON_COMMAND(ID_TRAYMENU_SHOW, OnTraymenuShow)
	ON_COMMAND(ID_TRAYMENU_EXIT, OnTraymenuExit)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_REQ_TIME_FROM_CBI, OnReqTimeFromCBI)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrDlg message handlers

BOOL CGPSSvrDlg::OnInitDialog()
{
	CTrayDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	TraySetIcon(IDR_MAINFRAME);
	TraySetToolTip("LSIS GPS Clock Server");
	TraySetMenu(IDR_MENU_TRAY);

	// TODO: Add extra initialization here
	InitSerialPortList();

	CString strComPort = AfxGetApp()->GetProfileString(_T("SYSTEM"), _T("DEFAULTPORT"), "");
	if(strComPort == "")
	{
		m_ctrlComboPort.SetCurSel(0);	// first COM port.
	}
	else
	{
		m_ctrlComboPort.SelectString(0, strComPort);
	}
	m_ctrlComboBaud.SelectString(0, "9600");
	m_ctrlComboData.SelectString(0, "8 bit");
	m_ctrlComboParity.SelectString(0, "none");
	m_ctrlComboStop.SelectString(0, "1 bit");
	m_ctrlComboFlow.SelectString(0, "no");

	m_ctrlUTCTime.SetNumberOfLines(1);
	m_ctrlUTCTime.SetXCharsPerLine(22);
	m_ctrlUTCTime.SetSize(CMatrixStatic::SMALL);
	m_ctrlUTCTime.AdjustClientXToSize(22);
	m_ctrlUTCTime.AdjustClientYToSize(1);
	m_ctrlUTCTime.SetText(" 0000/00/00 00;00;00 ");

	m_ctrlLocalTime.SetNumberOfLines(1);
	m_ctrlLocalTime.SetXCharsPerLine(22);
	m_ctrlLocalTime.SetSize(CMatrixStatic::SMALL);
	m_ctrlLocalTime.AdjustClientXToSize(22);
	m_ctrlLocalTime.AdjustClientYToSize(1);
	m_ctrlLocalTime.SetText(" 0000/00/00 00;00;00 ");

	m_ctrlCBIGrid.SetLSEventMode(TRUE);
	m_ctrlCBIGrid.SetEditable(FALSE);
	m_ctrlCBIGrid.SetListMode(TRUE);
	m_ctrlCBIGrid.SetSingleRowSelection(TRUE);
	m_ctrlCBIGrid.SetHeaderSort(FALSE);
	m_ctrlCBIGrid.EnableDragAndDrop(FALSE);
	m_ctrlCBIGrid.SetTextBkColor(RGB(0xFF, 0xFF, 0xFF));
	m_ctrlCBIGrid.SetDoubleBuffering(TRUE);
	
	m_ctrlCBIGrid.SetRowResize(FALSE);
	m_ctrlCBIGrid.SetColumnResize(FALSE);
	m_ctrlCBIGrid.SetRowCount(1);
	m_ctrlCBIGrid.SetColumnCount(6);
	m_ctrlCBIGrid.SetFixedRowCount(1);
	m_ctrlCBIGrid.SetFixedColumnCount(0);
	
	m_ctrlCBIGrid.SetItemText(0, 0, "NO.");
	m_ctrlCBIGrid.SetItemText(0, 1, "STATION");
	m_ctrlCBIGrid.SetItemText(0, 2, "CBI #1 IP");
	m_ctrlCBIGrid.SetItemText(0, 3, "CON");
	m_ctrlCBIGrid.SetItemText(0, 4, "CBI #2 IP");
	m_ctrlCBIGrid.SetItemText(0, 5, "CON");

	RECT rectClient;
	m_ctrlCBIGrid.GetClientRect(&rectClient);
	m_ctrlCBIGrid.SetColumnWidth(0, 40);
	m_ctrlCBIGrid.SetColumnWidth(1, rectClient.right - 320);
	m_ctrlCBIGrid.SetColumnWidth(2, 100);
	m_ctrlCBIGrid.SetColumnWidth(3, 40);
	m_ctrlCBIGrid.SetColumnWidth(4, 100);
	m_ctrlCBIGrid.SetColumnWidth(5, 40);
	
	LOGFONT lf;
	CFont Font;
	m_ctrlCBIGrid.GetFont()->GetLogFont(&lf);
	memset(lf.lfFaceName, 0, sizeof(lf.lfFaceName));
	strcpy(lf.lfFaceName, "Verdana");
	Font.CreateFontIndirect(&lf);
	m_ctrlCBIGrid.SetFont(&Font);
	Font.DeleteObject();
	m_ctrlCBIGrid.GetNewRowsHeight();
	m_UDPAgent.Initialize(m_hWnd);

	int nRow = 0;
	CString strIndex;

	for(int i = 0 ; i < m_UDPAgent.m_arrCBIList.GetSize() ; i++)
	{
		strIndex.Format("%d", i + 1);
		nRow = m_ctrlCBIGrid.InsertRow(strIndex);
		m_ctrlCBIGrid.SetItemText(nRow, 1, m_UDPAgent.m_arrCBIList[i].strStn);
		m_ctrlCBIGrid.SetItemText(nRow, 2, m_UDPAgent.m_arrCBIList[i].strCBIIP1);
		m_ctrlCBIGrid.SetItemText(nRow, 3, "  X");
		m_ctrlCBIGrid.SetItemText(nRow, 4, m_UDPAgent.m_arrCBIList[i].strCBIIP2);
		m_ctrlCBIGrid.SetItemText(nRow, 5, "  X");
	}
	
	CString strAutoStart = AfxGetApp()->GetProfileString(_T("SYSTEM"), _T("AUTOSTART"), "NO");
	CString strPort;
	m_ctrlComboPort.GetWindowText(strPort);

	if((strAutoStart == "YES") && !(strPort.IsEmpty()))
	{
		SetTimer(AUTOSTART_TIMER_ID, 500, NULL);
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGPSSvrDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CTrayDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGPSSvrDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CTrayDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGPSSvrDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CGPSSvrDlg::InitSerialPortList()
{
	HKEY h_CommKey;
	LONG Reg_Ret;
	DWORD Size = MAX_PATH;
	char i_str[MAX_PATH];
	
	Reg_Ret = RegOpenKeyEx(HKEY_LOCAL_MACHINE,"HARDWARE\\DEVICEMAP\\SERIALCOMM", 0, KEY_READ | KEY_QUERY_VALUE, &h_CommKey);   
	
	if(Reg_Ret == ERROR_SUCCESS)
	{
		for(int i = 0; Reg_Ret == ERROR_SUCCESS; i++)
		{
			Reg_Ret = RegEnumValue(h_CommKey, i, i_str, &Size, NULL, NULL, NULL, NULL);
			if(Reg_Ret == ERROR_SUCCESS)
			{
				DWORD dwType, dwSize = MAX_PATH;
				char szBuffer[MAX_PATH];
				
				RegQueryValueEx(h_CommKey, i_str, 0, &dwType, (LPBYTE)szBuffer, &dwSize);
				
				m_ctrlComboPort.AddString(szBuffer);  // 리스트 박스에 레지스트리 내용 추가(여기서는 COM PORT)
				//하위 레지스트리 값을 얻을 수 있음.
			}
			
			Size = MAX_PATH;
		}
	}
	RegCloseKey(h_CommKey); 
}

void CGPSSvrDlg::SetSerialPortParam(UINT &nBaudRate, UINT &nParity, UINT &nDataBit, UINT &nStopBit, BOOL &bFlowCtrl)
{
	CString strPort, strBaudRate, strParity, strDataBit, strStopBit, strFlowCtrl;

	m_ctrlComboBaud.GetWindowText(strBaudRate);
	m_ctrlComboParity.GetWindowText(strParity);
	m_ctrlComboData.GetWindowText(strDataBit);
	m_ctrlComboStop.GetWindowText(strStopBit);
	m_ctrlComboFlow.GetWindowText(strFlowCtrl);

	if(strBaudRate == "110")
	{
		nBaudRate = 110;
	}
	else if(strBaudRate == "300")
	{
		nBaudRate = 300;
	}
	else if(strBaudRate == "600")
	{
		nBaudRate = 600;
	}
	else if(strBaudRate == "1200")
	{
		nBaudRate = 1200;
	}
	else if(strBaudRate == "2400")
	{
		nBaudRate = 2400;
	}
	else if(strBaudRate == "4800")
	{
		nBaudRate = 4800;
	}
	else if(strBaudRate == "9600")
	{
		nBaudRate = 9600;
	}
	else if(strBaudRate == "14400")
	{
		nBaudRate = 14400;
	}
	else if(strBaudRate == "19200")
	{
		nBaudRate = 19200;
	}
	else if(strBaudRate == "38400")
	{
		nBaudRate = 38400;
	}
	else if(strBaudRate == "57600")
	{
		nBaudRate = 57600;
	}
	else if(strBaudRate == "115200")
	{
		nBaudRate = 115200;
	}
	else if(strBaudRate == "128000")
	{
		nBaudRate = 128000;
	}
	else if(strBaudRate == "256000")
	{
		nBaudRate = 256000;
	}
		
	if(strParity == "none")
	{
		nParity = 0;
	}
	else if(strParity == "even")
	{
		nParity = 2;
	}
	else if(strParity == "odd")
	{
		nParity = 1;
	}

	if(strDataBit == "7 bit")
	{
		nDataBit = 7;
	}
	else if(strDataBit == "8 bit")
	{
		nDataBit = 8;
	}

	if(strStopBit == "1 bit")
	{
		nStopBit = 1;
	}
	else if(strStopBit == "2 bit")
	{
		nStopBit = 2;
	}

	if(strFlowCtrl == "yes") 
	{
		bFlowCtrl = TRUE;
	}
	else if(strFlowCtrl == "no")
	{
		bFlowCtrl = FALSE;
	}
}

void CGPSSvrDlg::OnButtonStart() 
{
	// TODO: Add your control notification handler code here
	CString	strPort;
	UINT	nBaudRate, nParity, nDataBit, nStopBit;
	BOOL	bFlowCtrl;
	
	m_ctrlComboPort.GetWindowText(strPort);
	if(strPort.IsEmpty())
	{
		AfxMessageBox("No serial port");
		return;
	}
	
	SetSerialPortParam(nBaudRate, nParity, nDataBit, nStopBit, bFlowCtrl);
	
	if(m_pGPSComm != NULL)
	{
		m_pGPSComm->End();	
	}
	
	m_pGPSComm = (CGPSComm*)AfxBeginThread(RUNTIME_CLASS(CGPSComm), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	
	if(m_pGPSComm->Start(strPort, nBaudRate, nParity, nDataBit, nStopBit, bFlowCtrl) == TRUE)
	{
		UpdateData(TRUE);

		GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_BAUD)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_DATA)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_PARITY)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_STOP)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_FLOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_INTERVAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_DATETIME_ATTIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_INTERVAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_ATTIME)->EnableWindow(FALSE);

		m_UDPAgent.Start();
		
		SetTimer(TICK_TIMER_ID, 100, NULL);
		SetTimer(REFRESH_TIMER_ID, 5000, NULL);
	}
	else
	{
		m_pGPSComm = NULL;
		
		CString strMsg;
		AfxMessageBox(strPort + " port already used.");
	}
}

void CGPSSvrDlg::OnButtonStop() 
{
	// TODO: Add your control notification handler code here
	if(m_pGPSComm != NULL)
	{
		m_pGPSComm->End();
		m_pGPSComm = NULL;

		GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(FALSE);	
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_BAUD)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_DATA)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PARITY)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_STOP)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_FLOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_INTERVAL)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_ATTIME)->EnableWindow(TRUE);

		if(m_nSyncMode)
		{
			GetDlgItem(IDC_DATETIME_ATTIME)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_EDIT_INTERVAL)->EnableWindow(TRUE);			
		}
		
		SetDlgItemText(IDC_STATIC_STATUS, "Not Connected");

		KillTimer(TICK_TIMER_ID);
		KillTimer(REFRESH_TIMER_ID);

		m_UDPAgent.Stop();
	}
}

void CGPSSvrDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == TICK_TIMER_ID)
	{
		if(m_pGPSComm != NULL)
		{
			SYSTEMTIME time = m_pGPSComm->GetUTCTime();
			CString strDateTime;
			
			strDateTime.Format(" %04d/%02d/%02d %02d;%02d;%02d ", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
			m_ctrlUTCTime.SetText(strDateTime);
			
			time = m_pGPSComm->GetLocalTime();
			
			strDateTime.Format(" %04d/%02d/%02d %02d;%02d;%02d ", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
			m_ctrlLocalTime.SetText(strDateTime);
			
			CTime curTime(time);
			
			if(curTime != m_PrevTime)
			{
				m_PrevTime = curTime;
				
				m_nGPSAliveCheckCnt = 0;
			}
			else
			{
				m_nGPSAliveCheckCnt++;
			}
			
			if(m_pGPSComm->GetGPSStatus() && (m_nGPSAliveCheckCnt < GPS_ALIVE_CHECK_TIMEOUT))
			{
				SetDlgItemText(IDC_STATIC_STATUS, "Active");
				
				if(m_LastSentTime.GetMinute() == 0)
				{
					m_UDPAgent.SendTimeSyncAll(time);
					m_LastSentTime = curTime;
				}
				else
				{
					if(m_nSyncMode)
					{	
						if(curTime > m_timeTarget)
						{
							m_UDPAgent.SendTimeSyncAll(time);
							m_LastSentTime = curTime;
						}
					}
					else
					{
						CTimeSpan timeSpan = curTime - m_LastSentTime;
						if((timeSpan.GetHours() * 60 + timeSpan.GetMinutes()) > m_nSyncInterval)
						{

							m_UDPAgent.SendTimeSyncAll(time);
							m_LastSentTime = curTime;
						}
					}
				}
			}
			else
			{
				SetDlgItemText(IDC_STATIC_STATUS, "Void");
			}
		}
	}
	else if(nIDEvent == AUTOSTART_TIMER_ID)
	{
		m_ctrlCBIGrid.Invalidate();

		KillTimer(AUTOSTART_TIMER_ID);

		OnButtonStart();

		PostMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0);
	}
	else if(nIDEvent == REFRESH_TIMER_ID)
	{
		if(m_pGPSComm != NULL)
		{
			m_UDPAgent.SendAliveCheck();
		}

		for(int i = 0 ; i < m_UDPAgent.m_arrCBIList.GetSize() ; i++)
		{
			if(m_UDPAgent.m_arrCBIList[i].nCBI1AliveCheckCnt >= CBI_ALIVE_CHECK_TIMEOUT)
			{
				m_ctrlCBIGrid.SetItemText(i+1, 3, "  X");
			}
			else
			{
				m_ctrlCBIGrid.SetItemText(i+1, 3, "  O");
			}

			if(m_UDPAgent.m_arrCBIList[i].nCBI2AliveCheckCnt >= CBI_ALIVE_CHECK_TIMEOUT)
			{
				m_ctrlCBIGrid.SetItemText(i+1, 5, "  X");
			}
			else
			{
				m_ctrlCBIGrid.SetItemText(i+1, 5, "  O");
			}
		}
		
		m_ctrlCBIGrid.Invalidate();
	}

	CTrayDialog::OnTimer(nIDEvent);
}

void CGPSSvrDlg::OnRadioInterval() 
{
	// TODO: Add your control notification handler code here
	m_nSyncMode = 0;
	UpdateData(FALSE);

	GetDlgItem(IDC_EDIT_INTERVAL)->EnableWindow(TRUE);
	GetDlgItem(IDC_DATETIME_ATTIME)->EnableWindow(FALSE);
}

void CGPSSvrDlg::OnRadioAttime() 
{
	// TODO: Add your control notification handler code here
	m_nSyncMode = 1;
	UpdateData(FALSE);

	GetDlgItem(IDC_EDIT_INTERVAL)->EnableWindow(FALSE);
	GetDlgItem(IDC_DATETIME_ATTIME)->EnableWindow(TRUE);
}

void CGPSSvrDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0);
	//	CTrayDialog::OnClose();
}

void CGPSSvrDlg::OnTraymenuShow() 
{
	// TODO: Add your command handler code here
	RestoreDialog();
}

void CGPSSvrDlg::OnTraymenuExit() 
{
	// TODO: Add your command handler code here
	OnButtonStop();

	OnOK();
}

void CGPSSvrDlg::OnReqTimeFromCBI(WPARAM wParam, LPARAM lParam)
{
	int nCBIIndex = (UINT)wParam;

	if(m_pGPSComm != NULL)
	{
		SYSTEMTIME time = m_pGPSComm->GetLocalTime();
		
		if(m_pGPSComm->GetGPSStatus() && (m_nGPSAliveCheckCnt < GPS_ALIVE_CHECK_TIMEOUT))
		{
			m_UDPAgent.SendTimeSync(time, nCBIIndex);
		}
	}
}
