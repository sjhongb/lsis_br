#if !defined(AFX_UDPAGENT_H__2D06D8FD_0750_4B13_A765_EF5688CDCAD0__INCLUDED_)
#define AFX_UDPAGENT_H__2D06D8FD_0750_4B13_A765_EF5688CDCAD0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDPAgent.h : header file
//

typedef struct _ST_CBI_INFO {
	BOOL	bUse;
	CString strStn;
	CString strCBIIP1;
	CString strCBIIP2;
	UINT	nCBI1AliveCheckCnt;
	UINT	nCBI2AliveCheckCnt;
} ST_CBI_INFO;

#define GPS_ALIVE_CHECK_TIMEOUT	20
#define CBI_ALIVE_CHECK_TIMEOUT	5

#define WM_REQ_TIME_FROM_CBI	WM_USER + 1000

/////////////////////////////////////////////////////////////////////////////
// CUDPAgent command target

class CUDPAgent : public CAsyncSocket
{
// Attributes
public:
	CArray<ST_CBI_INFO, ST_CBI_INFO> m_arrCBIList;
	CString m_strServerAddr;

// Operations
public:
	CUDPAgent();
	virtual ~CUDPAgent();

// Overrides
public:
	void Stop();
	void Initialize(HWND hWnd);
	void SendTimeSync(SYSTEMTIME &time, int nCBIIndex);
	void SendTimeSyncAll(SYSTEMTIME &time);
	void SendAliveCheck();
	BOOL Start();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUDPAgent)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CUDPAgent)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:

private:
	HWND m_hMainWnd;
	BYTE m_cSeqNo;

	void MakeCRC(BYTE* sndBuffer, int rcvLen, BYTE* crcLow, BYTE* crcHigh);
	BOOL CheckCRC(BYTE* rcvBuffer, int rcvlen);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDPAGENT_H__2D06D8FD_0750_4B13_A765_EF5688CDCAD0__INCLUDED_)
