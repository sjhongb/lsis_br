// GPSComm.cpp: implementation of the CGPSComm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GPSSvr.h"
#include "GPSComm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CGPSComm, CSerialComm)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGPSComm::CGPSComm()
{
	memset(&m_GPSTime, 0, sizeof(m_GPSTime));
	memset(m_pRcvBuffer, 0, MAX_BUFFER_LEN);
	m_nRcvLen = 0;
	m_bGPSOK = FALSE;

	InitializeCriticalSection(&m_CSTime);
}

CGPSComm::~CGPSComm()
{
	DeleteCriticalSection(&m_CSTime);
}

void CGPSComm::ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen)
{
	if(MAX_BUFFER_LEN < (m_nRcvLen + nMsgLen))
	{
		memset(m_pRcvBuffer, 0, MAX_BUFFER_LEN);
		m_nRcvLen = 0;
	}

	memcpy(m_pRcvBuffer + m_nRcvLen, pBuffer, nMsgLen);
	m_nRcvLen += nMsgLen;
	nMsgLen = 0;	// Call by Reference로 받은 값이므로 반드시 0을 만들어준다.

	CString strRcvData, strMsg, strToken;
	UINT nHour, nMin, nSec, nYear, nMon, nDay;
	BOOL bStatusOK = FALSE;

	strRcvData = m_pRcvBuffer;

	int nPos = 0, nCurPos = 0, nNextPos = 0, nTimePos = 0, nDatePos = 0;

	nCurPos = strRcvData.Find('$', 0);
	while(nCurPos >= 0)
	{
		nNextPos = strRcvData.Find('$', nCurPos + 1);
		if(nNextPos > 0)
		{
			strMsg = strRcvData.Mid(nCurPos, nNextPos - nCurPos);
			nCurPos = nNextPos;

			if(strMsg.Left(6) == "$GPRMC")
			{
				nPos = strMsg.Find(',', 0);	// UTC Time
				if(nPos < 0)
				{
					break;
				}

				nTimePos = nPos + 1;

				nPos = strMsg.Find(',', nPos + 1);	// Status 
				if(nPos < 0)
				{
					break;
				}

				if(strMsg[nPos+1] == 'A')
				{
					bStatusOK = TRUE;
				}
				else
				{
					bStatusOK = FALSE;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Latitude
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Latitude (bearing)
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Longitude 
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Longitude (bearing)
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Speed over the ground in knots
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Track angle in degrees True
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Date
				if(nPos < 0)
				{
					break;
				}

				nDatePos = nPos + 1;
				
				nPos = strMsg.Find(',', nPos + 1);	// Magnetic Variation
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Magnetic Variation (bearing)
				if(nPos < 0)
				{
					break;
				}

				nPos = strMsg.Find(',', nPos + 1);	// Checksum
				if(nPos < 0)
				{
					break;
				}

				m_bGPSOK = bStatusOK;

				if(m_bGPSOK)
				{
					strToken = strMsg.Mid(nTimePos, 2);
					nHour = atoi(strToken);
					
					strToken = strMsg.Mid(nTimePos + 2, 2);
					nMin = atoi(strToken);
					
					strToken = strMsg.Mid(nTimePos + 4, 2);
					nSec = atoi(strToken);
					
					strToken = strMsg.Mid(nDatePos, 2);
					nDay = atoi(strToken);
					
					strToken = strMsg.Mid(nDatePos + 2, 2);
					nMon = atoi(strToken);
					
					strToken = strMsg.Mid(nDatePos + 4, 2);
					nYear = atoi(strToken);

					EnterCriticalSection(&m_CSTime);

					m_GPSTime.wYear = 2000 + nYear;
					m_GPSTime.wMonth	= nMon;
					m_GPSTime.wDay		= nDay;
					m_GPSTime.wHour		= nHour;
					m_GPSTime.wMinute	= nMin;
					m_GPSTime.wSecond	= nSec;

					LeaveCriticalSection(&m_CSTime);
				}
			}
		}
		else
		{
			break;
		}
	}

	if(nCurPos > 0)
	{
		m_nRcvLen -= nCurPos;

		if(m_nRcvLen > 0)
		{
			memcpy(m_pRcvBuffer, m_pRcvBuffer + nCurPos, m_nRcvLen);
			memset(m_pRcvBuffer + m_nRcvLen, 0, MAX_BUFFER_LEN - m_nRcvLen);
		}
	}
}

BOOL CGPSComm::SendMsg(BYTE *pBuffer, USHORT nMsgLen)
{
	return CSerialComm::SendMsg(pBuffer, nMsgLen);
}

SYSTEMTIME CGPSComm::GetUTCTime()
{
	SYSTEMTIME UtcTime;
	memset(&UtcTime, 0, sizeof(UtcTime));

	EnterCriticalSection(&m_CSTime);
	
	memcpy(&UtcTime, &m_GPSTime, sizeof(m_GPSTime));
				
	LeaveCriticalSection(&m_CSTime);

	return UtcTime;
}

SYSTEMTIME CGPSComm::GetLocalTime()
{
	SYSTEMTIME UtcTime, LocalTime;
	memset(&UtcTime, 0, sizeof(UtcTime));
	memset(&LocalTime, 0, sizeof(LocalTime));
	
	EnterCriticalSection(&m_CSTime);
	
	memcpy(&UtcTime, &m_GPSTime, sizeof(m_GPSTime));
				
	LeaveCriticalSection(&m_CSTime);

	TIME_ZONE_INFORMATION timeZoneInfo;
	memset(&timeZoneInfo, 0, sizeof(timeZoneInfo));

	GetTimeZoneInformation(&timeZoneInfo);

	SystemTimeToTzSpecificLocalTime(&timeZoneInfo, &UtcTime, &LocalTime);
	
	return LocalTime;
}

BOOL CGPSComm::GetGPSStatus()
{
	return m_bGPSOK;
}
