// UDPAgent.cpp : implementation file
//

#include "stdafx.h"
#include "GPSSvr.h"
#include "UDPAgent.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define DLE				0x10
#define STX				0x02
#define ETX				0x03
#define OPCODE_REQTIME	0x90
#define OPCODE_STDTIME	0x91
#define OPCODE_AYT		0xAC
#define OPCODE_ACK		0xA0


/////////////////////////////////////////////////////////////////////////////
// CUDPAgent

CUDPAgent::CUDPAgent()
{
	m_cSeqNo = 0x01;
}

CUDPAgent::~CUDPAgent()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CUDPAgent, CAsyncSocket)
	//{{AFX_MSG_MAP(CUDPAgent)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CUDPAgent member functions

BOOL CUDPAgent::Start()
{
	if(Create(9500, SOCK_DGRAM, FD_READ|FD_WRITE, m_strServerAddr) == 0)
	{
		return TRUE;
	}

	return FALSE;
}


void CUDPAgent::Stop()
{
	Close();
}


void CUDPAgent::SendTimeSyncAll(SYSTEMTIME &time)
{
	BYTE pSendBuf[4096] = {0,};
	BYTE cCRCLow = 0, cCRCHigh = 0;

	pSendBuf[0] = DLE;
	pSendBuf[1] = STX;
	pSendBuf[2] = 0x09;	// STN_NO(1) + SEQ(1) + OPCODE(1) + DATA(6)
	pSendBuf[3] = 0x00;
	pSendBuf[4] = 0x00;			// STN_NO
	pSendBuf[5]	= m_cSeqNo++;	// SEQ
	if(m_cSeqNo == 0)
	{
		m_cSeqNo++;
	}
	pSendBuf[6]	= OPCODE_STDTIME;	// OPCODE (STD_TIME)
	pSendBuf[7] = (BYTE)(time.wYear - 2000);
	pSendBuf[8] = (BYTE)time.wMonth;
	pSendBuf[9] = (BYTE)time.wDay;
	pSendBuf[10] = (BYTE)time.wHour;
	pSendBuf[11] = (BYTE)time.wMinute;
	pSendBuf[12] = (BYTE)time.wSecond;

	MakeCRC(pSendBuf, 13, &cCRCLow, &cCRCHigh);

	pSendBuf[13] = cCRCLow;
	pSendBuf[14] = cCRCHigh;
	pSendBuf[15] = DLE;
	pSendBuf[16] = ETX;

	for(int i = 0 ; i < m_arrCBIList.GetSize() ; i++)
	{
		if(m_arrCBIList[i].bUse == TRUE)
		{
			SendTo(pSendBuf, 17, 9500, m_arrCBIList[i].strCBIIP1, 0);
			SendTo(pSendBuf, 17, 9500, m_arrCBIList[i].strCBIIP2, 0);
		}
	}
}

void CUDPAgent::SendTimeSync(SYSTEMTIME &time, int nCBIIndex)
{
	if(nCBIIndex >= m_arrCBIList.GetSize())
	{
		return;
	}

	BYTE pSendBuf[4096] = {0,};
	BYTE cCRCLow = 0, cCRCHigh = 0;
	
	pSendBuf[0] = DLE;
	pSendBuf[1] = STX;
	pSendBuf[2] = 0x09;	// STN_NO(1) + SEQ(1) + OPCODE(1) + DATA(6)
	pSendBuf[3] = 0x00;
	pSendBuf[4] = 0x00;			// STN_NO
	pSendBuf[5]	= m_cSeqNo++;	// SEQ
	if(m_cSeqNo == 0)
	{
		m_cSeqNo++;
	}
	pSendBuf[6]	= OPCODE_STDTIME;	// OPCODE (STD_TIME)
	pSendBuf[7] = (BYTE)(time.wYear - 2000);
	pSendBuf[8] = (BYTE)time.wMonth;
	pSendBuf[9] = (BYTE)time.wDay;
	pSendBuf[10] = (BYTE)time.wHour;
	pSendBuf[11] = (BYTE)time.wMinute;
	pSendBuf[12] = (BYTE)time.wSecond;
	
	MakeCRC(pSendBuf, 13, &cCRCLow, &cCRCHigh);
	
	pSendBuf[13] = cCRCLow;
	pSendBuf[14] = cCRCHigh;
	pSendBuf[15] = DLE;
	pSendBuf[16] = ETX;
	
	if(m_arrCBIList[nCBIIndex].bUse == TRUE)
	{
		SendTo(pSendBuf, 17, 9500, m_arrCBIList[nCBIIndex].strCBIIP1, 0);
		SendTo(pSendBuf, 17, 9500, m_arrCBIList[nCBIIndex].strCBIIP2, 0);
	}
}

void CUDPAgent::SendAliveCheck()
{
	BYTE pSendBuf[4096] = {0,};
	BYTE cCRCLow = 0, cCRCHigh = 0;
	
	pSendBuf[0] = DLE;
	pSendBuf[1] = STX;
	pSendBuf[2] = 0x09;	// STN_NO(1) + SEQ(1) + OPCODE(1) + DATA(6)
	pSendBuf[3] = 0x00;
	pSendBuf[4] = 0x00;			// STN_NO
	pSendBuf[5]	= m_cSeqNo++;	// SEQ
	if(m_cSeqNo == 0)
	{
		m_cSeqNo++;
	}
	pSendBuf[6]	= OPCODE_AYT;	// OPCODE (AYT)
	
	MakeCRC(pSendBuf, 7, &cCRCLow, &cCRCHigh);
	
	pSendBuf[7] = cCRCLow;
	pSendBuf[8] = cCRCHigh;
	pSendBuf[9] = DLE;
	pSendBuf[10] = ETX;
	
	for(int i = 0 ; i < m_arrCBIList.GetSize() ; i++)
	{
		if(m_arrCBIList[i].bUse == TRUE)
		{
			SendTo(pSendBuf, 11, 9500, m_arrCBIList[i].strCBIIP1, 0);
			SendTo(pSendBuf, 11, 9500, m_arrCBIList[i].strCBIIP2, 0);

			m_arrCBIList[i].nCBI1AliveCheckCnt++;
			m_arrCBIList[i].nCBI2AliveCheckCnt++;
		}
	}
}

void CUDPAgent::Initialize(HWND hWnd)
{
	m_hMainWnd = hWnd;

	ST_CBI_INFO CBIInfo;

	CString strEntryName;
	CString strStnName, strPriAddr, strSecAddr;
	
	int nIndex = 1;

	m_strServerAddr = AfxGetApp()->GetProfileString(_T("SYSTEM"), _T("IPADDRESS"), "");
	
	while(1)
	{
		strEntryName.Format("ID%d", nIndex++);

		strStnName = AfxGetApp()->GetProfileString(_T("STATION_LIST"), strEntryName, "");
		strPriAddr = AfxGetApp()->GetProfileString(_T("PRIMARY_ADDR"), strEntryName, "");
		strSecAddr = AfxGetApp()->GetProfileString(_T("SECONDARY_ADDR"), strEntryName, "");

		if(strStnName == "")
		{
			break;
		}

		CBIInfo.bUse		= TRUE;
		CBIInfo.strStn		= strStnName;
		CBIInfo.strCBIIP1	= strPriAddr;
		CBIInfo.nCBI1AliveCheckCnt	= CBI_ALIVE_CHECK_TIMEOUT;
		CBIInfo.strCBIIP2	= strSecAddr;
		CBIInfo.nCBI2AliveCheckCnt	= CBI_ALIVE_CHECK_TIMEOUT;

		m_arrCBIList.Add(CBIInfo);
	}
}

void CUDPAgent::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	BYTE pRecvBuf[4096] = {0,};
	BYTE cCRCLow = 0, cCRCHigh = 0;
	CString strCBIAddr;
	UINT nCBIPort;

	BYTE nRecvSize = ReceiveFrom(pRecvBuf, 4096, strCBIAddr, nCBIPort, 0);

	if(nRecvSize != 11)
	{
		return;
	}
	
	if((pRecvBuf[0] != DLE) || (pRecvBuf[1] != STX) || (pRecvBuf[9] != DLE) || (pRecvBuf[10] != ETX))
	{
		return;
	}

	MakeCRC(pRecvBuf, 7, &cCRCLow, &cCRCHigh);

	if((pRecvBuf[7] != cCRCLow) || (pRecvBuf[8] != cCRCHigh))
	{
		return;
	}

	for(int i = 0 ; i < m_arrCBIList.GetSize() ; i++)
	{
		if(m_arrCBIList[i].strCBIIP1 == strCBIAddr)
		{
			m_arrCBIList[i].nCBI1AliveCheckCnt = 0;

			if(pRecvBuf[6] == OPCODE_REQTIME)
			{
				PostMessage(m_hMainWnd, WM_REQ_TIME_FROM_CBI, (WPARAM)i, 0);
			}
		}
		else if(m_arrCBIList[i].strCBIIP2 == strCBIAddr)
		{
			m_arrCBIList[i].nCBI2AliveCheckCnt = 0;
			
			if(pRecvBuf[6] == OPCODE_REQTIME)
			{
				PostMessage(m_hMainWnd, WM_REQ_TIME_FROM_CBI, (WPARAM)i, 0);
			}
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}


void CUDPAgent::MakeCRC(BYTE* sndBuffer, int rcvLen, BYTE* crcLow, BYTE* crcHigh )
{
	
	USHORT crct = 0;
	for (USHORT lenIdx = 0; lenIdx < rcvLen; lenIdx++)
	{
		crct = (USHORT)((crct & 0xff00) | (crct ^ (USHORT)(sndBuffer[lenIdx] & 0xff)));
		
		for (BYTE relenIdx = 0; relenIdx < 8; relenIdx++)
		{
			if ((crct & 0x0001) > 0)
			{
				crct = (USHORT)((crct >> 1) ^ 0xa001);
			}
			else
			{
				crct = (USHORT)(crct >> 1);
			}
		}
	}
	
	*crcLow = (BYTE)(crct & 0x00ff);
	*crcHigh = (BYTE)((crct >> 8) & 0xff);
}

BOOL CUDPAgent::CheckCRC(BYTE* rcvBuffer, int rcvlen)
{
	bool crcCheck = false;
	
	BYTE cmpCrcBuffer[2] = {0,};
	
	MakeCRC(rcvBuffer, rcvlen, &cmpCrcBuffer[0], &cmpCrcBuffer[1]);
	
	if ((cmpCrcBuffer[0] == rcvBuffer[rcvlen]) &&
		(cmpCrcBuffer[1] == rcvBuffer[rcvlen + 1]))
		crcCheck = true;
	
	return crcCheck;
}
