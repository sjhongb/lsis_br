// GPSSvrDlg.h : header file
//

#if !defined(AFX_GPSSVRDLG_H__61BAF20B_F764_4DF5_88F1_096F3EA3581A__INCLUDED_)
#define AFX_GPSSVRDLG_H__61BAF20B_F764_4DF5_88F1_096F3EA3581A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GPSComm.h"	// Added by ClassView
#include "MatrixStatic.h"
#include "../include/GridCtrl.h"
#include "UDPAgent.h"	// Added by ClassView
#include "TrayDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CGPSSvrDlg dialog

class CGPSSvrDlg : public CTrayDialog
{
// Construction
public:
	CGPSComm *m_pGPSComm;
	CGPSSvrDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CGPSSvrDlg)
	enum { IDD = IDD_GPSSVR_DIALOG };
	CComboBox	m_ctrlComboPort;
	CComboBox	m_ctrlComboStop;
	CComboBox	m_ctrlComboParity;
	CComboBox	m_ctrlComboFlow;
	CComboBox	m_ctrlComboData;
	CComboBox	m_ctrlComboBaud;
	CMatrixStatic	m_ctrlUTCTime;
	CMatrixStatic	m_ctrlLocalTime;
	short	m_nSyncInterval;
	int		m_nSyncMode;
	CTime	m_timeTarget;
	//}}AFX_DATA
	CGridCtrl	m_ctrlCBIGrid;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGPSSvrDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CGPSSvrDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonStart();
	afx_msg void OnButtonStop();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRadioInterval();
	afx_msg void OnRadioAttime();
	afx_msg void OnClose();
	afx_msg void OnTraymenuShow();
	afx_msg void OnTraymenuExit();
	//}}AFX_MSG
	afx_msg void OnReqTimeFromCBI(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CTime m_LastSentTime;
	CTime m_PrevTime;
	CUDPAgent m_UDPAgent;
	UINT m_nGPSAliveCheckCnt;

	void InitSerialPortList();
	void SetSerialPortParam(UINT &nBaudRate, UINT &nParity, UINT &nDataBit, UINT &nStopBit, BOOL &bFlowCtrl);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GPSSVRDLG_H__61BAF20B_F764_4DF5_88F1_096F3EA3581A__INCLUDED_)
