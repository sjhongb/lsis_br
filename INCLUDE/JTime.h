// CJTime.h : header file
//

#ifndef   _CJTIME_H
#define   _CJTIME_H

// Base is 2000. 1. 1 (00:00:00)
// 
//

/////////////////////////////////////////////////////////////////////////////
#define MAX_SECOND_FOR_DAY		86400

/////////////////////////////////////////////////////////////////////////////
#define CJTIME_BASE_YEAR		2000

/////////////////////////////////////////////////////////////////////////////

class CJTime 
{
// Attributes
public:
	BYTE nSec;
	BYTE nMin;
	BYTE nHour;
//	BYTE nDayOfWeek;
	BYTE nDate;
	BYTE nMonth;
	BYTE nYear;

// Operations
public:
	int  GetYear()
	{
		return int(nYear) + CJTIME_BASE_YEAR;		// Based 2000 Year.
	}
	void SetYear( int year )
	{
		year -= CJTIME_BASE_YEAR;			// Based 2000 Year.
		if ( year < 0 ) year = 0;
		nYear = year;
	}
	long GetTime();
	long GetDays();
	long GetSecInDay();
	int  GetMinuteInDay();
	BOOL isLeapYear(int year);
	CString Format( const char *lpszFormat );

public:
	static CJTime GetCurrentTime();

public:
	CJTime& operator = ( long );
	CJTime& operator = ( const CJTime & );
// Implementation
public:
	CJTime();           // constructor used by dynamic creation
	CJTime( long lt ); 
	CJTime( const CJTime & ); 
	CJTime( int year, int month, int day, int hour, int min, int sec ); 

	~CJTime() {};
};

/////////////////////////////////////////////////////////////////////////////

BOOL operator == ( CJTime &obj1, CJTime &obj2 );
BOOL operator != ( CJTime &obj1, CJTime &obj2 );
BOOL operator >= ( CJTime &obj1, CJTime &obj2 );
BOOL operator <= ( CJTime &obj1, CJTime &obj2 );
BOOL operator > ( CJTime &obj1, CJTime &obj2 );
BOOL operator < ( CJTime &obj1, CJTime &obj2 );

/////////////////////////////////////////////////////////////////////////////
#endif // _CJTIME_H
