//=======================================================
//==              FMSGList.cpp
//=======================================================
//  파 일 명 :  FMSGList.cpp
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//				1. 2001.8.24 
//				- 각종 메시지 문자열 데이터를 영문화하고 메시지 파일의 이름을 한글용, 영
//				  문용으로 구분하여 파일처리하도록 코드 변경하였음(즉, 언어별 메시지 파일
//				  생성처리함)
//=======================================================

#include "stdafx.h"
#include <fstream.h>
#include <strstrea.h>
#include "Parser.h"
#include "scrinfo.h"
#include "OPMsgCode.h"
#include "FMSGList.h"
#include "../LES/LES.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int _LineNo = 0;

	const char *_pStrMSG_NonoList = { "not exist message list file." };
	const char *_pStrMSG_Unkown = { "Unknown Message" };
	const char *_pCardTypeName[] = {
		"UNKNOWN",	// 0
		"IN",		// 1
		"OUT",		// 2
		"SDM",		// 3
		"OPT",		// 4
		"IOC",		// 5
		"CPU",		// 6
		"COM",		// 7
		" "
	};

	const char *_pEipMsgFileName = "LISMSGKR.TXT";

/////////////////////////////////////////////////////////////////////////////
// CFMSGList

CFMSGList::CFMSGList()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	
	m_bHelpOn = FALSE;
	Init();
	if (pApp->m_pEIPMsgList == NULL) {
		pApp->m_pEIPMsgList = this;
	}
}

CFMSGList::~CFMSGList()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();

	Flush();
	if (pApp->m_pEIPMsgList != NULL) {
		if (pApp->m_pEIPMsgList == this) {
			pApp->m_pEIPMsgList = NULL;
		}
	}
}

FileMsgItem *CFMSGList::GetItem(int no)
{
	if ((no>=0) && (no<MAX_FMSG_LIST)) {
		return m_MsgList[no];
	}
	return NULL;
}

BYTE CFMSGList::GetMode(int no)
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();

	FileMsgItem *msgItem = pApp->m_pEIPMsgList->GetItem(no);
	if ( msgItem ) return msgItem->uMode;
	return 0;
}

BOOL CFMSGList::SetItem(int no, BYTE mode, const char *Msg, const char *Help)
{
	if ((no>=0) && (no<MAX_FMSG_LIST)) {		//1000
		if ( m_MsgList[no] ) {
			CString s;
			s.Format("Warning!"
					 "\n Duplicate data exists in message file(\"%s\"). (\"%d-%d\")"
					 , _pEipMsgFileName, _LineNo, no);
			AfxMessageBox(s);
			return FALSE;
		}
		else {
			FileMsgItem *p = new FileMsgItem;
			m_MsgList[no] = p;
			if ( p ) {
				p->uMode = mode;
				if ( Msg ) p->Msg  = Msg;
				if ( Help ) p->Help = Help;
				return TRUE;
			}
		}
	}
	return FALSE;
}

void CFMSGList::GetMessage(CString &string, MessageEvent *pEvent)
{
	MsgTimeStrings Msgs;

	GetMessage( Msgs, pEvent );

	string = ' ';
	string += Msgs.strType;
	string += "   ";
	string += Msgs.strTime;
	string += "   ";
	string += Msgs.strMsgNo;
	string += "   ";
	string += Msgs.strMessage;

	if ( m_bHelpOn && (Msgs.strHelp != "") ) {
		string += "  --  ";
		string += Msgs.strHelp;
	}
}

void CFMSGList::GetMessage(MsgTimeStrings &strings, MessageEvent *pEvent)
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();

	CString First, L, Last, appmsg;
	int  n;
	CString strName, strMsg1, strMsg2;

	strings.strTime  = pEvent->time.Format("%d/%m/%Y  %H:%M:%S");
	strName.Format(".%2.2X", (int)(pEvent->hsec));
	strings.strTime += strName;
	strName = "";

	strings.strMsgNo.Format("%3.3d", (int)(pEvent->msgno));

	strings.strType = "NONE";
	strings.strHelp = "";

	if (pApp->m_pEIPMsgList == NULL) { // 메시지 도움말 검색
		strings.strMessage = _pStrMSG_NonoList;		//"메세지 목록 파일 없음"
		return;
	}
	FileMsgItem *msgItem = GetItem(pEvent->msgno);
	if (msgItem == NULL) {
		strings.strMessage = _pStrMSG_Unkown;		//"알수 없는 메세지 "
		return;
	}

	switch (pEvent->Type.nMode) {	// 메시지 타입 문구 삽입 // 상황 분류
	case MSG_MODE_ERROR:			// 장애관련 메세지				1
		strings.strType = "ERROR  ";
		break;
	case MSG_MODE_INTERLOCKING:		// 연동관련 메세지		2
		strings.strType = "STATUS ";
		break;
	case MSG_MODE_OPERATE:			// 제어관련 메세지		4
		strings.strType = "CONTROL";
		break;
	case MSG_MODE_MESSAGE:			// 기타(시스템) 메세지	  8
	default:
		strings.strType = "ETC    ";
		break;
	}

	strName = pEvent->name;				// 메시지 대상명 삽입
	strName += " ";
	strMsg2 = pEvent->EndOfMsg;

	switch ( pEvent->msgno ) {
	case MSGNO_TRACK_OP_LOCK:			//202 : track operation lock
		break;
	case MSGNO_TRACK_OP_FREE:			
		break;
	case MSGNO_SIGNAL_ON:				
	case MSGNO_SIGNAL_APPROACH_SLOCK:	
	case MSGNO_SIGNAL_APPROACH_ELOCK:	
		//strMsg2.Format("(%d Second)", (int)(pEvent->item));
		break;
	case MSGNO_ERR_CARDFAIL:			//25 
	case MSGNO_ERR_CARDRECOVERY:		//26
		strMsg1.Format(" %d계 ", pEvent->op);
		break;
	case MSGNO_ERR_MODULE_RECOVERY:		//50
	case MSGNO_ERR_MODULE_FAULT:		//1
//		if ( pEvent->exloc.nType == 5 ) 
//			strMsg1.Format("%d Rack %d 계 ", pEvent->loc.nRack+1, pEvent->loc.nMain+1);
//		else 
//			strMsg1.Format("%d계 %s ", pEvent->exloc.nMain+1, _pCardTypeName[pEvent->exloc.nType]);
//		strName += "카드 ";
		break;
	case MSGNO_ERR_IO_PORT_UNVAL:	//5:	I, II 계 입출력 포트 불일치.
//		strName.Format("%d : %s Card %d Port %d", pEvent->exloc.nMain+1, _pCardTypeName[pEvent->exloc.nType], 
//												pEvent->exloc.nCard, pEvent->exloc.nPort+1);
			strName.Format("FDIM%d port %d ",pEvent->exloc.nCard,pEvent->exloc.nPort+1);
		break;
	case MSGNO_ERR_IOFAULTOUT:
			if ( pEvent->exloc.nCard == 0 ) strName.Format("SFDOM%d port %d ",pEvent->exloc.nCard,pEvent->exloc.nPort+1);
			else if ( pEvent->exloc.nCard == 11 ) strName.Format("FDBM%d port %d ",pEvent->exloc.nCard,pEvent->exloc.nPort+1);
			else strName.Format("FDOM%d port %d ",pEvent->exloc.nCard,pEvent->exloc.nPort+1);
//			strName.Format("%d : %s Card %d port %d", pEvent->exloc.nMain+1, _pCardTypeName[pEvent->exloc.nType], 
//													pEvent->exloc.nCard, pEvent->exloc.nPort+1);
	case MSGNO_ERR_SIG_S3_FAULTOUT:		//65:	"신호기 부정출력검지(S3)"		
	case MSGNO_ERR_SIG_S2_FAULTOUT:		//66:	"신호기 부정출력검지(S2)"
	case MSGNO_ERR_SIG_S23_FAULTOUT:	//67:	"신호기 부정출력검지(S2,S3)"
	case MSGNO_ERR_SIG_S1_FAULTOUT:		//68:	"신호기 부정출력검지(S1)"
	case MSGNO_ERR_SIG_S13_FAULTOUT:	//69:	"신호기 부정출력검지(S1,S3)"
	case MSGNO_ERR_SIG_S12_FAULTOUT:	//70:	"신호기 부정출력검지(S1,S2)"
	case MSGNO_ERR_SIG_S123_FAULTOUT:	//71:	"신호기 부정출력검지(S1,S2,S3)"
		n = 0;
		break;
	default :
		break;
	}

		strings.strMessage  = strMsg1;
		if(strName !=" ")
		strings.strMessage += strName;
		strings.strMessage += msgItem->Msg;
		strings.strMessage += strMsg2;
		if ( pEvent->Mode  & 0x80 ) {	// CTC mode ?
			strings.strMessage += "/CTC";
		}
		strings.strHelp = msgItem->Help;
}

void CFMSGList::Init()
{
	for(int i=0; i<MAX_FMSG_LIST; i++) {
		m_MsgList[i] = NULL;
	}
}

void CFMSGList::Flush()
{
	for(int i=0; i<MAX_FMSG_LIST; i++) {	//1000
		if ( m_MsgList[i] ) {
			delete m_MsgList[i];
			m_MsgList[i] = NULL;
		}
	}
}

BOOL CFMSGList::Create()
{
	BOOL IsNormal = TRUE;
	char bf[512] = { 0 };
	int n, fMode, step=0;
	char *s;
	CString fmsg, fhelp;
	CString warnningmsg;
	CParser Parser;

	ifstream is( _pEipMsgFileName );					// EIPMSGEN.TXT or EIPMSGKR.TXT depend on Korean or English.

	while (!is.eof()) {
		is.getline(bf,511);								// Extract strings from the input stream line-by-line.
		_LineNo++;
		Parser.Create(bf," \t\n\r");					// set string using bf, and set the pdel
		s = Parser.GetToken();							// get first valid string 
		if ( !s );
		else if ( !*s ) ;
		else if (s[0] != ';') {							// if ";", skip. and read next line 
			if (!strcmp(s,"HEADER_1:")) {				// if HEADER_1:, read next string contained with "" in same line.
				s = Parser.GetToken("\"", FALSE);		// read string until occur  "     ( start of string )    \" => Double quotation mark  
				s = Parser.GetToken("\"", FALSE);		// read string unitl occur  "     ( end of string)   so s retain string between " amd "
				if (s) m_Head1 = s;						// m_Head1 set.
				else {
					warnningmsg.Format("Warning!\n Header line error.\n error line no : %d ", _LineNo);
					AfxMessageBox(warnningmsg);
					IsNormal = FALSE;
					break;
				}
			}
			else if (!strcmp(s,"HEADER_2:")) {	
				s = Parser.GetToken("\"", FALSE);
				s = Parser.GetToken("\"", FALSE);
				if (s) m_Head2 = s;
				else {
					warnningmsg.Format("Warning !\n Header line error.\n 오류 라인은 %d 입니다. ", _LineNo);
					AfxMessageBox(warnningmsg);
					IsNormal = FALSE;
					break;
				}
			}
			else if ( !strcmp(s,"BEGIN:") ) step = 1;		// read msg from here. exist msg from next line
			else if (step == 1) {
				if ( !strcmp(s,"END:") ) 
					break;
				istrstream line(bf,512);
				line.flags(line.flags() | ios::hex);
				line >> fMode;
				line.flags(line.flags() & ~ios::hex);
				line >> n;
				s = Parser.GetToken();
				s = Parser.GetToken("\"", FALSE);			//
				fmsg = Parser.GetToken("\"", FALSE);		// msg
				s = Parser.GetToken("\"", FALSE);			//
				fhelp = Parser.GetToken("\"", FALSE);		// help
				if (n>=0 && n<1000) {
					IsNormal = SetItem(n, fMode, fmsg, fhelp);
					if (IsNormal == FALSE) break;
				}
			}
		}
	}
	if ( !IsNormal )
		::MessageBox( NULL, _pEipMsgFileName , "File Load Error", MB_OK|MB_ICONINFORMATION );
	return IsNormal;
}


