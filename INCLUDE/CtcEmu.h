// CtcEmu.h : header file
//

#ifndef _CTCEMU_H
#define _CTCEMU_H

/////////////////////////////////////////////////////////////////////////////
// CCtcEmu document

typedef unsigned char BYTE;

class CCtcEmu
{
protected:
	CCtcEmu();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	public:
	protected:

// Implementation
public:
	virtual ~CCtcEmu();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
};


#define CTC_LK_RO			0x80
#define CTC_LK_MODE			1
#define CTC_LK_POWER		2
#define CTC_LK_POWER_EXT	3
#define CTC_LK_COMMON		4
#define CTC_LK_KT_UPDN		5
#define CTC_LK_KT_ABCD		6
#define CTC_LK_CBI			7
#define CTC_LK_MCCR			8
#define CTC_LK_ADJCOMM		9
#define CTC_LK_AXL			10
#define CTC_LK_CALLON_LSB	11
#define CTC_LK_CALLON_MSB	12
#define CTC_LK_ERR_LSB		13
#define CTC_LK_ERR_MSB		14
#define CTC_LK_AXLB2_LSB	15
#define CTC_LK_AXLB2_MSB	16
#define CTC_LK_AXLB4_LSB	17
#define CTC_LK_AXLB4_MSB	18
#define CTC_LK_ARROW		19
#define CTC_LK_CA			20
#define CTC_LK_LCR			21
#define CTC_LK_CBB			22
#define CTC_LK_BCR			23
#define CTC_LK_SW			24
#define CTC_LK_MSW			25
#define CTC_LK_SG			26
#define CTC_LK_SG_EXT		27
#define CTC_LK_TK			28
#define CTC_LK_LC			29
#define CTC_LK_OSS			30

#define CTC_CTRL_SLS_ON 	0x21
#define CTC_CTRL_SLS_OFF	0x22
#define CTC_CTRL_DAY   		0x23
#define CTC_CTRL_NIGHT		0x24	
#define CTC_CTRL_BLK_CA		0x41	
#define CTC_CTRL_BLK_LCR	0x42
#define CTC_CTRL_BLK_CBB	0x43
#define CTC_CTRL_SW_PLS		0x51
#define CTC_CTRL_SW_MIN		0x52 
#define CTC_CTRL_SW_BLK		0x53
#define CTC_CTRL_SW_REL		0x54
#define CTC_CTRL_SG_STOP	0x61
#define CTC_CTRL_SG_RET		0x62
#define CTC_CTRL_MAIN_SET	0x71
#define CTC_CTRL_MAIN_REL	0x72
#define CTC_CTRL_SHNT_SET	0x73
#define CTC_CTRL_SHNT_REL	0x74
#define CTC_CTRL_CALL_SET	0x75
#define CTC_CTRL_CALL_REL	0x76
#define CTC_CTRL_LOS_SET	0x81
#define CTC_CTRL_LOS_REL	0x82
#define CTC_CTRL_LC_CALL	0x91
#define CTC_CTRL_OSS_STOP	0xA1
#define CTC_CTRL_OSS_RET	0xA2
#define CTC_CTRL_CTC_REQ	0xB1

#ifdef _WINDOWS
typedef struct {
//LSB
	//BYTE m_nObjNo2;	// Shunt Route Number
	//BYTE m_nObjNo1;	// Callon Route Number
	BYTE m_nObjNo;	// object number or signal object number.
//MSB
	BYTE m_nType;	// element type(D7=0) or route index number(D7=1)
} CRemoteLinkTableType;


// CTC Element class
class CRemRoute {
public:
	BYTE RO_MAIN	: 1; // bit0
	BYTE RO_MAINRQ	: 1; // bit1
	BYTE RO_SHNT	: 1; // bit2
	BYTE RO_SHNTRQ	: 1; // bit3
	BYTE RO_CALL	: 1; // bit4
	BYTE RO_CALLRQ	: 1; // bit5
	BYTE reserved	: 2; // bit6-7
public:
	void MakeRemoteData( void *pData );
};

class CRemMode {
	BYTE MD_LOCAL	: 1; // bit0
	BYTE MD_CTC		: 1; // bit1
	BYTE reserved	: 6; // bit2 -7
public:
	void MakeRemoteData( void *pData );
};

// byte 0
class CRemPower {
	BYTE PW_MAIN	: 1; // bit0	(MAIN_POWER)
	BYTE PW_GENRUN	: 1; // bit1	(GENERATOR_RUN, RUN:1 / STANDBY:0)
	BYTE PW_GEN		: 1; // bit2
	BYTE PW_B24		: 1; // bit3	(CBI_B24)
	BYTE PW_DCEX	: 1; // bit4	(EXT_B24)
	BYTE PW_GND		: 1; // bit5	(GND_DETECT:1)
	BYTE PW_UPS1	: 1; // bit6
	BYTE PW_UPS2	: 1; // bit7
public:
	void MakeRemoteData( void *pData );
};

// byte 1
class CRemPowerExt {
	BYTE PW_BATLOW	: 1; // bit0	(NORMAL:1 / LOW:0)
	BYTE PW_BATCHG	: 1; // bit1	(FULL:1 / CHARGE:0)
	BYTE PW_RELFUSE	: 1; // bit2	(FAIL:0)
	BYTE reserved	: 5; // bit3-7
public:
	void MakeRemoteData( void *pData );
};

class CRemCommon {
	BYTE CM_DAY		: 1; // bit0	(DAY:1 / NIGHT:0)
	BYTE CM_SLS		: 1; // bit1
	BYTE CM_AXL		: 1; // bit2
	BYTE CM_SG		: 1; // bit3	(ALL_SIGNALS)
	BYTE CM_SW		: 1; // bit4	(ALL_POINTMACHINES)
	BYTE CM_COMM	: 1; // bit5	(ALL_COMMS)
	BYTE CM_CLOSE	: 1; // bit6	(STATION_CLOSE, CLOSE:1 / OPEN:0)
	BYTE reserved	: 1; // bit7
public:
	void MakeRemoteData( void *pData );
};

class CRemKTUPDN {
	BYTE KT_UEPK	: 1; // bit0	(UEPK, LOCK:0)
	BYTE KT_DEPK	: 1; // bit1	(DEPK, LOCK:0)
	BYTE reserved1	: 2; // bit2-3
	BYTE KT_EEPK	: 1; // bit4	(EEPK, LOCK:0)
	BYTE reserved2	: 3; // bit5-7
public:
	void MakeRemoteData( void *pData );
};

class CRemKTABCD {
	BYTE KT_AEPK	: 1; // bit0	(LOCK:0)
	BYTE KT_BEPK	: 1; // bit1	(LOCK:0)
	BYTE KT_CEPK	: 1; // bit2	(LOCK:0)
	BYTE KT_DEPK	: 1; // bit3	(LOCK:0)
	BYTE KT_EEPK	: 1; // bit4	(LOCK:0)
	BYTE KT_FEPK	: 1; // bit5	(LOCK:0)
	BYTE KT_GEPK	: 1; // bit6	(LOCK:0)
	BYTE KT_HEPK	: 1; // bit7	(LOCK:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemCBI {
	BYTE CBI_PRI		: 1; // bit0
	BYTE CBI_PRION		: 1; // bit1
	BYTE CBI_SEC		: 1; // bit2
	BYTE CBI_SECON		: 1; // bit3
	BYTE CBI_PRIFAN		: 1; // bit4
	BYTE CBI_SECFAN		: 1; // bit5
	BYTE CBI_FUSE		: 1; // bit6
	BYTE reserved		: 1; // bit7
public:
	void MakeRemoteData( void *pData );
};

class CRemMCCR {
	BYTE CC_PRI		: 1; // bit0
	BYTE CC_PRION	: 1; // bit1
	BYTE CC_SEC		: 1; // bit2
	BYTE CC_SECON	: 1; // bit3
	BYTE reserved   : 4; // bit4-7
public:
	void MakeRemoteData( void *pData );
};

class CRemAdjComm {
	BYTE ADJ_LSTN1 : 1; // bit0
	BYTE ADJ_LSTN2 : 1; // bit1
	BYTE ADJ_LSTN3 : 1; // bit2
	BYTE ADJ_LSTN4 : 1; // bit3
	BYTE ADJ_RSTN1 : 1; // bit4
	BYTE ADJ_RSTN2 : 1; // bit5
	BYTE ADJ_RSTN3 : 1; // bit6
	BYTE ADJ_RSTN4 : 1; // bit7
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataRev( void *pData );
};

class CRemAXL {
public:
	BYTE AXL_OCC1 : 1; // bit0		(OCC:0)
	BYTE AXL_DST1 : 1; // bit1		(DST:0)
	BYTE AXL_OCC2 : 1; // bit2		(OCC:0)
	BYTE AXL_DST2 : 1; // bit3		(DST:0)
	BYTE AXL_OCC3 : 1; // bit4		(OCC:0)
	BYTE AXL_DST3 : 1; // bit5		(DST:0)
	BYTE AXL_OCC4 : 1; // bit6		(OCC:0)
	BYTE AXL_DST4 : 1; // bit7		(DST:0)
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataRev( void *pData );
};

class CRemCallOnLSB {
	BYTE CLON_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemCallOnMSB {
	BYTE CLON_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemERRLSB {
	BYTE ERR_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemERRMSB {
	BYTE ERR_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB2LSB {
	BYTE AXB2_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB2MSB {
	BYTE AXB2_MSB;
public:
	void MakeRemoteData( void *pData );
};
class CRemAXLB4LSB {
	BYTE AXB4_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB4MSB {
	BYTE AXB4_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemArrow {
	BYTE ARW_BK		: 1; // bit0	
	BYTE ARW_WT		: 1; // bit1	
	BYTE ARW_WTFL	: 1; // bit2	
	BYTE ARW_RD		: 1; // bit3	
	BYTE ARW_RDFL	: 1; // bit4	
	BYTE reserved	: 3; // bit5-7
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
	void MakeRemoteDataTokenTGB( void *pData );
	void MakeRemoteDataTokenTCB( void *pData );
};

class CRemCA {
	BYTE CA_WT		: 1; // bit0
	BYTE CA_YW		: 1; // bit1
	BYTE CA_YWFL	: 1; // bit2
	BYTE reserved	: 5; // bit3-7
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemLCR {
	BYTE LCR_WT		: 1; // bit0
	BYTE LCR_GN		: 1; // bit1
	BYTE LCR_GNFL	: 1; // bit2
	BYTE reserved	: 5; // bit3-7
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemCBB {
	BYTE CBB_WT		: 1; // bit0
	BYTE reserved	: 7; // bit1-7
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemBCR {
	BYTE BCR_GY		: 1; // bit0
	BYTE reserved	: 7; // bit1-7
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemTokenArrow {
	BYTE TKA_BK		: 1; // bit0
	BYTE TKA_WT		: 1; // bit1	
	BYTE TKA_WTFL	: 1; // bit2	
	BYTE TKA_RD		: 1; // bit3	
	BYTE reserved	: 4; // bit4-7
public:
	void MakeRemoteData( void *pData );
};

class CRemSwitch {
	BYTE SW_PLS		: 1; // bit0
	BYTE SW_MIN		: 1; // bit1 
	BYTE SW_LOCK	: 1; // bit2	(LOCK:0)
	BYTE SW_FAIL	: 1; // bit3	(INCONSISTENCY:0)
	BYTE SW_MOV		: 1; // bit4	(MOVING:1)
	BYTE SW_OVLREL	: 1; // bit5	(OVERLAP_RELEASE:1)
	BYTE SW_KTIN	: 1; // bit6	(KEY_REMOVED:0)
	BYTE SW_BLK		: 1; // bit7	(BLOCK_SET:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemMSwitch {
	BYTE MSW_PLS	: 1; // bit0
	BYTE MSW_MIN	: 1; // bit1 
	BYTE reserved1	: 1; // bit2 
	BYTE MSW_FAIL	: 1; // bit3	(INCONSISTENCY:0)
	BYTE reserved2	: 1; // bit4 
	BYTE reserved3	: 1; // bit5
	BYTE MSW_KTIN	: 1; // bit6	(KEY_REMOVED:0)
	BYTE MSW_KTREL	: 1; // bit7	(KT_LOCK:0)
public:
	void MakeRemoteData( void *pData );
};

//byte 0
class CRemSignal {
	BYTE SG_GN		: 1; // bit0
	BYTE SG_YW		: 1; // bit1 
	BYTE SG_RD		: 1; // bit2 
	BYTE SG_YY		: 1; // bit3
	BYTE SG_FL		: 1; // bit4	(RYG_FAIL:0)
	BYTE SG_YYFL	: 1; // bit5	(YY_FAIL:0)
	BYTE SG_LIND	: 1; // bit6
	BYTE SG_RIND	: 1; // bit7
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataAStart( void *pData );
};

//byte 1
class CRemSignalExt {
	BYTE SG_APLOCK	: 1; // bit0	(LOCK:1)
	BYTE SG_CALL	: 1; // bit1
	BYTE SG_SHNT	: 1; // bit2 
	BYTE SG_CALLFL	: 1; // bit3	(FAIL:0)
	BYTE SG_SHNTFL	: 1; // bit4	(FAIL:0)
	BYTE SG_INDFL	: 1; // bit5	(FAIL:0)
	BYTE SG_CLR		: 1; // bit6	(CLEAR:1)
	BYTE SG_CLRWT	: 1; // bit7	(CLEAR_WAITING:1)
public:
	void MakeRemoteData( void *pData );
};

class CRemTrack {
	BYTE TK_OCC		: 1; // bit0	(OCC:1)
	BYTE TK_ROREQ	: 1; // bit1	(REQ:1)
	BYTE TK_RO		: 1; // bit2	(SET:1)
	BYTE reserved1	: 1; // bit3	
	BYTE reserved2	: 1; // bit4
	BYTE TK_LOS		: 1; // bit5	(LOS:1)
	BYTE TK_NOSEQ	: 1; // bit6	(NOSEQ_OCC:0)
	BYTE TK_EMREL	: 1; // bit7	(EMREL:1)
public:
	void MakeRemoteData( void *pData );
};

class CRemLC {
	BYTE LC_CLOSE	: 1; // bit0	(CLOSE:1)
	BYTE LC_KEYCTRL	: 1; // bit1	(PERMIT:1)
	BYTE LC_CALLOP	: 1; // bit2
	BYTE LC_CALLACK	: 1; // bit3	
	BYTE reserved	: 4; // bit4-7
public:
	void MakeRemoteDataLC1( void *pData );
	void MakeRemoteDataLC2( void *pData );
	void MakeRemoteDataLC3( void *pData );
	void MakeRemoteDataLC4( void *pData );
};

class CRemOSS {
	BYTE OSS_SET	: 1; // bit0	(SET:1)
	BYTE reserved	: 7; // bit1-7
public:
	void MakeRemoteData( void *pData );
};

#else  // (not) VI_LITTLE_ENDIAN

typedef struct {
//MSB
	BYTE m_nType;	// element type(D7=0) or route index number(D7=1)
//LSB
	BYTE m_nObjNo;	// object number or signal object number.
} CRemoteLinkTableType;


// CTC Element class
class CRemRoute {
public:
	BYTE reserved	: 2; // bit6-7
	BYTE RO_CALLRQ	: 1; // bit5
	BYTE RO_CALL	: 1; // bit4
	BYTE RO_SHNTRQ	: 1; // bit3
	BYTE RO_SHNT	: 1; // bit2
	BYTE RO_MAINRQ	: 1; // bit1
	BYTE RO_MAIN	: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

class CRemMode {
	BYTE reserved	: 6; // bit2 -7
	BYTE MD_CTC		: 1; // bit1
	BYTE MD_LOCAL	: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

// byte 0
class CRemPower {
	BYTE PW_UPS2	: 1; // bit7
	BYTE PW_UPS1	: 1; // bit6
	BYTE PW_GND		: 1; // bit5	(GND_DETECT:1)
	BYTE PW_DCEX	: 1; // bit4	(EXT_B24)
	BYTE PW_B24		: 1; // bit3	(CBI_B24)
	BYTE PW_GEN		: 1; // bit2
	BYTE PW_GENRUN	: 1; // bit1	(GENERATOR_RUN, RUN:1 / STANDBY:0)
	BYTE PW_MAIN	: 1; // bit0	(MAIN_POWER)
public:
	void MakeRemoteData( void *pData );
};

// byte 1
class CRemPowerExt {
	BYTE reserved	: 5; // bit3
	BYTE PW_RELFUSE	: 1; // bit2	(FAIL:0)
	BYTE PW_BATCHG	: 1; // bit1	(FULL:1 / CHARGE:0)
	BYTE PW_BATLOW	: 1; // bit0	(NORMAL:1 / LOW:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemCommon {
	BYTE reserved	: 1; // bit7
	BYTE CM_CLOSE	: 1; // bit6	(STATION_CLOSE, CLOSE:1 / OPEN:0)
	BYTE CM_COMM	: 1; // bit5	(ALL_COMMS)
	BYTE CM_SW		: 1; // bit4	(ALL_POINTMACHINES)
	BYTE CM_SG		: 1; // bit3	(ALL_SIGNALS)
	BYTE CM_AXL		: 1; // bit2
	BYTE CM_SLS		: 1; // bit1
	BYTE CM_DAY		: 1; // bit0	(DAY:1 / NIGHT:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemKTUPDN {
	BYTE reserved2	: 3; // bit5-7
	BYTE KT_EEPK	: 1; // bit4	(EEPK, LOCK:0)
	BYTE reserved1	: 2; // bit2-3
	BYTE KT_DEPK	: 1; // bit1	(DEPK, LOCK:0)
	BYTE KT_UEPK	: 1; // bit0	(UEPK, LOCK:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemKTABCD {
	BYTE KT_HEPK	: 1; // bit7	(LOCK:0)
	BYTE KT_GEPK	: 1; // bit6	(LOCK:0)
	BYTE KT_FEPK	: 1; // bit5	(LOCK:0)
	BYTE KT_EEPK	: 1; // bit4	(LOCK:0)
	BYTE KT_DEPK	: 1; // bit3	(LOCK:0)
	BYTE KT_CEPK	: 1; // bit2	(LOCK:0)
	BYTE KT_BEPK	: 1; // bit1	(DEPK, LOCK:0)
	BYTE KT_AEPK	: 1; // bit0	(UEPK, LOCK:0)
public:
	void MakeRemoteData( void *pData );
};

class CRemCBI {
	BYTE reserved		: 1; // bit7
	BYTE CBI_FUSE		: 1; // bit6
	BYTE CBI_SECFAN		: 1; // bit5
	BYTE CBI_PRIFAN		: 1; // bit4
	BYTE CBI_SECON		: 1; // bit3
	BYTE CBI_SEC		: 1; // bit2
	BYTE CBI_PRION		: 1; // bit1
	BYTE CBI_PRI		: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

class CRemMCCR {
	BYTE reserved   : 4; // bit4-7
	BYTE CC_SECON	: 1; // bit3
	BYTE CC_SEC		: 1; // bit2
	BYTE CC_PRION	: 1; // bit1
	BYTE CC_PRI		: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

class CRemAdjComm {
	BYTE ADJ_RSTN4 : 1; // bit7
	BYTE ADJ_RSTN3 : 1; // bit6
	BYTE ADJ_RSTN2 : 1; // bit5
	BYTE ADJ_RSTN1 : 1; // bit4
	BYTE ADJ_LSTN4 : 1; // bit3
	BYTE ADJ_LSTN3 : 1; // bit2
	BYTE ADJ_LSTN2 : 1; // bit1
	BYTE ADJ_LSTN1 : 1; // bit0
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataRev( void *pData );
};

class CRemAXL {
public:
	BYTE AXL_DST4 : 1; // bit7		(DST:0)
	BYTE AXL_OCC4 : 1; // bit6		(OCC:0)
	BYTE AXL_DST3 : 1; // bit5		(DST:0)
	BYTE AXL_OCC3 : 1; // bit4		(OCC:0)
	BYTE AXL_DST2 : 1; // bit3		(DST:0)
	BYTE AXL_OCC2 : 1; // bit2		(OCC:0)
	BYTE AXL_DST1 : 1; // bit1		(DST:0)
	BYTE AXL_OCC1 : 1; // bit0		(OCC:0)
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataRev( void *pData );
};

class CRemCallOnLSB {
	BYTE CLON_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemCallOnMSB {
	BYTE CLON_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemERRLSB {
	BYTE ERR_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemERRMSB {
	BYTE ERR_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB2LSB {
	BYTE AXB2_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB2MSB {
	BYTE AXB2_MSB;
public:
	void MakeRemoteData( void *pData );
};
class CRemAXLB4LSB {
	BYTE AXB4_LSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemAXLB4MSB {
	BYTE AXB4_MSB;
public:
	void MakeRemoteData( void *pData );
};

class CRemArrow {
	BYTE reserved	: 3; // bit5-7
	BYTE ARW_RDFL	: 1; // bit4	
	BYTE ARW_RD		: 1; // bit3	
	BYTE ARW_WTFL	: 1; // bit2	
	BYTE ARW_WT		: 1; // bit1	
	BYTE ARW_BK		: 1; // bit0	
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
	void MakeRemoteDataTokenTGB( void *pData );
	void MakeRemoteDataTokenTCB( void *pData );
};

class CRemCA {
	BYTE reserved	: 5; // bit3-7
	BYTE CA_YWFL	: 1; // bit2
	BYTE CA_YW		: 1; // bit1
	BYTE CA_WT		: 1; // bit0
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemLCR {
	BYTE reserved	: 5; // bit3-7
	BYTE LCR_GNFL	: 1; // bit2
	BYTE LCR_GN		: 1; // bit1
	BYTE LCR_WT		: 1; // bit0
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemCBB {
	BYTE reserved	: 7; // bit1-7
	BYTE CBB_WT		: 1; // bit0
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemBCR {
	BYTE reserved	: 7; // bit1-7
	BYTE BCR_GY		: 1; // bit0
public:
	void MakeRemoteDataTGB( void *pData );
	void MakeRemoteDataTCB( void *pData );
};

class CRemTokenArrow {
	BYTE reserved	: 4; // bit4-7
	BYTE TKA_RD		: 1; // bit3	
	BYTE TKA_WTFL	: 1; // bit2	
	BYTE TKA_WT		: 1; // bit1	
	BYTE TKA_BK		: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

class CRemSwitch {
	BYTE SW_BLK		: 1; // bit7	(BLOCK_SET:0)
	BYTE SW_KTIN	: 1; // bit6	(KEY_REMOVED:0)
	BYTE SW_OVLREL	: 1; // bit5	(OVERLAP_RELEASE:1)
	BYTE SW_MOV		: 1; // bit4	(MOVING:1)
	BYTE SW_FAIL	: 1; // bit3	(INCONSISTENCY:0)
	BYTE SW_LOCK	: 1; // bit2	(LOCK:0)
	BYTE SW_MIN		: 1; // bit1 
	BYTE SW_PLS		: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

class CRemMSwitch {
	BYTE MSW_KTREL	: 1; // bit7	(KT_LOCK:0)
	BYTE MSW_KTIN	: 1; // bit6	(KEY_REMOVED:0)
	BYTE reserved3	: 1; // bit5
	BYTE reserved2	: 1; // bit4 
	BYTE MSW_FAIL	: 1; // bit3	(INCONSISTENCY:0)
	BYTE reserved1	: 1; // bit2 
	BYTE MSW_MIN	: 1; // bit1 
	BYTE MSW_PLS	: 1; // bit0
public:
	void MakeRemoteData( void *pData );
};

//byte 0
class CRemSignal {
	BYTE SG_RIND	: 1; // bit7
	BYTE SG_LIND	: 1; // bit6
	BYTE SG_YYFL	: 1; // bit5	(YY_FAIL:0)
	BYTE SG_FL		: 1; // bit4	(RYG_FAIL:0)
	BYTE SG_YY		: 1; // bit3
	BYTE SG_RD		: 1; // bit2 
	BYTE SG_YW		: 1; // bit1 
	BYTE SG_GN		: 1; // bit0
public:
	void MakeRemoteData( void *pData );
	void MakeRemoteDataAStart( void *pData );
};

//byte 1
class CRemSignalExt {
	BYTE SG_CLRWT	: 1; // bit7	(CLEAR_WAITING:1)
	BYTE SG_CLR		: 1; // bit6	(CLEAR:1)
	BYTE SG_INDFL	: 1; // bit5	(FAIL:0)
	BYTE SG_SHNTFL	: 1; // bit4	(FAIL:0)
	BYTE SG_CALLFL	: 1; // bit3	(FAIL:0)
	BYTE SG_SHNT	: 1; // bit2 
	BYTE SG_CALL	: 1; // bit1
	BYTE SG_APLOCK	: 1; // bit0	(LOCK:1)
public:
	void MakeRemoteData( void *pData );
};

class CRemTrack {
	BYTE TK_EMREL	: 1; // bit7	(EMREL:1)
	BYTE TK_NOSEQ	: 1; // bit6	(NOSEQ_OCC:0)
	BYTE TK_LOS		: 1; // bit5	(LOS:1)
	BYTE reserved2	: 1; // bit4
	BYTE reserved1	: 1; // bit3	
	BYTE TK_RO		: 1; // bit2	(SET:1)
	BYTE TK_ROREQ	: 1; // bit1	(REQ:1)
	BYTE TK_OCC		: 1; // bit0	(OCC:1)
public:
	void MakeRemoteData( void *pData );
};
	
class CRemLC {
	BYTE reserved	: 4; // bit4-7
	BYTE LC_CALLACK	: 1; // bit3	
	BYTE LC_CALLOP	: 1; // bit2
	BYTE LC_KEYCTRL	: 1; // bit1	(PERMIT:1)
	BYTE LC_CLOSE	: 1; // bit0	(CLOSE:1)
public:
	void MakeRemoteDataLC1( void *pData );
	void MakeRemoteDataLC2( void *pData );
	void MakeRemoteDataLC3( void *pData );
	void MakeRemoteDataLC4( void *pData );
};
	
class CRemOSS {
	BYTE reserved	: 7; // bit1-7
	BYTE OSS_SET	: 1; // bit0	(SET:1)
public:
	void MakeRemoteData( void *pData );
};

#endif // _WINDOWS

#endif
