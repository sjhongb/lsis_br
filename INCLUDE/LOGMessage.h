/////////////////////////////////////////////////////////////////////////////
// LOGMessage.h : header file

#ifndef   _LOGMESSAGE_H
#define   _LOGMESSAGE_H

#include "StrList.h"
#include "MenuItem.h"
#include "LOGRecord.h"
#include "MessageInfo.h"
#include "../LES/LES.h"

/////////////////////////////////////////////////////////////////////////////
// ILD Infomation list
#define MYLIST_TRACK	0
#define MYLIST_SIGNAL	1
#define MYLIST_SWITCH	2
#define MYLIST_BUTTON	3
#define MYLIST_ROUTE	4
#define MYLIST_MAX		5
/////////////////////////////////////////////////////////////////////////////
// CLOGMessage class


class CLOGMessage 
{
// constructor
public:
	int GetButtonIDFromSignalDest( int nSignal, int nDestTrack );
	CLOGMessage();

public:
	BOOL TrackItemCheck( MenuItemStruct *pIMenu, int nMax );
	BOOL SwitchItemCheck( MenuItemStruct  *pIMenu, int nMax );
	BOOL RouteItemCheck( MenuItemRoute *pRouteMenu, int nMax );
	BOOL ClearDestTrackID();
	CString GetButtonName(int nID);
	CString GetTrackName(int nID);
	CString GetSwitchName(int nID);
	CString GetSignalName(int nID);

// Attrib
public:
	BOOL		m_bNoneBase;
	CLOGRecord	m_OldRecord;
	CLESApp*	pApp;

public:
	static int  m_nCreateCount;

// Operations
protected:
	int  GetUnitSendMessage(CObList &objList, BYTE* ptr, BYTE* pNew,
								EventKeyType &EventKey, BYTE &errorMode, BOOL bDownMsg );	// 0:None Alter, 1:Alter, -1:Alter & System Down.
	int  GetSystemMessage(CObList &objList, void *pOld, void *pCur, 
								EventKeyType &EventKey, BYTE &errorMode, BOOL bDownMsg );	// 0:None Alter, 1:Alter, -1:Alter & System Run Off.
	int  GetAlarmMessage(CObList &objList, BYTE* pOld, BYTE* pCur,
								EventKeyType &EventKey, BYTE &errorMode);
	int  GetOperateMessage(CObList &objList, BYTE* pOp, BYTE* pOld, BYTE* pNew,
								EventKeyType &EventKey, BYTE &errorMode);
	int  GetIOCardMessage(CObList &objList, void *pOld, void *pCur, 
								EventKeyType &EventKey, BYTE &errorMode);
// Operations
public:
	int GetSwitchControlMode( int nID, void *pNew );
	int GetTrackID(LPCSTR lpszName);
	int GetSignalID(LPCSTR lpszName);
	int GetSwitchID(LPCSTR lpszName);
	int GetButtonID(LPCSTR lpszName);
	int GetRouteCtrlID(CString &strRoute, int &SignalID, int &ButtonID);
	int GetRouteName(CString &strRoute, int SignalID, int ButtonID);

	void SetBaseRecord(CLOGRecord &pOld);
	void CreateCardInfo();
	BOOL CreateInfo( CMessageInfo *pScrInfo, int nMax );		// TRUE/Create Normal, FALSE/Create fail.
	BOOL CreateMenuInfo(MenuItems *pMenu, MenuItemType createType);
	int  CreateMessage(CLOGRecord &pCur, CObList &objList); // -1/All, 0/None, +Other/Part

protected:
	BOOL CreateRouteInfo(MenuItems *pMenu, BOOL bSearchItem);

// Implementation
public:
	int m_iCountOfDestTrack;
	BYTE m_bNameOfDestTrackID[50];
	int m_iNumberOfRoute;
	BOOL MainOlePostMessage(long message, long wParam, long lParam, long &lResult);
	BOOL isAlterRecordData(CLOGRecord &OldRec,CLOGRecord &ScrRec,BOOL bChkBlock = FALSE);
	~CLOGMessage();
};

/////////////////////////////////////////////////////////////////////////////

extern BYTE Info_cUsedCardMask[8];				// 8 card for 1 rack.
extern CMessageInfo *Info_pScrMessageInfo;

/////////////////////////////////////////////////////////////////////////////

#endif // _LOGMESSAGE_H
