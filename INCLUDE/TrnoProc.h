// Trnoproc.h : header file
//

#ifndef   _TRNOPROC_H
#define   _TRNOPROC_H

#include "UnitMsg.h"
#include "LOGMessage.h"

/////////////////////////////////////////////////////////////////////////////

typedef struct tagTrNoType {
	char TrNos[6];
	tagTrNoType() {
		TrNos[0] = 
		TrNos[1] = 
		TrNos[2] = 
		TrNos[3] = 
		TrNos[4] = 
		TrNos[5] = 0x00;
	};
} TrNoType;

/////////////////////////////////////////////////////////////////////////////

CString GetTrainNoInCmd( BYTE *pData );
char *GetTracinNoHead(CLOGRecord &log);
void SetTrainNo(int nid, char *trno);
BOOL IsExsitTrainNo( BYTE *pData );
BOOL CmpTrainNo(int nid, char *trno);	// FALSE/same too, True/difference
BOOL IsChangeUnitCmd(BYTE *pOld, BYTE *pNew);
BOOL IsChangeUnitMsg(BYTE *pOld, BYTE *pNew);

/////////////////////////////////////////////////////////////////////////////

extern TrNoType *_pTrainNos;

/////////////////////////////////////////////////////////////////////////////

#endif // _TRNOPROC_H
