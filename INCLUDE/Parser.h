/////////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2004 < (��) LG���� >
//
//	Module Name:
//		CParser.h
//
//	Abstract:
//		
//
//	Author:
//		<omani> (xxxx@lgis.com) 2004.4.9
//
//	Revision History:
//
//	Notes:
//
/////////////////////////////////////////////////////////////////////////////

// CParser.h : implement class
//
#ifndef   _CPARSER_H
#define   _CPARSER_H

/////////////////////////////////////////////////////////////////////////////
// CParser class
class CParser
{
public:
	CString m_strString;
	CString m_strDelimiter;
	CString m_strToken;
	BOOL	m_bDelimiter;
	int		m_iEnd;

	CParser();
	CParser(char *str, char *pDel = NULL);
	CParser(const CString& str, char *pDel = NULL);
	~CParser() {};

	void Create(char *str, const char *pDel=NULL);
	void Create(const CString &str, const char *pDel=NULL);
	BOOL GetToken(CString &str, const char *pDel=NULL, BOOL bDefDel = TRUE);
	char *GetToken(const char *pDel=NULL, BOOL bDefDel = TRUE);
	char *gettokenl(BOOL bDefDel = TRUE);

};
#endif // _CPARSER_H

