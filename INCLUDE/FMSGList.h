// FMSGList.h : interface of the CLOGRecord class
//
#ifndef   _FMSGLIST_H
#define   _FMSGLIST_H

#include "MessageNo.h"

/////////////////////////////////////////////////////////////////////////////
#define MAX_FMSG_LIST	1000 

/////////////////////////////////////////////////////////////////////////////
typedef struct {
	BYTE uMode;
	CString Msg; 
	CString Help;
} FileMsgItem;

typedef struct {
	int  nLeft;
	int  nHead1;
	int  nHead2;
	int  nTop;
	int  nLines;
} PrintMargin;

/////////////////////////////////////////////////////////////////////////////
//class CFMSGList;
class CFMSGList 
{
// Attributes
protected:
	FileMsgItem *m_MsgList[MAX_FMSG_LIST];

// Atribute
public:
//	PrintMargin m_Margin;
	BOOL m_bHelpOn;
	CString m_Head1;
	CString m_Head2;

// Operations
public:
	FileMsgItem *GetItem(int no);
	BYTE GetMode(int no);
	void GetMessage(CString &string, MessageEvent *pEvent);
	void GetMessage(MsgTimeStrings &strings, MessageEvent *pEvent);

protected:
	BOOL SetItem(int no, BYTE mode, const char *Msg, const char *Help);
	void Init();
	void Flush();

public: // create from serialization only
	CFMSGList();

	BOOL Create();

// Implementation
public:
	~CFMSGList();
};

/////////////////////////////////////////////////////////////////////////////

extern CFMSGList *_pEIPMsgList;

#endif // _FMSGLIST_H
