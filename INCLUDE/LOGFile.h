//=======================================================
//==              LOGFile.h
//=======================================================
//  파 일 명 :  LOGFile.h
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//2001.05.02(진행)
//	* 검색 데이터의 레코드 블럭을 'LOGMessage.cpp'에서 삭제하고 'LOGFile.cpp'
//	  내로 가져옴. 이것은 레코드의 변경 데이터 생성 및 파일 저장 처리를 파일 
//	  클래스에서 처리하기 위함이며, 처리 또는 코드를 블럭화 하기 위함임.
//	- Search Record 블럭 데이터 명칭변경(_pSearchRecord -> _RecordBlk)
//	- CLOGFile 의 데이터 명칭변경(m_Record -> m_rcRecord)
//	- CLOGFile 의 데이터 추가.
//	  $m_woRecord$   ----   최종 저장된 데이터의 원형.
//	  $m_waReocrd$   ----   최종 저장된 데이터(압축된 데이터)
//	  $extern CLOGRecord  _pRecordBlk[]$   ----   1 분간의 Search 데이터 블럭
//
//=======================================================

#include "ScrInfo.h"
#include "LOGRecord.h"

#ifndef   _LOGFILE_H
#define   _LOGFILE_H

//=======================================================
//
//  LOG Search
//
//=======================================================
#define _LOG_SEARCH_NONE		        0
#define _LOG_SEARCH_FIND		        1
#define _LOG_SEARCH_OVER		        2

#define _MAX_RECORD_MINUTE  240
#define MAX_INDEX_RECORD	1440									// Minute index count for Day(60M * 24H)
#define MAX_INDEX_MINUYE	60										// Index Record count in 1 Minute(60S * 1M).
#define MAX_RECORD_MINUTE	240										// Data Record count in 1 Minute(4 * 60S * 1M).
#define MAX_IDBYTE			32
#define LOG_SEARCH_NONE		0
#define LOG_SEARCH_FIND		1
#define LOG_SEARCH_OVER		2
#define SIZE_INDEXTABLE		(sizeof(long) * MAX_INDEX_RECORD)		// long * MAX_INDEX_RECORD
#define SIZE_LOGINDEX		(SIZE_INDEXTABLE + MAX_IDBYTE)			// index table + 32 byte(ID)

typedef struct tagLOGIndexType {
	BYTE pID[MAX_IDBYTE];				// "LES logging file Ver 2.00"
	long table[MAX_INDEX_RECORD+1];		// pointer address table for minute data.
} LOGIndexType;

/////////////////////////////////////////////////////////////////////////////
// CLOGFile class

class CLOGFile 
{
	LOGIndexType m_stIndex;

	// Control data for Index Table
	long m_lLastIndex;
	long m_lCurSeek;
	long m_lLastTime;

	// Datas for Searching
	BOOL  m_bLastRecord;
	BOOL  m_bFind;
	long  m_lTimeStart;
	long  m_lTimeEnd;
	long  m_lTimeSearch;
	long  m_lTimeIndex;
	BOOL  m_bSpecialPath;

	CString m_strMode;		// File Open Mode
public:

	CString m_strPath;		// Pull Path directory
	CString m_strName;		// Pull Path & log Name

	CLOGRecord m_rcRecord;	// Read Record.
	CLOGRecord m_woRecord;	// 2001.05.02 jin - add data
	CLOGRecord m_waRecord;	// 2001.05.02 jin - add data

// Attributes
public:

// Operations
public:
	// Index control functions
	void InitIndex();
	void InitID();
	int  SetIndex(int index, long seek, FILE *fp=NULL);
	long GetIndex(int index);
	int  ReadIndex( FILE *fp );
	int  WriteIndex( FILE *fp );

	// File control functions
	void InitFile(const char *pszMode);
	void InitLogFile( char *pszFile);
	int  AttachRecord(CLOGRecord *curR, BOOL bSave, CLOGRecord *oldR=NULL);	//2001.05.02 jin - Modified(Add) code.

	// Search Function
	BOOL IsStrIDVersion();
	BOOL IsGetLastRecord() { return m_bLastRecord; }
	BOOL GetSearchTime( CJTime &SearchTime );
	int  FindRecord( BOOL bDayNext = FALSE );
	int  NextRecord( BOOL bDayNext = FALSE );
	int  PrevRecord( BOOL bDayNext = FALSE );
	int  GetRecordInMinute( CLOGRecord *fRecord, CLOGRecord &mRec, long &forSeek, BOOL bFillRecord = FALSE, BOOL bUpdateIndex = FALSE );
	int  SetFind( CJTime searcht, BOOL bNext, CJTime *pSTime = NULL, CJTime *pETime = NULL );
	int  SetFindOneday( CJTime searcht, BOOL bNext, CJTime *pSTime = NULL, CJTime *pETime = NULL );
	void SetRange( CJTime st, CJTime et/*, BYTE filter*/ );
	void MakeDir(CJTime time);
	BOOL IsEqualMinute(CJTime &dest, CJTime &scr);
	void RecordPartToAll(CLOGRecord &destR, CLOGRecord &scrR, CLOGRecord &oldR);  // part -> all
	BOOL RecordAllToPart(CLOGRecord *destR, CLOGRecord *scrR, CLOGRecord *oldR); // all -> part

protected:
	void GenError( int no, CString & );
	void GenError( int no, LPCTSTR pcszMessage );
	BOOL IsUpdateDate(CJTime scrt);
	BOOL UpdatePath(CJTime scrt, BOOL bMakePath=FALSE, CString strPath = "");
	FILE*FileOpen(int fmode=0);		// 0/reserved, 1/r+b, 2/w+b

// Constructor & Destructor
public:
	CLOGFile();					
	CLOGFile(const char *pszMode);					
//	CLOGFile(char *pszFile); // 2006년 7월 22일 LOG 기록안되는 문제 때문에 삭제 					
	~CLOGFile();
};

extern CLOGRecord	 _pRecordBlk[];
BOOL isAlterRecordData( CLOGRecord &OldRec, CLOGRecord &ScrRec, BOOL bChkBlock = FALSE );

#endif // _LOGFILE_H
