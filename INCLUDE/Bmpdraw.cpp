//===============================================================
//====                     Bmpdraw.cpp                       ====
//===============================================================
// 파  일  명 : Bmpdraw.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 선택된 대상을 화면에 직접 그려주는 역할을 한다.
//
//===============================================================

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <strstrea.h>
#include "Bmpdraw.h"
#include "bmpinfo.h"

UINT _nGRIDSIZE = 10;           // Used for DES to SFM 
float VIEW_XRATE = 0.5f;		// x * VIEW_XRATE 
float VIEW_YRATE = 1.0f;		// y * VIEW_XRATE

UINT  CELLSIZE_Y   = 10;     	//_nDotPerCell; 
UINT  CELLSIZE_X   = 5;	    	//_nDotPerCell / 2; 

int   OFFSET_X  = 540;
int   OFFSET_Y  = -375;
int   REVERSE = 1;

SymbolBitmapInfo SymbolInfo10[] = 
{
        //           x,    y,  w,   h,  cx,  cy
        // !NO  { -xxx,-xxx,-xxx,-xxx,-xxx,-xxx },   // COMMENT
		/*  0*/ {    0,   0,   2,  10,   0,   5 },   // Terminate  직선 DIR=0
		/*  1*/ {    5,   0,   6,  10,   3,   5 },   // Straight   직선 DIR=0,4
		/*  2*/ {   22,   0,  16,  10,   6,   5 },   // Straight  사선 DIR=1,5
        /*  3*/ {   40,   0,  15,  10,   3,   5 },   // Elbow
	    /*  4*/ {   55,   1,  20,   9,  11,   4 },   // Terminate  사선 우상		
        /*  5*/ {    0,   0,  15,  10,  10,   5 },   // LTerminate    DIR=0
	    /*  6*/ {   75,   0,  11,  11, -12,   5 },   // Shuting signal
	    /*  7*/ {   91,   0,  11,  11, -12,   5 },   // Main signal
	    /*  8*/ {   76,  13,  10,  10,   5,   5 },   // signal small lamp
	    /*  9*/ {    4,   0,  15,  10,  10,   5 },   // LStraight  직선 dir=0

	    /* 10*/ {    0,   0,  10,  21,   5,   5 },   //
	    /* 11*/ {    0,   0,  10,  21,   5,   5 },   //
	    /* 12*/ {  283,   0,  24,  17,  19,   8 },   // 방향표시
	    /* 13*/ {    4,  10,  16,  10,  -5,   4 },   // 방향표시 직선
	    /* 14*/ {    4,   0,  15,  10,  10,   5 },   //
	    /* 15*/ {    2,  10,  15,  10,   5,  10 },   //
	    /* 16*/ {   15,  10,   5,  10,   5,   5 },   // TERMINATE   직선 DIR=4
	    /* 17*/ {    2,  10,  15,  10,   5,   5 },   // LStraight  직선 dir=4
	    /* 18*/ {   22,  10,  16,  10,   6,   5 },   // Straight  사선   DIR=3,7
        /* 19*/ { 40 , 10, 15, 10,  3, 5 },   // Elbow
        /* 20*/ { 55 , 10, 20, 9, 11, 5 },   // Terminate  사선 우하
		
        /*21*/ {  5 , 10, 15, 10,  5, 5 },   // LTerminate    DIR=4
        /*22*/ {  0 , 20, 10, 20,  5, 10 },   //BUTTON ON (
        /*23*/ {  5 , 20, 10, 20,  5, 10 },   //BUTTON ON |
        /*24*/ { 10 , 20, 10, 20,  5, 10 },   //BUTTON ON )
        /*25*/ { 20 , 20, 10, 20,  5, 10 },   //BUTTON OFF (
        /*26*/ { 25 , 20, 10, 20,  5, 10 },   //BUTTON OFF |
        /*27*/ { 30 , 20, 10, 20,  5, 10 },   //BUTTON OFF )
        /*28*/ { 79 , 11,  9,  9,  4, 4 },   // signal small lamp Up
        /*29*/ { 88 , 11,  9,  9,  4, 4 },   // signal small lamp Down
        /*30*/ {102 , 21, 19, 19,  9, 9 },   // TTB Normal
        /*31*/ {102 , 61, 19, 19,  9, 9 },   // TTB On

        /*32*/ {161 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=2
        /*33*/ {161 ,  5, 10, 10,  5, 5 },   //	Straight   직선 DIR=2,6
        /*34*/ {451 ,226, 23,  5, 12, 0 },   // 건널목 line(OFF)
        /*35*/ { 40 , 20, 15, 10, 8, 5 },   // Elbow
        /*36*/ { 55 , 21, 20, 9,  6, 4 },   // Terminate  사선 좌상		
        /*37*/ {492 ,226, 23, 5, 12, 0},   // 건널목 line(ON)
		/*38*/ { 70 , 20, 11, 11, 23, 5 },   // Shuting signal
		/*39*/ { 86 , 20, 11, 11, 23, 5 },   // Main signal
        /*40*/ { 70 , 31,  9,  5,  4, 2 },   // 선별등 OFF
        /*41*/ { 79 , 31,  9,  5,  4, 2 },   // 선별등 ON
        /*42*/ { 359 , 113, 33, 33, 16, 16}, // 조작반 down
        /*43*/ { 359 , 147, 33, 33, 16, 16}, // 조작반 up
        /*44*/ {283 , 20, 24, 17,  3, 8 },   //방향표시
        /*45*/ {  0 ,  0, 16, 10, 19, 4 },   //방향표시 직선
        /*46*/ {164 ,141, 15, 15,  7, 7 },   // LDK Signal Off/On
        /*47*/ {0 ,0, 0,  0, 0, 0 },   // 

        /*48*/ {171 ,  0, 10, 10,  5, 5 },   //	Termin(ate  직선 DIR=6
        /*49*/ {449 ,203, 25, 24, 14, 12 },   // 건널목 up(OFF)
        /*50*/ {490 ,203, 25, 24, 14, 12},   // 건널목 up(ON)
        /*51*/ { 40 , 30, 15, 10, 8, 5 },   // Elbow
	    /*52*/ { 55 , 30, 20, 9,  6, 5 },   // Terminate  사선 좌하
        /*53*/ { 170, 11,  6,  6,  2, 2 },   // 입환신호기 내부 원 
        /*54*/ { 70 ,  0,  5, 11, -7, 5 },   // 신호기 막대 부분 왼
        /*55*/ { 81 , 20,  5, 11, 12, 5 },   // 신호기 막대 부분 오
        /*56*/ {161 ,  0, 20, 11, -12, 5 },  // 3,4,5현시 신호등 오
		/*57*/ {161 ,  0, 20, 11,  32, 5 },  // 3,4,5현시 신호등 
        /*58*/ {161 , 11,  9,  9, -13, 4 },   //       RIGHT 
        /*59*/ {161 , 11,  9,  9,  22, 4 },   //       LEFT	
        /*60*/ {216 ,  6, 10, 10,  6, 10 },   //   수직선 아래끝
        /*61*/ {216 , 18, 10, 10,  6, 0 },   //   수직선 위 끝 
		/*62*/ {216 ,  0, 10, 10,  6, 5 },   //   수직 직선 
        /*63*/ {181 ,  0, 14, 10,  6, 5 },   //  bottom , right_top 
		/*64*/ { 199,  4, 14, 10,  10, 5 },  //  bottom , left_top
        /*65*/ { 181, 15, 14, 10,  6, 5 },   //   top , right_bottom
		/*66*/ { 199 ,11, 14, 10,  10, 5 },   //   top , left_bottom	
		/*67*/ { 55 ,  1, 20,   9, 11, 4 },  //  4번 Terminate  사선 우상 (LEFT_BOTTOM)
		/*68*/ { 55 , 10, 20,   9, 11, 5 },  // 20번 Terminate  사선 우하 (LEFT_TOP)
		/*69*/ { 55 , 21, 20, 9,  6, 4 },   // 36번 Terminate  사선 좌상 (RIGHT_BOTTOM)
		/*70*/ { 55 , 30, 20, 9,  6, 5 },    // 52번 Terminate  사선 좌하 (RIGHT_TOP)
		/*71*/ { 228, 0, 10, 10,  6, 5 },     // 수직 굴절    LEFT_BOTTOM
		/*72*/ { 239, 0, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_BOTTOM
		/*73*/ { 228, 10, 10, 10,  6, 5 },     // 수직 굴절    LEFT_TOP
		/*74*/ { 239, 10, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_TOP
        /*75*/ { 220,  0, 20, 16,  10, 8 },   //BUTTON ON ( 도착점
        /*76*/ { 227,  0, 10, 16,  5, 8 },   //BUTTON ON |  
        /*77*/ { 228,  0, 20, 16,  10, 8 },   //BUTTON ON )
        /*78*/ { 220, 18, 20, 16,  10, 8 },   //BUTTON OFF (  
        /*79*/ { 227, 18, 10, 16,  5, 8 },   //BUTTON OFF |  
        /*80*/ { 228, 18, 20, 16,  10, 8 },   //BUTTON OFF )  
	    /*81*/ { 5, 200, 5, 10,  3, 5 },	// 트랙 일부분(5*10)	
		/*82*/ { 300, 209,  140,  9,  70, 4 },   // 인접역 표시 화살표 우
		/*83*/ { 300, 292,  140,  9,  70, 4 },   // 인접역 표시 화살표 좌
        /*84*/ { 291,  0, 10, 16,  5, 8 },   //BUTTON ON ( 도착점
        /*85*/ { 298,  0, 5, 16,  2, 8 },   //BUTTON ON |  
        /*86*/ { 301,  0, 10, 16,  5, 8 },   //BUTTON ON )
        /*87*/ { 291, 18, 10, 16,  5, 8 },   //BUTTON OFF (  
        /*88*/ { 298, 18, 5, 16,  2, 8 },   //BUTTON OFF |  
        /*89*/ { 301, 18, 12, 16,  5, 8 },   //BUTTON OFF )  
        /*90*/ { 382 , 2,  14,  14,  7, 7 },   // 전철기 normal
        /*91*/ { 400 , 2,  14,  14,  7, 7 },   // 전철기 reverse
        /*92*/ { 314 , 20, 18, 23,  12, 13 },
        /*93*/ { 324 , 20, 12, 23,  9, 13 },   //BUTTON ON |
        /*94*/ { 331 , 20, 16, 23,  7, 13 },   //BUTTON ON )
        /*95*/ { 0 , 0, 0, 0, 0, 0}, // 
	    /*96*/ { 76 ,  0, 55, 13, -10, 6 },   // 4현시 오른쪽 방향
		/*97*/ { 76 , 23, 55, 13, 64, 6 },   // 4현시 왼쪽 방향
        /*98*/ { 413 ,  0,  6, 14, -7, 5 },   // 신호기 막대 부분 왼
        /*99*/ { 423 , 17,  6, 14, 12, 8 },   // 신호기 막대 부분 오
        /*100*/ { 394 , 214,  67, 66, 34, 33 },   // 화살표 측선
		/*101*/ { 455 ,  0, 14, 14, -13, 6 },   // Shuting signalㅛ
		/*102*/ { 450 , 20, 14, 14, 26, 7 },   // Shuting signal
        /*103*/ { 414 , 0, 265, 37, 133, 18 },   // option button pack
        /*104*/ { 587 , 162, 181, 27, 14, 13 },   // LS마크
		/*105*/ { 132 ,  0, 14, 13, -14, 6 },   // Call on 오른쪽
		/*106*/ { 132 , 23, 14, 13, 27, 6 },   // Call on 왼쪽
        /*107*/ { 147 , 0, 20, 8, -14, 14 },   // junction indicator L 위
        /*108*/ { 147 , 28, 20, 8, 33, -7 },   // junction indicator R 아래
        /*109*/ { 147 , 5, 20, 8, -14, -7 },   // junction indicator L 아래
        /*110*/ { 147 , 23, 20, 8, 33, 14 },   // junction indicator R 위
        /*111*/ { 169 , 0, 43, 13, -10, 6 },   // 3현시 오른쪽 방향
        /*112*/ { 169 , 23, 43, 13, 52, 6 },   // 3현시 왼쪽 방향
        /*113*/ { 250 , 0, 31, 13, -10, 6 },   // 2현시 오른쪽 방향
        /*114*/ { 250 , 23, 31, 13, 40, 6 },   // 2현시 왼쪽 방향
        /*115*/ { 310 , 3, 42, 17, 23, 8 },   // RGB
        /*116*/ { 94 , 40, 13, 13, -28, 6 },   // Filament fail red 우(4현시)  
        /*117*/ { 100 , 63, 13, 13, 40, 6 },   // Filament fail red 좌(4현시)
        /*118*/ { 256 , 40, 13, 13, -16, 6 },   // Filament fail red 우
        /*119*/ { 262 , 63, 13, 13, 28, 6 },   // Filament fail red 좌
        /*120*/ { 458 , 143, 64, 53, 30, 26 },   // counter
        /*121*/ { 610 , 0, 68, 19, 34, 15 },   // TCB 우
        /*122*/ { 610 , 20, 68, 19, 34, 15 },   // TCB 좌
        /*123*/ { 680 , 0, 68, 19, 34, 9 },   // TGB 우
        /*124*/ { 680 , 20, 68, 19, 34, 9 },   // TGB 좌
        /*125*/ { 610 , 204, 38, 23, 16, 7 },   // Toggle 스위치(ON)
        /*126*/ { 610 , 244, 38, 23, 16, 7 },   // Toggle 스위치(OFF)
        /*127*/ { 426 , 7, 32, 24, 15, 11 },   // OPT1 (Alarm Switch)
        /*128*/ { 461 , 7, 32, 24, 15, 11 },   // OPT2 (BUZZER Switch)
        /*129*/ { 496 , 7, 32, 24, 15, 11 },   // OPT3 (Battery Switch)
        /*130*/ { 531 , 7, 32, 24, 15, 11 },   // OPT4 (COM Switch)
        /*131*/ { 566 , 7, 32, 24, 15, 11 },   // OPT5 (S Switch)
        /*132*/ { 601 , 7, 32, 24, 15, 11 },   // OPT6 (P Switch)
        /*133*/ { 636 , 7, 32, 24, 15, 11 },   // OPT7 (GROUND Switch)
		/*134*/ { 106 , 0, 13, 13, -40, 6 },   // outer 오른쪽
		/*135*/ { 88 , 23, 13, 13, 52, 6 },   // outer 왼쪽
		/*136*/ { 567 , 204, 31, 42, 15, 23 },   // PAS IN
		/*137*/ { 567 , 256, 31, 42, 15, 23 },   // PAS OUT
		/*138*/ { 778 , 209, 33, 18, 16, 9 },   // Night 
		/*149*/ { 778 , 234, 33, 18, 16, 9 },   // Day 
		/*140*/ { 741 , 257, 33, 18, 16, 9 },   // ON, N 
		/*141*/ { 741 , 282, 33, 18, 16, 9 },   // OFF, R 
		/*142*/ { 741 , 209, 33, 18, 16, 9 },   // ON, N (왼쪽)
		/*143*/ { 741 , 234, 33, 18, 16, 9 },   // OFF, R (왼쪽)
		/*144*/ { 382 , 20, 19, 8, 9, 4 },   // Hand point Key
		/*145*/ { 683 , 0, 121, 31, 60, 15 },   // Block TCB(L), TGB(R)
		/*146*/ { 683 , 0, 31, 31, 60, 15 },   // ca
		/*147*/ { 713 , 0, 31, 31, 30, 15 },   // lcr
		/*148*/ { 743 , 0, 31, 31, 0, 15 },   // cbb
		/*149*/ { 773 , 0, 31, 31, -30, 15 },   // arrow
		/*150*/ { 806 , 0, 121, 31, 60, 15 },   // Block TCB(R), TGB(L)
		/*151*/ { 836 , 0, 31, 31, 30, 15 },   // ca
		/*152*/ { 866 , 0, 31, 31, 0, 15 },   // lcr
		/*153*/ { 896 , 0, 31, 31, -30, 15 },   // cbb
		/*154*/ { 806 , 0, 31, 31, 60, 15 },   // arrow
		/*155*/ { 1 , 288, 13, 13, -65, 6 },   // loop indicator L
		/*156*/ { 1 , 288, 13, 13, 77, 6 },   // loop indicator R
		/*157*/ { 700 , 209, 21, 21, 6, 11 },   // Level ca 
		/*158*/ { 700 , 249, 21, 21, 6, 11 },   // Level ca(ready) 
		/*159*/ { 557 , 95, 31, 27, 16, 14 },   // Charger Fail 
		/*160*/ { 605 , 204, 34, 26, 17, 5 },   // OPT8 (LCC1) 
		/*161*/ { 639 , 204, 34, 26, 17, 5 },   // OPT9 (LCC2) 
		/*162*/ { 419 , 194,  20,  39,  -50, 19 },   // 인접역 표시 화살표 머리 우
		/*163*/ { 301 , 277,  20,  39,  70, 19 },   // 인접역 표시 화살표 머리 좌
		/*164*/ { 0 , 272, 19, 13, 10, 7 },   // wanner(L) 
		/*165*/ { 23 , 272, 19, 13, 10, 7 },   // wanner(R)
		/*166*/ { 50 , 272, 13, 13, -53, 6 },   // outer 3현시 오른쪽
		/*167*/ { 50 , 272, 13, 13, 65, 6 },   // outer 3현시 왼쪽
		/*168*/ { 50 , 290, 4, 1, -49, 6 },   // 바 지우기 (오른쪽)
		/*169*/ { 50 , 290, 4, 1, 52, 6 },   // 바 지우기 (왼쪽)
		/*170*/ { 282 ,  0, 14, 13, -14, 6 },   // Shunt 오른쪽
		/*171*/ { 282 , 23, 14, 13, 27, 6 },   // Shunt 왼쪽
		/*172*/ { 297 , 0, 8, 13, 4, 6 },   // Astart 바 오른쪽
		/*173*/ { 297 , 23, 8, 13, 3, 6 },   // Astart 바 왼쪽
		/*174*/ { 883 , 184, 31, 31, -5, 15 },   // Token right
		/*175*/ { 853 , 184, 31, 31, 27, 15 },   // Token left
		/*176*/ { 76, 300,  113,  23,  56, 12 },   // 비활성 인접역 표시 화살표 우
		/*177*/ { 76, 328,  113,  23,  56, 12 },   // 비활성 인접역 표시 화살표 좌
		/*178*/ { 250, 0,  5,  12,  3, 6 },   // 신호기 우측 바
		/*179*/ { 276, 23,  5,  12,  3, 6 },   // 신호기 좌측 바
		/*180*/ { 82 , 0, 13, 13, -16, 6 },   // outer 오른쪽
		/*181*/ { 112 , 23, 13, 13, 28, 6 },   // outer 왼쪽
        /*182*/ { 640, 280, 20, 21, 10, 10},
        /*183*/ { 297, 40, 12, 13, 40, 6},      // Free to Shunt OFF
        /*184*/ { 297, 80, 12, 13, 40, 6},      // Free to Shunt ON
//		/*185*/ { 0 , 343, 19, 13, 2, 7 },   // STOP(L) ->
//		/*186*/ { 23 , 343, 19, 13, 18, 7 },   // STOP(R) <-
		/*185*/ { 206 , 250, 19, 13, 2, 7 },   // STOP(L) ->
		/*186*/ { 206 , 273, 19, 13, 2, 7 },   // STOP(R) <-
		/*187*/ { 212 , 250, 13, 13, 0, 6 },   // 1현시 R ON ->
		/*188*/ { 212 , 290, 13, 13, 0, 0 },   // 1현시 R OFF <-
        /*189*/ { 414 , 0, 230, 37, 133, 18 },   // 아카우라 바이패스역 option button pack
		/*190*/ { 668 , 0, 12, 37, -90, 18 },     // 아카우라 바이패스역 option button pack 끝부분
		/*191*/ { 0 , 361, 26 , 28 , 13 , 9 },		// Axle Counter Normal
		/*192*/ { 26 , 361, 26 , 28 , 13 , 9 },		// Axle Counter Red
		/*193*/ { 209 , 364, 85 , 25 , 0 , 0 },		// Axle Counter-Counter 1칸
		/*194*/ { 56 , 364, 145 , 25 , 0 , 0 },		// Axle Counter-Counter 2칸
		/*195*/ { 463 , 247 , 17 , 21 , 9 , 11 },	// LC Road wanner
		/*196*/ { 480 , 247 , 17 , 21 , 9 , 11 },	// LC Road wanner 뒤집힌모양.
		/*197*/ { 464 , 280 , 23 , 23 , 12 , 7}		// LC Gate Lodge
//	int x,y,w,h,cx,cy;

};

BOOL _BlinkDuty;
BOOL _Blink;

CDC *_MyImageDC = NULL;
CDC *_MyImageDC1 = NULL;
CDC *_MyImageDC2 = NULL;
CDC *_MyTimer = NULL;

DWORD _DrawMode = MERGECOPY;

SymbolBitmapInfo *SymbolInfo = SymbolInfo10;

// Bipmap 이미지 불러오기
void BmpDraw::Draw(int id) 
{

	if (id > 300) return;

	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if (Base & MY_MOVE_OUT)
		color |= MY_MOVE_OUT;
	if ((Base & MY_FLASHING) && (Base & MY_MOVE_OUT) && _BlinkDuty) 
		color = 0;
	else if ((Base & MY_FLASHING) && !(Base & MY_MOVE_OUT) && !_BlinkDuty) 
		color = 0;
	if ((Base & 0x2000) && _BlinkDuty) 
	{
		color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;	
	}

#ifdef _LSD
#else
	if ( id == 1 ) {
/*
		if ( m_strStationName.Find ( "AKHA",0 ) >=0 ) {
			BGDraw();
		}
*/
		BGDraw( m_iDispBG );
		return;
	}
#endif


	SymbolBitmapInfo &b = SymbolInfo[id];
	if ((id == 34) || (id == 37))
	{
		_DrawMode = SRCPAINT;
	}
	else
	{
		_DrawMode = MERGECOPY;
	}

	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting()) {
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			,SRCCOPY);
		}
		else
		{
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			,_DrawMode );
		}            
	}

}
// Bipmap 겹치기
void BmpDraw::OrDraw(int id) {
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;


	SymbolBitmapInfo &b = SymbolInfo[id];

	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			,SRCPAINT);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			,SRCPAINT );
            
	}
}

void BmpDraw::BGDraw(int iDispBG ) 
{
	if ( m_pDC && iDispBG == 1 )
	{
		m_pDC->BitBlt( 521, 166, 1398, 598, _MyImageDC2, 1, 1,SRCPAINT);
	} else if ( m_pDC && iDispBG == 2 ) {
		//m_pDC->BitBlt( 1, 1, 1398, 598, _MyImageDC2, 1400, 200,SRCPAINT);
	}
}

// Bitmap 바꾸기
void BmpDraw::CHDraw(int id) {
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;				

	SymbolBitmapInfo &b = SymbolInfo[id];

	if (m_pDC && WP.x && WP.y)
	{
		m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color,SRCAND);
		m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x+56, b.y + color,SRCPAINT );  
	}
}

// OUTER 신호기 그리기

void BmpDraw::HomeDraw(int id) {
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_RED & 0xff0)>>4) * _nGRIDSIZE;				

	SymbolBitmapInfo &b = SymbolInfo[id];
	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			,SRCCOPY);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			,MERGECOPY );
            
	}
}

// RGB 상태창 그리기

void BmpDraw::RGBDraw(int id) {
int color = 0;
	if (Base == 1 ) color = _Blink;

	SymbolBitmapInfo &b = SymbolInfo[id];
	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			,SRCCOPY);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			,MERGECOPY );
            
	}
}


// Shunt 신호기 그리기

void BmpDraw::BMPOutSh( int dir )
{
	BOOL chek=0;
	if(dir > 19 && dir < 24)   //입환 신호기
	{						 
		dir -= 4;
		chek=1;
	}
	int id = 101;
	if (dir & 1)
	{
		id = 102;
	//	Draw(55);										// 막대 그리기 
	}
	//else Draw(54);
	int color = ((Base & 0xfff)>>4)*10;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && !_BlinkDuty) color = (MY_YELLOW>>4)*10;

	SymbolBitmapInfo &b = SymbolInfo[id];
	int xx = b.x;
	if (dir & 2) 
		xx += 22;		
	if ( m_pDC && WP.x && WP.y)
	{
		m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy,
			 b.w, b.h, _MyImageDC,
			 xx, b.y + color, SRCCOPY);
	}
	if(chek)
	{								       // 입환 신호기 안에 원찍기
		SymbolBitmapInfo b1 = SymbolInfo[53];
		int yy = (WP.y - b.cy) + (b.h / 2);        // 원 위치가 신호기 중앙에 
			xx = (WP.x - b.cx) + (b.w / 2);	
		if(id == 6)   xx-=1;
		if (!(dir & 2)) yy-=1;
		if ( m_pDC && WP.x && WP.y)
		{
			m_pDC->BitBlt( xx - b1.cx, yy - b1.cy,
				 b1.w, b1.h, _MyImageDC,b1.x, b1.y, SRCCOPY);
		}
	}
}

extern int _nDotPerCell;

// Track 그리기

void BmpDraw::DrawLong()
{	
	if (!m_nRepeat) return;
	int color = ((Base & 0xff0)>>6) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>6) * _nGRIDSIZE;

	while (m_nRepeat)
	{
		int len = m_nRepeat;
		if (len > 58) len = 58;
		SymbolBitmapInfo &b = SymbolInfo[81]; // 트랙 일부분(5*10)
		if (m_pDC && WP.x && WP.y)
		{
			if (m_pDC->IsPrinting())				
				m_pDC->BitBlt( WP.x - b.cx , WP.y - b.cy, len * b.w , b.h, _MyImageDC1, b.x, b.y + color, SRCCOPY);
			else				
				m_pDC->BitBlt( WP.x - b.cx , WP.y - b.cy, len * b.w , b.h, _MyImageDC, b.x, b.y + color, SRCCOPY);
		}

		m_nRepeat -= len;
		WP.x += len * (_nDotPerCell/2);
	}

}

CDC *BmpDraw::m_pDC = NULL;
int BmpDraw::m_nRepeat = 0;

// Track상에 그림띄우기(사용금지, 모타카, 단전 등)

void BmpDraw::DrawInhibit( CPoint &point, int nType )
{
	if (m_pDC)
	{
        int bx = 250;
        if (nType < 1 || nType > 3) return; //nType = 1,2,3
        int by = 10 + nType * 20;
		m_pDC->BitBlt( point.x - 10 , point.y - 10, 20, 20, _MyImageDC, bx+20, by, SRCAND);
		m_pDC->BitBlt( point.x - 10 , point.y - 10, 20, 20, _MyImageDC, bx, by, SRCPAINT);
	}
}

void BmpDraw::PutChar( CPoint &point, int nChar, int nColor )
{
    if ( (nChar >= '0' && nChar <= '9') || (nChar >= 'A' && nChar <= 'Z') )
	{
        nChar -= '0';
    }
    else nChar = 10;    // SPACE

    int yBase = 250 + ((nColor >> 6) & 0xF )* 10;
	m_pDC->BitBlt( point.x, point.y - 4, 5, 8, _MyImageDC, nChar * 5, yBase, SRCCOPY);
    point.x += 5;
}

// 방향과 진로특성을 가진 버튼 띄우기.

void BmpDraw::DrawButton(int id, DWORD _DrawMode2)
{
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;				

	SymbolBitmapInfo &b = SymbolInfo[id];
	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			, MERGECOPY);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			, MERGECOPY);
            
	}
}
