// IOList.cpp : implementation file
//

#include "stdafx.h"
#include "../LSD/LSD.h"
#include "Parser.h"
#include "IOList.h"

#include "ScrInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void ErrorLogging( char *fmt, ... );

CString GetSwitchName( CString &str );

/////////////////////////////////////////////////////////////////////////////
// CIOList dialog


CIOList::CIOList(CString &strPath, CObList *pInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CIOList::IDD, pParent)
{
	m_strFilePath = strPath;
	m_Document.SetPathName( m_strFilePath );	// RDF 파일 데이터 테이블
	m_pListInfo = pInfo;						// ILT 파일의 라인 텍스트 LIST 데이터 

	m_pSignal = NULL;
	m_pTrack = NULL;
	m_pSwitch = NULL;
	m_pGeneral = NULL;

	m_pIOCardInfo = NULL;

	memset(m_pAssignTable, 0x00, sizeof(m_pAssignTable));
	memset(m_pCtcElementTable, 0x00, sizeof(m_pCtcElementTable));
	memset(m_pConfigListTable, 0x00, sizeof(m_pConfigListTable));
	//{{AFX_DATA_INIT(CIOList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CIOList::~CIOList()
{
	int count = m_pTableList.GetSize();		
	for (int i=0; i<count; i++ ) {
		m_pTable = (CLinkTable*)m_pTableList.GetAt( i );			
		delete m_pTable;
	}
	count = m_pTableListRdf.GetSize();
	for ( i=0; i<count; i++ ) {
		m_pTable = (CLinkTable*)m_pTableListRdf.GetAt( i );			
		delete m_pTable;
	}
}

void CIOList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIOList)
	DDX_Control(pDX, IDC_LIST_NOASSIO, m_listNoAssIO);
	DDX_Control(pDX, IDC_LIST_MODULE, m_listModule);
	DDX_Control(pDX, IDC_LIST_ASSIO, m_listAssIO);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIOList, CDialog)
	//{{AFX_MSG_MAP(CIOList)
	ON_BN_CLICKED(IDC_MAKETABLE, OnMaketable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIOList message handlers

#define NUM_COLUMNS	6

//
// DON01001.DES 981229-233420  5458 981229-233526  3454   40
//          1         2         3         4         5
//012345678901234567890123456789012345678901234567890123456789
// 1            14            28    34            48    54   59

static _TCHAR *_gszColumnLabelModule[]= {	_T("MODULE"), _T("NO"),    _T("USED") };
static int _gnColumnFmtModule[] =		{	LVCFMT_LEFT,  LVCFMT_LEFT, LVCFMT_LEFT };
static int _gnColumnWidthModule[] =		{	75,			  35,          50 };

BOOL CIOList::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int i, j;  // insert columns
	short n;
	LV_COLUMN lvc;

	if ( m_pIOCardInfo ) {
		memset(m_pIOCardInfo, 0x00, 16);
	}
// 모듈정보: 리스트박스 생성 - 타이틀 및 컬럼 분할 (3개)
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	for(i = 0; i<3; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabelModule[i];
		lvc.cx = _gnColumnWidthModule[i];
		lvc.fmt = _gnColumnFmtModule[i];
		m_listModule.InsertColumn(i,&lvc);
	}

// insert items
	LV_ITEM lvi;
// set item text for additional columns


// RDF 파일 읽기 및 데이터 테이블 생성
	if (m_Document.OnOpenDocument( m_strFilePath ))		// Rdf file open and read....
	{	
		n = 0;
		int  nBytePos = -1;
		for (i=0; i<MAX_RACK; i++) 
		{
			int  nBitPos = 0;
			for (j=0; j<CARD_PER_RACK; j++) 
			{
				IOCardType *pCard = &m_Document.m_Rack[ i ].m_IOCard[ j ];
				short addr = j + i * (CARD_PER_RACK) + 1;
				if (pCard->nType != 0) 
				{
					lvi.mask = LVIF_TEXT;	// | LVIF_STATE;
					lvi.iItem = n;
					lvi.iSubItem = 0;
					lvi.pszText = szCardTypeStr[pCard->nType];
					lvi.iImage = 0;
					lvi.stateMask = 0; //LVIS_STATEIMAGEMASK;
					lvi.state = 0; //INDEXTOSTATEIMAGEMASK(1);

					m_listModule.InsertItem(&lvi);				// Module 1th column
					CString strAddr;
					strAddr.Format( "%d", addr );
					m_listModule.SetItemText(n++,1, strAddr );	// Module 2th column
				}

				if ( (j % 4) == 0 ) 
				{	
					nBitPos = 0;
				}

				nBytePos = (addr-1) / 4;
				if ( m_pIOCardInfo && pCard->nType ) 
				{
			 		int mode =0;
					if (pCard->nType == CardINPUT)
						mode = 1;
					else if (pCard->nType == CardOUTPUT)
						mode = 2;

					m_pIOCardInfo[nBytePos] |= (pCard->nType & 3) << nBitPos;
				}

				if ( m_Document.m_Rack[ i ].IsEmptyCardItem( j ) ) 
				{	
					m_Document.m_pRealAddress[ (i*CARD_PER_RACK)+j ] |= 0x80;
				}

				nBitPos += 2;
			}
			if ( m_pIOCardInfo ) 
			{
				nBytePos++;
			}
		}
	}

	CreateDefaultIO();		// ILT 파일 라인 분석 및 LSD에 정의된 기본 I/O 생성 

static _TCHAR *_gszColumnLabelNoAssIO[]= {	_T("NAME"),	_T("TYPE") };
static int _gnColumnFmtNoAssIO[] =		{	LVCFMT_LEFT,	LVCFMT_LEFT };
static int _gnColumnWidthNoAssIO[] =		{	130,			80 };
// 미지정 입출력 정보(DES의 기본적인 데이터): 리스트 박스 생성 - 타이틀 및 컬럼 생성

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	for(i = 0; i<2; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabelNoAssIO[i];
		lvc.cx = _gnColumnWidthNoAssIO[i];
		lvc.fmt = _gnColumnFmtNoAssIO[i];
		m_listNoAssIO.InsertColumn(i,&lvc);
	}


	n = 0;
	POSITION pos;
	CString err="";
	for( pos = m_DefaultIO.GetHeadPosition(); pos != NULL; )		// ILT 에서 생성된 데이터 LIST
	{
		CString &item = *(CString*)m_DefaultIO.GetNext( pos );

		short id = m_Document.FindIOName( (char*)(LPCTSTR)item );	// RDF 파일 데이터 테이블에서 검색 (위치 리턴)

		if (id == 0) 
		{
			lvi.mask = LVIF_TEXT;	// | LVIF_STATE;
			lvi.iItem = n;
			lvi.iSubItem = 0;
			lvi.pszText = ((char*)(LPCTSTR)item) + 2;
			lvi.iImage = 0;
			lvi.stateMask = 0; //LVIS_STATEIMAGEMASK;
			lvi.state = 0; //INDEXTOSTATEIMAGEMASK(1);

			m_listNoAssIO.InsertItem(&lvi);
			CString strType;
			char c = item.GetAt(0);
			if (c == 'I') strType = "INPUT";
			else if (c == 'O') strType = "OUTPUT";
			else if (c == 'X') strType = "COMIN";
			else if (c == 'Z') strType = "COMOUT";
			else if (c == 'S') strType = "SDM";
			m_listNoAssIO.SetItemText( n++, 1, strType);
			err+=("/"+item);
		}
	}
static _TCHAR *_gszColumnLabelAssIO[]= {	_T("NAME"),		_T("TYPE"),		_T("PORT") };
static int _gnColumnFmtAssIO[] = {			LVCFMT_LEFT,	LVCFMT_LEFT,	LVCFMT_LEFT };
static int _gnColumnWidthAssIO[] = {		130,			85,				60 };
// 지정 입출력 정보(실제 정의된 정보): 리스트 박스 생성 - 타이틀 및 컬럼 생성 
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	for(i = 0; i<3; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabelAssIO[i];
		lvc.cx = _gnColumnWidthAssIO[i];
		lvc.fmt = _gnColumnFmtAssIO[i];
		m_listAssIO.InsertColumn(i,&lvc);
	}

	n = 0;
	short m = 0;
	for (i=0; i<MAX_RACK; i++) 
	{				// MAX_RACK = 8
		for (j=0; j<CARD_PER_RACK; j++) 
		{		// CARD_PER_RACK = 7
			IOCardType *pCard = &m_Document.m_Rack[ i ].m_IOCard[ j ];
			short addr = j + (i * (CARD_PER_RACK)) + 1;
			if (m_Document.m_pRealAddress[ (i*CARD_PER_RACK)+j ] & 0x80) {	// Empty Card = Spare Card.
				;		
			} 
			else if (pCard->nType != 0) 
			{
				CString strType;
				strType.Format( "%s:%d", szCardTypeStr[pCard->nType], pCard->nCardNo );
				short used = 0;
				for (short k = 0; k < 32; k++) 
				{
					if (pCard->szNames[ k ][0] != 0) 
					{
						used++;

						lvi.mask = LVIF_TEXT;	// | LVIF_STATE;
						lvi.iItem = n;
						lvi.iSubItem = 0;
						lvi.pszText = pCard->szNames[ k ];
						lvi.iImage = 0;
						lvi.stateMask = 0; //LVIS_STATEIMAGEMASK;
						lvi.state = 0; //INDEXTOSTATEIMAGEMASK(1);

						m_listAssIO.InsertItem(&lvi);					// real	1th column
						CString strAddr;
						strAddr.Format( "%d", k+1 );
						m_listAssIO.SetItemText(n, 1, strType);			//		2th column
						m_listAssIO.SetItemText(n++, 2, strAddr);		//		3th column
					}
				}
				CString strUsed;
				strUsed.Format( "%d", used );
				m_listModule.SetItemText(m++,2, strUsed );		// module 3th column
			}
		}
	}	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


//   IN/OUT - Name - BitLoc
char *_ppSignalDefualt[] = {
	"O HR 1  O DR 0  O HHR 2                       O ALR 25  I LOR 24  I HHLOR 30             I HRPR 5  I DRPR 4  I HHRPR 6"  , // Outer Home
	"O HR 1  O DR 0  O CR  2  O SR 3  O DIR(L) 12  O ALR 25  I LOR 24  I CLOR  30  I SLOR 27  I HRPR 5  I DRPR 4  I CRPR  6  I SRPR 7  I DIRLOR(L) 22",					 // Home has Left direction
	"O HR 1  O DR 0  O CR  2  O SR 3  O DIR(R) 13  O ALR 25  I LOR 24  I CLOR  30  I SLOR 27  I HRPR 5  I DRPR 4  I CRPR  6  I SRPR 7                  I DIRLOR(R) 23 ",  // Home has Right direction
	"O HR 1  O DR 0  O CR  2  O SR 3               O ALR 25  I LOR 24  I CLOR  30  I SLOR 27  I HRPR 5  I DRPR 4  I CRPR  6  I SRPR 7  I DIRLOR(L) 22  I DIRLOR(R) 23",	 // Home have both direction
	"        O DR 0                                O ALR 25  I LOR 24                                   I DRPR 4                                                       Z BLKCOMMOUT 28 X ADJCONDIN 29 Z ADJCONDOUT 29",	 // Advanced Starter
	"O HR 1  O DR 0  O HHR 2  O SR 3               O ALR 25  I LOR 24  I HHLOR 30  I SLOR 27  I HRPR 5  I DRPR 4  I HHRPR 6  I SRPR 7  I DIRLOR(L) 22  I DIRLOR(R) 23",  // Main Starter
	"O HR 1                   O SR 3               O ALR 25  I LOR 24              I SLOR 27  I HRPR 5                       I SRPR 7  I DIRLOR(L) 22  I DIRLOR(R) 23",	 // Loop Starter
	"Z GCAOUT 0  Z GLCROUT 1  Z GCBBOUT 4  Z GCOMPLETEOUT 15  Z GDEPARTOUT 10  Z GARRIVEOUT 12  O NBGR 24  O RBGR 25  X CCAIN 0  X CLCRIN 2  X CCBBIN 5  X CCOMPLETEIN 7  X CDEPARTIN 11  X CARRIVEIN 13  X CAACKIN 22 I NBGPR 28  I RBGPR 29 O AXLRST 18 I AXLOCC 19 I AXLDST 20 X AXLOCCIN 19 X AXLDSTIN 20 Z AXLOCCOUT 19 Z AXLDSTOUT 20 X AXLREQIN 32 X AXLACCIN 33 X AXLDECIN 34 Z AXLREQOUT 32 Z AXLACCOUT 33 Z AXLDECOUT 34 O SAXLRST 40 I SAXLOCC 41 I SAXLDST 42 X SAXLOCCIN 41 X SAXLDSTIN 42 Z SAXLOCCOUT 41 Z SAXLDSTOUT 42 X SAXLREQIN 43 X SAXLACCIN 44 X SAXLDECIN 45 X SAXLACTIN 47 Z SAXLREQOUT 43 Z SAXLACCOUT 44 Z SAXLDECOUT 45 Z SAXLACTOUT 46 Z BLKRSTREQ 16 X BLKRSTREQIN 17 Z BLKRSTACC 35 X BLKRSTACCIN 36 Z BLKRSTDEC 37 X BLKRSTDECIN 38 O GCADOUT 0  O GLCRDOUT 1  O GCBBDOUT 4  O GCOMPLETEDOUT 15  O GDEPARTDOUT 10  O GARRIVEDOUT 12  I CCAACKDIN 22  I CLCRDIN 2  I CCBBDIN 5  I CCOMPLETEDIN 7  I CARRIVEDIN 13  I AXLOCCDIN 19 I AXLDSTDIN 20 O AXLOCCDOUT 39 I AXLREQDIN 32 I AXLACCDIN 33 I AXLDECDIN 34 O AXLREQDOUT 32 O AXLACCDOUT 33 O AXLDECDOUT 34 Z SWEEPING 23 X SWEEPINGACKIN 14", // TGB BLOCK (Tokenless)
	"Z CCAOUT 0  Z CLCROUT 1  Z CCBBOUT 4  Z CCOMPLETEOUT 15  Z CDEPARTOUT 10  Z CARRIVEOUT 12  Z CAACKOUT 22 O NBCR 26  O RBCR 27  X GCAIN 0  X GLCRIN 2  X GCBBIN 5  X GCOMPLETEIN 7  X GDEPARTIN 11  X GARRIVEIN 13  I NBCPR 30  I RBCPR 31 O AXLRST 18 I AXLOCC 19 I AXLDST 20 X AXLOCCIN 19 X AXLDSTIN 20 Z AXLOCCOUT 19 Z AXLDSTOUT 20 X AXLREQIN 32 X AXLACCIN 33 X AXLDECIN 34 Z AXLREQOUT 32 Z AXLACCOUT 33 Z AXLDECOUT 34 O SAXLRST 40 I SAXLOCC 41 I SAXLDST 42 X SAXLOCCIN 41 X SAXLDSTIN 42 Z SAXLOCCOUT 41 Z SAXLDSTOUT 42 X SAXLREQIN 43 X SAXLACCIN 44 X SAXLDECIN 45 X SAXLACTIN 47 Z SAXLREQOUT 43 Z SAXLACCOUT 44 Z SAXLDECOUT 45 Z SAXLACTOUT 46 Z BLKRSTREQ 16 X BLKRSTREQIN 17 Z BLKRSTACC 35 X BLKRSTACCIN 36 Z BLKRSTDEC 37 X BLKRSTDECIN 38 O CCAACKDOUT 22  O CLCRDOUT 1  O CCBBDOUT 4  O CCOMPLETEDOUT 15  O CARRIVEDOUT 12  I GCADIN 0  I GLCRDIN 2  I GCBBDIN 5  I GCOMPLETEDIN 7  I GDEPARTDIN 11  I GARRIVEDIN 13  I AXLOCCDIN 19 I AXLDSTDIN 20 O AXLOCCDOUT 39 I AXLREQDIN 32 I AXLACCDIN 33 I AXLDECDIN 34 O AXLREQDOUT 32 O AXLACCDOUT 33 O AXLDECDOUT 34 Z SWEEPINGACK 14 X SWEEPINGIN 23", // TCB BLOCK (Tokenless)
	"I TGPR 2,  I TOLPR 10",  // TGB BLOCK (Token)
	"I TCPR 1",				 // TCB BLOCK (Token)
	"                         O SR 3               O ALR 25  I LOR 24              I SLOR 27                                              I SRPR 7  I DIRLOR(L) 22", // SHUNT
};
	
// 세번째 인수는 쓰이지 않고 대신 테이블의 위치(i) 가 Scr + 0x40 에
// 더해져서 위치 결정됨.
char *_ppDefaultSystemIO[] = {
    "I ACTIVEON1I		24",		// CBI 1 main 
    "I ACTIVEON2I		25",		// CBI 2 main
    "I VPOR1			26",
    "I VPOR2			27",
    "I FAN1				38",
    "I FAN2				39",
    "I FAN3				316",
    "I FAN4				317",
    "I FUSE1			42",
    "I FUSE2			43",
    "I FUSE3			318",
    "I FUSE4			319",
	"I FUSE_PR          54",
	"I 24V(N)EXT		46",	
	"I 24V(INT)			11",	
	"I 24V(S)EXT		46",	
    "I GEN(RUN)			33",	
	"I GEN(LOWFUEL)		14",	
	"I GEN(FAIL)		15",	
	"I MAIN_POWER		23",
	"I CHARGE_FAIL		16",
	"I LOW_VOLTAGE		45",
	"I CHARGING			44",
	"I UPS2_FAIL		13",
	"I UPS1_FAIL		12",
	"I GRD_FAIL			2",
	"I D/NRI	    	47",
	"I NKWEPR(N)		50",
	"I NKWEPR(S)		51",
	"I MRI				56",
	"I LC1NKXPR			289",
	"I LC1BELACK		291",
	"I LC2NKXPR			293",
	"I LC2BELACK		295",
	"I LC3NKXPR			297",
	"I LC3BELACK		299",
	"I LC4NKXPR			301",
	"I LC4BELACK		303",
	"I NKWEPR(A)		304",
	"I NKWEPR(B)		305",
	"I NKWEPR(C)		306",
	"I NKWEPR(D)		307",
	"I NKWEPR(E)		308",
	"I NKWEPR(F)		309",
	"I NKWEPR(G)		310",
	"I LC5NKXPR			22",
	"I LC5BELACK		56",
	"I UPONLINE			36",
	"I DNONLINE			37",
	"I MIDONLINE		20",
	"O VPOR1			28",	
	"O VPOR2			29",	
	"O ACTIVEON1O		40",	// CBI 1 Active
	"O ACTIVEON2O		41",	// CBI 2 Active
	"O TMON1			40",	
	"O TMON2			41",	
	"O ALR				48",
	"O TCS				49",
	"O D/NR				9",
	"O KWEZR(N)			52",
	"O KWEZR(S)			53",
	"O MRO				56",
	"O KWEZR(A)			57",
	"O KWEZR(B)			58",
	"O KWEZR(C)			59",
	"O KWEZR(D)			60",
	"O KWEZR(E)			61",
	"O KWEZR(F)			62",
	"O KWEZR(G)			63",
	"O DayNightO		9",
	"O TCSR				1",
	"O LC1KXLR			288",
	"O LC1BELL			290",
	"O LC2KXLR			292",
	"O LC2BELL			294",
	"O LC3KXLR			296",
	"O LC3BELL			298",
	"O LC4KXLR			300",
	"O LC4BELL			302",
	"O LC5KXLR			21",
	"O LC5BELL			55",
};

/////////////////////////////////////////////////////////////////////////////

#define SIZE_SCR_SIGNAL sizeof(ScrInfoSignal)
#define SIZE_SCR_SWITCH sizeof(ScrInfoSwitch)
#define SIZE_SCR_TRACK  sizeof(ScrInfoTrack)

void CIOList::CreateDefaultIO()
{
	short nSignal = 0, nTrack = 0, nSwitch = 0, nGeneral = 0;
	if (m_pTrack) {
		nTrack = m_pTrack->GetCount();
	}
	if (m_pSignal) {
		nSignal = m_pSignal->GetCount();
	}
	if (m_pSwitch) {
		nSwitch = m_pSwitch->GetCount();
	}	
	if (m_pGeneral) {
		nGeneral = m_pGeneral->GetCount();
	}	

	short n;
	short count=0;
	short bitno;

	if (m_pListInfo) 
	{
		CString *item;
		POSITION pos;

		for ( int i = 0; i < nTrack; i++ ) 
		{
			CString token = *(m_pTrack->GetAtIndex( i ));
			CString	strTrackName = token;

			CString io = "";
			if (token.GetAt(0) != '?')	
			{
				io = "I.";
				io += token;
				if(	!m_DefaultIO.CheckAdd( io , TRUE ) ) 
					continue;		// 중복 입력 방지  동일T 이면서 00.1 , 00.2 로 구분되는 T는 한개로 처리 

				BitLocType pLoc;
				pLoc.nBitLoc = m_Document.FindIOName( io );    // rack 위치 알아오기 ( rack,card,port)

				m_pTable = new CLinkTable();
				m_pTable->Name = token;

				if(pLoc.nBitLoc == 0)	
					m_pTable->RdfOffset = -1;
				else
					m_pTable->RdfOffset = (pLoc.nRack*(CARD_PER_RACK)*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;

				m_pTable->Type = CardINPUT;
				
				n = i;
				m_pTable->ScrOffset = (m_nOffsetTrack + (n * SIZE_SCR_TRACK)) * 8 + 1;     // 전체 1BYTE중  BIT0 -> BIT1
				m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 20130904 Track Disturb추가.
				token = strTrackName + "DST";
				io = "I." + token;

				BitLocType pLoc2;
				pLoc2.nBitLoc = m_Document.FindIOName( io );

				m_pTable = new CLinkTable();
				m_pTable->Name = token;

				if(pLoc2.nBitLoc == 0)
					m_pTable->RdfOffset = -1;
				else
					m_pTable->RdfOffset = (pLoc2.nRack*(CARD_PER_RACK)*32) + ((pLoc2.nCard-1)*32) + pLoc2.nPort;

				m_pTable->Type = CardINPUT;
				m_pTable->ScrOffset = (m_nOffsetTrack + (n * SIZE_SCR_TRACK)) * 8 + 8;
				m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 20150126 Track AXL Reset추가.
				token = strTrackName + "RSTIN";
				io = "I." + token;
				
				pLoc2.nBitLoc = m_Document.FindIOName( io );
				
				m_pTable = new CLinkTable();
				m_pTable->Name = token;
				
				if(pLoc2.nBitLoc == 0)
					m_pTable->RdfOffset = -1;
				else
					m_pTable->RdfOffset = (pLoc2.nRack*(CARD_PER_RACK)*32) + ((pLoc2.nCard-1)*32) + pLoc2.nPort;
				
				m_pTable->Type = CardINPUT;
				m_pTable->ScrOffset = (m_nOffsetTrack + (n * SIZE_SCR_TRACK)) * 8 + 9;
				m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);

				token = strTrackName + "RSTOUT";
				io = "O." + token;
				
				pLoc2.nBitLoc = m_Document.FindIOName( io );
				
				m_pTable = new CLinkTable();
				m_pTable->Name = token;
				
				if(pLoc2.nBitLoc == 0)
					m_pTable->RdfOffset = -1;
				else
					m_pTable->RdfOffset = (pLoc2.nRack*(CARD_PER_RACK)*32) + ((pLoc2.nCard-1)*32) + pLoc2.nPort;
				
				m_pTable->Type = CardOUTPUT;
				m_pTable->ScrOffset = (m_nOffsetTrack + (n * SIZE_SCR_TRACK)) * 8 + 10;
				m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				token = strTrackName + "SRPR";
				io = "I." + token;
				CString head = "X";
				short nBitLoc = m_Document.FindIOName( io );    // rack 위치 알아오기 ( rack,card,port)
				if ( nBitLoc < 0 ) 
				{
					token = strTrackName + "SR";
					io = "O." + token;
					head = "Z";
					nBitLoc = m_Document.FindIOName( io );    // rack 위치 알아오기 ( rack,card,port)
				}

				if (nBitLoc > 0) 
				{
					m_pTable = new CLinkTable();
					m_pTable->Name = token;
					m_pTable->RdfOffset = nBitLoc;
					n = i;
					if (head == "X")
					{
						m_pTable->Type = CardINPUT;
						m_pTable->ScrOffset = (m_nOffsetTrack + n) * 8 + 7;
						m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);

						token = strTrackName + "SR";
						io = "O." + token;
						nBitLoc = m_Document.FindIOName( io );    // rack 위치 알아오기 ( rack,card,port)
						if (nBitLoc)
						{
							head = "Z";
							m_pTable = new CLinkTable();
							m_pTable->Name = token;
							m_pTable->RdfOffset = nBitLoc;
						}
					}

					if (head == "Z") 
					{
						m_pTable->Type = CardOUTPUT;
						m_pTable->ScrOffset = (m_nOffsetTrack + n) * 8 + 3;
						m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);
					}
				}
			}
		}

		for ( i = 0; i < nGeneral; i++ ) 
		{
			CString token = *(m_pGeneral->GetAtIndex( i ));
			CString io = "";
			io = "I.";
			io += token;
			if(	!m_DefaultIO.CheckAdd( io , TRUE ) ) 
				continue;		// 중복 입력 방지  동일T 이면서 00.1 , 00.2 로 구분되는 T는 한개로 처리 
			BitLocType pLoc;
			pLoc.nBitLoc = m_Document.FindIOName( io );    // rack 위치 알아오기 ( rack,card,port)
			m_pTable = new CLinkTable();
			m_pTable->Name = token;
			if(pLoc.nBitLoc == 0)	
				m_pTable->RdfOffset = -1;
			else
				m_pTable->RdfOffset = (pLoc.nRack*(CARD_PER_RACK)*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;
			m_pTable->Type = CardINPUT;
			
			n = i;
			m_pTable->ScrOffset = (m_nOffsetGeneral + n) * 8;
			m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);
		}


		for( pos = m_pListInfo->GetHeadPosition(); pos != NULL; )
		{
			item = (CString*)m_pListInfo->GetNext( pos );
			if (item->GetAt(0) == '@') 
			{
				CParser line( *item, " =:,\r\n\t" );
				CString token;
				line.GetToken( token );

				CString io = "";
				if (token == "@SIGNAL") 
				{
                    line.GetToken( token );     // :
					line.GetToken( token );     // C1
					line.GetToken( token );     // =
					line.GetToken( token );     // signal name
					if ( (n = token.Find('.')) > -1 )	// 신호 이름에 X.X  처리 
						token = token.Left(n);

					CString signal = token;
					line.GetToken( token );     // ,
					line.GetToken( token );     // signal type
					CString sigtype = token.Left(2);
					if(token == "STOP")
					{
						sigtype = token;
					}
                    CString strSignalType = token;
					line.GetToken( token );     // ,
					line.GetToken( token );		// signal direction
					line.GetToken( token );     // ,
					line.GetToken( token );		// signal Aspect
					CString sigAspect = token.Left(3);
					line.GetToken( token );     // ,
					line.GetToken( token );		// Callon
					line.GetToken( token );     // ,
					line.GetToken( token );		// route indicator
					CString routeIndi = token.Left(2);
					if (routeIndi == "S")
					{
						line.GetToken( token );     // ,
						line.GetToken( token );		// route indicator
						routeIndi = token.Left(2);
					}

					byte c = 0;

					if (sigtype == "OH")
						c = 0;
					else if (sigtype == "HO" && routeIndi == "L")
						c = 1;
					else if (sigtype == "HO" && routeIndi == "R")
						c = 2;
					else if (sigtype == "HO" && (routeIndi == "LR" || routeIndi == "RI"))
						c = 3;
					else if (sigtype == "HO")	// 20181116 ashuganj 폐색장치의 home 신호기 처리를 위해 추가
						c = 3;
					else if (sigtype == "AS")
						c = 4;
					else if ( (sigtype == "ST" || sigtype == "IS") && (sigAspect == "GYR" || sigAspect == "GR") )
						c = 5;
					else if ( (sigtype == "ST" || sigtype == "IS") && (sigAspect == "YR" || sigAspect == "R") )
						c = 6;
					else if (sigtype == "TG")
						c = 7;
					else if (sigtype == "TC")
						c = 8;
					else if (sigtype == "OG")
						c = 9;
					else if (sigtype == "OC")
						c = 10;
					else if (sigtype == "SH")
						c = 11;
					else if (sigtype == "STOP")
						c = 11;
					else 
						continue;

					CString defstr = _ppSignalDefualt[c];						
					CParser def( defstr ); 						
					CString head, tail;

					if (c==7 || c==8 || c==4)    // Block
					{
						while ( def.GetToken( head ) )			
						{
							def.GetToken( tail );

								//io = head + "." + MakeBlockIOString( signal, tail );
							/*
							if ( head == 'I' ) 
								head = 'X';
							if ( head == 'O' ) 
								head = 'Z';
							*/

							io = head + "." + signal + tail;

							m_DefaultIO.CheckAdd( io, TRUE );

							BitLocType pLoc;
							pLoc.nBitLoc = m_Document.FindIOName( io );

							m_DefaultIO.CheckAdd( io , TRUE );
							def.GetToken( tail );
							bitno = atoi( tail );
							m_pTable = new CLinkTable();
							
							m_pTable->Name = io.Mid(2);

							if(pLoc.nBitLoc == 0)	
							{
								m_pTable->RdfOffset = -1;
							}
							else	
							{
								m_pTable->RdfOffset = pLoc.nBitLoc;
							}

							if(head == "I")
							{
								m_pTable->Type = CardINPUT;
							}
							else if(head == "O")
							{
								m_pTable->Type = CardOUTPUT;
							}
							else if (head == "X")
							{
								m_pTable->Type = CardCOMIN;
							}
							else if(head == "Z")
							{
								m_pTable->Type = CardCOMOUT;
							}

							n = m_pSignal->FindIndex( signal );		// 해당 신호기 몇번째 인가 신호기 배열 위치 
							if (n>=0)
								m_pTable->ScrOffset = (m_nOffsetSignal + (n * SIZE_INFO_SIGNAL)) * 8 + bitno;

							m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);
						}
					}

					while ( def.GetToken( head ) )			
					{
						if (head != ".")	
						{
							def.GetToken( tail );

							io = head + "." + signal + tail;

							m_DefaultIO.CheckAdd( io, TRUE );
							def.GetToken( tail );
							bitno = atoi( tail );

							BitLocType pLoc;
							pLoc.nBitLoc = m_Document.FindIOName( io );
							m_pTable = new CLinkTable();								
							m_pTable->Name = io.Mid(2);
							if(pLoc.nBitLoc == 0)	
							{
								m_pTable->RdfOffset = -1;
							}
							else	
							{
								m_pTable->RdfOffset = (pLoc.nRack*CARD_PER_RACK*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;
							}
							if(head == "I")
							{
								m_pTable->Type = CardINPUT;
							}
							else if(head == "O")
							{
								m_pTable->Type = CardOUTPUT;
							}
							n = m_pSignal->FindIndex( signal );		// 해당 신호기 몇번째 인가 신호기 배열 위치 
							if (n>=0)
							    m_pTable->ScrOffset = (m_nOffsetSignal + n * SIZE_SCR_SIGNAL) * 8 + bitno;

							m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);
						}
					}
				}
				else if (token == "@NODE")	 
				{
					line.GetToken( token ); // '='
					line.GetToken( token );
					CString sw = "";
					if (token.Find("@B") < 0) 
					{
						sw = token.Left( 2 );
						line.GetToken( token ); // ','
						line.GetToken( token );
					}
					int l = token.Find("@B");

					if (l > 0) 
					{
						if (sw == "") 
						{
							token = token.Left( l );
							sw = GetSwitchName( token );
						}
						io = "S." + sw;
						m_DefaultIO.CheckAdd( io , TRUE );

                        CString sLmName[] = {"NKR","RKR","NWPR","RWPR","WLRI","NKWPR","WLRO","NWR","RWR", "KWLZR"};
						if(m_pSwitch) 
						{
							n = m_pSwitch->FindIndex( sw );
						}
						short nSwSeq[] = { 0, 1, 2, 3, 4, 18, 13, 7, 6, 17 };
						for (i=0; i<10; i++) 
						{
							//BYTE nCrankNo = m_pSwitchInfoTable[i].nPointKeyNo;

							short seq = nSwSeq[i];
							BitLocType pLoc;
    						io = sw + sLmName[i];
							short id = m_Document.FindIOName( io );
							pLoc.nBitLoc = id;
							m_pTable = new CLinkTable();								
							m_pTable->ScrOffset = (m_nOffsetSwitch + (n * SIZE_SCR_SWITCH)) * 8 + seq;
							m_pTable->Name = io;

							if(i < 6)
								m_pTable->Type = CardINPUT;
							else 
								m_pTable->Type = CardOUTPUT;

							if(id == 0)	
								m_pTable->RdfOffset = -1;
							else
								m_pTable->RdfOffset = (pLoc.nRack*CARD_PER_RACK*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;

							m_pTableList.SetAtGrow(m_pTableList.GetSize(), (CObject*)m_pTable);
						}
					}
				}

				else if (token == "@TRACK")	 
				{
					line.GetToken( token ); // '='
					line.GetToken( token );
					int l = token.Find("@L");

					if (l > 0) 
					{
						CString track = "";
						if (track == "") 
						{
							track = token.Left( l );
							int d = token.Find(".");
							if (d > 0)
							    track = track.Left( d );
						}
						io = "O." + track + "LOSR";
						m_DefaultIO.CheckAdd( io , TRUE );

						BitLocType pLoc;
						short id = m_Document.FindIOName( io );
						pLoc.nBitLoc = id;
						m_pTable = new CLinkTable();

						n = m_pTrack->FindIndex( track );		 

						m_pTable->ScrOffset = (m_nOffsetTrack + (n * SIZE_SCR_TRACK)) * 8 + 7;
						io = track + "LOSR";
						m_pTable->Name = io;
						m_pTable->Type = CardOUTPUT;
						m_pTable->RdfOffset = (pLoc.nRack*CARD_PER_RACK*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;

						m_pTableList.SetAtGrow(m_pTableList.GetSize(), (CObject*)m_pTable);
					}
				}
			}
		}
	}


// Default System Input
	short nBitOffset;
	int   nMax = sizeof(_ppDefaultSystemIO) / sizeof(char*);
    for ( short i=0; i<nMax; i++ ) 
	{
	    CString defstr = _ppDefaultSystemIO[i];
        if ( defstr != "" ) 
		{
		    CParser def( defstr ); 						
		    CString head, loc, name;
		    def.GetToken( head );   // IO type
		    def.GetToken( name );   // IO Name
		    def.GetToken( loc );	// IO Pos
		    m_DefaultIO.CheckAdd( name , TRUE );

			nBitOffset = atoi( loc );	// bit position.

			BitLocType pLoc;
		    pLoc.nBitLoc = m_Document.FindIOName( name );
		    m_pTable = new CLinkTable();								
		    m_pTable->Name = name;
		    if ( pLoc.nBitLoc < 0 ) 
				m_pTable->RdfOffset = -1;
			else
				m_pTable->RdfOffset = (pLoc.nRack*(CARD_PER_RACK)*32) + ((pLoc.nCard-1)*32) + pLoc.nPort;

            m_pTable->ScrOffset = nBitOffset + 64;

			if(head == "I")		 
				m_pTable->Type = CardINPUT;
			else if(head == "O") 
				m_pTable->Type = CardOUTPUT;

		    m_pTableList.SetAtGrow(m_pTableList.GetSize(),(CObject*)m_pTable);
        }
    }

    MakeTableRdf();
}


void CIOList::OnOK() 
{
	// TODO: Add extra validation here
RdfTableSave();
	CDialog::OnOK();
}

BOOL CIOList::OnFileSave()
{
	/*
	// 코드 추가해야할 부분.
	// 미지정 정보 데이터 있는지 검사.
	//  있으면 저장 여부 묻고 취소는 FALSE 반환.
	//  저장 방법의 상태 저장.
	*/

	CString filename = m_strFilePath;
	int n = filename.GetLength();
	filename.SetAt(n-1, '1');
	return TRUE;
}

CLinkTable* CIOList::FindLinkTable(CString name, short type )
{
	CLinkTable *pTable = NULL;
	int count = m_pTableList.GetSize();		
	for (int i=0; i<count; i++ ) 
	{
		pTable = (CLinkTable*)m_pTableList.GetAt( i );

		if( pTable->Name == name ) 
		{
			if ( pTable->Type == type ) 
				return pTable;
			else
			{
				if ( type == CardCOMIN || type == CardCOMOUT)
				{
					if (pTable->Type == CardINPUT)
					{
						if (type == CardCOMOUT)
						{
							pTable->Type = CardOUTPUT;
							return pTable;
						}
						else
						    return pTable;
					}
				}
			}
		}
	}
	return NULL;	
}

int _nSysBtnIndex[20];
int _nSysCount = 20;

void CIOList::MakeTableRdf()
{
	int i,j,k;

	for ( k=0; k<_nSysCount; k++ ) 
	{	
		_nSysBtnIndex[k] = -1;
	}

	int loc=0;
	int nIOMAX = 32 * 48;
	IOCardType *pCard;

	for (i=0; i<MAX_RACK; i++) 
	{
		for (j=0; j<CARD_PER_RACK; j++) 
		{
			pCard = (IOCardType *)&m_Document.m_Rack[ i ].m_IOCard[ j ];
			for (k=0; k<32; k++) 
			{
				MakeTableRdfDo( loc, pCard, k );
				loc++;
				if ( nIOMAX <= loc ) 
					break;
			}
			if ( nIOMAX <= loc ) 
				break;
		}
		if ( nIOMAX <= loc ) 
			break;
	}

	for (j=0; j<3; j++) 
	{
		pCard = (IOCardType *)&m_Document.m_DTS.m_In[ j ];
		for (k=0; k<128; k++) 
		{
			MakeTableRdfDo( loc, pCard, k );
			loc++;
		}
	}
	for (j=0; j<3; j++) 
	{
		pCard = (IOCardType *)&m_Document.m_DTS.m_Out[ j ];
		for (k=0; k<128; k++) 
		{
			MakeTableRdfDo( loc, pCard, k );
			loc++;
		}
	}
}


void CIOList::MakeTableRdfDo( short loc, IOCardType *pCard, int k ) 
{
	CString str = pCard->szNames[k];
	CLinkTable *pTable = NULL;
	if(str != "") 
	{
		int bInverse = str.GetAt(0) == '!';
		if (bInverse) 
		{
			str = (char*)(LPCTSTR)str + 1;
		}

		pTable = FindLinkTable(str,pCard->nType);

		m_pTable = new CLinkTable();
		m_pTable->Name = str;
		m_pTable->Offset =0;
		m_pTable->Type = pCard->nType;
		m_pTable->RdfOffset = loc;

		if(pTable)	
		{
			m_pAssignTable[loc].BitPos = pTable->ScrOffset;
			if( bInverse ) 
				m_pAssignTable[loc].BitPos |= 0x2000;
			
			if (pTable->Type > 2)
				m_pAssignTable[loc].Type = pTable->Type - 2;
			else
				m_pAssignTable[loc].Type = pTable->Type;
			
			m_pTable->ScrOffset = pTable->ScrOffset;					
			if( loc  != pTable->RdfOffset) {
				ErrorLogging( " RDF Offset [%10s] LOC = %4X : pTable->RdfOffset = %4X", str, loc, pTable->RdfOffset );
			}
		}
		else 
		{
			int nBitPos = 0;
			int nType = 0;
			m_pTable->ScrOffset = -1;
			m_pAssignTable[loc].BitPos = nBitPos;
			m_pAssignTable[loc].Type = nType;
		}				
		m_pTableListRdf.SetAtGrow(m_pTableListRdf.GetSize(),(CObject*)m_pTable);					
	}
}





void CIOList::OnMaketable() 
{
}

void CIOList::RdfTableSave()
{
	CString docname = m_strFilePath;	
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "IOM" ); //"TIR" );
	docname.ReleaseBuffer( );

	CString txtdocname = m_strFilePath;
	l = txtdocname.Find('.');
	LPTSTR pf = txtdocname.GetBuffer(l+3);
	strcpy( pf+l+1, "TXT" );
	txtdocname.ReleaseBuffer( );

	CFile tf;
	tf.Open( txtdocname,CFile::modeCreate|CFile::modeWrite);	
	CArchive ar2(&tf,CArchive::store);			
	char buf[100];
	char name[16];	
	//int no ;
	ar2.WriteString(" No   (Table pos)  RK.CD.PT   NAME           T  ScrOffset\r\n");

	IOCardType *pCard;
	int i,j,k;
	int loc=0;
	int nIOMAX = 32 * 48;

	WORD nByteAddr, nBitLoc, nInterBitAddr;
	for (i=0; i<MAX_RACK; i++)		
	{
		for (j=0; j<CARD_PER_RACK; j++) 
		{
			pCard = &m_Document.m_Rack[ i ].m_IOCard[ j ];
			for (k=0; k<32; k++) 
			{								
				strcpy( name, pCard->szNames[k]);
				//no = (i*(CARD_PER_RACK)*32) + (j*32) + k;
				nInterBitAddr = m_pAssignTable[loc].BitPos;

				nBitLoc = loc & 7;
				nByteAddr = loc >> 3;

				char cType = '_';
				if ( m_pAssignTable[loc].Type == 1 ) 
					cType = 'I';
				else if ( m_pAssignTable[loc].Type == 2 ) 
					cType = 'O';
				else if ( m_pAssignTable[loc].Type == 3 ) 
					cType = 'S';

				sprintf(buf,"$%4.4X %4.4X(%4.4X.%d) %02.02d.%02.02d.%02.02d %-15.15s %c:%4.4X(%c%4.4X.%d)\r\n",
					        loc, loc*2, nByteAddr, nBitLoc,
							i+1, j+1, k+1, name, cType, nInterBitAddr,
							nInterBitAddr & 0x2000 ? '!' :' ', ((nInterBitAddr) & 0x1FFF) >> 3, nInterBitAddr & 7
							);
				ar2.WriteString(buf);

				loc++;
				if ( nIOMAX <= loc )
					break;

			}
			if ( nIOMAX <= loc )
				break;

		}
		if ( nIOMAX <= loc )
			break;

	}


	ar2.WriteString( "BLOCK COMMUNICATION\r\n" );
	i = -1;
	for (j=0; j<3; j++) 
	{
		pCard = (IOCardType *)&m_Document.m_DTS.m_In[ j ];
		for (k=0; k<128; k++) 
		{
			strcpy( name, pCard->szNames[k]);
			char cType = '_';
			nInterBitAddr = 0;

			nInterBitAddr = m_pAssignTable[loc].BitPos;
			nBitLoc = loc & 7;
			nByteAddr = loc >> 3;

			if ( m_pAssignTable[loc].Type == 1 ) cType = 'I';
			else if ( m_pAssignTable[loc].Type == 2 ) cType = 'O';
			else if ( m_pAssignTable[loc].Type == 3 ) cType = 'S';

			sprintf(buf,"$%4.4X %4.4X(%4.4X.%d) %02.02d.%02.02d.%02.02d %-15.15s %c:%4.4X(%c%4.4X.%d)\r\n",
					    loc, loc*2, nByteAddr, nBitLoc,
						i+1, j+1, k+1, name, cType, nInterBitAddr,
						nInterBitAddr & 0x2000 ? '!' :' ', ((nInterBitAddr) & 0x1FFF) >> 3, nInterBitAddr & 7
					);
			ar2.WriteString(buf);


			loc++;
		}
	}

	for (j=0; j<3; j++) 
	{
		pCard = (IOCardType *)&m_Document.m_DTS.m_Out[ j ];
		for (k=0; k<128; k++) 
		{
			strcpy( name, pCard->szNames[k]);
			char cType = '_';
			nInterBitAddr = 0;

			nInterBitAddr = m_pAssignTable[loc].BitPos;
			nBitLoc = loc & 7;
			nByteAddr = loc >> 3;
			if ( m_pAssignTable[loc].Type == 1 ) cType = 'I';
			else if ( m_pAssignTable[loc].Type == 2 ) cType = 'O';
			else if ( m_pAssignTable[loc].Type == 3 ) cType = 'S';

			sprintf(buf,"$%4.4X %4.4X(%4.4X.%d) %02.02d.%02.02d.%02.02d %-15.15s %c:%4.4X(%c%4.4X.%d)\r\n",
					    loc, loc*2, nByteAddr, nBitLoc,
						i+1, j+1, k+1, name, cType, nInterBitAddr,
						nInterBitAddr & 0x2000 ? '!' :' ', ((nInterBitAddr) & 0x1FFF) >> 3, nInterBitAddr & 7
					);
			ar2.WriteString(buf);


			loc++;
		}
	}

	ar2.Close();
	tf.Close();
}

void CIOList::AssignError( int eno, CString &str )
{
	CString errstr;
	errstr.Format( "IO Not Define at RDF [ %s ]", str );
	ErrorLogging( "%s", errstr );
}

void ErrorLogging( char *fmt, ... ) {
	char buffer[512];
	va_list arglist;

	va_start( arglist, fmt );     
	vsprintf( buffer, fmt, arglist );
	va_end( arglist );              

	FILE *fp = fopen( "ERR_LOG.TXT", "a+t" );
	if ( fp ) {
		fprintf( fp, "%s\n", buffer );
		fclose( fp );
	}
}

BYTE * CIOList::GetRealAddressPtr()
{
	return m_Document.m_pRealAddress;
}
