#ifndef   _LSMOPT_H
#define   _LSMOPT_H

// Ole Control condition.
#define OPTION_SINGLECLICK		0x01
#define OPTION_NODESTDIALOG		0x02
#define OPTION_NOCONFIRM		0x04
#define OPTION_NOCONTROL		0x08

//2000-12-12 추가 임옥빈
#define OPTION_VIEWINFO			0x10

#define OPTION_SG_SINGLE        0x20    // Single Signal Aspect
#define OPTION_SMALLTEXT        0x40    // for small screen, no TNI
#define OPTION_NODRAW			0x80	// None Draw LSM
// Object Control condition.
#define OPTION_NOBUTTONCTRL		0x01
#define OPTION_NOTRACKCTRL		0x02
#define OPTION_NOSIGNALCTRL		0x04
#define OPTION_NOSWITCHCTRL		0x08
#define OPTION_NOBUTTONVIEW		0x10
#define OPTION_NOTRACK2NAME		0x20
#define OPTION_NOTRAINNO		0x40	// 열차번호 출력안함
#define OPTION_COMFAIL			0x80	// Not User define.

#endif    _LSMOPT_H