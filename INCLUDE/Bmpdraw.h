//===============================================================
//====                     Bmpdraw.h                         ====
//===============================================================
// 파  일  명 : Bmpdraw.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 선택된 대상을 화면에 직접 그려주는 역할을 한다.
//
//===============================================================

#ifndef _BMPDRAW_H
#define _BMPDRAW_H

#include <strstrea.h>

extern CString m_strStationName;
extern short m_iDispBG;

struct SymbolBitmapInfo
{
	int x,y,w,h,cx,cy;
};

class BmpDraw
 {
  public:
	void PutChar( CPoint &point, int nChar, int nColor );
	void DrawInhibit( CPoint &point, int nType );
    void OrDraw(int id); 
	void CHDraw(int id);
	void BGDraw(int iDispBG);

	void DrawLong();
	short  Base, dir;
	CPoint WP;
	static CDC *m_pDC;
	static int m_nRepeat;


  public:
	void DrawButton(int id, DWORD _DrawMode2);
	void BMPOutSh( int dir );
	void RGBDraw(int id);
	void HomeDraw(int id);
	BmpDraw()
	{ Base = 0;
	  m_nRepeat = 0;
	}
	void SetBase(int c) { Base = c; }
	void SetWP(CPoint &p) { WP = p; }
	CPoint &GetWP() { return WP; }	 
	void Draw(int);
};
                                // For TEXT    
#define MY_BLUE		0x000       // 0, BLACK
#define MY_RED		0x040       // 1, RED
#define MY_YELLOW	0x080       // 2, YELLOW
#define MY_GREEN	0x0C0       // 3, GREEN
#define MY_GRAY		0x100       // 4, GRAY
#define MY_FLASHING	0x1000
#define MY_FRED     0x1040
#define MY_FYELLOW  0x1080
#define MY_FGREEN   0x10C0
#define MY_FGRAY    0x1100
#define MY_MOVE_OUT	0x4000		// move out For just flashing


#define BMP_TTB_OFF     30
#define BMP_TTB_ON      31
#define BMP_ROUTE_OFF   40
#define BMP_ROUTE_ON    41


extern BOOL _BlinkDuty;
extern SymbolBitmapInfo *SymbolInfo;
extern SymbolBitmapInfo SymbolInfo8[];
extern SymbolBitmapInfo SymbolInfo10[];

#endif