// JTime.cpp : Implementation
//

#include "stdafx.h"
#include "JTime.h"
#define DAYS_IN_STD_YEAR	365	//353
#define DAYS_IN_LEAP_YEAR	366	//354

int StdYearDays[12]  = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };	// month day 
int LeapYearDays[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };	// month day 

BOOL CJTime::isLeapYear(int year) 
{
	if ( !(year % 4) && ((year % 100) || !(year % 400)) ) 
		return TRUE;
	else 
		return FALSE;
}

long CJTime::GetTime()
{
	long days = GetDays();
	long secs = GetSecInDay();

	days *= MAX_SECOND_FOR_DAY;
	secs += days;
	return secs;
}
long CJTime::GetDays()
{
	int  i;
	long days = 0;
	for(i=0; i<int(nYear); i++) {
		if ( isLeapYear( (i + CJTIME_BASE_YEAR) ) ) 
			days += DAYS_IN_LEAP_YEAR;
		else 
			days += DAYS_IN_STD_YEAR;

	}
	BOOL bLeapYear = isLeapYear( (nYear + CJTIME_BASE_YEAR) );
	int  *pMonth = ( bLeapYear ) ? LeapYearDays : StdYearDays;
	for(i=1; i<int(nMonth); i++) {
		days += pMonth[i-1]; 
	}
	days += long(nDate) - 1;
	return days;
}
long CJTime::GetSecInDay()
{
	long t = long(nSec);
	t += long(nMin) * 60;
	t += long(nHour) * 3600;
	return t;
}
int  CJTime::GetMinuteInDay()
{
	int t = int(nMin);
	t += int(nHour) * 60;
	return t;
}

CString CJTime::Format( const char *lpszFormat )
{
	CString strtime = "";

	if ( !lpszFormat ) return strtime;
	char str[10];
	BOOL bFormat = FALSE;
	BYTE c;
	while( *lpszFormat ) {
		c = *lpszFormat;
		if ( bFormat ) {
			switch (c) {
			case 'd':	// Day of month as decimal number (01 � 31)
				sprintf(str,"%2.2d", int(nDate));
				strtime += str;
				break;
			case 'H':	// Hour in 24-hour format (00 � 23)
				sprintf(str,"%2.2d", int(nHour));
				strtime += str;
				break;
			case 'm':	// Month as decimal number (01 � 12)
				sprintf(str,"%2.2d", int(nMonth));
				strtime += str;
				break;
			case 'M':	// Minute as decimal number (00 � 59)
				sprintf(str,"%2.2d", int(nMin));
				strtime += str;
				break;
			case 'S':	// Second as decimal number (00 � 59)
				sprintf(str,"%2.2d", int(nSec));
				strtime += str;
				break;
			case 'Y':	// Year with century, as decimal number
				sprintf(str,"%4.4d", GetYear());
				strtime += str;
				break;
			default:
				strtime += c;
				break;
			}
			bFormat = FALSE;
		}
		else if (c == '%') bFormat = TRUE;
		else strtime += c;
		lpszFormat++;
	}
	return strtime;
}

CJTime CJTime::GetCurrentTime()
{
	CJTime ct;
	SYSTEMTIME systime;
	GetLocalTime( &systime );
	ct.nSec = BYTE( systime.wSecond );
	ct.nMin = BYTE( systime.wMinute );
	ct.nHour = BYTE( systime.wHour );
	ct.nDate = BYTE( systime.wDay );
	ct.nMonth = BYTE( systime.wMonth );
	ct.nYear = BYTE( systime.wYear - CJTIME_BASE_YEAR );

	return ct;
}

CJTime::CJTime()
{
	nSec  = 0;		// 2000. 1. 1. (00:00:00)
	nMin  = 0;
	nHour = 0;
	nDate = 1;
	nMonth = 1;
	nYear = 0;
}
CJTime::CJTime( long lt )
{
	*this = lt;
}
CJTime::CJTime( const CJTime &objt )
{
	*this = objt;
}
CJTime::CJTime( int year, int month, int day, int hour, int min, int sec )
{
// check Time
	ASSERT( sec < 60 );
	nSec = BYTE( sec );
	ASSERT( min < 60 );
	nMin = BYTE( min );
	ASSERT( hour < 24 );
	nHour = BYTE( hour );
// check Date
	ASSERT( day > 0 );
	ASSERT( day <= 31 );
	nDate = BYTE( day );
	ASSERT( month > 0 );
	ASSERT( month <= 12 );
	nMonth = BYTE( month );
	ASSERT( year >= 2000 );
	ASSERT( year < 2099 );
	nYear = BYTE( year - CJTIME_BASE_YEAR );
}

CJTime& CJTime::operator=(long lt)
{
	int days = lt / MAX_SECOND_FOR_DAY;
	int secs = lt % MAX_SECOND_FOR_DAY;

	nSec = BYTE(secs % 60);
	secs /= 60;
	nMin = BYTE(secs % 60);
	secs /= 60;
	nHour = BYTE(secs);

	int maxdays, v=0;
	BOOL bLeapYear;
	do {
		bLeapYear = isLeapYear( (v + CJTIME_BASE_YEAR) );
		maxdays = ( bLeapYear ) ? DAYS_IN_LEAP_YEAR : DAYS_IN_STD_YEAR;
		if (days >= maxdays) {
			days -= maxdays;
			v++;
		}
		else break;
	} while (days >=  DAYS_IN_STD_YEAR);
	nYear = BYTE( v );
	 bLeapYear = isLeapYear( (v + CJTIME_BASE_YEAR) );
	int  *pMonth = ( bLeapYear ) ? LeapYearDays : StdYearDays;
	for(v=1; v<=12; v++) {
		if (days >= pMonth[v-1]) days -= pMonth[v-1];
		else break;
	}
	nMonth = BYTE( v );
	nDate  = BYTE( ++days );

	return *this;
}

CJTime& CJTime::operator=(const CJTime &objt)
{
	nSec = objt.nSec;
	nMin = objt.nMin;
	nHour = objt.nHour;
//	nDayOfWeek = objt.nDayOfWeek;
	nDate = objt.nDate;
	nMonth = objt.nMonth;
	nYear = objt.nYear;

	return *this;
}

BOOL operator == ( CJTime &obj1, CJTime &obj2 )
{
	if (obj1.nSec == obj2.nSec)
		if (obj1.nMin == obj2.nMin)
			if (obj1.nHour == obj2.nHour)
				if (obj1.nYear == obj2.nYear) 
					if (obj1.nMonth == obj2.nMonth)
						if (obj1.nDate == obj2.nDate)
							return TRUE;
	return FALSE;
}
BOOL operator != ( CJTime &obj1, CJTime &obj2 )
{
	return !(obj1 == obj2);
}
BOOL operator >= ( CJTime &obj1, CJTime &obj2 )
{
	return( ( obj1.GetTime() >= obj2.GetTime() ) ? TRUE : FALSE);
}
BOOL operator <= ( CJTime &obj1, CJTime &obj2 )
{
	return( (obj1.GetTime() <= obj2.GetTime()) ? TRUE : FALSE);
}
BOOL operator > ( CJTime &obj1, CJTime &obj2 )
{
	return !(obj1 <= obj2);
}
BOOL operator < ( CJTime &obj1, CJTime &obj2 )
{
	return !(obj1 >= obj2);
}
