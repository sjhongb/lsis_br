/////////////////////////////////////////////////////////////////////////////
// MessageNo.h : header file

#ifndef   _MESSAGENO_H
#define   _MESSAGENO_H

#include  "JTime.h"
#include  "scrinfo.h"

/////////////////////////////////////////////////////////////////////////////
#define MSG_MODE_ERROR          1 // 장애관련 메세지
#define MSG_MODE_INTERLOCKING   2 // 연동관련 메세지
#define MSG_MODE_OPERATE        4 // 제어관련 메세지
#define MSG_MODE_MESSAGE        8 // 기타(시스템) 메세지
//#define MSG_MODE_ALL         15  //

/////////////////////////////////////////////////////////////////////////////
#define MSG_TYPE_TRACK          1 // 궤도 관련 메세지
#define MSG_TYPE_SIGNAL         2 // 신호기 관련 메세지
#define MSG_TYPE_SWITCH         4 // 선로전환기 관련 메세지
#define MSG_TYPE_SYSTEM         8 // 시스템(기타) 메세지

/////////////////////////////////////////////////////////////////////////////
// Message No List
// BR 관련 새로 작성한 메세지 리스트 

//;
//; 1. System Status Message 
//;
//; Module state
//;
#define MSGNO_SYS_CBI1_RUN                1   // "CBI I system is running"
#define MSGNO_SYS_CBI1_DOWN               2   // "CBI I system is down"
#define MSGNO_SYS_CBI2_RUN                3   // "CBI II system is running"
#define MSGNO_SYS_CBI2_DOWN               4   // "CBI II system is down"
#define MSGNO_SYS_CBI1_STARTUP            5   // "CBI I system is started up"
#define MSGNO_SYS_CBI2_STARTUP            6   // "CBI II system is started up"
#define MSGNO_ERR_SYSTEM_RUN              7   // "CBI system is running"
#define MSGNO_ERR_SYSTEM_DOWN             8   // "CBI system is down"
#define MSGNO_ERR_MODULE_FAULT            9   // "module is abnormal"
#define MSGNO_ERR_MODULE_RECOVERY        10    // "module is recovered"
#define MSGNO_ERR_SYSTEM_FAIL            11    // "System Fail"
#define MSGNO_ERR_SYSTEM_RECOVERY        12    // "System Recovery"
//;
//; CBI state
//;
#define MSGNO_ERR_CBI1_MAIN              15    // "CBI I system is switched to main system"
#define MSGNO_ERR_CBI1_SUB               16    // "CBI I system is switched to sub system"
#define MSGNO_ERR_CBI2_MAIN              17    // "CBI II system is switched to main system"
#define MSGNO_ERR_CBI2_SUB               18    // "CBI II system is switched to sub system"
#define MSGNO_SYS_ACTIVE1_REQ            19    // "CBI1 main Request"
#define MSGNO_SYS_ACTIVE2_REQ            20    // "CBI2 main Request"
#define MSGNO_SYS_CBI_CHANGE_REQ         21    // "Changeover Request"
#define MSGNO_SYS_CBI1_SUB               22    // "CBI1 SUB"
#define MSGNO_SYS_CBI1_MAIN              23    // "CBI1 MAIN"
#define MSGNO_SYS_CBI2_SUB               24    // "CBI2 SUB"
#define MSGNO_SYS_CBI2_MAIN              25    // "CBI2 MAIN"
//;
//; LCC State
//;
#define MSGNO_SYS_LCC1_OFFLINE           26    // "IPC I communication is failed"
#define MSGNO_SYS_LCC1_ONLINE            27    // "IPC I communication is recovered"         
#define MSGNO_SYS_LCC2_OFFLINE           28    // "IPC II communication is failed"         
#define MSGNO_SYS_LCC2_ONLINE            29    // "IPC II communication is recovered"
//;
//; Adjacent Station State
//;
#define MSGNO_SYS_UPMODEM_FAIL           31    // "Adjacent communication to up directional station is failed "        
#define MSGNO_SYS_UPMODEM_RECOVERY       32    // "Adjacent communication to up directional station is recovered "     
#define MSGNO_SYS_DNMODEM_FAIL           33    // "Adjacent communication to down directional station is failed"       
#define MSGNO_SYS_DNMODEM_RECOVERY       34    // "Adjacent communication to down directional station is recovered"      
//;
//; Etc State 
//;
#define MSGNO_SYS_IVOPR1_OFF             36    // "VPOR1 off detected"
#define MSGNO_SYS_IVPOR1_ON              37    // "VPOR1 on detected"
#define MSGNO_SYS_IVPOR2_OFF             38    // "VPOR2 off detected"
#define MSGNO_SYS_IVPOR2_ON              39    // "VPOR2 on detected"
#define MSGNO_SYS_OVOPR1_OFF             40    // "VPOR1 off output"
#define MSGNO_SYS_OVPOR1_ON              41    // "VPOR1 on output"
#define MSGNO_SYS_OVPOR2_OFF             42    // "VPOR2 off output"
#define MSGNO_SYS_OVPOR2_ON              43    // "VPOR2 on output"
#define MSGNO_SYS_FAN1_FAIL              44    // "Fan1 is failed"
#define MSGNO_SYS_FAN1_RECOVERY          45    // "Fan1 is recovered"
#define MSGNO_SYS_FAN2_FAIL              46    // "Fan2 is failed"
#define MSGNO_SYS_FAN2_RECOVERY          47    // "Fan2 is recovered"
#define MSGNO_SYS_FAN3_FAIL              48    // "Fan3 is failed"
#define MSGNO_SYS_FAN3_RECOVERY          49    // "Fan3 is recovered"
#define MSGNO_SYS_FAN4_FAIL              50    // "Fan4 is failed"
#define MSGNO_SYS_FAN4_RECOVERY          51    // "Fan4 is recovered"
#define MSGNO_SYS_FUSE1_FAIL             52    // "Fuse1 is failed"
#define MSGNO_SYS_FUSE1_RECOVERY         53    // "Fuse1 is recovered"
#define MSGNO_SYS_FUSE2_FAIL             54    // "Fuse2 is failed"
#define MSGNO_SYS_FUSE2_RECOVERY         55    // "Fuse2 is recovered"
#define MSGNO_SYS_FUSE3_FAIL             56    // "Fuse3 is failed"
#define MSGNO_SYS_FUSE3_RECOVERY         57    // "Fuse3 is recovered"
#define MSGNO_SYS_FUSE4_FAIL             58    // "Fuse4 is failed"
#define MSGNO_SYS_FUSE4_RECOVERY         59    // "Fuse4 is recovered"
#define MSGNO_SYS_COMM_ALL_FAIL          60    // "Both COM1 and COM2 Fail"
#define MSGNO_SYS_COMM_ALL_RECOVERY      61    // "Both COM1 and COM2 Recovery"
#define MSGNO_SYS_USERLOGIN              62    //  "User Log In"
//;
//; I/O State 
//;
#define MSGNO_ERR_IO_PORT_UNVAL          66    // "input signal is different between CBI 1 system and CBI 2 system"
#define MSGNO_ERR_IOFAULTOUT             67    // "output signal is different between CBI 1 system and CBI 2 system"
//;
//; Status of Powers 
//;
#define MSGNO_SYS_POWER_FAIL             71    // "Main power is failed"
#define MSGNO_SYS_POWER_RECOVERY         72    // "Main power is recovered"
#define MSGNO_SYS_GEN_STOP               73    // "Generator is at standby mode"
#define MSGNO_SYS_GEN_RUN                74    // "Generator is at running mode"
#define MSGNO_SYS_GEN_LOWFUEL            75    // "Generator is warning at Low Fuel state"
#define MSGNO_SYS_GEN_FULLFUEL           76    // "Generator is Full Fuel state"
#define MSGNO_SYS_GEN_FAIL               77    // "Generator is failed"
#define MSGNO_SYS_GEN_RECOVERY           78    // "Generator is recovered"
#define MSGNO_SYS_UPS1_FAIL              79    // "UPS1 is failed"
#define MSGNO_SYS_UPS1_RECOVERY          80    // "UPS1 is recovered"
#define MSGNO_SYS_UPS2_FAIL              81    // "UPS2 is failed"
#define MSGNO_SYS_UPS2_RECOVERY          82    // "UPS2 is recovered"
#define MSGNO_SYS_CHARGE_FAIL            83    // "Battery Charger is failed"
#define MSGNO_SYS_CHARGE_RECOVERY        84    // "Battery Charger is recovered"
#define MSGNO_SYS_B24PWR_FAIL            85    // "Internal B24 power is failed"
#define MSGNO_SYS_B24PWR_RECOVERY        86    // "Internal B24 power is recovered"
#define MSGNO_SYS_EXTERNPOWER_FAIL       87    // "South external power is failed"
#define MSGNO_SYS_EXTERNPOWER_RECOVERY   88    // "South external power is recovered"
#define MSGNO_SYS_SOUTHPOWER_FAIL        89    // "South external power is failed"
#define MSGNO_SYS_SOUTHPOWER_RECOVERY    90    // "South external power is recovered"
#define MSGNO_SYS_NORTHPOWER_FAIL        91    // "North external power is failed"
#define MSGNO_SYS_NORTHPOWER_RECOVERY    92    // "North external power is recovered"
#define MSGNO_SYS_CHARGING_START         93    // "Battery charging...."
#define MSGNO_SYS_CHARGING_END           94    // "Battery charging end"
#define MSGNO_SYS_LOWVOLT                95    // "Battery voltage is low"
#define MSGNO_SYS_LOWVOLT_RECOVERY       96    // "Battery low voltage recovery"
#define MSGNO_SYS_GND_RECOVERY           97    // "Ground is recovered"
#define MSGNO_SYS_GND_FAIL               98    // "Ground is failed"
//;
//; 2.System Function message
//;
//;
//; System Operation 
//;
#define MSGNO_MSG_CTRLSYSRUN           101   // "SYSTEM RUN is operated"
#define MSGNO_MSG_CTRLSYSRESET         102   // "SYSTEM RESET is operated"
#define MSGNO_MSG_CTRLTIME             103   // "System time is set "
//;
//; Day/Night Operation 
//;
#define MSGNO_SYS_OP_DAY               106   // "Day/Night button is operated"
#define MSGNO_SYS_ODAY                 107   // "Signal lamp dimming control is ON"
#define MSGNO_SYS_ONIGHT               108   // "Signal lamp dimming control is OFF"
#define MSGNO_SYS_IDAY                 109   // "Signal lamp dimming changed is ON"
#define MSGNO_SYS_INIGHT               110   // "Signal lamp dimming changed is OFF"
#define MSGNO_SYS_OP_NIGHT             111   // "Day/Night button is operated"
//;
//; PAS Operation
//;  
#define MSGNO_SYS_OP_PAS               116    // "PAS button is operated"
#define MSGNO_SYS_PASLOCK              117    // "LCC operation is locked"
#define MSGNO_SYS_PASUNLOCK            118    // "LCC operation is unlocked"
//;
//; SLS Operation
//;                      
#define MSGNO_SYS_OP_SLS_ON            121   // "SLS button is operated"
#define MSGNO_SYS_SLS_ON               122   // "SLS control is ON"
#define MSGNO_SYS_SLS_OFF              123   // "SLS control is OFF"
#define MSGNO_SYS_OP_SLS_OFF           124   // "SLS button is operated"
//;
//; ATB Operation
//;                      
#define MSGNO_SYS_OP_ATB               126   // "ATB button is operated"
#define MSGNO_SYS_ATB_ON               127   // "ATB control is ON"
#define MSGNO_SYS_ATB_OFF              128   // "ATB control is OFF"
//;
//; Alarm Operation 
//;
#define MSGNO_SYS_OP_ACKBUTTON         131     // "Alarm clear operated"
#define MSGNO_SYS_OP_PDBBUTTON         132     // "Buzzer Clear operated"
#define MSGNO_SYS_ALARM1_OCCUR         133   // "Alarm1 is occurence"
#define MSGNO_SYS_ALARM1_CLEAR         134   // "Alarm1 is clear"
#define MSGNO_SYS_ALARM2_OCCUR         135   // "Alarm2 is occurence"
#define MSGNO_SYS_ALARM2_CLEAR         136   // "Alarm2 is clear"
#define MSGNO_SYS_ALARM3_OCCUR         137   // "Alarm3 is occurence"
#define MSGNO_SYS_ALARM3_CLEAR         138   // "Alarm3 is clear"
#define MSGNO_SYS_ALARM4_OCCUR         139   // "Alarm4 is occurence"
#define MSGNO_SYS_ALARM4_CLEAR         140   // "Alarm4 is clear"
#define MSGNO_SYS_ALARM5_OCCUR         159   // "Alarm5 is occurence"
#define MSGNO_SYS_ALARM5_CLEAR         160   // "Alarm5 is clear"
#define MSGNO_SYS_ALARM6_OCCUR         161   // "Alarm6 is occurence"
#define MSGNO_SYS_ALARM6_CLEAR         162   // "Alarm6 is clear"
#define MSGNO_SYS_ALARM7_OCCUR         163   // "Alarm7 is occurence"
#define MSGNO_SYS_ALARM7_CLEAR         164   // "Alarm7 is clear"
#define MSGNO_SYS_ALARM8_OCCUR         165   // "Alarm8 is occurence"
#define MSGNO_SYS_ALARM8_CLEAR         166   // "Alarm8 is clear"
//;
//; Operation 
//;
#define MSGNO_SIGNAL_OP_ON             145   // "route set is operated"
#define MSGNO_SIGNAL_OP_OFF            146   // "route release is operated"
#define MSGNO_SIGNAL_OP_CANCEL         147   // "signal cancellation is operated"
#define MSGNO_SIGNAL_OP_RECOVERY	   168	 // "signal aspect recovery is operated"
#define MSGNO_SIGNAL_OP_CALLON_ON      148   // "Call-On route set is operated"
#define MSGNO_TRACK_OP_FREESUBROUTE    149   // "Emergency route release"
/* 20130311 ibjang add */
#define MSGNO_RELAYFUSE_FAIL		   150	 // "Relay rack fuse is failed"
#define MSGNO_RELAYFUSE_RECOVERY	   151	 // "Relay rack fuse is recovered"
/* 20130311 ibjang add */
/* 20130314 ibjang add */
#define MSGNO_SYS_ALR_DROP			   152   // "ALR Relay is dropped"
#define MSGNO_SYS_ALR_PICKUP		   153	 // "ALR Relay is picked up"
/* 20130314 ibjang add */
#define MSGNO_SYS_OP_MODE_LOCAL		   154	 // "LOCAL control mode is operated"
#define MSGNO_SYS_OP_MODE_CTC		   155	 // "CTC control mode is operated"
#define MSGNO_SYS_MODE_LOCAL		   156	 // "Switching to LOCAL control mode is completed"
#define MSGNO_SYS_MODE_CTC			   157	 // "Switching to CTC control mode is completed"
#define MSGNO_SYS_OP_MODE_CTC_REQUEST  158	 // "CTC control mode is requested by CTC" 
#define MSGNO_SYS_OP_TRAIN_NUMBER	   167	 // "(Train Number) is sent to CTC"
//;
//; Counter 
//;
#define MSGNO_SYS_COUNTER_CALLON_INC   171	 // "Callon Counter increased to ___ from ___"
#define MSGNO_SYS_COUNTER_CALLON_CHG   172	 // "Callon Counter changed to ___ from ___"
#define MSGNO_SYS_COUNTER_ERR_INC	   173	 // "Emergency Route Release Counter increased to ___ from ___"
#define MSGNO_SYS_COUNTER_ERR_CHG	   174	 // "Emergency Route Release Counter changed to ___ from ___"
#define MSGNO_SYS_COUNTER_AXLRST_INC   175	 // "XX Reset Counter increased to ___ from ___"
#define MSGNO_SYS_COUNTER_AXLRST_CHG   176	 // "XX Reset Counter changed to ___ from ___"
//;
//; 3.Track Message
//;
//;
//; LOS Operation
//;                      
#define MSGNO_SYS_OP_COS_ON            201   // "COS button is operated"
#define MSGNO_SYS_LOOP1_OFF            202   // "loop1 track off"
#define MSGNO_SYS_LOOP1_ON             203   // "loop1 track on"
#define MSGNO_SYS_LOOP2_OFF            204   // "loop2 track off"
#define MSGNO_SYS_LOOP2_ON             205   // "loop2 track on"
#define MSGNO_SYS_OP_POS_ON            206   // "LOS button is operated"
#define MSGNO_SYS_OP_POS_OFF           207   // "LOS button is operated"
#define MSGNO_SYS_OP_COS_OFF           208   // "COS button is operated"
#define MSGNO_SYS_OP_LOS			   209	 // "LOS button is operated"
//;
//; Track Message
//;                      
#define MSGNO_TRACK_ON                 211   // "track is occupied"
#define MSGNO_TRACK_OFF                212   // "track is unoccupied"
#define MSGNO_TRACK_OP_LOCK            213   // "track is recovered"
#define MSGNO_TRACK_OP_FREE            214   // "track is failed"
#define MSGNO_TRACK_COS_SET            215   // "track COS is set to ON"
#define MSGNO_TRACK_COS_OFF            216   // "track COS is set to OFF"
/* 20130312 ibjang add */
#define MSGNO_TRACK_OVERLAP_SET		   217	 // "track is set to overlap"
#define MSGNO_TRACK_OVERLAP_RES		   218	 // "track is released to overlap"
/* 20130312 ibjang add */
/* 20130314 ibjang add */
#define MSGNO_TRACK_ROUTE_EMER_SET	   219   //
#define MSGNO_TRACK_ROUTE_EMER_RES	   220	 //
/* 20130314 ibjang add */
#define MSGNO_TRACK_DISTURB			   221	 // "track is disturbed"
#define MSGNO_TRACK_RECOVERY		   222   // "track is recovered"
#define MSGNO_TRACK_LOS_SET            223   // "track LOS is set to ON"
#define MSGNO_TRACK_LOS_OFF            224   // "track LOS is set to OFF"

//;
//; 4.Signal Message 
//;
#define MSGNO_SYS_SIG_FAIL             301   // "Signal is failed"
#define MSGNO_SYS_SIG_RECOVERY              302   // "Signal is recovered"
#define MSGNO_SIGNAL_ON                     303     // "Signal is on"
#define MSGNO_SIGNAL_OP_OBLK_OFF            304     // "signal STOP is displayed"
#define MSGNO_SIGNAL_APPROACH_SLOCK         305   // "signal approach locking is started"
#define MSGNO_SIGNAL_APPROACH_ELOCK         306   // "signal approach locking is released"
#define MSGNO_SIGNAL_ROUTELOCK_END          307   // "signal approack lock release"
#define MSGNO_SIGNAL_BLOCKADE_FAULT         308   // "Call-On Signal is failed"
#define MSGNO_SIGNAL_BLOCKADE_GOOD          309   // "signal is recovered"
#define MSGNO_SIGNAL_MAIN_FILAMENT_GOOD 310   // "Red signal main filament is recovered"
#define MSGNO_SIGNAL_MAIN_FILAMENT_FAIL 311   // "Red signal main filament is failed"
#define MSGNO_SIGNAL_CALLON_FILAMENT_GOOD 312 // "Callon signal main filament is recovered"
#define MSGNO_SIGNAL_CALLON_FILAMENT_FAIL 313 // "Callon signal main filament is failed"
#define MSGNO_SIGNAL_OSS_ON       315   // "OSS is on"
#define MSGNO_SIGNAL_OSS_OFF      316   // "OSS is off"
#define MSGNO_SIGNAL_IND_ON       317     // "signal LI on"
#define MSGNO_SIGNAL_IND_OFF      318     // "signal LI off"
#define MSGNO_SIGNAL_OUT          319   // "signal value change output"
#define MSGNO_SIGNAL_Y__OUT       320   // "signal GO (Y) output"
#define MSGNO_SIGNAL_G__OUT       321   // "signal GO (G) output"
#define MSGNO_SIGNAL_R__OUT       322   // "signal STOP (R) output"
#define MSGNO_SIGNAL_YY_OUT       323   // "signal GO (YY) output"
#define MSGNO_SIGNAL_GY_OUT       324   // "signal GO (GY) output"
#define MSGNO_SIGNAL_RY_OUT       325   // "signal GO (RY) output"
#define MSGNO_SIGNAL_SY_OUT       326   // "signal GO (SY) output"
#define MSGNO_SIGNAL_CR_OUT       327   // "signal GO (Callon) output"
#define MSGNO_SIGNAL_SR_OUT       328   // "signal GO (Shunt) output"

#define MSGNO_SIGNAL_IN           329   // "signal display value changed"
#define MSGNO_SIGNAL_Y__IN        330   // "signal GO (Y) is displayed"
#define MSGNO_SIGNAL_G__IN        331   // "signal GO (G) is displayed"
#define MSGNO_SIGNAL_R__IN        332   // "signal STOP (R) is displayed"
#define MSGNO_SIGNAL_YY_IN        333   // "signal GO (YY) is displayed"
#define MSGNO_SIGNAL_GY_IN        334   // "signal GO (GY) is displayed"
#define MSGNO_SIGNAL_RY_IN        335   // "signal GO (RY) is displayed"
#define MSGNO_SIGNAL_SY_IN        336   // "signal GO (SY) is displayed"
#define MSGNO_SIGNAL_CR_IN        337   // "signal GO (Callon) is displayed"
#define MSGNO_SIGNAL_SR_IN        338   // "signal GO (Shunt) is displayed"
#define MSGNO_SIGNAL_NOT_DISPLAY  339   // "signal Input of signal apparatus is unconfirmed (LOR)"
#define MSGNO_SIGNAL_G_MF_FAIL    340   // "signal G Aspect main filament fail"
#define MSGNO_SIGNAL_Y_MF_FAIL    341   // "signal Y Aspect main filament fail"
#define MSGNO_SIGNAL_R_MF_FAIL    342   // "signal R Aspect main filament fail"
#define MSGNO_SIGNAL_C_MF_FAIL    343   // "signal C Aspect main filament fail"
#define MSGNO_SIGNAL_G_MF_RECOVERY  344   // "signal G Aspect main filament recovery"
#define MSGNO_SIGNAL_Y_MF_RECOVERY  345   // "signal Y Aspect main filament recovery"
#define MSGNO_SIGNAL_R_MF_RECOVERY  346   // "signal R Aspect main filament recovery"
#define MSGNO_SIGNAL_C_MF_RECOVERY  347   // "signal C Aspect main filament recovery"
#define MSGNO_SIGNAL_G_MSF_FAIL     348   // "signal G Aspect filament fail"
#define MSGNO_SIGNAL_Y_MSF_FAIL     349   // "signal Y Aspect filament fail"
#define MSGNO_SIGNAL_R_MSF_FAIL     350   // "signal R Aspect filament fail"
#define MSGNO_SIGNAL_C_MSF_FAIL     351   // "signal C Aspect filament fail"
#define MSGNO_SIGNAL_S_MSF_FAIL     352   // "signal S Aspect filament fail"
#define MSGNO_SIGNAL_SY_MSF_FAIL    353   // "signal SY Aspect filament fail"
#define MSGNO_SIGNAL_LI_FAIL        354     // "signal Loop Indicator fail"
#define MSGNO_SIGNAL_OP_OBLK_ON     356
#define MSGNO_SIGNAL_NONE_ON_LAMP   357   // 
#define MSGNO_SIGNAL_ASPECT_OFF     358     //
#define MSGNO_SIGNAL_ASPECT_ON      359     //
#define MSGNO_SIGNAL_FAIL           360     // "signal fail"
#define MSGNO_MSG_OSSBUTTON_SET_N   361  // "OSS button is operated set to N"
#define MSGNO_MSG_OSSBUTTON_SET_R   362  // "OSS button is operated set to R"
#define MSGNO_SIGNAL_IND_FAIL       363
#define MSGNO_ALR_STATE_ON			364	 // "signal lamp caused by ALR is changed"
#define MSGNO_ALR_STATE_OFF			365	 // "signal lamp caused by ALR is recovered"
#define MSGNO_SIGNAL_SR_OFF			366	 // "signal GO (Shunt) is turned off"
#define MSGNO_SIGNAL_CR_OFF			367	 // "signal GO (Callon) is turned off"
#define MSGNO_SIGNAL_S_FAIL			368   // "signal Shunt Lamp fail"
#define MSGNO_SIGNAL_S_RECOVERY		369   // "signal Shunt Lamp Recovery"
//;
//; 5.Point Message
//;
//;
//; Operation
//;
#define MSGNO_SWITCH_OPERATE_P      401   // "point is operated to normal position"
#define MSGNO_SWITCH_OPERATE_M      402   // "point is operated to reverse position"
#define MSGNO_SYS_OP_NOUSE          403     // "Nouse Button Operated"
#define MSGNO_HSWITCH_OPERATE_RELEASE 404   // "point is operated to normal position"
#define MSGNO_HSWITCH_OPERATE_LOCK    405   // "point is operated to reverse position"
#define MSGNO_POINT_OPERATE_BLOCK		485	// "point block is operated"
#define MSGNO_POINT_OPERATE_UNBLOCK		486 // "point unblock is operated"
//;
//; State
//;
#define MSGNO_SYS_OP_APKUPBUTTON_R    406     // "Electric Point Key button is operated"
#define MSGNO_NORTH_IPOINT_KEY_RELEASE  407   // "North point key release detect"
#define MSGNO_NORTH_IPOINT_KEY_LOCK   408   // "North point key lock detect"
#define MSGNO_SOUTH_IPOINT_KEY_RELEASE  409   // "South point key release detect"
#define MSGNO_SOUTH_IPOINT_KEY_LOCK   410   // "South point key lock detect"
#define MSGNO_NORTH_OPOINT_KEY_RELEASE  411   // "North point key release control"
#define MSGNO_NORTH_OPOINT_KEY_LOCK   412   // "North point key lock control"
#define MSGNO_SOUTH_OPOINT_KEY_RELEASE  413   // "South point key release control"
#define MSGNO_SOUTH_OPOINT_KEY_LOCK   414   // "South point key lock control"
#define MSGNO_SWITCH_CHANGE_P     415   // "point is switched to normal position"
#define MSGNO_SWITCH_CHANGE_M     416   // "point is switched to reverse position"
#define MSGNO_SWITCH_OUT_P        417   // "point is normal output"
#define MSGNO_SWITCH_OUT_M        417   // "point is reverse output"
#define MSGNO_SWITCH_DATA_ON      419   // "ON"
#define MSGNO_SWITCH_DATA_OFF     420   // "OFF"
#define MSGNO_SWITCH_UNVAL_P      421   // "discord normal position"
#define MSGNO_SWITCH_UNVAL_M      422   // "discord reverse position"
#define MSGNO_SWITCH_COVER_UNVAL_P    423   // "discord normal position is recovered"
#define MSGNO_SWITCH_COVER_UNVAL_M    424   // "discord reverse position is recovered"
#define MSGNO_SWITCH_FAULT_P      425   // "point normal position is impossible"  
#define MSGNO_SWITCH_FAULT_M      426   // "point reverse position is impossible" 
#define MSGNO_POINT_KEY_RELEASE     427   // "point key is release"
#define MSGNO_POINT_KEY_LOCK      428   // "point key is locked"    
#define MSGNO_HPOINT_KEY_RELEASE    429   // "hand Point key transmitter is operated"
#define MSGNO_HPOINT_KEY_LOCK     430   // "hand Point key transmitter is operated"
#define MSGNO_A_IPOINT_KEY_RELEASE    432   // "A electric Point key transmitter is released."
#define MSGNO_A_IPOINT_KEY_LOCK     433   // "A electric point key transmitter is locked."
#define MSGNO_B_IPOINT_KEY_RELEASE    434   // "B electric Point key transmitter is released."
#define MSGNO_B_IPOINT_KEY_LOCK     435   // "B electric point key transmitter is locked."
#define MSGNO_C_IPOINT_KEY_RELEASE    436   // "C electric Point key transmitter is released."
#define MSGNO_C_IPOINT_KEY_LOCK     437   // "C electric point key transmitter is locked."
#define MSGNO_D_IPOINT_KEY_RELEASE    438   // "D electric Point key transmitter is released."
#define MSGNO_D_IPOINT_KEY_LOCK     439   // "D electric point key transmitter is locked."
#define MSGNO_E_IPOINT_KEY_RELEASE    440   // "E electric Point key transmitter is released."
#define MSGNO_E_IPOINT_KEY_LOCK     441   // "E electric point key transmitter is locked."
#define MSGNO_F_IPOINT_KEY_RELEASE    442   // "F electric Point key transmitter is released."
#define MSGNO_F_IPOINT_KEY_LOCK     443   // "F electric point key transmitter is locked."
#define MSGNO_G_IPOINT_KEY_RELEASE    444   // "G electric Point key transmitter is released."
#define MSGNO_G_IPOINT_KEY_LOCK     445   // "G electric point key transmitter is locked."
#define MSGNO_H_IPOINT_KEY_RELEASE    446   // "H electric Point key transmitter is released."
#define MSGNO_H_IPOINT_KEY_LOCK     447   // "H electric point key transmitter is locked."
#define MSGNO_A_OPOINT_KEY_RELEASE    448   // "A electric point key  transmitter is operated to be released."
#define MSGNO_A_OPOINT_KEY_LOCK     449   // "A electric point key transmitter  is operated to be locked."
#define MSGNO_B_OPOINT_KEY_RELEASE    450   // "B electric point key  transmitter is operated to be released."
#define MSGNO_B_OPOINT_KEY_LOCK     451   // "B electric point key transmitter  is operated to be locked."
#define MSGNO_C_OPOINT_KEY_RELEASE    452   // "C electric point key  transmitter is operated to be released."
#define MSGNO_C_OPOINT_KEY_LOCK     453   // "C electric point key transmitter  is operated to be locked."
#define MSGNO_D_OPOINT_KEY_RELEASE    454   // "D electric point key  transmitter is operated to be released."
#define MSGNO_D_OPOINT_KEY_LOCK     455   // "D electric point key transmitter  is operated to be locked."
#define MSGNO_E_OPOINT_KEY_RELEASE    456   // "E electric point key  transmitter is operated to be released."
#define MSGNO_E_OPOINT_KEY_LOCK     457   // "E electric point key transmitter  is operated to be locked."
#define MSGNO_F_OPOINT_KEY_RELEASE    458   // "F electric point key  transmitter is operated to be released."
#define MSGNO_F_OPOINT_KEY_LOCK     459   // "F electric point key transmitter  is operated to be locked."
#define MSGNO_G_OPOINT_KEY_RELEASE    460   // "G electric point key  transmitter is operated to be released."
#define MSGNO_G_OPOINT_KEY_LOCK     461   // "G electric point key transmitter  is operated to be locked."
#define MSGNO_H_OPOINT_KEY_RELEASE    462   // "H electric point key  transmitter is operated to be released."
#define MSGNO_H_OPOINT_KEY_LOCK     463   // "H electric point key transmitter  is operated to be locked."
#define MSGNO_SYS_POINT_FAIL      464   // "Point is failed"
#define MSGNO_SYS_POINT_RECOVERY    465   // "Point is recovered"
#define MSGNO_POINT_NOUSE_SET			466		// "H electric point key transmitter  is operated to be locked."
#define MSGNO_POINT_NOUSE_RELEASE		467		// "H electric point key transmitter  is operated to be locked."
#define MSGNO_SYS_OP_APKDNBUTTON_R        468
#define MSGNO_SYS_OP_APKUPBUTTON_L    469     // "Electric Point Key button is operated"
#define MSGNO_SYS_OP_APKDNBUTTON_L    470     // "Electric Point Key button is operated"
/*20130313 ibjang add 알람 추가 */
#define MSGNO_SYS_OP_APK_A_BUTTON_ON	471
#define MSGNO_SYS_OP_APK_A_BUTTON_OFF	472
#define MSGNO_SYS_OP_APK_B_BUTTON_ON	473
#define MSGNO_SYS_OP_APK_B_BUTTON_OFF	474
#define MSGNO_SYS_OP_APK_C_BUTTON_ON	475
#define MSGNO_SYS_OP_APK_C_BUTTON_OFF	476
#define MSGNO_SYS_OP_APK_D_BUTTON_ON	477
#define MSGNO_SYS_OP_APK_D_BUTTON_OFF	478
#define MSGNO_SYS_OP_APK_E_BUTTON_ON	479
#define MSGNO_SYS_OP_APK_E_BUTTON_OFF	480
#define MSGNO_SYS_OP_APK_F_BUTTON_ON	481
#define MSGNO_SYS_OP_APK_F_BUTTON_OFF	482
#define MSGNO_SYS_OP_APK_G_BUTTON_ON	483
#define MSGNO_SYS_OP_APK_G_BUTTON_OFF	484
/*20130313 ibjang add 알람 추가 */
#define MSGNO_POINT_BLOCK				487 // "point is blocked"
#define MSGNO_POINT_UNBLOCK				488 // "point is unblocked"
#define MSGNO_I_IPOINT_KEY_RELEASE		489   // "I electric Point key transmitter is released."
#define MSGNO_I_IPOINT_KEY_LOCK			490   // "I electric point key transmitter is locked."
#define MSGNO_I_OPOINT_KEY_RELEASE		491   // "I electric point key  transmitter is operated to be released."
#define MSGNO_I_OPOINT_KEY_LOCK			492   // "I electric point key transmitter  is operated to be locked."
#define MSGNO_SYS_OP_APK_H_BUTTON_ON	493
#define MSGNO_SYS_OP_APK_H_BUTTON_OFF	494
#define MSGNO_SYS_OP_APK_I_BUTTON_ON	495
#define MSGNO_SYS_OP_APK_I_BUTTON_OFF	496
//;
//; 6.Block Message
//;
#define MSGNO_BLOCK_CA_ON        501   // "CA button is ON"
#define MSGNO_BLOCK_CA_OFF        502   // "CA button is OFF"
#define MSGNO_BLOCK_LCR_ON        503   // "LCR button is ON"
#define MSGNO_BLOCK_LCR_OFF       504   // "LCR button is OFF"
#define MSGNO_BLOCK_LCG_ON        505   // "LCG button is ON"
#define MSGNO_BLOCK_LCG_OFF       506   // "LCG button is OFF"

#define MSGNO_MSG_CAACK                 507     // "CA Ack is operated"
#define MSGNO_MSG_CAEMER                508     // "CBB Emergency operated"
#define MSGNO_MSG_UTG_CA                509     // "Up direction TGB CA operated"
#define MSGNO_MSG_UTG_LCR               510     // "Up direction TGB LCR operated"
#define MSGNO_MSG_UTG_CBB               511     // "Up direction TGB BCB operated"
#define MSGNO_MSG_UTC_LCRREQ			512     // "Up direction TCB LCG requested"
#define MSGNO_MSG_UTC_LCR               513     // "Up direction TCB LCG operated"
#define MSGNO_MSG_UTC_CBB               514     // "Up direction TCB BRB operated"
#define MSGNO_MSG_DTG_CA                515     // "Down direction TGB CA operated"
#define MSGNO_MSG_DTG_LCR               516     // "Down direction TGB LCR operated"
#define MSGNO_MSG_DTG_CBB               517     // "Down direction TGB BCB operated"
#define MSGNO_MSG_DTC_LCRREQ            518     // "Down direction TCB LCG requested"
#define MSGNO_MSG_DTC_LCR               519     // "Down direction TCB LCG operated"
#define MSGNO_MSG_DTC_CBB               520     // "Down direction TCB BRB operated"


// #define MSGNO_BLOCK_LCRIN_ON            521   // 
// #define MSGNO_BLOCK_LCRIN_OFF     522   // 
// #define MSGNO_BLOCK_LCROP_ON      523   // 
// #define MSGNO_BLOCK_LCROP_OFF     524   // 
// #define MSGNO_BLOCK_CBBOP_ON      525   // 
// #define MSGNO_BLOCK_CBBOP_OFF     526   // 
// #define MSGNO_BLOCK_COMPLETE_ON     527   // 
// #define MSGNO_BLOCK_COMPLETE_OFF    528   //
/* 20130312 ibjang add */
#define MSGNO_BLOCK_BCB_ON				521		// "BCB button is ON"
#define MSGNO_BLOCK_BCB_OFF				522		// "BCB button is OFF"
#define MSGNO_BLOCK_BRB_ON				523		// "BRB button is ON"
#define MSGNO_BLOCK_BRB_OFF				524		// "BRB button is OFF"

#define MSGNO_BLOCK_UP_BGR_RST_REQ		531		// "Up direction BGR clear requested"
#define MSGNO_BLOCK_UP_BCR_RST_REQ		532		// "Up direction BCR clear requested"
#define MSGNO_BLOCK_DN_BGR_RST_REQ		533		// "Down direction BGR clear requested"
#define MSGNO_BLOCK_DN_BCR_RST_REQ		534		// "Down direction BCR clear requested"
#define MSGNO_BLOCK_UP2_BGR_RST_REQ		535		// "Up2 direction BGR clear requested"
#define MSGNO_BLOCK_UP2_BCR_RST_REQ		536		// "Up2 direction BCR clear requested"
#define MSGNO_BLOCK_DN2_BGR_RST_REQ		537		// "Down2 direction BGR clear requested"
#define MSGNO_BLOCK_DN2_BCR_RST_REQ		538		// "Down2 direction BCR clear requested"

#define MSGNO_BLOCK_UP_BGR_OCC			541		// "Up direction BGR Occupied"
#define MSGNO_BLOCK_UP_BGR_CLR			542		// "Up direction BGR Cleared"
#define MSGNO_BLOCK_UP_BCR_OCC			543		// "Up direction BCR Occupied"
#define MSGNO_BLOCK_UP_BCR_CLR			544		// "Up direction BCR Cleared"
#define MSGNO_BLOCK_DN_BGR_OCC			545		// "Down direction BGR Occupied"
#define MSGNO_BLOCK_DN_BGR_CLR			546		// "Down direction BGR Cleared"
#define MSGNO_BLOCK_DN_BCR_OCC			547		// "Down direction BCR Occupied"
#define MSGNO_BLOCK_DN_BCR_CLR			548		// "Down direction BCR Cleared"
#define MSGNO_BLOCK_UP2_BGR_OCC			549		// "Up2 direction BGR Occupied"
#define MSGNO_BLOCK_UP2_BGR_CLR			550		// "Up2 direction BGR Cleared"
#define MSGNO_BLOCK_UP2_BCR_OCC			551		// "Up2 direction BCR Occupied"
#define MSGNO_BLOCK_UP2_BCR_CLR			552		// "Up2 direction BCR Cleared"
#define MSGNO_BLOCK_DN2_BGR_OCC			553		// "Down2 direction BGR Occupied"
#define MSGNO_BLOCK_DN2_BGR_CLR			554		// "Down2 direction BGR Cleared"
#define MSGNO_BLOCK_DN2_BCR_OCC			555		// "Down2 direction BCR Occupied"
#define MSGNO_BLOCK_DN2_BCR_CLR			556		// "Down2 direction BCR Cleared"

#define MSGNO_MSG_UTG2_CBB				561		// "Up2 direction TGB BCB operated"				
#define MSGNO_MSG_UTC2_CBB				562		// "Up2 direction TCB BRB operated"				
#define MSGNO_MSG_DTG2_CBB				563		// "Up2 direction TGB BCB operated"				
#define MSGNO_MSG_DTC2_CBB				564		// "Up2 direction TCB BRB operated"				

#define MSGNO_BLOCK_UP_BGR_RST_ACC		571		// "Up direction BGR clear Accepted"
#define MSGNO_BLOCK_UP_BCR_RST_ACC		572		// "Up direction BCR clear Accepted"
#define MSGNO_BLOCK_DN_BGR_RST_ACC		573		// "Down direction BGR clear Accepted"
#define MSGNO_BLOCK_DN_BCR_RST_ACC		574		// "Down direction BCR clear Accepted"
#define MSGNO_BLOCK_UP2_BGR_RST_ACC		575		// "Up2 direction BGR clear Accepted"
#define MSGNO_BLOCK_UP2_BCR_RST_ACC		576		// "Up2 direction BCR clear Accepted"
#define MSGNO_BLOCK_DN2_BGR_RST_ACC		577		// "Down2 direction BGR clear Accepted"
#define MSGNO_BLOCK_DN2_BCR_RST_ACC		578		// "Down2 direction BCR clear Accepted"

#define MSGNO_BLOCK_UP_BGR_RST_DEC		581		// "Up direction BGR clear Declined"
#define MSGNO_BLOCK_UP_BCR_RST_DEC		582		// "Up direction BCR clear Declined"
#define MSGNO_BLOCK_DN_BGR_RST_DEC		583		// "Down direction BGR clear Declined"
#define MSGNO_BLOCK_DN_BCR_RST_DEC		584		// "Down direction BCR clear Declined"
#define MSGNO_BLOCK_UP2_BGR_RST_DEC		585		// "Up2 direction BGR clear Declined"
#define MSGNO_BLOCK_UP2_BCR_RST_DEC		586		// "Up2 direction BCR clear Declined"
#define MSGNO_BLOCK_DN2_BGR_RST_DEC		587		// "Down2 direction BGR clear Declined"
#define MSGNO_BLOCK_DN2_BCR_RST_DEC		588		// "Down2 direction BCR clear Declined"


#define MSGNO_MSG_UTG2_CA               589     // "Up2 direction TGB CA operated"
#define MSGNO_MSG_UTG2_LCR              590     // "Up2 direction TGB LCR operated"
#define MSGNO_MSG_UTC2_LCRREQ			591     // "Up2 direction TCB LCR requested"
#define MSGNO_MSG_UTC2_LCR              592     // "Up2 direction TCB LCR operated"
#define MSGNO_MSG_DTG2_CA               593     // "Down2 direction TGB CA operated"
#define MSGNO_MSG_DTG2_LCR              594     // "Down2 direction TGB LCG operated"
#define MSGNO_MSG_DTC2_LCRREQ           595     // "Down2 direction TCB LCG requested"
#define MSGNO_MSG_DTC2_LCR              596     // "Down2 direction TCB LCG operated"

#define MSGNO_BLOCK_UP_BGR_SWEEPING_S	701		// "Up direction BGR train sweeping started"
#define MSGNO_BLOCK_UP_BCR_SWEEPING_S	702		// "Up direction BCR train sweeping started"
#define MSGNO_BLOCK_DN_BGR_SWEEPING_S	703		// "Down direction BGR train sweeping started"
#define MSGNO_BLOCK_DN_BCR_SWEEPING_S	704		// "Down direction BCR train sweeping started"
#define MSGNO_BLOCK_UP2_BGR_SWEEPING_S	705		// "Up2 direction BGR train sweeping started"
#define MSGNO_BLOCK_UP2_BCR_SWEEPING_S	706		// "Up2 direction BCR train sweeping started"
#define MSGNO_BLOCK_DN2_BGR_SWEEPING_S	707		// "Down2 direction BGR train sweeping started"
#define MSGNO_BLOCK_DN2_BCR_SWEEPING_S	708		// "Down2 direction BCR train sweeping started"

#define MSGNO_BLOCK_UP_BGR_SWEEPING_E	711		// "Up direction BGR train sweeping completed"
#define MSGNO_BLOCK_UP_BCR_SWEEPING_E	712		// "Up direction BCR train sweeping completed"
#define MSGNO_BLOCK_DN_BGR_SWEEPING_E	713		// "Down direction BGR train sweeping completed"
#define MSGNO_BLOCK_DN_BCR_SWEEPING_E	714		// "Down direction BCR train sweeping completed"
#define MSGNO_BLOCK_UP2_BGR_SWEEPING_E	715		// "Up2 direction BGR train sweeping completed"
#define MSGNO_BLOCK_UP2_BCR_SWEEPING_E	716		// "Up2 direction BCR train sweeping completed"
#define MSGNO_BLOCK_DN2_BGR_SWEEPING_E	717		// "Down2 direction BGR train sweeping completed"
#define MSGNO_BLOCK_DN2_BCR_SWEEPING_E	718		// "Down2 direction BCR train sweeping completed"

#define MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP		719		// "Up direction BGR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP		720		// "Up direction BCR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP		721		// "Down direction BGR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP		722		// "Down direction BCR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP		723		// "Up2 direction BGR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP		724		// "Up2 direction BCR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP		725		// "Down2 direction BGR clear Accepted / Waiting for train sweeping"
#define MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP		726		// "Down2 direction BCR clear Accepted / Waiting for train sweeping"

/* 20130312 ibjang add */
//;
//; 7.LevelCrossing Message
//;
#define MSGNO_LC_OPEN          601     // "level crossing is open"
#define MSGNO_LC_CLOSE         602     // "level crossing is closed"
#define MSGNO_LC_KEY_LOCK        603     // "level crossing key transmitter locked"
#define MSGNO_LC_KEY_RELEASE     604     // "level crossing key transmitter released"
#define MSGNO_LC_BELL_CTRL       605     // "level crossing bell out"
#define MSGNO_LC_BELL_ACK        606     // "level crossing bell acknowledge"
#define MSGNO_LC2_OPEN          607     // "level crossing is open"
#define MSGNO_LC2_CLOSE         608     // "level crossing is closed"
#define MSGNO_LC2_KEY_LOCK        609     // "level crossing key transmitter locked"
#define MSGNO_LC2_KEY_RELEASE     610     // "level crossing key transmitter released"
#define MSGNO_LC2_BELL_CTRL       611     // "level crossing bell out"
#define MSGNO_LC2_BELL_ACK        612     // "level crossing bell acknowledge"
/* 20130312 ibjang add */
#define MSGNO_LC3_OPEN			  613		// "level crossing is open"
#define MSGNO_LC3_CLOSE			  614		// "level crossing is closed"
#define MSGNO_LC3_KEY_LOCK		  615		// "level crossing key transmitter locked"
#define MSGNO_LC3_KEY_RELEASE	  616		// "level crossing key transmitter released"
#define MSGNO_LC3_BELL_CTRL		  617		// "level crossing bell out"
#define MSGNO_LC3_BELL_ACK		  618		// "level crossing bell acknowledge"
#define MSGNO_LC4_OPEN			  619		// "level crossing is open"
#define MSGNO_LC4_CLOSE			  620		// "level crossing is closed"
#define MSGNO_LC4_KEY_LOCK		  621		// "level crossing key transmitter locked"
#define MSGNO_LC4_KEY_RELEASE	  622		// "level crossing key transmitter released"
#define MSGNO_LC4_BELL_CTRL		  623		// "level crossing bell out"
#define MSGNO_LC4_BELL_ACK		  624		// "level crossing bell acknowledge"

//;
//; 8. AXLE Counter Message
//; 2013.02.28 ibjang add, RES=Release
#define MSGNO_AX_RCV_OCCUPYED_SET		800		// "Axle Counter (AX#) is occupied"			
#define MSGNO_AX_RCV_OCCUPYED_RES		801		// "Axle Counter (AX#) is unoccupied"
#define MSGNO_AX_RCV_DISTURBED_SET		802		// "Axle Counter (AX#) is disturbed"
#define MSGNO_AX_RCV_DISTURBED_RES		803		// "Axle Counter (AX#) is normalization"
#define MSGNO_AX_OP_RESET_REQUEST		811		// "Axle Counter reset request is operated"
#define MSGNO_AX_RESET_REQUESTED		812		// "Axle Counter reset is requested"
#define MSGNO_AX_RESET_ACCEPT			813		// "Axle Counter reset is accepted"
#define MSGNO_AX_RESET_DECLINE			814		// "Axle Counter reset is declined"
#define MSGNO_AX_RESET_OUT				815		// "Axle Counter reset"
#define MSGNO_AX_SAXLACT_ACTIVATE		821		// "Super-Block is Activated"
#define MSGNO_AX_SAXLACT_DEACTIVATE		822		// "Super-Block is Deactivated"
//; 2013.02.28 ibjang add, RES=Release
//;
//; ETC 
//;
#define MSGNO_SYS_STARTUP_OFF     951   // "StartUp Bit Set 0"
#define MSGNO_SYS_STARTUP_ON      952   // "StartUp Bit Set 1"
#define MSGNO_SYS_ALR_OFF       953   // "ALR Bit Set 0"
#define MSGNO_SYS_ALR_ON        954   // "ALR Bit Set 1"
#define MSGNO_SYS_TCS_OFF       955   // "TCS Bit Set 0"
#define MSGNO_SYS_TCS_ON        956   // "TCS Bit Set 1"
#define MSGNO_NOTHING           957   // "메세지 처리안함"


// BR 관련 새로 작성한 메세지 리스트 
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Message No List
#define MSGNO_SYS_OP_COMMBUTTON		901
#define MSGNO_MSG_CTRLSYS1			902
#define MSGNO_MSG_CTRLSYS2			903
#define MSGNO_MSG_POWERBUTTON		904
#define MSGNO_MSG_GENBUTTON			905
#define MSGNO_MSG_OP_UPS2BUTTON     906
#define MSGNO_MSG_GRDBUTTON			907
#define MSGNO_SYS_OP_UPS1BUTTON     908
#define MSGNO_SYS_OP_BATTERYBUTTON  909
#define MSGNO_SYS_OP_GNDBUTTON      910
#define MSGNO_SYS_OP_CLOSEBUTTON    911
#define MSGNO_SYS_OP_REVERSEBUTTON  912
#define MSGNO_SYS_OP_LOOP1BUTTON    913
#define MSGNO_SYS_OP_LOOP2BUTTON    914
#define MSGNO_ERR_CARDFAIL			916
#define MSGNO_ERR_CARDRECOVERY      917
#define MSGNO_ERR_SWITCH_FAULTOUT   918
#define MSGNO_ERR_SIG_S3_FAULTOUT   919
#define MSGNO_ERR_SIG_S2_FAULTOUT   920
#define MSGNO_ERR_SIG_S23_FAULTOUT  921
#define MSGNO_ERR_SIG_S1_FAULTOUT   922
#define MSGNO_ERR_SIG_S13_FAULTOUT  923
#define MSGNO_ERR_SIG_S12_FAULTOUT  924
#define MSGNO_ERR_SIG_S123_FAULTOUT 925
#define MSGNO_SYS_CHANGE			926
#define MSGNO_SYS_CHANGE_MAIN		927
#define MSGNO_SYS_CHANGE_SUB		928
#define MSGNO_SYS_RUN_STOP			929
#define MSGNO_SYS_RUN_GOOD			930
#define MSGNO_SYS_PWR_CHANGE		931
#define MSGNO_SYS_CHARGE_ON			932
#define MSGNO_SYS_CHARGE_END		933
#define MSGNO_SYS_FUSE_FAULT		934
#define MSGNO_SYS_FUSE_GOOD			935
#define MSGNO_SYS_OP_SBUTTON		936
#define MSGNO_SYS_OP_PBUTTON		937
#define MSGNO_SYS_OP_FUSEBUTTON     938
#define MSGNO_SYS_OP_BLSBUTTON      939
#define MSGNO_SYS_COMM_CHANGE		940
#define MSGNO_SYS_COMM_OPTFAIL      941
#define MSGNO_SYS_COMM_OPTRECOVER   942
#define MSGNO_SYS_ACR_FAIL			943
#define MSGNO_SYS_ACR_GOOD			944
#define MSGNO_TRACK_ON_OVERLAP      945
#define MSGNO_TRACK_OFF_OVERLAP     946
#define MSGNO_TRNO_ARRIVAL			947
#define MSGNO_TRNO_DEPARTURE		948
#define MSGNO_MSG_START_OPU			949
#define MSGNO_MSG_END_OPU			950
/////////////////////////////////////////////////////////////////////////////

// Bit :  3   2   1   0 
// (OUT)  U  YY   Y   G 
//   R        x   0   0 
//   YY       0   1   0 
//   Y        1   1   0 
//   YG       x   0   1 
//   G        x   1   1 
//   U    1   x   x   x 
// 출력 :  0/정지, 1/현시
// 입력 : 출력의 역상태 
// Output
#define SIG_BIT_R   0 // R 현시, 신호기 정지
#define SIG_BIT_R_YY  4 // R 현시, 신호기 정지
#define SIG_BIT_YY    2 // YY 현시
#define SIG_BIT_Y   6 // Y 현시
#define SIG_BIT_YG    1 // YG 현시
#define SIG_BIT_YG_YY 5 // YG 현시
#define SIG_BIT_G   3 // G 현시
#define SIG_BIT_G_YY  7 // G 현시
// 신호기 마스크 
#define SIG_MASK_2S   2
#define SIG_MASK_34S  3
#define SIG_MASK_5S   7
#define SIG_MASK_YUDO 8

typedef enum SignalLampType { 
  SignalLampFail = -1,
  SignalLampR = 0, 
  SignalLampYY, 
  SignalLampY, 
  SignalLampYG, 
  SignalLampG,
  SignalLampU,
  SignalLampNone,
  SignalLampSU,
  SignalLampNull
};

// Bit :    3    2    1    0 
// (LMR)  LM4  LM3  LM2  LM1 
//   R      1    1    0    0 
//   YY     1    1    1    1 
//   Y      0    0    1    1 
//   YG     1    1    1    1 
//   G      1    1    0    0 
//   U      0    0    0    0 
// 입력 :  1/현시, 0/정지 
// Input

/////////////////////////////////////////////////////////////////////////////
typedef union {
  WORD nBitLoc;
  struct {
    WORD nPort : 5;
    WORD nCard : 3;
    WORD nRack : 3;
    WORD nType : 3;
    WORD nState: 1;
    WORD nMain : 1;
  };
} BitLocType;

typedef union {
  WORD nBitLoc;
  struct {
    WORD nPort : 5;
    WORD nCard : 6;
    WORD nType : 3;
    WORD nState: 1;
    WORD nMain : 1;
  };
} BitLocTypeEx;

typedef union {
  unsigned short nEvnLoc;
  struct {
    unsigned short nRec: 4;       // Order In  Sec Record
    unsigned short nLoc: 12;      // Order for Event 
  };
} EvnLocType;

typedef union {
  BYTE MsgType;
  struct {
    BYTE nMode : 4;               // 메시지 상황 분류 형태
    BYTE nType : 4;               // 메시지 장치 분류 형태
  };
  struct {
    BYTE bModeError   : 1;    // error message
    BYTE bModeILocking  : 1;    // interlocking message
    BYTE bModeOperate : 1;    // control message
    BYTE bModeMessage : 1;    // etc(system) message
    BYTE bTypeTrack   : 1;    // track message
    BYTE bTypeSignal  : 1;    // signal message
    BYTE bTypeSwitch  : 1;    // point message
    BYTE bTypeSystem  : 1;    // system(etc) message
  };
} MsgModeType; 

typedef union {
  BYTE MsgForm;
  struct {
    BYTE nType : 4;         // ID type
    BYTE nForm : 4;         // Format type
  };
} MsgFormatType;

#define MSG_MAX_NAME_LENTH  40

typedef struct tagMessageEvent {
  short msgno;            // common item(track,signal,switch,operate,system,I/O).
  MsgModeType   Type;       // common item(track,signal,switch,operate,system,I/O). 
  MsgFormatType Form;       // common item(track,signal,switch,operate,system,I/O).
  CJTime time;            // second, minutes, hour, day, month, year
  BYTE  hsec;             // ms
  char  name[ MSG_MAX_NAME_LENTH ]; // common item(track,signal,switch,operate,system,I/O).
  char	EndOfMsg[ MSG_MAX_NAME_LENTH ];
  BYTE  Mode;             // Bit D7: 1/CTC, 0/LOCAL.
  EvnLocType evn;           // common items(All).
  union {
    BitLocType    loc;      // used I/O port
    BitLocTypeEx  exloc;      // used I/O port
    short item;         // used system status
    struct {
      BYTE  op;         // used track
      union {
        BYTE  real;     // siganl, switch
        BYTE  cause;      // signal
      };
    };
  };

public:
  tagMessageEvent()
  {
    msgno = -1;
    evn.nRec = 0;
    evn.nLoc = 0;
    Mode = 0x00;
    name[0] = 0x00;
	EndOfMsg[0] = '\0';
  }
  BOOL IsAttach( MsgModeType &filter ) 
  {
    if ( Type.nMode & filter.nMode ) {
      if ( Type.nType & filter.nType ) 
        return TRUE;
    }
    return FALSE;
  }
} MessageEvent;

typedef struct tagMsgTimeStrings {
  CString strTime;
  CString strType;
  CString strMsgNo;
  CString strMessage;
  CString strHelp;
} MsgTimeStrings;

typedef struct tagMsgStateStrings {
  int nType;
  CString strName;
  CString strState;
  CString strTime;
} MsgStateStrings;

#endif // _MESSAGENO_H
