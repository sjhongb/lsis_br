/////////////////////////////////////////////////////////////////////////////
// MessageInfo.cpp : implementation file

#include "stdafx.h"
#include "FMSGList.h"
#include "MessageInfo.h"
#include "../LES/LES.h"

CEipEmu *_pEipEmu = NULL;
int  Info_nMaxTrack     = 0;
int  Info_nMaxSignal    = 0;
int  Info_nMaxSwitch    = 0;
int  Info_nMaxButton    = 0;
int  Info_nMaxLamp      = 0;
int  Info_nSizeOfRecord = 0;
extern class CFMSGList *_pEIPMsgList;

CMessageInfo::CMessageInfo()
{

}

#ifdef    _DEBUG_DUMP

void ConvertLtoS(BYTE *p, long d)
{
	int  i, n;
	long mask = 1;

	for(n=i=0; i<32; i++,n++) {
		if (i && !(i % 8)) {
			p[n++] = ' ';
		}
		p[n] = (d & mask) ? '1' : '0';
		mask <<= 1;
	}
	p[n] = 0x00;
}

extern BYTE _SystemVMEM[];

void CMessageInfo::Dump(CDumpContext& dc)
{
	CString Name;
	BYTE *pBuf = &_SystemVMEM[ m_nMemOffset ];
	BYTE pStr[50];
	switch ( m_nType ) {
	case SCRINFO_SIGNAL:
		ConvertLtoS( &pStr[0], *(long*)pBuf);
		break;
	case SCRINFO_TRACK:
	case SCRINFO_SWITCH:
	case SCRINFO_LAMP:
		break;
	}
	Name.Format("%-12.12s:", m_strName);
	dc << Name << (LPCTSTR)(&pStr[0]) << "\n";
}

#endif // _DEBUG_DUMP

int  CMessageInfo::GetTrackMessage(CObList &objList, void *pOld, void *pCur,
									  EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent)
{
	CLESApp* pApp;
	pApp= (CLESApp*)AfxGetApp();

	int no;
	union {
		void*			poldBuf;
		ScrInfoTrack*	poldT;
	};
	union {
		void*			pnewBuf;
		ScrInfoTrack*	pnewT;
	};
	ScrInfoTrack	xchgT;

	memset(&xchgT, 0, sizeof(xchgT));

	int  nAlter = 0;
	MessageEvent *pEvent;

	poldBuf = pOld;
	pnewBuf = pCur;

	xchgT.data[0] = poldT->data[0] ^ pnewT->data[0];
	xchgT.data[1] = poldT->data[1] ^ pnewT->data[1];
	if ( pPrnEvent == NULL ) 
	{
		if ( xchgT.TRACK && m_strName.Find("?") == -1 ) 
		{	
			no = (pnewT->TRACK) ? MSGNO_TRACK_OFF : MSGNO_TRACK_ON;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_TRACK;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
		if ( xchgT.data ) nAlter |= 3;
	} 
	else 
	{
		if ( pnewT->OVERLAP ) 
		{
			pPrnEvent->Track.nState = (pnewT->TRACK) ? 0 : 1;
		} else 
		{
			pPrnEvent->Track.nState = (pnewT->TRACK) ? 0 : 2;
		}

		pPrnEvent->strName = m_strName;
		if ( xchgT.TRACK ) nAlter |= 3;
	}

	if ( xchgT.INHIBIT && m_strName.Find("?") == -1 ) 
	{	
		if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" )
		{
			no = (!pnewT->INHIBIT) ? MSGNO_TRACK_LOS_OFF : MSGNO_TRACK_LOS_SET;
		}
		else
		{
			no = (!pnewT->INHIBIT) ? MSGNO_TRACK_COS_OFF : MSGNO_TRACK_COS_SET;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_TRACK;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}
	

	/* 20130312 ibjang add, 오버랩 관련 알람 출력 */
	//if (xchgT.TRACK || xchgT.ROUTE || xchgT.ROUTEDIR || xchgT.TKREQUEST || (xchgT.RightToLeftRouteNo && xchgT.LeftToRightRouteNo))
// 	if ( (xchgT.RightToLeftRouteNo || xchgT.LeftToRightRouteNo) )
// 	{
// 		no = 0;
// 		
// 		//if (pnewT->TRACK && pnewT->ROUTE && pnewT->ROUTEDIR && !pnewT->TKREQUEST && (pnewT->RightToLeftRouteNo || pnewT->LeftToRightRouteNo) )
// 		
// 		if ( !(poldT->RightToLeftRouteNo && poldT->LeftToRightRouteNo) && (pnewT->RightToLeftRouteNo || pnewT->LeftToRightRouteNo) )
// 			no = MSGNO_TRACK_OVERLAP_SET;
// 		else if ( (poldT->RightToLeftRouteNo || poldT->LeftToRightRouteNo) && !(pnewT->RightToLeftRouteNo && pnewT->LeftToRightRouteNo) )
// 			no = MSGNO_TRACK_OVERLAP_RES;
// 		
// 		if ( no != 0 )
// 		{
// 			pEvent = new MessageEvent;
// 			pEvent->msgno = no;
// 			pEvent->Type.nType  = MSG_TYPE_TRACK;
// 			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
// 			pEvent->time = *(EventKey.pTime);
// 			pEvent->hsec = EventKey.hsec;
// 			pEvent->evn  = EventKey.EvnLoc;
// 			EventKey.EvnLoc.nLoc++;
// 			strcpy(pEvent->name, m_strName);
// 			objList.AddTail( (CObject*)pEvent );
// 			errorMode |= pEvent->Type.nMode & 0x0f;
// 		}
// 	}								// IBJANG's code
	if ( poldT->LeftToRightRouteNo != pnewT->LeftToRightRouteNo )
	{
		no = ( pnewT->LeftToRightRouteNo ) ? MSGNO_TRACK_OVERLAP_SET : MSGNO_TRACK_OVERLAP_RES; 
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_TRACK;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}

	if ( poldT->RightToLeftRouteNo != pnewT->RightToLeftRouteNo )
	{
		no = ( pnewT->RightToLeftRouteNo ) ? MSGNO_TRACK_OVERLAP_SET : MSGNO_TRACK_OVERLAP_RES; 
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_TRACK;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}
	/* 20130312 ibjang add, 오버랩 관련 알람 출력 */

	/* 20130314 ibjang add, 궤도별 Emergency state 알람 출력 */
	/* 20130403 ibjang revise, Emergency state alarm condition revise */
	if (xchgT.EMREL)
	{
		no = 0;

		if ( pnewT->EMREL )
			no = MSGNO_TRACK_ROUTE_EMER_SET;
		else if ( !pnewT->EMREL )
			no = MSGNO_TRACK_ROUTE_EMER_RES;
		
		if ( no != 0 )
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_TRACK;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}
	/* 20130403 ibjang revise, Emergency state alarm condition revise */
	/* 20130314 ibjang add, 궤도별 Emergency state 알람 출력 */

	if (xchgT.DISTURB && m_strName.Find("?") == -1)
	{
		no = 0;
		
		if ( pnewT->DISTURB )
			no = MSGNO_TRACK_RECOVERY;
		else
			no = MSGNO_TRACK_DISTURB;
		
		if( no != 0 )
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_TRACK;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	if (xchgT.RESETBUF)
	{
		no = 0;
		
		if ( pnewT->RESETBUF )
			no = MSGNO_AX_RESET_REQUESTED;
		
		if( no != 0 )
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_TRACK;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}
	
	if (xchgT.RESETOUT)
	{
		no = 0;
		
		if ( pnewT->RESETOUT )
			no = MSGNO_AX_RESET_OUT;
		
		if( no != 0 )
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_TRACK;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}
	
	return nAlter;
}

BOOL CMessageInfo::CheckBlockSignal ( int nSignalType ) 
{
	BOOL bBlock;
	if		(( nSignalType & 0xf0 ) == BLOCKTYPE_AUTO_IN ) bBlock = TRUE;
	else if (( nSignalType & 0xf0 ) == BLOCKTYPE_AUTO_OUT ) bBlock = TRUE;
	else if (( nSignalType & 0xf0 ) == BLOCKTYPE_INTER_IN ) bBlock = TRUE;
	else if (( nSignalType & 0xf0 ) == BLOCKTYPE_INTER_OUT ) bBlock = TRUE;
	else bBlock = FALSE;
	return bBlock;
}

int CMessageInfo::CheckSignalType ( int nSignalType ) 
{
	int nSigType;
	if		(( nSignalType & 0xf0 ) == SIGNALTYPE_NONE )	nSigType = SIGNALTYPE_NONE;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_IN )		nSigType = SIGNALTYPE_IN;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_INBLOCK ) nSigType = SIGNALTYPE_INBLOCK;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_OUT )		nSigType = SIGNALTYPE_OUT;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_SHUNT )	nSigType = SIGNALTYPE_SHUNT;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_GSHUNT )	nSigType = SIGNALTYPE_GSHUNT;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_ISHUNT )	nSigType = SIGNALTYPE_ISHUNT;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_OHOME	)	nSigType = SIGNALTYPE_OHOME;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_ASTART )	nSigType = SIGNALTYPE_ASTART;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_MPASS )	nSigType = SIGNALTYPE_MPASS;
	else if (( nSignalType & 0xf0 ) == SIGNALTYPE_CALLON )	nSigType = SIGNALTYPE_CALLON;
	else nSigType = SIGNALTYPE_NONE;
	return nSigType;
}
									 
int  CMessageInfo::GetSignalMessage(CObList &objList, void *pOld, void *pCur,
							  EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent)
{
	CLESApp* pApp;
	pApp= (CLESApp*)AfxGetApp();

	int  nAlter = 0;
	int no;
	// 신호기 관련 메세지 처리용
	union {
		void*			poldBuf;
		ScrInfoSignal*	poldS;
	};
	union {
		void*			pnewBuf;
		ScrInfoSignal*	pnewS;
	};

	ScrInfoSignal	xchgS;
	
	// Block 관련 메세지 처리용 
	union {
		void*			poldBBuf;
		ScrInfoBlock*	poldBS;
	};
	union {
		void*			pnewBBuf;
		ScrInfoBlock*	pnewBS;
	};

	ScrInfoBlock	xchgBS;

	int  nSigAspType;
	BYTE nSignalType = 0;
	BYTE nSigType = 0;
	BYTE nBlockType;
	BYTE oldSigLamp;
	BYTE newSigLamp;
	BYTE oldLMRBit;
	BYTE newLMRBit;
	BOOL bCheck = FALSE;
	BOOL bYudoCheck = FALSE;
	BOOL bTTB = FALSE;
	BOOL bBlock = FALSE;

	MessageEvent *pEvent;

	poldBuf = pOld;
	pnewBuf = pCur;

	xchgS.pData[0]  = poldS->pData[0] ^ pnewS->pData[0];
	xchgS.pData[1]  = poldS->pData[1] ^ pnewS->pData[1];
	xchgS.pData[2]  = poldS->pData[2] ^ pnewS->pData[2];
	xchgS.pData[3]  = poldS->pData[3] ^ pnewS->pData[3];
	xchgS.DESTTRACK = poldS->DESTTRACK ^ pnewS->DESTTRACK;
	
    ScrInfoSignal OldSignal = *poldS;
	ScrInfoSignal NewSignal = *pnewS;

	CString copyStrName;

	// 블록 관련 
	poldBBuf = pOld;
	pnewBBuf = pCur;

	xchgBS.pData[0] = poldBS->pData[0] ^ pnewBS->pData[0];
	xchgBS.pData[1] = poldBS->pData[1] ^ pnewBS->pData[1];
	xchgBS.pData[2] = poldBS->pData[2] ^ pnewBS->pData[2];
	xchgBS.pData[3] = poldBS->pData[3] ^ pnewBS->pData[3];
	xchgBS.pData[4] = poldBS->pData[4] ^ pnewBS->pData[4];
	xchgBS.pData[5] = poldBS->pData[5] ^ pnewBS->pData[5];
	
	ScrInfoBlock NewBlock = *pnewBS;

	if ( pApp->m_pEipEmu == NULL) {
		nSigAspType = 0;
		nBlockType = 0;
		bCheck = FALSE;
	} else {
		nSignalType = pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nSignalType;
		nBlockType = pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nSignalType & 0xf0;
		nSigAspType = pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nSignalType & 3;
		if ( nSigAspType ) 
			bCheck = TRUE;  // 3,4,5 현시만 LMR 검사. 
		else 
			bCheck = FALSE;	 // 2 현시
		if ( pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nSignalType & SIGNALTYPE_YUDO_BIT )
			bYudoCheck = TRUE;
	}
	
	bBlock   = CheckBlockSignal ( nSignalType );
	nSigType = CheckSignalType  ( nSignalType );

	oldSigLamp = OldSignal.GetSignalInState( nSigAspType, oldLMRBit );
	newSigLamp = NewSignal.GetSignalInState( nSigAspType, newLMRBit );
	BYTE bNewLampOut = NewSignal.pData[0] & 7;
	BYTE bOldLampOut = OldSignal.pData[0] & 7;

	if ( (NewSignal.BitIN & 7) == 4 ) {
		if ( (OldSignal.BitIN & 7) == 0 ) {
			newLMRBit &= ~(BIT_LMRFAIL_Y + BIT_LMRFAIL_Y1);
		}
	}
	// 보류, 접근 쇄정 검사
	if ( xchgS.ON_TIMER && bBlock == FALSE) {
		if ( pPrnEvent == NULL ) {
			no = ( NewSignal.ON_TIMER ) ? MSGNO_SIGNAL_APPROACH_SLOCK : MSGNO_SIGNAL_ROUTELOCK_END;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			pEvent->item = (short)(pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nDelayTime);
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
		else {
			if ( NewSignal.ON_TIMER ) pPrnEvent->Signal.nNote = 5;
			nAlter |= 3;
		}
	}

//		2015.04.07	뒤에 ALR제어에 의한 OFF/Recovery 메시지가 따로 정의 되어 있음.. 
//					거기서  msg no 를 다시 Null로 만들기 때문에 하기는 아무 소용 없음.
//     // 신호기가 켜지는지 꺼지는지 검사 
// 	if ( xchgS.LM_ALR && bBlock == FALSE ) {
// 		if ( pPrnEvent == NULL ) {
// 			no = ( NewSignal.LM_ALR ) ? MSGNO_SIGNAL_ASPECT_OFF : MSGNO_SIGNAL_ASPECT_ON;
// 			pEvent = new MessageEvent;
// 			pEvent->msgno = no;
// 			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
// 			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
// 			pEvent->time = *(EventKey.pTime);
// 			pEvent->hsec = EventKey.hsec;
// 			pEvent->evn  = EventKey.EvnLoc;
// 			EventKey.EvnLoc.nLoc++;
// 			strcpy(pEvent->name, m_strName);
// 			objList.AddTail( (CObject*)pEvent );
// 			errorMode |= pEvent->Type.nMode & 0x0f;
// 		}
// 		else {
// 			if ( NewSignal.LM_ALR ) pPrnEvent->Signal.nNote = 5;
// 			nAlter |= 3;
// 		}
// 	}

	// 신호기 출력 검지 // 현재 출력에 대한 메시지는 표출하지 않음..(LISMSGKR에서 빠져있음.)
	if ( ( xchgS.SOUT1 || xchgS.SOUT2 || xchgS.SOUT3 || xchgS.U )&& bBlock == FALSE ) {
		if ( pPrnEvent == NULL ) {
			if      ( NewSignal.SOUT1 == 1 && NewSignal.SOUT2 == 1 && NewSignal.SOUT3 == 0 && NewSignal.U == 0 ) no = MSGNO_SIGNAL_G__OUT;
			else if ( NewSignal.SOUT1 == 0 && NewSignal.SOUT2 == 1 && NewSignal.SOUT3 == 0 && NewSignal.U == 0 ) no = MSGNO_SIGNAL_Y__OUT;
			else if ( NewSignal.SOUT1 == 0 && NewSignal.SOUT2 == 0 && NewSignal.SOUT3 == 0 && NewSignal.U == 0 ) no = MSGNO_SIGNAL_R__OUT;
			else if ( NewSignal.SOUT1 == 0 && NewSignal.SOUT2 == 1 && NewSignal.SOUT3 == 1 && NewSignal.U == 0 ) no = MSGNO_SIGNAL_YY_OUT;
			else if ( NewSignal.SOUT1 == 0 && NewSignal.SOUT2 == 0 && NewSignal.SOUT3 == 1 && NewSignal.U == 0 ) no = MSGNO_SIGNAL_CR_OUT;
			else if ( NewSignal.SOUT1 == 0 && NewSignal.SOUT2 == 0 && NewSignal.SOUT3 == 0 && NewSignal.U == 1 ) no = MSGNO_SIGNAL_SR_OUT;
			else no = MSGNO_SIGNAL_OUT;

			if ( nSigType == SIGNALTYPE_OHOME ) {
				if ( no == MSGNO_SIGNAL_Y__OUT ) no = MSGNO_SIGNAL_SY_OUT;
			}

			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

//			IB.JANG Coding....
// 	// 신호기 입력 검지 
// 	if ( ( xchgS.SIN1 || xchgS.SIN2 || xchgS.SIN3 || xchgS.INU )&& bBlock == FALSE ) {
// 		if ( pPrnEvent == NULL ) {
// 			if ( NewSignal.SIN1 == 1 && NewSignal.SIN2 == 1 && NewSignal.SIN3 == 0 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) {
// 				no = MSGNO_SIGNAL_G__IN;
// 			}
// 			else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.SIN3 == 0 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) no = MSGNO_SIGNAL_Y__IN;
// 			else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.SIN3 == 0 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) no = MSGNO_SIGNAL_R__IN;
// 			else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.SIN3 == 1 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) no = MSGNO_SIGNAL_YY_IN;
// 			//			else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.SIN3 == 1 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 && NewSignal.LM_CLOR == 1 ) no = MSGNO_SIGNAL_CR_IN;
// 			else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.SIN3 == 0 && NewSignal.INU == 1 ) no = MSGNO_SIGNAL_SR_IN;
// 			else if ( xchgS.INU && !NewSignal.INU ) no = MSGNO_SIGNAL_SR_OFF;
// 			else no = MSGNO_SIGNAL_IN;
// 			
// 			//Outer 신호기는 
// 			if ( nSigType == SIGNALTYPE_OHOME) {
// 				if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.SIN3 == 0 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) {
// 					no = MSGNO_SIGNAL_SY_IN;
// 				}
// 			}
// 			if ( nSigType == SIGNALTYPE_ASTART) {
// 				if ( NewSignal.SIN1 == 1 && NewSignal.SIN2 == 0 && NewSignal.SIN3 == 0 && NewSignal.INU == 0 && NewSignal.LM_LOR == 1 ) {
// 					no = MSGNO_SIGNAL_G__IN;
// 				}
// 			}
// 			
// 			pEvent = new MessageEvent;
// 			pEvent->msgno = no;
// 			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
// 			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
// 			pEvent->time = *(EventKey.pTime);
// 			pEvent->hsec = EventKey.hsec;
// 			pEvent->evn  = EventKey.EvnLoc;
// 			EventKey.EvnLoc.nLoc++;
// 			strcpy(pEvent->name, m_strName);
// 			
// 			/* 20130313 ibjang 수정 */
// 			// 			if ( m_strName == "A1" ) {
// 			// 				m_strName = m_strName;
// 			// 			}
// 			
// 			/* 20130313 ibjang 수정, outer signal G, YY, 1Y Lamp aspect alarm print */
// 			if ( nSigType == SIGNALTYPE_OHOME && ( no == MSGNO_SIGNAL_Y__IN || 
// 				//no == MSGNO_SIGNAL_G__IN ||
// 				//no == MSGNO_SIGNAL_R__IN ||
// 				//no == MSGNO_SIGNAL_YY_IN ||
// 				no == MSGNO_SIGNAL_GY_IN ||
// 				no == MSGNO_SIGNAL_RY_IN ||
// 				//no == MSGNO_SIGNAL_SY_IN ||
// 				no == MSGNO_SIGNAL_CR_IN ||
// 				no == MSGNO_SIGNAL_SR_IN )) 
// 			{
// 			} 
// 			else 
// 			{
// 				objList.AddTail( (CObject*)pEvent );
// 			}
// 			errorMode |= pEvent->Type.nMode & 0x0f;
// 		}
// 	}

	// 신호기 입력 검지 
	if ( ( xchgS.SIN1 || xchgS.SIN2 ) && NewSignal.LM_LOR && bBlock == FALSE && nSigType != SIGNALTYPE_OHOME) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.SIN1)
				no = MSGNO_SIGNAL_G__IN;
			else if ( NewSignal.SIN2)
				no = MSGNO_SIGNAL_Y__IN;
			else
				no = MSGNO_SIGNAL_R__IN;
		
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	if(xchgS.LM_CLOR && bBlock == FALSE && nSigType != SIGNALTYPE_OHOME)
	{
		if(pPrnEvent == NULL)
		{
			if(NewSignal.LM_CLOR && NewSignal.SIN3)
			{
				no = MSGNO_SIGNAL_CR_IN;
			}
			else if(!NewSignal.LM_CLOR)
			{
				no = MSGNO_SIGNAL_CR_OFF;
			}
			
			if(no != MSGNO_NOTHING)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if( (xchgS.SIN1 || xchgS.SIN2 || xchgS.LM_CLOR) && NewSignal.LM_LOR && bBlock == FALSE && nSigType == SIGNALTYPE_OHOME)
	{
		if(pPrnEvent == NULL)
		{
			if(NewSignal.SIN1)
			{
				no = MSGNO_SIGNAL_G__IN;
			}
			else if(NewSignal.SIN2)
			{
				if(NewSignal.SIN3 && NewSignal.LM_CLOR)
				{
					no = MSGNO_SIGNAL_YY_IN;
				}
				else if( !NewSignal.SIN3 )
				{
					no = MSGNO_SIGNAL_SY_IN;
				}
			}
			else if ( !NewSignal.SIN1 && !NewSignal.SIN2 )
			{
				no = MSGNO_SIGNAL_R__IN;
			}
			
			if(no != MSGNO_NOTHING)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if(xchgS.INU && (bBlock == FALSE))
	{
		if(pPrnEvent == NULL)
		{
			if(NewSignal.INU)
			{
				no = MSGNO_SIGNAL_SR_IN;
			}
			else
			{
				no = MSGNO_SIGNAL_SR_OFF;
			}
			
			if(no != MSGNO_NOTHING)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	/*
	// 신호기 fail 을 종류별로 판단하여 로깅을 생성한다. 
	if ( ( xchgS.SIN1 || xchgS.SIN2 || xchgS.SIN3 || xchgS.INU || xchgS.LM_LOR || xchgS.LM_CLOR || xchgS.SignalFail )&& bBlock == FALSE ) {
		if ( pPrnEvent == NULL ) {
			if ( nSigType == SIGNALTYPE_NONE) {
			} else if ( nSigType == SIGNALTYPE_IN) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else if ( nSigType ==SIGNALTYPE_INBLOCK ) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else if ( nSigType == SIGNALTYPE_OUT) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else if ( nSigType == SIGNALTYPE_ISHUNT) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else if ( nSigType == SIGNALTYPE_OHOME) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_SY_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
			} else if ( nSigType == SIGNALTYPE_ASTART) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else if ( nSigType == SIGNALTYPE_MPASS) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;				
			} else if ( nSigType == SIGNALTYPE_CALLON) {
				if ( NewSignal.SIN1 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_G_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 1 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_Y_MSF_FAIL;
				else if ( NewSignal.SIN1 == 0 && NewSignal.SIN2 == 0 && NewSignal.LM_LOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_R_MSF_FAIL;
				else if ( NewSignal.SIN3 == 1 && NewSignal.LM_CLOR == 0 && NewSignal.LM_ALR == 0 && NewSignal.SignalFail == 1 ) no = MSGNO_SIGNAL_C_MSF_FAIL;
				else no = MSGNO_SIGNAL_FAIL;
			} else no = MSGNO_SIGNAL_FAIL;
			
			if ( no != MSGNO_SIGNAL_FAIL ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	*/
	/*
	// 주심 단심 검사 
	if ( ( xchgS.LM_RL || xchgS.SignalFail )&& (bBlock == FALSE) ) {
		no = ( NewSignal.LM_RL ) ? MSGNO_SIGNAL_R_MF_FAIL : MSGNO_SIGNAL_R_MF_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SIGNAL;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;

	}
	*/

	/*
	if ( ( xchgS.LM_YL || xchgS.SignalFail ) && (bBlock == FALSE) ) {
		no = ( NewSignal.LM_YL ) ? MSGNO_SIGNAL_Y_MF_FAIL : MSGNO_SIGNAL_Y_MF_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SIGNAL;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}
	*/

	//---20130604 ibjang 신호기 고장 수정 
// if ( (xchgS.SOUT1 || xchgS.SOUT2 || xchgS.SOUT3 || xchgS.SignalFail) && !bBlock )
// {
// 	no = 0;
// 	
// 	if (NewSignal.SOUT1 && NewSignal.SOUT2 && !NewSignal.SOUT3 /*&& !NewSignal.U*/)	//--- HR, DR의 입력이 존재
// 	{
// 		if (xchgS.SignalFail && NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_G_MF_FAIL;
// 		}
// 		else if (xchgS.SignalFail && !NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_G_MF_RECOVERY;
// 		}
// 	}
// 	else if (!NewSignal.SOUT1 && NewSignal.SOUT2 && !NewSignal.SOUT3 /*&& !NewSignal.U*/) //--- YELLOW 입력이 존재
// 	{
// 		if (xchgS.SignalFail && NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_Y_MF_FAIL;
// 		}
// 		else if (xchgS.SignalFail && !NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_Y_MF_RECOVERY;
// 		}
// 	}
// 	else if (!NewSignal.SOUT1 && NewSignal.SOUT2 && NewSignal.SOUT3 /*&& !NewSignal.U*/) //--- YY 현시 시에 -> 버그 수정 후 재 코딩
// 	{
// 		if (xchgS.SignalFail && NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_Y_MF_FAIL;
// 		}
// 		else if (xchgS.SignalFail && !NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_Y_MF_RECOVERY;				
// 		}
// 	}
// 	else if (!NewSignal.SOUT1 && !NewSignal.SOUT2 && !NewSignal.SOUT3 /*&& !NewSignal.U*/ && xchgS.SignalFail) //--- 평상 시에 LOR 낙하 시
// 	{
// 		if (xchgS.SignalFail && NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_R_MF_FAIL;
// 		}
// 		else if (xchgS.SignalFail && !NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_R_MF_RECOVERY;
// 		}
// 	}
// 	else if (!NewSignal.SOUT1 && !NewSignal.SOUT2 && (NewSignal.SOUT3 || NewSignal.CS) /*&& !NewSignal.U*/)	//--- Call ON 현시 시에
// 	{
// 		if (xchgS.SignalFail && NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_C_MF_FAIL;
// 		}
// 		else if (xchgS.SignalFail && !NewSignal.SignalFail)
// 		{
// 			no = MSGNO_SIGNAL_C_MF_RECOVERY;
// 		}
// 	}
// 	
// 	if (no != NULL)
// 	{
// 		pEvent = new MessageEvent;
// 		pEvent->msgno = no;
// 		pEvent->Type.nType = MSG_TYPE_SIGNAL;
// 		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
// 		pEvent->time = *(EventKey.pTime);
// 		pEvent->hsec = EventKey.hsec;
// 		pEvent->evn  = EventKey.EvnLoc;
// 		EventKey.EvnLoc.nLoc++;
// 		strcpy(pEvent->name, m_strName);
// 		objList.AddTail( (CObject*)pEvent );
// 		errorMode |= pEvent->Type.nMode & 0x0f;
// 	}
// }

if ( xchgS.SignalFail && NewSignal.SignalFail && !bBlock )
{
	no = 0;
	
	if ( OldSignal.SOUT1 )
	{
		no = MSGNO_SIGNAL_G_MF_FAIL;
	}
	else if ( OldSignal.SOUT2 )
	{
		no = MSGNO_SIGNAL_Y_MF_FAIL;
	}
	else if ( OldSignal.SOUT3 )
	{
		no = MSGNO_SIGNAL_C_MF_FAIL;
	}
	else if ( OldSignal.U )
	{
		no = MSGNO_SIGNAL_S_FAIL;
	}
	else
	{
		no = MSGNO_SIGNAL_R_MF_FAIL;
	}
	
	if (no != NULL)
	{
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType = MSG_TYPE_SIGNAL;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}
}

if ( xchgS.LoopSigFail && NewSignal.LoopSigFail && !bBlock )
{
	no = MSGNO_SIGNAL_LI_FAIL;
	
	if (no != NULL)
	{
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType = MSG_TYPE_SIGNAL;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
	}
}

	if (xchgS.LM_ALR && !bBlock && (m_strName.Find("-") < 0))
	{
		no = NULL;

		if (NewSignal.LM_ALR)
		{
			no = MSGNO_ALR_STATE_ON;
		}
		else
		{
			no = MSGNO_ALR_STATE_OFF;
		}

		if (no != NULL)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	/*
	if ( ( xchgS.LM_GL || xchgS.SignalFail )&& (bBlock == FALSE) ) {
		no = ( NewSignal.LM_GL ) ? MSGNO_SIGNAL_G_MF_FAIL : MSGNO_SIGNAL_G_MF_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SIGNAL;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, m_strName);
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;

	}
	*/

	/*
	if ( ( xchgS.LM_CEKR || xchgS.SignalFail )&& (bBlock == FALSE) ) {
		if ( NewSignal.LM_CEKR == 0 && NewSignal.LM_CLOR == 1 ) {
			if ( nSigType == SIGNALTYPE_OHOME) {
				no = MSGNO_SIGNAL_Y_MF_FAIL;
			} else {
				no = MSGNO_SIGNAL_C_MF_FAIL;
			}
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;

		}
	}
	*/
	/*
	// 신호기 Fail 검사 
	if ( xchgS.SignalFail && bBlock == FALSE ) {
		if ( pPrnEvent == NULL ) {
			if ( ( NewSignal.LEFT == 1 || NewSignal.RIGHT == 1 ) && ( NewSignal.INLEFT == 0 && NewSignal.INRIGHT == 0 ) && NewSignal.SignalFail == 1) no = MSGNO_SIGNAL_LI_FAIL;
			else if ( NewSignal.SignalFail == 1 ) no = MSGNO_SYS_SIG_FAIL;
			else no = MSGNO_SYS_SIG_RECOVERY;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
		else {
			if ( NewSignal.SignalFail ) pPrnEvent->Signal.nNote = 5;
			nAlter |= 3;
		}
	}
	*/
                                                                                                                                                                                                                                                                                                                                                                                          
	// 루트 인디케이터 입력 검지 
	if (( xchgS.INLEFT || xchgS.INRIGHT ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.INLEFT == 1 || NewSignal.INRIGHT == 1)
				no = MSGNO_SIGNAL_IND_ON;
			else
				no = MSGNO_SIGNAL_IND_OFF;

			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				pEvent->item = (short)(pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nDelayTime);
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if ((  xchgS.INLEFT || xchgS.INRIGHT ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.INLEFT == 0 && NewSignal.INRIGHT == 0) {
				if ( NewSignal.LEFT == 1 || NewSignal.RIGHT == 1 ) no = MSGNO_SIGNAL_IND_FAIL;
				else no = MSGNO_NOTHING;
			} else {
				no = MSGNO_NOTHING;
			}
			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
/*
	if (( xchgS.LEFT ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.LEFT == 1 ) {
				if ( NewSignal.INLEFT == 1 ) no = MSGNO_SIGNAL_IND_ON;
				else no = MSGNO_NOTHING;
			} else {
				if ( NewSignal.INLEFT == 1 ) no = MSGNO_SIGNAL_IND_OFF;
				else no = MSGNO_NOTHING;
			}
			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				pEvent->item = (short)(pApp->m_pEipEmu->m_pSignalInfoTable[m_nID].nDelayTime);
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if ((  xchgS.INLEFT  ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.INLEFT == 0 && NewSignal.INRIGHT == 0) {
				if ( NewSignal.LEFT == 1 || NewSignal.RIGHT == 1 ) no = MSGNO_SIGNAL_IND_FAIL;
				else no = MSGNO_NOTHING;
			} else {
				no = MSGNO_NOTHING;
			}
			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (( xchgS.RIGHT ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.RIGHT == 1 ) {
				if ( NewSignal.INRIGHT == 1 ) no = MSGNO_SIGNAL_IND_ON;
				else no = MSGNO_NOTHING;
			} else {
				if ( NewSignal.INRIGHT == 1 ) no = MSGNO_SIGNAL_IND_OFF;
				else no = MSGNO_NOTHING;
			}
			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if ((  xchgS.INRIGHT  ) && (bBlock == FALSE) ) {
		if ( pPrnEvent == NULL ) {
			if ( NewSignal.INRIGHT == 0 && NewSignal.INLEFT == 0) {
				if ( NewSignal.LEFT == 1 || NewSignal.RIGHT == 1 ) no = MSGNO_SIGNAL_IND_FAIL;
				else no = MSGNO_NOTHING;
			} else {
				no = MSGNO_NOTHING;
			}
			if ( no != MSGNO_NOTHING ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
*/
	/* 20130312 ibjang 수정, 방향 변경에 따라 방향 변수를 참고하여 로그 생성 */
	// CA 검지 
	if ( xchgBS.CA  && bBlock == TRUE ) {
		if ( pPrnEvent == NULL ) {
			if ( NewBlock.CA == 1 ) no = MSGNO_BLOCK_CA_ON;
			else no = MSGNO_BLOCK_CA_OFF;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;

			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Up2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Down2 direction TCB";
			}
			else
			{
				if ( m_strName == "B1" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Down2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Up2 direction TCB";
			}

			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;

		}
	}

	// CBB 검지 
	if ( xchgBS.CBB  && bBlock == TRUE ) 
	{
		if ( pPrnEvent == NULL ) 
		{
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Up2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Down2 direction TCB";
			}
			else
			{
				if ( m_strName == "B1" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Down2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Up2 direction TCB";
			}

			if ( NewBlock.CBB && (m_strName == "B1" || m_strName == "B3" || m_strName == "B11") )
				no = MSGNO_BLOCK_BCB_ON;
			else if ( !NewBlock.CBB && (m_strName == "B1" || m_strName == "B3" || m_strName == "B11") )
				no = MSGNO_BLOCK_BCB_OFF;
			else if ( NewBlock.CBB && (m_strName == "B2" || m_strName == "B4" || m_strName == "B12") )
				no = MSGNO_BLOCK_BRB_ON;
			else if ( !NewBlock.CBB && (m_strName == "B2" || m_strName == "B4" || m_strName == "B12") )
				no = MSGNO_BLOCK_BRB_OFF;

			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;

			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	// LCR 검지
	if ( xchgBS.LCR  && bBlock == TRUE ) 
	{
		if ( pPrnEvent == NULL ) 
		{
			if ( NewBlock.LCR == 1 ) 
			{
				if (nSignalType&BLOCKTYPE_AUTO_OUT || nSignalType&BLOCKTYPE_INTER_OUT)
					no = MSGNO_BLOCK_LCR_ON;
				else
					no = MSGNO_BLOCK_LCG_ON;
			}
			else
			{
				if (nSignalType&BLOCKTYPE_AUTO_OUT || nSignalType&BLOCKTYPE_INTER_OUT)
					no = MSGNO_BLOCK_LCR_OFF;
				else
					no = MSGNO_BLOCK_LCG_OFF;
			}
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Up2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Down2 direction TCB";
			}
			else
			{
				if ( m_strName == "B1" ) copyStrName = "Down direction TGB";
				if ( m_strName == "B2" ) copyStrName = "Up direction TCB";
				if ( m_strName == "B3" ) copyStrName = "Up direction TGB";
				if ( m_strName == "B4" ) copyStrName = "Down direction TCB";
				if ( m_strName == "B11" ) copyStrName = "Down2 direction TGB";
				if ( m_strName == "B12" ) copyStrName = "Up2 direction TCB";
			}
			
			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}
	/* 20130312 ibjang 수정, 방향 변경에 따라 방향 변수를 참고하여 로그 생성 */
	
	// LCR 검지
	if ( xchgBS.LCRIN  && bBlock == TRUE && NewBlock.LCRIN) 
	{
		if ( pPrnEvent == NULL && !(nSignalType&BLOCKTYPE_INTER_IN) && !(nSignalType&BLOCKTYPE_INTER_OUT)) 
		{
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B2" ) no = MSGNO_MSG_DTC_LCRREQ;
				if ( m_strName == "B4" ) no = MSGNO_MSG_UTC_LCRREQ;
				if ( m_strName == "B12" ) no = MSGNO_MSG_DTC2_LCRREQ;
			}
			else
			{
				if ( m_strName == "B2" ) no = MSGNO_MSG_UTC_LCRREQ;
				if ( m_strName == "B4" ) no = MSGNO_MSG_DTC_LCRREQ;
				if ( m_strName == "B12" ) no = MSGNO_MSG_UTC2_LCRREQ;
			}

			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			
			
			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}
	/* 20130312 ibjang 수정, 방향 변경에 따라 방향 변수를 참고하여 로그 생성 */
	
	// BGPR 반응.
	if ( xchgBS.RBGPR  && bBlock == TRUE ) 
	{
		if ( pPrnEvent == NULL ) 
		{
			if ( NewBlock.RBGPR == 1 )
			{
				if ( pApp->m_bLeftSideIsUpDirection == TRUE )
				{
					if ( m_strName == "B1" )
						no = MSGNO_BLOCK_UP_BGR_OCC;
					else if ( m_strName == "B3" )
						no = MSGNO_BLOCK_DN_BGR_OCC;
					else if ( m_strName == "B11" )
						no = MSGNO_BLOCK_UP2_BGR_OCC;
				}
				else
				{
					if ( m_strName == "B1" )
						no = MSGNO_BLOCK_DN_BGR_OCC;
					else if ( m_strName == "B3" )
						no = MSGNO_BLOCK_UP_BGR_OCC;
					else if ( m_strName == "B11" )
						no = MSGNO_BLOCK_DN2_BGR_OCC;
				}
			}
			else
			{
				if ( pApp->m_bLeftSideIsUpDirection == TRUE )
				{
					if ( m_strName == "B1" )
						no = MSGNO_BLOCK_UP_BGR_CLR;
					else if ( m_strName == "B3" )
						no = MSGNO_BLOCK_DN_BGR_CLR;
					else if ( m_strName == "B11" )
						no = MSGNO_BLOCK_UP2_BGR_CLR;
				}
				else
				{
					if ( m_strName == "B1" )
						no = MSGNO_BLOCK_DN_BGR_CLR;
					else if ( m_strName == "B3" )
						no = MSGNO_BLOCK_UP_BGR_CLR;
					else if ( m_strName == "B11" )
						no = MSGNO_BLOCK_DN2_BGR_CLR;
				}
			}
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	// BCPR 반응.
	if ( xchgBS.RBCPR  && bBlock == TRUE ) 
	{
		if ( pPrnEvent == NULL ) 
		{
			if ( NewBlock.RBCPR == 1 )
			{
				if ( pApp->m_bLeftSideIsUpDirection == TRUE )
				{
					if ( m_strName == "B2" )
						no = MSGNO_BLOCK_DN_BCR_OCC;
					else if ( m_strName == "B4" )
						no = MSGNO_BLOCK_UP_BCR_OCC;
					else if ( m_strName == "B12" )
						no = MSGNO_BLOCK_DN2_BCR_OCC;
				}
				else
				{
					if ( m_strName == "B2" )
						no = MSGNO_BLOCK_UP_BCR_OCC;
					else if ( m_strName == "B4" )
						no = MSGNO_BLOCK_DN_BCR_OCC;
					else if ( m_strName == "B12" )
						no = MSGNO_BLOCK_UP2_BCR_OCC;
				}
			}
			else
			{
				if ( pApp->m_bLeftSideIsUpDirection == TRUE )
				{
					if ( m_strName == "B2" )
						no = MSGNO_BLOCK_DN_BCR_CLR;
					else if ( m_strName == "B4" )
						no = MSGNO_BLOCK_UP_BCR_CLR;
					else if ( m_strName == "B12" )
						no = MSGNO_BLOCK_DN2_BCR_CLR;
				}
				else
				{
					if ( m_strName == "B2" )
						no = MSGNO_BLOCK_UP_BCR_CLR;
					else if ( m_strName == "B4" )
						no = MSGNO_BLOCK_DN_BCR_CLR;
					else if ( m_strName == "B12" )
						no = MSGNO_BLOCK_UP2_BCR_CLR;
				}
			}
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, copyStrName);
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
	}

	//--- 20130305, ibjang 추가, AXLE COUNTER OCCUPIED 알람 출력
	if (xchgBS.AXLOCC && bBlock == TRUE && !NewBlock.SAXLACTIN) //--- 점유 정보 변경시
	{
		if (pPrnEvent == NULL )
		{
			no = 0;
			if (NewBlock.AXLOCC == 1) //-- Normal 상태
				no = MSGNO_AX_RCV_OCCUPYED_RES;
			else if (NewBlock.AXLOCC == 0) //-- 점유 상태 (RESET 가능)
				no = MSGNO_AX_RCV_OCCUPYED_SET;
			
			if (pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
			{
				if ( m_strName == "B2" || m_strName == "B4" )
					no = 0;
			}

			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Block ";
				}

				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLOCC && bBlock == TRUE && NewBlock.SAXLACTIN) //--- 점유 정보 변경시
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.SAXLOCC == 1) //-- Normal 상태
				no = MSGNO_AX_RCV_OCCUPYED_RES;
			else if (NewBlock.SAXLOCC == 0) //-- 점유 상태 (RESET 가능)
				no = MSGNO_AX_RCV_OCCUPYED_SET;
			
			if (pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
			{
				if ( m_strName == "B2" || m_strName == "B4" )
					no = 0;
			}

			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Super-Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Super-Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	/* 20130319 ibjang revise, AX Counter 숫자 확인하여 alarm 출력 */
	//--- 20130305, ibjang 추가, AXLE COUNTER DISTURB 알람 출력
	if (xchgBS.AXLDST && bBlock == TRUE && !NewBlock.SAXLACTIN) //--- DISTURB 정보 변경시
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			
			if (NewBlock.AXLDST == 1) //-- Normal 상태
				no = MSGNO_AX_RCV_DISTURBED_RES;
			else if (NewBlock.AXLDST == 0)
				no = MSGNO_AX_RCV_DISTURBED_SET;
			
			if (pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
			{
				if ( m_strName == "B2" || m_strName == "B4" )
					no = 0;
			}

			if ( no != 0 )
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLDST && bBlock == TRUE && NewBlock.SAXLACTIN) //--- DISTURB 정보 변경시
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			
			if (NewBlock.SAXLDST == 1) //-- Normal 상태
				no = MSGNO_AX_RCV_DISTURBED_RES;
			else if (NewBlock.SAXLDST == 0)
				no = MSGNO_AX_RCV_DISTURBED_SET;
			
			if (pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
			{
				if ( m_strName == "B2" || m_strName == "B4" )
					no = 0;
			}

			if ( no != 0 )
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;

				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Super-Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Super-Block ";
				}

				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.AXLREQ && bBlock == TRUE && !NewBlock.SAXLACTIN)
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.AXLREQ == 1)
				no = MSGNO_AX_RESET_REQUESTED;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLREQ && bBlock == TRUE && NewBlock.SAXLACTIN)
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.SAXLREQ == 1)
				no = MSGNO_AX_RESET_REQUESTED;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Super-Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Super-Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.AXLACC && bBlock == TRUE && !NewBlock.SAXLACTIN)
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.AXLACC == 1) 
				no = MSGNO_AX_RESET_ACCEPT;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLACC && bBlock == TRUE && NewBlock.SAXLACTIN)
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.SAXLACC == 1) 
				no = MSGNO_AX_RESET_ACCEPT;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Super-Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Super-Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.AXLDEC && bBlock == TRUE && !NewBlock.SAXLACTIN) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.AXLDEC == 1) 
				no = MSGNO_AX_RESET_DECLINE;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLDEC && bBlock == TRUE && NewBlock.SAXLACTIN) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.SAXLDEC == 1) 
				no = MSGNO_AX_RESET_DECLINE;
			
			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming Super-Block ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going Super-Block ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming Super-Block ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going Super-Block ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming Super-Block ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.SAXLACTIN && bBlock == TRUE) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (NewBlock.SAXLACTIN == 1) 
				no = MSGNO_AX_SAXLACT_ACTIVATE;
			else
				no = MSGNO_AX_SAXLACT_DEACTIVATE;
			
			if (pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
			{
				if ( m_strName == "B1" || m_strName == "B3" )
					no = 0;
			}

			if (no != 0)
			{
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_TRACK;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				if (pApp->m_bLeftSideIsUpDirection == TRUE)
				{
					if ( m_strName == "B1" ) copyStrName = "Up Direction Going ";
					if ( m_strName == "B2" ) copyStrName = "Down Direction Coming ";
					if ( m_strName == "B3" ) copyStrName = "Down Direction Going ";
					if ( m_strName == "B4" ) copyStrName = "Up Direction Coming ";
				}
				else
				{
					if ( m_strName == "B1" ) copyStrName = "Down Direction Going ";
					if ( m_strName == "B2" ) copyStrName = "Up Direction Coming ";
					if ( m_strName == "B3" ) copyStrName = "Up Direction Going ";
					if ( m_strName == "B4" ) copyStrName = "Down Direction Coming ";
				}
				
				if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
				{
					copyStrName.Replace(" Coming ", " ");
					copyStrName.Replace(" Going ", " ");
				}

				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.BLKRSTREQ && bBlock == TRUE && NewBlock.BLKRSTREQ && !NewBlock.SWEEPING) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_REQ;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_REQ;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_REQ;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_REQ;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_REQ;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_REQ;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_REQ;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_REQ;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();
				
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.BLKRSTACC && bBlock == TRUE && NewBlock.BLKRSTACC && !NewBlock.SWEEPING) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
				}
				else
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
				}
			}
			else
			{
				if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
				}
				else
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
				}
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.BLKRSTDEC && bBlock == TRUE && NewBlock.BLKRSTDEC) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_DEC;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_DEC;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_DEC;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_DEC;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_DEC;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_DEC;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_DEC;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_DEC;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}

	if (xchgBS.BLKRSTREQIN && bBlock == TRUE && NewBlock.BLKRSTREQIN && !NewBlock.SWEEPING) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_REQ;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_REQ;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_REQ;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_REQ;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_REQ;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_REQ;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_REQ;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_REQ;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.BLKRSTACCIN && bBlock == TRUE && NewBlock.BLKRSTACCIN && !NewBlock.SWEEPING) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
				}
				else
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
				}
			}
			else
			{
				if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
				}
				else
				{
					if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC;
					if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_ACC;
					if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC;
					if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_ACC;
					if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
					if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
					if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
					if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
				}
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.BLKRSTDECIN && bBlock == TRUE && NewBlock.BLKRSTDECIN) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_DEC;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_RST_DEC;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_RST_DEC;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_RST_DEC;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_DEC;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_RST_DEC;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_RST_DEC;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_RST_DEC;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.SWEEPING && bBlock == TRUE && NewBlock.SWEEPING) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_SWEEPING_S;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_SWEEPING_S;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_SWEEPING_S;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_SWEEPING_S;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_SWEEPING_S;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_SWEEPING_S;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_SWEEPING_S;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_SWEEPING_S;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_SWEEPING_S;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_SWEEPING_S;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_SWEEPING_S;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_SWEEPING_S;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_SWEEPING_S;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_SWEEPING_S;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_SWEEPING_S;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_SWEEPING_S;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();

				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if (xchgBS.SWEEPINGACK && bBlock == TRUE && NewBlock.SWEEPINGACK) 
	{
		if (pPrnEvent == NULL)
		{
			no = 0;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_UP_BGR_SWEEPING_E;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_DN_BCR_SWEEPING_E;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_DN_BGR_SWEEPING_E;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_UP_BCR_SWEEPING_E;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_UP2_BGR_SWEEPING_E;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_DN2_BCR_SWEEPING_E;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_DN2_BGR_SWEEPING_E;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_UP2_BCR_SWEEPING_E;
			}
			else
			{
				if ( m_strName == "B1" ) no = MSGNO_BLOCK_DN_BGR_SWEEPING_E;
				if ( m_strName == "B2" ) no = MSGNO_BLOCK_UP_BCR_SWEEPING_E;
				if ( m_strName == "B3" ) no = MSGNO_BLOCK_UP_BGR_SWEEPING_E;
				if ( m_strName == "B4" ) no = MSGNO_BLOCK_DN_BCR_SWEEPING_E;
				if ( m_strName == "B11" ) no = MSGNO_BLOCK_DN2_BGR_SWEEPING_E;
				if ( m_strName == "B12" ) no = MSGNO_BLOCK_UP2_BCR_SWEEPING_E;
				if ( m_strName == "B13" ) no = MSGNO_BLOCK_UP2_BGR_SWEEPING_E;
				if ( m_strName == "B14" ) no = MSGNO_BLOCK_DN2_BCR_SWEEPING_E;
			}
			
			if (no != 0)
			{
				copyStrName.Empty();
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SIGNAL;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				
				strcpy(pEvent->name, copyStrName);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
	}
	
	if ( pPrnEvent == NULL ) {
		if ( xchgS.pData[0] || xchgS.pData[1] || xchgS.pData[2] || xchgS.DESTTRACK) {
			nAlter |= 3;
		}
	}
	return nAlter;
}




//2001.04.22 jin - add code
typedef struct tagDetailInfoType {
	BYTE nMask;
	const char *pName;
} DetailInfoType;

DetailInfoType _pDetailSwitchList[4][8] = {
// BYTE 0
	{	{ 0x03, " KR-P " },		//				4th
		{ 0x03, " KR-M " },		//	4th
		{ 0x03, " WR-P " },		//	3th
		{ 0x03,	" WR-M " },		//	3th
		{ 0x03, " WLR " },		//	2th	on			6th off
		{ 0x02, " FREE " },		//
		{ 0x03, " WRO-M "},		//	1th	on			5th off
		{ 0x03, " WRO-P "}		//	1th	on			5th off
	},
// BYTE 1
	{
		{ 0x02, " BACKROUTE "},	//
		{ 0x02, " ROUTELOCK "},	//
		{ 0x02, " REQUEST-P "},	//
		{ 0x02, " REQUEST-M "},	//
		{ 0x02, " ON TIMER "},	//
		{ 0x03, " WLRO "},		//	1th on			5th off
		{ 0x02, " SWFAIL "},	//
		{ 0x02, " TRACKLOCK " }	//
	},
// BYTE 2
	{
		{ 0x03, " AMCRO " },	//16
		{ 0x03, " BMCRO " }, 	//17
		{ 0x03, " AMCRI " },	//18
		{ 0x03, " BMCRI " },	//19
		{ 0x03, " AKR_P " },	//20
		{ 0x03, " AKR_M " },	//21
		{ 0x03, " BKR_P " },	//22
		{ 0x03, " BKR_M " },	//23
	},
// BYTE 3
	{
		{ 0x00, ""	},			//24
		{ 0x03, " PHPR " },		//25
		{ 0x00, ""	}, 			//26
		{ 0x00, ""	},			//27
		{ 0x00, ""  },			//28
		{ 0x00, ""	},			//29
		{ 0x00, ""	},			//30
		{ 0x00, ""	},			//31
	}
};

int  CMessageInfo::GetSwitchMessage(CObList &objList, void *pOld, void *pCur,
									  EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent)
{
	CLESApp* pApp;
	pApp= (CLESApp*)AfxGetApp();

	BYTE d;
	union {
		void*			 poldBuf;
		ScrInfoSwitch	*poldP;
	};
	union {
		void*			 pnewBuf;
		ScrInfoSwitch	*pnewP;
	};
	union {
		ScrInfoSwitch	xchgP;
	};

	int  nAlter = 0;
	MessageEvent *pEvent;

	poldBuf = pOld;
	pnewBuf = pCur;

	BOOL bHighXChgInput = FALSE;

	xchgP.pData[0] = poldP->pData[0] ^ pnewP->pData[0];
	xchgP.pData[1] = poldP->pData[1] ^ pnewP->pData[1];
	xchgP.pData[2] = poldP->pData[2] ^ pnewP->pData[2];

	if ( xchgP.pData[0] || xchgP.pData[1] || xchgP.pData[2]  ) {	//
		nAlter |= 3;

		if ( xchgP.NOUSED ) {
			int no = ( pnewP->NOUSED ) ? MSGNO_POINT_NOUSE_SET : MSGNO_POINT_NOUSE_RELEASE;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SWITCH;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}


		if ( xchgP.KEYOPER ) {
			int no = ( pnewP->KEYOPER ) ? MSGNO_HPOINT_KEY_LOCK : MSGNO_HPOINT_KEY_RELEASE;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SWITCH;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}

		if ( xchgP.WKEYKR ) {
			int no = ( pnewP->WKEYKR ) ? MSGNO_POINT_KEY_LOCK : MSGNO_POINT_KEY_RELEASE;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SWITCH;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}

		if ( xchgP.BLOCK )
		{
			int no = ( pnewP->BLOCK ) ? MSGNO_POINT_UNBLOCK : MSGNO_POINT_BLOCK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType = MSG_TYPE_SWITCH;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, m_strName);
			pEvent->item = 0;
			objList.AddTail((CObject*)pEvent);
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}

#ifndef   _NOMASKINFO_
		if ( pPrnEvent == NULL ) {
			if ( pApp->m_RecordMaskInfo.bSwitch && pApp->m_RecordMaskInfo.pSwitch[ m_nID-1 ] ) {
				BYTE mask;
				BYTE detail  = 1 << pApp->m_RecordMaskInfo.bDetailSwitch;
				int  maxloop = 2;	
				for(int i=0; i<maxloop; i++) {
					mask = 0x80;
					for(int j=7; j>=0; j--) {
						if ( xchgP.pData[i] & mask ) {
							if (_pDetailSwitchList[i][j].nMask & detail) {
								if ( *_pDetailSwitchList[i][j].pName ) {
									int no = (pnewP->pData[i] & mask) ? MSGNO_SWITCH_DATA_ON : MSGNO_SWITCH_DATA_OFF;
									pEvent = new MessageEvent;
									pEvent->msgno = no;
									pEvent->Type.nType  = MSG_TYPE_SWITCH;
									pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
									pEvent->time = *(EventKey.pTime);
									pEvent->hsec = EventKey.hsec;
									pEvent->evn  = EventKey.EvnLoc;
									EventKey.EvnLoc.nLoc++;
									strcpy(pEvent->name, m_strName);
									strcat(pEvent->name, _pDetailSwitchList[i][j].pName);
									pEvent->op   = mask;
									pEvent->real = pnewP->pData[i];
									objList.AddTail( (CObject*)pEvent );
									errorMode |= pEvent->Type.nMode & 0x0f;

								}
							}
						}
						mask >>= 1;
					}
				}
			}
		}
#endif // _NOMASKINFO_
	}

	d = BYTE(m_nID);
	// out
	if ( pPrnEvent == NULL ) {
		if ( xchgP.BitOUT ) {		// XOR buffer, alternated in state ?
				if ( xchgP.WRO_P ) {
					if (pnewP->WRO_P && !pnewP->WRO_M) {
						pEvent = new MessageEvent;
						pEvent->Type.nType  = MSG_TYPE_SWITCH;
						pEvent->msgno = MSGNO_SWITCH_OUT_P;
						pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( MSGNO_SWITCH_OUT_P );
						pEvent->time = *(EventKey.pTime);
						pEvent->hsec = EventKey.hsec;
						pEvent->evn  = EventKey.EvnLoc;
						EventKey.EvnLoc.nLoc++;
						strcpy(pEvent->name, m_strName);
						pEvent->op   = d;
						pEvent->real = pnewP->BitOUT;
						objList.AddTail( (CObject*)pEvent );
						errorMode |= pEvent->Type.nMode & 0x0f;

					}
				}
				if ( xchgP.WRO_M ) {
					if (pnewP->WRO_M && !pnewP->WRO_P) {
						pEvent = new MessageEvent;
						pEvent->msgno = MSGNO_SWITCH_OUT_M;
						pEvent->Type.nType  = MSG_TYPE_SWITCH;
						pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( MSGNO_SWITCH_OUT_M );
						pEvent->time = *(EventKey.pTime);
						pEvent->hsec = EventKey.hsec;
						pEvent->evn  = EventKey.EvnLoc;
						EventKey.EvnLoc.nLoc++;
						strcpy(pEvent->name, m_strName);
						pEvent->op   = d;
						pEvent->real = pnewP->BitOUT;
						objList.AddTail( (CObject*)pEvent );
						errorMode |= pEvent->Type.nMode & 0x0f;

					}
				}
		}
	}
	
	int  bRecever = 0 ;
	if ( xchgP.SWFAIL && !pnewP->SWFAIL ) 
	{
		int no = 0;

		bRecever = 1;

			if ( (pnewP->WR_P == pnewP->KR_P) && (pnewP->WR_M == pnewP->KR_M) ) {
				if ( pPrnEvent ) { 
					pPrnEvent->Switch.nNote = ( pnewP->WR_P ) ? MSGNO_SWITCH_COVER_UNVAL_P : MSGNO_SWITCH_COVER_UNVAL_M;
				}
				else {
					no = ( pnewP->WR_P ) ? MSGNO_SWITCH_COVER_UNVAL_P : MSGNO_SWITCH_COVER_UNVAL_M;
				}
			}
			if ( pPrnEvent == NULL ) {
				if ( no ) {
					pEvent = new MessageEvent;
					pEvent->msgno = no;
					pEvent->Type.nType  = MSG_TYPE_SWITCH;
					pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
					pEvent->time = *(EventKey.pTime);
					pEvent->hsec = EventKey.hsec;
					pEvent->evn  = EventKey.EvnLoc;
					EventKey.EvnLoc.nLoc++;
					strcpy(pEvent->name, m_strName);
					pEvent->op   = d;
					pEvent->real = pnewP->pData[0];
					objList.AddTail( (CObject*)pEvent );
					errorMode |= pEvent->Type.nMode & 0x0f;
					no = 0;

				}
			}
			else {
				if ( pPrnEvent->Switch.nNote ) nAlter |= 2;
			}
	}
	int no = 0;
	if ( xchgP.SWFAIL && pnewP->SWFAIL ) {
	// 불일치 
		if ( pnewP->WRO_P ) {
			if ( pPrnEvent ) 
				pPrnEvent->Switch.nNote = 1;
			else
				no = MSGNO_SWITCH_FAULT_P;
		}
		else if ( pnewP->WRO_M ) {
			if ( pPrnEvent ) 
				pPrnEvent->Switch.nNote = 2;
			else
				no = MSGNO_SWITCH_FAULT_M;
		}
		else if ( (pnewP->WR_P != pnewP->KR_P) || (pnewP->WR_M != pnewP->KR_M) ) {
			if ( pPrnEvent ) { 
				pPrnEvent->Switch.nNote = ( pnewP->WR_P ) ? 1 : 2;
			}
			else {
				no = ( pnewP->WR_P ) ? MSGNO_SWITCH_UNVAL_P : MSGNO_SWITCH_UNVAL_M;
			}
		}
		else if ( pnewP->WR_P == 0 && pnewP->WR_M == 0 && pnewP->KR_P == 0 && pnewP->KR_M == 0 ) {
			no = MSGNO_SWITCH_UNVAL_P;
		}
		else {
			if ( pPrnEvent ) 
				pPrnEvent->Switch.nNote = 0;
			else
				no = 0;
		}
		if ( pPrnEvent == NULL ) {
			if ( no ) {
				pEvent = new MessageEvent;
				pEvent->msgno = no;
				pEvent->Type.nType  = MSG_TYPE_SWITCH;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, m_strName);
				pEvent->op   = d;
				pEvent->real = pnewP->pData[0];
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
			}
		}
		else {
			if ( pPrnEvent->Switch.nNote ) nAlter |= 2;
		}
	}
	if ( xchgP.BitIN || bHighXChgInput ) {		// XOR buffer, alternated in state ?
		if ( xchgP.KR_P && !bRecever ) {
			if (pnewP->WR_P && pnewP->KR_P && !pnewP->KR_M) {	// Current Buffer
				nAlter |= 2;
				if ( pPrnEvent == NULL ) {
					pEvent = new MessageEvent;
					pEvent->msgno = MSGNO_SWITCH_CHANGE_P;
					pEvent->Type.nType  = MSG_TYPE_SWITCH;
					pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( MSGNO_SWITCH_CHANGE_P );
					pEvent->time = *(EventKey.pTime);
					pEvent->hsec = EventKey.hsec;
					pEvent->evn  = EventKey.EvnLoc;
					EventKey.EvnLoc.nLoc++;
					strcpy(pEvent->name, m_strName);
					pEvent->op   = d;
					pEvent->real = pnewP->BitIN;
					objList.AddTail( (CObject*)pEvent );
					errorMode |= pEvent->Type.nMode & 0x0f;

				}
			}
		}
		if ( xchgP.KR_M && !bRecever ) {
			if (pnewP->WR_M && pnewP->KR_M && !pnewP->KR_P) {	// Current buffer
				nAlter |= 2;
				if ( pPrnEvent == NULL ) {
					pEvent = new MessageEvent;
					pEvent->msgno = MSGNO_SWITCH_CHANGE_M;
					pEvent->Type.nType  = MSG_TYPE_SWITCH;
					pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( MSGNO_SWITCH_CHANGE_M );
					pEvent->time = *(EventKey.pTime);
					pEvent->hsec = EventKey.hsec;
					pEvent->evn  = EventKey.EvnLoc;
					EventKey.EvnLoc.nLoc++;
					strcpy(pEvent->name, m_strName);
					pEvent->op   = d;
					pEvent->real = pnewP->BitIN;
					objList.AddTail( (CObject*)pEvent );
					errorMode |= pEvent->Type.nMode & 0x0f;
				}
			}
		}
	}

	if ( pPrnEvent ) {
		pPrnEvent->Switch.nState = ( pnewP->WR_P ) ? 0 : 1;
		pPrnEvent->strName = m_strName;
	}
	return nAlter;
}

int  CMessageInfo::GetLampMessage(CObList &objList, void *pOld, void *pCur,
									  EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent)
{
	CLESApp* pApp;
	pApp= (CLESApp*)AfxGetApp();

	BYTE d;
	union {
		void*			poldBuf;
		ScrInfoLamp*	poldL;
	};
	union {
		void*			pnewBuf;
		ScrInfoLamp*	pnewL;
	};
	ScrInfoLamp		xchgL;

	int  no;
	int  nAlter = 0;
	MessageEvent *pEvent;

	poldBuf = pOld;
	pnewBuf = pCur;

	xchgL.data = poldL->data ^ pnewL->data;

	d = BYTE(m_nID);

	if ( xchgL.BitLamp ) {		// XOR buffer, alternated in state ?
		if ( pPrnEvent == NULL ) {
			nAlter |= 3;	// 2;
			no = (pnewL->BitLamp) ? MSGNO_SIGNAL_BLOCKADE_GOOD : MSGNO_SIGNAL_BLOCKADE_FAULT;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SIGNAL;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			char *s = m_strName.GetBuffer(1);
			BOOL find = FALSE;
			while(*s) {
				if (*s++ == '.') {
					find = TRUE;
					break;
				}
			}
// 폐색 신호기 명칭 - "B2200T.1.대전1"
//						|     |   + 로깅용 명칭
//						|     + SFM 표시 
//						+ ID명칭
			if ( !find ) {
				s = m_strName.GetBuffer(1);
			}
			else {
				if ( s[1] == '.' ) s += 2;
			}
			strcpy(pEvent->name, s);
			pEvent->op   = d;
			pEvent->real = pnewL->data;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
		}
		else {
			pPrnEvent->Signal.nNote  = ( pnewL->BitLamp ) ? 0 : 1;
			pPrnEvent->Signal.nMode  = 3;
			pPrnEvent->strName = m_strName;
			if (pnewL->BitLamp) {	// Current Buffer
				nAlter |= 3;
			}
		}
	}
	return nAlter;
}


int  CMessageInfo::GetMessage(CObList &objList, void *pOld, void *pCur,
								  EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent)
{
	int  nAlter = 0;
	if (!pOld || !pCur) return nAlter;

	switch (m_nType) {
	case SCRINFO_TRACK:
		nAlter = GetTrackMessage( objList, pOld, pCur, EventKey, errorMode, pPrnEvent );
		break;
	case SCRINFO_SIGNAL:
		nAlter = GetSignalMessage( objList, pOld, pCur, EventKey, errorMode, pPrnEvent );
		break;
	case SCRINFO_SWITCH:
		nAlter = GetSwitchMessage( objList, pOld, pCur, EventKey, errorMode, pPrnEvent );
		break;
	case SCRINFO_BUTTON:
		break;
	default:
		break;
	}
	return nAlter;
}

BOOL CMessageInfo::GetEvent(PrintTimeEvent &PrnEvent, void *pOld, void *pCur)
{
	int nAlter = 0;
	if (!pOld || !pCur) return (BOOL)(nAlter);

	BYTE errorMode = 0x00;
	CObList   objList;
	EventKeyType EventKey;

	switch (m_nType) {
	case SCRINFO_TRACK:
		nAlter = GetTrackMessage( objList, pOld, pCur, EventKey, errorMode, &PrnEvent );
		break;
	case SCRINFO_SIGNAL:
		nAlter = GetSignalMessage( objList, pOld, pCur, EventKey, errorMode, &PrnEvent );
		break;
	case SCRINFO_SWITCH:
		nAlter = GetSwitchMessage( objList, pOld, pCur, EventKey, errorMode, &PrnEvent );
		break;
	case SCRINFO_BUTTON:
		break;
	case SCRINFO_LAMP:
		nAlter = GetLampMessage( objList, pOld, pCur, EventKey, errorMode, &PrnEvent );
		break;
	default:
		break;
	}
	return (BOOL)(nAlter);
}

/////////////////////////////////////////////////////////////////////////////
