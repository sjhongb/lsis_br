#ifndef _IOINFO_H
#define _IOINFO_H

// IOInfo.h : header file
//


#include <iostream.h>
#include <fstream.h>
#include "iorack.h"

#define MAX_RACK	 4

/////////////////////////////////////////////////////////////////////////////

class CIOInfo : public CDocument
{
public:
	BOOL LoadFileRDF( LPCTSTR pPath );
	BYTE m_pRealAddress[256];
	void Out( ostream &os );
	short FindIOName( CString &str );
	short SetIOName( char *szName, short nAddr );
	short FindIOName( char *szName );
	CIOInfo();           // protected constructor used by dynamic creation
protected:
	DECLARE_DYNCREATE(CIOInfo)

// Attributes
public:
	CIORack     m_Rack[MAX_RACK];
    CDTSInfo    m_DTS;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOInfo)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIOInfo();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CIOInfo)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
