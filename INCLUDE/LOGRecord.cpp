/////////////////////////////////////////////////////////////////////////////
// CLOGRecord.cpp : implementation file
//

#include "stdafx.h"

#include <string.h>
#include "LOGRecord.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////

CLOGRecord::CLOGRecord()
{
	// TODO: add one-time construction code here
#ifdef _LLR
	pApp= (CLLRApp*)AfxGetApp();
#else
	pApp= (CLESApp*)AfxGetApp();
#endif

	m_Head.Type  = 0x00;
	m_Head.Flag  = 0;
	m_Head.Order = 0;
	m_Head.Length = pApp->nVerRecordSize;

	m_nVersion = REC_VER_200;		//REC_VER_101 -> REC_VER_200

	m_SysHead.nYear  = 0x01;		//2000(0x00) -> 2001(0x01)
	m_SysHead.nMonth = 0x01;	// 1
	m_SysHead.nDate  = 0x01;	// 1
	m_SysHead.nHour  = 0x00;
	m_SysHead.nMin   = 0x00;
	m_SysHead.nSec   = 0x00;
	m_dwPosition = 0;

}

CLOGRecord::CLOGRecord(CLOGRecord &Record)
{
	// TODO: add one-time construction code here
#ifdef _LLR
	pApp= (CLLRApp*)AfxGetApp();
#else
	pApp= (CLESApp*)AfxGetApp();
#endif
	*this = Record;
}

CLOGRecord::~CLOGRecord()
{
}

void CLOGRecord::RecordOR( CLOGRecord &orRec )
{
	if ((m_Head.Type != 'A') || (m_Head.Type != orRec.m_Head.Type) || (m_Head.Length != orRec.m_Head.Length)) 
		return;
	int  i, j;
	for(i=0; i<pApp->nSfmRecordSize; i++) {					// Track,Signal,Switch Object Data
		m_BitBlock[i] |= orRec.m_BitBlock[i];
	}
	if ( !m_BitBlock[i] && orRec.m_BitBlock[i] ) {		// Operate Cmd
		for(j=0; j<MAX_OPERATE_CMD_CODESIZE; j++) {
			m_BitBlock[i] = orRec.m_BitBlock[i];
			i++;
		}
	}
	else i += MAX_OPERATE_CMD_CODESIZE;
	if ( !m_BitBlock[i] && orRec.m_BitBlock[i] ) {		// Error, Track NO Unit code
		for(j=0; j<MAX_OPERATE_CMD_UNITCODE; j++) {
			m_BitBlock[i] = orRec.m_BitBlock[i];
			i++;
		}
	}
}

void CLOGRecord::InitEventClear()
{
	m_BitBlock[pApp->nSfmRecordSize] = 0x00;
	m_BitBlock[pApp->nSfmRecordSize+MAX_OPERATE_CMD_CODESIZE] = 0xff;
	m_BitBlock[pApp->nSfmRecordSize+MAX_OPERATE_CMD_CODESIZE+1] = 0x00;
}

void CLOGRecord::SetRecord(BYTE *record, int length)
{
	m_Head.Type   = 'A';
	m_Head.Flag   = 0x00;
	m_Head.Length = (short)length;
	m_nVersion = REC_VER_200;	//REC_VER_101 -> REC_VER_200
	memcpy(m_BitBlock, record, length);
}

void CLOGRecord::Write(FILE *fp) 
{
	m_dwPosition = ftell( fp );
	
	if (m_Head.Length > pApp->nVerRecordSize) {
		m_Head.Length = pApp->nVerRecordSize;
	}

	fwrite(&m_Head, MaxExtSize(), 1, fp);
	fwrite(&m_dwPosition, sizeof(DWORD), 1, fp);		// 4 Byte.
}

int  CLOGRecord::Read(FILE *fp, BOOL bVersion)
{
	int  n = fread(&m_Head, sizeof(LOGHeaderType), 1, fp);
	BYTE c = m_Head.Type;
	if ( n && (m_Head.IsVersion || bVersion) ) {
		n = fread(&m_nVersion, sizeof(short), 1, fp);
	}
	else {	
		m_nVersion = 0;		// before Ver 1.0.
	}		
	if ( n ) {
		
		int len = m_Head.Length;
		if (len > pApp->nVerRecordSize) {
			m_Head.Length = pApp->nVerRecordSize;
		}
		
		n = fread(&m_BitBlock[0], len, 1, fp);
		if ( n && (m_nVersion || bVersion) ) {
			n = fread(&m_dwPosition, sizeof(DWORD), 1, fp);
		}
	}
	if (bVersion && !m_nVersion) {
		m_nVersion = REC_VER_200;
		m_Head.IsVersion = 1;
	}

	return n;
}

void CLOGRecord::Write(CFile *fp) 
{
	m_dwPosition = fp->GetPosition();
	if (m_Head.Length > pApp->nVerRecordSize) {
		m_Head.Length = pApp->nVerRecordSize;
	}
	fp->Write(&m_Head, MaxExtSize());
	fp->Write(&m_dwPosition, sizeof(DWORD));
}

UINT CLOGRecord::Read(CFile *fp, BOOL bVersion)
{
	UINT n = fp->Read(&m_Head, sizeof(LOGHeaderType));
	if ( n && (m_Head.IsVersion || bVersion) ) {
		n = fp->Read(&m_nVersion, sizeof(short));
	}
	else {	
		m_nVersion = 0;
	}		
	if ( n ) {
		int len = m_Head.Length;
		if (len > pApp->nVerRecordSize) {
			m_Head.Length = pApp->nVerRecordSize;
		}
	
		n = fp->Read(&m_BitBlock[0], len);
		if ( n && (m_nVersion || bVersion) ) {
			n = fp->Read(&m_dwPosition, sizeof(DWORD));
		}
	}
	if (bVersion && !m_nVersion) {
		m_nVersion = REC_VER_200;
		m_Head.IsVersion = 1;
	}
	return n;
}

/////////////////////////////////////////////////////////////////////////////
// CLOGRecord diagnostics

CLOGRecord& CLOGRecord::operator=(const CLOGRecord &Record)
{
	short len;
#ifdef _LLR
	pApp= (CLLRApp*)AfxGetApp();
#else
	pApp= (CLESApp*)AfxGetApp();
#endif
	m_Head = Record.m_Head;
	m_nVersion = Record.m_nVersion;
	len = Record.m_Head.Length;
	if (len > pApp->nVerRecordSize) {
		len = pApp->nVerRecordSize;
		m_Head.Length = len;
	}
	memcpy(&m_BitBlock[0], &Record.m_BitBlock[0], (size_t)(len));
	return *this;
}
