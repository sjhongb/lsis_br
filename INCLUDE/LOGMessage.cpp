//=======================================================
//==            LOGMessage.cpp
//=======================================================
//  파 일 명 :  CLOGMessage.cpp
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :	2.01 
//  설    명 :	LOG 메세지를 만든다. 
//
//=======================================================
#include "stdafx.h"
#include "Logic.h"
#include "FMSGList.h"
#include "TrnoProc.h"
#include "LOGMessage.h"
#include "../LES/LESView.h"
#include "../LES/MainFrm.h"

#define MY_BLUE		0x000
#define MY_RED		0x040
#define MY_YELLOW	0x080
#define MY_GREEN	0x0C0
#define MY_GRAY		0x100

int  CLOGMessage::m_nCreateCount = 0;

extern class CFMSGList *_pEIPMsgList;

BYTE Info_cUsedCardMask[8] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }; // 8 card for 1 rack.
BYTE Info_CardTypeNo[8][10] = {
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
};

CLOGMessage::CLOGMessage()
{
	pApp= (CLESApp*)AfxGetApp();
	CLOGMessage::m_nCreateCount++;
	m_bNoneBase = TRUE;
	m_iCountOfDestTrack = 0;
}
CLOGMessage::~CLOGMessage()
{
	if (CLOGMessage::m_nCreateCount > 0) {
		CLOGMessage::m_nCreateCount--;
		if (CLOGMessage::m_nCreateCount == 0) {
			if (pApp->Info_pScrMessageInfo != NULL) {
				delete [] pApp->Info_pScrMessageInfo;
				pApp->Info_pScrMessageInfo = NULL;
			}
		}
	}
}

int CLOGMessage::GetSwitchControlMode( int nID, void *pNew )
{
	if ( pApp->m_pEipEmu == NULL ) return -1;
	int n = sizeof(DataHeaderType) + (SIZE_INFO_TRACK * pApp->nMaxTrack) 
								   + (SIZE_INFO_SIGNAL * pApp->nMaxSignal)
								   + (SIZE_INFO_SWITCH * nID);
	BYTE* p =(BYTE*)pNew;
	ScrInfoSwitch* pnewP = (ScrInfoSwitch*)&p[n];

	return ( (pnewP->WRO_P && !pnewP->WRO_M) ? 1 : 0 );
}

int  CLOGMessage::GetUnitSendMessage(CObList &objList, BYTE* ptr, BYTE* pNew,
									EventKeyType &EventKey, BYTE &errorMode, BOOL bDownMsg)
{
	int nAlter = 0;

	if ( !ptr ) return nAlter;

	int  i;
	int  iPort = 0;

	/*
	union {
		BYTE d1;
		struct {
			BYTE byte : 2;	// 0 - 3
			BYTE card : 3;	// 0 - 7
			BYTE rack : 3;  // 0 - 7
		} Bit;
	};

	union {
		BitLocType		Loc;
		BitLocTypeEx	ExLoc;
	};
	*/

	MessageEvent *pEvent;
	CString name;


	short no = 0;
	DataHeaderType		*pnewHead	= (DataHeaderType *)pNew;
	SystemStatusType	*pnewVar	= &pnewHead->nSysVar;
	UnitMessageType		*pUnitMsg	= (UnitMessageType*)ptr;

	switch ( pUnitMsg->nCmd ) {
	case (BYTE)(0xFF):

		no = pUnitMsg->nMsg;
		i = 0;
		switch ( pUnitMsg->nMsg ) {
		case 0:
			break;
		case 0x05:
			if ( pApp->m_iFDIM_Err_Backup[0] == (int)(pUnitMsg->nBits[0]/4) && pApp->m_iFDIM_Err_Backup[1] == (int)(((pUnitMsg->nBits[0]%4)*8) + iPort) ) {
			} else {
				pApp->m_iFDIM_Err_Backup[0] = (int)(pUnitMsg->nBits[0]/4);
				pApp->m_iFDIM_Err_Backup[1] = (int)(((pUnitMsg->nBits[0]%4)*8) + iPort);
				pEvent = new MessageEvent;
				pEvent->msgno = MSGNO_ERR_IO_PORT_UNVAL;
				pEvent->Type.nType  = MSG_TYPE_SYSTEM;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, LPCTSTR(name));
				if ( pUnitMsg->nBits[1] == 0x1  ) iPort = 0;
				if ( pUnitMsg->nBits[1] == 0x2  ) iPort = 1;
				if ( pUnitMsg->nBits[1] == 0x4  ) iPort = 2;
				if ( pUnitMsg->nBits[1] == 0x8  ) iPort = 3;
				if ( pUnitMsg->nBits[1] == 0x10 ) iPort = 4;
				if ( pUnitMsg->nBits[1] == 0x20 ) iPort = 5;
				if ( pUnitMsg->nBits[1] == 0x40 ) iPort = 6;
				if ( pUnitMsg->nBits[1] == 0x80 ) iPort = 7;
				pEvent->exloc.nCard = (int)(pUnitMsg->nBits[0]/4);
				pEvent->exloc.nPort = (int)(((pUnitMsg->nBits[0]%4)*8) + iPort);
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
				nAlter = 1;
				}
			break;
		case 0x49:
			if ( pApp->m_iFDOM_Err_Backup[0] == (int)pUnitMsg->nBits[0] && pApp->m_iFDOM_Err_Backup[1] == (int)(pUnitMsg->nBits[1]-1) ) {
			} else {
				pApp->m_iFDOM_Err_Backup[0] = (int)pUnitMsg->nBits[0];
				pApp->m_iFDOM_Err_Backup[1] = (int)pUnitMsg->nBits[1]-1;
				pEvent = new MessageEvent;
				pEvent->msgno = MSGNO_ERR_IOFAULTOUT;
				pEvent->Type.nType  = MSG_TYPE_SYSTEM;
				pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
				pEvent->time = *(EventKey.pTime);
				pEvent->hsec = EventKey.hsec;
				pEvent->evn  = EventKey.EvnLoc;
				EventKey.EvnLoc.nLoc++;
				strcpy(pEvent->name, LPCTSTR(name));
				if ( pUnitMsg->nBits[0] > 0x0B ) pEvent->exloc.nCard = pUnitMsg->nBits[0]-0x0B;
				else pEvent->exloc.nCard = pUnitMsg->nBits[0];
				pEvent->exloc.nPort = pUnitMsg->nBits[1]-1;
				objList.AddTail( (CObject*)pEvent );
				errorMode |= pEvent->Type.nMode & 0x0f;
				nAlter = 1;
			}
			break;
		}
	}
	return nAlter;
}

//=======================================================
//
//  함 수 명 :  GetOperateMessage
//  함수출력 :
//  함수입력 :
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  제어 관련 메세지의 표시 
//
//=======================================================
int  CLOGMessage::GetOperateMessage(CObList &objList, BYTE* pOp, BYTE* pOld, BYTE* pNew,
									EventKeyType &EventKey, BYTE &errorMode)
{
	int bAlter = 0;

	if ( !pOp ) return bAlter;

	union {
		short ID;
		struct {
			BYTE dl;
			BYTE dh;
		};
	};
	
	char cmd;

	short no;
	CString name,name1,name2;
	DataHeaderType		*pcurHead = (DataHeaderType *)pNew;
	DataHeaderType		*poldHead	= (DataHeaderType *)pOld;
	SystemStatusType	*poldVar	= &poldHead->nSysVar;
	SystemStatusType	*pnewVar	= &pcurHead->nSysVar;
	SystemStatusType	xchgSysVar;

	MessageEvent *pEvent;
	BYTE nSignalType;
	BYTE type;
	BYTE *pCmd = pOp;
	BOOL bCtcMode = FALSE;
//	BYTE bBlockID;
	ScrInfoSignal* pSigInfo;
//	ScrInfoBlock * pBlkInfo;
	ScrInfoSwitch* pSwInfo;
	ScrInfoTrack * pTrackInfo;

	xchgSysVar.pVar[0] = poldVar->pVar[0] ^ pnewVar->pVar[0];
	xchgSysVar.pVar[1] = poldVar->pVar[1] ^ pnewVar->pVar[1];
	xchgSysVar.pVar[2] = poldVar->pVar[2] ^ pnewVar->pVar[2];
	xchgSysVar.pVar[3] = poldVar->pVar[3] ^ pnewVar->pVar[3];
	xchgSysVar.pVar[4] = poldVar->pVar[4] ^ pnewVar->pVar[4];
	xchgSysVar.pVar[5] = poldVar->pVar[5] ^ pnewVar->pVar[5];
	xchgSysVar.pVar[6] = poldVar->pVar[6] ^ pnewVar->pVar[6];
	xchgSysVar.pVar[7] = poldVar->pVar[7] ^ pnewVar->pVar[7];

	cmd = *pCmd++;		// operate command(cmd STX)
	if ((cmd < 0x40) || (cmd > 0x4f) ) 
	{
		return bAlter;
	}

	cmd = *pCmd++;		// cmd code
	if ( !cmd ) 
		return bAlter;
	else 
	{
		if ( cmd & 0x80 ) bCtcMode = TRUE;
		cmd &= 0x7f;
	}
	ID = *(short*)pCmd;				// signal ID
	pCmd += sizeof(short);
	pEvent = NULL;
	switch ( cmd ) {

	case WS_OPMSG_START:		// 시스템 기동(cmd, ~cmd, cmd)
		cmd = cmd & ~dl & dh;
		if (cmd == WS_OPMSG_START) {
			no = MSGNO_MSG_CTRLSYSRUN;		// 915
			type = MSG_TYPE_SYSTEM;
			name = "";
		}
		else 
			cmd = 0;
		break;
	case WS_OPMSG_RESET:		// 시스템 재기동(cmd, ~cmd, cmd)
		cmd = cmd & ~dl & dh;
		if (cmd == WS_OPMSG_RESET) {
			no = MSGNO_MSG_CTRLSYSRESET;	// 916
			type = MSG_TYPE_SYSTEM;
			name = "";
		}
		else 
			cmd = 0;
		break;
	case WS_OPMSG_SETTIMER:		// UNIT 시계 조정(초,분,시,일,월,년)
		no = MSGNO_MSG_CTRLTIME;	// 914
		type = MSG_TYPE_SYSTEM;
		name = "";
		break;
	case WS_OPMSG_TRAIN_NUMBER:
		no = MSGNO_SYS_OP_TRAIN_NUMBER;
		type = MSG_TYPE_SYSTEM;
		name = "";
		break;
	case WS_OPMSG_ASPECT:
		if ((dl < 1) || (dl > pApp->nMaxSignal)) {	// dl:signal ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SIGNAL;
			if ( pApp->m_pEipEmu ) {
				nSignalType  = pApp->m_pEipEmu->m_pSignalInfoTable[ dl ].nSignalType & 0xf0;
			}
			else nSignalType = 0;
			if (nSignalType == BLOCKTYPE_INTER_OUT) {	// 연동폐색 출발 취소
			}
			else {
				no = MSGNO_SIGNAL_OP_RECOVERY;	//301
				// 				name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName;		// 신호기
				UINT nInfoPtr;
				BYTE *pInfo;
				long ldl;
				BYTE ldh;
				ldl = (long)dl;
				ldh = (BYTE)dh;
				
				WORD nRouteNo = pApp->m_pEipEmu->m_pSignalInfoTable[ ldl ].wRouteNo;
				nRouteNo += (ldh & 0x3F);
				
				// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
				nInfoPtr = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[ nRouteNo ];
				pInfo = &pApp->m_pEipEmu->m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
				int nButtonID = pInfo[RT_OFFSET_BUTTON];
				BYTE nRouteStatus = pInfo[RT_OFFSET_ROUTESTATUS];
				
				if ( nButtonID ) {
					GetRouteName( name, ldl, nButtonID );
					if ( nRouteStatus & RT_CALLON ) name = name + "(C)";
					else if ( nRouteStatus & RT_SHUNTSIGNAL ) name = name +"(S)";
					else name = name + "(M)"; 
					
					if ( pInfo[RT_OFFSET_ROUTETAG] != 0 )
					{
						CString strTag;
						strTag.Format("-%c", (char)pInfo[RT_OFFSET_ROUTETAG]);
						
						name += strTag;
					}
				}
			}
		}
		break;
	case WS_OPMSG_SIGNALDEST:	// 신호 
		if ((dl < 1) || (dl > pApp->nMaxSignal)) {	// dl:signal ID, dh:Track ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SIGNAL;
			long ldl;
			BYTE ldh;
			ldl = (long)dl;
			ldh = (BYTE)dh;
			if ( pApp->m_pEipEmu ) {
				nSignalType  = pApp->m_pEipEmu->m_pSignalInfoTable[ ldl ].nSignalType & 0xf0;
			}
			else nSignalType = 0;
			if (nSignalType == BLOCKTYPE_INTER_OUT) {	// 연동폐색 출발 취급
				no = MSGNO_SIGNAL_OP_OBLK_ON;	//320
				name  = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName[0];	// 폐색출발 
				name += "Line";
			}
			else {	
				UINT nInfoPtr;	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
				BYTE *pInfo;
				no = MSGNO_SIGNAL_OP_ON;		//300				
				WORD nRouteNo = pApp->m_pEipEmu->m_pSignalInfoTable[ ldl ].wRouteNo;
				nRouteNo += (ldh & 0x3F);

				// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
				nInfoPtr = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[ nRouteNo ];
				pInfo = &pApp->m_pEipEmu->m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
				int nButtonID = pInfo[RT_OFFSET_BUTTON];
				BYTE nRouteStatus = pInfo[RT_OFFSET_ROUTESTATUS];

				if ( nButtonID ) {
					GetRouteName( name, ldl, nButtonID );
					if ( nRouteStatus & RT_CALLON ) name = name + "(C)";
					else if ( nRouteStatus & RT_SHUNTSIGNAL ) name = name +"(S)";
					else name = name + "(M)"; 

					if ( pInfo[RT_OFFSET_ROUTETAG] != 0 )
					{
						CString strTag;
						strTag.Format("-%c", (char)pInfo[RT_OFFSET_ROUTETAG]);

						name += strTag;
					}
				}
			}
		}
		break;
	case WS_OPMSG_SIGNALCANCEL:	// 진로해정
		if ((dl < 1) || (dl > pApp->nMaxSignal)) {	// dl:signal ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SIGNAL;
			if ( pApp->m_pEipEmu ) {
				nSignalType  = pApp->m_pEipEmu->m_pSignalInfoTable[ dl ].nSignalType & 0xf0;
			}
			else nSignalType = 0;
			if (nSignalType == BLOCKTYPE_INTER_OUT) {	// 연동폐색 출발 취소
				no = MSGNO_SIGNAL_OP_OBLK_OFF;	//320
				name  = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName[0];	// 폐색 출발
				name += "Line";
			}
			else {
				no = MSGNO_SIGNAL_OP_OFF;	//301
// 				name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName;		// 신호기
				UINT nInfoPtr;
				BYTE *pInfo;
				long ldl;
				BYTE ldh;
				ldl = (long)dl;
				ldh = (BYTE)dh;

				WORD nRouteNo = pApp->m_pEipEmu->m_pSignalInfoTable[ ldl ].wRouteNo;
				nRouteNo += (ldh & 0x3F);
				
				// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
				nInfoPtr = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[ nRouteNo ];
				pInfo = &pApp->m_pEipEmu->m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
				int nButtonID = pInfo[RT_OFFSET_BUTTON];
				BYTE nRouteStatus = pInfo[RT_OFFSET_ROUTESTATUS];
				
				if ( nButtonID ) {
					GetRouteName( name, ldl, nButtonID );
					if ( nRouteStatus & RT_CALLON ) name = name + "(C)";
					else if ( nRouteStatus & RT_SHUNTSIGNAL ) name = name +"(S)";
					else name = name + "(M)"; 
					
					if ( pInfo[RT_OFFSET_ROUTETAG] != 0 )
					{
						CString strTag;
						strTag.Format("-%c", (char)pInfo[RT_OFFSET_ROUTETAG]);
						
						name += strTag;
					}
				}
			}
		}
		break;
	case WS_OPMSG_SIGNALSTOP:	// 신호 취소
		if ((dl < 1) || (dl > pApp->nMaxSignal)) {	// dl:signal ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SIGNAL;
			if ( pApp->m_pEipEmu ) {
				nSignalType  = pApp->m_pEipEmu->m_pSignalInfoTable[ dl ].nSignalType & 0xf0;
			}
			else nSignalType = 0;
			if (nSignalType == BLOCKTYPE_INTER_OUT) {	// 연동폐색 출발 취소
				no = MSGNO_SIGNAL_OP_OBLK_OFF;	//320
				name  = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName[0];	// 폐색 출발
				name += "Line";
			}
			else {
				no = MSGNO_SIGNAL_OP_CANCEL;	//301
				name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName;		// 신호기
			}
		}
		break;
	case WS_OPMSG_SWTOGGLE:		// 선로전환기 정,반위 제어
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSwitch)) {	//  dl:switch ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SWITCH;
			if ( dl & 0x80 ) no = MSGNO_SWITCH_OPERATE_M;
			else no = MSGNO_SWITCH_OPERATE_P;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+pApp->nMaxSignal+(dl & 0x7f)-1].m_strName;
		}
		break;
	case WS_OPMSG_POINT_BLOCK:		// 선로전환기 정,반위 제어
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSwitch)) {	//  dl:switch ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SWITCH;
			if ( dl & 0x80 ) no = MSGNO_POINT_OPERATE_UNBLOCK;
			else no = MSGNO_POINT_OPERATE_BLOCK;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+pApp->nMaxSignal+(dl & 0x7f)-1].m_strName;
		}
		break;
	case WS_OPMSG_HSW:		// 선로전환기 정,반위 제어
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSwitch)) {	//  dl:switch ID
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SWITCH;
			if ( dl & 0x80 ) no = MSGNO_HSWITCH_OPERATE_RELEASE;
			else no = MSGNO_HSWITCH_OPERATE_LOCK;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+pApp->nMaxSignal+(dl & 0x7f)-1].m_strName;
		}		
		break;
	case WS_OPMSG_POS_ON:				// LOS 버튼 제어
	case WS_OPMSG_POS_OFF:				// LOS 버튼 제어
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSwitch)) {	//  dl:switch ID
			cmd = 0;
		}
		else {
			pSwInfo = (ScrInfoSwitch *)&pNew[pApp->m_pEipEmu->m_TableBase.InfoSwitch.nStart + ((dl-1) & 0x7f) * sizeof(ScrInfoSwitch)];
			if ( pSwInfo->NOUSED ) {
				no = MSGNO_SYS_OP_POS_OFF;
			} else {
				no = MSGNO_SYS_OP_POS_ON;
			}
			type = MSG_TYPE_SWITCH;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+pApp->nMaxSignal+(dl & 0x7f)-1].m_strName;
		}
		break;
	case WS_OPMSG_NOUSE:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSwitch)) {	//  dl:switch ID
			cmd = 0;
		}
		else {
			pSwInfo = (ScrInfoSwitch *)&pNew[pApp->m_pEipEmu->m_TableBase.InfoSwitch.nStart + ((dl-1) & 0x7f) * sizeof(ScrInfoSwitch)];
			if ( pSwInfo->NOUSED ) {
				no = MSGNO_SYS_OP_POS_OFF;
			} else {
				no = MSGNO_SYS_OP_POS_ON;
			}
			type = MSG_TYPE_SWITCH;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+pApp->nMaxSignal+(dl & 0x7f)-1].m_strName;
		}
		break;
	case WS_OPMSG_FREESUBTRACK:		// 비상 구분 진로 해정
		if ((dl < 1) || (dl > pApp->nMaxTrack)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_TRACK;
			no = MSGNO_TRACK_OP_FREESUBROUTE;
			dh = 0x00;
			type = MSG_TYPE_TRACK;
			name = pApp->Info_pScrMessageInfo[dl-1].m_strName;
		}
		break;
	case WS_OPMSG_TRACK_RESET:
		if ((dl < 1) || (dl > pApp->nMaxTrack)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_TRACK;
			if ( dh == 1 )
			{
				no = MSGNO_AX_RESET_ACCEPT;
			}
			else
			{
				no = MSGNO_AX_RESET_DECLINE;
			}
			name = pApp->Info_pScrMessageInfo[dl-1].m_strName;
		}
		break;
	case WS_OPMSG_AXL_RESET:
		if ( dl == (BtnB2AXLRST & 0xFF) )
		{
			no = MSGNO_AX_OP_RESET_REQUEST;
			type = MSG_TYPE_TRACK;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
				name = "Down Direction Coming Block ";
			else
				name = "Up Direction Coming Block ";
		}
		else if ( dl == (BtnB4AXLRST & 0xFF) )
		{
			no = MSGNO_AX_OP_RESET_REQUEST;
			type = MSG_TYPE_TRACK;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
				name = "Up Direction Coming Block ";
			else
				name = "Down Direction Coming Block ";
		}
		if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL )
		{
			name.Replace(" Coming ", " ");
		}
		if ( dh & 0x80 )
			name.Replace("Block","Super-Block");
		break;
	case WS_OPMSG_BLOCK_CLEAR:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSignal)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SYSTEM;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+(dh & 0x7f)-1].m_strName;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( dl == BLKRST_CMD_REQ )
				{
					if ( name == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_REQ;
					else if ( name == "B2") no = MSGNO_BLOCK_DN_BCR_RST_REQ;
					else if ( name == "B3") no = MSGNO_BLOCK_DN_BGR_RST_REQ;
					else if ( name == "B4") no = MSGNO_BLOCK_UP_BCR_RST_REQ;
					else if ( name == "B11") no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
					else if ( name == "B12") no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
					else if ( name == "B13") no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
					else if ( name == "B14") no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
				}
				else if ( dl == BLKRST_CMD_ACC )
				{
					if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
					{
						if ( name == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
						else if ( name == "B2") no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
						else if ( name == "B3") no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
						else if ( name == "B4") no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
						else if ( name == "B11") no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
						else if ( name == "B12") no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
						else if ( name == "B13") no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
						else if ( name == "B14") no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
					}
					else
					{
						if ( name == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_ACC;
						else if ( name == "B2") no = MSGNO_BLOCK_DN_BCR_RST_ACC;
						else if ( name == "B3") no = MSGNO_BLOCK_DN_BGR_RST_ACC;
						else if ( name == "B4") no = MSGNO_BLOCK_UP_BCR_RST_ACC;
						else if ( name == "B11") no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
						else if ( name == "B12") no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
						else if ( name == "B13") no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
						else if ( name == "B14") no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
					}
				}
				else if ( dl == BLKRST_CMD_DEC )
				{
					if ( name == "B1" ) no = MSGNO_BLOCK_UP_BGR_RST_DEC;
					else if ( name == "B2") no = MSGNO_BLOCK_DN_BCR_RST_DEC;
					else if ( name == "B3") no = MSGNO_BLOCK_DN_BGR_RST_DEC;
					else if ( name == "B4") no = MSGNO_BLOCK_UP_BCR_RST_DEC;
					else if ( name == "B11") no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
					else if ( name == "B12") no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
					else if ( name == "B13") no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
					else if ( name == "B14") no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
				}
			}
			else
			{
				if ( dl == BLKRST_CMD_REQ )
				{
					if ( name == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_REQ;
					else if ( name == "B2") no = MSGNO_BLOCK_UP_BCR_RST_REQ;
					else if ( name == "B3") no = MSGNO_BLOCK_UP_BGR_RST_REQ;
					else if ( name == "B4") no = MSGNO_BLOCK_DN_BCR_RST_REQ;
					else if ( name == "B11") no = MSGNO_BLOCK_DN2_BGR_RST_REQ;
					else if ( name == "B12") no = MSGNO_BLOCK_UP2_BCR_RST_REQ;
					else if ( name == "B13") no = MSGNO_BLOCK_UP2_BGR_RST_REQ;
					else if ( name == "B14") no = MSGNO_BLOCK_DN2_BCR_RST_REQ;
				}
				else if ( dl == BLKRST_CMD_ACC )
				{
					if(pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13" || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH") == NULL)
					{
						if ( name == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC_SWEEP;
						else if ( name == "B2") no = MSGNO_BLOCK_UP_BCR_RST_ACC_SWEEP;
						else if ( name == "B3") no = MSGNO_BLOCK_UP_BGR_RST_ACC_SWEEP;
						else if ( name == "B4") no = MSGNO_BLOCK_DN_BCR_RST_ACC_SWEEP;
						else if ( name == "B11") no = MSGNO_BLOCK_DN2_BGR_RST_ACC_SWEEP;
						else if ( name == "B12") no = MSGNO_BLOCK_UP2_BCR_RST_ACC_SWEEP;
						else if ( name == "B13") no = MSGNO_BLOCK_UP2_BGR_RST_ACC_SWEEP;
						else if ( name == "B14") no = MSGNO_BLOCK_DN2_BCR_RST_ACC_SWEEP;
					}
					else
					{
						if ( name == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_ACC;
						else if ( name == "B2") no = MSGNO_BLOCK_UP_BCR_RST_ACC;
						else if ( name == "B3") no = MSGNO_BLOCK_UP_BGR_RST_ACC;
						else if ( name == "B4") no = MSGNO_BLOCK_DN_BCR_RST_ACC;
						else if ( name == "B11") no = MSGNO_BLOCK_DN2_BGR_RST_ACC;
						else if ( name == "B12") no = MSGNO_BLOCK_UP2_BCR_RST_ACC;
						else if ( name == "B13") no = MSGNO_BLOCK_UP2_BGR_RST_ACC;
						else if ( name == "B14") no = MSGNO_BLOCK_DN2_BCR_RST_ACC;
					}
				}
				else if ( dl == BLKRST_CMD_DEC )
				{
					if ( name == "B1" ) no = MSGNO_BLOCK_DN_BGR_RST_DEC;
					else if ( name == "B2") no = MSGNO_BLOCK_UP_BCR_RST_DEC;
					else if ( name == "B3") no = MSGNO_BLOCK_UP_BGR_RST_DEC;
					else if ( name == "B4") no = MSGNO_BLOCK_DN_BCR_RST_DEC;
					else if ( name == "B11") no = MSGNO_BLOCK_DN2_BGR_RST_DEC;
					else if ( name == "B12") no = MSGNO_BLOCK_UP2_BCR_RST_DEC;
					else if ( name == "B13") no = MSGNO_BLOCK_UP2_BGR_RST_DEC;
					else if ( name == "B14") no = MSGNO_BLOCK_DN2_BCR_RST_DEC;
				}
			}
			name = "";
		}
		break;
	case WS_OPMSG_CBBOP:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSignal)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SYSTEM;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+(dl & 0x7f)-1].m_strName;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( name == "B1" ) no = MSGNO_MSG_UTG_CBB;
				else if ( name == "B2") no = MSGNO_MSG_DTC_CBB;
				else if ( name == "B3") no = MSGNO_MSG_DTG_CBB;
				else if ( name == "B4") no = MSGNO_MSG_UTC_CBB;
				else if ( name == "B11") no = MSGNO_MSG_UTG2_CBB;
				else if ( name == "B12") no = MSGNO_MSG_DTC2_CBB;
			}
			else
			{
				if ( name == "B1" ) no = MSGNO_MSG_DTG_CBB;
				else if ( name == "B2") no = MSGNO_MSG_UTC_CBB;
				else if ( name == "B3") no = MSGNO_MSG_UTG_CBB;
				else if ( name == "B4") no = MSGNO_MSG_DTC_CBB;
				else if ( name == "B11") no = MSGNO_MSG_DTG2_CBB;
				else if ( name == "B12") no = MSGNO_MSG_UTC2_CBB;
			}
			name = "";
		}
		break;
	case WS_OPMSG_CAON:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSignal)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SYSTEM;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+(dl & 0x7f)-1].m_strName;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( name == "B1" ) no = MSGNO_MSG_UTG_CA;
				else if ( name == "B3") no = MSGNO_MSG_DTG_CA;
				else if ( name == "B11") no = MSGNO_MSG_UTG2_CA;
			}
			else
			{
				if ( name == "B1" ) no = MSGNO_MSG_DTG_CA;
				else if ( name == "B3") no = MSGNO_MSG_UTG_CA;
				else if ( name == "B11") no = MSGNO_MSG_DTG2_CA;
			}
			name = "";
		}
		break;
	case WS_OPMSG_LCROP:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSignal)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SYSTEM;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+(dl & 0x7f)-1].m_strName;
			if (pApp->m_bLeftSideIsUpDirection == TRUE)
			{
				if ( name == "B1" ) no = MSGNO_MSG_UTG_LCR;
				else if ( name == "B2") no = MSGNO_MSG_DTC_LCR;
				else if ( name == "B3") no = MSGNO_MSG_DTG_LCR;
				else if ( name == "B4") no = MSGNO_MSG_UTC_LCR;
				else if ( name == "B11") no = MSGNO_MSG_UTG2_LCR;
				else if ( name == "B12") no = MSGNO_MSG_DTC2_LCR;
			}
			else
			{
				if ( name == "B1" ) no = MSGNO_MSG_DTG_LCR;
				else if ( name == "B2") no = MSGNO_MSG_UTC_LCR;
				else if ( name == "B3") no = MSGNO_MSG_UTG_LCR;
				else if ( name == "B4") no = MSGNO_MSG_DTC_LCR;
				else if ( name == "B11") no = MSGNO_MSG_DTG2_LCR;
				else if ( name == "B12") no = MSGNO_MSG_UTC2_LCR;
			}
			name = "";
		}
		break;
	case WS_OPMSG_UTG_CA:
		 no = MSGNO_MSG_UTG_CA;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_UTG_LCR:
		 no = MSGNO_MSG_UTG_LCR;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_UTG_CBB:
		 no = MSGNO_MSG_UTG_CBB;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_UTC_CA:
// 		 no = MSGNO_MSG_UTC_CA;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_UTC_LCR:
		 no = MSGNO_MSG_UTC_LCR;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_UTC_CBB:
		 no = MSGNO_MSG_UTC_CBB;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
	case WS_OPMSG_DTG_CA:
		 no = MSGNO_MSG_DTG_CA;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_DTG_LCR:
		 no = MSGNO_MSG_DTG_LCR;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_DTG_CBB:
		 no = MSGNO_MSG_DTG_CBB;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
 	case WS_OPMSG_DTC_CA:
// 		 no = MSGNO_MSG_DTC_CA;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_DTC_LCR:
		 no = MSGNO_MSG_DTC_LCR;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_DTC_CBB:
		 no = MSGNO_MSG_DTC_CBB;
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;
	case WS_OPMSG_CAACK:
		if (((dl & 0x7f) < 1) || ((dl & 0x7f) > pApp->nMaxSignal)) {
			cmd = 0;
		}
		else {
			type = MSG_TYPE_SYSTEM;
			name = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+(dl & 0x7f)-1].m_strName;
			 no = MSGNO_MSG_CAACK;	// 916
			 type = MSG_TYPE_SYSTEM;
			 if (pApp->m_bLeftSideIsUpDirection == TRUE)
			 {
				 if ( name == "B2") name = "Down direction TCB";
				 else if ( name == "B4") name = "Up direction TCB";
				 else if ( name == "B12") name = "Down2 direction TCB";
			 }
			 else
			 {
				 if ( name == "B2" ) name = "Up direction TCB";
				 else if ( name == "B4") name = "Down direction TCB";
				 else if ( name == "B12") name = "Up2 direction TCB";
			 }
		}
// 		 name = "";
		 break;
	case WS_OPMSG_CBBOPEMER:
		 no = MSGNO_MSG_CAEMER;	// 916
		 type = MSG_TYPE_SYSTEM;
		 name = "";
		 break;		 
	case WS_OPMSG_PAS:				// PAS 버튼 제어
	case WS_OPMSG_PAS_IN:				// PAS 버튼 제어
	case WS_OPMSG_PAS_OUT:				// PAS 버튼 제어
		 type = MSG_TYPE_SYSTEM;
		 no = MSGNO_SYS_OP_PAS;
		 name = "";
		 break;
	case WS_OPMSG_LOS:				// LOS 버튼 제어
	case WS_OPMSG_LOS_ON:				// LOS 버튼 제어
	case WS_OPMSG_LOS_OFF:				// LOS 버튼 제어
		pTrackInfo =(ScrInfoTrack *)&pNew[pApp->m_pEipEmu->m_TableBase.InfoTrack.nStart + (dl-1) * SIZE_INFO_TRACK];
		type = MSG_TYPE_TRACK;
		if ( pTrackInfo->INHIBIT ) {
			if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13")
			{
				no = MSGNO_SYS_OP_LOS;
			}
			else
			{
				no = MSGNO_SYS_OP_COS_OFF;
			}
		} else {
			if ( pApp->m_VerifyObjectForMessage.strProjectName == "StandardOf13")
			{
				no = MSGNO_SYS_OP_LOS;
			}
			else
			{
				no = MSGNO_SYS_OP_COS_ON;
			}
		}
		name = pApp->Info_pScrMessageInfo[dl-1].m_strName;;
		break;
	case WS_OPMSG_OSS:				// OSS 버튼 제어 
		type = MSG_TYPE_SIGNAL;
        pSigInfo = (ScrInfoSignal *)&pNew[pApp->m_pEipEmu->m_TableBase.InfoSignal.nStart + (dl-1) * SIZE_INFO_SIGNAL];
		if ( pSigInfo->OSS ) /* 기존 pSigInfo->OSS 조건을 변경, 20130313 ibjang revise  */
		{
			no = MSGNO_MSG_OSSBUTTON_SET_R;
		} 
		else 
		{
			no = MSGNO_MSG_OSSBUTTON_SET_N;
		}
		name  = pApp->Info_pScrMessageInfo[pApp->nMaxTrack+dl-1].m_strName;
		break;
	// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
	case WS_OPMSG_BUTTON:				// 시스템 버튼 제어
		type = MSG_TYPE_SYSTEM;
		switch ( dl ) {
		case BtnPas:
			no = MSGNO_SYS_OP_PAS;
			name = "";
			break;
		case BtnLocalMode:
			no = MSGNO_SYS_OP_MODE_LOCAL;
			name = "";
			break;
		case BtnCTCMode:
			no = MSGNO_SYS_OP_MODE_CTC;
			name = "";
			break;
		case BtnDay:
			if ( pnewVar->bN2 ) no = MSGNO_SYS_OP_NIGHT;
			else no = MSGNO_SYS_OP_DAY;
			name = "";
			break;
		case BtnSls:	// event
			if ( pnewVar->bN3 ) no = MSGNO_SYS_OP_SLS_OFF;
			else no = MSGNO_SYS_OP_SLS_ON;
			name = "";
			break;
		case BtnAtb:
			no = MSGNO_SYS_OP_ATB;
			name = "";
			break;
		case BtnS:			// event
			no = MSGNO_SYS_OP_SBUTTON;
			name = "";
			break;
		case BtnP:			// event
			no = MSGNO_SYS_OP_PBUTTON;
			name = "";
			break;
		case BtnFuse:		// event
			no = MSGNO_SYS_OP_FUSEBUTTON;
			name = "";
			break;
		case BtnComm:		// event
			no = MSGNO_SYS_OP_COMMBUTTON;
			name = "";
			break;
		case BtnSysRun:		// 시스템 기동(cmd, ~cmd, cmd)
			no = MSGNO_MSG_CTRLSYSRUN;		// 915
			type = MSG_TYPE_SYSTEM;
			name = "";
			break;
		case BtnSys1:		// 시스템 기동(cmd, ~cmd, cmd)
			no = MSGNO_MSG_CTRLSYS1;		// 915
			type = MSG_TYPE_SYSTEM;
			name = "";
			break;
		case BtnSys2:		// 시스템 기동(cmd, ~cmd, cmd)
			no = MSGNO_MSG_CTRLSYS2;		// 915
			type = MSG_TYPE_SYSTEM;
			name = "";
			break;
		case BtnPower:		// 시스템 기동(cmd, ~cmd, cmd)
			no = MSGNO_MSG_POWERBUTTON;		// 915
			type = MSG_TYPE_SYSTEM;
			name = "";
			break;
		case BtnGen:		// 시스템 기동(cmd, ~cmd, cmd)
			no = MSGNO_MSG_GENBUTTON;		// 915
			name = "";
			break;
		case BtnUps2:
			no = MSGNO_MSG_OP_UPS2BUTTON;		// 915
			name = "";
			break;
		case BtnGround:
			no = MSGNO_MSG_GRDBUTTON;		// 915
			name = "";
			break;
		case BtnUps1:		// event
			no = MSGNO_SYS_OP_UPS1BUTTON;
			name = "";
			break;
		case BtnBattery:		// event
			no = MSGNO_SYS_OP_BATTERYBUTTON;
			name = "";
			break;
		case BtnAck:
			no = MSGNO_SYS_OP_ACKBUTTON;
			name = "";
			break;
		case BtnPDB:
			no = MSGNO_SYS_OP_PDBBUTTON;
			name = "";
			break;
		case BtnGnd:
			no = MSGNO_SYS_OP_GNDBUTTON;
			name = "";
			break;
		case BtnClose:
			no = MSGNO_SYS_OP_CLOSEBUTTON;
			name = "";
			break;
		case BtnReserved:
			no = MSGNO_SYS_OP_REVERSEBUTTON;
			name = "";
			break;
		case BtnLoop1:
			no = MSGNO_SYS_OP_LOOP1BUTTON;
			name = "";
			break;
		case BtnLoop2:
			no = MSGNO_SYS_OP_LOOP2BUTTON;
			name = "";
			break;
		case BtnAPKup:
			if ( pnewVar->bPKeyContN )
			{
				if ( pApp->m_VerifyObjectForMessage.iNumberOfEPK > 7 ) no = MSGNO_SYS_OP_APK_H_BUTTON_OFF;
				else no = MSGNO_SYS_OP_APKUPBUTTON_R;
			}
			else
			{
				if (pApp->m_VerifyObjectForMessage.iNumberOfEPK > 7 ) no = MSGNO_SYS_OP_APK_H_BUTTON_ON;
				else no = MSGNO_SYS_OP_APKUPBUTTON_L;
			}
			name = "";
			break;
		case BtnAPKdn:
			if ( pnewVar->bPKeyContS )
			{
				if ( pApp->m_VerifyObjectForMessage.iNumberOfEPK > 8 ) no = MSGNO_SYS_OP_APK_I_BUTTON_OFF;
				else no = MSGNO_SYS_OP_APKDNBUTTON_R;
			}
			else
			{
				if ( pApp->m_VerifyObjectForMessage.iNumberOfEPK > 8 ) no = MSGNO_SYS_OP_APK_I_BUTTON_ON;
				else no = MSGNO_SYS_OP_APKDNBUTTON_L;
			}
			name = "";
			break;
		/* 20130313 ibjang add, A~G EPK OPERATROR 로그 출력 생성 */
		case BtnAPKA:
			if ( pnewVar->bPKeyContA) no = MSGNO_SYS_OP_APK_A_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_A_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKB:
			if ( pnewVar->bPKeyContB) no = MSGNO_SYS_OP_APK_B_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_B_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKC:
			if ( pnewVar->bPKeyContC) no = MSGNO_SYS_OP_APK_C_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_C_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKD:
			if ( pnewVar->bPKeyContD) no = MSGNO_SYS_OP_APK_D_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_D_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKE:
			if ( pnewVar->bPKeyContE) no = MSGNO_SYS_OP_APK_E_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_E_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKF:
			if ( pnewVar->bPKeyContF) no = MSGNO_SYS_OP_APK_F_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_F_BUTTON_OFF;
			name = "";
			break;
		case BtnAPKG:
			if ( pnewVar->bPKeyContG) no = MSGNO_SYS_OP_APK_G_BUTTON_ON;
			else no = MSGNO_SYS_OP_APK_G_BUTTON_OFF;
			name = "";
			break;	
		/* 20130313 ibjang add, A~G EPK OPERATROR 로그 출력 생성 */

		default:
			cmd = 0;
		}
		dh = 0;
		break;
	case WS_OPMSG_MARKINGTRACK:					// 사용중지 설정 
		if ((dl < 1) || (dl > pApp->nMaxTrack)) {	//  dl:track ID
			cmd = 0;
		}
		else {
			no = ( dh & TRACKLOCK_USING ) ? MSGNO_TRACK_OP_LOCK : MSGNO_TRACK_OP_FREE;
			type = MSG_TYPE_TRACK;
			name = pApp->Info_pScrMessageInfo[dl-1].m_strName;
		}
		break;
	case 0:
	default:
		cmd = 0;
		return bAlter;
	}
	if ( cmd ) {
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType = type;
		pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->Mode = ( bCtcMode ) ? 0x80 : 0x00;
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;

		int n = name.GetLength();
		if ( n ) {
			if ( n >= MSG_MAX_NAME_LENTH ) n = MSG_MAX_NAME_LENTH - 1;
			memcpy(pEvent->name, LPCTSTR(name), n);
		}
		pEvent->name[n] = 0x00;

		pEvent->item  = ID;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		if (no == MSGNO_MSG_CTRLSYSRESET) // 916
			bAlter = -1;
		else 
			bAlter = 1;
	}
	return bAlter;
}

int  CLOGMessage::GetSystemMessage(CObList &objList, void *pOld, void *pCur,
								   EventKeyType &EventKey, BYTE &errorMode, BOOL bDownMsg)
{

	// 시스템 메세지 추출 및 가공 
	int  no;
	int  nAlter = 0;
	DataHeaderType		*poldHead	= (DataHeaderType *)pOld;
	DataHeaderType		*pcurHead	= (DataHeaderType *)pCur;

	SystemStatusType	*poldVar	= &poldHead->nSysVar;
	SystemStatusType	*pnewVar	= &pcurHead->nSysVar;
	SystemStatusType	xchgSysVar;

	/* 20130313 ibjang add, APK STATUS 추가 */
	SystemStatusTypeExt		*poldSysVarExt = &poldHead->nSysVarExt;
	SystemStatusTypeExt		*pnewSysVarExt = &pcurHead->nSysVarExt;
	SystemStatusTypeExt		xchgSysVarExt;
	/* 20130313 ibjang add, APK STATUS 추가 */

	CommStatusType		xchgCommVar;
	UnitStatusType		xchgUnitVar;
	MessageEvent *pEvent;

	// 변화된 부분 확인
	xchgSysVar.pVar[0] = poldVar->pVar[0] ^ pnewVar->pVar[0];
	xchgSysVar.pVar[1] = poldVar->pVar[1] ^ pnewVar->pVar[1];
	xchgSysVar.pVar[2] = poldVar->pVar[2] ^ pnewVar->pVar[2];
	xchgSysVar.pVar[3] = poldVar->pVar[3] ^ pnewVar->pVar[3];
	xchgSysVar.pVar[4] = poldVar->pVar[4] ^ pnewVar->pVar[4];
	xchgSysVar.pVar[5] = poldVar->pVar[5] ^ pnewVar->pVar[5];
	xchgSysVar.pVar[6] = poldVar->pVar[6] ^ pnewVar->pVar[6];
	xchgSysVar.pVar[7] = poldVar->pVar[7] ^ pnewVar->pVar[7];

	xchgCommVar.pVar[0] = poldHead->CommState.pVar[0] ^ pcurHead->CommState.pVar[0];
	xchgUnitVar.pVar[0]	= poldHead->UnitState.pVar[0] ^ pcurHead->UnitState.pVar[0];

	xchgSysVarExt.pVar[0] = poldHead->nSysVarExt.pVar[0] ^ pcurHead->nSysVarExt.pVar[0];
	xchgSysVarExt.pVar[1] = poldHead->nSysVarExt.pVar[1] ^ pcurHead->nSysVarExt.pVar[1];
	xchgSysVarExt.pVar[2] = poldHead->nSysVarExt.pVar[2] ^ pcurHead->nSysVarExt.pVar[2];
	xchgSysVarExt.pVar[3] = poldHead->nSysVarExt.pVar[3] ^ pcurHead->nSysVarExt.pVar[3];

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//  UinitStatus Type
	//

	// 시스템 RUN 검사
	if ( !xchgUnitVar.bSysRun || (xchgUnitVar.bSysRun && pcurHead->UnitState.bSysRun) ) {
		nAlter = -1;
	}

	// 시스템 DOWN Message 전체 시스템의 RUN / DOWN 검사 LSM 화면의 RUN 버튼 상태 
	if ( xchgUnitVar.bSysRun ) {
		no = ( pcurHead->UnitState.bSysRun ) ? MSGNO_ERR_SYSTEM_RUN : MSGNO_ERR_SYSTEM_DOWN;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	// 시스템 DOWN Message 전체 시스템의 RUN / DOWN 검사 LSM 화면의 RUN 버튼 상태 
	if ( xchgUnitVar.bSysFail ) {
		no = ( pcurHead->UnitState.bSysFail ) ? MSGNO_ERR_SYSTEM_RECOVERY : MSGNO_ERR_SYSTEM_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	// Sys 1 StartUp 상태 확인 
	if ( xchgUnitVar.bSys1StartUp ) {
		if ( pcurHead->UnitState.bSys1StartUp ) {
			nAlter |= 1;
			pEvent = new MessageEvent;
			pEvent->msgno = MSGNO_SYS_CBI1_STARTUP;
			pEvent->Type.nType = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( MSGNO_SYS_CBI1_STARTUP );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}

	// Sys 2 StartUp 상태 확인 
	if ( xchgUnitVar.bSys2StartUp ) {
		if ( pcurHead->UnitState.bSys2StartUp ) {
			nAlter |= 1;
			pEvent = new MessageEvent;
			pEvent->msgno = MSGNO_SYS_CBI2_STARTUP;
			pEvent->Type.nType = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( MSGNO_SYS_CBI2_STARTUP );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter = -2;
		}
	}

	// bSubRequest 
	if ( xchgUnitVar.bSubRequest ) {
		if ( pcurHead->UnitState.bSubRequest ) {
			nAlter |= 1;
			pEvent = new MessageEvent;
			pEvent->msgno = MSGNO_SYS_CBI_CHANGE_REQ;
			pEvent->Type.nType = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode = pApp->m_pEIPMsgList->GetMode( MSGNO_SYS_CBI_CHANGE_REQ );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter = -2;
		}
	}

	// bActiveOn1 
	if ( xchgUnitVar.bActiveOn1 ) {
		no = ( pcurHead->UnitState.bActiveOn1 ) ? MSGNO_SYS_CBI1_SUB : MSGNO_SYS_CBI1_MAIN;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// bActiveOn1 
	if ( xchgUnitVar.bActiveOn2 ) {
		no = ( pcurHead->UnitState.bActiveOn2 ) ? MSGNO_SYS_CBI2_SUB : MSGNO_SYS_CBI2_MAIN;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bComFail 
	if ( xchgUnitVar.bComFail ) {
		no = ( pcurHead->UnitState.bComFail ) ? MSGNO_SYS_COMM_ALL_RECOVERY : MSGNO_SYS_COMM_ALL_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
//////////////////////////////////////////////////////////////////////////
//      2013.01.28  UserID 변경.>> 별도로..
/*	CString strUser1;
	CString strUser2;
	strUser1 = poldHead->UserID;
	strUser2 = pcurHead->UserID;
	if ( strUser1 != strUser2 ) {
		no = MSGNO_SYS_USERLOGIN;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		memcpy( &pEvent->name, &pcurHead->UserID , 6);
		pEvent->name[5] = '\0';
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;	
	}
*/
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//  SystemStatus Type
	//
	
	// Mode LOCAL/CTC 
	if ( xchgSysVar.bMode ) {
		no = ( pnewVar->bMode ) ? MSGNO_SYS_MODE_CTC : MSGNO_SYS_MODE_LOCAL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// CTC Request 
	if ( xchgSysVar.bCTCRequest ) {
		if ( pnewVar->bCTCRequest )
		{
			no = MSGNO_SYS_OP_MODE_CTC_REQUEST;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// PAS KEY 에따른 시스템 잠김 상태 확인 
	if ( xchgSysVar.bN1 ) {
		no = ( pnewVar->bN1 ) ? MSGNO_SYS_PASLOCK : MSGNO_SYS_PASUNLOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// Day/Night 
	if ( xchgSysVar.bN2 ) {
		no = ( pnewVar->bN2 ) ? MSGNO_SYS_INIGHT : MSGNO_SYS_IDAY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// SLS 버튼  
	if ( xchgSysVar.bN3 ) {
		no = ( pnewVar->bN3 ) ? MSGNO_SYS_SLS_ON : MSGNO_SYS_SLS_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// InterNal B24V Power  
	if ( xchgSysVar.bB24PWR ) {
		no = ( pnewVar->bB24PWR ) ? MSGNO_SYS_B24PWR_RECOVERY : MSGNO_SYS_B24PWR_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// GROUND  
	if ( xchgSysVar.bGround ) {
		if ( pnewVar->bGround == 0) {
			no = MSGNO_SYS_GND_RECOVERY;
		} else {
			no = MSGNO_SYS_GND_FAIL;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// UPS1  
	if ( xchgSysVar.bUPS1 ) {
		no = ( pnewVar->bUPS1 ) ? MSGNO_SYS_UPS1_RECOVERY : MSGNO_SYS_UPS1_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// UPS2  
	if ( xchgSysVar.bUPS2 ) {
		no = ( pnewVar->bUPS2 ) ? MSGNO_SYS_UPS2_RECOVERY : MSGNO_SYS_UPS2_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
/*
	// bGenLowFuel  
	if ( xchgSysVar.bGenLowFuel ) {
		no = ( pnewVar->bGenLowFuel ) ? MSGNO_SYS_GEN_FULLFUEL : MSGNO_SYS_GEN_LOWFUEL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
*/ //LowFuel을 LCC제어권으로 변경.
	// bGenLowFuel  
	if ( xchgSysVar.bGen ) {
		no = ( pnewVar->bGen ) ? MSGNO_SYS_GEN_RECOVERY : MSGNO_SYS_GEN_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bCharge  
	if ( xchgSysVar.bCharge ) {
		no = ( pnewVar->bCharge ) ? MSGNO_SYS_CHARGE_RECOVERY : MSGNO_SYS_CHARGE_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bPointFail  
	if ( xchgSysVar.bPointFail ) {
		no = ( pnewVar->bPointFail ) ? MSGNO_SYS_POINT_FAIL : MSGNO_SYS_POINT_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SWITCH;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	// bSound1  
	if ( xchgSysVarExt.bSound1 ) {
		no = ( pcurHead->nSysVarExt.bSound1 ) ? MSGNO_SYS_ALARM1_OCCUR : MSGNO_SYS_ALARM1_CLEAR;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bSound2  
	if ( xchgSysVarExt.bSound2 ) {
		no = ( pcurHead->nSysVarExt.bSound2 ) ? MSGNO_SYS_ALARM2_OCCUR : MSGNO_SYS_ALARM2_CLEAR;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bSound3  
	if ( xchgSysVarExt.bSound3 ) {
		no = ( pcurHead->nSysVarExt.bSound3 ) ? MSGNO_SYS_ALARM3_OCCUR : MSGNO_SYS_ALARM3_CLEAR;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// bSound4  
	if ( xchgSysVarExt.bSound4 ) {
		no = ( pcurHead->nSysVarExt.bSound4 ) ? MSGNO_SYS_ALARM4_OCCUR : MSGNO_SYS_ALARM4_CLEAR;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
// 	// bSound5  
// 	if ( xchgSysVarExt.bSound5 ) {
// 		no = ( pcurHead->nSysVarExt.bSound5 ) ? MSGNO_SYS_ALARM5_OCCUR : MSGNO_SYS_ALARM5_CLEAR;
// 		pEvent = new MessageEvent;
// 		pEvent->msgno = no;
// 		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
// 		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
// 		pEvent->time = *(EventKey.pTime);
// 		pEvent->hsec = EventKey.hsec;
// 		pEvent->evn  = EventKey.EvnLoc;
// 		EventKey.EvnLoc.nLoc++;
// 		strcpy(pEvent->name, "");
// 		pEvent->item  = 0;
// 		objList.AddTail( (CObject*)pEvent );
// 		errorMode |= pEvent->Type.nMode & 0x0f;
// 		//nAlter = -2;
// 		nAlter |= 1;
// 	}
// 	
// 	// bSound4  
// 	if ( xchgSysVarExt.bSound6 ) {
// 		no = ( pcurHead->nSysVarExt.bSound6 ) ? MSGNO_SYS_ALARM6_OCCUR : MSGNO_SYS_ALARM6_CLEAR;
// 		pEvent = new MessageEvent;
// 		pEvent->msgno = no;
// 		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
// 		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
// 		pEvent->time = *(EventKey.pTime);
// 		pEvent->hsec = EventKey.hsec;
// 		pEvent->evn  = EventKey.EvnLoc;
// 		EventKey.EvnLoc.nLoc++;
// 		strcpy(pEvent->name, "");
// 		pEvent->item  = 0;
// 		objList.AddTail( (CObject*)pEvent );
// 		errorMode |= pEvent->Type.nMode & 0x0f;
// 		//nAlter = -2;
// 		nAlter |= 1;
// 	}
// 	
// 	// bSound4  
// 	if ( xchgSysVarExt.bSound7 ) {
// 		no = ( pcurHead->nSysVarExt.bSound7 ) ? MSGNO_SYS_ALARM7_OCCUR : MSGNO_SYS_ALARM7_CLEAR;
// 		pEvent = new MessageEvent;
// 		pEvent->msgno = no;
// 		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
// 		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
// 		pEvent->time = *(EventKey.pTime);
// 		pEvent->hsec = EventKey.hsec;
// 		pEvent->evn  = EventKey.EvnLoc;
// 		EventKey.EvnLoc.nLoc++;
// 		strcpy(pEvent->name, "");
// 		pEvent->item  = 0;
// 		objList.AddTail( (CObject*)pEvent );
// 		errorMode |= pEvent->Type.nMode & 0x0f;
// 		//nAlter = -2;
// 		nAlter |= 1;
// 	}

	// bPowerFail  
	if ( xchgSysVar.bPowerFail ) {
		no = ( pnewVar->bPowerFail ) ? MSGNO_SYS_POWER_RECOVERY : MSGNO_SYS_POWER_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bIActiveOn  
	if ( xchgSysVar.bIActiveOn1 ) {
		no = ( pnewVar->bIActiveOn1 ) ? MSGNO_ERR_CBI1_MAIN : MSGNO_ERR_CBI1_SUB;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bIActiveOn  
	if ( xchgSysVar.bIActiveOn2 ) {
		no = ( pnewVar->bIActiveOn2 ) ? MSGNO_ERR_CBI2_MAIN : MSGNO_ERR_CBI2_SUB;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bIVPOR1  
	if ( xchgSysVar.bIVPOR1 ) {
		no = ( pnewVar->bIVPOR1 ) ? MSGNO_SYS_IVPOR1_ON : MSGNO_SYS_IVOPR1_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bIVPOR2  
	if ( xchgSysVar.bIVPOR2 ) {
		no = ( pnewVar->bIVPOR2 ) ? MSGNO_SYS_IVPOR2_ON : MSGNO_SYS_IVPOR2_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bVPOR1  
	if ( xchgSysVar.bVPOR1 ) {
		no = ( pnewVar->bIVPOR1 ) ? MSGNO_SYS_OVPOR1_ON : MSGNO_SYS_OVOPR1_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bVPOR2  
	if ( xchgSysVar.bVPOR2 ) {
		no = ( pnewVar->bIVPOR2 ) ? MSGNO_SYS_OVPOR2_ON : MSGNO_SYS_OVPOR2_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bLcc1Online  
//	if ( xchgSysVar.bLcc1Online ) {
//		no = ( pnewVar->bLcc1Online ) ? MSGNO_SYS_LCC1_ONLINE : MSGNO_SYS_LCC1_OFFLINE;
	if ( xchgCommVar.bLCCCom1Fail ) {	// COM 1 Fail or Recovery
		no = ( pcurHead->CommState.bLCCCom1Fail ) ?  MSGNO_SYS_LCC1_OFFLINE : MSGNO_SYS_LCC1_ONLINE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bLcc1Online  
//	if ( xchgSysVar.bLcc2Online ) {
//		no = ( pnewVar->bLcc2Online ) ? MSGNO_SYS_LCC2_ONLINE : MSGNO_SYS_LCC2_OFFLINE;
	if ( xchgCommVar.bLCCCom2Fail ) {	// COM 1 Fail or Recovery
		no = ( pcurHead->CommState.bLCCCom2Fail ) ?  MSGNO_SYS_LCC2_OFFLINE : MSGNO_SYS_LCC2_ONLINE ;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bStartUp  
	if ( xchgSysVar.bStartUp ) {
		no = ( pnewVar->bStartUp ) ? MSGNO_SYS_STARTUP_ON : MSGNO_SYS_STARTUP_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bGenRun  
	if ( xchgSysVar.bGenRun ) {
		no = ( pnewVar->bGenRun ) ? MSGNO_SYS_GEN_RUN : MSGNO_SYS_GEN_STOP;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bATB  
	if ( xchgSysVar.bATB ) {
		no = ( pnewVar->bATB ) ? MSGNO_SYS_ATB_ON : MSGNO_SYS_ATB_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bExtPowerS  
	if ( xchgSysVar.bExtPowerS ) {
		if ( pApp->m_SystemValue.nExternPower == 2 ) {
			no = ( pnewVar->bExtPowerS ) ? MSGNO_SYS_SOUTHPOWER_RECOVERY : MSGNO_SYS_SOUTHPOWER_FAIL;
		} else {
			no = ( pnewVar->bExtPowerS ) ? MSGNO_SYS_EXTERNPOWER_RECOVERY : MSGNO_SYS_EXTERNPOWER_FAIL;		
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bUpModem  
	if ( xchgSysVar.bUpModem ) {
		if (pApp->m_bLeftSideIsUpDirection == 1) // 왼쪽이 UP
			no = ( pnewVar->bUpModem ) ? MSGNO_SYS_DNMODEM_RECOVERY:MSGNO_SYS_DNMODEM_FAIL;
		else
			no = ( pnewVar->bUpModem ) ? MSGNO_SYS_UPMODEM_RECOVERY:MSGNO_SYS_UPMODEM_FAIL;

		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bDnModem  
	if ( xchgSysVar.bDnModem ) {
		if (pApp->m_bLeftSideIsUpDirection == 1)
			no = ( pnewVar->bDnModem ) ? MSGNO_SYS_UPMODEM_RECOVERY:MSGNO_SYS_UPMODEM_FAIL;
		else
			no = ( pnewVar->bDnModem ) ? MSGNO_SYS_DNMODEM_RECOVERY:MSGNO_SYS_DNMODEM_FAIL;

		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bFan1  
	if ( xchgSysVar.bFan1 ) {
		no = ( pnewVar->bFan1 ) ? MSGNO_SYS_FAN1_RECOVERY : MSGNO_SYS_FAN1_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bFan2  
	if ( xchgSysVar.bFan2 ) {
		no = ( pnewVar->bFan2 ) ? MSGNO_SYS_FAN2_RECOVERY : MSGNO_SYS_FAN2_FAIL;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bActive1  
	if ( xchgSysVar.bActive1 ) {
		no = MSGNO_SYS_ACTIVE1_REQ;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bActive2  
	if ( xchgSysVar.bActive2 ) {
		no = MSGNO_SYS_ACTIVE2_REQ;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bFuse1  
	if ( xchgSysVar.bFuse1 ) {
		no = ( pnewVar->bFuse1 ) ? MSGNO_SYS_FUSE1_FAIL : MSGNO_SYS_FUSE1_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bFan2  
	if ( xchgSysVar.bFuse2 ) {
		no = ( pnewVar->bFuse2 ) ? MSGNO_SYS_FUSE2_FAIL : MSGNO_SYS_FUSE2_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bFuse3  
	if ( xchgSysVarExt.bFuse3 ) {
		no = ( pcurHead->nSysVarExt.bFuse3 ) ? MSGNO_SYS_FUSE3_FAIL : MSGNO_SYS_FUSE3_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// bFuse4  
	if ( xchgSysVarExt.bFuse4 ) {
		no = ( pcurHead->nSysVarExt.bFuse4 ) ? MSGNO_SYS_FUSE4_FAIL : MSGNO_SYS_FUSE4_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130311 ibjang add */
	if ( xchgSysVar.bFusePR )
	{
		no = (pnewVar->bFusePR) ? MSGNO_RELAYFUSE_FAIL : MSGNO_RELAYFUSE_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	/* 20130311 ibjang add */

	// bCharging  
	if ( xchgSysVar.bCharging ) {
		no = ( pnewVar->bCharging ) ? MSGNO_SYS_CHARGING_END : MSGNO_SYS_CHARGING_START;
		if ( pnewVar->bPowerFail ) {
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	// bLowVoltage  
	if ( xchgSysVar.bLowVoltage ) {
		if ( pnewVar->bLowVoltage == 1 )  no = MSGNO_SYS_LOWVOLT;
		else no = MSGNO_SYS_LOWVOLT_RECOVERY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bLowVoltage  
	if ( xchgSysVar.bExtPowerN ) {
		if ( pApp->m_SystemValue.nExternPower == 2 ) {
			no = ( pnewVar->bExtPowerN ) ? MSGNO_SYS_NORTHPOWER_RECOVERY : MSGNO_SYS_NORTHPOWER_FAIL;
		} else {
			no = ( pnewVar->bExtPowerN ) ? MSGNO_SYS_EXTERNPOWER_RECOVERY : MSGNO_SYS_EXTERNPOWER_FAIL;		
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bALR  //--- 20130314 message number 수정, ibjang revise
	if ( xchgSysVar.bALR ) {
		no = ( pnewVar->bALR ) ? MSGNO_SYS_ALR_PICKUP : MSGNO_SYS_ALR_DROP;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	// bTCS  
	if ( xchgSysVar.bTCS ) {
		no = ( pnewVar->bTCS ) ? MSGNO_SYS_TCS_ON : MSGNO_SYS_TCS_OFF;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyN  
	if ( xchgSysVar.bPKeyN && pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2) {
		if ( pApp->m_SystemValue.strStationFileName == "SYT" || pApp->m_SystemValue.strStationFileName == "AKA") {
			no = ( pnewVar->bPKeyN ) ? MSGNO_SYS_FAN3_RECOVERY : MSGNO_SYS_FAN3_FAIL;
		} else {
			no = ( pnewVar->bPKeyN ) ? MSGNO_NORTH_IPOINT_KEY_LOCK : MSGNO_NORTH_IPOINT_KEY_RELEASE;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyS  
	if ( xchgSysVar.bPKeyS && pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2) {
		if ( pApp->m_SystemValue.strStationFileName == "SYT" || pApp->m_SystemValue.strStationFileName == "AKA" ) {
			no = ( pnewVar->bPKeyS ) ? MSGNO_SYS_FAN4_RECOVERY : MSGNO_SYS_FAN4_FAIL;
		} else {
			no = ( pnewVar->bPKeyS ) ? MSGNO_SOUTH_IPOINT_KEY_LOCK : MSGNO_SOUTH_IPOINT_KEY_RELEASE;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContN  
	if ( xchgSysVar.bPKeyContN && pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2 ) {
		if ( pApp->m_SystemValue.strStationFileName == "SYT" || pApp->m_SystemValue.strStationFileName == "AKA") {
			no = ( pnewVar->bPKeyContN ) ? MSGNO_SYS_FUSE3_FAIL : MSGNO_SYS_FUSE3_RECOVERY;
		} else {
			no = ( pnewVar->bPKeyContN ) ? MSGNO_NORTH_OPOINT_KEY_RELEASE : MSGNO_NORTH_OPOINT_KEY_LOCK;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContN  
	if ( xchgSysVar.bPKeyContS && pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2 ) {
		if ( pApp->m_SystemValue.strStationFileName == "SYT" || pApp->m_SystemValue.strStationFileName == "AKA") {
			no = ( pnewVar->bPKeyContS ) ? MSGNO_SYS_FUSE4_FAIL : MSGNO_SYS_FUSE4_RECOVERY;
		} else {
			no = ( pnewVar->bPKeyContS ) ? MSGNO_SOUTH_OPOINT_KEY_RELEASE : MSGNO_SOUTH_OPOINT_KEY_LOCK;
		}
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContA  
	if ( xchgSysVar.bPKeyContA && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContA ) ? MSGNO_A_OPOINT_KEY_RELEASE : MSGNO_A_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContB  
	if ( xchgSysVar.bPKeyContB && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContB ) ? MSGNO_B_OPOINT_KEY_RELEASE : MSGNO_B_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContC  
	if ( xchgSysVar.bPKeyContC && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContC ) ? MSGNO_C_OPOINT_KEY_RELEASE : MSGNO_C_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContD  
	if ( xchgSysVar.bPKeyContD && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContD ) ? MSGNO_D_OPOINT_KEY_RELEASE : MSGNO_D_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContE  
	if ( xchgSysVar.bPKeyContE && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContE ) ? MSGNO_E_OPOINT_KEY_RELEASE : MSGNO_E_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContF  
	if ( xchgSysVar.bPKeyContF && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContF ) ? MSGNO_F_OPOINT_KEY_RELEASE : MSGNO_F_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	// bPKeyContG  
	if ( xchgSysVar.bPKeyContG && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 ) {
		no = ( pnewVar->bPKeyContG ) ? MSGNO_G_OPOINT_KEY_RELEASE : MSGNO_G_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// bPKeyContH  
	if ( xchgSysVar.bPKeyContN && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 7 ) {
		no = ( pnewVar->bPKeyContN ) ? MSGNO_H_OPOINT_KEY_RELEASE : MSGNO_H_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	// bPKeyContI  
	if ( xchgSysVar.bPKeyContS && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 8 ) {
		no = ( pnewVar->bPKeyContS ) ? MSGNO_I_OPOINT_KEY_RELEASE : MSGNO_I_OPOINT_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	/* 20130313 ibjang add, APK STATUS 추가 */
	if ( xchgSysVarExt.bPKeyA && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyA )
			no = MSGNO_A_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_A_IPOINT_KEY_LOCK;

		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyB && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyB )
			no = MSGNO_B_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_B_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyC && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyC )
			no = MSGNO_C_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_C_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyD && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyD )
			no = MSGNO_D_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_D_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyE && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyE )
			no = MSGNO_E_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_E_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyF && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyF )
			no = MSGNO_F_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_F_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, EPK 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bPKeyG && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 2 )
	{
		no = 0;
		if ( pnewSysVarExt->bPKeyG )
			no = MSGNO_G_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_G_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	if ( xchgSysVar.bPKeyN && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 7 )
	{
		no = 0;
		if ( pnewVar->bPKeyN )
			no = MSGNO_H_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_H_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}

	if ( xchgSysVar.bPKeyS && pApp->m_VerifyObjectForMessage.iNumberOfEPK > 8 )
	{
		no = 0;
		if ( pnewVar->bPKeyS )
			no = MSGNO_I_IPOINT_KEY_RELEASE;
		else
			no = MSGNO_I_IPOINT_KEY_LOCK;
		
		if (no != 0)
		{
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, "");
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			//nAlter = -2;
			nAlter |= 1;
		}
	}
/* 20130313 ibjang add, APK STATUS 추가 */


/* bIDayNight 사용안하는것 같다.
	if ( xchgSysVar.bIDayNight ) {
		no = ( pnewVar->bIDayNight ) ?  MSGNO_SYS_INIGHT : MSGNO_SYS_IDAY;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		//nAlter = -2;
		nAlter |= 1;
	}
*/
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//  Levelcross Type
	//

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC1KXLR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 0 ) {
		if ( pcurHead->nSysVarExt.bLC1KXLR == 1 ) no = MSGNO_LC_KEY_RELEASE;
		else no = MSGNO_LC_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[1]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC1NKXPR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 0 ) {
		if ( pcurHead->nSysVarExt.bLC1NKXPR == 1 ) no = MSGNO_LC_OPEN;
		else no = MSGNO_LC_CLOSE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[1]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC1BELL && pApp->m_VerifyObjectForMessage.iNumberOfLC > 0 ) {
		if ( pcurHead->nSysVarExt.bLC1BELL == 1 ) {
			no = MSGNO_LC_BELL_CTRL;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[1]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC1BELACK && pApp->m_VerifyObjectForMessage.iNumberOfLC > 0 ) {
		if ( pcurHead->nSysVarExt.bLC1BELACK == 1 ) {
			no = MSGNO_LC_BELL_ACK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[1]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC2KXLR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 1 ) {
		if ( pcurHead->nSysVarExt.bLC2KXLR == 1 ) no = MSGNO_LC_KEY_RELEASE;
		else no = MSGNO_LC_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[2]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC2NKXPR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 1 ) {
		if ( pcurHead->nSysVarExt.bLC2NKXPR == 1 ) no = MSGNO_LC_OPEN;
		else no = MSGNO_LC_CLOSE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[2]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC2BELL && pApp->m_VerifyObjectForMessage.iNumberOfLC > 1 ) {
		if ( pcurHead->nSysVarExt.bLC2BELL == 1 ) {
			no = MSGNO_LC_BELL_CTRL;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[2]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}

	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC2BELACK && pApp->m_VerifyObjectForMessage.iNumberOfLC > 1 ) {
		if ( pcurHead->nSysVarExt.bLC2BELACK == 1 ) {
			no = MSGNO_LC_BELL_ACK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[2]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC3KXLR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 2 ) {
		if ( pcurHead->nSysVarExt.bLC3KXLR == 1 ) no = MSGNO_LC_KEY_RELEASE;
		else no = MSGNO_LC_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[3]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC3NKXPR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 2 ) {
		if ( pcurHead->nSysVarExt.bLC3NKXPR == 1 ) no = MSGNO_LC_OPEN;
		else no = MSGNO_LC_CLOSE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[3]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC3BELL && pApp->m_VerifyObjectForMessage.iNumberOfLC > 2 ) {
		if ( pcurHead->nSysVarExt.bLC3BELL == 1 ) {
			no = MSGNO_LC_BELL_CTRL;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[3]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC3BELACK && pApp->m_VerifyObjectForMessage.iNumberOfLC > 2 ) {
		if ( pcurHead->nSysVarExt.bLC3BELACK == 1 ) {
			no = MSGNO_LC_BELL_ACK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[3]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC4KXLR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 3 ) {
		if ( pcurHead->nSysVarExt.bLC4KXLR == 1 ) no = MSGNO_LC_KEY_RELEASE;
		else no = MSGNO_LC_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[4]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC4NKXPR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 3 ) {
		if ( pcurHead->nSysVarExt.bLC4NKXPR == 1 ) no = MSGNO_LC_OPEN;
		else no = MSGNO_LC_CLOSE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[4]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC4BELL && pApp->m_VerifyObjectForMessage.iNumberOfLC > 3 ) {
		if ( pcurHead->nSysVarExt.bLC4BELL == 1 ) {
			no = MSGNO_LC_BELL_CTRL;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[4]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVarExt.bLC4BELACK && pApp->m_VerifyObjectForMessage.iNumberOfLC > 3 ) {
		if ( pcurHead->nSysVarExt.bLC4BELACK == 1 ) {
			no = MSGNO_LC_BELL_ACK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[4]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVar.bLC5KXLR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 4 ) {
		if ( pcurHead->nSysVar.bLC5KXLR == 1 ) no = MSGNO_LC_KEY_RELEASE;
		else no = MSGNO_LC_KEY_LOCK;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[5]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVar.bLC5NKXPR && pApp->m_VerifyObjectForMessage.iNumberOfLC > 4 ) {
		if ( pcurHead->nSysVar.bLC5NKXPR == 1 ) no = MSGNO_LC_OPEN;
		else no = MSGNO_LC_CLOSE;
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[5]);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVar.bLC5BELL && pApp->m_VerifyObjectForMessage.iNumberOfLC > 4 ) {
		if ( pcurHead->nSysVar.bLC5BELL == 1 ) {
			no = MSGNO_LC_BELL_CTRL;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[5]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}
	
	/* 20130319 ibjang 수정, LC 의 수의 따라 알람 출력 유무 결정 */
	if ( xchgSysVar.bLC5BELACK && pApp->m_VerifyObjectForMessage.iNumberOfLC > 4 ) {
		if ( pcurHead->nSysVar.bLC5BELACK == 1 ) {
			no = MSGNO_LC_BELL_ACK;
			pEvent = new MessageEvent;
			pEvent->msgno = no;
			pEvent->Type.nType  = MSG_TYPE_SYSTEM;
			pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
			pEvent->time = *(EventKey.pTime);
			pEvent->hsec = EventKey.hsec;
			pEvent->evn  = EventKey.EvnLoc;
			EventKey.EvnLoc.nLoc++;
			strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strLCName[5]);
			pEvent->item  = 0;
			objList.AddTail( (CObject*)pEvent );
			errorMode |= pEvent->Type.nMode & 0x0f;
			nAlter |= 1;
		}
	}

//////////////////////////////////////////////////////////////////////
//		Counter
//////////////////////////////////////////////////////////////////////

	if ( pcurHead->Counter[0] != poldHead->Counter[0] )
	{
		CString strMSGBuffer;
		unsigned short CounterOld = htons(poldHead->Counter[0]);
		unsigned short CounterNew = htons(pcurHead->Counter[0]);
		
		if ( CounterNew > CounterOld ) 
			no = MSGNO_SYS_COUNTER_CALLON_INC;
		else
			no = MSGNO_SYS_COUNTER_CALLON_CHG;

		strMSGBuffer.Format(" %d from %d", CounterNew, CounterOld );
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		strcpy(pEvent->EndOfMsg, strMSGBuffer);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	if ( pcurHead->Counter[1] != poldHead->Counter[1] )
	{
		CString strMSGBuffer;
		unsigned short CounterOld = htons(poldHead->Counter[1]);
		unsigned short CounterNew = htons(pcurHead->Counter[1]);
		
		if ( CounterNew > CounterOld ) 
			no = MSGNO_SYS_COUNTER_ERR_INC;
		else
			no = MSGNO_SYS_COUNTER_ERR_CHG;
		
		strMSGBuffer.Format(" %d from %d", CounterNew, CounterOld );
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, "");
		strcpy(pEvent->EndOfMsg, strMSGBuffer);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	if ( pcurHead->Counter[2] != poldHead->Counter[2] )
	{
		CString strMSGBuffer;
		unsigned short CounterOld = htons(poldHead->Counter[2]);
		unsigned short CounterNew = htons(pcurHead->Counter[2]);
		
		if ( CounterNew > CounterOld ) 
			no = MSGNO_SYS_COUNTER_AXLRST_INC;
		else
			no = MSGNO_SYS_COUNTER_AXLRST_CHG;
		
		strMSGBuffer.Format(" %d from %d", CounterNew, CounterOld );
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strBlokState[2]);
		strcpy(pEvent->EndOfMsg, strMSGBuffer);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	if ( pcurHead->Counter[3] != poldHead->Counter[3] )
	{
		CString strMSGBuffer;
		unsigned short CounterOld = htons(poldHead->Counter[3]);
		unsigned short CounterNew = htons(pcurHead->Counter[3]);
		
		if ( CounterNew > CounterOld ) 
			no = MSGNO_SYS_COUNTER_AXLRST_INC;
		else
			no = MSGNO_SYS_COUNTER_AXLRST_CHG;
		
		strMSGBuffer.Format(" %d from %d", CounterNew, CounterOld );
		pEvent = new MessageEvent;
		pEvent->msgno = no;
		pEvent->Type.nType  = MSG_TYPE_SYSTEM;
		pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
		pEvent->time = *(EventKey.pTime);
		pEvent->hsec = EventKey.hsec;
		pEvent->evn  = EventKey.EvnLoc;
		EventKey.EvnLoc.nLoc++;
		strcpy(pEvent->name, pApp->m_VerifyObjectForMessage.strBlokState[4]);
		strcpy(pEvent->EndOfMsg, strMSGBuffer);
		pEvent->item  = 0;
		objList.AddTail( (CObject*)pEvent );
		errorMode |= pEvent->Type.nMode & 0x0f;
		nAlter |= 1;
	}

	
	return nAlter;
}


int  CLOGMessage::GetAlarmMessage(CObList &objList, BYTE* pOld, BYTE* pCur,
									EventKeyType &EventKey, BYTE &errorMode)
{
	int  nAlter = 0;
	DataHeaderType *poldHead = (DataHeaderType *)pOld;
	DataHeaderType *pcurHead = (DataHeaderType *)pCur;
	SystemStatusType *poldVar = &poldHead->nSysVar;
	SystemStatusType *pnewVar = &pcurHead->nSysVar;
	SystemStatusType xchgSysVar;

	xchgSysVar.pVar[2] = poldVar->pVar[2] ^ pnewVar->pVar[2];
	return nAlter;
}

BOOL CLOGMessage::GetIOCardMessage(CObList &objList, void *pOld, void *pCur, 
								   EventKeyType &EventKey, BYTE &errorMode)
{
	int  i, nRack, nCard;
	int  no;
	union {
		BitLocType		nLoc;
		BitLocTypeEx	nExLoc;
	};
	BYTE d1, d2, c, bitMask, bitCard;
	BYTE *pOldIOC = (BYTE*)pOld; 
	BYTE *pCurIOC = (BYTE*)pCur; 
	BOOL bAlter = FALSE;
	MessageEvent *pEvent;

	// I, II 계 IOC 검사
	nLoc.nPort = 0;
	for(i=1; i<=2; i++) {
		for(nRack=0; nRack<8; nRack++) {
			bitCard = Info_cUsedCardMask[nRack];
			if ( bitCard ) {
				d1 = *pOldIOC;
				d2 = *pCurIOC;
				c  = d1 ^ d2;
				if ( c ) {
					bitMask = 0x01;
					for(nCard=0; nCard<8; nCard++) {
						if (bitCard & bitMask) {
							if ( c & bitMask ) {
								nExLoc.nCard = Info_CardTypeNo[nRack][2+nCard];	// 1 - xx
								//nLoc.nType  0/none, 1/in, 2/out, 3/sdm, 4/opt, 5/ioc, 6/cpu, 7/com, 8/power, ..etc.
								no = (nCard % 4) << 1;
								if ( nCard >= 4 ) {
									nLoc.nType = (Info_CardTypeNo[nRack][1] >> no) & 3;
								}
								else {
									nLoc.nType = (Info_CardTypeNo[nRack][0] >> no) & 3;
								}
								nLoc.nMain = i - 1;
								//no = (d2 & bitMask) 
								//			? MSGNO_ERR_MODULE_RECOVERY 
								//			: MSGNO_ERR_MODULE_FAULT;
								if (d2 & bitMask) 
									no = MSGNO_ERR_MODULE_RECOVERY;
								else
									no = MSGNO_ERR_MODULE_FAULT;
								pEvent = new MessageEvent;
								pEvent->msgno = no;
								pEvent->Type.nType  = MSG_TYPE_SYSTEM;
								pEvent->Type.nMode  = pApp->m_pEIPMsgList->GetMode( no );
								pEvent->time = *(EventKey.pTime);
								pEvent->hsec = EventKey.hsec;
								pEvent->evn  = EventKey.EvnLoc;
								EventKey.EvnLoc.nLoc++;
								itoa( nCard + nRack * 8 + 1 , pEvent->name, 10 );
								pEvent->loc  = nLoc;
								objList.AddTail( (CObject*)pEvent );
								errorMode |= pEvent->Type.nMode & 0x0f;
								bAlter = TRUE;
							}
						}
						bitMask <<= 1;
					}
				}
			}
			pOldIOC++;
			pCurIOC++;
		}
	}
	return bAlter;
}


void CLOGMessage::CreateCardInfo()
{
	if ( pApp->m_pEipEmu == NULL ) return;
	BYTE c;
	BYTE d;
	BYTE mask;
	BYTE newmode, oldmode;
	int  i, j, k;
	int  byteloc = 0;
	for(i=0; i<8; i++) {
		mask = 1;
		for(j=0; j<2; j++) {
			c = pApp->m_pEipEmu->m_TableBase.IOCardInfo[ (i<<1)+j ];
			Info_CardTypeNo[i][j] = c;
			for(k=0; k<4; k++) {
				newmode = c & 3;
				if (pApp->m_pEipEmu->m_TableBase.RealID[byteloc] & 0x80) {
					newmode = 0;
				}
				if ( newmode ) {
					Info_cUsedCardMask[i] |= mask;
				}
				c >>= 2;
				mask <<= 1;
				byteloc++;
			}
		}
		if ( Info_cUsedCardMask[i] ) {
			Info_cUsedCardMask[i] |= 1;
		}
	}
	d = 0x00;
	oldmode = 0x00;
	for(i=0; i<8; i++) {
		if ( !Info_cUsedCardMask[i] ) d = 0;
		for(j=0; j<2; j++) {
			c = Info_CardTypeNo[i][j];
			for(k=0; k<4; k++) {
				newmode = c & 3;
				if (newmode && (newmode != oldmode)) {
					d = 0x00;
				}
				Info_CardTypeNo[i][2+(j*4)+k] = (j || k) ? ++d : 0;
				if ( newmode ) {
					oldmode = newmode;
				}
				c >>= 2;
			}
		}
	}
}

BOOL CLOGMessage::CreateRouteInfo(MenuItems *pMenus, BOOL bSearchItem)
{
	if ( pApp->m_pEipEmu == NULL ) return FALSE;
	BYTE nSignalID, nButtonID, nOldSignalID, nOldButtonID;
	BYTE nToTrackID;
	BYTE nType;
	BYTE *pInfo;
	UINT nInfoPtr;		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
	int  n;
	int  nRouteCount, nMainSignalCount, nCallonSignalCount, nShuntSignalCount , nRouteStatus , nOldRouteStatus;
	BOOL bRet = TRUE;
	CString strName;

	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
	nRouteCount = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[0];
	nMainSignalCount = nCallonSignalCount = nShuntSignalCount = nOldButtonID = nOldSignalID = 0;
	nOldRouteStatus = -1;
	for (n=1; n<=nRouteCount; n++) {
		nInfoPtr = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[ n ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
		pInfo = &pApp->m_pEipEmu->m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
		nButtonID = pInfo[RT_OFFSET_BUTTON];
		nSignalID = pInfo[RT_OFFSET_SIGNAL];
		nToTrackID = pInfo[RT_OFFSET_TO];
		nRouteStatus = pInfo[RT_OFFSET_ROUTESTATUS];
		nType = pApp->m_pEipEmu->m_pSignalInfoTable[ nSignalID ].nSignalType & 0x0f0;
		if (((nButtonID != nOldButtonID) || (nOldSignalID != nSignalID) || (nOldRouteStatus != nRouteStatus) ) 
		     && (nType != BLOCKTYPE_INTER_OUT) && (nType != BLOCKTYPE_AUTO_OUT) && (nType != BLOCKTYPE_INTER_IN) )
		{
			if ( nRouteStatus & RT_CALLON ) {  // 콜온진로 
				if ( !bSearchItem ) {
					pMenus[1].pRMenu[nCallonSignalCount].nRouteID = n;
					pMenus[1].pRMenu[nCallonSignalCount].nDisable = 0;
					pMenus[1].pRMenu[nCallonSignalCount].nType = nType;
					pMenus[1].pRMenu[nCallonSignalCount].nToTrackID = nToTrackID;
					pMenus[1].pRMenu[nCallonSignalCount].nSignalID = nSignalID;
					pMenus[1].pRMenu[nCallonSignalCount].nButtonID = nButtonID;
					GetRouteName( strName, nSignalID, nButtonID );
					strncpy(pMenus[1].pRMenu[nCallonSignalCount].Name, strName, 64);
				}
				nCallonSignalCount++;
			}
			else if ( nRouteStatus & RT_SHUNTSIGNAL ) {								// 션트진로 
				if ( !bSearchItem ) {
					pMenus[2].pRMenu[nShuntSignalCount].nRouteID = n;
					pMenus[2].pRMenu[nShuntSignalCount].nDisable = 0;
					pMenus[2].pRMenu[nShuntSignalCount].nType = nType;
					pMenus[2].pRMenu[nShuntSignalCount].nToTrackID = nToTrackID;
					pMenus[2].pRMenu[nShuntSignalCount].nSignalID = nSignalID;
					pMenus[2].pRMenu[nShuntSignalCount].nButtonID = nButtonID;
					GetRouteName( strName, nSignalID, nButtonID );
					strncpy(pMenus[2].pRMenu[nShuntSignalCount].Name, strName, 64);
				}
				nShuntSignalCount++;
			} else {
				if ( !bSearchItem ) {
					pMenus[0].pRMenu[nMainSignalCount].nRouteID = n;
					pMenus[0].pRMenu[nMainSignalCount].nDisable = 0;
					pMenus[0].pRMenu[nMainSignalCount].nType = nType;
					pMenus[0].pRMenu[nMainSignalCount].nToTrackID = nToTrackID;
					pMenus[0].pRMenu[nMainSignalCount].nSignalID = nSignalID;
					pMenus[0].pRMenu[nMainSignalCount].nButtonID = nButtonID;
					GetRouteName( strName, nSignalID, nButtonID );
					strncpy(pMenus[0].pRMenu[nMainSignalCount].Name, strName, 64);
				}
				nMainSignalCount++;
			}

			nOldButtonID = nButtonID;
			nOldSignalID = nSignalID;
			nOldRouteStatus = nRouteStatus;
		}
	}
	if ( bSearchItem ) {
		if ( nMainSignalCount ) {
			pMenus[0].pRMenu = new MenuItemRoute [ nMainSignalCount ];
			pMenus[0].nMax  = nMainSignalCount;
			if ( pMenus[0].pRMenu == NULL )
				bRet = FALSE;
		}
		if ( nCallonSignalCount ) {
			pMenus[1].pRMenu = new MenuItemRoute [ nCallonSignalCount ];
			pMenus[1].nMax  = nCallonSignalCount;
			if ( pMenus[1].pRMenu == NULL )
				bRet = FALSE;
		}
		if ( nShuntSignalCount ) {
			pMenus[2].pRMenu = new MenuItemRoute [ nShuntSignalCount ];
			pMenus[2].nMax  = nShuntSignalCount;
			if ( pMenus[2].pRMenu == NULL )
				bRet = FALSE;
		}
	}
	else if ( !nMainSignalCount && !nCallonSignalCount && !nShuntSignalCount ) {
		bRet = FALSE;
	}

	return bRet;
}

BOOL CLOGMessage::CreateMenuInfo(MenuItems *pMenus, MenuItemType createType)	// signal(main/shount), switch, track
{
	if ( pApp->m_pEipEmu == NULL ) return FALSE;
	int  i, n;
	int  nMaxTrack  = pApp->nMaxTrack;
	int  nMaxSignal = pApp->nMaxSignal;
	int  nMaxSwitch = pApp->nMaxSwitch;
	BOOL bRet = FALSE;

	switch ( createType ) {
	case MenuSignalType:
		bRet = CreateRouteInfo( pMenus, TRUE );
		if ( bRet ) {
			bRet = CreateRouteInfo( pMenus, FALSE );
		}
		break;
	case MenuSwitchType:
		pMenus->pIMenu = new MenuItemStruct [ nMaxSwitch ];
		if ( pMenus->pIMenu != NULL ) {
			n = nMaxTrack + nMaxSignal;
			for (i=0; i<nMaxSwitch; i++) {
				pMenus->pIMenu[i].nID  = i+1;
				strncpy(pMenus->pIMenu[i].Name, pApp->Info_pScrMessageInfo[n].m_strName, 64);
				n++;
			}
			pMenus->nMax = nMaxSwitch;
			bRet = TRUE;
		}
		break;

	case MenuTrackType:
		pMenus->pIMenu = new MenuItemStruct [ nMaxTrack ];
		if ( pMenus->pIMenu != NULL ) {
			for (i=n=0; n<nMaxTrack; n++) {
				if ((pApp->Info_pScrMessageInfo[n].m_strName != "") && (pApp->Info_pScrMessageInfo[n].m_strName.GetAt(0) != '?')) {
					pMenus->pIMenu[i].nID = n+1;
			   		strncpy(pMenus->pIMenu[i].Name, pApp->Info_pScrMessageInfo[n].m_strName, 64);
					i++;
				}
			}
			pMenus->nMax = i;
			for (; i<nMaxTrack; i++) {
				pMenus->pIMenu[i].nID = 0;
				memset(pMenus->pIMenu[i].Name, 0, 64);
			}
			bRet = TRUE;
		}
		break;

	default:
		pMenus->pIMenu = NULL;
		break;
	}

	return bRet;
}

BOOL CLOGMessage::CreateInfo( CMessageInfo *pScrInfo, int nMax ) 	// TRUE/Create Normal, FALSE/Create fail.
{
	if ( pApp->Info_pScrMessageInfo == NULL ) {
		if ( nMax > 0 ) {
			CMessageInfo *pInfo = new CMessageInfo[ nMax ];
			if ( pInfo ) {
				char szName[32];	
				for(int i=0; i<nMax; i++) {
					strcpy( szName, (LPCTSTR)(pScrInfo[i].m_strName));
					pInfo[i].m_strName = szName;
					pInfo[i].m_pEipData = pScrInfo[i].m_pEipData;
					pInfo[i].m_nMemOffset = pScrInfo[i].m_nMemOffset;
					pInfo[i].m_nType = pScrInfo[i].m_nType;
					pInfo[i].m_nID = pScrInfo[i].m_nID;
				}
				pApp->Info_pScrMessageInfo = pInfo;
				return TRUE;
			}
		}
		return FALSE;
	}
	return TRUE;
}


void CLOGMessage::SetBaseRecord(CLOGRecord &oldR)
{
	pApp= (CLESApp*)AfxGetApp();
	m_OldRecord = oldR;
	m_bNoneBase = FALSE;
}

int  CLOGMessage::CreateMessage(CLOGRecord &RecCur, CObList &objList) // -1/All, 0/None, +Other/Part
{
	BYTE uFlag  = 0x00;					// status(D2), error(D1), control(D0)
	int  nState;
	int  nAlter = 0;					// d0:alter, d1:track, d2:signal, d3:switch, d4:system. d8:all
	BOOL bPart  = TRUE;
	EventKeyType EventKey;

	if (RecCur.m_Head.Type == LOG_RECTYPE_SAME) 
		return nAlter;
	if ( m_bNoneBase ) {
		SetBaseRecord( RecCur );
	}
	if ( !isAlterRecordData( m_OldRecord, RecCur, TRUE ) ) {
		m_OldRecord = RecCur;
		return 0;
	}
	nAlter |= 1;

	int count = objList.GetCount();
	EventKey.pTime = (CJTime*)&RecCur.m_SysHead.nSec;
	EventKey.hsec  = RecCur.m_SysHead.Cycle;		// ms 데이터가 없으므로 Cycle로 대신함.
	EventKey.EvnLoc.nRec = RecCur.m_Head.Order;
	EventKey.EvnLoc.nLoc = 0;
	// 변환된 데이터에 대한 정보 검색 및 변환 데이터 추출
	if ( pApp->Info_pScrMessageInfo && pApp->m_pEipEmu ) {
		int i, array;
		BYTE *pOldVMem = m_OldRecord.m_BitBlock;
		BYTE *pCurVMem = RecCur.m_BitBlock;
		// 제어 메시지 추출
		// 통신 출력있는 메시지
		int  oppos;
		oppos = pApp->nSfmRecordSize;
		BYTE *ptr = &pCurVMem[ oppos ];
		int nOpState = GetOperateMessage(objList, ptr , pOldVMem ,pCurVMem, EventKey, uFlag);
		if ( nOpState ) {
			nAlter |= 1;
		}

		if(objList.GetCount())
		{
			TRACE("Operate Message Count = %d\n", objList.GetCount());
		}

		ptr = &pCurVMem[ oppos + MAX_OPERATE_CMD_CODESIZE ];
		UnitStatusType *poldVar = &m_OldRecord.m_SysHead.UnitState;
		UnitStatusType *pnewVar = &RecCur.m_SysHead.UnitState;

		nState = GetUnitSendMessage(objList, ptr, pCurVMem, EventKey, uFlag, (!poldVar->bSysRun && !pnewVar->bSysRun) ? TRUE : FALSE);
		if ( nState ) {
			nAlter |= 1;
		}

		nState = GetSystemMessage(objList, pOldVMem, pCurVMem, EventKey, uFlag, ((nState==-1) ? TRUE : FALSE)); // nState == -1 : SYSTEM DOWN Message 발생
		if ( nState ) {
			nAlter |= 17;
			if (nState < -1) 			// 시스템 다운 또는 컴퓨터 프로그램 기동.
				nAlter |= 0x1000;		// 메시지 목록 표시 상자 재 검색 표시.
			if (nState < 0 )
				nState = -1;			// 후속 메시지 처리 않함. 
		}

		pOldVMem += sizeof(DataHeaderType);
		pCurVMem += sizeof(DataHeaderType);

		for( array=i=0; i<pApp->nMaxTrack; i++) {
			nState = pApp->Info_pScrMessageInfo[array++].GetMessage(objList, pOldVMem, pCurVMem, EventKey, uFlag);
			if ( nState ) {
				if (nState & 2) nAlter |= 2;
				nAlter |= 1;
			}
			pOldVMem += SIZE_INFO_TRACK;
			pCurVMem += SIZE_INFO_TRACK;
		}
		for(i=0; i<pApp->nMaxSignal; i++) {
			nState = pApp->Info_pScrMessageInfo[array++].GetMessage(objList, pOldVMem, pCurVMem, EventKey, uFlag);
			if ( nState ) {
				if (nState & 2) nAlter |= 4;
				nAlter |= 1;
			}
			pOldVMem += SIZE_INFO_SIGNAL;
			pCurVMem += SIZE_INFO_SIGNAL;
		}
		for(i=0; i<pApp->nMaxSwitch; i++) {
			nState = pApp->Info_pScrMessageInfo[array++].GetMessage(objList, pOldVMem, pCurVMem, EventKey, uFlag);
			if ( nState ) {
				if (nState & 2) nAlter |= 2;
				nAlter |= 1;
			}
			pOldVMem += SIZE_INFO_SWITCH;
			pCurVMem += SIZE_INFO_SWITCH;
		}
		pOldVMem = m_OldRecord.m_BitBlock;
		pCurVMem = RecCur.m_BitBlock;
		nState = GetAlarmMessage(objList, pOldVMem, pCurVMem, EventKey, uFlag); // nState == -1 : SYSTEM DOWN Message 발생
		if ( nState ) nAlter |= 17;
	}

	RecCur.m_Head.Flag = uFlag;

	m_OldRecord = RecCur;
	if ( count != objList.GetCount() ) {
		return nAlter;
	}
	return nAlter;		//0 None Alternation
}

int CLOGMessage::GetTrackID(LPCSTR lpszName)
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strTrack.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strTrack.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

int CLOGMessage::GetSignalID(LPCSTR lpszName)
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strSignal.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strSignal.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

int CLOGMessage::GetSwitchID(LPCSTR lpszName)
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strSwitch.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strSwitch.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

int CLOGMessage::GetButtonID(LPCSTR lpszName)
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strButton.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strButton.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

int CLOGMessage::GetRouteName(CString &strRoute, int SignalID, int ButtonID)
{
	strRoute = "UnKnown"; 
	SignalID--;
	ButtonID--;
	if ((SignalID < 0) || (ButtonID < 0)) 
		return 0;

	CString strSignal = GetSignalName( SignalID );
	CString strButton = GetButtonName( ButtonID );
	if ((strSignal != "") && (strButton != "")) {
		strRoute  = strSignal;
		strRoute += ":";
		strRoute += strButton;
		return 1;
	}

	return 0;
}

CString CLOGMessage::GetSignalName(int nID)
{
	CString strName;

	if ( nID >= 0 ) {
		int id = 0;
		POSITION pos;
		for(pos = pApp->m_pEipEmu->m_strSignal.GetHeadPosition(); pos != NULL; ) {
			strName = *(CString*)pApp->m_pEipEmu->m_strSignal.GetNext(pos);
			if (id == nID) break;
			else {
				strName = "";
				id++;
			}
		}
	}
	pApp->m_VoiceErr = strName+"[S]";
	return strName;
}

CString CLOGMessage::GetSwitchName(int nID)
{
	CString strName;

	if ( nID >= 0 ) {
		int id = 0;
		POSITION pos;
		for(pos = pApp->m_pEipEmu->m_strSwitch.GetHeadPosition(); pos != NULL; ) {
			strName = *(CString*)pApp->m_pEipEmu->m_strSwitch.GetNext(pos);
			if (id == nID) break;
			else {
				strName = "";
				id++;
			}
		}
	}
	pApp->m_VoiceErr = strName+"[P]";
	return strName;
}

CString CLOGMessage::GetTrackName(int nID)
{
	CString strName;

	if ( nID >= 0 ) {
		int id = 0;
		POSITION pos;
		for(pos = pApp->m_pEipEmu->m_strTrack.GetHeadPosition(); pos != NULL; ) {
			strName = *(CString*)pApp->m_pEipEmu->m_strTrack.GetNext(pos);
			if (id == nID) break;
			else {
				strName = "";
				id++;
			}
		}
	}
	pApp->m_VoiceErr = strName+"[T]";
	return strName;
}

CString CLOGMessage::GetButtonName(int nID)
{
	CString strName;

	if ( nID >= 0 ) {
		int id = 0;
		POSITION pos;
		for(pos = pApp->m_pEipEmu->m_strButton.GetHeadPosition(); pos != NULL; ) {
			strName = *(CString*)pApp->m_pEipEmu->m_strButton.GetNext(pos);
			if (id == nID) break;
			else {
				strName = "";
				id++;
			}
		}
	}
	return strName;
}

BOOL CLOGMessage::TrackItemCheck( MenuItemStruct  *pIMenu, int nMax )
{
	int i;
	if ( pApp->m_pEipEmu  == NULL ) {
		for(i=0; i<nMax; i++) {
			pIMenu[i].nDisable = 1;
		}
		return FALSE;
	}

	BYTE newf;
	int  id;
	CMessageInfo*  pMessageScrInfo;
	ScrInfoTrack* pTrackInfo;

	for(i=0; i<nMax; i++) {
		id = pIMenu[i].nID;
		newf = 0;
		if ( id ) {
			pMessageScrInfo = &pApp->Info_pScrMessageInfo[ id-1 ];
			pTrackInfo  = (ScrInfoTrack*)&pApp->SystemVMEM[ pMessageScrInfo->m_nMemOffset ];
			if ( ((!pTrackInfo->ROUTE && !pTrackInfo->ROUTEDIR) || (!pTrackInfo->ROUTE && pTrackInfo->ROUTEDIR)) && !pTrackInfo->EMREL ) 
			{																				// 비상진로구분해정 할 수 있는 경우
				for( int j = 0; j < m_iCountOfDestTrack; j++)
				{
					if ( ((m_bNameOfDestTrackID[j] & 0x7f) == id ) && ( m_bNameOfDestTrackID[j] & 0x80) )
					{
						newf |= CHECK_TRACK_FREEROUTE;
						break;
					}
				}
			}
			else {
				if ( pTrackInfo->INHIBIT ) newf |= CHECK_TRACK_MOTERCAR;
			}
		}
		else newf |= CHECK_TRACK_ALLDISABLE;
		pIMenu[i].nDisable = newf;
	}
	return TRUE;
}

BOOL CLOGMessage::SwitchItemCheck( MenuItemStruct  *pIMenu, int nMax )
{
	int i, n;
	if ( pApp->m_pEipEmu  == NULL ) {
		for(i=0; i<nMax; i++) {
			pIMenu[i].nDisable = 1;
		}
		return FALSE;
	}

	BYTE state;
	CMessageInfo*  pMessageScrInfo;
	ScrInfoSwitch* pSwitchInfo;

	int  nMaxTrack  = pApp->nMaxTrack;
	int  nMaxSignal = pApp->nMaxSignal;
	for(i=0; i<nMax; i++) {
		state = 0;
		if ( pIMenu[i].nID ) {
			n = nMaxTrack + nMaxSignal + pIMenu[i].nID;
			pMessageScrInfo = &pApp->Info_pScrMessageInfo[ n - 1 ];
			pSwitchInfo  = (ScrInfoSwitch*)pMessageScrInfo->GetBufferPtr(pApp->SystemVMEM );
			if ( !pSwitchInfo->FREE || pSwitchInfo->INROUTE ) // 쇄정상태(철사쇄정+진로쇄정)
				state = 1;
		}
		pIMenu[i].nDisable = state;
	}
	return TRUE;
}

BOOL CLOGMessage::ClearDestTrackID()
{
	m_iCountOfDestTrack = 0;
	for ( int i = 0; i < 50; i++)
	{
		m_bNameOfDestTrackID[i] = NULL;
	}
	return TRUE;
}
BOOL CLOGMessage::RouteItemCheck( MenuItemRoute *pMenu, int nMax )
{
	CMainFrame* pWnd;
	CLESView* pView;
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();

	if ( pApp->m_pEipEmu  == NULL ) {
		for(int i=0; i<nMax; i++) {
			pMenu[i].nDisable |= 1;
		}
		return FALSE;
	}

	ParamType wParam;
	ParamType lResult;

	CMessageInfo*  pMessageScrInfo;
	ScrInfoSignal* pSigInfo;
	int  nType;
	BOOL bSignalOn;
	BOOL bIsRoute;
	int  nRouteID, nSignalID;

	for(int n=0; n<nMax; n++) {
		nSignalID = pMenu[n].nSignalID;
		nRouteID  = pMenu[n].nRouteID;
		pMessageScrInfo = &pApp->Info_pScrMessageInfo[pApp->nMaxTrack+nSignalID-1];
		pSigInfo = (ScrInfoSignal*)&pApp->SystemVMEM[ pMessageScrInfo->m_nMemOffset ];
		wParam.ndl = nRouteID;
		BOOL bNoEmp = MainOlePostMessage( WS_OPMSG_CALLCHECKROUTE, wParam.wParam, 0, lResult.lParam );
		bIsRoute = ((lResult.ndl & ROUTECHECK_ENABLE) == 0) ? TRUE : FALSE;
		
		
		nType = pApp->m_pEipEmu->m_pSignalInfoTable[ nSignalID ].nSignalType & 0x0f0;
		if (nType == BLOCKTYPE_INTER_OUT)		//	연동 출발 폐색
			nType = 7;
		else if (nType == BLOCKTYPE_INTER_IN)	//	연동 장내 폐색
			nType = 8;
		else if (nType == BLOCKTYPE_AUTO_OUT)	//	자동 출발 폐색 
			nType = 7;
		else if (nType == BLOCKTYPE_AUTO_IN)	//	자동 장내 폐색 
			nType = 8;
		else									//	0/2현시, 1/3현시, 2/4현시, 3/5현시
			nType = pApp->m_pEipEmu->m_pSignalInfoTable[ nSignalID ].nSignalType & 0x03;
		BYTE *pData = &pApp->SystemVMEM[ pMessageScrInfo->m_nMemOffset ];
		bSignalOn = pMessageScrInfo->isSignalOn( nType, pData );
		if ( pApp->m_pEipEmu->m_pSignalInfoTable[ nSignalID ].nSignalType & SIGNALTYPE_YUDO_BIT ) {
			if ( pSigInfo->INU ) 
				bSignalOn = TRUE;
		}
		if ( bSignalOn ) 
		{
			if ((pSigInfo->DESTTRACK & 0x7f) == pMenu[n].nToTrackID)
			{
				pMenu[n].nDisable = 0x81;	// On & Disable

				if ( pMenu[n].nType != 0xc0 || pSigInfo->CS )	// Outer Home 신호기의 main 진로(Home 의 종속 신호.)
				{
					BOOL bHaveSameDestID = FALSE;

					for ( int i = 0; i < m_iCountOfDestTrack; i++)
					{
						if ( m_bNameOfDestTrackID[m_iCountOfDestTrack] == pSigInfo->DESTTRACK )
						{
							bHaveSameDestID = TRUE;
							break;
						}
					}
					if ( !bHaveSameDestID )
					{
						m_bNameOfDestTrackID[m_iCountOfDestTrack] = pSigInfo->DESTTRACK;		// Emergency Route Release에서 Dest Track만 Menu에 표시하기 위해서.
						m_iCountOfDestTrack++;
					}
				}
			}
			else 
				pMenu[n].nDisable = 0x80;	// Disable
		}
		else if ( bIsRoute ) {				// 가능
			pMenu[n].nDisable = 0;			// Clear
		}
		else if (!bIsRoute && !pSigInfo->FREE) 
		{
			if ((pSigInfo->DESTTRACK & 0x7f) == pMenu[n].nToTrackID)
			{
				pMenu[n].nDisable = 2;		// On or Cancel - 2000.12.11

				if ( pMenu[n].nType != 0xc0 || pSigInfo->CS )	// Outer Home 신호기의 main 진로(Home 의 종속 신호.)
				{
					BOOL bHaveSameDestID = FALSE;
					
					for ( int i = 0; i < m_iCountOfDestTrack; i++)
					{
						if ( m_bNameOfDestTrackID[i] == pSigInfo->DESTTRACK )
						{
							bHaveSameDestID = TRUE;
							break;
						}
					}
					if ( !bHaveSameDestID )
					{
						m_bNameOfDestTrackID[m_iCountOfDestTrack] = pSigInfo->DESTTRACK;		// Emergency Route Release에서 Dest Track만 Menu에 표시하기 위해서.
						m_iCountOfDestTrack++;
					}
				}
			}
			else 
				pMenu[n].nDisable = 0x80;	// Disable
		}
		else {								// 불가
			pMenu[n].nDisable = 0x80;		// Disable
		}

		if ( pMenu[n].nType == 0xc0 && !pSigInfo->CS )	// Outer Home 신호기의 main 진로(Home 의 종속 신호.)
			pMenu[n].nDisable |= 0x04;
		
	}

	m_iNumberOfRoute = m_iNumberOfRoute;

	return TRUE;
}

BOOL CLOGMessage::MainOlePostMessage(long message, long wParam, long lParam, long &lResult)
{
	CMainFrame* pWnd;
	CLESView* pView;
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();
	lResult = pView->m_Screen.OlePostMessage(message, wParam, lParam);
	return TRUE;
}

BOOL CMessageInfo::isSignalOn( int nType, BYTE *pData )
{
	ScrInfoSignal *pSigInfo = (ScrInfoSignal *)pData;

	if (nType == 0) {	// 2 현시(입환신호기, 입환표지)
		if ( pSigInfo->SIN3 )
			return TRUE;
	}
	else if ((nType == 7) || (nType == 8)) {	// 출발 폐색, 장내 폐색.
		if (!pSigInfo->SIN1 || !pSigInfo->SIN2 || !pSigInfo->SIN3) {
			return  TRUE;
		}
	}
	else {
        if ( pSigInfo->SIN3 ) return TRUE;
	}
	return FALSE;
}

int CLOGMessage::GetButtonIDFromSignalDest( int nSignal, int nDestTrack )
{
	if ( pApp->m_pEipEmu == NULL ) return 0;
	BYTE nSignalID, nButtonID;
	BYTE nToTrackID;
	BYTE *pInfo;
	UINT nInfoPtr;
	int  n;
	int  nRouteCount;

	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
	nRouteCount = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[0];
	for (n=1; n<=nRouteCount; n++) {
		nInfoPtr = *(UINT*)&pApp->m_pEipEmu->m_pRouteAddTable[ n ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
		pInfo = &pApp->m_pEipEmu->m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
		nButtonID = pInfo[RT_OFFSET_BUTTON];
		nSignalID = pInfo[RT_OFFSET_SIGNAL];
		nToTrackID = pInfo[RT_OFFSET_TO];
		if ( nSignalID == nSignal && nToTrackID == nDestTrack ) {
			return nButtonID;
		}
	}
	return 0;
}

BOOL CLOGMessage::isAlterRecordData(CLOGRecord &OldRec, CLOGRecord &ScrRec, BOOL bChkBlock)
{
	int  i;
	int  max=sizeof(DataHeaderType);
	BYTE *pOld = &OldRec.m_BitBlock[0];
	BYTE *pScr = &ScrRec.m_BitBlock[0];

	// 시스템 데이터 검사 
	for(i=0; i<2; i++) {
		if ( *pOld != *pScr ) {
			return TRUE;		// 주,부계 또는 메시지 타입 변화 있음.
		}
		pOld++;
		pScr++;
	}
	// Timer 데이터 검사 제외.
	i = 8;		
	pOld += 6;
	pScr += 6;

	for(; i<13; i++) {			// 13 변경(Cycle검사).
		if ( *pOld != *pScr ) {
			return TRUE;		// SystemStatusType 의 'nSysVar' 변수 변화 있음.
		}
		pOld++;
		pScr++;
	}

	for(; i<max; i++) {			// 시스템 데이터 마지막 IOC1, IOC2 블럭까지 포함하여 검사.
		if ( *pOld != *pScr ) {
			return TRUE;		// Cycle 을 제외한 부분에 변화가 있으면 TRUE
		}
		pOld++;
		pScr++;
	}

	if ( bChkBlock ) {
	// 기타의 통신 데이터 검사 
		max = pApp->nVerRecordSize;
		for(; i<max; i++) {		//
			if ( *pOld != *pScr ) {
				return TRUE;		// Object list, op command, unit message 부분의 변화 있으면 TRUE.
			}
			pOld++;
			pScr++;
		}
	}

	return FALSE;
}


