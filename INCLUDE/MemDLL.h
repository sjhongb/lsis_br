#if !defined(AFX_MAKEM_H__CD6D177E_E886_11D2_815A_94DCBE0375F7__INCLUDED_)
#define AFX_MAKEM_H__CD6D177E_E886_11D2_815A_94DCBE0375F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MakeM.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMakeM view

class AFX_EXT_CLASS CMemDLL
{
public:
	CMemDLL(LPCTSTR fileName, long size);    
	virtual ~CMemDLL();
public:
	HANDLE m_hMapping;
	LPVOID m_Data;

public:
	LPVOID GetMemoryAddress();
};
#endif // !defined(AFX_MAKEM_H__CD6D177E_E886_11D2_815A_94DCBE0375F7__INCLUDED_)
