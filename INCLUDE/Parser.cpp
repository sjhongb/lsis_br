//===============================================================
//====                       Parser.cpp                       ====
//===============================================================
// 파  일  명 : Parser.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 다른 클래스에서 사용될 각종 연산 함수를 정의한다.
//              
//
//===============================================================


#include "stdafx.h"
#include "Parser.h"

char DEFDELIMETER[] = " ()!+*=;{}\t\n\r";

CParser::CParser()
{
	m_iEnd = 0;
}

CParser::CParser(char *str, char *pDel)
{
	Create(str, pDel);
}

CParser::CParser(const CString& str, char *pDel)
{
	m_strString = str;
	if ( pDel ) m_strDelimiter = pDel; 
	else m_strDelimiter = " ";;
	m_iEnd = 0;
}
	
void CParser::Create(char *str, const char *pDel)
{
	m_strString = str;
	if ( pDel ) m_strDelimiter = pDel; 
	else m_strDelimiter = DEFDELIMETER;
	m_iEnd = 0;
}

void CParser::Create(const CString &str, const char *pDel)
{
	m_strString = str;
	if ( pDel ) m_strDelimiter = pDel; 
	else m_strDelimiter = DEFDELIMETER;
	m_iEnd = 0;
}

BOOL CParser::GetToken(CString &str, const char *pDel, BOOL bDefDel)
{
	str = "";
	const char *pdelimiter;
	if ( pDel ) pdelimiter = pDel;
	else pdelimiter = m_strDelimiter.GetBuffer(1);
	if (m_iEnd >= m_strString.GetLength()) return FALSE;
	char c;
	for(; m_iEnd<m_strString.GetLength(); m_iEnd++) {
		c = m_strString.GetAt(m_iEnd);
		if ((c != ' ') && (c != '\t')) break;
	}
	if (m_iEnd >= m_strString.GetLength()) return FALSE;
	m_bDelimiter = FALSE;
	for(; m_iEnd<m_strString.GetLength(); m_iEnd++) {
		c = m_strString.GetAt(m_iEnd);

		if ((strchr( pdelimiter, c) != NULL) || (bDefDel && ((c == ' ') || (c == '\t'))) ) {
			if (str == "") {
				str += c;
				m_iEnd++;
				m_bDelimiter = TRUE;
			}
			return TRUE;
		}
		str += c;
	}
	if (str == "") return FALSE;
	return TRUE;
}

char *CParser::GetToken(const char *pDel, BOOL bDefDel)
{
	m_strToken = "";
	const char *pdelimiter;
	if ( pDel ) pdelimiter = pDel;
	else pdelimiter = m_strDelimiter.GetBuffer(1);
	if (m_iEnd >= m_strString.GetLength()) return NULL;
	char c;
	for(; m_iEnd<m_strString.GetLength(); m_iEnd++) {
		c = m_strString.GetAt(m_iEnd);
		if ((c != ' ') && (c != '\t')) break;
	}
	if (m_iEnd >= m_strString.GetLength()) return NULL;
	m_bDelimiter = FALSE;
	for(; m_iEnd<m_strString.GetLength(); m_iEnd++) {
		c = m_strString.GetAt(m_iEnd);

		if ((strchr( pdelimiter, c) != NULL) || (bDefDel && ((c == ' ') || (c == '\t'))) ) {
			m_bDelimiter = TRUE;
			if (m_strToken == "") {
				m_strToken += c;
			}
			m_iEnd++;
			break;
		}
		else m_strToken += c;
	}
	if ((m_strToken == "") && !m_bDelimiter) return NULL;
	return (char*)(m_strToken.GetBuffer(1));
}

char *CParser::gettokenl(BOOL bDefDel)
{
	CString token;
	m_strToken = "";
	while ( GetToken( token, NULL, bDefDel ) ) {
		char c = token.GetAt(0);
		if ( !strchr(m_strDelimiter.GetBuffer(1), c) ) {
			m_strToken = token;
			break;
		}
	}
	return (char*)m_strToken.GetBuffer(1);
}

