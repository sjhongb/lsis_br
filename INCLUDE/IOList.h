// IOList.h : header file
//

#ifndef   _IOLIST_H
#define   _IOLIST_H

#include "ioinfo.h"
#include "strlist.h"
#define	MAX_IOBITMAP			(8 * 8 * 32) + 0x100
#define MAX_CTC_ELEMENT_SIZE	2048
#define MAX_CONFIG_LIST_SIZE	128

/////////////////////////////////////////////////////////////////////////////
// CIOList dialog

class CLinkTable : public CObject{
public:
	short Offset;
	short RdfOffset;
	short ScrOffset;
	short Type;
	CString   Name;
	short Find(CString  name){
		return (Name == name);
	};
} ;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
	union {
		unsigned short  sInfo;
		struct	{
			unsigned short  BitPos	: 14;
			unsigned short  Type	: 2;
		};
	};
} IOBitMatchInfoType;

/////////////////////////////////////////////////////////////////////////////

class CIOList : public CDialog
{
// Construction
public:	
	BYTE * GetRealAddressPtr();
	void AssignError( int eno, CString &str );
	BOOL OnFileSave();
	void CreateDefaultIO();
	~CIOList();
	CIOList(CString &strPath, CObList *pInfo, CWnd* pParent = NULL);   // standard constructor

	CString m_strFilePath;
	CIOInfo m_Document;
	CObList *m_pListInfo;
	CMyStrList m_DefaultIO;
	CMyStrList *m_pSignal, *m_pTrack, *m_pSwitch, *m_pGeneral;

	void RdfTableSave();
	void TableSave();
	CLinkTable* FindLinkTable(CString name, short type );
	void MakeTableRdf();
	void MakeTableRdfDo( short loc, IOCardType *pCard, int k );

	CObArray m_pTableList;
	CLinkTable *m_pTable;
	CObArray m_pTableListRdf;
	IOBitMatchInfoType	m_pAssignTable[MAX_IOBITMAP];
	WORD m_pCtcElementTable[MAX_CTC_ELEMENT_SIZE];	// for CTC
	WORD m_pConfigListTable[MAX_CONFIG_LIST_SIZE];

	short m_nOffsetSignal;		// VMEM 상의 각 오브젝트 
	short m_nOffsetTrack;		// 오프셋
	short m_nOffsetSwitch;		// 각 오브젝트별로 따로 있음.
	short m_nOffsetGeneral;		
    short m_nOffsetSystemInput;

	BYTE* m_pIOCardInfo;	// 랙 별 IO Card TYPE 목록(EITInfo.h 의 16 Byte 버퍼 포인트)

// Dialog Data
	//{{AFX_DATA(CIOList)
	enum { IDD = IDD_DIALOG_IOLIST };
	CListCtrl	m_listNoAssIO;
	CListCtrl	m_listModule;
	CListCtrl	m_listAssIO;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIOList)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnMaketable();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif // _IOLIST_H
