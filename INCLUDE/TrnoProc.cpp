// Trnoproc.cpp : implementation file
//

#include "stdafx.h"
#include "TrnoProc.h"

TrNoType *_pTrainNos = NULL;

BOOL CmpTrainNo(int nid, char *trno)	// FALSE/same too, True/difference
{
	if ( !_pTrainNos ) return FALSE;
	char *p = &_pTrainNos[ nid ].TrNos[0];
	for(int i=0; i<5; i++) {
		if (*p != *trno) { 
			return TRUE;
		}
		if ( !*trno ) {
			break;
		}
		else {
			p++;
			trno++;
		}
	}
	return FALSE;
}

void SetTrainNo(int nid, char *trno)
{
	if ( !_pTrainNos ) return;
	char *p = &_pTrainNos[ nid ].TrNos[0];
	for(int i=0; i<5; i++) {
		*p = *trno;
		p++;
		trno++;
	}
}

char *GetTracinNoHead(CLOGRecord &log)
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	char *ptr = (char*)&log.m_BitBlock[ pApp->nSfmRecordSize + MAX_OPERATE_CMD_CODESIZE ];	// Track no(5Byte), ID(1Byte), Cmd(1Byte)
	return ptr;
}

/////////////////////////////////////////////////////////////////////////////

BOOL IsExsitTrainNo( BYTE *pData ) 
{
	if ( !pData ) return FALSE;
	char c;
	BOOL bRet = FALSE;
	for(int i=0; i<5; i++) {
		c = *pData++;
		 if (c != ' ') {
			if ( c ) bRet = TRUE;
			break;
		}
	}
	return bRet;
}

CString GetTrainNoInCmd( BYTE *pData )
{
	CString string = "";
	
	if ( pData ) {
		int  i;
		char str[10];
		for(i=0; i<5; i++) {
			str[i] = *pData++;
			if ( str[i] == 0x00 ) break;
		}
		str[i] = 0x00;
		string = str;
	}
	return string;
}

/////////////////////////////////////////////////////////////////////////////

BOOL IsChangeUnitCmd(BYTE *pOld, BYTE *pNew)
{
	UnitCommandType *pOldCmd = (UnitCommandType*)pOld;			// MAX_OPERATE_CMD_CODESIZE size
	UnitCommandType *pNewCmd = (UnitCommandType*)pNew;			// MAX_OPERATE_CMD_CODESIZE size
	// Operator Command check
	if ((pNewCmd->nSTX >= 0x40) && (pNewCmd->nSTX <= 0x4f)) {
		if ((pNewCmd->nSTX != pOldCmd->nSTX) || (pNewCmd->nCmd != pOldCmd->nCmd)
											 || (pNewCmd->nIDl != pOldCmd->nIDl)) {
			return TRUE;
		}
	} 
	return FALSE;
}

BOOL IsChangeUnitMsg(BYTE *pOld, BYTE *pNew)
{
	UnitMessageType *pOldMsg = (UnitMessageType*)pOld;			// MAX_OPERATE_CMD_UNITCODE size
	UnitMessageType *pNewMsg = (UnitMessageType*)pNew;			// MAX_OPERATE_CMD_UNITCODE size
	// UnitMessage check
	if (pNewMsg->nCmd == 0xff) 
		return TRUE;
	else if ( CmpTrainNo((int)(pNewMsg->nCmd), (char*)&pNewMsg->nTrno[0]) )
		return TRUE;
	else 
		return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

