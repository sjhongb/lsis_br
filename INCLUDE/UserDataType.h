// UserDataType.h : header file
//

#ifndef   _USERDATATYPE_H
#define	 _USERDATATYPE_H
/////////////////////////////////////////////////////////////////////////////

typedef union  {
	long lParam;
	long wParam;
	struct {
		BYTE d1;
		BYTE d2;
		BYTE d3;
		BYTE d4;
	};
	struct {
		WORD ndl;
		WORD ndh;
	};
} ParamType;
 
typedef union {
	LPARAM lParam;
	struct {
		LPARAM count	: 16;	// 키보드가 눌린 횟수
		LPARAM scancode :  8;	// OEM 키값 
		LPARAM extended	:  1;	// 오른쪽 ALT, CTRL를 포함한 101, 102 키 및 기타 확장키들이 눌린 상태값 
		LPARAM notused	:  4;	// 사용안함
		LPARAM alt		:  1;	// ALT 눌린 상태
		LPARAM prev		:  1;	// 이전 키값이 있는 경우
		LPARAM trans	:  1;	// 전송 상태값(WM_SYSKEYDOWN 에 대해서는 항상 0)
	} K;
} lKeyType;

typedef struct {
	BOOL bValid;
	int  nLeft;
	int  nTop;
	int  nCX;
	int  nCY;
	short nOption;
} WndRectType;

typedef struct tagSystemParameterType {
	CString strStationRealName;
	CString strStationFileName;
	CString strCurrentOperator;
	CString strLockOperator;
	CString strLSMResolution;
	BOOL bSimulate;
	BOOL bOperate;
	BOOL bCtrlLock;
	BOOL bSound;
	BOOL bReplay;
	BOOL bMsgHelp;
	BOOL bAnyMenu;
	BOOL bSharedMemory;
	short nProcOption;
	short nOption;
	short nMainStartX;		
	short nMainStartY;		
	short nMainSizeX;		
	short nMainSizeY;	
	short nMenuStartX ;		
	short nMenuStartY;		
	short nMenuSizeX;		
	short nMenuSizeY;	
	short nLogStartX;		
	short nLogStartY;		
	short nLogSizeX;		
	short nLogSizeY;	
	short nLSMHeight;	
	short nComport1;
	short nComport2;
	CString strGPSComport;
	int nBaudrate;
	int nUserNumber;
	int nCallonCounter;
	int nRouteRelease;
	int nB2AXLCounter;
	int nB4AXLCounter;
	int nExternPower;
	int nSystemType;
	int nSystemNo;
	BOOL bTestMode;
	BOOL bVoice;
	BOOL bLanUse;
	int nTRCID;
	int nMode;
	CString strNet1Local;
	CString strNet2Local;
	CString strNet1PriCBI;
	CString strNet2PriCBI;
	CString strNet1SecCBI;
	CString strNet2SecCBI;
	BOOL bIOSim;
} SystemParameterType;

typedef struct {
	char UserID[8];
	char UserPassword[8];
} User;

typedef struct {
	char cHeader;
	char cOP;
	long lDate;
	int  CallOnCounter;
	int  RouteReleaseCounter;
	int  DataCounter;
	User UserData[10];
} UserDataType;

// WM_COMMAND transfer struct type
typedef struct {
	int	  Port;
	int   Length;
	BYTE *pData;
} COMCommandType;

typedef struct tagLoginItem {
	CString strID;
	CString strPassword;
} LoginItem;

typedef struct tagEid_AppOptionType {
	CString strStationHName;
	CString strStationEName;
	CString strSubWndName;
	CString strMainWndMetrix;
	CString strSubWndMetrix;
	CString strOperator;
	CString strCtrlLock;
	BOOL bMainTitle;
	BOOL bSimulate;
	BOOL bOperate;
	BOOL bCtrlLock;
	BOOL bSound;
	BOOL bReplay;
	BOOL bMsgHelp;
	short nCommPort;
	short nProcOption;
} Eid_AppOptionType;

typedef struct tagLLR_AppOptionType {
	CString strStationHName;
	CString strStationEName;
	CString strSfmMetrix;
	WndRectType SfmWnd;
	WndRectType RecWnd;
} LLR_AppOptionType;

/////////////////////////////////////////////////////////////////////////////
// Defined Processing Configure
// flag  loggings
#define PROC_NO_MAKEMSG			0x001
#define PROC_NO_LOGWRITE		0x002
#define PROC_NO_VIEWLIST		0x004
// flag menus
#define PROC_NO_UPSYSTEM		0x010
#define PROC_NO_UPSIGNAL		0x020
#define PROC_NO_UPTRACK			0x040
#define PROC_NO_UPSWITCH		0x080
// flag LSM
#define PROC_NO_UPSCREEN		0x100
// flag ctrl
#define PROC_ON_SETTRACK		0x1000
// flag comm mode
#define LAN_COMMUNICATION		1
#define TRC_COMMUNICATION		2
#define SERIAL_COMMUNICATION	3
/////////////////////////////////////////////////////////////////////////////

#endif // _USERDATATYPE_H
