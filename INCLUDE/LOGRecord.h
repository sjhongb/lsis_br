// LOGRecord.h : interface of the CLOGRecord class
//
/////////////////////////////////////////////////////////////////////////////


#ifndef   _LOGRECORD_H
#define   _LOGRECORD_H

#include "MessageNo.h"
#include "ScrInfo.h"
#ifdef _LLR
#include "../LLR/LLR.h"
#else
#include "../LES/LES.h"
#endif
#include "../LES/LOGFilter.h"

/////////////////////////////////////////////////////////////////////////////
// Record Version.
#define REC_VER_101		101		
#define REC_VER_200		200	

/////////////////////////////////////////////////////////////////////////////
// 
//#define LOG_RECTYPE_BASE	BYTE('B')
#define LOG_RECTYPE_ALL		BYTE('A')	
#define LOG_RECTYPE_PART	BYTE('P')
#define LOG_RECTYPE_SAME	BYTE('S')

/////////////////////////////////////////////////////////////////////////////

typedef union {
	unsigned short Addr;
	struct {
		unsigned short nPos:	12;
		unsigned short nRep:	4;
	};
} PartAddrType;

/////////////////////////////////////////////////////////////////////////////
//class CLOGFile;

typedef struct tagLOGHeaderType {
	BYTE  Type;					// All('A'), Part('P'), Same('S')
	union {
		BYTE State;
		struct {
			BYTE Flag :	4;		// status(D2), error(D1), control(D0), 0 is Same.
			BYTE Order:	3;		// Record order in Sec
			BYTE IsVersion:	1;	// 버전 데이터 사용 여부(Ver 1.01 부터 Set)
		};
	};
	short Length;				// Data record size
	//
} LOGHeaderType;

/////////////////////////////////////////////////////////////////////////////

class CLOGRecord //: public CObject
{
//	DECLARE_DYNCREATE(CLOGRecord)	

// Attributes
public:
	LOGHeaderType	m_Head;
	short m_nVersion;				// Record Version.
	union {
		DataHeaderType	m_SysHead;	// header data
		BYTE m_BitBlock[MAX_MSGBLOCK_SIZE];	// logging data(status or message)+Index item.
	};
	DWORD m_dwPosition;

#ifdef _LLR
	CLLRApp* pApp;
#else 
	CLESApp* pApp;
#endif

// Operations
public:
	void RecordOR( CLOGRecord & );
	void InitEventClear();
	void SetRecord(BYTE *record, int length);
	void Write(FILE *fp);
	void Write(CFile *fp);
	int  Read(FILE *fp, BOOL bVersion);
	UINT Read(CFile *fp, BOOL bVersion);
	int  MaxExtSize()
	{
		return(m_Head.Length+sizeof(short)+sizeof(LOGHeaderType));	// head, version, sfm record(include CMD 16 byte, EXT time)
	}
public: // create from serialization only
	CLOGRecord();
	CLOGRecord(CLOGRecord &Record);

// Implementation
public:
	CLOGRecord& operator=(const CLOGRecord &Record);

public:
	~CLOGRecord();
};

/////////////////////////////////////////////////////////////////////////////
// CLOGRecord commands

#endif // _LOGRECORD_H

