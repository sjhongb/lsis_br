//=======================================================
//==              StrList.h
//=======================================================
//	파 일 명 :  StrList.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  궤도,전철기,신호기 등 스트링 리스트 
//
//=======================================================

#ifndef _STRLIST_H
#define _STRLIST_H

#include "stdafx.h"

class CMyStrList : public CObList {
public:
	void ImportFromFileFixed16( FILE *fp, short count );
	void ExportToFileFixed16( FILE *fp );
	CString * GetAtIndex( int nIndex );
	void RemoveAtIndex( int nIndex );
	void RemoveString( CString strTemp );
	CString* GetNextString( CString &str );
	int FindIndex( CString &str );
	void Sort( int methode );
	virtual void Dump(CDumpContext& dc) const;
	BOOL Modify(CString &mod , int iType = 0 );
	BOOL Compare(CString& result, CMyStrList* other);
	void Purge();
	BOOL CheckAdd(CString& str, BOOL add = TRUE);
	BOOL CheckAddTail(CString& str, BOOL add = TRUE);
	BOOL CheckAddHead(CString& str, BOOL add = TRUE);
	BOOL CheckPointAdd(CString& str, BOOL add = TRUE);
	BOOL CheckTrackAdd(CString& str, BOOL add = TRUE);
	BOOL  GetRealCount();
	BOOL CheckIns(CString& str, BOOL ins = TRUE);
	virtual CObject *RemoveTail();
	POSITION AddTail(CString& str);
	POSITION AddHead(CString& str);
	~CMyStrList();
	const CMyStrList& operator = (CMyStrList &src);
	const BOOL operator == (CMyStrList &src);
	const CMyStrList& operator += (CMyStrList &src);
	void Draw(CDC *pDC, int x, int y);
};

#endif