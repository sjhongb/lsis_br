// EventFile.cpp: implementation of the CEventFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EventFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEventFile::CEventFile()
{
	m_bIsOpen = FALSE;
}

CEventFile::~CEventFile()
{
	
}

int CEventFile::Open(CString strFilePath, CString strStnName, BOOL bReadOnly)
{
	BOOL bIsNew = TRUE;

	if(m_bIsOpen)
	{
		Close();
	}

	m_strFilePath = strFilePath;
	m_strStnName = strStnName;

	if(bReadOnly)
	{
		if(!m_File.Open(m_strFilePath, CFile::modeRead | CFile::shareDenyNone | CFile::typeBinary))
		{
			return 0;
		}
	}
	else
	{
		if(!m_File.Open(m_strFilePath, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::shareDenyWrite | CFile::typeBinary))
		{
			return 0;
		}
	}

	char pBuffer[2048] = {0,};
	MessageEvent *pEvent = NULL;
		
	m_File.SeekToBegin();

	// 파일이 이미 있는 경우에 첫 4바이트를 읽어본다.
	if(m_File.Read(pBuffer, 28) != 0)
	{
		// 아스키 값이 그대로 노출되지 않도록 
		for(int i = 0 ; i < strStnName.GetLength() ; i++)
		{
			pBuffer[24 + i] &= 0x7F;
		}

		if(strStnName == (char*)&pBuffer[24])
		{
			bIsNew = FALSE;
		}
		else
		{
			m_File.Close();

			if(bReadOnly)
			{
				return 0;
			}

			// 파일을 덮어씌우고 새로 만든다.
			if(!m_File.Open(m_strFilePath, CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite | CFile::typeBinary))
			{
				return 0;
			}
		}
	}

	if(bIsNew)
	{
		memset(pBuffer, 0, sizeof(pBuffer));
		strncpy(pBuffer, "LSIS EVT File Ver. 1.0", 24);

		for(int i = 0 ; i < strStnName.GetLength() ; i++)
		{
			pBuffer[24 + i] = strStnName[i] | 0x80;
		}
		
		m_File.Write(pBuffer, 28);
	}
	else
	{
		while(m_File.Read(pBuffer, sizeof(MessageEvent)) != 0)
		{
			pEvent = new MessageEvent;

			memset(pEvent, 0, sizeof(MessageEvent));
			memcpy(pEvent, pBuffer, sizeof(MessageEvent));

			m_List.AddTail((CObject*)pEvent);
		}
	}

	m_bIsOpen = TRUE;
	return 1;
}


int CEventFile::Close()
{
	if(!m_bIsOpen)
	{
		return 0;
	}

	m_List.RemoveAll();
	m_File.Close();

	m_bIsOpen = FALSE;

	return 1;
}

int CEventFile::GetAllRecords(CObList &obList)
{
	obList.AddTail(&m_List);

	return m_List.GetCount();
}

int CEventFile::GetPeriodRecords(CObList &obList, CJTime &startTime, CJTime &endTime)
{
	MessageEvent *pEvent = NULL;
	POSITION pos = m_List.GetHeadPosition();
	
	int nRecord = 0;

	while(pos != NULL)
	{
		pEvent = (MessageEvent*)m_List.GetNext(pos);
		if(pEvent != NULL)
		{
			CJTime &recordTime = pEvent->time;

			if(recordTime.GetDays() == startTime.GetDays())
			{
				if(recordTime.GetSecInDay() < startTime.GetSecInDay())
				{
					delete pEvent;
					continue;
				}
			}
			
			if(recordTime.GetDays() == endTime.GetDays())
			{
				if(recordTime.GetSecInDay() > endTime.GetSecInDay())
				{
					delete pEvent;
					continue;
				}
			}

			obList.AddTail((CObject*)pEvent);

			nRecord++;
		}
	}
	
	return nRecord;
}

int CEventFile::Add(MessageEvent *pNewEvent)
{
	if(m_bIsOpen)
	{
		m_List.AddTail((CObject*)pNewEvent);

		m_File.SeekToEnd();
		m_File.Write(pNewEvent, sizeof(MessageEvent));
	}

	return 1;
}


int CEventFile::GetRecordCount()
{
	return m_List.GetCount();
}
