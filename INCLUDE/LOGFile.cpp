//=======================================================
//==              LOGFile.cpp
//=======================================================
//  파 일 명 :  LOGFile.cpp
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :  1.0
//  설    명 :  로그파일의 생성및 처리를 담당 한다. 
//
//=======================================================

#include "stdafx.h"
#include <iostream.h>
#include <fstream.h>
#include <direct.h>
#include "TrnoProc.h"
#include "LOGRecord.h"
#include "LOGFile.h"
#ifdef _LLR
#include "../LLR/LLR.h"
#else
#include "../LES/LES.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CLOGRecord	 _pRecordBlk[_MAX_RECORD_MINUTE+1];

//=======================================================
//
//  함 수 명 :  MakeDir
//  함수출력 :  없음 
//  함수입력 :  CJTime time
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  로그 파일을 저장하기 위한 디렉토리를 만든다. 
//
//=======================================================
void CLOGFile::MakeDir(CJTime time)
{
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif

	CString stringDir = (const char *)&pApp->m_LOGDirInfo[0];

	stringDir += '\\';
	stringDir += time.Format("%Y");
	_mkdir( LPCTSTR(stringDir) );	// Make Year directory

	stringDir += '\\';
	stringDir += time.Format("%m");
	_mkdir( LPCTSTR(stringDir) );	// Make Month directory
}

//=======================================================
//
//  함 수 명 :  RecordPartToAll
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CLOGFile::RecordPartToAll(CLOGRecord &destR, CLOGRecord &scrR, CLOGRecord &oldR)  // part -> all
{
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif

	int len;
	PartAddrType ID;

	ID.Addr = 0;
	BYTE *pOld = oldR.m_BitBlock; 
	BYTE *pCur = scrR.m_BitBlock; 
	BYTE *pNew = destR.m_BitBlock;
	UnitMsgType *OldMsg = (UnitMsgType*)&pOld[pApp->nSfmRecordSize];
	UnitMsgType *NewMsg = (UnitMsgType*)&pNew[pApp->nSfmRecordSize];

	len =  pApp->nVerRecordSize;
	memcpy(pNew, pCur, len);
	destR.m_Head = scrR.m_Head;
	destR.m_Head.Length = len;
}

//=======================================================
//
//  함 수 명 :  isAlterRecordData
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  시스템 값에 변화가 있으면 TRUE 리턴 변화가 없으면 FALSE 리턴 
//
//=======================================================
BOOL isAlterRecordData( CLOGRecord &OldRec, CLOGRecord &ScrRec, BOOL bChkBlock )
{
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif

	int  i;
	int  max=sizeof(DataHeaderType);
	BYTE *pOld = &OldRec.m_BitBlock[0];
	BYTE *pScr = &ScrRec.m_BitBlock[0];

	// 시스템 데이터 검사 
	for(i=0; i<2; i++) {
		if ( *pOld != *pScr ) {
			return TRUE;		// 주,부계 또는 메시지 타입 변화 있음.
		}
		pOld++;
		pScr++;
	}
	// Timer 데이터 검사 제외.
	i = 8;		
	pOld += 6;
	pScr += 6;
	
	for(; i<13; i++) {			//2001.07.02 - jin. 12 -> 13 변경(Cycle검사).
		if ( *pOld != *pScr ) {
			return TRUE;		// SystemStatusType 의 'nSysVar' 변수 변화 있음.
		}
		pOld++;
		pScr++;
	}
	for(; i<max; i++) {			// 시스템 데이터 마지막 IOC1, IOC2 블럭까지 포함하여 검사.
		if ( *pOld != *pScr ) {
			return TRUE;		// Cycle 을 제외한 부분에 변화가 있으면 TRUE
		}
		pOld++;
		pScr++;
	}
	if ( bChkBlock ) {
	// 기타의 통신 데이터 검사 
		max = pApp->nVerRecordSize;
		for(; i<max; i++) {		//
			if ( *pOld != *pScr ) {
				return TRUE;		// Object list, op command, unit message 부분의 변화 있으면 TRUE.
			}
			pOld++;
			pScr++;
		}
	}
	return FALSE;
}


//=======================================================
//
//  함 수 명 :  RecordAllToPart
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
BOOL CLOGFile::RecordAllToPart(CLOGRecord *destR, CLOGRecord *scrR, CLOGRecord *oldR) // all -> part
{
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif
	int  len;

	destR->m_Head = scrR->m_Head;
	destR->m_Head.Length = 0;
	destR->m_nVersion = scrR->m_nVersion;

	len = pApp->nVerRecordSize;
	destR->m_Head.Length = len; 
	memcpy(destR->m_BitBlock, scrR->m_BitBlock, len);		
	destR->m_Head.Type = LOG_RECTYPE_ALL;
	return TRUE;
}


//=======================================================
//
//  함 수 명 :  CLOGFile
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
CLOGFile::CLOGFile()
{
	InitID();
	m_bSpecialPath = FALSE;
}

//=======================================================
//
//  함 수 명 :  CLOGFile
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
CLOGFile::CLOGFile(const char *pszMode)
{
	InitFile( pszMode );
}

/* // 2006년 7월 22일 LOG 기록안되는 문제 때문에 삭제 					
CLOGFile::CLOGFile(char *pszFile)
{
	InitLogFile( pszFile );
}
*/

//=======================================================
//
//  함 수 명 :  ~CLOGFile
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
CLOGFile::~CLOGFile()
{
}

const char *_pVerString = "LSIS LOG FILE VER 2.00BR";

//=======================================================
//
//  함 수 명 :  InitID
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CLOGFile::InitID()
{
	strcpy((char *)&m_stIndex.pID[0], _pVerString);

	int n = MAX_IDBYTE - 5; // #define MAX_IDBYTE			32
	m_stIndex.pID[n++] = 0x1a;
	m_stIndex.pID[n++] = 0x01;
	m_stIndex.pID[n++] = 0x02;
	m_stIndex.pID[n++] = 0x03;
	m_stIndex.pID[n]   = 0x04;
}

//=======================================================
//
//  함 수 명 :  ReadIndex
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int  CLOGFile::ReadIndex( FILE *fp )
{
	if ( fp == NULL ) return -2;	// None file pointer.
	int i, length;
	if ( !fseek( fp, 0, SEEK_SET) ) {
		length = fread(&m_stIndex, SIZE_LOGINDEX, 1, fp);
		if ( strcmp((char*)&m_stIndex.pID[0], _pVerString) ) {
			fseek( fp, 0, SEEK_SET);
			length = fread(&m_stIndex.table[0], SIZE_INDEXTABLE, 1, fp);
			m_stIndex.pID[0] = 0x00;
		}
	}
	else {
		GenError( 1, "Seek Error" );
	}
	if ( !length ) i = -1;
	else {
		for(i=MAX_INDEX_RECORD-1; i>=0; i--) {	
			if (m_stIndex.table[i] != -1) break;
		}
	}
	m_stIndex.table[MAX_INDEX_RECORD] = -1;
	m_lLastIndex = i+1;
	return length;
}

//=======================================================
//
//  함 수 명 :  WriteIndex
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int  CLOGFile::WriteIndex( FILE *fp )
{
	if ( fp == NULL ) return -2;	// None file pointer.
	if( !fseek(fp, 0, SEEK_SET) ) {
		if ( strcmp((char*)&m_stIndex.pID[0], _pVerString) ) 
			return fwrite(&m_stIndex.table[0], SIZE_INDEXTABLE, 1, fp);
		else
			return fwrite(&m_stIndex, SIZE_LOGINDEX, 1, fp);
	}
	else {
		GenError( 2, "Seek Error" );
	}
	return 0;
}

//=======================================================
//
//  함 수 명 :  InitIndex
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CLOGFile::InitIndex()
{
	for(int i=0; i<=MAX_INDEX_RECORD; i++) {
		m_stIndex.table[i] = -1;		
	}
	m_lLastIndex = 0;
}

//=======================================================
//
//  함 수 명 :  GetInde
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
long CLOGFile::GetIndex(int index)
{
	if (index <= m_lLastIndex) {
		return m_stIndex.table[index];
	}
	return -1;
}

//=======================================================
//
//  함 수 명 :  SetIndex
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int  CLOGFile::SetIndex(int index, long seek, FILE *fp)
{
	if (m_stIndex.table[index] == -1) {			//	if (m_IndexTable[index] == -1) {
		m_stIndex.table[index] = seek;			//		m_IndexTable[index] = seek;
		if ( fp ) {
			WriteIndex( fp );
		}
		if (index >= m_lLastIndex) {
			m_lLastIndex = index + 1;
		}
		return 0;
	}
	return 1;
}

//=======================================================
//
//  함 수 명 :  GenError
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CLOGFile::GenError( int no, CString &strMessage )
{
	GenError( no, (LPCTSTR)(strMessage) );
}

//=======================================================
//
//  함 수 명 :  GenErro
//  함수출력 :
//  함수입력 :
//  작 성 자 :  omani
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CLOGFile::GenError( int no, LPCTSTR pcszMessage )
{
	int  n = m_strName.GetLength();
	CString fName = m_strName.Left(n-3);
	fName += "TXT";

	ofstream os( fName, ios::app );
	if ( !os.is_open() ) return;
	CString str = (CJTime::GetCurrentTime()).Format("\n%Y.%m.%d-%H:%M:%S : ");
	os << (LPCTSTR)(str) << no << " - " << pcszMessage;
}

void CLOGFile::InitFile(const char *pszMode)
{
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif

	CString strSpecialPath;
	m_strMode = pszMode;

	if(m_strMode.Find(":\\",0) > -1)
	{
		m_strPath = pszMode;
		m_strMode = "rb";
		strSpecialPath = m_strPath;
		m_bSpecialPath = TRUE;
	}
	else
	{
		m_strPath = (const char *)&pApp->m_LOGDirInfo[0];
		strSpecialPath = "";
		m_bSpecialPath = FALSE;
	}

	CJTime st = CJTime::GetCurrentTime();
	m_lLastTime = st.GetTime();	
	
	m_lCurSeek = 0;

	BOOL bMakePath = (pszMode[0] == 'a') ? TRUE : FALSE;
	UpdatePath( st, bMakePath, strSpecialPath );

	m_bFind = FALSE;
	m_bLastRecord = FALSE;
}

BOOL CLOGFile::IsStrIDVersion()
{
	if ( strcmp((char*)&m_stIndex.pID[0], _pVerString) ) 
		return FALSE;
	else 
		return TRUE;
}

BOOL CLOGFile::IsUpdateDate(CJTime scrt)
{
	CJTime lastt(m_lLastTime);
	long lCurDays  = scrt.GetDays();
	long lLastDays = lastt.GetDays();
	if (lCurDays != lLastDays) 
		return TRUE;
	else 
		return FALSE;
}

BOOL CLOGFile::UpdatePath(CJTime scrt, BOOL bMakePath, CString strPath)
{
	if ( bMakePath ) MakeDir(scrt);

	if (m_bSpecialPath == FALSE)
	{
		CString s  = scrt.Format("\\%Y\\%m\\%d");
		m_strName  = m_strPath;
		m_strName += s;
		m_strName += ".LOG";
	}

	if(strPath != "")
	{
		m_strName = strPath;
	}

	FILE *fp = FileOpen();
	if ( fp ) {
		if ( fseek( fp, 0, SEEK_END) ) 
			GenError( 3, "Seek Error" );
		long seek = ftell( fp );
		if (seek >= SIZE_INDEXTABLE) {			//		if (seek >= (sizeof(long) * MAX_INDEX_RECORD)) {
			ReadIndex( fp );
			if ( fseek( fp, 0, SEEK_END) ) 
				GenError( 4, "Seek Error" );
		}
		else if ( bMakePath ) {
			fclose( fp );
			InitIndex();
			InitID();
			fp = FileOpen(2);	// w+b
			if ( fp ) 
				WriteIndex( fp );
		}
		else {
			InitIndex();
			InitID();
		}
		m_lCurSeek = ftell( fp );
		if ( fp ) fclose( fp ); 
		return TRUE;
	}
	return FALSE;
}

FILE* CLOGFile::FileOpen(int fmode /*=0*/)	// 0/reserved, 1/r+b, 2/w+b
{
	char strfmode[10];
	if ( fmode == 1 ) strcpy(strfmode, "r+b");
	else if ( fmode == 2 ) strcpy(strfmode, "w+b");
	else strcpy(strfmode, LPCTSTR(m_strMode));
	return fopen(m_strName.GetBuffer(1), strfmode);
}


// Write function
int  CLOGFile::AttachRecord(CLOGRecord *curR, BOOL bSave, CLOGRecord *oldR /* =NULL */)	//2001.05.02 jin - Modified(Add) code.
{
	int index;
	long ltime;
	BOOL bState;
	BOOL bIndexSave;
	DataHeaderType *Head;
	CJTime *pTime;

	if ( oldR ) {
		m_woRecord = *oldR;
	}

	if (m_strMode.GetAt(0) != 'a') {	// Not append mode ?
		m_woRecord = *curR;		// Backup Data record
		return -1;	// 추가 저장 모드가 아니다.
	}
	if ( !bSave ) {
		m_woRecord = *curR;		// Backup Data record
		return 0;	// 저장하지 않는 경우 복귀.
	}
	// 변화된 데이터 검사 및 압축 데이터 생성.
	bState = RecordAllToPart(&m_waRecord, curR, &m_woRecord);
	if ( !bState ) {	
		m_woRecord = *curR;		// Backup Data record
		return 0;	// OLD 레코드와 NEW 레코드의 데이터가 같다.
	}

	m_woRecord = *curR;		// Backup Data record

	Head = &curR->m_SysHead;
	pTime = (CJTime*)&Head->nSec;
	ltime = pTime->GetTime();
	index = pTime->GetMinuteInDay();
	// Option is Saving Mode
	if (m_lLastIndex <= 0) bIndexSave = TRUE;				// 파일내 저장 데이터가 없는 경우 ?
	else if ( GetIndex(index) < 0 ) bIndexSave = TRUE;		// 해당 분(minute)의 인덱스가 없는 경우 ?
	else bIndexSave = FALSE;								// 그외.

	// Check & update file name for the update Year, Month, day data. And Open Logging file 
	if ( IsUpdateDate( *pTime ) ) {
		UpdatePath( *pTime, TRUE );
	}
	int  ret = 1;
	FILE *fp = FileOpen();
	if ( fp ) {
		if ( bIndexSave ) {		// 분당 강제 저장 
			fclose( fp );
			fp = FileOpen(1);						// 인덱스 저장을 위해 다시 열기 수행함(read & write).
			if ( fp ) {
				SetIndex(index, m_lCurSeek, fp);	// index 정보 셋트 및 저장
				fclose( fp );
			}
			fp = FileOpen();						// 데이터 저장을 위해 다시 열기 수행(appended)
			if ( fp ) {
				if ( !fseek(fp, 0, SEEK_END) ) {
					if ( strcmp((char*)&m_stIndex.pID[0], _pVerString) ) {
						curR->Write( fp );				// 분(minute)의 바로 현재 데이터 All 저장(Before Ver 2.00)
					}
					else {
						m_woRecord.Write( fp );			// 분(minute)의 바로 이전 데이터 All 저장(After Ver 2.00)
						bIndexSave = FALSE;
					}
				}
				else 
					GenError( 5, "Seek Error - Old" );
				m_lCurSeek = ftell( fp );
			}
			else {
				GenError( 11, "Open Error - Ignore writing(Index)" );
				bIndexSave = FALSE;
			}
		}
		if ( fp ) { 
			if ( !fseek(fp, 0, SEEK_END) ) {
				if ( !bIndexSave )
					m_woRecord.Write( fp );				// Current 데이터 저장
			}
			else 
				GenError( 5, "Seek Error" );
			m_lCurSeek = ftell( fp );
			fclose( fp );
			m_lLastTime = ltime;	// Last time for file data.
		} else {
			GenError( 11, "Open Error - Ignore writing(Alter)" );
			ret = -1;
		}
	} else {
		GenError( 11, "Open Error - Ignore writing(append)" );
		ret = -1;
	}

	m_woRecord = *curR;		// Backup Data record

	return ret;
}

// Search Option Set Function
void CLOGFile::SetRange( CJTime st, CJTime et )
{
	m_lTimeStart  = st.GetTime();
	m_lTimeEnd    = et.GetTime();
	m_lTimeSearch = m_lTimeStart;
	m_lTimeIndex  = st.GetMinuteInDay();
	m_bFind = FALSE;
}

int  CLOGFile::SetFindOneday( CJTime searcht, BOOL bNext, CJTime *pSTime /*= NULL*/, CJTime *pETime /*= NULL*/ )
{
	if ( !pSTime ) {
		CJTime st(2000, 1, 1, 0, 0, 0);
		m_lTimeStart = st.GetTime();
	}
	else 
		m_lTimeStart = pSTime->GetTime();
	if ( !pETime ) {
		CJTime et = CJTime::GetCurrentTime();
		m_lTimeEnd = et.GetTime();
	}
	else 
		m_lTimeEnd = pETime->GetTime();

	m_lTimeSearch = searcht.GetTime();

	if (m_lTimeSearch < m_lTimeStart) 
		return 0;
	if (m_lTimeSearch > m_lTimeEnd) 
		return 0;
	
	BOOL bFirst = TRUE;

	if ( UpdatePath(CJTime(m_lTimeSearch), FALSE) ) {
		if (m_lLastIndex > 0) {
			searcht = m_lTimeSearch;
			m_lTimeIndex  = searcht.GetMinuteInDay();
			m_bFind = TRUE;
			return 1;
		}
	}
	return 0;
}

int  CLOGFile::SetFind( CJTime searcht, BOOL bNext, CJTime *pSTime /*= NULL*/, CJTime *pETime /*= NULL*/ )
{
	if ( !pSTime ) {
		CJTime st(2000, 1, 1, 0, 0, 0);
		m_lTimeStart = st.GetTime();
	} else {
		m_lTimeStart = pSTime->GetTime();
	}

	if ( !pETime ) {
		CJTime et = CJTime::GetCurrentTime();
		m_lTimeEnd = et.GetTime();
	} else {
		m_lTimeEnd = pETime->GetTime();
	}

	m_lTimeSearch = searcht.GetTime();

	if (m_lTimeSearch < m_lTimeStart) 
		return 0;
	if (m_lTimeSearch > m_lTimeEnd) 
		return 0;
	
	BOOL bFirst = TRUE;
	long lt;
	do {
		if ( UpdatePath(CJTime(m_lTimeSearch), FALSE ) ) {
			if (m_lLastIndex > 0) 
				break;
		}
		// none file.
		if ( bNext ) {		// Next Search
			if ( bFirst ) {
				lt = m_lTimeSearch % MAX_SECOND_FOR_DAY;
				m_lTimeSearch += (MAX_SECOND_FOR_DAY - lt);	// next day 
			}
			else 
				m_lTimeSearch += MAX_SECOND_FOR_DAY;		// add one day.
			if (m_lTimeSearch > m_lTimeEnd) 
				return 0;
		} else {				// Prev Search
			if ( bFirst ) {
				lt = m_lTimeSearch % MAX_SECOND_FOR_DAY;
				m_lTimeSearch -= lt + 1;					// Prev day
			} else {
				m_lTimeSearch -= MAX_SECOND_FOR_DAY;		// sub one day
			}

			if (m_lTimeSearch < m_lTimeStart) {
				return 0;
			}
		}
		bFirst = FALSE;
	} while ( 1 );
	searcht = m_lTimeSearch;
	m_lTimeIndex  = searcht.GetMinuteInDay();
	m_bFind = TRUE;
	return 1;
}

// Search Function
int  CLOGFile::FindRecord( BOOL bDayNext )
{
	if ( bDayNext ) {
		long daytime = m_lTimeSearch % MAX_SECOND_FOR_DAY;
		m_lTimeSearch += MAX_SECOND_FOR_DAY - daytime;
		m_lTimeIndex = 0;
	}
	if (m_lTimeSearch > m_lTimeEnd) {
		return LOG_SEARCH_OVER;
	}
	if ( !UpdatePath( CJTime(m_lTimeSearch), FALSE) ) {	// none file.
		return FindRecord( TRUE );
	}
	else if (m_lLastIndex < 1) {	// none data in file 
		return FindRecord( TRUE );			
	}
	else {
		long endindex = m_lLastIndex - 1;
		int find = 0;
		m_lTimeIndex = (m_lTimeSearch % MAX_SECOND_FOR_DAY) / 60; // 시간당
		if ((m_lTimeSearch/MAX_SECOND_FOR_DAY) == (m_lTimeEnd/MAX_SECOND_FOR_DAY)) {	// 마지막 날짜를 검색 ?
			endindex = (m_lTimeEnd % MAX_SECOND_FOR_DAY) / 60;							// 마지막 날짜 LOG 데이터의 최종 시간 변경.
		}
		do {
			if (m_lTimeIndex > endindex) 
				break;
			else if (m_stIndex.table[m_lTimeIndex] != -1) {		//			else if (m_IndexTable[m_lTimeIndex] != -1) {
				find = 1;
			}
			else m_lTimeIndex++;
		} while( !find );
		if ( !find ) {
			return FindRecord( TRUE );  
		}
		m_bFind = TRUE;
		return LOG_SEARCH_FIND;
	}
}

BOOL CLOGFile::GetSearchTime( CJTime &SearchTime )
{
	long ltime = m_lTimeSearch - (m_lTimeSearch % MAX_SECOND_FOR_DAY);
	ltime += m_lTimeIndex * 60;
	SearchTime = ltime;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////
// NextRecord() ------- 재귀호출 함수                                                //
///////////////////////////////////////////////////////////////////////////////////////
int  CLOGFile::NextRecord( BOOL bDayNext )
{
	Sleep(0);	// for Task Switching.
	if ( !m_bFind ) return LOG_SEARCH_OVER;
	if ( bDayNext ) {
		long daytime = m_lTimeSearch % MAX_SECOND_FOR_DAY;
		m_lTimeSearch += MAX_SECOND_FOR_DAY - daytime;
		if (m_lTimeSearch > m_lTimeEnd) {
			return LOG_SEARCH_OVER;
		}
		else if ( !UpdatePath( m_lTimeSearch, FALSE) ) {	// None file.
			return NextRecord( TRUE );
		}
		else if (m_lLastIndex < 1) {	// none data in file 
			return NextRecord( TRUE );
		}
		m_lTimeIndex = (m_lTimeSearch % MAX_SECOND_FOR_DAY) / 60;
	}
	else m_lTimeIndex++;
	int find = 0;
	long endindex = m_lLastIndex - 1;
	if ((m_lTimeSearch/MAX_SECOND_FOR_DAY) == (m_lTimeEnd/MAX_SECOND_FOR_DAY)) {
		endindex = (m_lTimeEnd % MAX_SECOND_FOR_DAY) / 60;
	}
	do {
		if (m_lTimeIndex > endindex) 
			break;
		else if (m_stIndex.table[m_lTimeIndex] != -1) {		//		else if (m_IndexTable[m_lTimeIndex] != -1) {
			find = 1;
		}
		else m_lTimeIndex++;
	} while( !find );
	if ( !find ) {
		return NextRecord( TRUE );
	}
	return LOG_SEARCH_FIND;
}

///////////////////////////////////////////////////////////////////////////////////////
// PrevRecord() ------- 재귀호출 함수                                                //
///////////////////////////////////////////////////////////////////////////////////////
int  CLOGFile::PrevRecord( BOOL bDayNext )
{
	Sleep(0);	// for Task Switching.
	if ( !m_bFind ) return LOG_SEARCH_OVER;
	if ( bDayNext ) {
		long daytime = m_lTimeSearch % MAX_SECOND_FOR_DAY;
		m_lTimeSearch -= daytime+1;
		if (m_lTimeSearch < m_lTimeStart) {
			return LOG_SEARCH_OVER;
		}
		else if ( !UpdatePath( m_lTimeSearch, FALSE ) ) {	// None file.
			return PrevRecord( TRUE );
		}
		else if (m_lLastIndex < 1) {	// none data in file 
			return PrevRecord( TRUE );
		}
		m_lTimeIndex = (m_lTimeSearch % MAX_SECOND_FOR_DAY) / 60;
	}
	else m_lTimeIndex--;
	long startIndex = 0;
	int  find = 0;
	if ((m_lTimeSearch/MAX_SECOND_FOR_DAY) == (m_lTimeStart/MAX_SECOND_FOR_DAY))
		startIndex = (m_lTimeStart % MAX_SECOND_FOR_DAY) / 60;
	do {
		if (m_lTimeIndex < startIndex) 
			break;
		else if (m_stIndex.table[m_lTimeIndex] != -1) {		//		else if (m_IndexTable[m_lTimeIndex] != -1) {
			find = 1;
		}
		else m_lTimeIndex--;
	} while( !find );
	if ( !find ) {
		return PrevRecord( TRUE );
	}
	return LOG_SEARCH_FIND;
}

/////////////////////////////////////////////////////////////////////////////
// CLOGFile diagnostics

BOOL CLOGFile::IsEqualMinute(CJTime &dest, CJTime &scr)
{
	if ( (dest.nMin == scr.nMin) && (dest.nHour == scr.nHour) 
			&& (dest.nDate == scr.nDate) && (dest.nMonth == scr.nMonth) && (dest.nYear == scr.nYear) )
		return TRUE;
	else 
		return FALSE;
}


int  CLOGFile::GetRecordInMinute( CLOGRecord *fRecord, CLOGRecord &mRec, long &forSeek, BOOL bFillRecord, BOOL bUpdateIndex )
{

	if ((m_lTimeIndex < 0) || (m_lTimeIndex >= m_lLastIndex)) 
		return 0;
	if (m_stIndex.table[m_lTimeIndex] == -1)		//	if (m_IndexTable[m_lTimeIndex] == -1) 
		return 0;									//		return 0;
	
	int  flag;
	BOOL bFirstRec;
	FILE *fp;

	bFirstRec = TRUE;
	flag = LOG_SEARCH_FIND;

	if ( !forSeek ) 
		m_lCurSeek = m_stIndex.table[m_lTimeIndex];		//		m_lCurSeek = m_IndexTable[m_lTimeIndex];
	else 
		m_lCurSeek = forSeek;
	
	forSeek = 0;
	fp = FileOpen();
	if ( fp ) {
		long index, newindex;
		long oldSeek;
		long maxSeek;
		long lastseek;
		int  i, count, find;
		BOOL bVersion;
		BOOL bLastIndex;
		CLOGRecord fileRecord;
		CLOGRecord *poldRecord = &mRec;
		CJTime* poldT;
		CJTime* pnewT = (CJTime*)&m_rcRecord.m_SysHead.nSec;

		index = count = find = 0;
		bVersion = IsStrIDVersion();

		lastseek = m_stIndex.table[m_lLastIndex-1];
		bLastIndex = (m_lLastIndex == (m_lTimeIndex+1)) ? TRUE : FALSE;
		maxSeek = lastseek;
		for(i=m_lTimeIndex+1; i<m_lLastIndex; i++) {
			if ( m_stIndex.table[i] > 0 ) {
				maxSeek = m_stIndex.table[i];
				break;
			}
		}

		if ( !fseek(fp, m_lCurSeek, SEEK_SET) ) {
			do {
				oldSeek = m_lCurSeek;
				if ( !bLastIndex ) {
					if (m_lCurSeek >= maxSeek) {
						break;
					}
				}
				if ( !fileRecord.Read( fp, bVersion ) ) {
					break;
				}
				m_lCurSeek = ftell( fp );

				if (m_lCurSeek >= lastseek)
					m_bLastRecord = TRUE;
				else 
					m_bLastRecord = FALSE;
				 
				RecordPartToAll(m_rcRecord, fileRecord, *poldRecord);	// part -> all, func(dest, curr, old)

				if ( bFirstRec ) {
					bFirstRec = FALSE;
					mRec = m_rcRecord;
					poldRecord = &mRec;
				}
				else  {
					poldT = (CJTime*)&poldRecord->m_SysHead.nSec;
					if ( find && !IsEqualMinute(*pnewT, *poldT) ) {	// 분(min)이 다른가 ?
						if ( bUpdateIndex ) {
							if ((pnewT->nHour >=0) && (pnewT->nHour < 24) && (pnewT->nMin >=0) && (pnewT->nMin < 60)) {
								int ltime = (int)(pnewT->nHour) * 60;
								ltime += pnewT->nMin;
								if ( m_stIndex.table[ltime] != oldSeek ) {	//	if ( m_IndexTable[ltime] != oldSeek ) {
									forSeek = oldSeek;
								}
							}
							else 
								forSeek = oldSeek;
						}
						break;
					}
					else if ( find && (pnewT->nSec < poldT->nSec) ) {
						forSeek = oldSeek;
						break;
					}
					else {
						if ( bFillRecord ) {
							if ((flag != LOG_SEARCH_FIND) || (*poldT != *pnewT)) {
								BYTE nsec = pnewT->nSec;
								if ( nsec > 59 ) nsec = 59;
								newindex = long(nsec) * 4;
								int order = poldRecord->m_Head.Order;
								for(; index<newindex; index++) {
									if (index >= MAX_RECORD_MINUTE) break;
									order++;
									if ( !(index % 4) ) order = 0;
									fRecord[index] = *poldRecord;
									fRecord[index].m_SysHead.nSec = index / 4;
									fRecord[index].m_Head.Type = LOG_RECTYPE_SAME;
									fRecord[index].m_Head.Flag = 0x00;
									fRecord[index].m_Head.Order = order;
									fRecord[index].InitEventClear();
									find++;
									Sleep(0);	// for Task Switching.
								}
							}
						}
						fRecord[index] = m_rcRecord;
						poldRecord = &fRecord[index];
						index++;
						find++;
						if (index > MAX_RECORD_MINUTE) {
							forSeek = oldSeek;
							break;
						}
					}
				}	// bFirstRec
				Sleep(0);	// for Task Switching.
			} while(1);
			if ( bFillRecord && !feof(fp) ) {
				int order = poldRecord->m_Head.Order;
				for(; index<MAX_RECORD_MINUTE; index++) {
					order++;
					if ( !(index % 4) ) order = 0;
					fRecord[index] = *poldRecord;
					fRecord[index].m_SysHead.nSec = index / 4;
					fRecord[index].m_Head.Type = LOG_RECTYPE_SAME;
					fRecord[index].m_Head.Flag = 0x00;
					fRecord[index].m_Head.Order = order;
					fRecord[index].InitEventClear();
					find++;
					Sleep(0);	// for Task Switching.
				}
			}
		}
		fclose( fp );
		return find;
	}
	return 0;
}
