// EIPEMU.h : header file
//
#ifndef _EIPEMU_H
#define _EIPEMU_H

#include "scrinfo.h"
#ifdef _WINDOWS
#include "strlist.h"
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif

#define MSG_CTC	 0xEA

typedef unsigned long       DWORD;
typedef int                 BOOL;
//typedef unsigned char       BYTE;
typedef unsigned short      WORD;

/////////////////////////////////////////////////////////////////////////////
// CEipEmu document

typedef struct {
//	union {
//		BYTE pData[ 32 ];		// SIGTYPE TIME n T1 T2 T3 T4 T5, 접근 쇄정 테이블
//		struct {
			BYTE nSignalType;
			BYTE nDelayTime;
			BYTE nSGContSigID;          // 폐색 신호기 정보  
			BYTE nFirstTrackID;         // 진로의 첫 궤도
			BYTE nSignalTrackID;        // 신호기가 있는 궤도, 진로의 첫 궤도 앞의 궤도
			BYTE nRouteCount;
			WORD wRouteNo;
			WORD wCheckResult;			// 마지막 CheckRoute()의 결과를 저장 (중계 신호기 제어에 참조를 위해)
			BYTE m_nLamp;
			BYTE m_nLampExt;
			BYTE pReserved[20];	        // 접근 쇄정 테이블		T1 0 T2 SW1 SW2 0 T3 0 ...   2007.7.18 [11]은 접근해 오는 폐색 열차의 Home진로 approaching을 위해 설정한다.
//		} byte;
//	};
} SignalInfoTableType;

typedef struct {
	BYTE nPointKeyNo;
	BYTE nSwitchType;  // Single point - 0x10, Double point - 0x20, Hand point - 0x30, 
	BYTE reserved[2];
} SwitchInfoTableType;

typedef struct {
	BYTE pData[ 2 ];
} SwitchOnTrackTableType;

// #define TI_EXT_LC1			0x01
// #define TI_EXT_LC2			0x02
// #define TI_EXT_LC3			0x04
// #define TI_EXT_LC4			0x08
// #define TI_EXT_LC5			0x10
// #define TI_EXT_LC6			0x20
// #define TI_EXT_LC7			0x40
// #define TI_EXT_LC8			0x80

#ifdef _WINDOWS
typedef union {
	BYTE pData[ 2 ];
	struct {
		BYTE bNOTRACK		:1;	// 0x01 No track -- SDG...
		BYTE bBLKLTRACK		:1;	// 0x02 Left Block end track
		BYTE bBLKRTRACK		:1;	// 0x04 Right Block end track
		BYTE bLCNOTINTRK	:1;	// 0x08 LC is not in track
		BYTE bRESERVED2		:1;	// 0x10
		BYTE bSWITCHTRACK	:1;	// 0x20 전철기가 포함된 궤도.
		BYTE bDESTTRACK		:1;	// 0x40 진로의 종착 궤도를 나타낸다....
		BYTE bALARM			:1;	// 0x80 접근 궤도에 대한 경보용으로 사용한다....

		BYTE bLC1			:1;	// 0x01	
		BYTE bLC2			:1;	// 0x02
		BYTE bLC3			:1;	// 0x04
		BYTE bLC4			:1;	// 0x08
		BYTE bLC5			:1;	// 0x10
		BYTE bRESERVED4		:1;	// 0x20
		BYTE bRESERVED5		:1;	// 0x40
		BYTE bRESERVED6		:1;	// 0x80
	};
} TrackInfoTableType;
#else
typedef struct {
		BYTE bALARM			:1;	// 0x80 접근 궤도에 대한 경보용으로 사용한다....
		BYTE bDESTTRACK		:1;	// 0x40 진로의 종착 궤도를 나타낸다....
		BYTE bSWITCHTRACK	:1;	// 0x20 전철기가 포함된 궤도.
		BYTE bRESERVED2		:1;	// 0x10
		BYTE bLCNOTINTRK	:1;	// 0x08 LC is not in track
		BYTE bBLKRTRACK		:1;	// 0x04 Right Block end track
		BYTE bBLKLTRACK		:1;	// 0x02 Left Block end track
		BYTE bNOTRACK		:1;	// 0x01 No track -- SDG...

		BYTE bRESERVED6		:1;	// 0x80
		BYTE bRESERVED5		:1;	// 0x40
		BYTE bRESERVED4		:1;	// 0x20
		BYTE bLC5			:1;	// 0x10
		BYTE bLC4			:1;	// 0x08
		BYTE bLC3			:1;	// 0x04
		BYTE bLC2			:1;	// 0x02
		BYTE bLC1			:1;	// 0x01
} TrackInfoTableType;
#endif

typedef struct {
			BYTE nTrackID;
			BYTE nDelay;
			WORD wRouteID;
} SwitchOverlapTableType;

typedef struct {
	DWORD nRouteOffset : 32;		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
//	WORD nIsTTB   : 1;
} RouteAddTableType;

// 20140205 sjhong - DB 데이터 관리용 구조체를 별도로 선언. (DB 데이터 크기의 WORD크기 초과)
typedef struct {
	WORD nCRC;
	WORD nReserved;
	DWORD nMemSize;
} DownInfoType;

typedef struct {
	DWORD nStart;
	DWORD nSize;
} InfoMemType;

typedef struct {
	WORD nStart;
	WORD nSize;
} TableInfoType;

typedef struct {
	WORD nStart;
    WORD nCount;
} TableInfoCountType;

typedef struct {
	DownInfoType DownInfo;					// 8
	InfoMemType InfoMem;					// 8
	TableInfoType InfoHeader;				// 4
	TableInfoCountType InfoSignal;			// 4
	TableInfoCountType InfoTrack;			// 4	
	TableInfoCountType InfoSwitch;			// 4	
	TableInfoType InfoGeneralIO;			// 4 for Lamp
	TableInfoType InfoRouteAddTable;		// 4
	TableInfoType InfoSwitchOnTrackTable;	// 4
	TableInfoType InfoSignalInfoTable;		// 4

    BYTE nSwitchDelay;
    BYTE nDefaultApproachDelay;

	BYTE IOCardInfo[16];	// IO 카드의 IN(1), OUT(2), SDM(3) 정보(카드별 2 BIT씩 * 8개 랙)
	BYTE RealID[64];		// = 128-42-16 = 70
	BYTE Reserved[6];		//

	BYTE ACode[16];
	BYTE LGInfo[112];	// 
} TableBaseInfoType;

/*
typedef	struct {
	BYTE TIMERL		: 4;
	BYTE TIMERH		: 4;
} LHTimerType;
*/
typedef struct {
	BYTE SINGLE_TRACK;
	BYTE REVERSE_LAYOUT;
	BYTE AXLCNT_TRACK;
	BYTE SUPER_BLOCK;
	BYTE B1B2_AXLSYNC;
	BYTE B3B4_AXLSYNC;
	BYTE B11B12_AXLSYNC;
	BYTE B13B14_AXLSYNC;
	BYTE B1B2_NOAXL;
	BYTE B3B4_NOAXL;
	BYTE B11B12_NOAXL;
	BYTE B13B14_NOAXL;
	BYTE B1B2_SWAPAXL;
	BYTE B3B4_SWAPAXL;
	BYTE B2_SELF_AXLRST;
	BYTE B4_SELF_AXLRST;
	BYTE CTC_ZONENO;
	BYTE CTC_STATIONNO;
	BYTE GPS_ZONENO;
	BYTE GPS_STATIONNO;
	BYTE GPS_SERVERNO;
	BYTE UP_STATIONNO;
	BYTE DN_STATIONNO;
	BYTE MD_STATIONNO;
	BYTE B1B2_SELF_BLKRST;
	BYTE B3B4_SELF_BLKRST;
	BYTE B11B12_SELF_BLKRST;
	BYTE B13B14_SELF_BLKRST;
	BYTE USE_MMCR_NET;
	BYTE USE_MCCR_NET;
	BYTE USE_MODEM_BKUP;
	BYTE BLK_TRAIN_SWEEP;
	BYTE UP_NEWCBI_IFC;
	BYTE DN_NEWCBI_IFC;
	BYTE BLK_DEV_MODE;
} ConfigListType;


class CEipEmu
{
protected:
//	DECLARE_DYNCREATE(CEipEmu)

// Attributes
public:
	WORD FindPresetDependRoute(BYTE nRouteCount, WORD wBaseDependRouteNo);
	int RemoveRouteQue( BYTE nTrkID, BYTE arg2, BYTE nDependSignal );
	unsigned short GetRouteNo( BYTE nSG, BYTE CS, BYTE nTK );
	BYTE GetSwitchWRState( BYTE nSwID );
	void SignalAspectAdjustForLMR(BYTE *pInfo);
	void SetTNIQue( BYTE nTNI );
	int m_nTNIQueInPtr;
	int m_nTNIQueOutPtr;
	BYTE m_pTNIQue[16];
	BYTE GetTNIQue();
	void SetErrorCode( BYTE nCode, BYTE *pErrCode );
	void SetSwitchFail(BYTE nSwID);
	void SetSignalFail(BYTE nFailMask, BYTE *pInfo);
	int  TrackDelay(BYTE *nEmergencyStop);
	void ConvertToReal4();
	void CheckAddSwitchRoute(BYTE *pInfo, BYTE *dest, BYTE nTrack);
	void LoadTable( void *pTable );
#ifdef _WINDOWS
	BOOL LoadTable( LPCSTR lpszPath );
#endif
	int m_nTableSize;
	int m_nRamSize;
	BYTE *m_pVMEM;
    BYTE *m_pMsgBuffer;
	int m_nTNIBase;
	CEipEmu();

	void opSignalCan( BYTE nSignal );
	void opSignalDest( BYTE nSignalID, BYTE nDestID );
	void opClearAllRoute();
	void opHandSwitch( BYTE nSwID );
	void opSwitch( BYTE nSwID );
	void opPointBlock( BYTE nSwID );
	void opSignalStop( short nRoute );
    void opSignalOSS( BYTE nSignalID, BYTE nFlag );
    void opTrackLOS( BYTE nTrackID, BYTE nFlag );
	void opSwitchUSE( BYTE nSwitchID, BYTE nFlag );
	void opBlockCA ( BYTE nBlkID );
	void opBlockLCR ( BYTE nBlkID );
	void opBlockCBB ( BYTE nBlkID );

	BYTE RouteToSignal( short nRoute );
//	BYTE RouteToContSignal( short nRoute );
	void CancelRoute( short nRoute );
	BOOL RunOverlap( short nRoute );
	BYTE DetectOverlap( short nRoute );
	BYTE StartDetectOverlap( short nRoute );

	void RunRoute( short nRoute );
	short GetFindRoute( short nRoute );

	WORD CheckRoute( short nRoute );
	void RunInterlock( char *pMsg );

#include "ROMAREA.H"

#define N_SG_COUNT 128		//
#define N_SW_COUNT 64		//	20150127 전철기 개수 축소. 메모리 버퍼의 초과 및 계간통신 데이터 과대화 방지  128 -> 64
#define N_TK_COUNT 128		//

// -------------------------------------------------------------------------------------------------------
	WORD m_pRouteQue[N_SG_COUNT];											// 256
	BYTE m_pSwitchRouteTrack[N_SW_COUNT * 2];								// 256 -> 128 (DOWNSIZE)
	SwitchOverlapTableType m_pSwitchOverTrack1[N_SW_COUNT];				// 512 -> 256 (DOWNSIZE)
	SwitchOverlapTableType m_pSwitchOverTrack2[N_SW_COUNT];				// 512 -> 256 (DOWNSIZE)
	BYTE m_pSignalTimer[N_SG_COUNT];										// 128
	BYTE m_pSwitchTimer[N_SW_COUNT];										// 128 -> 64 (DOWNSIZE)x`
	BYTE m_pTrackExtraTimer[N_TK_COUNT];									// 128 (REPLACE)
	long m_pTrackTimer[N_TK_COUNT];											// 512
	BYTE m_pTrackPrev[N_TK_COUNT];	// 진로 내 인접 직전 궤도의 ID 테이블 // 128
    BYTE m_pLastTrack[N_TK_COUNT];	// 설정된 진로의 종착궤도 테이블	  // 128
// --------------------- 기존 m_nRamSize 영역 : 2688Bytes --> 1984Bytes (704Bytes Save) ------------------
	WORD m_pOwnerRouteQue[N_SG_COUNT];	// 중계진로의 본진로 번호 정보		  // 256
	BYTE m_pSignalFail[N_SG_COUNT];		// 신호기 Fail 정보					   // 128 
    BYTE m_pSwitchFail[N_SW_COUNT];		// 전철기 Fail 정보					   // 64
	//2012.11.29 추가...
	BYTE m_pTrackNext[N_TK_COUNT];		// 진로 내 인접 다음 궤도의 ID 테이블 // 128
	BYTE m_pOverTrack[N_TK_COUNT];		// Overlap 설정된 궤도의 ID 테이블	   // 128
	BYTE m_pSyncSwitch[N_SW_COUNT];		// 진로 외 동기화 전철기 정보 테이블  // 64
	BYTE m_pTrackStick[N_TK_COUNT];		// 궤도에 LC가 있을때				   // 128
	BYTE m_pLCBellTimer[6];				// LC Bell 타이머 테이블(0는 사용안함)// 6
	BYTE m_nPowerFail;					// Power Fail 알람 설정 값			   // 1
	BYTE m_nATBTimer;					// ATB Timer							// 1
	BYTE m_bSLSStick;					// SLS Stick Flag						// 1
// 계간 동기화가 필요한 데이터는 이 위치에 추가한다.
// ------------- 추가 m_nRamSize 영역 : 현재 905 Bytes 사용 --------

	ConfigListType	m_ConfigList;

//////////////////////////////////////////////////////////////////////////////////
#ifdef _WINDOWS
	CMyStrList m_strTrack, m_strSignal, m_strSwitch, m_strButton, m_strLamp;
#endif

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEipEmu)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEipEmu();
#ifdef _DEBUG
//	virtual void AssertValid() const;
//	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CEipEmu)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()

public:
    BYTE REGW;

    WORD m_wRouteNo;
    WORD m_wCheckResult;
    //BYTE nCheckResultNext;
    BYTE LoopCountL;
    BYTE LoopCountH;
    BYTE SG_Timer;
    BYTE SW_Timer;
    BYTE TK_Timer;


	union {
		BYTE *m_pRouteInfo;
		RouteBasPtr *m_pRouteBasPtr;
	};


	BOOL m_bIsInBlock;
    BOOL m_bIsYudo;
    BOOL m_bCancel;
	BOOL m_bStartInterlock;

	ScrInfoTrack  m_TrackInfo;
	ScrInfoSignal m_SignalInfo;
	ScrInfoSwitch m_SwitchInfo;
	BYTE m_nTrackID;
	BYTE m_nSignalID;
	BYTE m_nSwitchID;
	BYTE m_nCheckOverlap;
	BYTE m_nDependSignalRecoveryCount;	

	BYTE m_nTr51ATID;
	BYTE m_nSw51ID;
	BOOL m_bFirstTrackFree;

	BYTE m_arrRouteNo[256];
	BYTE m_nB1ID, m_nB2ID;
	BYTE m_nB3ID, m_nB4ID;
	BYTE m_nB11ID, m_nB12ID;
	BYTE m_nB13ID, m_nB14ID;
	BYTE m_SignalFailDetect[N_SG_COUNT];
	BYTE m_nLC1NKXPR, m_nLC2NKXPR, m_nLC3NKXPR, m_nLC4NKXPR, m_nLC5NKXPR;

    BYTE GetSignalAspected( int nSignalID );
	void SignalAspect(BYTE *pInfo);
    void SignalCheck(BYTE nSigType, BYTE *pInfo);
//    void SignalProcess();
    void TrackProcess();
    void SwitchProcess();
    void SwitchUpdate(BYTE nID, short istrack);
    void BitOperProcess();
    int Operate( char *pMsg );

    void *int_get(BYTE nType, BYTE nID); 
    void int_put(BYTE nType);

    void SysDownProc();
    void SetTkRequest(BYTE *pInfo);
    void ClearRequest(BYTE *pInfo);

	//2006.11.16 Overlap 표시를 위해 추가...
	void ClearOverlap( short nRoute, BOOL bOption );
	BYTE m_nSwitchFail;

#ifndef _WINDOWS
	int	GetAXLOverwriteBlkPos();
#endif

    ScrInfoSignal *GP_InfoSignal( int i );
    ScrInfoSwitch *GP_InfoSwitch( int i );
    ScrInfoTrack  *GP_InfoTrack( int i );

    void SignalStop();
    void SignalStopU();

	// Axle Counter Reset Timer Variable
	BYTE m_nB2AXLRSTTimer;
	BYTE m_nB4AXLRSTTimer;	

	BYTE GetAllAXLFailStatus();
	BYTE VerifyInfoSwitchTable(BYTE *pCurData, BYTE *pNewData);
	void UpdateAXLStatusByConfig();

	int MakeCtcElementImage( BYTE *pBuffer );
	int ProcessCtcCommand( BYTE *pCtcMsg );
	int ProcessConfigList();

	int GetBlockCommPorts(BYTE *LeftPorts, BYTE *RightPorts, BYTE *MidPorts);
};

/////////////////////////////////////////////////////////////////////////////
//
#include "OPMsgCode.h"

/////////////////////////////////////////////////////////////////////////////

#define SignalLoad	1
#define SwitchLoad	2
#define TrackLoad	3
#define SignalGet   4
#define SwitchGet   5
#define TrackGet    6
#define SignalPut   7
#define SwitchPut   8
#define TrackPut    9


#define ROUTECHECK_TRACK		0x0001	// SGOFF 조건
#define ROUTECHECK_SWITCH		0x0002	// SGOFF 조건
#define ROUTECHECK_SIGNAL		0x0004	// SGOFF 조건
#define ROUTECHECK_ROUTE		0x0008
#define ROUTECHECK_SWFREE		0x0010	// SGOFF 조건
#define ROUTECHECK_REROUTE		0x0020
#define ROUTECHECK_ALLROUTE		0x0040
#define ROUTECHECK_CONTACT		0x0080	// SGOFF 조건
#define ROUTECHECK_OCCTMOUT		0x0100
#define ROUTECHECK_OVERLAP		0x0200
#define ROUTECHECK_OSS			0x0400	// SGOFF 조건
#define ROUTECHECK_OVERLAPTRACK	0x0800	// SGOFF 조건 --> OVERLAP 구간 Track의 이상
#define ROUTECHECK_ALR			0x1000
#define ROUTECHECK_NOGREEN		0x2000

#define ROUTECHECK_GOOD			0

//#define ROUTECHECK_ENABLE   0x0E+ROUTECHECK_REROUTE
#define ROUTECHECK_ENABLE		0x2E
#define ROUTECHECK_REENABLE		0x0E

#define ROUTECHECK_SGOFF		ROUTECHECK_SWFREE+ROUTECHECK_TRACK+ROUTECHECK_SIGNAL+ROUTECHECK_CONTACT+ROUTECHECK_SWITCH+ROUTECHECK_OVERLAPTRACK+ROUTECHECK_OSS	// 0x0C97
#define ROUTECHECK_SGOFF_OSS	ROUTECHECK_SWFREE+ROUTECHECK_TRACK+ROUTECHECK_SIGNAL+ROUTECHECK_CONTACT+ROUTECHECK_SWITCH+ROUTECHECK_OVERLAPTRACK					// 0x0897

#define ROUTE_MASK				0x01FF
#define ROUTE_INVOKE			0x8000
#define ROUTE_CANCEL			0x4000
#define ROUTE_ONPROCESS			0x2000
#define ROUTE_ONEMCANCEL		0x1000
#define ROUTE_DEPENDSET			0x0800	// 20140214 sjhong - Depend Route 표시를 위해 용도 변경. ROUTE_DEPBLOCK --> ROUTE_DEPENDSET	
#define ROUTE_SIGNALSTOP		0x0400
#define ROUTE_ASPECT			0x0200

#define SIGNAL_MASK			0xF0	// 20140418 sjhong - Signal Type Mask 선언
#define SIGNALID_MASK		0x7F	// 20150831 sjhong

#define SIGNALTYPE_NONE		0
#define SIGNALTYPE_BUFSTART	0x00	// Buffer Signal Lamp
#define SIGNALTYPE_INBLOCK	0x10	//구내폐색 -- 자동폐색 장내만 하도록 함 (장내 신호는 삭제)
#define BLOCKTYPE_AUTO_IN	0x10	//자동폐색 장내
#define BLOCKTYPE_AUTO_OUT	0x20	//자동폐색 출발
#define BLOCKTYPE_INTER_IN	0x30	//연동폐색 장내
#define BLOCKTYPE_INTER_OUT 0x40	//연동폐색 출발
#define SIGNALTYPE_ISHUNT	0x50	// 
#define SIGNALTYPE_STOP		0x60	//
#define SIGNALTYPE_ISTART	0x70	// 
#define SIGNALTYPE_IN		0x80	// 80->20
#define SIGNALTYPE_GSHUNT	0x90	// 20140418 sjhong - 신규 정의
#define SIGNALTYPE_OUT		0xA0	//
#define SIGNALTYPE_SHUNT	0xB0	// B0->80
#define SIGNALTYPE_OHOME	0xC0    // B0->80
#define SIGNALTYPE_ASTART	0xD0	//
#define SIGNALTYPE_CALLON	0xE0	// CALLON신호
#define SIGNALTYPE_MPASS	0xF0	//
// 
#define SIGNALTYPE_YUDO_BIT	0x08
#define SIGNALTYPE_DIR_BIT	0x04

#define BLOCKTYPE_DIR_LEFT	0x01
#define BLOCKTYPE_DIR_RIGHT	0x02
#define	BLOCKTYPE_DIR_MID	0x04

#define BLKRST_CMD_REQ		0x01
#define BLKRST_CMD_ACC		0x02
#define BLKRST_CMD_DEC		0x04

// =======Interlock Table Offset define============
#define RT_OFFSET_BASE			16		// sizeof(RouteBasPtr)
#define RT_OFFSET_FROM			(RT_OFFSET_BASE + 0)
#define RT_OFFSET_TO			(RT_OFFSET_BASE + 1)
#define RT_OFFSET_SIGNAL		(RT_OFFSET_BASE + 2)
#define RT_OFFSET_BUTTON		(RT_OFFSET_BASE + 3)
#define RT_OFFSET_ROUTESTATUS	(RT_OFFSET_BASE + 4)
#define RT_OFFSET_PREVSIGNAL	(RT_OFFSET_BASE + 5)	// RT_OFFSET_BASE + 8에서 이동		(RT_OFFSET_DEPENDSHUNT2 위치)
#define RT_OFFSET_DEPENDSIG1	(RT_OFFSET_BASE + 6)	// RT_OFFSET_DEPENDSIGNAL에서 변경	(RT_OFFSET_DEPENDSHUNT1 위치)
#define RT_OFFSET_DEPENDSIG2	(RT_OFFSET_BASE + 7)	// RT_OFFSET_DEPENDSHUNT1에서 변경	(RT_OFFSET_DEPENDSIGNAL 위치)
#define RT_OFFSET_DEPENDSIG3	(RT_OFFSET_BASE + 8)	// RT_OFFSET_DEPENDSHUNT2에서 변경 	(RT_OFFSET_PREVSIGNAL 위치)
#define RT_OFFSET_DEPENDSIG4	(RT_OFFSET_BASE + 9)	// 신규 생성						(RT_OFFSET_CALLONTK 위치)
#define RT_OFFSET_LEVELCROSS    (RT_OFFSET_BASE + 10)
#define RT_OFFSET_CONFLICTBLK	(RT_OFFSET_BASE + 11)
#define RT_OFFSET_ROUTEORDER	(RT_OFFSET_BASE + 12)
#define RT_OFFSET_MULTIDEST		(RT_OFFSET_BASE + 13)	// 다중 종착 궤도 개수 저장.
#define RT_OFFSET_ROUTETAG		(RT_OFFSET_BASE + 14)	// CTC 제어를 위해 진로명의 DASH알파벳을 저장함.
#define RT_OFFSET_BERTH_TIME	(RT_OFFSET_BASE + 15)	// BERTH TRACK에 대한 점유 유지 시간 (RT_OFFSET_CALLONTK에서 변경)
#define RT_OFFSET_APPRCH_TIME	(RT_OFFSET_BASE + 16)	// APPROACH TRACK에 대한 진로 유지 시간 (신규 생성)
#define RT_OFFSET_APPRCHTK1		(RT_OFFSET_BASE + 17)
#define RT_OFFSET_APPRCHTK2		(RT_OFFSET_BASE + 18)
#define RT_OFFSET_APPRCHTK3		(RT_OFFSET_BASE + 19)
#define RT_OFFSET_APPRCHTK4		(RT_OFFSET_BASE + 20)
#define RT_OFFSET_APPRCHTK5		(RT_OFFSET_BASE + 21)
#define RT_OFFSET_APPRCHTK6		(RT_OFFSET_BASE + 22)
#define RT_OFFSET_RESERVED		(RT_OFFSET_BASE + 23)
#define RT_OFFSET_SWITCH		(RT_OFFSET_BASE + 24)

// 20140123 sjhong - 중계 신호기 처리 정리
#define RT_OFFSET_DEPENDSIG_BEGIN	RT_OFFSET_DEPENDSIG1
#define RT_OFFSET_DEPENDSIG_END		RT_OFFSET_DEPENDSIG4

// 20150827 sjhong - 접근 궤도 처리 정리
#define RT_OFFSET_APPRCHTK_BEGIN	RT_OFFSET_APPRCHTK1
#define RT_OFFSET_APPRCHTK_END		RT_OFFSET_APPRCHTK6

#define RT_OFFSET_BLKPREVSIG1	(RT_OFFSET_BASE + 5)
#define RT_OFFSET_BLKPREVSIG2	(RT_OFFSET_BASE + 6)
#define RT_OFFSET_BLKPREVSIG3	(RT_OFFSET_BASE + 7)
#define RT_OFFSET_BLKPREVSIG4	(RT_OFFSET_BASE + 8)
#define RT_OFFSET_BLKPREVSIG5	(RT_OFFSET_BASE + 9)
#define RT_OFFSET_BLKARRIVETK	(RT_OFFSET_BASE + 13)
#define RT_OFFSET_BLKTRACK1		(RT_OFFSET_BASE + 16)
#define RT_OFFSET_BLKTRACK2		(RT_OFFSET_BASE + 17)
#define RT_OFFSET_BLKTRACK3		(RT_OFFSET_BASE + 18)
#define RT_OFFSET_BLKTRACK4		(RT_OFFSET_BASE + 19)
#define RT_OFFSET_BLKTRACK5		(RT_OFFSET_BASE + 20)
#define RT_OFFSET_PROHIBITBLK	(RT_OFFSET_BASE + 21)	// RT_OFFSET_BASE + 5 를 재정의
#define RT_OFFSET_BLKDIR		(RT_OFFSET_BASE + 22)	
#define RT_OFFSET_MAINBLKPORT	(RT_OFFSET_BASE + 23)
#define RT_OFFSET_SUBBLKPORT	(RT_OFFSET_BASE + 24)

#define MAX_BLKPREVSIG_QTY		5
#define MAX_BLKTRACK_QTY		5

#define RT_CALLON		0x80
#define RT_SHUNTSIGNAL	0x40
#define RT_FREESHUNT	0x20
//#define RT_LEFT  		0x20
//#define RT_RIGHT		0x10

#define SG_LAMP_R		0x01
#define SG_LAMP_Y		0x02
#define SG_LAMP_G		0x04
#define SG_LAMP_SHUNT	0x08
#define SG_LAMP_CALL	0x10
#define SG_LAMP_SY		0x20
#define SG_LAMP_LL		0x40
#define SG_LAMP_LR		0x80
#define SG_LAMP_EXT_RI	0x01

#define FLAG_LC1		0x01
#define FLAG_LC2		0x02
#define FLAG_LC3		0x04
#define FLAG_LC4		0x08
#define FLAG_LC5		0x10

#define SWT_DETECT_ONLY		0x80	// 검지만 수행하는 전철기
#define SWT_FORCED_MOVE		0x40	// 오버랩 방향으로 강제 전환하는 전철기
#define SWT_COMBO_MASTER	0x20	// 동시 제어되는 진로 구간 내 전철기
#define SWT_COMBO_SLAVE		0x10	// 동시 제어되는 진로 구간 외 전철기
#define SWT_IN_OVERLAP		0x08	// 오버랩에 속하는 전철기

#define SYNCSWT_MASTER	0x80	// 동기화 전철기 정보 배열의 MASTER/SLAVE 플래그
#define SYNCSWT_MASK	0x7F	// 동기화 전철기 정보 배열의 전철기 ID 검출용 MASK

#define TRACK_MASK		0x7F
#define TRACK_DIR_MASK	0x80
#define TRACK_DIR_LEFT	TRACK_DIR_MASK	// LEFT : 0x80 , RIGHT : 0x00
#define TRACK_STAY_DEST			0x80	// 체류종착궤도임을 구분하는 FLAG
#define TRACK_STAY_DEST_TIME	60		// 체류종착궤도 체류 유지 시간 (1 ~ 255초 설정 가능)
#define TRACK_BERTH_MASK		0x80	// BERTH TRACK 체크 FLAG (CALLON, SHUNT)

#define BLOCK_IDENTIFIER	0xFF

#define BLOCK_ALARM_MASK			0x1F	// FLAG를 제외한 알람 시간을 추출하는 MASK	(1 ~ 31초 설정 가능)
#define BLOCK_ALARM_DEPARTOUT		0x80	// Signal Fail 중 TGB의 DEPART ALARM 항목임을 구분하는 FLAG
#define BLOCK_ALARM_DEPARTIN		0x40	// Signal Fail 중 TCB의 DEPART ALARM 항목임을 구분하는 FLAG
#define BLOCK_ALARM_APPROACH		0x20	// Signal Fail 중 APPROACH ALARM 항목임을 구분하는 FLAG

#define BLOCK_ALARM_DEPART_TIME		30		// 열차가 폐색으로 진입한 경우, TCB측에서 송출하는 알람의 유지 시간 (1 ~ 31초 설정 가능)
#define BLOCK_ALARM_APPROACH_TIME	30		// 열차가 폐색에서 구내로 진입한 경우, 송출하는 알람의 유지 시간	(1 ~ 31초 설정 가능)
#define LC_BELL_TIME				120		// LC CALL 버튼 취급에 의한 BELL 출력을 유지하는 시간 (1 ~ 255초 설정 가능)

#define SIG_DEPEND_SET_ONLY			0x80	// 중계 신호기이나, 진로만 설정되고 현시는 별도로 수행하는 신호기

#define ATB_TEST_TIME				60
 
#define FUNC_LDA	1
#define FUNC_STA	2
#define FUNC_AND	3
#define FUNC_IOR	4

#define CONFIG_SINGLE_TRACK		0x30
#define CONFIG_REVERSE_LAYOUT	0x31
#define CONFIG_AXLCNT_TRACK		0x32
#define CONFIG_SUPER_BLOCK		0x33
#define CONFIG_BLK_DEV_MODE		0x34

#define CONFIG_B1B2_AXLSYNC		0x40
#define CONFIG_B3B4_AXLSYNC 	0x41
#define CONFIG_B11B12_AXLSYNC 	0x42
#define CONFIG_B13B14_AXLSYNC 	0x43
#define CONFIG_B1B2_NOAXL		0x44
#define CONFIG_B3B4_NOAXL		0x45
#define CONFIG_B11B12_NOAXL		0x46
#define CONFIG_B13B14_NOAXL		0x47
#define CONFIG_B1B2_SWAPAXL		0x48
#define CONFIG_B3B4_SWAPAXL		0x49
#define CONFIG_B2_SELF_AXLRST	0x4A
#define CONFIG_B4_SELF_AXLRST	0x4B

#define CONFIG_CTC_ZONENO		0x50
#define CONFIG_CTC_STATIONNO	0x51
#define CONFIG_GPS_ZONENO		0x52
#define CONFIG_GPS_STATIONNO	0x53
#define CONFIG_GPS_SERVERNO		0x54
#define CONFIG_UP_STATIONNO		0x55
#define CONFIG_DN_STATIONNO		0x56
#define CONFIG_MD_STATIONNO		0x57

#define CONFIG_B1B2_SELF_BLKRST		0x60
#define CONFIG_B3B4_SELF_BLKRST		0x61
#define CONFIG_B11B12_SELF_BLKRST	0x62
#define CONFIG_B13B14_SELF_BLKRST	0x63
#define CONFIG_BLK_TRAIN_SWEEP		0x64

#define CONFIG_USE_MMCR_NET		0x70
#define CONFIG_USE_MCCR_NET		0x71
#define CONFIG_USE_MODEM_BKUP	0x72

#define CONFIG_UP_NEWCBI_IFC	0x80
#define CONFIG_DN_NEWCBI_IFC	0x81
#endif
