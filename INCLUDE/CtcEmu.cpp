// CtcEmu.cpp : implementation file
//

#ifdef _WINDOWS
#include "stdafx.h"
#else
#include "string.h"
#endif

#ifndef _WINDOWS
#include "LogTask.h"
#endif

#include "scrinfo.h"
#include "CtcEmu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCtcEmu

CCtcEmu::CCtcEmu()
{
}

CCtcEmu::~CCtcEmu()
{
}

/////////////////////////////////////////////////////////////////////////////
// CCtcEmu diagnostics

#ifdef _DEBUG
void CCtcEmu::AssertValid() const
{
	return;     //CDocument::AssertValid();
}

void CCtcEmu::Dump(CDumpContext& dc) const
{
	return; //CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCtcEmu commands
//
void CRemMode::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	MD_CTC 	 = nSysVar.bMode;
	MD_LOCAL = !nSysVar.bMode;
}

void CRemPower::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	PW_MAIN		= nSysVar.bPowerFail;
	PW_GENRUN	= nSysVar.bGenRun;
	PW_GEN		= !nSysVar.bGen;
	PW_B24		= nSysVar.bB24PWR;
	PW_DCEX		= nSysVar.bExtPowerN;
	PW_GND		= !nSysVar.bGround;
	PW_UPS1		= nSysVar.bUPS1;
	PW_UPS2		= nSysVar.bUPS2;
}

void CRemPowerExt::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	PW_BATLOW	= !nSysVar.bLowVoltage;
	PW_BATCHG	= nSysVar.bCharging;
	PW_RELFUSE	= !nSysVar.bFusePR;
}

void CRemCommon::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	CM_DAY		= !nSysVar.bIDayNight;
	CM_SLS		= nSysVar.bN3;
	CM_AXL		= !nSysVar.bAXLFail; 
	CM_SG		= !nSysVar.bSignalFail;	
	CM_SW		= !nSysVar.bPointFail;
	CM_COMM		= nSysVar.bUpModem & nSysVar.bDnModem;
	CM_CLOSE	= nSysVar.bStation;
}

void CRemKTUPDN::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	KT_UEPK		= nSysVar.bPKeyContN;
	KT_DEPK		= nSysVar.bPKeyContS;
	KT_EEPK		= nSysVar.bPKeyContE;
}

void CRemKTABCD::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	KT_AEPK		= nSysVar.bPKeyContA;
	KT_BEPK		= nSysVar.bPKeyContB;
	KT_CEPK		= nSysVar.bPKeyContC;
	KT_DEPK		= nSysVar.bPKeyContD;
	KT_EEPK		= nSysVar.bPKeyContE;
	KT_FEPK		= nSysVar.bPKeyContF;
	KT_GEPK		= nSysVar.bPKeyContG;
	KT_HEPK		= 0;
}

void CRemCBI::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	CBI_PRI		= nSysVar.bSYS1Good;
	CBI_PRION	= nSysVar.bSYS1;
	CBI_SEC		= nSysVar.bSYS2Good;
	CBI_SECON	= nSysVar.bSYS2;	
	CBI_PRIFAN	= nSysVar.bFan1;
	CBI_SECFAN	= nSysVar.bFan2;
	CBI_FUSE	= !(nSysVar.bFuse1 | nSysVar.bFuse2);
}

void CRemMCCR::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    LCCStatusType &LCCStatus = pHead->LCCStatus;

	CC_PRI		= LCCStatus.bLCC1Alive;
	CC_PRION	= LCCStatus.bLCC1Active;
	CC_SEC		= LCCStatus.bLCC2Alive;
	CC_SECON	= LCCStatus.bLCC2Active;	
}

void CRemAdjComm::MakeRemoteData( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;

	ADJ_LSTN1	= nSysVar.bUpModem;
	ADJ_RSTN1	= nSysVar.bDnModem;
}

void CRemAdjComm::MakeRemoteDataRev( void *pData ) 
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusType &nSysVar = pHead->nSysVar;
	
	ADJ_LSTN1	= nSysVar.bDnModem;
	ADJ_RSTN1	= nSysVar.bUpModem;
}

void CRemCallOnLSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	CLON_LSB	= (BYTE)(pHead->Counter[0] & 0xFF);
}

void CRemCallOnMSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	CLON_MSB	= (BYTE)((pHead->Counter[0] >> 8) & 0xFF);
}

void CRemERRLSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	ERR_LSB		= (BYTE)(pHead->Counter[1] & 0xFF);
}

void CRemERRMSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	ERR_MSB		= (BYTE)((pHead->Counter[1] >> 8) & 0xFF);
}

void CRemAXLB2LSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	AXB2_LSB	= (BYTE)(pHead->Counter[2] & 0xFF);
}

void CRemAXLB2MSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	AXB2_MSB	= (BYTE)((pHead->Counter[2] >> 8) & 0xFF);
}

void CRemAXLB4LSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	AXB4_LSB	= (BYTE)(pHead->Counter[3] & 0xFF);
}

void CRemAXLB4MSB::MakeRemoteData( void *pData)
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
	
	AXB4_MSB	= (BYTE)((pHead->Counter[3] >> 8) & 0xFF);
}

void CRemArrow::MakeRemoteDataTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	ARW_BK		= !pBlkInfo->LCR;
	ARW_WT		= (pBlkInfo->LCR & !pBlkInfo->LCRIN)  
			   		|| (pBlkInfo->LCR & pBlkInfo->LCRIN & pBlkInfo->ASTART & !pBlkInfo->DEPART);
	ARW_WTFL	= pBlkInfo->LCR & pBlkInfo->LCRIN & !pBlkInfo->ASTART & !pBlkInfo->DEPART & ((pBlkInfo->SAXLACT & pBlkInfo->SAXLOCC) | (!pBlkInfo->SAXLACT & pBlkInfo->AXLOCC));
	ARW_RD		= pBlkInfo->LCR & pBlkInfo->LCRIN & (pBlkInfo->DEPART | (pBlkInfo->SAXLACT & !pBlkInfo->SAXLOCC) | (!pBlkInfo->SAXLACT & !pBlkInfo->AXLOCC));
	ARW_RDFL	= 0;
}

void CRemArrow::MakeRemoteDataTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	ARW_BK		= !pBlkInfo->LCR & !pBlkInfo->LCRIN;
	ARW_WT		= pBlkInfo->LCR & pBlkInfo->LCRIN & !pBlkInfo->DEPARTIN;
	ARW_WTFL	= !pBlkInfo->LCR & pBlkInfo->LCRIN;
	ARW_RD		= pBlkInfo->LCR & pBlkInfo->LCRIN & ((pBlkInfo->DEPARTIN & !pBlkInfo->ARRIVE) | !pBlkInfo->AXLOCC);
	ARW_RDFL	= pBlkInfo->LCR & pBlkInfo->LCRIN & pBlkInfo->DEPARTIN & pBlkInfo->ARRIVE;
}

void CRemArrow::MakeRemoteDataTokenTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;
	
	ARW_BK		= !pBlkInfo->LCRIN;
	ARW_WT		= pBlkInfo->LCRIN & pBlkInfo->ASTART & !pBlkInfo->DEPART;
	ARW_WTFL	= pBlkInfo->LCRIN & !pBlkInfo->ASTART & !pBlkInfo->DEPART;
	ARW_RD		= pBlkInfo->LCRIN & pBlkInfo->DEPART;
	ARW_RDFL	= 0;
}

void CRemArrow::MakeRemoteDataTokenTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;
	
	ARW_BK		= !pBlkInfo->LCR;
	ARW_WT		= pBlkInfo->LCR & !pBlkInfo->DEPARTIN;
	ARW_WTFL	= 0;
	ARW_RD		= pBlkInfo->LCR & pBlkInfo->DEPARTIN & !pBlkInfo->ARRIVE;
	ARW_RDFL	= pBlkInfo->LCR & pBlkInfo->DEPARTIN & pBlkInfo->ARRIVE;
}

void CRemCA::MakeRemoteDataTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	CA_WT		= !pBlkInfo->CA;
	CA_YW		= pBlkInfo->CA;
	CA_YWFL		= 0;
}

void CRemCA::MakeRemoteDataTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	CA_WT		= !pBlkInfo->CA & !pBlkInfo->CAACK;
	CA_YW		= pBlkInfo->CA & pBlkInfo->CAACK;
	CA_YWFL		= pBlkInfo->CA & !pBlkInfo->CAACK;
}

void CRemLCR::MakeRemoteDataTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	LCR_WT		= !pBlkInfo->LCR && !pBlkInfo->LCRIN;
	LCR_GN		= pBlkInfo->LCR | pBlkInfo->LCRIN;
	LCR_GNFL	= 0;
}

void CRemLCR::MakeRemoteDataTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	LCR_WT		= !pBlkInfo->LCR & !pBlkInfo->LCRIN;
	LCR_GN		= pBlkInfo->LCR & pBlkInfo->LCRIN; 
	LCR_GNFL	= !pBlkInfo->LCR & pBlkInfo->LCRIN;
}

void CRemCBB::MakeRemoteDataTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	CBB_WT		= !(pBlkInfo->LCR & pBlkInfo->LCRIN & (pBlkInfo->ASTART | pBlkInfo->DEPART));
}

void CRemCBB::MakeRemoteDataTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;
	CBB_WT		= !(pBlkInfo->LCR & pBlkInfo->LCRIN & !pBlkInfo->ARRIVE);
}

void CRemBCR::MakeRemoteDataTGB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	BCR_GY		= !pBlkInfo->RBGPR;
}

void CRemBCR::MakeRemoteDataTCB( void *pData ) 
{
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pData;

	BCR_GY		= !pBlkInfo->RBCPR;
}

void CRemSwitch::MakeRemoteData( void *pData ) 
{
	ScrInfoSwitch *pSwitch = (ScrInfoSwitch *)pData;

	SW_PLS 		= pSwitch->WR_P;
	SW_MIN 		= pSwitch->WR_M;
	SW_LOCK 	= !pSwitch->ROUTELOCK & pSwitch->TRACKLOCK;
	SW_FAIL 	= !pSwitch->SWFAIL;
	SW_MOV 		= !pSwitch->SWFAIL & (!pSwitch->KR_P & pSwitch->WR_P) | (!pSwitch->KR_M & pSwitch->WR_M);
	SW_OVLREL	= pSwitch->ON_TIMER;
	SW_KTIN 	= pSwitch->WKEYKR;
	SW_BLK		= pSwitch->BLOCK;
}

void CRemMSwitch::MakeRemoteData( void *pData ) 
{
	ScrInfoSwitch *pSwitch = (ScrInfoSwitch *)pData;

	MSW_PLS 	= pSwitch->WR_P;
	MSW_MIN 	= pSwitch->WR_M;
	MSW_FAIL 	= !pSwitch->SWFAIL;
	MSW_KTIN 	= pSwitch->WKEYKR;
	MSW_KTREL	= pSwitch->KEYOPER;
}

void CRemSignal::MakeRemoteData( void *pData ) 
{
	ScrInfoSignal *pSignal = (ScrInfoSignal *)pData;

	SG_GN		= !pSignal->LM_ALR & pSignal->LM_LOR & pSignal->SIN1 & pSignal->SIN2;
	SG_YW		= !pSignal->LM_ALR & pSignal->LM_LOR & !pSignal->SIN1 & pSignal->SIN2;
	SG_RD		= !pSignal->LM_ALR & pSignal->LM_LOR & !SG_YW & !SG_GN;
	SG_YY		= !pSignal->LM_ALR & pSignal->LM_CLOR & pSignal->SIN3;
	SG_FL		= pSignal->LM_ALR | pSignal->LM_LOR;
	SG_YYFL		= pSignal->LM_ALR | !(pSignal->LM_CLOR ^ pSignal->SOUT3);
	SG_LIND		= /*!SG_RD & */(pSignal->INLEFT | pSignal->INRIGHT) & pSignal->LEFT;
	SG_RIND		= /*!SG_RD & */(pSignal->INLEFT | pSignal->INRIGHT) & pSignal->RIGHT;
}

void CRemSignal::MakeRemoteDataAStart( void *pData ) 
{
	ScrInfoSignal *pSignal = (ScrInfoSignal *)pData;

	SG_GN		= !pSignal->LM_ALR & pSignal->LM_LOR & pSignal->SIN1;
	SG_YW		= !pSignal->LM_ALR & pSignal->LM_LOR & !pSignal->SIN1 & pSignal->SIN2;
	SG_RD		= !pSignal->LM_ALR & pSignal->LM_LOR & !SG_YW & !SG_GN;
	SG_YY		= !pSignal->LM_ALR & pSignal->LM_CLOR & pSignal->SIN3;
	SG_FL		= pSignal->LM_ALR | pSignal->LM_LOR;
	SG_YYFL		= pSignal->LM_ALR | !(pSignal->LM_CLOR ^ pSignal->SOUT3);
	SG_LIND		= /*!SG_RD & */(pSignal->INLEFT | pSignal->INRIGHT) & pSignal->LEFT;
	SG_RIND		= /*!SG_RD & */(pSignal->INLEFT | pSignal->INRIGHT) & pSignal->RIGHT;
}

void CRemSignalExt::MakeRemoteData( void *pData ) 
{
	ScrInfoSignal *pSignal = (ScrInfoSignal *)pData;

	SG_APLOCK 	= !pSignal->FREE & pSignal->ON_TIMER;
	SG_CALL		= pSignal->LM_CLOR & pSignal->SIN3;
	SG_SHNT		= pSignal->INU;
	SG_CALLFL	= pSignal->LM_CLOR;
	SG_SHNTFL	= !pSignal->INU | pSignal->U;
	SG_INDFL	= !pSignal->LEFT & !pSignal->RIGHT | pSignal->INLEFT | pSignal->INRIGHT;
	SG_CLRWT	= !pSignal->FREE & pSignal->TM1 & !pSignal->ON_TIMER 
				/*& !(pSignal->SE1 | pSignal->SE2 | pSignal->SE3 | pSignal->SE4) */
				& !(pSignal->SOUT1 | pSignal->SOUT2 | pSignal->SOUT3 | pSignal->U);	// 8bit
	SG_CLR		= !pSignal->FREE & !SG_CLRWT & !SG_APLOCK;							// 7bit
}

void CRemTrack::MakeRemoteData( void *pData ) 
{
	ScrInfoTrack *pTrack = (ScrInfoTrack *)pData;

	TK_OCC 		= !pTrack->TRACK;
	TK_ROREQ	= pTrack->TKREQUEST & pTrack->ROUTE;
	TK_RO		= !pTrack->ROUTE;
	TK_LOS		= pTrack->INHIBIT;
	TK_NOSEQ	= pTrack->OVERLAP | pTrack->TRACK;
	TK_EMREL	= pTrack->EMREL;
}

void CRemLC::MakeRemoteDataLC1( void *pData )
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusTypeExt &nSysVarExt = pHead->nSysVarExt;

	LC_CLOSE	= !nSysVarExt.bLC1NKXPR;
	LC_KEYCTRL	= nSysVarExt.bLC1KXLR;
	LC_CALLOP	= nSysVarExt.bLC1BELL;
	LC_CALLACK	= nSysVarExt.bLC1BELACK;
}

void CRemLC::MakeRemoteDataLC2( void *pData )
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusTypeExt &nSysVarExt = pHead->nSysVarExt;

	LC_CLOSE	= !nSysVarExt.bLC2NKXPR;
	LC_KEYCTRL	= nSysVarExt.bLC2KXLR;
	LC_CALLOP	= nSysVarExt.bLC2BELL;
	LC_CALLACK	= nSysVarExt.bLC2BELACK;
}

void CRemLC::MakeRemoteDataLC3( void *pData )
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusTypeExt &nSysVarExt = pHead->nSysVarExt;
	
	LC_CLOSE	= !nSysVarExt.bLC3NKXPR;
	LC_KEYCTRL	= nSysVarExt.bLC3KXLR;
	LC_CALLOP	= nSysVarExt.bLC3BELL;
	LC_CALLACK	= nSysVarExt.bLC3BELACK;
}

void CRemLC::MakeRemoteDataLC4( void *pData )
{
	DataHeaderType* pHead = (DataHeaderType*)pData;
    SystemStatusTypeExt &nSysVarExt = pHead->nSysVarExt;
	
	LC_CLOSE	= !nSysVarExt.bLC4NKXPR;
	LC_KEYCTRL	= nSysVarExt.bLC4KXLR;
	LC_CALLOP	= nSysVarExt.bLC4BELL;
	LC_CALLACK	= nSysVarExt.bLC4BELACK;
}

void CRemOSS::MakeRemoteData( void *pData ) 
{
	ScrInfoSignal *pSignal = (ScrInfoSignal *)pData;

	OSS_SET		= !pSignal->OSS;
}

