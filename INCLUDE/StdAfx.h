#if !defined(AFX_STDAFX_H__599A5E5B_C9E6_4128_8C45_48AE04D51662__INCLUDED_)
#define AFX_STDAFX_H__599A5E5B_C9E6_4128_8C45_48AE04D51662__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#include <afxsock.h>		// MFC socket extensions
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__599A5E5B_C9E6_4128_8C45_48AE04D51662__INCLUDED_)
