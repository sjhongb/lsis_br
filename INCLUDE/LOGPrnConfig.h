/////////////////////////////////////////////////////////////////////////////
// LOGPrnConfig.h : header file
//

#ifndef   __LOGPRNCONFIG_H
#define   __LOGPRNCONFIG_H

#include  "MessageNo.h"

/////////////////////////////////////////////////////////////////////////////

#define PRN_PAGESIZE 69		// ML3500 레이저 프린터를 기준으로 함

/////////////////////////////////////////////////////////////////////////////

typedef struct tagLOGPrnConfigType {
	BOOL bFirst;
	int  nPrnMode;
	int  nPageLine;
	CJTime STime;
	CJTime ETime;
	MsgModeType Type;

public:
	void init()	
	{
		bFirst = TRUE;
		nPrnMode = 0;
		nPageLine = PRN_PAGESIZE;
		STime.nSec = 0;
		STime.nMin = 0;
		STime.nHour = 0;
		STime.nDate = 1;
		STime.nMonth = 1;
		STime.nYear = 0;
		ETime.nSec = 0;
		ETime.nMin = 0;
		ETime.nHour = 0;
		ETime.nDate = 1;
		ETime.nMonth = 1;
		ETime.nYear = 0;
		Type.MsgType = 0xff;
	}
} LOGPrnConfigType;

/////////////////////////////////////////////////////////////////////////////

#endif // __LOGPRNCONFIG_H


