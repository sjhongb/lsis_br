// EventFile.h: interface for the CEventFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EVENTFILE_H__DA7A3B60_EE8F_428A_BDA6_AD11F72A99D4__INCLUDED_)
#define AFX_EVENTFILE_H__DA7A3B60_EE8F_428A_BDA6_AD11F72A99D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LOGMessage.h"

class CEventFile
{
public:
	int GetRecordCount();
	int Open(CString strFilePath, CString strStnName, BOOL bReadOnly);
	int Close();
	int Add(MessageEvent *pNewEvent);
	int GetAllRecords(CObList &obList);
	int GetPeriodRecords(CObList &obList, CJTime &startTime, CJTime &endTime);
	CEventFile();
	virtual ~CEventFile();

private:
	BOOL m_bIsOpen;
	CString m_strFilePath;
	CString m_strStnName;
	CFile m_File;
	CObList m_List;
};

#endif // !defined(AFX_EVENTFILE_H__DA7A3B60_EE8F_428A_BDA6_AD11F72A99D4__INCLUDED_)
