/////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------+
* 1. License
*		All Right Reserved. No part of this code may be reproduced,
*		in any form or by any means, without permission in writing 
*		from the company.
*
* 2. 파일명 : 
*		OPmsgCode.h
* 
* 3. 파일설명 :
*		LSM OCX 의 Post 메시지 Func Code 정의 헤더 파일. 
*
* 4. 프로젝트명 : 
*		EIP-2 전자 연동 장치 - (LSM).
*
* 5. 작 성 자 : 
*		정영진 
*
* 6. 작성일자 : 
*		1991.06 - 2001.07
*
* 7. 사용기종 :
*		개발환경 : PC(Pentium)
*		운영체제 : WINDOWS '95, WINDOWS '98
*		Target	 : WINDOWS '95 이상 버전
*		Language : VC++ 4.2(C class)
*
* 8. O.S Version : 
*		WINDOWS '98
*
* 9. 개발이력 :
*		Ver   개시일      작성자              설명
*		------------------------------------------------------------
*		1.0   1991.06                          Create
*		1.a   2001.07.04   정영진              개정
*
*10. 유지보수 :
*		2001.07.04 - * 로깅(Logging) 데이터의 재생 기능에서 송신된 시간 정보의 오류 시간 
*					   상태까지 표현할 수 있도록 하기 위해 좌측 상위 리얼 타임, 그 아래에
*					   로그 타임을 표현한다.  이를 위해 외부 지정시만 로그 타임을 표현하도록  
*					   기능 번호를 부여한다.  기능의 데이터는 TRUE/FALSE 상태값으로 전송.
*					   따라서 기능번호는 추가되고 할당된 기능번호는 60 번이다.
*
+------------------------------------------------------------------------------------------*/
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// OPMsgCode.h
 
#ifndef   _OPMSGCODE_H
#define   _OPMSGCODE_H
 
/////////////////////////////////////////////////////////////////////////////
// 통신 메세지 & 제어 메세지
/*
#define OPMSG_START			1		// 시스템 기동
#define OPMSG_RESET 		2		// UNIT 시계 조정(초,분,시,일,월,년)
#define OPMSG_SETTIMER		3		// UNIT 시계 조정(초,분,시,일,월,년)
#define OPMSG_SIGNALDEST	5		// 신호 
#define OPMSG_SIGNALCAN		6		// 신호 취소
#define OPMSG_TTB			7		// 
#define OPMSG_TTBCAN		8
#define OPMSG_SWTGL			9		// 선로전환기 토글(toggle)
#define OPMSG_BUTTON		10		// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
#define OPMSG_MARKINGTRACK	14		// 궤도 사용금지/해제(모터카, 사용금지, 단선)
#define OPMSG_FREESUBTRACK	15		// 구분진로비상해정 
*/
/////////////////////////////////////////////////////////////////////////////

// User 생성 변경 관련 메세지.
#define WS_USEROPMSG_CREATE			1
#define WS_USEROPMSG_DELETE			2
#define WS_USEROPMSG_USER_CHANGE	3
#define WS_USEROPMSG_PASS_CHANGE	4
#define WS_USEROPMSG_REQUEST_USER	5

// 통신 메세지 & 제어 메세지 & Functions Call Message
#define WS_OPMSG_CMDCLEAR		0		// CMD CLEAR
#define WS_OPMSG_START			1		// 시스템 기동
#define WS_OPMSG_RESET 			2		// 시스템 재기동
#define WS_OPMSG_SETTIMER		3		// UNIT 시계 조정(초,분,시,일,월,년)
#define WS_OPMSG_SETTRAINNAME	4		// 열차 번호 지정
#define WS_OPMSG_SIGNALDEST		5		// main 신호 취급
#define WS_OPMSG_SIGNALCANCEL	6		// 신호 취소
//#define WS_OPMSG_CASIGNALDEST	7		// call on신호 취급
//#define WS_OPMSG_SHSIGNALDEST	8		// shunt 신호 취급
#define WS_OPMSG_SWTOGGLE		9		// 선로전환기 토글(toggle)
#define WS_OPMSG_BUTTON			10		// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
#define WS_OPMSG_SIGNALSTOP		11		// 신호 (폐색 포함) STOP
#define WS_OPMSG_OSS			12		// Outer Signal Forced Stop 
#define WS_OPMSG_LOS			13		// Loop Track Toggle switch


// 궤도 메세지(LSM 로 메세지 경유해서 선택시만 출력), (CMD, ID, ON/OFF)
#define WS_OPMSG_MARKINGTRACK	14		// 궤도 사용금지/해제(모터카, 사용금지, 단선)
#define WS_OPMSG_FREESUBTRACK	15		// 구분진로비상해정 
#define WS_OPMSG_OBJCLICK       16
#define WS_OPMSG_HSW			17      // Hand switch operate for key release 
#define WS_OPMSG_CAON			18		// Block CA call
#define WS_OPMSG_LCROP			19		// Block line clear
#define WS_OPMSG_CBBOP			20		// Block 해제 request
#define WS_OPMSG_PAS            21      // PAS 버튼 IN/OUT
#define WS_OPMSG_PAS_IN         22      // PAS 버튼 IN
#define WS_OPMSG_PAS_OUT        23      // PAS 버튼 OUT
#define WS_OPMSG_LOS_ON         24      // LOS 버튼 ON
#define WS_OPMSG_LOS_OFF        25      // LOS 버튼 OFF
#define WS_OPMSG_NOUSE          26      // NOUSE 버튼 ON
#define WS_OPMSG_BLOCK          27      // BLOCK 버튼 control
#define WS_OPMSG_POS_ON         28      // POS 버튼 ON
#define WS_OPMSG_POS_OFF        29      // POS 버튼 OFF
#define WS_OPMSG_ASPECT			30		// Signal Aspect recovery
#define WS_OPMSG_BLOCK_CLEAR	31		// Block Clear Operation

// Function Call Message
#define WS_OPMSG_LOADFILE		32		// SFM Load
#define WS_OPMSG_ASSIGNSCRINFO	33		// Assign Info(Down.bin)
#define WS_OPMSG_UPDATESCREEN	34		// Update VMem
#define WS_OPMSG_SETLSMSIZE		35			// LSM SIZE (resizing)
#define WS_OPMSG_SETLSMOPTION	36		// LSM Option
// Fail Functions
#define WS_OPMSG_COMSTATUS		50		// 통신 상태 지정(0:/normal, other/fail)
#define WS_OPMSG_OPERATESTATE	51		// 제어 상태. 
//2001.07.04 Jin, 사용자 기능번호 추가
#define WS_OPMSG_SETDISPTIME	60		// 기록 시간 정보의 표시 유무(좌상의 기존 시간 밑에 표시) 
//...// 
#define WS_OPMSG_COUNTER		70		// SHORT 2 개 
#define WS_OPMSG_USERID			71		// 5BYTE
#define WS_OPMSG_AXL_COUNTER	72		// SHORT 2 개
#define WS_OPMSG_AXL_RESET		73		// AXL Reset
#define WS_OPMSG_REQ_ACTIVATE	74		// Activate요구.
#define WS_OPMSG_POINT_BLOCK	75		// Point Block toggle
#define WS_OPMSG_TRAIN_NUMBER	76		// Train Number
#define WS_OPMSG_TRACK_RESET	77		// Track AXL Reset

#define WS_OPMSG_CALLCHECKROUTE	90		// Call Check route in EipEmp class
#define WS_OPMSG_GETSCRINFO		98		// Get ScrInfo class Table ptr
#define WS_OPMSG_GETEMUPTR		99		// Get EipEmp class ptr

#define WS_OPMSG_SLS            101      // SLS 버튼 ON/OFF
#define WS_OPMSG_ATB            102      // ATB 버튼 ON/OFF
#define WS_OPMSG_ND             103      // NIGHT/DAY 버튼 ON/OFF
#define WS_OPMSG_ALARMCLR       104      // ALARM CLEAR
#define WS_OPMSG_BUZZERCLR      105      // BUZZER CLEAR
#define WS_OPMSG_UTG_CA			106      // UP 방향 TGB CA 버튼 
#define WS_OPMSG_UTG_LCR		107      // UP 방향 TGB LCR 버튼 
#define WS_OPMSG_UTG_CBB		108      // UP 방향 TGB CBB 버튼 
#define WS_OPMSG_UTC_CA			109      // UP 방향 TCB CA 버튼 
#define WS_OPMSG_UTC_LCR		110      // UP 방향 TCB LCR 버튼 
#define WS_OPMSG_UTC_CBB		112      // UP 방향 TCB CBB 버튼 
#define WS_OPMSG_DTG_CA			113      // DN 방향 TGB CA 버튼 
#define WS_OPMSG_DTG_LCR		114      // DN 방향 TGB LCR 버튼 
#define WS_OPMSG_DTG_CBB		115      // DN 방향 TGB CBB 버튼 
#define WS_OPMSG_DTC_CA			116      // DN 방향 TCB CA 버튼 
#define WS_OPMSG_DTC_LCR		117      // DN 방향 TCB LCR 버튼 
#define WS_OPMSG_DTC_CBB		118      // DN 방향 TCB CBB 버튼 

#define WS_OPMSG_CBBOPEMER		119      // CBB 비상취급
#define WS_OPMSG_CAACK			120      // CA Acknowledge 버튼

#define WS_OPMSG_SIGNALDEST_CTC	121		 // 진로 취급 by CTC

#define WS_OPMSG_MESSAGE		0xED		 // MESSAGE 전송

#define WS_OPMSG_CONT_IOSIM		200		// IO Sim에서 Object Control을 위한 Define

#define WS_OPMSG_OBJECTMODIFY	254		// 궤도 선택
#define WS_OPMSG_TRKSEL			255		// 궤도 선택
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// 사용금지 데이터 값
#define TRACKLOCK_FREE			0x10
#define ROUTELOCK_FREE			0x20
#define DROUTELOCK_FREE			0x30

#define TRACKLOCK_PWRDOWN		0x80
#define TRACKLOCK_MOTORCAR		0x80
#define TRACKLOCK_USING			0x80

/////////////////////////////////////////////////////////////////////////////

#endif // _OPMSGCODE_H
