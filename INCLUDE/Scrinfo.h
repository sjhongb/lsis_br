/* ----------------------------------------------------------------------- */
// ScrInfo.h
//

#ifndef _SCRINFO_H
#define _SCRINFO_H

/* ----------------------------------------------------------------------- */

#define MAX_MSGBLOCK_SIZE			2048
#define MAX_OPERATE_CMD_SIZE		16
#define MAX_USER_SIZE				8
#define MAX_PASS_SIZE				8
#define USER_CMD_SIZE				17
#define IPM_VERSION_SIZE			6
#define LCC_COM_SIZE				51
#define MAX_OPERATE_CMD_CODESIZE	8
#define MAX_OPERATE_CMD_UNITCODE	8

#define SIZE_SCR_HEADER_BYTE		48

/* ----------------------------------------------------------------------- */
#define SCRINFO_TRACK	1
#define SCRINFO_SIGNAL	2
#define SCRINFO_SWITCH	3
#define SCRINFO_BUTTON	4
#define SCRINFO_LAMP	5
#define SCRINFO_SYSTEM	99

#define OFFSET_DATA_VERSION	0x01
#define OFFSET_TIME_TABLE	0x02
#define OFFSET_TIME_SEC		0x02
#define OFFSET_TIME_MIN		0x03
#define OFFSET_TIME_HOUR	0x04
#define OFFSET_TIME_DAY		0x05
#define OFFSET_TIME_MON		0x06
#define OFFSET_TIME_YEAR	0x07
#define OFFSET_SYS_VAR		0x08
#define OFFSET_SYS_CONTROL	0x0B
#define OFFSET_CYCLE		0x10
#define OFFSET_COMM_STATUS	0x11
#define OFFSET_UNIT_STATUS	0x12
#define OFFSET_CARD_INFO	0x13
#define OFFSET_LCC_STATUS	0x23
#define OFFSET_COUNTER		0x24
#define OFFSET_SYS_VAR_EXT	0x2C

#ifdef _WINDOWS

#include "stdafx.h"

class CScrInfo 
{
// attribute
public:
//	int Load( FILE *fp );
//	void Save( FILE *fp );
	CString m_strName;
	BYTE *m_pEipData;
	short m_nMemOffset;
	short m_nType;
	short m_nID;

// Constructor
public:
	CScrInfo();
};

/* ----------------------------------------------------------------------- */
// Field Bit position of SystemStatusType struct.
typedef enum SysVarNoType {
// Byte 0
	BitNO_STATION = 64,     // Base Address = VMEM[8];
	BitNo_ModemRun,
	BitNo_Ground,
	BitNo_TrackOcc,
	BitNo_SYS1,
	BitNo_SYS1GOOD,
	BitNo_SYS2,
	BitNo_SYS2GOOD,
// Byte 1
	BitNo_N1,
	BitNo_N2,
	BitNo_N3,
	BitNo_B24PWR,
	BitNo_UPS1,
	BitNo_UPS2,
	BitNo_FUSE,
	BitNo_Discharger,
// Byte 2
	BitNo_Charger,
	BitNo_S,
	BitNo_P,
	BitNo_Sound1,		// 접근 
	BitNo_Sound2,		// S,P 부저(경고) 
	BitNo_Sound3,		// Power 경고
	BitNo_Sound4,		// 폐색
	BitNo_PowerFail,
// Byte 3
	BitNo_SysRun,
	BitNo_GenRun,
	BitNo_SysFail,
	BitNo_StartUp,
	BitNo_SubRequest,
	BitNo_Active,
    BitNo_ATB,
	BitNo_ComFail
};

/* ----------------------------------------------------------------------- */
typedef union {
	BYTE pVar[1];
	struct {
// OFFSET 0 -----------------------80    from 32904
	BYTE bLCCCom1Fail	:1;		// 0 LCC COM 1 status(1/failure, 0/normal)   
	BYTE bLCCCom2Fail	:1;		// 1 LCC COM 2 status(1/failure, 0/normal)
	BYTE bIFComFail		:1;		// 2 IF COM status(1/failure, 0/normal)
	BYTE bReserved1		:1;		// 3 DTS 1 communication status
	BYTE bReserved2		:1;		// 4 DTS 2 communication status
	BYTE bReserved3		:1;		// 5 MODEM communication status for block with Middle station
	BYTE bReserved4		:1;		// 6 MODEM communication status for block with Up station
	BYTE bReserved5		:1;		// 7 MODEM communication status for block with Down station
	};
} CommStatusType;


typedef union {
	BYTE pVar[1];
	struct {
// OFFSET 0 -----------------------88    from 32912
	BYTE bSysRun		:1;		// 0 0/fail-flashing, 1/normal            
	BYTE bSysFail		:1;		// 1 bSysGood 과 같음. 장애 발생시 Set(Cpu 동작중의 장애 발생시 포함) 
	BYTE bSys1StartUp	:1;		// 2 cbi 1 boot up. 
	BYTE bSys2StartUp	:1;		// 3 cbi 2 boot up.
	BYTE bSubRequest	:1;		// 4 자기계가 서브로 전환할려고 상대계에 요구
	BYTE bActiveOn1		:1;		// IN 5 out running CBI 1 unit (main/sub).
	BYTE bActiveOn2		:1;		// IN 6 out running CBI 2 unit (main/sub).
	BYTE bComFail		:1;		// 7 COM1 and COM2 all failure.
	};
} UnitStatusType;

typedef union {
	BYTE pVar[1];
	struct {                          
	BYTE bLCC1Active	:1;		// 0 LCC1 is Active(Online)
	BYTE bLCC2Active	:1;		// 1 LCC2 is Active(Online)
	BYTE bLCC1Alive		:1;		// 2 LCC1 communication is alive(1/alive, 0/dead).
	BYTE bLCC2Alive		:1;		// 3 LCC2 communication is alive(1/alive, 0/dead).
	BYTE bSIMModeSW		:1;		// 4 r1
	BYTE bSIMModeIO		:1;		// 5 r2
	BYTE bSIMMode3		:1;		// 6 r3
	BYTE bSIMMode4		:1;		// 7 r4
	};
} LCCStatusType;

typedef union {
	BYTE pVar[1];
	struct {                          
	BYTE bCom1Active	:1;		// 0 communication by the LCC 1
	BYTE bCom2Active	:1;		// 1 communication by the LCC 2
	BYTE bLCC1CtrlLock	:1;		// 2 operation lock control by the LCC 1 (1/locking, 0/normal)
	BYTE bLCC2CtrlLock	:1;		// 3 operation lock control by the LCC 2 (1/locking, 0/normal)
	BYTE bCom1Online	:1;		// 4 IPC 1 communication is online(1/online, 0/failure).
	BYTE bCom2Online	:1;		// 5 IPC 2 communication is online(1/online, 0/failure).
	BYTE breserved3		:1;		// 6 r3
	BYTE breserved4		:1;		// 7 r4
	};
} LCCCommStatusType;

typedef struct {
	union {
		BYTE UserData[17];
		struct {
			BYTE UserMsgType;
			BYTE UserName[8];
			BYTE UserPassword[8];
		};
	};
} RealUserDataType;

typedef struct {
	union {
		BYTE pSendData[51];
		struct {
			BYTE OperateData[16];
			BYTE CurrentUserName[8];
			BYTE CurrentUserPass[8];
			RealUserDataType RealUserData;
			BYTE CRCCheck[2];
		};
	};
} LCCCommDataType;

// Address 44*8-64=288
typedef union {
	BYTE pVar[4];
	struct {                          
		BYTE bLC1KXLR    	:1;		//33120// 288 - 0 LC1 OUT KEY Control (1/Can Key Release, 0/Cannot) 33120
		BYTE bLC1NKXPR		:1;		//33121// 289 - 1 LC1 IN  KEY Indication (1/normal,Key가 건널목에 있음, 0/locking,key가 transmitter에 있음) 33121
		BYTE bLC1BELL		:1;		//33122// 290 - 2 LC1 OUT BELL Control (1/Belling, 0/Normal)
		BYTE bLC1BELACK		:1;		//33123// 291 - 3 LC1 IN  BELL ACKnowledge (1/Ack, 0/Normal) 
		BYTE bLC2KXLR    	:1;		//33124// 292 - 4 LC2 OUT KEY Control (1/Can Key Release, 0/Cannot) 33124
		BYTE bLC2NKXPR		:1;		//33125// 293 - 5 LC2 IN  KEY Indication (1/normal, 0/locking) 33125
		BYTE bLC2BELL		:1;		//33126// 294 - 6 LC2 OUT BELL Control (1/Belling, 0/Normal)
		BYTE bLC2BELACK		:1;		//33127// 295 - 7 LC2 IN  BELL ACKnowledge (1/Ack, 0/Normal) 

		BYTE bLC3KXLR    	:1;		//33128// 296 - 8  LC3 OUT KEY Control (1/Can Key Release, 0/Cannot) 33128
		BYTE bLC3NKXPR		:1;		//33129// 297 - 9  LC3 IN  KEY Indication (1/normal,Key가 건널목에 있음, 0/locking,key가 transmitter에 있음) 33129
		BYTE bLC3BELL		:1;		//33130// 298 - 10 LC3 OUT BELL Control (1/Belling, 0/Normal)
		BYTE bLC3BELACK		:1;		//33131// 299 - 11 LC3 IN  BELL ACKnowledge (1/Ack, 0/Normal) 
		BYTE bLC4KXLR    	:1;		//33132// 300 - 12 LC4 OUT KEY Control (1/Can Key Release, 0/Cannot) 33132
		BYTE bLC4NKXPR		:1;		//33133// 301 - 13 LC4 IN  KEY Indication (1/normal, 0/locking) 33133
		BYTE bLC4BELL		:1;		//33134// 302 - 14 LC4 OUT BELL Control (1/Belling, 0/Normal)
		BYTE bLC4BELACK		:1;		//33135// 303 - 15 LC4 IN  BELL ACKnowledge (1/Ack, 0/Normal) 

		BYTE bPKeyA   	 	:1;		//33136// 304
		BYTE bPKeyB			:1;		//33137// 305
		BYTE bPKeyC			:1;		//33138// 306
		BYTE bPKeyD			:1;		//33139// 307
		BYTE bPKeyE   	 	:1;		//33140// 308
		BYTE bPKeyF			:1;		//33141// 309
		BYTE bPKeyG			:1;		//33142// 310
		BYTE reserved		:1;		//33143// 311
                      
		BYTE bSound1   		:1;		//33144// 312	접근, 폐색 경보
		BYTE bSound2		:1;		//33145// 313	S,P 부저(경고)
		BYTE bSound3		:1;		//33146// 314	Power 경고
		BYTE bSound4		:1;		//33147// 315	TCB CA 경보
		BYTE bFan3			:1;		//33148// 316
		BYTE bFan4			:1;		//33149// 317
		BYTE bFuse3			:1;		//33150// 318
		BYTE bFuse4			:1;		//33151// 319
	};
} SystemStatusTypeExt;

// Base Address 0x08                    from  32832
typedef union {
	BYTE pVar[8];
	struct {
	BYTE bStation	:1;	//0  -32832- Station open/close (1 - close, 0 - open)
	BYTE bTCSR		:1;	//1  -32833- 1 - All Track circuits are deenergizer (역 폐쇄 시 적용 됨)
	BYTE bGround	:1;	//2	 -32834- IN  Ground detect (0 - Green 정상, 1 - Red Failure)
	BYTE bTrackOcc	:1;	//3  -32835- Track occupy and signal lamp can prove
	BYTE bSYS1 		:1;	//4  -32836- Sys1 online status (1 - Green, 0 - Gray)
	BYTE bSYS1Good	:1;	//5  -32837- Sys1 status (1 - Gray, 0 - Red flashing)
	BYTE bSYS2 		:1;	//6  -32838- Sys2 online status (1 - Green, 0 - Gray)
	BYTE bSYS2Good	:1;	//7  -32839- Sys2 status (1 - Gray, 0 - Red flashing)

	BYTE bN1   		:1;	//8   -32840- PAS (1 - Lock, Operation)
	BYTE bN2	   	:1;	//9   -32841- OUT  Day/Night (1 - Night, 0 - Day Operation) 
	BYTE bN3  		:1;	//10  -32842- SLS  (1 - On, Signal lamp can prove, Operation)
	BYTE bB24PWR  	:1;	//11  -32843- IN  internal B24 (1 - Green, 0 - Red Flashing)
	BYTE bUPS1 		:1;	//12  -32844- IN  UPS1 (1 - Good)
	BYTE bUPS2		:1;	//13  -32845- IN  UPS2 (1 - Good)
	BYTE bGenLowFuel:1;	//14  -32846- IN  GEN-LOWFUEL (1 - Good, 0 - White)
	BYTE bGen		:1;	//15  -32847- IN  Generator status (1 - Gray, 0 - Ref Flashing)

	BYTE bCharge	:1;	//16  -32848- IN  Battery Charger status   (1 - Good)
	BYTE bSignalFail:1;	//17  -32849- S infomation  (0 - Good, 1 - Fail)
	BYTE bPointFail :1;	//18  -32850- P information (0 - Good, 1 - Fail)
    BYTE bAXLFail 	:1;	//19  -32851- AXL information (0 - Good, 1 - Fail) 
	BYTE bMidModem  :1;	//20  -32852- Mid Modem status (1 - Green, 0 - Red flashing)
    BYTE bLC5KXLR	:1;	//21  -32853- OUT LC5 KEY Control (1/Can Key Release, 0/Cannot)
    BYTE bLC5NKXPR	:1;	//22  -32854- IN  LC5 KEY Indication (1/normal, 0/locking)
    BYTE bPowerFail :1;	//23  -32855- IN  Main power (1 - Good)

	BYTE bIActiveOn1:1;	//24  -32856- IN signal of the CBI 1 running status.(1/on, 0/off)
	BYTE bIActiveOn2:1;	//25  -32857- IN signal of the CBI 2 running status.(1/on, 0/off)
	BYTE bIVPOR1	:1;	//26  -35858- IN signal of the system 1 VPOR.(1/on, 0/off)
	BYTE bIVPOR2	:1;	//27  -32859- IN signal of the system 2 VPOR.(1/on, 0/off)
	BYTE bVPOR1		:1;	//28  -32860- OUT signal of the logical OK.(1/on, 0/off)
	BYTE bVPOR2		:1;	//29  -32861- OUT signal of the Physical OK.(1/on, 0/off)
	BYTE bMode		:1;	//30  -32862- 0:LOCAL, 1:CTC 
	BYTE bCTCRequest:1; //31  -32863- 0:NORMAL, 1:REQUEST

    BYTE bStartUp	:1;	//32  -32864- Start up 내부사용
	BYTE bGenRun	:1;	//33  -32865- IN  Generator running (1 - Green flashing, 0 - Gray)
    BYTE bATB	    :1;	//34  -32866- ATB test (1 - ATB test, and after 60sec 0)
	BYTE bExtPowerS :1;	//35  -32867- South External power (1 - Good) 
    BYTE bUpModem	:1;	//36  -32868- Up Modem status (1 - Green, 0 - Red flashing)  
    BYTE bDnModem	:1;	//37  -32869- Down Modem status (1 - Green, 0 - Red flashing)  
    BYTE bFan1		:1; //38  -32870- IN  Fan 1계 status (1 - Good)
    BYTE bFan2		:1;	//39  -32871- IN  Fan 2계 status (1 - Good)

    BYTE bActive1	:1;	//40  -32872- OUT 1계 동작 요구
    BYTE bActive2	:1;	//41  -32873- OUT 2계 동작 요구
    BYTE bFuse1     :1;	//42  -32874- IN  Fuse 1계 status (1 - Good)
	BYTE bFuse2  	:1;	//43  -32875- IN  Fuse 2계 status (1 - Good)
    BYTE bCharging  :1;	//44  -32876- IN  충전중 (0 - 충전중)
    BYTE bLowVoltage:1;	//45  -32877- IN  방전   (0 - 방전) 
    BYTE bExtPowerN :1;	//46  -32878- IN  North External Power (1 - Good)
    BYTE bIDayNight	:1; //47  -32879- IN  Day Night (1 - Night)

    BYTE bALR		:1;	//48  -32880- OUT  신호기 ALR 제어 (1 - Signal Dark 요구)
    BYTE bTCS		:1;	//49  -32881- OUT All track 회로 단절 
   	BYTE bPKeyN		:1;	//50  -32882- IN  North(Up) Point key 표시 (1 - Key 삽입)
    BYTE bPKeyS		:1;	//51  -32883- IN  South(Down) Point key 표시
    BYTE bPKeyContN	:1;	//52  -32884- OUT North(Up) Point key 제어 (1 - Key 제거 가능)
    BYTE bPKeyContS	:1; //53  -32885- OUT SOUTH(Down) Point key 제어
	BYTE bFusePR	:1;	//54  -32886- IN  Power Rack Fuse status ( 1 - Good)
	BYTE bLC5BELL	:1;	//55  -32887- OUT LC5 BELL Control (1/Belling, 0/Normal)

	BYTE bLC5BELACK	:1;	//56  -32888- IN  LC5 BELL ACKnowledge (1/Ack, 0/Normal)
    BYTE bPKeyContA :1;	//57  -32889- OUT Group A Point key 제어  
    BYTE bPKeyContB :1; //58  -32890- OUT Group B Point key 제어
    BYTE bPKeyContC :1;	//59  -32891- OUT Group C Point key 제어 
    BYTE bPKeyContD :1; //60  -32892- OUT Group D Point key 제어
    BYTE bPKeyContE :1;	//61  -32893- OUT Group E Point key 제어
    BYTE bPKeyContF :1; //62  -32894- OUT Group F Point key 제어
    BYTE bPKeyContG :1;	//63  -32895- OUT Group G Point key 제어 
	};
} SystemStatusType;

/* ----------------------------------------------------------------------- */

typedef enum SysButtonIDTypes { 
// Id = 0xE0  224
	BtnPas = 0x0E0,			// PAS   1 - Lock
	BtnDay,					// Day/Night  1 - Day
	BtnSls,					// SLS  1 - On (Signal power off)
	BtnS,					// event, Signal failure  
	BtnP,					// event, Point failure
	BtnFuse,				// event, Fuse failure
	BtnComm,				// event, Modem Comm failure
	BtnSysRun,				// event, System Run
// ID = 0xe8  232
	BtnSys1,				// System 1 CPU display
	BtnSys2,				// System 2 CPU display
	BtnPower,				// Main power failure 
	BtnGen,					// Generator failure & running
	BtnC,					// Left side OSS control >> Left, Right 구분없음.
	BtnD,					// Right side OSS control >> Reserved
	BtnAtb,					// ATB   1 - ATB on 
	BtnUps2,				// UPS2 failure
// ID = 0xf0  240
	BtnGround = 0x0F0,		// Ground failure
	BtnUps1,				// UPS1 failure
	BtnBattery,				// Battery Charger
	BtnB24,					// B24V failure
	BtnAck,					// Alarm ack & off -- SDB function
	BtnPDB,					// Left CA  -- PDB function
	BtnGnd,					// Ground detect...
	BtnClose,		   		// Station Close or Open
// ID = 248
	BtnReserved,            // Reserved
	BtnLoop1,				// Loop1 Deenergy << Parameter로 요놈만 사용.
	BtnLoop2,				// Loop2 Deenergy << 1만사용. 요놈은 사용안함.
	BtnAPKup,               // up dir. APK
	BtnAPKdn,               // down dir. APK
	BtnLCB,                 // Level Crossing Control Button
	BtnLCA,                 // Level Crossing Call
	BtnNOUSED,              // NOuse 버튼
// ID = 256
	BtnB1ClearReq = 0x00,// (0x00)
	BtnB2ClearReq,		// 
	BtnLoop3,				//  Loop3 Deenergy << 1만사용. 요놈은 사용안함.
	BtnLoop4,				//  Loop4 Deenergy << 1만사용. 요놈은 사용안함.
	BtnLoop5,               //  Loop5 Deenergy << 1만사용. 요놈은 사용안함.
	BtnAPKA, 				// 
	BtnAPKB, 				// 
	BtnAPKC, 				// 
// ID = 264	(0x08)
 	BtnAPKD, 				// 
	BtnAPKE, 				// 
	BtnAPKF, 				// 
	BtnAPKG, 				// 
	BtnB2AXLRST, 			// 
	BtnB4AXLRST, 			// 
	BtnLC2B,				// LC2 Control Button
	BtnLC2A,				// LC2 Call Button
// ID = 272	(0x10)
	BtnB3ClearReq,
	BtnB4ClearReq,
	BtnLocalMode,			// Local Mode로 강제변경.
	BtnCTCMode,				// CTC Request시 CTC Mode 승인
	BtnB11ClearReq,
	BtnB12ClearReq,
	BtnLC3B,
	BtnLC3A,
//	ID = 280 (0x18)
	BtnLC4B,
	BtnLC4A,
	BtnTKAXLRST,
	BtnB1AXLRSTACC,
	BtnB1AXLRSTDEC,
	BtnB3AXLRSTACC,
	BtnB3AXLRSTDEC,
	BtnB13ClearReq,
//	ID = 288 (0x20)
	BtnB14ClearReq,
	BtnB1ClearAcc,
	BtnB2ClearAcc,
	BtnB3ClearAcc,
	BtnB4ClearAcc,
	BtnB11ClearAcc,
	BtnB12ClearAcc,
	BtnB13ClearAcc,
//	ID = 296 (0x28)
	BtnB14ClearAcc,
	BtnB1ClearDec,
	BtnB2ClearDec,
	BtnB3ClearDec,
	BtnB4ClearDec,
	BtnB11ClearDec,
	BtnB12ClearDec,
	BtnB13ClearDec,
//	ID = 304 (0x30)
	BtnB14ClearDec,
	BtnLC5B,
	BtnLC5A,
};

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE pData[SIZE_SCR_HEADER_BYTE];			
		struct {
			BYTE nMsgHeader;	// 부계:0x05, 주계0x0A
			BYTE nDataVer;
			BYTE nSec;
			BYTE nMin;
			BYTE nHour;
			BYTE nDate;
			BYTE nMonth;
			BYTE nYear;
// Offset 0x08 - 0x0F (8 Bytes)
			SystemStatusType nSysVar;
// Offset 0x10
			BYTE Cycle;
// Offset 0x11 (1 Byte)
			CommStatusType	CommState;		// Make status for EID SW(Not control system).
// Offset 0x12 (1 Byte)
			UnitStatusType	UnitState;		// Unit status
// Offset 0x13 - 0x22 (16Bytes)
			BYTE IOC1Status[8]; 			// rack 1 - 8	D0:IOC, D1-D7:Card 1-7
			BYTE IOC2Status[8]; 			// 
// Offset 0x23
			LCCStatusType	LCCStatus;
// Offset 0x24 - 0x2B (8Bytes)
			unsigned short Counter[4];	// 계수기 -> 0 - Callon 신호 현시, 1 - 비상진로 해정
// Offset 0x2C - 0x2F			
			SystemStatusTypeExt nSysVarExt;
		};
	};
} DataHeaderType;

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE data[2];
		struct {
			BYTE ALARM		:1;	// 0 Approach
// internal
			BYTE TRACK		:1; // 1
			BYTE ROUTE		:1;	// 2 1:NORMAL, 0/LOCK(GRREN)
			BYTE TKREQUEST	:1; // 3
			BYTE OVERLAP	:1; // 4
			BYTE ROUTEDIR	:1;	// 5 
// in
			BYTE EMREL		:1;	// 6 Route Emergency stop
			BYTE INHIBIT	:1;	// 7 COS (궤도 사용금지 Operation)

			BYTE DISTURB	:1;	// 8	13역 AXL Distrub INPUT
			BYTE RESETIN	:1; // 9	13역 AXL Track Field Reset INPUT
			BYTE RESETOUT	:1;	// 10	13역 AXL Track Reset OUTPUT
			BYTE RESETBUF	:1; // 11	13역 AXL Track Field Reset Buffer (30초간 유지)
			BYTE MULTIDEST	:1; // 12
			BYTE BLKTRACE	:1;	// 13	13역 폐색 퇴행 추적을 위한 필드 추가
			BYTE BLKSWEEP	:1; // 14	폐색 씨발 스위핑을 위한 비트 추가 
			BYTE BLKILLOCC	:1;	// 15	폐색 씨발 불법 점유를 위한 비트 추가
		};
	};
	WORD RouteNo;				// 20140527 sjhong - 진로 설정될 경우 해당 진로번호 저장을 위해 생성
	WORD RightToLeftRouteNo;	// 20130904 sjhong - Overlap 설정된 Track에 진로번호 저장을 위해 생성	// 20140519 sjhong 2bytes 진로번호 수용가능하도록 수정
	WORD LeftToRightRouteNo;	// 20130904 sjhong - Overlap 설정된 Track에 진로번호 저장을 위해 생성	// 20140519 sjhong 2bytes 진로번호 수용가능하도록 수정
// 	void ClearRoute() {
// 		ROUTE = 1;
// 		TKREQUEST = 0;
// 		EMREL = 0;
// 		ROUTEDIR = 0;
// 	}
} ScrInfoTrack;


typedef struct {
	union {
		BYTE pData[3];
		struct {
			BYTE BitIN		:4;
			BYTE BitLOCK	:2;
			BYTE BitOUT		:2;
			BYTE BitINTERNAL:8;
		};
		struct {
// BYTE 0
// in
			BYTE KR_P		:1;     //0 
			BYTE KR_M		:1;     //1 
			BYTE WR_P		:1;     //2 WRI - N
			BYTE WR_M		:1;     //3 WRI - R
			BYTE WLR		:1;     //4 WLRI
// out
			BYTE FREE		:1;		//5 1=전환가능 상태, 0 쇄정
			BYTE WRO_M		:1;     //6 Output WRO - R
			BYTE WRO_P		:1;     //7 Output WRO - N
// BYTE 1
// internal
			BYTE BACKROUTE  :1;     //8
			BYTE ROUTELOCK	:1;     //9
			BYTE REQUEST_P	:1;     //10
			BYTE REQUEST_M	:1;     //11
			BYTE ON_TIMER	:1;     //12
            BYTE WLRO       :1;     //13 Output (1/control enable, 0/lock) 
            BYTE SWFAIL     :1;     //14
            BYTE TRACKLOCK  :1;     //15
// BYTE 2
			BYTE NOUSED		:1;		//16 샤이스타간지 27전철기의 경우 이것이 1이면 27전철기를 지나가는 진로를 설정할 수 없다.
			BYTE KEYOPER	:1;		//17 수동전철기의 Key 체결 해제 취급 (KWLZR	--- Key를 뺄 수 있는 상태 - 1, Key 뺄 수 없는 상태 - 0)
			BYTE WKEYKR		:1;		//18 전철기 KEY 체결 (NKWEPR 체결 - 1)
			BYTE ROUTE_P	:1;		//19
			BYTE ROUTE_M	:1;		//20
			BYTE INROUTE	:1;		//21
			BYTE BLOCK		:1;		//22 20130812 선로전환기 Block을 위해 사용.
			BYTE PASSROUTE	:1;		//23 20140716 sjhong 통과 진로 시 조사 쇄정 Flashing 안하도록 하는 정보
		};
	};
} ScrInfoSwitch;

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE pData[4];
		struct {
			BYTE BitOUT		:4;
			BYTE BitIN		:4;
			BYTE BitLMR		:4;
			BYTE BitINTERNAL1:4;
			BYTE BitINTERNAL2:8;
			BYTE BitINTERNAL3:8;
		};
		struct {
// OFFSET 0
// out
			BYTE SOUT1		:1;     //0 S1 - DR (G)
			BYTE SOUT2		:1;     //1 S2 - HR (Y)
			BYTE SOUT3		:1;     //2 S3 - CALLON (Outer Yellow)
			BYTE U			:1;     //3 S4 - SHUNT 
// in
			BYTE SIN1		:1;     //4 - DR (G - DR Control Relay Contact input - LOR Relay contact과 and 해야함)
			BYTE SIN2		:1;     //5 - HR (Y - HR Control Relay Contact input - LOR Relay contact과 and 해야함)
			BYTE SIN3		:1;     //6 - CR or HHR ( CR or HHR Control Relay Contact input )
			BYTE INU		:1;     //7 - SHUNT 
//=====================================
// OFFSET 1
// 필요한 제어신호, 실제 출력은 G,Y,YY로 나감.
			BYTE SE1		:1;     //8  - CTR DR
			BYTE SE2	    :1;     //9  - CTR HR
			BYTE SE3		:1;     //10 - CTR CALLON
			BYTE SE4		:1;     //11 - CTR SHUNT (Outer Yellow)
// internal
			BYTE LEFT		:1;     //12 - LEFT Direction Output (실제 제어는 하지 않음)
            BYTE RIGHT	    :1;     //13 - Right Direction Output (실제 제어는 하지 않음)
			BYTE CS			:1;		//14	; Callon, Shunt진로 설정 중 set...
			BYTE FREE		:1;		//15 - 신호 현시 상태, 0-쇄정
//=====================================
// OFFSET 2
            BYTE TM1		:1;     //16 - Internal Timer 1
			BYTE SignalFail	:1;     //17 - Signal Fail (Failure = 1)
			BYTE ON_TIMER	:1;     //18 - Approach timer running
			BYTE REQUEST	:1;     //19 - Stored route request
			BYTE ENABLED	:1;		//20 - 신호현시 상태에서 전방 궤도가 낙하하여 신호가 정지한 경우 세트
			BYTE RCVRONLY	:1;     //21 - 중계 신호 등에서 Signal Recovery 제어만 가능하도록 하는 비트
            BYTE INLEFT     :1;     //22 - IN, Left Direction  (LRDIRLOR relay)
            BYTE INRIGHT    :1;		//23 - IN, Right Direction (RDIRLOR relay)
//=====================================
// OFFSET 3
            BYTE LM_LOR     :1;     //24 - IN,  Signal main lamp aspect detection (ECPR)
            BYTE LM_ALR     :1;     //25 - Signal Lighting Dark Operation control (모든 신호기 중 한개라도 1일 경우 System 출력제어)
            BYTE LoopSigFail:1;     //26 - Loop Signal Lamp Fail
            BYTE LM_SLOR	:1;     //27 - Shunt lamp aspect detection
            BYTE BLKCOMM	:1;     //28 - Block Communication Status (Always 1)
            BYTE ADJCOND	:1;     //29 - Additional signal aspect condition from adjacent station
			BYTE LM_CLOR	:1;     //30 - IN, CLOR, HHLOR Signal lamp detection (Callon, S-Yellow ECPR)
            BYTE OSS        :1;     //31 - OUT, Outer Signal STOP Operation (1 - R(stop 유지), 0 - N), SHUNT일 경우 PRESET으로 내부사용...
		};
	};

	BYTE DESTTRACK;
	BYTE DESTID;

	BYTE GetSignalInState( int nSigAspType, BYTE &flmrBit );
	BYTE GetSignalControlState( int nSignalType );
	BYTE GetSignalLampState();

} ScrInfoSignal;

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE pData[6];
		struct {
// OFFSET 0
			BYTE CA		    :1;     // 0  폐색 요청 (TGB CA - G, TCB CA - GF) 
			BYTE LCR		:1;     // 1  라인비우기 요청 수행 (TGB LCR - G, TCB LCR - G, TGB - W, TCB - W)
									//    TGB의 경우 LCROPR이 1이면 1, TCB의 경우 LCROPR + CONFIRM이 모두 1이면 1
			BYTE LCRIN		:1;     // 2  IN (TGB - LCR이 1일 경우 Wf, TCB - Wf)
			BYTE LCROP		:1;     // 3  LCR 운영자 제어시 1 (TGB LCR - Y, TCB LCR - Y)
            BYTE CBB	    :1;     // 4  폐색 해정 요청 (TGB CBB - G, TCB CBB - G, TGB, TCB는 적색)
									//    TCB의 경우 CBBOPR + ARRIVETR이 모두 1이면 1
			BYTE CBBIN	    :1;     // 5  IN (TGB, TCB는 변화 없음)
            BYTE CBBOP		:1;     // 6  CBB 운영자 제어시 1 (TGB CBB - Y, TCB CBB - Y)
            BYTE COMPLETEIN :1;		// 7  폐색수속 완료 입력
			//=====================================
// OFFSET 1
            BYTE CONFIRM    :1;     // 8  선행S와 외부S사이 해정 상태 (TGB, TCB는 변화 없음 - 항상Check)
            BYTE ASTART	    :1;     // 9  선행S Clear 상태 (TGB, TCB는 변화 없음 - 항상Check)
			                        //    AS는 TGB의 LCR, LCRIN이 모두 1인 경우 취급가능, CBB취급은 ASTART가 0에서만 가능(1이면 CBB 회색)
            BYTE DEPART     :1;     // 10 선행S 궤도 점유 - 정보Stick  (TGB - R)
            BYTE DEPARTIN   :1;     // 11 TCB - R
            BYTE ARRIVE     :1;     // 12 열차 도착 (TCB - RF, 이때 TGB CA가 1, COMPLETE이 0이면 TGB는 COMPLETE 수행 - 항상Check)
            BYTE ARRIVEIN   :1;     // 13 TGB - R (항상Check)
            BYTE SWEEPINGACK:1;     // 14 Train Sweeping 완료 상태 비트
            BYTE COMPLETE   :1;     // 15 폐색수속 완료 - CBBIN에 의해 수행 CA,LCR,CBB는 무표시 (TGB, TCB 무표시)
			//=====================================
// OFFSET 2
            BYTE BLKRSTREQ  :1;     // 16	BGR/BCR RESET REQUEST OUT
            BYTE BLKRSTREQIN:1;     // 17	BGR/BCR RESET REQUEST IN
			BYTE AXLRST		:1;		// 18  
			BYTE AXLOCC		:1;		// 19
			BYTE AXLDST		:1;		// 20  
			BYTE ASTARTSTICK:1;		// 21  TGB의 경우 ASTART signal 이 설정되면 STICK -- (1 -- STICK)
			BYTE CAACK		:1;		// 22  CA ACKnowledge   -- (1 - ACKnowledge)
			BYTE SWEEPING	:1;		// 23  Train Sweeping 상태 비트
// OFFSET 3
            BYTE NBGR       :1;     // 24  OUT
            BYTE RBGR 	    :1;     // 25  OUT
			BYTE NBCR	    :1;		// 26  OUT
			BYTE RBCR		:1;		// 27  OUT
			BYTE NBGPR		:1;		// 28  IN
			BYTE RBGPR		:1;		// 29  IN
			BYTE NBCPR		:1;		// 30  IN
			BYTE RBCPR		:1;		// 31  IN
// OFFSET 4
			BYTE AXLREQ		:1;		// 32
			BYTE AXLACC		:1;		// 33
			BYTE AXLDEC		:1;		// 34
			BYTE BLKRSTACC	:1;		// 35	BGR/BCR RESET ACCEPT OUT
			BYTE BLKRSTACCIN:1;		// 36	BGR/BCR RESET ACCEPT IN
			BYTE BLKRSTDEC	:1;		// 37	BGR/BCR RESET DECLINE OUT
			BYTE BLKRSTDECIN:1;		// 38	BGR/BCR RESET DECLINE IN
			BYTE AXLOCCDOUT	:1;		// 39	PBY역 AXLOCC 정보 DOUT 출력 / 폐색장치 모드에서 Adv. Starter 제어 허용 출력(ASCR)
// OFFSET 5
			BYTE SAXLRST	:1;		// 40
			BYTE SAXLOCC	:1;		// 41
			BYTE SAXLDST	:1;		// 42
			BYTE SAXLREQ	:1;		// 43
			BYTE SAXLACC	:1;		// 44
			BYTE SAXLDEC	:1;		// 45
            BYTE SAXLACT    :1;     // 46  Station Close & PAS OUT & SAXL_USE for my Station
			BYTE SAXLACTIN	:1;		// 47  Station Close & PAS OUT & SAXL_USE from Adj. Station
		};
	};
} ScrInfoBlock;

/* ----------------------------------------------------------------------- */
#define BIT_LMRFAIL_Y1	1
#define BIT_LMRFAIL_G	2
#define BIT_LMRFAIL_R	4
#define BIT_LMRFAIL_Y	8

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE data;
		struct {
			BYTE BitLamp	:1;
		};
	};
} ScrInfoLamp;

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE data;
		struct {
			BYTE SHIFT_LEFT	:1;
			BYTE SHIFT_RIGHT	:1;
		} bits;
	};
} ScrInfoTrkNode;

/* ----------------------------------------------------------------------- */

#else  //_WINDOWS (System Side)

#ifdef _WINDOWS
#include "stdio.h"
#endif

typedef unsigned char BYTE;

class CScrInfo {
// attribute
public:
	char m_szName[32];
	BYTE *m_pEipData;
	short m_nMemOffset;
	short m_nType;
	short m_nID;

public:
	CScrInfo();
};

/* ----------------------------------------------------------------------- */
// Field Bit position of SystemStatusType struct.
typedef enum SysVarNoType {
// Byte 0
	BitNO_STATION = 64,     // Base Address = VMEM[8];
	BitNo_ModemRun,
	BitNo_Ground,
	BitNo_TrackOcc,
	BitNo_SYS1,
	BitNo_SYS1GOOD,
	BitNo_SYS2,
	BitNo_SYS2GOOD,
// Byte 1
	BitNo_N1,
	BitNo_N2,
	BitNo_N3,
	BitNo_B24PWR,
	BitNo_UPS1,
	BitNo_UPS2,
	BitNo_FUSE,
	BitNo_Discharger,
// Byte 2
	BitNo_Charger,
	BitNo_S,
	BitNo_P,
	BitNo_Sound1,		// 접근 
	BitNo_Sound2,		// S,P 부저(경고) 
	BitNo_Sound3,		// Power 경고
	BitNo_Sound4,		// 폐색
	BitNo_PowerFail,
// Byte 3
	BitNo_SysRun,
	BitNo_GenRun,
	BitNo_SysFail,
	BitNo_StartUp,
	BitNo_SubRequest,
	BitNo_Active,
    BitNo_ATB,
	BitNo_ComFail
};


/* ----------------------------------------------------------------------- */


typedef struct {
	BYTE bSYS2Good	:1;	//7  -- Sys2 status (1 - Gray, 0 - Red flashing)
	BYTE bSYS2 		:1;	//6  -- Sys2 online status (1 - Green, 0 - Gray)
	BYTE bSYS1Good	:1;	//5  -- Sys1 status (1 - Gray, 0 - Red flashing)
	BYTE bSYS1 		:1;	//4  -- Sys1 online status (1 - Green, 0 - Gray)
	BYTE bTrackOcc	:1;	//3  -- Track occupy and signal lamp can prove
	BYTE bGround	:1;	//2	 -- IN  Ground detect (0 - Green 정상, 1 - Red Failure)
	BYTE bTCSR		:1;	//1  -- 
	BYTE bStation	:1;	//0  -- Station open/close (1 - close, 0 - open)

	BYTE bGen		:1;	//15  -- IN  Generator status (1 - Gray, 0 - Ref Flashing)
	BYTE bGenLowFuel:1;	//14  -- IN  FUSE (1 - Good)
	BYTE bUPS2		:1;	//13  -- IN  UPS2 (1 - Good)
	BYTE bUPS1  	:1;	//12  -- IN  UPS1 (1 - Good)
	BYTE bB24PWR  	:1;	//11  -- IN  internal B24 (1 - Green, 0 - Red Flashing)
	BYTE bN3  		:1;	//10  -- SLS  (1 - On, Signal lamp can prove, Operation)
	BYTE bN2	   	:1;	//9   -- OUT  Day/Night (1 - Night, 0 - Day Operation) 
	BYTE bN1   		:1;	//8   -- PAS (1 - Lock, Operation)

    BYTE bPowerFail :1;	//23  -- IN  Main power (1 - Good)
    BYTE bLC5NKXPR	:1;	//22  -- LC5  KEY Indication (1/normal, 0/locking)
    BYTE bLC5KXLR	:1;	//21  -- LC5  KEY Control (1/Can Key Release, 0/Cannot)
	BYTE bMidModem	:1;	//20  -- Mid Modem status (1 - Green, 0 - Red flashing)
    BYTE bAXLFail 	:1;	//19  -- AXL information (1 - Fail, 0 - Good)  
	BYTE bPointFail :1;	//18  -- P information (1 - Fail, 0 - Good)
	BYTE bSignalFail:1;	//17  -- S infomation  (1 - Fail, 0 - Good)
	BYTE bCharge	:1;	//16  -- IN  Battery Charger status   (1 - Good)

	BYTE bCTCRequest:1; //31 -- 0:NORMAL, 1:REQUEST.
	BYTE bMode		:1;	//30 -- 0:LOCAL, 1:CTC.
	BYTE bVPOR2		:1;	//29 -- OUT signal of the Physical OK.(1/on, 0/off)
	BYTE bVPOR1		:1;	//28 -- OUT signal of the logical OK.(1/on, 0/off)
	BYTE bIVPOR2	:1;	//27 -- IN singal of the system 2 VPOR.(1/on, 0/off)
	BYTE bIVPOR1	:1;	//26 -- IN signal of the system 1 VPOR.(1/on, 0/off)
	BYTE bIActiveOn2:1;	//25 -- IN signal of the CBI 2 running status.(1/on, 0/off)
	BYTE bIActiveOn1:1;	//24 -- IN singal of the CBI 1 running status.(1/on, 0/off) 

    BYTE bFan2		:1;	//39  -- IN  Fan 2계 status (1 - Good)
    BYTE bFan1		:1; //38  -- IN  Fan 1계 status (1 - Good)
    BYTE bDnModem	:1;	//37  -- Down Modem status (1 - Green, 0 - Red flashing) 
    BYTE bUpModem	:1;	//36  -- Up Modem status (1 - Green, 0 - Red flashing)  
	BYTE bExtPowerS :1;	//35  -- South External power (1 - Good) 
    BYTE bATB	    :1;	//34  -- ATB test (1 - ATB test, and after 60sec 0)
	BYTE bGenRun	:1;	//33  -- IN  Generator running (1 - Green flashing, 0 - Gray)
    BYTE bStartUp	:1;	//32  -- Start up 내부사용  

    BYTE bIDayNight	:1; //47  -- IN  Day Night (1 - Night)
    BYTE bExtPowerN :1;	//46  -- IN  North External Power (1 - Good)
    BYTE bLowVoltage:1;	//45  -- IN  방전   (0 - 방전) 
    BYTE bCharging  :1;	//44  -- IN  충전중 (0 - 충전중)
	BYTE bFuse2  	:1;	//43  -- IN  Fuse 2계 status (0 - Good)
    BYTE bFuse1     :1;	//42  -- IN  Fuse 1계 status (0 - Good)
    BYTE bActive2	:1;	//41  -- OUT 2계 동작 요구
    BYTE bActive1	:1;	//40  -- OUT 1계 동작 요구


    BYTE bLC5BELL	:1;	//55  -- LC5  BELL Control
	BYTE bFusePR	:1;	//54  -- IN  Power Rack Fuse status ( 0 - Good)
    BYTE bPKeyContS	:1; //53  -- OUT SOUTH Point key 제어
    BYTE bPKeyContN	:1;	//52  -- OUT North Point key 제어 
    BYTE bPKeyS		:1;	//51  -- IN  South Point key 제어 
	BYTE bPKeyN		:1;	//50  -- IN  North Point key 제어 
    BYTE bTCS		:1;	//49  -- OUT All track 회로 단절 
    BYTE bALR		:1;	//48  -- OUT  신호기 ALR 제어 (1 - Signal Dark 요구) 

    BYTE bPKeyContG :1;	//63  --  
    BYTE bPKeyContF :1; //62  -- 
    BYTE bPKeyContE :1;	//61  --  
    BYTE bPKeyContD :1; //60  -- 
    BYTE bPKeyContC :1;	//59  --  
    BYTE bPKeyContB :1; //58  -- 
    BYTE bPKeyContA :1;	//57  -- 
	BYTE bLC5BELACK	:1;	//56  -- LC5  BELL ACKnowledge (1/Ack, 0/Normal) 
} SystemStatusType;

/* ------------------------------------------------------------------------ */
typedef struct {
		BYTE bReserved5		:1;		// 7 MODEM communication status for block with Down station(1/failure, 0/normal)
		BYTE bReserved4		:1;		// 6 MODEM communication status for block with Up station(1/failure, 0/normal)
		BYTE bReserved3		:1;		// 5 MODEM communication status for block with Middle station(1/failure, 0/normal)
		BYTE bReserved2		:1;		// 4 DTS 2 communication status(1/failure, 0/normal)
		BYTE bReserved1		:1;		// 3 DTS 1 communication status(1/failure, 0/normal)
		BYTE bIFComFail		:1;		// 2 IF COM status(1/failure, 0/normal)
		BYTE bLCCCom2Fail	:1;		// 1 LCC COM 2 status(1/failure, 0/normal)
		BYTE bLCCCom1Fail	:1;		// 0 LCC COM 1 status(1/failure, 0/normal)
} CommStatusType;

/* ------------------------------------------------------------------------ */
typedef struct {
		BYTE bComFail		:1;		// 7 COM1 and COM2 all failure.
		BYTE bActiveOn2		:1;		// 6 out running CBI 2 unit (main/sub).
		BYTE bActiveOn1		:1;		// 5 out running CBI 1 unit (main/sub).
		BYTE bSubRequest	:1;		// 4 자기계가 서브로 전환할려고 상대계에 요구
		BYTE bSys2StartUp	:1;		// 3 cbi 2 boot up.
		BYTE bSys1StartUp	:1;		// 2 cbi 1 boot up. 
		BYTE bSysFail		:1;		// 1 bSysGood 과 같음. 장애 발생시 Set(Cpu 동작중의 장애 발생시 포함) 
		BYTE bSysRun		:1;		// 0 0/fail-flashing, 1/normal
} UnitStatusType;

typedef struct {
		BYTE bSIMMode4		:1;		// 7 r4
		BYTE bSIMMode3		:1;		// 6 r3
		BYTE bSIMModeIO		:1;		// 5 r2
		BYTE bSIMModeSW		:1;		// 4 r1
		BYTE bLCC2Alive		:1;		// 3 LCC2 communication is alive(1/alive, 0/dead).
		BYTE bLCC1Alive		:1;		// 2 LCC1 communication is alive(1/alive, 0/dead).
		BYTE bLCC2Active	:1;		// 1 LCC2 is Active(Online)
		BYTE bLCC1Active	:1;		// 0 LCC1 is Active(Online)
} LCCStatusType;

typedef struct {
	BYTE bLC2BELACK		:1;		// 7 LC2  BELL ACKnowledge (1/Ack, 0/Normal) 
	BYTE bLC2BELL		:1;		// 6 LC2  BELL Control
	BYTE bLC2NKXPR		:1;		// 5 LC2  KEY Indication (1/normal, 0/locking)
	BYTE bLC2KXLR    	:1;		// 4 LC2  KEY Control (1/Can Key Release, 0/Cannot)
	BYTE bLC1BELACK		:1;		// 3 LC1  BELL ACKnowledge (1/Ack, 0/Normal) 
	BYTE bLC1BELL		:1;		// 2 LC1  BELL Control
	BYTE bLC1NKXPR		:1;		// 1 LC1  KEY Indication (1/normal, 0/locking)
	BYTE bLC1KXLR    	:1;		// 0 LC1  KEY Control (1/Can Key Release, 0/Cannot)

	BYTE bLC4BELACK		:1;		// 15 LC4 IN  BELL ACKnowledge (1/Ack, 0/Normal) 
	BYTE bLC4BELL		:1;		// 14 LC4 OUT BELL Control (1/Belling, 0/Normal)
	BYTE bLC4NKXPR		:1;		// 13 LC4 IN  KEY Indication (1/normal, 0/locking) 33133
	BYTE bLC4KXLR    	:1;		// 12 LC4 OUT KEY Control (1/Can Key Release, 0/Cannot) 33132
	BYTE bLC3BELACK		:1;		// 11 LC3 IN  BELL ACKnowledge (1/Ack, 0/Normal) 
	BYTE bLC3BELL		:1;		// 10 LC3 OUT BELL Control (1/Belling, 0/Normal)
	BYTE bLC3NKXPR		:1;		// 9  LC3 IN  KEY Indication (1/normal,Key가 건널목에 있음, 0/locking,key가 transmitter에 있음) 33129
	BYTE bLC3KXLR    	:1;		// 8  LC3 OUT KEY Control (1/Can Key Release, 0/Cannot) 33128

	BYTE bReserved1		:1;		// 23
	BYTE bPKeyG			:1;		// 22
	BYTE bPKeyF			:1;		// 21
	BYTE bPKeyE   	 	:1;		// 20
	BYTE bPKeyD			:1;		// 19
	BYTE bPKeyC			:1;		// 18
	BYTE bPKeyB			:1;		// 17
	BYTE bPKeyA  	  	:1;		// 16

	BYTE bFuse4   		:1;		// 31
	BYTE bFuse3			:1;		// 30
	BYTE bFan4			:1;		// 29
	BYTE bFan3			:1;		// 28
	BYTE bSound4  	 	:1;		// 27 Left TCB CA 경보
	BYTE bSound3		:1;		// 26 Power 경고
	BYTE bSound2		:1;		// 25 S,P 부저(경고)
	BYTE bSound1		:1;		// 24 접근, 폐색 경보
} SystemStatusTypeExt;


/* ------------------------------------------------------------------------ */
typedef enum SysButtonIDTypes { 
// Id = 0xE0  224
	BtnPas = 0x0E0,			// PAS   1 - Lock
	BtnDay,					// Day/Night  1 - Day
	BtnSls,					// SLS  1 - On (Signal power off)
	BtnS,					// event, Signal failure  
	BtnP,					// event, Point failure
	BtnFuse,				// event, Fuse failure
	BtnComm,				// event, Modem Comm failure
	BtnSysRun,				// event, System Run
// ID = 0xe8  232
	BtnSys1,				// System 1 CPU display
	BtnSys2,				// System 2 CPU display
	BtnPower,				// Main power failure 
	BtnGen,					// Generator failure & running
	BtnC,					// Left side OSS control >> Left, Right 구분없음.
	BtnD,					// Right side OSS control >> Reserved
	BtnAtb,					// ATB   1 - ATB on 
	BtnUps2,				// UPS2 failure
// ID = 0xf0  240
	BtnGround = 0x0F0,		// Ground failure
	BtnUps1,				// UPS1 failure
	BtnBattery,				// Battery Charger
	BtnB24,					// B24V failure
	BtnAck,					// Alarm ack & off -- SDB function
	BtnPDB,					// Left CA  -- PDB function
	BtnGnd,					// Ground detect...
	BtnClose,		   		// Station Close or Open
// ID = 248
	BtnReserved,            // Reserved
	BtnLoop1,				// Loop1 Deenergy << Parameter로 요놈만 사용.
	BtnLoop2,				// Loop2 Deenergy << 1만사용. 요놈은 사용안함.
	BtnAPKup,               // up dir. APK
	BtnAPKdn,               // down dir. APK
	BtnLCB,                 // Level Crossing Control Button
	BtnLCA,                 // Level Crossing Call
	BtnNOUSED,              // NOuse 버튼
// ID = 256
	BtnB1ClearReq = 0x00,	// (0x00)
	BtnB2ClearReq,			// 
	BtnLoop3,				//  Loop3 Deenergy << 1만사용. 요놈은 사용안함.
	BtnLoop4,				//  Loop4 Deenergy << 1만사용. 요놈은 사용안함.
	BtnLoop5,               //  Loop5 Deenergy << 1만사용. 요놈은 사용안함.
	BtnAPKA, 				// 
	BtnAPKB, 				// 
	BtnAPKC, 				// 
// ID = 264
	BtnAPKD, 				// 
	BtnAPKE, 				// 
	BtnAPKF, 				// 
	BtnAPKG, 				// 
	BtnB2AXLRST, 			// 
	BtnB4AXLRST, 			// 
	BtnLC2B,				// LC2 Control Button
	BtnLC2A,					// LC2 Call Button
//	ID = 272
	BtnB3ClearReq,
	BtnB4ClearReq,
	BtnLocalMode,			// Local Mode로 강제변경.
	BtnCTCMode,				// CTC Request시 CTC Mode 승인
	BtnB11ClearReq,
	BtnB12ClearReq,
	BtnLC3B,
	BtnLC3A,
//	ID = 280
	BtnLC4B,
	BtnLC4A,
	BtnTKAXLRST,
	BtnB1AXLRSTACC,
	BtnB1AXLRSTDEC,
	BtnB3AXLRSTACC,
	BtnB3AXLRSTDEC,
	BtnB13ClearReq,
//	ID = 288 (0x20)
	BtnB14ClearReq,
	BtnB1ClearAcc,
	BtnB2ClearAcc,
	BtnB3ClearAcc,
	BtnB4ClearAcc,
	BtnB11ClearAcc,
	BtnB12ClearAcc,
	BtnB13ClearAcc,
//	ID = 296 (0x28)
	BtnB14ClearAcc,
	BtnB1ClearDec,
	BtnB2ClearDec,
	BtnB3ClearDec,
	BtnB4ClearDec,
	BtnB11ClearDec,
	BtnB12ClearDec,
	BtnB13ClearDec,
//	ID = 304 (0x30)
	BtnB14ClearDec,
	BtnLC5B,
	BtnLC5A,
};

/* ----------------------------------------------------------------------- */
typedef struct {
			BYTE nMsgHeader;	// 부계:0x05, 주계0x0A
			BYTE nDataVer;
			BYTE nSec;
			BYTE nMin;
			BYTE nHour;
			BYTE nDate;
			BYTE nMonth;
			BYTE nYear;
// Offset 0x08 - 0x0F (8Bytes)
			SystemStatusType nSysVar;
// Offset 0x10
			BYTE Cycle;
// Offset 0x11 (1 Byte)
		CommStatusType	CommState;		// Make status for EID SW(Not control system).
// Offset 0x12 (1 Byte)
		UnitStatusType	UnitState;		// Unit status
// Offset 0x13 - 0x22 (16Bytes)
		BYTE IOC1Status[8];				// rack 1 - 8	D0:IOC, D1-D7:Card 1-7
		BYTE IOC2Status[8];				// 
// Offset 0x23
			LCCStatusType	LCCStatus;
// Offset 0x24 - 0x2B (8Bytes)
			unsigned short Counter[4];	// 계수기 -> 0 - Callon 신호 현시, 1 - 비상진로 해정
// Offset 0x2C - 0x2F (4Bytes)		
			SystemStatusTypeExt 	nSysVarExt;
} DataHeaderType;



/* ----------------------------------------------------------------------- */
// MOTOROLA
typedef struct {
			BYTE INHIBIT	:1;	// 사용금지 (기타 사용금지)
			BYTE EMREL		:1;	// Route Emergency stop
			BYTE ROUTEDIR	:1;	// 5 
			BYTE OVERLAP	:1; // 4
			BYTE TKREQUEST	:1; // 3
			BYTE ROUTE		:1;	// 2 1:NORMAL, 0/LOCK(GRREN)
// in
			BYTE TRACK		:1; // 1
			BYTE ALARM		:1;	// 0 접근

			BYTE BLKILLOCC	:1;	// 15	폐색 불법 점유를 위한 비트 추가
			BYTE BLKSWEEP	:1; // 14	폐색 스위핑을 위한 비트 추가 
			BYTE BLKTRACE	:1;	// 13	13역 폐색 퇴행 추적을 위한 필드 추가
			BYTE MULTIDEST	:1; // 12
			BYTE RESETBUF	:1; // 11	13역 AXL Track Field Reset Buffer (30초간 유지)
			BYTE RESETOUT	:1;	// 10	13역 AXL Track Reset OUTPUT
			BYTE RESETIN	:1; // 9	13역 AXL Track Field Reset INPUT
			BYTE DISTURB	:1;	// 8	13역 AXL Distrub INPUT

			unsigned short RouteNo;				// 20140527 sjhong - 진로 설정될 경우 해당 진로번호 저장을 위해 생성
			unsigned short RightToLeftRouteNo;	// 20130904 sjhong - Overlap 설정된 Track에 진로번호 저장을 위해 생성	// 20140519 sjhong 2bytes 진로번호 수용가능하도록 수정
			unsigned short LeftToRightRouteNo;	// 20130904 sjhong - Overlap 설정된 Track에 진로번호 저장을 위해 생성	// 20140519 sjhong 2bytes 진로번호 수용가능하도록 수정
} ScrInfoTrack;

/* ----------------------------------------------------------------------- */
typedef struct {
// BYTE 0
// out
			BYTE WRO_P		:1;     //7 WRO - N
			BYTE WRO_M		:1;     //6 WRO - R
			BYTE FREE		:1;		//5 1=전환가능 상태, 0 쇄정
// in
			BYTE WLR		:1;     //4 WLRI
			BYTE WR_M		:1;     //3 WRI - R
			BYTE WR_P		:1;     //2 WRI - N
			BYTE KR_M		:1;     //1 
			BYTE KR_P		:1;     //0 
// BYTE 1
// internal
            BYTE TRACKLOCK  :1;     //15
            BYTE SWFAIL     :1;     //14
            BYTE WLRO       :1;     //13 
			BYTE ON_TIMER	:1;     //12
			BYTE REQUEST_M	:1;     //11
			BYTE REQUEST_P	:1;     //10	
			BYTE ROUTELOCK	:1;     //9		//
			BYTE BACKROUTE  :1;     //8		//
// BYTE 2
			BYTE PASSROUTE	:1;		//23	// 20140716 sjhong 통과 진로 시 조사 쇄정 Flashing 안하도록 하는 정보
			BYTE BLOCK		:1;		//22	// 20130812 선로전환기 block을 위해 사용. 
			BYTE INROUTE	:1;		//21	// 
			BYTE ROUTE_M	:1;		//20	// 
			BYTE ROUTE_P	:1;		//19	// 
			BYTE WKEYKR		:1;		//18	// 
			BYTE KEYOPER	:1;		//17	//
			BYTE NOUSED		:1;		//16	// 
//		};
//	};
} ScrInfoSwitch;

/* ----------------------------------------------------------------------- */
// MOTOROLA
typedef struct {
// OFFSET 0
// in
			BYTE INU		:1;     //7
			BYTE SIN3		:1;     //6
			BYTE SIN2		:1;     //5
			BYTE SIN1		:1;     //4
// out
			BYTE U			:1;     //3
			BYTE SOUT3		:1;     //2 S3
			BYTE SOUT2		:1;     //1 S2
			BYTE SOUT1		:1;     //0 S1
//=====================================
// OFFSET 1
// 필요한 제어신호, 실제 출력은 G,Y,YY로 나감.
// internal
			BYTE FREE		:1;		//15	; 신호 현시 상태, 0-쇄정
			BYTE CS			:1;		//14	; Callon, Shunt진로 설정 중 set...
            BYTE RIGHT      :1;     //13
			BYTE LEFT   	:1;     //12
			BYTE SE4		:1;     //11
			BYTE SE3		:1;     //10
			BYTE SE2	    :1;     //9
			BYTE SE1		:1;     //8
//=====================================
// OFFSET 2
            BYTE INRIGHT    :1;		//23 0/normal, 1/fail
            BYTE INLEFT     :1;     //22
			BYTE RCVRONLY	:1;     //21 - 중계 신호 등에서 Signal Recovery 제어만 가능하도록 하는 비트   
			BYTE ENABLED	:1;		//20
			BYTE REQUEST	:1;     //19
			BYTE ON_TIMER	:1;     //18
			BYTE SignalFail	:1;     //17 - Signal Fail (Failure = 1)
            BYTE TM1		:1;     //16
//=====================================
// OFFSET 3
            BYTE OSS        :1;     //31
			BYTE LM_CLOR	:1;     //30 - IN, CLOR, HHLOR Signal lamp detection (Callon, S-Yellow ECPR)
            BYTE ADJCOND    :1;     //29 - Additional signal aspect condition from adjacent station
            BYTE BLKCOMM	:1;     //28 - Block Communication Status (Always 1)
            BYTE LM_SLOR	:1;     //27
            BYTE LoopSigFail:1;     //26
            BYTE LM_ALR     :1;     //25
            BYTE LM_LOR     :1;     //24

	BYTE DESTTRACK;
	BYTE DESTID;

	BYTE GetSignalControlState( int nSignalType );
	BYTE GetSignalLampState();

} ScrInfoSignal;

/* ----------------------------------------------------------------------- */
typedef struct {
// OFFSET 0
            BYTE COMPLETEIN :1;		// 폐색수속 완료 입력
            BYTE CBBOP		:1;     // CBB 운영자 제어시 1 (TGB CBB - Y, TCB CBB - Y)
			BYTE CBBIN	    :1;     // IN (TGB, TCB는 변화 없음)
            BYTE CBB	    :1;     // 폐색 해정 요청 (TGB CBB - G, TCB CBB - G, TGB, TCB는 적색)
									// TCB의 경우 CBBOPR + ARRIVETR이 모두 1이면 1
			BYTE LCROP		:1;     // LCR 운영자 제어시 1 (TGB LCR - Y, TCB LCR - Y)
			BYTE LCRIN		:1;     // IN (TGB - LCR이 1일 경우 Wf, TCB - Wf)
			BYTE LCR		:1;     // 라인비우기 요청 수행 (TGB LCR - G, TCB LCR - G, TGB - W, TCB - W)
									// TGB의 경우 LCROPR이 1이면 1, TCB의 경우 LCROPR + CONFIRM이 모두 1이면 1
			BYTE CA		    :1;     // 폐색 요청 (TGB CA - G, TCB CA - GF) 
			//=====================================
// OFFSET 1
            BYTE COMPLETE   :1;     // 폐색수속 완료 - CBBIN에 의해 수행 CA,LCR,CBB는 무표시 (TGB, TCB 무표시)
            BYTE SWEEPINGACK:1;     // Train Sweeping 완료 상태
            BYTE ARRIVEIN   :1;     // TGB - R (항상Check)
            BYTE ARRIVE     :1;     // 열차 도착 (TCB - RF, 이때 TGB CA가 1, COMPLETE이 0이면 TGB는 COMPLETE 수행 - 항상Check)
            BYTE DEPARTIN   :1;     // TCB - R
            BYTE DEPART     :1;     // 선행S 궤도 점유 - 정보Stick  (TGB - R)
            BYTE ASTART	    :1;     // 선행S Clear 상태 (TGB, TCB는 변화 없음 - 항상Check)
            BYTE CONFIRM    :1;     // 선행S와 외부S사이 해정 상태 (TGB, TCB는 변화 없음 - 항상Check)
			                        // AS는 TGB의 LCR, LCRIN이 모두 1인 경우 취급가능, CBB취급은 ASTART가 0에서만 가능(1이면 CBB 회색)
			//=====================================
// OFFSET 2
			BYTE SWEEPING	:1;		// 23  Train Sweeping
			BYTE CAACK		:1;		// 22  CA ACKnowledge -- (1 - ACKnowledge)
			BYTE ASTARTSTICK:1;		// 21  TGB의 경우  ASTART signal 이 설정되면 STICK -- (1 -- STICK)
			BYTE AXLDST		:1;		// 20  
			BYTE AXLOCC		:1;		// 19
			BYTE AXLRST		:1;		// 18
            BYTE BLKRSTREQIN:1;     // 17	BGR/BCR RESET REQUEST IN
            BYTE BLKRSTREQ  :1;     // 16	BGR/BCR RESET REQUEST OUT
			//=====================================
// OFFSET 3
			BYTE RBCPR		:1;		// 31  IN
			BYTE NBCPR		:1;		// 30  IN
			BYTE RBGPR		:1;		// 29  IN
			BYTE NBGPR		:1;		// 28  IN
			BYTE RBCR		:1;		// 27  OUT
			BYTE NBCR	    :1;		// 26  OUT
            BYTE RBGR 	    :1;     // 25  OUT
            BYTE NBGR       :1;     // 24  OUT
			//=====================================
// OFFSET 4
			BYTE AXLOCCDOUT	:1;		// 39
			BYTE BLKRSTDECIN:1;		// 38	BGR/BCR RESET DECLINE IN
			BYTE BLKRSTDEC	:1;		// 37	BGR/BCR RESET DECLINE OUT
			BYTE BLKRSTACCIN:1;		// 36	BGR/BCR RESET ACCEPT IN
			BYTE BLKRSTACC	:1;		// 35	BGR/BCR RESET ACCEPT OUT
			BYTE AXLDEC		:1;		// 34
			BYTE AXLACC		:1;		// 33
			BYTE AXLREQ		:1;		// 32
			//=====================================
// OFFSET 5
			BYTE SAXLACTIN	:1;		// 47
			BYTE SAXLACT	:1;		// 46
			BYTE SAXLDEC	:1;		// 45
			BYTE SAXLACC	:1;		// 44
			BYTE SAXLREQ	:1;		// 43
			BYTE SAXLDST	:1;		// 42
			BYTE SAXLOCC	:1;		// 41
			BYTE SAXLRST	:1;		// 40
} ScrInfoBlock;

/* ----------------------------------------------------------------------- */
typedef struct {
	BYTE BitLamp	:1;
} ScrInfoLamp;

/* ----------------------------------------------------------------------- */
typedef struct {
	union {
		BYTE data;
		struct {
			BYTE SHIFT_LEFT	:1;
			BYTE SHIFT_RIGHT	:1;
		} bit;
	};
} ScrInfoTrkNode;

/* ----------------------------------------------------------------------- */
#endif	// VERSION_B

/* ----------------------------------------------------------------------- */

#define SIZE_INFO_TRACK		sizeof(ScrInfoTrack)
#define SIZE_INFO_SIGNAL	sizeof(ScrInfoSignal)
#define SIZE_INFO_SWITCH	sizeof(ScrInfoSwitch)
#define SIZE_INFO_LAMP		sizeof(ScrInfoLamp)



// Aspect Types
#define ASPECT_R         1
#define ASPECT_Y         2
#define ASPECT_G         4
#define ASPECT_CO        8
#define ASPECT_SY     0x10
#define ASPECT_SH     0x20
#define ASPECT_LL     0x40
#define ASPECT_LR     0x80

#define AHEAD_R	         1
#define AHEAD_Y	         2
#define AHEAD_G	         4
#define AHEAD_LL	  0x10
#define AHEAD_LR	  0x20
#define AHEAD_LM	  0x40

#define MODE_LOCAL	0
#define MODE_CTC	1
#define TAG_CTC		0xEA

#endif // _SCRINFO_H
