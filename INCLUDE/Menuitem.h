// MenuItem.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////
#ifndef   _MENUITEM_H
#define   _MENUITEM_H

typedef struct {
	char Name[64];
//$$$    char szName[16];
	BYTE  nType;
	BYTE  nTTB;
	BYTE  nDisable;		// 0x00:Enable, 0x80:Diable, 0x01:Route On, 0x03:TTB On.
	BYTE  nToTrackID;	// ���� �˵� ID
	BYTE  nSignalID;	// ���� ��ȣ�� ID
	BYTE  nButtonID;	// ���� ��ư ID
	short nRouteID;		//
} MenuItemRoute;

/////////////////////////////////////////////////////////////////////////////

typedef struct {
	char Name[64];
//@@@    char szName[16];
	BYTE  nDisable;
	short nID;
} MenuItemStruct;

/////////////////////////////////////////////////////////////////////////////

typedef struct {
	short nMax;
	short nMaxID;
	union {
		MenuItemRoute  *pRMenu;
		MenuItemStruct *pIMenu;
	};
} MenuItems;

/////////////////////////////////////////////////////////////////////////////

typedef enum MenuItemType {
	MenuSignalType,
	MenuSwitchType,
	MenuTrackType
};

// Track Menu Check Type.
#define CHECK_TRACK_PWRDOWN		0x01
#define CHECK_TRACK_MOTERCAR	0x02
#define CHECK_TRACK_CTRLLOCK	0x03
#define CHECK_TRACK_FREEROUTE	0x04
#define CHECK_TRACK_CTRLDISABLE	0x07
#define CHECK_TRACK_ALLDISABLE	0x0f

/////////////////////////////////////////////////////////////////////////////

#endif // _MENUITEM_H
