#ifndef _SCROBJ_H
#define _SCROBJ_H

#include "Logic.h"
#include "LSMOpt.h"
#include "Bmpdraw.h"


#define OBJ_NONE		' '
#define OBJ_TRACK		'T'
#define OBJ_TRACK2		'Z'
#define OBJ_SIGNAL		'S'
#define OBJ_SWITCH		'W'
#define OBJ_LAMP		'D'
#define OBJ_BUTTON		'B'
#define OBJ_STATION		'Z'
#define OBJ_TNI			'N'
#define OBJ_ETC 		'E'
#define OBJ_SYSPUSH		'P'
#define OBJ_SYSBUTTON	'Y'
#define OBJ_BLOCK       'K'

#define _13Stations_PJT	1
#define _Tongi_PJT		2
#define _Laksam_PJT		3

class CLSMCtrl;
class COleControl; 
class CLoader; 
int KeyCheck();

#include "ScrInfo.h"

class TScrobj : public CRgn {
protected:
	int	isvalid;
	CRect   m_rectArea;
	BOOL    m_bDrawValid;

public:	
	int MyDrawText( LPCTSTR lpszString, int nCount, LPRECT lpRect, UINT nFormat, UINT nColor );
	short	m_nID;
	int     m_nObjType;
	UINT	mColor;		// select bmp color
	CPoint	mPos;
	char	mName[100];
	char    m_szDrawName[100];
	CString m_strUserID;
	short	m_cConfig;
	BYTE	m_cObjectType;
	BOOL	m_IsPress;
	BOOL    m_nDir;
	int     m_iWith; //2007.04.04 Bridge 길이 표시 하기 위해 추가.
public:
	void    *m_pData;

public:
	COleControl *m_pDoc;

public:
	TScrobj(COleControl *pDoc, const char *name = 0, int x = 0, int y = 0 );
	TScrobj();
	virtual ~TScrobj();
	virtual TScrobj* PtInArea( CPoint &point );
	virtual void Draw( CDC *pDC );
	virtual void Update() {};
	virtual void SetName(char *iname);
	virtual void InvalObj() { m_bDrawValid = FALSE;	}
	virtual void SetObjectType() = 0;
	virtual void ConvertPos(){};
	virtual TScrobj *FindObject( char cType, short nID ){return NULL;};
	virtual TScrobj *FindObject( char cType, char * szName ){return NULL;};
	BOOL operator ==(const TScrobj& other) const
	{
		return &other == this;
	}
	virtual void GenMessage( WORD msg, int id = 0 ) {
	}
friend CLSMCtrl;
friend CLoader;
friend int KeyCheck();
};

typedef struct {
	char *str;
	int code;
} ModeBitDefine;

#endif