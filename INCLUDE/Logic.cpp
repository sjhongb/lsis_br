//===============================================================
//====                       Logic.cpp                       ====
//===============================================================
// 파  일  명 : Logic.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 다른 클래스에서 사용될 각종 연산 함수를 정의한다.
//              
//
//===============================================================


#include "stdafx.h"
//#include "WERC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "Logic.h"

CLogicParser::CLogicParser( char *str, char *pDel ) {
	m_pStr = strdup(str);
	m_pPtr = m_pStr;
	m_nLen = strlen( str );
	m_nLoc = 0;
	m_pLogic = NULL;
	m_nLogCount = 0;
	m_pDelemter = pDel;
}

CLogicParser::CLogicParser() {
	m_pStr = NULL;
	m_pPtr = NULL;
	m_nLen = 0;
	m_nLoc = 0;
	m_pLogic = NULL;
	m_nLogCount = 0;
	m_pDelemter = "";
}

void CLogicParser::Create( char *str, char *pDel )
{
	if (m_pStr) free(m_pStr);
	if (m_pLogic) delete [] m_pLogic;
	m_pStr = strdup(str);
	m_pPtr = m_pStr;
	m_nLen = strlen( str );
	m_nLoc = 0;
	m_pLogic = NULL;
	m_nLogCount = 0;
	m_pDelemter = pDel;
}

CLogicParser::~CLogicParser() {
	if (m_pStr) free(m_pStr);
	if (m_pLogic) delete [] m_pLogic;
}

//char DELIMETER[] = " \t()!+*=;{}";

BOOL CLogicParser::GetToken( CString &str ) {
	str = "";
	if (*m_pPtr == 0) return FALSE;
	char c;
	while (c = *m_pPtr, c && (c == ' ' || c == '\t')) {
		m_pPtr++;
	}
	if (*m_pPtr == 0) return FALSE;
	m_bDelimeter = FALSE;
	while (c = *m_pPtr, c) {
		if (strchr( m_pDelemter, c) != NULL) {
			if (str == "") {
				str += c;
				m_pPtr++;
				m_bDelimeter = TRUE;
			}
			return TRUE;
		}
		str += c;
		m_pPtr++;
	}
	if (str == "") return FALSE;
	return TRUE;
}

int	_PARSERERR;
BOOL CLogicParser::CreateLogic()
{
	CString token;
	_PARSERERR = 0;
	if ( !GetToken( token ) ) return FALSE;
	_PARSERERR = 1;
	if ( token != "{" ) return FALSE;
	_PARSERERR = 2;
	BOOL done = FALSE;
	while (!done) {
		GetToken( token );
		if (token == "}") break;
		if ( token.GetAt(0) != '.') return FALSE;
		CLogic *plogic = new CLogic;
		plogic->m_Name = token;
		if ( !GetToken( token ) ) return FALSE;
		if (token != "=") return FALSE;
		CString str;
		while ( GetToken( token ) ) {
			if (token == "}") {
				done = TRUE;
				break;
			}
			else if (token == ";") break;
			str += token;
		}
		plogic->Create( str );
		if (m_pLogic) {
			CLogic *tmp = new CLogic[m_nLogCount+1];
			for(int i=0; i<m_nLogCount; i++) tmp[i] = m_pLogic[i];
			delete [] m_pLogic;
			m_pLogic = tmp;
		}
		else m_pLogic = new CLogic[1];
		m_pLogic[m_nLogCount++] = *plogic;
		delete plogic;
	}
	return TRUE;
}

class CBool {
public:
	CBool *m_pData, *m_pNext;
	WORD m_nID;
	BYTE m_bInverse;
	CBool() {
		m_pData = NULL;
		m_pNext = NULL;
		m_nID = LOGEQ;
		m_bInverse = 0;
	}
	virtual ~CBool();
	void Add( CBool *pAdd );
	virtual BYTE GetValue();
	virtual BOOL GetState(UINT n) {
		return FALSE;
	}
};

CBool::~CBool() {
	CBool *pBool = m_pData, *pLast;
	while (pBool) {
		pLast = pBool;
		pBool = pBool->m_pNext;
		delete pLast;
	}
//	if (m_pData) delete m_pData;
	m_pData = NULL;
}

void CBool::Add( CBool *pAdd ) {
	if (m_pData) {
		CBool *p = m_pData;
		while (p->m_pNext) p = p->m_pNext;
		p->m_pNext = pAdd;
	}
	else m_pData = pAdd;
	pAdd->m_pNext = NULL;
}

BYTE CBool::GetValue() {
	if (m_bInverse) return !GetState( m_nID );
	return GetState( m_nID );
}

class CBoolAnd : public CBool {
public:
	CBoolAnd() {
		m_nID = LOGAND;
	}
	BYTE GetValue();
};

BYTE CBoolAnd::GetValue() {
	BOOL res = 1;
	CBool *pBool = m_pData;
	while ( pBool ) {
		res &= pBool->GetValue();
		if (!res) return m_bInverse;
		pBool = pBool->m_pNext;
	}
	return !m_bInverse;
}

class CBoolOr : public CBool {
public:
	CBoolOr() {
		m_nID = LOGOR;
	}
	BYTE GetValue();
};

BYTE CBoolOr::GetValue() {
	BOOL res = 0;
	CBool *pBool = m_pData;
	while ( pBool ) {
		res |= pBool->GetValue();
		if (res) return !m_bInverse;
		pBool = pBool->m_pNext;
	}
	return m_bInverse;
}

CLogic::CLogic() {
	m_pData = NULL;
	m_bState = FALSE;
//	m_nSize = 0;
	m_Name = "";
}

CLogic::~CLogic() {
//	CBool *pBool = m_pData, *pLast;
//	while  ( pBool ) {
//		pLast = pBool;
//		pBool = pBool->m_pData;
//		delete pLast;
//	}
	if (m_pData) delete m_pData;
}

void CLogic::Update() {
	if ( m_pData ) {
		m_bState = m_pData->GetValue();
	}
}

//	C = (A+!(!(A+(1+A)*!(A*D)))+B)*(!A+!B);	
//                                  

CBool *CLogic::Reduce( CBool *pStart, CBool *pEnd )
{
	CBool *p, *pLast;
// reduce ()
	WORD t;
	BOOL done;
// ()
	done = FALSE;	p = pStart; pLast = NULL;
	while ( p && !done ) {
		t = p->m_nID;
		if ( t == LOGPUSH ) {
			CBool *p1 = p->m_pNext, *p2 = p->m_pNext, *p3 = NULL;
			if (pLast) pLast->m_pNext = p1;
			else pStart = p->m_pNext;
			delete p;				// delete '('
			int cnt = 0;
			while ( p2 ) {
				t = p2->m_nID;
				if ( t == LOGPOP ) {
					cnt--;
					if (cnt == 0) {
						CBool *pr = Reduce( p1, p3 );
						if (pLast) pLast->m_pNext = pr;
						pr->m_pNext = p3->m_pNext;
						if (p2 == pEnd) done = TRUE;
						delete p2;	// delete ')'
						p = pr;
						break;
					}
				}
				else if ( t == LOGPUSH ) cnt++;
				p3 = p2;
				p2 = p2->m_pNext;
			}
		}
		pLast = p;
		p = p->m_pNext;
		if (p == pEnd) done = TRUE;
	}
// !
	done = FALSE;	p = pStart; pLast = NULL;
	while ( p && !done ) {
		t = p->m_nID;
		if ( t == LOGNOT ) {
			CBool *p1 = p->m_pNext;
			if (pLast) pLast->m_pNext = p1;
			else pStart = p->m_pNext;
			delete p;				// delete '('
			p1->m_bInverse = 1;
			p = p1;
			if (p == pEnd) done = TRUE;
		}
		pLast = p;
		p = p->m_pNext;
		if (p == pEnd) done = TRUE;
	}
// *
// +
	WORD op[] = { LOGAND, LOGOR };
	for (int i=0; i<2; i++) {
		WORD COP = op[i];
		done = FALSE; p = pStart; pLast = NULL;
		CBool *pLastPrev;
		pLastPrev = NULL;
		while ( p && !done ) {
			t = p->m_nID;
			if ( t == COP && !p->m_pData ) {
				if (pLast && p->m_pNext) {
					CBool *p1 = pLast, *p2 = p->m_pNext;
					delete p;				// delete '*'
					if (p2 == pEnd) done = TRUE;
					if (p1->m_nID != COP && p1->m_nID != COP) {
						CBool *pr;
						if (COP == LOGAND) pr = new CBoolAnd;
						else pr = new CBoolOr;
						if (pLastPrev) pLastPrev->m_pNext = pr;
						else pStart = pr;
						pr->m_pNext = p2->m_pNext;
						pr->Add( p1 );
						pr->Add( p2 );
						p = pr;
					}
					else if (p1->m_nID == COP) {	 // p1 <- p2
						p1->m_pNext = p2->m_pNext;
						p1->Add( p2 );
						p = p1;
					}
					else if (p2->m_nID != COP) {  // p1 -> p2
						if (pLastPrev) pLastPrev->m_pNext = p2;
						else pStart = p2;
						p2->Add( p1 );
						p = p2;
					}
				}
				if (p == pEnd) done = TRUE;
			}
			pLastPrev = pLast;
			pLast = p;
			p = p->m_pNext;
			if (p == pEnd) done = TRUE;
		}
	}
	return pStart;
}

CLogic &CLogic::operator = (CLogic &obj) {
	if (!m_pData) {
//		m_nSize = obj.m_nSize;
		m_pData = obj.m_pData;
//		memcpy(m_pData, obj.m_pData, m_nSize * sizeof(WORD));
		obj.m_pData = NULL;
	}
	m_bState = obj.m_bState;
	m_Name = obj.m_Name;
	return *this;
}

void CLogic::Create( char *bf ) {
	CLogicParser parser(bf);
	CString token;
	while (parser.GetToken( token )) {
		if (parser.m_bDelimeter) {
			char c = token.GetAt(0);
			switch (c) {
			case '(' : AddData( LOGPUSH ); break;
			case ')' : AddData( LOGPOP ); break;
			case '!' : AddData( LOGNOT ); break;
			case '+' : AddData( LOGOR ); break;
			case '*' : AddData( LOGAND ); break;
			}
		}
		else {
			WORD id = idsearch( (char*)(LPCTSTR)token );
			AddData( id );
		}
	}
	CBool *pTail = m_pData;
	while (pTail->m_pNext) pTail = pTail->m_pNext;
	m_pData = Reduce( m_pData, pTail );
}

void CLogic::Create( CString &s )
{
	Create((char *)(LPCTSTR)s);
}

void CLogic::AddData( WORD id )
{
	CBool *tmp = new CBool;
	tmp->m_nID = id;
	if (m_pData) {
		CBool *p = m_pData;
		while (p->m_pNext) p = p->m_pNext;
		p->m_pNext = tmp;
	}
	else m_pData = tmp;
}

void CLogic::Create( WORD iID )
{
	m_pData = new CBool;
	m_pData->m_nID = iID;
}
