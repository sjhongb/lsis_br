/////////////////////////////////////////////////////////////////////////////
// MessageInfo.h : header file

#ifndef   _MESSAGEINFO_H
#define   _MESSAGEINFO_H

#include  "JTime.h"
#include  "ScrInfo.h"
#include  "EipEmu.h"
#include  "MessageNo.h"

/////////////////////////////////////////////////////////////////////////////

typedef struct tagPrintSwitchState {
	int nState;		// 0/정위, 1/반위
	int nNote;		// 0/정상, 1/정위불일치, 2/반위불일치
} PrintSwitchState;

typedef struct tagPrintSignalState {
	int nState;		// 0/Fail, 1/정지, 2/경계, 3/주의, 4/감속, 5/진행, 6/유도, 7소등,8/무유도,9/완료
	int nNote;		// 0/정상, 1/주심단심or폐색장애, 2/주-부심단심, 3/TTB-ON, 4/TTB-OFF,5/보류-접근쇄정,6/진로선별,7/진로선별오류
	int nLMR;		// D0:G,   D1:Y,   D2:YY,  D3:U
	int nMode;		// 0/입환, 1/출발,장내, 2/폐색
	int nState1;	// 0/Fail, 1/정지, 2/경계, 3/주의, 4/감속, 5/진행, 6/유도, 7소등,8/완료
} PrintSignalState;

typedef struct tagPrintTrackState {
	int nState;		// 0/복구, 1/점유, 2/장애
	int nNote;		// 0/정상, 2/단선작업, 3/모터카점유, 4/사용금지
} PrintTrackState;

typedef struct tagPrintTimeEvent {
	BOOL bMsgOn;
	CJTime  T;
//2001.07.06 jin, add ms
	BYTE    hsec;
//...//
	CString strName;
	union {
		PrintSwitchState	Switch;
		PrintSignalState	Signal;
		PrintTrackState		Track;
	};
} PrintTimeEvent;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
	CString strStationName;
	CString strProjectName;
	int iNumberOfEPK;
	int iNumberOfLC;
	CString strBlokState[17];
	CString strLCName[6];
} VerifyObjectType;
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

typedef struct {
	CJTime *pTime;
//2001.07.06 jin, add ms
	BYTE   hsec;
//...//
	EvnLocType EvnLoc;
} EventKeyType;

/////////////////////////////////////////////////////////////////////////////

class CMessageInfo : public CScrInfo
{
protected:
	int  GetTrackMessage(CObList &objList, void *pOld, void *pCur,		// D1:track, D0:other
							EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent = NULL);
	int  GetSignalMessage(CObList &objList, void *pOld, void *pCur,		// D1:signal, D0:other
							EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent = NULL);
	int  GetSwitchMessage(CObList &objList, void *pOld, void *pCur,		// D1:switch, D0:other
							EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent = NULL);
	int  GetLampMessage(CObList &objList, void *pOld, void *pCur,		// D1:lamp, D0:other
							EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent = NULL);

// Constructor
public:
	CMessageInfo();
	BOOL CheckBlockSignal ( int nSignalType );
	int  CheckSignalType ( int nSignalType );
// Operations
public:
	int  GetMessage(CObList &objList, void *pOld, void *pCur, 
							EventKeyType &EventKey, BYTE &errorMode, PrintTimeEvent *pPrnEvent = NULL);

	BOOL GetEvent(PrintTimeEvent &PrnEvent, void *pOld, void *pCur);
	BOOL isSignalOn( int nType, BYTE *pData );

	BYTE*GetBufferPtr( BYTE *pBuf ) 
	{
		return &pBuf[ m_nMemOffset ];
	}

// Implementation
public:
	~CMessageInfo() {};

#ifdef    _DEBUG_DUMP
	void Dump(CDumpContext& dc);
#endif // _DEBUG_DUMP
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

extern int  Info_nMaxTrack;
extern int  Info_nMaxSignal;
extern int  Info_nMaxSwitch;
extern int  Info_nMaxButton;
#ifndef   VERSION_B
extern int  Info_nMaxLamp;
#endif // VERSION_B
extern int  Info_nSizeOfRecord;

/////////////////////////////////////////////////////////////////////////////

extern CEipEmu *_pEipEmu;

/////////////////////////////////////////////////////////////////////////////

#endif // _MESSAGEINFO_H
