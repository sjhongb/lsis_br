// EipEmu.cpp : implementation file
//

#ifdef _WINDOWS
#include "stdafx.h"
#else
#include "string.h"
#endif

#ifndef _WINDOWS
#include "LogTask.h"
unsigned char g_nBlockType = 0; // Tokenless Block Interface Type from CPU Rotary Switch
#endif

#include "CtcEmu.h"
#include "EipEmu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _WINDOWS
#define ASSERT
#define TRACE
#endif

#ifdef _WINDOWS
#define _CHOTEST
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define SXADD_SG_INFO	0x10	// signal info table (0x10 byte)
#define SXADD_TRACK		0x26	// track object (1 byte)	27->26
#define SXADD_POINT		0x27	// point object (2 byte)	28->27
#define SXADD_SIGNAL	0x2b	// signal object (5 byte)
#define SXADD_SIGNALID	0x40	// signal id (1 byte)
#define SXADD_SWITCHID	0x41	// switch id (1 byte)
#define SXADD_TRACKID	0x42	// track id (1 byte)
#define SXADD_ROUTEDB	0x80	// route table data( 0x80 byte).

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int OnSGTimerTick = 0;
//

// #ifdef _CHOTEST
// #define ATB_TEST_TIME	20
// #else
// #define ATB_TEST_TIME	30
// #endif
//#define ApproachBellTime 10



//m_pDestTrack 배열 0는 Power 경보 Bell 관련..
//m_pDestTrack 배열 70은 접근 경보 Bell 관련 (접근궤도).. --> 신호기별로 배정한다(115까지).. (폐색이 여러개일 경우 구분을 위해)
//m_pDestTrack 배열 121 이상은 Timer관련 정보를 저장한다....
//m_pDestTrack 배열 127은 AtbTimer 관련..

#define	CallonCount	0			// Callon 계수기 index number
#define EmerCount   1			// Emergency stop index number
#define B2AXLCounter	2
#define B4AXLCounter	3

#ifdef _LSM
extern CString m_strStationName;
#endif

CEipEmu::CEipEmu() 
{
	m_nTNIQueInPtr = 0;
	m_nTNIQueOutPtr = 0;

	m_pVMEM = NULL;
    m_pMsgBuffer = NULL;

	memset(m_pRouteQue, 0, sizeof(m_pRouteQue));
	memset(m_pSwitchRouteTrack, 0, sizeof(m_pSwitchRouteTrack));
	memset(m_pSwitchOverTrack1, 0, sizeof(m_pSwitchOverTrack1));
	memset(m_pSwitchOverTrack2, 0, sizeof(m_pSwitchOverTrack2));
	memset(m_pSignalTimer, 0, sizeof(m_pSignalTimer));            //신호기별 접근쇄정시간... 
	memset(m_pSwitchTimer, 0, sizeof(m_pSwitchTimer));
	memset(m_pTrackExtraTimer, 0, sizeof(m_pTrackExtraTimer));	// 궤도 엑셀세션 리셋 출력 제어 (5초간) 및 쳬류 종착궤도 체류시간 타이머 (설정시간)
	memset(m_pTrackTimer, 0, sizeof(m_pTrackTimer));
	memset(m_pTrackPrev, 0, sizeof(m_pTrackPrev));
    memset(m_pLastTrack, 0, sizeof(m_pLastTrack));
	memset(m_pOwnerRouteQue, 0, sizeof(m_pOwnerRouteQue));
	memset(m_pSignalFail, 0, sizeof(m_pSignalFail));
	memset(m_pSwitchFail, 0, sizeof(m_pSwitchFail));
	memset(m_pTrackNext, 0, sizeof(m_pTrackNext));
	memset(m_pOverTrack, 0, sizeof(m_pOverTrack));
	memset(m_pSyncSwitch, 0, sizeof(m_pSyncSwitch));
	memset(m_pTrackStick, 0, sizeof(m_pTrackStick));
	memset(m_pLCBellTimer, 0, sizeof(m_pLCBellTimer));
	m_nPowerFail = 0;
	m_nATBTimer = 0;
	m_bSLSStick = 0;

	m_nRamSize = 
		sizeof(m_pRouteQue) +
		sizeof(m_pSwitchRouteTrack) +
		sizeof(m_pSwitchOverTrack1) +
		sizeof(m_pSwitchOverTrack2) +
		sizeof(m_pSignalTimer) +
		sizeof(m_pSwitchTimer) +
		sizeof(m_pTrackExtraTimer) +
		sizeof(m_pTrackTimer) +
		sizeof(m_pTrackPrev) +
		sizeof(m_pLastTrack) +
		sizeof(m_pOwnerRouteQue) + 
		sizeof(m_pSignalFail) +
	   	sizeof(m_pSwitchFail) +
		sizeof(m_pTrackNext) +
   		sizeof(m_pOverTrack) +
	   	sizeof(m_pSyncSwitch) +
		sizeof(m_pTrackStick) +
		sizeof(m_pLCBellTimer) +
		sizeof(m_nPowerFail) +
		sizeof(m_nATBTimer) +
		sizeof(m_bSLSStick);

	m_nB12ID = m_nB14ID = m_nB2ID = m_nB4ID = 0;
	m_nB11ID = m_nB13ID = m_nB1ID = m_nB3ID = 0;

	memset(m_SignalFailDetect, 0, N_SG_COUNT);

	m_bStartInterlock = FALSE;
	m_nLC1NKXPR = m_nLC2NKXPR = m_nLC3NKXPR = m_nLC4NKXPR = m_nLC5NKXPR = 0;

	// Axle Counter Reset Timer Variable Initialize
	m_nB2AXLRSTTimer = 0;
	m_nB4AXLRSTTimer = 0;

	memset(&m_ConfigList, 0x00, sizeof(m_ConfigList));
}


CEipEmu::~CEipEmu() 
{
}


ScrInfoSignal *CEipEmu::GP_InfoSignal ( int i ) 
{
	if (i<1)
	{
		BYTE pbSignal[6];
		pbSignal[0] = 0;
		pbSignal[1] = 0;
		pbSignal[2] = 0;
		pbSignal[3] = 0;
		pbSignal[4] = 0;
		pbSignal[5] = 0;

		return (ScrInfoSignal *)&pbSignal[0];
	}

    i--;
    return (ScrInfoSignal *)&m_pVMEM[m_TableBase.InfoSignal.nStart + i * SIZE_INFO_SIGNAL];
}

ScrInfoSwitch *CEipEmu::GP_InfoSwitch ( int i ) 
{
	if (i<1)
	{
		BYTE pbSwitch[3];
		pbSwitch[0] = 0;
		pbSwitch[1] = 0;
		pbSwitch[2] = 0;

		return (ScrInfoSwitch *)&pbSwitch[0];
	}

    i--;
    i &= 0x7F;
    return (ScrInfoSwitch *)&m_pVMEM[m_TableBase.InfoSwitch.nStart + i * SIZE_INFO_SWITCH];
}

ScrInfoTrack *CEipEmu::GP_InfoTrack ( int i ) 
{
	if (i<1)
	{
		BYTE pbTrack[SIZE_INFO_TRACK];
		memset(&pbTrack, 0, sizeof(pbTrack));

		return (ScrInfoTrack *)&pbTrack[0];
	}

    i--;
    return (ScrInfoTrack *)&m_pVMEM[m_TableBase.InfoTrack.nStart + i * SIZE_INFO_TRACK];
}




void CEipEmu::opClearAllRoute()
{
	int nSize = m_pMsgBuffer - m_pVMEM;

	memset( m_pVMEM + sizeof (DataHeaderType), 0, nSize - sizeof (DataHeaderType) );

	// 여기서 m_nRamSize만큼 전부 초기화한다.
	memset( m_pRouteQue, 0, m_nRamSize );

	BYTE i;
	WORD nSignal = m_TableBase.InfoSignal.nCount;

	for (i=1; i<=nSignal; i++) 
	{
		BYTE nSignalType = m_pSignalInfoTable[i].nSignalType & SIGNAL_MASK;
		if (nSignalType == BLOCKTYPE_AUTO_OUT || nSignalType == BLOCKTYPE_AUTO_IN )
		{
			ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, i);

			pBlkInfo->COMPLETE = 1;
			pBlkInfo->AXLOCC = 1;
			pBlkInfo->AXLDST = 1;
			pBlkInfo->AXLRST = 0;
		}
		else if(nSignalType != SIGNALTYPE_BUFSTART)
		{
			ScrInfoSignal *pInfo = (ScrInfoSignal *)int_get(SignalGet, i);
	/*
			pInfo->FREE = 1;
			pInfo->ENABLED = 1;
			pInfo->ON_TIMER	 = 0;
	*/
			pInfo->FREE = 1;

			pInfo->ON_TIMER	 = 1;
			pInfo->ADJCOND = 1;
			pInfo->BLKCOMM = 1;
			pInfo->LM_SLOR = 1;
			pInfo->ENABLED = 0;

			pInfo->TM1 = 0;

			if(m_ConfigList.BLK_DEV_MODE)
			{
				pInfo->LM_ALR = 0;

				if(i == 7 || i == 8)
				{
					pInfo->LM_LOR = 1;
				}
				else
				{
					pInfo->LM_LOR = 0;
				}
				pInfo->SIN1 = 0;
				pInfo->SIN2 = 0;
				pInfo->SIN3 = 0;
				pInfo->INU	= 0;
			}

			m_pSignalTimer[i] = 0;
		}
	}

	short nTrack = m_TableBase.InfoTrack.nCount;
	for (i=1; i<=nTrack; i++) 
	{
		ScrInfoTrack *pInfo = (ScrInfoTrack *)int_get(TrackGet, i);
		pInfo->TRACK = 1;
		pInfo->ROUTE = 1;
		pInfo->DISTURB = 1;
		pInfo->TKREQUEST = 0;
		pInfo->MULTIDEST = 0;
		pInfo->RouteNo = 0;
		pInfo->RightToLeftRouteNo = 0;
		pInfo->LeftToRightRouteNo = 0;
	}

	short nSwitch = m_TableBase.InfoSwitch.nCount;
	for (i=1; i<=nSwitch; i++) 
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)int_get(SwitchGet, i);
		pInfo->TRACKLOCK = 0;
		pInfo->BLOCK = 1;

		m_pSwitchOverTrack1[i].nTrackID = 0;
		m_pSwitchOverTrack1[i].nDelay = 0;
		m_pSwitchOverTrack1[i].wRouteID = 0;

		m_pSwitchOverTrack2[i].nTrackID = 0;
		m_pSwitchOverTrack2[i].nDelay = 0;
		m_pSwitchOverTrack2[i].wRouteID = 0;

		m_pSwitchRouteTrack[i * 2] = 0;
		m_pSwitchRouteTrack[i * 2 + 1] = 0;
	}
}


void CEipEmu::opSignalStop( short nSignal )
{
	m_pRouteQue[ nSignal ] |= ROUTE_SIGNALSTOP;
}

void CEipEmu::opSignalOSS( BYTE nSignalID, BYTE nFlag )
{
/*   
	// nFlag가 이상하게 전송되어 사  생략 .. 아래 로직으로 대체..

	if (nFlag == 1)
	{
		int_get(SignalLoad, nSignalID);

        m_SignalInfo.OSS = 1;	// OSS switch ON outer signal forced stop.
		int_put(SignalPut);
	}
	else
	{
		int_get(SignalLoad, nSignalID);

        m_SignalInfo.OSS = 0;	// OSS switch OFF.
		int_put(SignalPut);
	}
*/
	int_get(SignalLoad, nSignalID);
	if (m_SignalInfo.OSS == 0)
	{
		m_SignalInfo.OSS = 1;
	}
	else
	{
		m_SignalInfo.OSS = 0;

		// 20151112 sjhong - Outer Home 신호기에서만 OSS가 걸리므로, OSS OFF 시 조건 없이 무조건 복구시킨다.
		//if (m_SignalInfo.CS)
		m_SignalInfo.TM1 = 1;  //2005.10.20 Outer home callon 취급 후 OSS취급하여 정지를 만든 후 다시 OSS복귀시에도 Main signal 처럼 신호가 설정 되도록 한다.
	}
	int_put(SignalPut);

}

void CEipEmu::opTrackLOS( BYTE nTrackID, BYTE nFlag )
{
/*
	// nFlag가 이상하게 전송되어 사  생략 .. 아래 로직으로 대체..

	if (nFlag == 1)
	{
		int_get(TrackLoad, nTrackID);

        m_TrackInfo.INHIBIT = 1;   // Toggle switch OFF and track cannot detect.
		int_put(TrackPut);
	}
	else
	{
		int_get(TrackLoad, nTrackID);

        m_TrackInfo.INHIBIT = 0;	// Toggle switch ON and track can detect.
		int_put(TrackPut);
	}
*/
	int_get(TrackLoad, nTrackID);
	if (m_TrackInfo.INHIBIT == 0)
		m_TrackInfo.INHIBIT = 1;
	else
		m_TrackInfo.INHIBIT = 0;

	int_put(TrackPut);
}

void CEipEmu::opSwitchUSE( BYTE nSwitchID, BYTE nFlag )
{
	int_get(SwitchLoad, nSwitchID);
	if (m_SwitchInfo.NOUSED == 0)
		m_SwitchInfo.NOUSED = 1;
	else
		m_SwitchInfo.NOUSED = 0;

	int_put(SwitchPut);
}

void CEipEmu::opSignalDest( BYTE nSignalID, BYTE nDestID )
{
/*
	BYTE n, nDestTrackID;

	BYTE nSignalType = m_pSignalInfoTable[nSignalID].nSignalType & SIGNAL_MASK;
	if (nSignalType == BLOCKTYPE_INTER_OUT) 
	{	
		int_get(SignalLoad, nSignalID);

        m_SignalInfo.REQUEST = 1;
		int_put(SignalPut);
        return;
    }
*/
	WORD wRouteNo = m_pSignalInfoTable[ nSignalID ].wRouteNo;
	wRouteNo += (nDestID & 0x3F);

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
    BYTE nOverloadRouteNo = pInfo[ RT_OFFSET_ROUTESTATUS ] & 0x0F;

	for(int i = 0 ; i < nOverloadRouteNo ; i++)
	{
		if(GetFindRoute((wRouteNo & ROUTE_MASK) + i))
		{
			// 실제 오버랩의 전철기 방향이 같은 진로를 찾은 경우 진로번호를 변경한다.
			wRouteNo += i;
			
			// 최종 발견 진로로 진로 정보 재 설정.....
			nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];	// 진로Table의 자기진로 정보 위치 
			pInfo = &m_pInfoMem[nInfoPtr];						// 연동도표 정보 테이블
			break;
		}
	}

    BYTE nSigID = RouteToSignal(wRouteNo);

   	if (!(m_pRouteQue[nSigID] & ROUTE_MASK)) {
		m_pRouteQue[nSigID] = wRouteNo;
	}

	// 2006.5.19  Callon counter는 home signal의 callon 이나 shunt 진로인 경우만 증가한다. -- Alam의 요구로 수정함...
// 	if ( ((m_pSignalInfoTable[ nSigID ].nSignalType & SIGNAL_MASK)==SIGNALTYPE_IN) &&
// 		 (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON || pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL) )

	// 20150331 sjhong - Callon Counter는 Home 또는 Outer Home Signal의 CallOn 진로에 대해서만 증가시킨다. -- Mr. Ha 요구로 수정함.
	if((((m_pSignalInfoTable[ nSigID ].nSignalType & SIGNAL_MASK) == SIGNALTYPE_IN) 
	|| ((m_pSignalInfoTable[ nSigID ].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME))
	&& (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON))
	{
		DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;

#ifdef _WINDOWS
		WORD wCounter = pHead->Counter[CallonCount];
		wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
		pHead->Counter[CallonCount] = wCounter;
#endif
		pHead->Counter[CallonCount]++;
		if (pHead->Counter[CallonCount] == 10000)
			pHead->Counter[CallonCount] = 0;
#ifdef _WINDOWS
		wCounter = pHead->Counter[CallonCount];
		wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
		pHead->Counter[CallonCount] = wCounter;
#endif
	}
}


void CEipEmu::opSignalCan( BYTE nSignal )
{
	m_pRouteQue[ nSignal ] |= ROUTE_CANCEL;
	BYTE nSignalType = m_pSignalInfoTable[nSignal].nSignalType & SIGNAL_MASK;

/* 20140227 sjhong - 불필요한 코드 삭제
	if (nSignalType == BLOCKTYPE_INTER_OUT) // 연동폐색 출발
	{	
		int_get(SignalLoad, nSignal);
		m_SignalInfo.REQUEST = 0;
		int_put(SignalPut);
	}
*/
}


BYTE CEipEmu::RouteToSignal( short nRoute )
{
	DWORD nInfoPtr = m_pRouteAddTable[ nRoute ].nRouteOffset;	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
	BYTE nID = pInfo[ RT_OFFSET_SIGNAL ];

	return nID;
}

void CEipEmu::opBlockCA ( BYTE nBlkID )
{
	int_get(SignalLoad, nBlkID);
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;

	if((pBlkInfo->CA == FALSE) && (pBlkInfo->COMPLETE == TRUE)
	&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
	{
		pBlkInfo->CA = TRUE;

		int_put(SignalPut);
	}
}

void CEipEmu::opBlockLCR ( BYTE nBlkID )
{
	int_get(SignalLoad, nBlkID);
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;

	// CA & CAACK & AXLOCC 조건 추가
	// 20130823 sjhong - LCR 처리 과정에 CA와 CAACK를 확인하지 않도록 수정함.
	// 20130823 sjhong - CA요청 절차 중에는 LCR 요청을 낼수 없도록 일부 차단함.
	// 20150731 sjhong - CA 절차와 무관하게 LCR 요청이 가능하도록 변경함. (BR요청사항)

	if(m_ConfigList.BLK_TRAIN_SWEEP)
	{
	// 20161114 sjhong - AXLOCC 상태에 무관하게 LCR 취급이 가능하도록 변경함. (비상 LCR 취급)
		if(/*(pBlkInfo->CA == TRUE) && (pBlkInfo->CAACK == TRUE) && */
		   /*!(pBlkInfo->CA ^ pBlkInfo->CAACK) &&*/ (pBlkInfo->LCROP == FALSE) && (pBlkInfo->LCR == FALSE) 
		/*&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE)))*/)
		{	
			pBlkInfo->LCROP = TRUE;

			int_put(SignalPut);
		}
	}
	else
	{
		if(/*(pBlkInfo->CA == TRUE) && (pBlkInfo->CAACK == TRUE) && 
		   /*!(pBlkInfo->CA ^ pBlkInfo->CAACK) &&*/ (pBlkInfo->LCROP == FALSE) && (pBlkInfo->LCR == FALSE) 
		&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
		{
			pBlkInfo->LCROP = TRUE;
	
			int_put(SignalPut);
		}
	}
}

void CEipEmu::opBlockCBB ( BYTE nBlkID )
{
	int_get(SignalLoad, nBlkID);
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;

	if((pBlkInfo->CBBOP == FALSE) && (pBlkInfo->CBB == FALSE) 
	&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
	{
		pBlkInfo->CBBOP = TRUE;
		int_put(SignalPut);
	}
}

void CEipEmu::opHandSwitch( BYTE nSwID )
{
	ScrInfoSwitch *pSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nSwID);
	
	if (pSwInfo->KEYOPER == 1) 
	{
		pSwInfo->KEYOPER = 0;
	}
	else
	{
		pSwInfo->KEYOPER = 1;
	}
}

void CEipEmu::opSwitch( BYTE nSwID )
{
	ScrInfoSwitch *pSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nSwID);
	
	if (!pSwInfo->FREE) 
		return;

	if(!pSwInfo->BLOCK)
		return;

	BYTE swpos = nSwID & 0x80;
	nSwID &= 0x7f;

	BYTE p, m;
	if (swpos == 0) 
	{
		p = 1;
		m = 0;
	}
	else 
	{
		p = 0;
		m = 1;
	}

	BYTE n1stTrkID = m_pSwitchOnTrackTable[nSwID].pData[0];
	BYTE n2ndTrkID = m_pSwitchOnTrackTable[nSwID].pData[1];
	
	ScrInfoTrack *p1stTrkInfo = (ScrInfoTrack *)int_get(TrackGet, n1stTrkID);
	ScrInfoTrack *p2ndTrkInfo = (ScrInfoTrack *)int_get(TrackGet, n2ndTrkID);
	
	if ((pSwInfo->WR_P != p || pSwInfo->WR_M != m) && ((n1stTrkID == 0) || p1stTrkInfo->TRACK) && ((n2ndTrkID == 0) || p2ndTrkInfo->TRACK))
	{
		pSwInfo->REQUEST_P = p;
		pSwInfo->REQUEST_M = m;
	}

	m_pSwitchTimer[nSwID] = 0;

	return;
}

void CEipEmu::opPointBlock( BYTE nSwID )
{
	ScrInfoSwitch *pSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nSwID);
	
	BYTE swpos = nSwID & 0x80;
	if (swpos == 0) 
	{
		if(pSwInfo->SWFAIL) 
			return;
	
		pSwInfo->BLOCK = !pSwInfo->BLOCK;
	}
	else 
	{
		pSwInfo->BLOCK = !pSwInfo->BLOCK;
	}

	return;
}

void CEipEmu::SignalStop() 
{
	m_SignalInfo.SOUT1 = 0;  //출력  // G
	m_SignalInfo.SOUT2 = 0;          // Y
	m_SignalInfo.SOUT3 = 0;          // Callon
	m_SignalInfo.U = 0;              // Shunt or OuterHome Yellow
	/*
	m_SignalInfo.SE1 = 0;    //출력검지
	m_SignalInfo.SE2 = 0;
	m_SignalInfo.SE3 = 0;
	m_SignalInfo.SE4 = 0;
	*/

	m_wCheckResult &= ~ROUTECHECK_OCCTMOUT;

//#ifndef _WINDOWS				// S/W 시뮬레이터 사용 시 LEFT, RIGHT 값은 유지가 불가능 하므로 건너 뛰자.(Indicator)
	m_SignalInfo.LEFT = 0;
	m_SignalInfo.RIGHT = 0;
//#endif
}


void CEipEmu::SignalStopU() 
{
	//2006.6.15  Free shunt signal의 경우 마주보는 signal의 aspect를 현시하기 위해...
	if (m_SignalInfo.OSS && m_SignalInfo.SE4 && m_SignalInfo.LEFT)
		return;

    SignalStop();

//#ifndef _WINDOWS				// S/W 시뮬레이터 사용 시 SE값은 유지가 불가능 하므로 건너 뛰자.(Callon)
	m_SignalInfo.SE1 = 0;    //출력검지
	m_SignalInfo.SE2 = 0;
	m_SignalInfo.SE3 = 0;
	m_SignalInfo.SE4 = 0;
//#endif
}


BYTE CEipEmu::GetSignalAspected( int nSignalID ) 
{
	BYTE nNextAhead = 0;
	BYTE nNextSigType = m_pSignalInfoTable[ nSignalID ].nSignalType;

	ScrInfoSignal *pNextSignal = (ScrInfoSignal *)int_get(SignalGet, nSignalID);

	
	if ( pNextSignal->SIN1 && pNextSignal->LM_LOR) 
	{	// G
		nNextAhead |= AHEAD_G;

		if ( (pNextSignal->LEFT && pNextSignal->INRIGHT) || (pNextSignal->RIGHT && pNextSignal->INLEFT) )
		{
			nNextAhead |= AHEAD_LM;
		}
		else if ( pNextSignal->LEFT || pNextSignal->INLEFT )   
		{
			nNextAhead |= AHEAD_LL;
			//nNextAhead |= AHEAD_LR;
		}
		else if ( pNextSignal->RIGHT || pNextSignal->INRIGHT ) 
		{
			//nNextAhead |= AHEAD_LL;
			nNextAhead |= AHEAD_LR;
		}
	}
	else if ( !pNextSignal->SIN1 && pNextSignal->SIN2 && pNextSignal->LM_LOR) 
	{ // Y
		nNextAhead |= AHEAD_Y;

		if ( pNextSignal->LEFT )
		{
			nNextAhead |= AHEAD_LL;
			//nNextAhead |= AHEAD_LR;
		}
		
		if ( pNextSignal->RIGHT )
		{
			//nNextAhead |= AHEAD_LL;
			nNextAhead |= AHEAD_LR;
		}
	}
	else 
	{ 
		// 2005.11.25 pNextSignal->SignalFail 조건 추가....
		if (pNextSignal->LM_LOR || !pNextSignal->SignalFail)  
		    nNextAhead |= AHEAD_R;
	}
	return nNextAhead;
}


void CEipEmu::SignalAspect(BYTE *pInfo)
{

	BYTE bShuntSigOn = 0;
    BYTE nID = LoopCountH + 1;
	BYTE nSignalType = m_pSignalInfoTable[nID].nSignalType & SIGNAL_MASK;
		
	BYTE nRouteStatus = m_pRouteInfo[ RT_OFFSET_ROUTESTATUS ];
	BYTE nPrevSigID = pInfo[ RT_OFFSET_PREVSIGNAL ];
	BYTE nNextType = m_pSignalInfoTable[nPrevSigID].nSignalType & SIGNAL_MASK;

	if ( nNextType==BLOCKTYPE_AUTO_IN || nNextType==BLOCKTYPE_AUTO_OUT || nNextType==BLOCKTYPE_INTER_IN || nNextType==BLOCKTYPE_INTER_OUT )
		nPrevSigID = 0;

	if(nSignalType == SIGNALTYPE_ASTART)
	{
		if(!m_SignalInfo.ADJCOND)
		{
			m_SignalInfo.TM1 = 0;
			return;
		}
	}

    BYTE nAspect, nAhead;
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)m_pRouteInfo;

	BYTE nMyAspect = 0;
	BYTE *pMyAspectInfo = m_pRouteInfo + pRouteBasPtr->nOfsAspect;
	BYTE nMyAspectCount = *pMyAspectInfo++;
	if ( nMyAspectCount == 0 ) 
		return;

	BYTE nMyNextAhead = 0;

	if(nPrevSigID) // 전방신호가 존재...
	{
		// 전방신호기의 현재 현시 상태...
		nMyNextAhead = GetSignalAspected( nPrevSigID );
		if(!nMyNextAhead)
		{
			// 신호 현시 중에 전방 신호가 주부심 단심 검지 될 경우 ..., Shunt, callon은 전방신호를 감시하지 않는다...
			if ( m_SignalInfo.SOUT1 || m_SignalInfo.SOUT2 || (m_SignalInfo.SOUT3 && nNextType==SIGNALTYPE_OHOME ) )
			{
				m_SignalInfo.TM1 = 0;
				return;
			}
		}
	}
	else
	{
		nMyNextAhead = 0xCF;
	}

	// 20140625 sjhong - 마주보는 신호기들을 조사한다...
	pRouteBasPtr = (RouteBasPtr*)pInfo;
	BYTE *pLamp = pInfo + pRouteBasPtr->nOfsLamp;
	BYTE nLampCount = *pLamp++;
	BYTE lcount = 0;
	while(lcount < (nLampCount - 1)) 
	{
		BYTE nLampID	= *pLamp++;
		
		if(nLampID)
		{
			// 마주보는 신호기가 OFF 상태이면,
			BYTE nLampSts = GetSignalAspected( nLampID );
			if(!nLampSts)
			{
				// 신호기 Clear 상태가 아닌 경우에만 RED로 떨어뜨린다.
				if(!m_SignalInfo.ENABLED)
				{
					m_SignalInfo.TM1 = 0;

					// 20140626 sjhong - 리플렉트 신호기들도 연계되서 동작하도록 한다.
					BYTE nDependSigID = 0;
					ScrInfoSignal *pDependSigInfo = NULL;
					for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
					{
						nDependSigID = (pInfo[cSigOffset] & SIGNALID_MASK);
						if(nDependSigID)
						{
							pDependSigInfo = (ScrInfoSignal*)int_get(SignalGet, nDependSigID);
							if(pDependSigInfo != NULL)
							{
								pDependSigInfo->TM1 = 0;
							}
						}
					}

					// 여기서 RETURN을 해주면 마주보는 신호기의 상태가 정상이 될 때 자동 현시가 된다.
					return;
				}
			}
		}
	
		lcount++;
	}

	if (pInfo[ RT_OFFSET_LEVELCROSS ])
	{
		SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;
		SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
		
		if (pInfo[ RT_OFFSET_LEVELCROSS ] & FLAG_LC1)
		{
			if (nSysVarExt.bLC1NKXPR || nSysVarExt.bLC1KXLR)  //1 - Level crossing open....
			{
				m_SignalInfo.TM1 = 0;
			}
		}
		
		if (pInfo[ RT_OFFSET_LEVELCROSS ] & FLAG_LC2)
		{
			if (nSysVarExt.bLC2NKXPR || nSysVarExt.bLC2KXLR)  //2 - Level crossing open....
			{
				m_SignalInfo.TM1 = 0;
			}
		}
		
		if (pInfo[ RT_OFFSET_LEVELCROSS ] & FLAG_LC3)
		{
			if (nSysVarExt.bLC3NKXPR || nSysVarExt.bLC3KXLR)  //3 - Level crossing open....
			{
				m_SignalInfo.TM1 = 0;
			}
		}
		
		if (pInfo[ RT_OFFSET_LEVELCROSS ] & FLAG_LC4)
		{
			if (nSysVarExt.bLC4NKXPR || nSysVarExt.bLC4KXLR)  //4 - Level crossing open....
			{
				m_SignalInfo.TM1 = 0;
			}
		}

		if (pInfo[ RT_OFFSET_LEVELCROSS ] & FLAG_LC5)
		{
			if (nSysVar.bLC5NKXPR || nSysVar.bLC5KXLR)  //5 - Level crossing open....
			{
				m_SignalInfo.TM1 = 0;
			}
		}

		if(m_SignalInfo.TM1 == 0)
		{
			// 20150206 sjhong - 리플렉트 신호기들도 연계되서 동작하도록 한다.
			BYTE nDependSigID = 0;
			ScrInfoSignal *pDependSigInfo = NULL;
			for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
			{
				nDependSigID = (pInfo[cSigOffset] & SIGNALID_MASK);
				if(nDependSigID)
				{
					pDependSigInfo = (ScrInfoSignal*)int_get(SignalGet, nDependSigID);
					if(pDependSigInfo != NULL)
					{
						pDependSigInfo->TM1 = 0;
					}
				}
			}
			
			// 여기서 RETURN을 해주면 L/C CLOSE & KT ON 상태가 정상이 될 때 자동 현시가 된다.
			return;	
		}
	}

	m_SignalInfo.SE4 = 0;
	m_SignalInfo.SE3 = 0;
	m_SignalInfo.SE2 = 0;
	m_SignalInfo.SE1 = 0;

	// 2007.7.18  Conflict signal의 LOR 검지를 위해 삽입....
// 	BYTE nConflictCount=12;
// 	BYTE nConflictAspect = 1;
// 	while (BYTE nConflictID = m_pSignalInfoTable[nID].pApproach[nConflictCount++])
// 	{
// 		ScrInfoSignal *pConflictSignal = (ScrInfoSignal *)int_get(SignalGet, nConflictID);
// 		if (!pConflictSignal->LM_LOR)
// 		{
// 			nConflictAspect = 0;
// 		}
// 	}

	BOOL bIsReplacedAspect = FALSE;
	BOOL bDependSetOnly = FALSE;
	
	// 기본적으로 자신의 Aspect 정보를 조회한다.
	BYTE *pAspectInfo = pMyAspectInfo;
	BYTE nAspectCount = nMyAspectCount;
	BYTE nNextAhead	  = nMyNextAhead;

	// 20140916 sjhong - 중계 신호기로 동작할 때에 주 신호기의 전방 신호기 조건에 맞추어 동작하도록 수정
	if((m_pRouteQue[nID] & ROUTE_DEPENDSET) && (nSignalType != SIGNALTYPE_OHOME))
	{
		WORD wMainRouteNo = (m_pOwnerRouteQue[nID] & ROUTE_MASK);
		if(wMainRouteNo != 0)	// 중계 신호기일 때만 값이 있음
		{
			DWORD nMainInfoPtr				= *(DWORD*)&m_pRouteAddTable[wMainRouteNo];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pMainInfo					= &m_pInfoMem[nMainInfoPtr];
			RouteBasPtr *pMainRouteBasPtr	= (RouteBasPtr *)pMainInfo;
			
			BYTE nMainSigID			= pMainInfo[RT_OFFSET_SIGNAL];
			BYTE nMainPrevSigID		= pMainInfo[RT_OFFSET_PREVSIGNAL];

			BOOL bIsDependSignal	= FALSE;

			for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
			{
				if(nID == (pMainInfo[cSigOffset] & SIGNALID_MASK))
				{
					bIsDependSignal = TRUE;

					if(pMainInfo[cSigOffset] & SIG_DEPEND_SET_ONLY)
					{
						bDependSetOnly = TRUE;
						break;
					}
				}
			}

			if(/*(nMainPrevSigID == nPrevSigID) && */bIsDependSignal && !bDependSetOnly)
			{
				// 종착 신호기가 동일한 중계 진로일 경우 주 신호기의 Aspect 정보를 조회한다.
				pAspectInfo		= pMainInfo + pMainRouteBasPtr->nOfsAspect;
				nAspectCount	= *pAspectInfo++;
				
				// 전방신호기의 현재 현시 상태...
				if(nMainPrevSigID != 0)
				{
					nNextAhead = GetSignalAspected(nMainPrevSigID);
				}

				bIsReplacedAspect = TRUE;
			}
		}
	}

	int n = nAspectCount;
	int nAspectOn = 0;
	while ( n-- ) 
	{
		nAhead = pAspectInfo[nAspectCount];			// 전방 신호기 현시 조건...
		if ( nAhead == 0 ) 
		{
			nAhead = 0xFF;
		}
		else if ( (nAhead & AHEAD_LM) && (nNextAhead & AHEAD_LR) )
		{
			nNextAhead += 0x20;
		}

		nAspect = *pAspectInfo;	// 자기 신호기의 현시 조건...

		// 20140916 sjhong - 주 신호기가 정지일 경우 중계 신호기는 현시하면 안된다.
		if ((nNextAhead & nAhead) || (nAspect & (ASPECT_SH+ASPECT_CO)) ) 
		{
			for(int i = 0; i < nMyAspectCount ; i++)
			{
				nMyAspect = *(pMyAspectInfo + i);

				if(bIsReplacedAspect)
				{
					// 주신호기와 중계신호기의 Aspect 값이 완전히 동일하지 않고 일치하는 신호만 있어도 처리될 수 있게 함.
					// LKM역 49:65(M) 진로와 같은 CASE				
					if(nMyAspect & nAspect)
					{
						nAspect &= nMyAspect;
						break;	
					}
				}
				else
				{
					if(nMyAspect == nAspect)
					{
						break;
					}
				}
			}

			if(i == nMyAspectCount)
			{
				nAspect = nMyAspect;
			}

			if ( (nAspect & ASPECT_G) && ((nSignalType != SIGNALTYPE_OHOME) || (!(nNextAhead & AHEAD_LL) && !(nNextAhead & AHEAD_LR))) ) 
			{
				// 2006.7.26 Home신호기의 경우 출발 신호기의 상태를 check하여 출발 신호기가 Green일 경우에만 Green이 되어야 한다...
				if ( (m_pSignalInfoTable[nID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_IN )
				{
					if ((nNextAhead & ASPECT_G) /*&& nConflictAspect*/)  // 2007.7.18 nConflictAspect 조건 추가
					{
						m_SignalInfo.SE2 = 1;
						m_SignalInfo.SE1 = 1;
						m_SignalInfo.SE3 = 0;
						nAspectOn = 1;
						
						if(nAspect & ASPECT_LL)
						{
							m_SignalInfo.RIGHT = 0;
							m_SignalInfo.LEFT = 1;
						} 
						else if(nAspect & ASPECT_LR)
						{
							m_SignalInfo.LEFT = 0;
							m_SignalInfo.RIGHT = 1;
						}
						else
						{
							m_SignalInfo.LEFT = 0;
							m_SignalInfo.RIGHT = 0;
						}
					}
				}
				else
				{
					/*if (nConflictAspect)*/ // 2007.7.18 nConflictAspect 조건 추가
					{
						m_SignalInfo.SE2 = 1;
						m_SignalInfo.SE1 = 1;
						m_SignalInfo.SE3 = 0;
						nAspectOn = 1;

						if(nAspect & ASPECT_LL)
						{
							m_SignalInfo.RIGHT = 0;
							m_SignalInfo.LEFT = 1;
						} 
						else if(nAspect & ASPECT_LR)
						{
							m_SignalInfo.LEFT = 0;
							m_SignalInfo.RIGHT = 1;
						}
						else
						{
							m_SignalInfo.LEFT = 0;
							m_SignalInfo.RIGHT = 0;
						}
					}
				}
			}
			else if ( (nAspect & ASPECT_Y) || (nAspect & ASPECT_SY) )
			{
				/*if (nConflictAspect)*/ // 2007.7.18 nConflictAspect 조건 추가
				{
					m_SignalInfo.SE2 = 1;
					m_SignalInfo.SE1 = 0;

					if ( (nAspect & ASPECT_Y) && (nAspect & ASPECT_SY) )
					{
						m_SignalInfo.SE3 = 1;
					}

					if(nAspect & ASPECT_LL)
					{
						m_SignalInfo.RIGHT = 0;
						m_SignalInfo.LEFT = 1;
					} 
					else if(nAspect & ASPECT_LR)
					{
						m_SignalInfo.LEFT = 0;
						m_SignalInfo.RIGHT = 1;
					}
					else
					{
						m_SignalInfo.LEFT = 0;
						m_SignalInfo.RIGHT = 0;
					}

					nAspectOn = 1; 
				}
			}
			else if ((nAspect & ASPECT_R) && m_SignalInfo.LM_LOR)  // Red aspect가 현시 되는 상황에서 Callon 및 Shunt제어가 가능하다.
			{
				if (nAspect & ASPECT_CO)
				{
					m_SignalInfo.SE3 = 1;
					nAspectOn = 1; 

					if(nAspect & ASPECT_LL)
					{
						m_SignalInfo.RIGHT = 0;
						m_SignalInfo.LEFT = 1;
					} 
					else if(nAspect & ASPECT_LR)
					{
						m_SignalInfo.LEFT = 0;
						m_SignalInfo.RIGHT = 1;
					}
					else
					{
						m_SignalInfo.LEFT = 0;
						m_SignalInfo.RIGHT = 0;
					}
				}
			}

			// Shunt 신호는 Callon 신호와는 달리 자기 신호기의 R 현시 조건을 Proving하지 않는다...
			if (nAspect & ASPECT_SH) 
			{
				m_SignalInfo.SE4 = 1;
				nAspectOn = 1; 
				
				if(nAspect & ASPECT_LL)
				{
					m_SignalInfo.RIGHT = 0;
					m_SignalInfo.LEFT = 1;
				} 
				else if(nAspect & ASPECT_LR)
				{
					m_SignalInfo.LEFT = 0;
					m_SignalInfo.RIGHT = 1;
				}
				else
				{
					m_SignalInfo.LEFT = 0;
					m_SignalInfo.RIGHT = 0;
				}

				if (m_SignalInfo.OSS)
				{
					if (nAhead != 0xff && !(nNextAhead & nAhead) )  // 2006.10.18  (nNextAhead != nAhead)에서 수정함..  AKA 61 shunt signal 때문...
					{
						m_SignalInfo.SE4 = 0;
						nAspectOn = 0; 
					}
				}
			}
		}
		pAspectInfo++;
	}

// 20150526 sjhong - 어떤 용도인지 모르겠으나 전방 신호가 떨어질 경우에 함께 신호가 떨어지므로 막아둔다.
// 	if (!nAspectOn && m_SignalInfo.ENABLED)
// 		m_SignalInfo.TM1 = 0;


	if((m_wCheckResult & ROUTECHECK_OCCTMOUT) && !(m_wCheckResult & ROUTECHECK_ALR) )
	{
		BYTE nNewControl = *((BYTE*)&m_SignalInfo + 1);
/*
		if ( (nOldControl ^ nNewControl) & nOldControl & 0x04 )   //CALLON 제어 중
		{	
			m_SignalInfo.TM1 = 1;
			return;
		}
		else 
		{
			if ( m_SignalInfo.TM1 == 1 ) {
				m_SignalInfo.TM2 = 1;
			}
			else {
				m_SignalInfo.TM2 = 0;
			}
		}
		m_SignalInfo.TM1 = 0;
*/

		m_SignalInfo.SOUT2 = 0;
		m_SignalInfo.SOUT1 = 0;
		m_SignalInfo.SOUT3 = 0;
		m_SignalInfo.U = 0;
// 		m_SignalInfo.LEFT = 0;
// 		m_SignalInfo.RIGHT = 0;

		m_SignalInfo.SOUT3 = m_SignalInfo.SE3;
		m_SignalInfo.SOUT2 = m_SignalInfo.SE2;
		m_SignalInfo.SOUT1 = m_SignalInfo.SE1;
		m_SignalInfo.U = m_SignalInfo.SE4;

		// Left, Right direction 제어는 유경과 하지 않기로 하였으나, 고장 검지를 위해 Setting해야 한다..
		// Left, Right direction 제어는 Lamp 검지 후 
		// Left, Right direction 제어는 출력 제어와 동시로 바꿈... (Signal aspect recovery 후 Outter Signal이 G로 되었다가 YY로 되는 현상 방지)
// 		if ( m_SignalInfo.SOUT2 || m_SignalInfo.SOUT3 || m_SignalInfo.U ) 
// 		{
// 			if ( nRouteStatus & RT_LEFT )
// 			{
		if(nRouteStatus & RT_FREESHUNT)
		{
			m_SignalInfo.LEFT = 1;
 				
			// 2006.6.15  Free shunt signal의 경우 마주보는 signal의 aspect를 현시하기 위해...				
			// 2007.7.18  F 신호기가 검지되어야 상대편 Shunt 신호 및 F 신호를 출력하도록 조치함 (m_SignalInfo.INLEFT 조건 추가함)... 
			if (m_SignalInfo.U && (m_pTrackInfoTable[ pInfo[RT_OFFSET_FROM] ].bNOTRACK) && m_SignalInfo.INLEFT)
			{
				ScrInfoSignal *pDependSignal = (ScrInfoSignal *)int_get(SignalGet, nPrevSigID);
				pDependSignal->SE4 = 1;
				pDependSignal->U = 1;
				pDependSignal->LEFT = 1;
				pDependSignal->OSS = 1;
			}
		}
// 			}
// 			else
// 				m_SignalInfo.LEFT = 0;
// 
// 			if ( nRouteStatus & RT_RIGHT )
// 				m_SignalInfo.RIGHT = 1;
// 			else
// 				m_SignalInfo.RIGHT = 0;
//		}
// 		else 
// 		{
// 			m_SignalInfo.LEFT = 0;
// 			m_SignalInfo.RIGHT = 0;
//  		}

		m_SignalInfo.DESTID |= 0x80;
	}
	else 
	{
		m_SignalInfo.SOUT2 = 0;
		m_SignalInfo.SOUT1 = 0;
		m_SignalInfo.SOUT3 = 0;
		m_SignalInfo.U = 0;
		m_SignalInfo.LEFT = 0;
		m_SignalInfo.RIGHT = 0;

		//m_SignalInfo.ENABLED = 0;	//2005.11.25
	}

	return;
}



void CEipEmu::CheckAddSwitchRoute(BYTE *pInfo, BYTE *dest, BYTE nTrack)
{
    BYTE *pRoute;
	unsigned short i; 
	unsigned short nCount;

	RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
	pRoute = pInfo + pRouteBasPtr->nOfsRoute;

	nCount = *pRoute++; // Route

	for (i=0; i<nCount; i++) 
	{
		BYTE nID = *pRoute++;  //진로내의 궤도 
        if (nTrack == nID) 
		{
            if (!*dest) 
			{
                *dest = nTrack;
                return;
            }
            else if (*dest == nTrack) 
			{
                return;
            }
            else if (!*(dest+1)) 
			{
                *(dest+1) = nTrack;
                return;
            }
        }
	}

	return;
}


BOOL CEipEmu::RunOverlap( short nRoute )
{

	ASSERT( nRoute );

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
	BYTE nID = pInfo[ RT_OFFSET_SIGNAL ];			// 신호기 ID 

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

    // Switch lock
	BYTE nOverSigID = 0;
	BYTE nTime = 0;

	BYTE nTrkID = *p++;     // 종착궤도 
	BYTE nOverTime = *p++;	// Overlap 보류쇄정 시간 

    // COUNT TRK SW4 SG4 SW3 SG3 00 SW2 SW1
	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;
		nOverSigID = *p++;
		
		BYTE swpos = nID & 0x80;
		nID &= 0x7f;

		int_get(SwitchLoad, nID);

//		if (nOverSigID & SWT_MASK_OVERLAP)  //Overlap 전철기
		{
			short nSigFree = 1;

			BYTE nSwitchType = m_pSwitchInfoTable[nID].nSwitchType;
			// Hand point의 경우 Overlap 설정을 하지 않는다.
			if ( nSigFree && (nSwitchType != 0x30) ) 
			{				// 정위(정지)
				if ( nOverSigID & SWT_FORCED_MOVE )
				{
					if (!m_SwitchInfo.WRO_P && !m_SwitchInfo.WRO_P)
					{
						BYTE swp, swm;
						if (swpos == 0) 
						{ 
							swp = 1; 
							swm = 0;	
						}
						else            
						{ 
							swp = 0; 
							swm = 1; 
						}

						SW_Timer = 0;  // 전철기 전환 대기 시간 
						m_SwitchInfo.REQUEST_P = swp;
						m_SwitchInfo.REQUEST_M = swm;


						if (m_SwitchInfo.WR_P != swp || m_SwitchInfo.WR_M != swm) 
						{   // 방향 불일치인 경우 전철기 전환을 해야 한다.
							SW_Timer = nTime;  // 전환해야 할 전철기의 갯수에 따라 전체 진로의 전철기 전환 대기 시간이 결정된다.
#ifdef _WINDOWS
							nTime += 5;
#else
							nTime += 5;
#endif
							m_SwitchInfo.ROUTELOCK = 0;  // 진로쇄정을  free 상태로 만든다.
						}
					}
					else
					{
						return FALSE;
					}
				}
			}

			m_pSwitchTimer[ nID ] = SW_Timer;  // 전철기 전환 대기 시간 저장 
			int_put(SwitchPut);
		}
	}
	
	return TRUE;
}


BYTE CEipEmu::DetectOverlap( short nRoute )
{
	ASSERT( nRoute );

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
	BYTE nSigID = pInfo[ RT_OFFSET_SIGNAL ];		// 신호기 ID 
	BYTE nRouteDest = pInfo[ RT_OFFSET_TO ];        // 종착궤도

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

    // Switch lock
	BYTE nOverSigID = 0;

	BYTE nTrkID = *p++;     // 종착궤도 
	BYTE nOverTime = *p++;	// Overlap 보류쇄정 시간 
	nOverTime = 60;
	
#ifdef _CHOTEST
	nOverTime /= 10;
#endif

	BYTE CheckDetect = 0;

	BYTE nRouteDirection = 0;
	if ((m_pSignalInfoTable[nSigID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2)   // 좌,우행 결정...  Left -> 1
		nRouteDirection = 1;

    if (m_pCommonTrack[nRouteDest]) 
		nRouteDest = m_pCommonTrack[nRouteDest]; 

	// Desttrack과 DestID를 최종적으로 다시 등록해 준다...
	m_SignalInfo.DESTTRACK = nRouteDest;  // 종착궤도 등록 

	int_put(SignalPut);

	BYTE nID = 0;
    // COUNT TRK SW4 SG4 SW3 SG3 00 SW2 SW1
	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;
		nOverSigID = *p++;
		
		BYTE swpos = nID & 0x80;
		nID &= 0x7f;

		int_get(SwitchLoad, nID);

		if (nOverSigID & SWT_IN_OVERLAP)   //Overlap 전철기
		{
			BYTE nSwitchType = m_pSwitchInfoTable[nID].nSwitchType;

			// Hand point의 경우 Overlap 설정을 하지 않는다.
			if ( nSwitchType != 0x30 ) 
			{				// 정위(정지)
				BYTE swp, swm;
				if (swpos == 0) 
				{ 
					swp = 1; 
					swm = 0;	
				}
				else            
				{ 
					swp = 0; 
					swm = 1; 
				}

				if ( ((m_SwitchInfo.KR_P == m_SwitchInfo.WR_P) && (m_SwitchInfo.KR_P == swp)) && 
					 ((m_SwitchInfo.KR_M == m_SwitchInfo.WR_M) && (m_SwitchInfo.KR_M == swm)) )
				{
					
					if ( !m_SwitchInfo.ROUTELOCK  && m_SwitchInfo.TRACKLOCK)
					{
						CheckDetect |= 0x0f;
					}

//					m_pRouteCompleteTK[nTrkID] = 0;	

					if(m_pSwitchOverTrack1[nID].wRouteID == 0)
					{
						m_pSwitchOverTrack1[nID].nTrackID = nTrkID;
						m_pSwitchOverTrack1[nID].nDelay = nOverTime;  // Overlap 시간을 설정한다...
						m_pSwitchOverTrack1[nID].wRouteID = (nRoute & ROUTE_MASK);
					}
					else if(m_pSwitchOverTrack2[nID].wRouteID == 0)
					{
						m_pSwitchOverTrack2[nID].nTrackID = nTrkID;							;
						m_pSwitchOverTrack2[nID].nDelay = nOverTime;  // Overlap 시간을 설정한다...
						m_pSwitchOverTrack2[nID].wRouteID = (nRoute & ROUTE_MASK);
					}
				}
				else
				{
					CheckDetect |= 0xf0;  // 전환이 아직 이루어 지지 않은 경우 혹은 (불일치가 발생하는 경우)
				}
			}
			else
			{
				if ( !m_SwitchInfo.ROUTELOCK )
					CheckDetect |= 0x0f;
			}
		}
	}

	//2006.11.16 Overlap 표시를 위해 추가...
	//2013.3.12  CheckDetect 조건 삭제...
	if (!m_nSwitchFail && /*!CheckDetect &&*/ (m_wCheckResult & ROUTECHECK_OVERLAP) )
	{
		int j;

		// Track
		RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
		p = pInfo + pRouteBasPtr->nOfsTrack;

		nCount = *p++;
		BYTE nTrackCount = 0;
		BYTE nTrackID[20] ={0};
		ScrInfoTrack *pTrkInfo;	

		for (j=0; j<nCount; j++) 
		{
			if (*p)
			{
				nTrackID[nTrackCount++] = *p++;
			}
			else
				*p++;
		}

		// Route
		nCount = *p++;

		if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET))
		{
			BYTE nRouteDest = nTrackID[nCount - 1];
			BYTE nCurTrkID = nRouteDest;

			for (j=nCount; j<nTrackCount; j++)
			{
				if (nTrackID[j])
				{
					pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, (nTrackID[j] & TRACK_MASK));

					if (nRouteDirection)
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 저장함.
						pTrkInfo->RightToLeftRouteNo = (nRoute & ROUTE_MASK);
					}
					else
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 저장함.
						pTrkInfo->LeftToRightRouteNo = (nRoute & ROUTE_MASK);
					}

					// 20140902 sjhong - Overlap Track 배열에 종착 궤도부터 시작하는 해당 진로의 모든 overlap 궤도를 저장한다.
					if(m_pOverTrack[nCurTrkID] == 0)
					{
						m_pOverTrack[nCurTrkID] = (nTrackID[j] & TRACK_MASK);
							
						if (nRouteDirection)
						{
							m_pOverTrack[nCurTrkID] |= TRACK_DIR_LEFT;
						}

						nCurTrkID = nTrackID[j];
					}
				}
			}
		}
	}

	return CheckDetect;
}

// 2012.11.29 추가함...
BYTE CEipEmu::StartDetectOverlap( short nRoute )
{

	ASSERT( nRoute );

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
	BYTE nID = pInfo[ RT_OFFSET_SIGNAL ];			// 신호기 ID 
	BYTE nRouteDest = pInfo[ RT_OFFSET_TO ];        // 종착궤도

	BYTE CheckDetect = 0;

	BYTE nRouteDirection = 0;
	if ((m_pSignalInfoTable[nID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2)   // 좌,우행 결정...  Left -> 1
		nRouteDirection = 1;

	//2006.11.16 Overlap 표시를 위해 추가...
	if (!m_nSwitchFail && !CheckDetect)
	{
		int j;

		// Track
		RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
		BYTE *p = pInfo + pRouteBasPtr->nOfsTrack;

		unsigned short nCount = *p++;
		BYTE nTrackCount = 0;
		BYTE nTrackID[20] ={0};
		ScrInfoTrack *pTrkInfo;	

		for (j=0; j<nCount; j++) 
		{
			if (*p)
			{
				nTrackID[nTrackCount++] = *p++;
			}
			else
				*p++;
		}

		if(!(m_pRouteQue[nID] & ROUTE_DEPENDSET))
		{
			BYTE nCurTrkID = nRouteDest;

			// Route
			nCount = *p++;
			
			for (j=nCount; j<nTrackCount; j++)
			{
				if (nTrackID[j])
				{
					pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, (nTrackID[j] & 0x7f));

					if (nRouteDirection)
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 설정함.
						pTrkInfo->RightToLeftRouteNo = (nRoute & ROUTE_MASK);
					}
					else
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 설정함.
						pTrkInfo->LeftToRightRouteNo = (nRoute & ROUTE_MASK);
					}
					
					// 20140902 sjhong - Overlap Track 배열에 종착 궤도부터 시작하는 해당 진로의 모든 overlap 궤도를 저장한다.
					if(m_pOverTrack[nCurTrkID] == 0)
					{
						m_pOverTrack[nCurTrkID] = (nTrackID[j] & TRACK_MASK);
						
						if (nRouteDirection)
						{
							m_pOverTrack[nCurTrkID] |= TRACK_DIR_LEFT;
						}
						
						nCurTrkID = nTrackID[j];
					}
				}
			}
		}
	}

	return CheckDetect;
}


void CEipEmu::RunRoute( short nRoute )
{

	ASSERT( nRoute );

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
	BYTE nID = pInfo[ RT_OFFSET_SIGNAL ];			// 신호기 ID 
	BYTE nPrevSigID = pInfo[ RT_OFFSET_PREVSIGNAL ];	// 전방신호기 ID
	BYTE nRouteDest = pInfo[ RT_OFFSET_TO ];        // 종착궤도
	BYTE nMultiDestCount = 0;
	BYTE nStayDestTrackID = 0;
	
	if(pInfo[RT_OFFSET_MULTIDEST] & TRACK_STAY_DEST)
	{
		nStayDestTrackID = pInfo[RT_OFFSET_MULTIDEST] & TRACK_MASK;	// 체류종착궤도
	}
	else
	{
		nMultiDestCount = pInfo[RT_OFFSET_MULTIDEST];	// 다중종착궤도
	}

// 20150402 sjhong - Main 진로의 신호기가 순차점유 해정에 의해 해정되었을 때, 이 진로에 이어서 Shunt 진로가 나는 것을 방지하는 코드.
// 현재는 보류하고 추후 충분히 시험된 이후에 적용해야함.
 	if(!(m_pRouteQue[nID] & ROUTE_DEPENDSET))
 	{
 		m_pOwnerRouteQue[ nPrevSigID ] = (nRoute & ROUTE_MASK);
 	}

	// 종착궤도에 계속 점유되어 있는 열차를 위해 Overlap을 해제 한다.
	// 이것이 해제되지 않으면 다음 진로 설정 시 Overlap이 설정 되지 않는다...
	ScrInfoTrack *pDestTrack = (ScrInfoTrack *)int_get(TrackGet, nRouteDest);

    if (m_pCommonTrack[nRouteDest]) 
		nRouteDest = m_pCommonTrack[nRouteDest]; 

	BYTE nSigID = nID;

	BYTE nRouteDirection = (m_pSignalInfoTable[nSigID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2;  // Left -> 1

	SG_Timer = 0;  // 접근 쇄정시간..
	m_SignalInfo.ON_TIMER = 0;
//	m_SignalInfo.LMRTIMER = 0;
    m_SignalInfo.FREE = 0;                // 신호기를 쇄정 시킨다.

	int_put(SignalPut);

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

    // Switch lock
	BYTE nOverSigID = 0;
	BYTE nTime = 0;

	BYTE nTrkID = *p++;     // 종착궤도 
	BYTE nOverTime = *p++;	// Overlap 보류쇄정 시간 

	BYTE bIsSyncSwitch = FALSE;

    // COUNT TRK SW4 SG4 SW3 SG3 00 SW2 SW1
	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;
		nOverSigID = *p++;
		
		BYTE swpos = nID & 0x80;
		nID &= 0x7f;

		int_get(SwitchLoad, nID);

      // 오버랩의 경우 SWID-SIGID 순서로 반복
        m_SwitchInfo.BACKROUTE = 0;

		if(!(nOverSigID & SWT_IN_OVERLAP))   //Overlap 전철기가 아니면,
		{	// m_pSwitchRouteTrack정보에 분기에 대응하는 쇄정정보용 궤도 ID 지정
			BYTE *pRouteTrack = &m_pSwitchRouteTrack[nID * 2];

            BYTE nRouteTrack = m_pSwitchOnTrackTable[nID].pData[0];
            CheckAddSwitchRoute( pInfo, pRouteTrack, nRouteTrack );

            nRouteTrack = m_pSwitchOnTrackTable[nID].pData[1];
            CheckAddSwitchRoute( pInfo, pRouteTrack, nRouteTrack );

			BYTE swp = 0, swm = 0;

			if (swpos == 0) 
			{ 
				swp = 1; 
				swm = 0;
			}
			else            
			{ 
				swp = 0; 
				swm = 1; 
			}

			SW_Timer = 0;  // 전철기 전환 대기 시간 
			m_SwitchInfo.REQUEST_P = swp;
			m_SwitchInfo.REQUEST_M = swm;

			if (m_SwitchInfo.WR_P != swp || m_SwitchInfo.WR_M != swm) 
			{   // 방향 불일치인 경우 전철기 전환을 해야 한다.
				SW_Timer = nTime;  // 전환해야 할 전철기의 갯수에 따라 전체 진로의 전철기 전환 대기 시간이 결정된다.
#ifdef _WINDOWS
				nTime += 20;
#else
				nTime += 5;
#endif
                m_SwitchInfo.ROUTELOCK = 0;  // 진로쇄정을  free 상태로 만든다.
			}

			// 20141015 sjhong - 동기화 전철기의 정보를 저장한다.
			if(nOverSigID & SWT_COMBO_SLAVE)
			{
				bIsSyncSwitch = TRUE;
			}
			else if(nOverSigID & SWT_COMBO_MASTER)
			{
				if(bIsSyncSwitch)
				{
					// 바로 이전 전철기의 ID를 저장한다.
					BYTE nSlaveSwitchID = *(p - 4) & SYNCSWT_MASK;

					// MASTER 전철기 정보에 이전 전철기 ID를 저장한다.
					m_pSyncSwitch[nID] = nSlaveSwitchID | SYNCSWT_MASTER;

					// SLAVE 전철기 정보에 현재 전철기 ID를 저장한다.
					m_pSyncSwitch[nSlaveSwitchID] = nID & SYNCSWT_MASK;
				}

				bIsSyncSwitch = FALSE;
			}
		}

		m_pSwitchTimer[ nID ] = SW_Timer;  // 전철기 전환 대기 시간 저장 
		int_put(SwitchPut);
	}

// Route
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
	p = pInfo + pRouteBasPtr->nOfsRoute;

	BYTE *pByteTrack = p;   // 아래에서 사용하기 위해 pointer 저장 
	nCount = *p++;
    nID = 0;
	
	for (i=0; i<nCount; i++) 
	{
		nID = *p++;
		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);
		pTrkInfo->TKREQUEST = 1;

		if(m_pRouteQue[nSigID] & ROUTE_DEPENDSET)
		{
			pTrkInfo->RouteNo = (m_pOwnerRouteQue[nSigID] & ROUTE_MASK);
		}
		else
		{
			pTrkInfo->RouteNo = nRoute;
		}

		// 마지막 종착궤도를 제외한 다중 종착궤도들에 MULTIDEST 값을 설정한다.
		if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET))
		{
			if(nMultiDestCount && (i == (nCount - nMultiDestCount - 1)))
			{
				pTrkInfo->MULTIDEST = 1;
				nMultiDestCount--;
			}
			else
			{
				pTrkInfo->MULTIDEST = 0;
			}
		}
	}

    if (nID) 
	{
		ASSERT( (nID & 0x80) == 0 );		// for Track128

		if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET))
		{
			m_pLastTrack[ nID ] = nRouteDest;
		}
	}

	if(nStayDestTrackID)
	{
		if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET))
		{
			m_pLastTrack[ nStayDestTrackID ] = nStayDestTrackID | TRACK_STAY_DEST;
		}
	}

	BYTE temp[24];
	memset( temp, 0, 24 );
	short temptr = 0;


	BYTE pTrk[64];
	nCount = 0;

	if (m_bIsYudo)
	{
		if (!m_SignalInfo.OSS && pInfo[RT_OFFSET_APPRCHTK_BEGIN])
			pTrk[nCount++] = (pInfo[RT_OFFSET_APPRCHTK_BEGIN] & TRACK_MASK);
	}
	else 
	{
		for(BYTE cTkOffset = RT_OFFSET_APPRCHTK_BEGIN ; cTkOffset <= RT_OFFSET_APPRCHTK_END ; cTkOffset++)
		{
			if(pInfo[cTkOffset] > 0)
			{
				pTrk[nCount++] = pInfo[cTkOffset];
			}
		}
	}

    short nAppCount = nCount;
	for (short nSeq = nCount-1; nSeq>=0; nSeq--) // reverse
	{	
		nID = pTrk[nSeq];
		if (nID) 
			temp[temptr++] = nID;
	}

	if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET) || ((m_pSignalInfoTable[ nSigID ].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME))
	{
		p = pByteTrack;   // 위에서 저장한 진로 내 궤도 사용 (Route Track)...
		nCount = *p++;
		for (i=0; i<nCount; i++)
		{
			nID = *p++;

			temp[temptr++] = nID;

			// 20141113 sjhong - 진로 내 인접 다음 궤도에 대한 테이블을 저장한다.
			if((i + 1) == nCount)
			{
				m_pTrackNext[nID] = 0;
			}
			else
			{
 				m_pTrackNext[nID] = *p;

			}
		}

		BYTE t0, t1, t2;
		t0 = m_pSignalInfoTable[nSigID].nSignalTrackID;  // 신호기 궤도 저장 
	/*
		if (m_pTrackInfoTable[t0] & TI_NOTRACK) 
		{
			t0 = 0;
		}
	*/
		for (i=nAppCount; i<temptr; i++) 
		{
			t1 = temp[i];
			if (i+1 < temptr) 
				t2 = temp[i+1];
			else 
				t2 = 0;

		  // 쇄정 방향 설정...
			ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, t1);
			pTrkInfo->ROUTEDIR = nRouteDirection; // 좌,우행 결정...  Left -> 1

		  //진로내의 궤도 중 진로에 따른 이전 인접 궤도를 설정한다....
			m_pTrackPrev[ t1 ] = t0;  
			t0 = t1;
		}
	}

	if(nStayDestTrackID)
	{
		if(m_pTrackNext[nStayDestTrackID] == nRouteDest)
		{
			ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nStayDestTrackID);

			pTrkInfo->MULTIDEST = 1;
		}
	}
}


void CEipEmu::CancelRoute( short wRoute )
{
    wRoute &= ROUTE_MASK;
	ASSERT( wRoute );

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRoute ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];	  // 연동도표 정보 테이블
	BYTE nID = pInfo[ RT_OFFSET_SIGNAL ];     // 신호기 기준이므로 신호기 정보임.
	BYTE nRouteDest = pInfo[ RT_OFFSET_TO ];  // 종착궤도
	BYTE nSigID = nID;

	BYTE nRouteDirection = 0;
	if ((m_pSignalInfoTable[nSigID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2)   // 좌,우행 결정...  Left -> 1
		nRouteDirection = 1;

    if (m_pCommonTrack[nRouteDest]) 
		nRouteDest = m_pCommonTrack[nRouteDest];

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

	BYTE nDependSigID = 0;
	for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
	{
		nDependSigID = (pInfo[cSigOffset] & SIGNALID_MASK);
		if(nDependSigID)
		{
			// 20140214 sjhong - Depend Signal로 설정된 진로에 대해서만 진로 취소가 되도록 한다.
			if(m_pRouteQue[nDependSigID] & ROUTE_DEPENDSET)
			{
				m_pRouteQue[nDependSigID] |= ROUTE_CANCEL;
			}
		}
	}

	BYTE nTrkID = *p++;     //종착궤도
	BYTE nOverTime = *p++;  //보류쇄정시간
	BYTE nOverSigID = 0;

	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;
		nID &= 0x7F;

		nOverSigID = *p++;

		int_get(SwitchLoad, nID);

		m_SwitchInfo.REQUEST_P = 0;   // 전철기 제어를 Clear한다...
		m_SwitchInfo.REQUEST_M = 0;
        m_SwitchInfo.BACKROUTE = 0;
        m_SwitchInfo.ON_TIMER = 0;
		
		int_put(SwitchPut);


		if((nOverSigID & SWT_IN_OVERLAP) && !m_SignalInfo.REQUEST) 
		{
            if(m_pSwitchOverTrack1[nID].wRouteID == wRoute)
			{
				m_pSwitchOverTrack1[nID].nTrackID = 0;
				m_pSwitchOverTrack1[nID].nDelay = 0;
				m_pSwitchOverTrack1[nID].wRouteID = 0;
            }

            if(m_pSwitchOverTrack2[nID].wRouteID == wRoute) 
			{
				m_pSwitchOverTrack2[nID].nTrackID = 0;
				m_pSwitchOverTrack2[nID].nDelay = 0;
				m_pSwitchOverTrack2[nID].wRouteID = 0;
            }
		}
	}

// Track - Track Table의 ID는 TRACK_MASK를 적용해야한다.
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
	p = pInfo + pRouteBasPtr->nOfsTrack;

// 진로 취소 시 중첩되어 있는 OVERLAP이 없어지면 안된다.
// 	nCount = *p++;	
// 	for (i=0; i<nCount; i++) 
// 	{ 
// 		nID = *p++;	
// 	
// 		// 2006.11.16 Overlap 표시를 위해 삽입...
// 		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, (nID & TRACK_MASK));
// 		if (nRouteDirection)
// 		{
// 			// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
// 			pTrkInfo->RightToLeftRouteNo = 0;
// 		}
// 		else
// 		{
// 			// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
// 			pTrkInfo->LeftToRightRouteNo = 0;
// 		}
// 	}

// Route - Route Track Table의 ID는 TRACK_MASK 적용하지 않아도 된다.
	p = pInfo + pRouteBasPtr->nOfsRoute;

	nCount = *p++;

    for (i=0; i<nCount; i++) 
	{ 
        nID = *p++;
		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);
		pTrkInfo->TKREQUEST = 0;
		pTrkInfo->RouteNo = 0;
		pTrkInfo->ROUTE = 1;
		pTrkInfo->MULTIDEST = 0;

		for (BYTE nSwID=0 ; nSwID <= m_TableBase.InfoSwitch.nCount ; nSwID++)
		{
			if (m_pSwitchRouteTrack[nSwID * 2] == nID)
			{
				m_pSwitchRouteTrack[nSwID * 2] = 0;
			}
			
			if (m_pSwitchRouteTrack[nSwID * 2 + 1] == nID)
			{
				m_pSwitchRouteTrack[nSwID * 2 + 1] = 0;
			}
		}
	}


	BYTE nOverTrkID = m_pOverTrack[nID] & TRACK_MASK;
	if (nOverTrkID)
	{
		ScrInfoTrack *pOverTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nOverTrkID);
					
		if(nRouteDirection)
		{
			// 20140522 sjhong - 진로 방향에 따라 Overlap을 삭제함.
			ClearOverlap(pOverTrkInfo->RightToLeftRouteNo, 0);
		}
		else
		{
			// 20140522 sjhong - 진로 방향에 따라 Overlap을 삭제함.
			ClearOverlap(pOverTrkInfo->LeftToRightRouteNo, 0);
		}

		// Overlap Track 테이블 초기화.	- Overlap Track 버퍼(m_pOverTrack)의 ID는 TRACK_MASK 적용해야함.
		BYTE nNextTrkID = 0;
		BYTE nDelTrkID = nID;
		while(nDelTrkID != 0)
		{
			nNextTrkID = m_pOverTrack[nDelTrkID];
			m_pOverTrack[nDelTrkID] = 0;
			nDelTrkID = nNextTrkID & TRACK_MASK;
		}
	}

    if(nID) 
	{
		ASSERT(nID != 0);

        m_pLastTrack[ nID ] = 0;
    }

	m_SignalInfo.REQUEST = 0;
	m_SignalInfo.FREE = 1;
	m_pSignalTimer[nID] = 0; //접근 쇄정시간 정보를 Clear한다.. 
	m_SignalInfo.DESTTRACK = 0;
	m_SignalInfo.CS = 0;
	m_SignalInfo.DESTID = 0;
	m_SignalInfo.OSS = 0;

	// 초기화 한다.
	m_pOwnerRouteQue[nID] = 0;

	if(!(m_pRouteQue[nID] & ROUTE_DEPENDSET))
	{
		BYTE nPrevSigID = pInfo[RT_OFFSET_PREVSIGNAL];

		m_pOwnerRouteQue[nPrevSigID] = 0;
	}

   	SignalStopU();

}

short CEipEmu::GetFindRoute( short nRoute )
{
	short nResult = 0;
	
	if ((nRoute & ROUTE_MASK) == 0) 
		return 0;

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블...

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

	BYTE nTrkID = *p++;			//종착궤도
	BYTE nOverTime = *p++;		//보류쇄정시간

	BYTE nOverSigID = 0;
	BYTE nID;
	BYTE index=0, nSwResult=0, nFindNoused=0, nSwitchFail=0, nCheckGood=0;

	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;

		nOverSigID = *p++;
		BYTE swpos = nID & 0x80;
		nID &= 0x7f;

		ScrInfoSwitch *pSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nID);

		short nSigFree = 1;
		if ( (nOverSigID & SWT_DETECT_ONLY) || (pSwInfo->NOUSED == 1) ) 
		{
			BYTE swp, swm;
			if (swpos == 0) 
			{
				swp = 1;
				swm = 0;
			}
			else 
			{
				swp = 0;
				swm = 1;
			}

			// 샤이스타 간지 처럼 전철기를 사용하지 않는 것으로 선택 할 경우 check 하지 않는다... 
			if ( (pSwInfo->NOUSED == 1) && !(nOverSigID & SWT_DETECT_ONLY) )
			{
				if (swp==1 && swm==0)  // 샤이스타간지 27에 대한 Special case 이다...
				{
					nSwResult++;
					nCheckGood = 1;
				}
				nFindNoused = 1;
			}
			else 
			{
				if ( pSwInfo->WR_P == swp && pSwInfo->WR_M == swm )
					nSwResult++;
			}

			index++;
		}

		if ( !pSwInfo->REQUEST_P && !pSwInfo->REQUEST_M )
		{
			if ( !(pSwInfo->KR_P == pSwInfo->WR_P && pSwInfo->KR_M == pSwInfo->WR_M)
				   || (!pSwInfo->KR_P && !pSwInfo->WR_P && !pSwInfo->KR_M && !pSwInfo->WR_M) ) 
			{
				nSwitchFail = 1;
			}
		}
	}

	if (index == nSwResult)
		nResult = 1;
	else if ( !nFindNoused && nSwitchFail )
	{
		nResult = 1;
		m_nSwitchFail = 1;    //2006.11.16  Overlap route표시를 위해...
	}
	else if ( nFindNoused && nCheckGood && nSwitchFail )
	{
		nResult = 1;
		m_nSwitchFail = 1;	//2006.11.16  Overlap route표시를 위해...
	}

	return nResult;
}


WORD CEipEmu::CheckRoute( short wRoute )
{
	WORD wCheckResult = 0;
	if ((wRoute & ROUTE_MASK) == 0) 
		return 0;

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블..
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)pInfo;

	BYTE nSigID = pInfo[ RT_OFFSET_SIGNAL ];
	BYTE nPrevSigID = pInfo[ RT_OFFSET_PREVSIGNAL ];
	BYTE nSigType = m_pSignalInfoTable[ nSigID ].nSignalType & SIGNAL_MASK;

	ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, nSigID);

	BYTE nRouteDest = pInfo[RT_OFFSET_TO];
	BYTE nRouteStatus = pInfo[ RT_OFFSET_ROUTESTATUS ];

	// 20150123 sjhong - 블록 Info인 경우이므로 여기서 바로 리턴한다.
	if(nRouteStatus == BLOCK_IDENTIFIER)
	{
		return wCheckResult;
	}

	// 중계 신호기 일 떄, 주진로의 조건을 반영하거나, 제거하는 루틴이므로 맨 앞으로 위치 이동.
	if((nSigType != SIGNALTYPE_OHOME) && pSigInfo->OSS)  // Ishunt or preset
	{
		//nReqTime = 0;
		wCheckResult |= ROUTECHECK_OCCTMOUT;

		// 20140423 sjhong - 중계 진로일 때, 주진로의 Overlap 구간 전철기 및 궤도 상태에 대한  체크 로직을 수정 (별도 테이블 사용하지 않고 직접 주진로를 조사하도록.)
		if(m_pRouteQue[nSigID] & ROUTE_DEPENDSET)
		{
			WORD wMainRouteNo = (m_pOwnerRouteQue[nSigID] & ROUTE_MASK);
			if(wMainRouteNo != 0)
			{
				DWORD nMainInfoPtr	= *(DWORD*)&m_pRouteAddTable[wMainRouteNo];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				BYTE *pMainInfo		= &m_pInfoMem[nMainInfoPtr];
				RouteBasPtr *pMainRouteBasPtr = (RouteBasPtr*)pMainInfo;
 
				BYTE nMainSigID		= pMainInfo[RT_OFFSET_SIGNAL];
				ScrInfoSignal *pMainSigInfo = (ScrInfoSignal *)int_get(SignalGet, nMainSigID);

				WORD wMainCheckResult = m_pSignalInfoTable[nMainSigID].wCheckResult;
				// 주 진로의 SGOFF 조건(TRACK 제외)을 중계 진로에도 적용한다.
				wCheckResult |= wMainCheckResult;					

				// TRACK 조건의 경우, 순차 진입으로 인한 주신호기의 OFF는 정상이므로 구분해서 처리한다.
				BYTE *pMainTrackInfo = pMainInfo + pMainRouteBasPtr->nOfsTrack;
				BYTE nMainTrackCount = *pMainTrackInfo++;
				BYTE bFindFirstTrack = FALSE;
				BYTE bSignalClearOnlyTrackMask = FALSE;
				BYTE nMainTrkID;
				BYTE bIsOverlapTrk = FALSE;

				ScrInfoTrack *pMainTrkInfo = NULL;
				ScrInfoTrack *pFromTrkInfo = NULL;

				// 20150831 sjhong - Main진로 구간의 불법 점유는 무시하고 Overlap 구간에 한정하여 처리하도록 원복.
				// 20140513 sjhong - Main진로 구간의 불법 점유에 대한 처리 추가 (Overlap 구간에 한정하여 처리하던 것을 Main 진로 전체에 대해 처리하도록 수정함)
				for(BYTE j = 0; j < nMainTrackCount / 2 ; j++, pMainTrackInfo += 2)
				{
					nMainTrkID = *pMainTrackInfo;
					if(nMainTrkID & 0x80)
					{
						bSignalClearOnlyTrackMask = TRUE; // 진로 설정 시에만 Check하고, 진로가 설정된 다음에는 check하지 않는 궤도...
						nMainTrkID &= 0x7F;
					}

					pMainTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nMainTrkID);

					// Overlap이 설정된 궤도이면서 연속 점유가 아닌 점유 상태이면 
					if(!bSignalClearOnlyTrackMask && !pMainTrkInfo->TRACK && !pMainTrkInfo->OVERLAP)
					{
						// OVERLAP TRACK
						if((wMainRouteNo == pMainTrkInfo->RightToLeftRouteNo) || (wMainRouteNo == pMainTrkInfo->LeftToRightRouteNo))
						{
							wCheckResult |= ROUTECHECK_OVERLAPTRACK;
							break;
						}
					}
				}
				
				// 20150823 sjhong - BR요청에 의해 주 진로에 연속 점유 궤도가 없을 경우에만 중계 신호기의 현시를 떨군다.
				// 20150714 sjhong - BR요청에 의해 원복한다.
				// 20150507 sjhong - 본 진로 구간의 전철기에 불일치가 발생했을 때, 해당 전철기가 속해있는 궤도에 진로가 설정되어 있다면, 중계 신호기의 현시를 떨어뜨린다.
				pMainTrackInfo = pMainInfo + pMainRouteBasPtr->nOfsRoute;
				nMainTrackCount = *pMainTrackInfo++;
					
				// 진로 구간의 궤도 중에 연속 점유 궤도가 있을 경우 본 진로의 SGOFF 조건을 무시한다.
				for (j=0 ; j < nMainTrackCount ; j++, pMainTrackInfo++)
				{
					nMainTrkID = *pMainTrackInfo;
					pMainTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nMainTrkID);

					if(!pMainTrkInfo->TRACK && (pMainTrkInfo->OVERLAP || ((j == 0) && (pMainSigInfo->SIN1 || pMainSigInfo->SIN2 || pMainSigInfo->SIN3 || pMainSigInfo->INU))))
					{
						wCheckResult &= ~ROUTECHECK_TRACK;
						wCheckResult &= ~ROUTECHECK_SWITCH;
						wCheckResult &= ~ROUTECHECK_SWFREE;
						break;
					}
				}
			}
		}
	}

	// Advnaced Starter 신호기가 블록 취급 상황에 따라 제어할 수 있도록 하는 루틴.
	if (nPrevSigID) 
	{
		BYTE nPrevSigType = m_pSignalInfoTable[ nPrevSigID ].nSignalType & SIGNAL_MASK;
		if (nPrevSigType == BLOCKTYPE_AUTO_OUT || nPrevSigType == BLOCKTYPE_INTER_OUT) // 연동폐색 출발...
		{
			ScrInfoBlock *pPrevBlkInfo = (ScrInfoBlock *)int_get(SignalGet, nPrevSigID);
			if(!pPrevBlkInfo->LCR || !pPrevBlkInfo->LCRIN || pPrevBlkInfo->DEPART
			|| ((nPrevSigType == BLOCKTYPE_AUTO_OUT) && ((!pPrevBlkInfo->SAXLACTIN && !pPrevBlkInfo->AXLOCC) || (pPrevBlkInfo->SAXLACTIN && !pPrevBlkInfo->SAXLOCC))))
			{
				wCheckResult |= ROUTECHECK_SIGNAL;
			}

			// 05.09.07   Advanced start signal은 설정 후 다시 꺼져도 Block정보의 Astart정보는 유지 해야 한다.
			// 그리하여  다시 Astart signal을 설정하기 위해서는 폐색 취급을 cancel하고 다시 처음부터 폐색 취급을 하여 설정한다.			
			if ( !pSigInfo->SIN1 && !pSigInfo->SIN2 && (!pSigInfo->SE1 && !pSigInfo->SE2) )
			{
				if(pPrevBlkInfo->ASTARTSTICK)
				{
					//printf("LINE: %d, ASTARTSTICK=%d\n", __LINE__, pPrevBlkInfo->ASTARTSTICK);
					wCheckResult |= ROUTECHECK_SIGNAL;
				}
			}
		}
	}

	// 20130904 sjhong - FAT 이슈에 의해 TCB 블록 취급이 완료된 상태라면 진입 궤도를 건너가는 출발 진로가 나지 않도록 함. 
	BYTE nConflictBlkID = pInfo[ RT_OFFSET_CONFLICTBLK ];
	if(nConflictBlkID)
	{
		BYTE nSigType = m_pSignalInfoTable[ nConflictBlkID ].nSignalType;
		nSigType &= SIGNAL_MASK;
		
		if (nSigType == BLOCKTYPE_AUTO_IN || nSigType == BLOCKTYPE_INTER_IN) // 연동폐색 출발...
		{
			ScrInfoBlock *pConflictBlkInfo = (ScrInfoBlock *)int_get(SignalGet, nConflictBlkID);
			if(pConflictBlkInfo->LCR && pConflictBlkInfo->LCRIN) 
			{
				wCheckResult |= ROUTECHECK_SIGNAL;
			}
		}
	}


	//Require Track Occupation & time...
	BYTE nReqTime = (pInfo[RT_OFFSET_BERTH_TIME] & TRACK_MASK);
	BOOL bBerthCheck = (pInfo[RT_OFFSET_BERTH_TIME] & TRACK_BERTH_MASK);
	BYTE nBerthTrk = 0;

#ifdef _CHOTEST
	nReqTime /= 10;
#endif
	
	if(bBerthCheck)
	{
		for(BYTE cTkOffset = RT_OFFSET_APPRCHTK_BEGIN ; cTkOffset <= RT_OFFSET_APPRCHTK_END ; cTkOffset++)
		{
			nBerthTrk = pInfo[cTkOffset];
			if(nBerthTrk == 0)
			{
				break;
			}
			
			BYTE &tktimer1 = *((BYTE*)&m_pTrackTimer[nBerthTrk]+1);	// for calc occ time
			ScrInfoTrack *pBerthTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nBerthTrk);
			if (!pBerthTrkInfo->TRACK) 
			{
				// 20150901 BERTH TRACK이 복수개일 때 열차 진행에 따른 궤도 해정 시 다시 BERTH TIME CHECK가 들어가지 않도록 한다.
				if((tktimer1 > nReqTime) || pSigInfo->INU || pSigInfo->SIN3) 
				{
					wCheckResult |= ROUTECHECK_OCCTMOUT;
				}
			}
		}

		if(cTkOffset == RT_OFFSET_APPRCHTK_BEGIN)
		{
			// BERTH TRACK이 하나도 없는 경우
			wCheckResult |= ROUTECHECK_OCCTMOUT;
		}
	}
	else	// forced signal aspect 
	{
		wCheckResult |= ROUTECHECK_OCCTMOUT;
	}

// 	BYTE nReqOcc = (pInfo[ RT_OFFSET_CALLONTK ] & TRACK_MASK);
// 	BYTE nReqTime = (pInfo[ RT_OFFSET_CALLONTK ] & TRACK_HOLD_TIME_ZERO) ? 0 : 30;
// 
// #ifdef _CHOTEST
// 	nReqTime /= 10;
// #endif
// 
// 	if ( nReqOcc ) 
// 	{
// 		BYTE &tktimer1 = *((BYTE*)&m_pTrackTimer[nReqOcc]+1);	// for calc occ time
// 		ScrInfoTrack *pTrkOcc = (ScrInfoTrack *)int_get(TrackGet, nReqOcc);
// 		if ( !pTrkOcc->TRACK ) 
// 		{			
// 			if ( tktimer1 > nReqTime ) 
// 			{
// 				wCheckResult |= ROUTECHECK_OCCTMOUT;
// 			}
// 		}
// 	}
// 	else {	// forced signal aspect
// 		wCheckResult |= ROUTECHECK_OCCTMOUT;
// 	}

	BYTE *p = pInfo + RT_OFFSET_SWITCH;
	unsigned short i, nCount = *p++;

	BYTE nTrkID = *p++;			//종착궤도
	BYTE nOverTime = *p++;		//보류쇄정시간

	BYTE nOverSigID = 0;
	BYTE nID;

	for (i=2; i<nCount; i+= 2) 
	{
		nID = *p++;
		nOverSigID = *p++;
		BYTE swpos = nID & 0x80;
		nID &= 0x7f;

		int_get(SwitchLoad, nID);

 		short nSigFree = 1;
 
		if (nOverSigID & SWT_IN_OVERLAP)	// Overlap 전철기
 		{
 			if ( !(nOverSigID & SWT_DETECT_ONLY) && !(nOverSigID & SWT_FORCED_MOVE) ) 
 			{		
 				ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, nOverSigID );
 				nSigFree = pSigInfo->FREE;
 			}
 			if (!nSigFree)		// 반위(현시) -> 무시한다...
			{
 				continue;
			}

			wCheckResult |= ROUTECHECK_OVERLAP;

			// 다른 진로가 전철기를 선점하여 사용 중....
			if ( !m_SwitchInfo.ROUTELOCK && (m_SwitchInfo.REQUEST_P || m_SwitchInfo.REQUEST_M) )
			{
				wCheckResult |= ROUTECHECK_SWITCH;
			}
		}
		else	// 진로 구간 전철기
		{
			BYTE nSwitchType = m_pSwitchInfoTable[nID].nSwitchType;

			// 20150823 sjhong - 수동전철기일 경우 전철기 고장 시에도 진로가 설정되는 오류 보
			if((/*(nSwitchType != 0x30) &&*/ !m_SwitchInfo.ROUTELOCK))
			{
				wCheckResult |= ROUTECHECK_SWFREE;
			}
		}

		// 2006.4.24   Overlap switch 뿐만 아니라 진로 내 Switch도 Fail 정보를 check하로록 함....
		if (m_SwitchInfo.SWFAIL)
		{
			wCheckResult |= ROUTECHECK_SWFREE;
		}

		BYTE swp, swm;
		if (swpos == 0) {
			swp = 1;
			swm = 0;
		}
		else {
			swp = 0;
			swm = 1;
		}

		if ( m_SwitchInfo.REQUEST_P ) 
		{
			if ( swm ) 
				wCheckResult |= ROUTECHECK_SWITCH;   // #define ROUTECHECK_SWITCH  2
		}
		else if ( m_SwitchInfo.REQUEST_M ) 
		{
			if ( swp ) 
				wCheckResult |= ROUTECHECK_SWITCH;
		}

		if (m_SwitchInfo.NOUSED && !(nOverSigID & SWT_IN_OVERLAP))
		{
			if (swp==0 && swm==1)  // NOUSED인 전철기를 진로내의 반위제어로 사용 할 경우 진로 설정을 하지 않는다...
				wCheckResult |= ROUTECHECK_SWITCH;
		}

		// 20130822 sjhong - 선로전환기 BLOCK 설정 시 유관 진로가 실행될 수 없도록 차단함.
		if(!m_SwitchInfo.BLOCK && ((swp && m_SwitchInfo.WR_M) || (swm && m_SwitchInfo.WR_P)))
		{
			wCheckResult |= ROUTECHECK_SWITCH;   // #define ROUTECHECK_SWITCH  2
		}

		if ( m_SwitchInfo.ROUTELOCK || !m_SwitchInfo.TRACKLOCK)  // 전철기가 쇄정되어 있는 경우 Check...
		{
			if (m_SwitchInfo.WR_P != swp || m_SwitchInfo.WR_M != swm)
			{
				wCheckResult |= ROUTECHECK_SWITCH;  // Overlap 전철기 오방향 쇄정 시 노랑색 유지.... // 20140720 sjhong - CONTACT -> SWITCH로 조건 변경
			}
		}
		// 쇄정되지 않은 Overlap전철기가 있다면, 다른 진로로 설정가능 하므로 노란색 상태를 유지하기 위해...
		else if(nOverSigID & SWT_IN_OVERLAP) 
		{
			if ( (nOverSigID & SWT_DETECT_ONLY) && (wCheckResult & ROUTECHECK_SWITCH) )
			{
				wCheckResult |= ROUTECHECK_SWFREE;
				wCheckResult &= ~ROUTECHECK_SWITCH;
			}
		}

		// Key lock check ...
		if ( !m_SwitchInfo.WKEYKR )    // Key transmitter에서 key remove 상태..  
			wCheckResult |= ROUTECHECK_SWITCH;

		else  // key가 빠져 있지 않을 경우... (group중 다른 key는 빠져 있을 수 있다.)  05.09.07 
		{
	        // key 제어 check ... Key제어가 on일 진로 설정이 되지 않는다... 
		    SystemStatusType &nSysStatus = ((DataHeaderType*)m_pVMEM)->nSysVar;

			// 2005.09.22 - Hand point의 경우 Key release 상태에서도 진로 설정이 되지 않아야 한다..
			BYTE nSwitchType = m_pSwitchInfoTable[nID].nSwitchType;
			if (nSwitchType == 0x30)  
			{
				if ( m_SwitchInfo.KEYOPER )
				    wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80

				//2006.03.06  LSM에서 출발진로 설정후 home에 위치한 hand point를 알기 위해 수정 - 이때 hand point의 key를 뺄 수 없다...
				m_SwitchInfo.INROUTE = 1;
				int_put(SwitchPut);

				continue;
			}

			BYTE nCrankNo = m_pSwitchInfoTable[nID].nPointKeyNo;
			if (nCrankNo)
			{
				if (nCrankNo == 1)  // down
				{
					if (nSysStatus.bPKeyContS)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.  (key group중 어떤 key가 빠져 있는지 알 수 없다.. 그러므로 on에서 off제어는 할 수 없다.)
						wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
				}
				else if (nCrankNo == 2) // up
				{
					if (nSysStatus.bPKeyContN)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
						wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
				}
				else if (nCrankNo) // for sylhet station
				{
					if (nCrankNo == 11)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContA)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 12)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContB)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 13)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContC)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 14)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContD)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 15)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContE)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 16)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContF)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}
					else if (nCrankNo == 17)  // 1 --  Key 체결
					{
						if (nSysStatus.bPKeyContG)   // 1 --  Key 제어 중인 상태.. 이때는 contact 조건이다.
							wCheckResult |= ROUTECHECK_CONTACT; // #define ROUTECHECK_CONTACT 0x80
					}

				}
			}
		}
	}


/*  Block을 위해 사용 할 수 있음.... 
    if ( m_pSignalInfoTable[ nSigID ].nSGContSigID ) 
	{
        nID = m_pSignalInfoTable[ nSigID ].nSGContSigID;
		ScrInfoSignal *pSigLockInfo = (ScrInfoSignal *)int_get(SignalGet, nID);

		if (pSigLockInfo->REQUEST) 
			wCheckResult |= ROUTECHECK_SIGNAL;  // #define ROUTECHECK_SIGNAL  4
    }
*/


// 대항진로를 검색하여 Check한다.
/*
if ((nRoute & ROUTE_MASK) == 2) 
int debug = 1;
*/

	// Preset으로 나는 진로일 경우 대항진로를 검색하지 않는다.
	if(!(m_pRouteQue[nSigID] & ROUTE_DEPENDSET))
	{
		p = pInfo + pRouteBasPtr->nOfsSignal;

		// 20140515 sjhong - 대항진로 Table을 BYTE(1byte) --> WORD(2bytes) 로 확장한다.
		WORD *pWordPtr = (WORD*)p;	// WORD형의 포인터를 선언하고 참조하도록 한다.
		nCount = *pWordPtr++;

		for(i = 0 ; i < nCount; i++) 
		{
			WORD wProhibitNo = *pWordPtr++;
			if(wProhibitNo == 0)
			{
				continue;
			}

			DWORD nProhibitInfoPtr = *(DWORD*)&m_pRouteAddTable[ wProhibitNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pInfoRt = &m_pInfoMem[nProhibitInfoPtr];
			BYTE nProhibitSigID = pInfoRt[ RT_OFFSET_SIGNAL ];
			BYTE nProhibitPrevSigID = pInfoRt[ RT_OFFSET_PREVSIGNAL ];
			BYTE nProhibitRtStatus = pInfoRt[RT_OFFSET_ROUTESTATUS];
			ScrInfoSignal *pProhibitSignal = (ScrInfoSignal *)int_get(SignalGet, nProhibitSigID );
			BYTE nProhibitSigType = m_pSignalInfoTable[nProhibitSigID].nSignalType & SIGNAL_MASK;

			// main 진로 설정 중 preset으로 난 진로가 있으면 무시한다.
			if ((nProhibitSigType != SIGNALTYPE_OHOME) && pProhibitSignal->OSS)
			{
				continue;
			}

			// 현재 대항진로가 주진로와 같으면 무시한다.
			if((m_pRouteQue[nSigID] & ROUTE_DEPENDSET) && ((m_pRouteQue[nProhibitSigID] & ROUTE_MASK) == (m_pOwnerRouteQue[nSigID] & ROUTE_MASK)))
			{
				continue;
			}

			WORD wRealProhibitNo = 0;
			
			// 진로제어 Que에 진로를 가지고 있을 경우....
			// 20131231 sjhong - LSM의 경우 m_pRouteQue에 값을 세팅하지 않으므로 항상 else 구문으로 처리된다.
			if ( m_pRouteQue[nProhibitSigID] )
			{
				// 설정되어 있는 대항 진로의 신호기를 통해 현재의 진로 상태 (대항진로 상태)를 가져온다.. 
				wRealProhibitNo = (m_pRouteQue[nProhibitSigID] & ROUTE_MASK);
			}
			else // 진로제어 Que에 진로를 가지지 않을 경우(LSM) 다시한번 조사 한다...
			{
				BYTE CS = pProhibitSignal->CS;

				// 20140114 sjhong - 동일 Signal에서 시작하는 다수의 진로에 대해 Route Order 값을 저장하고 진로 설정 시 이를 사용하여 DESTID를 설정.
				wRealProhibitNo = GetRouteNo(nProhibitSigID, CS, pProhibitSignal->DESTID & 0x3F/*(pRelSignal->DESTTRACK & 0x7f)*/);

				// 진로 값이 안나오는 경우, 전방신호기의 m_pOwnerRouteQue를 확인한다.
				if(wRealProhibitNo == 0)
				{
					if((nProhibitPrevSigID == nSigID) && (wProhibitNo == (m_pOwnerRouteQue[nProhibitPrevSigID] & ROUTE_MASK)))
					{
						wRealProhibitNo = wProhibitNo;
					}
				}
			}

			if ( wRealProhibitNo != 0 && wProhibitNo == wRealProhibitNo )  //현재의 진로가 대항진로와 같을 때...
			{
				if((m_pOwnerRouteQue[nProhibitSigID] == 0) || ((m_pOwnerRouteQue[nProhibitSigID] & ROUTE_MASK) != (wRoute & ROUTE_MASK)))
				{
					wCheckResult |= ROUTECHECK_SIGNAL;
				}
			}
		}
	}

	p = pInfo + pRouteBasPtr->nOfsTrack;

	nCount = *p++;
	BYTE *pStart = p;  // 아래에서 사용하기 위해 pointer 저장 -- 전철기 정보부터 가지고 있음.

	while ( *p ) 
	{
		BOOL bSignalClearOnlyTrackMask = FALSE;
        BYTE nTrkID = *p++;

		if (nTrkID > 0x80) 
		{
			bSignalClearOnlyTrackMask = TRUE; // 진로 설정 시에만 Check하고, 진로가 설정된 다음에는 check하지 않는 궤도...
			nTrkID &= 0x7F;
		}

		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nTrkID);

		short swused = 1;
		BYTE nSwID, nCount1=0;

		nSwID = *p++;
		while ( nSwID ) 
		{
			nCount1++;
			if (nCount1 > 1)
				p++;

			int_get(SwitchLoad, nSwID);

			BYTE swp,swm;
			BYTE swpos = nSwID & 0x80;  // 진로의 전철기 요구 방향... 

			if (swpos == 0) 
			{  
				swp = 1;  
				swm = 0;  
			}
			else            
			{  
				swp = 0;  
				swm = 1;  
			}

			if (m_SwitchInfo.KR_P == swm && m_SwitchInfo.KR_M == swp) 
				swused = 0;  //전철기 오방향상태 --> 전철기를 전환 시켜야 함...

			nSwID = *p;
		}

        if (pTrkInfo->INHIBIT && swused) 
		{
            wCheckResult |= ROUTECHECK_TRACK;		// #define ROUTECHECK_TRACK   1
        }

		if (!pTrkInfo->TRACK && swused) 
		{
            BOOL bInRoute = FALSE;

            //BYTE *rt = (pStart-1) + nCount;
            BYTE *rt = pStart + nCount;
            BYTE nRtCount = *rt++;

	        for (BYTE j=0; j<nRtCount; j++) 
			{
		        BYTE nRtID = *rt++;
                if (nRtID == nTrkID)
				{
					bInRoute = TRUE;
				}
            }

			ScrInfoSignal *pSignalInfo = (ScrInfoSignal *)int_get(SignalGet, nSigID );
			if ( !bSignalClearOnlyTrackMask || !(pSignalInfo->SIN1 || pSignalInfo->SIN2 || pSignalInfo->SIN3 || pSignalInfo->INU) )
			{
				if(bInRoute)
				{
					wCheckResult |= ROUTECHECK_TRACK;
				}
				else
				{
					wCheckResult |= ROUTECHECK_OVERLAPTRACK; // 20140526 sjhong - Overlap 구간의 Track은 별도 Bit으로 처리
				}
			}
        }
	}

	// 진로내 궤도중에 진로가 해정된 궤도가 있는지 조사 한다... 
    BYTE *rt = pStart + nCount;
    BYTE nRtCount = *rt++;

	for (BYTE j=0; j<nRtCount; j++) 
	{
		BYTE nRtID = *rt++;
		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nRtID);
		if (pTrkInfo->ROUTE)
		{
			wCheckResult |= ROUTECHECK_NOGREEN;
		}
    }

    p++;
	ASSERT( pStart = p + nCount );

	// Flank protection을 조사한다...
	BYTE *pFlank = pInfo + pRouteBasPtr->nOfsFlank;

	BYTE nFlankCount = *pFlank++;
	BYTE fcount = 0;
	while ( fcount<(nFlankCount-1) ) 
	{
		BYTE nFlankTrkID	= *pFlank++;
		BYTE nFlankSwID		= *pFlank++;
		BYTE nFlankSwOpr	= *pFlank++;
		BYTE nFlankSwID2	= *pFlank++;

		BOOL bSignalClearOnlyTrackMask = FALSE;

		if(nFlankTrkID & 0x80)
		{
			bSignalClearOnlyTrackMask = TRUE;
			nFlankTrkID &= 0x7F;
		}

		ScrInfoTrack *pFlankTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nFlankTrkID);
		
		if (!pFlankTrkInfo->TRACK && !pFlankTrkInfo->OVERLAP) 
		{
        }

		if ( !nFlankSwID )
		{
			if (!pFlankTrkInfo->TRACK && !pFlankTrkInfo->OVERLAP) // 진로가 설정되지 않고 점유된 Flank궤도 처리...
			{
				if ( !bSignalClearOnlyTrackMask || !(pSigInfo->SIN1 || pSigInfo->SIN2 || pSigInfo->SIN3 || pSigInfo->INU) )
				{
					wCheckResult |= ROUTECHECK_CONTACT;
				}
			}
		}
		else
		{
			ScrInfoSwitch *pFlankSwInfo = NULL;
			BYTE swp,swm;
			BYTE swpos;
			BYTE nFlankResult = 0, nFlankResult2 = 0;
			
			pFlankSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nFlankSwID);
			swpos = nFlankSwID & 0x80;  // 진로의 전철기 요구 방향... 
			if (swpos == 0) 
			{  
				swp = 1;
				swm = 0;  
			}
			else            
			{  
				swp = 0;  
				swm = 1;  
			}

			if ((pFlankSwInfo->KR_P == swp && pFlankSwInfo->KR_M == swm) || pFlankSwInfo->SWFAIL) 
			{
				// 진로가 설정되지 않고 점유된 Flank궤도 처리..., 전철기를 가진 궤도이므로 더 Check한다...
				if(!pFlankTrkInfo->TRACK && (!pFlankTrkInfo->OVERLAP && !(!pFlankTrkInfo->ROUTE && pFlankTrkInfo->TKREQUEST)))
				{
					nFlankResult = 1;
				}
			}

			// 20131119 sjhong - Flank Protection의 조건 전철기를 2개로 확장.
			if(nFlankSwOpr)
			{
				pFlankSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nFlankSwID2);
				swpos = nFlankSwID2 & 0x80;
				if (swpos == 0) 
				{  
					swp = 1;  
					swm = 0;  
				}
				else            
				{  
					swp = 0;  
					swm = 1;  
				}
				
				if ((pFlankSwInfo->KR_P == swp && pFlankSwInfo->KR_M == swm) || pFlankSwInfo->SWFAIL) 
				{
					// 진로가 설정되지 않고 점유된 Flank궤도 처리..., 전철기를 가진 궤도이므로 더 Check한다...
					if(!pFlankTrkInfo->TRACK && (!pFlankTrkInfo->OVERLAP && !(!pFlankTrkInfo->ROUTE && pFlankTrkInfo->TKREQUEST)))
					{
						nFlankResult2 = 1;
					}
				}

				if(nFlankSwOpr == '&')
				{
					if(nFlankResult && nFlankResult2)
					{
						if ( !bSignalClearOnlyTrackMask || !(pSigInfo->SIN1 || pSigInfo->SIN2 || pSigInfo->SIN3 || pSigInfo->INU) )
						{
							wCheckResult |= ROUTECHECK_CONTACT;
						}
					}
				}
				else if(nFlankSwOpr == '|')
				{
					if(nFlankResult || nFlankResult2)
					{
						if ( !bSignalClearOnlyTrackMask || !(pSigInfo->SIN1 || pSigInfo->SIN2 || pSigInfo->SIN3 || pSigInfo->INU) )
						{
							wCheckResult |= ROUTECHECK_CONTACT;
						}
					}
				}
			}
			else
			{
				if(nFlankResult)
				{
					if ( !bSignalClearOnlyTrackMask || !(pSigInfo->SIN1 || pSigInfo->SIN2 || pSigInfo->SIN3 || pSigInfo->INU) )
					{
						wCheckResult |= ROUTECHECK_CONTACT;
					}
				}
			}
		}

		fcount += 4;	// Flank 조건 DB 2Bytes -> 4Bytes 변경
	}

	nCount = *p++;
	for (i=0; i<nCount; i++) 
	{
		nID = *p++;
		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);

		if ( !pTrkInfo->ROUTE || pTrkInfo->TKREQUEST )  
		{
			wCheckResult |= ROUTECHECK_REROUTE; // #define ROUTECHECK_REROUTE 0x20
		}

#ifdef _LSM
		if ( m_strStationName.Find ( "YPA",0 ) > 0 )
		{
			if ( pTrkInfo->INHIBIT && !(nRouteStatus & RT_SHUNTSIGNAL) )
			{
				wCheckResult |= ROUTECHECK_ROUTE;   // #define ROUTECHECK_ROUTE   8
			}
		}
		else
		{
			if ( pTrkInfo->INHIBIT && !((nRouteStatus & RT_CALLON) || (nRouteStatus & RT_SHUNTSIGNAL)) )
			{
				wCheckResult |= ROUTECHECK_ROUTE;   // #define ROUTECHECK_ROUTE   8
			}
		}
#else
		if ( pTrkInfo->INHIBIT && !((nRouteStatus & RT_CALLON) || (nRouteStatus & RT_SHUNTSIGNAL)) )
		{
			wCheckResult |= ROUTECHECK_ROUTE;   // #define ROUTECHECK_ROUTE   8
		}
#endif
	}

	// CheckRoute의 최종 결과 중에서 SGOFF(신호기 OFF) 조건에 해당하는 필드만 저장한다.
	// 단, TRACK 조건은 중계 진로의 CheckRoute 처리에서 별도 체크하므로 저장하지 않는다.
	m_pSignalInfoTable[nSigID].wCheckResult = (wCheckResult & (ROUTECHECK_SGOFF/* & ~ROUTECHECK_TRACK*/));

	return wCheckResult;
}

void *CEipEmu::int_get(BYTE nType, BYTE nID) 
{

    switch (nType)
	{
	case SignalLoad:
		    memcpy( &m_SignalInfo, GP_InfoSignal( nID ), SIZE_INFO_SIGNAL );
		    m_nSignalID = nID;
			break;

	case SwitchLoad:
	        memcpy( &m_SwitchInfo, GP_InfoSwitch( nID ), SIZE_INFO_SWITCH );
			m_nSwitchID = nID;
			break;

	case TrackLoad:
		    memcpy( &m_TrackInfo, GP_InfoTrack( nID ), SIZE_INFO_TRACK );
		    m_nTrackID = nID;
			break;

	case SignalGet:
			return GP_InfoSignal( nID );
	case SwitchGet:
			return GP_InfoSwitch( nID );
	case TrackGet:
			return GP_InfoTrack( nID );
	}

	return NULL;
}


void CEipEmu::int_put(BYTE nType) 
{
    if (nType == SignalPut)
	{
	    memcpy( GP_InfoSignal( m_nSignalID ), &m_SignalInfo, SIZE_INFO_SIGNAL );
	}
	else if (nType == SwitchPut)
	{
	    memcpy( GP_InfoSwitch( m_nSwitchID ), &m_SwitchInfo,  SIZE_INFO_SWITCH );
	}
	else if (nType == TrackPut)
	{
	    memcpy( GP_InfoTrack( m_nTrackID ), &m_TrackInfo, SIZE_INFO_TRACK );
	}	
}


int _Time1Sec = 0;
BYTE nOldSec = 0;

//DEL void CEipEmu::PreTrackProcess() 
//DEL {
//DEL 
//DEL 	LoopCountL = (BYTE)m_TableBase.InfoTrack.nCount;
//DEL 	for (LoopCountH=0; LoopCountH<LoopCountL;LoopCountH++)
//DEL 	{
//DEL 		int_get(TrackLoad, LoopCountH+1);
//DEL 
//DEL 		BOOL accup = FALSE;
//DEL 
//DEL         if (m_TrackInfo.TRACK) 
//DEL 			m_TrackInfo.ALARM = 0;
//DEL 
//DEL 		if (m_TrackInfo.ROUTE != 0)
//DEL 		{
//DEL             m_TrackInfo.TKREQUEST = 0;
//DEL 		}
//DEL 
//DEL 		int_put(TrackPut);
//DEL 	}
//DEL }

void CEipEmu::RunInterlock( char *pMsg )
{
	if (!m_pVMEM) return;

	// 2007.7.18  비상해정 시 Overlap이 다시 설정되는 현상때문에 추가 
	BYTE nEmergencyStop[N_TK_COUNT] = {0,};
	int bTrackGood = TrackDelay(nEmergencyStop);

	_Time1Sec++;

#ifdef _WINDOWS
	if (_Time1Sec >= 17) { // 20
		_Time1Sec = 0;
	}
#else
	BYTE &nSec = ((DataHeaderType*)m_pVMEM)->nSec;
	if ( nOldSec != nSec ) {
		_Time1Sec = 0;
	}
	nOldSec = nSec;
#endif

	UnitStatusType &UnitState 	= ((DataHeaderType*)m_pVMEM)->UnitState;
    SystemStatusType &nSysVar 	= ((DataHeaderType*)m_pVMEM)->nSysVar;
	SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
	BYTE TcbCaOn = 0, TgbCaOn = 0;

	// Level crossing key transmitter 처리.... 
	if (!nSysVarExt.bLC1NKXPR && m_nLC1NKXPR)  // Key가 transmitter에서 빠져 있는 경우  
	{
		nSysVarExt.bLC1KXLR = 0;  // Key of transmitter will be automatically locked
	}
	m_nLC1NKXPR = nSysVarExt.bLC1NKXPR;		// 2006.6.5 보완
	
 	if (nSysVarExt.bLC1BELACK)  // Gate lodge에서 BELL 경보 제어에 대한 Ack취급이 있을경우 제어를 취소한다...
	{
		nSysVarExt.bLC1BELL = 0;  // Key of transmitter will be locked
		m_pLCBellTimer[1] = 0;	// LC BELL TIME 값 초기화
	}

	// LC BELL 출력을 설정 후 120초 경과하면 자동 OFF한다.
	if(nSysVarExt.bLC1BELL)
	{
		if((_Time1Sec == 0) && (m_pLCBellTimer[1] < LC_BELL_TIME))
		{
			m_pLCBellTimer[1]++;
		}

		if(m_pLCBellTimer[1] == LC_BELL_TIME)
		{
			nSysVarExt.bLC1BELL = 0;  // Key of transmitter will be locked
			m_pLCBellTimer[1] = 0;	// LC BELL TIME 값 초기화
		}
	}

	if (!nSysVarExt.bLC2NKXPR && m_nLC2NKXPR)  // Key가 transmitter에서 빠져 있는 경우  
	{
		nSysVarExt.bLC2KXLR = 0;  // Key of transmitter will be automatically locked
	}
	m_nLC2NKXPR = nSysVarExt.bLC2NKXPR;
	
 	if (nSysVarExt.bLC2BELACK)  // Gate lodge에서 BELL 경보 제어에 대한 Ack취급이 있을경우 제어를 취소한다...
	{
		nSysVarExt.bLC2BELL = 0;  // Key of transmitter will be locked
		m_pLCBellTimer[2] = 0;	// LC BELL TIME 값 초기화
	}
	
	// LC BELL 출력을 설정 후 120초 경과하면 자동 OFF한다.
	if(nSysVarExt.bLC2BELL)
	{
		if((_Time1Sec == 0) && (m_pLCBellTimer[2] < LC_BELL_TIME))
		{
			m_pLCBellTimer[2]++;
		}
		
		if(m_pLCBellTimer[2] == LC_BELL_TIME)
		{
			nSysVarExt.bLC2BELL = 0;  // Key of transmitter will be locked
			m_pLCBellTimer[2] = 0;	// LC BELL TIME 값 초기화
		}
	}

	if (!nSysVarExt.bLC3NKXPR && m_nLC3NKXPR)  // Key가 transmitter에서 빠져 있는 경우  
	{
		nSysVarExt.bLC3KXLR = 0;  // Key of transmitter will be automatically locked
	}
	m_nLC3NKXPR = nSysVarExt.bLC3NKXPR;
	
	if (nSysVarExt.bLC3BELACK)  // Gate lodge에서 BELL 경보 제어에 대한 Ack취급이 있을경우 제어를 취소한다...
	{
		nSysVarExt.bLC3BELL = 0;  // Key of transmitter will be locked
		m_pLCBellTimer[3] = 0;	// LC BELL TIME 값 초기화
	}

	// LC BELL 출력을 설정 후 120초 경과하면 자동 OFF한다.
	if(nSysVarExt.bLC3BELL)
	{
		if((_Time1Sec == 0) && (m_pLCBellTimer[3] < LC_BELL_TIME))
		{
			m_pLCBellTimer[3]++;
		}
		
		if(m_pLCBellTimer[3] == LC_BELL_TIME)
		{
			nSysVarExt.bLC3BELL = 0;  // Key of transmitter will be locked
			m_pLCBellTimer[3] = 0;	// LC BELL TIME 값 초기화
		}
	}

	if (!nSysVarExt.bLC4NKXPR && m_nLC4NKXPR)  // Key가 transmitter에서 빠져 있는 경우  
	{
		nSysVarExt.bLC4KXLR = 0;  // Key of transmitter will be automatically locked
	}
	m_nLC4NKXPR = nSysVarExt.bLC4NKXPR;

	if (nSysVarExt.bLC4BELACK)  // Gate lodge에서 BELL 경보 제어에 대한 Ack취급이 있을경우 제어를 취소한다...
	{
		nSysVarExt.bLC4BELL = 0;  // Key of transmitter will be locked
		m_pLCBellTimer[4] = 0;	// LC BELL TIME 값 초기화
	}

	// LC BELL 출력을 설정 후 120초 경과하면 자동 OFF한다.
	if(nSysVarExt.bLC4BELL)
	{
		if((_Time1Sec == 0) && (m_pLCBellTimer[4] < LC_BELL_TIME))
		{
			m_pLCBellTimer[4]++;
		}
		
		if(m_pLCBellTimer[4] == LC_BELL_TIME)
		{
			nSysVarExt.bLC4BELL = 0;  // Key of transmitter will be locked
			m_pLCBellTimer[4] = 0;	// LC BELL TIME 값 초기화
		}
	}

	if (!nSysVar.bLC5NKXPR && m_nLC5NKXPR)  // Key가 transmitter에서 빠져 있는 경우  
	{
		nSysVar.bLC5KXLR = 0;  // Key of transmitter will be automatically locked
	}
	m_nLC5NKXPR = nSysVar.bLC5NKXPR;
	
	if (nSysVar.bLC5BELACK)  // Gate lodge에서 BELL 경보 제어에 대한 Ack취급이 있을경우 제어를 취소한다...
	{
		nSysVar.bLC5BELL = 0;  // Key of transmitter will be locked
		m_pLCBellTimer[5] = 0;	// LC BELL TIME 값 초기화
	}
	
	// LC BELL 출력을 설정 후 120초 경과하면 자동 OFF한다.
	if(nSysVar.bLC5BELL)
	{
		if((_Time1Sec == 0) && (m_pLCBellTimer[5] < LC_BELL_TIME))
		{
			m_pLCBellTimer[5]++;
		}
		
		if(m_pLCBellTimer[5] == LC_BELL_TIME)
		{
			nSysVar.bLC5BELL = 0;  // Key of transmitter will be locked
			m_pLCBellTimer[5] = 0;	// LC BELL TIME 값 초기화
		}
	}
	
	BYTE bExistOutSignal = 0;
	BYTE bATBTimeout = 0;

    if (nSysVar.bATB == 1)
	{
	    if (_Time1Sec == 0) // 초가 증가...
		    m_nATBTimer++;

#ifdef _CHOTEST
		if (m_nATBTimer == (ATB_TEST_TIME / 10))
#else
		if (m_nATBTimer == ATB_TEST_TIME)
#endif
		{
			m_nATBTimer = 0;
		
			nSysVar.bATB = 0;
			bATBTimeout = 1;
		}
	}

	BYTE nAstartOn=0;
    BYTE *pInfo;			// 연동도표 정보 테이블
	BYTE nFromTrack;
	BYTE *p;
	BYTE nID, nRouteOrder;
	WORD j, nCount;


	// Power 경보 처리
	if (!nSysVar.bUPS1 || !nSysVar.bUPS2 || (!nSysVar.bPowerFail && !nSysVar.bGenRun))
	{
		if (m_nPowerFail == 0)
		{
			if(!m_ConfigList.BLK_DEV_MODE)
			{
				nSysVarExt.bSound3 = 1;
			}
			m_nPowerFail = 1;
		}
	}
	else
	{
		m_nPowerFail = 0;
	}


	// 20131010 sjhong - AXL Counter Fail 정보 추가
	nSysVar.bAXLFail = GetAllAXLFailStatus(); 

	// 선로전환기 Fail 처리
	BOOL nSearchMask = FALSE;
    for (nCount = 1 ; nCount < N_SW_COUNT ; nCount++)
	{
		if (m_pSwitchFail[nCount] == 1)
		{
			nSearchMask = TRUE;
		}
	}

    if (nSearchMask == FALSE)
	{
		nSysVar.bPointFail = 0;
	}


	// BLOCK ALARM 처리용 변수
	BYTE bIsDepartTCB = FALSE;		// SLS 처리를 위해 현재 DEPARTIN 상태인 TCB가 있는지 확인하는 Flag
	nSysVarExt.bSound1 = 0;			// 폐색 알람은 일단 OFF이다.

	// 신호기 Fail 처리
	nSearchMask = FALSE;
    for (nCount = 1 ; nCount < N_SW_COUNT ; nCount++)
	{
		// 일반 Signal이 Fail일 경우
		if (m_pSignalFail[nCount] == 1)
		{
			nSearchMask = TRUE;
		}

		// 폐색 알람 출력 처리
 		if((m_pSignalFail[nCount] & BLOCK_ALARM_DEPARTOUT) || (m_pSignalFail[nCount] & BLOCK_ALARM_DEPARTIN) || (m_pSignalFail[nCount] & BLOCK_ALARM_APPROACH)) 
		{
			if(m_pSignalFail[nCount] & BLOCK_ALARM_DEPARTIN)
			{
				bIsDepartTCB = TRUE; 
			}

			// 1초 TICK 마다 처리한다.
			if(_Time1Sec == 0)
			{
				if((m_pSignalFail[nCount] & BLOCK_ALARM_MASK) > 0)
				{
					m_pSignalFail[nCount]--;	// 시간 값을 -1 한다.
				}
			}

			// 알람 시간이 0보다 클 경우에만 알람을 출력한다.
			if((m_pSignalFail[nCount] & BLOCK_ALARM_MASK) > 0)
			{
				if(!m_ConfigList.BLK_DEV_MODE)
				{
	 				nSysVarExt.bSound1 = 1;
				}
			}
 		}
	}

    if (nSearchMask == FALSE)
	{
		nSysVar.bSignalFail = 0;
	}

	// Automatic point key 설정
	short nSwitch = m_TableBase.InfoSwitch.nCount;

#if 0	// 20130226 Key Transmitter 검지 처리 변경
	BYTE nWKEYKR=0, nPreCrankNo=0;
	BYTE nCrankState[20] = {0};
#endif

	for (int i=1; i<=nSwitch; i++)
	{
		BYTE nCrankNo = m_pSwitchInfoTable[i].nPointKeyNo;
#if 0	// 20130226 Key Transmitter 검지 처리 변경
		if ( nPreCrankNo==0 || nPreCrankNo < nCrankNo )
			nWKEYKR = 0;
#endif

		ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch *)int_get ( SwitchGet, i);
		if (nCrankNo)
		{
			if (nCrankNo == 1)  // down
			{
				if (nSysVar.bPKeyS)   // 1 --  Key 체결 
					pSwitchInfo->WKEYKR = 1;
				else
					pSwitchInfo->WKEYKR = 0;
			}
			else if (nCrankNo == 2) // up
			{
				if (nSysVar.bPKeyN)  // 1 --  Key 체결
					pSwitchInfo->WKEYKR = 1;
				else
					pSwitchInfo->WKEYKR = 0;
			}
			else
			{
				switch(nCrankNo)
				{
				case 11:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyA;
					break;
				case 12:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyB;
					break;
				case 13:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyC;
					break;
				case 14:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyD;
					break;
				case 15:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyE;
					break;
				case 16:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyF;
					break;
				case 17:
					pSwitchInfo->WKEYKR = nSysVarExt.bPKeyG;
					break;
				default:
					break;
				}
			}
#if 0	// 20130226 Key Transmitter 검지 처리 변경
			else if (nCrankNo) // 
			{
				if (nWKEYKR==0 && nPreCrankNo < nCrankNo)  // 1 --  Key 체결
				{
					if (pSwitchInfo->WKEYKR)
					    nWKEYKR = 1;	// Key insert status
					else
					    nWKEYKR = 2;	// Key remove status

					nCrankState[nCrankNo] = pSwitchInfo->WKEYKR; 
					nPreCrankNo = nCrankNo;
					continue;
				}

			    if (nWKEYKR==1 && nPreCrankNo==nCrankNo)  
					pSwitchInfo->WKEYKR = 1;		// 공통 Crank 설정 처리
			    else if (nWKEYKR==2 && nPreCrankNo==nCrankNo)
					pSwitchInfo->WKEYKR = 0;		// Key remove 처리.
				else if (nPreCrankNo>nCrankNo)
				{
					pSwitchInfo->WKEYKR = nCrankState[nCrankNo];
					continue;
				}
			}
#endif
		}
		else
		{
			//2006.03.06  LSM에서 출발진로 설정후 home에 위치한 hand point를 알기 위해 수정 - 이때 hand point의 key를 뺄 수 없다...
			if (m_pSwitchInfoTable[i].nSwitchType == 0x30)  
			{
				// 20151120 sjhong SLAVE 전철기가 수동일 경우 MASTER 전철기가 완전히 ROUTELOCK이 해정되지 않아도 락이 풀리는 문제 보완.
				if(m_pSyncSwitch[i] && !(m_pSyncSwitch[i] & SYNCSWT_MASTER))
				{
					BYTE nMasterSwID = m_pSyncSwitch[i];	

					if(m_pSwitchInfoTable[nMasterSwID].nSwitchType != 0x30)
					{
						ScrInfoSwitch *pMasterSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nMasterSwID);
						if(pMasterSwInfo->ROUTELOCK)
						{
							continue;
						}
					}
				}

				pSwitchInfo->INROUTE = 0;
			}
			continue;
		}

#if 0	// Key Transmitter 검지 처리 변경
		nPreCrankNo = nCrankNo;
#endif
	}

	if(m_ConfigList.BLK_DEV_MODE)
	{
		short nSignal = m_TableBase.InfoSignal.nCount;

		for (i=1; i<=nSignal; i++)
		{
			BYTE *pSigId = &m_pSignalInfoTable[i].nSignalType;		
			BYTE nSigType = *pSigId;
			BYTE nBlockType = nSigType & 0x0f0;

			if (nBlockType == BLOCKTYPE_AUTO_IN || nBlockType == BLOCKTYPE_AUTO_OUT || i==7 || i==11) 
				continue;

			ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, i);

			pSigInfo->LM_ALR = 0;
			
			if(i == 7 || i == 8)
			{
				pSigInfo->LM_LOR = 1;
			}
			else
			{
				pSigInfo->LM_LOR = 0;
			}
		}
	}

// 아카우라역 신호기 simulator가 부족하여 5개(2,4,47,49,59)를 임의로 정보를 만들어 준다...
/*
	short nSignal = m_TableBase.InfoSignal.nCount;
    for (i=1; i<=nSignal; i++)
    {
		BYTE *pSigId = &m_pSignalInfoTable[i].nSignalType;		
		BYTE nSigType = *pSigId;
		BYTE nBlockType = nSigType & 0x0f0;

		//if ( nBlockType != SIGNALTYPE_ASTART && i!=nSignal-1 && i!=nSignal-2)
		//if ( nBlockType != SIGNALTYPE_ASTART && i!=18)  // 클라우라 역....

		if (nBlockType == BLOCKTYPE_AUTO_IN || nBlockType == BLOCKTYPE_AUTO_OUT) 
			continue;
		else if ( nBlockType != SIGNALTYPE_ASTART )
			continue;
		//else if (i==7)  // 1번 신호기만 제외하고 모두 s/w simulation 처리....
		//	continue;

		ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, i);

		// Simulation을 위해 출력을 입력으로 대치한다...
		pSigInfo->SIN1 = pSigInfo->SOUT1;
	    //pSigInfo->SIN2 = pSigInfo->SOUT2;
	    pSigInfo->LM_CLOR = pSigInfo->SIN3 = pSigInfo->SOUT3;
	    pSigInfo->INLEFT = pSigInfo->LEFT;
	    pSigInfo->INRIGHT = pSigInfo->RIGHT;

	    pSigInfo->INU = pSigInfo->U;

		// pInfo->ROUTE는 좀 더 정확한 이해를 해야 할 것 같다....
		// 신호기에 대한 전체 입력이 1일 경우에 비로서 0이된다... 
	    //pSigInfo->ROUTE = !pSigInfo->SIN1 || !pSigInfo->SIN2 || !pSigInfo->SIN3 || !pSigInfo->INU;

		pSigInfo->LM_YL = 0;
		pSigInfo->LM_RL = 0;
		pSigInfo->LM_GL = 0;

		// 신호기 현시별로 simulation을 처리한다.... --> SIN3가 신호기 현시의 가장 LOW한 단계임...
		if (pSigInfo->LM_CLOR) 
		{
			pSigInfo->LM_CEKR = 1;
		}
		pSigInfo->LM_LOR = 1;
		pSigInfo->LM_EKR = 1;
	}
*/


	short bEmCancel = 0;	

	LoopCountL = (BYTE)m_TableBase.InfoSignal.nCount;

    for (LoopCountH=0; LoopCountH<LoopCountL; LoopCountH++)
	{
		BYTE nRID = LoopCountH+1;
		m_wRouteNo = m_pRouteQue[nRID];

		BYTE nOldControl = 0;
		BYTE nDependSigID = 0;
	    BYTE nNextSigID = 0;
		DWORD nInfoPtr = 0;

		m_nCheckOverlap = 0;

#if 0
		// 20140116 sjhong - 정지 신호기의 경우에도 진로를 낼 수 있도록 예외처리를 삭제한다.
		if ( (m_pSignalInfoTable[nRID].nSignalType & SIGNAL_MASK) == 0x60 )	// STOP sign은 처리하지 않는다....
			continue;
#endif

		if ( m_wRouteNo ) 
		{
 			if ( (m_wRouteNo & ROUTE_MASK) == 0)
			{
				m_pRouteQue[nRID] = 0;
			    continue;
			}

			nInfoPtr = *(DWORD*)&m_pRouteAddTable[ m_wRouteNo & ROUTE_MASK ];  // 진로Table의 자기진로 정보 위치	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			m_pRouteInfo = pInfo;

			// 첫 중계신호기에 대한 처리
			nDependSigID 	= (pInfo[ RT_OFFSET_DEPENDSIG1 ] & SIGNALID_MASK);
	    	nNextSigID		= pInfo[ RT_OFFSET_PREVSIGNAL ];
			
			// 2006.11.16 Overlap route 표시를 위해....
			m_nSwitchFail = 0;

			// 20140114 sjhong - 동일 Signal에서 시작하는 다수의 진로에 대해 Route Order 값을 저장하고 진로 설정 시 이를 사용하여 DESTID를 설정.
			nRouteOrder = pInfo[RT_OFFSET_ROUTEORDER];

#if 0
#ifndef _WINDOWS
			// 이것은 시험이 끝나고 설치 시 삽입해야 한다.. 진로 정보 오류로 인해 오진로가 설정되는 것을 방지해야 하기 때문이다.. 
			if (nNewRouteNo == 0)
			{
				m_pRouteQue[nRID] = 0;
				continue;
			}
#endif
#endif
		}
		else 
		{
			pInfo = NULL;
		}

 		SG_Timer = m_pSignalTimer[nRID];

		int_get(SignalLoad, nRID);

		if ( !UnitState.bSysRun )
		{
			SysDownProc();
			continue;
		}

		BYTE nSignalType = m_pSignalInfoTable[nRID].nSignalType & SIGNAL_MASK;  // 신호기 종류..

		// bIsInBlock이 1이면 진로가 설정되지 않는다.  주의....
        m_bIsInBlock = ( (nSignalType == BLOCKTYPE_AUTO_OUT) || (nSignalType == BLOCKTYPE_AUTO_IN) || (nSignalType == BLOCKTYPE_INTER_IN) || (nSignalType == BLOCKTYPE_INTER_OUT) );
		if (!m_bIsInBlock) 
		{
			if(m_wRouteNo & ROUTE_DEPENDSET)
			{
				WORD wOwnerRouteNo = m_pOwnerRouteQue[nRID] & ROUTE_MASK;
				for(int i = 0 ; i < N_SG_COUNT ; i++)
				{
					if((m_pRouteQue[i] & ROUTE_MASK) == wOwnerRouteNo)
					{
						m_SignalInfo.RCVRONLY = 0;
						break;
					}
				}
				
				if(i == N_SG_COUNT)
				{
					m_SignalInfo.RCVRONLY = 1;
				}
			}
			else
			{
				m_SignalInfo.RCVRONLY = 0;
			}
			
			if((nSignalType == SIGNALTYPE_OHOME) && m_SignalInfo.SE2 && !m_SignalInfo.TM1)
			{
				m_SignalInfo.RCVRONLY = 1;
			}

			if(nSysVar.bN3 == 1)
			{
				m_SignalInfo.LM_ALR = 0;
			}
			else
			{
				if (m_SignalInfo.SOUT1 || m_SignalInfo.SOUT2 || m_SignalInfo.SOUT3 || m_SignalInfo.U)
				{
					bExistOutSignal = 1;
				}

				if(!m_ConfigList.BLK_DEV_MODE)
				{
					if(!bATBTimeout && (bExistOutSignal || m_bSLSStick))
					{
						m_SignalInfo.LM_ALR = 0;
					}
					else
					{
						m_SignalInfo.LM_ALR = 1;
					}
				}
			}
		}

		BYTE nDepSignalType = 0;
		if (nDependSigID) 
		{
			nDepSignalType = m_pSignalInfoTable[nDependSigID].nSignalType & SIGNAL_MASK;
		}

        m_bIsYudo = ((m_pSignalInfoTable[nRID].nSignalType & SIGNALTYPE_YUDO_BIT) != 0);
        /*
		// 2006.10,18 Akaura역 10T 시작 Main 진로의 경우 Bypass의 outer home 진로 설정에 대한 Check를 위해 출력을 설정한다...
		// 만약 Signal 52의 값이 31에서 다른 값으로 바뀌면 아래 로직도 수정해야 한다...
		// 이것은 DB에 설정하고 찾아야하나, 아카우라만 적용되는 경우 이므로 physical한 값을 그냥 사용하기로 함....
		if (nRID== 31 && m_SignalInfo.FREE)
			nSysVar.bMainRoute = 0;
        */
		if ( (pInfo) && !((pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON) || (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL)) ) 
			m_bIsYudo = FALSE;

		bEmCancel = m_wRouteNo & ROUTE_ONEMCANCEL;

		if (m_wRouteNo & ROUTE_ASPECT)	// Signal recovery logic
		{
			if (m_SignalInfo.SIN1 || m_SignalInfo.SIN2 || m_SignalInfo.SIN3 || m_SignalInfo.INU)
			{
			    m_wRouteNo &= ~ROUTE_ASPECT;
			}

			// 모든 중계신호기에 대하여 처리
			for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
			{
				BYTE nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);
				if(nDependID)
				{
					// 20140214 sjhong - 중계 신호기로 설정된 진로가 아니면 패스
					if(!(m_pRouteQue[nDependID] & ROUTE_DEPENDSET))
					{
						continue;
					}

					ScrInfoSignal *pDepSigInfo = (ScrInfoSignal *)int_get (SignalGet, nDependID);
					if(pDepSigInfo->TM1)
					{
						if(pDepSigInfo->SIN1 || pDepSigInfo->SIN2)
						{
							m_SignalInfo.TM1 = 1;
							m_SignalInfo.DESTID &= 0x7f;
							m_wRouteNo &= ~ROUTE_ASPECT;
						}

						if(m_nDependSignalRecoveryCount > 21)   // 21/3=7, 전방에 있는 DependSignal이 off상태인 것을 대비하여 7Sec wait....
						{
							m_wRouteNo &= ~ROUTE_ASPECT;
						}
						else
						{
							m_nDependSignalRecoveryCount++;
						}
					}
					else
					{
						if(!(pDepSigInfo->SIN1 || pDepSigInfo->SIN2) && (pDepSigInfo->SE2 || pDepSigInfo->ENABLED))
						{
							pDepSigInfo->TM1 = 1;
							pDepSigInfo->DESTID &= 0x7f;
						}

						if ( (m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME )
						{
							m_wRouteNo &= ~ROUTE_ASPECT;
						}
						else
						{
							m_pRouteQue[nDependID] &= ~ROUTE_SIGNALSTOP;  // Ground shunt에 대해 적용 (Outer는 적용하지 않음).. 
							if (m_nDependSignalRecoveryCount > 21)   // 21/3=7, 전방에 있는 DependSignal이 off상태인 것을 대비하여 7Sec wait....
							{
								m_wRouteNo &= ~ROUTE_ASPECT;
							}
							else
							{
								m_nDependSignalRecoveryCount++;
							}	
						}
					}
				}
			}
		}

		if ( !m_bIsInBlock && (m_wRouteNo & ROUTE_MASK || m_SignalInfo.ON_TIMER) ) 
		{
			nOldControl = *(BYTE*)&m_SignalInfo;  // 현재의 출력정보를 이전 정보로 저장한다...

			short invoke = m_wRouteNo & ROUTE_INVOKE;
			short cancel = m_wRouteNo & ROUTE_CANCEL;
			short onroute = m_wRouteNo & ROUTE_ONPROCESS;

			// 20150327 sjhong - 리턴값을 클래스 멤버변수인 m_wCheckResult에 저장한다.
			m_wCheckResult = CheckRoute( m_wRouteNo );

#ifndef _WINDOWS
			LogInd(LOG_TYPE_EIP, "m_wRouteNo = 0x%X, m_wCheckResult = 0x%X", m_wRouteNo, m_wCheckResult); 
#endif

			if (!(m_wCheckResult & ROUTECHECK_SGOFF))
			{
				if (m_wRouteNo & ROUTE_SIGNALSTOP)  // 개별적으로 신호기 정지가 수행된 진로....
				{
					/*
					m_SignalInfo.TM1 = 0;
					m_SignalInfo.ENABLED = 0;
					SignalStopU();
					m_wCheckResult &= ~ROUTECHECK_OCCTMOUT;
					*/
					m_wCheckResult |= ROUTECHECK_OSS;   // 위의 4줄과 같은 효과...

					// 모든 중계신호기에 대하여 처리
					for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
					{
						BYTE nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);
						if(nDependID)
						{
							if((m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) != SIGNALTYPE_OHOME)
							{
								m_pRouteQue[nDependID] |= ROUTE_SIGNALSTOP;
							}
						}
					}
				}

				// 1. Outer signal Stop이 수행된 진로....
				if ( (nSignalType == SIGNALTYPE_OHOME) && m_SignalInfo.OSS )     
				{
					m_wCheckResult |= ROUTECHECK_OSS;
				}

				if ( m_SignalInfo.LM_ALR  )
				{
					m_wCheckResult |= ROUTECHECK_ALR;  // Signal is not aspected...
				}

				 
				if (nSignalType == SIGNALTYPE_OHOME && nNextSigID && !(pInfo[ RT_OFFSET_ROUTESTATUS ] & 0x80))
				{
					ScrInfoSignal *pNextSigInfo = (ScrInfoSignal *)int_get(SignalGet, nNextSigID);
					if (pNextSigInfo->REQUEST == 1)
					{
						m_wCheckResult |= ROUTECHECK_OSS;
					}
				}
			}

			if ( m_SignalInfo.ON_TIMER )  
			{
				SignalCheck(nSignalType, pInfo);

	            if (pInfo)
				{
					ClearRequest(pInfo);
				}

				if (!m_wRouteNo && nDependSigID && nDepSignalType != SIGNALTYPE_OHOME) 
				{
					//진로의 중간에 있는 depend signal은 여기에서 취소를 해야 한다....
					m_pRouteQue[nDependSigID] |= ROUTE_CANCEL;
				}
            }


			else if ( cancel ) 
			{		
				if ( ( onroute || !( m_wCheckResult & ROUTECHECK_TRACK) || (m_SignalInfo.ENABLED /*&& m_SignalInfo.TM1*/) ) ||
					 ( (nSignalType == SIGNALTYPE_OHOME) && (!m_SignalInfo.CS) ) || (m_bIsYudo && m_SignalInfo.TM1) )
				{						
					m_SignalInfo.ON_TIMER = 1;

					//if ( bIsYudo )   // Callon은 Approach locking 무시... --> 2005.4.28 수정 Callon,Shunt 는 30초 적용...
					//	m_SignalInfo.ENABLED = 0;

					SG_Timer = 0;

					//2006.6.15  Free shunt signal의 경우 마주보는 signal의 aspect를 danger하기 위해...
					if (m_SignalInfo.U && (m_pTrackInfoTable[ pInfo[RT_OFFSET_FROM] ].bNOTRACK) )
					{
						ScrInfoSignal *pPrevSignal = (ScrInfoSignal *)int_get(SignalGet, pInfo[RT_OFFSET_PREVSIGNAL]);
						pPrevSignal->OSS = 0;
					}

					SignalStopU();
				}
				else
					m_SignalInfo.ENABLED = 0;     

				m_wRouteNo &= ~ROUTE_CANCEL;
				m_wRouteNo &= ~ROUTE_SIGNALSTOP;   // 신호기 정지 취급을 clear한다...

				SignalCheck(nSignalType, pInfo);
				
				if(pInfo)
				{
					ClearRequest(pInfo);
				}

				m_SignalInfo.TM1 = 0;     

				if ( nSignalType != SIGNALTYPE_OHOME ) 
				{
					if (m_SignalInfo.OSS)
					{
						m_SignalInfo.OSS = 0;
					}
				}

				// 2006.11.16 Depend Signal의 Route cancel 뿐만 아니라 Time lock이 걸렸을 때 즉시 신호기 Stop도 수행해야 한다.
				// 모든 중계신호기에 대하여 처리
				for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
				{
					BYTE nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);
					if(nDependID)
					{
						// 신호기가 Outer Home 일 경우 무시한다.
						if((m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME)
						{
							continue;
						}

						m_pRouteQue[nDependID] |= ROUTE_SIGNALSTOP;
					}
				}
			}
			else if (onroute) 
			{
				if (m_SignalInfo.DESTID & 0x40)
				{
				    m_arrRouteNo[nRID] = (m_wRouteNo & ROUTE_MASK);
				}
				else
				{
				    m_arrRouteNo[nRID] = 0;
				}
			
				// 우선적으로 RLR 상태의 대항진로 Check를 위해 현재의 진로 정보를 등록한다...
				// 최종적으로는 DetectOverlap에서 등록한다...
				m_SignalInfo.DESTTRACK = pInfo[ RT_OFFSET_TO ];  // 종착궤도 등록 

#if 1
				// 20140114 sjhong - 동일 Signal에서 시작하는 다수의 진로에 대해 Route Order 값을 저장하고 진로 설정 시 이를 사용하여 DESTID를 설정.
				if(m_SignalInfo.DESTID & 0x40)
				{
					m_SignalInfo.DESTID = nRouteOrder + 0x40;
				}
				else
				{
					m_SignalInfo.DESTID = nRouteOrder;
				}
#else
				index += ( m_SignalInfo.DESTID & 0x40 ) ? 0x40 : 0;
				m_SignalInfo.DESTID = index + 1; // 종착ID 등록 
#endif
				
				m_nCheckOverlap = 0xf0;

				if ( (m_wCheckResult & ROUTECHECK_SGOFF) == ROUTECHECK_GOOD )  //#define ROUTECHECK_GOOD     0
				{
/*
if (nNewRouteNo == 5)
    int debug = 1;
*/
					// Overlap switch locking... 
					BOOL nCheckOverlap = TRUE;
					if (m_wCheckResult & ROUTECHECK_OVERLAP)
					{
						nCheckOverlap = RunOverlap(m_wRouteNo);
					}

					BYTE nNoDetect = DetectOverlap(m_wRouteNo);
					
					if(!nNoDetect && nCheckOverlap)
					{
						m_wRouteNo &= ~ROUTE_ONPROCESS;  // Onprocess flag를 clear함.
						m_wRouteNo |= ROUTE_INVOKE;      // Invoke flag를 seting함.

						m_SignalInfo.REQUEST = 0;	// 신호의 진로 요구 중임을 표시 (Yellow)...
						m_SignalInfo.U = 0;

						m_nCheckOverlap = 0xff;

						// 20140121 sjhong - 리플렉트 신호기 처리부의 단순화
						if(!m_SignalInfo.CS)
						{
							// 모든 중계신호기에 대하여 처리
							for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
							{
								BYTE nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);
								if(nDependID)
								{
									if((m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME)
									{
										continue;
									}

									ScrInfoSignal *pDependSigInfo = (ScrInfoSignal *)int_get(SignalGet, nDependID);
									if(pDependSigInfo)
									{									
										pDependSigInfo->OSS = 1;
									}
								}
							}
						}
					}
					else if (nNoDetect == 0x0f)  // Overlap 전철기가 원하는 방향으로 전환이 완료된 경우... 
					{
						m_nCheckOverlap = 0xff;
					}
				}
				else if ((m_wCheckResult & ROUTECHECK_TRACK) && m_bIsYudo) 
				{
					/*
					if ( (m_wCheckResultNext & ROUTECHECK_SGOFF) == ROUTECHECK_GOOD ) 
					{
						m_wRouteNo &= ~ROUTE_ONPROCESS;
						m_wRouteNo |= ROUTE_INVOKE;

						m_SignalInfo.REQUEST = 0;

						m_SignalInfo.SE1 = 0;		
						m_SignalInfo.SE2 = 0;		
						m_SignalInfo.SE3 = 0;
						m_nCheckOverlap = 0xff;
					}
					*/
				}

				if(nDepSignalType == SIGNALTYPE_OHOME) 
				{
					ScrInfoSignal *pDependSigInfo = (ScrInfoSignal *)int_get(SignalGet, nDependSigID);

					// 20131114 sjhong - Outer Home에 Callon 진로가 설정 중일 경우에 중계 신호를 내지 않도록 수정.					
					if((m_pRouteQue[nDependSigID] & ROUTE_MASK) == 0)
					{
						m_pRouteQue[nDependSigID] = (m_pSignalInfoTable[ nDependSigID ].wRouteNo | ROUTE_DEPENDSET);
						m_pOwnerRouteQue[nDependSigID] = (m_wRouteNo & ROUTE_MASK) | ROUTE_DEPENDSET;	// 20140423 sjhong - 주진로 번호 저장.

						//pDependSigInfo->OOC = 1;
						pDependSigInfo->REQUEST = 1;
					}
				}
			}

			else if (!invoke) 
			{	
                short nResult = m_wCheckResult;

				if (m_SignalInfo.FREE) // 처음 취급 
				{				
					nResult &= ROUTECHECK_ENABLE;
					
					m_SignalInfo.TM1 = 1;   // 해당신호가 Free임을 Marking...

					// Free shunt의 직진진로시 계속적인 진로설정을 위해....
					// 2006.6.15   Free shunt의 반대편 신호기 Aspect 설정을 위해 수정함...
					if ( m_bIsYudo && (nRID==/*nNextSigID*/nDependSigID) )
					{
						m_SignalInfo.DESTID |= 0x40;
						nResult &= ~ROUTECHECK_REROUTE;
					}					
				}
                else 
				{
					nResult &= ROUTECHECK_REENABLE;    // #define ROUTECHECK_REENABLE 0x0E
				}

				// 진로내에 있는 Shunt 진로를 위해 설정....
				if ( nResult==0x20 &&  ((pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL) || (nSignalType==SIGNALTYPE_ISHUNT) || (nSignalType==SIGNALTYPE_ISTART) || (nSignalType==SIGNALTYPE_SHUNT) || (nSignalType==SIGNALTYPE_OUT)))
				{
					nResult = ROUTECHECK_GOOD;
				}

				if ( nResult == ROUTECHECK_GOOD ) 
				{
                    if ( !m_bIsInBlock )
					{
	                    RunRoute( m_wRouteNo );
					}
/*
if (nSignalType==SIGNALTYPE_OHOME && nNextSigID)
	int debug = 1;
*/

        			if (m_SignalInfo.FREE)
					{
                        m_SignalInfo.TM1 = 1;
					}
					m_SignalInfo.REQUEST = 1;

					
					//if ( m_SignalInfo.ENABLED )     
					if ( m_SignalInfo.TM1 )     
					{         
						m_wRouteNo |= ROUTE_ONPROCESS;
					}
					else 
					{
						m_wRouteNo |= ROUTE_INVOKE;
					}


					if ( !( m_wCheckResult & ROUTECHECK_TRACK ) ) // 단락 궤도가 없으면.
					{	
						m_SignalInfo.FREE = 0;
						m_wRouteNo |= ROUTE_ONPROCESS;			// 분기전환중
                        m_SignalInfo.TM1 = 1;
					}
					/*
					else 
					{	
						if ( bIsYudo ) 
						{
							if ( !( m_wCheckResultNext & ROUTECHECK_TRACK)) // 다음 진로의 단락 궤도가 없으면.
							{	
								m_SignalInfo.FREE = 0;
								m_wRouteNo |= ROUTE_ONPROCESS;			// 분기전환중
                                //m_SignalInfo.ENABLED = 1;
                                m_SignalInfo.TM1 = 1;
							}
						}
					}
					*/
					if ( m_bIsYudo )
						m_SignalInfo.CS = 1;
				}
				else 
				{
					m_wRouteNo = 0;
					ClearRequest(pInfo);
				}
			}
// Invoke.....
			else  
			{
				//2006.11.16 Overlap 표시를 위해...
				if (!(m_wRouteNo & ROUTE_ONEMCANCEL) && !m_nSwitchFail)
				{
					// 2007.3.7.  수정
					/*
					for (index=0; index<nRouteRp+1; index++)
					{
						ClearOverlap( (m_wRouteNo & ROUTE_MASK)+index, 1 );
					}
					*/
					
					if (m_wCheckResult & ROUTECHECK_OVERLAP)
					{
					    //ClearOverlap( (m_wRouteNo & ROUTE_MASK), 1 );
					    RunOverlap(m_wRouteNo);
				        DetectOverlap(m_wRouteNo);
					}
					// 2012.11.29 추가함...
					else
					{
					    //ClearOverlap( (m_wRouteNo & ROUTE_MASK), 1 );
				        StartDetectOverlap(m_wRouteNo);
					}
				}
                /*
				// 2006.10,18 Akaura역 10T==44 시작 Main 진로의 경우 Bypass의 outer home 진로 설정에 대한 Check를 위해 출력을 설정한다...
				// 만약 10T의 값이 44에서 다른 값으로 바뀌면 아래 로직도 수정해야 한다...
				// 이것은 DB에 설정하고 찾아야하나, 아카우라만 적용되는 경우 이므로 physical한 값을 그냥 사용하기로 함....
				if (m_wRouteNo && pInfo[RT_OFFSET_FROM] == 44 && !m_SignalInfo.FREE && !m_SignalInfo.CS)
					nSysVar.bMainRoute = 1;
                */
				nFromTrack = pInfo[RT_OFFSET_FROM];
				ScrInfoTrack *pFromTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nFromTrack);

				p = pInfo + RT_OFFSET_PREVSIGNAL;
				//nID = *p++;		// nID not use -- Next signal ID

				//nFromTrack = *pInfo;
				BYTE nTrackID, nLastCheckTrackID;

				// Track
				RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
				p = pInfo + pRouteBasPtr->nOfsTrack;

				nCount = *p++;			
				for (j=0; j<nCount; j++) 
				{ 
					if (j<nCount-3 && *p)
					    nLastCheckTrackID = *p;
					if (j==0)
						nTrackID = nLastCheckTrackID;
					p++;
				}
			// Route
				nCount = *p++;

				nID = *p;

				// ISHUNT, ISTART와 같이 Route track이 없는 진로를 위해 보완....
				if (nCount == 0)
				{
					nID = nTrackID;
				}

				ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);

				if ( !pTrkInfo->ROUTE || m_SignalInfo.TM1 ) 
				{
					m_SignalInfo.DESTTRACK &= 0x7f;  // 0x80을 초기화 한다....

					if ( m_bIsYudo ) 
					{
						short bSon = !(m_wCheckResult & ROUTECHECK_SGOFF);

						if ( bSon && m_SignalInfo.TM1 == 1) 
						{
							SignalAspect(pInfo);

							if (m_SignalInfo.SOUT1 || m_SignalInfo.SOUT2 || m_SignalInfo.SOUT3 || m_SignalInfo.U)
							{
							    m_SignalInfo.ENABLED = 1;
							}

							//2013.3.27 bEmCancel 조건 추가함....
							if (bEmCancel)
							{
								m_SignalInfo.DESTTRACK |= 0x80;
							}
						}
						else 
						{
//							if (m_SignalInfo.SIN1 || m_SignalInfo.SIN2 || m_SignalInfo.SIN3 || m_SignalInfo.U)
							
							if ( m_SignalInfo.ENABLED )
							{
							    m_SignalInfo.TM1 = 0;
							}

							//2013.3.27 bEmCancel 조건 추가함....
							if ( (m_wCheckResult & ROUTECHECK_TRACK) || bEmCancel)
							{
								m_SignalInfo.DESTTRACK |= 0x80;
							}
							
							//2006.6.15  Free shunt signal의 경우 마주보는 signal의 aspect를 danger하기 위해...
							if (m_SignalInfo.U && (m_pTrackInfoTable[ pInfo[RT_OFFSET_FROM] ].bNOTRACK) )
							{
								ScrInfoSignal *pPrevSignal = (ScrInfoSignal *)int_get(SignalGet, pInfo[RT_OFFSET_PREVSIGNAL]);
								pPrevSignal->OSS = 0;
							}

 							SignalStop();
							//m_SignalInfo.ENABLED = 0;
						}
					}
					else 
					{
						if (m_wCheckResult & ROUTECHECK_SGOFF || !m_SignalInfo.TM1) 
						{
							// Outer home main 진로는 main home signal에 의존하므로 main home signal이 설정된 경우
							// 신호혀ㄴ시에 문제 없는 상황에서는 신호가 자동 복귀 되어야 한다...
							if ( ((m_pSignalInfoTable[nRID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME) && 
								 !(m_wCheckResult & ROUTECHECK_SGOFF) && !m_SignalInfo.TM1 )
							{
								BYTE nNextID = pInfo[ RT_OFFSET_PREVSIGNAL ];
								BYTE nNextAhead = GetSignalAspected( nNextID );
								if (nNextAhead > 1)
								{
									// 20150626 sjhong - Outer Home 신호기의 G, Y, YY 램프가 고장일 경우, 신호기가 핑퐁치는 현상에 대한 보완
									// Outer Home 신호기가 Home 신호기에 따라 난 경우, 램프 고장이 발생하면 Auto Recovery 하지 않는다.
									// 대신 Outer Home에 대한 Recovery 제어가 가능하도록 VDU에서 취급을 open함.
									// G현시, Y현시 Double Y현시 모두 SE2/SOUT2 출력이  있으므로 SE2/SOUT2가 있는지 체크함, Signal Stop의 경우 SE2/SOUT2가 0이 된다.
									// Double Y현시일 때 YY 램프가 고장일 경우 CLOR도 확인해야한다.
									if(m_SignalInfo.LM_LOR && m_SignalInfo.SE2 && m_SignalInfo.SOUT2 && (!m_SignalInfo.SE3 || m_SignalInfo.LM_CLOR))
									{
										SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
											
										if(!((pInfo[RT_OFFSET_LEVELCROSS] & FLAG_LC1) && (nSysVarExt.bLC1NKXPR || nSysVarExt.bLC1KXLR))
										&& !((pInfo[RT_OFFSET_LEVELCROSS] & FLAG_LC2) && (nSysVarExt.bLC2NKXPR || nSysVarExt.bLC2KXLR))
										&& !((pInfo[RT_OFFSET_LEVELCROSS] & FLAG_LC3) && (nSysVarExt.bLC3NKXPR || nSysVarExt.bLC3KXLR))
										&& !((pInfo[RT_OFFSET_LEVELCROSS] & FLAG_LC4) && (nSysVarExt.bLC4NKXPR || nSysVarExt.bLC4KXLR))
										&& !((pInfo[RT_OFFSET_LEVELCROSS] & FLAG_LC5) && (nSysVar.bLC5NKXPR || nSysVar.bLC5KXLR)))
										{
											m_SignalInfo.TM1 = 1;
											m_SignalInfo.DESTTRACK &= 0x7f;
										}
									}
								}
							}
														
							if ( (m_wCheckResult & ROUTECHECK_SIGNAL) || 
								(m_SignalInfo.SIN1 || m_SignalInfo.SIN2 || m_SignalInfo.SIN3 || m_SignalInfo.U) )
							{
								m_SignalInfo.TM1 = 0;
							}

							// LSM에서 비상 진로 해정 상태를 파악하기 위해 삽입함...
							//2013.3.5 bEmCancel 조건 추가함....
							if ( (m_wCheckResult & ROUTECHECK_TRACK) || bEmCancel)
							{	
								m_SignalInfo.DESTTRACK |= 0x80;
							}

							SignalStop();
							//if ( !(m_wCheckResult & ROUTECHECK_OSS) )  // 신호기 Clear 후 정지 시에는 Approach lock이 설정되어야 한다...
							    //m_SignalInfo.ENABLED = 0;  //신호기를 사용할 수 있음...
						}
					    // 신호를 현시해야 할 상황...
						else 
						{
						    SignalAspect(pInfo);

							if (m_SignalInfo.SOUT1 || m_SignalInfo.SOUT2 || m_SignalInfo.SOUT3 || m_SignalInfo.U)
							{
							    m_SignalInfo.ENABLED = 1;
							}

							//2013.3.27 bEmCancel 조건 추가함....
							if (bEmCancel)
							{
								m_SignalInfo.DESTTRACK |= 0x80;
							}
						}
					}

					// 2006.5.19  신호기가 신호를 현시할 경우에 Depend signal을 같이 현시한다.
					// Onroute에 있는 조건을 이곳으로 이동했다...
					if( !(m_SignalInfo.DESTTRACK & 0x80) )
					{
						BYTE nDependID = 0;
						ScrInfoSignal *pDependSigInfo = NULL;
						ScrInfoTrack *pDestTrkInfo = NULL;

						// 모든 중계신호기에 대하여 처리
						for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
						{
							// 중계 신호기가 없는 경우 무시한다.
							BOOL bDependSetOnly = (pInfo[cSigOffset] & SIG_DEPEND_SET_ONLY);
							BYTE nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);
							if(!nDependID)
							{
								continue;
							}

							// 신호기가 Outer Home 일 경우 무시한다
							if((m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME)
							{
								continue;
							}
									
							ScrInfoSignal *pDependSigInfo = (ScrInfoSignal *)int_get(SignalGet, nDependID);


							// 2006.5.19   main 진로는 Special processing ....
							BYTE nSignalAspectOK = 0;
 							if (/*nDepSignalType==SIGNALTYPE_ISHUNT &&*/ !m_bIsYudo && !bEmCancel)  // Depend signal이 Gshunt인 경우 처리가 되지 않으므로 수정..
 							{
 								ScrInfoTrack *pDestTrkInfo = (ScrInfoTrack *)int_get( TrackGet, (m_SignalInfo.DESTTRACK & 0x7f) );
 								if (!pDestTrkInfo->EMREL)
 								{
 									nSignalAspectOK = 1;
 								}
 							}


							if ( m_SignalInfo.SOUT1 || m_SignalInfo.SOUT2 || m_SignalInfo.SOUT3 || m_SignalInfo.U || nSignalAspectOK )
							{
								// 전철기 방향에 따라 알맞는 진로를 찾는 logic이 추가되어야 한다....
							 	WORD wDependRouteNo = FindPresetDependRoute(m_pSignalInfoTable[nDependID].nRouteCount, m_pSignalInfoTable[ nDependID ].wRouteNo);

								// 먼저 진로가 있더라도 새 진로로 변경....
								if(wDependRouteNo != (m_pRouteQue[nDependID] & ROUTE_MASK))
								{
									m_pRouteQue[nDependID] = wDependRouteNo | ROUTE_DEPENDSET;
									m_pOwnerRouteQue[nDependID] = (m_wRouteNo & ROUTE_MASK) | ROUTE_DEPENDSET;	// 20140423 sjhong - 주진로 번호 저장
									
									//pDependSigInfo->OOC = 1;
									pDependSigInfo->REQUEST = 1;
								}
								else
								{
									if(m_SignalInfo.TM1)
									{
										pDependSigInfo->TM1 = 1;
									}
								}
	
								// 2006.5.19 Shunt, Callon route 설정 시 따라 나는 진로 처리의 대항진로 설정 Check를 위해 삭제해야 한다...
								// 그러나 ISTART의 main signal의 경우에는 설정되지 않는다....
								if(!bDependSetOnly)
								{
									pDependSigInfo->OSS = 1;
								}
							}
							else if ( m_SignalInfo.TM1)
							{
								if (pDependSigInfo->OSS)
								{
									pDependSigInfo->OSS = 0;
									pDependSigInfo->DESTTRACK = 0;
									pDependSigInfo->DESTID = 0;
								}
							}
						}
					}
				}
				else 
				{
					if ( m_wCheckResult & ROUTECHECK_NOGREEN )  //진로궤도가 모두 Green일 경우에만 초기화 된다....
						m_SignalInfo.DESTTRACK |= 0x80;

					SignalStopU();
				}

				
				if ( bTrackGood == 0 ) //연동을 처리하지 않는다.
				{
					SetTkRequest(pInfo);

					m_pRouteQue[nRID] = m_wRouteNo;

					// 신호기 단심 검지 처리
					SignalAspectAdjustForLMR(pInfo);
					int_put(SignalPut);

					continue;
				}


                BOOL bRoute = pFromTrkInfo->ROUTE;   // 진로 설정시 0 

				BYTE nFirstSignalID = LoopCountH + 1;
				/*
				if (m_pTrackInfoTable[nFromTrack] & TI_NOTRACK) 
				{   
					pFromTrkInfo->TRACK = 0;
				}
				*/

	            BYTE nNextTrackID = 0;
                if (nCount > 1) {	// 진로내의 궤도가 한개 이상인 경우 2번째 진로궤도 
                    nNextTrackID = *(p+1);
                }
                else {  // 그렇지 않은 경우 종착 궤도를 대상으로한다. -- 보완를 위해 Coding 
					//2006.6.19  1개 궤도짜리 진로일 경우 상황에 따라 처리가 달라진다.... 
                    //nNextTrackID = *(pInfo + RT_OFFSET_TO);
                    nNextTrackID = 0;
                }

				ScrInfoTrack *pTrkInfoTo = NULL;
                if (nNextTrackID) 
            		pTrkInfoTo = (ScrInfoTrack *)int_get(TrackGet, nNextTrackID);

				// 단진로가 비상해정 시 ROUTE가 풀리지 않도록 조건 추가
                if ( !pFromTrkInfo->TRACK && pTrkInfo->TRACK && pTrkInfo->OVERLAP & !pTrkInfo->EMREL )
				{                    
                    if (nNextTrackID) 
					{
            		    //pTrkInfoTo = (ScrInfoTrack *)int_get(TrackGet, nNextTrackID);

						if (!pTrkInfoTo->TRACK && ((m_pTrackInfoTable[nFromTrack].bNOTRACK && m_pTrackInfoTable[nNextTrackID].bNOTRACK) || !m_pTrackInfoTable[nNextTrackID].bNOTRACK))
						{ 
							//2006.11.16  진로 속의 Shunt 진로 처리를 위해 보완.... 
							if ( !(m_bIsYudo && !pFromTrkInfo->ROUTE) )
							{
								pTrkInfo->ROUTE = 1;   
							}

                        }
						else 
						{
							pTrkInfo->OVERLAP = 0;
						}
					}
                }

                //BOOL bFirstTrackFree = pTrkInfo->ROUTE && !m_SignalInfo.ENABLED && !m_SignalInfo.ON_TIMER;
                //BOOL bFirstTrackFree = pTrkInfo->ROUTE && !m_SignalInfo.TM1 && !m_SignalInfo.ON_TIMER;
				
                m_bFirstTrackFree = (pTrkInfo->ROUTE || (!pTrkInfo->TRACK && !pFromTrkInfo->TRACK)) && 
									(!m_SignalInfo.TM1 || !m_SignalInfo.ENABLED) && !m_SignalInfo.ON_TIMER;
				BOOL bSecondTrackFree = FALSE;
				
				if (nNextTrackID)
				{
					/*
					// 2007.2.26  아카우라역 55, 57, 50 신호기의 Free ground shunt의 양방향 해정을 위해 삽입..
					// Data가 변경되면 수정 될 필요가 있다..   이것은 아카우라만 적용되어야 한다....
					if ((nRID==34 || nRID==35 || nRID==29 ) && bNextIsControl && bFirstTrackFree && bIsYudo)
					{
						pTrkInfo->ROUTE=1;
					}
					*/

                    bSecondTrackFree = !pTrkInfoTo->TRACK && pTrkInfo->ROUTE &&
											(!m_SignalInfo.TM1 || !m_SignalInfo.ENABLED) && !m_SignalInfo.ON_TIMER;
				}
				else				
                    bSecondTrackFree = pTrkInfo->ROUTE && (!m_SignalInfo.TM1 || !m_SignalInfo.ENABLED) && !m_SignalInfo.ON_TIMER;

/*
                bFirstTrackFree = (pTrkInfo->ROUTE || (!pTrkInfo->TRACK&& !pFromTrkInfo->TRACK)) && 
									(!m_SignalInfo.TM1) && !m_SignalInfo.ON_TIMER;
				BOOL bSecondTrackFree = FALSE;

				if (nNextTrackID)
                    bSecondTrackFree = !pTrkInfoTo->TRACK && pTrkInfo->ROUTE && 
											(!m_SignalInfo.TM1 ) && !m_SignalInfo.ON_TIMER;
				else				
                    bSecondTrackFree = pTrkInfo->ROUTE && (!m_SignalInfo.TM1 ) && !m_SignalInfo.ON_TIMER;
*/				
                if ( invoke ) 
				{
                    BOOL bReroute = TRUE;
                    BYTE nTrackFromID, nTrackToID;
                    nTrackFromID = pInfo[ RT_OFFSET_FROM ];
                    nTrackToID = pInfo[ RT_OFFSET_TO ];

					/*	m_pDestTrack은 구내 신호기의 구내 구분 진로를 위해 사용 해 왔는데, 
					    Signal, Point의 경보 해정을 위해 사용하고, 만약 위와 같이 사용 할 경우 
						다시 정의 하여 사용해야 한다...
					if ( pTrkInfo->TRACK )
						m_pDestTrack[ nFirstSignalID ] = 0;
					*/

					if ( !pFromTrkInfo->TRACK && !pTrkInfo->TRACK ) 
					{
					    SignalStopU(); // 신호기를 정지 상태로 만든다...

						// 2005.09.22   Advnced starter signal의 경우 Block 출발 정보 (red arrow)를 설정하기 위해 depart 정보를 만든다...
						// 아잠푸르역의 BYpass역 정보 설정은 추후 고려....
						BYTE nSigType = m_pSignalInfoTable[ LoopCountH + 1 ].nSignalType;
						BYTE nDir = (nSigType & SIGNALTYPE_DIR_BIT) >> 2;  // Left Direction-> 1
						nSigType &= SIGNAL_MASK;
        
						// 아래의 설정은 샤하지	바자 1-43 main진로 설정 후 2T, W2122T 점유 후 비상진로 해정 시 Outer main진로가 해정되지 않음.. 
						//m_SignalInfo.ENABLED = 0;  // Approach locking 설정 상태를 해제한다...
						
						// 2007.7.18  비상해정 시 Overlap이 다시 설정되는 현상때문에 추가
						// 20140404 sjhong - 연속 점유 조건(OVERLAP) 설정 시 DISTURB를 체크하도록 함.
						if((nEmergencyStop[nID] == FALSE) && pFromTrkInfo->OVERLAP && (pTrkInfo->EMREL == FALSE) && (pTrkInfo->ROUTE == FALSE) && (pTrkInfo->DISTURB == TRUE))
						{
						    pTrkInfo->OVERLAP = 1;
						}

						// m_pDestTrack[ nFirstSignalID ] = nTrackToID | 0x80;

/*
						if (pTrkInfo->ROUTE && !m_SignalInfo.TM1 && !m_SignalInfo.ON_TIMER)
						{
							pTrkInfo->ClearRoute();
							m_SignalInfo.ENABLED = 0;
						}
*/
						// 열차 도착 정보 설정...   
					}


					ScrInfoTrack *pDestTrack = NULL;
					pDestTrack = (ScrInfoTrack *)int_get (TrackGet, pInfo[RT_OFFSET_TO]);

					// 20140424 sjhong - ERR 등 조건에 따른 진로 해정 처리 정리
                    if(bSecondTrackFree || (bEmCancel && !pDestTrack->EMREL))  
					{ 
						BYTE nPrevSigID = pInfo[RT_OFFSET_PREVSIGNAL];

						m_SignalInfo.FREE = 1;

						if(bEmCancel)	// ERR의 경우
						{
							m_wRouteNo = 0;	// 진로 큐에서 삭제......

							if(!(m_pRouteQue[nRID] & ROUTE_DEPENDSET))
							{
								m_pOwnerRouteQue[nPrevSigID] = 0;
							}

						    m_SignalInfo.DESTID	= 0;
						}
						else
						{
							m_wRouteNo = 0;	// 진로 큐에서 삭제......

							// Outer Home Depend signal이 존재하면 이 진로도 해정 해 주어야 한다...
							if(nDependSigID && ((nDepSignalType & SIGNAL_MASK) == SIGNALTYPE_OHOME))
							{
								m_pRouteQue[nDependSigID] |= ROUTE_CANCEL;				
							}

							m_SignalInfo.DESTID	= ( m_SignalInfo.DESTID & 0x40 ) ? 0x40 : 0;
						}

						m_SignalInfo.DESTTRACK = 0;

						m_SignalInfo.ENABLED = 0;
					    m_SignalInfo.TM1 = 0;

						m_SignalInfo.OSS = 0;
					    m_SignalInfo.CS = 0;
                    }
                    else if(!m_bIsInBlock)
					{
						//if (m_SignalInfo.ENABLED)
						if ( !m_bFirstTrackFree )
						{
							for (j=0; j<nCount; j++) 
							{
								nID = *p++;
								ScrInfoTrack *pTrackInfo = (ScrInfoTrack *)int_get(TrackGet, nID);

								// 신호기가 현시 가능한 상태임..
								if (!(m_wCheckResult & ROUTECHECK_SGOFF_OSS)) 
								{
									if ( m_wRouteNo ) // 진로궤도에 진로쇄정 정보를 설정...
									{  
										pTrackInfo->ROUTE = 0;
									}

									// 진로궤도에 진로요구정보를 clear시킨다. --> 진로쇄정 정보가 설정되었으므로.. 
									pTrackInfo->TKREQUEST = 0;
									pTrackInfo->RouteNo = 0;
								}
							}

							bReroute = FALSE;

							//신호가 현시되지 않은 상황에서 진입궤도 점유 후 진로 첫궤도 점유되고, 진로 첫 궤도가 다시 해정 될 때 
							//그 이후 신호를 다시 현시 해 주기 위해 삽입....
							if (!m_SignalInfo.ENABLED)
							    m_SignalInfo.TM1 = 1;


							if (nSignalType == SIGNALTYPE_ASTART)
							{
								if (m_SignalInfo.SIN1 || m_SignalInfo.SIN2)
								{
								    nAstartOn = 1;
								}
							}
						}
						// 신호가 현시되지 않았더라도, 진입궤도와 진로 첫궤도가 동시에 점유되면 초록색 해정 시키기 위해 삽입.... 
						else
						{
							/*
							if (bIsYudo && (m_pSignalInfoTable[nDependSigID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_ISHUNT)
							{
							}
							else
							*/
							    m_SignalInfo.TM1 = 0;
						}

						if (bEmCancel)
							SignalStop();							
				    }


					if(bEmCancel)
					{
						// OHOME 진로 처리
						// 20140214 sjhong - 중계 신호기로 설정된 진로가 아니면 Emergency Route Release에서도 제외한다.
						if(nDependSigID && (nDepSignalType == SIGNALTYPE_OHOME) && (m_pRouteQue[nDependSigID] & ROUTE_DEPENDSET) && !pFromTrkInfo->EMREL)
						{
							ScrInfoSignal *pDepSigInfo = (ScrInfoSignal *)int_get (SignalGet, nDependSigID);
							if (pDepSigInfo->REQUEST)
							{
								m_pRouteQue[nDependSigID] |= ROUTE_CANCEL;
							}
							else
							{
								char pMsg[8];

								pMsg[1] = WS_OPMSG_FREESUBTRACK;
								pMsg[2] = nFromTrack;
								pMsg[3] = TRACKLOCK_FREE;
								pMsg[4] = 0x79;  // Emer count not increase

								Operate( pMsg );
							}

							// 20140215 sjhong - Emergency Route Release 시 진로의 진로큐에서 삭제를 TRACK에 대한 EMREL이 설정된 이후에 한다.
							m_pRouteQue[nDependSigID] = 0;	// 진로 큐에서 삭제......
							m_pOwnerRouteQue[nDependSigID] = 0;	// 20140423 sjhong - 주진로 정보를 삭제함.

							ScrInfoSignal *pDependInfo = (ScrInfoSignal *)int_get (SignalGet, nDependSigID);
							
							pDependInfo->DESTID	= 0;
							pDependInfo->DESTTRACK = 0;
							
							pDependInfo->ENABLED = 0;
							pDependInfo->TM1 = 0;
							
							pDependInfo->OSS = 0;
							pDependInfo->CS = 0;
						}
					}
                }
			} //End of 진로 처리 flag 처리 (INVOKE, ONPROCESS 등....)

			SetTkRequest(pInfo);
		} // End of 진로가 설정요구중이거나, 접근쇄정 중...
        else 
		{      // Off Signal Process
		    BYTE *pSigId = &m_pSignalInfoTable[ LoopCountH + 1 ].nSignalType;		
		    BYTE nSigType = *pSigId++;

		    BYTE nBlockType = nSigType & 0x0f0;
			BYTE nBlockArriveTKID = 0;
			BYTE nBlockTKID[MAX_BLKTRACK_QTY]={0};
			BYTE nStartSigID[MAX_BLKPREVSIG_QTY]={0};
			BYTE nBlkTkCount = 0;
			BYTE nBlkSigCount = 0;

			BYTE nDir = (m_pSignalInfoTable[ LoopCountH + 1 ].nSignalType & SIGNALTYPE_DIR_BIT) >> 2;  // Left Direction-> 1
			BYTE nBlockDir;

			ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;

			if (nSignalType == SIGNALTYPE_ASTART)
			{
				nAstartOn = 2;
			}

			// Token block - Token당, Going(Block inter out), Coming(Block inter in), Depart(Block inter out)의 3정보로 정의한다...
			// 정보는 Going의 경우 LCRIN, DEPART정보로 수진하며, Coming의 경우 LCR정보로 수신한다....		
		    if (nBlockType == BLOCKTYPE_INTER_IN || nBlockType == BLOCKTYPE_INTER_OUT || nBlockType == BLOCKTYPE_AUTO_IN || nBlockType == BLOCKTYPE_AUTO_OUT) 
			{
            	m_wRouteNo = m_pSignalInfoTable[ LoopCountH + 1 ].wRouteNo;
		        nInfoPtr = *(DWORD*)&m_pRouteAddTable[ m_wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
		        pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
				
				nBlockDir = pInfo[RT_OFFSET_BLKDIR];

				p = pInfo + RT_OFFSET_BLKARRIVETK;
				nBlockArriveTKID = *p;

				p = pInfo + RT_OFFSET_BLKTRACK1;

				for(int i = 0 ; i < MAX_BLKTRACK_QTY ; i++)
				{
					nBlockTKID[i] = *p++;
					if(nBlockTKID[i] != 0)
					{
						++nBlkTkCount;
					}
				}

				p = pInfo + RT_OFFSET_BLKPREVSIG1;

				for(i = 0 ; i < MAX_BLKPREVSIG_QTY ; i++)
				{
					nStartSigID[i] = *p++;  // 대항신호기를 설정한다....
					if(nStartSigID[i] != 0)
					{
						++nBlkSigCount;
					}
				}
			}			

// Token TCB......
		    if (nBlockType == BLOCKTYPE_INTER_IN) //TCB
			{
				BYTE nApproachTKID = pInfo[RT_OFFSET_BLKTRACK1];
				ScrInfoTrack *pApproachTKInfo = (ScrInfoTrack *)int_get(TrackGet, nApproachTKID);

				if (pBlkInfo->LCR==TRUE)
				{
					pBlkInfo->CA = TRUE;
					pBlkInfo->LCRIN = TRUE;
					pBlkInfo->LCROP = TRUE;
					pBlkInfo->CONFIRM = TRUE;

					pBlkInfo->COMPLETE = FALSE;
					pBlkInfo->COMPLETEIN = FALSE;

					if (!pApproachTKInfo->TRACK)
					{
						// TOLR 정보를 실선으로 받지 못하는 경우 열차가 접근궤도에 진입할때 화살표를 RED로 변하게 함.
						pBlkInfo->DEPARTIN = TRUE;
						
						// BLOCK APPROACH ALARM을 설정한다.
						if(!(m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH))
						{
							m_pSignalFail[nRID] = BLOCK_ALARM_APPROACH | BLOCK_ALARM_APPROACH_TIME;
						}
					}
					else if(pBlkInfo->DEPARTIN) // 인접역에서 열차가 폐색으로 진입한 경우
					{
						// BLOCK DEPART ALARM을 설정한다. (TOLR로 인해 DEPARTIN이 접근 궤도 점유보다 먼저 발생할 경우에만 설정된다.)
						if(!(m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH) && !(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTIN))
						{
							m_pSignalFail[nRID] = BLOCK_ALARM_DEPARTIN | BLOCK_ALARM_DEPART_TIME;  
						}

						// TOKEN BLOCK의 경우에도 ARRIVE 처리가 필요하다.
// 						BYTE *nLastBlockTKID = &nBlockTKID[nBlkTkCount-1];
// 						
// 						BOOL bDirMatch = FALSE;
// 						ScrInfoTrack *pBlkArriveTK = (ScrInfoTrack *)int_get(TrackGet, nBlockArriveTKID);
// 						
// 						BYTE nPrevTrackID = m_pTrackPrev[nBlockArriveTKID];
// 						
// 						if ( (nDir == 0) && (pBlkArriveTK->ROUTEDIR) && (nPrevTrackID == *nLastBlockTKID))
// 							bDirMatch = TRUE;
// 						else if ( (nDir != 0) && !(pBlkArriveTK->ROUTEDIR) && (nPrevTrackID == *nLastBlockTKID))
// 							bDirMatch = TRUE;
// 						
// 						if ( bDirMatch && !pBlkArriveTK->ROUTE && pBlkInfo->DEPARTIN) 
// 						{
// 							if ( !pBlkArriveTK->TRACK ) 
// 							{
// 								ScrInfoTrack *pLastTKInfo = (ScrInfoTrack *)int_get(TrackGet, *nLastBlockTKID);
// 								if ( pLastTKInfo->TRACK )
// 								{
// 									if (pBlkInfo->CONFIRM)
// 									{
// 										// 적색 TCB 화살표가 점멸하게 된다.
// 										pBlkInfo->ARRIVE = TRUE;
// 									}
// 									
// 								}
// 							}
// 						}
					}
				}
				else
				{
					// TOKEN 취급 없이 열차가 접근궤도로 진입한 경우 
					if (!pApproachTKInfo->TRACK)
					{
						// 접근 궤도가 점유가 되었어도 TGB의 열차 출발의 경우에는 경보 설정을 하지 않는다.... (단선일 경우)
						BYTE nProhibitBlkID = pInfo[RT_OFFSET_PROHIBITBLK];
						ScrInfoBlock *pProhibitBlk = (ScrInfoBlock *)int_get(SignalGet, nProhibitBlkID);
						if (!pProhibitBlk->DEPART)
						{
							// BLOCK APPROACH ALARM을 설정한다.
							if(!(m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH))
							{
								m_pSignalFail[nRID] = BLOCK_ALARM_APPROACH | BLOCK_ALARM_APPROACH_TIME;
							}
						}
					}
					else
					{
						// TOKEN 취급도 없고, 접근 궤도 점유도 아니므로 알람을 끈다.
						if((m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH) || (m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTIN))
						{
							m_pSignalFail[nRID] = 0; 
						}
					}

					pBlkInfo->CA = FALSE;
					pBlkInfo->LCRIN = FALSE;
					pBlkInfo->LCROP = FALSE;
					pBlkInfo->DEPARTIN = FALSE;
					pBlkInfo->ARRIVE = FALSE;	// ARRIVE 처리를 해주었으므로 ARRIVE도 초기화 해야한다.

					pBlkInfo->CONFIRM = TRUE;
					pBlkInfo->COMPLETE = TRUE;
					pBlkInfo->COMPLETEIN = TRUE;
				}

				int_put(SignalPut);
				continue;
			}

// Token TGB......
		    else if (nBlockType == BLOCKTYPE_INTER_OUT) //TGB
			{
				if (pBlkInfo->LCRIN==TRUE)
				{
					pBlkInfo->CA = TRUE;
					pBlkInfo->LCR = TRUE;
					pBlkInfo->LCROP = TRUE;

					pBlkInfo->COMPLETE = FALSE;
					pBlkInfo->COMPLETEIN = FALSE;

					BOOL bDirMatch = FALSE;
					ScrInfoTrack *pBlkStartTk = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[0]);
					
					if ( nDir != 0 && pBlkStartTk->ROUTEDIR)
						bDirMatch = TRUE;
					else if ( nDir == 0 && !pBlkStartTk->ROUTEDIR)
						bDirMatch = TRUE;
					
					if(!pBlkStartTk->ROUTE && bDirMatch)
					{
						if(pBlkStartTk->TRACK == FALSE )
						{
							pBlkInfo->DEPART = TRUE;

							if(!(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTOUT))
							{
								m_pSignalFail[nRID] = BLOCK_ALARM_DEPARTOUT | BLOCK_ALARM_DEPART_TIME;
							}
						}
					}
/*
					if (pBlkInfo->CBBOP)   // 열차가 출발하여 TOLPR정보가 있는 경우....
					{
						pBlkInfo->DEPART = TRUE;
					}
					*/
				}
				else
				{
					pBlkInfo->CA = FALSE;
					pBlkInfo->LCR = FALSE;
					pBlkInfo->LCROP = FALSE;
					pBlkInfo->DEPART = FALSE;

					pBlkInfo->COMPLETE = TRUE;
					pBlkInfo->COMPLETEIN = TRUE;
					pBlkInfo->ASTARTSTICK = FALSE;

					if(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTOUT)
					{
						m_pSignalFail[nRID] = 0;
					}
				}

				int_put(SignalPut);
				continue;
			}

// TCB......
		    else if (nBlockType == BLOCKTYPE_AUTO_IN) //TCB
			{
				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					if(!pBlkInfo->SWEEPING && pBlkInfo->SWEEPINGACK)
					{
						pBlkInfo->SWEEPINGACK = FALSE;
						pBlkInfo->BLKRSTREQ = FALSE;
						pBlkInfo->BLKRSTACC = FALSE;
					}
				}

				// 20190320 sjhong - 폐색장치 모드가 아닐 경우에만 설정.
				if(!m_ConfigList.BLK_DEV_MODE)
				{
					// 20150104 sjhong - PBY역의 AX1OCC 정보를 MLKII로 전달할때 사용하는 출력 채널에 대한 값을 수동 설정하도록 변경
					if(pBlkInfo->AXLOCC)
					{
						pBlkInfo->AXLOCCDOUT = 1;
					}
					else
					{
						pBlkInfo->AXLOCCDOUT = 0;
					}
				}

				if(pBlkInfo->SAXLACTIN)
				{
					if(pBlkInfo->SAXLREQ)
					{
						if(pBlkInfo->SAXLOCC && pBlkInfo->SAXLDST)
						{
							pBlkInfo->SAXLREQ = 0;
						}

						if(pBlkInfo->SAXLACC)
						{
							pBlkInfo->SAXLRST = 1;
							//pBlkInfo->SAXLREQ = 0;
							
							DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;
							
							if(nDir)
							{
#ifdef _WINDOWS
								WORD wCounter = pHead->Counter[B2AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B2AXLCounter] = wCounter;
#endif
								pHead->Counter[B2AXLCounter]++;
								if (pHead->Counter[B2AXLCounter] == 10000)
									pHead->Counter[B2AXLCounter] = 0;
#ifdef _WINDOWS
								wCounter = pHead->Counter[B2AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B2AXLCounter] = wCounter;
#endif
							}
							else
							{
#ifdef _WINDOWS
								WORD wCounter = pHead->Counter[B4AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B4AXLCounter] = wCounter;
#endif
								pHead->Counter[B4AXLCounter]++;
								if (pHead->Counter[B4AXLCounter] == 10000)
									pHead->Counter[B4AXLCounter] = 0;
#ifdef _WINDOWS
								wCounter = pHead->Counter[B4AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B4AXLCounter] = wCounter;
#endif
							}
						}
						else if(pBlkInfo->SAXLDEC)
						{
							pBlkInfo->SAXLREQ = 0;
						}
					}
					
					// Axle Counter Reset Output Control (keep 5 seconds only) 
					if (pBlkInfo->SAXLRST)
					{
						if(nDir)
						{
							if(_Time1Sec == 0)
							{
								m_nB2AXLRSTTimer++;
							}
							
							if(m_nB2AXLRSTTimer > 5)
							{
								m_nB2AXLRSTTimer = 0;
								
								pBlkInfo->SAXLRST = 0;
							}
						}
						else
						{
							if(_Time1Sec == 0)
							{
								m_nB4AXLRSTTimer++;
							}
							
							if(m_nB4AXLRSTTimer > 5)
							{
								m_nB4AXLRSTTimer = 0;
								
								pBlkInfo->SAXLRST = 0;
							}
						}
					}
				}
				else
				{
					if(pBlkInfo->AXLOCC && pBlkInfo->AXLDST)
					{
						pBlkInfo->AXLREQ = 0;
					}

					if(pBlkInfo->AXLREQ)
					{
						if(pBlkInfo->AXLACC)
						{
							pBlkInfo->AXLRST = 1;
							//pBlkInfo->AXLREQ = 0;
							
							DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;

							if(nDir)
							{
#ifdef _WINDOWS
								WORD wCounter = pHead->Counter[B2AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B2AXLCounter] = wCounter;
#endif
								pHead->Counter[B2AXLCounter]++;
								if (pHead->Counter[B2AXLCounter] == 10000)
									pHead->Counter[B2AXLCounter] = 0;
#ifdef _WINDOWS
								wCounter = pHead->Counter[B2AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B2AXLCounter] = wCounter;
#endif
							}
							else
							{
#ifdef _WINDOWS
								WORD wCounter = pHead->Counter[B4AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B4AXLCounter] = wCounter;
#endif
								pHead->Counter[B4AXLCounter]++;
								if (pHead->Counter[B4AXLCounter] == 10000)
									pHead->Counter[B4AXLCounter] = 0;
#ifdef _WINDOWS
								wCounter = pHead->Counter[B4AXLCounter];
								wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
								pHead->Counter[B4AXLCounter] = wCounter;
#endif
							}
						}
						else if(pBlkInfo->AXLDEC)
						{
							pBlkInfo->AXLREQ = 0;
						}
					}

					// Axle Counter Reset Output Control (keep 5 seconds only) 
					if (pBlkInfo->AXLRST)
					{
						if(nDir)
						{
							if(_Time1Sec == 0)
							{
								m_nB2AXLRSTTimer++;
							}
							
							if(m_nB2AXLRSTTimer > 5)
							{
								m_nB2AXLRSTTimer = 0;
								
								pBlkInfo->AXLRST = 0;
							}
						}
						else
						{
							if(_Time1Sec == 0)
							{
								m_nB4AXLRSTTimer++;
							}
							
							if(m_nB4AXLRSTTimer > 5)
							{
								m_nB4AXLRSTTimer = 0;
								
								pBlkInfo->AXLRST = 0;
							}
						}
					}
				}

				// Block Reset Request 제어를 낸 경우, Accept나 Decline을 받으면 Request를 0으로 만든다.
				if(pBlkInfo->BLKRSTREQ)
 				{
					if(!m_ConfigList.BLK_TRAIN_SWEEP)
					{
	 					if(pBlkInfo->BLKRSTACCIN)
	 					{
	 						pBlkInfo->BLKRSTREQ = 0;
	 						
	 						// TCB Clear
	 						if(pBlkInfo->RBCPR || !pBlkInfo->NBCPR)
	 						{
								pBlkInfo->CAACK		= 0;
								pBlkInfo->LCR		= 0;
								pBlkInfo->ARRIVE	= 0;
								pBlkInfo->COMPLETE	= 1;

	 							pBlkInfo->RBCR = 0;
	 							pBlkInfo->NBCR = 1;  
	 						}
	 					}
	 				}

					if(pBlkInfo->BLKRSTDECIN)
 					{
 						pBlkInfo->BLKRSTREQ = 0;
 					}
				}
			
		
				// Block Reset Request 제어가 없을 때(요청이 Clear 됐을 때) 항상 Accept와 Decline 을 0으로 만든다.
				if(!pBlkInfo->BLKRSTREQIN)
				{
					pBlkInfo->BLKRSTACC = 0;
					pBlkInfo->BLKRSTDEC = 0;
				}
				
				// 20131010 sjhong - TCB에서 열차 도착 시 CA가 자동으로 켜지는 현상 차단. (CA 종속성 제거)
				//if (pBlkInfo->ARRIVE == TRUE)
				//	pBlkInfo->CA = TRUE;

//				if (pBlkInfo->CA == FALSE)
//					pBlkInfo->COMPLETE = TRUE;

				if (pBlkInfo->LCRIN == TRUE && pBlkInfo->COMPLETE == TRUE)
				{
					pBlkInfo->COMPLETE = FALSE;
				}

				BYTE nConfirm = TRUE;
				BYTE nSigConfirm = TRUE;

				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					ScrInfoTrack *pBlkStartTk = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[0]);
					
					if(((pBlkInfo->BLKRSTREQ && pBlkInfo->BLKRSTACCIN) || (pBlkInfo->BLKRSTREQIN && pBlkInfo->BLKRSTACC)) && pBlkInfo->SWEEPING)
					{
						if(!pBlkStartTk->TRACK)
						{
							pBlkStartTk->BLKSWEEP = TRUE;
						}
					}
	
					ScrInfoTrack *pBlockTKInfo = pBlkStartTk;
					ScrInfoTrack *pPrevTKInfo = NULL;
					
					for(int i = 1 ; i < nBlkTkCount ; i++)
					{
						pPrevTKInfo = pBlockTKInfo;
						pBlockTKInfo = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[i]);
						
						if(!pBlockTKInfo->TRACK)
						{
							if(pPrevTKInfo->BLKSWEEP)
							{
								pBlockTKInfo->BLKSWEEP = TRUE;
							}
						}
						
						if(pPrevTKInfo->TRACK)
						{
							pPrevTKInfo->BLKSWEEP = FALSE;
						}
					}
				}
				
				for(int i = 0 ; i < nBlkTkCount ; i++)
				{
					ScrInfoTrack *pBlockTKInfo = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[i]);
					if (pBlockTKInfo->TRACK == FALSE)
					{
						nConfirm = FALSE;
					}
				}

				// 2009.04.22 이리콘 T&C 시 요청사항에 따라 Home, Outerhome signal Dark시 LCR 취급이 되지않도록 조건 추가....
				for(i = 0 ; i < nBlkSigCount ; i++)
				{
					ScrInfoSignal *pStartSigInfo = (ScrInfoSignal *)int_get(SignalGet, nStartSigID[i]);
					if (pStartSigInfo->LM_LOR == FALSE)
					{
						nSigConfirm = FALSE;
					}
				}
				
				pBlkInfo->CONFIRM = nConfirm;

				if (pBlkInfo->LCRIN == TRUE && pBlkInfo->LCROP == TRUE /*&& pBlkInfo->CONFIRM == TRUE*/ && nSigConfirm)
				{
					pBlkInfo->LCR = TRUE;
				}
	
				BYTE *nLastBlockTKID = &nBlockTKID[nBlkTkCount-1];

				BYTE nProhibitBlkID = pInfo[RT_OFFSET_PROHIBITBLK];
				ScrInfoBlock *pProhibitBlk = NULL;

				if (nDir)  // Left direction -> B2
				{
					if ( nBlockDir & BLOCKTYPE_DIR_MID )
						m_nB12ID = LoopCountH + 1;
					else
						m_nB2ID = LoopCountH + 1;
				}
				else	// Right direction -> B4
				{
					if ( nBlockDir & BLOCKTYPE_DIR_MID )
						m_nB14ID = LoopCountH + 1;
					else
						m_nB4ID = LoopCountH + 1;
				} 

				BOOL bDirMatch = FALSE;
				BYTE nPrevTrackID = m_pTrackPrev[nBlockArriveTKID];
				ScrInfoTrack *pBlkArriveTK = (ScrInfoTrack *)int_get(TrackGet, nBlockArriveTKID);
				ScrInfoTrack *pLastTKInfo = (ScrInfoTrack *)int_get(TrackGet, *nLastBlockTKID);

				if ( (nDir == 0) && (pBlkArriveTK->ROUTEDIR) && (nPrevTrackID == *nLastBlockTKID))
					bDirMatch = TRUE;
				else if ( (nDir != 0) && !(pBlkArriveTK->ROUTEDIR) && (nPrevTrackID == *nLastBlockTKID))
					bDirMatch = TRUE;

				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					if (!pBlkArriveTK->TRACK)
					{
						if(pLastTKInfo->TRACK && ((!pBlkInfo->SAXLACTIN && pBlkInfo->AXLOCC) || (pBlkInfo->SAXLACTIN && pBlkInfo->SAXLOCC)))
						{
							if(pLastTKInfo->BLKSWEEP)
							{
								pLastTKInfo->BLKSWEEP = FALSE;
	
								if(pBlkInfo->SWEEPING)
								{
									pBlkInfo->BLKRSTREQ	= FALSE;
									pBlkInfo->BLKRSTACC = FALSE;
										
									pBlkInfo->SWEEPINGACK = TRUE;
										
									if(pBlkInfo->RBCPR)
									{
										pBlkInfo->RBCR = 0;  
										pBlkInfo->NBCR = 1;
									}
								}
							}
						}
					}
				}
				
				if (bDirMatch && !pBlkArriveTK->ROUTE && !pBlkArriveTK->TRACK)
				{
					if(pLastTKInfo->TRACK)
					{
						//2009.04.22 이리콘과 Block T&C 중 pBlkInfo->CONFIRM 조건 삽입하여 수정함....
						if (pBlkInfo->CONFIRM && pBlkInfo->DEPARTIN)
						{
						    pBlkInfo->ARRIVE = TRUE;
						}
						else if(pLastTKInfo->BLKTRACE)	// Block에서 작업하고 돌아 오는 열차... (퇴행) 
						{
							ScrInfoBlock *pBlockTGB = (ScrInfoBlock *)int_get(SignalGet, nProhibitBlkID);
							if (pBlockTGB->DEPART && pBlockTGB->LCRIN && !pBlockTGB->COMPLETE)
							{
								// 도착 처리
								pBlkInfo->ARRIVE = TRUE;
								
								if(pBlockTGB->CA)
								{
									pBlkInfo->CAACK = TRUE;
								}
								pBlkInfo->LCR = TRUE;
								pBlkInfo->COMPLETE = FALSE;
								
								if(!pBlkInfo->RBCPR)
								{
									pBlkInfo->RBCR = 1;  
									pBlkInfo->NBCR = 0;
								}

								pLastTKInfo->BLKTRACE = 0;
							}
						}
					}
				}
				else if(m_ConfigList.BLK_DEV_MODE)
				{
					if(pBlkInfo->LCR && pBlkInfo->LCRIN && pBlkInfo->DEPARTIN)
					{
						if(!pLastTKInfo->TRACK && pBlkArriveTK->TRACK)
						{
							pLastTKInfo->BLKTRACE = TRUE;
						}
						else if(pLastTKInfo->BLKTRACE && !pLastTKInfo->TRACK && !pBlkArriveTK->TRACK)
						{
							pLastTKInfo->BLKTRACE = FALSE;
							pBlkArriveTK->BLKTRACE = TRUE;
						}
						else if(pBlkArriveTK->BLKTRACE && pLastTKInfo->TRACK && !pBlkArriveTK->TRACK)
						{
							//pBlkArriveTK->BLKTRACE = FALSE;
							//pBlkInfo->ARRIVE = TRUE;
						}
						else if(pBlkArriveTK->BLKTRACE && pLastTKInfo->TRACK && pBlkArriveTK->TRACK)
						{
							pBlkArriveTK->BLKTRACE = FALSE;
							pBlkInfo->ARRIVE = TRUE;
						}
					}
				}

				// 진로DB를 가지고 있는 경우에만 처리한다...
				if ( m_wRouteNo )
				{
					// Approach track 조사...  
					BYTE nApproachTKID = pInfo[RT_OFFSET_BLKTRACK1];
					ScrInfoTrack *pApproachTKInfo = (ScrInfoTrack *)int_get(TrackGet, nApproachTKID);

					pProhibitBlk = (ScrInfoBlock *)int_get(SignalGet, nProhibitBlkID);

					if (!pApproachTKInfo->TRACK) // 점유..
					{
						// 점유가 되었어도 TGB의 열차 출발의 경우에는 경보 설정을 하지 않는다.... (단선일 경우)
						if(!nProhibitBlkID || !pProhibitBlk->DEPART)
						{
							// 폐색 수속하고 출발하는 열차가 아닐 경우 접근 경보 설정.
							if(!(m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH))
							{
								m_pSignalFail[nRID] = BLOCK_ALARM_APPROACH | BLOCK_ALARM_APPROACH_TIME;   
							}
						}
					}
					else
					{
						// 폐색 취급이 정상적으로 이루어지지 않은 열차가 진입할 경우 ALARM CLEAR 시점이 여기밖에 없다.
						if(!pBlkInfo->DEPARTIN)
						{
							if((m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH) || (m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTIN))
							{
								m_pSignalFail[nRID] = 0;   
							}
						}
					}
				}
				else
				{
					pBlkInfo->CONFIRM = TRUE;
				}

				if (pBlkInfo->DEPARTIN)  // 열차가 반대편 역에서 출발 했다는 정보를 받음.. 
				{
					/*
					if (pBlkInfo->ARRIVETR && pBlkInfo->CBBOP) // 열차가 도착한 후에만  폐색 취소제어가 가능하다..
					{
						pBlkInfo->CBB = TRUE;
						pBlkInfo->CA = FALSE;
						pBlkInfo->COMPLETE = TRUE;
					}
					*/

					// CBI가 Down되었다가 Up되었을 때를 대비하여 제어한다. --> 열차 폐색으로 출발...
					// RBCR이 제어되면, TCB와 TGB의 CA 및 LCR이 취급되지 않는다.
					if (pBlkInfo->NBCPR && (!pBlkInfo->CBBOP && !pBlkInfo->CBBIN))
					{
						pBlkInfo->RBCR = 1;  // CBI Down시를 대비하여 전역에서 열차가 출발 할 경우 열차가 오고 있음을 설정한다... 
						pBlkInfo->NBCR = 0;
					}

					if (!(m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH) && !(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTIN))
					{
					    m_pSignalFail[nRID] = BLOCK_ALARM_DEPARTIN | BLOCK_ALARM_DEPART_TIME;
					}
				}
				else	// DEPARTIN = 0 인 경우
				{
					// 2005.3.11 추가... 페색 궤도가 점유 되었을 경우 허용하지 않는다...
					if (!pBlkInfo->CONFIRM && !pBlkInfo->LCR)   // 2009.4.22 이리콘 T&C 시 요청사항에 따라 !pBlkInfo->LCR 조건 추가....
					{
						pBlkInfo->LCR = FALSE;
						pBlkInfo->LCROP = FALSE;
					}

					// 인접역에서 열차가 폐색으로 진입했다가 퇴행한 경우 BCR을 해정한다.
					if(!pBlkInfo->ARRIVE && pBlkInfo->COMPLETEIN && !pBlkInfo->LCRIN && pBlkInfo->LCR)
					{
						pBlkInfo->COMPLETE = TRUE;

						if (pBlkInfo->RBCPR && (!pBlkInfo->CBBOP && !pBlkInfo->CBBIN))
						{
							pBlkInfo->RBCR = FALSE; 
							pBlkInfo->NBCR = TRUE;
						}
					}
				}

				// 열차의 도착에 관계없이 폐색 취소가 가능하게 설정....
				// 2005.09.23  열차가 도착해야만 취소 할 수 있도록 다시 수정함 ...  pBlkInfo->LCRIN 삭제.
				// 2005.10.07  TGB로 부터 LCRIN을 받고 LCR취급을 안한 상태에서는 CBB로 취소 할 수 있다. --> 계약서 반영 
				// 2013.02.19 AXLOCC 조건 추가
				if(((pBlkInfo->LCRIN && !pBlkInfo->LCR) ||  pBlkInfo->ARRIVE) && pBlkInfo->CBBOP 
				&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
				{
					pBlkInfo->CBB = TRUE;
					pBlkInfo->CA = FALSE;
					pBlkInfo->COMPLETE = TRUE;
					pBlkInfo->ARRIVE = FALSE;
					pBlkInfo->CAACK = FALSE;

					// 페색 계전기를 정상적으로 복귀...
					if (pBlkInfo->RBCPR == TRUE || pBlkInfo->NBCPR == FALSE)
					{
						pBlkInfo->RBCR = 0;  
						pBlkInfo->NBCR = 1;  

					}
					// Going 페색 계전기도 정상적으로 복귀한다...
					if (nProhibitBlkID && (pProhibitBlk->RBGPR || !pProhibitBlk->NBGPR))
					{
						pProhibitBlk->RBGR = 0;  
						pProhibitBlk->NBGR = 1;  
					}

					if((m_pSignalFail[nRID] & BLOCK_ALARM_APPROACH) || (m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTIN))
					{
						m_pSignalFail[nRID] = 0;   
					}
				}


				if(pBlkInfo->LCRIN && pBlkInfo->CBBIN)
				{
					pBlkInfo->CA = FALSE;
					pBlkInfo->CAACK = FALSE;
					pBlkInfo->COMPLETE = TRUE;
					pBlkInfo->ARRIVE = FALSE;

					// 페색 계전기를 정상적으로 복귀...
					if (pBlkInfo->RBCPR || !pBlkInfo->NBCPR)
					{
						pBlkInfo->RBCR = 0;  
						pBlkInfo->NBCR = 1;  
					}
				}
				else if (pBlkInfo->CBBIN)
				{
					if (pBlkInfo->RBCPR)
					{
						pBlkInfo->RBCR = 0;  
						pBlkInfo->NBCR = 1;

					}
				}

				if (pBlkInfo->COMPLETE == TRUE)
				{
					if (pBlkInfo->LCRIN == TRUE) // 도중 취소 시 고려..
						pBlkInfo->CA = TRUE;

					pBlkInfo->LCR = FALSE;
					pBlkInfo->LCROP = FALSE;
				}

				if (pBlkInfo->COMPLETEIN)
				{
					pBlkInfo->CBB = FALSE;
					pBlkInfo->CBBOP = FALSE;
				}

				if (pBlkInfo->NBCPR)
				{
					pBlkInfo->NBCR = 0;
				}
				else if (pBlkInfo->RBCPR)
				{
					pBlkInfo->RBCR = 0;
				}

				if (pBlkInfo->CA == FALSE)
					pBlkInfo->CAACK = FALSE;

				int_put(SignalPut);

				// 폐색 요청을 받으면 경보를 울린다.  --> alarm4 
				if(pBlkInfo->CA && !pBlkInfo->CAACK && !pBlkInfo->LCR && !pBlkInfo->LCRIN && !pBlkInfo->CBB)
				{
					TcbCaOn = 1;

					nSysVarExt.bSound4 = 1;
				}
				else
				{
					if(!TcbCaOn)
					{
						nSysVarExt.bSound4 = 0;
					}
				}

				continue;
			}

// TGB......
		    else if (nBlockType == BLOCKTYPE_AUTO_OUT)  //TGB
			{
				if (nDir)   // Left direction -> B1    아잠푸르는 B3
				{
					if ( nBlockDir & BLOCKTYPE_DIR_MID )
						m_nB11ID = LoopCountH + 1;
					else
						m_nB1ID = LoopCountH + 1;
				}
				else
				{
					if ( nBlockDir & BLOCKTYPE_DIR_MID )
						m_nB13ID = LoopCountH + 1;
					else
						m_nB3ID = LoopCountH + 1;
				}
				
				if(pBlkInfo->SAXLACTIN)
				{
					if(!pBlkInfo->SAXLREQ)
					{
						pBlkInfo->SAXLACC = 0;
						pBlkInfo->SAXLDEC = 0;
					}
				}
				else
				{
					if(!pBlkInfo->AXLREQ)
					{
						pBlkInfo->AXLACC = 0;
						pBlkInfo->AXLDEC = 0;
					}
				}

				// Block Reset Request 제어를 낸 경우, Accept나 Decline을 받으면 Request를 0으로 만든다.
				if(pBlkInfo->BLKRSTREQ)
				{
					if(!m_ConfigList.BLK_TRAIN_SWEEP)
					{
	 					if(pBlkInfo->BLKRSTACCIN)
	 					{
	 						pBlkInfo->BLKRSTREQ = 0;
	 						
	 						// TGB Clear
	 						if(pBlkInfo->RBGPR || !pBlkInfo->NBGPR)
	 						{
								pBlkInfo->CA		= 0;
								pBlkInfo->LCR		= 0;
								pBlkInfo->DEPART	= 0;
								pBlkInfo->COMPLETE	= 1;

	 							pBlkInfo->RBGR = 0;
	 							pBlkInfo->NBGR = 1;  
	 						}
	 					}
					}

					if(pBlkInfo->BLKRSTDECIN)
					{
						pBlkInfo->BLKRSTREQ = 0;
					}
				}
				
				// Block Reset Request 제어가 없을 때(요청이 Clear 됐을 때) 항상 Accept와 Decline 을 0으로 만든다.
				if(!pBlkInfo->BLKRSTREQIN)
				{
					pBlkInfo->BLKRSTACC = FALSE;
					pBlkInfo->BLKRSTDEC = FALSE;
				}

				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					if(pBlkInfo->SWEEPING && pBlkInfo->SWEEPINGACK)
					{
						pBlkInfo->BLKRSTREQ = FALSE;
						pBlkInfo->BLKRSTACC = FALSE;
	
						pBlkInfo->SWEEPING = FALSE;
						
						// TGB Clear
						if(pBlkInfo->RBGPR || !pBlkInfo->NBGPR)
						{
							pBlkInfo->RBGR = 0;
							pBlkInfo->NBGR = 1;  
	 					}
					}
				}	

// 				if(pBlkInfo->CA == FALSE)
// 				{
// 					pBlkInfo->COMPLETE = TRUE;
// 				}

				// 20130823 sjhong - LCR 처리 과정에 CA와 CAACK를 확인하지 않도록 수정함.
				if(/*(pBlkInfo->CA == TRUE) && (pBlkInfo->CAACK == TRUE) && */(pBlkInfo->LCROP == TRUE) && (pBlkInfo->COMPLETE == TRUE))
				{
					pBlkInfo->LCR = TRUE;
					pBlkInfo->COMPLETE = FALSE;
				}
				
				BOOL bDirMatch = FALSE;

				ScrInfoTrack *pBlkStartTk = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[0]);

				if ( nDir != 0 && pBlkStartTk->ROUTEDIR)
				{
					bDirMatch = TRUE;
				}
				else if ( nDir == 0 && !pBlkStartTk->ROUTEDIR)
				{
					bDirMatch = TRUE;
				}

				// 진로가 폐색 진출 방향으로 설정된 경우
				if(!pBlkStartTk->ROUTE && bDirMatch)
				{
					// 순차 점유로 궤도가 점유가 된 경우.
					if(!pBlkStartTk->TRACK && pBlkStartTk->OVERLAP)
					{
						// 최초 폐색 트랙이 출발 진로가 설정되고 점유되었을 경우에 BLKTRACE 값을 설정한다.
						if(!pBlkInfo->DEPART)
						{
							pBlkStartTk->BLKTRACE = TRUE;
						}

						pBlkInfo->DEPART = TRUE;

						if (pBlkInfo->NBGPR == TRUE && pBlkInfo->CBBIN == FALSE)
						{
							pBlkInfo->RBGR = 1;
							pBlkInfo->NBGR = 0;
						}

						if(!(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTOUT))
						{
							m_pSignalFail[nRID] = BLOCK_ALARM_DEPARTOUT | BLOCK_ALARM_DEPART_TIME;
						}
					}
				}

				if(m_ConfigList.BLK_DEV_MODE)
				{
					ScrInfoSignal *pStartSigInfo = (ScrInfoSignal *)int_get(SignalGet, nStartSigID[0]);

					// 20190321 sjhong - 폐색장치의 경우엔 PREVSIGNAL 항목의 신호기 조건을 참조하여 SIN1(DR) 현시되면 ASTARTSTICK 잡히도록 함.
					if(pBlkInfo->LCR && pBlkInfo->LCRIN && pStartSigInfo->SIN1)
					{
						pBlkInfo->ASTARTSTICK = TRUE;
					}

					// 폐색 취급 후 열차가 폐색으로 진입하지 않은 상황에서만 Adv. Starter가 제어되도록 한다.
					if(pBlkInfo->LCR && pBlkInfo->LCRIN && !pBlkInfo->DEPART && (pStartSigInfo->SIN1 || !pBlkInfo->ASTARTSTICK))
					{
						pBlkInfo->AXLOCCDOUT	= 1;	// 실제로는 ASCR 출력임 (Block Bit 부족으로 공용 사용).
					}
					else
					{
						pBlkInfo->AXLOCCDOUT	= 0;	// 실제로는 ASCR 출력임 (Block Bit 부족으로 공용 사용).
					}

					BYTE nFromTKID = pInfo[RT_OFFSET_BLKARRIVETK];

 					ScrInfoTrack *pBlkFromTk	= (ScrInfoTrack *)int_get(TrackGet, nFromTKID);

					if(pBlkInfo->LCR && pBlkInfo->LCRIN && !pBlkFromTk->TRACK && !pBlkStartTk->TRACK)
					{
						pBlkInfo->DEPART = TRUE;

						if (pBlkInfo->NBGPR == TRUE && pBlkInfo->CBBIN == FALSE)
						{
							pBlkInfo->RBGR = 1;
							pBlkInfo->NBGR = 0;
						}
					}
				}

				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					if(((pBlkInfo->BLKRSTREQ && pBlkInfo->BLKRSTACCIN) || (pBlkInfo->BLKRSTREQIN && pBlkInfo->BLKRSTACC)) && !pBlkInfo->SWEEPING)
					{
						if(!pBlkStartTk->TRACK)
						{
							pBlkStartTk->BLKSWEEP = TRUE;
						}
					}
					
					if(!pBlkInfo->RBGPR && pBlkInfo->LCR && pBlkInfo->LCRIN)
					{
						if(pBlkStartTk->ROUTE && !pBlkStartTk->TRACK)
						{
							pBlkStartTk->BLKILLOCC = TRUE;
						}
					}
				}

				ScrInfoTrack *pBlockTKInfo = pBlkStartTk;
				ScrInfoTrack *pPrevTKInfo = NULL;
				
				for(int i = 1 ; i < nBlkTkCount ; i++)
				{
					pPrevTKInfo = pBlockTKInfo;
					pBlockTKInfo = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[i]);

					// 직전 폐색 트랙에 BLKTRACE값이 있을 경우에만 처리한다.
					if(pPrevTKInfo->BLKTRACE && pPrevTKInfo->TRACK && !pBlockTKInfo->ROUTE && pBlockTKInfo->OVERLAP && !pBlockTKInfo->TRACK && !pBlockTKInfo->BLKTRACE)
					{	
						// 진로가 설정되고, 연속점유로 점유되었을 경우에만 BLKTRACE 값을 이동시킨다.
						
						pPrevTKInfo->BLKTRACE = FALSE;
						pBlockTKInfo->BLKTRACE = TRUE;
					}

					if(m_ConfigList.BLK_TRAIN_SWEEP)
					{
						if(!pBlockTKInfo->TRACK)
						{
							if(pPrevTKInfo->BLKSWEEP)
							{
								pBlockTKInfo->BLKSWEEP = TRUE;
							}
	
							if(pPrevTKInfo->BLKILLOCC)
							{
								pBlockTKInfo->BLKILLOCC = TRUE;
							}
						}
						
						if(pPrevTKInfo->TRACK)
						{
							pPrevTKInfo->BLKSWEEP = FALSE;
							pPrevTKInfo->BLKILLOCC = FALSE;
						}
					}
				}

				if(m_ConfigList.BLK_TRAIN_SWEEP)
				{
					if(pBlockTKInfo->BLKSWEEP && pBlockTKInfo->TRACK)
					{
						pBlockTKInfo->BLKSWEEP = FALSE;
	
						if((!pBlkInfo->SAXLACTIN && !pBlkInfo->AXLOCC) || (pBlkInfo->SAXLACTIN && !pBlkInfo->SAXLOCC))
						{
							pBlkInfo->SWEEPING = TRUE;
						}
					}
	
					if(pBlockTKInfo->BLKILLOCC && ((!pBlkInfo->SAXLACTIN && !pBlkInfo->AXLOCC) || (pBlkInfo->SAXLACTIN && !pBlkInfo->SAXLOCC)))
					{
	
						if(pBlockTKInfo->TRACK)
						{
							pBlockTKInfo->BLKILLOCC = FALSE;
						}
	
						pBlkInfo->DEPART = TRUE;
								
						if(pBlkInfo->NBGPR)
						{
							pBlkInfo->RBGR = 1;
							pBlkInfo->NBGR = 0;
						}
					}
				}

				BYTE nProhibitBlkID = pInfo[RT_OFFSET_PROHIBITBLK];
				if(nProhibitBlkID)
				{
					WORD wTcbNo = m_pSignalInfoTable[ nProhibitBlkID ].wRouteNo;
					DWORD nTcbInfoPtr = *(DWORD*)&m_pRouteAddTable[ wTcbNo ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
					BYTE *pTcbInfo = &m_pInfoMem[nTcbInfoPtr];					// 연동도표 정보 테이블
										
					BYTE nTcbTKID[MAX_BLKTRACK_QTY]={0};
					BYTE nTcbTkCount = 0;
					BYTE nTcbArriveTKID = 0;

					p = pTcbInfo + RT_OFFSET_BLKTRACK1;
					for(int i = 0 ; i < MAX_BLKTRACK_QTY ; i++)
					{
						nTcbTKID[i] = *p++;
						if(nTcbTKID[i] != 0)
						{
							++nTcbTkCount;
						}
					}

					ScrInfoTrack *pTcbTKInfo = NULL;

					for(i = 0 ; i < nTcbTkCount ; i++)
					{
						pTcbTKInfo = (ScrInfoTrack *)int_get(TrackGet, nTcbTKID[i]);

						if(pPrevTKInfo && pPrevTKInfo->BLKTRACE && pPrevTKInfo->TRACK && !pTcbTKInfo->BLKTRACE && !pTcbTKInfo->TRACK 
						&& (pTcbTKInfo->ROUTE || (!pTcbTKInfo->ROUTE && (pTcbTKInfo->ROUTEDIR != nDir))))
						{
							pPrevTKInfo->BLKTRACE = FALSE;
							pTcbTKInfo->BLKTRACE = TRUE;
						}

						pPrevTKInfo = pTcbTKInfo;
					}

					// 폐색의 자기 TCB에서 열차가 도착함을 검지한 경우...
					ScrInfoBlock *pBlockTCB = (ScrInfoBlock *)int_get(SignalGet, nProhibitBlkID);
					if(pBlockTCB->ARRIVE)
					{
						// 한번만 호출되도록 한다.
						if(!pBlkInfo->COMPLETE && pBlkInfo->DEPART)
						{
							pBlkInfo->COMPLETE = TRUE;
							pBlkInfo->DEPART = FALSE;
							pBlkInfo->CA = FALSE;
							pBlkInfo->LCR = FALSE;
						}
						
						if(pBlkInfo->RBGPR)
						{
							pBlkInfo->RBGR = 0;
							pBlkInfo->NBGR = 1;
						}
					}

	 				if(pBlkInfo->ARRIVEIN && !pBlkInfo->LCR)  // 인접역열차가 폐색작업을 마치고 그 역으로 돌아 간 경우..
					{
						if(pBlkInfo->CAACK)
						{
							pBlkInfo->CA = TRUE;
						}

 						pBlkInfo->LCR = TRUE;
 						pBlkInfo->DEPART = TRUE;
 						pBlkInfo->COMPLETE = FALSE;
 
 						// 2007.3.1  강가사역과 통신시 떨림 현상으로 삭제....
 						/*
 						if (!pBlkInfo->RBGPR)
 						{
 							pBlkInfo->RBGR = 1;
 							pBlkInfo->NBGR = 0;
 						}
 						*/
 					}
				}

// 2004.12.01 CA만 누른 상태에서도 CBB를 눌러 취소가 가능하도록 조정 했음...
//				if (pBlkInfo->CBBOP && pBlkInfo->LCRIN)
				if(pBlkInfo->CBBOP)
				{
					if((pBlkInfo->DEPART == FALSE) // 열차가 인접역으로 출발을 한 경우 이역에서는 취소를 취급 할 수 없다.
					&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
					{
						pBlkInfo->CBB = TRUE;
						pBlkInfo->CA = FALSE;
						pBlkInfo->COMPLETE = TRUE;
						pBlkInfo->ASTARTSTICK = FALSE;

						// 상태편으로 부터 LCRIN을 받았다면 CBBIN을 받아야 모든 폐색이 Clear되나,
						// 상대편과 통신 두절 상태로 LCRIN을 받지 않았으면, CBB만 가지고도 폐색을 clear한다...
						if(pBlkInfo->LCRIN == FALSE)
						{
							pBlkInfo->LCR = FALSE;
							pBlkInfo->LCROP = FALSE;
							pBlkInfo->CBB = FALSE;
							pBlkInfo->CBBOP = FALSE;
							pBlkInfo->ASTARTSTICK = FALSE;
						}
					}
				}


				// 인접역에서 열차를 받아 취소 요청을 한 경우...
				// AXLOCC 점유 조건 추가
				if((pBlkInfo->CBBIN == TRUE) && ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
				{
					// 열차가 상대편역에 정상적으로 도착했거나, 반대편 역에서 폐색작업이 끝나고 다시 그 역으로 돌아 감 혹은 취소...
					if(pBlkInfo->ARRIVEIN == TRUE || pBlkInfo->DEPART == FALSE)
					{

						pBlkInfo->COMPLETE = TRUE;
						pBlkInfo->CA = FALSE;
						pBlkInfo->ASTARTSTICK = FALSE;

						pBlkInfo->RBGR = 0;  
						pBlkInfo->NBGR = 1;  

						ScrInfoTrack *pBlockTKInfo = NULL;

						for(int i = 0 ; i < nBlkTkCount ; i++)
						{
							pBlockTKInfo = (ScrInfoTrack *)int_get(TrackGet, nBlockTKID[i]);

							pBlockTKInfo->BLKTRACE = 0;
						}
					}
				}

				if (pBlkInfo->COMPLETE)
				{
					// 반대편 역에서 폐색작업이 끝나고 다시 그 역으로 돌아 감 혹은 도중 취소 시 고려..
					if (pBlkInfo->LCR == TRUE && pBlkInfo->CBB == FALSE && pBlkInfo->CBBIN == FALSE && pBlkInfo->ARRIVE == FALSE)  
					{
						pBlkInfo->CA = TRUE;
					}
					
					pBlkInfo->LCR = FALSE;
					pBlkInfo->LCROP = FALSE;
					pBlkInfo->ASTARTSTICK = FALSE;
				}


				if (pBlkInfo->COMPLETEIN)
				{
					pBlkInfo->CBB = FALSE;
					pBlkInfo->CBBOP = FALSE;
					pBlkInfo->DEPART = FALSE;
					pBlkInfo->ASTARTSTICK = FALSE;
										
					if(m_pSignalFail[nRID] & BLOCK_ALARM_DEPARTOUT)
					{
						m_pSignalFail[nRID] = 0;
					}
				}

				if (pBlkInfo->NBGPR)
				{
					pBlkInfo->NBGR = 0;
				}
				else if (pBlkInfo->RBGPR)
				{
					pBlkInfo->RBGR = 0;
				}

				int_put(SignalPut);

				// 폐색 요청을 받으면 경보를 울린다.  --> alarm4 
				// 20130823 sjhong - LCR 처리 과정에 CA와 CAACK를 확인하지 않도록 수정함.
// 				if( /*pBlkInfo->CA && pBlkInfo->CAACK &&*/ !pBlkInfo->LCROP && !pBlkInfo->LCR && !pBlkInfo->LCRIN && !pBlkInfo->CBBOP && !pBlkInfo->CBB )
// 				{
// 					TgbCaOn = 1;
// 
// 					nSysVarExt.bSound4 = 1;
// 				}
// 				else
// 				{
// 					if(!TgbCaOn)
// 					{
// 						nSysVarExt.bSound4 = 0;
// 					}
// 				}

			    continue;
			}

			if((nSigType & 0x0f0) == SIGNALTYPE_MPASS) 
			{
				m_SignalInfo.SOUT1 = m_SignalInfo.SOUT2 = m_SignalInfo.SOUT3 = 0;
				m_SignalInfo.SE1 = m_SignalInfo.SE2 = m_SignalInfo.SE3 = m_SignalInfo.SE4 = 0;

				m_SignalInfo.U = 0;
				m_SignalInfo.FREE = 1;	
				m_SignalInfo.ENABLED = 0;
				m_SignalInfo.TM1 = 0;

       			m_wRouteNo = m_pSignalInfoTable[ LoopCountH + 1 ].wRouteNo;
				nInfoPtr = *(DWORD*)&m_pRouteAddTable[ m_wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

		    	nDependSigID = (pInfo[ RT_OFFSET_DEPENDSIG1 ] & SIGNALID_MASK);
			    nNextSigID = pInfo[ RT_OFFSET_PREVSIGNAL ];

				ScrInfoSignal *pNextSigInfo = NULL;
				if ( nDependSigID ) 
					pNextSigInfo = (ScrInfoSignal *)int_get(SignalGet, nDependSigID);
				else if ( nNextSigID )
					pNextSigInfo = (ScrInfoSignal *)int_get(SignalGet, nNextSigID);

				if ( pNextSigInfo ) 
				{
					int bSigOn = 0;
					if ( pNextSigInfo->SIN3 ) 
					{	
						m_SignalInfo.SOUT3 = m_SignalInfo.SE3 = 1;  
						bSigOn = 1;
					}

					if ( bSigOn ) 
					{
						m_SignalInfo.FREE = 0;
						
						RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
						p = pInfo + pRouteBasPtr->nOfsRoute;
						nCount = *p++;	// Route lock

						for (WORD i=0; i<nCount; i++) 
						{
							int_get(TrackLoad, *p++);
							if ( !m_TrackInfo.TRACK )   // 궤도 점유 조사.
							{	
								m_SignalInfo.SOUT3 = 0; // 궤도가 점유되면 출력을 차단. 
								m_SignalInfo.SE3 = 0;
								m_SignalInfo.FREE = 1;	
							}
						}
					}
				}
				m_SignalInfo.SIN1 = m_SignalInfo.SOUT1;  
				m_SignalInfo.SIN2 = m_SignalInfo.SOUT2;  
				m_SignalInfo.SIN3 = m_SignalInfo.SOUT3;
				m_SignalInfo.INU  = m_SignalInfo.U;

		        SignalAspectAdjustForLMR(pInfo);
				int_put(SignalPut);
			    continue;

			} //End of 중계 신호기 


			m_SignalInfo.FREE = 1;	
			m_SignalInfo.ENABLED = 0;
			m_SignalInfo.TM1 = 0;
			
			SignalStopU();

			m_SignalInfo.REQUEST = 0;


/*
            if (m_SignalInfo.FREE) 
			{
                m_SignalInfo.ENABLED = 1;
				pbSignal = (BYTE*)&m_SignalInfo;

				pbSignal[0] &= 0xF0;
				pbSignal[1] &= 0xF0;

                BYTE temp = pbSignal[0];

				if ( nSignalAsp & 2 ) {	// 3,4 현시
					temp &=	0x70;
				}
				else {					// 입환은 S3 만 체크
					temp &= 0x40;
				}

                if ( temp ) 
				{
					if ( SG_Timer & 0x80 ) 
					{
						if ( nSysVar.bSysFail == 0 ) {
							SG_Timer++;
							if ( SG_Timer & 0x04 ) {		// 08->04
								BYTE temp = ~pbSignal[0] >> 4;
								temp &= 0x07;
								temp |= 0x40;
								SetErrorCode( temp, &m_pMsgBuffer[8] );
							}
						}
					}
					else {
						SG_Timer = 0;
					}

					SG_Timer |= 0x80;
				}
				else {
					SG_Timer = 0;
				}
			}
*/

			// 2006.8.7  signal fail 공통 Lamp 점등으로 인해 Block 정보의 경우 처리하지 않음...
			if ( !m_bIsInBlock )
				SignalAspectAdjustForLMR(pInfo);

			int_put(SignalPut);

			// Advanced Starter signal Put back 상태 update....			
			if (nSignalType == SIGNALTYPE_ASTART)
			{
            	m_wRouteNo = m_pSignalInfoTable[ LoopCountH + 1 ].wRouteNo;
		        nInfoPtr = *(DWORD*)&m_pRouteAddTable[ m_wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
		        pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			    nNextSigID = pInfo[ RT_OFFSET_PREVSIGNAL ];

				if (nNextSigID && nAstartOn==2)
				{
					int_get(SignalLoad, nNextSigID);
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;
					pBlkInfo->ASTART = 0;
					int_put(SignalPut);
					nAstartOn = 0;
				}
			}
			

			continue;
        }

		if ( (m_wRouteNo & ROUTE_MASK) == 0 )
			m_wRouteNo = 0;


		if ((m_SignalInfo.DESTID & 0x40) && m_wRouteNo==0 && m_bIsYudo)
		{
			m_wRouteNo = m_arrRouteNo[nRID];
		}

		m_pRouteQue[ LoopCountH + 1 ] = m_wRouteNo;

		// 2006.8.7  signal fail 공통 Lamp 점등으로 인해 Block 정보의 경우 처리하지 않음...
		if ( !m_bIsInBlock )
		{
			SignalAspectAdjustForLMR(pInfo);
		}

		int_put(SignalPut);

		// Advanced Starter signal 상태 update....
		if (nSignalType == SIGNALTYPE_ASTART)
		{
			if (nNextSigID && nAstartOn==1)
			{
				int_get(SignalLoad, nNextSigID);
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)&m_SignalInfo;

				pBlkInfo->ASTART = TRUE;
				pBlkInfo->ASTARTSTICK = TRUE;
				int_put(SignalPut);
				nAstartOn = 0;
			}
		}
	}

	if ( bTrackGood ) 
		TrackProcess();

    SwitchProcess();
//    BitOperProcess();


	//UnitStateType *EI_UnitStateType = (UnitStateType *)&m_pVMEM[14];
	LoopCountL = (BYTE)m_TableBase.InfoTrack.nCount;

	int tkGood = 1;
	LoopCountH = 0;

	for (LoopCountH=0; LoopCountH<LoopCountL; LoopCountH++)
	{
		int_get(TrackLoad, LoopCountH+1);

		if ( !m_TrackInfo.TRACK && !m_TrackInfo.OVERLAP )  // ????? --> 무슨 처리지????
			tkGood = 0;
/*
		if (m_pTrackInfoTable[ LoopCountH + 1 ] & TI_ALARM)
		{
			if (m_TrackInfo.ALARM)
				ApproachAlarm = TRUE;
		}
*/
	}

	if ( UnitState.bSysRun && !m_bStartInterlock )
	{
		m_bStartInterlock = TRUE;
	}

	if(!nSysVar.bN3 && ((((!(nSysVar.bStation && nSysVar.bN1) && nSysVar.bTrackOcc) || bIsDepartTCB || bExistOutSignal) && !bATBTimeout) || nSysVar.bATB))
	{
		m_bSLSStick = 1;
	}
	else
	{
		m_bSLSStick = 0;
	}

	return;
}



void CEipEmu::SysDownProc()
{
		//m_SignalInfo.ENABLED = 0;
		m_SignalInfo.TM1 = 0;
		BYTE *pbSignal = (BYTE*)&m_SignalInfo;

		pbSignal[0] &= 0xF0;
		pbSignal[1] &= 0xC0;

        int_put(SignalPut);
}

void CEipEmu::SetTkRequest(BYTE *pInfo)
{
	BYTE nFromTrack;
	BYTE *p, j, nID;
	WORD nCount;

	if ( pInfo && (m_SignalInfo.TM1 || (!m_SignalInfo.TM1 && !m_bFirstTrackFree)) ) 
	{
		BYTE nSigID = pInfo[RT_OFFSET_SIGNAL];

		nFromTrack = *pInfo;
		ScrInfoTrack *pTrkInfo;

	// Route
		RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
		p = pInfo + pRouteBasPtr->nOfsRoute;

		nCount = *p++;

		for (j=0; j<nCount; j++) 
		{
			nID = *p++;
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);

			if ( !(m_wCheckResult & ROUTECHECK_SGOFF) && (m_nCheckOverlap != 0xf0) ) // #define ROUTECHECK_SGOFF  0x97 --> 10010111 (off상태)
			{
				if ( m_wRouteNo ) 
				{
					pTrkInfo->ROUTE = 0;
				}
			}
			else {
//				pTrkInfo->ROUTEDIR = 0;        
			}

			if ( !m_bIsInBlock && !(m_pRouteQue[nSigID] & ROUTE_CANCEL) )
			{
    			pTrkInfo->TKREQUEST = 1;

				if((m_pRouteQue[nSigID] & ROUTE_DEPENDSET) && ((m_pSignalInfoTable[nSigID].nSignalType & SIGNAL_MASK) != SIGNALTYPE_OHOME))
				{
					pTrkInfo->RouteNo = m_pOwnerRouteQue[nSigID];
				}
				else
				{
					pTrkInfo->RouteNo = m_wRouteNo;
					pTrkInfo->ROUTEDIR = ((m_pSignalInfoTable[nSigID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2);
				}	
			}
		}
	}
}


void CEipEmu::ClearRequest(BYTE *pInfo)
{
    BYTE *p, nID;
	BYTE nFromTrack = *pInfo;

//	p = pInfo + RT_OFFSET_SWITCH;
	WORD nCount;

// Route
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
	p = pInfo + pRouteBasPtr->nOfsRoute;
	nCount = *p++;

	ScrInfoTrack *pFromTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nFromTrack);

	nID = *p;

	ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);

    /*
	if (m_pTrackInfoTable[nFromTrack] & TI_NOTRACK) 
	{   
		pFromTrkInfo->TRACK = 0;
	}
	*/

    if ( !pFromTrkInfo->TRACK && pTrkInfo->TRACK && pTrkInfo->OVERLAP )
	{
        BYTE nNextTrackID = 0;

        
        if (nCount > 1) {	// 진로내의 궤도가 한개 이상인 경우 2번째 진로궤도 
            nNextTrackID = *(p+1);
        }
        else {  // 그렇지 않은 경우 종착 궤도를 대상으로한다. -- 보완를 위해 Coding 
            nNextTrackID = *(pInfo + RT_OFFSET_TO);
        }

        if (nNextTrackID) 
		{
            ScrInfoTrack *pTrkInfoTo = (ScrInfoTrack *)int_get(TrackGet, nNextTrackID);

			if ( !pTrkInfoTo->TRACK ) 
			{ 
                pTrkInfo->ROUTE = 1; 
            }
		}
    }
}


void CEipEmu::SignalAspectAdjustForLMR(BYTE *pInfo)
{
	BYTE nSigID = LoopCountH + 1;

	m_pSignalTimer[ nSigID ] = SG_Timer;

	// RED LAMP가 없는 Ground shunt signal 은 Failure detection을 하지 않는다... (Signal Fail 강제 초기화)
	// Ground shunt signal이라도 RED LAMP가 붙은 경우에는 Signal Fail 체크를 해야한다.
// 	if( (((m_pSignalInfoTable[nSigID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_SHUNT) || ((m_pSignalInfoTable[nSigID].nSignalType & SIGNAL_MASK) == SIGNALTYPE_GSHUNT))	
// 	&& (((m_pSignalInfoTable[nSigID].m_nLamp & SG_LAMP_R) && m_SignalInfo.U) || !(m_pSignalInfoTable[nSigID].m_nLamp & SG_LAMP_R)) )
// 	{
// 		m_pSignalFail[nSigID] = 0;	// 경보 Bell 복귀...
// 		m_SignalInfo.SignalFail = 0;
// 		m_SignalInfo.LoopSigFail = 0;
// 		m_SignalFailDetect[nSigID] = 0;
// 		return;
// 	} 

	BYTE nFailMask = 0;
	BYTE nSignalType = m_pSignalInfoTable[nSigID].nSignalType & SIGNAL_MASK;

	if(nSignalType == SIGNALTYPE_BUFSTART)
	{
		m_SignalInfo.SignalFail = 0;
		m_SignalInfo.LoopSigFail = 0;
		m_SignalFailDetect[nSigID] = 0;
		return;
	}

	// Signal failure (Both Main and Secondary filament) 표시....
	if (m_SignalInfo.LM_ALR == 0)
	{
		// ALR출력 상태에서 LOR이 검지되지 않으면 Dual main filament failure...
		/*
		if (m_SignalInfo.SIN1 == 1 || m_SignalInfo.SIN2 == 1)
		{
			if (m_SignalInfo.LM_LOR == 0)
				nFailMask |= 1;
		}
		*/
		if(m_SignalInfo.LM_LOR == 0)
		{
			nFailMask |= 0x01;
		}

		if (m_SignalInfo.SIN3 == 1)
		{
			if (m_SignalInfo.LM_CLOR == 0)
				nFailMask |= 0x01;
		}

		// 20170518 sjhong - BCI역 SHUNT 고장 검지 처리 추가
		if (m_SignalInfo.INU == 1)
		{
			if (m_SignalInfo.LM_SLOR == 0)
				nFailMask |= 0x01;
		}

		// 2005.09.08  방향검지가 되지 않는 경우 신호기를 정지 시킨다.
		if ( m_SignalInfo.LEFT == 1 && (m_SignalInfo.SIN1==1 || m_SignalInfo.SIN2==1 || m_SignalInfo.SIN3==1 || m_SignalInfo.INU==1) )
		{
			if (m_SignalInfo.INLEFT == 0 && m_SignalInfo.INRIGHT == 0)  
				nFailMask |= 0x80;
		}
		if ( m_SignalInfo.RIGHT == 1 && (m_SignalInfo.SIN1==1 || m_SignalInfo.SIN2==1 || m_SignalInfo.SIN3==1 || m_SignalInfo.INU==1) )
		{
			if (m_SignalInfo.INRIGHT == 0 && m_SignalInfo.INLEFT == 0) 
				nFailMask |= 0x80;
		}
	}

	
	// Main filament failure 표시...
// 	if ( m_SignalInfo.LM_LOR == 1)  // R,Y,G signal
// 	{
// 		if ( (nOldControl & 0x01) && (nOldControl & 0x02) )  // HR+DR제어
// 		{
// 			if (!m_SignalInfo.LM_EKR)		// Main signal EKR
// 			{
// 				nFailMask |= 0x40;
// 			}
// 			else
// 				m_SignalInfo.LM_GL = 0;		
// 		}
// 		else if (nOldControl & 0x02)  // HR 제어
// 		{
// 			if (!m_SignalInfo.LM_EKR)
// 			{
// 				nFailMask |= 0x20;
// 			}
// 			else
// 				m_SignalInfo.LM_YL = 0;		
// 		}
// 		else  
// 		{
// 			if (!m_SignalInfo.LM_EKR)
// 			{
// 				nFailMask |= 0x10;
// 			}
// 			else
// 				m_SignalInfo.LM_RL = 0;		
// 		}
// 		
// 		if (m_SignalInfo.LM_EKR)
// 		{
// 			m_SignalInfo.LM_RL = 0;		
// 			m_SignalInfo.LM_YL = 0;		
// 			m_SignalInfo.LM_GL = 0;		
// 		}
// 	}
// 	else
// 	{
// 		// Main filament failure 상태를 모두 Clear한다....
// 		m_SignalInfo.LM_RL = 0;		
// 		m_SignalInfo.LM_YL = 0;		
// 		m_SignalInfo.LM_GL = 0;		
// 	}
// 	
// 	// S-Yellow signal Main filament failure 표시...
// 	if((m_SignalInfo.LM_CLOR == 1) && (nSignalType == SIGNALTYPE_OHOME))  
// 	{
// 		if(!m_SignalInfo.LM_CEKR)
// 		{
// 			nFailMask |= 0x01;
// 		}
// 	}

	if(nFailMask)
	{
		SetSignalFail(nFailMask, pInfo);
	}
	else
	{
		m_pSignalFail[nSigID] = 0;	// 경보 Bell 복귀...
		m_SignalInfo.SignalFail = 0;
		m_SignalInfo.LoopSigFail = 0;
		m_SignalFailDetect[nSigID] = 0;
	}

	return;
}


void CEipEmu::SetSignalFail( BYTE nFailMask, BYTE *pInfo ) 
{
	BYTE nSigID = LoopCountH + 1;

    SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;
	SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
	BYTE nSignalType = m_pSignalInfoTable[nSigID].nSignalType & SIGNAL_MASK;

	// Signal Fail의 검지 시간 지연을 위해 사용....
	if (!(m_SignalFailDetect[nSigID] & 0x80))
	{
		m_SignalFailDetect[nSigID] += 0x10;
		return;
	}
	m_SignalFailDetect[nSigID] = 0;

	/* 2013.1.25 LED signal은 main filament failure가 없어 삭제함...
	if (nFailMask & 0x10)
	{
		m_SignalInfo.LM_GL = 0;		
		m_SignalInfo.LM_YL = 0;		
		m_SignalInfo.LM_RL = 1;		// R main filament failure
	}
	else if (nFailMask & 0x20)
	{
		m_SignalInfo.LM_RL = 0;		
		m_SignalInfo.LM_GL = 0;		
		m_SignalInfo.LM_YL = 1;		// Y main filament failure
	}
	else if (nFailMask & 0x40)
	{
		m_SignalInfo.LM_RL = 0;		
		m_SignalInfo.LM_YL = 0;		
		m_SignalInfo.LM_GL = 1;		// G main filament failure
	}
    */

	// 신호 현시 중 LOR이 off되고 다시 LOR이 on 되면 신호 현시를 하지 않아야 한다...
	if (!m_SignalInfo.LM_LOR && (m_SignalInfo.SE1 || m_SignalInfo.SE2) && m_SignalInfo.TM1)
	{
//		if(nSignalType != SIGNALTYPE_OHOME)
		{
			m_SignalInfo.TM1 = 0;
		}
	}

	// Outer Home의 YY신호기의 HHLOR이 Off되면 신호기를 RED로 떨군다.
	// 20150823 sjhong - 일반 신호기의 Callon 신호기 또한 CLOR이 Off되면 신호기를 RED로 떨군다.
	if(/*(nSignalType == SIGNALTYPE_OHOME) && */!m_SignalInfo.LM_CLOR && m_SignalInfo.SE3)
	{
		m_SignalInfo.TM1 = 0;
	}

	if(!m_SignalInfo.LM_SLOR && m_SignalInfo.SE4)
	{
		m_SignalInfo.TM1 = 0;
	}
						
	/* 2013.3.5 대치 삭제함...
	if (nSignalType == SIGNALTYPE_OHOME) 
	{
		// 2005.10.20  Red filament fail 상태에서 Outer home의 callon signal  SE3 제어 후 시간 경과 후에 CLOR이 검지되어야하나 경과 시간이
		// 없기 때문에 경과 시간을 두기 위해 이 로직을 수행 한다.
		if (!m_SignalInfo.SE3)
			m_SignalFailDetect[nSigID] &= 0xf0;

		if (!m_SignalInfo.LM_CLOR && m_SignalInfo.SE3 && m_SignalInfo.TM1)
		{ 
			if (m_SignalFailDetect[nSigID] & 0x08)
			{
				m_SignalInfo.TM1 = 0;
			}
			else
				m_SignalFailDetect[nSigID] += 0x01;
		}
	}
	*/


	// 모든 Lamp의 dual filament failure 경종 작동....
	// 적색 현시 주 필라멘드가 소손되면 경종 작동....
	if((!m_SignalInfo.LM_ALR && !m_SignalInfo.LM_LOR && !m_pSignalFail[nSigID]) 
	|| ((nSignalType == SIGNALTYPE_OHOME) && !m_SignalInfo.LM_CLOR && m_SignalInfo.SE3 && !m_pSignalFail[nSigID])
	|| (!m_SignalInfo.LM_SLOR && m_SignalInfo.SE4 && !m_pSignalFail[nSigID])
	/*|| (m_SignalInfo.LM_RL && !m_pSignalFail[nSigID]) || */)
	{
		if(!m_ConfigList.BLK_DEV_MODE)
		{
	        nSysVarExt.bSound2 = 1;	// bSound_SP
		}

		m_pSignalFail[nSigID] = 1;   // // Bell동작을 masking
	}


	// Callon (SIGNALTYPE_IN에만 존재) 현시 필라멘드가 소손되면 경종 작동....
	// Callon signal은 만약 이중 Filament type으로 공급하지 않으면 고장 검지에 time delay를 필요로 한다...
	if (nSignalType == SIGNALTYPE_IN)
	{
		if ( m_SignalInfo.SIN3 == 1 )
		{
			if (!m_SignalInfo.LM_CLOR && !m_pSignalFail[nSigID]) // Callon은 Single filament로 하기로 결정...
			{
				if(!m_ConfigList.BLK_DEV_MODE)
				{
					nSysVarExt.bSound2 = 1;	// bSound_SP
				}
				m_pSignalFail[nSigID] = 1;  // Bell동작을 masking
			}
		}

		// 2005.09.08  Home signal의 방향검지가 안될 경우 신호 출력을 중단한다...
		if (nFailMask & 0x80)
		{
			SignalStop();
			m_SignalInfo.TM1 = 0;
		}
	}

	// 2007.3.10  출발신호기도 LI가 검지 되지 않으면 출력을 중단한다..
	if (nSignalType == SIGNALTYPE_OUT)
	{
		if (nFailMask & 0x80)
		{
			SignalStop();
			m_SignalInfo.TM1 = 0;
		}
	}


	if((pInfo != NULL) && (m_SignalInfo.TM1 == 0))
	{
		// 20150526 sjhong - 리플렉트 신호기들도 연계되서 동작하도록 한다.
		BYTE nDependID = 0;
		ScrInfoSignal *pDependInfo = NULL;

		for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
		{
			nDependID = (pInfo[cSigOffset] & SIGNALID_MASK);

			if(nDependID && ((m_pSignalInfoTable[nDependID].nSignalType & SIGNAL_MASK) != SIGNALTYPE_OHOME))
			{
				pDependInfo = (ScrInfoSignal*)int_get(SignalGet, nDependID);
				if(pDependInfo != NULL)
				{
					pDependInfo->TM1 = 0;
				}
			}
		}
	}

	if(nFailMask & 0x80)
	{
		m_SignalInfo.LoopSigFail = 1;
	}

	if(nFailMask & 0x7F)
	{
		m_SignalInfo.SignalFail = 1;
	}

	nSysVar.bSignalFail = 1;
	
	return;
}


void CEipEmu::SetSwitchFail(BYTE nSwID)
{
    SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;
	SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
	
	m_SwitchInfo.SWFAIL = 1;

	if ( !m_pSwitchFail[nSwID] ) 
	{
		if(!m_ConfigList.BLK_DEV_MODE)
		{
			nSysVarExt.bSound2 = 1;
		}

		m_pSwitchFail[nSwID] = 1;
	}
}

void CEipEmu::ConvertToReal4() 
{
#ifndef _WINDOWS
	short i;
	short nSignal = m_TableBase.InfoSignal.nCount;
	for (i=1; i<=nSignal; i++) 
	{
		BYTE pSigAsp = m_pSignalInfoTable[i].nSignalType & 0x03;
		BYTE nSignalType = m_pSignalInfoTable[i].nSignalType & SIGNAL_MASK;
        if  (nSignalType == BLOCKTYPE_AUTO_OUT) continue;
		ScrInfoSignal *pInfo = GP_InfoSignal(i);
		BYTE &inout = *(BYTE*)pInfo;
		if ( pSigAsp == 2 ) 
		{	// 4 현시
			if ( (inout & 0x7) == 5 ) {
				inout &= ~0x1;	// if SOUT2 == 0, SOUT1 => 0;
				inout |= 0x10;
			}
		}
	}
#endif
}

void CEipEmu::BitOperProcess() 
{
    short i;
	for (i=0; i<256; i++) 
	{
		WORD nInfoPtr = m_pInfoGeneral[ i ];
		if (nInfoPtr) 
		{
			BYTE *p = &m_pInfoMem[nInfoPtr];	
			BYTE nOP;
			WORD nArg1;
			BYTE nAcc = 0, nBitMask, nBitLoc, nInverse, nWork;
			while (*p) 
			{
				nOP = *p++;

				nArg1 = *(WORD*)p++;
				nInverse = ((nArg1 & 0x8000) != 0);
				nArg1 &= 0x7FFF;

				nBitLoc = nArg1 & 7;
				nBitMask = 1 << nBitLoc;

				nArg1 >>= 3;
				p++;

				switch ( nOP ) 
				{
					case FUNC_LDA :
						nAcc = (m_pVMEM[ nArg1 ] & nBitMask) >> nBitLoc;
						if (nInverse) 
							nAcc ^= 1;
						break;

					case FUNC_STA :
						if (nInverse) 
							nAcc ^= 1;

						m_pVMEM[ nArg1 ] = m_pVMEM[ nArg1 ] & ~nBitMask | (nAcc << nBitLoc);
						break;

					case FUNC_AND :
						nWork = (m_pVMEM[ nArg1 ] & nBitMask) >> nBitLoc;
						if (nInverse) 
							nWork ^= 1;

						nAcc &= nWork;
						break;

					case FUNC_IOR :
						nWork = (m_pVMEM[ nArg1 ] & nBitMask) >> nBitLoc;
						if (nInverse) 
							nWork ^= 1;

						nAcc |= nWork;
						break;
				}
			}
		}
		else 
			break;
	}
}


void CEipEmu::SignalCheck(BYTE nSigType, BYTE *pInfo)
{
	if (m_SignalInfo.ON_TIMER) 
	{
  		BOOL bApproachTrkOccupied = FALSE;

		if(pInfo == NULL)
		{
			SG_Timer = 120;
#ifdef _CHOTEST
			SG_Timer /= 10;
#endif
		}
		else
		{
			for(BYTE cTkOffset = RT_OFFSET_APPRCHTK_BEGIN ; cTkOffset <= RT_OFFSET_APPRCHTK_END ; cTkOffset++)
			{
				BYTE nApproachTrk = pInfo[cTkOffset];
				if(nApproachTrk == 0)
				{
					break;
				}
				
				ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nApproachTrk);
				
				if (!pTrkInfo->TRACK)
				{
					bApproachTrkOccupied = TRUE;
				}
				
				// APPROACH TRACK와 BERTH TRACK을 동일하게 한다.
				//  			if (m_bIsYudo)  // bIsYudo일 경우 loop를 1번만 적용한다...
				// 				{
				//  					break;
				// 				}
			}
			
			// Siding이 접근궤도인 경우 APPROACH TRACK이 없으므로 무조건 접근쇄정을 잡는다.
			if(cTkOffset == RT_OFFSET_APPRCHTK_BEGIN)
			{
				BYTE nFromTk = pInfo[RT_OFFSET_FROM];
				
				if(m_pTrackInfoTable[nFromTk].bNOTRACK)
				{
					bApproachTrkOccupied = TRUE;
				}
			}

			if (!SG_Timer && m_SignalInfo.ENABLED) 
			{
			// 신호기가 clear인 경우만 접근 허용.... -> 신호가 한번 설정 후 정지 된 경우는 Approach lock을 허용한다....
			/*
			if (m_SignalInfo.SIN1 || m_SignalInfo.SIN2 || m_SignalInfo.SIN3 || m_SignalInfo.U)
			{
			*/
				SG_Timer = pInfo[RT_OFFSET_APPRCH_TIME];

#ifdef _CHOTEST
				SG_Timer /= 10;
#endif

				// 20151126 sjhong - Approach Lock Timer 값이 존재할때만 ERR 카운트를 증가시킨다.
				// Outer Home Main이나 중계 신호기들은 카운트하지 않는다.
				if(bApproachTrkOccupied && !(m_pRouteQue[LoopCountH+1] & ROUTE_DEPENDSET))
				{
					DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;
#ifdef _WINDOWS
					WORD wCounter = pHead->Counter[EmerCount];
					
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[EmerCount] = wCounter;
#endif
					pHead->Counter[EmerCount]++;  // Counter를 위해 저장
					if (pHead->Counter[EmerCount] == 10000)
						pHead->Counter[EmerCount] = 0;
#ifdef _WINDOWS
					wCounter = pHead->Counter[EmerCount];
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[EmerCount] = wCounter;
#endif
				}
			}
		}

// 		// 2007.7.18  열차가 폐색에서 접근 중일 경우 Approach locking을 설정한다....
// 		if (m_pSignalInfoTable[LoopCountH+1].pApproach[11])
// 		{
// 			ScrInfoBlock *pBlockInfo = (ScrInfoBlock *)int_get(SignalGet, m_pSignalInfoTable[LoopCountH+1].pApproach[11]);
// 			if (pBlockInfo->DEPARTIN)
// 			{
// 				noaccup = 0;
// 			}
// 		}

		BYTE nFirstTrackID = m_pSignalInfoTable[LoopCountH+1].nFirstTrackID;
		ScrInfoTrack *pTrkInfo = NULL;

		if (nFirstTrackID) 
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nFirstTrackID);

		short bRoute = 1, bTkOcc = 1;
		if (pTrkInfo)
		{
			bRoute = pTrkInfo->ROUTE;
			bTkOcc = pTrkInfo->TRACK;
		}
		
        if ( SG_Timer ) 
		{
	        if (_Time1Sec == 0)
		        SG_Timer--;

			if ( SG_Timer ) 
			{
		        if (!bApproachTrkOccupied || bRoute || !bTkOcc) {	
			        SG_Timer = 0;
		        }
	        }
			OnSGTimerTick = SG_Timer;      
        }

		m_SignalInfo.ON_TIMER = ( SG_Timer != 0 );


		BYTE nOhomeClearCheck = 0; 
		if ( (nSigType == SIGNALTYPE_OHOME) && !m_bIsYudo )
		{
			nOhomeClearCheck = 1;
			bTkOcc = 1;
		}

		if ( !m_SignalInfo.ON_TIMER && (bTkOcc || m_SignalInfo.TM1) ) 
		{
            BOOL bTkFree = !(m_wCheckResult & ROUTECHECK_TRACK);

            if ( !bTkFree && m_bIsYudo /* && !(m_wCheckResultNext & ROUTECHECK_TRACK)*/) 
				bTkFree = TRUE;

			if ( (m_wRouteNo & ROUTE_MASK) && (bTkFree || (m_wRouteNo & ROUTE_ONPROCESS) /*|| m_SignalInfo.TM1*/ || nOhomeClearCheck) )  // #define ROUTE_MASK		0x1ff
			{
                CancelRoute( m_wRouteNo );
        		m_wRouteNo = 0;   //계속사용하고 있는 m_wRouteNo 초기화 
            }
		}
	}
}



void CEipEmu::TrackProcess() 
{

	//ScrInfoTrack  LastTrackInfo;
	//*(BYTE*)&LastTrackInfo = 0;
	
	LoopCountL = (BYTE)m_TableBase.InfoTrack.nCount;

	for (LoopCountH=0; LoopCountH<LoopCountL; LoopCountH++)
	{
		BYTE nTkID = LoopCountH + 1;

        int_get(TrackLoad, nTkID);

		BOOL accup = FALSE;
		
		// SDG track  occupy process....
		if (m_pTrackInfoTable[nTkID].bNOTRACK) 
		{   
			if(!m_ConfigList.BLK_DEV_MODE)
			{
				m_TrackInfo.TRACK = 0;
				m_TrackInfo.INHIBIT = 1;
			}
		}

		if ( m_TrackInfo.TRACK )
		{
			m_TrackInfo.ALARM = 0;
		}

		if(m_TrackInfo.RESETIN && !m_TrackInfo.RESETBUF)
		{
			m_TrackInfo.RESETBUF = 1;
		}

		// 2004.3.17 진로의 마지막 궤도 도착시에는 m_TrackInfo.ROUTE가 1이 되므로 Overlap 해정을 위해 보완...
		if ( !m_TrackInfo.ROUTE || (m_TrackInfo.OVERLAP && !m_TrackInfo.TKREQUEST) )
		{
			BYTE nTrackFrom = m_pTrackPrev[ nTkID ];  // Route방향의 이전 궤도... --> RunRoute logic에서 만들어 진다.
			ScrInfoTrack *pTrkInfoFrom = NULL;

			if (nTrackFrom)
			{
			    pTrkInfoFrom = (ScrInfoTrack *)int_get(TrackGet, nTrackFrom);              
			}

			if (!m_TrackInfo.TRACK) 
			{
				if (pTrkInfoFrom) //  인접 궤도의 상태를 check하여 인접궤도도 점유중일 때 오버랩 설정
				{
					if (!pTrkInfoFrom->TRACK && (pTrkInfoFrom->OVERLAP || ((pTrkInfoFrom->RouteNo & ROUTE_MASK) != (m_TrackInfo.RouteNo & ROUTE_MASK))))
					{
						// 2007.7.18  비상해정 중에는 연속 점유 조건 설정을 하지 않는다.
						// 20140404 sjhong - 연속 점유 조건(OVERLAP) 설정 시 DISTURB를 체크하도록 함.
						if (m_TrackInfo.EMREL == FALSE && m_TrackInfo.ROUTE == FALSE && m_TrackInfo.DISTURB == TRUE)
						{
                            m_TrackInfo.OVERLAP = 1;  // 즉 이전궤도가 점유되고 자신의 궤도가 점유되면 Overlap이 1....
						}
                    }
				}
				else
				{
					// 20140404 sjhong - 연속 점유 조건(OVERLAP) 설정 시 DISTURB를 체크하도록 함.
					if(m_TrackInfo.EMREL == FALSE && m_TrackInfo.ROUTE == FALSE && m_TrackInfo.DISTURB == TRUE)
					{
						m_TrackInfo.OVERLAP = 1;
					}
				}
			}

			BYTE nFromFree = 1;
			BYTE bIsDestTrack = 0;

			if ( pTrkInfoFrom )
			{
				if ( m_pTrackInfoTable[ nTrackFrom ].bDESTTRACK )
				{
					// 2006.11.16  Preset, Ishunt를 가진 진로는 Outermain을 가진 main진로와는 달리 신호기에 Desttrack을 최종 종착점으로 설정한다.
					short nSignalCount = m_TableBase.InfoSignal.nCount;
					BOOL bFindRoute = FALSE;
					for (int sc=1; sc<=nSignalCount; sc++)
					{
						ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, sc);
						if ( (pSigInfo->DESTTRACK & 0x7f) == nTrackFrom )
						{
							bFindRoute = TRUE;
							break;
						}
					}

					if (bFindRoute)
					{
						nFromFree = pTrkInfoFrom->TRACK;
					}
					else
					{
						nFromFree = pTrkInfoFrom->TRACK && pTrkInfoFrom->ROUTE;
					}
				}
				else 
				{
					nFromFree = (pTrkInfoFrom->ROUTE || !pTrkInfoFrom->TKREQUEST) && pTrkInfoFrom->TRACK;
				}

				// SDG track process.....
				// 2006.6.16   SDG 진로 열차 출발 시  진로 해정 및 비상진로 해정을 위한 처리...
 				if ( (m_pTrackInfoTable[nTrackFrom].bNOTRACK) && m_TrackInfo.TRACK/* && m_TrackInfo.OVERLAP*/) 
 				{	
					nFromFree = 1;
 				}
			}

			if (  pTrkInfoFrom && nFromFree && m_TrackInfo.OVERLAP && !pTrkInfoFrom->TKREQUEST )
			{
                m_pTrackPrev[ nTkID ] = 0;
			}

			if (m_pTrackInfoTable[ nTkID ].bDESTTRACK) 
			{
				if ( (m_pLastTrack[ nTkID ] & TRACK_MASK) == nTkID )
				{
					bIsDestTrack = 1;
				}
			}

// 			if ( ((m_pTrackInfoTable[ nTkID ].bLEFT2NDDEST && !m_TrackInfo.ROUTEDIR) || (m_pTrackInfoTable[ nTkID ].bRIGHT2NDDEST && m_TrackInfo.ROUTEDIR)) && m_TrackInfo.OVERLAP) 
// 			{
// 				// 모든 신호기를 조사 하여 신호기의 Desttrack에 자신을 가지고 있으면, 처리하지 않는다. - 진로 설정 중임...
//  				short nSignalCount = m_TableBase.InfoSignal.nCount;
//  				BOOL bFindRoute = FALSE;
//  				for (int sc=1; sc<=nSignalCount; sc++)
//  			    {
//  					ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get(SignalGet, sc);
//  					if ( (pSigInfo->DESTTRACK & 0x7f) == nTkID )
//  					{
//  						bFindRoute = TRUE;
//  						break;
//  					}
//  				}
//  
//  				if (!bFindRoute)
// 				{
//  				    nNoDualDestination = 1;
// 				}
// 			}

			// 20130426 sjhong - 비상해정 시 순차 궤도점유하면 Route가 해정되는 현상 방지
			if ((m_TrackInfo.TRACK || bIsDestTrack) && m_TrackInfo.EMREL == FALSE)
			{
				if ( (nFromFree && m_TrackInfo.OVERLAP) )
				{
					BYTE nNextTkID = m_pTrackNext[nTkID];
					if(nNextTkID)
					{
						ScrInfoTrack *pNextTkInfo = NULL;
						pNextTkInfo = (ScrInfoTrack *)int_get(TrackGet, nNextTkID);
						if(pNextTkInfo && !pNextTkInfo->TRACK)
						{
							m_TrackInfo.ROUTE = 1;		// 열차의 진행에 의해 진로를 해정함.... --> Green color 삭제...

							m_TrackInfo.RouteNo = 0;
						}
					}
					else
					{
						m_TrackInfo.ROUTE = 1;		// 열차의 진행에 의해 진로를 해정함.... --> Green color 삭제...
					}
				}
			}
		}


        else if ( m_pTrackInfoTable[ nTkID ].bALARM && !m_TrackInfo.OVERLAP ) 
		{
			SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;

			if ( m_TrackInfo.TRACK)  
			{
				m_TrackInfo.ALARM = 0;
			}
			else 
			{
				BYTE &tktimer = *(BYTE*)&m_pTrackTimer[nTkID];  // OCC time - 점유시간 저장...

				if ( !m_TrackInfo.ALARM ) 
				{
					tktimer &= 0x0F;
					tktimer |= 0x80;
					m_TrackInfo.ALARM = 1;
				}

				if ( tktimer & 0x80 ) 
				{
					//tktimer += 0x10;
					if(!m_ConfigList.BLK_DEV_MODE)
					{
						nSysVarExt.bSound1 = 1;		// bSound_App
					}
				}
			}
		}

		if(!m_TrackInfo.TRACK)  // 궤도가 점유 상태일떄의 처리...
		{
			if(m_pLastTrack[ nTkID ])
			{
				// 20140904 sjhong - 종착궤도 점유 이면서 직전 궤도가 해정될 경우의 Overlap 처리를 위해 추가.
				if(nTkID == (m_pLastTrack[nTkID] & TRACK_MASK))
				{
					// 일단 TRUE로 만든다.
					BOOL bIsArriveDestTrack = TRUE;
					BOOL bIsStayTimeout	= FALSE;

					ScrInfoTrack *pPrevTkInfo = NULL;
					BYTE nPrevTkID = m_pTrackPrev[nTkID];
					while(nPrevTkID)
					{
						pPrevTkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTkID);

						if(pPrevTkInfo->MULTIDEST)
						{
							if(!pPrevTkInfo->OVERLAP)	// 인접 궤도에 연속 점유 정보가 없으면 처리하지 않는다. (부정낙하 등)
							{
								bIsArriveDestTrack = FALSE;
								break;
							}
						}
						else
						{
							// 다중 종착궤도가 아니면서 m_TrackPrev에 존재하는 Track이 있음. 
							break;
						}

						nPrevTkID = m_pTrackPrev[nPrevTkID];
					}

					// 첫 종착궤도의 인접 궤도가 점유해정되었는지 체크한다.
					if(nPrevTkID > 0)
					{
						bIsArriveDestTrack = FALSE;
					}
					else
					{
						if(m_pLastTrack[nTkID] & TRACK_STAY_DEST)
						{
							bIsArriveDestTrack = FALSE;

							ScrInfoTrack *pNextTkInfo = NULL;
							BYTE nNextTkID = m_pTrackNext[nTkID];

							if((nNextTkID > 0) && (nNextTkID == m_pLastTrack[nNextTkID]))
							{
								// 다음 궤도가 종착궤도이면서, 순차 점유되어 있으면, 쳬류 해정 처리하지 않는다.
								pNextTkInfo = (ScrInfoTrack *)int_get(TrackGet, nNextTkID);
								if(!pNextTkInfo->TRACK && pNextTkInfo->OVERLAP)
								{
									bIsArriveDestTrack = TRUE;
								}
							}
													
							BYTE &stay_timer = m_pTrackExtraTimer[nTkID];

							if(bIsArriveDestTrack == TRUE)
							{
								stay_timer = 0;
							}
							else
							{
#ifdef _CHOTEST
								if(stay_timer > (TRACK_STAY_DEST_TIME / 10))
#else
								if(stay_timer > TRACK_STAY_DEST_TIME)
#endif
								{
									stay_timer = 0;
									bIsStayTimeout = TRUE;
								}
								else
								{
									if(_Time1Sec == 0)
									{
										stay_timer++;
									}
								}
							}
						}
					}

					// 순차 진행에 의해 끝 종착 궤도가 점유되었으면서 종착 직전 궤도가 해정된 경우
					if(m_TrackInfo.OVERLAP && (bIsArriveDestTrack || bIsStayTimeout))
					{
						if(m_TrackInfo.RouteNo)
						{
							WORD wMainRouteNo = m_TrackInfo.RouteNo & ROUTE_MASK;

							for(int i = 0 ; i < N_SG_COUNT ; i++)
							{
								// 중계신호기용 정보는 보존하고, 전방신호기용 정보만 삭제한다.
								if(((m_pOwnerRouteQue[i] & ROUTE_MASK) == wMainRouteNo) && !(m_pOwnerRouteQue[i] & ROUTE_DEPENDSET))
								{
									// 진로가 완전히 해정되므로 여기서 초기화한다.
									m_pOwnerRouteQue[i] = 0;
									break;
								}
							}
						}

						m_TrackInfo.TKREQUEST = 0;
						m_TrackInfo.RouteNo = 0;
						m_TrackInfo.ROUTEDIR = 0;
						m_TrackInfo.ROUTE = 1;
						
						nPrevTkID = m_pTrackPrev[nTkID];
						while(nPrevTkID)
						{
							pPrevTkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTkID);
							
							if(pPrevTkInfo->MULTIDEST)
							{
								pPrevTkInfo->RouteNo = 0;
								pPrevTkInfo->ROUTEDIR = 0;
								pPrevTkInfo->ROUTE = 1;
								m_pLastTrack[nPrevTkID] = 0;
							}
							else
							{
								break;
							}
							
							nPrevTkID = m_pTrackPrev[nPrevTkID];
						}

						if((m_pLastTrack[nTkID] & TRACK_STAY_DEST) && bIsStayTimeout)
						{
							ScrInfoTrack *pNextTkInfo = NULL;
							BYTE nNextTkID = m_pTrackNext[nTkID];

							// 체류종착궤도는 진로 중간에 있으므로 그 이후 궤도를 Clear 해준다.
							while(nNextTkID)
							{
								pNextTkInfo = (ScrInfoTrack *)int_get(TrackGet, nNextTkID);
								pNextTkInfo->RouteNo = 0;
								pNextTkInfo->ROUTEDIR = 0;
								pNextTkInfo->ROUTE = 1;
								nNextTkID = m_pTrackNext[nNextTkID];
							}
						}

						m_pLastTrack[nTkID] = 0;

						// 20140902 sjhong - Overlap 궤도 중 전철기가 없는 경우에 대한 Overlap 해정을 담당.. 
						BYTE bIsSwitchInOverlap = FALSE;
						BYTE nOverTkID = m_pOverTrack[nTkID] & TRACK_MASK;
						BYTE nRouteDirection = m_pOverTrack[nTkID] & TRACK_DIR_MASK;
							
						if(nOverTkID)
						{
							// 오버랩 트랙 중 전철기가 있는 트랙이 있는지 검사
							BYTE nSeekTkID = nOverTkID;
							while(nSeekTkID != 0)
							{
								if(m_pTrackInfoTable[nSeekTkID].bSWITCHTRACK)
								{
									bIsSwitchInOverlap = TRUE;
									break;
								}

								nSeekTkID = m_pOverTrack[nSeekTkID] & TRACK_MASK;
							}
						
							// 전철기가 있는 트랙이 없으면 바로 Overlap을 삭제한다.
							if(bIsSwitchInOverlap == FALSE)
							{
								ScrInfoTrack *pOverTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nOverTkID);
								if (nRouteDirection)
								{
									// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
									ClearOverlap(pOverTrkInfo->RightToLeftRouteNo, 0);
								}
								else
								{
									// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
									ClearOverlap(pOverTrkInfo->LeftToRightRouteNo, 0);
								}
								
								// Overlap Track 테이블 초기화.
								BYTE nNextTrkID = 0;
								BYTE nDelTrkID = nTkID & TRACK_MASK;
								while(nDelTrkID != 0)
								{
									nNextTrkID = m_pOverTrack[nDelTrkID];
									m_pOverTrack[nDelTrkID] = 0;
									nDelTrkID = nNextTrkID & TRACK_MASK;
								}
							}
						}
					}
				}
				else
				{
					ScrInfoTrack *pTrkDest = (ScrInfoTrack *)int_get(TrackGet, (m_pLastTrack[ nTkID ] & TRACK_MASK));
					// 20140404 sjhong - 연속 점유 조건(OVERLAP) 설정 시 DISTURB를 체크하도록 함.
					if (pTrkDest->TRACK == FALSE && pTrkDest->DISTURB == TRUE && pTrkDest->EMREL == FALSE)
					{
						pTrkDest->OVERLAP = 1;
					}
				}
			}
		}
		else 
		{
			m_TrackInfo.OVERLAP = 0;
			
			// 2012.11.29 추가...
			// 20140522 sjhong - Overlap 궤도가 2개 이상인 진로의 순차 진행 점유 후 종착 궤도가 해정되면 Overlap 궤도가 일부 남는 현상에 대한 보완.
			if (((m_pLastTrack[nTkID] & TRACK_MASK) == nTkID) && !m_TrackInfo.TKREQUEST && m_TrackInfo.ROUTE)
			{
				BYTE nOverTrkID = m_pOverTrack[nTkID] & TRACK_MASK;
				BYTE nRouteDirection = m_pOverTrack[nTkID] & TRACK_DIR_MASK;
				if (nOverTrkID)
				{
					ScrInfoTrack *pOverTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nOverTrkID);

					if (nRouteDirection)
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
						ClearOverlap(pOverTrkInfo->RightToLeftRouteNo, 0);
					}
					else
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
						ClearOverlap(pOverTrkInfo->LeftToRightRouteNo, 0);
					}

					// Overlap Track 테이블 초기화.
					BYTE nNextTrkID = 0;
					BYTE nDelTrkID = nTkID & TRACK_MASK;
					while(nDelTrkID != 0)
					{
						nNextTrkID = m_pOverTrack[nDelTrkID];
						m_pOverTrack[nDelTrkID] = 0;
						nDelTrkID = nNextTrkID & TRACK_MASK;
					}
				}				
			}	
			
		}

		if(m_pTrackStick[nTkID])
		{		
			if(!m_TrackInfo.TKREQUEST && !m_TrackInfo.OVERLAP && m_TrackInfo.ROUTE && m_TrackInfo.TRACK)	// 궤도해정...
			{
				// 2007.3.7  다른 진로로 Level crossing이 설정되어 있는지 조사한다...
				BOOL bOtherTkUsed = 0;
 				for(int nOtherTkID = 1; nOtherTkID <= LoopCountL; nOtherTkID++)
 				{
 					if(nTkID == nOtherTkID)
					{
 						continue;
					}
					
					if((m_pTrackInfoTable[nTkID].bLC1 & m_pTrackInfoTable[nOtherTkID].bLC1)
					|| (m_pTrackInfoTable[nTkID].bLC2 & m_pTrackInfoTable[nOtherTkID].bLC2)
					|| (m_pTrackInfoTable[nTkID].bLC3 & m_pTrackInfoTable[nOtherTkID].bLC3)
					|| (m_pTrackInfoTable[nTkID].bLC4 & m_pTrackInfoTable[nOtherTkID].bLC4)
					|| (m_pTrackInfoTable[nTkID].bLC5 & m_pTrackInfoTable[nOtherTkID].bLC5))
					{
						ScrInfoTrack *pOtherTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nOtherTkID);
						if(!pOtherTrkInfo->ROUTE || !pOtherTrkInfo->TRACK || pOtherTrkInfo->RightToLeftRouteNo || pOtherTrkInfo->LeftToRightRouteNo)
						{
							bOtherTkUsed = TRUE;
							break;
						}
					}
				}

				SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;
				SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;

				BYTE nNextTrackID = m_pTrackNext[nTkID];

				if (m_pTrackInfoTable[ nTkID ].bLC1 && !m_pTrackInfoTable[ nNextTrackID ].bLC1 && !m_pTrackInfoTable[ nTkID ].bLCNOTINTRK)
				{
					if (!nSysVarExt.bLC1NKXPR && !nSysVarExt.bLC1KXLR && !bOtherTkUsed) // Key가 삽입되어 있고 Off상태이면 자동으로 On상태로 만든다..
					{
						nSysVarExt.bLC1KXLR = 1;  // Key of transmitter will be automatically released
					}
				}

				if (m_pTrackInfoTable[ nTkID ].bLC2 && !m_pTrackInfoTable[ nNextTrackID ].bLC2 && !m_pTrackInfoTable[ nTkID ].bLCNOTINTRK)
				{
					if (!nSysVarExt.bLC2NKXPR && !nSysVarExt.bLC2KXLR && !bOtherTkUsed) // Key가 삽입되어 있고 Off상태이면 자동으로 On상태로 만든다..
					{
						nSysVarExt.bLC2KXLR = 1;  // Key of transmitter will be automatically released
					}
				}

				if (m_pTrackInfoTable[ nTkID ].bLC3 && !m_pTrackInfoTable[ nNextTrackID ].bLC3 && !m_pTrackInfoTable[ nTkID ].bLCNOTINTRK)
				{
					if (!nSysVarExt.bLC3NKXPR && !nSysVarExt.bLC3KXLR && !bOtherTkUsed) // Key가 삽입되어 있고 Off상태이면 자동으로 On상태로 만든다..
					{
						nSysVarExt.bLC3KXLR = 1;  // Key of transmitter will be automatically released
					}
				}

				if (m_pTrackInfoTable[ nTkID ].bLC4 && !m_pTrackInfoTable[ nNextTrackID ].bLC4 && !m_pTrackInfoTable[ nTkID ].bLCNOTINTRK)
				{
					if (!nSysVarExt.bLC4NKXPR && !nSysVarExt.bLC4KXLR && !bOtherTkUsed) // Key가 삽입되어 있고 Off상태이면 자동으로 On상태로 만든다..
					{
						nSysVarExt.bLC4KXLR = 1;  // Key of transmitter will be automatically released
					}
				}

				if (m_pTrackInfoTable[ nTkID ].bLC5 && !m_pTrackInfoTable[ nNextTrackID ].bLC5 && !m_pTrackInfoTable[ nTkID ].bLCNOTINTRK)
				{
					if (!nSysVar.bLC5NKXPR && !nSysVar.bLC5KXLR && !bOtherTkUsed) // Key가 삽입되어 있고 Off상태이면 자동으로 On상태로 만든다..
					{
						nSysVar.bLC5KXLR = 1;  // Key of transmitter will be automatically released
					}
				}

				m_pTrackStick[nTkID] = 0;
			}
		}
		
		if(m_pTrackInfoTable[ nTkID ].bLC1)
		{
			if (!m_TrackInfo.TRACK && !m_TrackInfo.ROUTE)
			{
				m_pTrackStick[nTkID] |= FLAG_LC1;
			}
		}

		if(m_pTrackInfoTable[ nTkID ].bLC2)
		{
			if (!m_TrackInfo.TRACK && !m_TrackInfo.ROUTE)
			{
				m_pTrackStick[nTkID] |= FLAG_LC2;
			}
		}

		if(m_pTrackInfoTable[ nTkID ].bLC3)
		{
			if (!m_TrackInfo.TRACK && !m_TrackInfo.ROUTE)
			{
				m_pTrackStick[nTkID] |= FLAG_LC3;
			}
		}
		
		if(m_pTrackInfoTable[ nTkID ].bLC4)
		{
			if (!m_TrackInfo.TRACK && !m_TrackInfo.ROUTE)
			{
				m_pTrackStick[nTkID] |= FLAG_LC4;
			}
		}

		if(m_pTrackInfoTable[ nTkID ].bLC5)
		{
			if (!m_TrackInfo.TRACK && !m_TrackInfo.ROUTE)
			{
				m_pTrackStick[nTkID] |= FLAG_LC5;
			}
		}

		if (m_TrackInfo.RESETOUT)
		{
			if(_Time1Sec == 0)
			{
				m_pTrackExtraTimer[nTkID]++;
			}
				
			if(m_pTrackExtraTimer[nTkID] > 5)
			{
				m_pTrackExtraTimer[nTkID] = 0;
					
				m_TrackInfo.RESETOUT = 0;
			}
		}

		int_put(TrackPut);
    }

    return;
}



void CEipEmu::SwitchProcess() 
{
    SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;

    BOOL bMatch = 0;
	BYTE trkid, nID;
	short isfreeR2L, isfreeL2R, istrack, isroute, isoverlap, n;

	LoopCountL = (BYTE)m_TableBase.InfoSwitch.nCount;

	for (LoopCountH=0; LoopCountH<LoopCountL; LoopCountH++)
	{
        nID = LoopCountH + 1;
		n = 0;
		istrack = 1;  
		isroute = 0;
		isoverlap = 0;

		int_get (SwitchLoad, nID);

		SW_Timer = m_pSwitchTimer[ nID ];


		if ( !m_SwitchInfo.WKEYKR ) 
		{
			m_SwitchInfo.REQUEST_P = 0;
			m_SwitchInfo.REQUEST_M = 0;
		    SW_Timer &= 0x7F;
			m_SwitchInfo.WRO_P = 0;
			m_SwitchInfo.WRO_M = 0;
            m_SwitchInfo.WLRO = 0;
		}

		BYTE nSwitchType = m_pSwitchInfoTable[nID].nSwitchType;
		// Hand point의 경우 WR 입력 정보가 없으므로 KR 입력으로 강제로 만들어 준다.
		if (nSwitchType == 0x30)
		{
			m_SwitchInfo.WR_P = m_SwitchInfo.KR_P;
			m_SwitchInfo.WR_M = m_SwitchInfo.KR_M;   //2009.10.25 추가..  아카우라 124전철기 반위 방향 Detect를 위해 
		}

		// 철사 쇄정 검사...
		while (trkid=m_pSwitchOnTrackTable[nID].pData[n], trkid && n<2)  // 2개의 궤도까지만 조사..
		{
			ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, trkid);

			if(!m_pTrackInfoTable[trkid].bNOTRACK)
			{
				if ( !pTrkInfo->TRACK || pTrkInfo->INHIBIT ) 
				{
					istrack = 0;
				}

				if (!pTrkInfo->ROUTE)
					isroute = 1;

				if (pTrkInfo->OVERLAP)
					isoverlap = 1;
			}

			n++;
		}

		m_SwitchInfo.TRACKLOCK = (BYTE)istrack;   // 0인 경우 철사 쇄정 상태이다..

		//2006.03.06  hand switch가 아닌 경우에만 적용함...  LSM에서 출발진로 설정시 home에 위치한 hand point를 알기 위해 수정...
		if (nSwitchType != 0x30)
		{
			m_SwitchInfo.INROUTE = (BYTE)isroute;
		}

		UnitStatusType &UnitState = ((DataHeaderType *)m_pVMEM)->UnitState;
		if ( !UnitState.bSysRun )
		{
			m_SwitchInfo.REQUEST_P = 0;
			m_SwitchInfo.REQUEST_M = 0;
		    SW_Timer &= 0x7F;

			m_SwitchInfo.WRO_P = 0;
			m_SwitchInfo.WRO_M = 0;
            m_SwitchInfo.WLRO  = 0;

		    SwitchUpdate(nID, istrack);
			continue;
		}


		// 2005.12.26 전철기가 고장났을 경우 진로를 설정하여 전철기를 전환한다.
		// 이때 진로를 전철기 전환시간인 12초 보다 먼저 해정하면 REQUEST_P등의 정보가 0으로 바뀐다.
		// 그러면 아래의 로직을 수행하지 못하므로 WLRO정보로를 추가로 참도하도록 함.
        if ( m_SwitchInfo.REQUEST_P || m_SwitchInfo.REQUEST_M || m_SwitchInfo.WLRO)
		{
			// 요구방향과 WR계전기의 방향이 틀림 (요구 방향대로 WR계전기가 동작하지 않음...)
			if ((m_SwitchInfo.WR_P != m_SwitchInfo.REQUEST_P) || (m_SwitchInfo.WR_M != m_SwitchInfo.REQUEST_M)) 
			{
				
				if ( SW_Timer & 0x80 ) 
				{
					/*
					if ( !( SW_Timer & 0x08 ) ) 
						SW_Timer++;

					if ( SW_Timer & 0x08 ) 
					{
						SetSwitchFail(nID);
						m_SwitchInfo.REQUEST_P = 0;
						m_SwitchInfo.REQUEST_M = 0;
					}
					*/

					// 2005.09.17 위의 로직과 아래 로직의 SW_Timer를 Comment처리하고 아래 사항으로 대치...
					// 2005.09.17  전철기 출력 끊는 시간을 단축 48은 약 20초 정도 소요됨. 48/20=2.4  즉 1초에 약 2.4회 로직 수행됨.
					// 그러므로 0x20=32/2.4=13 sec 로 수정 함...
					// 20151112 sjhong 전철기 전환 출력 시간을 7초로 변경함 (7 * 2.4 = 17 = 0x11). 단, LIS에서는 기존대로 동작하도록 함.
#ifdef _WINDOWS
					if (!( (SW_Timer & 0x7f) == 0x20 ))
#else
					if (!( (SW_Timer & 0x7f) == 0x11 ))	// 7 Sec
#endif
					{
						SW_Timer++;
					}

#ifdef _WINDOWS
					if ( (SW_Timer & 0x7f) >= 0x20 ) 
#else
					if ( (SW_Timer & 0x7f) >= 0x11 ) 
#endif
					{
						SetSwitchFail(nID);
						m_SwitchInfo.REQUEST_P = 0;
						m_SwitchInfo.REQUEST_M = 0;

						m_SwitchInfo.WRO_P = 0;   // 전환실패,  출력을 끊는다....
						m_SwitchInfo.WRO_M = 0;
						m_SwitchInfo.WLRO  = 0;
					}

				}				
				else if ( SW_Timer>0 ) {	// 시간차를 위해 설정한다...
					SW_Timer--;
				}
				else if ( m_SwitchInfo.TRACKLOCK ) 
				{
					m_SwitchInfo.WRO_P = m_SwitchInfo.REQUEST_P;  // WRO정보(Output정보)를 Seting 
					m_SwitchInfo.WRO_M = m_SwitchInfo.REQUEST_M;

					m_SwitchInfo.WLRO = 1;
					m_SwitchInfo.ROUTELOCK = 0;
					m_SwitchInfo.FREE = 1;  
					m_SwitchInfo.SWFAIL = 0;

    				SW_Timer = 0x80;  // 철사쇄정이 아님을 표시...
				}

				// 2007.7.18  전철기 전환 중에 철사쇄정이 걸려도 전철기는 진행 방향으로 모두 전환 시킨다.
				else if ( !m_SwitchInfo.TRACKLOCK && (m_SwitchInfo.WRO_P || m_SwitchInfo.WRO_M) ) 
				{
					m_SwitchInfo.WRO_P = m_SwitchInfo.REQUEST_P;  // WRO정보(Output정보)를 Seting 
					m_SwitchInfo.WRO_M = m_SwitchInfo.REQUEST_M;

					m_SwitchInfo.WLRO = 1;
					m_SwitchInfo.ROUTELOCK = 0;
					m_SwitchInfo.FREE = 1;  
					m_SwitchInfo.SWFAIL = 0;

    				SW_Timer = 0x80;  // 철사쇄정이 아님을 표시...
				}


		        SwitchUpdate(nID, istrack);
				continue;
            }

            else // WRI ON 상태..
			{ 
				if(m_SwitchInfo.KR_P == m_SwitchInfo.WR_P && m_SwitchInfo.KR_M == m_SwitchInfo.WR_M)
				{
					m_SwitchInfo.WRO_P = 0;   // 출력을 끊는다....
					m_SwitchInfo.WRO_M = 0;
					m_SwitchInfo.WLRO  = 0;

		            m_SwitchInfo.SWFAIL = 0;
   	    			SW_Timer = 0;
				}

				// 전환되지 않은 경우 8초 후에 Fail로 처리한다..
				else 
				{
					// 2005.09.17  전철기 출력 끊는 시간을 단축 48은 약 20초 정도 소요됨. 48/20=2.4  즉 1초에 약 2.4회 로직 수행됨.
					// 그러므로 0x20=32/2.4=13 sec 로 수정 함...
					// 20151112 sjhong 전철기 전환 출력 시간을 7초로 변경함 (7 * 2.4 = 17 = 0x11). 단, LIS에서는 기존대로 동작하도록 함.
#ifdef _WINDOWS
					if (!((SW_Timer & 0x7f) == 0x20))
#else
					if (!((SW_Timer & 0x7f) == 0x11))	// 7 Sec
#endif
					{
						SW_Timer++;
					}

#ifdef _WINDOWS
					if ((SW_Timer & 0x7f) >= 0x20) 
#else
				    if ((SW_Timer & 0x7f) >= 0x11) 
#endif
					{
						SetSwitchFail(nID);
						m_SwitchInfo.REQUEST_P = 0;
						m_SwitchInfo.REQUEST_M = 0;

						m_SwitchInfo.WRO_P = 0;   // 전환실패,  출력을 끊는다....
						m_SwitchInfo.WRO_M = 0;
						m_SwitchInfo.WLRO  = 0;
		            }
					// 2013.3.12 추가...
					else if (SW_Timer==1)
					{
						if (!m_SwitchInfo.WRO_P && !m_SwitchInfo.WRO_M)
						{
							SetSwitchFail(nID);
							m_SwitchInfo.REQUEST_P = 0;
							m_SwitchInfo.REQUEST_M = 0;
							m_SwitchInfo.WLRO  = 0;
						}
					}
	            }
            }
        }
        else 
		{
			if ( !(m_SwitchInfo.KR_P == m_SwitchInfo.WR_P && m_SwitchInfo.KR_M == m_SwitchInfo.WR_M)
				   || (!m_SwitchInfo.KR_P && !m_SwitchInfo.WR_P && !m_SwitchInfo.KR_M && !m_SwitchInfo.WR_M) ) 
			{
				SetSwitchFail(nID);
			}
			else 
			{
				m_SwitchInfo.SWFAIL = 0;
				m_pSwitchFail[nID] = 0; // 경보 Bell을 위한 복귀...  
			}
        }

		if ((m_SwitchInfo.WRO_P && (m_SwitchInfo.WRO_P != m_SwitchInfo.WR_P)) || 
			(m_SwitchInfo.WRO_M && (m_SwitchInfo.WRO_M != m_SwitchInfo.WR_M)) ) 
		{
			m_SwitchInfo.WRO_P = 0;
			m_SwitchInfo.WRO_M = 0;
			m_SwitchInfo.WLRO = 0;
			SetSwitchFail(nID);
		}



		if( m_SwitchInfo.WRO_P == m_SwitchInfo.WR_P && m_SwitchInfo.WRO_M == m_SwitchInfo.WR_M &&
			(m_SwitchInfo.KR_P == m_SwitchInfo.WR_P && m_SwitchInfo.KR_M == m_SwitchInfo.WR_M))
		{
			m_SwitchInfo.WRO_P = 0;
			m_SwitchInfo.WRO_M = 0;
			m_SwitchInfo.WLRO = 0;
		}


		if ( m_SwitchInfo.WLR && !m_SwitchInfo.WLRO ) 
		{

			if ( SW_Timer & 0x40 ) // bit 6
			{	
				if ( !UnitState.bSysFail )
				{
					SW_Timer++;
					if ( SW_Timer & 0x08 ) {	// bit3	08 (약 2초) -- Time delay를 위해 설정..
						SetErrorCode( 0x40, &m_pMsgBuffer[8] );	// Switch Fail -- Alarm 발생.
						SW_Timer = 0;  // 2004.03.03 추가 -- Timer 초기화
					}
				}
			}
			else 
			{
				SW_Timer = 0x40;	//; 처음-- Bit6을 seting한다...
			}
		}

		// 전철기(쌍동)의 경우 진로와 오버랩이 중첩되거나 오버랩과 오버랩이 중첩될 경우, 궤도가 점유된 경우, 쇄정 표시가 깜박이지 않게 한다.
		if(m_pSwitchOnTrackTable[nID].pData[0] && m_pSwitchOnTrackTable[nID].pData[1])
		{
			ScrInfoTrack *pTrkInfo1 = (ScrInfoTrack *)int_get(TrackGet, m_pSwitchOnTrackTable[nID].pData[0]);
			ScrInfoTrack *pTrkInfo2 = (ScrInfoTrack *)int_get(TrackGet, m_pSwitchOnTrackTable[nID].pData[1]);
			
			if((pTrkInfo1->RightToLeftRouteNo && pTrkInfo2->RightToLeftRouteNo && (pTrkInfo1->RightToLeftRouteNo != pTrkInfo2->RightToLeftRouteNo))	// 좌측 방향 진로의 오버랩이 전철기의 반위방향으로 날 경우
			|| (pTrkInfo1->LeftToRightRouteNo && pTrkInfo2->LeftToRightRouteNo && (pTrkInfo1->LeftToRightRouteNo != pTrkInfo2->LeftToRightRouteNo))	// 우측 방향 진로의 오버랩이 전철기의 반위방향으로 날 경우
			|| (pTrkInfo1->RightToLeftRouteNo && pTrkInfo2->LeftToRightRouteNo)	// 서로 다른 두 진로의 오버랩이 중첩될 경우
			|| (pTrkInfo1->LeftToRightRouteNo && pTrkInfo2->RightToLeftRouteNo)	// 서로 다른 두 진로의 오버랩이 중첩될 경우
			|| (pTrkInfo1->RouteNo && (pTrkInfo2->RightToLeftRouteNo || pTrkInfo2->LeftToRightRouteNo))		// 서로 다른 두 진로의 진로와 오버랩이 중첩될 경우
			|| (pTrkInfo2->RouteNo && (pTrkInfo1->RightToLeftRouteNo || pTrkInfo1->LeftToRightRouteNo))		// 서로 다른 두 진로의 진로와 오버랩이 중첩될 경우
			|| !m_SwitchInfo.TRACKLOCK)
			{
				m_SwitchInfo.PASSROUTE = 1;
			}
			else
			{
				m_SwitchInfo.PASSROUTE = 0;
			}
		}

		isfreeR2L = isfreeL2R = !m_SwitchInfo.ON_TIMER;
		
// 진로 쇄정 검사...   
        ScrInfoTrack *pTrkInfo;
		BYTE nTrkID;

        nTrkID = m_pSwitchRouteTrack[nID * 2];    // 전철기 궤도 조사...
        if (nTrkID) 
		{
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nTrkID);

		    if ( pTrkInfo->ROUTE == 0 || pTrkInfo->TKREQUEST ) 
			{
			    isfreeR2L = 0;  // overlap을 0으로 한다... --> overlap을 걸어둔다...
				isfreeL2R = 0;
		    }

			if(pTrkInfo->RouteNo)
			{
				DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ pTrkInfo->RouteNo & ROUTE_MASK ];
				BYTE *pInfo = &m_pInfoMem[nInfoPtr];
			    BYTE nFromTrkID = pInfo[ RT_OFFSET_FROM ];
				if(nFromTrkID == m_pSwitchOverTrack1[nID].nTrackID || nFromTrkID == m_pSwitchOverTrack2[nID].nTrackID)
				{
					m_SwitchInfo.PASSROUTE = 1;
				}
			}
        }


        nTrkID = m_pSwitchRouteTrack[nID * 2 + 1];  // 걸쳐 있는 궤도 조사...
        if (nTrkID) 
		{
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nTrkID);

 		    if ( pTrkInfo->ROUTE == 0 || pTrkInfo->TKREQUEST ) 
 			{
 			    isfreeR2L = 0;
				isfreeL2R = 0;
 		    }

			if(pTrkInfo->RouteNo)
			{
				DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ pTrkInfo->RouteNo & ROUTE_MASK ];
				BYTE *pInfo = &m_pInfoMem[nInfoPtr];
				BYTE nFromTrkID = pInfo[ RT_OFFSET_FROM ];
				if(nFromTrkID == m_pSwitchOverTrack1[nID].nTrackID || nFromTrkID == m_pSwitchOverTrack2[nID].nTrackID)
				{
					m_SwitchInfo.PASSROUTE = 1;
				}
			}
        }

		if(isfreeR2L && isfreeL2R)
		{
            m_pSwitchRouteTrack[nID * 2] = 0;	    // 진로쇄정 궤도 정보 삭제
            m_pSwitchRouteTrack[nID * 2 + 1] = 0;	// 진로쇄정 궤도 정보 삭제
		}

		if(!m_SwitchInfo.PASSROUTE)
		{
			if(m_pSyncSwitch[nID] && !(m_pSyncSwitch[nID] & SYNCSWT_MASTER))
			{
				if(m_pSwitchOnTrackTable[nID].pData[0] || m_pSwitchOnTrackTable[nID].pData[1])
				{
					ScrInfoTrack *pTrkInfo1 = (ScrInfoTrack *)int_get(TrackGet, m_pSwitchOnTrackTable[nID].pData[0]);
					ScrInfoTrack *pTrkInfo2 = (ScrInfoTrack *)int_get(TrackGet, m_pSwitchOnTrackTable[nID].pData[1]);

					if(pTrkInfo1->RouteNo || pTrkInfo1->RightToLeftRouteNo || pTrkInfo1->LeftToRightRouteNo
					|| pTrkInfo2->RouteNo || pTrkInfo2->RightToLeftRouteNo || pTrkInfo2->LeftToRightRouteNo)
					{
						m_SwitchInfo.PASSROUTE = 1;
					}
				}

				ScrInfoSwitch *pMasterSwInfo = (ScrInfoSwitch*)int_get(SwitchGet, m_pSyncSwitch[nID] & SYNCSWT_MASK);
				if(pMasterSwInfo->PASSROUTE)
				{
					m_SwitchInfo.PASSROUTE = 1;
				}
			}
		}

// Overlap 쇄정 검사...
		// 20140520 sjhong - Overlap 처리 로직 전면 수정
        BYTE nDestTrkID = m_pSwitchOverTrack1[nID].nTrackID;
        if (nDestTrkID) 
		{
			// 다중 종착 궤도 진로의 처리를 위해 추가.
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nDestTrkID);  // 도착진로의 경우 보통은 Home궤도...

			// 일단 TRUE로 만든다.
			BOOL bIsArriveDestTrack = TRUE;

			ScrInfoTrack *pPrevTrkInfo = NULL;
			BYTE nPrevTrkID = m_pTrackPrev[ nDestTrkID ];
			while(nPrevTrkID)
			{
				pPrevTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTrkID);
				
				if(pPrevTrkInfo->MULTIDEST)
				{
					if(!pPrevTrkInfo->OVERLAP)	// 인접 궤도에 연속 점유 정보가 없으면 처리하지 않는다. (부정낙하 등)
					{
						bIsArriveDestTrack = FALSE;
						break;
					}
				}
				else
				{
					// 다중 종착궤도가 아니면서 m_TrackPrev에 존재하는 Track이 있음. 
					break;
				}
				
				nPrevTrkID = m_pTrackPrev[nPrevTrkID];
			}
			
			// 다중 종착궤도의 인접 궤도가 점유해정되었는지 체크한다.
			if(nPrevTrkID > 0)
			{
				pPrevTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTrkID);
				if(!pPrevTrkInfo->TRACK || !pPrevTrkInfo->ROUTE)
				{
					bIsArriveDestTrack = FALSE;
				}
			}

			BYTE &tktimer3 = *((BYTE*)&m_pTrackTimer[nDestTrkID]+3);	// for calc occ time - 자기궤도 Overlap Timer
            BYTE &delay = m_pSwitchOverTrack1[nID].nDelay;  

			if(isfreeR2L || m_SwitchInfo.ON_TIMER || bIsArriveDestTrack)
			{
				// 순차 진행에 의해 끝 종착 궤도가 점유되었으면서 종착 직전 궤도가 해정된 경우
				if(!pTrkInfo->TRACK && pTrkInfo->OVERLAP && bIsArriveDestTrack)
				{
					if (tktimer3 < delay)
					{
						m_SwitchInfo.ON_TIMER = 1;

						isfreeR2L = 0;
					}
					else
					{
						//2006.11.16 Overlap route 표시를 위해....
						if (m_pSwitchOverTrack1[nID].wRouteID)
							ClearOverlap (m_pSwitchOverTrack1[nID].wRouteID & ROUTE_MASK, 0);

						m_pSwitchOverTrack1[nID].nTrackID = 0;
						m_pSwitchOverTrack1[nID].nDelay = 0;
						m_pSwitchOverTrack1[nID].wRouteID = 0;
						m_SwitchInfo.ON_TIMER = 0;
						m_SwitchInfo.PASSROUTE = 0;

						// Overlap Track 테이블 초기화.	- Overlap Track 버퍼(m_pOverTrack)의 ID는 TRACK_MASK 적용해야함.
						BYTE nNextTrkID = 0;
						BYTE nDelTrkID = nDestTrkID;
						while(nDelTrkID != 0)
						{
							nNextTrkID = m_pOverTrack[nDelTrkID];
							m_pOverTrack[nDelTrkID] = 0;
							nDelTrkID = nNextTrkID & TRACK_MASK;
						}

						//2012.11.29 추가...
// 						BYTE nFindOverlapTimer = 0;
// 						for (short nIdIndex=1; nIdIndex<=m_TableBase.InfoSwitch.nCount; nIdIndex++)
// 						{
// 							if (m_pSwitchOverTrack1[nIdIndex].nTrackID == nDestTrkID || m_pSwitchOverTrack2[nIdIndex].nTrackID == nDestTrkID)
// 								nFindOverlapTimer = 1;
// 						}
// 
// 						if(!nFindOverlapTimer)
// 						{
// 						    pTrkInfo->OVERLAP = 0;
// 						}
					}
				}
				else
				{
					isfreeR2L = 0;
				}
			}
			else
			{
				isfreeR2L = 0;
			}
	
			// 궤도 해정 상태...
            if(pTrkInfo->TRACK)
			{
				if(pTrkInfo->ROUTE && !pTrkInfo->OVERLAP && !pTrkInfo->TKREQUEST)
				{
					if (m_pSwitchOverTrack1[nID].wRouteID)
						ClearOverlap (m_pSwitchOverTrack1[nID].wRouteID & ROUTE_MASK, 0);

					m_SwitchInfo.ON_TIMER = 0;
					m_SwitchInfo.PASSROUTE = 0;

					m_pSwitchOverTrack1[nID].nTrackID = 0;
					m_pSwitchOverTrack1[nID].nDelay = 0;
					m_pSwitchOverTrack1[nID].wRouteID = 0;

					// Overlap Track 테이블 초기화.	- Overlap Track 버퍼(m_pOverTrack)의 ID는 TRACK_MASK 적용해야함.
					BYTE nNextTrkID = 0;
					BYTE nDelTrkID = nDestTrkID;
					while(nDelTrkID != 0)
					{
						nNextTrkID = m_pOverTrack[nDelTrkID];
						m_pOverTrack[nDelTrkID] = 0;
						nDelTrkID = nNextTrkID & TRACK_MASK;
					}
				}
			}
		}
	
        nDestTrkID = m_pSwitchOverTrack2[nID].nTrackID;
        if (nDestTrkID) 
		{
			// 다중 종착 궤도 진로의 처리를 위해 추가.
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nDestTrkID);  // 도착진로의 경우 보통은 Home궤도...
			
			// 일단 TRUE로 만든다.
			BOOL bIsArriveDestTrack = TRUE;
			
			ScrInfoTrack *pPrevTrkInfo = NULL;
			BYTE nPrevTrkID = m_pTrackPrev[ nDestTrkID ];
			while(nPrevTrkID)
			{
				pPrevTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTrkID);
				
				if(pPrevTrkInfo->MULTIDEST)
				{
					if(!pPrevTrkInfo->OVERLAP)	// 인접 궤도에 연속 점유 정보가 없으면 처리하지 않는다. (부정낙하 등)
					{
						bIsArriveDestTrack = FALSE;
						break;
					}
				}
				else
				{
					// 다중 종착궤도가 아니면서 m_TrackPrev에 존재하는 Track이 있음. 
					break;
				}
				
				nPrevTrkID = m_pTrackPrev[nPrevTrkID];
			}
			
			// 다중 종착궤도의 인접 궤도가 점유해정되었는지 체크한다.
			if(nPrevTrkID > 0)
			{
				pPrevTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nPrevTrkID);
				if(!pPrevTrkInfo->TRACK || !pPrevTrkInfo->ROUTE)
				{
					bIsArriveDestTrack = FALSE;
				}
			}
			
			BYTE &tktimer3 = *((BYTE*)&m_pTrackTimer[nDestTrkID]+3);	// for calc occ time - 자기궤도 Overlap Timer
            BYTE &delay = m_pSwitchOverTrack2[nID].nDelay;  
			
			if(isfreeL2R || m_SwitchInfo.ON_TIMER || bIsArriveDestTrack)
			{
				// 순차 진행에 의해 끝 종착 궤도가 점유되었으면서 종착 직전 궤도가 해정된 경우
				if(!pTrkInfo->TRACK && pTrkInfo->OVERLAP && bIsArriveDestTrack)
				{
					if (tktimer3 < delay)
					{
						m_SwitchInfo.ON_TIMER = 1;
						
						isfreeL2R = 0;
					}
					else
					{
						//2006.11.16 Overlap route 표시를 위해....
						if (m_pSwitchOverTrack2[nID].wRouteID)
							ClearOverlap (m_pSwitchOverTrack2[nID].wRouteID & ROUTE_MASK, 0);
						
						m_pSwitchOverTrack2[nID].nTrackID = 0;
						m_pSwitchOverTrack2[nID].nDelay = 0;
						m_pSwitchOverTrack2[nID].wRouteID = 0;
						m_SwitchInfo.ON_TIMER = 0;
						m_SwitchInfo.PASSROUTE = 0;

						// Overlap Track 테이블 초기화.	- Overlap Track 버퍼(m_pOverTrack)의 ID는 TRACK_MASK 적용해야함.
						BYTE nNextTrkID = 0;
						BYTE nDelTrkID = nDestTrkID;
						while(nDelTrkID != 0)
						{
							nNextTrkID = m_pOverTrack[nDelTrkID];
							m_pOverTrack[nDelTrkID] = 0;
							nDelTrkID = nNextTrkID & TRACK_MASK;
						}
						
						//2012.11.29 추가...
// 						BYTE nFindOverlapTimer = 0;
// 						for (short nIdIndex=1; nIdIndex<=m_TableBase.InfoSwitch.nCount; nIdIndex++)
// 						{
// 							if (m_pSwitchOverTrack2[nIdIndex].nTrackID == nDestTrkID || m_pSwitchOverTrack1[nIdIndex].nTrackID == nDestTrkID)
// 								nFindOverlapTimer = 1;
// 						}
// 						
// 						if(!nFindOverlapTimer)
// 						{
// 							pTrkInfo->OVERLAP = 0;
// 						}
					}
				}
				else
				{
					isfreeL2R = 0;
				}
			}
			else
			{
				isfreeL2R = 0;
			}
			
			// 궤도 해정 상태...
            if(pTrkInfo->TRACK)
			{
				if(pTrkInfo->ROUTE && !pTrkInfo->OVERLAP && !pTrkInfo->TKREQUEST)
				{
					if (m_pSwitchOverTrack2[nID].wRouteID)
						ClearOverlap (m_pSwitchOverTrack2[nID].wRouteID & ROUTE_MASK, 0);

					m_SwitchInfo.ON_TIMER = 0;
					m_SwitchInfo.PASSROUTE = 0;
					
					m_pSwitchOverTrack2[nID].nTrackID = 0;
					m_pSwitchOverTrack2[nID].nDelay = 0;
					m_pSwitchOverTrack2[nID].wRouteID = 0;

					// Overlap Track 테이블 초기화.	- Overlap Track 버퍼(m_pOverTrack)의 ID는 TRACK_MASK 적용해야함.
					BYTE nNextTrkID = 0;
					BYTE nDelTrkID = nDestTrkID;
					while(nDelTrkID != 0)
					{
						nNextTrkID = m_pOverTrack[nDelTrkID];
						m_pOverTrack[nDelTrkID] = 0;
						nDelTrkID = nNextTrkID & TRACK_MASK;
					}
				}
			}
		}

		bMatch = (m_SwitchInfo.REQUEST_P == m_SwitchInfo.KR_P) && (m_SwitchInfo.REQUEST_M == m_SwitchInfo.KR_M);

		if ( m_SwitchInfo.ROUTELOCK ) 
			bMatch = TRUE;

		// 20141016 sjhong - SYNC 전철기 해정을 위한 처리
		BYTE bIsRouteLock = FALSE;

		if(m_SwitchInfo.ROUTELOCK)
		{
			bIsRouteLock = TRUE;
		}

		// 20141021 sjhong - 진로 쇄정 조건에 SYNC 전철기(진로구간외 쇄정전철기)의 상태도 참조하도록 수정
		m_SwitchInfo.ROUTELOCK = (!isfreeR2L || !isfreeL2R || ((m_pSyncSwitch[nID] & SYNCSWT_MASK) && !(m_pSyncSwitch[nID] & SYNCSWT_MASTER))) && !m_SwitchInfo.SWFAIL && (m_SwitchInfo.WR_P == m_SwitchInfo.KR_P) && (m_SwitchInfo.WR_M == m_SwitchInfo.KR_M);

// 		if(nSwitchType == 0x30 && m_SwitchInfo.INROUTE == 0)
// 		{
// 			// 20141021 sjhong - 수동전철기의 경우에는 쇄정 기능을 ROUTELOCK을 참조하지 않고 INROUTE를 참조하므로 동일하게 수정한다.
// 			m_SwitchInfo.INROUTE = (m_pSyncSwitch[nID] & SYNCSWT_MASK) && !(m_pSyncSwitch[nID] & SYNCSWT_MASTER);
// 		}

		// 진로 쇄정이 설정되었다가 풀리는 경우(ROUTELOCK이 1 -> 0 으로 변화)에 SYNC 전철기 정보를 초기화한다.
		if(bIsRouteLock && !m_SwitchInfo.ROUTELOCK)
		{
			if(m_pSyncSwitch[nID] & SYNCSWT_MASTER)
			{
				m_pSyncSwitch[m_pSyncSwitch[nID] & SYNCSWT_MASK] = 0;
				m_pSyncSwitch[nID] = 0;
			}
		}


		// Key transmitter에서 key가 빠져 있는 상태는 전철기를 움직일 수 없는 상태, 즉 Lock상태로 만들어야 한다....
		if (!m_SwitchInfo.WKEYKR)
			m_SwitchInfo.ROUTELOCK = !m_SwitchInfo.WKEYKR;

		// 수동 전철기를 check하여 쇄정 상태를 update...
		// 수동전철기는 정위 방향이 감지되면 바로 lock 되어 있어야 한다...

		// 2007.3.7 HandSwitch는 반위 방향이 감지될 경우에도 바로 Lock 시킨다.
		if (nSwitchType == 0x30)
		{
			// KEYOPER=0 이면 LOCK 1 이면 Key제거 가능, WKEYKR=1 이면 키가 삽입되어 있음.
			m_SwitchInfo.ROUTELOCK = (m_SwitchInfo.KR_P || m_SwitchInfo.KR_M);
		}

		// 초록색 그림을 그리기 위해 설정해 준다....
		if (!m_SwitchInfo.ROUTELOCK)
		{
			// 2006.6.19  LSM 화면의 궤도 색상 변화의 문제점 Fix를 위해... 그리고 해정 시점또한 이 때가 맞다.
			if (!isoverlap)
			{
				m_SwitchInfo.ROUTE_P = 0;
				m_SwitchInfo.ROUTE_M = 0;
			}
/*
            BYTE nTrkID1 = m_pSwitchOverTrack1[nID].nTrackID;
            BYTE nTrkID2 = m_pSwitchOverTrack2[nID].nTrackID;
			if (nTrkID1 && !m_SwitchInfo.ON_TIMER)
			{
                m_pSwitchOverTrack1[nID].nTrackID = 0;
				m_pRouteCompleteTK[nTrkID1] = 0;
				m_pSwitchOverTrack1[nID].nDelay = 0;
				m_pSwitchOverTrack1[nID].nRouteID = 0;
			}
			if (nTrkID2 && !m_SwitchInfo.ON_TIMER)
			{
                m_pSwitchOverTrack2[nID].nTrackID = 0;
				m_pRouteCompleteTK[nTrkID2] = 0;
				m_pSwitchOverTrack2[nID].nDelay = 0;
				m_pSwitchOverTrack2[nID].nRouteID = 0;
			}
*/
		}
		else
		{
			if (m_SwitchInfo.KR_P)
			{
				m_SwitchInfo.ROUTE_P = 1;	
				m_SwitchInfo.ROUTE_M = 0;	//2006.11.16 추가..	
			}
			else if (m_SwitchInfo.KR_M)
			{
				m_SwitchInfo.ROUTE_M = 1;	
				m_SwitchInfo.ROUTE_P = 0;	//2006.11.16 추가..
			}
		}

		m_SwitchInfo.FREE = isfreeR2L & isfreeL2R & istrack & m_SwitchInfo.WKEYKR;


		if ( bMatch ) // 제어를 Clear한다...
		{
            m_SwitchInfo.REQUEST_P = 0;
            m_SwitchInfo.REQUEST_M = 0;
        }

//		if ( m_SwitchInfo.INROUTE )
//			m_SwitchInfo.ON_TIMER = 0;

		SwitchUpdate(nID, istrack);
    }
}


void CEipEmu::SwitchUpdate(BYTE nID, short istrack)
{
    SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;

	m_pSwitchTimer[ nID ] = SW_Timer;

	if (m_SwitchInfo.SWFAIL)
	{
		nSysVar.bPointFail = 1;
	}

	if ( !istrack ) 
	{
		// 2007.7.18  전철기 전환 중에 철사쇄정이 걸려도 전철기는 진행 방향으로 모두 전환 시킨다.
		if(!m_SwitchInfo.WRO_P && !m_SwitchInfo.WRO_M)
		{
			m_SwitchInfo.WLRO = 0;
		}
	}

	int_put(SwitchPut);
}


int CEipEmu::Operate( char *pMsg ) 
{
    BYTE cMsgType = pMsg[0];

    if(pMsg[4] != 0x79)
	{
	    memcpy( m_pMsgBuffer, pMsg, 8 );
	}

	pMsg++;

	BYTE bProcess = 0;

    BYTE &d = *(BYTE*)&m_TrackInfo;
    BYTE arg1, arg2, arg3, arg4;
    arg1 = *(pMsg+1);
    arg2 = *(pMsg+2);
    arg3 = *(pMsg+3);
    arg4 = *(pMsg+4);

//	BYTE nDestTrack;
	BYTE nTrkID;
	BYTE nB1OK = 0;
	BYTE nB3OK = 0;
	BYTE nB2OK = 0;
	BYTE nB4OK = 0;
	BYTE subcmd = pMsg[1];

	WORD wCheckID;

	DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;
    SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;
	SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
	UnitStatusType &UnitState = ((DataHeaderType*)m_pVMEM)->UnitState;

	BYTE nFunc = *pMsg & 0x7F;
	if ( nFunc ) 
	{
		switch ( nFunc ) 
		{
		case WS_OPMSG_START:
		    
			UnitState.bSysRun = 1;

	// for Simulation......
			/*
			if (UnitState.bSysRun != 1 )
			{
			    UnitState.bSysRun = 1;
				opClearAllRoute();
			}
			bStartInterlock = 0;
			if (!bStartInterlock)
			{
				
				ScrInfoBlock *pBlkInfo1 = (ScrInfoBlock *)int_get (SignalGet, m_nB2ID);
				pBlkInfo1->pData[0] = pBlkInfo1->pData[1] = pBlkInfo1->pData[2] = pBlkInfo1->pData[3] = 0;
				pBlkInfo1->DRBCR = 1;
				//ScrInfoBlock *pBlkInfo2 = (ScrInfoBlock *)int_get (SignalGet, m_nB1ID);
				//pBlkInfo2->pData[0] = pBlkInfo2->pData[1] = pBlkInfo2->pData[2] = pBlkInfo2->pData[3] = 0;
				//pBlkInfo2->DRBGR = 1;
				
				//ScrInfoBlock *pBlkInfo3 = (ScrInfoBlock *)int_get (SignalGet, m_nB4ID);
				//pBlkInfo3->URBCR = 1;
				//ScrInfoBlock *pBlkInfo4 = (ScrInfoBlock *)int_get (SignalGet, m_nB3ID);
				//pBlkInfo4->pData[0] = pBlkInfo4->pData[1] = pBlkInfo4->pData[2] = pBlkInfo4->pData[3] = 0;
				//pBlkInfo4->URBGR = 1;				
			}
			*/
			break;

		case WS_OPMSG_RESET	        :
			opClearAllRoute();

			*pMsg = 0;
			return 1;
			break;

		case WS_OPMSG_SIGNALDEST    : 
			opSignalDest( pMsg[1], pMsg[2] );	
			break;

		case WS_OPMSG_ASPECT    : 
			if ( pMsg[1] )
			{
				BYTE nSignal = pMsg[1];
				ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get (SignalGet, nSignal);
				if (pSigInfo->SE1 || pSigInfo->SE2 || pSigInfo->SE3 || pSigInfo->SE4 || pSigInfo->ENABLED)
				{
					pSigInfo->TM1 = 1;
					pSigInfo->DESTID &= 0x7f;

					if ( (m_pRouteQue[ nSignal ] & ROUTE_SIGNALSTOP) || (m_pRouteQue[ nSignal ] & ROUTECHECK_OSS) )
					{
					    m_pRouteQue[ nSignal ] &= ~ROUTE_SIGNALSTOP;   
					    m_pRouteQue[ nSignal ] &= ~ROUTECHECK_OSS;
					}
				}
// 
// 				if ( pMsg[2] )
// 				{
// 					m_pRouteQue[ nSignal ] |= ROUTE_ASPECT;
// 					
// 					if ((m_pSignalInfoTable[pMsg[2]].nSignalType & SIGNAL_MASK) != SIGNALTYPE_OHOME )
// 					{
// 						if ( (m_pRouteQue[ pMsg[2] ] & ROUTE_SIGNALSTOP) || (m_pRouteQue[ pMsg[2] ] & ROUTECHECK_OSS) )
// 						{
// 							m_pRouteQue[ pMsg[2] ] &= ~ROUTE_SIGNALSTOP;   
// 							m_pRouteQue[ pMsg[2] ] &= ~ROUTECHECK_OSS;
// 						}
// 						
// 						m_nDependSignalRecoveryCount = 0;
// 					}
// 				}				
			}		
			break;

		case WS_OPMSG_SIGNALCANCEL  : 
			opSignalCan( pMsg[1] );	

			if ( pMsg[1] )
			{
				BYTE nSignal = pMsg[1];
				ScrInfoSignal *pSigInfo = (ScrInfoSignal *)int_get (SignalGet, nSignal);

				if(pSigInfo != NULL)
				{
					pMsg[2] = (pSigInfo->DESTID & 0x7f) - 1;
				}
			}
			break;

		case WS_OPMSG_SIGNALSTOP  : 
			opSignalStop( pMsg[1] );	
			break;

		case WS_OPMSG_OSS  : 
			opSignalOSS( pMsg[1], pMsg[2] );	
			break;

		case WS_OPMSG_LOS  : 
			opTrackLOS( pMsg[1], pMsg[2] );	
			break;

		case WS_OPMSG_NOUSE  : 
			opSwitchUSE( pMsg[1], pMsg[2] );	
			break;

		case WS_OPMSG_SWTOGGLE      : 
			opSwitch( *(pMsg+1) ); 
			break;

		case WS_OPMSG_POINT_BLOCK	:
			opPointBlock( *(pMsg+1) );
			break;

		case WS_OPMSG_HSW      : 
			opHandSwitch( *(pMsg+1) ); 
			break;

		case WS_OPMSG_CAON     : 
			opBlockCA( *(pMsg+1) ); 
			break;

		case WS_OPMSG_CAACK     : 
			if ( pMsg[1] )
			{
				BYTE nSignal = pMsg[1];
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, nSignal);

				if((pBlkInfo->CAACK == FALSE) && (pBlkInfo->COMPLETE == TRUE) /*&& (pBlkInfo->COMPLETEIN == TRUE) */
				&& ((!pBlkInfo->SAXLACTIN && (pBlkInfo->AXLOCC == TRUE)) || (pBlkInfo->SAXLACTIN && (pBlkInfo->SAXLOCC == TRUE))))
				{
					pBlkInfo->CAACK = TRUE;

					int_put(SignalPut);
				}
			}
			break;

		case WS_OPMSG_LCROP    : 
			opBlockLCR( *(pMsg+1) ); 
			break;

		case WS_OPMSG_CBBOP    : 
			opBlockCBB( *(pMsg+1) ); 
			break;

		case WS_OPMSG_BLOCK_CLEAR: 
			{
				BYTE nCmd	= pMsg[1];
				BYTE nBlkID = pMsg[2];
				BYTE bIsSelf = 0;
				BYTE bIsTGB = 0;
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, nBlkID);

				// Block ID에 따른 정보 수집
				if(nBlkID == m_nB1ID)
				{
					bIsSelf = m_ConfigList.B1B2_SELF_BLKRST;
					bIsTGB = TRUE;
				}
				else if(nBlkID == m_nB2ID)
				{
					bIsSelf = m_ConfigList.B1B2_SELF_BLKRST;
					bIsTGB = FALSE;
				}
				else if(nBlkID == m_nB3ID)
				{
					bIsSelf = m_ConfigList.B3B4_SELF_BLKRST;
					bIsTGB = TRUE;
				}
				else if(nBlkID == m_nB4ID)
				{
					bIsSelf = m_ConfigList.B3B4_SELF_BLKRST;
					bIsTGB = FALSE;
				}
				else if(nBlkID == m_nB11ID)
				{
					bIsSelf = m_ConfigList.B11B12_SELF_BLKRST;
					bIsTGB = TRUE;
				}
				else if(nBlkID == m_nB12ID)
				{
					bIsSelf = m_ConfigList.B11B12_SELF_BLKRST;
					bIsTGB = FALSE;
				}
				else if(nBlkID == m_nB13ID)
				{
					bIsSelf = m_ConfigList.B13B14_SELF_BLKRST;
					bIsTGB = TRUE;
				}
				else if(nBlkID == m_nB14ID)
				{
					bIsSelf = m_ConfigList.B13B14_SELF_BLKRST;
					bIsTGB = FALSE;
				}

				if(nCmd == BLKRST_CMD_REQ)
				{
					if(bIsSelf)
					{
						if(bIsTGB)
						{
							if(pBlkInfo->RBGPR)
							{
								pBlkInfo->NBGR = 1;
								pBlkInfo->RBGR = 0;
							}
						}
						else
						{
							if(pBlkInfo->RBCPR)
							{
								if(pBlkInfo->LCR && pBlkInfo->LCRIN)
								{
									pBlkInfo->ARRIVE	= 1;
								}
								else
								{
									pBlkInfo->NBCR = 1;
									pBlkInfo->RBCR = 0;
								}
							}
						}
					}
					else
					{
						if((!pBlkInfo->SAXLACTIN && pBlkInfo->AXLOCC && pBlkInfo->AXLDST) || (pBlkInfo->SAXLACTIN && pBlkInfo->SAXLOCC && pBlkInfo->SAXLDST))
						{
							pBlkInfo->BLKRSTREQ = 1;
						}
					}
				}
				else if(nCmd == BLKRST_CMD_ACC)
				{
					if(pBlkInfo->BLKRSTREQIN && ((!pBlkInfo->SAXLACTIN && pBlkInfo->AXLOCC && pBlkInfo->AXLDST) || (pBlkInfo->SAXLACTIN && pBlkInfo->SAXLOCC && pBlkInfo->SAXLDST)))
					{
						pBlkInfo->BLKRSTACC = 1;

						if(!m_ConfigList.BLK_TRAIN_SWEEP)
						{
							if(bIsTGB)
							{
								if(pBlkInfo->RBGPR)
								{
									pBlkInfo->CA		= 0;
									pBlkInfo->LCR		= 0;
									pBlkInfo->DEPART	= 0;
									pBlkInfo->COMPLETE	= 1;
																		
									pBlkInfo->NBGR = 1;
									pBlkInfo->RBGR = 0;
								}
							}
							else
							{
								if(pBlkInfo->RBCPR)
								{
									pBlkInfo->CAACK		= 0;
									pBlkInfo->LCR		= 0;
									pBlkInfo->ARRIVE	= 0;
									pBlkInfo->COMPLETE	= 1;
									
									pBlkInfo->NBCR = 1;
									pBlkInfo->RBCR = 0;
								}
							}
						}
					}
				}
				else if(nCmd == BLKRST_CMD_DEC)
				{
					if(pBlkInfo->BLKRSTREQIN)
					{
						pBlkInfo->BLKRSTDEC = 1;
					}
				}
			}
			break;

		case WS_OPMSG_PAS:
#ifdef _WINDOWS
		case WS_OPMSG_PAS_IN:
		case WS_OPMSG_PAS_OUT:
#endif
			{
				// PAS는 자기역으로 경유되는 모든 폐색 취급이 없을 경우에 가능하다...
				if (m_nB1ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB1ID);
					if (pBlkInfo->COMPLETE == 1)
						nB1OK = 1;
				}
				if (m_nB3ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB3ID);
					if (pBlkInfo->COMPLETE == 1)
						nB3OK = 1;
				}
				if (m_nB2ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB2ID);
					if (pBlkInfo->COMPLETE == 1)
						nB2OK = 1;
				}
				if (m_nB4ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB4ID);
					if (pBlkInfo->COMPLETE == 1)
						nB4OK = 1;
				}
				
				// PAS OUT은 폐색이 취급되지 않았을 때만 가능하다....
				
				if ( (!m_nB1ID || (m_nB1ID && nB1OK)) && (!m_nB3ID || (m_nB3ID && nB3OK)) 
					  && (!m_nB2ID ||(m_nB2ID && nB2OK)) && (!m_nB4ID ||(m_nB4ID && nB4OK)) )
				{
				

					if ( nSysVar.bN1 ) 
					{
						nSysVar.bN1 = 0;
						nSysVar.bTCSR = 0;  // Track power on..
					}
					else
					{
						nSysVar.bN1 = 1;  //Lock

						if ( nSysVar.bStation && nSysVar.bN1)		// Station close & PAS OUT
						{						
							if(m_ConfigList.SUPER_BLOCK)
							{
								ScrInfoBlock *pBlkInfo = NULL;
								// 인접역에 역폐쇄를 알리기 위한 SAXLACT(역폐쇄 및 수퍼블록 사용 BIT)를 설정한다. 

								if(m_nB1ID)
								{
									pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB1ID);
									pBlkInfo->SAXLACT = 1;
								}

								if(m_nB2ID)
								{
									pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB2ID);
									pBlkInfo->SAXLACT = 1;
								}
								
								if(m_nB3ID)
								{
									pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB3ID);
									pBlkInfo->SAXLACT = 1;
								}

								if(m_nB4ID)
								{
									pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB4ID);
									pBlkInfo->SAXLACT = 1;
								}
							}

							// Station Close 취급과 같이 되면..
							// 인접역의 정보는 다음역으로 전송되어야 한다...  
							// B2 -> B3로 update되어 인접역으로 전송되어야 한다. B4 - > B1로 Update되어 인접역으로 전송되어야 한다..
							nSysVar.bTCSR = 1;  // Track power off...
							nSysVar.bN3 = 0;    // SLS off....
						}
					}
				}
			}
			break;

		case WS_OPMSG_COUNTER    :  // Counter initialized
			pHead->Counter[CallonCount] = (arg1 + (arg2*256));
			pHead->Counter[EmerCount] = (arg3 + (arg4*256));
#ifndef _WINDOWS
			LogDbg(LOG_TYPE_EIP, 
					"CallonCount = %d, EmerCount = %d, arg1 = %d, arg2 = %d, arg3 = %d, arg4 = %d\n",
					pHead->Counter[CallonCount], pHead->Counter[EmerCount], arg1, arg2, arg3, arg4);
#endif
			break;

		case WS_OPMSG_AXL_COUNTER    :  // Counter initialized
			pHead->Counter[B2AXLCounter] = arg1 + (arg2*256);
			pHead->Counter[B4AXLCounter] = arg3 + (arg4*256);
#ifndef _WINDOWS
			LogDbg(LOG_TYPE_EIP, 
					"B2AXLCounter = %d, B4AXLCounter = %d, arg1 = %d, arg2 = %d, arg3 = %d, arg4 = %d\n",
					pHead->Counter[B2AXLCounter], pHead->Counter[B4AXLCounter], arg1, arg2, arg3, arg4);
#endif
			break;

		case WS_OPMSG_USERID    :  // UserID initialized
//			pHead->UserID[0] = arg1;
//			pHead->UserID[1] = arg2;
//			pHead->UserID[2] = arg3;
//			pHead->UserID[3] = arg4;
//			pHead->UserID[4] = arg5;
			break;

        case WS_OPMSG_MARKINGTRACK	: // 14		// 궤도 사용금지/해제(모터카, 사용금지, 단선)
			int_get(TrackLoad, arg1);
            d &= 0x3F;
            d |= (arg2 & 0xC0);
			int_put(TrackPut);
            break;


        case WS_OPMSG_FREESUBTRACK : //15
			if (arg2 == ROUTELOCK_FREE /*|| arg2 == DROUTELOCK_FREE*/)
			{
				wCheckID = RemoveRouteQue( arg1, arg2, 0 );
				if (wCheckID == 0)
					return 0;

			    BYTE *pCheckRouteInfo = &m_pInfoMem[ *(DWORD*)&m_pRouteAddTable[ wCheckID & ROUTE_MASK ] ];			// 연동도표 정보 테이블
	    	    BYTE nCheckTkID = pCheckRouteInfo[ RT_OFFSET_FROM ];

				nTrkID = arg1;
				while ( nTrkID ) 
				{
					int_get(TrackLoad, nTrkID);
					if ( !m_TrackInfo.TKREQUEST ) 
						return 0;
					if ( m_TrackInfo.EMREL ) 
						return 0;

					m_TrackInfo.EMREL = 1;
					BYTE &tktimer2 = *((BYTE*)&m_pTrackTimer[nTrkID]+2);	// Em Release
					tktimer2 = 120;
	#ifdef _CHOTEST
		tktimer2 /= 10;
	#endif
					int_put(TrackPut);

					nTrkID = m_pTrackPrev[ nTrkID ];

					// 순차 진행으로 진로 구간 첫궤도에 열차가 안착한 상태에서 비상해정하면 해당 궤도에 진로값이 남는 버그 수정
					if (nTrkID == nCheckTkID)
						break;
				}
			}
			else if (arg2 == TRACKLOCK_FREE)
			{
				nTrkID = arg1;
				RemoveRouteQue( nTrkID, arg2, 0 );
				while ( nTrkID ) 
				{
					int_get(TrackLoad, nTrkID);

					// 20130617 sjhong - 비상해정 시 전방 궤도 점유이면 함께 Flashing 되는 현상 해소를 위해 주석 해제
					// Begin -->
					if ( m_TrackInfo.ROUTE ) 
						return 0;
					// End <--
					if ( m_TrackInfo.EMREL ) 
						return 0;

					m_TrackInfo.EMREL = 1;
					BYTE &tktimer2 = *((BYTE*)&m_pTrackTimer[nTrkID]+2);	// Em Release
					tktimer2 = 120;
	#ifdef _CHOTEST
		tktimer2 /= 10;
	#endif
					int_put(TrackPut);
					nTrkID = m_pTrackPrev[ nTrkID ];
				}
			}

			if (arg3 != 0x79)
			{
#ifdef _WINDOWS
				WORD wCounter = pHead->Counter[EmerCount];
				wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
				pHead->Counter[EmerCount] = wCounter;
#endif
				pHead->Counter[EmerCount]++;  // Counter를 위해 저장
				if (pHead->Counter[EmerCount] == 10000)
					pHead->Counter[EmerCount] = 0;
#ifdef _WINDOWS
				wCounter = pHead->Counter[EmerCount];
				wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
				pHead->Counter[EmerCount] = wCounter;
#endif
			}

            break;

		case WS_OPMSG_AXL_RESET :
			if(subcmd == BtnB2AXLRST)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB2ID);

				if(m_ConfigList.B2_SELF_AXLRST)
				{
			 		pBlkInfo->AXLRST = 1;
#ifndef _WINDOWS
					LogDbg(LOG_TYPE_EIP, "m_nB2ID = %d, AXLRST = %d\n", m_nB2ID, pBlkInfo->AXLRST);
#endif

#ifdef _WINDOWS
					WORD wCounter = pHead->Counter[B2AXLCounter];
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[B2AXLCounter] = wCounter;
#endif
					pHead->Counter[B2AXLCounter]++;
					if (pHead->Counter[B2AXLCounter] == 10000)
						pHead->Counter[B2AXLCounter] = 0;
#ifdef _WINDOWS
					wCounter = pHead->Counter[B2AXLCounter];
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[B2AXLCounter] = wCounter;
#endif

				}
				else
				{
					if(pBlkInfo->SAXLACTIN)
					{
						*(pMsg+2) |= 0x80;

						pBlkInfo->SAXLREQ = 1;
					}
					else
					{
						pBlkInfo->AXLREQ = 1;
					}

#ifndef _WINDOWS
					LogDbg(LOG_TYPE_EIP, "m_nB2ID = %d, AXLREQ = %d, SAXLREQ = %d\n", m_nB2ID, pBlkInfo->AXLREQ, pBlkInfo->SAXLREQ);
#endif
				}
								
			}
			else if(subcmd == BtnB4AXLRST)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB4ID);

				if(m_ConfigList.B4_SELF_AXLRST)
				{
					pBlkInfo->AXLRST = 1;
#ifndef _WINDOWS
					LogDbg(LOG_TYPE_EIP, "m_nB4ID = %d, AXLRST = %d\n", m_nB4ID, pBlkInfo->AXLRST);
#endif
	
#ifdef _WINDOWS
					WORD wCounter = pHead->Counter[B4AXLCounter];
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[B4AXLCounter] = wCounter;
#endif
					pHead->Counter[B4AXLCounter]++;
					if (pHead->Counter[B4AXLCounter] == 10000)
						pHead->Counter[B4AXLCounter] = 0;
#ifdef _WINDOWS
					wCounter = pHead->Counter[B4AXLCounter];
					wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
					pHead->Counter[B4AXLCounter] = wCounter;
#endif
				}
				else
				{
					if(pBlkInfo->SAXLACTIN)
					{
						*(pMsg+2) |= 0x80;

						pBlkInfo->SAXLREQ = 1;
					}
					else
					{
						pBlkInfo->AXLREQ = 1;
					}
				
#ifndef _WINDOWS
					LogDbg(LOG_TYPE_EIP, "m_nB4ID = %d, AXLREQ = %d, SAXLREQ = %d\n", m_nB4ID, pBlkInfo->AXLREQ, pBlkInfo->SAXLREQ);
#endif
				}
			}
			else if(subcmd == BtnB1AXLRSTACC)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB1ID);

				if(pBlkInfo->SAXLACTIN)
				{
					if(pBlkInfo->SAXLREQ)
					{
						pBlkInfo->SAXLACC = 1;
					}
				}
				else
				{
					if(pBlkInfo->AXLREQ)
					{
						pBlkInfo->AXLACC = 1;
					}
				}
			}
			else if(subcmd == BtnB1AXLRSTDEC)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB1ID);

				if(pBlkInfo->SAXLACTIN)
				{
					if(pBlkInfo->SAXLREQ)
					{
						pBlkInfo->SAXLDEC = 1;
					}
				}
				else
				{
					if(pBlkInfo->AXLREQ)
					{
						pBlkInfo->AXLDEC = 1;
					}
				}
			}
			else if(subcmd == BtnB3AXLRSTACC)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB3ID);

				if(pBlkInfo->SAXLACTIN)
				{
					if(pBlkInfo->SAXLREQ)
					{
						pBlkInfo->SAXLACC = 1;
					}
				}
				else
				{
					if(pBlkInfo->AXLREQ)
					{
						pBlkInfo->AXLACC = 1;
					}
				}
			}
			else if(subcmd == BtnB3AXLRSTDEC)
			{
				ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB3ID);

				if(pBlkInfo->SAXLACTIN)
				{
					if(pBlkInfo->SAXLREQ)
					{
						pBlkInfo->SAXLDEC = 1;
					}
				}
				else
				{
					if(pBlkInfo->AXLREQ)
					{
						pBlkInfo->AXLDEC = 1;
					}
				}
			}
			break;
			
		case WS_OPMSG_TRACK_RESET:
			{
				BYTE nTkID = arg1;
				BYTE nAccept = arg2;
				
				if(nTkID > 0)
				{
					ScrInfoTrack *pTkInfo = (ScrInfoTrack*)int_get(TrackGet, nTkID);
					
					if(pTkInfo != NULL)
					{
						pTkInfo->RESETBUF = 0;
						
						if(nAccept == 1)
						{
							pTkInfo->RESETOUT = 1;

							DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;

#ifdef _WINDOWS
							WORD wCounter = pHead->Counter[B2AXLCounter];
							wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
							pHead->Counter[B2AXLCounter] = wCounter;
#endif
							pHead->Counter[B2AXLCounter]++;
							if (pHead->Counter[B2AXLCounter] == 10000)
								pHead->Counter[B2AXLCounter] = 0;
#ifdef _WINDOWS
							wCounter = pHead->Counter[B2AXLCounter];
							wCounter = *((BYTE*)&wCounter) * 256 + *(((BYTE*)&wCounter) + 1);
							pHead->Counter[B2AXLCounter] = wCounter;
#endif
						}
					}
				}
			}
			break;

		case WS_OPMSG_BUTTON :
			switch ( subcmd ) 
			{
/*
   			case BtnPas:
				if ( nSysVar.bN1 ) 
					nSysVar.bN1 = 0;
				else
					nSysVar.bN1 = 1;  //Lock

				break;
*/
			case  BtnCTCMode:
				nSysVar.bMode = 1;
				nSysVar.bCTCRequest = 0;
				break;

			case  BtnLocalMode:
				nSysVar.bMode = 0;
				nSysVar.bCTCRequest = 0;
				break;

			case BtnDay:
				if ( nSysVar.bN2 ) 
				{
					nSysVar.bN2 		= 0;
					nSysVar.bIDayNight	= 0; // 20130604 Day/Night Input 정보 누락으로 강제 설정 추가
				}
				else
				{
					nSysVar.bN2 		= 1;  //Day
					nSysVar.bIDayNight	= 1; // 20130604 Day/Night Input 정보 누락으로 강제 설정 추가
				}

				break;

			case BtnSls:
				if ( nSysVar.bN3 ) 
					nSysVar.bN3 = 0;
				else
					nSysVar.bN3 = 1;  //Sls on -- 신호기 power on

				break;

			case BtnSysRun:
#ifdef _WINDOWS
			    UnitState.bSysRun ^= 1;
				//nSysVar.bSysRun ^= 1;				
				break;
#else
			    UnitState.bSysRun = 1;
				//nSysVar.bSysRun = 1;				
				break;
#endif

			case BtnAtb: 
				// 20151024 sjhong - ATB이 가능한 조건을 제한한다. SLS OFF 이면서 SLS OFF 무시 조건이 안걸린 경우에만 ATB가 ON된다.
				if(!nSysVar.bATB && !nSysVar.bN3 && !m_bSLSStick)
				{
					nSysVar.bATB = 1;
				}
				break;

			case BtnAck:
				nSysVarExt.bSound2 = 0;
				break;

			case BtnPDB:
				nSysVarExt.bSound3 = 0;
				break;

			case BtnClose:
				if (m_nB2ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB2ID);
					if (pBlkInfo->CA == TRUE && pBlkInfo->LCRIN == TRUE && pBlkInfo->CONFIRM == TRUE && pBlkInfo->DEPARTIN == FALSE)
						nB2OK = 1;
				}
				if (m_nB4ID != 0)
				{
					ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)int_get (SignalGet, m_nB4ID);
					if (pBlkInfo->CA == TRUE && pBlkInfo->LCRIN == TRUE && pBlkInfo->CONFIRM == TRUE && pBlkInfo->DEPARTIN == FALSE)
						nB4OK = 1;
				}

				if ( (m_nB2ID && nB2OK) && (nB4OK && m_nB4ID) )
				{
					if ( nSysVar.bStation ) 
					{
						nSysVar.bStation = 0;

					}
					else
					{
						nSysVar.bStation = 1;  //Station Close
					}

					ScrInfoBlock *pBlk2Info = (ScrInfoBlock *)int_get (SignalGet, m_nB2ID);
					pBlk2Info->CBB = 1;

					ScrInfoBlock *pBlk4Info = (ScrInfoBlock *)int_get (SignalGet, m_nB4ID);
					pBlk4Info->CBB = 1;
				}
				break;

			case BtnAPKup:
				if (nSysVar.bPKeyContN == 1)
					nSysVar.bPKeyContN = 0;
				else
					nSysVar.bPKeyContN = 1;
				break;

			case BtnAPKdn:
				if (nSysVar.bPKeyContS == 1)
					nSysVar.bPKeyContS = 0;
				else
					nSysVar.bPKeyContS = 1;
/*
				if ( arg2 == (BYTE)0x55 ) 
					OtherState.bSW_OGCPR = 1;
				else if ( arg2 == (BYTE)0xaa ) 
					OtherState.bSW_OGCPR = 0;
*/
				break;

			case BtnLCB:		//	Level Crossing LC1 Key was admited to be released.
				if (nSysVarExt.bLC1KXLR)
				{
					nSysVarExt.bLC1KXLR = 0;  // Key transmiter locking...
				}
				else
				{
					BYTE loopCnt, loopMax;
					ScrInfoTrack *pTrkInfo = NULL;

					loopMax = (BYTE)m_TableBase.InfoTrack.nCount;

					// loopCnt는 1부터 loopMax까지 순차증가한다.
					for(loopCnt = 1 ; loopCnt <= loopMax ; loopCnt++)
					{
						// 20130904	sjhong - Overlap 표시 방법 변경으로 LC 제어 조건 변경
						if(m_pTrackInfoTable[loopCnt].bLC1)
						{
							pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, loopCnt);

							if(pTrkInfo != NULL && ((pTrkInfo->TRACK == FALSE && !m_pTrackInfoTable[loopCnt].bLCNOTINTRK) || pTrkInfo->ROUTE == FALSE || pTrkInfo->OVERLAP == TRUE))
							{
#ifndef _WINDOWS
								LogDbg(LOG_TYPE_EIP, "TRACK = %d, ROUTE = %d, m_pTrackInfoTable[loopCnt] = 0x%X", 
									pTrkInfo->TRACK, pTrkInfo->ROUTE, m_pTrackInfoTable[loopCnt]);
#endif
								break;
							}
						}
					}

					if(loopCnt > loopMax)
					{
						if(!nSysVarExt.bLC1NKXPR)// 키가 삽입되어 있을 때만 제어가 가능하다. (bLC1NKXPR=1일때는 Key가 건널목에 있는 상태)
						{
				   			nSysVarExt.bLC1KXLR = 1;  // Locking 해정.. -- 이제 Key를 trnamiter에서 뺄 수 있다...
							break;
						}
					}
				}
				break;

			case BtnLCA:		//	Level Crossing LC1 BELL was admited to be released.
				// Bell 울림제어 취소명령은 없다... Bell울림제어는 BELACK 입력정보에 의해서만 취소 되어진다...
				if (!nSysVarExt.bLC1BELL)
				{
					if (!nSysVarExt.bLC1BELACK)  // Bell Ack입력이 없을때에만 Bell 울림 제어가 가능하다... 
					    nSysVarExt.bLC1BELL = 1;
				}
				break;

			case BtnLC2B:		//	Level Crossing LC2 Key was admited to be released.
				if (nSysVarExt.bLC2KXLR)
				{
					nSysVarExt.bLC2KXLR = 0;  // Key transmiter locking...
				}
				else
				{
					BYTE loopCnt, loopMax;
					ScrInfoTrack *pTrkInfo = NULL;

					loopMax = (BYTE)m_TableBase.InfoTrack.nCount;
					// loopCnt는 1부터 loopMax까지 순차증가한다.
					for(loopCnt = 1 ; loopCnt <= loopMax ; loopCnt++)
					{
						// 20130904	sjhong - Overlap 표시 방법 변경으로 LC 제어 조건 변경
						if(m_pTrackInfoTable[loopCnt].bLC2)
						{
							pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, loopCnt);

							if(pTrkInfo != NULL && ((pTrkInfo->TRACK == FALSE && !m_pTrackInfoTable[loopCnt].bLCNOTINTRK) || pTrkInfo->ROUTE == FALSE || pTrkInfo->OVERLAP == TRUE))
							{
#ifndef _WINDOWS
								LogDbg(LOG_TYPE_EIP, "TRACK = %d, ROUTE = %d, m_pTrackInfoTable[loopCnt] = 0x%X", 
									pTrkInfo->TRACK, pTrkInfo->ROUTE, m_pTrackInfoTable[loopCnt]);
#endif
								break;
							}
						}
					}

					if(loopCnt > loopMax)
					{
						if(!nSysVarExt.bLC2NKXPR)// 키가 삽입되어 있을 때만 제어가 가능하다. (bLC2NKXPR=1일때는 Key가 건널목에 있는 상태)
						{
				   			nSysVarExt.bLC2KXLR = 1;  // Locking 해정.. -- 이제 Key를 trnamiter에서 뺄 수 있다...
						}
					}
				}
				break;

			case BtnLC2A:		//	Level Crossing LC2 BELL was admited to be released.
				// Bell 울림제어 취소명령은 없다... Bell울림제어는 BELACK 입력정보에 의해서만 취소 되어진다...
				if (!nSysVarExt.bLC2BELL)
				{
					if (!nSysVarExt.bLC2BELACK)  // Bell Ack입력이 없을때에만 Bell 울림 제어가 가능하다... 
					    nSysVarExt.bLC2BELL = 1;
				}
				break;

			case BtnLC3B:		//	Level Crossing LC3 Key was admited to be released.
				if (nSysVarExt.bLC3KXLR)
				{
					nSysVarExt.bLC3KXLR = 0;  // Key transmiter locking...
				}
				else
				{
					BYTE loopCnt, loopMax;
					ScrInfoTrack *pTrkInfo = NULL;
					
					loopMax = (BYTE)m_TableBase.InfoTrack.nCount;
					// loopCnt는 1부터 loopMax까지 순차증가한다.
					for(loopCnt = 1 ; loopCnt <= loopMax ; loopCnt++)
					{
						// 20130904	sjhong - Overlap 표시 방법 변경으로 LC 제어 조건 변경
						if(m_pTrackInfoTable[loopCnt].bLC3)
						{
							pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, loopCnt);

							if(pTrkInfo != NULL && ((pTrkInfo->TRACK == FALSE && !m_pTrackInfoTable[loopCnt].bLCNOTINTRK) || pTrkInfo->ROUTE == FALSE || pTrkInfo->OVERLAP == TRUE))
							{
#ifndef _WINDOWS
								LogDbg(LOG_TYPE_EIP, "TRACK = %d, ROUTE = %d, m_pTrackInfoTable[loopCnt] = 0x%X", 
									pTrkInfo->TRACK, pTrkInfo->ROUTE, m_pTrackInfoTable[loopCnt]);
#endif
								break;
							}
						}
					}
					
					if(loopCnt > loopMax)
					{
						if(!nSysVarExt.bLC3NKXPR)// 키가 삽입되어 있을 때만 제어가 가능하다. (bLC3NKXPR=1일때는 Key가 건널목에 있는 상태)
						{
							nSysVarExt.bLC3KXLR = 1;  // Locking 해정.. -- 이제 Key를 trnamiter에서 뺄 수 있다...
						}
					}
				}
				break;
				
			case BtnLC3A:		//	Level Crossing LC3 BELL was admited to be released.
				// Bell 울림제어 취소명령은 없다... Bell울림제어는 BELACK 입력정보에 의해서만 취소 되어진다...
				if (!nSysVarExt.bLC3BELL)
				{
					if (!nSysVarExt.bLC3BELACK)  // Bell Ack입력이 없을때에만 Bell 울림 제어가 가능하다... 
						nSysVarExt.bLC3BELL = 1;
				}
				break;
				
			case BtnLC4B:		//	Level Crossing LC4 Key was admited to be released.
				if (nSysVarExt.bLC4KXLR)
				{
					nSysVarExt.bLC4KXLR = 0;  // Key transmiter locking...
				}
				else
				{
					BYTE loopCnt, loopMax;
					ScrInfoTrack *pTrkInfo = NULL;
					
					loopMax = (BYTE)m_TableBase.InfoTrack.nCount;
					// loopCnt는 1부터 loopMax까지 순차증가한다.
					for(loopCnt = 1 ; loopCnt <= loopMax ; loopCnt++)
					{
						// 20130904	sjhong - Overlap 표시 방법 변경으로 LC 제어 조건 변경
						if(m_pTrackInfoTable[loopCnt].bLC4)
						{
							pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, loopCnt);

							if(pTrkInfo != NULL && ((pTrkInfo->TRACK == FALSE && !m_pTrackInfoTable[loopCnt].bLCNOTINTRK) || pTrkInfo->ROUTE == FALSE || pTrkInfo->OVERLAP == TRUE))
							{
#ifndef _WINDOWS
								LogDbg(LOG_TYPE_EIP, "TRACK = %d, ROUTE = %d, m_pTrackInfoTable[loopCnt] = 0x%X", 
									pTrkInfo->TRACK, pTrkInfo->ROUTE, m_pTrackInfoTable[loopCnt]);
#endif
								break;
							}
						}
					}
					
					if(loopCnt > loopMax)
					{
						if(!nSysVarExt.bLC4NKXPR)// 키가 삽입되어 있을 때만 제어가 가능하다. (bLC4NKXPR=1일때는 Key가 건널목에 있는 상태)
						{
							nSysVarExt.bLC4KXLR = 1;  // Locking 해정.. -- 이제 Key를 trnamiter에서 뺄 수 있다...
						}
					}
				}
				break;
				
			case BtnLC4A:		//	Level Crossing LC4 BELL was admited to be released.
				// Bell 울림제어 취소명령은 없다... Bell울림제어는 BELACK 입력정보에 의해서만 취소 되어진다...
				if (!nSysVarExt.bLC4BELL)
				{
					if (!nSysVarExt.bLC4BELACK)  // Bell Ack입력이 없을때에만 Bell 울림 제어가 가능하다... 
						nSysVarExt.bLC4BELL = 1;
				}
				break;

			case BtnLC5B:		//	Level Crossing LC5 Key was admited to be released.
				if (nSysVar.bLC5KXLR)
				{
					nSysVar.bLC5KXLR = 0;  // Key transmiter locking...
				}
				else
				{
					BYTE loopCnt, loopMax;
					ScrInfoTrack *pTrkInfo = NULL;
					
					loopMax = (BYTE)m_TableBase.InfoTrack.nCount;
					// loopCnt는 1부터 loopMax까지 순차증가한다.
					for(loopCnt = 1 ; loopCnt <= loopMax ; loopCnt++)
					{
						// 20130904	sjhong - Overlap 표시 방법 변경으로 LC 제어 조건 변경
						if(m_pTrackInfoTable[loopCnt].bLC5)
						{
							pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, loopCnt);

							if(pTrkInfo != NULL && ((pTrkInfo->TRACK == FALSE && !m_pTrackInfoTable[loopCnt].bLCNOTINTRK) || pTrkInfo->ROUTE == FALSE || pTrkInfo->OVERLAP == TRUE))
							{
#ifndef _WINDOWS
								LogDbg(LOG_TYPE_EIP, "TRACK = %d, ROUTE = %d, m_pTrackInfoTable[loopCnt] = 0x%X", 
									pTrkInfo->TRACK, pTrkInfo->ROUTE, m_pTrackInfoTable[loopCnt]);
#endif
								break;
							}
						}
					}
					
					if(loopCnt > loopMax)
					{
						if(!nSysVar.bLC5NKXPR)// 키가 삽입되어 있을 때만 제어가 가능하다. (bLC5NKXPR=1일때는 Key가 건널목에 있는 상태)
						{
							nSysVar.bLC5KXLR = 1;  // Locking 해정.. -- 이제 Key를 trnamiter에서 뺄 수 있다...
						}
					}
				}
				break;
				
			case BtnLC5A:		//	Level Crossing LC5 BELL was admited to be released.
				// Bell 울림제어 취소명령은 없다... Bell울림제어는 BELACK 입력정보에 의해서만 취소 되어진다...
				if (!nSysVar.bLC5BELL)
				{
					if (!nSysVar.bLC5BELACK)  // Bell Ack입력이 없을때에만 Bell 울림 제어가 가능하다... 
						nSysVar.bLC5BELL = 1;
				}
				break;

			case BtnAPKA:
				if (nSysVar.bPKeyContA == 1)
					nSysVar.bPKeyContA = 0;
				else
					nSysVar.bPKeyContA = 1;

				break;

			case BtnAPKB:
				if (nSysVar.bPKeyContB == 1)
					nSysVar.bPKeyContB = 0;
				else
					nSysVar.bPKeyContB = 1;

				break;

			case BtnAPKC:
				if (nSysVar.bPKeyContC == 1)
					nSysVar.bPKeyContC = 0;
				else
					nSysVar.bPKeyContC = 1;

				break;

			case BtnAPKD:
				if (nSysVar.bPKeyContD == 1)
					nSysVar.bPKeyContD = 0;
				else
					nSysVar.bPKeyContD = 1;

				break;

			case BtnAPKE:
				if (nSysVar.bPKeyContE == 1)
					nSysVar.bPKeyContE = 0;
				else
					nSysVar.bPKeyContE = 1;

				break;

			case BtnAPKF:
				if (nSysVar.bPKeyContF == 1)
					nSysVar.bPKeyContF = 0;
				else
					nSysVar.bPKeyContF = 1;

				break;

			case BtnAPKG:
				if (nSysVar.bPKeyContG == 1)
					nSysVar.bPKeyContG = 0;
				else
					nSysVar.bPKeyContG = 1;

				break;
			}
			break;
		}
	}
	*pMsg = 0;
	return 0;
}

void CEipEmu::LoadTable( void *pTable )
{
	void *dest = &m_TableBase;
	unsigned int size = 
		sizeof(m_TableBase) +
		sizeof(m_pInfoMem) +
		sizeof(m_pInfoGeneral) +
		sizeof(m_pRouteAddTable) +
		sizeof(m_pRouteCrcTable) +
		sizeof(m_pRouteSizeTable) +
		sizeof(m_pSwitchOnTrackTable) +
		sizeof(m_pSignalInfoTable) +
		sizeof(m_pSwitchInfoTable) +
        sizeof(m_pTrackInfoTable) +
		sizeof(m_pCommonTrack);

	// DB Table의 전체 크기를 구한다.
	unsigned int nTotalTableSize = size + sizeof(m_pAssignTable) + sizeof(m_pCtcElementTable) + sizeof(m_pConfigListTable);

	memcpy(dest, pTable, nTotalTableSize);
	m_nTableSize = size;

#ifndef _WINDOWS
	if(nTotalTableSize != m_TableBase.DownInfo.nMemSize)
	{
		LogErr(LOG_TYPE_GEN, "DB Table Size Missmatch. nTotalTableSize = 0x%X, nMemSize = 0x%X", nTotalTableSize, m_TableBase.DownInfo.nMemSize);
	}
#endif	
}

#ifdef _WINDOWS
BOOL CEipEmu::LoadTable( LPCSTR lpszPath )
{
	void *dest = &m_TableBase;
	DWORD size =
		sizeof(m_TableBase) +
		sizeof(m_pInfoMem) +
		sizeof(m_pInfoGeneral) +
		sizeof(m_pRouteAddTable) +
		sizeof(m_pRouteCrcTable) +
		sizeof(m_pRouteSizeTable) +
		sizeof(m_pSwitchOnTrackTable) +
		sizeof(m_pSignalInfoTable) +
		sizeof(m_pSwitchInfoTable) +
        sizeof(m_pTrackInfoTable) +
		sizeof(m_pCommonTrack);

	CString FileName = ".\\";
	FileName +=lpszPath;
	FileName += "\\";
	FileName += lpszPath;
	FileName += ".BIN";
	FILE *fp = fopen( FileName, "rb" );

	if ( fp ) 
	{
		BOOL ret = ( fread( dest, size, 1, fp ) ) ? TRUE : FALSE;
        m_pMsgBuffer = &m_pVMEM[ m_TableBase.InfoGeneralIO.nStart + m_TableBase.InfoGeneralIO.nSize ];
        m_nTNIBase = m_TableBase.InfoGeneralIO.nStart + m_TableBase.InfoGeneralIO.nSize + 16;

        fseek( fp, m_TableBase.DownInfo.nMemSize, SEEK_SET );
        short nTrack, nSignal, nSwitch, nButton;
		short nLamp;
        fread( &nTrack, sizeof(short), 1, fp );
        fread( &nSignal, sizeof(short), 1, fp );
        fread( &nSwitch, sizeof(short), 1, fp );
        fread( &nButton, sizeof(short), 1, fp );
        fread( &nLamp, sizeof(short), 1, fp );
        m_strTrack.ImportFromFileFixed16( fp, nTrack );
        m_strSignal.ImportFromFileFixed16( fp, nSignal );
        m_strSwitch.ImportFromFileFixed16( fp, nSwitch );
        m_strButton.ImportFromFileFixed16( fp, nButton );
        m_strLamp.ImportFromFileFixed16( fp, nLamp );

		fclose( fp );

		return ret;
	}
	return FALSE;
}
#endif


int CEipEmu::TrackDelay(BYTE *nEmergencyStop)
{
	SystemStatusType &nSysVar = ((DataHeaderType*)m_pVMEM)->nSysVar;

	int nAccTrack = 0;
	BYTE nTkID = 0;

	LoopCountL = (BYTE)m_TableBase.InfoTrack.nCount;
	for (LoopCountH=0; LoopCountH<LoopCountL;LoopCountH++)
	{
		nTkID = LoopCountH+1;

		int_get(TrackLoad, nTkID);

		BYTE &tktimer = *(BYTE*)&m_pTrackTimer[nTkID];		// for Occupancy Release Holding Time
		BYTE &tktimer1 = *((BYTE*)&m_pTrackTimer[nTkID]+1);	// for Occupancy Time
		BYTE &tktimer2 = *((BYTE*)&m_pTrackTimer[nTkID]+2);	// for Emergency Route Release
		BYTE &tktimer3 = *((BYTE*)&m_pTrackTimer[nTkID]+3);	// for Overlap Release

		if ( m_TrackInfo.TRACK )   // 해정...
		{
			if ( tktimer & 0x0F )
			{
				tktimer--;
			}

			tktimer3 = 0;

#ifndef _WINDOWS
			if ( tktimer & 0x0F ) 
			{
				m_TrackInfo.TRACK = 0;  // 점유를 계속 delay 하고 있음....

			    nAccTrack++;  // 임의궤도 점유 상태를 나타냄.. (2005.09.24 점유를 계속 Delay하고 있으므로 점유로 인식해야한다.)
			}

			if (m_TrackInfo.TRACK)	// 해정 완료 됨...
			{
				tktimer1 = 0;
			}
#else
			tktimer1 = 0;
#endif
		}
		else // 점유...
		{
			tktimer &= 0xF0;
			
	        if (_Time1Sec == 0) 
			{
				if ( (tktimer1 & 0x80) == 0 ) 
					tktimer1++;

				if ( ((tktimer3 & 0x80) == 0 && m_TrackInfo.OVERLAP && !m_TrackInfo.TKREQUEST) ||  tktimer3 ) 
					tktimer3++;
			}
			
			if ( !m_TrackInfo.INHIBIT )
			{
			    nAccTrack++;  // 임의궤도 점유 상태를 나타냄.. (Inhibit이 아닌 임의 궤도 점유 시 신호기 On을 위해)

				// 2005.10.20 LOS를 취급하여 INHIBIT이 되면 Track의 delay timer를 계산하지 읺는다.
				// 이것은 LOS해정 후 track점유에 의해 신호기가 ON 되는 것을 막기 위한 것이다. 
				// 이 제어를 위해 Maintask에서의 runinterlock과 operate의 순서를 바꾸었다. SW에 의해 변환되는 INHIBIT이 앞서기 때문에 순서가 바뀌지 않으면 
				// 이 로직은 의미가 없어진다...
				tktimer |= 0x09;	// 8->A		, 3 sec at 3 cyc
			}
		}

        if (_Time1Sec == 0) 
		{
			if ( tktimer2 ) 
				tktimer2--;
		}

		if ( !tktimer2 && m_TrackInfo.EMREL ) 
		{
			// 진로의 종착 궤도일 경우
			if((m_pLastTrack[ nTkID ] & TRACK_MASK) == nTkID)
			{
				if(m_TrackInfo.RouteNo)
				{
					WORD wMainRouteNo = m_TrackInfo.RouteNo & ROUTE_MASK;
					
					for(int i = 0 ; i < N_SG_COUNT ; i++)
					{
						// 비상해정이 끝났으므로 대항진로용 진로 정보를 제거한다.
						if((m_pOwnerRouteQue[i] & ROUTE_MASK) == wMainRouteNo)
						{
							// 진로가 완전히 해정되므로 여기서 초기화한다.
							m_pOwnerRouteQue[i] = 0;
						}
					}
				}
			}
	
			m_TrackInfo.EMREL = 0;
			m_TrackInfo.ROUTEDIR = 0;
			m_TrackInfo.ROUTE = 1;
			m_TrackInfo.TKREQUEST = 0;
			m_TrackInfo.RouteNo = 0;
			m_TrackInfo.OVERLAP = 0;
			m_TrackInfo.MULTIDEST = 0;

			// 2007.7.18  비상해정 시 Overlap이 다시 설정되는 현상때문에 추가 
			nEmergencyStop[nTkID] = TRUE;

			// 2006.1.3  Emergency route release 한 후 Call on 진로를 설정하면 아래의 정보가 해정이 되지 않아 Overlap전철기가 설정되므로 수정함..
			for (BYTE nSwID = 0 ; nSwID <= m_TableBase.InfoSwitch.nCount ; nSwID++)
			{
				if (m_pSwitchOverTrack1[nSwID].nTrackID == nTkID ) 
				{
					m_pSwitchOverTrack1[nSwID].nTrackID = 0;
					m_pSwitchOverTrack1[nSwID].nDelay = 0;
					m_pSwitchOverTrack1[nSwID].wRouteID = 0;

				}

				if (m_pSwitchOverTrack2[nSwID].nTrackID == nTkID ) 
				{
					m_pSwitchOverTrack2[nSwID].nTrackID = 0;
					m_pSwitchOverTrack2[nSwID].nDelay = 0;
					m_pSwitchOverTrack2[nSwID].wRouteID = 0;
				}

				if (m_pSwitchRouteTrack[nSwID * 2] == nTkID)
				{
					m_pSwitchRouteTrack[nSwID * 2] = 0;
				}

				if (m_pSwitchRouteTrack[nSwID * 2 + 1] == nTkID)
				{
					m_pSwitchRouteTrack[nSwID * 2 + 1] = 0;
				}
			}

			// 진로의 종착 궤도일 경우
			if ( (m_pLastTrack[ nTkID ] & TRACK_MASK) == nTkID )
			{
				// 20140902 sjhong - 비상해정 후 Overlap 궤도의 Overlap 해정을 담당.. 
				BYTE bIsSwitchInOverlap = FALSE;
				BYTE nOverTkID = m_pOverTrack[nTkID] & TRACK_MASK;
				BYTE nRouteDirection = m_pOverTrack[nTkID] & TRACK_DIR_MASK;

				if(nOverTkID)
				{
					ScrInfoTrack *pOverTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nOverTkID);
					if (nRouteDirection)
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
						ClearOverlap(pOverTrkInfo->RightToLeftRouteNo, 0);
					}
					else
					{
						// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
						ClearOverlap(pOverTrkInfo->LeftToRightRouteNo, 0);
					}
					
					// Overlap Track 테이블 초기화.
					BYTE nNextTrkID = 0;
					BYTE nDelTrkID = nTkID;
					while(nDelTrkID != 0)
					{
						nNextTrkID = m_pOverTrack[nDelTrkID];
						m_pOverTrack[nDelTrkID] = 0;
						nDelTrkID = nNextTrkID & TRACK_MASK;
					}
				}

				// 종착 궤도 정보 클리어
				m_pLastTrack[ nTkID ] = 0;
			}
		}

		if (m_TrackInfo.ROUTE) 
		{
			m_TrackInfo.TKREQUEST = 0;
		}

		int_put(TrackPut);
    }

	nSysVar.bTrackOcc = 0;
	if (nAccTrack > 0)
	{
		nSysVar.bTrackOcc = 1;
	}

	int nTotal = m_TableBase.InfoTrack.nCount;

//	if (nTotal < 20)  // 궤도회로가 20개 보다 작을 경우 Minimize 갯수를 20개로 정의한다...
//		nTotal = 20;

	return nTotal >= nAccTrack;
}

void CEipEmu::SetErrorCode( BYTE nCode, BYTE *pErrCode ) 
{
	UnitStatusType &UnitState = ((DataHeaderType *)m_pVMEM)->UnitState;

	if (UnitState.bSysRun )
	//	if ( nSysVar.bSysRun ) 
	{
		pErrCode[1] = nCode;	// 0x19
		pErrCode[2] = LoopCountH + 1;	// 0x1A
		pErrCode[0] = 0xFF;		// 0x18
	}
	//nSysVar.bSysRun = 0;	//		System Down
	//UnitState.bSysRun = 0;
}

BYTE CEipEmu::GetTNIQue()
{
	if ( m_nTNIQueInPtr == m_nTNIQueOutPtr ) return 0xFF;
	BYTE res = m_pTNIQue[ m_nTNIQueOutPtr++ ];
	m_nTNIQueOutPtr &= 0x0F;
	return res;
}

void CEipEmu::SetTNIQue( BYTE nTNI )
{
	if ( ((m_nTNIQueInPtr + 1) & 0x0F) == m_nTNIQueOutPtr ) return;
	m_pTNIQue[ m_nTNIQueInPtr++ ] = nTNI;
	m_nTNIQueInPtr &= 0x0F;

	return;
}



BYTE CEipEmu::GetSwitchWRState( BYTE nSwID )
{
	ScrInfoSwitch *pSwInfo = (ScrInfoSwitch *)int_get(SwitchGet, nSwID );
	BYTE swpos = (BYTE)pSwInfo->WR_P;
	return swpos;
}


// 20131227 sjhong - 전철기 방향까지 정확히 일치하는 진로를 탐색하도록 수정함.
unsigned short CEipEmu::GetRouteNo( BYTE nSignalID, BYTE CS /* 1 = Callon, Shunt Route */, BYTE nDestID)
{
	BYTE n/*, nDestTrackID*/;
	BYTE nRouteCount = m_pSignalInfoTable[ nSignalID ].nRouteCount;
	WORD wRNO = m_pSignalInfoTable[ nSignalID ].wRouteNo;

    for (n = 0; n < nRouteCount; n++) 
	{
		DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRNO ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
		BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
#if 1
		if(pInfo[RT_OFFSET_ROUTEORDER] == nDestID)
		{
			return wRNO;
		}
#else
		nDestTrackID = pInfo[RT_OFFSET_TO];

		if (CS == 1)
		{
			if ( !(pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON) && !(pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL) ) 
			{
				wRNO++;
				continue;
			}
		}

		if (nDestID == nDestTrackID) 
		{
        	//BYTE nSigID = RouteToSignal(nRNO);

			return wRNO;
		}
#endif
		wRNO++;
	}
	return 0;
}

#if 0
BYTE CEipEmu::DetectSwitchDirConflict(BYTE *pSwitchA, BYTE nCountA, BYTE *pSwitchB, BYTE nCountB)
{
	BYTE i, j;

	if(nCountA <= 0 || nCountB <= 0 || pSwitchA == NULL || pSwitchB == NULL)
	{
		return 0;
	}


}
#endif

// 20140701 sjhong - 비상해정용 함수 전면 수정 (TrackInfo에 저장된 RouteNo를 사용하도록 수정)
int CEipEmu::RemoveRouteQue( BYTE nTrkID, BYTE arg2, BYTE nDependSignal )
{
	if(nTrkID)
	{
		ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)int_get(TrackGet, nTrkID);
		WORD wRouteNo = pTrkInfo->RouteNo & ROUTE_MASK;

		if(wRouteNo)
		{
			DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pInfo = &m_pInfoMem[nInfoPtr];
			BYTE nSignalID = pInfo[RT_OFFSET_SIGNAL];

			// 20140701 sjhong - Track이 가진 진로번호와 Signal이 가진 진로번호가 다른 경우엔 처리하지 않는다. (발생하면 안되는 경우임)
			if(wRouteNo == (m_pRouteQue[nSignalID] & ROUTE_MASK))
			{
				m_pRouteQue[nSignalID] |= ROUTE_ONEMCANCEL;
			}

			BYTE nDependSigID = 0;
			for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN; cSigOffset <= RT_OFFSET_DEPENDSIG_END; cSigOffset++)
			{
				nDependSigID = (pInfo[cSigOffset] & SIGNALID_MASK);
				if(nDependSigID && ((m_pSignalInfoTable[nDependSigID].nSignalType & SIGNAL_MASK) != SIGNALTYPE_OHOME) && (m_pRouteQue[nDependSigID] & ROUTE_DEPENDSET))
				{
					m_pRouteQue[nDependSigID] = 0;	// 진로 큐에서 삭제......
					m_pOwnerRouteQue[nDependSigID] = 0;	// 20140423 sjhong - 주진로 정보를 삭제함.
					
					ScrInfoSignal *pDependInfo = (ScrInfoSignal *)int_get (SignalGet, nDependSigID);
													
					pDependInfo->DESTID	= 0;
					pDependInfo->DESTTRACK = 0;
					
					pDependInfo->ENABLED = 0;
					pDependInfo->TM1 = 0;
					
					pDependInfo->OSS = 0;
					pDependInfo->CS = 0;
				}
			}

			return wRouteNo;
		}
	}

	return 0;
}

//DEL int CEipEmu::FindTrackAtRoute( short nRoute,  BYTE nTrkID, BYTE nDependSignal )
//DEL {
//DEL 	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
//DEL 	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
//DEL 	BOOL bTrkOcc = FALSE;
//DEL 
//DEL 	if (!nDependSignal)
//DEL 	{
//DEL 		BYTE nDestTrackID = pInfo[ RT_OFFSET_TO ];
//DEL 		if (nTrkID != nDestTrackID)
//DEL 			return 0;
//DEL 	}
//DEL 	else
//DEL 		bTrkOcc = TRUE;
//DEL 
//DEL 	BYTE nID;
//DEL 	unsigned short i, nCount;
//DEL 
//DEL 	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)pInfo;
//DEL 	BYTE *p = pInfo + pRouteBasPtr->nOfsRoute;
//DEL 	nCount = *p++;
//DEL 
//DEL 	if (nCount == 0)
//DEL 		return 1;
//DEL 
//DEL 	for (i=0; i<nCount; i++) 
//DEL 	{
//DEL 		nID = *p++;
//DEL 		ScrInfoTrack *pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, nID);
//DEL 		if (!pTrkInfo->TRACK || pTrkInfo->ROUTE)
//DEL 			bTrkOcc = TRUE;
//DEL 
//DEL 		if ( nID == nTrkID && bTrkOcc == TRUE ) 
//DEL 		{
//DEL 			return 1;
//DEL 		}
//DEL 	}
//DEL 
//DEL 	return 0;
//DEL }

//DEL void CEipEmu::FindConRoute( short wRoute )
//DEL {
//DEL 	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRoute ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
//DEL 	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
//DEL 
//DEL 	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)pInfo;
//DEL 	BYTE cDepDestTrack = pInfo[RT_OFFSET_FROM];
//DEL 	BYTE cDepSignal = (pInfo[RT_OFFSET_DEPENDSIG1] & SIGNALID_MASK);
//DEL 
//DEL 	if(cDepSignal)
//DEL 	{
//DEL 		if(m_pRouteQue[cDepSignal] & ROUTE_DEPENDSET)
//DEL 		{
//DEL             RemoveRouteQue(cDepDestTrack, 0, cDepSignal);
//DEL 		}
//DEL 	}
//DEL 
//DEL 	return;
//DEL }

WORD CEipEmu::FindPresetDependRoute(BYTE nRouteCount, WORD wBaseDependRouteNo)
{
	wBaseDependRouteNo	&= ROUTE_MASK;

	DWORD nInfoPtr		= *(DWORD*)&m_pRouteAddTable[m_wRouteNo & ROUTE_MASK];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pMainInfo		= &m_pInfoMem[nInfoPtr];
	RouteBasPtr *pMainRoutePtr = (RouteBasPtr *)pMainInfo;

	for(BYTE nIdx=0 ; nIdx < nRouteCount ; nIdx++)
	{
		//2006.11.16 Overlap switch 고장 시 Preset shunt signal stop을 위해 설정...
		BYTE nOverlapSWCount = 0;

		nInfoPtr = *(DWORD*)&m_pRouteAddTable[wBaseDependRouteNo + nIdx];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
		BYTE *pDependInfo	= &m_pInfoMem[nInfoPtr];
		RouteBasPtr *pDependRoutePtr = (RouteBasPtr *)pDependInfo;

		BYTE nDependSigID	= pDependInfo[RT_OFFSET_SIGNAL];

		
		// 20140701 sjhong - 중계 진로를 검색할 때 Track 리스트도 검사한다.
		BYTE *pMainTkPtr	= pMainInfo + pMainRoutePtr->nOfsTrack;
		BYTE *pDependTkPtr	= pDependInfo + pDependRoutePtr->nOfsTrack;
		if(*pMainTkPtr && *pDependTkPtr)
		{
			BYTE nMainTkCount	= *pMainTkPtr++ - 1;
			BYTE nDependTkCount	= *pDependTkPtr++ - 1;
			BYTE nMatchTkCount = 0;
			
			// 본 진로의 모든 트랙을 검색하여 
			for(BYTE i = 0 ; i < nMainTkCount; i += 2, pMainTkPtr += 2)
			{ 
				if(*pMainTkPtr == *pDependTkPtr)
				{
					pDependTkPtr += 2;
					nMatchTkCount++;
				}
			}
			
			// 현재 검사 진로의 모든 트랙이 본 진로에 포함되지 않으므로 이 진로는 패쓰한다.
			if(nMatchTkCount != nDependTkCount / 2)
			{
				continue;
			}
		}

		// 20140701 sjhong - 중계 진로를 검색할 때 Route트랙 리스트도 검사한다.
		pMainTkPtr	= pMainInfo + pMainRoutePtr->nOfsRoute;
		pDependTkPtr	= pDependInfo + pDependRoutePtr->nOfsRoute;
		if(*pMainTkPtr && *pDependTkPtr)
		{
			BYTE nMainTkCount	= *pMainTkPtr++;
			BYTE nDependTkCount	= *pDependTkPtr++;
			BYTE nMatchTkCount = 0;
			
			// 본 진로의 모든 트랙을 검색하여 
			for(BYTE i = 0 ; i < nMainTkCount; i++, pMainTkPtr++)
			{ 
				if(*pMainTkPtr == *pDependTkPtr)
				{
					pDependTkPtr ++;
					nMatchTkCount++;
				}
			}

			// 현재 검사 진로의 모든 트랙이 본 진로에 포함되지 않으므로 이 진로는 패쓰한다.
			if(nMatchTkCount != nDependTkCount)
			{
				continue;
			}
		}
	
		BYTE *pMainSWPtr	= pMainInfo + RT_OFFSET_SWITCH;
		BYTE *pDependSWPtr	= pDependInfo + RT_OFFSET_SWITCH;
		if(*pMainSWPtr && *pDependSWPtr)
		{
			BYTE i, nCompareCount=0, nICount=0;

			BYTE nMainSWCount	= *pMainSWPtr++;
			BYTE nMainDestTrkID		= *pMainSWPtr++;
			BYTE nMainOverRelTime	= *pMainSWPtr++;
			BYTE *pMainFirstSWPtr	= pMainSWPtr;
	
			BYTE nDependSWCount		= *pDependSWPtr++;
			BYTE nDependDestTrkID	= *pDependSWPtr++;
			BYTE nDependOverRelTime	= *pDependSWPtr++;

			BYTE nDependSWID		= *pDependSWPtr++;
			BYTE nDependOverFlag	= *pDependSWPtr++;

			// 첫 2 Byte는 종착궤도ID와 Overlap쇄정시간 값이므로 그 이후부터 탐색한다.
			for(i = 2 ; i < nMainSWCount ; i += 2)
			{
				// 검사할 첫 신호기를 설정한다.
				BYTE nMainSWID		= *pMainSWPtr++;
				BYTE nMainOverFlag	= *pMainSWPtr++;

				BYTE nMainSWPos		= nMainSWID & 0x80;
				nMainSWID &= 0x7f;

				// 20140425 sjhong - 본 진로가 SHUNT 진로이거나 Overlap 구간인 전철기는 중계 진로 탐색 매칭에서 제외한다.)
				if(m_SignalInfo.U || ((m_pSignalInfoTable[nDependSigID].nSignalType & SIGNAL_MASK)== SIGNALTYPE_GSHUNT))
				{
					// Overlap 구간 전철기 구분 방식 변경으로 수정
					if(nMainOverFlag & SWT_IN_OVERLAP)
					{
						continue;
					}
				} 

				// 본 진로에 진로 구간외 전철기가 있을 경우 비교하지 않고 무시한다.
				if(nMainOverFlag & SWT_COMBO_SLAVE)
				{
					continue;
				}
				
				// 현재 전철기가 진로 구간외 전철기이면 본 진로의 전철기에서 동일 전철기가 있는지 확인한다.
				if(nDependOverFlag & SWT_COMBO_SLAVE)
				{
					BYTE nSWPos = *pMainFirstSWPtr & 0x80;
					BYTE nSWID = *pMainFirstSWPtr & 0x7f;

					for(int j = 2 ; j < nMainSWCount ; j += 2)
					{
						// 동일 전철기가 있으나, 전철기의 방향이 반대이므로 Loop를 빠져나간다.
						if(((nDependSWID & 0x7f) == nSWID) && ((nDependSWID & 0x80) != nSWPos))
						{
							break;
						}

						nSWPos = *(pMainFirstSWPtr + j) & 0x80;
						nSWID = *(pMainFirstSWPtr + j) & 0x7f;
					}

					// 동일 전철기가 있으나, 전철기의 방향이 반대이므로 Loop를 빠져나간다.
					if(j != nMainSWCount)
					{
						break;
					}

					nDependSWID		= *pDependSWPtr++;
					nDependOverFlag	= *pDependSWPtr++;

					nCompareCount++;
					nICount += 2;
				}

				BYTE nCheckSWPos = nDependSWID & 0x80;
				BYTE nCheckSWID = nDependSWID & 0x7f;
				
				// 비교할 진로의 현재 전철기 값이 0이므로 Loop를 빠져나간다.
				if(nCheckSWID == 0)
				{
					break;
				}
				
				// 본 진로의 현재 전철기와 비교할 진로의 현재 전철기의 ID가 동일하면 방향 체크 시작
				if(nCheckSWID != nMainSWID)
				{
					continue;
				}

				// 본 진로와 비교할 진로의 현재 전철기 방향이 동일한지 확인
				if((nCheckSWPos == nMainSWPos)/* && (nDependOverFlag == nMainOverFlag)*/)
				{
					nCompareCount++;
					nICount += 2;

					nDependSWID		= *pDependSWPtr++;
					nDependOverFlag	= *pDependSWPtr++;
				}
				else
				{
					// 전철기 방향이 다르므로 이 진로는 무시한다.
					break;
				}

				if(nICount >= nDependSWCount - 2)
				{
					// 비교할 진로의 전철기를 모두 검사하였으므로 Loop를 빠져나간다.
					break;
				}
			}

			if (nCompareCount * 2 == nDependSWCount - 2)
			{
 				return wBaseDependRouteNo + nIdx;
			}
		}
	}

	return 0;
}


//2006.11.16 Overlap 표시를 위해 추가...
void CEipEmu::ClearOverlap( short nRoute, BOOL bOption )
{
	if ((nRoute & ROUTE_MASK) == 0) 
		return;

	DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ nRoute & ROUTE_MASK ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
	BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블...
	BYTE nSigID = pInfo[RT_OFFSET_SIGNAL];


	BYTE nRouteDirection = 0;
	if ((m_pSignalInfoTable[nSigID].nSignalType & SIGNALTYPE_DIR_BIT) >> 2)   // 좌,우행 결정...  Left -> 1
		nRouteDirection = 1;

	// Track
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr*)pInfo;
	BYTE *p = pInfo + pRouteBasPtr->nOfsTrack;

	ScrInfoTrack *pTrkInfo;	
	BYTE nTrackID[20]={0};
	unsigned short j, nTrackCount, nCount = *p++;

	nTrackCount = 0;			
	for (j=0; j<nCount; j++) 
	{
		if (*p)
		{
			nTrackID[nTrackCount++] = *p++;
		}
		else
			*p++;
	}

	nCount = *p++;

	for (j=nCount; j<nTrackCount; j++)
	{
		if(nTrackID[j])
		{
			pTrkInfo = (ScrInfoTrack *)int_get(TrackGet, (nTrackID[j] & 0x7f));
			if(pTrkInfo)
			{
				if (nRouteDirection)
				{
					// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
					pTrkInfo->RightToLeftRouteNo = 0;
				}
				else
				{
					// 20130904 sjhong - 진로 설정 시 Overlap 궤도에 진로 방향에 따라 진로 번호를 삭제함.
					pTrkInfo->LeftToRightRouteNo = 0;
				}
			}
		}	    
	}
}


BYTE CEipEmu::GetAllAXLFailStatus() 
{
	ScrInfoBlock *pBlkInfo = NULL;

	BYTE bAXLFail = 0;

	if(!m_ConfigList.B1B2_NOAXL)
	{
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB1ID);
		if(pBlkInfo)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}

		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB2ID);
		if(pBlkInfo)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
	}

	if(!m_ConfigList.B3B4_NOAXL)
	{
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB3ID);
		if(m_nB3ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}

		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB4ID);
		if(m_nB4ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
	}
	
	if(!m_ConfigList.B11B12_NOAXL)
	{
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB11ID);
		if(m_nB11ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
		
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB12ID);
		if(m_nB12ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
	}
	
	if(!m_ConfigList.B13B14_NOAXL)
	{
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB13ID);
		if(m_nB13ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
		
		pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB14ID);
		if(m_nB14ID)
		{
			bAXLFail |= !pBlkInfo->AXLDST;
		}
	}

	// 20140301 sjhong - Axle Counter 공통 정보에 Axle Counter Track의 Disturb 상태를 포함.
	if(m_ConfigList.AXLCNT_TRACK)
	{
		BYTE i;
		WORD nTrack = m_TableBase.InfoTrack.nCount;
		ScrInfoTrack *pTrackInfo;

		for(i = 1; i <= nTrack; i++) 
		{
			pTrackInfo = (ScrInfoTrack *)int_get(TrackGet, i);
			if(pTrackInfo)
			{
				bAXLFail |= !pTrackInfo->DISTURB;
			}
		}
	}

	return bAXLFail;
}

BYTE CEipEmu::VerifyInfoSwitchTable(BYTE *pCurData, BYTE *pNewData)
{
	if(pCurData == NULL || pNewData == NULL)
	{
		return 0;
	}	

	short i;
	short nCount = m_TableBase.InfoSwitch.nCount;
	short nStart = m_TableBase.InfoSwitch.nStart;

	BYTE	cMask[3] = {0,};
	ScrInfoSwitch *pMaskSwitch = (ScrInfoSwitch*)cMask;

	// Check할 상태 Bit들을 선택한다.
	pMaskSwitch->WRO_P		= 0;
	pMaskSwitch->WRO_M		= 0;
	pMaskSwitch->FREE		= 0;
	pMaskSwitch->WLR		= 1;
	pMaskSwitch->WR_M		= 1;
	pMaskSwitch->WR_P		= 1;
	pMaskSwitch->KR_M		= 1;
	pMaskSwitch->KR_P		= 1;

	pMaskSwitch->TRACKLOCK	= 0;
	pMaskSwitch->SWFAIL		= 0;
	pMaskSwitch->WLRO		= 0;
	pMaskSwitch->ON_TIMER	= 0;
	pMaskSwitch->REQUEST_M	= 0;
	pMaskSwitch->REQUEST_P	= 0;
	pMaskSwitch->ROUTELOCK	= 0;
	pMaskSwitch->BACKROUTE	= 0;

	pMaskSwitch->PASSROUTE	= 0;
	pMaskSwitch->BLOCK		= 1;
	pMaskSwitch->INROUTE	= 0;
	pMaskSwitch->ROUTE_M	= 0;
	pMaskSwitch->ROUTE_P	= 0;
	pMaskSwitch->WKEYKR		= 0;
	pMaskSwitch->KEYOPER	= 0;
	pMaskSwitch->NOUSED		= 0;

	for(i = 1; i <= nCount; i++)
	{
		// Mask를 씌워서 나온 값이 일치하는지 확인한다.
		if((pCurData[nStart + i * SIZE_INFO_SWITCH] & cMask[0]) != (pNewData[nStart + i * SIZE_INFO_SWITCH] & cMask[0])
		|| (pCurData[nStart + i * SIZE_INFO_SWITCH + 1] & cMask[1]) != (pNewData[nStart + i * SIZE_INFO_SWITCH + 1] & cMask[1])
		|| (pCurData[nStart + i * SIZE_INFO_SWITCH + 2] & cMask[2]) != (pNewData[nStart + i * SIZE_INFO_SWITCH + 2] & cMask[2]))
		{
			// 다른 비트가 하나라도 있으면 0
#ifndef _WINDOWS
			LogErr(LOG_TYPE_GEN, 
				"Point Machine (InfoSwitch) Data Missmatched!!!!, Cur[%02X,%02X,%02X], New[%02X,%02X,%02X]",
				(pCurData[nStart + i * SIZE_INFO_SWITCH] & cMask[0]), 
				(pCurData[nStart + i * SIZE_INFO_SWITCH + 1] & cMask[1]), 
				(pCurData[nStart + i * SIZE_INFO_SWITCH + 2] & cMask[2]), 
				(pNewData[nStart + i * SIZE_INFO_SWITCH] & cMask[0]),
				(pNewData[nStart + i * SIZE_INFO_SWITCH + 1] & cMask[1]),
				(pNewData[nStart + i * SIZE_INFO_SWITCH + 2] & cMask[2]));
#endif

			return 0;
		}
	}

#ifndef _WINDOWS
	LogDbg(LOG_TYPE_GEN, "Point Machine (InfoSwitch) Data Identical!!!!");
#endif
	// 정확히 일치하면 1
	return 1;
}

void CEipEmu::UpdateAXLStatusByConfig()
{
	BYTE nBlockType = 0;
	ScrInfoBlock *pB1Info, *pB2Info, *pB3Info, *pB4Info, *pB11Info, *pB12Info, *pB13Info, *pB14Info;

#ifndef _WINDOWS
//	LogDbg(LOG_TYPE_EIP, "AXLCNT_TRACK = %d, B1B2_AXLSYNC = %d, B3B4_AXLSYNC = %d", 
//		   m_ConfigList.AXLCNT_TRACK, m_ConfigList.B1B2_AXLSYNC, m_ConfigList.B3B4_AXLSYNC);
#endif

		pB1Info = (ScrInfoBlock *)int_get(SignalGet, m_nB1ID);
		pB2Info = (ScrInfoBlock *)int_get(SignalGet, m_nB2ID);

	if(m_ConfigList.B1B2_NOAXL)
	{
		pB1Info->AXLOCC = 1;
		pB1Info->AXLDST = 1;
		pB2Info->AXLOCC = 1;
		pB2Info->AXLDST = 1;
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B1 AXL[Occ = %d, Dst = %d], B2 AXL[Occ = %d, Dst = %d]", pB1Info->AXLOCC, pB1Info->AXLDST, pB2Info->AXLOCC, pB2Info->AXLDST);
#endif
	}
	else if(m_ConfigList.B1B2_AXLSYNC)
	{
		pB2Info->AXLOCC = pB1Info->AXLOCC;
		pB2Info->AXLDST = pB1Info->AXLDST;

		pB2Info->SAXLOCC = pB1Info->SAXLOCC;
		pB2Info->SAXLDST = pB1Info->SAXLDST;

#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B2 AXL[Occ = %d, Dst = %d], SAXL[Occ = %d, Dst = %d]", pB2Info->AXLOCC, pB2Info->AXLDST, pB2Info->SAXLOCC, pB2Info->SAXLDST);
#endif
	}

		pB3Info = (ScrInfoBlock *)int_get(SignalGet, m_nB3ID);
		pB4Info = (ScrInfoBlock *)int_get(SignalGet, m_nB4ID);
		
	if(m_ConfigList.B3B4_NOAXL)
	{
		pB3Info->AXLOCC = 1;
		pB3Info->AXLDST = 1;
		pB4Info->AXLOCC = 1;
		pB4Info->AXLDST = 1;
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B3 AXL[Occ = %d, Dst = %d], B4 AXL[Occ = %d, Dst = %d]", pB3Info->AXLOCC, pB3Info->AXLDST, pB4Info->AXLOCC, pB4Info->AXLDST);
#endif
	}
	else if(m_ConfigList.B3B4_AXLSYNC)
	{
		pB4Info->AXLOCC = pB3Info->AXLOCC;
		pB4Info->AXLDST	= pB3Info->AXLDST;

		pB4Info->SAXLOCC	= pB3Info->SAXLOCC;
		pB4Info->SAXLDST	= pB3Info->SAXLDST;

#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B4 AXL[Occ = %d, Dst = %d], SAXL[Occ = %d, Dst = %d]", pB4Info->AXLOCC, pB4Info->AXLDST, pB4Info->SAXLOCC, pB4Info->SAXLDST);
#endif
	}

		pB11Info = (ScrInfoBlock *)int_get(SignalGet, m_nB11ID);
		pB12Info = (ScrInfoBlock *)int_get(SignalGet, m_nB12ID);
		
	if(m_ConfigList.B11B12_NOAXL)
	{
		pB11Info->AXLOCC = 1;
		pB11Info->AXLDST = 1;
		pB12Info->AXLOCC = 1;
		pB12Info->AXLDST = 1;
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B11 AXL[Occ = %d, Dst = %d], B12 AXL[Occ = %d, Dst = %d]", pB11Info->AXLOCC, pB11Info->AXLDST, pB12Info->AXLOCC, pB12Info->AXLDST);
#endif
	}
	else if(m_ConfigList.B11B12_AXLSYNC)
	{
		pB12Info->AXLOCC = pB11Info->AXLOCC;
		pB12Info->AXLDST = pB11Info->AXLDST;
		
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B12 AXL[Occ = %d, Dst = %d]", pB12Info->AXLOCC, pB12Info->AXLDST);
#endif
	}
	
	
	pB13Info = (ScrInfoBlock *)int_get(SignalGet, m_nB13ID);
	pB14Info = (ScrInfoBlock *)int_get(SignalGet, m_nB14ID);
	
	if(m_ConfigList.B13B14_NOAXL)
	{
		pB13Info->AXLOCC = 1;
		pB13Info->AXLDST = 1;
		pB14Info->AXLOCC = 1;
		pB14Info->AXLDST = 1;
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B13 AXL[Occ = %d, Dst = %d], B14 AXL[Occ = %d, Dst = %d]", pB13Info->AXLOCC, pB13Info->AXLDST, pB14Info->AXLOCC, pB14Info->AXLDST);
#endif
	}
	else if(m_ConfigList.B13B14_AXLSYNC)
	{
		pB13Info = (ScrInfoBlock *)int_get(SignalGet, m_nB13ID);
		pB14Info = (ScrInfoBlock *)int_get(SignalGet, m_nB14ID);
		
		pB14Info->AXLOCC = pB13Info->AXLOCC;
		pB14Info->AXLDST = pB13Info->AXLDST;
		
#ifndef _WINDOWS
		LogDbg(LOG_TYPE_EIP, "After B14 AXL[Occ = %d, Dst = %d]", pB14Info->AXLOCC, pB14Info->AXLDST);
#endif
	}
}

int CEipEmu::MakeCtcElementImage( BYTE *pBuffer ) 
{
	WORD *pTable = m_pCtcElementTable;
	
	WORD nSize = *pTable++;

	DataHeaderType* pHeader = (DataHeaderType*)m_pVMEM;

	//if ( nSize > 256 ) nSize = 255;

	*(WORD*)pBuffer = nSize;
	pBuffer += 2;

	while ( nSize > 0 ) 
	{
		CRemoteLinkTableType &Link = *(CRemoteLinkTableType*)(pTable++);
		BYTE &nType = Link.m_nType;
		BYTE &nObjNo = Link.m_nObjNo;
		void *pObject;

		// Route 정보 Element의 경우 별도 처리
		if ( nType & CTC_LK_RO ) 
		{
			BYTE bIsPass = FALSE;

			CRemRoute &nState = *(CRemRoute*)pBuffer;

			WORD wRouteNo = (nType & 0x7F) * 256 + nObjNo;
			DWORD nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pInfoMem = &m_pInfoMem[nInfoPtr];	// 연동도표 정보 테이블
			BYTE nSignalID = pInfoMem[RT_OFFSET_SIGNAL];
			BYTE nButtonID = pInfoMem[RT_OFFSET_BUTTON];
			BYTE nRouteTag = pInfoMem[RT_OFFSET_ROUTETAG];

#ifndef _WINDOWS 
			LogDbg(LOG_TYPE_CTC, "wRouteNo = 0x%X, nSignalID = 0x%X, nButtonID = 0x%X, nRouteTag = %d", wRouteNo, nSignalID, nButtonID, nRouteTag);
#endif
			WORD wRouteQue = m_pRouteQue[nSignalID] & ROUTE_MASK;
			if(wRouteQue > 0)
			{
#ifndef _WINDOWS 
				LogDbg(LOG_TYPE_CTC, "wRouteQue = 0x%X, nSignalID = 0x%X, BUTTON = 0x%X, ROUTETAG = 0x%X", wRouteQue, nSignalID, pInfoMem[RT_OFFSET_BUTTON], pInfoMem[RT_OFFSET_ROUTETAG]);
#endif
				nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteQue ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				pInfoMem = (BYTE*)&m_pInfoMem[nInfoPtr];

				if((nButtonID == pInfoMem[RT_OFFSET_BUTTON]) && (nRouteTag == pInfoMem[RT_OFFSET_ROUTETAG]))
				{
					BYTE nSignalType = m_pSignalInfoTable[nSignalID].nSignalType & SIGNAL_MASK;
					BYTE nRouteStatus = pInfoMem[ RT_OFFSET_ROUTESTATUS ];
					BYTE nTrackID = pInfoMem[ RT_OFFSET_TO ];
				
					ScrInfoSignal *pSignal 	= (ScrInfoSignal *)int_get(SignalGet, nSignalID);
					ScrInfoTrack *pTrack 	= (ScrInfoTrack *)int_get(TrackGet, nTrackID);

#ifndef _WINDOWS
					LogDbg(LOG_TYPE_CTC, "wRouteQue = 0x%X, nSignalID = 0x%X, nTrackID = 0x%X, nRouteStatus - 0x%X\n", 
										 wRouteQue, nSignalID, nTrackID, nRouteStatus);
#endif
	
					if(nRouteStatus & RT_CALLON) 
					{
						nState.RO_CALLRQ 	= pSignal->REQUEST;
						nState.RO_CALL		= !pTrack->ROUTE;
					}
					else if((nRouteStatus & RT_SHUNTSIGNAL) 
					&& !(((nSignalType == SIGNALTYPE_SHUNT) || (nSignalType == SIGNALTYPE_ISHUNT) || (nSignalType == SIGNALTYPE_GSHUNT)) && (pSignal->OSS == 1)))
					{
						nState.RO_SHNTRQ 	= pSignal->REQUEST;
						nState.RO_SHNT		= !pTrack->ROUTE;
					}
					else
					{
						nState.RO_MAINRQ 	= pSignal->REQUEST;
						nState.RO_MAIN		= !pTrack->ROUTE;
					}
#ifndef _WINDOWS
					LogDbg(LOG_TYPE_CTC, "CALLRQ = %d, CALL = %d, SHNTRQ = %d, SHNT = %d, MAINRQ = %d, MAIN = %d\n",
					nState.RO_CALLRQ, nState.RO_CALL, nState.RO_SHNTRQ, nState.RO_SHNT, nState.RO_MAINRQ, nState.RO_MAIN);
#endif
				}
				else
				{
				nState.RO_CALLRQ 	= 0;
				nState.RO_CALL		= 0;
				nState.RO_SHNTRQ 	= 0;
				nState.RO_SHNT		= 0; 
				nState.RO_MAINRQ 	= 0;
				nState.RO_MAIN		= 0;
				}

			}
			else
			{
				nState.RO_CALLRQ 	= 0;
				nState.RO_CALL		= 0;
				nState.RO_SHNTRQ 	= 0;
				nState.RO_SHNT		= 0; 
				nState.RO_MAINRQ 	= 0;
				nState.RO_MAIN		= 0;
			}
		}
		else 
		{
			switch ( nType ) 
			{
			case CTC_LK_MODE:
				((CRemMode*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_POWER:
				((CRemPower*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_POWER_EXT:
				((CRemPowerExt*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_COMMON:
				((CRemCommon*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_KT_UPDN:
				((CRemKTUPDN*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_KT_ABCD:
				((CRemKTABCD*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_CBI:
				((CRemCBI*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_MCCR:
				((CRemMCCR*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_ADJCOMM:
				if(m_ConfigList.REVERSE_LAYOUT)
				{
					((CRemAdjComm*)pBuffer)->MakeRemoteDataRev( m_pVMEM );
				}
				else
				{
					((CRemAdjComm*)pBuffer)->MakeRemoteData( m_pVMEM );
				}
				break;
			case CTC_LK_AXL:
				{
					CRemAXL &cAXL = *(CRemAXL*)pBuffer;
					ScrInfoBlock *pBlkInfo = NULL;
					pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB2ID);
					if(pBlkInfo)
					{
						if(m_ConfigList.REVERSE_LAYOUT)
						{
							if(m_ConfigList.B3B4_SWAPAXL)
							{
								cAXL.AXL_OCC3 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST3 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC4 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST4 	= pBlkInfo->AXLDST;
							}
						}
						else
						{
							if(m_ConfigList.B1B2_SWAPAXL)
							{
								cAXL.AXL_OCC2 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST2 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC1 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST1 	= pBlkInfo->AXLDST;
							}
						}
					}

					pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB1ID);
					if(pBlkInfo)
					{
						if(m_ConfigList.REVERSE_LAYOUT)
						{
							if(m_ConfigList.B3B4_SWAPAXL)
							{
								cAXL.AXL_OCC4 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST4 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC3 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST3 	= pBlkInfo->AXLDST;
							}
						}
						else
						{
							if(m_ConfigList.B1B2_SWAPAXL)
							{
								cAXL.AXL_OCC1 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST1 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC2 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST2 	= pBlkInfo->AXLDST;
							}
						}
					}

					pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB3ID);
					if(pBlkInfo)
					{
						if(m_ConfigList.REVERSE_LAYOUT)
						{
							if(m_ConfigList.B1B2_SWAPAXL)
							{
								cAXL.AXL_OCC1 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST1 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC2 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST2 	= pBlkInfo->AXLDST;
							}
						}
						else
						{
							if(m_ConfigList.B3B4_SWAPAXL)
							{
								cAXL.AXL_OCC4 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST4 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC3 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST3 	= pBlkInfo->AXLDST;
							}
						}
					}

					pBlkInfo = (ScrInfoBlock *)int_get(SignalGet, m_nB4ID);
					if(pBlkInfo)
					{
						if(m_ConfigList.REVERSE_LAYOUT)
						{
							if(m_ConfigList.B1B2_SWAPAXL)
							{
								cAXL.AXL_OCC2 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST2 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC1 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST1 	= pBlkInfo->AXLDST;
							}
						}
						else
						{
							if(m_ConfigList.B3B4_SWAPAXL)
							{
								cAXL.AXL_OCC3 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST3 	= pBlkInfo->AXLDST;
							}
							else
							{
								cAXL.AXL_OCC4 	= pBlkInfo->AXLOCC;
								cAXL.AXL_DST4 	= pBlkInfo->AXLDST;
							}
						}
					}
				}
				break;
			case CTC_LK_CALLON_LSB:
				((CRemCallOnLSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_CALLON_MSB:
				((CRemCallOnMSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_ERR_LSB:
				((CRemERRLSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_ERR_MSB:
				((CRemERRMSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_AXLB2_LSB:
				((CRemAXLB2LSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_AXLB2_MSB:
				((CRemAXLB2MSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_AXLB4_LSB:
				((CRemAXLB4LSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_AXLB4_MSB:
				((CRemAXLB4MSB*)pBuffer)->MakeRemoteData( m_pVMEM );
				break;
			case CTC_LK_ARROW:
				{
					BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;
					ScrInfoBlock *pBlkInfo 	= (ScrInfoBlock *)int_get(SignalGet, nObjNo);

					if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
					{
						((CRemArrow*)pBuffer)->MakeRemoteDataTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_AUTO_IN)	// TCB
					{
						((CRemArrow*)pBuffer)->MakeRemoteDataTCB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_INTER_OUT)	// TOKEN TGB
					{
						((CRemArrow*)pBuffer)->MakeRemoteDataTokenTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_INTER_IN)	// TOKEN TCB
					{
						((CRemArrow*)pBuffer)->MakeRemoteDataTokenTCB( pBlkInfo );
					}					
				}
				break;
			case CTC_LK_CA:
				{
					BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;
					ScrInfoBlock *pBlkInfo 	= (ScrInfoBlock *)int_get(SignalGet, nObjNo);

					if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
					{
						((CRemCA*)pBuffer)->MakeRemoteDataTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_AUTO_IN)
					{
						((CRemCA*)pBuffer)->MakeRemoteDataTCB( pBlkInfo );
					}
				}
				break;
			case CTC_LK_LCR:
				{
					BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;
					ScrInfoBlock *pBlkInfo 	= (ScrInfoBlock *)int_get(SignalGet, nObjNo);

					if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
					{
						((CRemLCR*)pBuffer)->MakeRemoteDataTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_AUTO_IN)
					{
						((CRemLCR*)pBuffer)->MakeRemoteDataTCB( pBlkInfo );
					}
				}
				break;
			case CTC_LK_CBB:
				{
					BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;
					ScrInfoBlock *pBlkInfo 	= (ScrInfoBlock *)int_get(SignalGet, nObjNo);

					if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
					{
						((CRemCBB*)pBuffer)->MakeRemoteDataTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_AUTO_IN)
					{
						((CRemCBB*)pBuffer)->MakeRemoteDataTCB( pBlkInfo );
					}
				}
				break;
			case CTC_LK_BCR:
				{
					BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;
					ScrInfoBlock *pBlkInfo 	= (ScrInfoBlock *)int_get(SignalGet, nObjNo);

					if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
					{
						((CRemBCR*)pBuffer)->MakeRemoteDataTGB( pBlkInfo );
					}
					else if(nSignalType == BLOCKTYPE_AUTO_IN)
					{
						((CRemBCR*)pBuffer)->MakeRemoteDataTCB( pBlkInfo );
					}
				}
				break;
			case CTC_LK_SW:
				pObject = GP_InfoSwitch( nObjNo );
				((CRemSwitch*)pBuffer)->MakeRemoteData( pObject );
				break;
			case CTC_LK_MSW:
				pObject = GP_InfoSwitch( nObjNo );
				((CRemMSwitch*)pBuffer)->MakeRemoteData( pObject );
				break;
			case CTC_LK_SG:
				{
					BYTE nSignalType 	= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;

					pObject = GP_InfoSignal( nObjNo );

					if(nSignalType == SIGNALTYPE_ASTART)
					{
						((CRemSignal*)pBuffer)->MakeRemoteDataAStart( pObject );
					}
					else
					{
						((CRemSignal*)pBuffer)->MakeRemoteData( pObject );
					}
				}
				break;
			case CTC_LK_SG_EXT:
				pObject = GP_InfoSignal( nObjNo );
				((CRemSignalExt*)pBuffer)->MakeRemoteData( pObject );
				break;
			case CTC_LK_TK:
				pObject = GP_InfoTrack( nObjNo );
				((CRemTrack*)pBuffer)->MakeRemoteData( pObject );
				break;
			case CTC_LK_LC:
				switch(nObjNo)
				{
				case 1:
					((CRemLC*)pBuffer)->MakeRemoteDataLC1( m_pVMEM );
					break;
				case 2:
					((CRemLC*)pBuffer)->MakeRemoteDataLC2( m_pVMEM );
					break;
				case 3:
					((CRemLC*)pBuffer)->MakeRemoteDataLC3( m_pVMEM );
					break;
				case 4:
					((CRemLC*)pBuffer)->MakeRemoteDataLC4( m_pVMEM );
					break;
				default:
					break;
				}
				break;
			case CTC_LK_OSS:
				pObject = GP_InfoSignal( nObjNo );
				((CRemOSS*)pBuffer)->MakeRemoteData( pObject );
				break;
			default :
				*pBuffer = 0;
				break;
			}
		}

		pBuffer++;
		nSize--;
	}

	return 0;
}


int CEipEmu::ProcessCtcCommand( BYTE *pCtcMsg )
{
	BYTE pMsg[8] = {0,};
		
#ifdef _WINDOWS
	WORD nElement = *((WORD*)pCtcMsg);
#else
	WORD nElement = ((WORD)pCtcMsg[1] << 8) + pCtcMsg[0];
#endif
	BYTE nCommand = pCtcMsg[2];

#ifndef _WINDOWS
	LogInd(LOG_TYPE_CTC, "nElement = 0x%X, nCommand = 0x%X\n", nElement, nCommand);
#endif

	DataHeaderType* pHeader = (DataHeaderType*)m_pVMEM;

	if((pHeader->nSysVar.bMode != MODE_CTC) && (nCommand != CTC_CTRL_CTC_REQ))
	{
		return 1;
	}

	WORD *pTable  = m_pCtcElementTable;
	WORD nSize = *pTable++;
	

	CRemoteLinkTableType &Link = *(CRemoteLinkTableType*)(&pTable[ nElement ]);
	BYTE &nType = Link.m_nType;
	BYTE &nObjNo = Link.m_nObjNo;

	WORD nQueRoute;
	BYTE nSignal;
	int  bExitCmd;
	int  nRouteSize;

	nSignal  = 0;
	bExitCmd = 1;
	nQueRoute = m_pRouteQue[ nSignal ] & ROUTE_MASK;
	nRouteSize = ( Link.m_nType >> 4 ) & 7;

	switch(nCommand) 
	{
    case CTC_CTRL_DAY:
    case CTC_CTRL_NIGHT:

		pMsg[1] = WS_OPMSG_BUTTON;
		pMsg[2] = BtnDay;

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_SLS_ON :
	case CTC_CTRL_SLS_OFF :
		pMsg[1] = WS_OPMSG_BUTTON;
		pMsg[2] = BtnSls;
	
		Operate((char*)pMsg);
		break;

	case CTC_CTRL_BLK_CA :
		{
			BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;

			if(nSignalType == BLOCKTYPE_AUTO_OUT)	// TGB
			{
				pMsg[1] = WS_OPMSG_CAON;
			}
			else if(nSignalType == BLOCKTYPE_AUTO_IN)	// TCB
			{
				pMsg[1] = WS_OPMSG_CAACK;
			}
			else
			{
				break;
			}
			
			pMsg[2] = nObjNo;

			Operate((char*)pMsg);
		}
		break;

	case CTC_CTRL_BLK_LCR :
		{
			BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;

			if((nSignalType == BLOCKTYPE_AUTO_OUT) || (nSignalType == BLOCKTYPE_AUTO_IN))
			{
				pMsg[1] = WS_OPMSG_LCROP;
				pMsg[2] = nObjNo;
			}
			else
			{
				break;
			}
			
			Operate((char*)pMsg);
		}
		break;

	case CTC_CTRL_BLK_CBB :
		{
			BYTE nSignalType 		= m_pSignalInfoTable[nObjNo].nSignalType & SIGNAL_MASK;

			if((nSignalType == BLOCKTYPE_AUTO_OUT) || (nSignalType == BLOCKTYPE_AUTO_IN))
			{
				pMsg[1] = WS_OPMSG_CBBOP;
				pMsg[2] = nObjNo;
			}
			else
			{
				break;
			}
			
			Operate((char*)pMsg);
		}
		break;

	case CTC_CTRL_SW_PLS :
		pMsg[1] = WS_OPMSG_SWTOGGLE;
		pMsg[2] = nObjNo;

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_SW_MIN :
		pMsg[1] = WS_OPMSG_SWTOGGLE;
		pMsg[2] = 0x80 | nObjNo;

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_SW_BLK :
	case CTC_CTRL_SW_REL :
		pMsg[1] = WS_OPMSG_POINT_BLOCK;
		pMsg[2] = nObjNo;
			
		Operate((char*)pMsg);
		break;

	case CTC_CTRL_SG_STOP :
		pMsg[1] = WS_OPMSG_SIGNALSTOP;
		pMsg[2] = nObjNo;
			
		Operate((char*)pMsg);
		break;

	case CTC_CTRL_SG_RET :
		pMsg[1] = WS_OPMSG_ASPECT;
		pMsg[2] = nObjNo;
			
		Operate((char*)pMsg);
		break;

	case CTC_CTRL_MAIN_SET:
	case CTC_CTRL_SHNT_SET:
	case CTC_CTRL_CALL_SET:
		{
			WORD wRouteNo = (nType & 0x7F) * 256 + nObjNo;
			DWORD nInfoPtr = 0;		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pInfoMem = NULL;
			BYTE nSignalID = 0;
			BYTE nButtonID = 0;
			BYTE nRouteOrder = 0;
			BYTE nRouteCount = 0;
			BYTE nRouteTag = 0;
			BYTE nPrevSigID = 0;
			BYTE nRouteStatus = 0;

			nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			pInfoMem = (BYTE*)&m_pInfoMem[nInfoPtr];

			nSignalID = pInfoMem[RT_OFFSET_SIGNAL];
			nButtonID = pInfoMem[RT_OFFSET_BUTTON];
			nRouteOrder = pInfoMem[RT_OFFSET_ROUTEORDER];
			nRouteCount = m_pSignalInfoTable[nSignalID].nRouteCount;
			nRouteTag	= pInfoMem[RT_OFFSET_ROUTETAG];
			nPrevSigID = pInfoMem[RT_OFFSET_PREVSIGNAL];
 
			for(BYTE i = nRouteOrder ; i <= nRouteCount ; i++)
			{
				nRouteStatus = pInfoMem[RT_OFFSET_ROUTESTATUS];

				if(((nCommand == CTC_CTRL_MAIN_SET) && !(nRouteStatus & RT_SHUNTSIGNAL) && !(nRouteStatus & RT_CALLON))
				|| ((nCommand == CTC_CTRL_SHNT_SET) && (nRouteStatus & RT_SHUNTSIGNAL))
				|| ((nCommand == CTC_CTRL_CALL_SET) && (nRouteStatus & RT_CALLON)))
				{
#ifndef _WINDOWS
					LogInd(LOG_TYPE_CTC, "i = %d, nButtonID = %x, BUTTON = %x, nRouteTag = %d, ROUTETAG = %d", i, nButtonID, pInfoMem[RT_OFFSET_BUTTON], nRouteTag, pInfoMem[RT_OFFSET_ROUTETAG]);
#endif
					if((nButtonID == pInfoMem[RT_OFFSET_BUTTON]) && (nRouteTag == pInfoMem[RT_OFFSET_ROUTETAG]))
					{
						WORD wCheckResult = CheckRoute(wRouteNo);
						if(wCheckResult & ROUTECHECK_SIGNAL)
						{
#ifndef _WINDOWS
							LogErr(LOG_TYPE_CTC, "AmbwRouteNo = %d, nSignalID = %d, nDestID = %d", wRouteNo, nSignalID, i - 1);
#endif
							break;
						}

						pMsg[1] = WS_OPMSG_SIGNALDEST;
						pMsg[2] = nSignalID;
						pMsg[3] = i - 1;	// DESTID
	
#ifndef _WINDOWS
						LogInd(LOG_TYPE_CTC, "wRouteNo = %d, nSignalID = %d, nDestID = %d", wRouteNo, nSignalID, i - 1);
#endif
						Operate((char*)pMsg);
						break;
					}
				}

				nInfoPtr = *(DWORD*)&m_pRouteAddTable[ ++wRouteNo ];// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				pInfoMem = (BYTE*)&m_pInfoMem[nInfoPtr];
			}
		}
		break;

	case CTC_CTRL_MAIN_REL:
	case CTC_CTRL_SHNT_REL:
	case CTC_CTRL_CALL_REL:
		{
			WORD wRouteNo = (nType & 0x7F) * 256 + nObjNo;
			DWORD nInfoPtr = 0;		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			BYTE *pInfoMem = NULL;
			BYTE nSignalID = 0;
			BYTE nRouteOrder = 0;
			BYTE nRouteCount = 0;
			BYTE nRouteStatus = 0;

			nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
			pInfoMem = (BYTE*)&m_pInfoMem[nInfoPtr];

			nSignalID = pInfoMem[RT_OFFSET_SIGNAL];
			nRouteOrder = pInfoMem[RT_OFFSET_ROUTEORDER];
			nRouteCount = m_pSignalInfoTable[nSignalID].nRouteCount;

			for(BYTE i = nRouteOrder ; i <= nRouteCount ; i++)
			{
				nRouteStatus = pInfoMem[ RT_OFFSET_ROUTESTATUS ];

				if(((nCommand == CTC_CTRL_MAIN_REL) && !(nRouteStatus & RT_SHUNTSIGNAL) && !(nRouteStatus & RT_CALLON))
				|| ((nCommand == CTC_CTRL_SHNT_REL) && (nRouteStatus & RT_SHUNTSIGNAL))
				|| ((nCommand == CTC_CTRL_CALL_REL) && (nRouteStatus & RT_CALLON)))
				{
					if((wRouteNo == (m_pRouteQue[nSignalID] & ROUTE_MASK)) && !(m_pRouteQue[nSignalID] & ROUTE_DEPENDSET))
					{
						pMsg[1] = WS_OPMSG_SIGNALCANCEL;
						pMsg[2] = nSignalID;
						pMsg[3] = nRouteOrder;	// DESTID
				
	#ifndef _WINDOWS
						LogInd(LOG_TYPE_CTC, "wRouteNo = %d, nSignalID = %d, nDestID = %d", wRouteNo, nSignalID, nRouteOrder);
	#endif
						Operate((char*)pMsg);
						break;
					}
				}
				
				nInfoPtr = *(DWORD*)&m_pRouteAddTable[ ++wRouteNo ];// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				pInfoMem = (BYTE*)&m_pInfoMem[nInfoPtr];
			}
		}
		break;

	case CTC_CTRL_LOS_SET:
	case CTC_CTRL_LOS_REL:
		pMsg[1] = WS_OPMSG_LOS;
		pMsg[2] = nObjNo;

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_LC_CALL:
		pMsg[1] = WS_OPMSG_BUTTON;

		if(nObjNo == 1)
		{
			pMsg[2] = BtnLCA;
		}
		else if(nObjNo == 2)
		{
			pMsg[2] = BtnLC2A;
		}
		else
		{
			break;
		}

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_OSS_STOP:
	case CTC_CTRL_OSS_RET:
		pMsg[1] = WS_OPMSG_OSS;
		pMsg[2] = nObjNo;

		Operate((char*)pMsg);
		break;

	case CTC_CTRL_CTC_REQ:
		pHeader->nSysVar.bCTCRequest = 1;
#ifndef _WINDOWS
		LogInd(LOG_TYPE_CTC, "bCTCRequest = %d", pHeader->nSysVar.bCTCRequest);
#endif
		break;

	default:
		break;
	}

	pCtcMsg[0] = 0x00;	// source clearing - Element No. (LSB)
	pCtcMsg[1] = 0x00;	// source clearing - Element No. (MSB)  
	pCtcMsg[2] = 0x00;	// source clearing - Command No.

	return bExitCmd;
}

int CEipEmu::ProcessConfigList() 
{
	WORD *pTable = m_pConfigListTable;
	WORD nSize = *pTable++;

	while(nSize > 0) 
	{
		CRemoteLinkTableType &Link = *(CRemoteLinkTableType*)(pTable++);
		BYTE &nType = Link.m_nType;
		BYTE &nObjNo = Link.m_nObjNo;
		
#ifndef _WINDOWS
		LogInd(LOG_TYPE_GEN, "nType = 0x%X, nObjNo = 0x%X", nType, nObjNo);
#endif

		switch ( nType ) 
		{
		case CONFIG_AXLCNT_TRACK:
			m_ConfigList.AXLCNT_TRACK	= nObjNo;
			break;
		case CONFIG_B1B2_AXLSYNC:
			m_ConfigList.B1B2_AXLSYNC	= nObjNo;
			break;
		case CONFIG_B3B4_AXLSYNC:
			m_ConfigList.B3B4_AXLSYNC	= nObjNo;
			break;
		case CONFIG_B11B12_AXLSYNC:
			m_ConfigList.B11B12_AXLSYNC	= nObjNo;
			break;
		case CONFIG_B13B14_AXLSYNC:
			m_ConfigList.B13B14_AXLSYNC	= nObjNo;
			break;
		case CONFIG_CTC_ZONENO:
			m_ConfigList.CTC_ZONENO		= nObjNo;
			break;
		case CONFIG_CTC_STATIONNO:
			m_ConfigList.CTC_STATIONNO	= nObjNo;
			break;
		case CONFIG_SUPER_BLOCK:
			m_ConfigList.SUPER_BLOCK	= nObjNo;
			break;
		case CONFIG_SINGLE_TRACK:
			m_ConfigList.SINGLE_TRACK	= nObjNo;
			break;
		case CONFIG_B2_SELF_AXLRST:
			m_ConfigList.B2_SELF_AXLRST	= nObjNo;
			break;
		case CONFIG_B4_SELF_AXLRST:
			m_ConfigList.B4_SELF_AXLRST	= nObjNo;
			break;
		case CONFIG_B1B2_NOAXL:
			m_ConfigList.B1B2_NOAXL		= nObjNo;
			break;
		case CONFIG_B3B4_NOAXL:
			m_ConfigList.B3B4_NOAXL		= nObjNo;
			break;
		case CONFIG_B11B12_NOAXL:
			m_ConfigList.B11B12_NOAXL	= nObjNo;
			break;
		case CONFIG_B13B14_NOAXL:
			m_ConfigList.B13B14_NOAXL	= nObjNo;
			break;
		case CONFIG_B1B2_SWAPAXL:
			m_ConfigList.B1B2_SWAPAXL	= nObjNo;
			break;
		case CONFIG_B3B4_SWAPAXL:
			m_ConfigList.B3B4_SWAPAXL	= nObjNo;
			break;
		case CONFIG_GPS_ZONENO:
			m_ConfigList.GPS_ZONENO		= nObjNo;
			break;
		case CONFIG_GPS_STATIONNO:
			m_ConfigList.GPS_STATIONNO	= nObjNo;
			break;
		case CONFIG_GPS_SERVERNO:
			m_ConfigList.GPS_SERVERNO	= nObjNo;
			break;
		case CONFIG_UP_STATIONNO:
			m_ConfigList.UP_STATIONNO	= nObjNo;
			break;
		case CONFIG_DN_STATIONNO:
			m_ConfigList.DN_STATIONNO	= nObjNo;
			break;
		case CONFIG_MD_STATIONNO:
			m_ConfigList.MD_STATIONNO	= nObjNo;
			break;
		case CONFIG_REVERSE_LAYOUT:
			m_ConfigList.REVERSE_LAYOUT	= nObjNo;
			break;
		case CONFIG_B1B2_SELF_BLKRST:
			m_ConfigList.B1B2_SELF_BLKRST	= nObjNo;
			break;
		case CONFIG_B3B4_SELF_BLKRST:
			m_ConfigList.B3B4_SELF_BLKRST	= nObjNo;
			break;
		case CONFIG_B11B12_SELF_BLKRST:
			m_ConfigList.B11B12_SELF_BLKRST	= nObjNo;
			break;
		case CONFIG_B13B14_SELF_BLKRST:
			m_ConfigList.B13B14_SELF_BLKRST	= nObjNo;
			break;
		case CONFIG_USE_MMCR_NET:
			m_ConfigList.USE_MMCR_NET	= nObjNo;
			break;
		case CONFIG_USE_MCCR_NET:
			m_ConfigList.USE_MCCR_NET	= nObjNo;
			break;
		case CONFIG_USE_MODEM_BKUP:
			m_ConfigList.USE_MODEM_BKUP	= nObjNo;
			break;
		case CONFIG_BLK_TRAIN_SWEEP:
			m_ConfigList.BLK_TRAIN_SWEEP = nObjNo;
			break;
		case CONFIG_UP_NEWCBI_IFC:
			m_ConfigList.UP_NEWCBI_IFC	= nObjNo;
			break;
		case CONFIG_DN_NEWCBI_IFC:
			m_ConfigList.DN_NEWCBI_IFC	= nObjNo;
			break;
		case CONFIG_BLK_DEV_MODE:
			m_ConfigList.BLK_DEV_MODE	= nObjNo;
			break;
		default:
			break;
		}

		nSize--;
	}

	return 0;
}

int CEipEmu::GetBlockCommPorts(BYTE *LeftPorts, BYTE *RightPorts, BYTE *MidPorts) 
{
	WORD nSignalCount = m_TableBase.InfoSignal.nCount;
	BYTE nSignalType = 0;
	BYTE nBlockDir = 0;
	WORD wRouteNo = 0;
	DWORD nInfoPtr = 0;
	BYTE *pInfo = 0;
	BYTE nCount = 0;
 
	for(int i = 1 ; i <= nSignalCount ; i++)
	{
		nSignalType = m_pSignalInfoTable[i].nSignalType & SIGNAL_MASK;
		wRouteNo = m_pSignalInfoTable[i].wRouteNo & ROUTE_MASK;

		if(wRouteNo)
		{
			nInfoPtr = *(DWORD*)&m_pRouteAddTable[ wRouteNo ];
	  		pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

			if(nSignalType == BLOCKTYPE_AUTO_OUT || nSignalType == BLOCKTYPE_AUTO_IN)
			{
				nBlockDir = pInfo[RT_OFFSET_BLKDIR];

				printf("BLOCK DIR = %x\n", pInfo[RT_OFFSET_BLKDIR]);
				printf("MAIN = %x, SUB = %X\n", pInfo[RT_OFFSET_MAINBLKPORT], pInfo[RT_OFFSET_SUBBLKPORT]);
				if(nBlockDir & BLOCKTYPE_DIR_LEFT)
				{
					LeftPorts[0] = pInfo[RT_OFFSET_MAINBLKPORT];
					LeftPorts[1] = pInfo[RT_OFFSET_SUBBLKPORT];
				}
				else if(nBlockDir & BLOCKTYPE_DIR_RIGHT)
				{
					RightPorts[0] = pInfo[RT_OFFSET_MAINBLKPORT];
					RightPorts[1] = pInfo[RT_OFFSET_SUBBLKPORT];
				}
				else if(nBlockDir & BLOCKTYPE_DIR_MID)
				{
					MidPorts[0] = pInfo[RT_OFFSET_MAINBLKPORT];
					MidPorts[1] = pInfo[RT_OFFSET_SUBBLKPORT];
				}	
			}
		}
	}

	return 1;

}
