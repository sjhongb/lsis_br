// UnitMsg.h : struct of the Unit Message
//
/////////////////////////////////////////////////////////////////////////////

#ifndef   _UNITMSG_H
#define   _UNITMSG_H

#include  "JTime.h"

/////////////////////////////////////////////////////////////////////////////
typedef union {
	BYTE pData[8];
	struct {
		BYTE nSTX;
		BYTE nCmd;
		BYTE nIDl;
		union {
			BYTE nIDh;
			BYTE Trno[5]; 
		};
	};
} UnitCommandType;

typedef union {
	BYTE pData[8];
	struct {
		BYTE nCmd;
		union {
			struct {
				BYTE nMsg;
				union {
					BYTE nBits[3];
					BYTE nID;
				};
			};
			struct {
				BYTE nTrno[5];
				BYTE nTrID;
				BYTE nTrCmd;
			};
		};
	};
} UnitMessageType;

// LOG Record Last Data Struct
typedef struct tagUnitMsgType {
	UnitCommandType Cmd;			// MAX_OPERATE_CMD_CODESIZE size
	UnitMessageType Msg;			// MAX_OPERATE_CMD_UNITCODE size
	CJTime	PCTime;					// sizeof(CJTime)
} UnitMsgType;

/////////////////////////////////////////////////////////////////////////////

#endif // _UNITMSG_H


