//===============================================================
//====                     Bmpinfo.cpp                       ====
//===============================================================
// 파  일  명 : Bmpinfo.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 정의된 룰에 따라 화면에 그려질 대상의 상태, 위치,
//              색깔 정보를 관리한다.
//
//===============================================================
/*
Type HOME         0
Lamp CRYG         0
     SRYG         1
     CRY          2
     SRY          3
     SCRY         4
     RY           5
     RG           6
Type START        1
Lamp RYG          0
     RY           1
     SRYG         2
     SRY          3
     SRG          4
     SR           5
     YRYG         6
     RG           7
Type GSHUNT       2
Lamp S            10
     SF           11
     SR           12
Type ASTART       3
Type OHOME        4
Lamp YRYG         0
     YRY          1
     YR           2
     RY           3
     YRG          4
     RYG          5
Type TOKENLESSC   5
Type TOKENLESSG   6
Type OC           7
Type OG	          8
Type LOS	  9
Type FREE        10
Type STOP        11
Type ISTART      12
Type ISHUNT      13
Type SHUNT       14
Type CALLON      15
Type OCALLON     16
*/
//===============================================================


#include "stdafx.h"
#include "BmpInfo.h"
#include "../LSM/LSM.h"

#include "EipEmu.h"
#include "scrobj.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern UINT  CELLSIZE_X; 
extern UINT  CELLSIZE_Y;
extern int   OFFSET_X;
extern int   OFFSET_Y;
extern int   REVERSE;
int _UseSmallText = 0;
int   STATIONUPDN;
BOOL bAreThereOnlyUepkAndDepk = TRUE;

/////////////////////////////////////////////////// 

#define DES_GRID_SIZE 10
#define MAX_OVERLAP 50
#define INVALID_POS	(CPoint(10000,0))

/////////////////////////////////////////////////// 

int _nSFMViewXSize; 
int _nSFMViewYSize;
int _nSFMTimerX = 30;
int _nSFMTimerY = 15;
int _nDotPerCell = 10;


CObArray SwitchObject;
CObArray TrackObject;
CObArray SignalObject;

/////////////////////////////////////////////////// 

COLORREF _COLOR_SIGNAL_NAME = RGB( 0, 0, 0 );

COLORREF _COLOR_TRACK_NAME = RGB( 255, 255, 255 );
COLORREF _COLOR_TRACK_NAME_BACK = RGB( 62, 62, 62 );
COLORREF _COLOR_SWITCH_NAME = RGB( 255, 255, 255 );
COLORREF _COLOR_BUTTON_NAME = RGB( 255, 255, 255 );
COLORREF _COLOR_ETCBUTTON_NAME = RGB( 0, 0, 255 );

StateAdditionalBtn m_StateOfAdditionalBtn[20];
#ifdef _LSD
#else
	extern EScrInfo EtcScrInfo[256];
#endif

////////////////////////////////////////////////////
////////// CBmpTrackCell ///////////////////////////

CBmpTrackCell::CBmpTrackCell()
{
	mCellNo     = -1;
	mVectorType = 0;
	mNodeType   = 0;
}

CBmpTrackCell::CBmpTrackCell(int CellNo,CPoint p,UINT vectortype, UINT nodetype)
{
	mCellNo = CellNo;
	mLpPos = mPos = p;
	mNodeType = nodetype;	
	mVectorType=vectortype;	
}


void CBmpTrackCell::SetCell(CPoint p,UINT vectortype, UINT nodetype)
{	
	mPos = p;
	mNodeType = nodetype;	
	mVectorType = vectortype;	
}

BmpDraw CBmpTrackCell::m_TrackBmp;

void CBmpTrackCell::Draw( UINT color,UINT vectortype, UINT m_bLastTrack )
{
	int bmpno = 0;
	CPoint tPos;
	tPos = mPos;
	UINT VectorType = mVectorType;
	if(vectortype)	
	{
		VectorType = vectortype;  		
	}

	if (VectorType == 1 && m_bLastTrack == 1 && mNodeType == 2 ) 
	{
		if (mPos.x > (_nSFMViewXSize/2))
		{
			color = 0x300;
		}
	}

	if (VectorType == 17) 
	{
		if (m_TrackBmp.m_nRepeat == 0) 
		{
			m_TrackBmp.Base = color;
			m_TrackBmp.WP = tPos;
		}
		else 
		{
			if (m_TrackBmp.WP.x > mPos.x) m_TrackBmp.WP.x = mPos.x;
		}
		m_TrackBmp.m_nRepeat++;
		return;
	}
	m_TrackBmp.DrawLong();

	BmpDraw tbmp;
	tbmp.Base = color;	
	tbmp.WP.x = tPos.x;
	tbmp.WP.y = tPos.y;

	bmpno =tbmp.WP.y -1;
	switch ((UINT)VectorType)
	{
		case TYPE_RIGHT : // 01
			bmpno = 0;
			break;
		case TYPE_LEFT :  // 10
			bmpno = 16;		
			break;		
		case TYPE_LEFT | TYPE_RIGHT: // 11
			bmpno = 1;
			break;
		case TYPE_RIGHT_TOP :  //02
			bmpno = 52;
			break;
		case TYPE_RIGHT_TOP | TYPE_SPECIAL_CHECK:	// 크게 그림  1002
			bmpno = 70;			 
			break;
		case TYPE_LEFT_BOTTOM : //20
			bmpno = 4;			 
			break;		
		case TYPE_LEFT_BOTTOM | TYPE_SPECIAL_CHECK:		// 크게 그림  1020
			bmpno = 67;			 
			break;
		case TYPE_LEFT_BOTTOM | TYPE_RIGHT_TOP:			// 사선  22
			bmpno = 2;
			break;			
		case TYPE_LEFT_TOP : // 08
			bmpno = 20;
			break;		
		case TYPE_LEFT_TOP | TYPE_SPECIAL_CHECK:		// 크게 그림  1008
			bmpno = 68;			 
			break;
		case TYPE_RIGHT_BOTTOM : // 80
			bmpno = 36;
			break;		
		case TYPE_RIGHT_BOTTOM | TYPE_SPECIAL_CHECK:	// 크게 그림 1080
			bmpno = 69;			 
			break;
		case TYPE_RIGHT_BOTTOM | TYPE_LEFT_TOP :		//  사선 88
			bmpno = 18;
			break;	
		case TYPE_TOP :									// 수직 04
				bmpno = 60;
			break;
		case TYPE_BOTTOM :  // 40
				bmpno = 61;
			break;		
		case TYPE_BOTTOM | TYPE_TOP : // 44
				bmpno = 62;
			break;				
		// 굴절 
		case TYPE_LEFT_BOTTOM | TYPE_RIGHT : //21
			bmpno = 51;
			break;
		case TYPE_LEFT_TOP | TYPE_RIGHT :  //08
			bmpno = 35;
			break;		
		case TYPE_RIGHT_TOP | TYPE_LEFT : //12
			bmpno = 3;
			break;
		case TYPE_RIGHT_BOTTOM | TYPE_LEFT : //80
			bmpno = 19;
			break;
		// 수직 굴절
		case TYPE_BOTTOM | TYPE_RIGHT: 
			bmpno = 72;
			break;
		case TYPE_TOP | TYPE_RIGHT: 
			bmpno = 74;
			break;
		case TYPE_BOTTOM | TYPE_LEFT: 
			bmpno = 71;
			break;
		case TYPE_TOP | TYPE_LEFT: 
			bmpno = 73;
			break;
		case TYPE_BOTTOM | TYPE_RIGHT_TOP : 
			bmpno = 63;
			break;
		case TYPE_TOP | TYPE_RIGHT_BOTTOM : 
			bmpno = 65;
			break;		
		case TYPE_TOP | TYPE_LEFT_BOTTOM : 
			bmpno = 66;
			break;
		case TYPE_BOTTOM | TYPE_LEFT_TOP : 
			bmpno = 64;
			break;			
	}			
	if(bmpno == -1)return;
	tbmp.Draw(bmpno);
}

///////////////  CBmpSignal /////////////////////////////////////////////////////////
CBmpSignal::CBmpSignal()
{	
	SetObjectType();
    m_pButton = NULL;
}

CBmpSignal::CBmpSignal(int CellNo,CPoint p,CPoint namepos,char *name,CPoint AapNamePos
					   ,short Namecheck,int type,int mlamp, UINT HasLoop,int istop,int isright
					   ,UINT isudo,short HasRoute,int nBlockadeType )
{
	SetObjectType();		
	IsUdo = isudo;
	mPos = p;
	mNamePos = namepos;
	mAapguNamePos = AapNamePos;
	if (name) strcpy(mName,name);
	else mName[0] = 0;	
	mNamecheck = Namecheck;
	m_nType = type;
	mHasLoop = HasLoop;
	IsTop = istop;
	IsRight = isright;
	m_bTTB = FALSE;
	mLamp = mlamp;

	CString str = mName;
	int period = str.Find('.');
	mWidth = str.GetLength();

	mOperateLCR = 0;
    mOperateCBB = 0;

	mHasRoute = HasRoute;

	m_nPressTimer = 0;
    m_pButton = NULL;

	SetName ( name );

}

int  CBmpSignal::IsAble()
{
    int ret = 1;
	return ret;
}

void CBmpSignal::SetColor(UINT col, UINT r_col)
{
	mColor = col;
	mRightColor = r_col;
}

#define SCR_SG_TYPEIN       0
#define SCR_SG_TYPEOUT      1
#define SCR_SG_SHUNT        2
#define SCR_SG_SHUNTSIGNAL  3

void CBmpSignal::Draw( CDC *pDC )
{
    // 상태변수 초기화.... 
    int		bmpno;
	short	LockColor	= 0;
	short	nOnRoute	= 0;
	mColor				= 0;	
	mCAColor			= 0;
	mLCRColor			= 0;
	mCBBColor			= 0;
	mARROWColor			= 0;
	mColor				= 0;
	mRightColor			= 0;
	m_nTTBEnable		= 0;
	m_bOn				= FALSE;
	m_nSignalOn			= FALSE;
    m_nOnTimer			= FALSE;
	BYTE m_nCallOn		= FALSE;
	BYTE m_nShuntOn		= FALSE;

    // 진로 요구 명령에 의한 쇄정 표시 Timer를 감소... --> 6초 후에 표시가 없어 지도록.
	if (m_nPressTimer > 0) {
		m_nPressTimer--;
	}
 
	if (m_pData) 
	{
		ScrInfoSignal *pSigInfo = (ScrInfoSignal*)m_pData;
		ScrInfoBlock  *pBlkInfo = (ScrInfoBlock*) m_pData;

        m_nOnTimer = pSigInfo->ON_TIMER;

		if ( !pSigInfo->FREE || pSigInfo->ON_TIMER || pSigInfo->REQUEST )  
		{
			LockColor = MY_YELLOW; 
			m_bOn = TRUE;
		}
		else if (m_nPressTimer) 
		{
			LockColor = MY_YELLOW;
	 	}
		else 
		{
			LockColor = MY_BLUE;  // Unlocked 
		}
		
		if ( pSigInfo->ON_TIMER ) // White Flashing //문제
		{
			LockColor |= 0x1000; 
		}
		else if (LockColor && pSigInfo->TM1 && !(pSigInfo->SOUT1 || pSigInfo->SOUT2 || pSigInfo->SOUT3 || pSigInfo->U)/* && !pSigInfo->ENABLED*/)
		{
			LockColor = MY_RED; // Yellow
		}

		if (m_nType == 11)
		{
			if ( !pSigInfo->LM_ALR && pSigInfo->LM_LOR )
			{
				mColor = MY_RED;
			}
			else
			{
				mColor = MY_BLUE;
			}

			BmpDraw tbmp;
			tbmp.SetBase( mColor );
			CPoint Pos = mPos;
			tbmp.SetWP(mPos);
			if(IsRight)
			{
				tbmp.Draw(185);
			}
			else
			{
				tbmp.Draw(186);
			}
		}
		if (m_nType == 12 && mLamp !=8)
		{
			bmpno = 75;
			BmpDraw tbmp;
			short color = 0;
			CPoint Pos = mNamePos;
            int dx = int( _nDotPerCell/2 * 1.5);
			Pos.y += 40;
			
			DWORD Drawmode = MERGECOPY;
			tbmp.SetBase( color );
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno,Drawmode);
			Pos.x += dx;
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno+1,Drawmode);
			Pos.x += dx;
			tbmp.SetWP(Pos);
			
			tbmp.DrawButton(bmpno+5,Drawmode);
			pDC->SetBkMode(TRANSPARENT);
			
			pDC->SetTextColor( RGB(255,255,255) );
			
			Pos.x -= 8;
			CRect rect(Pos.x-8, Pos.y-8,Pos.x+8, Pos.y+8);
			MyDrawText( mName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, 0 );
		}

// TCB(5) or TGB(6)....
		if ((m_nType == 5) || (m_nType == 6)) 
		{
			if(m_nType == 5) // TCB
			{
				if ( pBlkInfo->CA && pBlkInfo->CAACK /*|| pBlkInfo->LCR*/)
				{
					mCAColor = MY_YELLOW;
				}
				else if ( pBlkInfo->CA )
					mCAColor = MY_YELLOW | 0x1000;

                if ( mOperateLCR && !mLCRColor)
				{
				    mLCRColor = MY_YELLOW;
				}
				if ( pBlkInfo->LCR)
				{
					mLCRColor = MY_GREEN; // Green
				}
				if (pBlkInfo->LCRIN && !pBlkInfo->LCROP)
				{
					mARROWColor = MY_YELLOW | 0x1000;
					mLCRColor = MY_GREEN | 0x1000;
				}
				if ( pBlkInfo->LCR /*&& pBlkInfo->CONFIRM*/)
				{
					mLCRColor = MY_GREEN; // Green
					mARROWColor = MY_YELLOW; //White
				}
				if ( pBlkInfo->DEPARTIN)
				{
					mARROWColor = MY_RED;
				}
				if (pBlkInfo->RBCPR )
				{
					mARROWColor = MY_RED;
				}
				if (pBlkInfo->ARRIVE)
				{
					mARROWColor = MY_RED | 0x1000;
				}
				else if ( (!pBlkInfo->SAXLACT && !pBlkInfo->AXLOCC && pBlkInfo->LCRIN) || (pBlkInfo->SAXLACT && !pBlkInfo->SAXLOCC && pBlkInfo->LCRIN) )
				{
					mARROWColor = MY_RED;
				}
				if ( mOperateCBB && !mCBBColor)
				{
					mCBBColor = MY_YELLOW;
				}
				if ( pBlkInfo->CBB )
				{
					mCBBColor = MY_GREEN; // Green
// 					mARROWColor = MY_RED;
				}
//				if ( pBlkInfo->CA && !pBlkInfo->ARRIVE && !(pBlkInfo->LCRIN && !pBlkInfo->LCR))
				if ( !pBlkInfo->ARRIVE && pBlkInfo->LCRIN && pBlkInfo->LCR)
				{
					mCBBColor = MY_RED;
				}
			}

			if(m_nType == 6) // TGB
			{
				if ( pBlkInfo->CA)
				{
					mCAColor = MY_YELLOW;
				}
				if (mOperateLCR && !mLCRColor)
				{   
				    mLCRColor = MY_YELLOW;
				}
				if ( pBlkInfo->LCR)
				{
					mLCRColor = MY_GREEN; // Green
					mARROWColor = MY_YELLOW;
					if (pBlkInfo->LCRIN)
					{
						mARROWColor |= 0x1000;
					}
				}
				if (pBlkInfo->ARRIVEIN)
				{
					mARROWColor = MY_RED;
				}
				if ( mOperateCBB && !mCBBColor )
				{
					mCBBColor = MY_YELLOW;
				}
				if ( pBlkInfo->CBB )
				{
					mCBBColor = MY_GREEN; //Green
// 					mARROWColor = MY_RED;
				}
				if ( pBlkInfo->ASTART )
				{
					mCBBColor = MY_RED;
					mARROWColor = MY_YELLOW;
				}
				if ( pBlkInfo->DEPART || (pBlkInfo->CA && pBlkInfo->ARRIVEIN))
				{
					mARROWColor = MY_RED;
					mCBBColor = MY_RED;
				}
				if ( pBlkInfo->RBGPR || (!pBlkInfo->SAXLACT && !pBlkInfo->AXLOCC && pBlkInfo->LCR) || (pBlkInfo->SAXLACT && !pBlkInfo->SAXLOCC && pBlkInfo->LCR) )
				{
					mARROWColor = MY_RED;
				}
// 				if ( pBlkInfo->DEPARTIN && !pBlkInfo->ARRIVETR)
// 				{
// 					mCBBColor = MY_RED;
// 				}			
			}
		}

		else if((m_nType == 7) || (m_nType == 8)) // TOKEN Block
		{
			
			mColor = 0;
			BmpDraw tbmp;
			CPoint Pos = mPos;
			if (m_nType == 7) // Token Come
			{
				if (pBlkInfo->LCRIN)
				{
					mColor = MY_YELLOW | 0x1000;
				}
				if ( pBlkInfo->LCR && pBlkInfo->CONFIRM)
				{
					mColor = MY_YELLOW; //White
				}
				if ( pBlkInfo->DEPARTIN)
				{
					mColor = MY_RED;
				}
				if (pBlkInfo->ARRIVE)
				{
					mColor = MY_RED | 0x1000;
				}
				if ( pBlkInfo->CBB )
				{
					mColor = MY_RED;
				}

				tbmp.SetBase( mColor );	
				tbmp.SetWP(Pos);
				if (IsRight)
				{
					tbmp.Draw(175);
				}
				else
				{
					tbmp.Draw(174);
				}
			}

			if (m_nType == 8) // Token Go
			{
				if ( pBlkInfo->LCR)
				{
					mColor = MY_YELLOW;
					if (pBlkInfo->LCRIN)
					{
						mColor |= 0x1000;
					}
				}
				if (pBlkInfo->ARRIVEIN)
				{
					mColor = MY_RED;
				}
				if ( pBlkInfo->CBB )
				{
					mColor = MY_RED;
				}
				if ( pBlkInfo->ASTART )
				{
					mColor = MY_YELLOW;
				}
				if ( pBlkInfo->DEPART)
				{
					mColor = MY_RED;
				}
				
				tbmp.SetBase( mColor );	
				tbmp.SetWP(Pos);
				if (IsRight)
				{
					tbmp.Draw(174);
				}
				else
				{
					tbmp.Draw(175);
				}
			}
			return;

		}

// HOME, OUTER, ADVANCED, MAIN, LOOP SIGNAL
		
		else 
		{
			if ( pSigInfo->LM_ALR) // Signal lighting dark operation detection
				mColor = MY_BLUE;         // Gray lamps
			else 
			{

//*** HOME SIGNAL ***//

				if (m_nType == 0) 
				{
					if (pSigInfo->LM_LOR)
					{
						if (pSigInfo->SIN2)  // HR(Y)
						{
							mColor = MY_YELLOW;
							if (mLamp == 6) mColor = MY_GREEN;
							if (pSigInfo->SIN1) // DR(G)
							{
								mColor = MY_GREEN;
								if (mLamp == 4) mColor = MY_YELLOW;
							}
						}
						else 
						{
							mColor = MY_RED;
						}
					}
					else
					{
						mColor = MY_BLUE;
					}
					if (pSigInfo->SOUT3 || pSigInfo->U)  // C or OY
					{
						m_nCallOn = 1;
					}
				}

//*** OUTER SIGNAL ***//

				else if (m_nType == 4) 
				{
					if ( mLamp == 5 ) {
						if (pSigInfo->LM_LOR)
						{
							if (pSigInfo->SIN2) {   mColor = MY_YELLOW;  // HR(Y)
								if (pSigInfo->SIN1) mColor = MY_GREEN;   // DR(G)
							} else {
								mColor = MY_RED;
							}
						}
					} else if ( mLamp == 1 ){
						if (pSigInfo->LM_LOR)
						{
							if (pSigInfo->SIN2)  // HR(Y)
							{
								mColor = 0x190;
								
								/*
								if (pSigInfo->LM_CLOR) 
								{
									mColor = MY_YELLOW;	
								}
								*/
							}
							else 
							{
									mColor = MY_RED;
							}
						}

					} else {
						if (pSigInfo->LM_LOR)
						{
							if (pSigInfo->SIN2)  // HR(Y)
							{
								mColor = 0x190;
								if (pSigInfo->SIN1) // DR(G)
								{
									mColor = MY_GREEN;
								}
								if (mLamp == 3) mColor = MY_YELLOW;
							}
							else 
							{
									mColor = MY_RED;
							}
						}
						else
						{
							mColor = MY_BLUE;
						}
						if (pSigInfo->SOUT3 || pSigInfo->U)  // C or OY
						{
							m_nCallOn = 1;
						}
					}
				}


//*** MAIN STARTER SIGNAL & LOOP STARTER SIGNAL ***//

				else if (m_nType == 1 ) 
				{
					if (pSigInfo->LM_LOR) {
						if (mLamp == 0 || mLamp == 2 )  {                // Main Starter
							if (pSigInfo->SIN2) {   mColor = MY_YELLOW;  // HR(Y)
								if (pSigInfo->SIN1) mColor = MY_GREEN;   // DR(G)
							} else {
								mColor = MY_RED;
							}
						}
						else if (mLamp == 1 || mLamp == 3 ) 
						{   // Loop Starter
							if (pSigInfo->SIN2) // HR 
							{
								mColor = MY_YELLOW;
							}
							else 
							{
								mColor = MY_RED;
							}
						}
						else if (mLamp == 4) // S,R,G lamp
						{
							if ( pSigInfo->SIN1) // DR
							{
								mColor = MY_GREEN;
							}
							else 
							{
								mColor = MY_RED;
							}
						} 
						else if ( mLamp == 5) // S+R lamp
						{
							mColor = MY_RED;
						}
						else if ( mLamp == 6)
						{
							if (pSigInfo->SIN2)  // HR(Y)
							{
								mColor = 0x190;

								//mColor = MY_YELLOW;
								if (pSigInfo->SIN1) // DR(G)
								{
									mColor = MY_GREEN;
								}
							}
							else 
							{
								mColor = MY_RED;
							}						
						}
						else if ( mLamp == 7) // R,G lamp
						{
							if ( pSigInfo->SIN1) // DRa
							{
								mColor = MY_GREEN;
							}
							else 
							{
								mColor = MY_RED;
							}
						}
					}
					else
					{
						mColor = MY_BLUE;
					}
				}

//*** ADVANCED STARTER SIGNAL ***//

				else if (m_nType == 3 || m_nType == 12) 
				{
					if (pSigInfo->LM_LOR)
					{
						if ( pSigInfo->SIN1) // DR
						{
							mColor = MY_GREEN;
						}
						else if ( pSigInfo->SIN2)	// YELLOW 추가
						{
							mColor = MY_YELLOW;
						}
						else 
						{
							mColor = MY_RED;
						}
					}
					else
					{
						mColor = MY_BLUE;
					}
				}


//*** GROUND SHUNT SIGNAL ***//
				else if (m_nType == 2) 
				{
					if (pSigInfo->LM_LOR)
					{
						if ( pSigInfo->INU) // DR
						{
							mColor = MY_YELLOW;
						}
						else 
						{
							mColor = MY_RED;
						}
					}
					else
					{
						mColor = MY_BLUE;
					}
				}
			}

/*			if (pSigInfo->LM_YL) 
			{
				mColor = MY_YELLOW | 0x1000;
				if (m_nType == 4)
				{
					mColor = 0x1190;
				}
			}
			if (pSigInfo->LM_GL) {
				mColor = MY_GREEN | 0x1000;
			}
			if (pSigInfo->LM_RL) {
				mColor = MY_RED | 0x1000;
			}
*/                                              //2013.01.24 LED등 사용으로 Filament 사용이 없으므로 삭제.
			if (!pSigInfo->LM_LOR)  {
				mColor = MY_BLUE;
			}

			m_nSignalOn = ( ((mColor & 0xfff) != MY_RED) || m_nCallOn || m_nShuntOn);
		}
	}
// 위에까지는 표시해야 할 Lamp의 color를 상태에 따라 정의하였다....
// 신호기 Bitmap 이미지 그리기

	CPoint Pos = mPos;
	BmpDraw tbmp;	
    ScrInfoSignal *pSigInfo = (ScrInfoSignal*)m_pData;

	short nDirection = -1; // left dir
	if (IsRight == 1)	
	{ 								// 신호기 모양  좌: 0, 우 :1	
		nDirection = 1;    // right dir
	}

	CPoint p = Pos;

	if (m_nType < 5 || (m_nType==12 && mLamp==8) ) // Locking indicator  change,  m_nType 10 -> m_nType 2, m_nLamp 1로 
	{
		p.x -= nDirection * (CELLSIZE_Y);					// 신호 막대를 기준으로 하기위해 반대 방향으로
		p.y -= nDirection * 7;
		tbmp.SetWP( p );	
		tbmp.Base = LockColor;		
		tbmp.Draw(8);	
		p.y += nDirection * 7;
	}
	else if (m_nType == 12)
	{
		p.x -= nDirection * (CELLSIZE_Y);
	}

	tbmp.Base = mColor; 
	tbmp.SetWP( p );	
    int i = 0;
	switch (m_nType) 
	{											//  시그널 종류 

//***************************************// Home //***************************************//
	case 0 : 
	  if (mLamp == 2 || mLamp == 3) // 3현시
	  {
		  tbmp.Draw(97 - IsRight);
		  tbmp.Base = 0;
		  tbmp.Draw(167-IsRight);
	  }
	  else if (mLamp == 4)
	  {
		  if ( IsRight ) {
		  tbmp.Draw(97 - IsRight);
		  p.x += 12;
		  tbmp.SetWP( p );
		  p.x -= 12;
		  tbmp.Draw(97 - IsRight);
		  tbmp.SetWP( p );
		  } else {
		  tbmp.Draw(97 - IsRight);
		  p.x -= 12;
		  tbmp.SetWP( p );
		  p.x += 12;
		  tbmp.Draw(97 - IsRight);
		  tbmp.SetWP( p );		  
		  }
	  } else if (mLamp == 5) {
		  p.x += 12;
		  tbmp.Draw(114 - IsRight);
		  p.x -= 12;
		  tbmp.SetWP(p);
	  } else if (mLamp == 6) {
		  tbmp.Draw(114 - IsRight);
	  } else if (mLamp == 7) {
		  tbmp.Draw(112 - IsRight);
	  } else if (mLamp == 8) {
		  if ( IsRight ) {
			  tbmp.Draw(97 - IsRight);
			  p.x += 12;
			  tbmp.SetWP( p );
			  p.x -= 12;
 			  tbmp.Draw(97 - IsRight);
			  tbmp.SetWP( p );
		  } else {
			  tbmp.Draw(97 - IsRight);
			  p.x -= 12;
			  tbmp.SetWP( p );
			  p.x += 12;
			  tbmp.Draw(97 - IsRight);
			  tbmp.SetWP( p );
		  }
	  } else {
	     tbmp.Draw(97 - IsRight);
	  }

	  mRightColor = 0;

// Call on Signal
	  if (mLamp == 0 || mLamp == 2 || mLamp == 4 || mLamp == 8) 
	  {
		  if ((pSigInfo != NULL ) && /*(pSigInfo->SE3) &&*/ ( !pSigInfo->LM_ALR))       
		  {
			  if(pSigInfo->SOUT3) m_nSignalOn = TRUE; 
			  mRightColor = 0x190;
			  
			  if(!pSigInfo->SIN3)  // Call wait
			  {
				  mRightColor = 0;
				  if(!pSigInfo->TM1) mRightColor = 0;
			  }
			  else
			  {
				  if (!pSigInfo->LM_CLOR)
				  {
                      mRightColor = MY_GRAY; //gray
				  }
			  }
		  }

		  if (mLamp == 4 || mLamp == 8)
		  {
			  if ( IsRight ) {
				p.x += 12;
				tbmp.SetWP( p );
				p.x -= 12;
			  } else {
				p.x -= 12;
				tbmp.SetWP( p );
				p.x += 12;
			  }
		  }

          tbmp.Base = mRightColor;
		  tbmp.Draw(106 - IsRight); // call on
	  }

// shunt signal
	  if (mLamp == 1 || mLamp == 3 || mLamp == 4 || mLamp == 8)
	  {
		  tbmp.SetWP( p );
		  mRightColor = MY_BLUE;
		  if ((pSigInfo != NULL ) && ( !pSigInfo->LM_ALR))       
		  {
			  if(pSigInfo->U && pSigInfo->SE4 ) m_nSignalOn = TRUE; 

			  if(pSigInfo->INU)  // Shunt wait
			  {
				  mRightColor = MY_YELLOW;
			  }
		  }

		  tbmp.Base = mRightColor;
		  tbmp.Draw(171 - IsRight); 

		  p.x += 21 + (-42*IsRight);
		  p.y += 12 + (-12*IsRight);
		  tbmp.SetWP(p);
		  tbmp.Base = 0;
		  if ( mLamp != 8)
		  {
 			  tbmp.Draw(169-IsRight);
		  }
		  p.x -= 21 + (-42*IsRight);
		  p.y -= 12 + (-12*IsRight);
		  tbmp.SetWP(p);
		  
	  } 
	  
	  mColor = 0;
	  tbmp.Base = mColor; 


// Loop Indicator draw//
	   if (mHasLoop == 3 || mHasLoop == 1) // Both and Left
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->LEFT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
 		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(108 - IsRight);
	   }

	   if (mHasLoop == 3 || mHasLoop == 2) // Both and Right
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(110 - IsRight);
	   }
	   if (mLamp == 2 || mLamp == 3 ) // 바 그리기
	   {
		   tbmp.Base = 0;
		   tbmp.Draw(169-IsRight);
		   p.y += 12;
		   tbmp.SetWP( p );
		   tbmp.Draw(169-IsRight);
		   p.y -= 12;
		   p.x += 12 + (-24 *IsRight);  // 3현시에서 문자선별등 위치 조절
		   tbmp.SetWP(p);
	   }
	   else if ( mLamp == 4)
	   {
		   if ( IsRight ) {
			   p.x += 12;
			   tbmp.SetWP( p );
			   tbmp.Base = 0;
			   tbmp.Draw(169-IsRight);
			   p.y += 12;
			   tbmp.SetWP( p );
			   tbmp.Draw(169-IsRight);
			   p.y -= 12;
			   p.x -= 12;
			   tbmp.SetWP(p);
		   } else{
			   p.x -= 12;
			   tbmp.SetWP( p );
			   tbmp.Base = 0;
			   tbmp.Draw(169-IsRight);
			   p.y -= 12;
			   tbmp.SetWP( p );
			   tbmp.Draw(169-IsRight);
			   p.y += 12;
			   p.x += 12;
			   tbmp.SetWP(p);
		   }  
	   }
	   if (mHasLoop == 4)                  // Guage indicator
	   {
		   mRightColor = 0;
			 if ( pSigInfo != NULL ) {
		   	if (/*(pSigInfo->LEFT || pSigInfo->RIGHT) &&*/ ( !pSigInfo->LM_ALR))
		   	{
			  	 if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
			   		{
				  		 mRightColor = MY_RED;
			   		}
		   	}
		   }

		   tbmp.Base = mRightColor;	

		   if ( mLamp == 8 )
		   {
			   if ( IsRight)
			   {
				   p.x += 12;
				   tbmp.SetWP( p );
				   tbmp.Draw(156 - IsRight);
				   p.x -= 12;
				   tbmp.SetWP( p );
			   }
			   else
			   {
				   p.x -= 12;
				   tbmp.SetWP( p );
				   tbmp.Draw(156 - IsRight);
				   p.x += 12;
				   tbmp.SetWP( p );
			   }
		   }
		   else
		   {
			   tbmp.Draw(156 - IsRight);
		   }
	   }

      break;

//***************************************// MAIN or LOOP //***************************************//
	case 1 :
	case 12 :
		if ( (mLamp > 1 && mLamp < 6 ) /*|| (m_nType == 12)*/) // shunt signal
		{
			if (mLamp == 3 || mLamp == 4 || mLamp == 5 /*|| m_nType == 12*/) // 2현시	ISTART에서 SHUNT표시 없는 신호기 존재하기에 삭제.
			{
				tbmp.Draw(114 - IsRight);
				p.x -= 12 + (-24 * IsRight);
				tbmp.SetWP(p);
				tbmp.Draw(114 - IsRight);



				if (mLamp == 5)  
				{
					p.x += 36 + (-72 * IsRight);
					p.x += 12;
					tbmp.SetWP( p );
					tbmp.Base = 0;
					tbmp.Draw(169-IsRight);
					p.y += 12;
					tbmp.SetWP( p );
					tbmp.Draw(169-IsRight);
					p.y -= 12;
					p.x -= 12;
					tbmp.SetWP(p);
					tbmp.Base = MY_YELLOW;
					tbmp.Draw(156 - IsRight);
					p.x -= 36 + (-72 * IsRight);
				}

			} 
			else   // 3현시
			{
				tbmp.Draw(112 - IsRight);
				p.x -= 12 + (-24 * IsRight);
				tbmp.SetWP(p);
				tbmp.Draw(112 - IsRight);
			}

			p.x += 12 + (-24 * IsRight);
			tbmp.SetWP(p);

			mRightColor = 0;

			if ((pSigInfo != NULL ) && (pSigInfo->SE4) && ( !pSigInfo->LM_ALR)) 
			{
				if(pSigInfo->U) m_nSignalOn = TRUE; 
				mRightColor = MY_YELLOW;

			    if(!pSigInfo->INU)  // Shunt wait
				{
					mRightColor = 0;
					if(!pSigInfo->TM1) mRightColor = 0;
				}
			}
			tbmp.Base = mRightColor;
			tbmp.Draw(171 - IsRight); 

			p.x += 21 + (-42*IsRight);
			p.y += 12 + (-12*IsRight);
			tbmp.SetWP(p);
			tbmp.Base = 0;
			tbmp.Draw(169-IsRight);
			p.x -= 21 + (-42*IsRight);
			p.y -= 12 + (-12*IsRight);
			tbmp.SetWP(p);
		}
		else
		{
			if ( mLamp == 6 ) { // 4현시 
				tbmp.Draw(97 - IsRight);
				mRightColor = 0;
				if ((pSigInfo != NULL ) && (pSigInfo->SOUT3) && ( !pSigInfo->LM_ALR))      // OUTER LOOP
				{
					bmpno = 135;
					mRightColor = MY_YELLOW;
					if (pSigInfo->LM_CLOR)
					{
//						if(!pSigInfo->LM_CEKR) // OUTER YELLOW Fail
//						{
//							mRightColor |= 0x1000;
//						}									//2013.01.24  LED등으로 바뀌면서 filament fail없어짐.
					}
					else
					{
						mRightColor = MY_BLUE; 
					}
				}
				else if ((pSigInfo != NULL ) && (pSigInfo->SE2) && (!pSigInfo->SIN2) && ( !pSigInfo->LM_ALR))  // Outer Callon
				{
					bmpno = 181;
					mRightColor = 0;
					if(!pSigInfo->TM1) mRightColor = 0;
				}
				tbmp.Base = mRightColor;
				tbmp.Draw(bmpno - IsRight); // OUTER YELLOW
				// Loop Indicator draw//
				if (mHasLoop == 3 || mHasLoop == 1) // Both and Left
				{
					mRightColor = 0;
					if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->LEFT) && ( !pSigInfo->LM_ALR))
					{
						mRightColor = 0x190;
 					}
					tbmp.Base = mRightColor;	
					tbmp.Draw(108 - IsRight);
				}

				if (mHasLoop == 3 || mHasLoop == 2) // Both and Right
				{
					mRightColor = 0;
					if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
					{
						mRightColor = 0x190;
					}
					tbmp.Base = mRightColor;	
					tbmp.Draw(110 - IsRight);
				}
/*
			   tbmp.Base = 0;
			   tbmp.Draw(169-IsRight);
				p.y += 12;
				tbmp.SetWP( p );
				tbmp.Draw(169-IsRight);
				p.y -= 12;
				p.x += 12 + (-24 *IsRight);  // 3현시에서 문자선별등 위치 조절
				tbmp.SetWP(p);
*/
/*				if (mHasLoop == 4)                  // Guage indicator
				{
					mRightColor = 0;
					if ( pSigInfo != NULL ) {
		   				if ((pSigInfo->LEFT || pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
				   		{
					  		if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
			   				{
				  			 mRightColor = MY_RED;
			   				}
		   				}
					}

				tbmp.Base = mRightColor;	

				tbmp.Draw(156 - IsRight);
				}
*/
			}
			else 
			{
				if (mLamp)	
				{
					tbmp.Draw(114 - IsRight); // RY
					p.x -= 25 + (-1*IsRight);
				}
				else 
				{
					tbmp.Draw(112 - IsRight); // TYG
					p.x -= 13 + (-1*IsRight);
				}
				p.y -= 12 + (-12*IsRight);
				tbmp.SetWP(p);
			}
		}

/*  	    if (mHasLoop == 4)                  // Guage indicator
		{
		   mRightColor = 0;
		   if ((pSigInfo != NULL ) && pSigInfo->LEFT && ( !pSigInfo->LM_ALR))
		   {
			   if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
			   {
				   mRightColor = MY_RED;
			   }
		   }
		   else if ( (pSigInfo != NULL ) && pSigInfo->RIGHT && ( !pSigInfo->LM_ALR))
		   {
			   if (pSigInfo->INRIGHT || pSigInfo->INLEFT)
			   {
				   mRightColor = MY_RED;
			   }
		   }
		   tbmp.Base = mRightColor;	

		   tbmp.Draw(156 - IsRight);
		}
*/
		if (mHasLoop == 5) { // Free Lamp
			mRightColor = 0;
			if ((pSigInfo != NULL ) /*&& (pSigInfo->LEFT || pSigInfo->RIGHT)*/ && ( !pSigInfo->LM_ALR))
			{
				if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
				{
					mRightColor = MY_RED;
				}
			}
			tbmp.Base = mRightColor;	

			if ( IsRight > 0 ) p.x += 93;
			else p.x += -24;
			tbmp.SetWP(p);
			tbmp.Draw(183);
		}
//////////////////////////////////
// Loop Indicator draw//
	   if (mHasLoop == 3 || mHasLoop == 1) // Both and Left
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->LEFT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
 		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(108 - IsRight);
	   }

	   if (mHasLoop == 3 || mHasLoop == 2) // Both and Right
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(110 - IsRight);
	   }
	   if (mLamp == 2 || mLamp == 3 ) // 바 그리기
	   {
		   tbmp.Base = 0;
//		   tbmp.Draw(169-IsRight);
		   p.y += 12;
		   tbmp.SetWP( p );
//		   tbmp.Draw(169-IsRight);
		   p.y -= 12;
		   if ( mLamp == 3 )
		   {
			   p.x += 12 + (-24 *IsRight);  // 3현시에서 문자선별등 위치 조절
		   }
		   tbmp.SetWP(p);
	   }
	   else if ( mLamp == 4)
	   {
		   if ( IsRight ) {
			p.x += 12;
			tbmp.SetWP( p );
			tbmp.Base = 0;
			tbmp.Draw(169-IsRight);
			p.y += 12;
			tbmp.SetWP( p );
			tbmp.Draw(169-IsRight);
			p.y -= 12;
			p.x -= 12;
			tbmp.SetWP(p);
		   } else{
			p.x -= 12;
			tbmp.SetWP( p );
			tbmp.Base = 0;
			tbmp.Draw(169-IsRight);
			p.y -= 12;
			tbmp.SetWP( p );
			tbmp.Draw(169-IsRight);
			p.y += 12;
			p.x += 12;
			tbmp.SetWP(p);
		   }

	   }
	   if (mHasLoop == 4)                  // Guage indicator
	   {
			mRightColor = 0;
			if ( pSigInfo != NULL ) 
			{
				if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
			   	{
					 mRightColor = MY_RED;
			   	}
		   	}

			tbmp.Base = mRightColor;	

			tbmp.Draw(156 - IsRight);
	   }

      break;



//***************************************// SHUNT //***************************************//
    case 2 :
	case 10 :

		if ( mLamp==12) // R+S 신호기 R 필라멘트 페일시 깜빡이지 않는 문제 때문에 12번 을 구분함. 2007.2.26
		{
			tbmp.Draw(114 - IsRight);
			p.x -= 12 + (-24 * IsRight);
			tbmp.SetWP(p);
			tbmp.Draw(114 - IsRight);

			if ( IsRight == 1 ) 
			{
				p.x += 36 + (-72);
				p.x += 12;

				tbmp.SetWP( p );
				tbmp.Base = 0;
				tbmp.Draw(168);
				p.y += 12;
				tbmp.SetWP( p );
				tbmp.Draw(168);
			} else {
				p.x += 36;
				p.x += 12;
				p.x -= 24;

				tbmp.SetWP( p );
				tbmp.Base = 0;
				tbmp.Draw(169);
				p.y += 12;
				tbmp.SetWP( p );
				tbmp.Draw(169);
				p.x += 24;

			}

			p.y -= 12;
			p.x -= 12;
			tbmp.SetWP(p);
			tbmp.Base = MY_YELLOW;
			tbmp.Draw(156 - IsRight);
			p.x -= 36 + (-72 * IsRight);
			
			p.x += 12 + (-24 * IsRight);
			tbmp.SetWP(p);

			mRightColor = MY_RED;

			if ((pSigInfo != NULL ) && ( !pSigInfo->LM_ALR)) 
			{
				if (pSigInfo->SE4 && pSigInfo->U )
				{
					m_nSignalOn = TRUE; 
				}

			    if(pSigInfo->INU)  // Shunt wait
				{
					mRightColor = MY_YELLOW;
				}
			}
			
			tbmp.Base = mRightColor;
			tbmp.Draw(171 - IsRight); 

			p.x += 21 + (-42*IsRight);
			p.y += 12 + (-12*IsRight);
			tbmp.SetWP(p);
			tbmp.Base = 0;
			tbmp.Draw(169-IsRight);
			p.x -= 21 + (-42*IsRight);
			p.y -= 12 + (-12*IsRight);
			tbmp.SetWP(p);

			if (mHasLoop == 4)                  // Guage indicator
			{
				mRightColor = 0;
				if ( pSigInfo != NULL ) {
					if (/*(pSigInfo->LEFT || pSigInfo->RIGHT) &&*/ ( !pSigInfo->LM_ALR))
					{
						if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
						{
							mRightColor = MY_RED;
						}
					}
				}

				if ( IsRight > 0 ) 
					p.x += -24;
				else
					p.x += 24;
				
				tbmp.SetWP(p);
				tbmp.Base = mRightColor;	
				tbmp.Draw(156 - IsRight);
			}

			if (mHasLoop == 5) { // Free Lamp
				mRightColor = 0;
				if ((pSigInfo != NULL ) /*&& (pSigInfo->LEFT || pSigInfo->RIGHT)*/ && ( !pSigInfo->LM_ALR))
				{
					if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
					{
						mRightColor = MY_RED;
					}
				}
				tbmp.Base = mRightColor;	

				if ( IsRight > 0 ) 
					p.x += 81;
				else
					p.x += -12;
				tbmp.SetWP(p);
				tbmp.Draw(183);
			}

		} 
		else 
		{
			tbmp.Base = 0;
			p.x -= 12 + (-24 * IsRight);
			tbmp.SetWP(p);
			tbmp.Draw(179 - IsRight );

			p.x += 12 + (-24 * IsRight);
			tbmp.SetWP(p);

			mRightColor = MY_RED;

			if ((pSigInfo != NULL ) && ( !pSigInfo->LM_ALR)) 
			{
				if (pSigInfo->SE4 && pSigInfo->U )
				{
					m_nSignalOn = TRUE; 
				}

				if(pSigInfo->INU)  // Shunt wait
				{
					mRightColor = MY_YELLOW;
				}
			}
			tbmp.Base = mRightColor;
			tbmp.Draw(171 - IsRight);

			if ( mLamp==11)
			{
			   mRightColor = 0;
			   if ((pSigInfo != NULL ) && (pSigInfo->LEFT || pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
			   {
				   if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
				   {
					   mRightColor = MY_RED;
				   }
			   }
			   tbmp.Base = mRightColor;	

			   if (IsRight > 0)
				   p.x += 68;
			   else
				   p.x += 1;
			   tbmp.SetWP(p);
			   tbmp.Draw(183);
			}

/*
		if ( mLamp==12)
		{
			if ( (pSigInfo != NULL ) && ( pSigInfo->LM_LOR) ) mRightColor = MY_RED;
			else mRightColor = 0;
			if ( IsRight > 0 ) p.x += 29;
			else p.x += -41;
			tbmp.Base = mRightColor;
			tbmp.SetWP(p);
			tbmp.Draw(187);
			if (mHasLoop == 5) { // Free Lamp
				mRightColor = 0;
				if ((pSigInfo != NULL ) && (pSigInfo->LEFT || pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
				{
					if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
					{
						mRightColor = MY_RED;
					}
				}
				tbmp.Base = mRightColor;	
				if ( IsRight > 0 ) p.x += 53;
				else p.x += 27;
				tbmp.SetWP(p);
				tbmp.Draw(183);
			}
		}
*/
		}

		break;

//***************************************// ADVANCED //***************************************//
    case 3 :
		tbmp.Draw(114 - IsRight );
	    tbmp.Base = 0;
		p.x -= 12 + (-24 * IsRight);
		tbmp.SetWP(p);
		tbmp.Draw(173 - IsRight );
		break;

//***************************************// OUTER //***************************************//
	case 4 :

        if (mLamp < 2 || mLamp==4)
		{
			if (pSigInfo != NULL && mLamp==4 && pSigInfo->SIN1 && pSigInfo->SIN2)
			{
			    tbmp.Draw(112 - IsRight);
			}
			else
 			    tbmp.Draw(97 - IsRight);
 			    
			if (mLamp == 1 || mLamp==4)
			{
				tbmp.Base = 0;
				tbmp.Draw(167-IsRight);
			}
			mRightColor = 0;
			if ((pSigInfo != NULL ) && (pSigInfo->SIN3) && ( !pSigInfo->LM_ALR))      // OUTER LOOP
			{
				bmpno = 135;
				mRightColor = MY_YELLOW;
				if (pSigInfo->LM_CLOR)
				{
//					if(!pSigInfo->LM_CEKR) // OUTER YELLOW Fail
//					{
//						mRightColor |= 0x1000;
//					}									//2013.01.24  LED등으로 바뀌면서 filament fail없어짐.
				}
				else
				{
					mRightColor = MY_BLUE; 
				}
				
			    tbmp.Base = mRightColor;
			    tbmp.Draw(bmpno - IsRight); // OUTER YELLOW
			}
			else if ((pSigInfo != NULL ) && (pSigInfo->SE2) && (!pSigInfo->SIN2) && ( !pSigInfo->LM_ALR))  // Outer Callon
			{
				bmpno = 181;
				mRightColor = 0;
				if(!pSigInfo->TM1) mRightColor = 0;
				
			    tbmp.Base = mRightColor;
			    tbmp.Draw(bmpno - IsRight); // OUTER YELLOW
			}
			//tbmp.Base = mRightColor;
			//tbmp.Draw(bmpno - IsRight); // OUTER YELLOW
			
			if (mLamp == 1 || mLamp==4)
			{
				tbmp.Base = 0;
				tbmp.Draw(169-IsRight);
				p.y += 12;
				tbmp.SetWP( p );
				tbmp.Draw(169-IsRight);
			}
		}
		else if ( mLamp == 5 ) {
//				p.x -= 12 + (-24 * IsRight);
				tbmp.SetWP(p);
				tbmp.Draw(112 - IsRight);
		}
		else // RY
		{
			if ((pSigInfo != NULL ) && (pSigInfo->SOUT3) && ( !pSigInfo->LM_ALR))      // OUTER LOOP
			{
				mColor = MY_YELLOW;
				if (pSigInfo->LM_CLOR)
				{
//					if(!pSigInfo->LM_CEKR) // OUTER YELLOW Fail
//					{
//						mColor |= 0x1000;
//					}											//2013.01.24  LED등으로 바뀌면서 filament fail없어짐.
				}
				else
				{
					mColor = MY_RED; 
				}
				tbmp.Base = mColor;
				tbmp.Draw(114 - IsRight); // OUTER YELLOW

			}
			else if ((pSigInfo != NULL ) && (pSigInfo->SE2) && (!pSigInfo->SIN2) && ( !pSigInfo->LM_ALR))  // Outer Callon
			{
				mRightColor = 0;
				if(!pSigInfo->TM1) mColor = MY_RED;
				tbmp.Base = mColor;
				tbmp.HomeDraw(114 - IsRight); // OUTER YELLOW
			}
			else
			{
				tbmp.Base = mColor;
				tbmp.Draw(114 - IsRight); // OUTER YELLOW
			}
		}

// Loop Indicator draw//
	   if (mHasLoop == 3 || mHasLoop == 1) // Both and Left
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->LEFT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
 		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(108 - IsRight);
	   }

	   if (mHasLoop == 3 || mHasLoop == 2) // Both and Right
	   {
		   mRightColor = 0;
		   if ( (pSigInfo != NULL ) && (pSigInfo->INLEFT || pSigInfo->INRIGHT) && (pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
		   {
			   mRightColor = 0x190;
		   }
		   tbmp.Base = mRightColor;	
		   tbmp.Draw(110 - IsRight);
	   }
	   if (mLamp == 2 || mLamp == 3 ) // 바 그리기
	   {
		   tbmp.Base = 0;
		   tbmp.Draw(169-IsRight);
		   p.y += 12;
		   tbmp.SetWP( p );
		   tbmp.Draw(169-IsRight);
		   p.y -= 12;
		   p.x += 12 + (-24 *IsRight);  // 3현시에서 문자선별등 위치 조절
		   tbmp.SetWP(p);
	   }
	   /*
	   else if ( mLamp == 4)
	   {
		   p.x += 12;
		   tbmp.SetWP( p );
		   tbmp.Base = 0;
		   tbmp.Draw(169-IsRight);
		   p.y += 12;
		   tbmp.SetWP( p );
		   tbmp.Draw(169-IsRight);
		   p.y -= 12;
		   p.x -= 12;
		   tbmp.SetWP(p);
	   }
	   */
	   
	   if (mHasLoop == 4)                  // Guage indicator
	   {
		   mRightColor = 0;
			 if ( pSigInfo != NULL ) {
		   	if ((pSigInfo->LEFT || pSigInfo->RIGHT) && ( !pSigInfo->LM_ALR))
		   	{
			  	 if (pSigInfo->INLEFT || pSigInfo->INRIGHT)
			   		{
				  		 mRightColor = MY_RED;
			   		}
		   	}
		   }

		   tbmp.Base = mRightColor;	

		   tbmp.Draw(156 - IsRight);
	   }

		break;

//***************************************// TCB //***************************************//
	case 5 : 

		tbmp.Base = 0;

		for (i = 0; i < 5; i++) 
		{

			if (i == 1)
			{
				tbmp.Base = mCAColor;
			}
			else if (i == 2)
			{
				tbmp.Base = mLCRColor;
			}
			else if (i == 3)
			{
				tbmp.Base = mCBBColor;
			}
			else if (i == 4)
			{
				tbmp.Base = mARROWColor;
			}

			tbmp.Draw(145 + i + IsRight*5);
		}


		break;

//***************************************// TGB //***************************************//
	case 6 : 

		tbmp.Base = 0;

		for (i = 0; i < 5; i++) 
		{
			if (i == 1)
			{
				tbmp.Base = mCAColor;
			}
			else if (i == 2)
			{
				tbmp.Base = mLCRColor;
			}
			else if (i == 3)
			{
				tbmp.Base = mCBBColor;
			}
			else if (i == 4)
			{
				tbmp.Base = mARROWColor;
			}

			tbmp.Draw(150 + i + IsRight*-5);
		}	

		break;
   }
	
	p.x += nDirection * (int)(CELLSIZE_X * 3.5);

//***************************************// Guage indicator name draw //***************************************//

if (mHasLoop == 4 && mRightColor == MY_RED)
{
	RECT r;	
	CFont font;

	if (m_sDigit.GetLength()>1)
	{
		font.CreateFont(15,4,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));
	}
	else
	{
		font.CreateFont(15,7,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));
	}
	CFont *oldfont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	if (((mLamp == 2 || mLamp == 3) && m_nType==0) || (m_nType==1 && (mLamp==0 || mLamp == 3))) // 3현시
	{
		::SetRect(&r,Pos.x - 49 + 98*IsRight, Pos.y, Pos.x - 49 + 98*IsRight, Pos.y);
	}
	else if (m_nType==1 && mLamp!=0 && mLamp!=2)  // 2현시
	{
		::SetRect(&r,Pos.x - 37 + 74*IsRight, Pos.y, Pos.x - 37 + 74*IsRight, Pos.y);
	}
	else if	(m_nType==2 && mLamp==12) // R+S+Route Indicator Ground Shunt
	{
		::SetRect(&r,Pos.x - 37 + 74*IsRight, Pos.y, Pos.x - 37 + 74*IsRight, Pos.y);
	}
	else if ( m_nType == 0 && mLamp == 8 )
	{
		::SetRect(&r,Pos.x - 73 + 146*IsRight, Pos.y, Pos.x - 73 + 146*IsRight, Pos.y);
	}
	else  // 4현시
	{
		::SetRect(&r,Pos.x - 61 + 122*IsRight, Pos.y, Pos.x - 61 + 122*IsRight, Pos.y);
	}

	pDC->SetTextColor( RGB(0,0,0) );
// 	if (pSigInfo->DESTTRACK)
// 	{
		MyDrawText( m_sDigit/* m_szDrawName */, -1, &r, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
// 	}

	font.DeleteObject();
	pDC->SelectObject(oldfont);
}




// Block name draw //

if (m_nType > 4 && m_nType < 7)
{
	RECT r;	
	CFont font;

    font.CreateFont(18,8,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	
		::SetRect(&r,Pos.x + 80-160*IsRight, Pos.y, Pos.x +100-200*IsRight, Pos.y);
    
    if (m_bCAClicked || mCAColor || m_bLCRClicked || mLCRColor || m_bCBBClicked || mCBBColor || mARROWColor)
	{
		pDC->SetTextColor( RGB(0,255,0) );
	}
	else
	{
		pDC->SetTextColor( RGB(255,255,255) );
	}

	if (m_nType == 5)
	{
	MyDrawText( "TCB", -1, &r, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
	}
	else
	{
	MyDrawText( "TGB", -1, &r, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
	}

	font.DeleteObject();

	CPoint p = mPos;
	CFont font2;
	font2.CreateFont(14,6,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	pDC->SelectObject(&font2);
	int gap;
    if((m_nType + IsRight) != 6)
	{
		gap = 43;
	}
	else
	{
		gap = 17;
	}
	
	pDC->SetTextColor(RGB(0,0,0));
	CRect rect(p.x-gap-11, p.y-11, p.x-gap+11, p.y+11);
	pDC->DrawText( "CA", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

    CRect rect2(p.x-gap-11+31, p.y-11, p.x-gap+11+31, p.y+11);
	if ( m_nType == 5)
	{
		pDC->DrawText( "LCG", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else
	{
		pDC->DrawText( "LCR", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}

    CRect rect3(p.x-gap-11+61, p.y-11, p.x-gap+11+61, p.y+11);
	if ( m_nType == 5 )
	{
		pDC->DrawText( "BRB", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else
	{
		pDC->DrawText( "BCB", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	font2.DeleteObject();
	pDC->SelectObject(oldfont);


}

#ifdef _Region
  CBrush brush2( RGB( 0, 255, 255) );
  DC->FrameRgn( this, &brush2, 1, 1 );
#endif
}

////////////////////////////////////////////////////
///////////////  CBmpSwitch ////////////////////////

CBmpSwitch::CBmpSwitch(int cellno,CPoint p,char *name , CPoint p2, BOOL bHandPointSW, BOOL bDoubleHSSW)
{

	m_bHandPointSW = bHandPointSW; //수동전철기 사용여부 = 1일때
	m_bDoubleHSSW = bDoubleHSSW;   //고속 전철기 쌍동여부


	SetObjectType();
	p = CPoint(p.x,p.y);

	mPos = p;	
	mNamePos = p2;	
	if (name) strcpy(mName,name);
	else mName[0] = 0;
	mColor = 0;
	mRightColor = 0;
	mState = NODE_CTYPE | NODE_NTYPE;	
	
	m_nHideDraw = 0; 
	if(cellno < 0) 
		m_nHideDraw=1;	
}

void CBmpSwitch::SetNode(UINT state )
{
	mState = state;
}

void CBmpSwitch::Draw( CDC *pDC ) 
{
    int bmpno = 0;
	if (m_pData) 
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch*)m_pData;

		if(pInfo->WR_P)	
		{ 
			bmpno = 90; // 정위
		}
		else  bmpno =91;              // 반위

		if ( m_bHandPointSW )
		{
			if (pInfo->KR_M)
				bmpno = 91;
			else
				bmpno = 90;
		}
		
		if ((!pInfo->TRACKLOCK) || pInfo->ROUTELOCK /* || pInfo->WLR */|| pInfo->SWFAIL) 
		{ 
			mColor = MY_YELLOW;       // 선로전환기 색상
		    if (/*pInfo->ON_TIMER ||*/ pInfo->SWFAIL)
				mColor |= 0x1000;  //FLASH
			else if ( pInfo->BLOCK == NULL && !m_bHandPointSW )
				mColor = MY_RED;
			else if ( pInfo->ON_TIMER && !pInfo->PASSROUTE )
				mColor |= 0x1000;  //FLASH
		}
		else if ( pInfo->BLOCK == NULL && !m_bHandPointSW )
		{
			mColor = MY_RED;
		}
		else mColor = 0;

		if (m_bHandPointSW)
		{
			m_bHandoper = pInfo->KEYOPER;
		}
		if (!pInfo->WKEYKR)
		{
			mRightColor = MY_YELLOW | 0x1000 ; //FLASH
		}
		else
		{
			mRightColor = MY_BLUE;
		}
	}

	CString strR = mName;
	CPoint Pos = mPos;

	if (mPos.y < mNamePos.y)
	{
  	   Pos.y += 3;  // dn
	}
	else
	{
  	   Pos.y -= 3;  // up
	}

	BOOL bIsExeptedXPoint = FALSE;
	if ( m_strStationName.Find("MYMENSINGH")==0 && (strR.Find("117X")==0 || strR.Find("118X")==0))
		bIsExeptedXPoint = TRUE;
	if(	(strR.Find("X") == 2 || strR.Find("X") == 3 )	&& (bIsExeptedXPoint == FALSE) )
	{
		RECT x;
	    ::SetRect(&x,Pos.x+18 , Pos.y +8 , Pos.x -18, Pos.y-8);
		pDC->SetTextColor( RGB(192,192,192) );
		MyDrawText( (LPCTSTR)mName, -1, &x, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
		return;
	}

	/*
	if(	strR.Find("A") > 0  && m_bHandPointSW )	
	{
		RECT x;
	    ::SetRect(&x,Pos.x+18 , Pos.y +8 , Pos.x -18, Pos.y-8);
		pDC->SetTextColor( RGB(192,192,192) );
		MyDrawText( (LPCTSTR)mName, -1, &x, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
		return;
	}
	*/
	

	BmpDraw tbmp;
	tbmp.SetBase( mColor );
    tbmp.SetWP(Pos);

	if( !m_nHideDraw ) 	
	{
		tbmp.Draw(bmpno);	// 선로전환기 그리기

		if (mRightColor)
		{
			tbmp.SetBase( mRightColor );
			if (!m_bDoubleHSSW)
			{
				if (mPos.y < mNamePos.y)
				{
					Pos.y += 32;  // dn
				}
				else
				{
					Pos.y -= 32;  // up
				}
			}
			else
			{
				Pos.x += 20;
			}

			tbmp.SetWP(Pos);
			tbmp.OrDraw(144);	// 핸드 포인트 키 그리기
		}
	}
			
	if(!mName) return; 

	pDC->SetBkMode(TRANSPARENT);
	if (m_bHandPointSW)
	{
		if (m_bHandoper)
		{
			pDC->SetTextColor( RGB(255,255,128) );
		}
		else
		{
			pDC->SetTextColor( RGB(62,62,62) );
		}
	}
	else
	{
		pDC->SetTextColor( _COLOR_SWITCH_NAME );
	}

	
	CPoint Posname = mPos;
 
	if (mPos.y < mNamePos.y)
	{
  	   Posname.y += 20; // dn
	}
	else
	{
  	   Posname.y -= 20;  // up
	}

	RECT r;
	::SetRect(&r,Posname.x+18 , Posname.y +8 , Posname.x -18, Posname.y-8);
    CPen pen;
	if (m_bHandPointSW)
	{
		if (m_bHandoper)
		{
			pen.CreatePen( PS_SOLID, 1, RGB(255,255,128));
		}
		else
		{
			pen.CreatePen( PS_SOLID, 1, RGB(62,62,62));
		}
	}
	else
	{
		pen.CreatePen( PS_SOLID, 1, _COLOR_SWITCH_NAME);
	}

	CBrush brush( RGB(0,0,0) );
    CPen *oldpen = pDC->SelectObject(&pen);
    CBrush *oldbrush = pDC->SelectObject(&brush);
	
	CPoint angle ;
	angle.x = 10;
	angle.y = 10;
	
	pDC->RoundRect(&r, angle);
	MyDrawText( (LPCTSTR)mName, -1, &r, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);
    
    pDC->SelectObject(oldpen);
    pDC->SelectObject(oldbrush);
	pen.DeleteObject();
	brush.DeleteObject();

#ifdef _Region
  CBrush brush2( RGB( 255, 0, 0) );
  DC->FrameRgn( this, &brush2, 1, 1 );
#endif

}

////////////////////////////////////////////////////
///////////////  CBmpButton ////////////////////////
CBmpButton::CBmpButton(int cellno, CPoint p, char *name, int isright, int nButtonType, CBmpTrack *pTrack )
{
	SetObjectType();

	p = CPoint(p.x,p.y);	
	mPos = p;
	m_pTrack = pTrack;

	SetName( name ); 

	l = strlen( m_szDrawName );
	if (l > 2) 
	{
		char *p = strstr( m_szDrawName, "UP" );
		if (p) *p = 0; 
		p = strstr( m_szDrawName, "DN" );
		if (p) {updown = 1; *p = 0; }  
	}

	IsRight = isright;
	mColor = 0;		

	m_IsPress=0;

	mWidth = strlen(m_szDrawName);
	if ( mWidth < 3 ) mWidth = 3;

    m_pSignal = NULL;
	m_nButtonType = nButtonType;

	for (int i = 0; i < 20; i++)
	{
		m_StateOfAdditionalBtn[i].updnBtnName = "";
		m_StateOfAdditionalBtn[i].ispressed = FALSE;
	}
	m_iFlagForAddtionalBtn = 0;
}

void CBmpButton::Draw( CDC *pDC ) 
{
	int Buttonview = 1; // 
	if (m_nButtonType == 5)
	{
		Buttonview = 0;
	}
	if (Buttonview == 1)
	{
		if ( m_cConfig & OPTION_NOBUTTONVIEW ) return;

		char *str; 
		char *str2; 
		char    m_str2[20];
		CPoint Pos;
		CPoint BtnPos;
		int k , iBtnCk;
		CString outer;
		CString strBtn;
		CString strRealBtnName;
		int iMatchWithAdditionalBtn = 0;

		str = m_szDrawName;
		strBtn = str;
		strRealBtnName = str;
		str2 = m_str2;
		
		strBtn.Remove('R');
		strBtn.Remove('L');
		strBtn.Remove('U');
		strBtn.Remove('/');

		memcpy( str2 , strBtn , strBtn.GetLength());
		str2[strBtn.GetLength()] = '\0';

		if(strchr(m_szDrawName,'/')!=NULL && m_iFlagForAddtionalBtn == 0 )
		{
			for(int i = 1; i < 20; i++)
			{
				if( m_StateOfAdditionalBtn[i].updnBtnName == strBtn )
				{
					m_iFlagForAddtionalBtn = i;
					break;
				}
				else if( m_StateOfAdditionalBtn[i].updnBtnName == "" )
				{
					m_StateOfAdditionalBtn[i].updnBtnName = strBtn;
					m_iFlagForAddtionalBtn = i;
					break;
				}
			}
		}
		if ( m_iFlagForAddtionalBtn == 0 )
		{
			for(int j = 1; j < 20; j++)
			{
				if ( m_StateOfAdditionalBtn[j].updnBtnName == strBtn && m_StateOfAdditionalBtn[j].ispressed == TRUE )
				{
					iMatchWithAdditionalBtn = j;
					break;
				}
			}
		}
		
		iBtnCk = 0;
		TScrobj *pRetObj = m_pTrack->FindObject( OBJ_SIGNAL, str2 );
		if (pRetObj == NULL) {
			//return;
			if ( m_iFlagForAddtionalBtn != 0 )
			{
				if ( m_pSignal && m_pSignal->m_nPressTimer)
				{
					m_StateOfAdditionalBtn[m_iFlagForAddtionalBtn].ispressed = TRUE;
				}
				else
				{
					m_StateOfAdditionalBtn[m_iFlagForAddtionalBtn].ispressed = FALSE;
				}
				iBtnCk = 0;
				return;
			}
			else if ( m_pSignal && m_pSignal->m_nPressTimer ) 
			{
				pRetObj = m_pTrack->FindObject( OBJ_BUTTON, str );
				BtnPos = pRetObj->mPos;
				iBtnCk = 1;
			}
			else if ( m_StateOfAdditionalBtn[iMatchWithAdditionalBtn].ispressed == TRUE ) 
			{
				pRetObj = m_pTrack->FindObject( OBJ_BUTTON, str );
				BtnPos = pRetObj->mPos;
				iBtnCk = 1;
			}
			else 
			{
				iBtnCk = 0;
				return;
			}
		}

		IsRight = pRetObj->m_nDir+1;

		Pos = pRetObj->mPos;
		k = pRetObj->m_nObjType;
		outer = str;

		CPoint p;

		if (_nSFMViewXSize == 1600 || _nSFMViewXSize == 1920 || REVERSE == 0) // 큰 역 (신호기 명 변경)
		{
			if (outer.Find("A") == 0 && ( m_strStationName == "BHAIRAB_BAZAR" && outer != "A42")) // Outer signal
			{
				if (IsRight == 1)  Pos.y += 24;
				else	Pos.y -= 24;				
			}
			else  
			{				
				if (pRetObj->m_nDir == 1) 
				{
					if (mPos.x < pRetObj->mPos.x)	Pos.x -= 35;
					else {
						if (mPos.y < pRetObj->mPos.y)	Pos.y -= 24;
						else Pos.y += 24;
					}
				} else {
					if (mPos.x > pRetObj->mPos.x)	Pos.x += 35;
					else {
						if (mPos.y < pRetObj->mPos.y)	Pos.y -= 24;
						else Pos.y += 24;
					}
				}
			}

			p = Pos;
		}

		else                        // 작은 역 (신호기 명 고정)
		{
			if (outer.Find("A") != -1) 
			{				
				if (IsRight == 1) Pos.y += 24;
				else	Pos.y -= 24;
				
				if ( m_strStationName.Find == "LAKSAM" == 0 && outer == "A46" )
				{
					Pos.y -= 24;
					Pos.x +=35;
				}
			}
			else
			{
				if (pRetObj->m_nDir == 1) 
				{
					if (mPos.x < pRetObj->mPos.x)	Pos.x -= 35;
					else {
						if (mPos.y < pRetObj->mPos.y)	Pos.y -= 24;
						else Pos.y += 24;
					}
				} else {
					if (mPos.x > pRetObj->mPos.x)	Pos.x += 35;
					else {
						if (mPos.y < pRetObj->mPos.y)	Pos.y -= 24;
						else Pos.y += 24;
					}
				}
			}
			p = Pos;
		}

		p = Pos;
		

		if ( iBtnCk == 1 ) p = Pos = BtnPos;

		int dx = int( _nDotPerCell/2 * 1.5);
		Pos.x -= dx * (mWidth-1) / 2;
				
		int bmpno = 75;
		BmpDraw tbmp;
		short color = 0;

		if (k==1)
		{
			// 2006. 5.18   starter signal button blue color....
			if ( !((CBmpSignal*)pRetObj)->mHasRoute )
				color = MY_YELLOW; // starter
		}
		else if (k==0)
		{
			color = MY_RED;  // home
		}
		pDC->SetTextColor( _COLOR_BUTTON_NAME );
	
#ifdef _LSD
#else

    	if (m_pSignal && m_pSignal->m_nPressTimer) 
		{
			pDC->SetTextColor( RGB(0,0,0) );
			color = MY_GREEN;
		}

		if ( m_StateOfAdditionalBtn[iMatchWithAdditionalBtn].ispressed == TRUE )
		{
			pDC->SetTextColor( RGB(0,0,0) );
			color = MY_GREEN;
		}

		if (strRealBtnName.Find('/') > 0)
			return;

		ScrInfoSignal *pSigInfo;
		for (int index=0; index<256; index++)
		{
			if (EtcScrInfo[index].str == strBtn )
			{
				if (EtcScrInfo[index].type == OBJ_SIGNAL) 
					break;
			}
			else
			{
				if (EtcScrInfo[index].m_pData == NULL)
					break;
			}
		}

		if (EtcScrInfo[index].m_pData != NULL)
		{
			pSigInfo = (ScrInfoSignal*)EtcScrInfo[index].m_pData;
			if ( !pSigInfo->FREE || pSigInfo->ON_TIMER || pSigInfo->REQUEST ) 
			{
                pDC->SetTextColor( RGB(0,0,0) );
				color = MY_GREEN;     // Main Signal On
				if (pSigInfo->CS)
				{
					color = MY_GRAY;  // Shunt & Callon Signal On
				} 	
			}
		}
#endif
		DWORD Drawmode = MERGECOPY;
 
		tbmp.SetBase( color ); // base = color
		if (((strchr(m_szDrawName, 'S') != NULL) && m_szDrawName[0] == 'S') || strchr(m_szDrawName, 'E') != NULL || strchr(m_szDrawName, 'W') != NULL )
		{
			Pos.x = Pos.x-(dx*2)+5;
			tbmp.SetWP(Pos); // WP = Pos
			tbmp.DrawButton(bmpno,Drawmode);  // 버튼 길이 226~236 출력(총 226~245)

			Pos.x = Pos.x+(dx);
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno+1,Drawmode);

			Pos.x = Pos.x+(dx);
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno+1,Drawmode);

			Pos.x = Pos.x+(dx);
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno+1,Drawmode);
			if ( strchr(m_szDrawName, 'E') != NULL || strlen( m_szDrawName )>4) 
			{
				Pos.x = Pos.x+(dx);
				tbmp.SetWP(Pos);
				tbmp.DrawButton(bmpno+1,Drawmode);

				if (strchr(m_szDrawName, 'E') != NULL || strchr(m_szDrawName, 'W') != NULL)
				{
					Pos.x = Pos.x+(dx);
					tbmp.SetWP(Pos);
					tbmp.DrawButton(bmpno+1,Drawmode);

					for ( int k = 5;k < strBtn.GetLength();k++)
					{
						Pos.x = Pos.x+(dx);
						tbmp.SetWP(Pos);
						tbmp.DrawButton(bmpno+1,Drawmode);
					}
				}
			}


		} else {
			tbmp.SetWP(Pos); // WP = Pos
			tbmp.DrawButton(bmpno,Drawmode);  // 버튼 길이 226~236 출력(총 226~245)

			Pos.x += dx;
			tbmp.SetWP(Pos);
			tbmp.DrawButton(bmpno+1,Drawmode);
		}

		Pos.x += dx;
		tbmp.SetWP(Pos);
		tbmp.DrawButton(bmpno+5,Drawmode);
		pDC->SetBkMode(TRANSPARENT);

		if ( m_IsPress ) 
		{
			p.x++;
			p.y++;
		}
		if (strchr(m_szDrawName, 'S') != NULL && m_szDrawName[0] == 'S')
		{
			CRect rect(p.x-14, p.y-8, p.x+3, p.y+8);
			MyDrawText( strBtn, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, 0 );
		}
		else
		{
			CRect rect(p.x-8, p.y-8, p.x+8, p.y+8);
			MyDrawText( strBtn, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, 0 );
		}
	}
#ifdef _Region
  CBrush brush2( RGB( 0, 255, 0) );
  DC->FrameRgn( this, &brush2, 1, 1 );
#endif
}
////////////////////////////////////////////////////
//////    CBmpEtcObj cpp    //////////////////////
CBmpEtcObj::CBmpEtcObj(CPoint p, char *name, char *name2, int Type,int id, int width, int height,
					   WORD nOnID, WORD nOnColor, WORD nFlashID, WORD nFlashColor)
{		

	m_nOnID = nOnID;
	m_nOnColor = nOnColor;
	m_nFlashID = nFlashID;
	m_nFlashColor = nFlashColor;
	m_Sysfail = TRUE;
	m_bBTNClick = FALSE;

	mPos = p;
	SetName( name );
	strcpy( m_szDrawName, name2 );

	int l = strlen( m_szDrawName );
	mColor = 0;
	m_IsPress = 0;
	if(width == 0)	{
		mWidth = strlen(m_szDrawName);
		if ( mWidth < 3 ) mWidth = 3;
		if(Type > 19) mWidth+=2;
	}
	else mWidth = width;
	mHeight = height;
	m_nType = Type;
	m_nID = id;
	SetObjectType();
}

CBmpEtcObj::CBmpEtcObj(CPoint p, char *name, int Type,int id,int width)
{		
	mPos = p;
	SetName( name );
	int l = strlen( m_szDrawName );	

	m_IsPress = 0;
	if(width == 0)	{
		mWidth = strlen(m_szDrawName);
		if ( mWidth < 3 ) mWidth = 3;
		if(Type > 19) mWidth+=2;
	}
	else mWidth = width;
	m_nType = Type;
	m_nID = id;
	SetObjectType();	
}

void CBmpEtcObj::SetObjectType() 
{
	if( m_nType > 19 ) 
	{
		if ( m_nOnID ) 
			m_cObjectType = OBJ_SYSPUSH;
		else 
			m_cObjectType = OBJ_SYSBUTTON;
	}
	else
		m_cObjectType = OBJ_ETC; 
}

TScrobj* CBmpEtcObj::PtInArea( CPoint &point )
{	
	m_bBTNClick = FALSE;
	switch( m_cObjectType ) 
	{
	case OBJ_SYSPUSH:	//OBJ_SYSBUTTON:
		if ( PtInRegion( point ) && m_nID)
		{
			m_bBTNClick = TRUE;
			return (TScrobj*)this;			
		}
		break;		
	}	
	return NULL;
}

TScrobj * CBmpEtcObj::FindObject( char cType, char * szName )
{

	switch( cType ) 
	{
	case OBJ_SYSPUSH:	//OBJ_SYSBUTTON:
		if (!strcmp( mName, szName ) && m_nID) return (TScrobj*)this;
		break;	
	}
	return NULL;
}

TScrobj * CBmpEtcObj::FindObject( char cType, short nID )
{
	switch( cType ) 
	{
	case OBJ_SYSPUSH:	//OBJ_SYSBUTTON:
		if ( m_nID == nID ) return (TScrobj*)this;
		break;	
	}	
	return NULL;
}

void CBmpEtcObj::ConvertPos() 
{		
	LptoDp(mPos);
	if ( (m_cObjectType == OBJ_SYSPUSH) && m_nID && !m_hObject ) {
		CPoint  p = mPos;
		int dx = CELLSIZE_X;
		int dy = CELLSIZE_Y;
		dx = (int)(dx * 1.5);

		if (m_nID == 253 || m_nID == 270 || m_nID == 278 || m_nID == 280 || m_nID == 305 )
		{
			p.y += mWidth*5 + 40;
		}

		CreateRectRgn( p.x - dx * mWidth / 2 - 2, p.y - dy - 2,
								   p.x + dx * mWidth / 2 + 2, p.y + dy + 2);
	}
}

BOOL GetBitLogic( void *ptr, WORD nBitLoc ) 
{
	BYTE *pBase = (BYTE*)ptr;
	pBase += ((nBitLoc & 0x7FFF) >> 3 );
    BYTE res = *pBase & (1 << (nBitLoc & 7));
	if (nBitLoc & 0x8000) 
		return res == 0; // 정상
	return res != 0;
}

void CBmpEtcObj::Draw(CDC *pDC)
{
	BmpDraw::m_pDC = pDC;//2012add

	mColor = 0;
	mRightColor = 0;
	CPoint Pos = mPos;
	int bmpno = 92;//75;
	BOOL bSuperBlock = FALSE;

	DataHeaderType *pDataHead = (DataHeaderType*)m_pData;
	
	CString strTrackName = mName;

	if(m_nType == 33)		// 건널목 -- ONLY DISPLAY
	{
		CPoint p = mPos;
		CPoint pline = mPos;
		BmpDraw tbmp;
		tbmp.SetBase( 0 );
		int lWidth = mWidth;
		CString str = m_szDrawName ;
		CString strRaw[5];
		int iFind$ = 0;
		int iRaw = 1;

		str.Replace('_',' ');

		for ( ; iRaw < 5; iRaw++)
		{
			if ( str.Find('$') < 0 )
			{
				strRaw[iRaw-1] = str;
				break;
			}
			else
			{
				strRaw[iRaw-1] = str.Left(str.Find('$'));
				str = str.Right(str.GetLength()-str.Find('$')-1);
			}
		}


		for(int i = 0; i <(lWidth-1); i++)
		{
			pline.y += 5*i;
			tbmp.SetWP(pline);
			tbmp.OrDraw(37);
			
			pline.y -= 10*i+5;
			tbmp.SetWP(pline);
			tbmp.OrDraw(37);
			pline = mPos;
		}

		p.y += mWidth*5;
		tbmp.SetWP( p );
		tbmp.Draw( 50 );

		p.y -= mWidth*10;
		tbmp.SetWP( p );
		tbmp.Draw( 50 );

        if ( iRaw == 1 && strRaw[0].Find("LC") == 0 )
			strRaw[0] += " CLOSED";
		CFont font;
		font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
		CFont *oldfont = pDC->SelectObject(&font);

		pDC->SetTextColor(RGB(192,192,192));

		for ( int iCount = 0; iCount < iRaw; iCount++)
		{
			CRect rect(p.x-5, p.y-30-14*(iRaw-iCount-1), p.x+5, p.y-20-14*(iRaw-iCount-1));
			pDC->DrawText( strRaw[iCount], -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		}
		
		pDC->SelectObject( oldfont );

	}

	if (m_nType >19 && m_nType < 31) // system BUTTON
	{
		mColor = 0;
		if (m_pData) 
		{
			if (m_nFlashID) 
			{
				if ((m_nID == 231) ) // Run
				{
					if (!GetBitLogic(m_pData, 32912))
					{
						mColor = m_nOnColor;    // bSysRun = 1 일때
					}
					else if(!GetBitLogic(m_pData, 32913))
					{
						mColor = m_nFlashColor | 0x1000; // bSysFail = 1 일때
					}
				}

				else if (m_nID == 274) // LOCAL MODE
				{
					if (GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = m_nOnColor;
					}
				}

				else if (m_nID == 275) // CTC MODE
				{
					if (!GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = m_nOnColor;
					}
					else if (!GetBitLogic(m_pData, m_nFlashID+1))
					{
						mColor = m_nOnColor;
						mColor |= 0x1000;
					}
				}

				else if ((m_nID == 232) || (m_nID == 233)) // sys1, sys2 
				{
					if (!GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = m_nOnColor;    // bSysRun = 1 일때
					}
					else if(GetBitLogic(m_pData, m_nFlashID + 1))
					{
						mColor = m_nFlashColor | 0x1000; // bSysGood = 0 일때
					}
				}

				else if (m_nID == 247) // STN
				{
					if (!GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = MY_GRAY;    // bStation = 1 일때
						strcpy(m_szDrawName,"CLOSE");
					}
					else 
					{
						mColor = m_nOnColor;
						strcpy(m_szDrawName,"OPEN");
					}
				}

				else if (m_nID == 235) // Generator
				{
					if (!GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = m_nOnColor ;  // 1일때
					}
					else mColor = MY_BLUE;

//					if( !pDataHead->nSysVar.bGenLowFuel )
//						mColor = MY_YELLOW | 0x1000;
					if( pDataHead->nSysVar.bGen )
						mColor = MY_RED | 0x1000;
				}

				else if (m_nType == 28) // EPK
				{
					if (m_nID > 260 && m_nID < 268)
					{
						mRightColor = 0;
						if(GetBitLogic(m_pData, m_nFlashID))
						{
							mRightColor = FALSE; // bPKeyCont = 0 일때
						}
						else
						{
							mRightColor = TRUE;
						}
					}
					else
					{
						mRightColor = 0;
						if(GetBitLogic(m_pData, m_nFlashID+2))
						{
							mRightColor = FALSE; // bPKeyCont = 0 일때
						}
						else
						{
							mRightColor = TRUE;
						}
					}
				}

				else if (m_nType == 25) // DayNight
				{
					mRightColor = 0;
					if(!GetBitLogic(m_pData, m_nFlashID))
					{
						mRightColor = TRUE; // bIDayNight = 1 일때
					}
					else
					{
						mRightColor = FALSE;
					}
					if(!GetBitLogic(m_pData, m_nFlashID-38))
					{
						mColor = m_nFlashColor | 0x1000; // bN2 = 1 일때
					}
					else
					{
						mColor = MY_BLUE;
					}
				}

				else if (m_nType == 29) // Level Crossing
				{
					mRightColor = 0;
					if(!GetBitLogic(m_pData, m_nFlashID ))
					{
						mColor = m_nFlashColor | 0x1000; // bLC1NKXPR = 1 일때
					}
					else 
					{
						mColor = MY_BLUE;
					}

					if(!GetBitLogic(m_pData, m_nFlashID -1))
					{
						mRightColor = m_nFlashColor | 0x1000;  // bLC1KXLR = 1일때
					}
					else
					{
						mRightColor = MY_BLUE;
					}
				}
				
				else if (m_nType == 30) // Level call
				{
					if(!GetBitLogic(m_pData, m_nFlashID +1))
					{
						mColor = MY_RED; // bLC1BELACK = 1 일때
					}
					
					else if(!GetBitLogic(m_pData, m_nFlashID ))
					{
						// NOP / SHB 는 막고 컴파일  // 예외처리 
//						if ( m_strStationName.Find ( "NOYA",0 ) < 0 && m_strStationName.Find  ( "SHAH",0 ) < 0 ) {
							mColor = MY_YELLOW | 0x1000; // bLC1BELL = 1 일때  05.09.04 ..SW처리 건널목인 경우 flashing 표시 할 수 없다..  이 Line이 Comment 처리..
//						}
					}

					else
					{
						mColor = MY_BLUE;
					}

				}
				else if (m_nType == 21) // Option 버튼 상태
				{
					mRightColor = 0;
					if (m_nID == 242) // Battery
					{
						if ( !GetBitLogic(m_pData, m_nFlashID + 1) )
						{
							mColor = m_nFlashColor | 0x1000; // bLowVoltage = 0 일때
						}
						else if ( GetBitLogic(m_pData, m_nFlashID) )
						{
							mColor = MY_YELLOW; // bCharging = 0 일때
						}

						if ( GetBitLogic(m_pData, m_nFlashID - 28) )
						{
							mRightColor = m_nFlashColor | 0x1000;  // bCharge = 0일때
						}
					}
				    else if (m_nID == 227 || m_nID == 228 || m_nID == 244 || m_nID == 245) // S, P, Alarm, Buzzer
					{
						if (!GetBitLogic( m_pData, m_nFlashID ))
						{
							mColor = m_nFlashColor | 0x1000; // 1 일때
						}
					}
				    else if ( m_nID == 253)
					{
						if ( !pDataHead->LCCStatus.bLCC1Alive )
							mColor = MY_RED | 0x1000;
						else if ( pDataHead->LCCStatus.bLCC1Active)
							mColor = MY_GREEN;
						else
							mColor = MY_BLUE;
					}
					else if ( m_nID == 254)
					{
						if ( !pDataHead->LCCStatus.bLCC2Alive )
							mColor = MY_RED | 0x1000;
						else if ( pDataHead->LCCStatus.bLCC2Active)
							mColor = MY_GREEN;
						else
							mColor = MY_BLUE;
					}

					else if (m_nID == 230) // Modem 
					{
//						if ( m_strStationName.Find ( "SYLHE",0 ) < 0 ) {
//#ifndef _SYT 2006.7.27 막음
							if (GetBitLogic( m_pData, m_nFlashID ))
							{
								mColor = m_nFlashColor | 0x1000; // 0 일때
							}
							else if (GetBitLogic( m_pData, m_nFlashID + 1 ))
							{
								mColor = m_nFlashColor | 0x1000; // 0 일때
							}
							else if (GetBitLogic( m_pData, 32852))
							{
								mColor = m_nFlashColor | 0x1000; // 0 일때
							}
//#endif 2006.7.27 막음
//						}
					}
					else // Ground
					{
						if (!GetBitLogic( m_pData, m_nFlashID ))
						{
							mColor = m_nFlashColor | 0x1000; // 1 일때
						}
					}
				}

				else if (m_nID == 229) //FUSEc  -- Fail시 1
				{
//					if ( m_strStationName.Find ( "SYLHE",0 ) >= 0 ) {
//#ifdef _SYT 2006.7.27 막음
//						if (!GetBitLogic( m_pData, m_nFlashID ) || !GetBitLogic( m_pData, m_nFlashID + 1)
//							|| !GetBitLogic( m_pData, m_nFlashID + 0x0a) || !GetBitLogic( m_pData, m_nFlashID + 0x0b) )
//						{
//							mColor = m_nFlashColor | 0x1000; // 0 일때
//						}
//						else 
//						{
//							mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
//						}

//#else 2006.7.27 막음
//					} else {
						if (!GetBitLogic( m_pData, m_nFlashID ) || !GetBitLogic( m_pData, m_nFlashID + 1) || !GetBitLogic( m_pData, 33150) || !GetBitLogic( m_pData, 33151) || !pDataHead->UnitState.bSysRun )
						{
								mColor = m_nFlashColor | 0x1000; // 0 일때
						}
//#endif 2006.7.27 막음
						else 
						{
							mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
						}
//					}
				}

				else if (m_nID == 301) //FUSEr
				{
					if (!GetBitLogic( m_pData, m_nFlashID ) || !pDataHead->UnitState.bSysRun )
					{
						mColor = m_nFlashColor | 0x1000; // 0 일때
					}

					else 
					{
						mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
					}
				}

				else if (m_nID == 251) //FAN -- Fail시 0
				{
					CString strForAXL = mName;
					if ( strForAXL == "AXL" )
					{
						if(!GetBitLogic( m_pData, 32851) || !pDataHead->UnitState.bSysRun )
						{
							mColor = m_nFlashColor | 0x1000;
						}
						else
						{
							mColor = MY_GREEN;
						}
// 						ScrInfoBlock *pReferBlock = NULL;
// 						CString strReferBlockName;
// 						int iAxlDpCount = 0;
// 						CString strForBlockNumber[4];
// 
// 						mColor = MY_GREEN;
// 
// 						for (int i=0; i<SignalObject.GetSize(); i++ ) 
// 						{
// 							TScrobj *pObj = (TScrobj *)SignalObject.GetAt( i );
// 							strReferBlockName = pObj->mName;
// 
// 							if ( strReferBlockName.Left(1) == 'B' && iAxlDpCount < 4)
// 							{
// 								strForBlockNumber[iAxlDpCount] = strReferBlockName;
// 								pReferBlock = (ScrInfoBlock*)pObj->m_pData;
// 								if ( !pReferBlock->AXLDST)
// 								{
// 									mColor = MY_RED | 0x1000;;
// 								}
// 								iAxlDpCount++;
// 							}
// 						}
					}
					else
					{
//						if ( m_strStationName.Find ( "SYLHE",0 ) >= 0 ) {
//	//#ifdef _SYT 2006.7.27 막음
//							if ( GetBitLogic( m_pData, m_nFlashID ) || GetBitLogic( m_pData, m_nFlashID+0x0c ) )
//							{
//								mColor = m_nFlashColor | 0x1000; // 0 일때
//							}
//							else 
//							{
//								mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
//							}
//						} else {
	//#else 2006.7.27 막음
							if (GetBitLogic( m_pData, m_nFlashID ) )
							{
								mColor = m_nFlashColor | 0x1000; // 0 일때
							}
							else 
							{
								mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
							}
	//#endif 2006.7.27 막음
//						}
					}
				}
	
				else			
				{
					if (GetBitLogic( m_pData, m_nFlashID ) )
					{
						mColor = m_nFlashColor | 0x1000; // 0 일때
					}

					else if (m_nType == 20)
					{
						mColor = MY_GREEN;  // 그림 버튼이 아닌경우 
					}
				}
			}
			else
			{
				if ( m_nID == 268 || m_nID == 269 )
				{
					TScrobj *pObj = NULL;
					ScrInfoBlock *pBlockInfo = NULL;

					CString strBlockName;

					strBlockName.Format("B%d",m_nOnColor);
					
					for ( int i = 0; i < SignalObject.GetSize(); i++ )
					{
						pObj = (TScrobj *)SignalObject.GetAt(i);
						
						if ( strcmp( pObj->mName, strBlockName ) == NULL)
						{
							pBlockInfo = (ScrInfoBlock*)pObj->m_pData;
							break;
						}
					}
					
					if ( pBlockInfo == NULL )
					{
						mColor = MY_BLUE;
					}
					else
					{
						if ( pBlockInfo->SAXLACTIN )
						{
							bSuperBlock = TRUE;

							if ( pBlockInfo->SAXLREQ )
							{
								mColor = MY_RED;
							}
							else if ( !pBlockInfo->SAXLDST )
							{
								mColor = MY_RED | MY_FLASHING;
							}
							else
							{
								mColor = MY_BLUE;
							}
						}
						else
						{
							bSuperBlock = FALSE;
							
							if ( pBlockInfo->AXLREQ )
							{
								mColor = MY_RED;
							}
							else if ( !pBlockInfo->AXLDST )
							{
								mColor = MY_RED | MY_FLASHING;
							}
							else
							{
								mColor = MY_BLUE;
							}
						}
					}
				}
				else if ( m_nID == 283 || m_nID == 285 )
				{
					TScrobj *pObj = NULL;
					ScrInfoBlock *pBlockInfo = NULL;
					
					CString strBlockName;
					
					strBlockName.Format("B%d",m_nOnColor);
					
					for ( int i = 0; i < SignalObject.GetSize(); i++ )
					{
						pObj = (TScrobj *)SignalObject.GetAt(i);
						
						if ( strcmp( pObj->mName, strBlockName ) == NULL)
						{
							pBlockInfo = (ScrInfoBlock*)pObj->m_pData;
							break;
						}
					}
					
					if ( pBlockInfo == NULL )
					{
						mColor = MY_BLUE;
					}
					else
					{				
						if ( pBlockInfo->SAXLACTIN )
						{
							bSuperBlock = TRUE;

							if ( pBlockInfo->SAXLREQ && ( pBlockInfo->SAXLACC || pBlockInfo->SAXLDEC ) )
							{
								mColor = MY_RED;
							}
							else if ( pBlockInfo->SAXLREQ )
							{
								mColor = MY_RED | MY_FLASHING;
							}
							else
							{
								mColor = MY_BLUE;
							}
						}
						else
						{
							bSuperBlock = FALSE;
							
							if ( pBlockInfo->AXLREQ && ( pBlockInfo->AXLACC || pBlockInfo->AXLDEC ) )
							{
								mColor = MY_RED;
							}
							else if ( pBlockInfo->AXLREQ )
							{
								mColor = MY_RED | MY_FLASHING;
							}
							else
							{
								mColor = MY_BLUE;
							}
						}
					}
				}
				else if ( m_nID == 282 )
				{
					ScrInfoTrack *pTKInfo = NULL;
					CString strButtonName = mName;

					if ( strButtonName.Find('_') > 0)
					{
						strButtonName = strButtonName.Left(strButtonName.Find('_'));
					}

					for ( int i = 0; i < TrackObject.GetSize(); i++ )
					{
						TScrobj *pObj = (TScrobj *)TrackObject.GetAt(i);
						
						CString strTKName = pObj->mName;
						
						if ( strTKName.Find('.') > 0)
						{
							strTKName = strTKName.Left(strTKName.Find('.'));
						}
						
						if ( strcmp( strTKName, strButtonName ) == NULL)
						{
							pTKInfo = (ScrInfoTrack*)pObj->m_pData;
							break;
						}
					}
					
					if ( pTKInfo == NULL )
					{
						mColor = MY_BLUE;
					}
					else
					{
						if ( pTKInfo->RESETBUF )
						{
							mColor = MY_RED | MY_FLASHING;
						}
						else
						{
							mColor = MY_BLUE;
						}
					}
				}
			}

			CLSMApp* pApp = (CLSMApp*)AfxGetApp();

			if ( (m_cConfig & OPTION_COMFAIL) && pApp->m_iMySystemNo != 3 ) 
			{
				mColor = MY_RED | 0x1000;
			}
		}
		BmpDraw tbmp;

		
if (m_nType == 26)  //RGB
{
    if (mColor == m_nOnColor) mColor = 1;
	else mColor = m_nOnColor;

	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	tbmp.RGBDraw(115);

	CPoint p = mPos;
	CRect rect(p.x-5, p.y-25, p.x+5, p.y-15);
	pDC->SetTextColor(RGB(255,255,255) );
	pDC->DrawText( m_szDrawName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	CRect rect2(40, 320, 60, 340);
	CRect rect3(1180, 320, 1200, 340);
}



else if (m_nType == 27) // OSS (mColor = True이면 "N")
{

 	CString str;
	char nLen;
	str  = m_szDrawName;
	strcpy(mName,"OSS");
	nLen = str.Find(".");

#ifdef _LSD
#else

	ScrInfoSignal *pSigInfo;
	for (int index=0; index<256; index++)
	{
		if (EtcScrInfo[index].str.Left(nLen) == str.Left(nLen) )
		{
			if (EtcScrInfo[index].type == OBJ_SIGNAL) 
				break;
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	if (EtcScrInfo[index].m_pData != NULL) 
	{
		pSigInfo = (ScrInfoSignal*)EtcScrInfo[index].m_pData;

		if (pSigInfo->OSS)
			mColor = 0;     // "R"   -  Stop
		else
			mColor = 1;		// "N"   -  Normal 	
	}
#endif
	int dir;
	if (Pos.x < 1000) dir =0;
	else dir = 2;

	if (!mColor) bmpno = 141 + dir;
	else bmpno = 140 + dir;
	int state = mColor;
	mColor = 0;
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	tbmp.Draw(bmpno);
	CPoint p = mPos;

	CFont font;
	font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);
    CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	pDC->DrawText( m_szDrawName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

	CFont font2;
	font2.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
    pDC->SelectObject(&font2);
	font.DeleteObject();

	if (!state) 
	{
		CRect rect2(p.x+20-dir*22, p.y+5, p.x+24-dir*22, p.y+10);
		pDC->SetTextColor(RGB(255,0,0) );
        pDC->DrawText( "R", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else 
	{
		CRect rect3(p.x-24+dir*22, p.y+5, p.x-20+dir*22, p.y+10);
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "N", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	pDC->SelectObject( oldfont );
	font2.DeleteObject();

}

else if (m_nType == 28) // EPK
{
 	CString str;
	char nLen, nLen1;
	str  = m_szDrawName;
	if (STATIONUPDN == 1)
	{
		nLen = str.Find("UEPK");
		if ( m_nID == 251 )
		{
			nLen = NULL;
		}
	}
	else
	{
		nLen = str.Find("DEPK");
		if ( m_nID == 252 )
		{
			nLen = NULL;
		}
	}
	if(	nLen1 = str.Find("AEPK"))
	{
		if ( str.Find("G1") == NULL )
		{
			nLen1 = NULL;
			bAreThereOnlyUepkAndDepk = FALSE;
		}
	}
	else
	{
		bAreThereOnlyUepkAndDepk = FALSE;
	}

	if ( bAreThereOnlyUepkAndDepk == FALSE )
		nLen = -1;

	CPoint pline = mPos;

	short l = strlen(str);
    short n = str.Find( '_' ); 

	CFont font;
	font.CreateFont(18,9,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);

	for (;n > 0 ; n = str.Find( '_' ))
	{
		CString Temp = str.Right(l-n-1);
		str = str.Left( n );
		str += "  ";
		str += Temp;
		l++;
	}

	if (nLen == NULL || nLen1 == NULL )
	{
		CBrush brush( RGB(0,0,0) );
		CBrush *oldbrush = pDC->SelectObject(&brush);
		
		CPen pen( PS_SOLID, 2, RGB(128,128,128));
		CPen *oldpen = pDC->SelectObject(&pen);
		pDC->SelectObject(pen);
		
		CRect rectl;
		if ( ( nLen1 == NULL ) && _nSFMViewXSize == 2560 ) 
		{
			rectl.left = pline.x-75;
			rectl.top = pline.y-30;
			rectl.right = pline.x+480;
			rectl.bottom = pline.y+69;
		}
		else if ( nLen1 == NULL )
		{
			rectl.left = pline.x-75;
			rectl.top = pline.y-30;
			rectl.right = pline.x+480;
			rectl.bottom = pline.y+19;
		}
		else if (_nSFMViewXSize == 1600 ) 
		{// 큰 역 (신호기 명 변경)
			rectl.left = pline.x-38;
			rectl.top = pline.y-30;
			rectl.right = pline.x+430;
			rectl.bottom = pline.y+19;
			//rectl(pline.x-38, pline.y-30, pline.x+430, pline.y+19);
		}
		else if (_nSFMViewXSize == 1920 ) 
		{// 큰 역 (신호기 명 변경)
			rectl.left = pline.x-55;
			rectl.top = pline.y-30;
			rectl.right = pline.x+200;
			rectl.bottom = pline.y+19;
//			rectl(pline.x-38, pline.y-30, pline.x+830, pline.y+19);
		}
		if ( m_strStationName.Find("@StandardOfLC") > 0)
		{
			rectl.right+=100;
		}
		else if ( m_strStationName == "AKHAURA" )
		{
			rectl.left+=20;
			rectl.right+=220;
		}
		pDC->Rectangle(rectl); // MULTI EPK 버튼 상자
		
	

		CPen pen2( PS_SOLID, 2, RGB(255,255,255));
		pDC->SelectObject( pen2 );
		pen.DeleteObject();

		CRect rects;
		if ( nLen1 == NULL )
		{
			rects.left = pline.x-75;
			rects.top = pline.y-60;
			rects.right = pline.x+480;
			rects.bottom = pline.y-30;
		}
		else if (_nSFMViewXSize == 1600 || REVERSE == 0) // 큰 역 (신호기 명 변경) 
		{
			rects.left = pline.x+42;
			rects.top = pline.y-60;
			rects.right = pline.x+350;
			rects.bottom = pline.y-30;
		} 
		else 
		{
			rects.left = pline.x-55;
			rects.top = pline.y-60;
			rects.right = pline.x+200;
			rects.bottom = pline.y-30;
		}
		if ( m_strStationName.Find("@StandardOfLC") > 0)
		{
			rects.right+=100;
		}
		else if ( m_strStationName == "AKHAURA" )
		{
			rects.left+=20;
			rects.right+=220;
		}

		pDC->SetTextColor(RGB(255,255,255) );
		pDC->Rectangle(rects);
		pDC->DrawText( "KT CONTROL", -1, rects, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

		CPen pen3( PS_SOLID, 1, RGB(255,255,255));
		pDC->SelectObject( pen3 );
		pen2.DeleteObject();

		if ( ( nLen1 == NULL ) && _nSFMViewXSize == 2560 )	
		{
			for (int i = 0; i < 2; i++) 
			{
				if ( m_strStationName == "AKHAURA" )
				{
					pDC->MoveTo(pline.x-55-1+i, pline.y-30-1);
					pDC->LineTo(pline.x-55-1+i, pline.y+69-i);
					
					pDC->MoveTo(pline.x-55-1, pline.y-30-1+i);
					pDC->LineTo(pline.x+700-i, pline.y-30-1+i);
				}
				else
				{
					pDC->MoveTo(pline.x-75-1+i, pline.y-30-1);
					pDC->LineTo(pline.x-75-1+i, pline.y+69-i);
					
					pDC->MoveTo(pline.x-75-1, pline.y-30-1+i);
					pDC->LineTo(pline.x+480-i, pline.y-30-1+i);
				}
			}
		}
		else if ( nLen1 == NULL )
		{
			for (int i = 0; i < 2; i++) 
			{
				pDC->MoveTo(pline.x-75-1+i, pline.y-30-1);
				pDC->LineTo(pline.x-75-1+i, pline.y+19-i);
				
				pDC->MoveTo(pline.x-75-1, pline.y-30-1+i);
				pDC->LineTo(pline.x+480-i, pline.y-30-1+i);
			}
		}
		else if (_nSFMViewXSize == 1600 || REVERSE == 0) 
		{														// 큰 역 (신호기 명 변경)
			for (int i = 0; i < 2; i++) 
			{
				pDC->MoveTo(pline.x-38-1+i, pline.y-30-1);
				pDC->LineTo(pline.x-38-1+i, pline.y+19-i);
			
				pDC->MoveTo(pline.x-38-1, pline.y-30-1+i);
				pDC->LineTo(pline.x+430-i, pline.y-30-1+i);
			}
		} 
		else if (_nSFMViewXSize == 1920 || REVERSE == 0) 
		{														// 큰 역 (신호기 명 변경)
			for (int i = 0; i < 2; i++) 
			{
				pDC->MoveTo(pline.x-55-1+i, pline.y-30-1);
				pDC->LineTo(pline.x-55-1+i, pline.y+19-i);
			
				pDC->MoveTo(pline.x-55-1, pline.y-30-1+i);
				if ( m_strStationName.Find("@StandardOfLC") > 0)
				{
					pDC->LineTo(pline.x+300-i, pline.y-30-1+i);
				}
				else
				{
					pDC->LineTo(pline.x+200-i, pline.y-30-1+i);
				}
			}

		}
		
		pDC->SelectObject( oldpen );
		pDC->SelectObject( oldbrush );
		pen.DeleteObject();
		brush.DeleteObject();
	}

	int	nFlagToMatchWithKT;
	BOOL bHasKeyorNot = FALSE;

	if(pDataHead != NULL)
	{
		if((str.Find("UEPK") == 0 || m_nID == 251) && bAreThereOnlyUepkAndDepk == TRUE)
		{
			bHasKeyorNot = pDataHead->nSysVar.bPKeyN;
			nFlagToMatchWithKT = 0;
		}
		else if((str.Find("DEPK") == 0 || m_nID == 252) && bAreThereOnlyUepkAndDepk == TRUE)
		{
			bHasKeyorNot = pDataHead->nSysVar.bPKeyS;
			nFlagToMatchWithKT = 1;
		}
		else if(str.Find("AEPK") == 0 || str.Find("G1") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyA;
			nFlagToMatchWithKT = 2;
		}
		else if(str.Find("BEPK") == 0 || str.Find("G2") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyB;
			nFlagToMatchWithKT = 3;
		}
		else if(str.Find("CEPK") == 0 || str.Find("G3") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyC;
			nFlagToMatchWithKT = 4;
		}
		else if(str.Find("DEPK") == 0 || str.Find("G4") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyD;
			nFlagToMatchWithKT = 5;
		}
		else if(str.Find("EEPK") == 0 || str.Find("G5") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyE;
			nFlagToMatchWithKT = 6;
		}
		else if(str.Find("FEPK") == 0 || str.Find("G6") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyF;
			nFlagToMatchWithKT = 7;
		}
		else if(str.Find("GEPK") == 0 || str.Find("G7") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVarExt.bPKeyG;
			nFlagToMatchWithKT = 8;
		}
		else if(str.Find("HEPK") == 0 || str.Find("G8") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVar.bPKeyN;
			nFlagToMatchWithKT = 9;
		}
		else if (str.Find("IEPK") == 0 || str.Find("G9") == 0 )
		{
			bHasKeyorNot = pDataHead->nSysVar.bPKeyS;
			nFlagToMatchWithKT = 10;
		}
	}
	else
		bHasKeyorNot = TRUE;

	if (!mRightColor) bmpno = 141;  // OFF
	else bmpno = 140;         // ON(LOCKED)
	int state = mRightColor;
	if(bHasKeyorNot == TRUE)
		mRightColor = 0;
	else
		mRightColor = MY_MOVE_OUT | MY_FLASHING ; //(단순 Flashing을 위해...)
	
	tbmp.SetBase( mRightColor );
	tbmp.SetWP(mPos);

	tbmp.Draw(bmpno);
	CPoint p = mPos;
	
	CFont font3;
	font3.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	pDC->SelectObject(&font3);
	font.DeleteObject();
	
	CRect rect(Pos.x-5, Pos.y-25, Pos.x+5, Pos.y-15);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	
	pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	
	CFont font2;
	font2.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	pDC->SelectObject(&font2);
	font3.DeleteObject();
	
	if (_BlinkDuty == FALSE && bHasKeyorNot == FALSE)
		p.y += MY_MOVE_OUT;
	if (!state) 
	{
		CRect rect2(p.x+20, p.y+7, p.x+25, p.y+12);
		pDC->SetTextColor(RGB(255,0,0) );
		pDC->DrawText( "OFF", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else 
	{
		CRect rect3(p.x-25, p.y+7, p.x-20, p.y+12);
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "ON", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	pDC->SelectObject( oldfont );
	font2.DeleteObject();
}

else if (m_nType == 24 && m_nID == 255)  // Nouse 버튼
{

 	CString str;
	char nLen;
	str  = m_szDrawName;
	strcpy(mName,"POS");
	nLen = str.Find(".");
#ifdef _LSD
#else
	ScrInfoSwitch *pPntInfo;
	for (int index=0; index<256; index++)
	{
		if (EtcScrInfo[index].str.Left(nLen) == str.Left(nLen) )
		{
			if (EtcScrInfo[index].type == OBJ_SWITCH) 
				break;
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	if (EtcScrInfo[index].m_pData != NULL) 
	{
		pPntInfo = (ScrInfoSwitch*)EtcScrInfo[index].m_pData;

		if (pPntInfo->NOUSED)
			mColor = 0;     // "ON"   -  Enable
		else
			mColor = 1;		// "OFF"   -  Disable 	
	}
#endif
	if (!mColor) bmpno = 141 ;
	else bmpno = 140 ;
	int state = mColor;
	mColor = 0;
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	tbmp.Draw(bmpno);
	CPoint p = mPos;

	tbmp.SetWP(p);
	tbmp.Draw(bmpno);

	CFont font;
	font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);
    CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	pDC->DrawText( m_szDrawName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );


	CFont font2;
	font2.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
    pDC->SelectObject(&font2);
	font.DeleteObject();

	if (!state) 
	{
		CRect rect2(p.x+20, p.y+7, p.x+25, p.y+12);
		pDC->SetTextColor(RGB(255,0,0) );
        pDC->DrawText( "OFF", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else {

		CRect rect3(p.x-25, p.y+7, p.x-20, p.y+12);
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "ON", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}

	pDC->SelectObject( oldfont );
	font2.DeleteObject();
}

else if (m_nType == 24 && m_nID == 238)  // ATB
{
	CString str  = mName;
    CPoint p = mPos;

	if (mColor) mColor = MY_RED;
	else mColor = 0 ;

	tbmp.SetBase( mColor );
	tbmp.SetWP(p);
	tbmp.Draw(182);

	CFont font;
	font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);

    CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
    pDC->DrawText( m_szDrawName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	pDC->SelectObject( oldfont );
	font.DeleteObject();
}

else if (m_nType == 24 )  // SLS, LOS
{
	CString str  = mName;
	if ( m_nID == 249 ) // LOS만 적용. 20131018
	{
#ifdef _LSD
#else
	ScrInfoTrack *pTrackInfo;
	for (int index=0; index<256; index++)
	{
		if (EtcScrInfo[index].str.Left(2) == str.Left(2) )
		{
			if (EtcScrInfo[index].type == OBJ_TRACK) 
				break;
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	if (EtcScrInfo[index].m_pData != NULL ) 
	{
		pTrackInfo = (ScrInfoTrack*)EtcScrInfo[index].m_pData;
		if (pTrackInfo->INHIBIT)
			mColor = 1;     // "OFF"   -  Gray
		else
			mColor = 0;		// "ON"    -  normal 
    }
#endif
	}
	
    CPoint p = mPos;
	if (m_nID > 248)
	{
		mColor = !mColor;
	}

	if (mColor) bmpno = 141;
	else bmpno = 140 ;
	int state = mColor;
	mColor = 0;
	tbmp.SetBase( mColor );

	tbmp.SetWP(p);
	tbmp.Draw(bmpno);

	CFont font;
	font.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);

	if (state) 
	{
		CRect rect2(p.x+20, p.y+7, p.x+25, p.y+12);
		pDC->SetTextColor(RGB(255,0,0) );
        pDC->DrawText( "OFF", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else {

		CRect rect3(p.x-25, p.y+7, p.x-20, p.y+12);
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "ON", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}

	CFont font2;

	font2.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	pDC->SelectObject(&font2);
	font.DeleteObject();

    CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	if (m_strStationName.Find("@StandardOf13") > 0)
	{
		str.Replace("COS","LOS");
	}
    pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	pDC->SelectObject( oldfont );
	font2.DeleteObject();
}


else if(m_nType == 29)		// 건널목
{
	CPoint p = mPos;
	CPoint pline = mPos;
	BmpDraw tbmp;
	tbmp.SetBase( 0 );
	int lWidth = mWidth;
	CString str = "" ;
	CString strBuffer = m_szDrawName;
	CString strRaw[5];
	int iFind$ = 0;
	int iRaw = 1;
	
	if ( strBuffer.Find('@') > 0 )
	{
		str += strBuffer.Left(strBuffer.Find('@'));
	}
	else
	{
		str += m_szDrawName ;
	}

	str.Replace('_',' ');
	
	for ( ; iRaw < 5; iRaw++)
	{
		if ( str.Find('$') < 0 )
		{
			strRaw[iRaw-1] = str;
			break;
		}
		else
		{
			strRaw[iRaw-1] = str.Left(str.Find('$'));
			str = str.Right(str.GetLength()-str.Find('$')-1);
		}
	}

	if (mColor)
	{
		for(int i = 0; i <(lWidth-1); i++)
		{
			pline.y += 5*i;
			tbmp.SetWP(pline);
			tbmp.OrDraw(34);
			
			pline.y -= 10*i+5;
			tbmp.SetWP(pline);
			tbmp.OrDraw(34);
			pline = mPos;
		}
	}
	else
	{
		for(int i = 0; i <(lWidth-1); i++)
		{
			pline.y += 5*i;
			tbmp.SetWP(pline);
			tbmp.OrDraw(37);
			
			pline.y -= 10*i+5;
			tbmp.SetWP(pline);
			tbmp.OrDraw(37);
			pline = mPos;
		}
	}
	p.y += mWidth*5;
	tbmp.SetWP( p );
	if (mColor)
	{
		tbmp.Draw( 49 );
        if ( iRaw == 1 && strRaw[0].Find("LC") == 0 )
	        str += " OPENED";
	}
	else
	{
		tbmp.Draw( 50 );
        if ( iRaw == 1 && strRaw[0].Find("LC") == 0 )
	        str += " CLOSED";
	}
	p.y -= mWidth*10;
	tbmp.SetWP( p );
	if (mColor)
	{
		tbmp.Draw( 49 );
	}
	else
	{
		tbmp.Draw( 50 );
	}

	CFont font;
	font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);

	pDC->SetTextColor(RGB(192,192,192));

	for ( int iCount = 0; iCount < iRaw; iCount++)
	{
		CRect rect(p.x-5, p.y-30-14*(iRaw-iCount-1), p.x+5, p.y-20-14*(iRaw-iCount-1));
		pDC->DrawText( strRaw[iCount], -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}

	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	if ( strBuffer.Find('@') > 0 )
	{
		if ( iRaw == 1 && strRaw[0].Find("LC") == 0 )
			str = strBuffer.Left(strBuffer.Find('@'));
		else
			str = strRaw[0];
	}
	else
	{
        if ( iRaw == 1 && strRaw[0].Find("LC") == 0 )
			str = m_szDrawName ;
		else
			str = strRaw[0];
	}
	str += "K";
	pline.y += mWidth*5;
	CRect rect2(pline.x-5, pline.y+17, pline.x+5, pline.y+27);
    pDC->DrawText( str, -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

	if (mRightColor) bmpno = 140; //on
	else bmpno = 141 ; // off
	int state = mRightColor;
	mRightColor = 0;
	tbmp.SetBase( mRightColor );
    pline.y += 40;
	tbmp.SetWP(pline);
	tbmp.Draw(bmpno);

	CFont font2;
	font2.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
    pDC->SelectObject(&font2);
  	font.DeleteObject();

	if (!state) 
	{
		CRect rect3(pline.x+20, pline.y+7, pline.x+25, pline.y+12);
		pDC->SetTextColor(RGB(255,0,0) );
        pDC->DrawText( "OFF", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else 
	{
		CRect rect4(pline.x-25, pline.y+7, pline.x-20, pline.y+12);
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "ON", -1, rect4, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	pDC->SelectObject( oldfont );
}
else if (m_nType == 30)  // 건널목 CAll
{
	
	CPoint p = mPos;
	BmpDraw tbmp;
	tbmp.SetBase( mColor );
	int lWidth = mWidth;
	tbmp.SetWP(p);
	tbmp.Draw(157);
}
else if (m_nType == 25)  // NIGHT / DAY
{
 	if (mColor) bmpno = 138;
	else bmpno = 139 ;
	int state = mColor;
	mColor = 0;
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	tbmp.Draw(bmpno);
	CPoint p = mPos;

	CFont font;
	font.CreateFont(14,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);
	CRect rect(Pos.x-30, Pos.y-20, Pos.x-20, Pos.y-10);
	if(mRightColor)
	{
		pDC->SetTextColor(RGB(0,255,0));
	    pDC->DrawText( "NIGHT", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		CRect rect3(Pos.x-7, Pos.y-20, Pos.x+13, Pos.y-10);
		pDC->SetTextColor(RGB(128,128,128));
	    pDC->DrawText( "/", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		CRect rect4(Pos.x+20, Pos.y-20, Pos.x+30, Pos.y-10);
        pDC->DrawText( "DAY", -1, rect4, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

	}
	else
	{
		pDC->SetTextColor(RGB(128,128,128));
        pDC->DrawText( "NIGHT", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		CRect rect3(Pos.x-7, Pos.y-20, Pos.x+13, Pos.y-10);
	    pDC->DrawText( "/", -1, rect3, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		pDC->SetTextColor(RGB(255,255,128));
		CRect rect4(Pos.x+20, Pos.y-20, Pos.x+30, Pos.y-10);
        pDC->DrawText( "DAY", -1, rect4, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}

	CFont font2;
	font2.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
    pDC->SelectObject(&font2);
	font.DeleteObject();

	if (state) 
	{
		CRect rect2(p.x-28, p.y+7, p.x-25, p.y+12);
		pDC->SetTextColor(RGB(128,128,128) );
		pDC->DrawText( "NIGHT", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else 
	{
		CRect rect2(p.x+20, p.y+7, p.x+25, p.y+12);
		pDC->SetTextColor(RGB(255,255,128) );
        pDC->DrawText( "DAY", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

	}
	pDC->SelectObject( oldfont );
	font2.DeleteObject();
}

else if (m_nType == 23) // PAS
{
    if (!mColor) bmpno = 137;
	else  bmpno = 136;
	int state = mColor;
	mColor = 0;
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	tbmp.Draw(bmpno);

	CFont font;
	font.CreateFont(18,8,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
	CFont *oldfont = pDC->SelectObject(&font);
	CPoint p = mPos;
	CRect rect(p.x-5, p.y-35, p.x+5, p.y-30);
	if(m_bBTNClick)
	{
		pDC->SetTextColor(RGB(0,255,0));
	}
	else
	{
		pDC->SetTextColor(RGB(255,255,255));
	}
	pDC->DrawText( m_szDrawName, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

	CFont font2;
	font2.CreateFont(12,7,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
    pDC->SelectObject(&font2);
	font.DeleteObject();

	CRect rect2(p.x-5, p.y+25, p.x+5, p.y+35);
	if (!state) {
		pDC->SetTextColor(RGB(255,0,0) );
        pDC->DrawText( "OUT", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	else {
		pDC->SetTextColor(RGB(128,255,128) );
		pDC->DrawText( "IN", -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
	}
	pDC->SelectObject( oldfont );
	font2.DeleteObject();

	CBrush brush( RGB(0,0,0) );
    CBrush *oldbrush = pDC->SelectObject(&brush);

	CPen pen( PS_SOLID, 2, RGB(128,128,128));
    CPen *oldpen = pDC->SelectObject(&pen);
	pDC->SelectObject(pen);

	CRect rect3(p.x+28, p.y-22, p.x+262, p.y+19);
	pDC->Rectangle(rect3); // PAS 버튼 상자

	CPen pen2( PS_SOLID, 1, RGB(255,255,255));
    pDC->SelectObject( pen2 );
	pen.DeleteObject();

    for (int i = 0; i < 2; i++) 
	{
		pDC->MoveTo(p.x+28-1+i, p.y-22-1);
	    pDC->LineTo(p.x+28-1+i, p.y+19-i);

        pDC->MoveTo(p.x+28-1, p.y-22-1+i);
        pDC->LineTo(p.x+262-i, p.y-22-1+i);
	}

    pDC->SelectObject( oldpen );
    pDC->SelectObject( oldbrush );
	pen.DeleteObject();
	brush.DeleteObject();
	

}

else if (m_nType == 21 && !( m_nType == 21 && ( m_nID == 253 || m_nID == 254 || m_nID == 268 || m_nID == 269 || m_nID == 282 || m_nID == 283 || m_nID == 285)))  // OPtion button
{
    int no;
    if(m_nID == 244)
	{
		Pos.x += 105;
		tbmp.SetWP(Pos);
		if ( m_strStationName.Find ( "BYPA",0 ) >= 0 ) {
			tbmp.Draw(189);
			tbmp.Draw(190);
		} else {
			tbmp.Draw(103);
		}
		Pos.x -= 105;
	}

    if(m_nID == 242 && m_strStationName == "AKHAURA")
	{
  	tbmp.SetBase( mRightColor );
		Pos.y += 35;
		Pos.x += 3;
		tbmp.SetWP(Pos);
		tbmp.Draw(159);
		Pos.y -= 35;
		Pos.x -= 3;
	}


	switch (m_nID) 
	{
	case 244 : no = 0;
        break;
	case 245 : no = 1;
        break;
	case 242 : no = 2;
        break;
	case 230 : no = 3;
	    break;
	case 227 : no = 4;
	    break;
	case 228 : no = 5;
	    break;
	case 246 : no = 6;
        break;
	case 253 : no = 33;
        break;
	case 254 : no = 34;
        break;
	}
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	bmpno = 0x7F + no;
	tbmp.Draw(bmpno);
}

else if (m_nType == 20 || ( m_nType == 21 && ( m_nID == 253 || m_nID == 254 || m_nID == 268 || m_nID == 269 || m_nID == 282 || m_nID == 283 || m_nID == 285)))
{
		int dx = int( _nDotPerCell );
		Pos.x -= dx * (mWidth-1) / 2;
	    tbmp.SetBase( mColor );
		tbmp.SetWP(Pos);
		tbmp.Draw(bmpno);
		Pos.x += dx;
		
		for (int i=1; i<( mWidth - 1 );i++) 
		{
			tbmp.SetWP(Pos);
			tbmp.Draw(bmpno+1);
			Pos.x += dx;
		}
		tbmp.SetWP(Pos);
		tbmp.Draw(bmpno+2);

		CFont font;
		if (m_nID == 229 || m_nID == 301)
		{
			font.CreateFont(18,6,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
		}
		else
		{
			font.CreateFont(18,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
		}
		CFont *oldfont = pDC->SelectObject(&font);
        
		pDC->SetBkMode(TRANSPARENT);
	    if(m_bBTNClick)
		{
			pDC->SetTextColor(RGB(0,255,0));
		}
		else
		{
			pDC->SetTextColor(RGB(0,0,0));
		}
		CPoint p = mPos;
		p.y--;
		if ( m_IsPress ) 
		{
			p.x++;
			p.y++;
		}
		CRect rect(p.x-5, p.y-5, p.x+5, p.y+5);

		CString str = m_szDrawName;
		if ( m_nType == 21 && ( m_nID == 268 || m_nID == 269 || m_nID == 283 || m_nID == 285) && bSuperBlock == TRUE )
		{
			str = "S-BLOCK";
		}
		short l = strlen(str);
		short n = str.Find( '_' ); 
		for (;n > 0 ; n = str.Find( '_' ))
		{
			CString Temp = str.Right(l-n-1);
			str = str.Left( n );
			str += "  ";
			str += Temp;
			l++;
		}	

		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
     	pDC->SelectObject( oldfont );
		font.DeleteObject();
	}
}
// m_nType 19이상 끝
else if (m_nType == 36)  // 2012.08.27 Axle Counter
{
	CPoint p = mPos;
	BmpDraw tbmp;
	tbmp.SetBase( mColor );
	int lWidth = mWidth;
	tbmp.SetWP(p);

	RECT r;	
	CFont font;
	CString strDrawName;
	CString strReferBlock;
	CString strAXLSyncBlock;
	CString strBufferName = m_szDrawName;
	int  iFindForAt;
	int  iFindForAnd;
	int  iLengthOfBufferName = strBufferName.GetLength();
	BOOL bAXLDST = FALSE;
	BOOL bAXLOCC = FALSE;
	BOOL bSAXLDST = FALSE;
	BOOL bSAXLOCC = FALSE;
	BOOL bSAXLACTIN = FALSE;
	
	iFindForAt = strBufferName.Find('@');
	
	if ( iFindForAt )
	{
		strDrawName = strBufferName.Left(iFindForAt);
		strReferBlock = strBufferName.Right(iLengthOfBufferName-iFindForAt-1);
		
		iFindForAnd = strReferBlock.Find('&');
		
		if ( iFindForAnd == 2 || iFindForAnd == 3 )
		{
			strAXLSyncBlock = strReferBlock.Right(iLengthOfBufferName-iFindForAt-iFindForAnd-2);
			strReferBlock = strReferBlock.Left(iFindForAnd);
		}
	}
	else
	{
		strDrawName = strBufferName;
	}

	if ( iFindForAt <= 3 )
		font.CreateFont(18,6,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));
	else
		font.CreateFont(16,5,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));

	CFont *oldfont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	
	::SetRect(&r,Pos.x-38, Pos.y+4, Pos.x +38, Pos.y+4);

	ScrInfoBlock *pReferBlock = NULL;
	ScrInfoBlock *pAXLSyncBlock = NULL;
	BOOL bFindReferBlock = FALSE;
	BOOL bFindAXLSyncBlock = FALSE;
	if ( strAXLSyncBlock == "" )
	{
		bFindAXLSyncBlock = TRUE;
	}

	for (int i=0; i<SignalObject.GetSize(); i++ ) 
	{
		TScrobj *pObj = (TScrobj *)SignalObject.GetAt( i );
		if (strcmp(pObj->mName, strReferBlock) == NULL)
		{
			pReferBlock = (ScrInfoBlock*)pObj->m_pData;
			bFindReferBlock = TRUE;
		}
		if (strcmp(pObj->mName, strAXLSyncBlock) == NULL)
		{
			pAXLSyncBlock = (ScrInfoBlock*)pObj->m_pData;
			bFindAXLSyncBlock = TRUE;
		}
		if ( bFindReferBlock == TRUE && bFindAXLSyncBlock == TRUE )
		{
			break;
		}
	}

#ifdef _LSD
	tbmp.Draw(192);
	pDC->SetTextColor( RGB(0,0,0) );
#else
	if ( strAXLSyncBlock != "" )
	{
		bAXLDST = pReferBlock->AXLDST & pAXLSyncBlock->AXLDST;
		bAXLOCC = pReferBlock->AXLOCC & pAXLSyncBlock->AXLOCC;
		bSAXLDST = pReferBlock->SAXLDST & pAXLSyncBlock->SAXLDST;
		bSAXLOCC = pReferBlock->SAXLOCC & pAXLSyncBlock->SAXLOCC;
		bSAXLACTIN = pReferBlock->SAXLACTIN | pAXLSyncBlock->SAXLACTIN;
	}
	else
	{
		bAXLDST = pReferBlock->AXLDST;
		bAXLOCC = pReferBlock->AXLOCC;
		bSAXLDST = pReferBlock->SAXLDST;
		bSAXLOCC = pReferBlock->SAXLOCC;
		bSAXLACTIN = pReferBlock->SAXLACTIN;
	}

	if ( (!bSAXLACTIN && !bAXLDST) || (bSAXLACTIN && !bSAXLDST) )
	{
		if (!_BlinkDuty)
		{
			tbmp.Draw(191);
			pDC->SetTextColor( RGB(255,255,255) );
		}
		else
		{
			tbmp.Draw(192);
			pDC->SetTextColor( RGB(0,0,0) );
		}
	}
	else if ( (!bSAXLACTIN && !bAXLOCC) || (bSAXLACTIN && !bSAXLOCC) )
	{
		tbmp.Draw(192);
		pDC->SetTextColor( RGB(0,0,0) );
	}
	else
	{
		tbmp.Draw(191);
		pDC->SetTextColor( RGB(255,255,255) );
	}

	if ( bSAXLACTIN )
	{
		font.DeleteObject();
		strDrawName = "S-BLK";
		font.CreateFont(16,4,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("arial"));
		pDC->SelectObject(&font);
	}
#endif

	MyDrawText( strDrawName, -1, &r, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE, MY_GRAY);

	font.DeleteObject();
	
	pDC->SelectObject(oldfont);
	
}
else if (m_nType == 6) // 조작반
{
	
	BmpDraw tbmp;
	CPoint p = mPos;
	tbmp.SetWP(Pos);
	if (p.y > 512)
	{
		tbmp.Draw(42);
	}
	else
	{
		tbmp.Draw(43);
	}
	
	CFont font;
	font.CreateFont(19,9,0,0,FW_HEAVY,TRUE,TRUE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman Italic"));
	CFont *oldfont = pDC->SelectObject(&font);
	
	
    pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(0,0,0));
	pDC->SetTextColor( RGB(255,255,0) );
	
	CRect rect;
	
	if (strcmp(m_szDrawName, "#Type2") == 0)
	{
		CRect makedRect(p.x+100,p.y-10, p.x+230, p.y+10);
		
		rect = makedRect;
	}
	else
	{
		CRect makedRect(p.x+30,p.y-10, p.x+160, p.y+10);
		
		rect = makedRect;
	}
	
	
	//	CRect rect(p.x+30,p.y-10, p.x+120, p.y+10);
	pDC->DrawText( m_strUserID, -1, rect , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE | DT_EXPANDTABS | DT_TABSTOP);	
	pDC->SelectObject( oldfont );
	font.DeleteObject();
	
}
else if (m_nType == 37) // Road warner
{
	
	BmpDraw tbmp;
	CPoint p = mPos;
	p.y += 1;
	tbmp.SetWP(p);
	if (strcmp(m_szDrawName, "#Type2") == 0)
	{
		tbmp.Draw(196);
	}
	else
	{
		tbmp.Draw(195);
	}
	
}
else if (m_nType == 38) // Gate Lodge
{
	
	BmpDraw tbmp;
	CPoint p = mPos;
	tbmp.SetWP(Pos);
	tbmp.Draw(197);
	
}
else if (m_nType == 39) // X mark
{
	CFont font;
	font.CreateFont(40,15,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Arial"));
	CFont *oldfont = pDC->SelectObject(&font);
	
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(255,0,0) );
	CPoint p = mPos;
	CRect rect(p.x-5, p.y-5, p.x+5, p.y+5);
	pDC->DrawText( "X", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
   	pDC->SelectObject( oldfont );
	font.DeleteObject();
}
else if(m_nType == 34)		// 다리 (교량)
	{

	CPoint p = mPos;
		CPoint pline = mPos;
		CRect rect;
//		BmpDraw tbmp;
//		tbmp.SetBase( 0 );
		m_iWith = atoi(m_szDrawName);
		int lWidth = m_iWith;
// 		lWidth = 17; // 강제로 다리길이를 지정했다.
// 		CString str = "" ;
// 		str += m_szDrawName ;
 	    CPen pen( PS_SOLID, 2, RGB(255,255,255));
        CBrush brush( RGB(137,144,141) );
        CPen *oldpen = pDC->SelectObject(&pen);
        CBrush *oldbrush = pDC->SelectObject(&brush);

		// 교량 윗부분
		pDC->MoveTo(pline.x-2,pline.y-20);
		pDC->LineTo(pline.x,pline.y-15);
		pDC->LineTo(pline.x+(lWidth*5),pline.y-15);
		pDC->LineTo(pline.x+( lWidth*5)+2,pline.y-20);
		// 교량 아래부분		
		pDC->MoveTo(pline.x-2,pline.y+20);
		pDC->LineTo(pline.x,pline.y+15);
		pDC->LineTo(pline.x+(lWidth*5),pline.y+15);
		pDC->LineTo(pline.x+(lWidth*5)+2,pline.y+20);

		rect.left = pline.x;
		rect.top = pline.y-35;
		rect.right = pline.x + (lWidth*5);
		rect.bottom = pline.y-15;
// 		pDC->DrawText(m_szDrawName,rect,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);

    pDC->SelectObject( oldpen );
    pDC->SelectObject( oldbrush );

}

else if (m_nType == 9) // Platform - 플랫폼 
{
	CPoint p = mPos;

 	CPen pen( PS_SOLID, 2, RGB(255,255,255));
    CBrush brush( RGB(137,144,141) );
    CPen *oldpen = pDC->SelectObject(&pen);
    CBrush *oldbrush = pDC->SelectObject(&brush);
	CRect rect(p.x-mWidth/4, p.y-mHeight/2, p.x+mWidth/4, p.y+mHeight/2);
	pDC->Rectangle(rect);
    pDC->SelectObject( oldpen );
    pDC->SelectObject( oldbrush );

}



else if (m_nType == 4) // Lg logo
{
	BmpDraw tbmp;
	tbmp.SetWP(Pos);
	tbmp.Draw(104); //"LG Screen Moudle V2.0"

	CFont font;
	font.CreateFont(15,7,0,0,FW_BLACK,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,_T("Arial"));
	CFont *oldfont = pDC->SelectObject(&font);
	pDC->SetTextColor(RGB(255,255,128) );
	CPoint p = mPos;
	CRect rect(p.x+80, p.y-5, p.x+90, p.y+5);
//	pDC->DrawText( "LS Screen Module", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
   	pDC->SelectObject( oldfont );
	font.DeleteObject();

}
else if((m_nType == 11) || (m_nType == 12))	// warner signal
{
	BmpDraw tbmp;
	tbmp.SetBase( mColor );
	tbmp.SetWP(Pos);
	if(m_nType == 11)
	{
		tbmp.Draw(164);	
	}
	else
	{
		tbmp.Draw(165);	
	}
}
	
else if (m_nType == 7) // TEXT
{
	CString strbuf = mName;
	int iWhereIsSharp = strbuf.Find("#");
	CString strFontAngle = strbuf.Right(strlen(strbuf)-iWhereIsSharp-1);
	float floatFontAngle = (float)atof(strFontAngle)*10;
	int iFontAngle = (int)floatFontAngle;
	
	if ( iWhereIsSharp < 0 )
		iFontAngle = 0;
	else
		strbuf = strbuf.Left(iWhereIsSharp);

	int iWhereIsAt = strbuf.Find("@");
	CString str = strbuf.Left(iWhereIsAt);

	int iFontHeight;
	CString strFontHeight = strbuf.Right(strlen(strbuf)-iWhereIsAt-1);
	CString strFontColor;
	int iWhereIs2ndAt = strFontHeight.Find("@");
	if ( iWhereIs2ndAt > 0 )
	{
		iFontHeight = _ttoi(strFontHeight.Left(iWhereIs2ndAt));
		strFontColor = strFontHeight.Right(strlen(strFontHeight)-iWhereIs2ndAt-1);
	}
	else
	{
		iFontHeight = _ttoi(strFontHeight);
	}

	if ( iWhereIsAt < 0 )
	{
		str = strbuf;
		iFontHeight = 18;
	}

	int iFontWidth = iFontHeight / 3 + 1;

	CFont font;
	font.CreateFont(iFontHeight,iFontWidth,iFontAngle,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
	CFont *oldfont = pDC->SelectObject(&font);

	short l = strlen(str);
    short n = str.Find( '_' ); 
	for (;n > 0 ; n = str.Find( '_' ))
	{
		CString Temp = str.Right(l-n-1);
		str = str.Left( n );
		str += "  ";
		str += Temp;
		l++;
	}	


	pDC->SetBkMode(TRANSPARENT);
	
	if ( strFontColor == "W" )
		pDC->SetTextColor(RGB(255,255,255) );
	else
		pDC->SetTextColor(RGB(128,128,128) );

	CPoint p = mPos;
	CRect rect(p.x-5, p.y-5, p.x+5, p.y+5);
	
	pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
   	pDC->SelectObject( oldfont );
	font.DeleteObject();
}
else if (m_nType == 5) //Counter
{
	pDC->SetBkMode(TRANSPARENT);
	
	BmpDraw tbmp;
	tbmp.SetBase( 0 );
	tbmp.SetWP(mPos);
	tbmp.Draw(120);	

	CFont font;
	CFont font2;
	CFont font3;
	CString strForAX = mName;
	int iWhereIsAt = strForAX.Find("@");
	CString strRealAXName = strForAX.Left(iWhereIsAt);
	strForAX.Replace('_',' ');

	if (strForAX == "AX RST")
		font.CreateFont(19,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
	else
		font.CreateFont(19,9,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));

	CFont *oldfont = pDC->SelectObject(&font);
        
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor( RGB(255,255,255) );
	CPoint p = mPos;
	CRect rect2(p.x-4, p.y-20, p.x+6, p.y-10);
	if ( iWhereIsAt > 0 )
	{
		pDC->DrawText( strRealAXName, -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
	}
	else
	{
		pDC->DrawText( strForAX, -1, rect2, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );				
	}


	CRect rect(p.x-28, p.y+1, p.x+34, p.y+17);
	unsigned short nCount = 0;

	if (m_pData) 
	{
		nCount = (unsigned short) pDataHead->Counter[ m_nID ];
		BYTE n1 = nCount % 256;
		BYTE n2 = nCount / 256;

		nCount = n1*256 + n2;

	}
	CString str;
	str.Format( "%04d", nCount );
	CString temp = str.Mid(0,1);
	temp += " ";
	temp += str.Mid(1,1);
	temp += " ";
	temp += str.Mid(2,1);
	temp += " ";
	temp += str.Mid(3,1);
	str = temp;

	font2.CreateFont(19,9,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
	pDC->SelectObject(&font2);

 	pDC->SetTextColor( RGB(255,255,128) );
	pDC->DrawText( str, -1, rect , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE | DT_EXPANDTABS | DT_TABSTOP);	

	if ( ( strRealAXName.Find("AX") == 0 ) && strForAX.Find('@') > 0 )
	{
		if ( strForAX.Right(1) == "1" )
		{
			p.x-=40;
			p.y-=57;
			tbmp.SetWP(p);
			CRect rect3 = rect;
			rect3.top-=53;
			rect3.bottom-=53;
			rect3.left-=12;
			rect3.right = rect3.left + 85;
			font3.CreateFont(19,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
			pDC->SelectObject(&font3);
			
			tbmp.Draw(193);
			pDC->SetTextColor( RGB(255,255,255) );
			str = "AX RESET";
			pDC->DrawText( str, -1, rect3 , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE | DT_EXPANDTABS | DT_TABSTOP);	
		}
		else if ( strForAX.Right(1) == "2")
		{
			p.x-=30;
			p.y-=57;
			tbmp.SetWP(p);
			CRect rect3 = rect;
			rect3.top-=53;
			rect3.bottom-=53;
			rect3.right = rect3.left + 145;
			
			tbmp.Draw(194);
			pDC->SetTextColor( RGB(255,255,255) );
			str = "AX RESET";
			pDC->DrawText( str, -1, rect3 , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE | DT_EXPANDTABS | DT_TABSTOP);	
		}
	}

	pDC->SelectObject( oldfont );
	font.DeleteObject();

}


else if(m_nType == 1)	// 역명
{

	COLORREF nColor = RGB(0,128,0);
	if (m_pData) 
	{
		DataHeaderType  *pDataHead = (DataHeaderType*)m_pData;			
		SystemStatusType *pSysInfo = &pDataHead->nSysVar;
		//		if ((pSysInfo->bLcc1Online || pSysInfo->bLcc2Online) && (m_IsPress == 0)) 
		if (/*(pDataHead->LCCStatus.bLCC1Alive || pDataHead->LCCStatus.bLCC2Alive ) && */(m_IsPress == 0)) 
		{
			nColor = RGB(255, 0, 0);
			m_IsPress = 1;
		}
		else 
		{
			m_IsPress = 0;
		}
	}
	CPoint p = mPos;
    pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(0,0,0));
	CFont font;
	font.CreateFont(45,21,0,0,FW_BLACK,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));

	CFont *oldfont = pDC->SelectObject(&font);

    CRect rect(p.x-225, p.y-25, p.x+240, p.y+25); 
        
	CPen pen( PS_SOLID, 4, RGB(100,100,100));
	CBrush brush( RGB(0,0,0) );
    CPen *oldpen = pDC->SelectObject(&pen);
    CBrush *oldbrush = pDC->SelectObject(&brush);

	pDC->Rectangle(rect);

	CPen pen2( PS_SOLID, 1, RGB(255,255,255));
    pDC->SelectObject( pen2 );
	pen.DeleteObject();

	for (int i = 0; i < 4; i++) 
	{
		pDC->MoveTo(p.x-227+i, p.y-27);
	    pDC->LineTo(p.x-227+i, p.y+26-i);

		pDC->MoveTo(p.x-226, p.y-27+i);
		pDC->LineTo(p.x+241-i, p.y-27+i);
	}

	CString str = m_szDrawName;
	m_strStationName = str;

	short l = strlen(str);
    short n = str.Find( '_' ); 
    if (n > 0) 
	{
		CString Temp = str.Right(l-n-1);
		str = str.Left( n );
		str += "  ";
		str += Temp;
	}
	short k = str.Find('@');
	if (k > 0)
		str = str.Left(k);


	pDC->SetTextColor( nColor );

    CRect rect2(p.x-10, p.y-25, p.x+10, p.y+25); 

	pDC->DrawText(str, -1, rect2 , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE );	

	pDC->SelectObject( oldfont );
    pDC->SelectObject( oldpen );
    pDC->SelectObject( oldbrush );
	font.DeleteObject();
	pen2.DeleteObject();
	brush.DeleteObject();
    BmpDraw tbmp;
	tbmp.Draw(1);
}


else if (m_nType == 8) // 경유역
{
	COLORREF nColor = RGB(255,255,255);

	CPoint p = mPos;
    pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(0,0,0));
	CFont font;
	font.CreateFont(14,7,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
	CFont *oldfont = pDC->SelectObject(&font);

    CRect rect(p.x-40, p.y-15, p.x+40, p.y+15); 

    CPen pen( PS_SOLID, 1, nColor);
	CBrush brush( RGB(0,0,0) );
    CPen *oldpen = pDC->SelectObject(&pen);
    CBrush *oldbrush = pDC->SelectObject(&brush);
	
    pDC->Rectangle(rect);
	pDC->SetTextColor( nColor );
	pDC->DrawText(mName, -1, rect , DT_NOCLIP | DT_CENTER |DT_VCENTER | DT_SINGLELINE );	

	pDC->SelectObject( oldfont );
    pDC->SelectObject( oldpen );
    pDC->SelectObject( oldbrush );
	font.DeleteObject();
}

else if((m_nType == 3) || (m_nType == 14) || ((m_nType == 32) && ((m_nID == 272) || (m_nID == 273) || (m_nID == 276) || (m_nID == 277))))	// 인접 역명 우 ( 3 : Tokenless, 14 : Token )
{
	mColor = 0;
    BmpDraw tbmp;
	CFont font;
	CString str = mName, sName1, sName2;
	CPoint p = mPos;	
	short l = strlen(str);
    short n = str.Find( '_' ); 
	CString strBlockClearButtonName = m_szDrawName;

	if ( m_strStationName.Find ( "BYPA",0 ) >= 0 && str.Find ( "PAGH",0) >=0 ) 
	{
		if (!REVERSE)
		{
			sName1 = "B3";
			sName2 = "B4";
		}
		else
		{
			sName1 = "B1";
			sName2 = "B2";
		}
	} 
	else 
	{
		if (!REVERSE)
		{
			sName1 = "B1";
			sName2 = "B2";
		}
		else
		{
			sName1 = "B3";
			sName2 = "B4";
		}
	}

	if ( str == "BCB11" || str == "BCB12" )
	{
		sName1 = "B11";
		sName2 = "B12";
	}
	
/*
	if (str.Find("BYPASS", 0) > 0)
	{
		sName1 = "B11";
		sName2 = "B12";
	}
*/
#ifdef _LSD
#else
	ScrInfoBlock *pBlockInfo;
	ScrInfoBlock *pBlockInfo2;

	for (int index=0; index<256; index++) // TGB Block 상태
	{
		if (EtcScrInfo[index].str == sName1 )
		{
			if (EtcScrInfo[index].type == OBJ_SIGNAL) 
			{
				pBlockInfo = (ScrInfoBlock*)EtcScrInfo[index].m_pData;
				break;
			}
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	for (index=0; index<256; index++) // TCB Block 상태
	{
		if (EtcScrInfo[index].str == sName2 )
		{
			if (EtcScrInfo[index].type == OBJ_SIGNAL) 
			{
				pBlockInfo2 = (ScrInfoBlock*)EtcScrInfo[index].m_pData;
				break;
			}
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	if ((EtcScrInfo[index].m_pData != NULL) && (m_nType != 3))
	{
// 		if (pBlockInfo->RBGPR || pBlockInfo2->RBCPR) // TGB, TCB 블락 사용시 Red
// 			mColor = MY_RED;     // "OFF"   -  Gray
		if (pBlockInfo->RBGPR && ( strBlockClearButtonName == "BCB3" || strBlockClearButtonName == "BCB11" ))
			mColor = MY_RED;
		else if ( pBlockInfo2->RBCPR && ( strBlockClearButtonName == "BCB4" || strBlockClearButtonName == "BCB12" )) // TGB, TCB 블락 사용시 Red
			mColor = MY_RED;     // "OFF"   -  Gray
		else
			mColor = 0;		// "ON"    -  normal 
		
		if (( pBlockInfo->BLKRSTREQIN || pBlockInfo->BLKRSTREQ ) && ( strBlockClearButtonName == "BCB3" || strBlockClearButtonName == "BCB11" ))
		{
			if((pBlockInfo->BLKRSTREQIN && pBlockInfo->BLKRSTACC) || (pBlockInfo->BLKRSTREQ && pBlockInfo->BLKRSTACCIN))
			{
				mColor = MY_FYELLOW;
			}
			else
			{
				mColor = MY_RED;
				mColor|=0x1000;
			}
		}
		else if (( pBlockInfo2->BLKRSTREQIN || pBlockInfo2->BLKRSTREQ ) && ( strBlockClearButtonName == "BCB4" || strBlockClearButtonName == "BCB12" )) // TGB, TCB 블락 사용시 Red
		{
			if((pBlockInfo2->BLKRSTREQIN && pBlockInfo2->BLKRSTACC) || (pBlockInfo2->BLKRSTREQ && pBlockInfo2->BLKRSTACCIN))
			{
				mColor = MY_FYELLOW;
			}
			else
			{
				mColor = MY_RED;
				mColor|=0x1000;
			}
		}
    }
/////////////////////// 2012.08.27 Axle Counter로 표시를 대체함.
#endif
//    if (n > 0) // 역명 띄어쓰기
	for (;n > 0 ; n = str.Find( '_' ))
	{
		CString Temp = str.Right(l-n-1);
		str = str.Left( n );
		str += "  ";
		str += Temp;
		l++;
	}

    if (!m_nID) // 무정보 화살표
	{
		font.CreateFont(15,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		CFont *oldfont = pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);

		pDC->SetTextColor( RGB(192,192,192) );
		tbmp.SetWP(Pos);

		tbmp.SetBase(0);
		tbmp.Draw(176);
		p.x -=CELLSIZE_X*2;
		p.y +=CELLSIZE_Y*2;
		CRect rect(p.x-10, p.y-10, p.x+10, p.y); 		
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		pDC->SelectObject( oldfont );

	}
	else if (m_nType == 32)  // Block 버튼
	{
		Pos.y+=3;
		tbmp.SetBase( mColor );
		tbmp.SetWP(Pos);
		tbmp.Draw(182);

		CFont fontt;
		fontt.CreateFont(10,5,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
		CFont *oldfont = pDC->SelectObject(&fontt);
		
		CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
		if(m_bBTNClick)
		{
			pDC->SetTextColor(RGB(0,255,0));
		}
		else
		{
			pDC->SetTextColor(RGB(255,255,255));
		}
		if(strBlockClearButtonName == "BCB3" || strBlockClearButtonName == "BCB11")
			pDC->DrawText( "BGR", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		else if (strBlockClearButtonName == "BCB4" || strBlockClearButtonName == "BCB12")
			pDC->DrawText( "BCR", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		pDC->SelectObject( oldfont );
		fontt.DeleteObject();
	}
	else // Token, Tokenless화살표
	{
		if ( m_pData && m_nType == 3 ) 
		{
			if (!REVERSE)
			{
				if (GetBitLogic(m_pData, m_nFlashID))
				{
					mColor = MY_RED | 0x1000;    // Modem Down = 1 일때
				}
			}
			else
			{
//				CString str = m_szDrawName;
				if ( m_strStationName.Find ( "BYPA",0 ) >= 0 && str.Find ( "PAGH",0) >=0 ) {
					if (GetBitLogic(m_pData, 32869))
					{
						mColor = MY_RED | 0x1000;    // Modem Up = 1 일때
					}
				} else {
					if (GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = MY_RED | 0x1000;    // Modem Up = 1 일때
					}
				}
			}
		}
		CFont font2;

//		CString strMsg;
//		strMsg.Format("U:%d,D:%d",!GetBitLogic(m_pData, 32868),!GetBitLogic(m_pData, 32869));

		font2.CreateFont(19,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		CFont *oldfont = pDC->SelectObject(&font2);
		
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor( RGB(255,255,255) );

		tbmp.SetBase( mColor );
		tbmp.SetWP(Pos);
		tbmp.Draw(82);
		tbmp.Draw(162);

		p.x -=CELLSIZE_X*1;
		CRect rect(p.x+35-5*l, p.y-20, p.x+45-5*l, p.y-10); // 인접 역명 위치		
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

		font.CreateFont(18,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		pDC->SelectObject(&font);
        font2.DeleteObject();
// 		if (!STATIONUPDN)
// 		{
//             str = "UP TO(" ;
// 		}
// 		else
// 		{		
//			str = "DN TO(" ;
//		}
//		str += strMsg;
//		str = "(";
		str = m_szDrawName ;
//		str += ")" ;

		if ( str.Find("XXX") > 0 )
			str = "";
		
		rect = CRect(p.x+35-5*l, p.y+10, p.x+45-5*l, p.y+20);
//		rect = CRect(p.x-18, p.y+10, p.x-8, p.y+20);
		
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );	
		pDC->SelectObject( oldfont );
	}

 	font.DeleteObject();
}

else if((m_nType == 2) || (m_nType == 13) || ((m_nType == 31) && ((m_nID == 256) || (m_nID == 257) || (m_nID == 276) || (m_nID == 277))))	// 인접 역명 좌 ( 2 : Tokenless, 13 : Token )
{
	mColor = 0;
    BmpDraw tbmp;
	CFont font;
	CString str = mName, sName1, sName2;
	CPoint p = mPos;	
	short l = strlen(str);
    short n = str.Find( '_' ); 
	CString strBlockClearButtonName = m_szDrawName;

//	if ( m_strStationName.Find ( "BYPA",0 ) >= 0 && str.Find ( "PAGH",0) >=0 ) {
	if ( str.Find ( "KALI",0 ) >= 0 && str.Find ( "ASAD",0) >=0 ) 
	{
		if (!REVERSE)
		{
			sName1 = "B1";
			sName2 = "B2";
		}
		else
		{
			sName1 = "B5";
			sName2 = "B6";
		}
	} 
	else 
	{
		if (!REVERSE)
		{
			sName1 = "B3";
			sName2 = "B4";
		}
		else
		{
			sName1 = "B1";
			sName2 = "B2";
		}
	}

	if ( str == "BCB11" || str == "BCB12" )
	{
		sName1 = "B11";
		sName2 = "B12";
	}

/*
	if (str.Find("BYPASS", 0) > 0)
	{
		sName1 = "B11";
		sName2 = "B12";
	}
*/
#ifdef _LSD
#else
	ScrInfoBlock *pBlockInfo;
	ScrInfoBlock *pBlockInfo2;

	for (int index=0; index<256; index++) // TGB Block 상태
	{
		if (EtcScrInfo[index].str == sName1 )
		{
			if (EtcScrInfo[index].type == OBJ_SIGNAL) 
			{
				pBlockInfo = (ScrInfoBlock*)EtcScrInfo[index].m_pData;
				break;
			}
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	for (index=0; index<256; index++) // TCB Block 상태
	{
		if (EtcScrInfo[index].str == sName2 )
		{
			if (EtcScrInfo[index].type == OBJ_SIGNAL) 
			{
				pBlockInfo2 = (ScrInfoBlock*)EtcScrInfo[index].m_pData;
				break;
			}
		}
		else
		{
		    if (EtcScrInfo[index].m_pData == NULL)
			    break;
		}
	}

	if ((EtcScrInfo[index].m_pData != NULL) && (m_nType != 2)) 
	{
		if ( pBlockInfo->RBGPR && ( strBlockClearButtonName == "BCB1" || strBlockClearButtonName == "BCB11" ))
			mColor = MY_RED;
		else if ( pBlockInfo2->RBCPR && ( strBlockClearButtonName == "BCB2" || strBlockClearButtonName == "BCB12" )) // TGB, TCB 블락 사용시 Red
			mColor = MY_RED;     // "OFF"   -  Gray
		else
			mColor = 0;		// "ON"    -  normal 

		if (( pBlockInfo->BLKRSTREQIN || pBlockInfo->BLKRSTREQ ) && ( strBlockClearButtonName == "BCB1" || strBlockClearButtonName == "BCB11" ))
		{
			if((pBlockInfo->BLKRSTREQIN && pBlockInfo->BLKRSTACC) || (pBlockInfo->BLKRSTREQ && pBlockInfo->BLKRSTACCIN))
			{
				mColor = MY_FYELLOW;
			}
			else
			{
				mColor = MY_RED;
				mColor|=0x1000;
			}
		}
		else if (( pBlockInfo2->BLKRSTREQIN || pBlockInfo2->BLKRSTREQ ) && ( strBlockClearButtonName == "BCB2" || strBlockClearButtonName == "BCB12" )) // TGB, TCB 블락 사용시 Red
		{
			if((pBlockInfo2->BLKRSTREQIN && pBlockInfo2->BLKRSTACC) || (pBlockInfo2->BLKRSTREQ && pBlockInfo2->BLKRSTACCIN))
			{
				mColor = MY_FYELLOW;
			}
			else
			{
				mColor = MY_RED;
				mColor|=0x1000;
			}
		}
    }
//////////////////////// 2012.08.27 Axle Counter로 표시를 대체함.
#endif
    if (n > 0) 
	{
		CString Temp = str.Right(l-n-1);
		str = str.Left( n );
		str += "  ";
		str += Temp;
	}

    if (!m_nID)
	{
		font.CreateFont(15,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		CFont *oldfont = pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);

		pDC->SetTextColor( RGB(192,192,192) );
		tbmp.SetWP(Pos);

		tbmp.SetBase(0);
		tbmp.Draw(177);
		p.x -=CELLSIZE_X*1;
		p.y +=CELLSIZE_Y*2;

		CRect rect(p.x-10, p.y-20, p.x+10, p.y-10); 		
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		pDC->SelectObject( oldfont );

	}
	else if (m_nType == 31)  // Block 버튼
	{
		Pos.y+=3;
		tbmp.SetBase( mColor );
		tbmp.SetWP(Pos);
		tbmp.Draw(182);

		CFont fontt;
		fontt.CreateFont(10,5,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
		CFont *oldfont = pDC->SelectObject(&fontt);
		CRect rect(Pos.x-5, Pos.y-20, Pos.x+5, Pos.y-10);
		
		if(m_bBTNClick)
		{
			pDC->SetTextColor(RGB(0,255,0));
		}
		else
		{
			pDC->SetTextColor(RGB(255,255,255));
		}

		if(strBlockClearButtonName == "BCB1" || strBlockClearButtonName == "BCB11")
		{
			pDC->DrawText( "BGR", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		}
		else if (strBlockClearButtonName == "BCB2" || strBlockClearButtonName == "BCB12")
		{
			pDC->DrawText( "BCR", -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		}
		pDC->SelectObject( oldfont );
		fontt.DeleteObject();
	}

	else
	{
		if ( m_pData && m_nType == 2 ) 
		{
			if (!REVERSE)
			{
				if (GetBitLogic(m_pData, m_nFlashID))
				{
					mColor = MY_RED | 0x1000;    // Modem Down = 1 일때
				}
			}
			else
			{
				//				CString str = m_szDrawName;
				if ( m_strStationName.Find ( "BYPA",0 ) >= 0 && str.Find ( "PAGH",0) >=0 ) {
					if (GetBitLogic(m_pData, 32868))
					{
						mColor = MY_RED | 0x1000;    // Modem Up = 1 일때
					}
				} else {
					if (GetBitLogic(m_pData, m_nFlashID))
					{
						mColor = MY_RED | 0x1000;    // Modem Up = 1 일때
					}
				}
			}
		}
		
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor( RGB(255,255,255) );
		int len = strlen(str);
		int stnlen = 7;
		tbmp.SetBase( mColor );
		tbmp.SetWP(Pos);
		tbmp.Draw(83);
		tbmp.Draw(163);		

		CFont font2;
//		CString strMsg;
//		strMsg.Format("U:%d,D:%d",!GetBitLogic(m_pData, 32868),!GetBitLogic(m_pData, 32869));

		font2.CreateFont(19,stnlen,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		CFont *oldfont = pDC->SelectObject(&font2);

		p.x -=CELLSIZE_X*1;
		CRect rect(p.x-37+5*l, p.y-20, p.x-27+5*l, p.y-10); // 인접 역명 위치
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );

		font.CreateFont(18,7,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("Times New Roman"));
		pDC->SelectObject(&font);
        font2.DeleteObject();
// 		if (!STATIONUPDN)
// 		{
//             str = "DN TO(" ;
// 		}
// 		else
// 		{		
// 			str = "UP TO(" ;
// 		}
//		str += strMsg;
//		str = "(";
		str = m_szDrawName ;
//		str += ")" ;

		if ( str.Find("XXX") >= 0 )
			str = "";
		
		rect = CRect(p.x-37+5*l, p.y+10, p.x-27+5*l, p.y+20);
//		rect = CRect(p.x+18, p.y+10, p.x+28, p.y+20);
		
		pDC->DrawText( str, -1, rect, DT_NOCLIP | DT_CENTER | DT_VCENTER | DT_SINGLELINE );	
		pDC->SelectObject( oldfont );
	}
 	font.DeleteObject();

}



// Region 보기
//    CBrush brush( RGB( 0, 255, 255) );
//	pDC->FrameRgn( this, &brush, 1, 1 );

}

void CBmpEtcObj::Serialize(CArchive& ar) 
{	
	if(ar.IsStoring())	
	{		
		char buf[200];
		char cap[20];
		memset(buf,0x00,sizeof(buf));
		memset(cap,0x00,sizeof(cap));	
		char en[2];
		strcpy(en,"\r\n");
		strcpy(cap,"#[ETCOBJ]\r\n");
		ar.Write(cap,strlen(cap));					
		sprintf(buf,"%s, ( %d, %d ) %d, %d , %s ,%d ,%d",mName,mPos,m_nType,m_nID,m_szDrawName,mWidth,mHeight);
		ar.Write(buf,strlen(buf));
		if (m_nType > 19 || m_nType == 2 || m_nType == 3) 
		{
			sprintf(buf,", %d %c %d %c", m_nOnID, m_nOnColor, m_nFlashID, m_nFlashColor);
			ar.Write(buf,strlen(buf));
		}
		ar.Write(en,2);				
	}
	
}


////////////////////////////////////////////////////
//////    CBmpTrack cpp    //////////////////////


CBmpTrack::CBmpTrack()
{
	SetObjectType();
	m_bIsFileOpenPos = 0;
	pSwitch = NULL;
	pSwitch2 = NULL;	
	mSignalCount = 0;
	mButtonCount = 0;
    m_szTrainNo[0] = 0;
    m_rectTNI = CRect(0,0,0,0);
	m_bIsDoubleSlip = 0;
	m_bLineLimit = 0;
	m_bLastTrack = 0;
	m_nTrackInfoFlag = 0;
	m_strPoint = "0";
	m_strPoint2 = "0";
	m_strOverlapRoute = "0";
// 	for ( int i = 0; i < 3; i++ )
// 	{
// 		m_strDestinationTrackUS[i] = "0";
// 		m_strDestinationTrackDS[i] = "0";
// 		m_strReferencePointUS[i] = "0";
// 		m_strReferencePointDS[i] = "0";
// 	}
}

CBmpTrack::~CBmpTrack()
{	
	Init();
}

void CBmpTrack::Init()
{
	m_bIsDoubleSlip = 0;
	m_bLineLimit = 0;
	m_nTrackInfoFlag = 0;
	C = N = R =0;
	JeulyunInfo =0;
	IsJoinTrack = 0;
	mSelect		=1;
	m_IsPress	=0;	
	mCellCount=0;
	pCellInfo = NULL;
	mJoinCellNo=-1;		
	mJoinCtypeVector=0;
	mJoinNtypeVector=0;
	mJoinRtypeVector=0;
	mJoinCount=0;
	mTotNodeType =0;

	mNamePos1 = CPoint(0,0);
	for (int i=0; i< 3; i++)	
	{
		mSpCellNo[i]=-1; 	
		mSpCellVector[i]=0;	
		mJoinTrackInfo[i].Track = NULL;
		mJoinTrackInfo[i].Node = 0;
	}

	POSITION pos;
	CBmpTrackCell *pCell;

	for( pos = mCellInfoList.GetHeadPosition(); pos != NULL; )
	{
        pCell = (CBmpTrackCell *)mCellInfoList.GetNext( pos );
     	delete pCell;
	}

	mCellInfoList.RemoveAll();	

	mPosList.RemoveAll();
	if(pCellInfo) 
	{
		delete pCellInfo;
	}

	for (int n = 0; n<(int)mButtonCount; n++) 
	{
		delete pButton[n];
		pButton[n] = NULL;
	}
	for (n = 0; n<(int)mSignalCount; n++) 
	{
		delete pSignal[n];
		pSignal[n] = NULL;
	}
	mSignalCount = 0;
	mButtonCount = 0;	
	if(pSwitch) delete pSwitch;
	if(pSwitch2) delete pSwitch2;	
	pSwitch = pSwitch2 = NULL;


	// 2006.11.16 Overlap 구간의 초록색 표시를 위해 삽입함...
	TrackObject.SetAtGrow(TrackObject.GetSize(), (CObject*)this);
}

void CBmpTrack::SetCNRPntDes(CPoint c,CPoint n,CPoint r )
{
	ChangeMapping(c,n);
	ChangeMapping(r);	
	SetCNRPnt(c, n, r );
	}
void CBmpTrack::SetCNRPnt(CPoint c,CPoint n,CPoint r )
{
	C = c; 
	N = n; 
	R = r; 
}

void CBmpTrack::AddPosStrList(CPoint p1, CPoint p2,UINT nodetype)
{		
	CString str;
	char buf[200];
	memset(buf,0x00,200	);
	sprintf(buf,"( %4d , %4d )  ( %4d , %4d )  %3u",p1.x,p1.y,p2.x,p2.y,nodetype);	
	mPosList.Add( (LPCTSTR)buf);
}

void CBmpTrack::AddPosDes(CPoint p1, CPoint p2,UINT nodetype){

	ChangeMapping(p1,p2);
	AddPos( p1,  p2, nodetype);
}
void CBmpTrack::AddPos(CPoint p1, CPoint p2,UINT nodetype)
{	
	p1 = CPoint(p1.x,p1.y);	
	p2 = CPoint(p2.x,p2.y);	

	CPoint wid = p2 - p1;
	CPoint p = p1;
	int lx,ly;
	lx = abs(wid.x);
	ly = abs(wid.y);	
	if (lx == 0 && ly == 0) return;
	AddPosStrList(p1,p2,nodetype);
	/////////////////////
	UINT vectortype=0;	
	int i_size;
	if ( lx >= ly)
		i_size = lx / DES_GRID_SIZE ;						// x점을 기준으로 폭을 결정
	else 
		i_size = ly / DES_GRID_SIZE ;						// y점을 기준으로 폭을 결정

	if(wid.x > 0)									// 현재 선의 방향을 알아냄 
	{
		if(wid.y > 0)
		    vectortype = TYPE_RIGHT_BOTTOM;
		else if(wid.y == 0)
			vectortype = TYPE_RIGHT;
		else 
			vectortype = TYPE_RIGHT_TOP;
	}
	else if (wid.x == 0) 
	{
		if(wid.y > 0)
		    vectortype = TYPE_BOTTOM;		
		else 
			vectortype = TYPE_TOP;
	}
	else {
		if(wid.y > 0)
		    vectortype = TYPE_LEFT_BOTTOM;
		else if(wid.y == 0)
			vectortype = TYPE_LEFT;
		else 
			vectortype = TYPE_LEFT_TOP;
	}
	UINT startype = 0;									// 첫번째셀의 시작부분 의 모양 을 초기화 
	UINT endtype = vectortype;							// 첫번째셀의 끝부분   의 모양 (현재 진행중인 방향)
	UINT vtype;											// 해당셀의 모양 정보 
	mTotNodeType |= nodetype;							// 현재 트랙이 포함하고 있는 노트 정보 
	for (int i= 0 ; i <= i_size ; i++)
	{			
	    if(i == i_size )    endtype=0;   				// 마지막 셀일경우 끝부분을 초기화		
		vtype = startype | endtype;						// 현재 셀의 방향 정보
		int CellNo = ListFind(p);	                    // 겹치는 것이 있는가 찾기( Join셀 or 같은노드로 이어서 들어온 셀) 
		if(CellNo >= 0 )		{						// 겹친 셀의 번호가 있는가?
			CBmpTrackCell *trackcell = (CBmpTrackCell*)pCellInfo;				// 셀 정보를 가져옴 
			if(trackcell->mNodeType == nodetype)  	{	// 같은 노드의 연결부위 (같은노드로 이어서 들어온 셀) 
				trackcell->mVectorType |= vectortype;				
			}
			else			{
				if(mJoinCellNo >= 0 && mJoinPos != trackcell->mPos)  // 연결셀이 한개 이상일때는 오류 
				{	
					//Error("연결셀  중복");
					return;
				}
				else {
					mJoinPos = trackcell->mPos;
					mJoinCount++;
					mJoinCellNo = CellNo;				
				}
				if(nodetype == NODE_CTYPE)	{						 
			   		mJoinCtypeVector = vtype;							// 연결 안돼었을때의 모양을 저장 함
					if(trackcell->mNodeType == NODE_NTYPE)
						mJoinNtypeVector = trackcell->mVectorType;
					else if(trackcell->mNodeType == NODE_RTYPE) 
						mJoinRtypeVector = trackcell->mVectorType;
				}
				else if (nodetype == NODE_NTYPE)	{
					mJoinNtypeVector = vtype;
					if(trackcell->mNodeType == NODE_CTYPE)
						mJoinCtypeVector = trackcell->mVectorType;
					else if(trackcell->mNodeType == NODE_RTYPE)
						mJoinRtypeVector = trackcell->mVectorType;				
				}
				else {
					mJoinRtypeVector = vtype;
					if(trackcell->mNodeType == NODE_CTYPE)
						mJoinCtypeVector = trackcell->mVectorType;
					else if(trackcell->mNodeType == NODE_NTYPE)
						mJoinNtypeVector = trackcell->mVectorType;
				}
				trackcell->mNodeType = NODE_JTYPE;		
			}
		}
		else {
			pCellInfo = new  CBmpTrackCell(mCellCount,p,vtype,nodetype);
			mCellInfoList.AddTail(pCellInfo);
			mCellCount++;
		}			
		startype = MovePos(vectortype,p);
	}

	if(mJoinCount >= 2) {								// 연결셀 정리.....
		SetJoinCell();
	}	
}

void CBmpTrack::ConvertPos() 
{		
	int dx = CELLSIZE_X;
	int dy = CELLSIZE_Y;
	CPoint p;
	CBmpTrackCell *pCell;
	POSITION pos;
	LptoDp(mNamePos1);
	if (m_szDrawName[0] && m_bIsFileOpenPos) {		// des 에서 는 제외	
		int w = strlen(m_szDrawName) * CELLSIZE_X / 2;
		m_rectArea = CRect( mNamePos1.x - w, mNamePos1.y - CELLSIZE_Y, mNamePos1.x + w, mNamePos1.y + CELLSIZE_Y);
	}
	LptoDp(C);
	LptoDp(N);	 
	LptoDp(R); 	
	for( pos = mCellInfoList.GetHeadPosition(); pos != NULL; )
	{		
		pCell = (CBmpTrackCell *)mCellInfoList.GetNext( pos );
		LptoDp(pCell->mPos);				
	}

	if(pSwitch)			
	{
		LptoDp(pSwitch->mPos);		
		LptoDp(pSwitch->mNamePos);
        CString strR = pSwitch->mName;
		p = pSwitch->mPos;
		int position = strR.Find('B');

	    if (pSwitch->mPos.y < pSwitch->mNamePos.y)
		{
  	       p.y += 3;  // dn
		}
	    else
		{
			p.y -= 3;  // up
		}
		pSwitch->CreateEllipticRgn( p.x - dx-dx, p.y - dy, p.x + dx+dx, p.y + dy );

		p = pSwitch->mPos;
		if (pSwitch->mPos.y < pSwitch->mNamePos.y)
		{
			p.y += 20; // dn
		}
		else
		{
			p.y -= 20;  // up
		}
		CRgn rgn;
		rgn.CreateRectRgn( p.x - 17, p.y - dy, p.x + 17, p.y + dy );
		pSwitch->CombineRgn( pSwitch, &rgn, RGN_OR );
	}

	if(pSwitch2)		
	{
		LptoDp(pSwitch2->mPos);
		LptoDp(pSwitch2->mNamePos);				
			p = pSwitch2->mPos;
			pSwitch2->CreateEllipticRgn( p.x - dx, p.y - dy, p.x + dx, p.y + dy );
	}
	UINT i;

	dx = (int)(dx * 1.5);
	for (i = 0; i<mButtonCount; i++) {
		LptoDp(pButton[i]->mPos);				
			p = pButton[i]->mPos;
			pButton[i]->CreateRectRgn( p.x - dx * pButton[i]->mWidth / 2 - 2, p.y - dy - 2,
								   p.x + dx * pButton[i]->mWidth / 2 + 2, p.y + dy + 2);
	}

	for (i = 0; i<mSignalCount; i++) {
		LptoDp(pSignal[i]->mPos);	
		LptoDp(pSignal[i]->mNamePos);
		LptoDp(pSignal[i]->mAapguNamePos);

			CPoint po = pSignal[i]->mPos;
			BOOL bSetRgn = TRUE;


			// 2006.5.22  Istart signal 처리를 위해 수정.... 
			if (pSignal[i]->m_nType > 4 && pSignal[i]->m_nType != 12 ) {
					bSetRgn = FALSE;
			}
			else if ( pSignal[i]->m_nType == 12 && pSignal[i]->mLamp != 8) {
					bSetRgn = FALSE;
			}

			if ( bSetRgn ) {
				int Type = pSignal[i]->m_nType;				
				p = 0; 	
						
				p.x += (int)(CELLSIZE_X * 7);			
				dx += CELLSIZE_X;

				if(pSignal[i]->m_nType == 0) // Home Signal
				{
					switch ( pSignal[i]->mLamp )
					{
					case 0:		// CRYG
					case 1:		// SRYG
					case 4:		// SCRY
						p.x += CELLSIZE_X * 5;
						break;
					case 2:		// CRY
					case 3:		// SRY
						p.x += CELLSIZE_X * 2;
						break;
					case 8:		// SCRYG
						p.x += CELLSIZE_X * 8;
						break;
					default:
						break;	// 5: RY, 6: RG
					}
					
					if ( pSignal[i]->mHasLoop == 4 )
						p.x += CELLSIZE_X * 2;
				}
				else if(pSignal[i]->m_nType == 1) // Start Signal
				{
					switch ( pSignal[i]->mLamp )
					{
					case 0:		// RYG
						p.x += CELLSIZE_X * 3;
						break;
					case 2:		// SRYG
					case 6:		// YRYG
						p.x += CELLSIZE_X * 5;
						break;
					case 3:		// SRY
					case 4:		// SRG
						p.x += CELLSIZE_X * 2;
						break;
					default:	// 1: RY, 5: SR, 7: RG
						break;
					}
					
					if ( pSignal[i]->mHasLoop == 4 )
						p.x += CELLSIZE_X * 2;
				}
				else if( pSignal[i]->m_nType == 4 ) // Outer Home
				{
					p.x += CELLSIZE_X * 5;
				}
				else if( pSignal[i]->m_nType == 2 ) // Ground Shunt
				{
					switch ( pSignal[i]->mLamp )
					{
					case 10:	// S
						p.x -= CELLSIZE_X * 3;
						break;
					default:	// 11: SF, 12: SR
						break;
					}
				}

			    if(pSignal[i]->IsRight)
				{
					pSignal[i]->CreateEllipticRgn( po.x - dx, po.y - dy, po.x + p.x , po.y + dy  );
				}
				else 
				{
					pSignal[i]->CreateEllipticRgn( po.x  - p.x, po.y - dy, po.x + dx, po.y + dy );
				}

			    // 2006.5.22  Istart signal 처리를 위해 수정.... 
				if (pSignal[i]->m_nType < 5 || pSignal[i]->m_nType == 12)
				{
/* 		            CPoint p = pSignal[i]->mPos;
		            if (pSignal[i]->IsRight) p.y -= 24;
		            else	p.y += 24;
*/
#ifndef _LSD
					CString strSigName,strBtnName;
					strSigName = pSignal[i]->mName;
					CPoint p = pSignal[i]->mPos;
					CPoint SigPos = pSignal[i]->mPos;
					CPoint BtnPos;
					BOOL   bFind = FALSE;
					int iTemp;

					for ( int b = 0; b < 2 ; b++ ){
						strBtnName = pButton[b]->mName;
						iTemp = strBtnName.Find(".",0);
						if ( iTemp > 0 ) {
							strBtnName = strBtnName.Left(iTemp);
						}
						if ( strSigName == strBtnName ) {
							BtnPos = pButton[b]->mPos;
							bFind = TRUE;
						}
					}
					if ( bFind == TRUE ) {
						if (_nSFMViewXSize == 1600 || _nSFMViewXSize == 1920 || REVERSE == 0)  {
							if (strSigName.Find("A") == 0) {
								if (pSignal[i]->m_nDir != 1)  p.y += 24;
								else	p.y -= 24;				
							} else {				
								if (pSignal[i]->m_nDir == 1) {
									if (BtnPos.x < SigPos.x)	p.x -= 35;
									else {
										if (BtnPos.y < SigPos.y)	p.y -= 24;
										else p.y += 24;
									}
								} else {
									if (BtnPos.x > SigPos.x)	p.x += 35;
									else {
										if (BtnPos.y < SigPos.y)	p.y -= 24;
										else p.y += 24;
									}
								}
							}
						} else {
							if (strSigName.Find("A") != -1) {				
								if (pSignal[i]->m_nDir != 1) p.y += 24;
								else	p.y -= 24;				
							} else {
								if (pSignal[i]->m_nDir == 1) {
									if (BtnPos.x < SigPos.x)	p.x -= 35;
									else {
										if (BtnPos.y < SigPos.y)	p.y -= 24;
										else p.y += 24;
									}
								} else {
									if (BtnPos.x > SigPos.x)	p.x += 35;
									else {
										if (BtnPos.y < SigPos.y)	p.y -= 24;
										else p.y += 24;
									}
								}
							}
						}
						CRgn rgn;
						rgn.CreateRectRgn( p.x - 15, p.y - 9, p.x + 15, p.y + 9 );
						pSignal[i]->CombineRgn( pSignal[i], &rgn, RGN_OR );
					}
#endif
				}
			}
	}


// 궤도 영역의 Region 생성
	if ( !m_hObject ) {
		BOOL first = TRUE;
		for( pos = mCellInfoList.GetHeadPosition(); pos != NULL; )
		{		
			pCell = (CBmpTrackCell *)mCellInfoList.GetNext( pos );
			CPoint p = pCell->mPos;

			CRgn irgn;
			POINT pPoint[4];
			
			short dx = 5, dy = 5;

			pPoint[0].x = p.x - dx;
			pPoint[0].y = p.y - dy;
			pPoint[1].x = p.x + dx;
			pPoint[1].y = p.y - dy;
			pPoint[2].x = p.x + dx;
			pPoint[2].y = p.y + dy;
			pPoint[3].x = p.x - dx;
			pPoint[3].y = p.y + dy;

			if (first) {
				CRgn::CreatePolygonRgn( pPoint, 4, WINDING );
				first = FALSE;
			}
			else {
				irgn.CreatePolygonRgn( pPoint, 4, WINDING );
				CRgn::CombineRgn( this, &irgn, RGN_OR );
			}
		}
	}
}


void CBmpTrack::SetObjectConfig( BYTE cConfig )
{
	UINT i;

	m_cConfig = cConfig;
	for (i = 0; i<mButtonCount; i++) {
		pButton[i]->m_cConfig = cConfig;
	}

	for (i = 0; i<mSignalCount; i++) {
		pSignal[i]->m_cConfig = cConfig;
	}

// 열번창 RECT 영역 계산
	if ( !(m_cConfig & OPTION_NOTRAINNO) ) {
		if ( m_nTrackInfoFlag & (BIT_TK_HAS_MAINSIGNAL + BIT_TK_HAS_MAINDEST) ) {
    		CPoint Pos = mNamePos1;
			if (m_nTrackInfoFlag & (BIT_TK_MAINLINE)) {
				Pos.x += 35;
				Pos.y -= 16;
			}
			else {
				Pos.y -= 13;
			}
			if ( _UseSmallText == 0 ) {
			    if ( m_rectTNI.IsRectNull() ) {
					if (!strcmp((LPCTSTR)m_szDrawName,"1T")||!strcmp((LPCTSTR)m_szDrawName,"2T")
						||!strcmp((LPCTSTR)m_szDrawName,"3T")||!strcmp((LPCTSTR)m_szDrawName,"4T")
						||!strcmp((LPCTSTR)m_szDrawName,"5T")||!strcmp((LPCTSTR)m_szDrawName,"6T")
						||!strcmp((LPCTSTR)m_szDrawName,"7T")||!strcmp((LPCTSTR)m_szDrawName,"8T")
						||!strcmp((LPCTSTR)m_szDrawName,"9T"))  {
			    		Pos.x -= 35;
                    }
			        m_rectTNI.SetRect(Pos.x - (10*2+2), Pos.y-(5+1), Pos.x + (10*2+2), Pos.y+(5+1));
				}
			}
		}
	}
}

void CBmpTrack::SetArrow(int iArrow)
{
	m_iArow = iArrow;
}

void CBmpTrack::SetDoubleSlip()
{
	//m_bIsDoubleSlip = 1;
	//m_bLineLimit = 1;
	m_bLastTrack = 1;
}

void CBmpTrack::SetDifTrack()
{
	m_bLineLimit = 1;
}

void CBmpTrack::SetJoinCell()
{
	UINT TrackTypeSpecialCell = TYPE_SPECIAL_CHECK;		// 사선간격이 작을때 조금 크게 그리기 위해 
	CPoint p;
	UINT JoinCtypeVector; 
	if( mJoinCtypeVector < 32)
		JoinCtypeVector = mJoinCtypeVector << 4;
	else 
		JoinCtypeVector = mJoinCtypeVector >> 4;
	if( JoinCtypeVector == 256) JoinCtypeVector =1;
	if(mJoinNtypeVector == JoinCtypeVector )	{	//C-N이 직선 일때
		if(JoinCtypeVector != TYPE_RIGHT && JoinCtypeVector !=TYPE_LEFT)	{  // C가 수평이 않일때 
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);
			mSpCellNo[0] = ListFind(p);
			if(	mJoinNtypeVector & ( TYPE_LEFT_BOTTOM | TYPE_LEFT_TOP | TYPE_RIGHT_BOTTOM | TYPE_RIGHT_TOP) )
				mSpCellVector[0] = mJoinNtypeVector | TrackTypeSpecialCell;
			else 
				mSpCellVector[0] = mJoinNtypeVector;
			//
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);						
			mSpCellNo[1] = ListFind(p);
			mSpCellVector[1] = 0xffff;				// 모양 없음			
			//					
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);
			MovePos(mJoinRtypeVector,p);						
			mSpCellNo[2] = ListFind(p);
			mSpCellVector[2] = mJoinRtypeVector;  
		}
		else {
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);
			mSpCellNo[0] = ListFind(p);
			if(	mJoinRtypeVector & ( TYPE_LEFT_BOTTOM | TYPE_LEFT_TOP | TYPE_RIGHT_BOTTOM | TYPE_RIGHT_TOP) )
				mSpCellVector[0] = mJoinRtypeVector | TrackTypeSpecialCell;
			else 
				mSpCellVector[0] = mJoinRtypeVector;
			//
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);						
			mSpCellNo[1] = ListFind(p);
			mSpCellVector[1] = 0xffff;
			//					
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);
			MovePos(mJoinNtypeVector,p);						
			mSpCellNo[2] = ListFind(p);
			mSpCellVector[2] = mJoinNtypeVector;			
		}
	}
	else if (mJoinRtypeVector == JoinCtypeVector)	{   // C-R이 직선  일때
		if(JoinCtypeVector != TYPE_RIGHT && JoinCtypeVector !=TYPE_LEFT)	{  // C가 수평이 않일때 
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);
			mSpCellNo[0] = ListFind(p);
			if(	mJoinRtypeVector & ( TYPE_LEFT_BOTTOM | TYPE_LEFT_TOP | TYPE_RIGHT_BOTTOM | TYPE_RIGHT_TOP) )
				mSpCellVector[0] = mJoinRtypeVector | TrackTypeSpecialCell;
			else 
				mSpCellVector[0] = mJoinRtypeVector;
			//
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);						
			mSpCellNo[1] = ListFind(p);
			mSpCellVector[1] = 0xffff;				// 모양 없음			
			//					
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);
			MovePos(mJoinNtypeVector,p);						
			mSpCellNo[2] = ListFind(p);
			mSpCellVector[2] = mJoinNtypeVector;  
		}
		else {
			p = mJoinPos;
			MovePos(mJoinNtypeVector,p);
			mSpCellNo[0] = ListFind(p);
			if(	mJoinNtypeVector & ( TYPE_LEFT_BOTTOM | TYPE_LEFT_TOP | TYPE_RIGHT_BOTTOM | TYPE_RIGHT_TOP) )
				mSpCellVector[0] = mJoinNtypeVector | TrackTypeSpecialCell;
			else 
				mSpCellVector[0] = mJoinNtypeVector;
			//
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);						
			mSpCellNo[1] = ListFind(p);
			mSpCellVector[1] = 0xffff;
			//					
			p = mJoinPos;
			MovePos(mJoinRtypeVector,p);
			MovePos(mJoinRtypeVector,p);						
			mSpCellNo[2] = ListFind(p);
			mSpCellVector[2] = mJoinRtypeVector;  // 모양 없음			
		}		
	}
	else {   //  R - N 모두 직선이 아닌경우 
		p = mJoinPos;
		MovePos(mJoinNtypeVector,p);
		mSpCellNo[0] = ListFind(p);
		if(	mJoinNtypeVector & ( TYPE_LEFT_BOTTOM | TYPE_LEFT_TOP | TYPE_RIGHT_BOTTOM | TYPE_RIGHT_TOP) )
			mSpCellVector[0] = mJoinNtypeVector | TrackTypeSpecialCell;
		else 
			mSpCellVector[0] = mJoinNtypeVector;
		//
		p = mJoinPos;
		MovePos(mJoinRtypeVector,p);						
		mSpCellNo[1] = ListFind(p);
		mSpCellVector[1] = mJoinRtypeVector;
		//										
		mSpCellNo[2] = -1;
		mSpCellVector[2] = 0xffff;			
	}
}

UINT  CBmpTrack::MovePos(UINT vector,CPoint &p)
{
	UINT startype=0;
	switch (vector)	
	{
	case TYPE_RIGHT :
		p.x += DES_GRID_SIZE;
		startype = TYPE_LEFT;
		break;
	case TYPE_RIGHT_TOP :
		p.x += DES_GRID_SIZE;
		p.y -= DES_GRID_SIZE;
		startype = TYPE_LEFT_BOTTOM;
		break;
	case TYPE_TOP :
		p.y -= DES_GRID_SIZE;	
		startype = TYPE_BOTTOM;
		break;
	case TYPE_LEFT_TOP :
		p.x -= DES_GRID_SIZE;
		p.y -= DES_GRID_SIZE;
		startype = TYPE_RIGHT_BOTTOM;
		break;
	case TYPE_LEFT :
		p.x -= DES_GRID_SIZE;	
		startype = TYPE_RIGHT;
		break;
	case TYPE_LEFT_BOTTOM :
		p.x -= DES_GRID_SIZE;
		p.y += DES_GRID_SIZE;	
		startype = TYPE_RIGHT_TOP;
		break;
	case TYPE_BOTTOM :
		p.y += DES_GRID_SIZE;	
		startype = TYPE_TOP;
		break;
	case TYPE_RIGHT_BOTTOM :
		p.x += DES_GRID_SIZE;
		p.y += DES_GRID_SIZE;
		startype = TYPE_LEFT_TOP;
		break;
	}		
	return startype;
}

int CBmpTrack::ListFind(CPoint p)
{
	POSITION pos=mCellInfoList.GetHeadPosition();		
	while(pos)	{		
		pCellInfo=(CBmpTrackCell*)mCellInfoList.GetNext(pos);		
		if( pCellInfo->mLpPos == p ) {
			return pCellInfo->mCellNo;
		}
	}
	return -1;
}

UINT CBmpTrack::IsConnect(UINT node)
{		
	UINT connect=0;
	
	if (pSwitch && !(pSwitch->mState & node)) // 분기가 호출한 트랙으로 되어있는가?
		return 0;	
	if(IsJoinTrack) 					// 연결된 트랙이 있는가?
	{
		if(mJoinTrackInfo[0].Node) 		// c 에 연결된것이 있는가 
		{
			if( mJoinTrackInfo[0].Track->mJoinTrackInfo[0].Track == this ) // 예외 처리 ( 서로 C로 마주 볼때 )
			{
				if ( mJoinTrackInfo[1].Node == NODE_NTYPE ) 
				{		// N is Diamod
//					if (mJoinTrackInfo[1].Track->pSwitch == NULL)
//						return 0;
//
//					if ( mJoinTrackInfo[1].Track->pSwitch->mState & NODE_NTYPE )
//						return 1;
//					else 
//						return 0;
//		조금 더 지켜보기로 함 (진로표시 못하는 현상을 막기위해 수정)
					if (mJoinTrackInfo[1].Track->pSwitch != NULL)
					{
						if ( mJoinTrackInfo[1].Track->pSwitch->mState & NODE_NTYPE )
							return 1;
						else 
							return 0;
					}
					else
						return 1;
				}
				else if( mJoinTrackInfo[0].Track->mJoinTrackInfo[1].Node == NODE_NTYPE ) { // C'N is Diamond
				}
				else {
					return 1;
				}
			}
			connect = mJoinTrackInfo[0].Track->IsConnect(mJoinTrackInfo[0].Node);		
		}
		else {				// 자신이 루트일때, C에 연결된 궤도 없음.
			if(mJoinTrackInfo[1].Track)	{		// N에 궤도 연결됨.
				if(	!(mJoinTrackInfo[1].Track-> mJoinTrackInfo[0].Track) )		{ // 예외 처리 (서로 root 인 경우)
					UINT sw = mJoinTrackInfo[1].Track->GetSwitchState();
					if( sw  & mJoinTrackInfo[1].Node ) return 1;
					else 
						return 0;
				}
//2000-11-20 for Diamond
				else if ( mJoinTrackInfo[1].Node == NODE_NTYPE ) {		// N is Diamod
					if ( mJoinTrackInfo[1].Track->pSwitch != NULL )
					{
						if ( mJoinTrackInfo[1].Track->pSwitch->mState & NODE_NTYPE )
							return 1;
						else 
							return 0;
					}
				}
//
			}
			if(mJoinTrackInfo[2].Track)	{
				if( !(mJoinTrackInfo[2].Track-> mJoinTrackInfo[0].Track) )		{ // 예외 처리 (서로 root 인 경우)
					UINT sw = mJoinTrackInfo[2].Track->GetSwitchState();
					if( sw  & mJoinTrackInfo[2].Node ) return 1;
			//		else return 0;  //수정
				}
			}
			if(!pSwitch) return 1;				// 분기가 없을때 
			if (pSwitch->mState & node)
				connect = 1;
			else 
				connect = 0;
		}
	}
	else 
		return  1;
	return connect;
}

void CBmpTrack::Draw(CDC *pDC)
{
	BmpDraw::m_pDC = pDC;

	m_nColorNR = MY_BLUE;
	m_nColorC = MY_BLUE;

	UINT nodetype = mTotNodeType;

	BOOL bSwitchMismatch = FALSE;
	BOOL bRoute = FALSE;
	BOOL bAllDrawTrack = FALSE;
    int nInhibit = 0;
	CString str= m_szDrawName;
	UINT SelectColor = MY_BLUE;
// 	BOOL bUSDestinationTrackIsRouted[3];
// 	BOOL bDSDestinationTrackIsRouted[3];
// 	BOOL bRouteFromUS = FALSE;
// 	BOOL bRouteFromDS = FALSE;
// 	CString m_strDestinationTrackUS[3];
// 	CString m_strDestinationTrackDS[3];
// 
// 	for ( int j = 0; j < 3; j++ )
// 	{
// 		m_strDestinationTrackUS[j] = m_sDestinationTrackUS[j];
// 		m_strDestinationTrackDS[j] = m_sDestinationTrackDS[j];
// 		bUSDestinationTrackIsRouted[j] = FALSE;
// 		bDSDestinationTrackIsRouted[j] = FALSE;
// 	}

#ifdef _LSD
#else
    ScrInfoSwitch *pSwInfo = NULL, *pSwitchInfo = NULL, *pSwitchInfo2 = NULL, *pMySwitchInfo = NULL, *pSwitch133Info = NULL, *pSwitch135Info = NULL, *pSwitch138Info = NULL;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
//  ScrInfoSwitch *pSwInfo = NULL, *pSwitchInfo = NULL, *pMySwitchInfo = NULL, *pReferenceSwitchUSInfo[3], *pReferenceSwitchDSInfo[3];
// 	ScrInfoTrack *pUSDestinationTrackInfo[3];
// 	ScrInfoTrack *pDSDestinationTrackInfo[3];
// 	for ( int ii = 0; ii < 3; ii++ )
// 	{
// 		pUSDestinationTrackInfo[ii] = NULL;
// 		pDSDestinationTrackInfo[ii] = NULL;
// 		pReferenceSwitchUSInfo[ii] = NULL;
// 		pReferenceSwitchDSInfo[ii] = NULL;
// 	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int iRouteNumberForDrawingOverlap[MAX_OVERLAP];
	CString strRouteNumberForDrawingOverlap[MAX_OVERLAP];
	CString strBufferRouteNumberForDrawingOverlap = m_strOverlapRoute;
	int iFindAnd;

	for ( int nCounter = 0 ; nCounter < MAX_OVERLAP ; nCounter++)
	{
		iRouteNumberForDrawingOverlap[nCounter] = -1;
		strRouteNumberForDrawingOverlap[nCounter] = "";
	}

	char *SwitchName, *SwitchName2;
	if ( pSwitch ) 
	{
		pSwInfo = (ScrInfoSwitch*)pSwitch->m_pData;
	}


	CString aaa = mName;
	if (aaa.Find("138AT.4") > 0)
		int debug = 1;
	BOOL bHas2ndReferPoint = FALSE;
	BOOL bIsRoute = TRUE;

	//2005.11.1   전철기만 전환을 위한 정보를 위해 삽입..
	if (m_sPointName[0] != 0)
	{
		m_sPointName[4] = '\0';
		char MyPointName[5]={0};
		memcpy(&MyPointName[0], &m_szDrawName[1], 2);
		MyPointName[2] = 'B';
		char cPointName[4] = {0};
		char cPointName2[4] = {0};

		if (m_sPointName[2] == 'N' || m_sPointName[2] == 'R' )
			memcpy(&cPointName[0], &m_sPointName[0],2);
		else if (m_sPointName[3] == 'N' || m_sPointName[3] == 'R' )
			memcpy(&cPointName[0], &m_sPointName[0],3);

		if (m_sPointName2[2] == 'N' || m_sPointName2[2] == 'R' )
			memcpy(&cPointName2[0], &m_sPointName2[0],2);
		else if (m_sPointName2[3] == 'N' || m_sPointName2[3] == 'R' )
			memcpy(&cPointName2[0], &m_sPointName2[0],3);

		if (m_sPointName2[0] != 0)
		{
			m_sPointName2[4] = '\0';
			bHas2ndReferPoint = TRUE;
		}

		for (int i=0; i<SwitchObject.GetSize(); i++ ) 
		{
			TScrobj *pObj = (TScrobj *)SwitchObject.GetAt( i );
			if (strcmp(pObj->mName, "133") == NULL)
			{
				pSwitch133Info = (ScrInfoSwitch*)pObj->m_pData;
			}
			else if (strcmp(pObj->mName, "135") == NULL)
			{
				pSwitch135Info = (ScrInfoSwitch*)pObj->m_pData;
			}
			else if (strcmp(pObj->mName, "138A") == NULL)
			{
				pSwitch138Info = (ScrInfoSwitch*)pObj->m_pData;
			}

			if (strcmp(pObj->mName, m_sPointName) == NULL)
			{
				pSwitchInfo = (ScrInfoSwitch*)pObj->m_pData;
				SwitchName = pObj->mName;
			}
			else if ( bHas2ndReferPoint == TRUE && strcmp(pObj->mName, m_sPointName2) == NULL)
			{
				pSwitchInfo2 = (ScrInfoSwitch*)pObj->m_pData;
				SwitchName2 = pObj->mName;
			}
			else if (strcmp(pObj->mName, MyPointName) == NULL)
			{
				pMySwitchInfo = (ScrInfoSwitch*)pObj->m_pData;
			}
			else if (strcmp(pObj->mName, cPointName) == NULL)
			{
				pSwitchInfo = (ScrInfoSwitch*)pObj->m_pData;
				SwitchName = pObj->mName;
			}
			else if ( bHas2ndReferPoint == TRUE && strcmp(pObj->mName, cPointName2) == NULL)
			{
				pSwitchInfo2 = (ScrInfoSwitch*)pObj->m_pData;
				SwitchName2 = pObj->mName;
			}
		}
	}

	if ( strBufferRouteNumberForDrawingOverlap != "" )
	{
		for ( int nCounter = 0; nCounter < MAX_OVERLAP; nCounter++)
		{
			iFindAnd = strBufferRouteNumberForDrawingOverlap.Find('&');
			if ( iFindAnd < 0 )
				break;
			strRouteNumberForDrawingOverlap[nCounter] = strBufferRouteNumberForDrawingOverlap.Left(iFindAnd);
			iRouteNumberForDrawingOverlap[nCounter] = atoi(strRouteNumberForDrawingOverlap[nCounter]);
			strBufferRouteNumberForDrawingOverlap = strBufferRouteNumberForDrawingOverlap.Right(strBufferRouteNumberForDrawingOverlap.GetLength()-iFindAnd-1);
		}
		strRouteNumberForDrawingOverlap[nCounter] = strBufferRouteNumberForDrawingOverlap;
		iRouteNumberForDrawingOverlap[nCounter] = atoi(strRouteNumberForDrawingOverlap[nCounter]);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
// 	for ( int k = 0; k < 3; k++ )
// 	{
// 		if (m_sReferencePointUSName[k][0] != 0)
// 		{
// 			for (int i=0; i<SwitchObject.GetSize(); i++ ) 
// 			{
// 				TScrobj *pObj = (TScrobj *)SwitchObject.GetAt( i );
// 				
// 				if ( strcmp(pObj->mName, m_sReferencePointUSName[k]) == NULL)
// 				{
// 					pReferenceSwitchUSInfo[k] = (ScrInfoSwitch*)pObj->m_pData;
// 					m_strReferencePointUS[k] = pObj->mName;
// 				}
// 			}
// 		}
// 		if (m_sReferencePointDSName[k][0] != 0)
// 		{
// 			for (int i=0; i<SwitchObject.GetSize(); i++ ) 
// 			{
// 				TScrobj *pObj = (TScrobj *)SwitchObject.GetAt( i );
// 				
// 				if ( strcmp(pObj->mName, m_sReferencePointDSName[k]) == NULL)
// 				{
// 					pReferenceSwitchDSInfo[k] = (ScrInfoSwitch*)pObj->m_pData;
// 					m_strReferencePointDS[k] = pObj->mName;
// 				}
// 			}
// 		}
// 	}
// 	if (m_sDestinationTrackUS[0] != 0)
// 	{
// 		for ( int k=0; k<TrackObject.GetSize(); k++)
// 		{
// 			TScrobj *pUSDestTrackObj = (TScrobj *)TrackObject.GetAt( k );
// 			CString pUSDestTrackObjName = pUSDestTrackObj->mName;
// 			int nWhereIsPoint = pUSDestTrackObjName.Find('.');
// 
// 			for ( int j = 0; j < 3; j++ )
// 			{
// 				if ( nWhereIsPoint < 0 )
// 				{
// 					if (strcmp(pUSDestTrackObjName, m_sDestinationTrackUS[j]) == NULL)
// 					{
// 						pUSDestinationTrackInfo[j] = (ScrInfoTrack*)pUSDestTrackObj->m_pData;
// 						if ( (pUSDestinationTrackInfo[j]->TKREQUEST == 1 && pUSDestinationTrackInfo[j]->ROUTE == 0 ) || pUSDestinationTrackInfo[j]->OVERLAP == 1 )
// 							bUSDestinationTrackIsRouted[j] = TRUE;
// 						else
// 							bUSDestinationTrackIsRouted[j] = FALSE;
// 						
// 						break;
// 					}
// 				}
// 				else
// 				{
// 					if (strcmp(pUSDestTrackObjName.Left(nWhereIsPoint), m_sDestinationTrackUS[j]) == NULL)
// 					{
// 						pUSDestinationTrackInfo[j] = (ScrInfoTrack*)pUSDestTrackObj->m_pData;
// 						if ( (pUSDestinationTrackInfo[j]->TKREQUEST == 1 && pUSDestinationTrackInfo[j]->ROUTE == 0 ) || pUSDestinationTrackInfo[j]->OVERLAP == 1 )
// 							bUSDestinationTrackIsRouted[j] = TRUE;
// 						else
// 							bUSDestinationTrackIsRouted[j] = FALSE;
// 						
// 						break;
// 					}
// 				}
// 			}
// 		}
// 	}
// 	if (m_sDestinationTrackDS[0] != 0)
// 	{
// 		for ( int k=0; k<TrackObject.GetSize(); k++)
// 		{
// 			TScrobj *pDSDestTrackObj = (TScrobj *)TrackObject.GetAt( k );
// 			CString pDSDestTrackObjName = pDSDestTrackObj->mName;
// 			int nWhereIsPoint = pDSDestTrackObjName.Find('.');
// 			
// 			for ( int m = 0; m < 3; m++ )
// 			{
// 				if ( nWhereIsPoint < 0 )
// 				{
// 					if (strcmp(pDSDestTrackObjName, m_sDestinationTrackDS[m]) == NULL)
// 					{
// 						pDSDestinationTrackInfo[m] = (ScrInfoTrack*)pDSDestTrackObj->m_pData;
// 						if ( (pDSDestinationTrackInfo[m]->TKREQUEST == 1 && pDSDestinationTrackInfo[m]->ROUTE == 0 ) || pDSDestinationTrackInfo[m]->OVERLAP == 1 )
// 							bDSDestinationTrackIsRouted[m] = TRUE;
// 						else
// 							bDSDestinationTrackIsRouted[m] = FALSE;
// 						
// 						break;
// 					}
// 				}
// 				else
// 				{
// 					if (strcmp(pDSDestTrackObjName.Left(nWhereIsPoint), m_sDestinationTrackDS[m]) == NULL)
// 					{
// 						pDSDestinationTrackInfo[m] = (ScrInfoTrack*)pDSDestTrackObj->m_pData;
// 						if ( (pDSDestinationTrackInfo[m]->TKREQUEST == 1 && pDSDestinationTrackInfo[m]->ROUTE == 0 ) || pDSDestinationTrackInfo[m]->OVERLAP == 1 )
// 							bDSDestinationTrackIsRouted[m] = TRUE;
// 						else
// 							bDSDestinationTrackIsRouted[m] = FALSE;
// 						
// 						break;
// 					}
// 				}
// 			}
// 		}
// 	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (m_pData) 
	{
		ScrInfoTrack *pInfo = (ScrInfoTrack*)m_pData;
		CString strSwName = m_sPointName;
		CString strSwName2 = m_sPointName2;

		CString strdebugName = mName;
		if( strdebugName == "W3031BT.4")
		{
			int k = 6;
		}
        nInhibit = ((*(BYTE*)pInfo) >> 6) & 3;

		if ( pSwitchInfo != NULL )
		{
			if ((strSwName.Right(1) == 'A' || strSwName.Right(1) == 'N') && !pSwitchInfo->KR_P && ( pSwitchInfo->ROUTELOCK || pInfo->OVERLAP ))
				bIsRoute = FALSE;
			if ((strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R') && !pSwitchInfo->KR_M && ( pSwitchInfo->ROUTELOCK || pInfo->OVERLAP ))
				bIsRoute = FALSE;
			if ( bHas2ndReferPoint == TRUE )
			{
				if ((strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && !pSwitchInfo2->KR_P && ( pSwitchInfo2->ROUTELOCK || pInfo->OVERLAP ))
					bIsRoute = FALSE;
				if ((strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && !pSwitchInfo2->KR_M && ( pSwitchInfo2->ROUTELOCK || pInfo->OVERLAP ))
					bIsRoute = FALSE;
			}
		}

		if ( !strcmp(mName,"W130B133135T.2") || !strcmp(mName,"W130B133135T.4") || !strcmp(mName,"W130B133135T.6") )
		{
			if ( pSwitch ) 
			{
				pSwInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				if (pSwInfo->ROUTELOCK && ((strcmp(mName,"W130B133135T.4") != 0) || !(pSwitchInfo->KR_M && pSwitchInfo->ROUTELOCK)))
					bIsRoute = TRUE;
				else
					bIsRoute = FALSE;
			}
		}
		else if ( !strcmp(mName,"W130B133135T.1") || !strcmp(mName,"W130B133135T.3") || !strcmp(mName,"W130B133135T.5") || !strcmp(mName,"W130B133135T.7") || !strcmp(mName,"W130B133135T.8") )
		{
			if ( strSwName.Right(1) == 'N' && pSwitchInfo->KR_P && pSwitchInfo->ROUTELOCK )
			{
				bIsRoute = TRUE;
			}
			else if ( strSwName.Right(1) == 'R' && pSwitchInfo->KR_M && pSwitchInfo->ROUTELOCK )
			{
				bIsRoute = TRUE;
			}
			else if ( (!strcmp(mName,"W130B133135T.5") ||  !strcmp(mName,"W130B133135T.8")) && pSwitchInfo->ROUTELOCK )
			{
				bIsRoute = TRUE;
			}
			else
			{
				bIsRoute = FALSE;
			}
		}

		if ( (!strcmp(mName,"W130B133135T.1") || !strcmp(mName,"W130B133135T.2") || !strcmp(mName,"W130B133135T.4") || !strcmp(mName,"W130B133135T.5") || !strcmp(mName,"W130B133135T.6") || !strcmp(mName,"W130B133135T.8")) 
			&& bIsRoute == FALSE && !pInfo->TRACK && pInfo->OVERLAP && pSwitch133Info->KR_P && pSwitch135Info->KR_P )
		{
			bIsRoute = TRUE;
		}

		if ( (!strcmp(mName,"W129B131B138AT.1") || !strcmp(mName,"W129B131B138AT.2")) && !pSwitch138Info->ROUTELOCK)
		{
			bIsRoute = TRUE;
		}

		if ( (!strcmp(mName,"W129B131B138AT.3") || !strcmp(mName,"W129B131B138AT.7")) && !pSwitch138Info->ROUTELOCK && pSwitchInfo->KR_P)
		{
			bIsRoute = TRUE;
		}

		if ( (!pInfo->OVERLAP && !(pInfo->TRACK)) ||  pInfo->INHIBIT ) bAllDrawTrack = TRUE;
		if (!pInfo->ROUTE || pInfo->TKREQUEST || pInfo->OVERLAP ) bRoute = TRUE;

		if ( pInfo->INHIBIT /*|| pInfo->TRPWRDN*/ ) { // INHIBIT = 0일때 정상
			m_nColorC = MY_GRAY;
			m_nColorNR = MY_GRAY;
			if ( pInfo->INHIBIT ) 
			{
				if ( m_strStationName.Find ( "AKHA",0 ) >=0 && m_szDrawName[0] == '1' && m_szDrawName[1] == '0' && m_szDrawName[2] == 'T' ) {
					if (!(pInfo->TRACK)){ // 궤도 점유면,
						m_nColorC = MY_RED;
						m_nColorNR = MY_RED;
					} else {
						m_nColorC = MY_GREEN;
						m_nColorNR = MY_GREEN;
					}
				}

				if ( m_strStationName.Find ( "BYPA",0 ) >= 0 && m_szDrawName[0] == '1' && m_szDrawName[1] == '0' && m_szDrawName[2] == 'T' ) {
					if (!(pInfo->TRACK)){ // 궤도 점유면,
						m_nColorC = MY_RED;
						m_nColorNR = MY_RED;
					} else {
						m_nColorC = MY_GREEN;
						m_nColorNR = MY_GREEN;
					}
				}		
			}
		} 
		else if (!(pInfo->TRACK)) // 궤도 점유면,
		{ 
			if ( pSwitch ) 
			{
				pSwInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				if ( pSwitchInfo != NULL )
				{
					if ( bIsRoute == FALSE && pInfo->OVERLAP )
					{
						m_nColorC += 0x1000;
						m_nColorNR += 0x1000;
						if (m_strStationName == "AKHAURA" && ( !strcmp(mName,"W125B126AT.5") || !strcmp(mName,"W129B131B138AT.4")) && !pSwitchInfo->ROUTELOCK)
						{
							if ( !strcmp(mName,"W129B131B138AT.4") && pInfo->ROUTE) {}
							else
							{
								m_nColorC = MY_RED;
							}
						}
					}
					else
					{
						m_nColorC = MY_RED;
						if (!pInfo->OVERLAP) 
							m_nColorNR = MY_RED;
						if (pInfo->EMREL) {
							m_nColorC += 0x1000;
							m_nColorNR += 0x1000;
						}
					}
// 					if ( (strSwName.Right(1) == 'A' || strSwName.Right(1) == 'N') && pSwitchInfo->ROUTE_M == 1 && pInfo->OVERLAP )
// 					{
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
// 					else if ( (strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R') && pSwitchInfo->ROUTE_P == 1 && pInfo->OVERLAP )
// 					{
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
// 					else if ( bHas2ndReferPoint == TRUE )
// 					{
// 						if ( (strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && pSwitchInfo2->ROUTE_M == 1 && pInfo->OVERLAP )
// 						{
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 						else if ( (strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && pSwitchInfo2->ROUTE_P == 1 && pInfo->OVERLAP )
// 						{
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 						else
// 						{
// 							m_nColorC = MY_RED;
// 							if (!pInfo->OVERLAP) 
// 								m_nColorNR = MY_RED;
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 					}
// 					else
// 					{
// 						m_nColorC = MY_RED;
// 						if (/*!bRoute || */!pInfo->OVERLAP) 
// 						m_nColorNR = MY_RED;
// // 추후에 고려			if ( pInfo->ALARM ) m_nColorC |= 0x1000;
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
				}
				else if ( memcmp(m_sPointName, "HAND", 4) == 0 && pSwInfo->WR_P == 1 && pInfo->OVERLAP ) 	// HAND POINT의 개통방향이 탈선하는 방향일때 진로표시 안함
				{
					if (pInfo->EMREL) {
						m_nColorC += 0x1000;
						m_nColorNR += 0x1000;
				}

//	예외케이스제거
//				strcpy(m_sPointName,"124B");
//				strcpy(m_sPointName2,"108");
//				strcpy(m_sPointName3,"133B");
//				if ( (m_strStationName.Find ( "AKHA",0 ) >=0) && ( pSwInfo->KR_P==1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName2)==NULL)) ) {
//					m_nColorC = MY_BLUE;
//					if (/*!bRoute || */!pInfo->OVERLAP) {
//						m_nColorC = MY_RED;
//						m_nColorNR = MY_RED;
//					}
//				} else if ( (m_strStationName.Find ( "AKHA",0 ) >=0) && (pSwInfo->KR_M == 1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName2)==NULL)) ) {
//					m_nColorC = MY_RED;
//					if (/*!bRoute || */!pInfo->OVERLAP) 
//					m_nColorNR = MY_RED;
//// 추후에 고려		if ( pInfo->ALARM ) m_nColorC |= 0x1000;
//					if (pInfo->EMREL) {
//						m_nColorNR += 0x1000;
//					}
//				} else if ( (m_strStationName.Find ( "BYPA",0 ) >=0) && (pSwInfo->KR_P == 1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName3)==NULL)) ) {
//					m_nColorC = MY_BLUE;
//					if (/*!bRoute || */!pInfo->OVERLAP) {
//						m_nColorC = MY_RED;
//						m_nColorNR = MY_RED;
//					}
//// 추후에 고려		if ( pInfo->ALARM ) m_nColorC |= 0x1000;
//					if (pInfo->EMREL) {
//						m_nColorNR += 0x1000;
//					}
				}
				else{
					if ( strSwName == "FFF" )
					{
						if ( pInfo->OVERLAP && pMySwitchInfo->WR_M )
						{
							m_nColorC = MY_RED;
						}
						else if (!pInfo->OVERLAP || !pMySwitchInfo->WR_P) 
						{
							m_nColorC = MY_RED;
							m_nColorNR = MY_RED;
						}
						if (pInfo->EMREL) {
							m_nColorC += 0x1000;
							m_nColorNR += 0x1000;
						}
					}
					else
					{
						m_nColorC = MY_RED;
						if (/*!bRoute || */!pInfo->OVERLAP) 
						m_nColorNR = MY_RED;
// 추후에 고려			if ( pInfo->ALARM ) m_nColorC |= 0x1000;
						if (pInfo->EMREL) {
							m_nColorC += 0x1000;
							m_nColorNR += 0x1000;
						}
					}
				}
					
			} else {
				if ( pSwitchInfo != NULL )
				{
					if ( bIsRoute == FALSE && pInfo->OVERLAP )
					{
						m_nColorC += 0x1000;
						m_nColorNR += 0x1000;
						if (m_strStationName == "AKHAURA" && (!strcmp(mName,"W125B126AT.7") || !strcmp(mName,"W129B131B138AT.6")) && !pSwitchInfo->ROUTELOCK)
						{
							if ( !strcmp(mName,"W129B131B138AT.6") && pInfo->ROUTE) {}
							else
							{
								m_nColorC = MY_RED;
							}
						}
					}
					else
					{
						m_nColorC = MY_RED;
						if (!pInfo->OVERLAP) 
							m_nColorNR = MY_RED;
						if (pInfo->EMREL) {
							m_nColorC += 0x1000;
							m_nColorNR += 0x1000;
						}
					}
// 					if ( (strSwName.Right(1) == 'A' || strSwName.Right(1) == 'N') && pSwitchInfo->ROUTE_M == 1 && pInfo->OVERLAP )
// 					{
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
// 					else if ( (strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R') && pSwitchInfo->ROUTE_P == 1 && pInfo->OVERLAP )
// 					{
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
// 					else if ( bHas2ndReferPoint == TRUE )
// 					{
// 						if ( (strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && pSwitchInfo2->ROUTE_M == 1 && pInfo->OVERLAP )
// 						{
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 						else if ( (strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && pSwitchInfo2->ROUTE_P == 1 && pInfo->OVERLAP )
// 						{
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 						else
// 						{
// 							m_nColorC = MY_RED;
// 							if (/*!bRoute || */!pInfo->OVERLAP) 
// 								m_nColorNR = MY_RED;
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 								m_nColorNR += 0x1000;
// 							}
// 						}
// 					}
// 					else
// 					{
// 						m_nColorC = MY_RED;
// 						if (/*!bRoute || */!pInfo->OVERLAP) 
// 							m_nColorNR = MY_RED;
// 						// 추후에 고려			if ( pInfo->ALARM ) m_nColorC |= 0x1000;
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 							m_nColorNR += 0x1000;
// 						}
// 					}
				}
				else if ( strSwName == "FFF" )
				{
					if (!pInfo->OVERLAP || !pMySwitchInfo->WR_P) 
					{
						m_nColorC = MY_RED;
						m_nColorNR = MY_RED;
					}
					if (pInfo->EMREL) {
						m_nColorC += 0x1000;
						m_nColorNR += 0x1000;
					}
				}
				else
				{
					m_nColorC = MY_RED;
					if (/*!bRoute || */!pInfo->OVERLAP) 
					m_nColorNR = MY_RED;
// 추후에 고려		if ( pInfo->ALARM ) m_nColorC |= 0x1000;
					if (pInfo->EMREL) {
						m_nColorC += 0x1000;
						m_nColorNR += 0x1000;
					}
				}
			}

//			
//			m_nColorC = MY_RED;
//
//			if (/*!bRoute || */!pInfo->OVERLAP) 
//				m_nColorNR = MY_RED;
// 추후에 고려			if ( pInfo->ALARM ) m_nColorC |= 0x1000;
//			if (pInfo->EMREL) {
//                m_nColorC += 0x1000;
//			}
//
		}
		else if ( !pInfo->ROUTE && !(memcmp(m_sPointName, "HAND", 4) == 0)) //  진로 쇄정이면,  //	HAND의 경우 무조건 점유 아니면 그레이
		{ 

			// 아카우라 124B번 전철기 그린 표시 제거를 위해 삽입 
// 			if ( pSwitch ) 
// 			{
// 				pSwInfo = (ScrInfoSwitch*)pSwitch->m_pData;
// 
// 				if ( strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R')
// 				{
// 					if ( pSwitchInfo->WR_M == 1)
// 					{
// 						if ( bHas2ndReferPoint == TRUE)
// 						{
// 							if ( (strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && pSwitchInfo2->WR_M == 1)
// 							{
// 								m_nColorC = MY_GREEN;
// 								if (pInfo->EMREL) {
// 									m_nColorC += 0x1000;
// 								}
// 							}
// 							else if ( (strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && pSwitchInfo2->WR_P == 1)
// 							{
// 								m_nColorC = MY_GREEN;
// 								if (pInfo->EMREL) {
// 									m_nColorC += 0x1000;
// 								}
// 							}
// 						}
// 						else
// 						{
// 							m_nColorC = MY_GREEN;
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 							}
// 						}
// 					}
// 				}
// 				else if ( pSwitchInfo != NULL )
// 				{
// 					if ( pSwitchInfo->ROUTE_M == 1 )
// 					{
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 						}
// 					}
// 					else
// 					{
// 						m_nColorC = MY_GREEN;
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 						}
// 					}
//				}
//	HAND의 경우 무조건 점유 아니면 그레이
//				else if ( (memcmp(m_sPointName, "HAND", 4) == 0 ) && pSwInfo->WR_P == 1 )	// HAND POINT의 개통방향이 탈선하는 방향일때 진로표시 안함
//				{
//					if (pInfo->EMREL) {
//						m_nColorC += 0x1000;
//				}
//
//	 예외케이스제거
//				strcpy(m_sPointName,"124B");
//				strcpy(m_sPointName2,"108");
//				strcpy(m_sPointName3,"133B");
//
//				if ( (m_strStationName.Find ( "AKHA",0 ) >=0) && ( pSwInfo->KR_P == 1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName2)==NULL)) ) {
//					m_nColorC = MY_BLUE;
//					m_nColorNR = MY_BLUE;
//				} else if ( (m_strStationName.Find ( "AKHA",0 ) >=0) && ( pSwInfo->KR_M == 1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName2)==NULL)) ) {
//					m_nColorC = MY_GREEN;
//					//m_nColorNR = MY_GREEN;
//					if (pInfo->EMREL) {
//						m_nColorC += 0x1000;
//					}
//				} else if ( (m_strStationName.Find ( "BYPA",0 ) >=0) && ( pSwInfo->KR_P == 1) && ((strcmp(pSwitch->mName,m_sPointName)==NULL)||(strcmp(pSwitch->mName,m_sPointName3)==NULL)) ) {
//					m_nColorC = MY_BLUE;
//					//m_nColorNR = MY_GREEN;
//					if (pInfo->EMREL) {
//						m_nColorC += 0x1000;
//					}
				if ( pSwitchInfo != NULL && bIsRoute == FALSE )
				{
					if (pInfo->EMREL) 
					{
						m_nColorC += 0x1000;
					}
				} 
				else 
				{
					if ( strSwName == "FFF" )
					{
						if ( pMySwitchInfo->WR_M == 1 )
						{
							m_nColorC = MY_GREEN;
							if (pInfo->EMREL) 
							{
								m_nColorC += 0x1000;
							}
						}
					}
					else
					{
						m_nColorC = MY_GREEN;
						if (pInfo->EMREL) {
							m_nColorC += 0x1000;
						}
					}
				}
// 				else {
// 				if ( pSwitchInfo != NULL )
// 				{
// 					if ( strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R')
// 					{
// 						if ( pSwitchInfo->KR_M == 1)
// 						{
// 							if ( bHas2ndReferPoint == TRUE)
// 							{
// 								if ( (strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && pSwitchInfo2->KR_M == 1)
// 								{
// 									m_nColorC = MY_GREEN;
// 									if (pInfo->EMREL) {
// 										m_nColorC += 0x1000;
// 									}
// 								}
// 								else if ( (strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && pSwitchInfo2->KR_P == 1)
// 								{
// 									m_nColorC = MY_GREEN;
// 									if (pInfo->EMREL) {
// 										m_nColorC += 0x1000;
// 									}
// 								}
// 							}
// 							else
// 							{
// 								m_nColorC = MY_GREEN;
// 								if (pInfo->EMREL) {
// 									m_nColorC += 0x1000;
// 								}
// 							}
// 						}
// 					}
// 					else
// 					{
// 						if ( pSwitchInfo->ROUTE_M == 1 )
// 						{
// 							if (pInfo->EMREL) {
// 								m_nColorC += 0x1000;
// 							}
// 						}
// 						else
// 						{
// 							if ( bHas2ndReferPoint == TRUE )
// 							{
// 								if ( (strSwName2.Right(1) == 'A' || strSwName2.Right(1) == 'N') && pSwitchInfo2->KR_P )
// 								{
// 									m_nColorC = MY_GREEN;
// 									if (pInfo->EMREL) {
// 										m_nColorC += 0x1000;
// 									}
// 								}
// 								else if ( (strSwName2.Right(1) == 'B' || strSwName2.Right(1) == 'R') && pSwitchInfo2->KR_M )
// 								{
// 									m_nColorC = MY_GREEN;
// 									if (pInfo->EMREL) {
// 										m_nColorC += 0x1000;
// 									}
// 								}
// 							}
// 							else
// 							{
// 								m_nColorC = MY_GREEN;
// 								if (pInfo->EMREL) {
// 									m_nColorC += 0x1000;
// 								}
// 							}
// 						}
// 					}
// 				}
// 				else if ( strSwName == "FFF" )
// 				{
// 					if ( pMySwitchInfo->WR_M == 1 )
// 					{
// 						m_nColorC = MY_GREEN;
// 						if (pInfo->EMREL) {
// 							m_nColorC += 0x1000;
// 						}
// 					}
// 				}
// 				else
// 				{
// 					m_nColorC = MY_GREEN;
// 					if (pInfo->EMREL) {
// 						m_nColorC += 0x1000;
// 						}
// 				}
// 
// 
// 			}
        }
		else if ( pInfo->TKREQUEST && !(memcmp(m_sPointName, "HAND", 4) == 0 /* && pSwInfo->KR_P == 1 */))	// HAND POINT의 개통방향이 탈선하는 방향일때 진로표시 안함
		{
            if (pSwInfo) 
			{
// 					if ( strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R')
// 					{
// 						if ( pSwInfo->ROUTELOCK && ( pSwitchInfo->REQUEST_M || pSwitchInfo->WR_M ) )
// 							m_nColorC = MY_YELLOW;
// 					}
// 					else// if ( strSwName != "HAND")
// 					{
// 						if ( pSwInfo->ROUTELOCK && ( pSwitchInfo->REQUEST_P || pSwitchInfo->WR_P ))
// 							m_nColorC = MY_YELLOW;
// 					}
					//if ( pSwitchInfo->WR_M != 1 && strSwName.Right(1) != 'B')
					//{
					//	if ( pSwInfo->ROUTELOCK )
					//		m_nColorC = MY_YELLOW;
					//}
					//else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B')
					//{
					//	if ( pSwInfo->ROUTELOCK )
					//		m_nColorC = MY_YELLOW;
					//}
				if ( strSwName == "FFF" )
				{
					if ( pSwInfo->ROUTELOCK && pSwInfo->WR_M )
					{
						m_nColorC = MY_YELLOW;
					}
				}
				else if ( bIsRoute == TRUE && pSwInfo->ROUTELOCK == TRUE)
				{
					m_nColorC = MY_YELLOW;
				}
           }
            else
			{
				if ( strSwName == "FFF" )
				{
					if ( pMySwitchInfo->WR_M == 1 )
						m_nColorC = MY_YELLOW;
				}
				else if ( bIsRoute == TRUE)
				{
					m_nColorC = MY_YELLOW;
				}
// 				else if (strSwName.Right(1) == 'A' || strSwName.Right(1) == 'N')
// 				{
// 					if ( bHas2ndReferPoint == TRUE )
// 					{
// 						if ( strSwName2.Right == '')
// 						{
// 							int a;
// 						}
// 					}
// 					else
// 					{
// 						if (pSwitchInfo->KR_M || pSwitchInfo->ROUTE_M )
// 							m_nColorC = MY_YELLOW;
// 					}
// 				}
// 				else if (strSwName.Right(1) == 'B' || strSwName.Right(1) == 'R')
// 				{
// 					if (pSwitchInfo->KR_M || pSwitchInfo->ROUTE_M )
// 						m_nColorC = MY_YELLOW;
// 				}
// 				else if(pSwitchInfo == NULL)
// 					m_nColorC = MY_YELLOW;
// 				else if ( pSwitchInfo->KR_P == 1 || pSwitchInfo->ROUTE_P )
// 					m_nColorC = MY_YELLOW;				
			}
        }
		else
		{
			m_nColorC = MY_BLUE;
			if (pInfo->EMREL  && !(memcmp(m_sPointName, "HAND", 4) == 0)) //	HAND의 경우 무조건 점유 아니면 그레이 
			{
				m_nColorC = MY_YELLOW;
				m_nColorC += 0x1000;
			}
		}

		// 2006.11.16 Overlap 구간의 초록색 표시를 위해 삽입함...
		CString strDrawingTC = mName;
		m_bOverlapRoute = 0;

		int iFindDot = strDrawingTC.Find('.');
		CString strDrawingTCRemovedDot;
		if (iFindDot > 0)
			strDrawingTCRemovedDot = strDrawingTC.Left(iFindDot);
		else
			strDrawingTCRemovedDot = strDrawingTC;

		if (strDrawingTC.Find("W2325AT") == 0)
			int strDrawingTC = 1;

		if ((pInfo->LeftToRightRouteNo || pInfo->RightToLeftRouteNo) && ( m_nColorC == MY_BLUE || m_nColorC == MY_FLASHING ))
		{
			BOOL bIsOverlapRoute = FALSE;
			
			for( int nCounter = 0; nCounter < MAX_OVERLAP; nCounter++)
			{
				if ( pInfo->LeftToRightRouteNo == iRouteNumberForDrawingOverlap[nCounter] || pInfo->RightToLeftRouteNo == iRouteNumberForDrawingOverlap[nCounter])
				{
					bIsOverlapRoute = TRUE;
					break;
				}
			}
			
			if ( m_strOverlapRoute == "" )
				bIsOverlapRoute = TRUE;
			
			if ( bIsOverlapRoute == TRUE )
			{
				m_nColorC = MY_YELLOW;
			}
		}
/////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.( 20130930 추가변경.)
// 		if (m_nColorC == MY_BLUE)
// 		{
// 			BYTE nFindSwitchLock = 0;
// 			for (int i=0; i<TrackObject.GetSize(); i++ ) 
// 			{
// 				CBmpTrack *pObj = (CBmpTrack *)TrackObject.GetAt( i );
// 
// 				ScrInfoTrack* pObjTrackInfo = (ScrInfoTrack*)pObj->m_pData;
// 
// 				CString sTname = pObj->mName;
// 				BYTE nFind = sTname.Find('.');
// 				CString sTname1 = sTname.Left(nFind);
// 
// 				CString strReferenceTC = pObj->mName;
// 				CString strReferenceTCRemovedDot;
// 				if (strReferenceTC.Find("W2325AT") == 0)
// 					int debug = 1;
// 
// 				iFindDot = strReferenceTC.Find('.');
// 				if (iFindDot > 0)
// 					strReferenceTCRemovedDot = strReferenceTC.Left(iFindDot);
// 				else
// 					strReferenceTCRemovedDot = strReferenceTC;
// 
// 				ScrInfoSwitch* pInnerSwitchInfo = NULL;
// 				if (strcmp(sTname1, str)==NULL && strcmp(mName, pObj->mName)!=NULL )
// 				{
// 					if (pObj->pSwitch)
// 					{
// 						pInnerSwitchInfo = (ScrInfoSwitch*)pObj->pSwitch->m_pData;
// 						if ( (pInnerSwitchInfo->ROUTE_P || pInnerSwitchInfo->ROUTE_M) )
// 							nFindSwitchLock = 1;
// 					}
// 				}
// 				
// 				if (pSwitch)
// 				{
// 					pInnerSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
// 					//if (pInnerSwitchInfo->ROUTE_P)
// 					//{
// 						if ( ((C==pObj->C || C==pObj->N) || (N==pObj->C || N==pObj->N) || (R==pObj->C || R==pObj->N) ) 
// 							&& (pObj->m_bOverlapRoute==1) && pInfo->ROUTEDIR)
// 						{
// 							if (pInnerSwitchInfo->ROUTE_M || pInnerSwitchInfo->ROUTE_P)
// 								m_bOverlapRoute = 1;
// 							else if ( strDrawingTCRemovedDot == strReferenceTCRemovedDot)
// 								m_bOverlapRoute = 1;
// 						}
// 						else if( m_strStationName.Find("@Standard") > 0 &&
// 							R==pObj->R && (pObj->m_bOverlapRoute==1) && (pInnerSwitchInfo->ROUTE_M || pInnerSwitchInfo->ROUTE_P) && pInfo->ROUTEDIR)
// 							m_bOverlapRoute = 1;
// 						
// 						if (pObj->pSwitch && pInnerSwitchInfo->ROUTE_M && pInfo->ROUTEDIR)
// 						{
// 							if ( ((strcmp(sTname1, str) != NULL) && R==pObj->R) && (pObj->m_bOverlapRoute==1) )
// 								m_bOverlapRoute = 1;
// 						}
// 						
// 						if ( ((pInnerSwitchInfo->KR_P != pInnerSwitchInfo->WR_P) || (pInnerSwitchInfo->KR_M != pInnerSwitchInfo->WR_M) ||
// 							  (pInnerSwitchInfo->KR_P==0 && pInnerSwitchInfo->KR_M==0)) && ( strReferenceTCRemovedDot != strDrawingTCRemovedDot ) )
// 							m_bOverlapRoute = 0;
// 					//}
// 				}
// 				else if (pObj->pSwitch && (strcmp(sTname1, str)==NULL) )
// 				{
// 					pInnerSwitchInfo = (ScrInfoSwitch*)pObj->pSwitch->m_pData;
// 					if (pInnerSwitchInfo->WR_P)
// 					{
// 						if ( ((C==pObj->N || N==pObj->N) || (C==pObj->C || N==pObj->C)) && (pObj->m_bOverlapRoute==1) )
// 							m_bOverlapRoute = 1;
// 					}
// 					else if (pInnerSwitchInfo->WR_M)
// 					{
// 						if ( ((C==pObj->R || N==pObj->R || R==pObj->R) || (C==pObj->C || N==pObj->C)) && pObj->m_bOverlapRoute)
// 							m_bOverlapRoute = 1;
// 					}
// 					else
// 						m_bOverlapRoute = 0;
// 				}
// 				else
// 				{
// 					if ( (strcmp(sTname1, str) != NULL) && ((C==pObj->C || N==pObj->C) || (C==pObj->N || N==pObj->N)) || N==pObj->R)
// 					{
// 						/*
// 						ScrInfoTrack *pCNRInfoTrack = (ScrInfoTrack*)pObj->m_pData;
// 						if ( (((pCNRInfoTrack != NULL) && !pCNRInfoTrack->ROUTE) || nFindSwitchLock==1) && pObj->m_nColorC==MY_GREEN && pInfo->ROUTEDIR)
// 						{
// 							ScrInfoSignal* pInnerSignalInfo = NULL;
// 							BYTE SignalFind = 0;
// 							for (BYTE j=0; j<mSignalCount; j++)
// 							{
// 								pInnerSignalInfo = (ScrInfoSignal*)pSignal[j]->m_pData;
// 								if (pInnerSignalInfo->DESTID)
// 								{
// 									SignalFind = 1;
// 									break;
// 								}
// 							}
// 							if (!SignalFind)
// 							{
// 								m_bOverlapRoute = 1;
// 							}
// 							//else
// 							//	m_bOverlapRoute = 0;
// 						}
// 						else
// 							m_bOverlapRoute = 0;
// 						*/
// 						
// 						// 2007.3.3 수정함....
// 
// 						if ( (( pObj->m_nColorC & 0xEFFF ) == MY_GREEN || ( pObj->m_nColorC & 0xEFFF ) == MY_YELLOW 
// 							|| (( pObj->m_nColorC & 0xEFFF ) == MY_RED && pObj->m_bOverlapRoute == TRUE && pObjTrackInfo->ROUTE == 1 )) && pInfo->ROUTEDIR )
// 							m_bOverlapRoute = 1;
// 						else if ( (( pObj->m_nColorC & 0xEFFF ) == MY_GREEN || ( pObj->m_nColorC & 0xEFFF ) == MY_YELLOW 
// 							|| (( pObj->m_nColorC & 0xEFFF ) == MY_RED && pObjTrackInfo->ROUTE == 0 )) && pInfo->ROUTEDIR )
// 							m_bOverlapRoute = 1;
// 						else
// 							m_bOverlapRoute = 0;
// 						
// 					}
// 				}
// 
// 
// 
// 				if (m_bOverlapRoute)
// 				{
// 					BOOL bIsOverlapRoute = FALSE;
// 
// 					for( int nCounter = 0; nCounter < 20; nCounter++)
// 					{
// 						if ( pInfo->LeftToRightRouteNo == iRouteNumberForDrawingOverlap[nCounter] || pInfo->RightToLeftRouteNo == iRouteNumberForDrawingOverlap[nCounter])
// 						{
// 							bIsOverlapRoute = TRUE;
// 							break;
// 						}
// 					}
// 
// 					if ( m_strOverlapRoute == "" )
// 						bIsOverlapRoute = TRUE;
// 
// 					if ( bIsOverlapRoute == TRUE )
// 					{
// 						m_nColorC = MY_YELLOW;
// 						break;
// 					}
// 				}
// 
// 				if ( m_strStationName.Find("@StandardOf13") > 0 )
// 				{
// 					BOOL bIsThisUSOverlap = FALSE;
// 					BOOL bIsThisDSOverlap = FALSE;
// 					for ( int jj = 0; jj < 3; jj++ )
// 					{
// 						if ( bUSDestinationTrackIsRouted[jj] == TRUE && pInfo->US )
// 						{
// 							if ( m_strReferencePointUS[jj].Find('A') > 0 )
// 							{
// 								if( pReferenceSwitchUSInfo[jj]->WR_P )
// 								{
// 									bIsThisUSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else if ( m_strReferencePointUS[jj].Find('B') > 0)
// 							{
// 								if( pReferenceSwitchUSInfo[jj]->WR_M )
// 								{
// 									bIsThisUSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else
// 							{
// 								bIsThisUSOverlap = TRUE;
// 								break;
// 							}
// 						}
// 						if ( bDSDestinationTrackIsRouted[jj] == TRUE && pInfo->DS )
// 						{
// 							if ( m_strReferencePointDS[jj].Find('A') > 0 )
// 							{
// 								if( pReferenceSwitchDSInfo[jj]->WR_P )
// 								{
// 									bIsThisDSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else if ( m_strReferencePointDS[jj].Find('B') > 0)
// 							{
// 								if( pReferenceSwitchDSInfo[jj]->WR_M )
// 								{
// 									bIsThisDSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else
// 							{
// 								bIsThisDSOverlap = TRUE;
// 								break;
// 							}
// 						}
// 					}
// 
// 					int iMyXCoordinate = ( C.x + N.x ) / 2;
// 					int iXCoordinateOfPointOfReferenceTrack = 0;
// 					for (int i=0; i<TrackObject.GetSize(); i++ ) 
// 					{
// 						CBmpTrack *pObj2 = (CBmpTrack *)TrackObject.GetAt( i );
// 						CString strPointOfReferenceTrack = pObj2->pSwitch->mName;
// 
// 						if ( strPointOfReferenceTrack == strSwName )
// 						{
// 							iXCoordinateOfPointOfReferenceTrack = ( pObj2->C.x + pObj2->N.x ) / 2;
// 							break;
// 						}
// 					}
// 
// 					if ( bIsThisUSOverlap )
// 					{
// 						if ( pSwitchInfo != NULL )
// 						{
// 							if ( pSwitchInfo->WR_P == 1 && strSwName.Right(1) != 'B' )
// 								m_nColorC = MY_YELLOW;
// 							else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B' )
// 								m_nColorC = MY_YELLOW;
// 							else if ( pSwitchInfo->WR_P && strSwName.Right(1) == 'B' && ( iMyXCoordinate >= iXCoordinateOfPointOfReferenceTrack ))
// 								m_nColorC = MY_YELLOW;
// 						}
// 						else
// 						{
// 								m_nColorC = MY_YELLOW;
// 						}
// 					}
// 					else if ( bIsThisDSOverlap )
// 					{
// 						if ( pSwitchInfo != NULL )
// 						{
// 							if ( pSwitchInfo->WR_P == 1 && strSwName.Right(1) != 'B' )
// 								m_nColorC = MY_YELLOW;
// 							else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B' )
// 								m_nColorC = MY_YELLOW;
// 							else if ( pSwitchInfo->WR_P && strSwName.Right(1) == 'B' && ( iMyXCoordinate <= iXCoordinateOfPointOfReferenceTrack ))
// 								m_nColorC = MY_YELLOW;
// 						}
// 						else
// 						{
// 								m_nColorC = MY_YELLOW;
// 						}
// 					}
// 					
// 					if ( bIsThisDSOverlap == TRUE || bIsThisUSOverlap == TRUE )
// 						break;  // 2007.8.17  다른 궤도 처리과정에서 0으로의 Data갱신을 막기 위해 추가..
// 				}
// 				else if (m_bOverlapRoute)
// 				{
// 					BOOL bIsThisUSOverlap = FALSE;
// 					BOOL bIsThisDSOverlap = FALSE;
// 					for ( int jj = 0; jj < 3; jj++ )
// 					{
// 						if ( bUSDestinationTrackIsRouted[jj] == TRUE && pInfo->US )
// 						{
// 							if ( m_strReferencePointUS[jj].Find('A') > 0 )
// 							{
// 								if( pReferenceSwitchUSInfo[jj]->WR_P )
// 								{
// 									bIsThisUSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else if ( m_strReferencePointUS[jj].Find('B') > 0)
// 							{
// 								if( pReferenceSwitchUSInfo[jj]->WR_M )
// 								{
// 									bIsThisUSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else
// 							{
// 								bIsThisUSOverlap = TRUE;
// 								break;
// 							}
// 						}
// 						if ( bDSDestinationTrackIsRouted[jj] == TRUE && pInfo->DS )
// 						{
// 							if ( m_strReferencePointDS[jj].Find('A') > 0 )
// 							{
// 								if( pReferenceSwitchDSInfo[jj]->WR_P )
// 								{
// 									bIsThisDSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else if ( m_strReferencePointDS[jj].Find('B') > 0)
// 							{
// 								if( pReferenceSwitchDSInfo[jj]->WR_M )
// 								{
// 									bIsThisDSOverlap = TRUE;
// 									break;
// 								}
// 							}
// 							else
// 							{
// 								bIsThisDSOverlap = TRUE;
// 								break;
// 							}
// 						}
// 					}
// 
// 					if ( m_strStationName.Find("@Standard") < 0 )
// 					{
// 						if ( pSwitchInfo != NULL )
// 						{
// 							if ( pSwitchInfo->WR_P == 1 && strSwName.Right(1) != 'B' )
// 								m_nColorC = MY_YELLOW;
// 							else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B' )
// 								m_nColorC = MY_YELLOW;
// 						}
// 						else
// 						{
// 								m_nColorC = MY_YELLOW;
// 						}
// 					}
// 					else
// 					{
// 						int iMyXCoordinate = ( C.x + N.x ) / 2;
// 						int iXCoordinateOfPointOfReferenceTrack = 0;
// 						for (int i=0; i<TrackObject.GetSize(); i++ ) 
// 						{
// 							CBmpTrack *pObj2 = (CBmpTrack *)TrackObject.GetAt( i );
// 							CString strPointOfReferenceTrack = pObj2->pSwitch->mName;
// 
// 							if ( strPointOfReferenceTrack == strSwName )
// 							{
// 								iXCoordinateOfPointOfReferenceTrack = ( pObj2->C.x + pObj2->N.x ) / 2;
// 								break;
// 							}
// 						}
// 
// 						if ( bIsThisUSOverlap )
// 						{
// 							if ( pSwitchInfo != NULL )
// 							{
// 								if ( pSwitchInfo->WR_P == 1 && strSwName.Right(1) != 'B' )
// 									m_nColorC = MY_YELLOW;
// 								else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B' )
// 									m_nColorC = MY_YELLOW;
// 								else if ( pSwitchInfo->WR_P && strSwName.Right(1) == 'B' && ( iMyXCoordinate >= iXCoordinateOfPointOfReferenceTrack ))
// 									m_nColorC = MY_YELLOW;
// 							}
// 							else
// 							{
// 									m_nColorC = MY_YELLOW;
// 							}
// 						}
// 						else if ( bIsThisDSOverlap )
// 						{
// 							if ( pSwitchInfo != NULL )
// 							{
// 								if ( pSwitchInfo->WR_P == 1 && strSwName.Right(1) != 'B' )
// 									m_nColorC = MY_YELLOW;
// 								else if ( pSwitchInfo->WR_M == 1 && strSwName.Right(1) == 'B' )
// 									m_nColorC = MY_YELLOW;
// 								else if ( pSwitchInfo->WR_P && strSwName.Right(1) == 'B' && ( iMyXCoordinate <= iXCoordinateOfPointOfReferenceTrack ))
// 									m_nColorC = MY_YELLOW;
// 							}
// 							else
// 							{
// 									m_nColorC = MY_YELLOW;
// 							}
// 						}
// 					}
// 					break;  // 2007.8.17  다른 궤도 처리과정에서 0으로의 Data갱신을 막기 위해 추가..
// 				}
// 				/*
// 				CString sTname = pObj->mName;
// 
// 				BYTE nFind = sTname.Find('.');
// 				CString sTname1 = sTname.Left(nFind);
// 				if (strcmp(sTname1, str) == NULL)
// 				{
// 					ScrInfoSwitch* pInnerSwitchInfo = NULL;
// 					if (pObj->pSwitch)
// 					{
// 						pInnerSwitchInfo = (ScrInfoSwitch*)pObj->pSwitch->m_pData;
// 						if ( (pInnerSwitchInfo->ROUTE_P || pInnerSwitchInfo->ROUTE_M)  )
// 						{
// 							m_nColorC = MY_GREEN;
// 						}
// 					}
// 				}
// 				*/
// 
// 
// 			}
// 			
// 		}
// 		else if( m_nColorC == MY_RED )
// 		{
// 			BYTE nFindSwitchLock = 0;
// 			for (int i=0; i<TrackObject.GetSize(); i++ ) 
// 			{
// 				CBmpTrack *pObj = (CBmpTrack *)TrackObject.GetAt( i );
// 
// 				ScrInfoTrack* pObjTrackInfo = (ScrInfoTrack*)pObj->m_pData;
// 
// 				CString sTname = pObj->mName;
// 				BYTE nFind = sTname.Find('.');
// 				CString sTname1 = sTname.Left(nFind);
// 
// 				CString strReferenceTC = pObj->mName;
// 				CString strReferenceTCRemovedDot;
// 				if (strReferenceTC.Find("W2325AT") == 0)
// 					int debug = 1;
// 
// 				iFindDot = strReferenceTC.Find('.');
// 				if (iFindDot > 0)
// 					strReferenceTCRemovedDot = strReferenceTC.Left(iFindDot);
// 				else
// 					strReferenceTCRemovedDot = strReferenceTC;
// 
// 				ScrInfoSwitch* pInnerSwitchInfo = NULL;
// 				if (strcmp(sTname1, str)==NULL && strcmp(mName, pObj->mName)!=NULL )
// 				{
// 					if (pObj->pSwitch)
// 					{
// 						pInnerSwitchInfo = (ScrInfoSwitch*)pObj->pSwitch->m_pData;
// 						if ( (pInnerSwitchInfo->ROUTE_P || pInnerSwitchInfo->ROUTE_M) )
// 							nFindSwitchLock = 1;
// 					}
// 				}
// 				
// 				if (pSwitch)
// 				{
// 					pInnerSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
// 					//if (pInnerSwitchInfo->ROUTE_P)
// 					//{
// 						if ( ((C==pObj->C || C==pObj->N) || (N==pObj->C || N==pObj->N) || (R==pObj->C || R==pObj->N) ) 
// 							&& (pObj->m_bOverlapRoute==1) && pInfo->ROUTEDIR)
// 						{
// 							if (pInnerSwitchInfo->ROUTE_M || pInnerSwitchInfo->ROUTE_P)
// 								m_bOverlapRoute = 1;
// 							else if ( strDrawingTCRemovedDot == strReferenceTCRemovedDot)
// 								m_bOverlapRoute = 1;
// 						}
// 						else if( m_strStationName.Find("@Standard") > 0 &&
// 							R==pObj->R && (pObj->m_bOverlapRoute==1) && (pInnerSwitchInfo->ROUTE_M || pInnerSwitchInfo->ROUTE_P) && pInfo->ROUTEDIR)
// 							m_bOverlapRoute = 1;
// 						
// 						if (pObj->pSwitch && pInnerSwitchInfo->ROUTE_M && pInfo->ROUTEDIR)
// 						{
// 							if ( ((strcmp(sTname1, str) != NULL) && R==pObj->R) && (pObj->m_bOverlapRoute==1) )
// 								m_bOverlapRoute = 1;
// 						}
// 						
// 						if ( ((pInnerSwitchInfo->KR_P != pInnerSwitchInfo->WR_P) || (pInnerSwitchInfo->KR_M != pInnerSwitchInfo->WR_M) ||
// 							  (pInnerSwitchInfo->KR_P==0 && pInnerSwitchInfo->KR_M==0)) && ( strReferenceTCRemovedDot != strDrawingTCRemovedDot ) )
// 							m_bOverlapRoute = 0;
// 					//}
// 				}
// 				else if (pObj->pSwitch && (strcmp(sTname1, str)==NULL) )
// 				{
// 					pInnerSwitchInfo = (ScrInfoSwitch*)pObj->pSwitch->m_pData;
// 					if (pInnerSwitchInfo->WR_P)
// 					{
// 						if ( ((C==pObj->N || N==pObj->N) || (C==pObj->C || N==pObj->C)) && (pObj->m_bOverlapRoute==1) )
// 							m_bOverlapRoute = 1;
// 					}
// 					else if (pInnerSwitchInfo->WR_M)
// 					{
// 						if ( ((C==pObj->R || N==pObj->R || R==pObj->R) || (C==pObj->C || N==pObj->C)) && pObj->m_bOverlapRoute)
// 							m_bOverlapRoute = 1;
// 					}
// 					else
// 						m_bOverlapRoute = 0;
// 				}
// 				else
// 				{
// 					if ( (strcmp(sTname1, str) != NULL) && ((C==pObj->C || N==pObj->C) || (C==pObj->N || N==pObj->N)) || N==pObj->R)
// 					{
// 						/*
// 						ScrInfoTrack *pCNRInfoTrack = (ScrInfoTrack*)pObj->m_pData;
// 						if ( (((pCNRInfoTrack != NULL) && !pCNRInfoTrack->ROUTE) || nFindSwitchLock==1) && pObj->m_nColorC==MY_GREEN && pInfo->ROUTEDIR)
// 						{
// 							ScrInfoSignal* pInnerSignalInfo = NULL;
// 							BYTE SignalFind = 0;
// 							for (BYTE j=0; j<mSignalCount; j++)
// 							{
// 								pInnerSignalInfo = (ScrInfoSignal*)pSignal[j]->m_pData;
// 								if (pInnerSignalInfo->DESTID)
// 								{
// 									SignalFind = 1;
// 									break;
// 								}
// 							}
// 							if (!SignalFind)
// 							{
// 								m_bOverlapRoute = 1;
// 							}
// 							//else
// 							//	m_bOverlapRoute = 0;
// 						}
// 						else
// 							m_bOverlapRoute = 0;
// 						*/
// 						
// 						// 2007.3.3 수정함....
// 						if ( (( pObj->m_nColorC & 0xEFFF ) == MY_GREEN || ( pObj->m_nColorC & 0xEFFF ) == MY_YELLOW 
// 							|| (( pObj->m_nColorC & 0xEFFF ) == MY_RED && pObj->m_bOverlapRoute == TRUE && pObjTrackInfo->ROUTE == 1 )) && pInfo->ROUTEDIR )
// 							m_bOverlapRoute = 1;
// 						else if ( (( pObj->m_nColorC & 0xEFFF ) == MY_GREEN || ( pObj->m_nColorC & 0xEFFF ) == MY_YELLOW 
// 							|| (( pObj->m_nColorC & 0xEFFF ) == MY_RED && pObjTrackInfo->ROUTE == 0 )) && pInfo->ROUTEDIR )
// 							m_bOverlapRoute = 1;
// 						else
// 							m_bOverlapRoute = 0;
// 						
// 					}
// 				}
// 			}
// 		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				



		if ( pSwInfo ) 
		{
			if ( pSwInfo->WR_P ) 
				nodetype = 3;	                // C + N  (노멀 방향 트렉)
			else 
				nodetype = 5;					// C + R (리버스 방향 트렉)

			pSwitch->mState = nodetype;

			if ( pSwInfo->WR_P != pSwInfo->KR_P || pSwInfo->WR_M != pSwInfo->KR_M
				 || (pSwInfo->KR_P == pSwInfo->KR_M) || pSwInfo->SWFAIL) 
			{
				if (m_nColorC == MY_YELLOW /*|| m_nColorC == MY_RED*/) 
					m_nColorC |= 0x1000;
				else 
					m_nColorC |= 0x2000;	// YELLOW FLASH
				SelectColor = m_nColorC;

				bSwitchMismatch = TRUE;

// WR-N/R 모두 0 일때 KR-N/R에 의해 스위치 방향 설정
				if ( !pSwInfo->WR_P && !pSwInfo->WR_M ) 
				{
					if ( pSwInfo->KR_P ) 
						nodetype = 3;	// C + N
					else 
						nodetype = 5;	// C + R

					pSwitch->mState = nodetype;
				}
			}
			CString strTemp;
			strTemp.Format("%s",pSwitch->mName);
			if(pSwitch->m_bHandPointSW)
			{
// 				if ( (m_strStationName.Find ( "AKHA",0 ) >= 0) && (strTemp == "124A" || strTemp == "124B") ) {
// 
// 				}
// 				else if ( m_strStationName.Find("@Standard") > 0 && ( strTemp.Find("25") >= 0 ||  strTemp.Find("26") >= 0 )) {
// 				}
// 				else {
//					nodetype = 3;	// 2013.03.21 왜그랬는지 모르지만 일단 막자. 전철기 모양 변해야지 ...왜 수동은 안변하게 해놔서 사람 헷갈리게 만드냐..
					if ( pSwInfo->KR_P && !pSwInfo->KR_M)
						nodetype = 3;
					else if ( pSwInfo->KR_M && !pSwInfo->KR_P )
						nodetype = 5;
					else
						nodetype = 3;

					pSwitch->mState = nodetype;
// 				}
			}

// Not drawing for a track with Derailleur.
// 2006.04.20 요구중에 황색 표시 안되도록 변경. 
			if ( (/*!pInfo->ROUTE*/pInfo->OVERLAP || pInfo->TKREQUEST) && (pSwInfo->ROUTE_P || pSwInfo->ROUTE_M) && m_bLastTrack)
			{
				if (pSwitchInfo)
				{
					if ((m_strStationName.Find ( "AKHA",0 ) >=0)&&(strcmp(SwitchName,m_sPointName)==NULL)) {
					} else {
					
						if (pSwitchInfo->ROUTE_M && (SwitchName[2] == 'B' || SwitchName[3] == 'B') ) 
						{	
							m_nColorNR = MY_BLUE;
							m_nColorC  = MY_BLUE;
						}
						else if ( (pSwitchInfo->ROUTE_P && (SwitchName[2] == 'A' || SwitchName[3] == 'A')) && (pInfo->TRACK || pInfo->OVERLAP)) 
						{	
							m_nColorNR = MY_BLUE;
							m_nColorC  = MY_BLUE;
						}
					}
				}
				else
				{
					if ( (pSwInfo->ROUTE_P && (pSwitch->mName[2] == 'B' || SwitchName[3] == 'B') ) && (pInfo->TRACK || pInfo->OVERLAP) )
					{	
						m_nColorNR = MY_BLUE;
						m_nColorC  = MY_BLUE;
					}
				}
			}
		}
		else if (pSwitchInfo)
		{
			if (m_bLastTrack)
			{
				if (SwitchName[2] == 'A' || SwitchName[3] == 'A')
				{
					if (pSwitchInfo->ROUTE_P)
					{
						m_nColorNR = MY_BLUE;
						m_nColorC  = MY_BLUE;
					}
					else if ( ( !pInfo->TRACK && pSwitchInfo->KR_P && pMySwitchInfo->ROUTE_P )
					          || ( !pInfo->TRACK && pMySwitchInfo->KR_M && pMySwitchInfo->ROUTE_M ) )
					{
						m_nColorNR = MY_BLUE;
						m_nColorC  = MY_BLUE;
					}
				}
				else if ( (SwitchName[2] == 'B' || SwitchName[3] == 'B') && pSwitchInfo->ROUTE_M)
				{
					m_nColorNR = MY_BLUE;
					m_nColorC  = MY_BLUE;
				}
			}
			else
			{
				if ( (pSwitchInfo->ROUTE_M && (SwitchName[2] == 'B' || SwitchName[3] == 'B') ) && (pInfo->TRACK || pInfo->OVERLAP) )
				{	
//					m_nColorNR = MY_BLUE;
//					m_nColorC  = MY_BLUE;
				}
				//2006.05.15  단동 전철기 때문에 'A' 조건은 삭제했음....
				else if ( (pSwitchInfo->ROUTE_P /*&& SwitchName[2] == 'A'*/ ) && (pInfo->TRACK || pInfo->OVERLAP) )
				{
					CString bufName = mName;
					if ( bufName != "W35A36A37AT.4" && bufName != "W2325BT.2")	// else if문이 왜있는지 몰라 일단 임시처리 하였음
					{
//						m_nColorNR = MY_BLUE; //2012.11.07 - 일단 막았어요..
//						m_nColorC  = MY_BLUE;
					}
				}
			}
		}
	}
#endif
	POSITION pos;
	CBmpTrackCell *pCell;

	if( bAllDrawTrack || !bRoute || IsConnect(NODE_CTYPE) ) // || bSwitchMismatch ) 
		SelectColor = m_nColorC;   // 점유인가 , 연결된 트랙이면 연결트랙 정보 에 따라 	

	if ( bSwitchMismatch ) 
		SelectColor |= 0x1000;		// flash 
	m_nColorC = MY_BLUE;	

	if( strlen(mName) == 0 || mName[0] == '?' )   {						// 무정보 track
		SelectColor = m_nColorNR = m_nColorC = MY_GRAY;
	}
	UINT nTextBackColor = m_nColorNR;
	for( pos = mCellInfoList.GetHeadPosition(); pos != NULL; )
	{		
		pCell = (CBmpTrackCell *)mCellInfoList.GetNext( pos );				
		if ( pCell ) {
			if( pCell->mCellNo == mJoinCellNo )   {							// join 셀인 경우 
				UINT vectortype=0;		
				if (nodetype & NODE_CTYPE)
					 vectortype |= mJoinCtypeVector;
				if (nodetype & NODE_NTYPE)
					 vectortype |= mJoinNtypeVector;
				if (nodetype & NODE_RTYPE)
					vectortype |=  mJoinRtypeVector;
				pCell->Draw( SelectColor, vectortype, m_bLastTrack );
				if (pCell->mPos == mNamePos1) {
					nTextBackColor = SelectColor;
				}
			}
			else if ( pCell->mNodeType & nodetype )	// 연결된 셀
			{
				pCell->Draw( SelectColor,0,m_bLastTrack );
				if (pCell->mPos == mNamePos1) 
				{
					nTextBackColor = SelectColor;
				}
			}
			else	{														// 연결 안된 셀
				for(int i=0 ; i<3 ;i++)	              // 연결 안된 셀중 정위반위에 따라 처리해야할셀
				{									 
					if(mSpCellNo[i] == pCell->mCellNo)	
					{
						pCell->Draw( m_nColorNR, mSpCellVector[i] ,m_bLastTrack); 
						break;
					}
				}
				if(i==3) 
				{
					pCell->Draw( m_nColorNR,0,0); 
				}
			}
		}
	}



	m_nColorC = SelectColor;  // 2006.11.16
	if (m_bOverlapRoute)
		m_nColorC = MY_YELLOW;




	CBmpTrackCell::m_TrackBmp.DrawLong();

	if(pSwitch)		pSwitch->Draw(pDC);
	if(pSwitch2)		pSwitch2->Draw(pDC);

	UINT i;
	for (i = 0; i<mButtonCount; i++) 
	{
		pButton[i]->Draw( pDC );
	}

	for (i = 0; i<mSignalCount; i++) 
	{
		pSignal[i]->Draw( pDC );
	}

	// 텍스트 출력 
	if(mNamePos1 == CPoint(0,0) ) return;
	pDC->SetBkMode(OPAQUE);

	COLORREF TextBack = RGB(0,0,0);
	COLORREF TextColor = RGB(255,255,255);

	ScrInfoTrack *pInfo = (ScrInfoTrack*)m_pData;
	
#ifdef _LSD
	switch (nTextBackColor & 0xfff) 
	{
	case MY_BLUE : TextBack = RGB( 192,192,192 ); TextColor = RGB( 0, 0, 0 ); break;
	case MY_RED : TextBack = RGB( 128,0,0 ); TextColor = RGB( 255, 255, 255 ); break;
	case MY_YELLOW : TextBack = RGB( 255,255,0 ); TextColor = RGB( 0, 0, 0 ); break;
	case MY_GREEN : TextBack = RGB( 0,96,64 ); TextColor = RGB( 255, 255, 255 ); break;
	case MY_GRAY : TextBack = RGB( 62,62,62 ); TextColor = RGB( 255, 255, 255 ); break;
	}
#else
	if ( pInfo != NULL && mName[0] != '?')
	{
// 		if ( pInfo->DISTURB || !_BlinkDuty)
// 		{
// 			switch (nTextBackColor & 0xfff) 
// 			{
// 			case MY_BLUE : TextBack = RGB( 192,192,192 ); TextColor = RGB( 0, 0, 0 ); break;
// 			case MY_RED : TextBack = RGB( 128,0,0 ); TextColor = RGB( 255, 255, 255 ); break;
// 			case MY_YELLOW : TextBack = RGB( 255,255,0 ); TextColor = RGB( 0, 0, 0 ); break;
// 			case MY_GREEN : TextBack = RGB( 0,96,64 ); TextColor = RGB( 255, 255, 255 ); break;
// 			case MY_GRAY : TextBack = RGB( 62,62,62 ); TextColor = RGB( 255, 255, 255 ); break;
// 			}
// 			if (!_BlinkDuty)
// 			{
// 				if ( nTextBackColor == MY_YELLOW )
// 				{
// 					TextBack = RGB(192,192,192);
// 					TextColor = RGB(0,0,0);
// 				}
// 			}
// 		}
// 		else
// 		{
// 			nTextBackColor = MY_YELLOW;
// 			TextBack = RGB( 255,255,0 );
// 			TextColor = RGB( 0, 0, 0 );
// 		}
		if ( pInfo->DISTURB )
		{
			switch (nTextBackColor & 0xfff) 
			{
			case MY_BLUE : TextBack = RGB( 192,192,192 ); TextColor = RGB( 0, 0, 0 ); break;
			case MY_RED : TextBack = RGB( 128,0,0 ); TextColor = RGB( 255, 255, 255 ); break;
			case MY_YELLOW : TextBack = RGB( 255,255,0 ); TextColor = RGB( 0, 0, 0 ); break;
			case MY_GREEN : TextBack = RGB( 0,96,64 ); TextColor = RGB( 255, 255, 255 ); break;
			case MY_GRAY : TextBack = RGB( 62,62,62 ); TextColor = RGB( 255, 255, 255 ); break;
			}
		}
		else
		{
			if ( _BlinkDuty )
			{
				nTextBackColor = MY_YELLOW;
				TextBack = RGB( 255,255,0 );
				TextColor = RGB( 0, 0, 0 );
			}
			else
			{
				TextBack = RGB(192,192,192);
				TextColor = RGB(0,0,0);
			}
		}
	}
	else
	{
		switch (nTextBackColor & 0xfff) 
		{
		case MY_BLUE : TextBack = RGB( 192,192,192 ); TextColor = RGB( 0, 0, 0 ); break;
		case MY_RED : TextBack = RGB( 128,0,0 ); TextColor = RGB( 255, 255, 255 ); break;
		case MY_YELLOW : TextBack = RGB( 255,255,0 ); TextColor = RGB( 0, 0, 0 ); break;
		case MY_GREEN : TextBack = RGB( 0,96,64 ); TextColor = RGB( 255, 255, 255 ); break;
		case MY_GRAY : TextBack = RGB( 62,62,62 ); TextColor = RGB( 255, 255, 255 ); break;
		}
	}
#endif
//

    pDC->SetBkColor( TextBack );
    pDC->SetTextColor(TextColor);


	RECT r;	
	CPoint Pos = mNamePos1;
	int l = strlen(m_szDrawName);
	str = m_szDrawName;
	short n = str.Find( 'X' );
	if ( n > 0 ) 
	{
		if ( str.Find("XX")>0 ) {
			str.Replace("XX","A ");
		}

        pDC->SetBkColor( RGB(0,0,0) );
        pDC->SetTextColor( RGB(192,192,192));

		if(C.x > N.x)
		{
			Pos.y -= 13;
		}
		else
		{
			Pos.y += 13;
		}
		::SetRect(&r,Pos.x-( _nDotPerCell *4), Pos.y-( _nDotPerCell /2+1), Pos.x+( _nDotPerCell *4), Pos.y+( _nDotPerCell /2)+1);
		MyDrawText( (LPCTSTR)str, -1, &r, /*DT_NOCLIP |*/ DT_CENTER | DT_VCENTER | DT_SINGLELINE, nTextBackColor);
	}
	else
	{
		::SetRect(&r,Pos.x-( _nDotPerCell *8), Pos.y-( _nDotPerCell /2+1), Pos.x+( _nDotPerCell *8), Pos.y+( _nDotPerCell /2)+1);
		if ( m_strStationName.Find("ARIKHOLA") == 0 && str.Find("1T") == 0 )
		{
			str+="<AX5>";
			MyDrawText( (LPCTSTR)str, -1, &r, /*DT_NOCLIP |*/ DT_CENTER | DT_VCENTER | DT_SINGLELINE, nTextBackColor);
		}
		else
		{
			if ( m_strStationName == "AKHAURA" )
			{
				CFont font;
				font.CreateFont(18,6,0,0,FW_HEAVY,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH, _T("arial"));
				CFont *oldfont = pDC->SelectObject(&font);
				MyDrawText( (LPCTSTR)m_szDrawName, -1, &r, /*DT_NOCLIP |*/ DT_CENTER | DT_VCENTER | DT_SINGLELINE, nTextBackColor);
				pDC->SelectObject( oldfont );
			} 
			else
			{
				MyDrawText( (LPCTSTR)m_szDrawName, -1, &r, /*DT_NOCLIP |*/ DT_CENTER | DT_VCENTER | DT_SINGLELINE, nTextBackColor);
			}
		}
	}

	nInhibit = 0;
    if ( nInhibit ) 
	{
        BmpDraw tbmp;
        Pos.x +=  _nDotPerCell  * 2;
        tbmp.DrawInhibit( Pos, nInhibit );
    }

  pDC->SetBkColor( RGB(0,0,0) );
  pDC->SetTextColor(TextColor);	

int TNION = 0;
if(TNION)
{
    if ( !m_rectTNI.IsRectNull() )  // 압구상자 그리기
	{
        CPen pen( PS_SOLID, 1, RGB(128,128,128) );
        CBrush brush( RGB(0,0,0) );
        CPen *pOldPen = pDC->SelectObject( &pen );
        CBrush *pOldBrush = pDC->SelectObject( &brush );
        pDC->Rectangle( &m_rectTNI );
    	pDC->SetBkColor( RGB(0,0,0) );
	    pDC->SetTextColor( RGB(255,255,0) );

        CRect r = m_rectTNI;
        r.bottom -= 1;
        r.top += 1;

		CFont font;
		font.CreateFont(10,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,_T("Arial"));
		CFont *oldfont = pDC->SelectObject(&font);

        m_szTrainNo[5] = 0;
        int len = strlen(m_szTrainNo);
        if (len > 5) len = 5;
    	MyDrawText( (LPCTSTR)m_szTrainNo, len, &r, DT_CENTER | DT_VCENTER | DT_SINGLELINE, nTextBackColor);

		pDC->SelectObject( oldfont );
        pDC->SelectObject( pOldBrush );
        pDC->SelectObject( pOldPen );
		font.DeleteObject();
		pen.DeleteObject();
	    brush.DeleteObject();
    }
}
	
}

void CBmpTrack::SetSignalDes(CPoint p,CPoint namepos,char *name,CPoint AapguNamePos, short namecheck,
						 int type,int mLamp,UINT HasTTB,int IsTop,int IsRight,UINT IsUdo,short HasRoute,
						 int nBlockadeType)
{
	
	ChangeMapping(p,namepos);
	ChangeMapping(AapguNamePos);
	SetSignal( p, namepos,name, AapguNamePos,namecheck,type, mLamp,HasTTB,IsTop,IsRight,IsUdo,HasRoute, nBlockadeType);

}
void CBmpTrack::SetSignal(CPoint p,CPoint namepos,char *name,CPoint AapguNamePos, short namecheck,
						 int type,int mLamp,UINT HasTTB,int IsTop,int IsRight,UINT IsUdo,short HasRoute,
						 int nBlockadeType )
{		
	pSignal[mSignalCount] = new  CBmpSignal(mCellCount+mSignalCount,p,namepos,name,AapguNamePos,namecheck,
											type,mLamp,HasTTB,IsTop,IsRight,IsUdo,HasRoute, nBlockadeType);

    TScrobj* SignalObj = (TScrobj*)pSignal[mSignalCount];
	SignalObj->m_nObjType = type;
	SignalObj->m_nDir = IsRight;

	mSignalCount++;

    if ( type == 0 || type == 1 || type == 3 || type == 4 )
        m_nTrackInfoFlag |= BIT_TK_HAS_MAINSIGNAL;

	int nSignalCount = mSignalCount;

	for ( int nCountSignal = 0; nCountSignal < nSignalCount; nCountSignal++)
	{
		SignalObject.SetAtGrow( SignalObject.GetSize(), (CObject*)pSignal[nCountSignal] );
	}
}
									
void CBmpTrack::SetSwitch(CPoint p,char *name, CPoint p2, BOOL bHandPointSW, BOOL bDoubleHSSW) 	
{	

	if(pSwitch)		
	{		
		if(pSwitch2)	
			delete pSwitch2;		
									
		pSwitch2 = new CBmpSwitch(-1,p,name,p2, bHandPointSW, bDoubleHSSW);						
	}
	else 						
		pSwitch = new CBmpSwitch(mCellCount+mSignalCount,p,name,p2, bHandPointSW, bDoubleHSSW);	

	SwitchObject.SetAtGrow( SwitchObject.GetSize(), (CObject*)pSwitch ); 

}

void CBmpTrack::SetButtonDes(CPoint p, char *name, int isright, int nButtonType)
{
	ChangeMapping(p);
	if ( isright ) {
		if(isright == 2) {			// (Button->m_nLR + 1 == 2) ? DN : UP;
			strcat(name,"DN");
			p.x -= 30;
		}
		else { 
			strcat(name,"UP");
			p.x += 30;
		}
	}
	SetButton( p,name,isright, nButtonType );
}

void CBmpTrack::SetButton(CPoint p,char *name,int isright, int nButtonType)
{

	if( isright ) {		// 0: 일반버튼, 1 o r2:본선궤도버튼 (Button->m_nLR + 1 == 2) ? DN : UP;
		m_nTrackInfoFlag |= BIT_TK_MAINLINE;	// 본선 T 일경우 
	}
	pButton[mButtonCount++] = new CBmpButton(mCellCount+mSignalCount+mButtonCount,p,name,isright, nButtonType, this);
    if ( nButtonType & BIT_BT_MAINDEST )
        m_nTrackInfoFlag |= BIT_TK_HAS_MAINDEST;
}


void CBmpTrack::SetNoJeulyunDes(CPoint p) 
{
	ChangeMapping(p);
	SetNoJeulyun(p);
}

void CBmpTrack::SetNoJeulyun(CPoint p) // 절연없음을 셋팅 ->2개의 T가 한개 같이 사용할때 
{	
	int CellNo = ListFind(p);
	if(CellNo >= 0 )		{    		
		((CBmpTrackCell*)pCellInfo)->mVectorType |= ((CBmpTrackCell*)pCellInfo)->mVectorType << 4;
		JeulyunInfo |= ((CBmpTrackCell*)pCellInfo)->mNodeType;
	}
}

void CBmpTrack::SetNoJeulyun(UINT node) // 절연없음을 셋팅 ->2개의 T가 한개 같이 사용할때 
{
	CPoint p;
	if( node & NODE_CTYPE)	{
		p = C;		
		SetNoJeulyun(p);
	}
	if( node & NODE_NTYPE)	{
		p = N;		
		SetNoJeulyun(p);
	}
	if( node & NODE_RTYPE)	{
		p = R;		
		SetNoJeulyun(p);
	}	
}

void CBmpTrack::SetNamePos(CPoint p1, CPoint p2)
{
	mNamePos1 = p1;	
}

void CBmpTrack::SetAdjacencyPoint(CString strPoint)
{
	m_strPoint = strPoint;	
}

void CBmpTrack::SetAdjacencyPoint2(CString strPoint2)
{
	m_strPoint2 = strPoint2;	
}

void CBmpTrack::SetOverlapRoute(CString strOverlapRoute)
{
	m_strOverlapRoute = strOverlapRoute;
}
// void CBmpTrack::SetDestinationTrackUS(CString strTrack, int flag)
// {
// 	m_strDestinationTrackUS[flag] = strTrack;	
// }
// void CBmpTrack::SetReferencePointUS(CString strPoint, int flag)
// {
// 	if (strPoint.Right(1) == ' ')
// 		strPoint = "0";
// 	m_strReferencePointUS[flag] = strPoint;	
// }
// 
// void CBmpTrack::SetDestinationTrackDS(CString strTrack, int flag)
// {
// 	m_strDestinationTrackDS[flag] = strTrack;	
// }
// void CBmpTrack::SetReferencePointDS(CString strPoint, int flag)
// {
// 	if (strPoint.Right(1) == ' ')
// 		strPoint = "0";
// 	m_strReferencePointDS[flag] = strPoint;	
// }

UINT CBmpTrack::GetSwitchState()
{
	if (!pSwitch) return NODE_CTYPE | NODE_NTYPE;
	return pSwitch->mState;
}

void CBmpTrack::SetJoinTrack(int no,CBmpTrack *t,int node)
{
	mJoinTrackInfo[no].Track = t;
	if( node == 1 )
		mJoinTrackInfo[no].Node = NODE_CTYPE;
	else if( node == 2 )
		mJoinTrackInfo[no].Node = NODE_NTYPE;
	else if( node == 3 )
		mJoinTrackInfo[no].Node = NODE_RTYPE ;
	IsJoinTrack=1;
}

void CBmpTrack::Serialize(CArchive& ar) 
{	
	char buf[200];
	char cap[20];
	memset(buf,0x00,sizeof(buf));
	memset(cap,0x00,sizeof(cap));	
	char en[2];
	strcpy(en,"\r\n");
	strcpy(cap,"#[TRACK]\r\n");	
	if(ar.IsStoring())	{		
		ar.Write(cap,strlen(cap));
		
		sprintf(buf,"%s, (%d, %d), (%d, %d), (%d, %d), (%d, %d), %u, %d, %s, %s, %s",
					mName, mNamePos1.x, mNamePos1.y, C.x, C.y, N.x, N.y, R.x, R.y, JeulyunInfo, m_bLineLimit , m_strPoint, m_strPoint2, m_strOverlapRoute);

		ar.Write(buf,strlen(buf));
		ar.Write(en,2);		
		int no = mPosList.GetSize();	
		strcpy(cap,"[CELL]		");						
		for (int i=0 ; i< no ; i++){
			ar.Write(cap,strlen(cap));
			strcpy( buf, (char*)(LPCTSTR)mPosList.GetAt(i));
			ar.Write(buf,strlen(buf));
			ar.Write(en,2);
		}
		if ( mButtonCount == 2 ) {
			char *s1 = pButton[0]->mName;
			if ( !strcmp(pButton[0]->mName, pButton[1]->mName) && !isdigit(*s1) ) {
				pButton[0]->Serialize(ar);
			}
			else {
				pButton[0]->Serialize(ar);
				pButton[1]->Serialize(ar);
			}
		}
		else {
			for (i = 0; i< (int)mButtonCount; i++) 
				pButton[i]->Serialize(ar);
		}
		for (i = 0; i<(int)mSignalCount; i++)
			pSignal[i]->Serialize(ar);			
		if(pSwitch)
			pSwitch->Serialize(ar);
		if(pSwitch2)
			pSwitch2->Serialize(ar);
	}
	else  ;
}

void CBmpSignal::Serialize(CArchive& ar) 
{	
	char buf[200];
	memset(buf,0x00,200);	
	char en[2];
	strcpy(en,"\r\n");
	char name[10];
	strcpy(buf,"[SIGNAL]	");	
	if(ar.IsStoring())		
	{
		ar.Write(buf,strlen(buf));
		if( strcmp(mName,"") ) strcpy(name,mName);
		else	strcpy(name,"NONE");
		sprintf(buf,"%s, (%d, %d), (%d, %d), (%d, %d), %d, %d, %d, %u, %d, %d, %u, %d, %d",
					name,mNamePos.x,mNamePos.y,mPos.x,mPos.y,mAapguNamePos.x,mAapguNamePos.y,
					mNamecheck,m_nType,mLamp,mHasLoop,IsTop,IsRight,IsUdo,mHasRoute, 
					m_nBlockadeType);
		ar.Write(buf,strlen(buf));
		ar.Write(en,2);		
	}	
	else
		;	
}
TScrobj* CBmpSwitch::PtInArea( CPoint &point )
{	
	m_bPntClick = FALSE;
	switch( m_cObjectType ) {
	case OBJ_SWITCH:	//OBJ_SWITCH:
		if ( PtInRegion( point ))
		{
			m_bPntClick = TRUE;
			return (TScrobj*)this;			
		}
		break;		
	}	
	return NULL;
}
void CBmpSwitch::Serialize(CArchive& ar) 
{	
	char buf[200];
	memset(buf,0x00,200);	
	char en[2];	
	strcpy(en,"\r\n");
	strcpy(buf,"[SWITCH]	");	
	if(ar.IsStoring())		{
		ar.Write(buf,strlen(buf));		

		sprintf(buf,"%s, (%d, %d), (%d, %d), %d, %d", mName, mPos.x, mPos.y, mNamePos.x, mNamePos.y
									, ((m_bHandPointSW) ? 1 : 0), ((m_bDoubleHSSW) ? 1 : 0) );

		ar.Write(buf,strlen(buf));
		ar.Write(en,2);		
	}		
	else
		;	
}

void CBmpButton::Serialize(CArchive& ar) 
{	
	char buf[200];
	memset(buf,0x00,200);		
	char en[2];
	strcpy(en,"\r\n");
	strcpy(buf,"[BUTTON]	");	
	if(ar.IsStoring())			{
		ar.Write(buf,strlen(buf));		
		sprintf(buf,"%s, ( %d, %d ), %d, %d",mName,mPos.x,mPos.y,IsRight, m_nButtonType);
		ar.Write(buf,strlen(buf));
		ar.Write(en,2);		
	}
	else
		;	
}


CString GetSwitchName( char *pStr ) {
	char bf[32];
	strcpy( bf, pStr );
	char *p = bf;
// YN 2001.6.25 영문으로 시작되는 Switch처리. ( 109A -> B09A )	
	while (*p && !isdigit(*p)) p++;
	if ( *p ) {
// E
		while (*p) {
			if (!isdigit(*p)) {
				*p = 0;
				break;
			}
			p++;
		}
	}
	CString name = bf;
	return bf;
}
// 2000.6.20 end

TScrobj * CBmpTrack::FindObject( char cType, char * szName )
{
	UINT i;
	CString name;
	short n;
	switch( cType ) {
	case OBJ_TRACK:
			if (!strcmp( mName, szName )) 
				return (TScrobj*)this;  // 같으면 리턴
		break;
	case OBJ_TRACK2:
			if (!strcmp( m_szDrawName, szName ))
				return (TScrobj*)this;  // 같으면 리턴
		break;
	case OBJ_SIGNAL:
		for (i=0; i<mSignalCount; i++) {
			name = pSignal[i]->mName;
			n = name.Find( '.' );   // Copy 번호를 제외한 신호기 이름을 얻기위해.
			if (n > 0) {
				name = name.Left( n );  // ex) 58T.2 => n= 3 => name = 58T
			}
			if ( name == szName ) return (TScrobj*)pSignal[i];
		}
		break;
	case OBJ_BUTTON:
		for (i=0; i<mButtonCount; i++) {
			name = pButton[i]->mName;
			n = name.Find( '.' ); 
			if (n > 0) {
				name = name.Left( n );
			}
			if ( name == szName ) return (TScrobj*)pButton[i];
		}
		break;
	case OBJ_SWITCH:
		if (pSwitch) {
			CString s1,s2;
			s1 = GetSwitchName( pSwitch->mName );
			s2 = GetSwitchName( szName );
			if ( s1 == s2 ) return (TScrobj*)pSwitch;
		}
		break;
	}
	return NULL;
}

TScrobj * CBmpTrack::FindObject( char cType, short nID )
{
	UINT i;
	switch( cType ) {
	case OBJ_TRACK:
		if ( m_nID == nID ) return (TScrobj*)this;
		break;
	case OBJ_SIGNAL:
		for (i=0; i<mSignalCount; i++) {
			if ( pSignal[i]->m_nID == nID ) return (TScrobj*)pSignal[i];
		}
		break;
	case OBJ_BUTTON:
		for (i=0; i<mButtonCount; i++) {
			if ( pButton[i]->m_nID == nID ) return (TScrobj*)pButton[i];
		}
		break;
	case OBJ_SWITCH:
		if (pSwitch && pSwitch->m_nID == nID ) return (TScrobj*)pSwitch;
		break;
	}
	return NULL;
}

TScrobj* CBmpTrack::PtInArea( CPoint &point )
{
	UINT i;
	if ( mName[0] == '?' ) {
		if (strlen( mName ) == 1 )
			return NULL;
	}
//
	if ( pSwitch && pSwitch->m_hObject && pSwitch->PtInRegion( point ) ) 
		return (TScrobj*)pSwitch;

	for (i=0; i<mSignalCount; i++) {
		if ( pSignal[i]->PtInArea( point ) ) 
			return (TScrobj*)pSignal[i];
	}
	for (i=0; i<mButtonCount; i++) {
		if ( pButton[i]->m_hObject && pButton[i]->PtInRegion( point ) ) 
			return (TScrobj*)pButton[i];
	}
	if ( !m_rectTNI.IsRectNull() && m_rectTNI.PtInRect( point ) ) {
        m_bTNIClicked = TRUE;
        return (TScrobj*)this;
    }
    if ( CRgn::PtInRegion( point ) ) {
        m_bTNIClicked = FALSE;
        return (TScrobj*)this;
    }
	return NULL;
}

TScrobj* CBmpSignal::PtInArea( CPoint &point )
{
    m_bCAClicked = FALSE;
	m_bLCRClicked = FALSE;
	m_bCBBClicked = FALSE;

	PtInRegion( point );

	if (m_hObject && PtInRegion( point )) 
	{
		return this;
	}
	if (m_nType > 4 && m_nType < 7)
	{
		CPoint p = mPos;
		int gap;
		if((m_nType + IsRight) != 6)
		{
			gap = 43;
		}
		else
		{
			gap = 17;
		}
		CRect rect(p.x-gap-11, p.y-11, p.x-gap+11, p.y+11);
		if ( rect.PtInRect( point ) ) 
		{
			m_bCAClicked = TRUE;
			return this;
		}
		CRect rect2(p.x-gap-11+31, p.y-11, p.x-gap+11+31, p.y+11);
		if ( rect2.PtInRect( point ) ) 
		{
			m_bLCRClicked = TRUE;
			return this;
		}
		CRect rect3(p.x-gap-11+61, p.y-11, p.x-gap+11+61, p.y+11);
		if ( rect3.PtInRect( point ) ) 
		{
			m_bCBBClicked = TRUE;
			return this;
		}
	}
		return NULL;
}

void LptoDp( CPoint &p )
{
	double x = _nDotPerCell / 20.0;
	double y = _nDotPerCell / 10.0;
	p = CPoint((int)(p.x * x) , (int)(p.y * y));	
}

char *szTrackInfoStr[] = {
	"ALARM",
	"TRACK",
	"ROUTE",
	"TKREQUEST",
	"OVERLAP",
	"ROUTEDIR",
	"EMREL",
	"INHIBIT",
	"DISTURB",		
	"RESETIN",				
	"RESETOUT",		
	"RESETBUF",	
	"MULTIDEST",		
	"BLKTRACE",	
	"BLKSWEEP",	
	"BLKILLOCC",
	"ROUTENO",
	"OVERLAPNO<-",
	"OVERLAPNO->"
};

char *szSwitchInfoStr[] = {
// in
	"KR_P",		//0
	"KR_M",		//1
	"WR_P",		//2
	"WR_M",		//3
	"WLR",		//4
// out
	"FREE",		//5 1=전환가능 상태, 0 쇄정
	"WRO_M",	//6
	"WRO_P",	//7
// internal
	"BACKROUTE",//8
	"ROUTELOCK",//9
	"REQUEST_P",//10
	"REQUEST_M",//11
	"ON_TIMER",	//12
	"WLRO",		//13
	"SWFAIL",	//14
	"TRACKLOCK",	//15
	"NOUSED",	//16
	"KEYOPER",	//17
	"WKEYKR",	//18
	"ROUTE_P",	//19
	"ROUTE_M",	//20
	"INROUTE",	//21
	"BLOCK",	//22
	"PASSROUTE"	//23

};


char *szSignalInfoStr[] = {
// out
	"SOUT1",	//0 S1 - DR (G)
	"SOUT2",	//1 S2 - HR (Y)
	"SOUT3",	//2 S3 - CALLON
	"U",			//3 S4 - SHUNT (Outer Yellow)
// in
	"SIN1",			//4 - DR (G - DR Control Relay Contact input)
	"SIN2",			//5 - HR (Y - HR Control Relay Contact input)
	"SIN3",			//6 - CALLON - CLOR relay
	"INU",			//7 - SHUNT (Outer Yellow - HHLOR relay)
//=====================================
	"SE1",		    //8  - CTR DR
	"SE2",		    //9  - CTR HR
	"SE3",		    //10 - CTR CALLON
	"SE4",		    //11 - CTR SHUNT (Outer Yellow)
	"LEFT",	    	//12 - OUT LEFT Direction
	"RIGHT",		//13 - OUT Right Direction
	"CS",		    //14 - Callon, Shunt 진로 설정 중 set
	"FREE",			//15 - 신호 현시 상태, 0-쇄정

//=====================================
	"TM1",			//16 - Internal Timer 1
	"SignalFail",			//17 - Internal Timer 2
	"ON_TIMER",		//18 - Approach timer running
	"REQUEST",		//19
	"ENABLED",		//20	; 신호현시 상태에서 전방 궤도가 낙하하여 신호가 정지한 경우 세트
	"RCVRONLY",		//21 - Recovery Only Flag
	"INLEFT",		//22 - IN Left Direction  (LRDIRLOR relay)
	"INRIGHT",		//23 - IN Right Direction (RDIRLOR relay)

//=====================================
    "LM_LOR",		//24 - Signal main lamp aspect detection
    "LM_ALR",		//25 - Signal Lighting Dark Operation detection
    "LoopSigFail",	//26 - R Aspect main filament failure (RFILR relay - normal = 1)
    "Reserved1",	//27
	"BLKCOMM",		//28 - Block Communication Status (Always 1)
    "ADJCOND",		//29 - Additional signal aspect condition from adjacent station
    "LM_CLOR",		//30 - Signal Failure (normal =0, LOR not active =1)
    "OSS",			//31
//======================================
	"LM_EKR",		//32 - IN, R,Y,G main filament failure indication (EKR - R,Y,G Lamp 구동 시 Normal - 1) 
	"LM_CEKR",		//33 - IN, Outer yellow main filament failure indication (CEKR - OY Lamp 구동 시 Normal - 1)
	"LM_RL",		//34 - R Aspect main filament failure indication (normal = 0, Failure = 1) 
	"LM_YL",		//35 - Y Aspect main filament failure indication (normal = 0, Failure = 1)
	"LM_GL",		//36 - G Aspect main filament failure indication (normal = 0, Failure = 1)
	"LM_SLOR",		//37
	"reserved2",	//38
	"reserved3",	//39

	"DESTTRACK",
	"DESTID"
};

char *szblockInfoStr[] = {

  "CA",			//0
  "LCR",		//1	
  "LCRIN",		//2
  "LCROP",		//3
  "CBB",		//4 
  "CBBIN",		//5	
  "CBBOP",		//6
  "COMPLETEIN" ,//7
  "CONFIRM " ,	//8 
  "ASTART",		//9
  "DEPART" ,	//10
  "DEPARTIN" ,	//11
  "ARRIVE" ,	//12
  "ARRIVEIN" ,	//13
  "SWEEPINGACK",//14
  "COMPLETE",	//15
  "BLKRSTREQ",	//16
  "BLKRSTREQIN",//17
  "AXLRST",		//18
  "AXLOCC",		//19
  "AXLDST",		//20
  "ASTARTSTICK",//21
  "CAACK",		//22
  "SWEEPING",	//23
  "NBGR",		//24
  "RBGR",		//25
  "NBCR",		//26
  "RBCR",		//27
  "NBGPR",		//28
  "RBGPR",		//29
  "NBCPR",		//30
  "RBCPR",		//31
  "AXLREQ",		//32
  "AXLACC",		//33
  "AXLDEC",		//34
  "BLKRSTACC",	//35
  "BLKRSTACCIN",//36
  "BLKRSTDEC",	//37
  "BLKRSTDECIN",//38
  "AXLOCCDOUT",	//39
  "SAXLRST",	//40
  "SAXLOCC",	//41
  "SAXLDST",	//42
  "SAXLREQ",	//43
  "SAXLACC",	//44
  "SAXLDEC",	//45
  "SAXLACT",	//46
  "SAXLACTIN"	//47
};

void CBmpTrack::ListCtrlDraw( CString &strInfo , CListCtrl &m_list )
{
	LV_COLUMN lvcol;
	char *list[2] = { "NAME", "STATE" };
    int  width[2] = { 120, 80 };
	
    for(int i=0; i<2; i++)
    {
        lvcol.mask=LVCF_FMT|LVCF_SUBITEM|LVCF_TEXT|LVCF_WIDTH;
        lvcol.fmt=LVCFMT_CENTER;
        lvcol.pszText=list[i];
        lvcol.cx=width[i];
        m_list.InsertColumn(i,&lvcol);
    }
	
	DWORD style = LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES; 
	m_list.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LPARAM(style));
	
	MakeInfoList( strInfo , m_list, FALSE );
}

void CBmpTrack::MakeInfoList( CString &strInfo , CListCtrl &m_list, BOOL bRedraw ) 
{
		CString str;
		strInfo = str;
		LV_ITEM lvItem;
		lvItem.mask = LVIF_TEXT;
		lvItem.iSubItem = 0;

		BOOL pointcheck = FALSE;

		if ( m_pData && !(mName[0] == '?' && strlen( mName ) == 1) ) 
		{
			lvItem.iItem = 0;
			int ntemp = 1;
			CString Firstname;
			Firstname.Format("[ TRACK : %s ]",mName);
			lvItem.pszText = (LPSTR)(LPCTSTR)Firstname;
			if ( !bRedraw ) {
				m_list.InsertItem(&lvItem);	        
			}

			MakeBinaryInfoString( str, szTrackInfoStr, m_pData, 16, 1 , m_list , ntemp, bRedraw );
			strInfo += str;
            
			if ( pSwitch && pSwitch->m_hObject ) 
			{
				lvItem.iItem = 20;
				int ntemp = 21;
				pointcheck = TRUE;
				strInfo += str;
				CString Firstpoint;
				Firstpoint.Format("[ POINT : %s ]", pSwitch->mName);
				lvItem.pszText = (LPSTR)(LPCTSTR)Firstpoint;
				if ( !bRedraw ) {
					m_list.InsertItem(&lvItem);
				}

				if( pSwitch->m_bHandPointSW ) { //  고속 전철기인경우.. 
					MakeBinaryInfoString( str, szSwitchInfoStr, pSwitch->m_pData, 24, 0, m_list ,ntemp, bRedraw );
				}
				else{
					MakeBinaryInfoString( str, szSwitchInfoStr, pSwitch->m_pData, 24, 0, m_list ,ntemp, bRedraw );
				}

				strInfo += str;
			}

			int nRowNo = 0;
			for (UINT i=0; i<mSignalCount; i++) 
			{
				if ( !strcmp( pSignal[i]->m_szDrawName, "LOS" ) ) continue;	//2003.5.19. break;

				if ( pSignal[i]->m_pData == NULL ) break;
//
				int ntemp=0;
				int nType = pSignal[i]->m_nType;
				CString Firstsignal;
				
				if ( !pointcheck ) {
					lvItem.iItem = 20 + nRowNo;	//( i * 34 );
					ntemp = 21 + nRowNo;	//( i * 34 );
				}
				else {
					if( pSwitch->m_bHandPointSW )
					{
						lvItem.iItem = 50 + nRowNo;	//( i * 34 );
						ntemp = 51 + nRowNo;	//( i * 34 );
					}
					else
					{
						lvItem.iItem = 34 + nRowNo;	//( i * 34 );
						ntemp = 35 + nRowNo;	//( i * 34 );
					}
				}
				if(nType < 5 || nType == 12 || nType == 11)
				{
					strInfo += str;
					Firstsignal.Format("[ SIGNAL : %s ]",pSignal[i]->mName);
					lvItem.pszText = (LPSTR)(LPCTSTR)Firstsignal;
					if ( !bRedraw ) 
					{
						m_list.InsertItem(&lvItem);
					}
					MakeBinaryInfoString( str, szSignalInfoStr, pSignal[i]->m_pData, 40, 2, m_list , ntemp, bRedraw);
					strInfo += str;
					nRowNo += 43;
				}

				else if (nType > 4)
				{
					Firstsignal.Format("[ BLOCK : %s ]",pSignal[i]->mName);
					lvItem.pszText = (LPSTR)(LPCTSTR)Firstsignal;
					if ( !bRedraw ) 
					{
						m_list.InsertItem(&lvItem);
					}
					MakeBinaryInfoString( str, szblockInfoStr, pSignal[i]->m_pData, 48, 0 , m_list , ntemp, bRedraw);
					strInfo += str;
					nRowNo += 49;
				}

			}
		}
		else 
		{
			CString Firststr;
			Firststr.Format("VMEM Not Assigned [%8.8X]",m_pData);
			lvItem.pszText = (LPSTR)(LPCTSTR)Firststr;
			m_list.InsertItem(&lvItem);
			strInfo += Firststr;
		}
}

void CBmpTrack::MakeBinaryInfoString( CString &str, char *szTitle[], void *pData, short BitCount, int nByte/*=FALSE*/ , CListCtrl &m_list , int ntemp, BOOL bRedraw )	
{
	LV_ITEM lvItem;
	lvItem.mask = LVIF_TEXT|LVCF_FMT;
	lvItem.iSubItem = 0;
	
	short i;
    char bf[50];
	long data = *(long*)pData;
	
    str = "";
	for (i=0; i<BitCount; i++) 
	{
		bf[i] = '0'+(data & 1);
		bf[i+1] = 0; //bf[i] = (data & 1) ? '1' : '0';
		lvItem.iItem = ntemp+i;
		if ( !bRedraw ) {
			lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[i];
			m_list.InsertItem(&lvItem);	      
		}
		else {
			m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)&bf[i]);
		}
		str += bf;
		data >>= 1;
	}

	if ( nByte > 0 ) 
	{
		if( nByte == 1)
		{
			BitCount /= 16;
			
			WORD c = *((WORD*)pData + BitCount);
			CString temp;
			temp.Format("%d[%4.4X]",c&ROUTE_MASK,c&ROUTE_MASK);
			
			lvItem.iItem = ntemp + ( i );
			if ( !bRedraw ) {
				lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[16];
				m_list.InsertItem(&lvItem);	        
			}
			else {
				m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)temp);
			}
			str += bf;

			c = *((WORD*)pData + BitCount+1);
			temp.Format("%d[%4.4X]",c,c);
			
			lvItem.iItem = ntemp + ( i+1 );
			if ( !bRedraw ) {
				lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[17];
				m_list.InsertItem(&lvItem);	        
			}
			else {
				m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)temp);
			}
			str += bf;

			c = *((WORD*)pData + BitCount+2);
			temp.Format("%d[%4.4X]",c,c);
			
			lvItem.iItem = ntemp + ( i+2 );
			if ( !bRedraw ) {
				lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[18];
				m_list.InsertItem(&lvItem);	        
			}
			else {
				m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)temp);
			}
			str += bf;
		}

		if( nByte == 2)
		{
			BitCount /= 8;
			
			BYTE c = *((char*)pData + BitCount);
			CString temp;
			temp.Format("%d[%2.2X]",c,c);
			
			lvItem.iItem = ntemp + ( i );
			if ( !bRedraw ) {
				lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[40];
				m_list.InsertItem(&lvItem);	        
			}
			else {
				m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)temp);
			}
			str += bf;
			
			c = *((char*)pData + BitCount+1);
			temp.Format("%d[%2.2X]",c,c);
			
			lvItem.iItem = ntemp + ( i+1 );
			if ( !bRedraw ) {
				lvItem.pszText = (LPSTR)(LPCTSTR)szTitle[41];
				m_list.InsertItem(&lvItem);	        
			}
			else {
				m_list.SetItemText(lvItem.iItem, 1, (LPCTSTR)temp);
			}
			str += bf;
		}
	} 
}

long CBmpTrack::GetToggleObject( int nBitLoc )
{
	int nID = 0;
	int nType = 0;
	if ( m_pData && !(mName[0] == '?' && strlen( mName ) == 1) ) 
	{
		int ntemp = 1;
		if ( nBitLoc < 17 ) 
		{
			nID = m_nID;
			nType = 1;
			goto dowork;
		}

		nBitLoc -= 17;
		if ( pSwitch && pSwitch->m_hObject ) 
		{
			if ( nBitLoc < 25 ) {
				nID = pSwitch->m_nID;
				nType = 3;
				goto dowork;
			}
			nBitLoc -= 25;
		}
		
		for (UINT i=0; i<mSignalCount; i++) 
		{
			if ( nBitLoc < 34 ) 
			{
				nID = pSignal[i]->m_nID;
				nType = 2;
				goto dowork;
			}
			nBitLoc -= 34;
		}
	}
	return 0;

dowork:
	if ( nBitLoc == 0 ) return 0;
	if ( nID == 0 ) return 0;
	if ( nType == 0 ) return 0;
	nBitLoc--;
	return ( (nType << 24) + (nID << 8) + nBitLoc );

}


