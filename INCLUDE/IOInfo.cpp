// IOInfo.cpp : implementation file
//

#include "stdafx.h"
#include "IOInfo.h"
#include "Parser.h"
#include "strlist.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_STRING_RDF_FILE_V10		";I/O DEFINE V1.0" 

CString GetSwitchName( CString &name );

/////////////////////////////////////////////////////////////////////////////
// CIOInfo

IMPLEMENT_DYNCREATE(CIOInfo, CDocument)

CIOInfo::CIOInfo()
{
}

BOOL CIOInfo::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CIOInfo::~CIOInfo()
{
}


BEGIN_MESSAGE_MAP(CIOInfo, CDocument)
	//{{AFX_MSG_MAP(CIOInfo)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIOInfo diagnostics

#ifdef _DEBUG
void CIOInfo::AssertValid() const
{
	CDocument::AssertValid();
}

void CIOInfo::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIOInfo serialization

void CIOInfo::Serialize(CArchive& ar)
{
	memset(m_Rack,0,8*7*32);
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
	}
}

/////////////////////////////////////////////////////////////////////////////
// CIOInfo commands

BOOL CIOInfo::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if ( !LoadFileRDF(lpszPathName) )
		return FALSE;
	
	// TODO: Add your specialized creation code here
	
	SetPathName(lpszPathName);
	return TRUE;
}

short CIOInfo::FindIOName( char *szName )
{
	short i,j,k;
	CardTypes type = CardNONE;
	int len = strlen(szName);

	if ( !len ) 
		return 0;

	if ( len == 1 ) 
	{
		int debug = 1;
	}

	if (len > 2) 
	{
		if (szName[1] == '.') 
		{
			switch (szName[0]) 
			{
			case 'I': type = CardINPUT; break;
			case 'O': type = CardOUTPUT; break;
			case 'X': type = CardCOMIN; break;
			case 'Z': type = CardCOMOUT; break;
			}
			szName += 2;
		}
	}
	CString str;

	for (i=0; i<MAX_RACK; i++) 
	{
		for (j=0; j<CARD_PER_RACK; j++) 
		{
			IOCardType *pCard = &m_Rack[ i ].m_IOCard[ j ];
			for (k=0; k<32; k++) 
			{
				str = pCard->szNames[k];
				if ( str != "" && str.GetAt(0) == '!' ) 
				{
					str = (char*)(LPCTSTR)str + 1;
				}
				if (!strcmp( szName, str )) 
				{
					if (type != CardNONE) 
					{
						if (type != pCard->nType) 
							continue;
					}
					short n;
					n = i * CARD_PER_RACK * 32;
					n += j * 32;
					n += k;					
					return n;
				}
			}
		}
	}

// BASE 4.6.1
	for (j=0; j<3; j++) 
	{
		ComCardType *pCard = &m_DTS.m_In[ j ];
		for (k=0; k<32; k++) 
		{
			str = pCard->szNames[k];
			if ( str != "" && str.GetAt(0) == '!' ) 
			{
				str = (char*)(LPCTSTR)str + 1;
			}
			if (!strcmp( szName, str )) 
			{
				if (type != CardNONE) 
				{
					if (type != pCard->nType)
					{
						if (pCard->nType == CardCOMIN || pCard->nType == CardCOMOUT)
						{
							if ( !(type == CardINPUT) )
								continue;
						}
						else
						    continue;
					}
				}

				short n = 48 * 32;	
				n += j * 128;	    
				n += k;					
				return n;
			}
		}

		pCard = &m_DTS.m_Out[ j ];
		for (k=0; k<32; k++) 
		{
			str = pCard->szNames[k];
			if ( str != "" && str.GetAt(0) == '!' ) 
			{
				str = (char*)(LPCTSTR)str + 1;
			}
			if (!strcmp( szName, str )) 
			{
				if (type != CardNONE) 
				{
					if (type != pCard->nType) 
						continue;
				}

				short n = 60 * 32;	
				n += j * 128;		
				n += k;					
				return n;
			}
		}
	}

	return -1;
}

short CIOInfo::SetIOName( char *szName, short nAddr )
{
	return 0;
}

short CIOInfo::FindIOName( CString &str )
{
	return FindIOName( (char*)(LPCTSTR)str );
}

void CIOInfo::Out( ostream &os )
{
	os << ID_STRING_RDF_FILE_V10;
	os << '\n';
}

BOOL CIOInfo::LoadFileRDF( LPCTSTR pPath )
{
	CString str;				
	CString sName;
	int   period;
	int   nLine = 0;
	short nType = 0;
	short RackNo = 1;			
	short CardNo = 1;			
	short PortNo = 1;

	memset( m_pRealAddress, 0xff, sizeof(m_pRealAddress) );

	FILE *fp = fopen( pPath, "r+t" );
	if ( !fp ) return FALSE;

	char bf[1024];
	while (!feof(fp)) 
	{
		fgets( bf,1023,fp );
		str = bf;

		nLine++;
		CParser line((char*)(LPCTSTR) str, " \t\r\n:" );
		CString token;
		token = line.gettokenl();

        CardTypes type = CIORack::FindCardType((char*)(LPCTSTR)token);
        if ( type ) 
		{
            nType = type;
            continue;
        }
		else if ( token == "END" ) {
			break;
		}
		else if ( (token == "") || (token.Left(1) == "/") || (token.Left(1) == ";") ) {  // 주석 				
			continue;
		}
		else if ( (token.Left(4) == "RACK") ) {
			continue;
		}
		else if ( (token.Left(5) == "PORT=") ) {
			CardNo = token.GetAt(5) - 48 - 1;
			continue;
		}

		period = token.Find('.');
		if (period >= 0) 
		{
			if (period) 
			{
				if ( token.Left(3) == "ID=" ) 
				{
					int logicalno = atoi( ((char*)(LPCTSTR)token) + 3 );
// 16개의 card를 가지는 16개의 rack까지 가능... -> CARD_PER_RACK = 256
	                RackNo = (logicalno / CARD_PER_RACK) + 1;	// (logicalno is form No.0 to No.16) 가변 limit.
					CardNo = logicalno % CARD_PER_RACK;
					int nDolsign = token.Find( '$' );
					long temp = (long)logicalno;
					if ( nDolsign >= 3 ) 
					{
						char* strptr;
						temp = strtol( ((char*)(LPCTSTR)token) + nDolsign + 1, &strptr, 16 );
					}
					m_pRealAddress[ logicalno ] = (BYTE)temp;
				}
				else {
					ASSERT( 0 );
				}
			}
			CString imsi;
			imsi  = token.Right(token.GetLength() - (period+1));
			token = imsi;
		}
		if (token == "") 
			continue;

		PortNo  = atoi( token );
		token = line.gettokenl();
		sName = token;

		if ( nType == CardCOMIN ) 
		{
			if( CardNo > 2 || CardNo < 0)	
			{
				CString err;
				err.Format("card no err, Line: %d", nLine);
				AfxMessageBox(err);
				return FALSE;
			}		
			m_DTS.m_In[ CardNo ].nCardNo = CardNo;
			m_DTS.m_In[ CardNo ].nType = (CardTypes)nType;
			strcpy( m_DTS.m_In[ CardNo ].szNames[ PortNo-1 ], sName);
        }
        else if ( nType == CardCOMOUT ) 
		{
			if( CardNo > 2 || CardNo < 0)	
			{
				CString err;
				err.Format("card no err, Line: %d", nLine);
				AfxMessageBox(err);
				return FALSE;
			}		
			m_DTS.m_Out[ CardNo ].nCardNo = CardNo;
			m_DTS.m_Out[ CardNo ].nType = (CardTypes)nType;
			strcpy( m_DTS.m_Out[ CardNo ].szNames[ PortNo-1 ], sName);
        }
        else 
		{
			if (token =="") continue;

			if (RackNo > MAX_RACK || RackNo < 1 )	
			{
				CString err;
				err.Format(" RackNo over flow  = %d, Line: %d",RackNo, nLine);
				AfxMessageBox(err);
				return FALSE;
			}
			if ( CardNo >= CARD_PER_RACK || CardNo < 0)	
			{
				CString err;
				err.Format("card no err, Line: %d", nLine);
				AfxMessageBox(err);
				return FALSE;
			}		
			if ( PortNo > 32 || PortNo < 1)	
			{
				CString err;
				err.Format("port no err, Line: %d", nLine);
				AfxMessageBox(err);
				return FALSE;
			}

			m_Rack[RackNo-1].m_IOCard[CardNo].nCardNo = CardNo;
			m_Rack[RackNo-1].m_IOCard[CardNo].nType = (CardTypes)nType;
			
			strcpy(m_Rack[RackNo-1].m_IOCard[CardNo].szNames[PortNo-1], sName);
        }
    }
	fclose( fp );
	return TRUE;

}
