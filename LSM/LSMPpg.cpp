//===============================================================
//====                     LSMPpg.cpp                       ====
//===============================================================
// 파  일  명 : LSMPpg.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMctrl 클래스에서 사용되는 CLSMPropPage
//              factory와 quid를 초기화한다.
//
//===============================================================


#include "stdafx.h"
#include "LSM.h"
#include "LSMPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CLSMPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CLSMPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CLSMPropPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CLSMPropPage, "LSM.LSMPropPage.1",
	0x4152f444, 0x9a90, 0x11d3, 0xaa, 0x95, 0, 0xc0, 0x6c, 0, 0x3c, 0x3f)


/////////////////////////////////////////////////////////////////////////////
// CLSMPropPage::CLSMPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CLSMPropPage

BOOL CLSMPropPage::CLSMPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_LSM_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CLSMPropPage::CLSMPropPage - Constructor

CLSMPropPage::CLSMPropPage() :
	COlePropertyPage(IDD, IDS_LSM_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CLSMPropPage)
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CLSMPropPage::DoDataExchange - Moves data between page and properties

void CLSMPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CLSMPropPage)
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CLSMPropPage message handlers

