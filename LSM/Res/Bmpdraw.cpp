#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <strstrea.h>
#include "Bmpdraw.h"
#include "bmpinfo.h"

UINT _nGRIDSIZE = 10;           // Used for DES to SFM 
float VIEW_XRATE = 0.5f;		// x * VIEW_XRATE 
float VIEW_YRATE = 1.0f;		// y * VIEW_XRATE

UINT  CELLSIZE_Y   = 10;     	//_nDotPerCell; 
UINT  CELLSIZE_X   = 5;	    	//_nDotPerCell / 2; 

int   OFFSET_X  = 540;
int   OFFSET_Y  = -375;
int   REVERSE = 1;

SymbolBitmapInfo SymbolInfo10[] = 
{
//	int x,y,w,h,cx,cy;
        /* 0*/ {  0 ,  0,  5, 10,  0, 5 },   // Terminate  직선 DIR=0
        /* 1*/ {  5 ,  0,  6, 10,  3, 5 },   // Straight   직선 DIR=0,4
        /* 2*/ { 22 ,  0, 16, 10,  6, 5 },   // Straight  사선 DIR=1,5
        /* 3*/ { 40 ,  0, 15, 10,  3, 5 },   // Elbow
        /* 4*/ { 390 ,  1, 20,   9, 11, 4 },   // Terminate  사선 우상		
        /* 5*/ {  0 ,  0, 15, 10, 10, 5 },   // LTerminate    DIR=0
        /* 6*/ //{ 70 ,  0, 16, 11, -7, 5 },   // Shuting signalㅛ
		/* 6*/ { 75 ,  0, 11, 11, -12, 5 },   // Shuting signalㅛ
        /* 7*/ //{ 86 ,  0, 16, 11, -7, 5 },   // Main signal
	    /* 7*/ { 91 ,  0, 11, 11, -12, 5 },   // Main signal
        /* 8*/ { 70 , 11,  9,  9,  4, 4 },   // signal small lamp
        /* 9*/ {  4 ,  0, 15, 10, 10, 5 },   // LStraight  직선 dir=0
        /*10*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*11*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*12*/ {121 ,  0, 24, 17, 19, 8 },   // 방향표시
        /*13*/ {  4 , 10, 16, 10, -5, 4 },   //방향표시 직선
        /*14*/ {  4 ,  0, 15, 10, 10, 5 },   //
        /*15*/ {  2 , 10, 15, 10,  5,10 },   //

		/*16*/ { 15 , 10,  5, 10,  5, 5 },   // TERMINATE   직선 DIR=4
        /*17*/ {  2 , 10, 15, 10,  5, 5 },   // LStraight  직선 dir=4
        /*18*/ { 22 , 10, 16, 10,  6, 5 },   // Straight  사선   DIR=3,7
        /*19*/ { 40 , 10, 15, 10,  3, 5 },   // Elbow
        /*20*/ { 390 , 10, 20, 9, 11, 5 },   // Terminate  사선 우하
		
        /*21*/ {  5 , 10, 15, 10,  5, 5 },   // LTerminate    DIR=4
        /*22*/ {  0 , 20, 10, 20,  5, 10 },   //BUTTON ON (
        /*23*/ {  5 , 20, 10, 20,  5, 10 },   //BUTTON ON |
        /*24*/ { 10 , 20, 10, 20,  5, 10 },   //BUTTON ON )
        /*25*/ { 20 , 20, 10, 20,  5, 10 },   //BUTTON OFF (
        /*26*/ { 25 , 20, 10, 20,  5, 10 },   //BUTTON OFF |
        /*27*/ { 30 , 20, 10, 20,  5, 10 },   //BUTTON OFF )
        /*28*/ { 79 , 11,  9,  9,  4, 4 },   // signal small lamp Up
        /*29*/ { 88 , 11,  9,  9,  4, 4 },   // signal small lamp Down
        /*30*/ {102 , 21, 13, 13,  6, 6 },   // TTB Normal
        /*31*/ {102 , 61, 13, 13,  6, 6 },   // TTB On

        /*32*/ {161 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=2
        /*33*/ {161 ,  5, 10, 10,  5, 5 },   //	Straight   직선 DIR=2,6
        /*34*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*35*/ { 40 , 20, 15, 10, 8, 5 },   // Elbow
        /*36*/ { 390 , 21, 20, 9,  6, 4 },   // Terminate  사선 좌상		
        /*37*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*38*/ //{ 70 , 20, 16, 11, 23, 5 },   // Shuting signal
		/*38*/ { 70 , 20, 11, 11, 23, 5 },   // Shuting signal
        /*39*/// { 86 , 20, 16, 11, 23, 5 },   // Main signal
		/*39*/ { 86 , 20, 11, 11, 23, 5 },   // Main signal
        /*40*/ { 70 , 31,  9,  5,  4, 2 },   // 선별등 OFF
        /*41*/ { 79 , 31,  9,  5,  4, 2 },   // 선별등 ON
        /*42*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*43*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*44*/ {121 , 20, 24, 17,  3, 8 },   //방향표시
        /*45*/ {  0 ,  0, 16, 10, 19, 4 },   //방향표시 직선
        /*46*/ {164 ,141, 15, 15,  7, 7 },   // LDK Signal Off/On
        /*47*/ {  0 ,  0,  0,  0,  0, 0 },   //

        /*48*/ {171 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=6
        /*49*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*50*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*51*/ { 40 , 30, 15, 10, 8, 5 },   // Elbow
	    /*52*/ { 390 , 30, 20, 9,  6, 5 },   // Terminate  사선 좌하
        /*53*/ { 170, 11,  6,  6,  2, 2 },   // 입환신호기 내부 원 
        /*54*/ { 70 ,  0,  5, 11, -7, 5 },   // 신호기 막대 부분 왼
        /*55*/ { 81 , 20,  5, 11, 12, 5 },   // 신호기 막대 부분 오
        /*56*/ {161 ,  0, 20, 11, -12, 5 },  // 3,4,5현시 신호등 오
		/*57*/ {161 ,  0, 20, 11,  32, 5 },  // 3,4,5현시 신호등 
        /*58*/ {161 , 11,  9,  9, -13, 4 },   //       RIGHT 
        /*59*/ {161 , 11,  9,  9,  22, 4 },   //       LEFT	
        /*60*/ {216 ,  6, 10, 10,  6, 10 },   //   수직선 아래끝
        /*61*/ {216 , 18, 10, 10,  6, 0 },   //   수직선 위 끝 
		/*62*/ {216 ,  0, 10, 10,  6, 5 },   //   수직 직선 
        /*63*/ {181 ,  0, 14, 10,  6, 5 },   //  bottom , right_top 
	// 추가 		
		/*64*/ { 199,  4, 14, 10,  10, 5 },  //  bottom , left_top
        /*65*/ { 181, 15, 14, 10,  6, 5 },   //   top , right_bottom
		/*66*/ { 199 ,11, 14, 10,  10, 5 },   //   top , left_bottom	
		/*67*/ { 390 ,  1, 20,   9, 11, 4 },  //  4번 Terminate  사선 우상 (LEFT_BOTTOM)
		/*68*/ { 390 , 10, 20,   9, 11, 5 },  // 20번 Terminate  사선 우하 (LEFT_TOP)
		/*69*/ { 390 , 21, 20, 9,  6, 4 },   // 36번 Terminate  사선 좌상 (RIGHT_BOTTOM)
		/*70*/ { 390 , 30, 20, 9,  6, 5 },    // 52번 Terminate  사선 좌하 (RIGHT_TOP)
		/*71*/ { 228, 0, 10, 10,  6, 5 },     // 수직 굴절    LEFT_BOTTOM
		/*72*/ { 239, 0, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_BOTTOM
		/*73*/ { 228, 10, 10, 10,  6, 5 },     // 수직 굴절    LEFT_TOP
		/*74*/ { 239, 10, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_TOP
// 쭌 수정
        /*75*/ { 226,  0, 10, 16,  5, 8 },   //BUTTON ON ( 도착점
        /*76*/ { 233,  0, 5, 16,  2, 8 },   //BUTTON ON |  
        /*77*/ { 236,  0, 10, 16,  5, 8 },   //BUTTON ON )
        /*78*/ { 226, 18, 10, 16,  5, 8 },   //BUTTON OFF (  
        /*79*/ { 236, 18, 5, 16,  2, 8 },   //BUTTON OFF |  
        /*80*/ { 236, 18, 12, 16,  5, 8 },   //BUTTON OFF )  
// 쭌 수정끝
	    /*81*/ { 5, 200, 5, 10,  3, 5 },	// 트랙 일부분(5*10)	
		/*82*/ { 300, 194,  85,  38,  35, 19 },   // 인접역 표시 화살표 우
		/*83*/ { 300, 237,  85,  38,  43, 20 },   // 인접역 표시 화살표 좌
// 쭌 수정(추가)
        /*84*/ { 291,  0, 10, 16,  5, 8 },   //BUTTON ON ( 도착점
        /*85*/ { 298,  0, 5, 16,  2, 8 },   //BUTTON ON |  
        /*86*/ { 301,  0, 10, 16,  5, 8 },   //BUTTON ON )
        /*87*/ { 291, 18, 10, 16,  5, 8 },   //BUTTON OFF (  
        /*88*/ { 298, 18, 5, 16,  2, 8 },   //BUTTON OFF |  
        /*89*/ { 301, 18, 12, 16,  5, 8 },   //BUTTON OFF )  
        /*90*/ { 181 , 24,  16,  16,  8, 8 },   // 전철기 normal
        /*91*/ { 199 , 24,  16,  16,  8, 8 },   // 전철기 reverse
        /*92*/ { 314 , 20, 18, 23,  12, 13 },
        /*93*/ { 324 , 20, 12, 23,  9, 13 },   //BUTTON ON |
        /*94*/ { 331 , 20, 16, 23,  7, 13 },   //BUTTON ON )
        /*95*/ { 355 , 60, 23, 23, 11, 11}, // 조작반


// 쭌 수정끝
};

SymbolBitmapInfo SymbolInfo8[] = 
{
//	int x,y,w,h,cx,cy;
        /* 0*/ {  0 ,  0,  4, 8,  1, 4 },   // Terminate  직선 DIR=0
        /* 1*/ {  5 ,  0,  4, 8,  1, 4 },   // Straight   직선 DIR=0,4
        /* 2*/ { 20 ,  0, 14, 8,  7, 4 },   // Straight  사선 DIR=1,5
        /* 3*/ { 40 ,  0, 12, 8,  4, 4 },   // Elbow
        /* 4*/ { 56 ,  0, 10, 5,  8, 0 },   // Terminate  사선 우상		
        /* 5*/ {  0 ,  0, 15, 8, 10, 5 },   // LTerminate    DIR=0
        /* 6*/ //{ 70 ,  0, 16, 11, -7, 5 },   // Shuting signalㅛ
		/* 6*/ { 75 ,  0, 11, 11, -12, 5 },   // Shuting signalㅛ
        /* 7*/ //{ 86 ,  0, 16, 11, -7, 5 },   // Main signal
	    /* 7*/ { 91 ,  0, 11, 11, -12, 5 },   // Main signal
        /* 8*/ { 70 , 11,  9,  9,  4, 4 },   // signal small lamp
        /* 9*/ {  4 ,  0, 15, 10, 10, 5 },   // LStraight  직선 dir=0
        /*10*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*11*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*12*/ {121 ,  0, 24, 17, 19, 8 },   // 방향표시
        /*13*/ {  6 , 10, 14, 8, -5, 3 },   //방향표시 직선
        /*14*/ {  4 ,  0, 15, 10, 10, 5 },   //
        /*15*/ {  2 , 10, 15, 10,  5,10 },   //

		/*16*/ { 16 , 10,  4,  8,  4, 4 },   // TERMINATE   직선 DIR=4
        /*17*/ {  2 , 10, 15, 10,  5, 5 },   // LStraight  직선 dir=4
        /*18*/ { 20 , 10, 14, 8,   7, 4 },   // Straight  사선   DIR=3,7
        /*19*/ { 40 , 10, 12,  8,  4, 4 },   // Elbow
        /*20*/ { 56 , 13, 10,  5,  8, 5 },   // Terminate  사선 우하
		
        /*21*/ {  5 , 10, 15, 10,  5, 5 },   // LTerminate    DIR=4
        /*22*/ {  0 , 20, 10, 20,  5, 10 },   //BUTTON ON (
        /*23*/ {  5 , 20, 10, 20,  5, 10 },   //BUTTON ON |
        /*24*/ { 10 , 20, 10, 20,  5, 10 },   //BUTTON ON )
        /*25*/ { 20 , 20, 10, 20,  5, 10 },   //BUTTON OFF (
        /*26*/ { 25 , 20, 10, 20,  5, 10 },   //BUTTON OFF |
        /*27*/ { 30 , 20, 10, 20,  5, 10 },   //BUTTON OFF )
        /*28*/ { 79 , 11,  9,  9,  4, 4 },   // signal small lamp Up
        /*29*/ { 88 , 11,  9,  9,  4, 4 },   // signal small lamp Down
        /*30*/ {102 , 21, 11, 11,  5, 5 },   // TTB Normal
        /*31*/ {102 , 61, 11, 11,  5, 5 },   // TTB On

        /*32*/ {161 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=2
        /*33*/ {161 ,  5, 10, 10,  5, 5 },   //	Straight   직선 DIR=2,6
        /*34*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*35*/ { 40 , 20, 12,  8,  9, 4 },   // Elbow
        /*36*/ { 55 , 20, 13,  5,  4, 0 },   // Terminate  사선 좌상		
        /*37*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*38*/ //{ 70 , 20, 16, 11, 23, 5 },   // Shuting signal
		/*38*/ { 70 , 20, 11, 11, 23, 5 },   // Shuting signal
        /*39*/// { 86 , 20, 16, 11, 23, 5 },   // Main signal
		/*39*/ { 86 , 20, 11, 11, 23, 5 },   // Main signal
        /*40*/ { 70 , 31,  9,  5,  4, 2 },   // 선별등 OFF
        /*41*/ { 79 , 31,  9,  5,  4, 2 },   // 선별등 ON
        /*42*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*43*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*44*/ {121 , 20, 24, 17,  3, 8 },   //방향표시
        /*45*/ {  0 ,  0, 14,  8, 17, 3 },   //방향표시 직선
        /*46*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*47*/ {  0 ,  0,  0,  0,  0, 0 },   //

        /*48*/ {171 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=6
        /*49*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*50*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*51*/ { 42 , 30, 10,  8,  7, 4 },   // Elbow		
	    /*52*/ { 54 , 33, 12,  5,  5, 5 },   // Terminate  사선 좌하
        /*53*/ { 170, 11,  6,  6,  2, 2 },   // 입환신호기 내부 원 
        /*54*/ { 70 ,  0,  5, 11, -7, 5 },   // 신호기 막대 부분 왼
        /*55*/ { 81 , 20,  5, 11, 12, 5 },   // 신호기 막대 부분 오
        /*56*/ {161 ,  0, 20, 11, -12, 5 },  // 3,4,5현시 신호등 오
		/*57*/ {161 ,  0, 20, 11,  32, 5 },  // 3,4,5현시 신호등 
        /*58*/ {161 , 11,  9,  9, -13, 4 },   //       RIGHT 
        /*59*/ {161 , 11,  9,  9,  22, 4 },   //       LEFT	
        /*60*/ {216 ,  6, 10, 10,  6, 10 },   //   수직선 아래끝
        /*61*/ {216 , 18, 10, 10,  6, 0 },   //   수직선 위 끝 
		/*62*/ {216 ,  0, 10, 10,  6, 5 },   //   수직 직선 
        /*63*/ {181 ,  0, 14, 10,  6, 5 },   //  bottom , right_top 
	// 추가 		
		/*64*/ { 199,  4, 14, 10,  10, 5 },  //  bottom , left_top
        /*65*/ { 181, 15, 14, 10,  6, 5 },   //   top , right_bottom
		/*66*/ { 199 ,11, 14, 10,  10, 5 },   //   top , left_bottom	
			//4{ 56 ,  0, 10,  5,  9, 0 },   // Terminate  사선 우상		
		/*67*/ { 56 ,  0, 10,  7,  7, 3 },  //  4번 Terminate  사선 우상 (LEFT_BOTTOM)
		   //20{ 56 , 13, 10,  5,  9, 5 },   // Terminate  사선 우하
		/*68*/ { 56 , 11, 10,  8,  7, 4 },  // 20번 Terminate  사선 우하 (LEFT_TOP)
		   //36{ 55 , 21, 13,  5,  5, 0 },   // Terminate  사선 좌상		
		/*69*/ { 55 , 20, 13,  7,  5, 2 },   // 36번 Terminate  사선 좌상 (RIGHT_BOTTOM)
		   //52{ 54 , 32, 12,  5,  7, 5 },   // Terminate  사선 좌하
		/*70*/ { 54 , 30, 12,  8,  7, 4 },    // 52번 Terminate  사선 좌하 (RIGHT_TOP)
		/*71*/ { 228, 0, 10, 10,  6, 5 },     // 수직 굴절    LEFT_BOTTOM
		/*72*/ { 239, 0, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_BOTTOM
		/*73*/ { 228, 10, 10, 10,  6, 5 },     // 수직 굴절    LEFT_TOP
		/*74*/ { 239, 10, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_TOP
//
	    /*75*/ { 226,  0, 8, 14,  4, 7 },   //BUTTON ON ( 도착점
        /*76*/ { 231,  0, 8, 14,  4, 7 },   //BUTTON ON |
        /*77*/ { 236,  0, 8, 14,  4, 7 },   //BUTTON ON )
        /*78*/ { 226, 18, 8, 14,  4, 7 },   //BUTTON OFF (
        /*79*/ { 231, 18, 8, 14,  4, 7 },   //BUTTON OFF |
        /*80*/ { 236, 18, 8, 14,  4, 7 },   //BUTTON OFF )		
		/*81*/ { 5, 200, 4, 8,  3, 4 },   //BUTTON OFF )
		/*82*/ { 250, 0,  27,  9,  13, 5 },   // 인접역 표시 화살표 우
		/*83*/ { 250, 15, 27,  9,  13, 5 },   // 인접역 표시 화살표 좌
			

};

//};		 7 토트용 테이블 
SymbolBitmapInfo SymbolInfo7[] = 
{
//	int x,y,w,h,cx,cy;
		/* 0*/ {  0 ,  0,  4,  7,  0, 4 },   // Terminate  직선 DIR=0
        /* 1*/ {  4 ,  0,  5,  7,  2, 4 },   // Straight   직선 DIR=0,4
        /* 2*/ { 20 ,  0, 11, 7,  7, 4 },   // Straight  사선 DIR=1,5
        /* 3*/ { 40 ,  0, 9, 7,  4, 4 },   // Elbow
        /* 4*/ { 55 ,  0, 15,  6, 10, 1 },   // Terminate  사선 우상		
        /* 5*/ {  0 ,  0, 15, 10, 8, 5 },   // LTerminate    DIR=0
        /* 6*/ //{ 70 ,  0, 16, 11, -7, 5 },   // Shuting signalㅛ
		/* 6*/ { 75 ,  0, 11, 11, -12, 5 },   // Shuting signalㅛ
        /* 7*/ //{ 86 ,  0, 16, 11, -7, 5 },   // Main signal
	    /* 7*/ { 99 ,  0, 9, 9, -10, 4 },   // Main signal
        /* 8*/ { 81 , 12, 7,  7,  3, 3 },   // signal small lamp
        /* 9*/ {  4 ,  0, 15, 10, 10, 5 },   // LStraight  직선 dir=0
        /*10*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*11*/ {  0 ,  0, 10, 21,  5, 5 },   //
        /*12*/ {121 ,  0, 24, 17, 19, 8 },   // 방향표시
        /*13*/ {  4 , 10, 16, 10, -4, 5 },   //방향표시 직선
        /*14*/ {  4 ,  0, 15, 10, 10, 5 },   //
        /*15*/ {  2 , 10, 15, 10,  5,10 },   //

		/*16*/ { 11 , 10,  4, 7,  4, 4 },   // TERMINATE   직선 DIR=4
        /*17*/ {  2 , 10, 11, 7,  4, 4 },   // LStraight  직선 dir=4
        /*18*/ { 20 , 8, 12, 7,  7, 4 },   // Straight  사선   DIR=3,7
        /*19*/ { 40 , 10, 13, 7,  4, 4 },   // Elbow
        /*20*/ { 61 , 14, 12,  4, 10, 4 },   // Terminate  사선 우하
		
        /*21*/ {  5 , 10, 15, 10,  4, 4 },   // LTerminate    DIR=4
        /*22*/ {  0 , 20, 7, 14,  4, 4 },   //BUTTON ON (
        /*23*/ {  2 , 20, 7, 14,  4, 4 },   //BUTTON ON |
        /*24*/ { 6 , 20, 7, 14,  4, 4 },   //BUTTON ON )
        /*25*/ { 14 , 20, 7, 14,  4, 4 },   //BUTTON OFF (
        /*26*/ { 16 , 20, 7, 14,  4, 4 },   //BUTTON OFF |
        /*27*/ { 20 , 20, 7, 14,  4, 4 },   //BUTTON OFF )
        /*28*/ { 79 , 11,  9,  9,  4, 3 },   // signal small lamp Up
        /*29*/ { 88 , 11,  9,  9,  3, 4 },   // signal small lamp Down
        /*30*/ {120 , 17,  6,  6,  3, 3 },   // TTB Normal
        /*31*/ {126 , 17,  6,  6,  3, 3 },   // TTB On

        /*32*/ {161 ,  0, 10, 10,  4, 4 },   //	Terminate  직선 DIR=2
        /*33*/ {161 ,  5, 10, 10,  4, 4 },   //	Straight   직선 DIR=2,6
        /*34*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*35*/ { 40 , 20, 15, 10, 8, 4 },   // Elbow
        /*36*/ { 60 , 19, 12,  5,  2, 1 },   // Terminate  사선 좌상		
        /*37*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*38*/ //{ 70 , 20, 16, 11, 23, 5 },   // Shuting signal
		/*38*/ { 82 , 22, 8, 8, 20, 4 },   // Shuting signal
        /*39*/// { 86 , 20, 16, 11, 23, 5 },   // Main signal
		/*39*/ { 95 , 21, 9, 9, 20, 4},   // Main signal
        /*40*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*41*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*42*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*43*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*44*/ {121 , 20, 24, 17,  3, 8 },   //방향표시
        /*45*/ {  0 ,  0, 16, 10, 18, 5 },   //방향표시 직선
        /*46*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*47*/ {  0 ,  0,  0,  0,  0, 0 },   //

        /*48*/ {171 ,  0, 10, 10,  5, 5 },   //	Terminate  직선 DIR=6
        /*49*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*50*/ {  0 ,  0,  0,  0,  0, 0 },   //
        /*51*/ { 43 , 30, 11, 9, 8, 4 },   // Elbow
	    /*52*/ { 60 , 30, 12,  6,  3, 4 },   // Terminate  사선 좌하
        /*53*/ { 181, 11,  4,  4,  2, 2 },   // 입환신호기 내부 원 
        /*54*/ { 80 ,  0,  4, 8, -6, 4 },   // 신호기 막대 부분 왼
        /*55*/ { 90 , 22,  4, 8, 10, 4 },   // 신호기 막대 부분 오
        /*56*/ {175 ,  3, 13, 7, -10, 4 },  // 3,4,5현시 신호등 오
		/*57*/ {175 ,  3, 13, 7,  27, 4 },  // 3,4,5현시 신호등 
        /*58*/ {175 , 11,  5,  5, -12, 3 },   //       RIGHT 
        /*59*/ {175 , 11,  5,  5,  26, 3 },   //       LEFT	
        /*60*/ {215 ,  6, 10, 10,  5, 8 },   //   수직선 아래끝
        /*61*/ {215 , 18, 10, 10,  5, 0 },   //   수직선 위 끝 
		/*62*/ {215 ,  0, 10, 10,  5, 4 },   //   수직 직선 
        /*63*/ {180 ,  0, 14, 10,  5, 4 },   //  bottom , right_top 
	// 추가 		
		/*64*/ { 198,  4, 14, 10,  8, 4 },  //  bottom , left_top
        /*65*/ { 180, 15, 14, 10,  5, 4 },   //   top , right_bottom
		/*66*/ { 198 ,11, 14, 10,  9, 4 },   //   top , left_bottom	
		/*67*/ { 55 ,  0, 15,  10, 9, 3 },  //  4번 Terminate  사선 우상 (LEFT_BOTTOM)
		/*68*/ { 55 , 10, 15,   9, 8, 4 },  // 20번 Terminate  사선 우하 (LEFT_TOP)
		/*69*/ { 55 , 20, 15, 10,  3, 3 },   // 36번 Terminate  사선 좌상 (RIGHT_BOTTOM)
		/*70*/ { 55 , 30, 15, 9,  4, 4 },    // 52번 Terminate  사선 좌하 (RIGHT_TOP)
		/*71*/ { 228, 0, 10, 10,  6, 5 },     // 수직 굴절    LEFT_BOTTOM
		/*72*/ { 239, 0, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_BOTTOM
		/*73*/ { 228, 10, 10, 10,  6, 5 },     // 수직 굴절    LEFT_TOP
		/*74*/ { 239, 10, 10, 10,  6, 5 },     // 수직 굴절    RIGHT_TOP
//
        /*75*/ { 226,  0, 10, 16,  5, 8 },   //BUTTON ON ( 도착점
        /*76*/ { 231,  0, 10, 16,  5, 8 },   //BUTTON ON |
        /*77*/ { 236,  0, 10, 16,  5, 8 },   //BUTTON ON )
        /*78*/ { 226, 18, 10, 16,  5, 8 },   //BUTTON OFF (
        /*79*/ { 231, 18, 10, 16,  5, 8 },   //BUTTON OFF |
        /*80*/ { 236, 18, 10, 16,  5, 8 },   //BUTTON OFF )		
		/*81*/ { 5, 200, 4, 8,  3, 4 },   
		/*82*/ { 250, 0,  25,  9,  12, 5 },   // 인접역 표시 화살표 좌
		/*83*/ { 250, 15, 25,  9,  12, 5 },   // 인접역 표시 화살표 우

        
};


BOOL _BlinkDuty;
DWORD _DrawMode = SRCCOPY;
DWORD _DrawMode2 = MERGECOPY;
CDC *_MyImageDC = NULL;
CDC *_MyImageDC1 = NULL;
CDC *_MyTimer = NULL;

SymbolBitmapInfo *SymbolInfo = SymbolInfo10;

// Bipmap 이미지 불러오기
void BmpDraw::Draw(int id) {
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;				

	SymbolBitmapInfo &b = SymbolInfo[id];
	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			,_DrawMode);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			,MERGECOPY );
            
	}
}


void BmpDraw::BMPOutSh( int dir )
{
	BOOL chek=0;
	if(dir > 19 && dir < 24)   //입환 신호기
	{						 
		dir -= 4;
		chek=1;
	}
	int id = 6;
	if (dir & 1)
	{
		id = 0x26;
	//	Draw(55);										// 막대 그리기 
	}
	//else Draw(54);
	int color = ((Base & 0xfff)>>4)*10;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && !_BlinkDuty) color = (MY_YELLOW>>4)*10;

	SymbolBitmapInfo &b = SymbolInfo[id];
	int xx = b.x;
	if (dir & 2) 
		xx += 75;		
	if ( m_pDC && WP.x && WP.y)
	{
		m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy,
			 b.w, b.h, _MyImageDC,
			 xx, b.y + color, _DrawMode);
	}
	if(chek)
	{								       // 입환 신호기 안에 원찍기
		SymbolBitmapInfo b1 = SymbolInfo[53];
		int yy = (WP.y - b.cy) + (b.h / 2);        // 원 위치가 신호기 중앙에 
			xx = (WP.x - b.cx) + (b.w / 2);	
		if(id == 6)   xx-=1;
		if (!(dir & 2)) yy-=1;
		if ( m_pDC && WP.x && WP.y)
		{
			m_pDC->BitBlt( xx - b1.cx, yy - b1.cy,
				 b1.w, b1.h, _MyImageDC,b1.x, b1.y, _DrawMode);
		}
	}
}

extern int _nDotPerCell;

void BmpDraw::DrawLong()
{	
	if (!m_nRepeat) return;
	int color = ((Base & 0xff0)>>6) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>6) * _nGRIDSIZE;

	while (m_nRepeat)
	{
		int len = m_nRepeat;
		if (len > 58) len = 58;
		SymbolBitmapInfo &b = SymbolInfo[81]; // 트랙 일부분(5*10)
		if (m_pDC && WP.x && WP.y)
		{
			if (m_pDC->IsPrinting())				
				m_pDC->BitBlt( WP.x - b.cx , WP.y - b.cy, len * b.w , b.h, _MyImageDC1, b.x, b.y + color, _DrawMode);
			else				
				m_pDC->BitBlt( WP.x - b.cx , WP.y - b.cy, len * b.w , b.h, _MyImageDC, b.x, b.y + color, _DrawMode);
		}

		m_nRepeat -= len;
		WP.x += len * (_nDotPerCell/2);
	}

}

CDC *BmpDraw::m_pDC = NULL;
int BmpDraw::m_nRepeat = 0;

// Base = 250, 30
// Size = 20, 20

void BmpDraw::DrawInhibit( CPoint &point, int nType )
{
	if (m_pDC)
	{
        int bx = 250;
        if (nType < 1 || nType > 3) return; //nType = 1,2,3
        int by = 10 + nType * 20;
		m_pDC->BitBlt( point.x - 10 , point.y - 10, 20, 20, _MyImageDC, bx+20, by, SRCAND);
		m_pDC->BitBlt( point.x - 10 , point.y - 10, 20, 20, _MyImageDC, bx, by, SRCPAINT);
	}
}

void BmpDraw::PutChar( CPoint &point, int nChar, int nColor )
{
    if ( (nChar >= '0' && nChar <= '9') || (nChar >= 'A' && nChar <= 'Z') )
	{
        nChar -= '0';
    }
    else nChar = 10;    // SPACE

    int yBase = 250 + ((nColor >> 6) & 0xF )* 10;
	m_pDC->BitBlt( point.x, point.y - 4, 5, 8, _MyImageDC, nChar * 5, yBase, _DrawMode);
    point.x += 5;
}


void BmpDraw::DrawButton(int id, DWORD _DrawMode2)
{
	int color = ((Base & 0xff0)>>4) * _nGRIDSIZE;
	if ((Base & 0x1000) && !_BlinkDuty) color = 0;
	if ((Base & 0x2000) && _BlinkDuty) color = ((MY_YELLOW & 0xff0)>>4) * _nGRIDSIZE;				

	SymbolBitmapInfo &b = SymbolInfo[id];
	if (m_pDC && WP.x && WP.y)
	{
		if (m_pDC->IsPrinting())
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC1, b.x, b.y + color
			, _DrawMode2);
		else
			m_pDC->BitBlt( WP.x - b.cx, WP.y - b.cy, b.w, b.h, _MyImageDC, b.x, b.y + color
			, _DrawMode2 );
            
	}
}
