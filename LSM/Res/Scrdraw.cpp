// -------------------------------------------------------------------------- //
#define XBUG

// -------------------------------------------------------------------------- //
#include <owl\owlpch.h>

#include "ercsta.h"
#include "trainno.h"
#include "wcommand.h"
#ifdef    ERC
#include "kidhandl.h"
#else  // ERC
#include "eidhandl.h"
#endif // ERC
#include "weip.rh"
#include "zcmdchk.h"
#include "zscrcomm.h"
#include "ebutton.h"
#include "sysstate.h"

/* SCREEN DRAWING PROCEDURE
	SCRDRAW.CPP
*/

#define OPDELAY 15

#define LSWINX MWINX
#define LSWINY MWINY

#define CELLSIZE  10
#define CELLSIZEH  5

#define MY_BLUE    0x000
#define MY_RED     0x040
#define MY_YELLOW  0x080
#define MY_GREEN   0x0C0
#define MY_GRAY    0X100
#define MY_FBLUE    0x1000
#define MY_FRED     0x1040
#define MY_FYELLOW  0x1080
#define MY_FGREEN   0x10C0
#define MY_FGRAY    0X1100

#define LDPBUFSIZE  (24 * 32)

//#if defined(BI_PLAT_WIN16)

//#endif

#ifdef    ERC
RelayBase *RDF = 0;
#endif // ERC

extern int  _NoControl;
extern int  isUsedMCISound;

int   FlashIsOn = 1, ForceDraw = 0;
extern TMemoryDC *LockSaveDC;
extern TMemoryDC *LockDC[];
extern TMemoryDC *LockDCmask[];
DWORD DrawMode = SRCPAINT;
extern TLineStatus *StationLS = 0;
extern CommandLogic* _Commander;
TPoint *GetRoute(char *cmd);
void MakeRoute(char *,char *);

char HStationName[16];
char HStationUp[16];
char HStationDown[16];
char *GetHStationName() {return &HStationName[0];}
char *GetCompanyName() {return "(Ada System@1996 - 1998)";}

TPoint *PreRoute = 0;

unsigned char IOExtBuffer[LDPBUFSIZE];

void DrawPreRoute(HDC *mdc);

void DrawPreRoute(HDC *mdc) {
	if (PreRoute) {
		TPoint *p = PreRoute;
		HPEN hpen, hpenOld;

		hpen = CreatePen(PS_SOLID, 2, RGB(255, 255, 0));
		hpenOld = (HPEN)SelectObject(*mdc, hpen);
		MoveToEx(*mdc, p->x, p->y,0);
//		MoveTo(*mdc, p->x, p->y);
		p++;
		while (PreRoute && p->x && p->y) {
			LineTo(*mdc, p->x, p->y);
			p++;
		}

		SelectObject(*mdc, hpenOld);
		DeleteObject(hpen);
	}
}

void MsgPrintf(char *fmt,...);
void AbnormalExit(char *a,char *b);
void KeyCheck3();

int _GetState(int n) {
  if (n == -1) return 0;
  if (n == -2) return 1;
  int isinv = n & 0x8000;
  n &= 0x7fff;
  if (n>24*32) return 0;
  if (isinv) return !(int)(IOExtBuffer[n]);
  return (int)(IOExtBuffer[n]);
}

/*
void BitOn(int id) {
  IOExtBuffer[id] = 1;
}
void BitOff(int id) {
  IOExtBuffer[id] = 0;
}
*/
int _cdecl xsprintf(char *,const char *,...) { return 0; }
int _cdecl (*mysprintf)(char *,const char *,...) = xsprintf;
//#define _DEMO

class TFormattedStatic: public TStatic {
  public:
	 TFormattedStatic(TWindow* parent, int resId, const char far* text,
							const char far* text2 = 0);
	~TFormattedStatic();

  protected:
	 void SetupWindow();

  private:
	 char far* Text;    // Text to display
	 char far* Text2;
};

// WM_CTLCOLOR
#define CONFIRMDLGCOLOR (TColor(255,255,0))

// CLASS SIGNAL OP WINDOW
int xxx = 0;
static HBRUSH hbrYellow;
class ConfirmWindow : public TDialog {
public:
	ConfirmWindow(TWindow *parent, char *s1, char *s2, char *s3)
	: TDialog(parent,IDD_CONFIRM) {
		TDialog::SetCaption(s1);
		new TFormattedStatic(this, IDC_LINE1, s2);
		new TFormattedStatic(this, IDC_LINE2, s3);
		SetBkgndColor(CONFIRMDLGCOLOR);
		hbrYellow = CreateSolidBrush(RGB(255, 255, 0));
		xxx = 1;
	}
	virtual ~ConfirmWindow() { xxx = 0; }
//EV_WM_CTLCOLOR	HBRUSH EvCtlColor(HDC, HWND hWndChild, uint ctlType)
	HBRUSH EvCtlColor(HDC, HWND hWndChild, uint ctlType);

	DECLARE_RESPONSE_TABLE(ConfirmWindow);
};

DEFINE_RESPONSE_TABLE1(ConfirmWindow, TDialog)
  EV_WM_CTLCOLOR,
END_RESPONSE_TABLE;

// WM_CTLCOLOR

HBRUSH ConfirmWindow::EvCtlColor(HDC hdc, HWND hWndChild, uint ctlType) {
  switch(ctlType) {
		case CTLCOLOR_STATIC:
			 SetTextColor(hdc, RGB(0, 0, 0));
			 SetBkColor(hdc, RGB(255, 255, 0));
			 return hbrYellow;
  }
  return 0;
}

int Confirm(TWindow*parent,char*s1,char*s2,char*s3) {
	return ConfirmWindow(parent,s1,s2,s3).Execute();
}

// CLASS SIGNAL OP WINDOW END

TFormattedStatic::TFormattedStatic(TWindow* parent, int resId,
											  const char far* text, const char far* text2)
:
  TStatic(parent, resId, 0)
{
  Text = strnewdup(text);
  Text2 = text2 ? strnewdup(text2) : 0;
}
TFormattedStatic::~TFormattedStatic()
{
  delete[] Text;
  delete[] Text2;
}
void
TFormattedStatic::SetupWindow()
{
  TStatic::SetupWindow();
  static char buff[256];
  mysprintf(buff, Title, Text, Text2);
  SetText(buff);
  SetBkgndColor(CONFIRMDLGCOLOR);
}


HDC *LSmDC = 0;

class BmpDraw {
	 int Base,dir;
	 TPoint WP;
	 HDC *mdc;

  public:
	 BmpDraw(HDC *imdc) {
		mdc = imdc;
	 }
	 void SetBase(int c) { Base = c; }
	 void SetWP(TPoint &p) { WP = p; }
	 TPoint &GetWP() { return WP; }
	 void Straight( int vector );
	 void LStraight( int vector );
	 void Terminate( int vector );
	 void LTerminate( int vector );
	 void Elbow( int vector);
	 void Feed( int vector );
	 int LineDraw( int Headisopen, TPoint &p1, TPoint &p2, int color, int isopen );
	 void BMPOut(int);
	 void BMPOutSh(int);
};

int _XGetState(int n) { return n; }

class SymbolBitmapInfo {
	int x,y,w,h,cx,cy;
friend BmpDraw;
  public:
	void Readfile(istream &is) {
		is >> x >> y >> w >> h >> cx >> cy;
	}
};

SymbolBitmapInfo *SymbolInfo = 0;

void
BmpDraw::BMPOut(int id) {
  if (SymbolInfo) {
	 int color = ((Base & 0xfff)>>4)*10;
	 if (SystemState & SYS_COMERR) {
		 if (!FlashIsOn) color = (MY_RED>>4)*10;
		 else color = 0;
	 }
	 else {
		 if ((Base & 0x1000) && !FlashIsOn) color = 0;
		 if ((Base & 0x2000) && !FlashIsOn) color = (MY_YELLOW>>4)*10;
	 }
	 SymbolBitmapInfo &b = SymbolInfo[id];
	 if (mdc && WP.x && WP.y) {
		BitBlt(*mdc,WP.x - b.cx, WP.y - b.cy,
				 b.w, b.h, *memDC,
				 b.x, b.y + color, DrawMode);
	 }
  }
}

void
BmpDraw::BMPOutSh(int dir) {
  int id = 6;
  if (dir & 1) id = 0x26;
  if (SymbolInfo) {
	 int color = ((Base & 0xfff)>>4)*10;
	 if (SystemState & SYS_COMERR) {
		 if (!FlashIsOn) color = (MY_RED>>4)*10;
		 else color = 0;
	 }
	 else {
		 if ((Base & 0x1000) && !FlashIsOn) color = 0;
		 if ((Base & 0x2000) && !FlashIsOn) color = (MY_YELLOW>>4)*10;
	 }
	 SymbolBitmapInfo &b = SymbolInfo[id];
	 int xx = b.x;
	 if (dir & 2) {
		xx += 75;
	 }
	 if (mdc && WP.x && WP.y) {
		BitBlt(*mdc,WP.x - b.cx, WP.y - b.cy,
				 b.w, b.h, *memDC,
				 xx, b.y + color, DrawMode);
	 }
  }
}

int BmpDraw::LineDraw( int Headisopen, TPoint &p1, TPoint &p2, int base, int isopen ) {
  int lx, ly, i, tx;
  TPoint delta = p2 - p1;
  lx = abs(delta.x);
  ly = abs(delta.y);
  int size1, size2=0;
  Base = base;
  if (ly) {       // Elbow
/*
		  lx
	|           |
	 ======\\   |  --
			  \\  |    ly
				\\ |
				 \\   --

	  lx = size1 + ly / 2

*/
	 int ty;
//	 ty = lx-10;
//	 if (ty >= ly/2) ty = ly/2;
	 ty = ly/2;
	 tx = lx-ty;
	 size1 = tx / CELLSIZE;
//	 size1 = (lx - ty) / CELLSIZE;
	 size2 = ty / CELLSIZEH;
	 if (size2 <= 0) size2 = 1;
  }
  else {			// Straight line
	 tx = lx;
	 size1 = tx / CELLSIZE;
  }
  int dir;
  WP = p1;
  if (size1 > 0) {
	 if (delta.x>0) dir = 0;
	 else dir = 4;
//	 if (tx%CELLSIZE) LTerminate(dir);
//	 else Terminate(dir);
//	 for (i=1; i<size1; i++) Straight(dir);

	 if (!Headisopen) {
		 if (tx%CELLSIZE) LTerminate(dir);
		 else Terminate(dir);
		 for (i=1; i<size1; i++) Straight(dir);
	 }
	 else {
		 if (tx%CELLSIZE) LStraight(dir);
		 else Straight(dir);
		 for (i=1; i<size1; i++) Straight(dir);
	 }

  }

  else if (size1==0 && delta.y==0){
	 if (delta.x>0) {
		dir = 0;
		WP.x += 5;
	 }
	 else {
		dir = 4;
		WP.x -= 5;
	 }
	 Terminate(dir);
	 return dir;
  }

  if (size2 > 0) {
	 if (delta.y<0) {
		 if (delta.x<0) dir = 3;
		 else dir = 1;
	 }
	 else {
		 if (delta.x<0) dir = 5;
		 else dir = 7;
	 }
//	 if (size1) Elbow(dir);
//	 else Terminate(dir);
//	 for (i=1; i<size2; i++) Straight(dir);

	 if (size1) Elbow(dir);
	 else if (!Headisopen) Terminate(dir);
	 else Straight(dir);
	 for (i=1; i<size2; i++) Straight(dir);

  }
  if (!isopen) Terminate(dir ^ 4);

  return dir;
}

void
BmpDraw::Feed( int vector ) {
	switch ( vector ) {
		case 0 : WP.x += CELLSIZE; break;
		case 1 : WP.x += CELLSIZEH;
					WP.y -= CELLSIZE; break;
		case 3 : WP.x -= CELLSIZEH;
					WP.y -= CELLSIZE; break;
		case 4 : WP.x -= CELLSIZE; break;
		case 5 : WP.x -= CELLSIZEH;
					WP.y += CELLSIZE; break;
		case 7 : WP.x += CELLSIZEH;
					WP.y += CELLSIZE; break;
	}
}

void
BmpDraw::Straight( int vector )
{
	dir = vector;
	switch (vector) {
		case 0: case 4:
			BMPOut(1); break;
		case 1: case 5:
			BMPOut(2); break;
		case 3: case 7:
			BMPOut(0x12); break;
	}
	Feed(vector);
}

void
BmpDraw::LStraight( int vector )  //size 15 Straight
{
	dir = vector;
	switch (vector) {
		case 0:
			WP += CELLSIZEH;
			BMPOut(0x09); break;
		case 4:
			WP -= CELLSIZEH;
			BMPOut(0x11); break;
	}
	Feed(vector);
}

void
BmpDraw::Elbow( int vector )
{
	dir = vector;
	switch (vector) {
		case 1:
			BMPOut(3); break;
		case 3:
			BMPOut(0x23); break;
		case 5:
			BMPOut(0x33); break;
		case 7:
			BMPOut(0x13); break;
	}
	Feed(vector);
}

void
BmpDraw::Terminate( int vector )
{
	dir = vector;
	switch (vector) {
		case 0:
			BMPOut(0x00); break;
		case 1:
			BMPOut(0x34); break;
		case 3:
			BMPOut(0x14); break;
		case 4:
			BMPOut(0x10); break;
		case 5:
			BMPOut(0x04); break;
		case 7:
			BMPOut(0x24); break;
	}
	Feed(vector);
}

void
BmpDraw::LTerminate( int vector )  //size 15 terminate
{
	dir = vector;
	switch (vector) {
		case 0:
			WP += CELLSIZEH;
			BMPOut(0x5); break;
		case 4:
			WP -= CELLSIZEH;
			BMPOut(0x15); break;
	}
	Feed(vector);
}

int (*GetState)(int) = _XGetState;

TScrobj::TScrobj(const char *name, int x, int y) : p(x,y), Bitmap(0) {
	if (name) {
		strcpy(Name,name);
		n = strlen(Name);
		for (int i=0;i<n;i++) {
			if (Name[i] == '_') Name[i] = ' ';
		}
	}
	else Name[0]= 0;
	m_bValid = FALSE;
	dragging = 0;
	OnColor = Mask = 0;
	Area.left = p.x - 22;
	Area.right = p.x + 22;
	Area.top = p.y - 4;
	Area.bottom = p.y + 11;
}



#define S_HR   GetState(v[0])
#define S_PR   GetState(v[1])
#define S_ASR  GetState(v[2])
#define S_LMPR GetState(v[3])
#define S_TPR  GetState(v[4])

#define T_TPR   GetState(v[0])
#define T_TLSR  GetState(v[1])
#define T_TRSR  GetState(v[2])
#define T_NKR   GetState(v[3])
#define T_RKR   GetState(v[4])
#define T_WR_N  GetState(v[5])
#define T_WR_R  GetState(v[6])

void TScrobj::Draw(HDC *mdc) {
//    void SetTextColor(TDC &dc,long color) {
//  TPen
  if (Mask & 1) SetTextColor(*mdc,TColor(128,128,128));
  else if (Mask & 2) SetTextColor(*mdc,TColor(0,0,0));
  else if (OnColor == 'Y' || OnColor == 'G') SetTextColor(*mdc,TColor(0,0,0));
  else if (OnColor == 'B') SetTextColor(*mdc,TColor(255,255,0));
  else if (OnColor == 'T') SetTextColor(*mdc,TColor(0,0,0));
  else SetTextColor(*mdc,TColor(255,255,255));
//  SetROP2(*mdc,R2_XORPEN);
  int oldmode = SetBkMode(*mdc,TRANSPARENT);
  RECT r;
  SetRect(&r,p.x-5, p.y-5, p.x+5, p.y+5);
  if (OnColor == 'T') {
	  r.top++;
	  r.left+=2;
	  DrawText(*mdc,Name, -1, &r, DT_NOCLIP | DT_CENTER |DT_VCENTER);
	  SetTextColor(*mdc,TColor(255,255,255));
	  r.top--;
	  r.left-=2;
  }
  DrawText(*mdc,Name, -1, &r, DT_NOCLIP | DT_CENTER |DT_VCENTER);
  if (Mask & 0x80) {      // Inhibit
		int m = (Mask >> 4) & 3;
		BitBlt(*mdc,p.x-10, p.y-6, 20, 20, *LockDCmask[m], 0, 0, SRCAND);
		BitBlt(*mdc,p.x-10, p.y-6, 20, 20, *LockDC[m], 0, 0, SRCPAINT);
  }
  SetBkMode(*mdc,oldmode);
}

#ifdef    ERC
union ErcLogTime {
	 long  TIME;
	 struct time {
		 int hour : 8;
		 int min  : 8;
		 int sec  : 8;
		 int filter : 8;
	 };
};

typedef struct ErcLog {
	ErcLogTime  logtime;
	char message;
	char id0;
	char id1;
	char id2;
} ErcLog;

void GenMessage(int msgno, int id=0);
#endif // ERC
/*
void GenMessage(int msgno, int id=0) {
	if (OperateWindow)
		OperateWindow->Logging(msgno,id);
}
*/

class _USERCLASS TDirect : public TScrobj {
	 TPoint p1;
	 int x, c1, oldc1;   // x => 0:Normal, 1:Reverse, 16: >, 17: ==>

  public:
	 TDirect(const char *name) : TScrobj( name ) {}

	 void Init(int x1, int y1, int x2, int y2, int ia) {
		 x = ia;
		 p1 = TPoint(x1,y1);
		 p = p1 + TPoint(x2,y2);
	 }
	 void Draw(HDC*mdc);
	 void Update() {
#ifdef    XBUG
//		 return;
#endif // XBUG
		 c1 = MY_BLUE;
#ifdef    ERC
		 if ((v[0]!=-1 && GetState(v[0]) && GetState(v[1]))
				 && ((v[2]==-1) || (v[2]!=-1 && GetState(v[2])))) c1 = MY_GREEN;
		 if (v[3]!=-1 && GetState(v[3])
			  && ((v[4]==-1) || (v[4]!=-1 && GetState(v[4])))) c1 = MY_RED;
#else  // ERC
		 if ( GetState(v[0])) c1 = MY_YELLOW;
		 if ( GetState(v[1])) c1 = MY_RED;
#endif // ERC
		 if (oldc1 != c1) InvalObj();
		 oldc1=c1;
	 }
};

void
TDirect::Draw(HDC*mdc) {
  if (m_bValid) return;
  int oldmode = SetBkMode(*mdc,OPAQUE);
  BmpDraw tbmp(mdc);
  tbmp.SetBase(c1);
  tbmp.SetWP(p1);
  if (x>=16) {
	HPEN hpen, hpenOld;
	int d = (x & 1) ? 1 : -1;
	hpen = CreatePen(PS_SOLID, 1, RGB(128, 128, 128));
	hpenOld = (HPEN)SelectObject(*mdc, hpen);
	MoveToEx(*mdc, p1.x - d * 15, p1.y,0);
//		MoveTo(*mdc, p->x, p->y);
	LineTo(*mdc, p1.x + d * 15, p1.y);
	LineTo(*mdc, p1.x - d, p1.y - 8);

	SelectObject(*mdc, hpenOld);
	DeleteObject(hpen);
 }
 else {
  switch (x) {
	 case 0 : tbmp.BMPOut(0x0c); break;
	 case 1 : tbmp.BMPOut(0x2c); break;
//	 case 16 :  c1 = MY_YELLOW;
//				  tbmp.SetBase(c1);
//				  tbmp.BMPOut(0x0c);
//				  tbmp.BMPOut(0x0d); break;
//	 case 17 : c1 = MY_YELLOW;
//				  tbmp.SetBase(c1);
//				  tbmp.BMPOut(0x2c);
//				  tbmp.BMPOut(0x2d); break;
  }
 }
  TScrobj::Draw(mdc);
  m_bValid = TRUE;
  SetBkMode(*mdc,oldmode);
}

/*
class _OWLCLASS TButton : public TControl {
  public:
	 bool  IsDefPB;

	 TButton(TWindow*        parent,
				int             id,
				const char far* text,
				int X, int Y, int W, int H,
				bool            isDefault = false,
				TModule*        module = 0);

	 TButton(TWindow *parent, int resourceId, TModule* module = 0);

  protected:
	 bool  IsCurrentDefPB;

	 //
	 // message response functions
	 //
	 uint      EvGetDlgCode(MSG far*);
	 LRESULT   BMSetStyle(WPARAM, LPARAM);

	 //
	 // Override TWindow member functions
	 //
	 char far* GetClassName();
	 void      SetupWindow();

  private:
	 //
	 // hidden to prevent accidental copying or assignment
	 //
	 TButton(const TButton&);
	 TButton& operator=(const TButton&);

  DECLARE_RESPONSE_TABLE(TButton);
  DECLARE_STREAMABLE(_OWLCLASS, TButton, 1);
};

//
// button notification response table entry macros, methods are: void method()
//
// EV_BN_CLICKED(id, method)
// EV_BN_DISABLE(id, method)
// EV_BN_DOUBLECLICKED(id, method)
// EV_BN_HILITE(id, method)
// EV_BN_PAINT(id, method)
// EV_BN_UNHILITE(id, method)

TControl(TWindow* parent, int id, const char far* title, int x, int y, int w, int h, TModule* module = 0);
TControl(TWindow* parent, int resourceId, TModule* module = 0);

Protected Member Functions

virtual int CompareItem(COMPAREITEMSTRUCT far& compareInfo);
virtual void DeleteItem(DELETEITEMSTRUCT far& deleteInfo);
virtual void DrawItem(DRAWITEMSTRUCT far& drawInfo);
LRESULT EvCompareItem(uint ctrlId, COMPAREITEMSTRUCT far& comp);
virtual void EvDeleteItem(uint ctrlId, DELETEITEMSTRUCT far& deleteInfo);
void EvDrawItem(uint ctrlId, DRAWITEMSTRUCT far& draw);
void EvMeasureItem(uint ctrlId, MEASUREITEMSTRUCT far& meas);
void EvPaint();
virtual void MeasureItem(MEASUREITEMSTRUCT far& measureInfo);

virtual void ODADrawEntire(DRAWITEMSTRUCT far& drawInfo);
virtual void ODAFocus(DRAWITEMSTRUCT far& drawInfo);
virtual void ODASelect(DRAWITEMSTRUCT far& drawInfo);

typedef struct tagDRAWITEMSTRUCT {  ditm
	 UINT  CtlType;
	 UINT  CtlID;
	 UINT  itemID;
	 UINT  itemAction;
	 UINT  itemState;
	 HWND  hwndItem;
	 HDC   hDC;
	 RECT  rcItem;
	 DWORD itemData;
} DRAWITEMSTRUCT;

DEFINE_RESPONSE_TABLE1(TMyFrame, TFrameWindow)
  EV_WM_LBUTTONDOWN,
  EV_WM_LBUTTONUP,
  EV_WM_MOUSEMOVE,
  EV_WM_RBUTTONDOWN,
END_RESPONSE_TABLE;

EV_WM_MOUSEMOVE	void EvMouseMove(uint modKeys, TPoint& point)
EV_WM_KILLFOCUS	void EvKillFocus(HWND hWndGetFocus)


*/
extern TPoint *PreRoute;
TPoint *GetRoute(char *cmd);
void MakeRoute(char *,char *);
void DrawPreRoute(HDC *mdc);

ERCButton *ComFailButton =0;

#ifdef    EIP2
ERCButton *LastButton = 0;
void LastButtonDepressed() {
	if (LastButton) LastButton->Depressed();
}
#else  // EIP2
void LastButtonDepressed() {
}
#endif // EIP2

#ifdef    EIP2
DEFINE_RESPONSE_TABLE1(ERCButton,TControl)
  EV_WM_LBUTTONDOWN,
  EV_WM_LBUTTONUP,
  EV_WM_MOUSEMOVE,
  EV_WM_PAINT,
END_RESPONSE_TABLE;
#endif // EIP2
ERCButton::ERCButton(TWindow *parent, const char *name, int x, int y, int ia, int ib, char *tm) :
	 TScrobj( name, x, y )
#ifdef    EIP2
	 , TControl(parent,0,name,x-ia*CELLSIZE/2, y-5, ia*10, 18)
#endif // EIP2
{
		oldc1 = 0;
		a=ia;
		b=ib;
		p1 = TPoint(x-(a-1)*CELLSIZE/2 , y);
		OnColor = 'Y';
		ispressed = 0;
#ifdef    EIP2
		SetCursor(GetModule(), IDC_HANDS);
		Disable = 1;
		Type = *tm;
		if (Type == BT_LAMP) Disable = 0;
#endif // EIP2
}

#ifdef    EIP2
void
ERCButton::Pressed() {
	ispressed = 1;
	HDC hdc = GetDC(*Parent);
	Draw(&hdc);
	ReleaseDC(*Parent,hdc);
	LastButton = this;
	SetCursor(GetModule(), IDC_DOWNHAND);
	InvalObj();
}
void
ERCButton::EvLButtonUp(uint /*modKeys*/, TPoint& mp /*point*/) {
	EvButtonClicked(mp);
	Depressed();
}
void
ERCButton::EvLButtonDown(uint /*modKeys*/, TPoint& /*point*/) {
	Pressed();
}
void
ERCButton::Depressed() {
	ispressed = 0;
	HDC hdc = GetDC(*Parent);
	Draw(&hdc);
	ReleaseDC(*Parent,hdc);
	LastButton = 0;
	SetCursor(GetModule(), IDC_HANDS);
	InvalObj();
}

void
ERCButton::EvMouseMove(uint modKeys, TPoint& /*point*/) {
	if (LastButton && LastButton != this) LastButton->Depressed();
	if (modKeys & 1) {       // pressed
		if (!ispressed) Pressed();
	}
	else {
		if (ispressed) Depressed();
	}
}

void
ERCButton::EvPaint() {
	HDC hdc = GetDC(*Parent);
	Draw(&hdc);
	ReleaseDC(*Parent,hdc);
	Validate();
}

void Operate(char*, int=1);
void CheckOperate(char*, int=1);
void InhibitMsg();

void
ERCButton::EvButtonClicked(TPoint&) {
	if (!Disable && GroupCmdPtr[0]) {
		char cmdbf[20];
		strcpy(cmdbf,GroupCmdPtr[0]);
/*
		if (Type == BT_ROUTEDEST) {
			PreRoute = GetRoute(cmdbf);
			HDC hdc = GetDC(*Parent);
			DrawPreRoute(&hdc);
			char bf[20];
			wsprintf(bf,"  %s 진로",cmdbf);
			if (ConfirmWindow(this,"  신호 제어", bf,"  현시취급").Execute() == IDOK)
				Operate(cmdbf);
			DrawPreRoute(&hdc);
			ReleaseDC(*Parent,hdc);
			PreRoute = 0;
		}
		else CheckOperate(cmdbf);
*/
		CheckOperate(cmdbf,-1);
		if (Type == BT_ROUTEDEST && ButtonTimer && Group[0]) Group[0]->EnableOff();
	}
	InvalObj();
}

#endif // EIP2

void
ERCButton::Update(){
#ifdef    XBUG
//		 return;
#endif // XBUG
	c1 = MY_BLUE;
	if (Type == BT_LAMP) {
		if (v[0]!=-1 && GetState(v[1])) {
			if (OnColor == 'Y') c1 = MY_YELLOW;
			else if (OnColor == 'R') c1 = MY_RED;
			else if (OnColor == 'G') c1 = MY_GREEN;
		}
	}
	if (oldc1 != c1) InvalObj();
	oldc1 = c1;
}

void
ERCButton::Draw(HDC*mdc) {
  if (m_bValid) return;
  int oldmode = SetBkMode(*mdc,OPAQUE);

  TPoint WP = p1;
  BmpDraw tbmp(mdc);

  if (Type == BT_ROUTEDEST && !Disable) tbmp.SetBase(MY_GREEN);
  else tbmp.SetBase(c1);
  tbmp.SetWP(p1);

  DrawMode = SRCCOPY;
  tbmp.BMPOut(0x16+ispressed*3);
  WP.x += CELLSIZE;
  for (int i=1; i<(a-1);i++) {
	  tbmp.SetWP(WP);
	  tbmp.BMPOut(0x17+ispressed*3);
	  WP.x += CELLSIZE;
  }
  tbmp.SetWP(WP);
  tbmp.BMPOut(0x18+ispressed*3);
  DrawMode = SRCPAINT;

  if (ispressed) {
	  p.x++; p.y++;
  }
  Mask = Disable | 2;
  TScrobj::Draw(mdc);
  if (ispressed) {
	  p.x--; p.y--;
  }
  m_bValid = TRUE;
  SetBkMode(*mdc,oldmode);
}
// CLASS ERCBUTTON END

// CLASS SIGNAL
#ifdef    EIPMAN
class _USERCLASS TSignal : public TScrobj {
#else  // EIPMAN
class _USERCLASS TSignal : public ERCButton {
#endif // EIPMAN
	 TPoint s_p1 ;//,pp;
	 int a, b;
	 int s_c1,s_c2,s_oldc1,s_oldc2;   // v1 => 0:Normal(RED), 1:Reverse(GREEN)

  public:
	 TSignal(TWindow *parent,const char *name, int x1, int y1, int x2, int y2, int ia, int ib);
#ifndef    EIPMAN
	 void EvButtonClicked(TPoint&mp);
	 void EnableOff() {
		 int i=1;
		 while (GroupCmdPtr[i] && Group[i] && i<20) {
			 Group[i]->Enable(0,0);
			 i++;
		 }
		 Timer = 0;
		 ButtonTimer = 0;
//		 Display = 1;
	 }
	 char *GetName() { return Name; }
#endif // EIPMAN
	 void Draw(HDC*mdc);
//	 void Init(int x1, int y1, int x2, int y2, int ia, int ib);
	 void Update();
};


TSignal::TSignal(TWindow *parent,const char *name,int x1, int y1, int x2, int y2, int ia, int ib)
#ifdef    EIPMAN
 : TScrobj(name,x1+x2,y1+y2) {
#else  // EIPMAN
 : ERCButton(parent,name,x1+x2,y1+y2,3,2,"SIGNAL" ),
//	TControl(parent,0,name,x1-5, y1-4, 10, 10)
	TControl(parent,0,name,
				(ia & 1 && ib & 1) ? x1-35 : x1-15,
				y1-9,
				(ib & 1) ? 50 : 30,
				20) {

	Type = BT_ROUTEFROM;
#endif // EIPMAN
	s_oldc1 = s_oldc2 = 0;
	s_p1 = TPoint(x1, y1);
	a = ia;             // a=vecter
	b = ib;             //b=strlen
	OnColor = 'R';
//	OnColor = 'Y';      // Forced text colot = 'BLACK'
}

#ifndef   EIPMAN
void TSignal::EvButtonClicked(TPoint&mp) {
	if ((SystemState & SYS_CTC) || (SystemState & SYS_KEYLOCK)
										 #ifdef    EIPLDP
										 || (SystemState & SYS_LDM)
										 #endif // EIPLDP
		) return;

	int ttb = 0;
	if (v[3]) {
		if (a & 1) {
			if (mp.x <= 10) ttb = 1;
		}
		else {
			if (mp.x >= 35) ttb = 1;
		}
	}
	if (ttb) {
		char bf[10];
		strcpy(bf,Name);
		strcat(bf,"T");
		if (GetState(v[3])) {
			strcat(bf,"-");
			CheckOperate(bf,-1);
		}
		else if (GetState(v[4])) {
			CheckOperate(bf,-1);
		}
		else ttb = 0;
	}
	if (!ttb) {
		if (GetState(v[0])||GetState(v[2])) {        // if GKE
//			 char bf[20];
//			 mysprintf(bf,"  %s 신호기",Name);
//			 if (GroupCmdPtr[0] && ConfirmWindow(this,"  신호 정지 제어", bf,"  정지취급").Execute() == IDOK) {
//				 Operate(GroupCmdPtr[0]);          // 정지
//			 }
//			 Timer = 0;
			 if (GroupCmdPtr[0]) CheckOperate(GroupCmdPtr[0],-1);
		}
		else {
		 if ( ButtonTimer && ButtonTimer != this ) ButtonTimer->EnableOff();
		 int i=1;
		 while (GroupCmdPtr[i] && Group[i] && i<20) {
			 if (!Control->MakeCCMMsg(GroupCmdPtr[i],0))
				 Group[i]->Enable(this,GroupCmdPtr[i]);
			 i++;
		 }
		 Timer = OPDELAY;
		 ButtonTimer = this;
		}
	}
//		 Display = 1;
}
#endif // EIPMAN

void TSignal::Update() {
#ifdef    XBUG
//		 return;
#endif // XBUG

#ifdef    ERC
	 if (a==0 || a==1) {
		if (S_HR) {         // MAIN SIGNAL
			if (S_LMPR) c1 = MY_GREEN;
			else c1 = MY_FGREEN;
		}
		else {
			if (S_LMPR) c1 = MY_RED;
			else c1 = MY_FRED;
		}
	 }
	 else {            //SHURTING SIGNAL
		if (S_HR) c1 = MY_YELLOW;
		else c1 = MY_RED;
	 }

	c2 = MY_BLUE;
	if (GetState(v[4])) {  // SIGNAL SMALL LAMP  //GetStatev[4]==TPR
		if (S_PR) c2 = MY_YELLOW;
		else if (!S_ASR && GetState(v[5])) c2 = MY_FYELLOW;  //GetStatev[5]==URPR
	}
	if (oldc1 != c1) {
		if ((c1 & 0xff) != (oldc1 & 0xff)) {
			if ((c1 & 0xff) == MY_GREEN) {
				 GenMessage(501,v[0]);       //신호현시
			}
			else if ((c1 & 0xff) == MY_RED){
				 GenMessage(502,v[0]);      //신호정지
			}
		}
		else {
			if (c1 & 0x100) GenMessage(213,v[3]);   //단심
			else GenMessage(214,v[3]);      //단심복구
		}
	}

	if (oldc1 != c1 || oldc2 != c2) InvalObj();
#else  // ERC
#ifndef   EIPMAN
	ERCButton::Update();
#endif // EIPMAN
	s_c1 =MY_BLUE;
	if ( GetState(v[0]) ) s_c1 = MY_GREEN;
	if ( GetState(v[1]) ) s_c1 = MY_RED;
	if ( GetState(v[2]) ) s_c2 = MY_YELLOW;
	else s_c2 = MY_BLUE;
#endif // EIPMAN
	s_oldc1 = s_c1;
	s_oldc2 = s_c2;
}

void TSignal::Draw(HDC*mdc) {
  if (m_bValid) return;
  int oldmode = SetBkMode(*mdc,OPAQUE);

//  if (!ForceDraw && isvalid) return;
  BmpDraw tbmp(mdc);
  tbmp.SetBase(s_c1);
  tbmp.SetWP(s_p1);
  switch (a) {
	 case 0 : tbmp.BMPOut(0x7); break;
	 case 1 : tbmp.BMPOut(0x27); break;
	 case 16 :	// tbmp.BMPOut(0x6); break;
	 case 17 : 	// tbmp.BMPOut(0x26); break;
	 case 18 :
	 case 19 : tbmp.BMPOutSh(a); break;
  }
  tbmp.SetBase(s_c2);
#ifdef    EIPMAN
  if (s_c2 != MY_BLUE) {
	  tbmp.BMPOut(0x8);
  }
  else {
	  if (GetState(v[3])) tbmp.BMPOut(28);
	  else tbmp.BMPOut(0x8);
  }
#else  // EIPMAN
  if (Timer>0) tbmp.BMPOut(29);
  else if (s_c2 != MY_BLUE) {
	  tbmp.BMPOut(0x8);
  }
  else {
	  if (ispressed) tbmp.BMPOut(29);
	  else if (GetState(v[3])) tbmp.BMPOut(28);
	  else tbmp.BMPOut(0x8);
  }
#endif // EIPMAN
  if (v[3] != -1) {
	  if (a & 1) {
		  tbmp.SetWP(s_p1+TPoint(-30,0));
	  }
	  else {
		  tbmp.SetWP(s_p1+TPoint(30,0));
	  }
	  tbmp.SetBase(0);
	  if (GetState(v[3])) tbmp.BMPOut(31);
	  else tbmp.BMPOut(30);
  }
  TScrobj::Draw(mdc);
  m_bValid = TRUE;
}
// CLASS SIGNAL END

// POINT CLASS
#ifdef    EIPMAN
class _USERCLASS TTpoint : public TScrobj {
#else  // EIPMAN
class _USERCLASS TTpoint : public ERCButton {
#endif // EIPMAN
	 TPoint cir_p;
	 int cir_c1, cir_oldc1;

  public:
	 TTpoint(TWindow *parent,const char *name, int x1, int y1, int x2, int y2);
#ifndef   EIPMAN
	 void EvButtonClicked(TPoint&) {
		 if ((SystemState & SYS_CTC) || (SystemState & SYS_KEYLOCK)
			  #ifdef     EIPLDP
			  || (SystemState & SYS_LDM)
			  #endif // EIPLDP
			 ) return;
		 if (!GetState(v[0])) {
			 int dir = -1, m1,m2;     // Normal
			 m1 = Control->MakeCCMMsg(GroupCmdPtr[0],0);
			 m2 = Control->MakeCCMMsg(GroupCmdPtr[1],0);
			 if (m1==0) dir = 0;
			 else if (m2==0) dir = 1;
			 char bf1[20], bf2[20];
			 mysprintf(bf1,"  %s 전철기",Name);
			 mysprintf(bf2,"  %s위 전환", dir ? "반":"정");
			 if ( dir >= 0 && ConfirmWindow(this,"전철기 단독 제어",bf1,bf2).Execute() == IDOK) {
				 Operate(GroupCmdPtr[dir],-1);
			 }
		 }
	 }
	 char *GetName() { return Name; }
#endif // EIPMAN
	 void Draw(HDC*mdc);
	 void Update() {
#ifdef    XBUG
//		 return;
#endif // XBUG
		cir_oldc1 = cir_c1 = MY_BLUE;
#ifdef    ERC
		if (!GetState(v[0])) {
		  int i=1;
		  while ( v[i]!=-1 && i<10 ) {
			  if (!GetState(v[i++])) {
				 c1 = MY_YELLOW;
				 return;
			  }
		  }
		}
#else  // ERC
#ifndef   EIPMAN
		ERCButton::Update();
#endif // EIPMAN
		if ( GetState(v[0]) ) cir_c1 = MY_YELLOW;
#endif // ERC
		if (cir_oldc1 != cir_c1) {
		  InvalObj();
		  cir_oldc1 = cir_c1;
		}
	 }
};

TTpoint::TTpoint(TWindow *parent,const char *name, int x1, int y1, int x2, int y2)
#ifdef    EIPMAN
 : TScrobj(name,x1+x2,y1+y2) {
#else  // EIPMAN
 : ERCButton(parent,name,x1+x2,y1+y2,3,2,"POINT" ),
//	TControl(parent,0,name,x1-5, y1-4, 10, 10)
	TControl(parent,0,name,x1-15, y1-9, 30, 20) {

  Type = BT_POINT;
#endif // EIPMAN
//	 }
//	 void Init(int x1, int y1, int x2, int y2) {
  cir_p = TPoint(x1,y1);
  OnColor = 'R';
}

void
TTpoint::Draw(HDC*mdc) {
  BmpDraw tbmp(mdc);
  tbmp.SetBase(cir_c1);
  tbmp.SetWP(cir_p);
  if (cir_c1 != MY_BLUE) {
	  tbmp.BMPOut(0x8);
  }
  else {
#ifndef   EIPMAN
	  if (ispressed) tbmp.BMPOut(29);
	  else
#endif // EIPMAN
//	  if (GetState(v[3])) tbmp.BMPOut(28);
//	  else
		  tbmp.BMPOut(0x8);
  }

  TScrobj::Draw(mdc);
  m_bValid = TRUE;
}
// POINT CLASS END


class _USERCLASS TBoxView: public TScrobj {
	 TPoint p1;
	 int a, b, c1, oldc1;     //a=strlen , b=error message
  public:
	 TBoxView(const char *name, int x, int y, int ia, int ib, char *tm, char *onc) : TScrobj( name,x,y ) {
			oldc1 = 0;
			a=ia;
			b=ib;
			p1 = TPoint(x-(a-1)*CELLSIZE/2 , y);
			OnColor = *onc;
	 }

	 void Draw(HDC*mdc);
	 void Update();
};

void
TBoxView::Update(){
#ifdef    XBUG
//		 return;
#endif // XBUG
	c1 = MY_BLUE;
	if (v[0]!=-1 && GetState(v[0])) {
		if (OnColor == 'Y') c1 = MY_YELLOW;
		else if (OnColor == 'R') c1 = MY_RED;
		else if (OnColor == 'G') c1 = MY_GREEN;
	}
#ifdef    ERC
	if(b!=-1 && oldc1!=c1) {
		if(v[0]!=-1) {
			if (GetState(v[0])==1) {
				GenMessage(b,v[0]);
			}
		}
	}
	if (oldc1 != c1) InvalObj();
#endif // ERC
	oldc1 = c1;
}

void
TBoxView::Draw(HDC*mdc) {
//  Invalidate(true);
//  if (!ForceDraw && isvalid) return;
  TPoint WP = p1;
  BmpDraw tbmp(mdc);
  tbmp.SetBase(c1);
  tbmp.SetWP(p1);

  tbmp.BMPOut(0x16);
  WP.x += CELLSIZE;
  for (int i=1; i<(a-1);i++) {
	  tbmp.SetWP(WP);
	  tbmp.BMPOut(0x17);
	  WP.x += CELLSIZE;
  }
  tbmp.SetWP(WP);
  tbmp.BMPOut(0x18);

  TScrobj::Draw(mdc);

  m_bValid = TRUE;
}

class _USERCLASS TStation : public TScrobj {
	 TPoint p;
	 int a;
  public:
	 TStation(const char *name, int x, int y, int ia) : TScrobj( name, x, y ) {
			a = ia;
			p = TPoint(x, y);
		  if (HStationName[0]) AbnormalExit("역명이 두개 이상입니다","SFM File error");
		  strcpy(HStationName,name);
	 }
	 void Draw(HDC*mdc);
};

void
TStation::Draw(HDC*mdc) {
  TDC dc(*mdc);
  dc.SetTextColor(TColor(128,255,128));

  int oldmode = dc.SetBkMode(TRANSPARENT);
  TFont *font = new TFont(_T("HY견고딕"),25,30);
  dc.SelectObject(*font);
  dc.DrawText(Name, -1, TRect(p.x-5, p.y-5, p.x+5, p.y+5), DT_NOCLIP | DT_CENTER |DT_VCENTER);
  dc.RestoreFont();
  delete font;
  dc.SetBkMode(oldmode);
}

// Alarm function






extern int MCISound;

class _USERCLASS TMessage : public TScrobj {
	 int Type, Arg1;
	 int State;
  public:
	 TMessage(const char *name, int id, int type, int arg1) : TScrobj( name ) {
		 v[0] = id;
		 Type = 1 << type;
		 Arg1 = arg1;
		 State = 0;
	 }
	 void Draw(HDC*mdc) {}
	 void Update() {
#ifdef    XBUG
//		 return;
#endif // XBUG
		 MCISound = MCISound & (~Type);
		 if (GetState(v[0])) MCISound |= Type;
	 }
};

#define MU 5

class _USERCLASS TTrack : public TScrobj {
	TPoint p1,p2,p3,p4,p5;
	int a,b,c,d,e;
	int syx;     // 4 2 1
	int isXtype;
	int isYtype;
	int c1,c2,type,type1,oldc1,oldc2,oldtype,oldtype1, req;
	TTrack *RelationT;
	TTrack *PreT;
	char Connect;

	friend class TLineStatus;
	friend class XTrackRelate;

public:
	TTrack(const char *name, int x1, int y1, int x2, int y2)
	: TScrobj( name, x1+x2, y1+y2 ) {
		 oldc1 = oldc2 = oldtype = oldtype1 = 0;
		 RelationT = 0;
		 isXtype =isYtype = 0;
		 p1 = TPoint(x1, y1);   // Shape start pos.
		 OnColor = 'T';
		 type = 0;
		 Mask = 0;
		 c1 = MY_BLUE;
		 PreT = NULL;
		 req = 0;
	}

	BOOL NextRouted() {
		return (Connect == 'C' ||
				(type == 0 && Connect == 'N') ||
				(type == 0 && Connect == 'n') ||
				(type == 1 && Connect == 'R')
			   );
	}

	void Draw(HDC*mdc);

	// 97.3.13 jin
	void SetInbitOn(int on);
	void SetInbitOff();
	//
	bool SetInhibit(TPoint& point, int on);
	/*
	{
		 if (Area.Contains(point)) {
// IM.96.12.30
			 if (v[0] == -1) v[0] = v[1];
//
			 char bf[20];
			 mysprintf(bf,"  %s 궤도",Name);
			 if (KeyLock) InhibitMsg();
			 else {
				 if (on) {
					 if (!(IOExtBuffer[v[0]] & 0x80) && ConfirmWindow(StationLS->Parent,"  사용 금지 제어", bf,"  사용 금지").Execute() == IDOK) {
						 Mask = (Mask & 0x0f) | on;
						 c1 = c2 = MY_GRAY;
						 IOExtBuffer[v[0]] |= 0x80;
						 strcpy(bf,"MSG");
						 strcat(bf,Name);
						 Operate(bf,900+((on >> 4)&3));
					 }
				 }
				 else {
					 if ((IOExtBuffer[v[0]] & 0x80) &&  ConfirmWindow(StationLS->Parent,"  사용 금지 제어", bf,"  사용 개시").Execute() == IDOK) {
						 Mask &= 0x0f;
						 c1 = c2 = MY_GRAY;
						 IOExtBuffer[v[0]] &= 0x7f;
						 strcpy(bf,"MSG");
						 strcat(bf,Name);
						 Operate(bf,903);
					 }
				 }
			 }
			 return true;
		 }
		 return false;
	}
	*/
	void Convert( char * );      // converter
	void SetXTrack(TTrack *xtrack) { RelationT =xtrack; }

	void ValExpand(int &a, int b) {
		a=(b&0x0ff)*MU+((b&0x100)<<6);
	}
	int  isXTrack() { return isXtype; }
	void SetXTState(int xtstate) { type1 =xtstate; }

	void Init(int ia, int ib, int ic, int id, int ie, char *s) {
		ValExpand(a,ia);
		ValExpand(b,ib);
		ValExpand(c,ic);
		ValExpand(d,id);
		ValExpand(e,ie);

		Convert( s );
	}

	void Update();
};

// 97.3.13 jin
void TTrack::SetInbitOn(int on) {
	if ( !isXtype && RelationT ) {
		RelationT->SetInbitOn(on);
	}
	c1 = c2 = MY_GRAY;
	Mask = (Mask & 0x0f) | on;
	IOExtBuffer[v[0]] |= 0x80;	// 97.3.13
}

void TTrack::SetInbitOff() {
	if ( !isXtype && RelationT ) {
		RelationT->SetInbitOff();
	}
	c1 = c2 = MY_GRAY;
	Mask &= 0x0f;
	IOExtBuffer[v[0]] &= 0x7f;	// 97.3.13
}
//

bool TTrack::SetInhibit(TPoint& point, int on) {
	if (Area.Contains(point)) {
// IM.96.12.30
		if (v[0] == -1) v[0] = v[1];
//
		char bf[20];
		mysprintf(bf,"  %s 궤도",Name);
		if (KeyLock) InhibitMsg();
		else {
			if (on) {
				if (!(IOExtBuffer[v[0]] & 0x80) && ConfirmWindow(StationLS->Parent,"  사용 금지 제어", bf,"  사용 금지").Execute() == IDOK) {
					// 97.3.13
					//c1 = c2 = MY_GRAY;
					//Mask = (Mask & 0x0f) | on;
					//IOExtBuffer[v[0]] |= 0x80;
					SetInbitOn(on);
					strcpy(bf,"MSG");
					strcat(bf,Name);
					Operate(bf,900+((on >> 4)&3));
				}
			}
			else {
				if ((IOExtBuffer[v[0]] & 0x80) &&  ConfirmWindow(StationLS->Parent,"  사용 금지 제어", bf,"  사용 개시").Execute() == IDOK) {
					// 97.3.13
					//c1 = c2 = MY_GRAY;
					//Mask &= 0x0f;
					//IOExtBuffer[v[0]] &= 0x7f;
					SetInbitOff();
					strcpy(bf,"MSG");
					strcat(bf,Name);
					Operate(bf,903);
				}
			}
		}
		return true;
	}
	return false;
}

void TTrack::Update() {
#ifdef    XBUG
//		 return;
#endif // XBUG
//	type = 0;
//	type1 =0;

/*	// test code
	if ( isYtype || isXtype ) {
		IOExtBuffer[v[0]] |=1;
		IOExtBuffer[v[1]] |=1;
		IOExtBuffer[v[2]] |=0;
	}
	if ( !strncmp(Name,"51BT",4) ) {
		IOExtBuffer[v[0]] |=1;
		IOExtBuffer[v[1]] |=0;
		IOExtBuffer[v[2]] |=1;
	}
*/		//

#ifdef    ERC
	if (v[3] != -1) {
		if (!T_NKR || !T_RKR) {
			if (oldtype) type = !T_NKR;
			else type = T_RKR;
		}
	}
	c1 = c2 = MY_BLUE;
	if (v[1] == -1) v[1] = -2;
	if (v[2] == -1) v[2] = -2;
	//		if (!RelationT) {
		if (v[0] != -1) {
			if (T_TPR) {
				if (!T_TRSR || !T_TLSR) c1 = MY_GREEN; //점유
			}
			else {
				if (v[7] == -1) {
					c1 = MY_RED;                        //복구
				}
				else if (GetState(v[7])) c1 = MY_RED;   //양끝 트렉 점유
				else c1 = MY_FRED;
			}
		}
		else {
			c1 = c2 = MY_GRAY;
		}
//	}
//	else {
//	}

	int x = ((T_WR_N && !T_WR_R && !T_RKR && !T_NKR) ||
				(!T_WR_N && T_WR_R && !T_NKR && !T_RKR));
	if (x) c1 |= 0x2000;
	if ((c1 & 0xfff) != (oldc1 & 0xfff)) {
		if ((c1 & 0xfff) == MY_RED) GenMessage(504 ,v[0]); // 점유
		else if ((oldc1 & 0xfff) == MY_RED) GenMessage(503 ,v[0]); //복구
	}
	if (type != oldtype) {
		if (!type) GenMessage(505,v[5]);  //정위전환
		else GenMessage(506,v[6]);       //반위전환
	}

	if (oldc1 != c1 || oldc2 != c2) InvalObj();
#else  // ERC
	c1 = c2 = MY_BLUE;
	int cke = GetState(v[0]);

	int v1 = GetState(v[1]);
	int v4 = GetState(v[4]);
	int v2 = GetState(v[2]);
//	v2 = 1;
	if (b) {                      // if branch
	   req = cke & 2;
	   TTrack *preT = PreT;
	   while (preT) {
		  if (PreT->NextRouted()) {
			  req = GetState(preT->v[0]) & 2;
		  }
		  else {
			  req = 0;
			  break;
		  }
		  preT = preT->PreT;
	   }
	   if (req) {
		   v[9]++;
		   if (v[9]>100) v[9]=100;
	   }
	   else v[9] = 0;

		int isfl = 0;
		if ( cke & 1 ) c1 = MY_GREEN;
		//else if ( cke & 2 ) c1 = MY_YELLOW;
		if ( GetState(v[3]) ) c1 = MY_RED;

		if ( v1 || v4 ) {        // N
			type = 0;
// 97.4.2.IM {
			if ( c1 == MY_BLUE && ( req && v[9]>4 || oldtype & 0x10 ) ) {
				c1 = MY_YELLOW;
				oldtype |= 0x20;
			}
			else oldtype = 0;
// } 97.4.2.IM
		}
		else if ( v2 || GetState(v[5])) {   // R
			type = 1;
// 97.4.2.IM {
			if ( c1 == MY_BLUE && ( req && v[9]>4 || oldtype & 0x10 ) ) {
				c1 = MY_YELLOW;
				oldtype |= 0x20;
				if (req && PreT && PreT->Connect == 'n' && PreT->c1 == MY_YELLOW) {
					PreT->c1 = MY_BLUE;
				}
			}
			else oldtype = 1;
// } 97.4.2.IM
		}
		else {
			oldtype = 0x10;
			//type = v[9] ? 1 : 0;
		}
//		if (c1 == MY_YELLOW) {
//			   if (!req && (GetState(v[1]) || GetState(v[2]) || GetState(v[4]) || GetState(v[5])) && oldtype > 3) oldtype = 0;
//		}
		//v[9] = type;
	}
	else {
		if (Name[0]==' ') c1 = MY_GRAY;
		// IM.97.1.9			else if ( GetState(v[1]) ) c1 = MY_RED;
		else if ( GetState(v[1]) & 1 ) c1 = MY_RED;
		else if ( cke & 1 ) c1 = MY_GREEN;
		else if ( cke & 2 ) {
			c1 = MY_YELLOW;
			oldtype = 0;
		}
	}
#endif // ERC
// IM.96.1.9
//	if (Mask & 0x80) {
//		c1 = c2 = MY_GRAY;
//	}
	if (isXtype && RelationT)
		RelationT->SetXTState(type);
	else type1 =0;
#ifdef    ERC
	oldc1 = c1;
	oldtype = type;
	oldtype1 = type1;
#endif // ERC

	req |= (cke & 1);	// for YELLOW off, if CKE then YELLOW off
}

/*
			  a
  (p1)==========(p2)
			a
  (p1)=======
				\\ c                                        e
				 \\                                      ======(p2)
				  (p2)                                b//
			 a         b                       a     //
  (p1)=======(p3)========(p2)        (p1)=======(p3)
				  \\                                  \\
				 c \\  d                             c \\
					  ====(p4)                            =====(p4)

		 a       b        e
  (p1)===(p3)====(p5)====(p2)
			 \\     //
			c \\   // d
				(p4)

*/

void
TTrack::Convert( char *s ) {
//	int isYtype = 0;
//	int isXtype = 0;
	if (*s == 'Y') {
		s++;
		isYtype = 1;
	}
	if (*s == 'X') {
		s++;
		isXtype = 1;
	}
	int xy = 0;
	if (*s == 'S') {
	  xy = 0x40;
	}
	else {
	  if (*s == 'U') xy |= 0x20;
	  if (*(s+1) == 'L') xy |= 0x10;
	  if (*(s+2) == 'E') xy |= 0x80;
	}

 syx = xy;
 int dx,dy;
 if (xy & 0x10) dx = -1;
 else dx = 1;
 if (xy & 0x20) dy = -1;
 else dy = 1;

// TPoint p1, p2, p3, p4;
// p1 = TPoint(p);

 if (xy & 0x40) {
	 if ((c&0xfff) == 0) { //Straight
		 p2 = TPoint(p1.x+(a&0xfff)*dx, p1.y);
	 }
	 else {       //Elbow
		 p2 = TPoint(p1.x+((a&0xfff)+(c&0xfff)/2)*dx, p1.y+(c&0xfff)*dy);
	 }
 }
 else {
	 p3 = TPoint(p1.x+(a&0xfff)*dx, p1.y);
	 if (!isXtype) {
		 if (isYtype) {     //Ytype
			if ((e&0xfff)==0) {
				p2 = TPoint(p3.x+(b&0xfff)/2*dx, p3.y-(b&0xfff)*dy);
			}
			else {
				p2 = TPoint(p3.x+((b&0xfff)/2+(e&0xfff))*dx, p3.y-(b&0xfff)*dy);
			}
		 }
		 else	p2 = TPoint(p3.x+(b&0xfff)*dx, p3.y);

		 if ((d&0xfff)==0) {    //Breanch
			 p4 = TPoint(p3.x+((c&0xfff)/2)*dx, p3.y+(c&0xfff)*dy);   //B
		 }
		 else {
			 p4 = TPoint(p3.x+((c&0xfff)/2+(d&0xfff))*dx, p3.y+(c&0xfff)*dy);  //BE
		 }
	 }
	 else {    //Xtype
		p5 = TPoint(p3.x+(b&0xfff)*dx, p1.y);
		p2 = TPoint(p5.x+(e&0xfff)*dx, p1.y);
		p4 = TPoint(p3.x+((b&0xfff)/2)*dx, p3.y+(c&0xfff)*dy);
	 }

 }
	Area.left = p1.x;
	Area.right = p2.x;
	Area.top = p1.y;
	Area.bottom = p2.y;
	Area.Normalize();
	Area.Inflate(5,5);
}

int extern _TIMETICK;
void TTrack::Draw(HDC*mdc)
{
  int dx,dy, dir;
  if (syx & 0x10) dx = -5;
  else dx = 5;
  if (syx & 0x20) dy = -10;
  else dy = 10;

  BmpDraw tbmp(mdc);
  DrawMode = SRCCOPY;
// TPoint WP, delta, delta1, delta2, delta3, p3p;
  if (syx & 0x40) {   //Straight
//	 tbmp.LineDraw(p1,p2,c1,0);
	 tbmp.LineDraw(0,p1,p2,c1,0);
  }
  else {
	 if (isXtype==0 && type==0) {        // Breanch type=0 , YBreanch Normal
		if (e==0) {
			if ((a & 0x4000) && (b & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p2,c1,1);
				tbmp.Straight(dir);
			}
			else if ((a & 0x4000) && !(b & 0x4000)) {
				tbmp.LineDraw(1,p1,p2,c1,0);
			}
			else if (!(a & 0x4000) && (b & 0x4000)) {
				dir = tbmp.LineDraw(0,p1,p2,c1,1);
				tbmp.Straight(dir);
			}
			else {
				tbmp.LineDraw(0,p1,p2,c1,0);
			}

		}
		else {
			int lx1 = (abs(p1.y - p2.y)/2) * dx/5;
			TPoint p12(p3.x + lx1, p2.y);
			if ((a & 0x4000) && (e & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p12,c1,1);
				tbmp.Elbow(dir);
				tbmp.LineDraw(1,p2,p12,c1,1);
			}
			else if (!(a & 0x4000) && (e & 0x4000)) {
				dir = tbmp.LineDraw(0,p1,p12,c1,1);
				tbmp.Elbow(dir);
				tbmp.LineDraw(1,p2,p12,c1,1);
			}
			else if ((a & 0x4000) && !(e & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p12,c1,1);
				tbmp.Elbow(dir);
				tbmp.LineDraw(0,p2,p12,c1,1);
			}
			else {
				dir = tbmp.LineDraw(0,p1,p12,c1,1);
				tbmp.Elbow(dir ^ 4);
				tbmp.LineDraw(0,p2,p12,c1,1);
			}
		}
		if (d==0) {
			if (c & 0x4000) {
				dir = tbmp.LineDraw(1,p4,p3+TPoint(dx,dy),c2,0);
			}
			else {
				tbmp.LineDraw(0,p4,p3+TPoint(dx,dy),c2,0);
			}
		}
		else {
			if (d & 0x4000) {
				tbmp.LineDraw(1,p4,p3+TPoint(dx,dy),c2,0);
			}
			else {
				tbmp.LineDraw(0,p4,p3+TPoint(dx,dy),c2,0);
			}
		}
	 }
	 else if (isXtype==0 && type==1) {     // Breanch type=1, YBreanch
		int lx1 = (abs(p3.y - p4.y)/2) * dx/5;
		TPoint p34(p3.x + lx1, p4.y);
		if (p34.x != p4.x) {
			if ((a & 0x4000) && (d & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p34,c1,1);
				tbmp.Elbow(dir ^ 4);
				tbmp.LineDraw(1,p4,p34,c1,1);
			}
			else if (!(a & 0x4000) && (d & 0x4000)) {
				dir = tbmp.LineDraw(0,p1,p34,c1,1);
				tbmp.Elbow(dir ^ 4);
				tbmp.LineDraw(1,p4,p34,c1,1);
			}
			else if ((a & 0x4000) && !(d & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p34,c1,1);
				tbmp.Elbow(dir ^ 4);
				tbmp.LineDraw(0,p4,p34,c1,1);
			}
			else {
				dir = tbmp.LineDraw(0,p1,p34,c1,1);
				tbmp.Elbow(dir ^ 4);
				tbmp.LineDraw(0,p4,p34,c1,1);
							}
		}
		else {
			if ((a & 0x4000) && (c & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p4,c1,1);
				tbmp.Straight(dir);
			}
			else if (!(a & 0x4000) && (c & 0x4000)) {
				dir = tbmp.LineDraw(0,p1,p4,c1,1);
				tbmp.Straight(dir);
			}
			else if ((a & 0x4000) && !(c & 0x4000)) {
				dir = tbmp.LineDraw(1,p1,p4,c1,0);
			}
			else tbmp.LineDraw(0,p1,p4,c1,0);
		}
		if (p1.y == p2.y) {
		  dy = 0;
		  dx += dx*2;
		}
		if (e==0) {
			if (b & 0x4000) {
				dir = tbmp.LineDraw(1,p2,p3+TPoint(dx,-dy),c2,0);
			}
			else {
				dir = tbmp.LineDraw(0,p2,p3+TPoint(dx,-dy),c2,0);
			}
		}
		else {
			if (e & 0x4000) {
				dir = tbmp.LineDraw(1,p2,p3+TPoint(dx,-dy),c2,0);
			}
			else {
				dir = tbmp.LineDraw(0,p2,p3+TPoint(dx,-dy),c2,0);
			}
		}
	 }
	 else if (isXtype==1 && type==0 && type1 == 0){
		if ((a & 0x4000) && (e & 0x4000)) {
			dir = tbmp.LineDraw(1,p1,p2,c1,1);
			tbmp.Straight(dir);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else if ((a & 0x4000) && !(e & 0x4000)) {
			tbmp.LineDraw(1,p1,p2,c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else if (!(a & 0x4000) && (e & 0x4000)) {
			dir = tbmp.LineDraw(0,p1,p2,c1,1);
			tbmp.Straight(dir);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else {
			tbmp.LineDraw(0,p1,p2,c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
	 }
	 else if (isXtype==1 && type==0 && type1==1){
		if ((a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(1,p1,p5+TPoint(-dx*2,0),c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(1,p2,p4+TPoint(dx,0),c2,0);
		}
		else if (!(a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(0,p1,p5+TPoint(-dx*2,0),c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(1,p2,p4+TPoint(dx,0),c2,0);
		}
		else if ((a & 0x4000) && !(e & 0x4000)) {
			tbmp.LineDraw(1,p1,p5+TPoint(-dx*2,0),c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p2,p4+TPoint(dx,0),c2,0);
		}
		else {
			tbmp.LineDraw(0,p1,p5+TPoint(-dx*2,0),c1,0);
			tbmp.LineDraw(0,p4+TPoint(-dx,0),p3+TPoint(dx,dy),c2,0);
			tbmp.LineDraw(0,p2,p4+TPoint(dx,0),c2,0);
		}
	 }
	 else if (isXtype==1 && type==1 && type1==0) {
		if ((a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(1,p1,p4+TPoint(-dx,0),c1,0);
			dir = tbmp.LineDraw(0,p3+TPoint(dx*2,0),p2,c2,1);
			tbmp.Straight(dir);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else if ((a & 0x4000) && !(e & 0x4000)) {
			tbmp.LineDraw(1,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p2,c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else if (!(a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(0,p1,p4+TPoint(-dx,0),c1,0);
			dir = tbmp.LineDraw(0,p3+TPoint(dx*2,0),p2,c2,1);
			tbmp.Straight(dir);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
		else {
			tbmp.LineDraw(0,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p2,c2,0);
			tbmp.LineDraw(0,p4+TPoint(dx,0),p5+TPoint(-dx,dy),c2,0);
		}
	 }
	 else if (isXtype==1 && type==1 && type==1) {
		if ((a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(1,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p5+TPoint(-dx*2,0),c2,0);
			tbmp.LineDraw(1,p2,p4+TPoint(dx,0),c2,0);
		}
		else if(!(a & 0x4000) && (e & 0x4000)) {
			tbmp.LineDraw(0,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p5+TPoint(-dx*2,0),c2,0);
			tbmp.LineDraw(1,p2,p4+TPoint(dx,0),c2,0);
		}
		else if ((a & 0x4000) && !(e & 0x4000)) {
			tbmp.LineDraw(1,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p5+TPoint(-dx*2,0),c2,0);
			tbmp.LineDraw(0,p2,p4+TPoint(dx,0),c2,0);
		}
		else {
			tbmp.LineDraw(0,p1,p4+TPoint(-dx,0),c1,0);
			tbmp.LineDraw(0,p3+TPoint(dx*2,0),p5+TPoint(-dx*2,0),c2,0);
			tbmp.LineDraw(0,p2,p4+TPoint(dx,0),c2,0);
		}
	 }
  }
  DrawMode = SRCPAINT;
  TScrobj::Draw(mdc);
  m_bValid = TRUE;

	if (_TIMETICK && c1 == MY_YELLOW) {
		if (!(req & 2) && (oldtype & 0x10) && (oldtype & 0x20)) {
			oldtype += 0x100;
			if (oldtype & 0x8000 || oldtype > 0x300)	{
				oldtype = 0;
				c1 = MY_BLUE;
			}
		}
	}
}

// -------------------------------------------------------------------------- //
extern int MWINX,MWINY;
int KeyCheck();

// -------------------------------------------------------------------------- //
TLineStatus::TLineStatus(TWindow *parent, HDC *dc)
:
  TScrobjs(10,0,10)
{
  Parent = parent;
  LSmDC = dc;
  sx = LSWINX;
  sy = LSWINY;

  Scrobjs = new TScrobjs(1,0,1);
  if (LoadFileInfo()) return;
  if (KeyCheck()) return;

  SymbolInfo = new SymbolBitmapInfo[256];

  char bf[64];

  ifstream fis("SYMINFO.TXT");
  int id;
  do {
	 fis.getline(bf,500);
	 if (fis.gcount() && bf[0] && (bf[0] != ';')){
		 istream is(500,bf);
		 is >> id;
		 if (id>=0 && id<256) SymbolInfo[id].Readfile(is);
	 }
  } while (!fis.eof());
  ForceDraw = 1;
  InvalidateObjects();
}

//
TLineStatus::~TLineStatus()
{
  delete [] SymbolInfo;
  delete Scrobjs;
}

//
void TLineStatus::SetInhibit(TPoint& point, int on) {
	TScrobjsIterator i(*Scrobjs);
	while (i) {
		if ((i++)->SetInhibit(point,on)) break;
	}
}

void TLineStatus::GenMessage() {
	TScrobjsIterator i(*Scrobjs);
	while (i) {
		(i++)->Update();
	}
}

// -------------------------------------------------------------------------- //
extern int MsgOn;
void TLineStatus::Update(TDC& dc)
{
	if (LSmDC) {
#ifndef   EIPMAN
		ShiftCheck();
		ILCommand->AlarmTrack(&IOExtBuffer[0]);
#endif // EIPMAN
		TScrobjsIterator i(*Scrobjs);
		while (i) {
			(i++)->Update();
		}
		Refresh(dc,0);
		MsgOn = 1;
	}
}

void TLineStatus::InvalidateObjects()
{
	 TScrobjsIterator i(*Scrobjs);
	 while (i) {
		 (i++)->InvalObj();
	 }
}

void TLineStatus::Refresh(TDC&, int all, TRect *r)
{
  if (LSmDC) {
	 if ( ComFailButton ) {
		ComFailButton->Update();
        ComFailButton->InvalObj();
	 }
//	 if (all) {
		TDC mdc(*LSmDC);
		if (all) {
			mdc.SelectObject(TBrush(TColor(0,0,0)) );
			mdc.PatBlt(0, 0, sx, sy, PATCOPY);
		    InvalidateObjects();
		}
		TScrobjsIterator j(*Scrobjs);
		while (j) {
			(j++)->Draw(LSmDC);
		}
//	 }
	 DrawPreRoute(LSmDC);
	 HDC hdc = GetDC(*Parent);
	 if (hdc) {
		 TDC DC(hdc);
		 if (r) {
			 int w = r->right - r->left + 1, h = r->bottom - r->top + 1;
			 DC.BitBlt(r->left, r->top, w, h,*LSmDC, r->left, r->top);
		 }
		 else DC.BitBlt(0, 0,sx, sy,*LSmDC,0,0);
	 }
	 ReleaseDC(*Parent,hdc);

	 ForceDraw = 0;
	 KeyCheck3();
  }
}

//
void TLineStatus::Resize(HDC *dc)
{
	LSmDC = dc;
}

// -------------------------------------------------------------------------- //
class myfis : public istream {
	 char bf[100];
  public:
	 myfis(char *str) : istream(500,str) {
	 }
	 char *gettokenl() {
		if (!eof()) {
		  *this >> bf;
		  return bf;
		}
		return (char*)0;
	 }
};

// -------------------------------------------------------------------------- //
#ifdef    ERC
char *T_DEFEXTSTR[] = { "TPR","TLSR","TRSR","NKR","RKR","WR_N","WR_R" };
char *S_DEFEXTSTR[] = { "HR","PR","ASR","LMPR" };

int
idsearch(char *relayname) {
	if (!strcmp(relayname,"TRUE")) return -2;
	if (*relayname == '!') return RDF->GetRelayPoint( relayname+1 ) | 0x8000;
	return RDF->GetRelayPoint( relayname );
}

enum InfoModes { InfoNONE, InfoStation, InfoSignal, InfoTrack, InfoPoint,
					  InfoButton, InfoDirect, InfoBoxView, InfoMsg};
int TLineStatus::LoadFileInfo() {
  int i;

  char bf[512], shape[8];
  char idname[20], name[10], oncolor[10], temp[10];

  char fname[80];
  strcpy(fname,StationName);
  strcat(fname,"\\");
  strcat(fname,StationName);
  strcat(fname,".SFM");
  ifstream fis(fname);

  InfoModes mode = InfoNONE;
  TTrack *PreviewT;
  PreviewT = 0;
  int x1,y1,x2,y2,a,b,c,d,e;
  HStationName[0] = 0;
  do {
	 fis.getline(bf,500);
	 if (fis.gcount()){
		myfis is(bf);
		char *s = is.gettokenl();
		if (s && (*s != ';')) {
			strcpy(name,s);
			if (!strcmp(s,"END")) break;
			else if (!strcmp(s,"STATION")) mode = InfoStation;
			else if (!strcmp(s,"SIGNAL")) mode = InfoSignal;
			else if (!strcmp(s,"POINT")) mode = InfoPoint;
			else if (!strcmp(s,"TRACK")) mode = InfoTrack;
			else if (!strcmp(s,"BUTTON")) mode = InfoButton;
			else if (!strcmp(s,"BOXVIEW")) mode = InfoBoxView;
			else if (!strcmp(s,"DIRECT")) mode = InfoDirect;
			else if (!strcmp(s,"MESSAGE")) mode = InfoMsg;
			else {
				if (mode == InfoStation) {
//				  if (HStationName[0]) AbnormalExit("역명이 두개 이상입니다","SFM File error");
				  is >> x1 >> y1 >> a;
				  TStation *St = new TStation(s, x1, y1, a);
//				  strcpy(HStationName,s);
				  Scrobjs->Add(St);
				}
				else if (mode == InfoSignal) {
				  is >> x1 >> y1 >> x2 >> y2;
				  is >> a >> b;
				  TSignal *S = new TSignal(Parent,s,x1,y1,x2,y2,a,b);

// v : 0.L.HR  0.L.PR  0.L.ASR  0.LMPR  X.TPR
				  char LR[10];
				  is >> LR;
				  for (i=0; i<4; i++) {
					 strcpy(idname,name);
					 if (i<3) strcat(idname,LR);
					 strcat(idname,S_DEFEXTSTR[i]);
					 S->v[i] = idsearch(idname);
				  }
				  is >> LR;
				  S->v[4] = idsearch(LR);
				  S->v[5] = idsearch(LR);

				  Scrobjs->Add(S);
				}
				else if (mode == InfoPoint) {
				  TTpoint *P = new TTpoint(s);
				  is >> x1 >> y1 >> x2 >> y2;
				  P->Init(x1,y1,x2,y2);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) P->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  P->v[i] = -1;
				  Scrobjs->Add(P);
				}
				else if (mode == InfoDirect) {
				  TDirect *D = new TDirect(s);
				  is >> x1 >> y1 >> x2 >> y2;
				  is >> a;
				  D->Init(x1,y1,x2,y2,a);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) D->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  D->v[i] = -1;
				  Scrobjs->Add(D);
				}
				else if (mode == InfoButton) {
				  is >> x1 >> y1 >> a >> b;
				  is >> temp >> oncolor;
				  ERCButton *B = new ERCButton(Parent, s, x1, y1, a, b, temp);
				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) B->v[i] = idsearch(name);
					 else break;
					 i++;
					 if (i>=10) break;
				  }
				  B->v[i] = -1;
				  Scrobjs->Add(B);
				}
				else if (mode == InfoBoxView) {
				  is >> x1 >> y1 >> a >> b;
				  is >> temp >> oncolor;
				  TBoxView *BV = new TBoxView(s,x1,y1,a,b,temp,oncolor);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) BV->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  BV->v[i] = -1;
				  Scrobjs->Add(BV);
				}
				else if (mode == InfoTrack) {
				  is >> x1 >> y1 >> x2 >> y2 >> shape;
				  is >> a >> b >> c >> d >> e;
				  TTrack *T = new TTrack(s,x1,y1,x2,y2);
				  if (shape[0] == 'S') a-=2;
				  T->Init(a,b,c,d,e,shape);

				  if (PreviewT) {
					 if (!strcmp(PreviewT->Name, name)) {
						PreviewT->RelationT =T; //T->RelationT = PreviewT;
					 }
					 else if (T->isXTrack() && PreviewT->isXTrack()) {
						 if ( !(PreviewT->RelationT) ) {
							 PreviewT->RelationT =T;
							 T->RelationT =PreviewT;
						 }
					 }
				  }
				  PreviewT = T;

				  int n = strlen(name);
				  char LR[10];
				  if (name[n-1] == 'T') name[n-1] = 0;
								  for (i=0; i<7; i++) {
					 is >> LR;
					 if (LR[0] == '_') {
						 strcpy(idname,name);
						 if (i<3) {
							 strcat(idname,T_DEFEXTSTR[i]);
						 }
						 else {
							 if(name[n-2] == 'A' || name[n-2] == 'B') name[n-2] = 0;
							 strcpy(idname,name);
							 strcat(idname,T_DEFEXTSTR[i]);
						 }
					 }
					 else {
						 strcpy(idname,LR);
						 strcat(idname,T_DEFEXTSTR[i]);
					 }

					 int n = idsearch(idname);
					 if (n == -1) {
						 if (i==5) {
							 strcpy(idname,name);
							 strcat(idname,"WR");
							 n = idsearch(idname);
						 }
						 else if (i==6) {
							 strcpy(idname,"!");
							 strcat(idname,name);
							 strcat(idname,"WR");
							 n = idsearch(idname);
						 }
					 }
					 T->v[i] = n;
				  }

				  is >> idname;
				  if (idname[0]) T->v[7] = idsearch(idname);
				  else T->v[7] = -1;

				  Scrobjs->Add(T);
				}
//				else if (mode == InfoMsg) {
//				  TMessage *S = new TMessage(s);
//				}
		  }
		}
	 }
  } while (!fis.eof());

//  delete RDF;
}

#else  // ERC
/////////////////////////  전 자 연 동 용  //////////
// LDP_
extern int LDP_OUTBASE;
extern int LDP_INBASE;

int
idsearch(char *relayname) {
	if (!strcmp(relayname,"NULL")) return -1;
	if (!strcmp(relayname,"TRUE")) return -2;
	int id;
	if (*relayname == '!') id = RelayDBF->GetRelayPoint( relayname+1 );
	else id = RelayDBF->GetRelayPoint( relayname );
	if (id >= 0) {
		if (id>=LDP_OUTBASE) id -= LDP_OUTBASE;
		else id -= LDP_INBASE;
		if (*relayname == '!') id |= 0x8000;
	}
	return id;
}

#include "inhibit.cpp"
Inhibitor *Inhibit = 0;

enum InfoModes { InfoNONE, InfoStation, InfoSignal, InfoTrack, InfoPoint,
					  InfoButton, InfoDirect, InfoBoxView, InfoMsg, InfoTrainID};

int KeyCheck();
extern int LastTrainID;

int TLineStatus::LoadFileInfo() {
  int i;

  char bf[512], shape[8];
  char idname[20], name[10], oncolor[10], temp[10];

  if ( !RelayDBF ) {
	  RelayDBF = new RelayBase(StationName);
	  if ( !RelayDBF ) return 1;
  }

  char fname[80];


  memset(IOExtBuffer,0x00,LDPBUFSIZE);

  strcpy(fname,StationName);
  strcat(fname,"\\");
  strcat(fname,StationName);
  strcat(fname,".SFM");
  ifstream fis(fname);

  InfoModes mode = InfoNONE;
  TTrack	*PreviewT = 0;
  TrainNumber *TrNo = 0;
  int MyID = 0;
  PreviewT = 0;
  int x1,y1,x2,y2,a,b,c,d,e;
  do {
	 fis.getline(bf,500);
	 if (fis.gcount()){
		myfis is(bf);
		char *s = is.gettokenl();
		if (s && (*s != ';')) {
			strcpy(name,s);
			if (!strcmp(s,"END")) break;
			else if (!strcmp(s,"STATION")) mode = InfoStation;
			else if (!strcmp(s,"SIGNAL")) mode = InfoSignal;
			else if (!strcmp(s,"POINT")) mode = InfoPoint;
			else if (!strcmp(s,"TRACK")) mode = InfoTrack;
			else if (!strcmp(s,"BUTTON")) mode = InfoButton;
			else if (!strcmp(s,"BOXVIEW")) mode = InfoBoxView;
			else if (!strcmp(s,"DIRECT")) mode = InfoDirect;
			else if (!strcmp(s,"MESSAGE")) mode = InfoMsg;
			else if (!strcmp(s,"TRAINID")) mode = InfoTrainID;
			else {
				if (mode == InfoStation) {
				  is >> x1 >> y1 >> a;
				  TStation *St = new TStation(s, x1, y1, a);
				  Scrobjs->Add(St);
				}
				else if (mode == InfoSignal) {
				  is >> x1 >> y1 >> x2 >> y2;
				  is >> a >> b;
				  TSignal *S = new TSignal(Parent,s,x1,y1,x2,y2,a,b);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) S->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  S->v[i] = -1;
				  Scrobjs->Add(S);
				}
				else if (mode == InfoPoint) {
				  is >> x1 >> y1 >> x2 >> y2;
				  TTpoint *P = new TTpoint(Parent,s,x1,y1,x2,y2);
//				  P->Init(x1,y1,x2,y2);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if ( name[0] ) {
						 if (i < 8) P->v[i] = idsearch(name);
						 else if (i == 8) {
						 }
					 }
					 else break;
					 if (i>=8) break;
					 i++;
				  }
				  P->v[i] = -1;
				  Scrobjs->Add(P);
				}
				else if (mode == InfoDirect) {
				  TDirect *D = new TDirect(s);
				  is >> x1 >> y1 >> x2 >> y2;
				  is >> a;
				  D->Init(x1,y1,x2,y2,a);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (!strcmp(name,"STATIONUP")) {
						 strcpy(HStationUp,s);
						 break;
					 }
					 else if (!strcmp(name,"STATIONDOWN")) {
						 strcpy(HStationDown,s);
						 break;
					 }
					 if (name[0]) D->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  D->v[i] = -1;
				  Scrobjs->Add(D);
				}
				else if (mode == InfoButton) {
				  is >> x1 >> y1 >> a >> b;
				  is >> temp >> oncolor;
				  if (_NoControl) {
					  is >> name;
					  goto isBox;
				  }
				  ERCButton *B = new ERCButton(Parent, s, x1, y1, a, b, temp);
				  if (!strcmp(temp,"LAMP")) B->OnColor = *oncolor;
                  if ( !strncmp(s,"통신장애",8) ) ComFailButton=B;
				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) B->v[i] = idsearch(name);
					 else break;
					 i++;
					 if (i>=10) break;
				  }
				  B->v[i] = -1;
				  Scrobjs->Add(B);
				}
				else if (mode == InfoBoxView) {
				  is >> x1 >> y1 >> a >> b;
				  is >> temp >> oncolor;
isBox:
				  TBoxView *BV = new TBoxView(s,x1,y1,a,b,temp,oncolor);

				  i = 0;
				  while (!is.eof()){
					 is >> name;
					 if (name[0]) BV->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  BV->v[i] = -1;
				  Scrobjs->Add(BV);
				}
				else if (mode == InfoTrack) {
				  is >> x1 >> y1 >> x2 >> y2 >> shape;
				  is >> a >> b >> c >> d >> e;
				  TTrack *T = new TTrack(s,x1,y1,x2,y2);
				  if (shape[0] == 'S') a-=2;
				  T->Init(a,b,c,d,e,shape);
				  T->v[8] = -1;

				  if (PreviewT) {
					 if (!strcmp(PreviewT->Name, name)) {
						PreviewT->RelationT =T; //T->RelationT = PreviewT;
						T->v[8] = PreviewT->v[0];
						T->PreT = PreviewT;
					 }
					 else if (T->isXTrack() && PreviewT->isXTrack()) {
						 if ( !(PreviewT->RelationT) ) {
							 PreviewT->RelationT =T;
							 T->RelationT =PreviewT;
						 }
					 }
				  }
				  PreviewT = T;

				  i = 0;
				  while (!is.eof()){
					 is >> name;
//					 strupr(name);
					 if (!strncmp(name,"L_",2)) T->Connect = name[2];
					 if (name[0]) T->v[i] = idsearch(name);
					 else break;
					 if (i>=10) break;
					 i++;
				  }
				  if (i==8) T->v[9] = -1;
                                  else T->v[i] = -1;
				  Scrobjs->Add(T);
				}
				else if (mode == InfoMsg) {
				  is >> a >> b >> idname;
				  TMessage *S = new TMessage(s,idsearch(idname),a,b);
				  Scrobjs->Add(S);
				  isUsedMCISound =1;
				}
				else if (mode == InfoTrainID) {
#ifndef   EIPMAN
				  is >> a >> b;
				  is >> name >> idname >> temp >> oncolor;
				  if (strcmp(s,"CONT")) {
					  MyID = atoi(s);
					  if (!TrainNoTable) {
						  LastTrainID = MyID;
						  TrainNoTable = new TrainNumberPtr[MyID+1];
						  for (i = 0; i<=MyID; i++)
							  TrainNoTable[i] = 0;
					  }
					  TrainNoTable[MyID-1] = new TrainNumber(s,a,b);
					  Scrobjs->Add(TrainNoTable[MyID-1]);
				  }
				  if (MyID) MakeVector(MyID,atoi(oncolor),name,idname,temp);
#endif // EIPMAN
				}
		  }
		}
	 }
  } while (!fis.eof());
#ifndef   EIPMAN
  Inhibitor *Inhibit;
  Inhibit = new Inhibitor(Parent,0,"모타카",50,MWINY-170);
  Scrobjs->Add(Inhibit);
  Inhibit = new Inhibitor(Parent,1,"단전작업",115,MWINY-170);
  Scrobjs->Add(Inhibit);
  Inhibit = new Inhibitor(Parent,2,"사용금지",180,MWINY-170);
  Scrobjs->Add(Inhibit);
#endif // EIPMAN
  return 0;
}


unsigned char *DefKey1[] = {
    "EOFPKGM73GJ3 L83;2CI29O4@",	// 판대
    "H8J28O5F4J62 D<4B9I?16M7A",	// 간현
    "22H2NOL5IO75 HJ<5L2D46:39",	// 동화
    "LMGKM4PIKKFG 0@4H84JFGG45",	// 만종
    "N68JA0A08A42 ?2D8LD8@DDMF",	// 원주
    "P00AC86A1IKP @@H@N8L:O7E5",	// 유교
    "PCE3EON66P2L BL;6E5<4<LL2",	// 반곡
    "J5OKEMDF7AHM ;BB:81JK2INI",	// 금교
    "8LLI4IODN83G D:<DANGJ:GFH",	// 치악
    "5G4L6JBG8G1I 4=EJ4MD=:G>O",	// 창교
    "GKKFB05EO3NE 2=A2JI@A56M>",	// 신림
    "LPNHLEEMBJM0 N6>K4?;OH=DE",	// 연교
    "OP207P1EGNDP K:H@LHL770H0",	// 구학
    "HHKJ0OJ6BHCP >F15;F@F;@82",	// 망우
    "21IH0KNCCOHJ HL3MA?24GOE<",	// 동교
    "2AMK1NBLD9C9 O9:;92?=K?@B",	// 도농
    "77EAMHEACNPF 28H<LLJ8H=F@",	// 팔당
    "JNHMH4438K52 8AE>JIK4J213",	// 능내
    "G175NIFO3MJ1 @L4C2>FA8O45",	// 양수
    "GPEOCGO34C0F 2N385BB42GFG",	// 신원
    "G7CK3BK8O53K ?@>I9KH54BHI",	// 국수
    "JJOPJNFMOIGE K6EA2JCL;DOC",	// 아신
    "GE01D17DINM4 @CEM@A;2OG7:",	// 양평
    "HP0K3PK50BKH 00@G>3@;28LH",	// 전동
    "25OOPJ1JD5OJ 0IIF?7<E;K8C",	// 내판
    NULL
};

void CheckOk() { return; }
void CheckFail() {
	static int count = 0;
	if (count++ == 100)
	  AbnormalExit("이역은 프로그램 사용이 허가되지 않았읍니다","SYSTEM ERROR");
}
void (*CheckFun)() = CheckOk;
void KeyCheck3() {
	CheckFun();
}

int KeyCheck1(unsigned char *b2, unsigned char *b1) {
  int i;
  for (i=0; i<12; i++) {
	  b2[i] = ((b2[i]>>1) + (b2[i+1]<<1) + (b2[i+1]<<2)) * b1[i/3];
	  b2[i] = (b2[i] >> 3) + '0';
  }
  //for (i=0; i<20; i++) {
  //	 if (!memcmp(DefKey1[i]+13,b2,12)) {
  //		 mysprintf = wsprintf;
  //		 return 0;
  //	 }
  //}
  i=0; 	// '98.12.03
  while( DefKey1[i] ) {
	 if ( memcmp(DefKey1[i]+13,b2,12) ) i++;
     else {
		 mysprintf = wsprintf;
		 return 0;
	 }
  }
  return 1;
}

int KeyCheck() {
  int i;
  unsigned char b1[40],b2[24],b3[24];
  strncpy(b1,HStationName,8);
  strncpy(&b1[8],HStationUp,8);
  strncpy(&b1[16],HStationDown,8);
  memcpy(b2,b1,24);
  for (i=0; i<24; i++) b3[i] = b1[23-i] ^ b2[i] + b1[0];
  for (i=0; i<12; i++) {
	  b3[i] = (b1[23-i] ^ b2[i])*b1[1];
  }
  memcpy(b2,b3,24);
  for (i=0; i<12; i++) {
	  b3[i] = (b3[i] + (b3[i+1]>>4) + (b3[i+1]<<3)) * b1[i/3];
	  b3[i] = (b3[i] >> 3) + '0';
	  if (!isalnum(b3[i])) b3[i]= (b3[i]+0x10) & 0x7f;
  }
  //for (i=0; i<20; i++) {
  //	 if (!memcmp(DefKey1[i],b3,12)) {
  //		 GetState = _GetState;
  //		 return KeyCheck1(b2,b1);
  //	 }
  //}
  i=0; 	// '98.12.03
  while( DefKey1[i] ) {
	 if ( memcmp(DefKey1[i],b3,12) ) i++;	// not like to ~
     else {                       			// same to ~
  		 GetState = _GetState;
  		 return KeyCheck1(b2,b1);
	 }
  }
  CheckFun = CheckFail;
  return 1;
}

#endif // ERC

char LockCode[32];
class KeyLockWindow : public TDialog {
public:
	KeyLockWindow(TWindow *parent,char *tbf, char *msg)
	  : TDialog(parent,IDD_PASSWORD)
	{
		new TFormattedStatic(this, IDC_PASSTITLE, msg);
		new TEdit(this,IDC_PASSWORD,20);
		TransferBuffer = (void far*)tbf;
	}
};

void CmKeyLock(TWindow *parent) {
	char bf[32];
	bf[0] = 0;
	if (!KeyLock) {
		if (KeyLockWindow(parent,bf,"지정").Execute() == IDOK) {
			if (bf[0]) {
				strcpy(LockCode,bf);
				SystemState |= SYS_KEYLOCK;
				InhibitMsg();
			}
		}
	}
	else {
		if (KeyLockWindow(parent,bf,"해제").Execute() == IDOK) {
			if (bf[0]) {
				if (!strcmp(LockCode,bf)) {
					SystemState &= ~SYS_KEYLOCK;
					MessageBox(0,"제어가능 상태 입니다","SYSTEM LOCK",MB_OK);
				}
			}
		}
	}
}



