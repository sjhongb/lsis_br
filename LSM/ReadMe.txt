========================================================================
		OLE Control DLL : MSCR
========================================================================

ControlWizard has created this project for your MSCR OLE Control DLL,
which contains 1 control.

This skeleton project not only demonstrates the basics of writing an OLE
Control, but is also a starting point for writing the specific features
of your control.

This file contains a summary of what you will find in each of the files
that make up your MSCR OLE Control DLL.

MScr.mak
	The Visual C++ project makefile for building your OLE Control.

MScr.h
	This is the main include file for the OLE Control DLL.  It
	includes other project-specific includes such as resource.h.

MScr.cpp
	This is the main source file that contains code for DLL initialization,
	termination and other bookkeeping.

MScr.rc
	This is a listing of the Microsoft Windows resources that the project
	uses.  This file can be directly edited with the Visual C++ resource
	editor.

MScr.def
	This file contains information about the OLE Control DLL that
	must be provided to run with Microsoft Windows.

MScr.clw
	This file contains information used by ClassWizard to edit existing
	classes or add new classes.  ClassWizard also uses this file to store
	information needed to generate and edit message maps and dialog data
	maps and to generate prototype member functions.

MScr.odl
	This file contains the Object Description Language source code for the
	type library of your control.

/////////////////////////////////////////////////////////////////////////////
MScr control:

MScrCtl.h
	This file contains the declaration of the CMScrCtrl C++ class.

MScrCtl.cpp
	This file contains the implementation of the CMScrCtrl C++ class.

MScrPpg.h
	This file contains the declaration of the CMScrPropPage C++ class.

MScrPpg.cpp
	This file contains the implementation of the CMScrPropPage C++ class.

MScrCtl.bmp
	This file contains a bitmap that a container will use to represent the
	CMScrCtrl control when it appears on a tool palette.  This bitmap
	is included by the main resource file MScr.rc.

/////////////////////////////////////////////////////////////////////////////
Help Support:

MakeHelp.bat
	Use this batch file to create your OLE control's Help file, MScr.hlp.

MScr.hpj
	This file is the Help Project file used by the Help compiler to create
	your OLE control's Help file.

*.bmp
	These are bitmap files required by the standard Help file topics for
	Microsoft Foundation Class Library standard commands.  These files are
	located in the HLP subdirectory.

MScr.rtf
	This file contains the standard help topics for the common properties,
	events and methods supported by many OLE controls.  You can edit
	this to add or remove additional control specific topics.  This file is
	located in the HLP subdirectory.

/////////////////////////////////////////////////////////////////////////////
Licensing support:
MScr.lic
	This is the user license file.  This file must be present in the same
	directory as the control's DLL to allow an instance of the control to be
	created in a design-time environment.  Typically, you will distribute
	this file with your control, but your customers will not distribute it.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

stdafx.h, stdafx.cpp
	These files are used to build a precompiled header (PCH) file
	named stdafx.pch and a precompiled types (PCT) file named stdafx.obj.

resource.h
	This is the standard header file, which defines new resource IDs.
	The Visual C++ resource editor reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

ControlWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
