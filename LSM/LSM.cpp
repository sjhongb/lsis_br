//===============================================================
//====                     LSM.cpp                       ====
//===============================================================
// 파  일  명 : LSM.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMApp와 DLL 등록을 수행한다.
//
//===============================================================

#include "stdafx.h"
#include "LSM.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CLSMApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0x4152f441, 0x9a90, 0x11d3, { 0xaa, 0x95, 0, 0xc0, 0x6c, 0, 0x3c, 0x3f } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


////////////////////////////////////////////////////////////////////////////
// CLSMApp::InitInstance - DLL initialization

BOOL CLSMApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	SetRegistryKey(_T("LSIS\\LES"));
	m_iMySystemNo = GetProfileInt("", "SYSNO",5);
	m_iNumberOfLC = 0;
	m_iNumberOfEPK = 0;
	m_iNumberOfHPK = 0;
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
	
	CString strLCName;
	for ( int i = 1; i < 6; i++)
	{
		strLCName.Format("LC%d",i);
		WriteProfileString("", strLCName, "FALSE");
	}

	CString strBlockName;
	for ( int j = 1; j < 17; j++)
	{
		strBlockName.Format("B%d",j);
		WriteProfileString("BLOCK", strBlockName, "FALSE");
	}

	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
	
	for ( int k = 1; k < 6; k++)
	{
		strLCName.Format("LC%d",k);
		WriteProfileString("", strLCName, "FALSE");
	}
	
	CString strBlockName2;
	for ( int l = 1; l < 17; l++)
	{
		strBlockName2.Format("B%d",l);
		WriteProfileString("BLOCK", strBlockName2, "FALSE");
	}
	
	SetRegistryKey(_T("LSIS\\LIS"));
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
	
	for ( int m = 1; m < 6; m++)
	{
		strLCName.Format("LC%d",m);
		WriteProfileString("", strLCName, "FALSE");
	}
	
	CString strBlockName3;
	for ( int n = 1; n < 17; n++)
	{
		strBlockName3.Format("B%d",n);
		WriteProfileString("BLOCK", strBlockName3, "FALSE");
	}
	
	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}

void CLSMApp::SetStationUpDnDirection(int iStationUpDn)
{
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileInt("", "STATION_DIRECTION",iStationUpDn);
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileInt("", "STATION_DIRECTION",iStationUpDn);
}

void CLSMApp::SetBlockInformation(CString strBlockName)
{
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileString("BLOCK", strBlockName, "TRUE");
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileString("BLOCK", strBlockName, "TRUE");
}

void CLSMApp::SetAxleInformation(CString strBlockName, CString strAxleName)
{
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileString("BLOCK", strBlockName, strAxleName);
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileString("BLOCK", strBlockName, strAxleName);
}
void CLSMApp::SetLCInformation(int iLCNum, CString strLCName)
{
	CString strLCNum;
	strLCNum.Format("LC%d",iLCNum);
	if ( strLCName.Find('@') > 0)
		strLCName = strLCName.Left(strLCName.Find('@'));
	if ( strLCName.Find('$') > 0)
		strLCName = strLCName.Left(strLCName.Find('$'));
	
	SetRegistryKey(_T("LSIS\\LES"));
	CString strExistingLCName = GetProfileString("", strLCNum , NULL);
	if ( strExistingLCName != "FALSE")
	{
		strLCName = strLCName + "-" + strExistingLCName;
	}
	else
	{
		m_iNumberOfLC++;
	}
	WriteProfileString("", strLCNum, strLCName);
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);

	m_strLCName[iLCNum] = strLCName;
	
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);
	WriteProfileString("", strLCNum, strLCName);
	
	SetRegistryKey(_T("LSIS\\LIS"));
	WriteProfileInt("", "NumberOfLC", m_iNumberOfLC);
	WriteProfileString("", strLCNum, strLCName);
}
void CLSMApp::SetEPKInformation()
{
	m_iNumberOfEPK++;
	
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
	SetRegistryKey(_T("LSIS\\LIS"));
	WriteProfileInt("", "NumberOfEPK", m_iNumberOfEPK);
}
void CLSMApp::SetHPKInformation()
{
	m_iNumberOfHPK++;
	
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
	SetRegistryKey(_T("LSIS\\LIS"));
	WriteProfileInt("", "NumberOfHPK", m_iNumberOfHPK);
}
void CLSMApp::SetStationName(CString strStationName)
{
	CString strProjectName;
	if ( strStationName.Find('@') > 0)
	{
		int iFindForAt = strStationName.Find('@');
		
		strProjectName = strStationName.Right(strStationName.GetLength()-iFindForAt-1);
		strStationName = strStationName.Left(iFindForAt);
	}
	
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileString("", "StationName", strStationName);
	WriteProfileString("", "ProjectName", strProjectName);
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileString("", "StationName", strStationName);
	WriteProfileString("", "ProjectName", strProjectName);
	SetRegistryKey(_T("LSIS\\LIS"));
	WriteProfileString("", "StationName", strStationName);
	WriteProfileString("", "ProjectName", strProjectName);
}
void CLSMApp::SetFlagOfSyncWithLES()
{
	SetRegistryKey(_T("LSIS\\LES"));
	WriteProfileString("", "FlagOfFinishSync", "TRUE");
	SetRegistryKey(_T("LSIS\\LLR"));
	WriteProfileString("", "FlagOfFinishSync", "TRUE");
}

////////////////////////////////////////////////////////////////////////////
// CLSMApp::ExitInstance - DLL termination

int CLSMApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
