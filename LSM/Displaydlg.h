#if !defined(AFX_DISPLAYDLG_H__9888B6BA_274E_43FE_BFC4_39F3ABD1E55B__INCLUDED_)
#define AFX_DISPLAYDLG_H__9888B6BA_274E_43FE_BFC4_39F3ABD1E55B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Displaydlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDisplaydlg dialog

class CDisplaydlg : public CDialog
{
// Construction
public:
	CDisplaydlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDisplaydlg)
	enum { IDD = IDD_DIALOG_Display };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDisplaydlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDisplaydlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISPLAYDLG_H__9888B6BA_274E_43FE_BFC4_39F3ABD1E55B__INCLUDED_)
