//===============================================================
//====                     LSMCtl.h                         ====
//===============================================================
// 파  일  명 : LSMCtl.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMCtrl OLE control 클래스에서 수행하는 화면 내 
//              각종 제어 부분을 관리한다.            
//
//===============================================================

#include "Loader.h"
#include "LSMOpt.h"
#include "../include/EipEmu.h"
#include "ClockST.h"
#include "../include/scrobj.h"

/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl : See LSMCtl.cpp for implementation.

//#define VMEMSIZE MAX_MSGBLOCK_SIZE

/////////////////////////////////////////////////////////////////////////////

class CLoader;
class CBmpSignal;
class CBmpSwitch;
class CBmpButton;
class CLSMCtrl : public COleControl
{
	DECLARE_DYNCREATE(CLSMCtrl)

public:
		void ObjectModify( long nObject );
	
	void ViewObjectInfo( CBmpTrack* pTrack );
	BOOL OpMsgConfirm( const char *, short x, short y);
	BOOL OpMsgConfirm( const char *, long xy);
	BOOL CreateScrInfo();
	long OpMsgLoadFileInfo(  long sfmNamePtr, long modeNamePtr );
	long RunAssignScrInfo( short nCount, CScrInfo *pScrInfo );

	void OnRouteLineDraw(CDC *);
	void OnUpdateDest(CCmdUI* pCmdUI);
	void OnDestSelected( UINT nID );
	void DestEnableCheck( BYTE nSigID, CPoint &point );
	void DoSystemButtonEvent( TScrobj *pSysObj, CPoint &point);
	void DoButtonEvent( CBmpButton *pButton, CPoint &point );
	void DoSwitchEvent( CBmpSwitch *pSwitch, CPoint &point );
	void DoSignalEvent( CBmpSignal *pSignal, CPoint &point );
	void DoTrackEvent( CBmpTrack *pTrack, CPoint &point );
	BOOL CheckConfirm( CString &str, CPoint &point,  CString &strwindow, int symb0l = 1 );
	short Signalstop( CString &str, CString &release, CString &restr, CPoint &point );
	short PointControl( CString &str, CString &move, CString &block, CPoint &point );
	BOOL CheckOverlapSwitch ( BYTE *pInfo );
	CString CheckRouteIndicatorCondition( CString SigName);

	short m_nTTBID;
	int m_nLastRouteofs;
	CString m_strErrorMsg;
	CString m_strUserID;
	RICondition m_RouteIndicatorInfo;
	CBmpSignal *m_pLastSignal;
	TScrobj *m_pPressObj;
	int m_iProject;

	short m_nMaxScrInfo;
	short m_nSfmRecSize;
	char  m_cOption;	// Ole Control Bit Flag
	char  m_cConfig;	// Object Control Bit Flag
	BOOL  m_bDispSaveTime;
	RECT  m_WndRect;
    BYTE *m_pMsg;   // Opearte Message and TNI/ErrorCode Buffer, 16 BYTE

	CDC m_DC;
    CDC *m_pDC;
	CBitmap m_Bitmap;
	CClockST m_Clock;  
// Constructor
public:
	TScrobj *SetScrObject( TScrobj *pObj, CScrInfo *pInfo );
	TScrobj* SearchPtrByID( char cType, short nID );
	TScrobj* SearchPtrByName( char cType, char *szName );
	TScrobj* SearchblkByName( char *szName );
    void SearchPtrsByID( char cType, short nID, TScrobj* objs[5] );
	CRouteLine* SearchRoutePos(CString &);
	CRouteLine* SearchRoutePos(int sigID, int buttonID);
	void PurgeDoc();

	CLSMCtrl();

	CMyStrList m_listRouteMenu;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLSMCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
protected:

	CEipEmu  m_Engine;
	CScrInfo *m_pScrInfo;
	CObArray m_ScrObject;
	CObArray m_DestButtons;
	CObArray m_RoutePosObj;
	CRouteLine* m_pRouteLine;
	BYTE m_RouteLineType;
	UINT m_nTimerHandle;
	BOOL m_bComFail;
	BOOL m_bSysOptLOCK;
	BOOL m_bOptLOCK;
	BOOL m_bLOCK;
	BOOL m_bSysDownctl;
	BOOL m_bPasOut;
	BOOL m_bStation; 
	CPoint  m_MenuPoint;
	CString m_CompanyName;
	BYTE m_pScrMem[ MAX_MSGBLOCK_SIZE ];


	~CLSMCtrl();

	BEGIN_OLEFACTORY(CLSMCtrl)        // Class factory and guid
		virtual BOOL VerifyUserLicense();
		virtual BOOL GetLicenseKey(DWORD, BSTR FAR*);
	END_OLEFACTORY(CLSMCtrl)

	DECLARE_OLETYPELIB(CLSMCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CLSMCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CLSMCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CLSMCtrl)
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	//}}AFX_MSG
	afx_msg void OnUninitMenuPopup(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CLSMCtrl)
	afx_msg short LoadFileInfo(LPCTSTR pFileName);
	afx_msg short FindIndex(LPCTSTR szName);
	afx_msg void  SetTNI(short nID, LPCTSTR szNo);
	afx_msg short SearchByName(short cType, LPCTSTR szName);
	afx_msg void  SetButtonRoute(LPCTSTR pInfo, short nRoute);
	afx_msg long  AssignScrInfo(long pInfoBuffer);
	afx_msg void  ScreenUpdate(long pVMEM);
	afx_msg short OperateSignalOn(long ID);
	afx_msg short OperateBlockOn(long ID);
	afx_msg short OperateSignalOff(long ID);
	afx_msg short OperateSwitch(long ID);
	afx_msg short OnRoadMarking(short cmd);
	afx_msg void GetLSMSize(long pSize);
	afx_msg void SetLSMOption(short option);
	afx_msg long OlePostMessage(long message, long wParam, long lParam);
	afx_msg BOOL GetMenuStatus();
	afx_msg void DisplayClock(BOOL bDisplayClock);
	afx_msg void DisplayBG(short iStationNo);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	afx_msg void AboutBox();

#ifdef _DEBUG
	virtual void AssertValid() const {};
	virtual void Dump(CDumpContext& dc) const {};
	ExchangeVersion(class CPropExchange *,unsigned long,int=0) { return 0; };
#endif

// Event maps
	//{{AFX_EVENT(CLSMCtrl)
	void FireOperate(short nFunction, short nArgument)
		{FireEvent(eventidOperate,EVENT_PARAM(VTS_I2  VTS_I2), nFunction, nArgument);}
	void FireOleSendMessage(long message, long wParam, long lParam)
		{FireEvent(eventidOleSendMessage,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4), message, wParam, lParam);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	

	BOOL m_bFirst;
	BOOL m_bMenuStatus;
	BOOL m_bDisplayClock;
	enum {
	//{{AFX_DISP_ID(CLSMCtrl)
	dispidLoadFileInfo = 1L,
	dispidFindIndex = 2L,
	dispidSetTNI = 3L,
	dispidSearchByName = 4L,
	dispidSetButtonRoute = 5L,
	dispidAssignScrInfo = 6L,
	dispidScreenUpdate = 7L,
	dispidOperateSignalOn = 8L,
	dispidOperateSignalOff = 10L,
	dispidOperateSwitch = 11L,
	dispidOnRoadMarking = 12L,
	dispidGetLSMSize = 13L,
	dispidSetLSMOption = 14L,
	dispidOlePostMessage = 15L,
	dispidGetMenuStatus = 16L,
	dispidDisplayClock = 17L,
	dispidDisplayBG = 18L,
	eventidOperate = 1L,
	eventidOleSendMessage = 2L,
	//}}AFX_DISP_ID
	};
friend CLoader;
};

