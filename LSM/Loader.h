//===============================================================
//====                     Loader.h                          ====
//===============================================================
// 파  일  명 : Loader.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : input 데이터를 읽어들여 메모리에 할당한다.
//
//===============================================================

#ifndef   _LOADER_H
#define   _LOADER_H

/////////////////////////////////////////////////////////////////////////////
// CLoader window

#include "../include/bmpinfo.h"

/////////////////////////////////////////////////////////////////////////////

class CRouteLine : public CObject 
{
public :
	CString   m_strRoute;
	int m_nSigID;
	int m_nButtonID;
	CPtrArray  m_Array;
	CPtrArray  m_2ndArrayForOverlap;

public:
	CRouteLine( CString &route ) 
	{
		m_strRoute += route;
	};
	~CRouteLine() 
	{
		int count = m_Array.GetSize();
		CPoint *p;
		for (int i=0; i<count; i++ ) {
			p = (CPoint*)m_Array.GetAt( i );			
			delete p;
		}
		m_Array.RemoveAll();

		int count2 = m_2ndArrayForOverlap.GetSize();
		CPoint *p2;
		for (int i2=0; i2<count2; i2++ ) {
			p2 = (CPoint*)m_2ndArrayForOverlap.GetAt( i2 );			
			delete p2;
		}
		m_2ndArrayForOverlap.RemoveAll();
	};

	void DrawLine(CDC *pDC)
	{
		int count = m_Array.GetSize();
		CPoint *p;
		if ( !count ) return;
		for(int i=0; i<count; i++) {
			p = (CPoint*)m_Array.GetAt( i );
			if (i == 0) pDC->MoveTo(*p);
			else pDC->LineTo(*p);
		}
	}
	void Draw2ndLine(CDC *pDC)
	{
		int count = m_2ndArrayForOverlap.GetSize();
		CPoint *p;
		if ( !count ) return;
		for(int i=0; i<count; i++) {
			p = (CPoint*)m_2ndArrayForOverlap.GetAt( i );
			if (i == 0) pDC->MoveTo(*p);
			else pDC->LineTo(*p);
		}
	}
	void SetID(int sigID, int buttonID)
	{
		m_nSigID = sigID;
		m_nButtonID = buttonID;
	}
	void AddPoint(CPoint *p)
	{
		m_Array.SetAtGrow( m_Array.GetSize(), p);
	};
	void Add2ndPoint(CPoint *p)
	{
		m_2ndArrayForOverlap.SetAtGrow( m_2ndArrayForOverlap.GetSize(), p);
	};
};
/////////////////////////////////////////////////////////////////////////////
typedef struct
{
	CString AspectSigCondition[5];
	CString NonAspectSigCondition[5];
	CString PointCondition[10];
}RIAspectCondition;
typedef struct
{
	CString SigName[100];
	CString SigAspect[100];
	RIAspectCondition AspectCondition[100];
}RICondition;
/////////////////////////////////////////////////////////////////////////////
class CBmpTrack;
class CLoader
{
// Construction
public:
	CLoader();
	static short LoadRouteInfo( CObArray &mRoutePosList, LPCTSTR pFileName);
	static short LoadRouteIndicatorInfo( RICondition &RouteIndicatorInfo, LPCTSTR pFileName);
	static short LoadFileInfo( CObArray &mTrackList, LPCTSTR pFileName);
	//static short LoadFileInfo( CObArray &mTrackList, CObArray &mRoutePosList, LPCTSTR pFileName);
	static void FindJoinTrack( CObArray &TrackList );
	static CBmpTrack *TrackFind( CObArray &TrackList, CBmpTrack* T, int n, int& node);
    


// Attributes
public:


// Operations
public:


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoader)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLoader();

	// Generated message map functions
protected:
};

/////////////////////////////////////////////////////////////////////////////
#endif // _LOADER_H
