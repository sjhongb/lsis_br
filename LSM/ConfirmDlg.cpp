//===============================================================
//====                     ConfirmDlg.cpp                    ====
//===============================================================
// 파  일  명 : ConfirmDlg.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 메인 화면과 독립적인 Confirm Dialog를 화면에 띄어준다.
//
//===============================================================


#include "stdafx.h"
//#include "LSM.h"
#include "ConfirmDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfirmDlg dialog


CConfirmDlg::CConfirmDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfirmDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfirmDlg)
	m_strText = _T("");
	m_strWindow =_T("");
	m_strOK = _T("OK");
	m_strCancel = _T("CANCEL");
	//}}AFX_DATA_INIT
}


void CConfirmDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfirmDlg)
	DDX_Control(pDX, IDC_STATIC_ICON, m_stIcon);
	DDX_Text(pDX, IDC_STATIC_TEXT, m_strText);
	DDX_Text(pDX, IDOK, m_strOK);
	DDX_Text(pDX, IDCANCEL, m_strCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfirmDlg, CDialog)
	//{{AFX_MSG_MAP(CConfirmDlg)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfirmDlg message handlers

BOOL CConfirmDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
    
	m_stIcon.SetIcon(hIcon);
	SetWindowText(m_strWindow);

	

//	OnResizeWindow();
	return TRUE;  // return TRUE unless you set the focus to a control
	              
}


void GlovalShowWindow( CWnd *pWnd, int x, int y, int wx, int wy );
int CConfirmDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	m_stIcon.SetIcon(hIcon);
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
/*
	if ( m_nDirection & 1 ) {	// right
		m_Position.x += 20;
	}
	else if ( m_nDirection & 4 ) {	// left
		m_Position.x -= 20;
		m_Position.x -= lpCreateStruct->cx;
	}
	if ( m_nDirection & 2 ) {	// upper
		m_Position.y -= 40;
		m_Position.y -= lpCreateStruct->cy;
	}
	else if ( m_nDirection & 8 ) {	// lower
		m_Position.y += 100;
	}
*/

	m_Position.y += 100;
	if(m_Position.x > 640)
	{
		m_Position.x -= 250;
	}
	
//	GlovalShowWindow( this, m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
//	MoveWindow( m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
//	ShowWindow( SW_SHOW );
	
	return 0;
}



