//===============================================================
//====                 ObjectinfoDlg.h                       ====
//===============================================================
// 파  일  명 : ObjectinfoDlg.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : User가 직접 Application내에서 주고받는 In/Output
//              Data를 확인할 수 있도록 한다.
//
//===============================================================

#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CObjectInfoDlg dialog

#include "../include/bmpinfo.h"

class CLSMCtrl;
class CObjectInfoDlg : public CDialog
{
// Construction
public:

	CBmpTrack* m_pTrack;
	CObjectInfoDlg(CWnd* pParent = NULL);   // standard constructor

	CLSMCtrl *m_pCtrl;
// Dialog Data
	//{{AFX_DATA(CObjectInfoDlg)
	enum { IDD = IDD_DIALOG_INFO };
	CString	m_strInfo;
	CListCtrl m_list;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjectInfoDlg)
	public:
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CObjectInfoDlg)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
