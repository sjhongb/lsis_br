#ifndef _LOGIC_H
#define _LOGIC_H

#define LOGEQ		0x7f00
#define LOGPUSH		0x7f01
#define LOGPOP		0x7f02
#define	LOGAND		0x7f03
#define LOGOR		0x7f04
#define LOGNOT		0x7f05
#define LOGTRUE     0x7f06
#define LOGFALSE    0x7f07

class CBool;

class CLogic {
public:
	void Create( WORD iID );
	CBool *Reduce( CBool *pStart, CBool *pEnd );
	void AddData( WORD id );
	void Create( CString &s );
//	int m_nSize;
	CBool *m_pData;
	BOOL m_bState;
	CString m_Name;
public:
	CLogic();
	~CLogic();
	BOOL GetValue() { return m_bState; }
	void Update();
	void Create( char *bf );
	CLogic &operator = (CLogic &obj);
	CString &GetName() { return m_Name; }
	BOOL isValid() { return m_pData != NULL; }

	virtual int idsearch( char *name ) {
		return -1;
	}
};

class CLogicParser {
	char *m_pStr, *m_pPtr, *m_pDel;
	int  m_nLoc, m_nLen;
	CLogic *m_pLogic;
	int m_nLogCount;

public:
	BOOL m_bDelimeter;

	BOOL CreateLogic();
	CLogicParser( char *str, char *pDel = NULL );
	~CLogicParser();
	BOOL GetToken( CString &str );
	int GetCount() { return m_nLogCount; }
	CLogic &GetLogic( int id ) { return m_pLogic[id]; }
	char *gettokenl();
};

//int GetState(int n);
//int idsearch( char *name );

#endif
