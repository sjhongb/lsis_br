// SignalSelect.cpp : implementation file
//
#include "stdafx.h"
//#include "LSM.h"
#include "SignalSelect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSignalSelect dialog


CSignalSelect::CSignalSelect(CWnd* pParent /*=NULL*/)
	: CDialog(CSignalSelect::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSignalSelect)
	m_nSelectType = -1;
	//}}AFX_DATA_INIT
}


void CSignalSelect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSignalSelect)
	DDX_Radio(pDX, IDC_RADIO_REROUTE, m_nSelectType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSignalSelect, CDialog)
	//{{AFX_MSG_MAP(CSignalSelect)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSignalSelect message handlers


int CSignalSelect::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::DoModal();
}

int CSignalSelect::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	int dotmetricX  = GetSystemMetrics( SM_CXFULLSCREEN );
	int dotmetricY  = GetSystemMetrics( SM_CYFULLSCREEN );
	int dotmetricXM = dotmetricX / 2;
	int dotmetricYM = dotmetricY / 2;

	if ( m_Position.x < dotmetricXM ) 
		m_Position.x = 30;
	else 
		m_Position.x = dotmetricX - (lpCreateStruct->cx + 30);
	if ( m_Position.y < dotmetricYM )
		m_Position.y = 200;
	else 
		m_Position.y = 200 + dotmetricYM;

    MoveWindow( m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
    ShowWindow( SW_SHOW );

	return 0;
}
