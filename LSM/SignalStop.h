//===============================================================
//====                      SignalStop.h                     ====
//===============================================================
// 파  일  명 : SignalStop.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 신호기 정지 제어 Dialog를 띄운다. 
//
//===============================================================

#include "resource.h"

class CSignalStop : public CDialog
{
// Construction
public:
		CPoint m_Position;
	CSignalStop(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSignalStop)
	enum { IDD = IDD_DIALOG_SIGSTOP };
	int		m_nSelectType;
	CString	m_strText;
	CString m_retryText;
	CString m_releaseText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSignalStop)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSignalStop)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
