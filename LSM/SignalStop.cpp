//===============================================================
//====                     SignalStop.cpp                    ====
//===============================================================
// 파  일  명 : SignalStop.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 신호기 정지 제어 Dialog를 띄운다. 
//
//===============================================================

#include "stdafx.h"
#include "SignalStop.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSignalStop dialog


CSignalStop::CSignalStop(CWnd* pParent /*=NULL*/)
	: CDialog(CSignalStop::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSignalStop)
	m_nSelectType = -1;
	m_strText = _T("");
	m_retryText = _T("");
	m_releaseText = _T("");
	//}}AFX_DATA_INIT
}


void CSignalStop::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSignalStop)
	DDX_Radio(pDX, IDC_RADIO_ROUTE, m_nSelectType);
	DDX_Text(pDX, IDC_STATIC_TEXT2, m_strText);
	DDX_Text(pDX, IDC_RADIO_SIGCAN, m_retryText);
	DDX_Text(pDX, IDC_RADIO_ROUTE, m_releaseText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSignalStop, CDialog)
	//{{AFX_MSG_MAP(CSignalStop)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSignalStop message handlers


int CSignalStop::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::DoModal();
}

int CSignalStop::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	m_Position.y += 100;
	if(m_Position.x > 640)
	{
		m_Position.x -= 250;
	}
	
//    MoveWindow( m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
    ShowWindow( SW_SHOW );

	return 0;
}



BOOL CSignalStop::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
