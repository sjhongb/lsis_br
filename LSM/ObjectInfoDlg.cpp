//===============================================================
//====                 ObjectinfoDlg.cpp                     ====
//===============================================================
// 파  일  명 : ObjectinfoDlg.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : User가 직접 Application내에서 주고받는 In/Output
//              Data를 확인할 수 있도록 한다.
//
//===============================================================

#include "stdafx.h"
#include "LSMCtl.h"
#include "ObjectInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CObjectInfoDlg dialog
CObjectInfoDlg::CObjectInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjectInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjectInfoDlg)
	m_strInfo = _T("");
	//}}AFX_DATA_INIT
	m_pCtrl = NULL;
}

void CObjectInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjectInfoDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CObjectInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CObjectInfoDlg)
		ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CObjectInfoDlg message handlers
BOOL CObjectInfoDlg::OnInitDialog() 
{	
	CDialog::OnInitDialog();
	CString strInfo;
	m_pTrack->ListCtrlDraw( strInfo , m_list );

	//자동 update을 위해 Timer를 설정한다. --> Event ID 는 9이다.
	
	SetTimer(9,500,NULL);
	return TRUE;
}

// Timer 주기 처리 함수...
void CObjectInfoDlg::OnTimer(UINT nIDEvent) 
{
	m_pCtrl->m_bMenuStatus = 1;
	switch(nIDEvent)
	{
	case 9:
		CString strInfo;
		m_pTrack->MakeInfoList( strInfo , m_list, TRUE );
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void CObjectInfoDlg::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	DWORD lPos = GetMessagePos();
	POINTS pos = MAKEPOINTS( lPos );

	// 표출 위치를 지정한다...
	CPoint point = CPoint( pos.x, pos.y );
	m_list.ScreenToClient( &point );

	UINT uFlags = 0;
	int nHitItem = m_list.HitTest(point, &uFlags);

	if (uFlags & LVHT_ONITEM)
	{
		if ( m_pTrack ) 
		{
			int nObject = m_pTrack->GetToggleObject( nHitItem );
			if ( m_pCtrl ) {
				m_pCtrl->ObjectModify( nObject );
			}
			m_list.Invalidate( FALSE );
		}
	}

	*pResult = 0;
}

void CObjectInfoDlg::OnOK() 
{
	KillTimer(9);
	m_pCtrl->m_bMenuStatus = 0;	
	CDialog::OnOK();
}

