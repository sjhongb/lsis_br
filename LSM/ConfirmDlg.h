//===============================================================
//====                     ConfirmDlg.h                      ====
//===============================================================
// 파  일  명 : ConfirmDlg.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 메인 화면과 독립적인 Confirm Dialog를 화면에 띄어준다.
//
//===============================================================


#include "../include/EipEmu.h"	// Added by ClassView
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CConfirmDlg dialog

class CConfirmDlg : public CDialog
{
// Construction
public:
//	int m_nDirection;
	CPoint m_Position;
	HICON hIcon;
	CConfirmDlg(CWnd* pParent = NULL);   // standard constructor
// 0x09 = right, lower

// Dialog Data
	//{{AFX_DATA(CConfirmDlg)
	enum { IDD = IDD_DIALOG_CONFIRM };
	CStatic	m_stIcon;
	CString	m_strText;
	CString m_strWindow;
	CString m_strOK;
	CString m_strCancel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfirmDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfirmDlg)
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
