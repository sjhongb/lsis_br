#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CPointControl dialog

class CPointControl : public CDialog
{
// Construction
public:
	CPoint m_Position;
	CPointControl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPointControl)
	enum { IDD = IDD_DIALOG_POINT_CONTROL };
	int		m_nSeletType;
	CString m_strText;
	CString m_BlockText;
	CString m_MoveText;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPointControl)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPointControl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

