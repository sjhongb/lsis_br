//===============================================================
//====                     Loader.cpp                        ====
//===============================================================
// 파  일  명 : Loader.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : input 데이터를 읽어들여 메모리에 할당한다.
//
//===============================================================


#include "stdafx.h"
#include "Loader.h"
#include "LSM.h"

#include "../include/Scrobj.h"
#include "../include/Parser.h"
#include "../include/logic.h"

#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoader

CLoader::CLoader()
{
}

CLoader::~CLoader()
{
}

//enum InfoModes { InfoNONE, InfoStation, InfoSignal, InfoTrack, InfoPoint, InfoButton, InfoDirect, InfoBoxView};
enum InfoModes { InfoNONE, InfoStation, InfoSignal, InfoTrack, InfoPoint, InfoButton, InfoDirect, InfoBoxView, InfoMsg, InfoTrainID , InfoCell};

/*
class myfis : public istrstream {
	char bf[500];
public:
	myfis(char *str) : istrstream( str, 500 ) {}
	char *gettokenl() {
		bf[0] = 0;
		if (!eof()) {
			*this >> bf;
			if( !strcmp(bf,",") || !strcmp(bf,"[") || !strcmp(bf,"]") || !strcmp(bf,"(") || !strcmp(bf,")"))
				return (char*)gettokenl();
			return bf;
		}
		return (char*)0;
	}
	void Left( CString &line );
};

void myfis::Left( CString &line ) {
	strstreambuf *str = rdbuf();
	int l = tellg();
	char *p = str->str() + l;
	char c, oldc = 0;
	while (*p) {
		c = *p++;
		if (c == '\t') c = ' ';
		if (c != ' ' || oldc != ' ') line += c;
		oldc = c;
	}
}
*/

/////////////////////////////////////////////////////////////////////////////

int KeyCheck();
TScrobj *_pStation = NULL;

#define SFM_FILE_VERSION  "SFM EDIT FILE VER1.0"
#define RPP_FILE_VERSION  "ROUTE INFO FILE VER1.0"
#define RII_FILE_VERSION  "ROUTE INDICATOR INFO FILE VER1.0"

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CFileBuffer {
	char *m_pFileBuffer;
	long m_lReadPtr;
	long m_lSize;
public:
	CFileBuffer() {}
	~CFileBuffer() {
		delete [] m_pFileBuffer;
	}
	BOOL Load( LPCTSTR pFileName );
	BOOL ReadString( CString &str );
	BOOL ReadString( char *buffer, short size );
};

/////////////////////////////////////////////////////////////////////////////

long crc32tab[] = {                       /* CRC polynomial 0xedb88320 */
  0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
  0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
  0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
  0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
  0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
  0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
  0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
  0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
  0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
  0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
  0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
  0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
  0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
  0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
  0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
  0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
  0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
  0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
  0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
  0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
  0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
  0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
  0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
  0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
  0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
  0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
  0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
  0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
  0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
  0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
  0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
  0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
  0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
  0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
  0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
  0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
  0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
  0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
  0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
  0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
  0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
  0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
  0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
  0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
  0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
  0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
  0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
  0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
  0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
  0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
  0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
  0x2d02ef8dL
};

#define CRC32(c,crc) (crc32tab[((int)(crc) ^ (c)) & 0xff] ^ (((crc) >> 8) & 0x00FFFFFFl))

//#define PROTECT

long _CRC = 0;

BOOL CFileBuffer::Load( LPCTSTR pFileName ) {
	FILE *pFile = fopen( pFileName, "rb" );
	if (!pFile) return 1;
	m_lSize = _filelength( fileno( pFile ) );
	m_pFileBuffer = new char[m_lSize];
	fread( m_pFileBuffer, m_lSize, 1, pFile );
	fclose( pFile );
	m_lReadPtr = 0L;


	long crc = 0L;
	for (long i=0; i<m_lSize; i++) {
		char d = m_pFileBuffer[i];
        crc = CRC32( d, crc );
	}
	_CRC = crc;
#ifdef PROTECT
	return (crc != 0xbbdb7298);
#else
	return 0;
#endif
}

BOOL CFileBuffer::ReadString( CString &str ) {
	char buffer[2048];
	char c;
	short dest = 0;
	while ( m_lReadPtr < m_lSize ) {
		c = m_pFileBuffer[ m_lReadPtr++ ];
		if (dest && c == '\r') {
			if ( m_pFileBuffer[ m_lReadPtr ] == '\n')
				m_lReadPtr++;
			break;
		}
		else {
			buffer[dest++] = c;
		}
	}
	buffer[dest] = 0;
	if (dest) str = buffer;
	return dest;
}

BOOL CFileBuffer::ReadString( char *buffer, short size ) 
{
	char c;
	short dest = 0;

	memset (buffer, NULL, size);
	while ( m_lReadPtr < m_lSize ) 
	{
		c = m_pFileBuffer[ m_lReadPtr++ ];
		if (dest && c == '\r') 
		{
			if ( m_pFileBuffer[ m_lReadPtr ] == '\n')
				m_lReadPtr++;
			break;
		}
		else {
			buffer[dest++] = c;
		}
	}
	buffer[dest] = 0;
	return dest;
}


extern float VIEW_XRATE;
extern float VIEW_YRATE;
extern int   REVERSE;
extern int   STATIONUPDN;
extern int _nSFMViewXSize;
extern int _nSFMViewYSize;
extern int _nSFMTimerX;
extern int _nSFMTimerY;
extern int _nDotPerCell;

// SFM파일에서 각 정보를 추출하여 지정변수에 저장한다.

short CLoader::LoadFileInfo( CObArray &mTrackList, LPCTSTR pFileName)
{
	CLSMApp *pApp = (CLSMApp *)AfxGetApp();
	CFileBuffer ar;
	if (ar.Load( pFileName )) return 1;

	CBmpTrack *pTrack;

	CString str;
	char bf[2048];
	int i;

	ar.ReadString( bf, 2048 );
    for (i=0; bf[i]; i++)
	{
		bf[i] &= 0x7f;
	}
	str = bf;

	if( strcmp((char*)(LPCTSTR)str, SFM_FILE_VERSION) )	
	{
		AfxMessageBox((LPCTSTR)"버젼 오류\"*.SVI\" 입니다.");
		return 1;
	}
	int count = mTrackList.GetSize();
	for (i=0; i<count; i++ ) {
		TScrobj *obj = (TScrobj*)mTrackList.GetAt( i );
		if (obj) delete obj;
	}
	mTrackList.RemoveAll();

	CString		Name; 			
	CPoint		NamePos;
	UINT		Jeulyun;
	int			a , b, c, d, e, f, g, h, blockade;		
	CPoint		p1, p2 ,p3;
	UINT nodetype;					
	pTrack = NULL;
	char * cap;

	while ( ar.ReadString(bf, 2048) ) 
	{
		for (i=0; bf[i]; i++)
		{
			bf[i] &= 0x7f;
		}
		str = bf;

		CParser is( (char*)(LPCTSTR)str, "\t\r\n ,()" );
		cap = is.gettokenl();

		// 전체 Screen 사이즈
		if(! strcmp(cap, "#[SIZE]")) 
		{    
			ar.ReadString(bf, 2048);
			for (i=0; bf[i]; i++)
			{
				bf[i] &= 0x7f;
			}
			str = bf;

			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );			
			a = atoi(is1.gettokenl());			
			b = atoi(is1.gettokenl());
			_nSFMViewXSize = a; // X좌표 사이즈
			_nSFMViewYSize = b; // Y좌표 사이즈
		}
		else if(! strcmp(cap, "#[TIMER]")) 
		{ 
			ar.ReadString(bf, 2048);
			for (i=0; bf[i]; i++)
			{
				bf[i] &= 0x7f;
			}
			str = bf;

			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );			
			p1.x = atoi(is1.gettokenl());			
			p1.y = atoi(is1.gettokenl());
			//LptoDp( p1 );
			_nSFMTimerX = p1.x;
			_nSFMTimerY = p1.y;
		}
		// Screen 좌표 배율
		else if(! strcmp(cap, "#[HEADER]"))	
		{ 
			ar.ReadString(bf, 2048);
			for (i=0; bf[i]; i++)
			{
				bf[i] &= 0x7f;
			}
			str = bf;

			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );			
			a = atoi(is1.gettokenl());			
			b = atoi(is1.gettokenl());
			c = atoi(is1.gettokenl());
			_nDotPerCell = (UINT)a;
			STATIONUPDN = c;

			pApp->SetStationUpDnDirection(STATIONUPDN);

			// REVERSE = b;
			if(_nDotPerCell == 10)	{			
				VIEW_XRATE = 0.5f; // X좌표 단위(SFM파일 위치정보 * 0.5)
				VIEW_YRATE = 1.0f; // Y좌표 단위(SFM파일 위치정보 * 1)
			}
			else if(_nDotPerCell == 8 )	{
				VIEW_XRATE = 0.4f;
				VIEW_YRATE = 0.8f;
			}			
		}

        // Track 정보
		else if(! strcmp(cap, "#[TRACK]")) {
			if(pTrack)	{
				pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 
				//pTrack->ConvertPos();
				mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );
			}
			pTrack = new CBmpTrack();			
//			pTrack->Update=1;
			pTrack->m_bIsFileOpenPos=1;						// 1 : file load position  , 0 : des position
			pTrack->Init();		

			ar.ReadString(bf, 2048);
			for (i=0; bf[i]; i++)
			{
				bf[i] &= 0x7f;
			}
			str = bf;

			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );
			char *s = is1.gettokenl();							
if (strcmp(s, "W25B26AT.2") == NULL)
int debug=1;
			pTrack->SetName( s );
			p1.x = atoi(is1.gettokenl());
			p1.y = atoi(is1.gettokenl());				
			s = is1.gettokenl();
			if ( s && (*s == 'N') ) {
				p2.x = atoi(is1.gettokenl());
				p2.y = atoi(is1.gettokenl());				
				s = is1.gettokenl();
			}
			else {
				CPoint NonePoint(10000,0);
				p2 = NonePoint;
			}
			pTrack->SetNamePos(p1, p2);
			p1.x = atoi( s );
			p1.y = atoi(is1.gettokenl());
			p2.x = atoi(is1.gettokenl());
			p2.y = atoi(is1.gettokenl());				
			p3.x = atoi(is1.gettokenl());
			p3.y = atoi(is1.gettokenl());				
			pTrack->SetCNRPnt(p1,p2,p3);

			Jeulyun = atoi(is1.gettokenl());			
			a  = atoi(is1.gettokenl());
			if(a) pTrack->SetDoubleSlip();
			
			s = is1.gettokenl();
			if ( s == NULL || *s == NULL || s[0] < '1')
			{
				pTrack->m_sPointName[0] =  0;
			}
			else
				memcpy (pTrack->m_sPointName, s, sizeof(s));

			s = is1.gettokenl();
			if ( s == NULL || *s == NULL || s[0] < '1')
			{
				pTrack->m_sPointName2[0] =  0;
			}
			else
				memcpy (pTrack->m_sPointName2, s, sizeof(s));

			s = is1.gettokenl();
			if ( s == NULL || *s == NULL || s[0] < '1')
			{
				pTrack->m_strOverlapRoute = "";
			}
			else
			{
				pTrack->m_strOverlapRoute = s;
			}
//////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
// 			for ( int j = 0; j < 3; j++)
// 			{
// 				s = is1.gettokenl();
// 				if ( s == NULL || *s == NULL || s[0] < '1')
// 				{
// 					pTrack->m_sDestinationTrackUS[j][0] =  0;
// 				}
// 				else
// 					memcpy (pTrack->m_sDestinationTrackUS[j], s, sizeof(s));
// 				
// 				s = is1.gettokenl();
// 				if ( s == NULL || *s == NULL || s[0] < '1')
// 				{
// 					pTrack->m_sReferencePointUSName[j][0] =  0;
// 				}
// 				else
// 					memcpy (pTrack->m_sReferencePointUSName[j], s, sizeof(s));
// 			}
// 			
// 			for ( int i = 0; i < 3; i++)
// 			{
// 				s = is1.gettokenl();
// 				if ( s == NULL || *s == NULL || s[0] < '1')
// 				{
// 					pTrack->m_sDestinationTrackDS[i][0] =  0;
// 				}
// 				else
// 					memcpy (pTrack->m_sDestinationTrackDS[i], s, sizeof(s));
// 				
// 				s = is1.gettokenl();
// 				if ( s == NULL || *s == NULL || s[0] < '1')
// 				{
// 					pTrack->m_sReferencePointDSName[i][0] =  0;
// 				}
// 				else
// 					memcpy (pTrack->m_sReferencePointDSName[i], s, sizeof(s));
// 			}
///////////////////////////////////////////////////////////////////////////////
		}
		
		// 기타 버튼 정보
		else if(! strcmp(cap,"#[ETCOBJ]") ) 
		{
				if(pTrack)	{
					pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 			
					mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );		
				}
				pTrack = NULL;

// sName nX nY nType nid sName2 nWidth nOnID cOnColor nFlashID cFlashColor 

				ar.ReadString(bf, 2048);
				for (i=0; bf[i]; i++)
				{
					bf[i] &= 0x7f;
				}
				str = bf;

				CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );
				Name = is1.gettokenl();							
				p1.x = atoi(is1.gettokenl());
				p1.y = atoi(is1.gettokenl());				
				a = atoi(is1.gettokenl());				// type
				if (a==33)
					int debug = 1;

				b = atoi(is1.gettokenl());				// id				
				CString Name2 = is1.gettokenl();		//  인접역명
				c = atoi(is1.gettokenl());				// width
				d = atoi(is1.gettokenl());				// height

				if ((!strcmp((LPCTSTR)Name,"KULAURA") || !strcmp((LPCTSTR)Name,"AZAMPUR")) && a == 1)
				{
					REVERSE = 0;
				}
				WORD nOnID = 0, nOnColor = 0, nFlashID = 0, nFlashColor = 0;
				if (a==33)
				{
				}
				else if ( a == 36 && Name.Find("AX") == 0 )
				{
					int iWhereIsAt = Name.Find('@');
					int iLengthOfName = Name.GetLength();
					CString strBlockName = Name.Right(iLengthOfName - iWhereIsAt - 1);
					CString strAxleName = Name.Left(iWhereIsAt);

					if( b == 268 || b == 269 || b == 283 || b == 285 )
					{
						nOnID = 1;
					}

					if ( strBlockName.Find('&') > 0)
					{
						pApp->SetAxleInformation(strBlockName.Left(strBlockName.Find('&')), strAxleName);
						pApp->SetAxleInformation(strBlockName.Right(strBlockName.GetLength()-strBlockName.Find('&')-1), strAxleName);
					}
					else
					{
						pApp->SetAxleInformation(strBlockName, strAxleName);
					}
				}
				else if ((a > 19) || (a==13) || (a==14) || (a==2) || (a==3)) {		// For LAMP
					CString token;
					token = is1.gettokenl();
					nOnID = atoi( token );
					token = is1.gettokenl();
					char c = token.GetAt(0);
					nOnColor = MY_RED;
					if (c == 'R') nOnColor = MY_RED;
					else if (c == 'G') nOnColor = MY_GREEN;
					else if (b == 268 || b == 269 || b == 283 || b == 285) nOnColor = c-48;
					token = is1.gettokenl();
					nFlashID = atoi( token );
					token = is1.gettokenl();
					c = token.GetAt(0);
					if (c == 'R') nFlashColor = MY_RED;
					else if (c == 'G') nFlashColor = MY_GREEN;

					if ( a == 29 && (Name.Find("LC") == 0 || Name.Find('/') == 1 ) )
					{
						int iLCNum = 0;

						if ( b == 253 )
							iLCNum = 1;
						else if ( b == 270 )
							iLCNum = 2;
						else if ( b == 278 )
							iLCNum = 3;
						else if ( b == 280 )
							iLCNum = 4;
						else if ( b == 305 )
							iLCNum = 5;
						
						pApp->SetLCInformation(iLCNum, Name);
					}
					else if ( a == 28 && (Name.Find("EPK") == 1 || Name.Find("EPK") == 0) )
					{
						pApp->SetEPKInformation();
					}
				}
				else if ( a == 11 ) { // LDK, Block Signal Fail
					nOnID = 0;
					nFlashID = 0;
					nOnColor = 0;
					nFlashColor = MY_RED;
				}
				else if ( a == 1 )
				{
					pApp->SetStationName(Name);
				}

				CBmpEtcObj	*etc = new CBmpEtcObj(p1,(char*)(LPCTSTR)Name,(char*)(LPCTSTR)Name2,a,b,c,d,
							nOnID, nOnColor, nFlashID, nFlashColor);

				mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)etc);				
		}
		
		
		else if(! strcmp(cap,"[CELL]") ) {
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}											
			p1.x =  atoi(is.gettokenl());
			p1.y =  atoi(is.gettokenl());
			p2.x =  atoi(is.gettokenl());
			p2.y =  atoi(is.gettokenl());				
			nodetype=  atoi(is.gettokenl());
			pTrack->AddPos(p1,p2,nodetype);
		}
		
		// Signal 정보
		else if(! strcmp(cap,"[SIGNAL]") ) 
		{					
			if(! pTrack) 
			{
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}
												
			Name = is.gettokenl();			// name
			p1.x = atoi(is.gettokenl());	// signal point
			p1.y = atoi(is.gettokenl());	// 
			p2.x = atoi(is.gettokenl());	// signal name point
			p2.y = atoi(is.gettokenl());	//							
			p3.x = atoi(is.gettokenl());	// button name point
			p3.y = atoi(is.gettokenl());	//
			h = atoi(is.gettokenl());		// name draw check(1/button, 2/signal) bit
			a = atoi(is.gettokenl());		// signal type(0 - 8)
			b = atoi(is.gettokenl());		// lamp type
			c = atoi(is.gettokenl());		// HasLoop
			d = atoi(is.gettokenl());		// top
			e = atoi(is.gettokenl());		// right
			f = atoi(is.gettokenl());		// yudo
			g = atoi(is.gettokenl());		// hasRoute
			blockade = atoi(is.gettokenl());	// blockade type(only signal type 7 or 8)
			if(Name == "NONE") 
				Name.Empty();

			if ( a >= 5 && a <= 8 )
			{
				pApp->SetBlockInformation(Name);
			}

			pTrack->SetSignal(p2, p1, (char*)(LPCTSTR)Name, p3, h, a , b, (UINT)c, d, e,(UINT)f, g, blockade);
//			if(a < 5)
//			{
//				pTrack->SetSignal(p2, p1, (char*)(LPCTSTR)Name, p3, h, a , b, (UINT)c, d, e,(UINT)f, g, blockade);
//			}
//			else
//			{
//				pTrack->SetBlock(p2, p1, (char*)(LPCTSTR)Name, p3, h, a , b, (UINT)c, d, e,(UINT)f, g, blockade);
//			}
		}
		else if(! strcmp(cap,"[SWITCH]") ) {					
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}								
			a = b = 0;
			Name = is.gettokenl();							
			p1.x = atoi(is.gettokenl());
			p1.y = atoi(is.gettokenl());					
			p2.x = atoi(is.gettokenl());
			p2.y = atoi(is.gettokenl());
			char *p = is.gettokenl();		// 0/General Switch, 1/Handpoint Key Switch.
			if ( p ) a = atoi( p );
			if ( a == 1 && Name.Find("X") < 0 && Name.Find("B") < 0)
			{
				pApp->SetHPKInformation();
			}
			p = is.gettokenl();				// 0/individual switch, 1/double switch.
			if ( p ) b = atoi( p );		
			pTrack->SetSwitch(p1,(char*)(LPCTSTR)Name,p2, (BOOL)(a), (BOOL)(b));
		}
		else if(! strcmp(cap,"[BUTTON]") ){					
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}								
			Name = is.gettokenl();
if (Name == "SDG5.G")
int debug=1;
			p1.x = atoi(is.gettokenl());
			p1.y = atoi(is.gettokenl());
			a = atoi(is.gettokenl());
			char *p = is.gettokenl();
			int nButtonType = atoi(p);
			pTrack->SetButton( p1, (char*)(LPCTSTR)Name, a, nButtonType );
		}			
		else if ( *cap && (*cap != ';') ) {
			CString strR = cap;
			int hipp = strR.Find('-');
			if( hipp >= 0) {
				//CString strSig = strR.Left( hipp );
				//CString strBut = strR.Right( (strR.GetLength() - (hipp+1)) );
				//hipp = strBut.Find('.');
				//if ( hipp >= 0 ) strBut = strBut.Left( hipp );
				//strR = strSig;
				//strR += '-';
				//strR += strBut;
				if(pTrack)	{
					pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 			
					mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );		
				}
				pTrack = NULL;
				//CPoint *point;
				//int   x;
				//int   y;
				//char *s;
				//CRouteLine* pRoute = new CRouteLine( strR );
				//while( s = is.gettokenl(), *s ) { 
				//	x = atoi( s );
				//	s = is.gettokenl();
				//	if ( !*s ) break;
				//	else {
				//		y = atoi( s );
				//		point = new CPoint( x, y );
				//		if ( point ) {
				//			LptoDp( *point );
				//			pRoute->AddPoint( point );
				//		}
				//	}
				//} 
				//mRoutePosList.SetAtGrow( mRoutePosList.GetSize(), (CObject*)pRoute);
			}
		}
	}
	
	pApp->SetFlagOfSyncWithLES();

	if(pTrack)	{
		pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 
		//pTrack->ConvertPos();
		mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );
	}
	FindJoinTrack( mTrackList );	
	return 0;
}

short CLoader::LoadRouteInfo( CObArray &mRoutePosList, LPCTSTR pFileName) 
{
	CFileBuffer ar;
	if (ar.Load( pFileName )) 
		return 1;

	char * cap;
	CString str;
	char bf[2048];
	int i;

	ar.ReadString(bf, 2048);
	for (i=0; bf[i]; i++)
	{
		bf[i] &= 0x7f;
	}
	str = bf;

	if( strcmp((char*)(LPCTSTR)str, RPP_FILE_VERSION) )	
	{
		AfxMessageBox((LPCTSTR)"버젼 오류\"*.PRL\" 입니다.");
		return 1;
	}

	int count = mRoutePosList.GetSize();
	for (i=0; i<count; i++ ) 
	{
		CRouteLine *pRoute = (CRouteLine*)mRoutePosList.GetAt( i );
		if (pRoute) 
			delete pRoute;
	}
	mRoutePosList.RemoveAll();

	while ( ar.ReadString(bf, 2048) ) 
	{
		for (i=0; bf[i]; i++)
		{
			bf[i] &= 0x7f;
		}
		str = bf;

		CParser is( (char*)(LPCTSTR)str, "\t\r\n ,[]" );
		cap = is.gettokenl();		

		if ( *cap && (*cap != ';') ) 
		{
			CString strR = cap;
			int hipp = strR.Find(':');

			if( hipp >= 0) 
			{
				/*
				CString strSig = strR.Left( hipp );
				CString strBut = strR.Right( (strR.GetLength() - (hipp+1)) );
				hipp = strBut.Find('.');

				if ( hipp >= 0 ) 
					strBut = strBut.Left( hipp );
				
				strR = strSig;
				strR += ':';
				strR += strBut;
				*/

				CRouteLine* pRoute = new CRouteLine( strR );

				CPoint *point;
				int   x;
				int   y;
				char *s;
				BOOL iLineOverlap = FALSE;
				CString strBufS;

				while( s = is.gettokenl(), *s ) 
				{ 
					strBufS = s;
					if ( strBufS == '~' )
					{
						iLineOverlap = TRUE;
						s = is.gettokenl();
					}

					if ( iLineOverlap == FALSE ) //진로구간
					{
						x = atoi( s );
						s = is.gettokenl();

						if ( !*s ) 
							break;
						else 
						{
							y = atoi( s );
							point = new CPoint( x, y );
							if ( point ) 
							{
								LptoDp( *point );
								pRoute->AddPoint( point );
							}
						}
					}
					else // 오버랩 구간
					{
						x = atoi( s );
						s = is.gettokenl();
						
						if ( !*s ) 
							break;
						else 
						{
							y = atoi( s );
							point = new CPoint( x, y );
							if ( point ) 
							{
								LptoDp( *point );
								pRoute->Add2ndPoint( point );
							}
						}
					}
				}
				
				mRoutePosList.SetAtGrow( mRoutePosList.GetSize(), (CObject*)pRoute);

			}
		}
	}
	return 0;
}

short CLoader::LoadRouteIndicatorInfo( RICondition &RouteIndicatorInfo, LPCTSTR pFileName) 
{
	CFileBuffer ar;
	if (ar.Load( pFileName )) 
		return 1;

	char * cap;
	CString str;
	char bf[2048];
	int i = 0;

	ar.ReadString(bf, 2048);
	str = bf;

	if( strcmp((char*)(LPCTSTR)str, RII_FILE_VERSION) )	
	{
		AfxMessageBox((LPCTSTR)"DATA 오류\"*.RII\" 입니다.");
		return 1;
	}

	while ( ar.ReadString(bf, 2048) ) 
	{
		str = bf;

		CParser is( (char*)(LPCTSTR)str, "\t\r\n ,():[]" );
		cap = is.gettokenl();

		char * token;
		CString strBuf;
		if ( *cap && (*cap != ';') ) 
		{
			RouteIndicatorInfo.SigName[i] = cap;
			int iNonSigConditionCount = 0;
			int iSigConditionCount = 0;
			int iPointConditionCount = 0;

			RouteIndicatorInfo.SigAspect[i] = is.gettokenl();

			while( token = is.gettokenl(), *token ) 
			{
				strBuf = token;

				if ( strBuf.Find('M') > 0 || strBuf.Find('H') > 0 || strBuf.Find('D') > 0 || strBuf.Find('C') > 0 || strBuf.Find('S') > 0 || strBuf.Find('A') > 0 )
				{
					if ( strBuf.Find('!') == 0)
					{
						RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[iNonSigConditionCount++] = strBuf.Right(strBuf.GetLength()-1);
					}
					else
					{
						RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[iSigConditionCount++] = strBuf;
					}
				}
				else if ( strBuf.Find('N') > 0 || strBuf.Find('R') > 0 )
				{
					RouteIndicatorInfo.AspectCondition[i].PointCondition[iPointConditionCount++] = strBuf;
				}
			}
			i++;
		}
	}
	return 0;
}

void CLoader::FindJoinTrack( CObArray &TrackList )
{
	int no, node=0;
	CObArray &mTrackList = TrackList;
	CBmpTrack *t;
	TScrobj * obj;
	CBmpTrack *track = NULL;

	int count = mTrackList.GetSize();
	for (int i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );			
		if( obj->m_cObjectType == (BYTE)OBJ_TRACK )	{
			track = (CBmpTrack*)obj;
			for( no = 0 ; no<3 ; no++)	{
				t = TrackFind( mTrackList, track,no+1,node);
				if(t)
					track->SetJoinTrack(no,t,node);	
			}
		}
	}		

	//  좌표 변화 
	count = mTrackList.GetSize();
	for (i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );			
			obj->ConvertPos();
	}

}


int strcmpTrackName(const char *s1, const char *s2)
{
	CString t1, t2;

	t1 = s1;
	t2 = s2;
	int ispariod = t1.Find('.');
	if (ispariod >= 0) t1.SetAt(ispariod, 0x00);
	ispariod = t2.Find('.');
	if (ispariod >= 0) t2.SetAt(ispariod, 0x00);
	return ( t1 != t2 );
}

// Track T 의 n(C,N,R) 방향에 접속된 T를 찾음.
// node 는 접속된 T의 CNR
CBmpTrack *CLoader::TrackFind( CObArray &TrackList, CBmpTrack* T, int n, int& node) {
	CObArray &mTrackList = TrackList;
	CBmpTrack *track = NULL;
	TScrobj * obj;
	CPoint p;
	switch (n) {
	case 1: p = T->C; break;
	case 2: p = T->N; break;
	case 3: p = T->R; break;
	}
	node = 0;

	int count = mTrackList.GetSize();
	for (int i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );			
		if( obj->m_cObjectType == (BYTE)OBJ_TRACK )	{
			track = (CBmpTrack*)obj;		
			if (T != track) {
				if (track->C == p) node = 1;
				else if (track->N == p) node = 2;
				else if (track->R == p) node = 3;
				if (node) {	
					if(!strcmpTrackName(T->mName,track->mName))
						return track;				
					node = 0;				
				}
			}
		}
	}
	return NULL;
}
