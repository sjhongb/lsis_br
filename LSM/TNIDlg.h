// TNIDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTNIDlg dialog

class CTNIDlg : public CDialog
{
// Construction
public:
	CTNIDlg(CWnd* pParent = NULL);   // standard constructor

	CPoint m_Position;
    CString m_strTrackName;
// Dialog Data
	//{{AFX_DATA(CTNIDlg)
	enum { IDD = IDD_DIALOG_TNI };
	CString	m_strText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTNIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTNIDlg)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnButtonDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
