// Displaydlg.cpp : implementation file
//

#include "stdafx.h"
#include "LSM.h"
#include "Displaydlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDisplaydlg dialog


CDisplaydlg::CDisplaydlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDisplaydlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDisplaydlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDisplaydlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDisplaydlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDisplaydlg, CDialog)
	//{{AFX_MSG_MAP(CDisplaydlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDisplaydlg message handlers


