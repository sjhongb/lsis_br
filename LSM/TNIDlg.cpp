// TNIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LSM.h"
#include "TNIDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTNIDlg dialog


CTNIDlg::CTNIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTNIDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTNIDlg)
	m_strText = _T("");
	//}}AFX_DATA_INIT
}


void CTNIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTNIDlg)
	DDX_Text(pDX, IDC_EDIT_TNI, m_strText);
	DDV_MaxChars(pDX, m_strText, 5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTNIDlg, CDialog)
	//{{AFX_MSG_MAP(CTNIDlg)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTNIDlg message handlers

void GlovalShowWindow( CWnd *pWnd, int x, int y, int wx, int wy );
int CTNIDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	m_Position.x += 20;
	m_Position.y += 40;

	GlovalShowWindow( this, m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
//	MoveWindow( m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
//	ShowWindow( SW_SHOW );

    CString str = m_strTrackName;
    str += " ������ȣ";
    SetWindowText( str );

	return 0;
}

void CTNIDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	m_strText = _T("");
    UpdateData( FALSE );	
}
