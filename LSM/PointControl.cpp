// PointControl.cpp : implementation file
//

#include "stdafx.h"
#include "PointControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPointControl dialog


CPointControl::CPointControl(CWnd* pParent /*=NULL*/)
	: CDialog(CPointControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPointControl)
	m_nSeletType = -1;
	m_strText = _T("");
	m_MoveText = _T("");
	m_BlockText = _T("");
	//}}AFX_DATA_INIT
}


void CPointControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPointControl)
	DDX_Radio(pDX, IDC_RADIO_POINT_MOVE, m_nSeletType);
	DDX_Text(pDX, IDC_STATIC_TEXT3, m_strText);
	DDX_Text(pDX, IDC_RADIO_POINT_MOVE, m_MoveText);
	DDX_Text(pDX, IDC_RADIO_POINT_BLOCK, m_BlockText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPointControl, CDialog)
	//{{AFX_MSG_MAP(CPointControl)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPointControl message handlers

int CPointControl::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::DoModal();
}

int CPointControl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	m_Position.y += 100;
	if(m_Position.x > 640)
	{
		m_Position.x -= 250;
	}
	
	//    MoveWindow( m_Position.x, m_Position.y, lpCreateStruct->cx, lpCreateStruct->cy );
    ShowWindow( SW_SHOW );
	
	return 0;
}

BOOL CPointControl::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
