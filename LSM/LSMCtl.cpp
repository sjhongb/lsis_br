//===============================================================
//====                     LSMCtl.cpp                       ====
//===============================================================
// 파  일  명 : LSMCtl.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMCtrl OLE control 클래스에서 수행하는 화면 내 
//              각종 제어 부분을 관리한다.            
//
//===============================================================


#include "stdafx.h"
#include "LSM.h"
#include "LSMCtl.h"
#include "Loader.h"
#include "LSMPpg.h"
#include "confirmdlg.h"
#include "ObjectInfoDlg.h"
#include "../include/Scrobj.h"
#include "../include/JTime.h"
#include "../include/BMPINFO.H"
#include "../include/romarea.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CLSMCtrl, COleControl)

extern int _UseSmallText;
extern RECT  _WndRect;
extern int   REVERSE;
CString m_strStationName;
short m_iDispBG;
/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CLSMCtrl, COleControl)
    //{{AFX_MSG_MAP(CLSMCtrl)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOVE()
	ON_WM_SIZE()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_MENUSELECT()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
	ON_COMMAND_RANGE( 0x1000, 0x1100, OnDestSelected )
	ON_MESSAGE(WM_UNINITMENUPOPUP, OnUninitMenuPopup)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CLSMCtrl, COleControl)
    //{{AFX_DISPATCH_MAP(CLSMCtrl)
	DISP_FUNCTION(CLSMCtrl, "LoadFileInfo", LoadFileInfo, VT_I2, VTS_BSTR)
	DISP_FUNCTION(CLSMCtrl, "FindIndex", FindIndex, VT_I2, VTS_BSTR)
	DISP_FUNCTION(CLSMCtrl, "SetTNI", SetTNI, VT_EMPTY, VTS_I2 VTS_BSTR)
	DISP_FUNCTION(CLSMCtrl, "SearchByName", SearchByName, VT_I2, VTS_I2 VTS_BSTR)
	DISP_FUNCTION(CLSMCtrl, "SetButtonRoute", SetButtonRoute, VT_EMPTY, VTS_BSTR VTS_I2)
	DISP_FUNCTION(CLSMCtrl, "AssignScrInfo", AssignScrInfo, VT_I4, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "ScreenUpdate", ScreenUpdate, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "OperateSignalOn", OperateSignalOn, VT_I2, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "OperateBlockOn", OperateBlockOn, VT_I2, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "OperateSignalOff", OperateSignalOff, VT_I2, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "OperateSwitch", OperateSwitch, VT_I2, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "OnRoadMarking", OnRoadMarking, VT_I2, VTS_I2)
	DISP_FUNCTION(CLSMCtrl, "GetLSMSize", GetLSMSize, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "SetLSMOption", SetLSMOption, VT_EMPTY, VTS_I2)
	DISP_FUNCTION(CLSMCtrl, "OlePostMessage", OlePostMessage, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CLSMCtrl, "GetMenuStatus", GetMenuStatus, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CLSMCtrl, "DisplayClock", DisplayClock, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(CLSMCtrl, "DisplayBG", DisplayBG, VT_EMPTY, VTS_I2)
	DISP_STOCKPROP_CAPTION()
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CLSMCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)

END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CLSMCtrl, COleControl)
	//{{AFX_EVENT_MAP(CLSMCtrl)
	EVENT_CUSTOM("Operate", FireOperate, VTS_I2  VTS_I2)
	EVENT_CUSTOM("OleSendMessage", FireOleSendMessage, VTS_I4  VTS_I4  VTS_I4)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!

BEGIN_PROPPAGEIDS(CLSMCtrl, 1)
	PROPPAGEID(CLSMPropPage::guid)
END_PROPPAGEIDS(CLSMCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

	IMPLEMENT_OLECREATE_EX(CLSMCtrl, "LSM.LSMCtrl.1",
	0xc681cec6, 0x99e6, 0x11d3, 0xaa, 0x95, 0, 0xc0, 0x6c, 0, 0x3c, 0x3f)

/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CLSMCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DLSM =
		{ 0x4152f442, 0x9a90, 0x11d3, { 0xaa, 0x95, 0, 0xc0, 0x6c, 0, 0x3c, 0x3f } };
const IID BASED_CODE IID_DLSMEvents =
		{ 0x4152f443, 0x9a90, 0x11d3, { 0xaa, 0x95, 0, 0xc0, 0x6c, 0, 0x3c, 0x3f } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwLSMOleMisc =
	OLEMISC_SIMPLEFRAME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CLSMCtrl, IDS_LSM, _dwLSMOleMisc)


/////////////////////////////////////////////////////////////////////////////

#define OLE_LSM_TIMER_ID	99

EScrInfo EtcScrInfo[256];
DataHeaderType   *pDataHead = NULL;

/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::CLSMCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CLSMCtrl

BOOL CLSMCtrl::CLSMCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_LSM,
			IDB_LSM,
			afxRegApartmentThreading,
			_dwLSMOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// Licensing strings

static const TCHAR BASED_CODE _szLicFileName[] = _T("LSM.lic");

static const WCHAR BASED_CODE _szLicString[] =	L"Copyright (c) 1999 ";


/////////////////////////////////////////////////////////////////////////////
// SFM File View Size
extern int _nSFMViewXSize;
extern int _nSFMViewYSize;
extern int _nSFMTimerX;
extern int _nSFMTimerY;


/////////////////////////////////////////////////////////////////////////////

typedef	union {
	long lData;
	struct {
		short ndl;
		short ndh;
	};
} lParamType;

/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::CLSMCtrlFactory::VerifyUserLicense -
// Checks for existence of a user license

BOOL CLSMCtrl::CLSMCtrlFactory::VerifyUserLicense()
{
	return AfxVerifyLicFile(AfxGetInstanceHandle(), _szLicFileName,	_szLicString);
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::CLSMCtrlFactory::GetLicenseKey -
// Returns a runtime licensing key

BOOL CLSMCtrl::CLSMCtrlFactory::GetLicenseKey(DWORD dwReserved,
	BSTR FAR* pbstrKey)
{
	if (pbstrKey == NULL)
		return FALSE;

	*pbstrKey = SysAllocString(_szLicString);
	return (*pbstrKey != NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::CLSMCtrl - Constructor

CLSMCtrl::CLSMCtrl()
{
	m_bFirst = TRUE;
    m_pDC = NULL;
	InitializeIIDs(&IID_DLSM, &IID_DLSMEvents);

	EnableSimpleFrame();

	// TODO: Initialize your control's instance data here.
	m_pRouteLine = NULL;
	m_pLastSignal = NULL;
	m_nLastRouteofs = NULL;
	m_pPressObj = NULL;
	m_pScrInfo = NULL;
	m_nTTBID = 0;
	m_nTimerHandle = 0;
	m_bMenuStatus = FALSE;

	m_nMaxScrInfo = 0;
	m_nSfmRecSize = 0;
	m_cOption = 0x00;
	m_cConfig = 0x00;
	m_bDispSaveTime = FALSE;

	m_bComFail = FALSE;
	m_bSysOptLOCK = FALSE;
	m_bOptLOCK = FALSE;
	m_bPasOut = FALSE;
	m_bStation = FALSE;
	m_bSysDownctl = FALSE;
	m_bLOCK = FALSE;

	m_WndRect.top = 0;
	m_WndRect.left = 0;
	m_WndRect.bottom = 0;
	m_WndRect.right = 0;

	m_CompanyName = "LG Screen Module";
    m_pMsg = NULL;
	m_bDisplayClock = TRUE;
	m_iDispBG = 0;
	memset(m_pScrMem, 0, sizeof(m_pScrMem));

}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::~CLSMCtrl - Destructor

CLSMCtrl::~CLSMCtrl()
{
	// TODO: Cleanup your control's instance data here.
	PurgeDoc();
}

/////////////////////////////////////////////////////////////////////////////

BOOL isAlarm( int nID )
{
	BOOL bAlarm = FALSE;
	switch ( nID ) {
	case BtnPas:			// event
		bAlarm = TRUE;
		break;
	default:
		break;
	}
	return bAlarm;
}

/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::OnDraw - Drawing function
void KeyCheck3();

extern int _nDotPerCell;

const char *_pDispDateFormat = "%4.4d년 %2d / % 2d일";
const char *_pDispTimeFormat = "%2.2d:%2.2d:%2.2d";

void CLSMCtrl::OnDraw(
			CDC* pDC, const CRect& rcBounds, const CRect& rcInvalid)
{
	if ( m_cOption & OPTION_NODRAW ) return;
    m_pDC = pDC;

	CDC* pDrawDC = pDC;

	CRect client = rcBounds;
	CRect rect = client;

	int nSaveDC = pDC->SaveDC();

    if ( m_DC.m_hDC == NULL ) {
		if ( m_DC.CreateCompatibleDC(pDC)) {  // memory device 생성
			if ( m_Bitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height())) { // Bitmap 초기화
			    pDrawDC = &m_DC;
			    m_DC.OffsetViewportOrg(-rect.left,-rect.top); // 좌표 초기화

                m_DC.SelectObject( &m_Bitmap );
			    m_DC.SetBrushOrg(rect.left % 8,rect.top % 8);
			    m_DC.IntersectClipRect(client);
		    }
	    }
    }
    else {
	    pDrawDC = &m_DC;
    }

	CBrush brush;
	if (!brush.CreateSolidBrush(RGB(0,0,0))) return; //배경색 설정
	brush.UnrealizeObject();
	pDrawDC->FillRect(client, &brush);

	CFont* pOldFont=NULL;
	SymbolInfo = SymbolInfo10;
	
	DataHeaderType *Head = (DataHeaderType *)&m_pScrMem[0];
	int count = m_ScrObject.GetSize();
	for (int i=0; i<count; i++ ) 
	{
		TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
		if (obj) 
		{
			CString str;
			obj->m_strUserID = m_strUserID;
/////////////////////////////////////////////////////////////////
//			for (int index = 0; index < 5; index++) 
//			{
//				str = Head->UserID[ index ];
//				if ( str != "" ) obj->m_strUserID += str;
//			}
/////////////////////////////////////////////////////////////////
//							2013.01.28  UserID관련 구조변경.
			if (obj->m_strUserID == "")
			{
				obj->m_strUserID = "NO USER";
			}
			obj->Draw( pDrawDC );
			
			// 2006.03.01  MMCR  digit route indicator 표시 오류로 삽입....
			CBmpTrack *pTrack = (CBmpTrack *)obj;
 			CString strBufferTrackName = pTrack->mName;
// 			if ( (strchr(pTrack->mName, 'T')  != NULL || strBufferTrackName.Find("SDG") >= 0 || strBufferTrackName.Find("GOODS") >= 0) && (pTrack->m_nID > 0 && pTrack->m_nID < 64) )
			// 20150225 sjhong - MYN역의 신호기 10에서 내는 MAIN진로의 경우 SDG가 From궤도이므로 처리가 되지않는다. 따라서 From궤도가 SDG, GOODS인 경우도 포함시킨다.
			if ( ((strchr(pTrack->mName, 'T')  != NULL) || (strBufferTrackName.Find("SDG") >= 0) || (strBufferTrackName.Find("GOODS") >= 0)) && (pTrack->m_nID > 0 && pTrack->m_nID < /*64*/128) && memcmp(pTrack->mName, "AX_RST", sizeof(pTrack->mName) != 0))
			{
				for (char sigindex=0; sigindex<(char)pTrack->mSignalCount; sigindex++)
				{
					if ( pTrack->pSignal[sigindex] )
					{
						ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pTrack->pSignal[sigindex]->m_pData;
						if (pSignalInfo->DESTTRACK && 0x7f)
						{
							CBmpTrack *pSearchTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, (pSignalInfo->DESTTRACK & 0x7f) );
							if (pSearchTrack && pSearchTrack->mButtonCount)
							{
								CString strButton;
								if (pSearchTrack->pSwitch != NULL)
								{
									ScrInfoSwitch *pSearchSwitchInfo = (ScrInfoSwitch*)pSearchTrack->pSwitch->m_pData;
									if (pSearchSwitchInfo->KR_M)
									{
										strButton = pSearchTrack->pButton[1]->mName;
									}
								}
								char n = strButton.Find( '.' );

								if (n<=0 && pSearchTrack->pButton[0])
								{
									//CBmpButton *pButton = pSearchTrack->pButton[0];
									strButton = pSearchTrack->pButton[0]->mName;
									char n = strButton.Find( '.' );
									if (n>0)
									{
// 										pTrack->pSignal[sigindex]->m_sDigit = strButton.Right(1);
									}
								}
								else if (n>0)
								{
//									pTrack->pSignal[sigindex]->m_sDigit = strButton.Right(1);
								}
							}
						}
						pTrack->pSignal[sigindex]->m_sDigit = CheckRouteIndicatorCondition(pTrack->pSignal[sigindex]->mName);
					}
				}
			}
			
		}
	}

	if( pOldFont) {
		pDrawDC->SelectObject(pOldFont);		
    }

	OnRouteLineDraw( pDrawDC );

	CLSMApp* pApp = (CLSMApp*)AfxGetApp();

// 경고메시지
	if ( (m_bComFail && pApp->m_iMySystemNo != 3 ) || Head->UnitState.bSysRun == 0 ) {
		CFont font;
		int sz = _nDotPerCell;
		font.CreateFont(sz*2,sz,0,0,FW_BLACK,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,_T("Arial"));
		CFont* pOldFont = pDrawDC->SelectObject( &font );
		pDrawDC->SetTextColor( RGB( 0, 0, 0 ) );	// RED
		pDrawDC->SetBkColor( RGB( 255, 255, 128 ) );
		CString msg = " Would You give 'RUN' treatment? Please!";
		int oldmode = pDrawDC->SetBkMode( OPAQUE );
		CRect rect( _nSFMViewXSize/2-215, 200, _nSFMViewXSize/2+215, 224 );
		CPen pen( PS_SOLID, 2, RGB(255,255,255));
        CPen *oldpen = pDrawDC->SelectObject(&pen);
		CBrush brush( RGB(255,255,128) );
        CBrush *oldbrush = pDrawDC->SelectObject(&brush);
		pDrawDC->Rectangle(rect);
		pDrawDC->DrawText( msg, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER );
		pDrawDC->SelectObject(pOldFont);
		pDrawDC->SetBkMode( oldmode );
		pDrawDC->SelectObject( oldpen );
		pDrawDC->SelectObject( oldbrush );
	}

	if (pDrawDC != pDC) 
	{
		pDC->SetViewportOrg(0,0);
		pDC->SetWindowOrg(0,0);
		pDC->SetMapMode(MM_TEXT);
		m_DC.SetViewportOrg(0,0);
		m_DC.SetWindowOrg(0,0);
		m_DC.SetMapMode(MM_TEXT);
		pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&m_DC,0,0,SRCCOPY);
	}


	pDC->RestoreDC(nSaveDC);
	KeyCheck3();

}								 

void CLSMCtrl::OnRouteLineDraw(CDC *pDrawDC)
{
	if ( pDrawDC == NULL ) return;
	if ( m_pRouteLine ) 
	{
		CRouteLine* pRouteLine = m_pRouteLine;

		int width = _nDotPerCell/3;
		if ( !width ) width = 1;
		CPen  pen( PS_DOT, 3, RGB(0,255,0) );
		CPen* poldpen = pDrawDC->SelectObject( &pen );

		pRouteLine->DrawLine( pDrawDC );
		
		CPen pen2( PS_DOT,3,RGB(255,255,0));
		pDrawDC->SelectObject( &pen2 );
		pRouteLine->Draw2ndLine( pDrawDC );

		pDrawDC->SelectObject( poldpen );
	}

	if ( m_pRouteLine && m_RouteLineType==1)
	{
		CString RouteLineStr = "45:53(M)";
		CRouteLine* pRouteLineAdd = SearchRoutePos( RouteLineStr );

		if ( pRouteLineAdd ) 
		{
			int width = _nDotPerCell/3;
			if ( !width ) width = 1;
			CPen pen( PS_DOT, 3, RGB(255,255,0) );

			CPen* poldpen = pDrawDC->SelectObject( &pen );

			pRouteLineAdd->DrawLine( pDrawDC );

			pDrawDC->SelectObject( poldpen );
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::DoPropExchange - Persistence support

void CLSMCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::OnResetState - Reset control to default state
extern CDC *_MyImageDC;
extern CDC *_MyImageDC1;
extern CDC *_MyImageDC2;
extern CDC *_MyTimer;

void CLSMCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange
	if (_MyImageDC) {
		delete	_MyImageDC;
		_MyImageDC = NULL;
	}
	if (_MyImageDC1) {
		delete	_MyImageDC1;
		_MyImageDC1 = NULL;
	}
	if (_MyImageDC2) {
		delete	_MyImageDC2;
		_MyImageDC2 = NULL;
	}
	if (_MyTimer) {
		delete	_MyTimer;
		_MyTimer = NULL;
	}
	if (!_MyImageDC) {
		CBitmap Symbols;
		CBitmap LGlogo;
		UINT bitmap =IDB_SYMINFO7 ;
		BOOL t = Symbols.LoadBitmap( bitmap );
		if (t) {
			CDC *pDC = GetDC();
			_MyImageDC = new CDC;
			_MyImageDC->CreateCompatibleDC( pDC );
			_MyImageDC->SelectObject( &Symbols );
			Symbols.DeleteObject();
			ReleaseDC( pDC );
		}
	}
	if (!_MyImageDC1) {
		CBitmap Symbols;
		BOOL t = Symbols.LoadBitmap( IDB_SYMINFO1 );
		if (t) {
			CDC *pDC = GetDC();
			_MyImageDC1 = new CDC;
			_MyImageDC1->CreateCompatibleDC( pDC );
			_MyImageDC1->SelectObject( &Symbols );
			Symbols.DeleteObject();
			ReleaseDC( pDC );
		}
	}
	if (!_MyImageDC2) {
		CBitmap Symbols;
		BOOL t = Symbols.LoadBitmap( IDB_BITMAPAKASDG );
		if (t) {
			CDC *pDC = GetDC();
			_MyImageDC2 = new CDC;
			_MyImageDC2->CreateCompatibleDC( pDC );
			_MyImageDC2->SelectObject( &Symbols );
			Symbols.DeleteObject();
			ReleaseDC( pDC );
		}
	}
	m_nTimerHandle = SetTimer( OLE_LSM_TIMER_ID, 300, NULL );
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl::AboutBox - Display an "About" box to the user

void CLSMCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_LSM);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CLSMCtrl message handlers

void CLSMCtrl::Serialize(CArchive& ar) 
{
	if (ar.IsStoring())
	{	// storing code
	}
	else
	{	// loading code
	}
}


short CLSMCtrl::LoadFileInfo(LPCTSTR pFileName) 
{
	PurgeDoc();

	int b = CLoader::LoadFileInfo( m_ScrObject, pFileName );
	if ( !b )		// .RPP file open (-> .PRL로 변환)
	{
		char *p = (char*)pFileName;
		int len = strlen(p);
		p[len-3] = 'R';	
		p[len-2] = 'I';
		p[len-1] = 'I';
		CLoader::LoadRouteIndicatorInfo( m_RouteIndicatorInfo, p );

		p[len-3] = 'P';	
		p[len-2] = 'R';
		p[len-1] = 'L';
		CLoader::LoadRouteInfo( m_RoutePosObj, p );
	}
	OnResetState();
	return b;

}
void CLSMCtrl::PurgeDoc()
{
	int i;
	for (i=0; i<m_ScrObject.GetSize(); i++ ) {
		TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
		if (obj) delete obj;
	}
	m_ScrObject.RemoveAll();

	m_pRouteLine = NULL;

	for (i=0; i<m_RoutePosObj.GetSize(); i++ ) {
		CRouteLine *pRoute = (CRouteLine*)m_RoutePosObj.GetAt( i );
		if (pRoute) delete pRoute;
	}
	m_RoutePosObj.RemoveAll();

	if ( m_pScrInfo ) {
		delete [] m_pScrInfo;
		m_pScrInfo = NULL;
	}
}

short CLSMCtrl::FindIndex(LPCTSTR szName) 
{
	// TODO: Add your dispatch handler code here

	return 0;
}

void CLSMCtrl::OnDestroy() 
{
	COleControl::OnDestroy();
	// TODO: Add your message handler code here
	if ( m_nTimerHandle ) {
		KillTimer( m_nTimerHandle );	
	}
	if (_MyImageDC) {
		delete	_MyImageDC;
		_MyImageDC = NULL;
	}
	if (_MyImageDC1) {
		delete	_MyImageDC1;
		_MyImageDC1 = NULL;
	}
	if (_MyImageDC2) {
		delete	_MyImageDC2;
		_MyImageDC2 = NULL;
	}

}

extern BOOL _BlinkDuty;
extern BOOL _Blink;
void CLSMCtrl::OnTimer(UINT nIDEvent) 
{
	if ( nIDEvent == m_nTimerHandle) { // Activated Timer의 return value는 nIDEvent
		_BlinkDuty = !_BlinkDuty;
        _Blink += 40;
		if (_Blink == 200) _Blink = 40;

		if ( m_bDisplayClock == TRUE ) {
			m_Clock.ShowWindow(SW_SHOW);
		} else {
			m_Clock.ShowWindow(SW_HIDE);
		}
		InvalidateControl();
	}
	COleControl::OnTimer(nIDEvent);
}

TScrobj* CLSMCtrl::SearchPtrByName( char cType, char *szName )
{
	for (int i=0; i<m_ScrObject.GetSize(); i++ ) {
		TScrobj *pObj = (TScrobj *)m_ScrObject.GetAt( i );
		if ( pObj->m_cObjectType == OBJ_LAMP ) {
			if ( cType == OBJ_LAMP && !strcmp( szName, pObj->mName) ) {
				return pObj;
			}
		}
		else {
			CBmpTrack *pTrack = (CBmpTrack *)pObj;
			TScrobj *pRetObj = pTrack->FindObject( cType, szName );
			if (pRetObj) return pRetObj;
		}
	}
	return NULL;
}

TScrobj* CLSMCtrl::SearchblkByName( char *szName )
{
	for (int i=0; i<m_ScrObject.GetSize(); i++ ) {
		CBmpTrack *pTrack = (CBmpTrack *)m_ScrObject.GetAt( i );
		TScrobj *obj = pTrack->FindObject( OBJ_SIGNAL, szName );
		if ( obj != NULL ) {
			if ( !strcmp( szName, obj->mName) ) {
				return obj;
			}
		}
	}
	return NULL;
}

TScrobj* CLSMCtrl::SearchPtrByID( char cType, short nID )
{
	for (int i=0; i<m_ScrObject.GetSize(); i++ ) {
		CBmpTrack *pTrack = (CBmpTrack *)m_ScrObject.GetAt( i );
		TScrobj *obj = pTrack->FindObject( cType, nID );
		if (obj) return obj;
	}
	return NULL;
}

void CLSMCtrl::SearchPtrsByID( char cType, short nID, TScrobj* objs[5] )
{
	BYTE index = 0;
	for (int i=0; i<m_ScrObject.GetSize(); i++ ) {
		CBmpTrack *pTrack = (CBmpTrack *)m_ScrObject.GetAt( i );
		TScrobj *obj = pTrack->FindObject( cType, nID );
		if (obj)
		{
			objs[index++] = obj;
			if (cType != OBJ_TRACK)
				return;
		}
	}
}

void CLSMCtrl::SetTNI(short nID, LPCTSTR szNo)
{
	short iReturnValue;
	iReturnValue = 0;
	
	if ( nID == 0 )
	{
		m_strUserID  = szNo; // Set USER ID

		if( m_strUserID.GetLength() > 8 )
			m_strUserID = m_strUserID.Left(8);
	}
}

short CLSMCtrl::SearchByName(short cType, LPCTSTR szName)
{

	return -1;
}

void CLSMCtrl::SetButtonRoute(LPCTSTR pInfo, short nRoute)
{
	BYTE *p = (BYTE*)pInfo;
	BYTE nCount = *p++;
	short nID;
	for (BYTE n = 0; n < nCount; n++) {
		nID = *p++;
		CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_BUTTON, nID );
		if (pButton) {
		}
	}
}

TScrobj *CLSMCtrl::SetScrObject( TScrobj *pObj, CScrInfo *pInfo ) 
{ 
	if ( pObj && pInfo ) 
	{
		short offset = pInfo->m_nMemOffset; // nByteLoc

		if (offset >= 0 && offset < MAX_MSGBLOCK_SIZE) 
		{	// VMEMSIZE = MAX_MSGBLOCK_SIZE = 2048
			pObj->m_pData = &m_pScrMem[ offset ]; // BYTE m_pScrMem[ VMEMSIZE ];
		}
		
		pObj->m_nID = pInfo->m_nID;
	}
	return pObj;
}

extern long _CRC;

long CLSMCtrl::AssignScrInfo(long pInfoBuffer)
{
	CScrInfo *LPInfo = *(CScrInfo**)pInfoBuffer;
	short nCount = *(short*)(LPInfo + 4);
	void *pTable = *(void**)(LPInfo + 8);
	if ( pTable ) {
		m_Engine.LoadTable( pTable ); // CEipEmu m_Engine;
		if ( CreateScrInfo() ) {
			m_Engine.m_pVMEM = m_pScrMem;
			m_pMsg = m_pScrMem + m_Engine.m_TableBase.InfoGeneralIO.nStart + m_Engine.m_TableBase.InfoGeneralIO.nSize;
		}
		else {
			m_strErrorMsg = "< LSIS > Fail maked LSM scr info--";
			return (long)(LPCTSTR)(m_strErrorMsg);
		}
	}
	return RunAssignScrInfo( nCount, LPInfo );
}

long CLSMCtrl::RunAssignScrInfo( short nCount, CScrInfo *pScrInfo )
{
#ifdef _LSM_DEBUG
	CString str;
	str.Format( "RunAssignScrInfo\n nCount = %d", nCount );
	::MessageBox( NULL, str, "LSM DEBUG", IDOK );
#endif
	long lParam = 0;
	CString strMsg = "";

	CScrInfo *pInfo;
	TScrobj *pObj;
	BYTE num=0;

	// SYSTEM BUTTON Assign
	for (short i=0; i<m_ScrObject.GetSize(); i++ ) {
		TScrobj *obj = (TScrobj *)m_ScrObject.GetAt( i );
		if ( obj ) {
			if ((obj->m_cObjectType == OBJ_SYSBUTTON) || (obj->m_cObjectType == OBJ_SYSPUSH))
			{
				obj->m_pData = &m_pScrMem[0];

				if (pDataHead == NULL)
					pDataHead = (DataHeaderType*)obj->m_pData;

			}
			if ((obj->m_cObjectType == OBJ_ETC) 
				&& ((((CBmpEtcObj*)obj)->m_nType == 5) 
				|| (((CBmpEtcObj*)obj)->m_nType == 2) || (((CBmpEtcObj*)obj)->m_nType == 3) )) {
				obj->m_pData = &m_pScrMem[0];
			} 
		}
	}
	// Obj(Track, Signal, Switch, Button) Assign
	for ( i=0; i<nCount; i++) {
		pInfo = &pScrInfo[i];
		char tchar = 0;
		switch ( pInfo->m_nType ) {
		case SCRINFO_TRACK :
			tchar = OBJ_TRACK;
			break;
		case SCRINFO_SIGNAL :
			tchar = OBJ_SIGNAL;
			break;
		case SCRINFO_SWITCH :
			tchar = OBJ_SWITCH;
			break;
		case SCRINFO_LAMP :
			tchar = OBJ_LAMP;
			break;
		case SCRINFO_BUTTON :

			if (pInfo->m_strName == "TTB") {

				m_nTTBID = pInfo->m_nID;
				continue;
			}
			else
				tchar = OBJ_BUTTON;
			break;
		}
		if (tchar) 
		{
			if (tchar == OBJ_SIGNAL || tchar == OBJ_TRACK  || tchar == OBJ_SWITCH) 
			{
				EtcScrInfo[num].str = pInfo->m_strName;
				EtcScrInfo[num].type = tchar;
				EtcScrInfo[num++].m_pData = &m_pScrMem[ pInfo->m_nMemOffset ]; 			
			}
			BOOL bSkip = FALSE;
			TScrobj *pResObj = NULL;
			char bf[21];

			strncpy(bf, (char*)(LPCTSTR)pInfo->m_strName, 20);

			bf[20] = 0;
			if (tchar == OBJ_SWITCH) 
			{
				if (bf[0] == 'V') {
					bSkip = TRUE;
				}
				else {
					for (int ii=0; ii<m_ScrObject.GetSize(); ii++ ) {
						CBmpTrack *pTrack = (CBmpTrack *)m_ScrObject.GetAt( ii );
						pObj = pTrack->FindObject( tchar, bf );
						if (pObj) pResObj = SetScrObject( pObj, pInfo );
					}
				}
			}
			else {
				pObj = SearchPtrByName( tchar, bf );

				if ( pObj && tchar == OBJ_LAMP ) {

					pInfo->m_strName += ".";
					pInfo->m_strName += pObj->m_szDrawName;

				}
				if (!pObj && tchar == OBJ_TRACK) {
					strcat( bf,".");
					char *p = bf + strlen(bf);
					for (int i=1; i<=10; i++) {
						*p = '0'+i;
						*(p+1) = 0;
						pObj = SearchPtrByName( tchar, bf );
						if (pObj) pResObj = SetScrObject( pObj, pInfo );
					}
				}
				else if (pObj) {
					pResObj = SetScrObject( pObj, pInfo );
				}
			}
			if (!pResObj && !bSkip ) {
				strMsg += "(";
				CString strScr;
				strScr.Format( "Name:%s ID:%d Type:%d", bf/*pInfo->m_strName*/, pInfo->m_nID, pInfo->m_nType );
				strMsg += strScr;
				strMsg += ")";
		} 
		}
	}
	if ( strMsg != "" ) {
		m_strErrorMsg  = "< LGIS >";
		m_strErrorMsg += strMsg;
		lParam = (long)(LPCTSTR)(m_strErrorMsg);
	}
	return lParam;
}

void CLSMCtrl::ScreenUpdate(long pVMEM) 
{
	memcpy( m_pScrMem, (void*)pVMEM, MAX_MSGBLOCK_SIZE );

	DataHeaderType *pHead  = (DataHeaderType*)&m_pScrMem[0];
	SystemStatusType *pVar = &(pHead->nSysVar);
	UnitStatusType *pUnit = &(pHead->UnitState);



	if ( pUnit->bSysRun) 
	{
		m_bSysOptLOCK = FALSE;
		m_bOptLOCK = FALSE;
		m_bPasOut = FALSE;
		if(m_bFirst)
		{
			CRect trect;
						
			trect.left = 10;
			trect.top = 10;
			trect.right = 50;
			trect.bottom = 40;

			// System run일 경우 시간을 표시하게 된다..
			m_Clock.Create(NULL,NULL,trect,this,0);
			m_Clock.ShowWindow(SW_SHOW);
			m_Clock.Start(IDB_BITMAPP,IDB_BITMAPB,IDB_BITMAPS,0,m_pScrMem);
		}

		if (pVar->bN1)
		{	
			m_bSysOptLOCK = TRUE;
			m_bOptLOCK = TRUE;
			m_bPasOut = TRUE;
		}
		if(pVar->bStation)
		{
			m_bStation = TRUE;
		}
		else
		{
			m_bStation = FALSE;
		}

		m_bFirst =  FALSE;		
	}
	else 
	{
		m_bSysOptLOCK = TRUE;
		m_bOptLOCK = TRUE;

		if(!pVar->bN1)
		{
			m_bSysDownctl = TRUE;
			m_bPasOut = FALSE;
		}
		else
		{
			m_bPasOut = TRUE;
		}

	}
	BOOL bComFail = (pUnit->bComFail) ? TRUE : FALSE;
	if ( m_bComFail != bComFail ) {
		// SYSTEM BUTTON SET
		TScrobj *obj;
		for (short i=0; i<m_ScrObject.GetSize(); i++ ) {
			obj = (TScrobj *)m_ScrObject.GetAt( i );
			if ( obj && ((obj->m_cObjectType == OBJ_SYSBUTTON) || (obj->m_cObjectType == OBJ_SYSPUSH)) ) {
				if ( bComFail ) obj->m_cConfig |= OPTION_COMFAIL;
				else obj->m_cConfig &= ~OPTION_COMFAIL;
			}
		}
	}
	m_bComFail = bComFail;
}

short CLSMCtrl::OperateBlockOn(long ID) 
{
	lParamType ld;
	ld.lData = ID;
	CPoint point;
	char BlkName[4];

	if ( ld.ndl == 1 ) {
		BlkName[0] = 'B' ; BlkName[1] = '1' ; BlkName[2] = '\0' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 2 ) {
		BlkName[0] = 'B' ; BlkName[1] = '2' ; BlkName[2] = '\0' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 3 ) {
		BlkName[0] = 'B' ; BlkName[1] = '3' ; BlkName[2] = '\0' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 4 ) {
		BlkName[0] = 'B' ; BlkName[1] = '4' ; BlkName[2] = '\0' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 11 ) {
		BlkName[0] = 'B' ; BlkName[1] = '1' ; BlkName[2] = '1' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 12 ) {
		BlkName[0] = 'B' ; BlkName[1] = '1' ; BlkName[2] = '2' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 13 ) {
		BlkName[0] = 'B' ; BlkName[1] = '1' ; BlkName[2] = '3' ; BlkName[3] = '\0';
	} else if ( ld.ndl == 14 ) {
		BlkName[0] = 'B' ; BlkName[1] = '1' ; BlkName[2] = '4' ; BlkName[3] = '\0';
	} else return 0 ;

	CBmpSignal *pSignal = (CBmpSignal *)SearchblkByName( BlkName );
	if ( pSignal == NULL ) {
		return 0;
	}
	if ( pSignal ) 
	{
		if ( ld.ndh ) // Block 취급 (1 = CA, 2 = LCR, 3 = CBB)
		{
			if (ld.ndh == 1)      // CA
			{
				pSignal->m_bCAClicked = 1;
				point = pSignal->mPos;
				DoSignalEvent( pSignal, point );
			}
			else if (ld.ndh == 2) // LCR
			{
				pSignal->m_bLCRClicked = 1;
				point = pSignal->mPos;
				DoSignalEvent( pSignal, point );
			}
			else if (ld.ndh == 3) // CBB
			{
				pSignal->m_bCBBClicked = 1;
				point = pSignal->mPos;
				DoSignalEvent( pSignal, point );
			}
		}
	}
	return 0;
}

short CLSMCtrl::OperateSignalOn(long ID) 
{
	lParamType ld;
	ld.lData = ID;
	CPoint point;

	CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByID( OBJ_SIGNAL, ld.ndl );
	if ( pSignal ) 
	{
		if ( pSignal->m_bOn ) 	// 현시 후 궤도에 의해 정지 및 쇄정 
		{
		}
		else
		{
			pSignal->m_nOnTimer = 0;
			point = pSignal->mPos;
			DoSignalEvent( pSignal, point );
		}
	}
	return 0;
}


short CLSMCtrl::OperateSignalOff(long ID) 
{
	lParamType ld;
	ld.lData = ID;
	CPoint point;

	CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByID( OBJ_SIGNAL, ld.ndl );
	if ( pSignal ) 
	{
		if ( pSignal->m_bOn ) 	// 현시 후 궤도에 의해 정지 및 쇄정 
		{
			pSignal->m_nOnTimer = 0;
			point = pSignal->mPos;
			DoSignalEvent( pSignal, point );
		}
	}
	return 0;	
	

}

short CLSMCtrl::OperateSwitch(long ID) 
{
	lParamType ld;
	ld.lData = ID;
	CPoint point;

	CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, ld.ndl );
	if ( pSwitch ) {
		point = pSwitch->mPos;
		DoSwitchEvent( pSwitch, point );
	}
	
	return 0;
}

short CLSMCtrl::OnRoadMarking(short cmd)
{
	return 0;
}

void CLSMCtrl::GetLSMSize(long pSize) // 호출 무
{
	union {
		long lData;
		struct {
			short dl;
			short dh;
		};
	};

	dl = short( _nSFMViewXSize );
	dh = short( _nSFMViewYSize );

	memcpy((void*)pSize, &lData, sizeof(long));
}


void CLSMCtrl::SetLSMOption(short option) 
{
	union {
		short d;
		struct {
			BYTE dl;
			BYTE dh;
		};
	};

	d = option;

	m_cOption = dl;
	m_cConfig = dh;

    if ( m_cOption & OPTION_SMALLTEXT ) {
        _UseSmallText = 1;
    }

	for (short i=0; i<m_ScrObject.GetSize(); i++ ) {
		TScrobj *obj = (TScrobj *)m_ScrObject.GetAt( i );
		if ( obj ) {
			if ( obj->m_cObjectType == OBJ_TRACK ) 
			{
				((CBmpTrack*)obj)->SetObjectConfig( dh );
			}
			else  
				obj->m_cConfig = m_cConfig;
		}
	}
}

BOOL CLSMCtrl::CreateScrInfo()
{
	short nTrack  = m_Engine.m_strTrack.GetCount();
	short nSignal = m_Engine.m_strSignal.GetCount();
	short nSwitch = m_Engine.m_strSwitch.GetCount();
	short nButton = m_Engine.m_strButton.GetCount();
	short nLamp = m_Engine.m_strLamp.GetCount();
	m_nMaxScrInfo = nTrack + nSignal + nSwitch + nButton + nLamp;
	m_nSfmRecSize = (nTrack * SIZE_INFO_TRACK) + (nSignal * SIZE_INFO_SIGNAL) + 
		            (nSwitch * SIZE_INFO_SWITCH) + (nLamp * SIZE_INFO_LAMP)
					+ sizeof(DataHeaderType);


	if ( m_pScrInfo ) {
		delete [] m_pScrInfo;
	}
	m_pScrInfo = new CScrInfo[ m_nMaxScrInfo ];
	if ( !m_pScrInfo ) return FALSE;
	
	short nByteLoc = 0;
	short nNo = 0;

	POSITION pos;
	CString  *pStr;
	CScrInfo *pInfo;
	
	nByteLoc += sizeof( DataHeaderType );
	nTrack = 0;
	for ( pos = m_Engine.m_strTrack.GetHeadPosition(); pos != NULL; ) {
		pStr = (CString*)m_Engine.m_strTrack.GetNext( pos );
		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nMemOffset = nByteLoc;
		pInfo->m_nType = SCRINFO_TRACK;
		pInfo->m_nID = ++nTrack;
		nByteLoc += SIZE_INFO_TRACK;
		ASSERT( nByteLoc < MAX_MSGBLOCK_SIZE );		
	}
	nSignal = 0;
	for ( pos = m_Engine.m_strSignal.GetHeadPosition(); pos != NULL; ) {
		pStr = (CString*)m_Engine.m_strSignal.GetNext( pos );
		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nMemOffset = nByteLoc;
		pInfo->m_nType = SCRINFO_SIGNAL;
		pInfo->m_nID = ++nSignal;
		nByteLoc += SIZE_INFO_SIGNAL;
		ASSERT( nByteLoc < MAX_MSGBLOCK_SIZE );	
	}
	nSwitch = 0;
	for ( pos = m_Engine.m_strSwitch.GetHeadPosition(); pos != NULL; ) {
		pStr = (CString*)m_Engine.m_strSwitch.GetNext( pos );
		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		nSwitch++;
		pInfo->m_nMemOffset = nByteLoc;
		if ( *pStr == 'V' ) {
			pInfo->m_nMemOffset -= 2;
		}
		pInfo->m_nType = SCRINFO_SWITCH;
		pInfo->m_nID = nSwitch;
		nByteLoc += SIZE_INFO_SWITCH;

		ASSERT( nByteLoc < MAX_MSGBLOCK_SIZE );		
	}
	nButton = 0;
	for ( pos = m_Engine.m_strButton.GetHeadPosition(); pos != NULL; ) {
		pStr = (CString*)m_Engine.m_strButton.GetNext( pos );
		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nMemOffset = 0;			//nByteLoc

		pInfo->m_nType = SCRINFO_BUTTON;
		pInfo->m_nID = ++nButton;
	}
	nLamp = 0;
	for ( pos = m_Engine.m_strLamp.GetHeadPosition(); pos != NULL; ) {
		pStr = (CString*)m_Engine.m_strLamp.GetNext( pos );
		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nMemOffset = nByteLoc;

		pInfo->m_nType = SCRINFO_LAMP;
		pInfo->m_nID = ++nLamp;
		nByteLoc += SIZE_INFO_LAMP;
		ASSERT( nByteLoc < MAX_MSGBLOCK_SIZE );		
	}
	ASSERT( nNo == nMaxScrInfo );

	return TRUE;
}

long CLSMCtrl::OpMsgLoadFileInfo( long sfmNamePtr, long modeNamePtr )
{
	long lParam = 0;

	CString strName;
	LPCTSTR pSfmName  = (char*)sfmNamePtr;
	LPCTSTR pModeName = (char*)modeNamePtr;

	strName  = ".\\";
	strName += pSfmName;
	strName += "\\";
	strName += pSfmName;
	strName += ".SVI";

	if ( LoadFileInfo((LPCTSTR)(strName)) ) 
	{	// Load SFM file 
		m_strErrorMsg  = "Emergency\n \"";
		m_strErrorMsg += strName;
		m_strErrorMsg += "\" File Open Error.";
		PurgeDoc();
		lParam = (long)(LPCTSTR)(m_strErrorMsg);
	}
	else 
	{
		BOOL bOpen = m_Engine.LoadTable( pSfmName );
		if ( bOpen ) {
			if ( CreateScrInfo() ) {
				m_Engine.m_pVMEM = m_pScrMem;
				m_pMsg = m_pScrMem + m_Engine.m_TableBase.InfoGeneralIO.nStart 
					     + m_Engine.m_TableBase.InfoGeneralIO.nSize;

				lParam = RunAssignScrInfo( m_nMaxScrInfo, m_pScrInfo );
			}
			else {
				PurgeDoc();
				m_strErrorMsg = "Emergency!\n Screen information table create fail..";
				lParam = (long)(LPCTSTR)(m_strErrorMsg);
			}
		}
		else {
			PurgeDoc();
			m_strErrorMsg.Format("Emergency!\n Interlocking table file open error (%s).",pSfmName);
			lParam = (long)(LPCTSTR)(m_strErrorMsg);
		}
	}
	return lParam;
}

/////////////////////////////////////////////////////////////////////////////

void CLSMCtrl::DestEnableCheck( BYTE nSigID, CPoint &point )
{
	m_MenuPoint = point;
	m_DestButtons.RemoveAll();
	m_listRouteMenu.Purge();

	BYTE n, nButtonID, nSOldButtonID = 0, nCOldButtonID = 0, nMOldButtonID = 0;
	BYTE nRouteCount = m_Engine.m_pSignalInfoTable[ nSigID ].nRouteCount;
	WORD wRouteNo = m_Engine.m_pSignalInfoTable[ nSigID ].wRouteNo;
	BYTE nSignalType = m_Engine.m_pSignalInfoTable[nSigID].nSignalType & 0xf0;

	BYTE loop, nFindNoused;

	CRect rect;
	GetWindowRect( &rect );
	rect.OffsetRect( point );
	CMenu menu;
	menu.CreatePopupMenu();

	UINT menuID = 0x1000;
	UINT j, index = 0, count=0;
	char szName[10] = "\0";

	CBmpButton **plistButton = new CBmpButton* [nRouteCount];

	CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByID( OBJ_SIGNAL, (short)nSigID );
	if (pSignal) 
	{
		if (m_pLastSignal) 
		{
			m_pLastSignal->m_nPressTimer = 0;	// 마지막 눌러진 신호기의 시간 지움 -> 착점버튼 클리어
		}
		pSignal->m_nPressTimer = 20;			// 6 sec
		m_pLastSignal = pSignal;

		CBmpButton *pButton;

		CString strContRoute;
		int nRouteOfs = 0;
		CMyStrList listM;
		CMyStrList listS;
		CMyStrList listC;
		CMyStrList *pList = NULL;

		CString strCheckRouteNameToAvoidSameName[50];
		for ( int i = 0; i < 50; i++)
		{
			strCheckRouteNameToAvoidSameName[i] = "";
		}

		for (n=0; n<nRouteCount; n++) 
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			nButtonID = pInfo[RT_OFFSET_BUTTON];
			pButton = (CBmpButton *)SearchPtrByID( OBJ_BUTTON, (short)nButtonID );
			CBmpButton **plistButton = new CBmpButton* [nRouteCount];
			CString str = pSignal->m_szDrawName;
			str += ":";
			str += pButton->m_szDrawName;

			nFindNoused=0;
			if (nSignalType == SIGNALTYPE_OUT)
			{
				BYTE *p = &pInfo[RT_OFFSET_SWITCH];
				unsigned short i, nCount = *p++;
				BYTE nTrkID = *p++;			//종착궤도
				BYTE nOverTime = *p++;		//보류쇄정시간

				BYTE nOverSigID = 0;
				BYTE nID, swpos;
				ScrInfoSwitch *pSwitchInfo = NULL;

				for (i=2; i<nCount; i+=2) 
				{
					nID = *p++;
					nOverSigID = *p++;
					swpos = nID & 0x80;
					nID &= 0x7f;

					CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, nID );
					pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
					if (pSwitchInfo->NOUSED == 1)
					{
						nFindNoused = 1;
						break;
					}
				}
			}


			BYTE  nCheck = 0;
			short nRouteStatus = m_Engine.CheckRoute( wRouteNo );
			short nRtEnable = ( nRouteStatus & ROUTECHECK_ENABLE) == 0; //ROUTECHECK_ENABLE=0X2E
			if (nRtEnable == 1 && !(nRouteStatus & ROUTECHECK_ROUTE))
			{
				//2005.10.20 노야파라 Outer home signal 진로 설정 시 반대 편 Home진로와 맞 진로가 되었을 때 이 진로를 Check하여 막아 주지 못해 보완
				// 이것은  Loop line으로 가는 Home 진로 때문에 생기는 것이다.
				if ( nRouteStatus & ROUTECHECK_CONTACT )
				{
					if (nSignalType == SIGNALTYPE_IN)
					{
						BOOL nCheckFind = CheckOverlapSwitch(pInfo);
						if (nCheckFind)
						{
							pButton->m_pSignal = pSignal;
							nCheck = 1;
						}
					}
					else
					{
						pButton->m_pSignal = pSignal;
						nCheck = 1;
					}
				}
				else
				{
					pButton->m_pSignal = pSignal;
					nCheck = 1;
				}
			}
			else 
			{
				pButton->m_pSignal = NULL;
			}

			BYTE nRtStatus = pInfo[ RT_OFFSET_ROUTESTATUS ];
			BYTE nRouteRP = nRtStatus & 0x0f;

			if ( nRtStatus & RT_SHUNTSIGNAL ) 
			{	// Shunt Signal

				if (nRouteRP > 0)
				{
					for (loop=1; loop<nRouteRP; loop++)
					{
						nRtEnable = (m_Engine.CheckRoute( wRouteNo + loop) & ROUTECHECK_ENABLE) == 0; //ROUTECHECK_ENABLE=0X2E
						if (nRtEnable == 1)
						{
							pButton->m_pSignal = pSignal;
							nCheck = 1;
						}
					}
					n += (loop-1);
				}
				else {
					loop = 1;
				}
				
				str += "(S)";

				pList = &listS;
			}
			else if ( nRtStatus & RT_CALLON ) 
			{	// CALLON Signal

				if (nRouteRP > 0)
				{
					for (loop=1; loop<nRouteRP; loop++)
					{
						nRtEnable = (m_Engine.CheckRoute( wRouteNo + loop) & ROUTECHECK_ENABLE) == 0; //ROUTECHECK_ENABLE=0X2E
						if (nRtEnable == 1)
						{
							pButton->m_pSignal = pSignal;
							nCheck = 1;
						}
					}
					n += (loop-1);
				}
				else {
					loop = 1;
				}

				str += "(C)";
				
				if ( nRouteOfs == 0 )
				{
					strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
				}
				else
				{
					int iCountSameName = 0;
					for ( int i = 0; i < nRouteOfs; i++)
					{
						if ( strCheckRouteNameToAvoidSameName[i].Find(str) == 0 )
						{
							iCountSameName++;
						}
					}
					if ( iCountSameName )
					{
						char cAdditionalOverlap = iCountSameName + 'A' -1;
						str.Format( "%s-%c", str, cAdditionalOverlap );
						strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
					}
					else
					{
						strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
					}
				}

				// 2006.10.19 Akhaura역 58신호기의 52 진로 방향 Callon 진로를 막기 위해...
				BYTE nSignalID = pInfo[RT_OFFSET_SIGNAL];
				CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_SIGNAL, (short)nSignalID );
				CString strButtonName = pButton->m_szDrawName;
// 				if ( m_strStationName.Find ( "BYPASS",0 )==0 && strButtonName == "58")
// 				{
// 					if (pDataHead != NULL)
// 					{
// 						SystemStatusType &nSysVar = pDataHead->nSysVar;
// 						if (nSysVar.bMainRoute && str == "52(C)")
// 						{
// 							// 2007.3.3  자카리아 요구로 수정...
// 							str = "";
// 							nCheck = 0;
// 						}
// 					}
// 				}

				pList = &listC;
			}
			else 
			{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
// 				if (nRouteRP > 0)
// 				{
// 					if ( (nSignalType == SIGNALTYPE_OUT) && !nFindNoused )
// 					{
// 						n += nRouteRP;
// 						loop = nRouteRP;
// 						nRouteRP = 0;
// 					}
// 					else
// 					{
// 						for (loop=1; loop<nRouteRP; loop++)
// 						{
// 							nRouteStatus = m_Engine.CheckRoute( nRouteNo + loop); 
// 							nRtEnable = ( nRouteStatus & ROUTECHECK_ENABLE) == 0; //ROUTECHECK_ENABLE=0X2E
// 							if ( nRtEnable == 1 )
// 							{
// 								//2005.10.20 노야파라 Outer home signal 진로 설정 시 반대 편 Home진로와 맞 진로가 되었을 때 이 진로를 Check하여 막아 주지 못해 보완
// 								// 이것은  Loop line으로 가는 Home 진로 때문에 생기는 것이다.
// 								if ( nRouteStatus & ROUTECHECK_CONTACT )
// 								{
// 									if (nSignalType == SIGNALTYPE_IN)
// 									{
// 										BOOL nCheckFind = CheckOverlapSwitch(&m_Engine.m_pInfoMem[*(UINT*)&m_Engine.m_pRouteAddTable[ nRouteNo + loop ]]);	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
// 										if (nCheckFind)
// 										{
// 											pButton->m_pSignal = pSignal;
// 											nCheck = 1;
// 										}
// 									}
// 									else
// 									{
// 										pButton->m_pSignal = pSignal;
// 										nCheck = 1;
// 									}
// 								}
// 								else
// 								{
// 									pButton->m_pSignal = pSignal;
// 									nCheck = 1;
// 								}
// 							}
// 						}
// 						n += (loop-1);
// 					}
// 
// 
// 					// 2006.10.19 Akhaura역 58신호기의 52 진로 방향 Main 진로를 막기 위해...
// 					if ( m_strStationName.Find ( "AKHA",0 )==0 )
// 					{
// 						BYTE nSignalID = pInfo[RT_OFFSET_SIGNAL];
// 						CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_SIGNAL, (short)nSignalID );
// 						CString strButtonName = pButton->m_szDrawName;
// 						if (strButtonName == "52")
// 						{
// 							CBmpTrack *pTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, pInfo[RT_OFFSET_FROM] );
// 							ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pTrack->m_pData;
// 							if (pTrkInfo->INHIBIT)
// 							{
// 								// 2007.3.3  자타리아 요구로 수정...
// 								//str = "";
// 								nCheck = 0;
// 								//pButton->m_pSignal = NULL;
// 							}
// 						}
// 					}
// 				}
// 				else 
// 				{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					loop = 1;

					BYTE nSignalID = pInfo[RT_OFFSET_SIGNAL];
					CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_SIGNAL, (short)nSignalID );
					CString strButtonName = pButton->m_szDrawName;
					strcpy(szName, strButtonName);
					CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByName( OBJ_SIGNAL, szName );
					
// 					// Outer main일 경우 삭제한다...
// 					if (strButtonName.GetAt(0) == 'A')
// 						str = "";
// 					if (pSignal->mHasRoute == TRUE ) 
// 						str = "";

					// 2006.10.19 Akhaura역 58신호기의 52 진로 방향 Main 진로를 막기 위해...
// 					if ( m_strStationName.Find ( "BYPASS",0 )==0 && strButtonName == "58")
// 					{
// 						if (pDataHead != NULL)
// 						{
// 							SystemStatusType &nSysVar = pDataHead->nSysVar;
// 							if (!nSysVar.bMainRoute && str == "52")
// 							{
// 								// 2007.3.3  자카리아 요구로 수정...
// 								//str = "";
// 								nCheck = 0;
// 								//pButton->m_pSignal = NULL;
// 							}
// 
// 						}
// 					}
// 					else if ( m_strStationName.Find("LAKSAM") == 0 && ( strButtonName == "47" || strButtonName == "51" || strButtonName == "57" ))
// 					{
// 						if (pDataHead != NULL)
// 						{
// 							SystemStatusType &nSysVar = pDataHead->nSysVar;
// 							if (!nSysVar.bMainRoute && str == "65")
// 							{
// 								str = "";
// 								nCheck = 0;
// 								// 20130808 ISTART 관련 변경.
// 							}
// 						}
// 					}
// 
// 					// 2006.10.19 Akhaura역 52신호기의 Main 진로를 막기 위해...
// 					if ( m_strStationName.Find ( "AKHA",0 )==0 && strButtonName == "52")
// 					{
// 						CBmpTrack *pTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, pInfo[RT_OFFSET_FROM] );
// 						ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pTrack->m_pData;
// 						if (pTrkInfo->INHIBIT)
// 						{
// 							// 2007.3.3  자카리아 요구로 수정...
// 							//str = "";
// 							nCheck = 0;
// 							//pButton->m_pSignal = NULL;
// 						}
// 					}

// 				}
//////////////////////////////////////////////////////////////////////////////////////////////
// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
					str += "(M)";
					// Outer main일 경우 삭제한다...
					if (strButtonName.GetAt(0) == 'A')
						str = "";
					if (pSignal->mHasRoute == TRUE ) 
						str = "";

					if ( nRouteOfs == 0 )
					{
						strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
					}
					else
					{
						int iCountSameName = 0;
						for ( int i = 0; i < nRouteOfs; i++)
						{
							if ( strCheckRouteNameToAvoidSameName[i].Find(str) == 0 )
							{
								iCountSameName++;
							}
						}
						if ( iCountSameName )
						{
							char cAdditionalOverlap = iCountSameName + 'A' -1;
							str.Format( "%s-%c", str, cAdditionalOverlap );
							strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
						}
						else
						{
							strCheckRouteNameToAvoidSameName[nRouteOfs] = str;
						}
					}
////////////////////////////////////////////////////////////////////////////////////////////
				pList = &listM;
			}
			
			plistButton[count++] = pButton;

			index++;
			CString strRouteOfs;

			if (str != "")
				strRouteOfs.Format( ",%d#%c", nRouteOfs, nCheck ? '1':'0' );
			
			CString str1 = str + strRouteOfs;

			pList->AddTail( str1 );
			wRouteNo += loop;
			nRouteOfs += loop;
		}

		int mNo = 0;
		CString strSep = "=";

		if ( listM.GetCount() > 0 ) 
			mNo = 1;

		m_listRouteMenu = listM;

		if ( listC.GetCount() > 0 ) 
		{
			if ( mNo ) 
			{
				index++;
				m_listRouteMenu.AddTail( strSep );
			}
			mNo = 2;
			m_listRouteMenu += listC;
		}
		if ( listS.GetCount() > 0 ) 
		{
			if ( mNo ) 
			{
				index++;
				m_listRouteMenu.AddTail( strSep );
			}
			mNo = 3;
			m_listRouteMenu += listS;
		}

		for(j=0; j<index; j++) 
		{
			CString strDest = *m_listRouteMenu.GetAtIndex( j );
			if ( strDest == "=" ) {
				menu.InsertMenu( j,MF_SEPARATOR );
				continue;
			}

			int n = strDest.Find( '#' );
			char c = strDest.GetAt( n + 1 );
			n = strDest.Find( ',' );
			strDest = strDest.Left( n );

			if ( pSignal->m_nType == 3 )
			{
				strDest = pSignal->mName;
				strDest = strDest + " -> G";
			}								// 2012.12.26 갈릭 코멘트에 의해 수정.
			
			menu.InsertMenu( j, MFT_STRING | MF_BYPOSITION | MFS_GRAYED, menuID + j, strDest );

			if ( c == '1' ) 
			{
				menu.EnableMenuItem( j, MF_ENABLED | MF_BYPOSITION );
			}
			else 
			{
				menu.EnableMenuItem( j, MF_GRAYED | MF_BYPOSITION);
			}
		}

		for(j=0; j<count; j++) 
		{
			m_DestButtons.SetAtGrow( j, (CObject*)plistButton[j] );
		}
	}

	if (plistButton) 
	{
		delete	plistButton;
		plistButton = NULL;
	}
	m_bAutoMenuEnable = FALSE;
	m_bMenuStatus = TRUE;
	menu.TrackPopupMenu(TPM_LEFTALIGN + TPM_LEFTBUTTON, rect.left+30, rect.top+10, this);
	m_bMenuStatus = FALSE;
}


BOOL CLSMCtrl::CheckOverlapSwitch ( BYTE *pInfo )
{
	BYTE *p = &pInfo[RT_OFFSET_SWITCH];
	unsigned short i, nCount = *p++;
	BYTE nTrkID = *p++;			//종착궤도
	BYTE nOverTime = *p++;		//보류쇄정시간

	BYTE nOverSigID = 0;
	BYTE nID, swpos; 
	BOOL nCheckFind = TRUE;
	ScrInfoSwitch *pSwitchInfo = NULL;

	for (i=2; i<nCount; i+=2) 
	{
		nID = *p++;
		nOverSigID = *p++;
		swpos = nID & 0x80;
		nID &= 0x7f;

		CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, nID );
		pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
		if ( (nOverSigID & SWT_IN_OVERLAP) && pSwitchInfo->ROUTELOCK)
		{
			BYTE swp, swm;
			if (swpos == 0) {
				swp = 1;
				swm = 0;
			}
			else {
				swp = 0;
				swm = 1;
			}

			if (pSwitchInfo->WR_P != swp || pSwitchInfo->WR_M != swm)
			{
				nCheckFind = FALSE;									
			}
		}
	}

	return nCheckFind;
}

CRouteLine* CLSMCtrl::SearchRoutePos(CString &string)
{
	CRouteLine* pRoute;
	int count = m_RoutePosObj.GetSize();

	for(int i=0; i<count; i++) 
	{
		pRoute = (CRouteLine*)m_RoutePosObj.GetAt( i );
		if (pRoute->m_strRoute == string) 
			return pRoute;
	}
	return NULL;
}

CRouteLine* CLSMCtrl::SearchRoutePos(int sigID, int buttonID)
{
	CRouteLine* pRoute;
	int count = m_RoutePosObj.GetSize();

	for(int i=0; i<count; i++) {
		pRoute = (CRouteLine*)m_RoutePosObj.GetAt( i );
		if ((pRoute->m_nSigID == sigID) && (pRoute->m_nButtonID == buttonID)) 
			return pRoute;
	}
	return NULL;
}

void CLSMCtrl::OnDestSelected( UINT nID ) 
{
	typedef struct tagRouteBasPtr 
	{
		WORD	nOfsSignal;
		WORD	nOfsTrack;
		WORD	nOfsRoute;
		WORD	nOfsAspect;
		WORD	nOfsBlockNormal;
		WORD	n3;
		WORD	n4;
		WORD	n5;
	} RouteBasPtr;

	union {
		long lData;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
		struct {
			short dl;
			short dh;
		};
	};
	lData = 0;
	nID -= 0x1000;

    TScrobj *pRetObj;

	CBmpButton *pButton;

	char szName[10] = "\0";

	if ( (m_listRouteMenu.GetCount() > (int)nID) && (0 <= nID) ) 
	{
		CString sigName = m_pLastSignal->mName;
		CString str = sigName;
		CString strRouteName;
		CString strButton = *m_listRouteMenu.GetAtIndex( nID );

		if ( strButton == "=" ) 
		{
			m_listRouteMenu.Purge();
			return;
		}

		int n = strButton.Find( ',' );
		int nRouteOfs = atoi( (char*)(LPCTSTR)strButton + n + 1 );

        int nn = 0, cs=0;
        nn = strButton.Find( '(' );
		if (strButton.GetAt(nn+1) == 'C')
			cs = 1;
		else if (strButton.GetAt(nn+1) == 'S')
			cs = 2;

// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
//		if (nn > 0)
//		    strButton = strButton.Left( nn );
//		else
		    strButton = strButton.Left( n );


		// 2006.10.19 Akhaura역의 58:52(M), 58:52(C) 진로 설정 제한을 위해 삽입함.		
// 		if ( m_strStationName.Find ( "BYPASS",0 )==0 && sigName == "58")
// 		{
// 			if (pDataHead != NULL)
// 			{
// 				SystemStatusType &nSysVar = pDataHead->nSysVar;
// 				if (strButton == "52")
// 				{
// 					if (!nSysVar.bMainRoute)     // main이 설정되어 있지 않음.  Callon만 가능
// 					{
// 						if (cs != 1)
// 							return;
// 					}
// 					else     // main이 설정되어 있음.  main만 가능
// 					{
// 						if (cs == 1)
// 							return;
// 					}
// 				}
// 			}
// 		}
/////////////////////////////////////////////////////////////////////////////		
// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
//		str += ":";
//		str += strButton;
		strRouteName = strButton;

// 		if (nn > 0)
// 		{
// 			if (cs == 1)
// 			    strRouteName += "(C)";
// 			else
// 			    strRouteName += "(S)";
// 		}
// 		else
// 			strRouteName += "(M)";
///////////////////////////////////////////////////////////////////////////////

		BYTE count, nSigID, sCount=0;

		strcpy(szName, sigName);
		CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByName( OBJ_SIGNAL, szName );
		nSigID = (BYTE)pSignal->m_nID;

		BYTE nRouteCount = m_Engine.m_pSignalInfoTable[ nSigID ].nRouteCount;

		if (nRouteCount < 6 || (strRouteName.Find('(') > 0 && strRouteName.Find(')') > 0))		// 20130809 왜 6개이하로 제한했는지 모르겠으나 shunt진로가 6개 이상인 것이 많기에 수정.
			str = strRouteName;
		else
		    str += "(M)";

		WORD wRouteNo = m_Engine.m_pSignalInfoTable[ nSigID ].wRouteNo;


		// 2006.10.19 Akhaura역 58신호기의 52 진로 방향 Main 진로를 막기 위해...
		if ( m_strStationName.Find ( "AKHA",0 )==0 && sigName == "52")
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

			CBmpTrack *pTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, pInfo[RT_OFFSET_FROM] );
			ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pTrack->m_pData;
			if (pTrkInfo->INHIBIT && cs==0 )
			{
				return;
			}
		}


		for(count=0; count<nRouteCount; count++)
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRouteNo+count ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			BYTE nButtonID = pInfo[RT_OFFSET_BUTTON];

			if ( (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON) || (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL) )
				continue;

			CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_BUTTON, (short)nButtonID );
			if (strButton != pButton->m_szDrawName)
				continue;
                 
			sCount += 1;
			BYTE *p = pInfo + RT_OFFSET_SWITCH;
			unsigned short i, nCount = *p++;

			BYTE nTrkID = *p++;			//종착궤도
			BYTE nOverTime = *p++;		//보류쇄정시간

			BYTE nOverSigID = 0;
			BYTE nID, swpos;
			ScrInfoSwitch *pSwitchInfo = NULL;

			for (i=2; i<nCount; i+=2) 
			{
				nID = *p++;
				nOverSigID = *p++;
				swpos = nID & 0x80;
				nID &= 0x7f;

				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, nID );
				pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
				if (pSwitchInfo->NOUSED == 1 && swpos == 0  && sCount>1)
				{
					if ( !(nRouteCount < 6 && cs > 0) )
					{
						str += '-';
						str += (sCount-1)+48;
					}
					break;
				}
			}
			if (pSwitchInfo != NULL)
			{
				if ( (pSwitchInfo->NOUSED == 0 && sCount>2) || (((pSwitchInfo != NULL) && (pSwitchInfo->NOUSED == 1)) && swpos == 0 && sCount>1) )
				{
					break;
				}
			}
		}

		m_pRouteLine = SearchRoutePos( str );
		if (m_pRouteLine==NULL && m_strStationName.Find ( "SHAIS",0 )==0 && strButton=="53")
		{
			str.Replace("53","45");
			m_pRouteLine = SearchRoutePos( str );
			m_RouteLineType = 1;
		}
		// 2006.05.15  실헷의 중간 shunt진로 (strButton=="55","26")  클라우라의 중간 shunt진로 (strButton=="16")를 그린다...
		// 2006.08.16  아카우라 중간 shunt진로 (strButton=="SDG3","SDG4","SDG5","SDG6","SDG7","SDG8","SDG9","SDG10","SDG14", "5","48" szName[0]==3)을 그린다...
		else if (m_pRouteLine==NULL && nRouteCount>=6 && (strButton=="55" || strButton=="26" || strButton=="12" || strButton=="14" || strButton=="16" || strButton=="18" 
			|| strButton=="SDG3" || strButton=="SDG4" || strButton=="SDG5" || strButton=="SDG6" || strButton=="SDG7" || strButton=="SDG8" || strButton=="SDG9" 
			|| strButton=="SDG10" || strButton=="SDG11" || strButton=="SDG12" || strButton=="SDG13" || strButton=="SDG14" || strButton=="SDG15" ||
			strButton=="5" || strButton=="48" || strButton=="61" || szName[0]=='3') )
		{
			str = strRouteName;
			m_pRouteLine = SearchRoutePos( str );
		}
		else
			m_RouteLineType = 0;


        n = strButton.Find( '(' );
		if (n>0) 
			strButton = strButton.Left( n );

		strcpy(szName, strButton);
		pRetObj = SearchPtrByName( OBJ_BUTTON, szName );

		if (pRetObj) 
		{
			pButton = (CBmpButton*)pRetObj;
		}
		strButton = pButton->mName;

		if ( m_pRouteLine ) InvalidateControl();
		strRouteName += "  Route Set ?";		
		CString strtitle = "Signal Control";      
		m_nLastRouteofs = nRouteOfs;
		int symbol = 0;
		if ( CheckConfirm( strRouteName, m_MenuPoint, strtitle, symbol ) ) 
		{
			d1 = (BYTE)(m_pLastSignal->m_nID);
			d2 = nRouteOfs; //d2 = (BYTE)(pButton->m_pTrack->m_nID);
			FireOperate( WS_OPMSG_SIGNALDEST, dl );
		}

		m_pLastSignal->m_nPressTimer = 0;
		m_pRouteLine = NULL;		
	}
	m_listRouteMenu.Purge();
}

char *binfile = "OLE.BIN";

void CLSMCtrl::DoSignalEvent( CBmpSignal *pSignal, CPoint &point ) 
{
	union {
		long lData;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
		struct {
			short dl;
			short dh;
		};
	};
	lData = 0;
	m_MenuPoint = point;
	BYTE *pInfo = NULL;

	CString str = pSignal->mName;
	CString restr;
	CString release;
	ScrInfoSignal *pSigInfo = (ScrInfoSignal*)pSignal->m_pData;
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;

    CString strtitle = "Block Operation";

	BOOL bBlockWithoutAxlecounter = FALSE;

	if (( m_strStationName.Find("CHINKI-ASTANA") == 0 || m_strStationName.Find("BHAIRAP") == 0 ) && ( pSignal->mName == "B3" || pSignal->mName == "B4" ))
		bBlockWithoutAxlecounter = TRUE;
	else if ( m_strStationName.Find("LAKSAM") == 0 && ( pSignal->mName == "B1" || pSignal->mName == "B2" ))
		bBlockWithoutAxlecounter = TRUE;

	if (pSignal->m_bCAClicked && !pBlkInfo->CA) 
	{
		dl = pSignal->m_nID;
		WORD wRoute = m_Engine.m_pSignalInfoTable[ dl ].wRouteNo;


		// TCB에서는 CA제어를 할 수 없다....
		if ((m_Engine.m_pSignalInfoTable[ dl ].nSignalType & 0xf0) & BLOCKTYPE_AUTO_IN) 
		{
			return;
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 		UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ nRoute ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
// 		pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
// 
// 		BYTE nFromTk = pInfo[RT_OFFSET_FROM];
// 		ScrInfoTrack *pFromTk = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nFromTk);
// 		BYTE nToTk = pInfo[RT_OFFSET_TO];
// 		ScrInfoTrack *pToTk = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nToTk);
// 
// 		BYTE ProhibitBlkID = pInfo[ RT_OFFSET_SWITCH+5 ];
// 		ScrInfoBlock *pProhibitBlkInfo = (ScrInfoBlock *)m_Engine.int_get(SignalGet, ProhibitBlkID);
// 
// 		if ( m_strStationName.Find ( "BHAIRAB",0 )==0 && ( str=="B3") )
// 		{
// 			if (pProhibitBlkInfo->CA == TRUE || pProhibitBlkInfo->LCROP == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)  
// 			{
// 				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
// 				return;
// 			}
// 		}
// 		else if ( m_strStationName.Find ( "LAKSAM",0 )==0 && ( str=="B1") )
// 		{
// 			if (pProhibitBlkInfo->CA == TRUE || pProhibitBlkInfo->LCROP == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)  
// 			{
// 				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
// 				return;
// 			}
// 		}
// 		else if ( m_strStationName.Find( "@StandardOf13") > 0 && ( str=="B1" || str=="B3" ))
// 		{
// 			if (pProhibitBlkInfo->CA == TRUE || pProhibitBlkInfo->LCROP == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)  
// 			{
// 				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
// 				return;
// 			}
// 		}
// 		else
// 		{
// 			if (pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)
// 			{
// 				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
// 				return;
// 			}
// 		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



		// 2006.11.16  B3는 test용....
/*		if ( m_strStationName.Find ( "BYPASS",0 )==0 && (str=="B1" || str=="B3") )
		{
			if (pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)  // ASTART가 1일 경우, pFromTK, pToTk점유일 경우 다시 CA제어를 할 수 없다... 2005.09.08
			{
				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
				return;
			}
		}
		else
		{
			if (pProhibitBlkInfo->CA == TRUE || pProhibitBlkInfo->LCROP == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART || !pFromTk->TRACK || !pToTk->TRACK)  // ASTART가 1일 경우, pFromTK, pToTk점유일 경우 다시 CA제어를 할 수 없다... 2005.09.08
			{
				MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
				return;
			}
		}*/

		// 2006.5.23  폐색 통신이 두절되었을 때를 위해 보완 사항인 RBGPR, RBCPR을 check 한다.
		// 이거이 OK인 것은 폐색 안에 열차가 없음이 운영자에 의해 확인된 사항이다...

// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 		if (pBlkInfo->RBGPR || pBlkInfo->RBCPR)
// 		{
// 			str = "Train exists in Block. You must check the Block and Operate the CA'!";
// 			CheckConfirm( str, point, strtitle,13 );
// 			return;
// 		}

// 		if ( pBlkInfo->AXLOCC || bBlockWithoutAxlecounter == TRUE )
// 		{
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
			str = " Do you want to set CA 'ON' ?";
			if ( CheckConfirm( str, point, strtitle,13 ) )
			    FireOperate( WS_OPMSG_CAON, dl );
			    return;
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 		}
// 		else
// 		{
// 			MessageBox( " Occupation of block section is detected by Axle counter!", "BLOCK STATUS");
// 			return;
// 		}
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
	}

	// 2005.09.22 TCB에서 CA 제어를 하여 TGB CA의 Acknowledge를 할 수 있도록 수정 함...
	else if (pSignal->m_bCAClicked && pBlkInfo->CA)
	{
		dl = pSignal->m_nID;
        if ((m_Engine.m_pSignalInfoTable[ dl ].nSignalType & 0xf0) & BLOCKTYPE_AUTO_IN)
		{
			// 2006.5.23  폐색 통신이 두절되었을 때를 위해 보완 사항인 RBGPR, RBCPR을 check 한다.
			// 이거이 OK인 것은 폐색 안에 열차가 없음이 운영자에 의해 확인된 사항이다...
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 			if (pBlkInfo->RBGPR || pBlkInfo->RBCPR)
// 			{
// 				str = "Train exists in Block. You must check the Block and Operate the CA'!";
// 				CheckConfirm( str, point, strtitle,13 );
// 				return;
// 			}
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 

			if ( !pBlkInfo->CAACK )
			{
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 				if ( pBlkInfo->AXLOCC || bBlockWithoutAxlecounter == TRUE )
// 				{
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
					str = "Do you want to set CA 'Acknowledge'?";
					if ( CheckConfirm( str, point, strtitle,13 ) )
						FireOperate( WS_OPMSG_CAACK, dl );
						return;
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 				}
// 				else
// 				{
// 					MessageBox( " Occupation of block section is detected by Axle counter!", "BLOCK STATUS");
// 					return;
// 				}
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
			}
		}
	}


	if (pSignal->m_bLCRClicked && !pBlkInfo->LCR) 
	{
		dl = pSignal->m_nID;

// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 		if (!(pBlkInfo->CA==TRUE))
// 			return;
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 

		if (pSignal->m_nType == 5)
		{
			if (!pBlkInfo->LCRIN)
			{
				MessageBox(" LCR doesn't set now.\n Cannot operate LCG command.", "BLOCK STATUS");
				return;
			}
			else if (!pBlkInfo->CONFIRM)
			{
 				MessageBox(" Block Exit Track(s) is(are) occupied now.\n Cannot operate LCG command.", "BLOCK STATUS");
 				return;
			}
		}
		if (pSignal->m_nType == 6)  // TGB -- pFromTK, pToTk점유일 경우 다시 LCR제어를 할 수 없다... 2005.09.08
		{
			WORD wRoute = m_Engine.m_pSignalInfoTable[ dl ].wRouteNo;
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRoute ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

			BYTE nBlkTrack1 = pInfo[RT_OFFSET_BLKTRACK1];
			ScrInfoTrack *pBlkTrack1 = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nBlkTrack1);
			BYTE nBlkTrack2 = pInfo[RT_OFFSET_BLKTRACK2];
			ScrInfoTrack *pBlkTrack2 = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nBlkTrack2);
			BYTE nBlkTrack3 = pInfo[RT_OFFSET_BLKTRACK3];
			ScrInfoTrack *pBlkTrack3 = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nBlkTrack3);
			BYTE nBlkTrack4 = pInfo[RT_OFFSET_BLKTRACK4];
			ScrInfoTrack *pBlkTrack4 = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nBlkTrack4);
			BYTE nBlkTrack5 = pInfo[RT_OFFSET_BLKTRACK5];
			ScrInfoTrack *pBlkTrack5 = (ScrInfoTrack *)m_Engine.int_get(TrackGet, nBlkTrack5);

			if(!nBlkTrack1)
				pBlkTrack1->TRACK = TRUE;
			if(!nBlkTrack2)
				pBlkTrack2->TRACK = TRUE;
			if(!nBlkTrack3)
				pBlkTrack3->TRACK = TRUE;
			if(!nBlkTrack4)
				pBlkTrack4->TRACK = TRUE;
			if(!nBlkTrack5)
				pBlkTrack5->TRACK = TRUE;

			BOOL bBlkTrackOccupied = !pBlkTrack1->TRACK | !pBlkTrack2->TRACK | !pBlkTrack3->TRACK | !pBlkTrack4->TRACK | !pBlkTrack5->TRACK; 

			if ( m_strStationName.Find( "@StandardOf13") > 0)
			{
				if ( bBlkTrackOccupied )
				{
					if(MessageBox(" Warning!\n Block Entry Track(s) is(are) occupied now.\n Do you want to set Emergency LCR 'ON'?","BLOCK STATUS", MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2) == IDYES)
					{
					}
					else
					{
						return;
					}
				}
			}
			else
			{
				if ( bBlkTrackOccupied )
				{
					MessageBox(" Block Track is occupied! ","BLOCK STATUS");
					return;
				}
			}

			BYTE ProhibitBlkID = pInfo[ RT_OFFSET_PROHIBITBLK ];
			ScrInfoBlock *pProhibitBlkInfo = (ScrInfoBlock *)m_Engine.int_get(SignalGet, ProhibitBlkID);

#if 0
			if ( m_strStationName.Find ( "BHAIRAB",0 )==0 && ( str=="B3") )
			{
				if (pProhibitBlkInfo->LCRIN == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART )  
				{
					MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
					return;
				}
			}
			else 
#endif
			if ( m_strStationName.Find ( "LAKSAM",0 )==0 && ( str=="B1") )
			{
				if (pProhibitBlkInfo->LCRIN == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART )  
				{
					MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
					return;
				}
			}
			else if ( m_strStationName.Find( "@StandardOf13") > 0 && ( str=="B1" || str=="B3" ))
			{
				if (pProhibitBlkInfo->LCRIN == TRUE || pProhibitBlkInfo->RBGPR || pBlkInfo->ASTART )  
				{
					MessageBox(" Block doesn't be admitted! ","BLOCK STATUS");
					return;
				}
			}


// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 			if (!pBlkInfo->CAACK)
// 			{
// 				MessageBox(" Acknowledge don't received ! ","BLOCK STATUS");
// 				return;
// 			}
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 

		}
		if (pBlkInfo->RBGPR || pBlkInfo->RBCPR)
		{
			str = "Train exists in Block. You must check the Block and Operate the LCR'!";
			CheckConfirm( str, point, strtitle,13 );
			return;
		}
		else if ( pBlkInfo->AXLOCC || bBlockWithoutAxlecounter == TRUE )
		{
			pSignal->mOperateLCR = TRUE;
			if ( pSignal->m_nType == 5 )
			{
				str = " Do you want to set LCG 'ON' ?";
			}
			else
			{
				str = " Do you want to set LCR 'ON' ?";
			}
			if ( CheckConfirm( str, point, strtitle,14 ) )
			{
			   pSignal->mOperateLCR = FALSE;
   			   FireOperate( WS_OPMSG_LCROP, dl );
			   return;
			}
			pSignal->mOperateLCR = FALSE;
		}
		else
		{
			if ( m_strStationName.Find( "@StandardOf13") > 0)
			{
				if ( pSignal->m_nType == 5 )
				{
					if(MessageBox(" Warning!\n Occupation of block section is detected by Axle counter.\n Do you want to set Emergency LCG 'ON'?","BLOCK STATUS", MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2) == IDYES)
					{
						str = " Do you want to set LCG 'ON' ?";
						if ( CheckConfirm( str, point, strtitle,14 ) )
						{
							pSignal->mOperateLCR = FALSE;
							FireOperate( WS_OPMSG_LCROP, dl );
							return;
						}
						pSignal->mOperateLCR = FALSE;
					}
				}
				else
				{
					if(MessageBox(" Warning!\n Occupation of block section is detected by Axle counter.\n Do you want to set Emergency LCR 'ON'?","BLOCK STATUS", MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2) == IDYES)
					{
						str = " Do you want to set LCR 'ON' ?";
						if ( CheckConfirm( str, point, strtitle,14 ) )
						{
							pSignal->mOperateLCR = FALSE;
							FireOperate( WS_OPMSG_LCROP, dl );
							return;
						}
						pSignal->mOperateLCR = FALSE;
					}
				}
			}
			else
			{
				MessageBox( " Occupation of block section is detected by Axle counter!", "BLOCK STATUS");
				return;
			}
		}
	}

    if (pSignal->m_bCBBClicked && !pBlkInfo->CBB)
	{
		if (pSignal->m_nType == 6) // TGB 
		{
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
// 			if (!pBlkInfo->CA)
// 				return;
// 20130813 CA제어는 블럭제어와 별개로 하자는 FAT Comment에 의해 수정. 
			if (pBlkInfo->DEPARTIN)
			{
// 				if (!pBlkInfo->ARRIVETR)
					return;
			}
			else if (pBlkInfo->DEPART)   // 열차가 출발하면 CBB 취급을 할 수 없다...
			{
				return;
			}
			else if (pBlkInfo->ASTART || pBlkInfo->ARRIVEIN)
			{
				return;
			}
		}

		// 2005.09.08 -- TCB의 경우 이전역에서 열차가 출발했다는 정보가 있을 경우 비상(emergency) 취급을 해야 한다..
		if (pSignal->m_nType == 5) // TCB
		{
			/*
			if ( !pBlkInfo->DEPARTIN )
			{
				MessageBox("CBB does't be permissioned ! ","BLOCK STATUS");
				return;
			}
			*/
			if (!pBlkInfo->ARRIVE && pBlkInfo->LCRIN && pBlkInfo->LCR)
			{
				/*
				dl = pSignal->m_nID;
				FireOperate( WS_OPMSG_CBBOPEMER, dl );
				*/
				MessageBox(" Train doesn't arrived ! ","BLOCK STATUS");
				return;
			}
		}

		dl = pSignal->m_nID;
		pSignal->mOperateCBB = TRUE;

		if ( pBlkInfo->AXLOCC || bBlockWithoutAxlecounter == TRUE )
		{
			if ( pSignal->m_nType == 5 )
			{
				str = " Do you want to set BRB 'ON' ?";
			}
			else if ( pSignal->m_nType == 6 )
			{
				str = " Do you want to set BCB 'ON' ?";
			}
/*
	   if(pBlkInfo->ARRIVE)                                      // 폐색 진행 완료시(TCB)
	   {
		   strtitle = "Block Operation Terminate ";
	   }
	   else if(pSignal->m_nType == 5 && pBlkInfo->LCRIN && !pBlkInfo->LCR)  // 도착역에서 열차 진입 거부 (TCB)   
	   {
		   strtitle = "Block Operation Request Deny ";
	   }
	   else if(pBlkInfo->LCR && pBlkInfo->LCRIN)                 // 폐색운행 허가를 받은 후 이지만 출발역에서 취소 (TGB)          
	   {
		   strtitle = "Block Operation Cancel";
	   }
	   else if(pBlkInfo->LCR && !pBlkInfo->LCRIN)                // 폐색 운행 허가전에 취소(TGB)
	   {
		   strtitle = "Block Operation Request Cancel ";
	   }
	   else if(!pBlkInfo->LCR && !pBlkInfo->LCRIN)               // 폐색 운행 요청전에 취소(취급실수시)(TGB)
	   {
		   strtitle = "CA Cancel";                    
	   }
*/
			if ( CheckConfirm( str, point, strtitle,14 ) )
			{
			   pSignal->mOperateCBB = FALSE;
			   FireOperate( WS_OPMSG_CBBOP, dl );
			}
		}
		else
		{
			MessageBox( " Occupation of block section is detected by Axle counter!", "BLOCK STATUS");
		}
		
		pSignal->mOperateCBB = FALSE;
	}

	else if (pSignal->m_bOn) 
	{
		if (pSignal->m_nOnTimer) return;	// approach running.
		
		BYTE CS = pSigInfo->CS;
		WORD wRoute = m_Engine.GetRouteNo ( (BYTE)pSignal->m_nID, CS, (pSigInfo->DESTID & 0x3F)/*(pSigInfo->DESTTRACK & 0x7f)*/);

/*
		if (wRoute)
			wRoute += ( (pSigInfo->DESTID & 0x3f)-1 );
*/
		
		BYTE nDependSigID=0;

		if (wRoute >= 0)
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRoute ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			nDependSigID = pInfo[ RT_OFFSET_DEPENDSIG1 ];
		}
		else
		{
			if ( pSignal->m_nType == 4 )
			{
				wRoute = m_Engine.m_pSignalInfoTable[ (BYTE)pSignal->m_nID ].wRouteNo;
			}
		}

		short nResult = m_Engine.CheckRoute( wRoute );

		// 2006.5.22  Istart signal을 위해 수정....
		// 20140423 sjhong - 시그널 타입이 OUT(START)일 경우에도 종속신호기로 동작할 경우에는 팝업이 뜨지 않도록 한다.
		if((pSignal->m_nType == 4 && !CS) || (pSignal->m_nType == 12 && !CS) || (pSignal->m_nType == 2 && pSigInfo->OSS) || (pSignal->m_nType == 1 && pSigInfo->OSS))
		{
			if (/*pSignal->m_nType != 4 &&*/ pSigInfo->RCVRONLY)
			{
				dl = pSignal->m_nID;

				if ( pSigInfo->INU || pSigInfo->SIN2 )
				{
					str += " Signal Stop?";
					strtitle = "Signal Control";

					if ( CheckConfirm( str, m_MenuPoint, strtitle,1))
						FireOperate( WS_OPMSG_SIGNALSTOP, dl);
				}
				else
				{
					str += " Signal Recovery?";
					strtitle = "Signal Control";
					
					if ( CheckConfirm( str, m_MenuPoint, strtitle,1))
						FireOperate( WS_OPMSG_ASPECT, dl);
				}
			}
//			MessageBox( "Cannot control this signal!\t\nLook for smart alternatives!", "SIGNAL STATUS");
//			pSignal->m_bOn = FALSE
			return;
		}

		if ( !(pSigInfo->DESTTRACK & 0x80) && pSignal->m_nSignalOn && pSigInfo->LM_ALR ==0) 
		{
			ScrInfoTrack *pDestTk = (ScrInfoTrack *)m_Engine.int_get(TrackGet, pSigInfo->DESTTRACK);

			if( pDestTk->EMREL == 1 )
			{
				MessageBox( " You must try 'Emergency Route Release!'. ", "SIGNAL STATUS" );
				return;
			}
			else
			{
				// 2005.09.08 ASTART일 경우 signal stop 제어는 없다...  
				// 2005.11.9 Ground shunt signal의 경우 ALR이 항상 0이므로 Signal stop 제어가 없다.
				if (pSignal->m_nType==3 || pSignal->m_nType==2) 
				{
					dl = pSignal->m_nID;
					str += " Route Release?";
					strtitle = "Signal Control";
					
					if ( CheckConfirm( str, m_MenuPoint, strtitle,1 ) )
						FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
					return;
				}

				str  = pSignal->mName;
				str += "  Signal Control?";
				release = "Signal and Route Release";
				restr = "Signal Stop";
				short r = Signalstop( str, release, restr, m_MenuPoint );
				dl = pSignal->m_nID;

				if ( r == 1 ) 
				{  // 진로와 신호 취소
					d2 = pSigInfo->DESTID;
					d2 &= 0x7f;
					d2 -= 1;

					FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
				}
				else if ( r == 2 ) { // 신호기 Stop	
					FireOperate( WS_OPMSG_SIGNALSTOP, dl );
				}
    		}
		}
		else if ( (pSigInfo->DESTTRACK & 0x7f) || pSigInfo->TM1 || (nResult & ROUTECHECK_TRACK) )
		{
			str  = pSignal->mName;
			RECT rect;
			GetWindowRect( &rect );
			point.x += rect.left;
			point.y += rect.top;

			if ( ((pSigInfo->DESTTRACK & 0x80) /*&& !pSigInfo->TM1*/) || ((nResult & ROUTECHECK_TRACK) && !pSigInfo->TM1) ) 
			{
				MessageBox( " You must try 'Emergency Route Release!'. ", "SIGNAL STATUS" );
				return;
			}
			else 
			{
				if (pInfo != NULL)
				{
					BYTE nCount, nID;

					// Route
					RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)pInfo;
					BYTE *p = pInfo + pRouteBasPtr->nOfsRoute;

					nCount = *p++;

					nID = *p;  // 첫번째 route 궤도...

					if ( nID == 0 )
						return;

					CBmpTrack *pTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nID );
					if (pTrack == NULL)
						return;

					ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pTrack->m_pData;
					if (pTrkInfo->ROUTE == 1 && !pTrkInfo->TKREQUEST)
					{
						MessageBox( " You must try 'Emergency Route Release!'. ", "SIGNAL STATUS" );
						return;
					}
				}
/*				if (pSigInfo->REQUEST && pSignal->m_nType==0)
				{
					str  = pSignal->mName;
					str += "  Signal Control?";
					release = "Route Release";
					restr = "Set Alternative Route";
					short result = Signalstop( str , release, restr, m_MenuPoint );

					if ( result == 1 )
					{
						dl = pSignal->m_nID;
						FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
						return;
					}
					else if ( result == 2 )
					{
						DestEnableCheck( (BYTE)pSignal->m_nID, point );
						return;
					}
				}													// FAT 대안 진로 관련 코딩 원복........
				else*/ if (pSigInfo->REQUEST || pSignal->m_nType==3) // 2005.09.18  Astart는 signal recovery가 없다... 
				{
					dl = pSignal->m_nID;
					str += " Route Release?";
					strtitle = "Signal Control";
					
					if ( CheckConfirm( str, m_MenuPoint, strtitle,1 ) )
						FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
					return;
				}
///////////////////////////////////////////////////////////////////////////// FAT수정.
/*				if ( nResult & ROUTECHECK_SWFREE )
				{
					str  = pSignal->mName;
					str += "  Signal Control?";
					release = "Route Release";
					restr = "Set Alternative Route";
					short r = Signalstop( str, release, restr, m_MenuPoint );
					if ( r == 1 )       // 진로와 신호 취소
					{  
						dl = pSignal->m_nID;
						FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
					}
					else if ( r == 2 )  // 신호기 재취급
					{
						DestEnableCheck( (BYTE)pSignal->m_nID, point );
						return;
					}
				}
				else
				{
					str  = pSignal->mName;
					str += "  Signal Control?";
					release = "Route Release";
					restr = "Signal Aspect Recovery";
					short r = Signalstop( str, release, restr, m_MenuPoint );
					if ( r == 1 )       // 진로와 신호 취소
					{  
						dl = pSignal->m_nID;
						FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
					}
					else if ( r == 2 )  // 신호기 재취급
					{
						d1 = (BYTE)pSignal->m_nID;
						d2 = nDependSigID;
						FireOperate( WS_OPMSG_ASPECT, dl );
					}
				}
/**/
				str  = pSignal->mName;
				str += "  Signal Control?";
				release = "Route Release";
				restr = "Signal Aspect Recovery";
				short r = Signalstop( str, release, restr, m_MenuPoint );
				if ( r == 1 )       // 진로와 신호 취소
				{  
					dl = pSignal->m_nID;
					d2 = pSigInfo->DESTID;
					d2 &= 0x7f;
					d2 -= 1;

					FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
				}
				else if ( r == 2 )  // 신호기 재취급
				{
					d1 = (BYTE)pSignal->m_nID;
					d2 = pSigInfo->DESTID;
					d2 &= 0x7f;
					d2 -= 1;

					FireOperate( WS_OPMSG_ASPECT, dl );
				}
/**/
///////////////////////////////////////////////////////////////////////////// FAT수정.
/* Release Only
				dl = pSignal->m_nID;
				str += " Route Release?";
				strtitle = "Signal Control";

				if ( CheckConfirm( str, m_MenuPoint, strtitle,1 ) )
					FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
				/*

  
/* Retry (Old version)
	            short r = SelectSignalRetry( str, point );
				if ( r == 1 ) 
				{  // 신호 재취급
			    	d1 = (BYTE)(pSignal->m_nID);
					d2 = (BYTE)( (pSigInfo->DESTTRACK & 0x7f) );
					FireOperate( WS_OPMSG_SIGNALDEST, dl );
				}
				else if ( r == 2 ) 
				{
					dl = pSignal->m_nID;
					FireOperate( WS_OPMSG_SIGNALCANCEL, dl );
				}
				*/
			}
		}
	}
	else 
	{
		if (pSignal->m_nOnTimer) return;	// approach running.
		
	    DestEnableCheck( (BYTE)pSignal->m_nID, point );
	}
}

CString GetSwitchName( char *pStr );
void CLSMCtrl::DoSwitchEvent( CBmpSwitch *pSwitch, CPoint &point ) {
	union {
		long lData;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
		struct {
			short dl;
			short dh;
		};
	};
	
	lData = 0;
	CString strtitle;
	strtitle = "HandPoint Key Control";

	ScrInfoSwitch *pSwInfo = (ScrInfoSwitch*)pSwitch->m_pData;

	CString str = GetSwitchName( pSwitch->mName );
	CString strSwitchName = pSwitch->mName;
	CString strMove;
	CString strBlock;
//	if ( strSwitchName.Find("B") > 1 );					// 2013.03.04 Consultant로부터 B에서는 제어 안먹히도록 수정해달라는 요청에 의해 막았다가 
//	else												// 메뉴에 의한 제어도 먹히지 않아 화면에서만 제어 불가능 하도록 수정.
//	{
		str += " ";
		point.x -= 300;
		if (pSwitch->m_bHandPointSW)
		{

			// 2005.09.08 수정,  수동 전철기 진로 설정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
			if (!pSwInfo->KEYOPER)  // Key를 뺄 수 없는 상태... 
			{
				if ( pSwInfo->INROUTE /*|| !pSwInfo->TRACKLOCK*/)  // 2005.09.08  Track lock 조건 삭제 - BR rule에 따라. (Key를 뺄 수 있는 상태로 만들지 못한다.) 
				{
					MessageBox( " Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
					return;			
				}
			}
			
			if (!pSwInfo->WKEYKR) // 2005.09.08   수동전철기 Key가 빠져있으면 제어하지 못한다...
			{
				MessageBox( " HandPoint key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
				return;
			}

			if ( pSwInfo->KEYOPER ) 
			{
     			str  = " Key Transmitter Lock?";
			}
			else
			{
				str  = " Key Transmitter Release?";
			}
			if ( CheckConfirm( str, point,strtitle,12 ) ) 
			{
				dl = pSwitch->m_nID;
				FireOperate( WS_OPMSG_HSW, dl );
			}
		}

		else
		{
			strtitle = "Point Control";

    		if ( pSwInfo || !pSwInfo->WLR) 
			{
				if ( pSwInfo->BLOCK )
				{
					if ( pSwInfo->ROUTELOCK || !pSwInfo->TRACKLOCK )
					{
						str  = pSwitch->mName;
						str += " Block ?";
						dl = pSwitch->m_nID;
						if ( CheckConfirm( str, point, strtitle,3 ))
						{
							FireOperate( WS_OPMSG_POINT_BLOCK, dl);
						}
					}
					else
					{
						str  = GetSwitchName( pSwitch->mName );
						str += "  Point Control?";
						strMove = "Point Move";
						strBlock = "Point Block";
						short r = PointControl( str, strMove, strBlock, m_MenuPoint );
						dl = pSwitch->m_nID;
						
						if ( r == 1 ) 
						{  // 선로전환기 전환.
							str  = GetSwitchName( pSwitch->mName );
							str += " Change to";
							if ( pSwInfo->WR_P ) str += " Reverse ?";
							else str += " Normal ?";
							if ( !pSwInfo->BLOCK )
							{
								MessageBox(" Point Blocked! Cannot be operated! ","POINT STATUS");
								return;
							}
							else if ( !pSwInfo->FREE || pSwInfo->ROUTELOCK ) 
								// 					if ( !pSwInfo->FREE || pSwInfo->ROUTELOCK ) 
							{
								MessageBox(" Point Locked! Cannot be operated! ","POINT STATUS");
								return;
								//				str += " Control Impossible!";
								//				CheckConfirm( str, point,strtitle,2 );
							}
							else if ( pSwInfo->KR_P != pSwInfo->KR_M || ( !pSwInfo->KR_P && !pSwInfo->KR_M ))// && pSwInfo->KR_P != pSwInfo->KR_M ) 
							{
								//					str += " Change ?";
								if ( CheckConfirm( str, point,strtitle,3 ) ) 
								{
									if ( pSwInfo->WR_P ) dl |= 0x80;
									FireOperate( WS_OPMSG_SWTOGGLE, dl );
								}
							}
							else 
							{
								MessageBox(" Point Failed! ","POINT STATUS");
								return;
								//				str += " Point Failed";
								//				CheckConfirm( str, point,strtitle,2 );
							}
						}
						else if ( r == 2 ) 
						{ // 선로전환기 block
							str  = pSwitch->mName;
							str += " Block ?";
							if ( CheckConfirm( str, point, strtitle,3 ))
							{
								FireOperate( WS_OPMSG_POINT_BLOCK, dl);
							}
						}
					}
				}
				else
				{
					str	= pSwitch->mName;
					str += " Unblock ?";
					if ( CheckConfirm( str, point, strtitle,3))
					{
						dl = pSwitch->m_nID;
						dl |= 0x80;
						FireOperate( WS_OPMSG_POINT_BLOCK, dl);
					}
				}
			}
			else 
			{
				MessageBox(" Point Locked! Cannot be operated! ","POINT STATUS");
				return;
	//			str += " Locking! Control Impossible!";
	//			CheckConfirm( str, point,strtitle,3 );
			}
		}
//	}
}
void CLSMCtrl::DoTrackEvent( CBmpTrack *pTrack, CPoint &point )
{
	typedef struct tagRouteBasPtr 
	{
		WORD	nOfsSignal;
		WORD	nOfsTrack;
		WORD	nOfsRoute;
		WORD	nOfsAspect;
		WORD	nOfsBlockNormal;
		WORD	n3;
		WORD	n4;
		WORD	n5;
	} RouteBasPtr;

	union {
		long wParam;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
	} ;
	union {
		long lParam;
		struct {
			short ndl;
			short ndh;
		};
	} ;

    short nSignalID;
	wParam = 0;
	if ( pTrack ) 
	{
		ScrInfoTrack *pInfo = (ScrInfoTrack*)pTrack->m_pData;

		// 궤도 정보를 가지고 있지 않은 경우 처리하지 않는다.....
		if (pInfo == NULL)
			return;

		d1 = (BYTE)pTrack->m_nID;
		ndl = (short)(point.x);
		ndh = (short)(point.y);

		BOOL bFindMask = FALSE, bFindTrack = FALSE;
		short nSignal = m_Engine.m_strSignal.GetCount();
		CBmpSignal *pSignal;
		ScrInfoSignal *pSigInfo;

		for (nSignalID=1; nSignalID<=nSignal; nSignalID++)
		{
			pSignal = (CBmpSignal *)SearchPtrByID( OBJ_SIGNAL, nSignalID );
			pSigInfo = (ScrInfoSignal*)pSignal->m_pData;

			if ( (pSigInfo->DESTTRACK & 0x7f) == d1)
			{
				if ( !((m_Engine.m_pSignalInfoTable[nSignalID].nSignalType & 0xf0) == SIGNALTYPE_OHOME) && pSigInfo->OSS )
					continue;
				else if ( (m_Engine.m_pSignalInfoTable[nSignalID].nSignalType & 0xf0) == SIGNALTYPE_OHOME )
				{
					if (!pSigInfo->CS)
						return;
				}

				bFindTrack = TRUE;
				if (pInfo)
				{
					// 비상해정 check 조건에서 TM1 check를 삭제함...
					// 즉 신호가 현시되지 않은 상황에서도 궤도가 점유되거나, 부분적으로 진로가 해정된 궤도가 있으면 비상해정 취급을 한다...
					if ( !pInfo->ROUTE /*&& pSigInfo->TM1==0*/ && (pSigInfo->DESTTRACK & 0x80) && !pInfo->EMREL)
					{
						//nFindSignal++;
						d2 = ROUTELOCK_FREE;
						FireOleSendMessage( WS_OPMSG_FREESUBTRACK, wParam, lParam );
						return;
					}
					else
					{
						if ( pSigInfo->TM1 || (pSigInfo->SE1 || pSigInfo->SE2 || pSigInfo->SE3 || pSigInfo->SE4) )
						{
							    return;
						}
					}
				}
			}
				
			if ( (pSigInfo->DESTTRACK & 0x7f) == d1 /*&& pSigInfo->TM1==0*/ /*&& pSigInfo->ROUTE==1*/)
			{
				bFindMask = TRUE;
				break;
			}
			
		}

		if (bFindMask == TRUE)
		{
			BYTE CS = pSigInfo->CS;
						
			short nRoute = m_Engine.GetRouteNo ( (BYTE)pSignal->m_nID, CS, pSigInfo->DESTID & 0x3F/*pSigInfo->DESTTRACK & 0x7f*/);
/*
			nRoute += ((pSigInfo->DESTID & 0x3f) -1);
*/
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ nRoute ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfoMem = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

			BYTE nID;
			unsigned short i, nCount;

			RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)pInfoMem;
			BYTE *p = pInfoMem + pRouteBasPtr->nOfsRoute;
			nCount = *p++;

			for (i=0; i<nCount; i++) 
			{
				nID = *p++;
				CBmpTrack *pObjTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nID );
				ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pObjTrack->m_pData;

				if ((!pTrkInfo->TRACK || pTrkInfo->ROUTE) && !pTrkInfo->EMREL)
				{
					d2 = ROUTELOCK_FREE;
					FireOleSendMessage( WS_OPMSG_FREESUBTRACK, wParam, lParam );
					return;
				}
			}

			if ( !nCount )
			{
				if (pInfo)
				{
					if (!pInfo->ROUTE && !pInfo->EMREL)
					{
						d2 = ROUTELOCK_FREE;
						FireOleSendMessage( WS_OPMSG_FREESUBTRACK, wParam, lParam );
						return;
					}
				}
			}
		}
		else
		{

//			if (m_Engine.m_pTrackInfoTable[d1] & TI_DESTTRACK) 
//			{
			// 20140527 sjhong - TrackInfo에 진로번호를 추가하고 이를 통해 현재 비상해정 명령을 받은 궤도가 종착궤도인지 체크한다.
			if(pInfo->RouteNo)
			{
				UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ pInfo->RouteNo & ROUTE_MASK ];
				BYTE *pInfoMem = &m_Engine.m_pInfoMem[nInfoPtr];
				BYTE nDestTrk = pInfoMem[RT_OFFSET_TO] & 0x7F;

				if (!pInfo->ROUTE && (nDestTrk == d1))
				{

					// 2006.11.16 AKA역의 경우 비상정지를 할때 SDG 궤도에 의해 W107108T, W124125T, 3T가 선택될 수 있다. 
					// 이것을 막기 위한 로직 추가...
					if ( m_strStationName.Find ( "AKHA",0 )==0 )
					{

						CString sName = pTrack->mName;
						TScrobj *pObjs[5] = {NULL}; 
						BYTE Signal1Find=0, Switch1Find=0, Switch2Find=0, Switch3Find=0;

						if ( (sName.Find ("W107108T", 0) == 0)  )
						{
							SearchPtrsByID( OBJ_TRACK, pTrack->m_nID, pObjs );

							for (BYTE tIndex=0; tIndex<5; tIndex++)
							{
								CBmpTrack *pObjTrack;
								if (pObjs[tIndex] != NULL)
								{
									pObjTrack = (CBmpTrack *)pObjs[tIndex];
									if (pObjTrack->pSwitch)
									{
										CString sSwitch = pObjTrack->pSwitch->mName;
										if ( (sName.Find ("W107108T", 0) == 0) && (sSwitch.Find ("107", 0) == 0) )
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
												return;
										}
									}
								}
								else
									break;
							}
						}
						else if ( (sName.Find ("3T", 0) == 0) || (sName.Find ("W109T", 0) == 0) ) 
						{
							if (sName.Find ("3T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "5" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
							}
							else if (sName.Find ("W109T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "8" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
								pRetObj = SearchPtrByName( OBJ_SIGNAL, "10" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
							}
						}
						else if ( (sName.Find ("W121T", 0) == 0) || (sName.Find ("W121122T", 0) == 0) || (sName.Find ("W122123T", 0) == 0) || 
								  (sName.Find ("W130131T", 0) == 0) || (sName.Find ("W101103T", 0) == 0) || (sName.Find ("W124125T", 0) == 0) ||
								  (sName.Find ("W128129T", 0) == 0) || (sName.Find ("W128T", 0) == 0) ) 
						{
							SearchPtrsByID( OBJ_TRACK, pTrack->m_nID, pObjs );

							for (BYTE tIndex=0; tIndex<5; tIndex++)
							{
								CBmpTrack *pObjTrack;
								if (pObjs[tIndex] != NULL)
								{
									pObjTrack = (CBmpTrack *)pObjs[tIndex];
									if (pObjTrack->pSwitch)
									{
										CString sSwitch = pObjTrack->pSwitch->mName;
										if (sSwitch.Find ("121", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch1Find = 1;
											}
										}
										else if (sSwitch.Find ("122", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch2Find = 1;
											}
										}
										else if (sSwitch.Find ("123", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch3Find = 1;
											}
										}
										else if (sSwitch.Find ("131", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch1Find = 1;
											}
										}
										else if (sSwitch.Find ("130", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_P)
											{
												Switch2Find = 1;
											}
										}
										else if (sSwitch.Find ("101", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch2Find = 1;
											}
										}
										else if (sSwitch.Find ("103", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch3Find = 1;
											}
										}
										else if (sSwitch.Find ("124", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_P)
											{
												Switch1Find = 1;
											}
										}
										else if (sSwitch.Find ("125", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch2Find = 1;
											}
										}
										else if (sSwitch.Find ("128", 0) == 0)
										{
											ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pObjTrack->pSwitch->m_pData;
											if (pSwitchInfo->KR_M)
											{
												Switch1Find = 1;
											}
										}
									}
								}
								else
									break;
							}

							if (sName.Find ("W121T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "41" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										Signal1Find = 1;
									}
								}
								if (Switch1Find==1 && Signal1Find==1)
									return;
							}
							else if (sName.Find ("W121122T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "43" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										Signal1Find = 1;
									}
								}
								if ( (Signal1Find==1 && Switch2Find==1) || (Switch1Find==1 && Switch2Find==1) )
									return;
							}
							else if (sName.Find ("W122123T", 0) == 0)
							{
							    if (Switch3Find==1)
									return;
							}
							else if (sName.Find ("W124125T", 0) == 0)
							{
							    if (Switch1Find==1 || Switch2Find==1)
									return;
							}
							else if (sName.Find ("W130131T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "59" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										Signal1Find = 1;
									}
								}
								pRetObj = SearchPtrByName( OBJ_SIGNAL, "50" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										if (!Switch1Find)
											return;
									}
								}
								if ( (Signal1Find==1 && Switch1Find==1) || (Switch1Find==1 && Switch2Find==1) )
									return;
							}
							else if (sName.Find ("W101103T", 0) == 0)
							{
								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "3" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										if (Switch2Find==1 || Switch3Find==1)
											return;
									}
								}

								pRetObj = SearchPtrByName( OBJ_SIGNAL, "4" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										if (Switch2Find==1 || Switch3Find==1)
											return;
									}
								}
							}
							else if (sName.Find ("W128129T", 0) == 0)
							{
								if (Switch1Find == 1)
									return;

								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "57" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
								pRetObj = SearchPtrByName( OBJ_SIGNAL, "55" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
							}
							else if (sName.Find ("W128T", 0) == 0)
							{
								if (Switch1Find == 1)
								{
									CBmpTrack *pRetTrack;
									TScrobj *pRetObj = SearchPtrByName( OBJ_TRACK, "W128129T.1" );
									if (pRetObj) 
									{
										pRetTrack = (CBmpTrack*)pRetObj;
										ScrInfoTrack *pTrackInfo = (ScrInfoTrack*)pRetTrack->m_pData;
										if (pTrackInfo->ROUTE || pTrackInfo->OVERLAP)
										{
										}
										else
											return;
									}
								}

								TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "48" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
								pRetObj = SearchPtrByName( OBJ_SIGNAL, "61" );
								if (pRetObj) 
								{
									pSignal = (CBmpSignal*)pRetObj;
									ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
									if (pSignalInfo->DESTID)
									{
										return;
									}
								}
							}
						}


					}
					else if ( m_strStationName.Find ( "BYPASS",0 )==0 )
					{
						CString sName = pTrack->mName;

						if (sName.Find ("13T", 0) == 0)
						{
							TScrobj *pRetObj = SearchPtrByName( OBJ_SWITCH, "133A" );
							if (pRetObj) 
							{
								CBmpSwitch *pSwitch = (CBmpSwitch*)pRetObj;
								ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
								if (pSwitchInfo->KR_M)
								{
									return;
								}
							}
						}
						else if (sName.Find ("12T", 0) == 0)
						{
							TScrobj *pRetObj = SearchPtrByName( OBJ_SIGNAL, "63" );
							if (pRetObj) 
							{
								pSignal = (CBmpSignal*)pRetObj;
								ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
								if (pSignalInfo->DESTID)
								{
									return;
								}
							}
							pRetObj = SearchPtrByName( OBJ_SIGNAL, "65" );
							if (pRetObj) 
							{
								pSignal = (CBmpSignal*)pRetObj;
								ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
								if (pSignalInfo->DESTID)
								{
									return;
								}
							}
						}
					}

					d2 = TRACKLOCK_FREE;
					FireOleSendMessage( WS_OPMSG_FREESUBTRACK, wParam, lParam );
					return;
				}
			}
		}
	}
}

void CLSMCtrl::DoButtonEvent( CBmpButton *pButton, CPoint &point ) {
	union {
		long lData;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
		struct {
			short dl;
			short dh;
		};
	};

	lData = 0;
	if (pButton->m_pSignal) 
	{ 
		CString str = pButton->m_pSignal->mName;
		str += "-";
		str += pButton->mName;

		m_pRouteLine = SearchRoutePos( str );

		if ( m_pRouteLine ) InvalidateControl();

					
		str += " Signal Clear?";
        CBmpSignal *pSignal = pButton->m_pSignal;   // Save Signal pointer for Press Timeout

	    if ( pSignal->IsRight )  
			point.x -=200;
		CString strtitle;
		strtitle = "Signal Control";
			
		if ( CheckConfirm( str, point,strtitle,0 ) ) 
		{
			d1 = (BYTE)(pSignal->m_nID);
			d2 = (BYTE)(pButton->m_pTrack->m_nID);

			pSignal->m_pButton = pButton;
			FireOperate( WS_OPMSG_SIGNALDEST, dl );
		}
		pSignal->m_nPressTimer = 0;
		pButton->m_pSignal = NULL;
			m_pRouteLine = NULL;
	}
}

void CLSMCtrl::DoSystemButtonEvent( TScrobj *pSysObj, CPoint &point )
{
	if ( pSysObj == NULL ) return;
	if ( pSysObj->m_pData == NULL ) return;

	union {
		long wParam;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
	};

	union {
		long lParam;
		struct {
			short ndl;
			short ndh;
		};
	};
	wParam = 0;
	lParam = 0;

	CString str;
    
	BOOL bConfirm = FALSE;
	BOOL bSendMsg = FALSE;

	//pDataHead = (DataHeaderType*)pSysObj->m_pData;			
	SystemStatusType &nSysVar = pDataHead->nSysVar;	
	SystemStatusTypeExt  &nSysVarExt = pDataHead->nSysVarExt;
	UnitStatusType   &UnitState = pDataHead->UnitState;

	TScrobj *pRetObj;
	TScrobj *pRetObj2;
	CBmpSignal *pSignal;
	CBmpTrack  *pTrack;
	CBmpSwitch *pSwitch;
	ScrInfoSignal *pSigInfo;
	ScrInfoSwitch *pPntInfo;
	ScrInfoBlock *pBlkInfo;
	ScrInfoTrack *pTrackInfo;

	int dir;
	int symbol = 0;
	char nLen;
	BYTE index;
	CString strtitle;
	strtitle = "System Control";
	CString temp = "W";
	CString strTemp;
	CString strBufferLCName = pSysObj->m_szDrawName;
	int		iConflictRouteNumberForLC[50];
	CString ConflictRouteNumberForLC[50];
	CString strBuffer = pSysObj->m_szDrawName;
	int iWhereIs$ = strBuffer.Find('$');

	CLSMApp* pApp;
	pApp= (CLSMApp*)AfxGetApp();

	CString strLCName;
	
	switch((BYTE)pSysObj->m_nID)
	{
	case BtnLCA:
	case BtnLCB:
		strLCName = pApp->m_strLCName[1];
		break;
	case BtnLC2A:
	case BtnLC2B:
		strLCName = pApp->m_strLCName[2];
		break;
	case BtnLC3A:
	case BtnLC3B:
		strLCName = pApp->m_strLCName[3];
		break;
	case BtnLC4A:
	case BtnLC4B:
		strLCName = pApp->m_strLCName[4];
		break;
	case BtnLC5A:
	case BtnLC5B:
		strLCName = pApp->m_strLCName[5];
		break;
	}
	strBuffer = strBuffer.Right(strBuffer.GetLength()-strBuffer.Find('@')-1);
				
	for(int nCounter = 0; nCounter < 50; nCounter++)
	{
		if ( strBuffer.Find('&') < 0 && strBufferLCName.Find('@') < 0 )
		{
			ConflictRouteNumberForLC[nCounter] = "";
			iConflictRouteNumberForLC[nCounter] = 0;
		}
		else if ( strBuffer.Find('&') < 0 && strBufferLCName.Find('@') > 0 )
		{
			ConflictRouteNumberForLC[nCounter] = strBuffer;
			iConflictRouteNumberForLC[nCounter] = atoi(ConflictRouteNumberForLC[nCounter]);
			strBuffer = "";
		}
		else
		{
			ConflictRouteNumberForLC[nCounter] = strBuffer.Left(strBuffer.Find('&'));
			iConflictRouteNumberForLC[nCounter] = atoi(ConflictRouteNumberForLC[nCounter]);
			strBuffer = strBuffer.Right(strBuffer.GetLength()-strBuffer.Find('&')-1);
		}
		if (iConflictRouteNumberForLC[nCounter] == 0)
			iConflictRouteNumberForLC[nCounter]--;
	}

	char szName[10] = "\0", sName1[10]= "\0", sName2[10]= "\0";
	
	char cRealName[100] = "\0";
	memcpy(cRealName, pSysObj->mName, sizeof(cRealName));
	
	for (int i = 0;i<100;i++)
	{
		if ( cRealName[i] == '_' )
		{
			cRealName[i] = '\0';
			break;
		}
		else if ( cRealName[i] == '\0')
		{
			break;
		}
	}

	switch ((BYTE)pSysObj->m_nID) 
	{
	case BtnSysRun:	              // 시스템 동작
        symbol = 4;
		if (!UnitState.bSysRun)
		{
			str = "System Run?";
			bConfirm = TRUE;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnLocalMode:
		if (!nSysVar.bMode)
		{
			return;
		}
		else
		{
			str = " Do you want to set 'LOCAL MODE' ?";
			bConfirm = TRUE;
			symbol = 12;
			d1 = (BYTE)pSysObj->m_nID;
			break;
		}

	case BtnCTCMode:
		if (!nSysVar.bCTCRequest || nSysVar.bMode)
		{
			return;
		}
		else
		{
			str = " Do you want to set 'CTC MODE' ?";
// 			bConfirm = TRUE;
			symbol = 12;

			int iCheck = CheckConfirm( str, point, strtitle, symbol );
				
			if ( iCheck == 1 )
			{
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				d1 = (BYTE)pSysObj->m_nID - 1;
			}

			FireOleSendMessage( WS_OPMSG_BUTTON, wParam, lParam );
			break;
		}

	case BtnPas:                 // PAS 버튼(운영 차단)	
// 		int b;
// 		for (b=1; b<5; b++)
// 		{
// 			switch ( b ) 
// 			{
// 			case 1 :
// 				strcpy(szName, "B1");
// 			break;	
// 			case 2 :
// 				strcpy(szName, "B2");
// 			break;	
// 			case 3 :
// 				strcpy(szName, "B3");
// 			break;	
// 			case 4 :
// 				strcpy(szName, "B4");
// 			break;	
// 			}
// 			pRetObj = SearchPtrByName( OBJ_SIGNAL, szName );
// 			if (pRetObj) 
// 			{
// 				pSignal = (CBmpSignal*)pRetObj;
// 				pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
// 				if ((pBlkInfo->CA) && (pBlkInfo->LCRIN))
// 				{
// 
// 					return;
// 				}
// 			}
// 		}

		if ( nSysVar.bN1 ) 
		{
			// MessageBox("PAS OUT -> IN ");
            FireOleSendMessage( WS_OPMSG_PAS_IN, wParam, lParam );
		}
		else
		{
			// MessageBox("PAS IN -> OUT ");
            FireOleSendMessage( WS_OPMSG_PAS_OUT, wParam, lParam );
		}
		return;
	case BtnSls:               // SLS 버튼(신호기 정지)
		if ( nSysVar.bN3 ) 
		{
			str  = " Do you want to set Signal 'OFF' ?";
			bConfirm = TRUE;
			symbol = 1;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " Do you want to set Signal 'ON' ?";
			bConfirm = TRUE;
			symbol = 0;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;
	case BtnAtb:               // ATB 버튼(신호기 테스트)
		if ( !nSysVar.bATB ) 
		{
			str  = " Do you want to set ATB 'ON' ?";
			bConfirm = TRUE;
            symbol = 0;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;		
	case BtnDay:		      // Dimmer 스위치
		if ( nSysVar.bN2 ) 
		{
			str  = " Do you want to set 'Day' ?";
			bConfirm = TRUE;
			symbol = 6;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " Do you want to set 'Night' ?";
			bConfirm = TRUE;
			symbol = 7;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnLCB:		      // 건널목 control
//		if ( !strcmp(pSysObj->m_szDrawName, "LC1") )
		if ( strBufferLCName.Find("LC1") == 0 || ( strBufferLCName.Find('/') == 1 ))
		{
			if (nSysVarExt.bLC1NKXPR) 
			{
				MessageBox(" Level Crossing Opened! Cannot control this button! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC1KXLR ) 
			{
				short nTrack = m_Engine.m_strTrack.GetCount();
				BYTE nTrackID;
				CBmpTrack *pTrackForLC;
				ScrInfoTrack *pTrackInfoForLC;
				BOOL bIsInAOverlapRoute = FALSE;
				
				for (nTrackID=1; nTrackID<=nTrack; nTrackID++)
				{
					pTrackForLC = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nTrackID );
					pTrackInfoForLC = (ScrInfoTrack *)pTrackForLC->m_pData;

					for( int nCounter = 0; nCounter < 50; nCounter++)
					{
						if ( pTrackInfoForLC->LeftToRightRouteNo == iConflictRouteNumberForLC[nCounter] || pTrackInfoForLC->RightToLeftRouteNo == iConflictRouteNumberForLC[nCounter] 
							|| (( pTrackInfoForLC->LeftToRightRouteNo != NULL || pTrackInfoForLC->RightToLeftRouteNo != NULL ) && ConflictRouteNumberForLC[0].Find("ALL") == NULL ))
						{
							bIsInAOverlapRoute = TRUE;
							break;
						}
					}

					if ((!pTrackInfoForLC->TRACK && !m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK) || !pTrackInfoForLC->ROUTE || bIsInAOverlapRoute == TRUE )
					{
						if(	m_Engine.m_pTrackInfoTable[nTrackID].bLC1)
						{
							MessageBox(" Level Crossing Locked! Cannot operate! ","Level Crossing STATUS");
							return;
						}
					}
				}

				str.Format(" %s KEY Transmitter Release?", strLCName);
				strtitle = "LCK ON";
				bConfirm = TRUE;
				symbol = 9;
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				str.Format(" %s KEY Transmitter Lock?", strLCName);
				strtitle = "LCK OFF";
				bConfirm = TRUE;
				symbol = 8;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;

	case BtnLC2B:
		//		if ( !strcmp(pSysObj->m_szDrawName, "LC2") )
		if ( strBufferLCName.Find("LC2") == 0 || ( strBufferLCName.Find('/') == 1))
		{
			if (nSysVarExt.bLC2NKXPR) 
			{
				MessageBox(" Level Crossing Opened! Cannot control this button! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC2KXLR ) 
			{
				short nTrack = m_Engine.m_strTrack.GetCount();
				BYTE nTrackID;
				CBmpTrack *pTrackForLC;
				ScrInfoTrack *pTrackInfoForLC;
				BOOL bIsInAOverlapRoute;
				
				for (nTrackID=1; nTrackID<=nTrack; nTrackID++)
				{
					pTrackForLC = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nTrackID );
					pTrackInfoForLC = (ScrInfoTrack *)pTrackForLC->m_pData;
					
					bIsInAOverlapRoute = FALSE;

					for( int nCounter = 0; nCounter < 50; nCounter++)
					{
						if ( pTrackInfoForLC->LeftToRightRouteNo == iConflictRouteNumberForLC[nCounter] || pTrackInfoForLC->RightToLeftRouteNo == iConflictRouteNumberForLC[nCounter] 
							|| (( pTrackInfoForLC->LeftToRightRouteNo != NULL || pTrackInfoForLC->RightToLeftRouteNo != NULL ) && ConflictRouteNumberForLC[0].Find("ALL") == NULL ))
						{
							bIsInAOverlapRoute = TRUE;
							break;
						}
					}
					
					if ((!pTrackInfoForLC->TRACK && !m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK) 
						|| (!pTrackInfoForLC->TRACK && m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK && pTrackInfoForLC->OVERLAP ) 
						|| !pTrackInfoForLC->ROUTE || bIsInAOverlapRoute == TRUE )
					{
						if(m_Engine.m_pTrackInfoTable[nTrackID].bLC2)
						{
							MessageBox(" Level Crossing Locked! Cannot operate! ","Level Crossing STATUS");
							return;
						}
					}
				}

				str.Format(" %s KEY Transmitter Release?", strLCName);
				strtitle = "LCK ON";
				bConfirm = TRUE;
				symbol = 9;
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				str.Format(" %s KEY Transmitter Lock?", strLCName);
				strtitle = "LCK OFF";
				bConfirm = TRUE;
				symbol = 8;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;

	case BtnLC3B:
		//		if ( !strcmp(pSysObj->m_szDrawName, "LC3") )
		if ( strBufferLCName.Find("LC3") == 0 || (strBufferLCName.Find('/') == 1))
		{
			if (nSysVarExt.bLC3NKXPR) 
			{
				MessageBox(" Level Crossing Opened! Cannot control this button! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC3KXLR ) 
			{
				short nTrack = m_Engine.m_strTrack.GetCount();
				BYTE nTrackID;
				CBmpTrack *pTrackForLC;
				ScrInfoTrack *pTrackInfoForLC;
				BOOL bIsInAOverlapRoute = FALSE;
				
				for (nTrackID=1; nTrackID<=nTrack; nTrackID++)
				{
					pTrackForLC = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nTrackID );
					pTrackInfoForLC = (ScrInfoTrack *)pTrackForLC->m_pData;
					
					for( int nCounter = 0; nCounter < 50; nCounter++)
					{
						if ( pTrackInfoForLC->LeftToRightRouteNo == iConflictRouteNumberForLC[nCounter] || pTrackInfoForLC->RightToLeftRouteNo == iConflictRouteNumberForLC[nCounter] 
							|| (( pTrackInfoForLC->LeftToRightRouteNo != NULL || pTrackInfoForLC->RightToLeftRouteNo != NULL ) && ConflictRouteNumberForLC[0].Find("ALL") == NULL ))
						{
							bIsInAOverlapRoute = TRUE;
							break;
						}
					}
					
					if ((!pTrackInfoForLC->TRACK && !m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK) || !pTrackInfoForLC->ROUTE || bIsInAOverlapRoute == TRUE )
					{
						if(m_Engine.m_pTrackInfoTable[nTrackID].bLC3)
						{
							MessageBox(" Level Crossing Locked! Cannot operate! ","Level Crossing STATUS");
							return;
						}
					}
				}
				
				str.Format(" %s KEY Transmitter Release?", strLCName);
				strtitle = "LCK ON";
				bConfirm = TRUE;
				symbol = 9;
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				str.Format(" %s KEY Transmitter Lock?", strLCName);
				strtitle = "LCK OFF";
				bConfirm = TRUE;
				symbol = 8;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
	case BtnLC4B:
		//		if ( !strcmp(pSysObj->m_szDrawName, "LC4") )
		if ( strBufferLCName.Find("LC4") == 0 || (strBufferLCName.Find('/') == 1))
		{
			if (nSysVarExt.bLC4NKXPR) 
			{
				MessageBox(" Level Crossing Opened! Cannot control this button! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC4KXLR ) 
			{
				short nTrack = m_Engine.m_strTrack.GetCount();
				BYTE nTrackID;
				CBmpTrack *pTrackForLC;
				ScrInfoTrack *pTrackInfoForLC;
				BOOL bIsInAOverlapRoute = FALSE;
				
				for (nTrackID=1; nTrackID<=nTrack; nTrackID++)
				{
					pTrackForLC = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nTrackID );
					pTrackInfoForLC = (ScrInfoTrack *)pTrackForLC->m_pData;

					bIsInAOverlapRoute = FALSE;
					
					for( int nCounter = 0; nCounter < 50; nCounter++)
					{
						if ( pTrackInfoForLC->LeftToRightRouteNo == iConflictRouteNumberForLC[nCounter] || pTrackInfoForLC->RightToLeftRouteNo == iConflictRouteNumberForLC[nCounter] 
							|| (( pTrackInfoForLC->LeftToRightRouteNo != NULL || pTrackInfoForLC->RightToLeftRouteNo != NULL ) && ConflictRouteNumberForLC[0].Find("ALL") == NULL ))
						{
							bIsInAOverlapRoute = TRUE;
							break;
						}
					}
					
					if ((!pTrackInfoForLC->TRACK && !m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK) || !pTrackInfoForLC->ROUTE || bIsInAOverlapRoute == TRUE )
					{
						if(m_Engine.m_pTrackInfoTable[nTrackID].bLC4)
						{
							MessageBox(" Level Crossing Locked! Cannot operate! ","Level Crossing STATUS");
							return;
						}
					}
				}

				str.Format(" %s KEY Transmitter Release?", strLCName);
				strtitle = "LCK ON";
				bConfirm = TRUE;
				symbol = 9;
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				str.Format(" %s KEY Transmitter Lock?", strLCName);
				strtitle = "LCK OFF";
				bConfirm = TRUE;
				symbol = 8;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
	case BtnLC5B:
		if ( strBufferLCName.Find("LC5") == 0 || (strBufferLCName.Find('/') == 1))
		{
			if (nSysVar.bLC5NKXPR) 
			{
				MessageBox(" Level Crossing Opened! Cannot control this button! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVar.bLC5KXLR ) 
			{
				short nTrack = m_Engine.m_strTrack.GetCount();
				BYTE nTrackID;
				CBmpTrack *pTrackForLC;
				ScrInfoTrack *pTrackInfoForLC;
				BOOL bIsInAOverlapRoute = FALSE;
				
				for (nTrackID=1; nTrackID<=nTrack; nTrackID++)
				{
					pTrackForLC = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, nTrackID );
					pTrackInfoForLC = (ScrInfoTrack *)pTrackForLC->m_pData;
					
					bIsInAOverlapRoute = FALSE;
					
					for( int nCounter = 0; nCounter < 50; nCounter++)
					{
						if ( pTrackInfoForLC->LeftToRightRouteNo == iConflictRouteNumberForLC[nCounter] || pTrackInfoForLC->RightToLeftRouteNo == iConflictRouteNumberForLC[nCounter] 
							|| (( pTrackInfoForLC->LeftToRightRouteNo != NULL || pTrackInfoForLC->RightToLeftRouteNo != NULL ) && ConflictRouteNumberForLC[0].Find("ALL") == NULL ))
						{
							bIsInAOverlapRoute = TRUE;
							break;
						}
					}
					
					if ((!pTrackInfoForLC->TRACK && !m_Engine.m_pTrackInfoTable[nTrackID].bLCNOTINTRK) || !pTrackInfoForLC->ROUTE || bIsInAOverlapRoute == TRUE )
					{
						if(m_Engine.m_pTrackInfoTable[nTrackID].bLC5)
						{
							MessageBox(" Level Crossing Locked! Cannot operate! ","Level Crossing STATUS");
							return;
						}
					}
				}
				
				str.Format(" %s KEY Transmitter Release?", strLCName);
				strtitle = "LCK ON";
				bConfirm = TRUE;
				symbol = 9;
				d1 = (BYTE)pSysObj->m_nID;
			}
			else
			{
				str.Format(" %s KEY Transmitter Lock?", strLCName);
				strtitle = "LCK OFF";
				bConfirm = TRUE;
				symbol = 8;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
	case BtnLCA:		      // 건널목 CALL
		if ( !strcmp(pSysObj->m_szDrawName, "LCA") )
		{
			if (nSysVarExt.bLC1BELACK) 
			{ 
				MessageBox(" Level Crossing Bell Ack Already received! This button cannot be controlled! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC1BELL ) 
			{
				str.Format(" Do you want to ring a %s LC bell?", strLCName);
				bConfirm = TRUE;
				symbol = 13;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;

	case BtnLC2A:
		if ( !strcmp(pSysObj->m_szDrawName, "LCA") )
		{
			if (nSysVarExt.bLC2BELACK) 
			{ 
				MessageBox(" Level Crossing Bell Ack Already received! This button cannot be controlled! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC2BELL ) 
			{
				str.Format(" Do you want to ring a %s LC bell?", strLCName);
				bConfirm = TRUE;
				symbol = 13;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;

	case BtnLC3A:
		if ( !strcmp(pSysObj->m_szDrawName, "LCA") )
		{
			if (nSysVarExt.bLC3BELACK) 
			{ 
				MessageBox(" Level Crossing Bell Ack Already received! This button cannot be controlled! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC3BELL ) 
			{
				str.Format(" Do you want to ring a %s LC bell?", strLCName);
				bConfirm = TRUE;
				symbol = 13;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
	case BtnLC4A:
		if ( !strcmp(pSysObj->m_szDrawName, "LCA") )
		{
			if (nSysVarExt.bLC4BELACK) 
			{ 
				MessageBox(" Level Crossing Bell Ack Already received! This button cannot be controlled! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVarExt.bLC4BELL ) 
			{
				str.Format(" Do you want to ring a %s LC bell?", strLCName);
				bConfirm = TRUE;
				symbol = 13;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
	case BtnLC5A:
		if ( !strcmp(pSysObj->m_szDrawName, "LCA") )
		{
			if (nSysVar.bLC5BELACK) 
			{ 
				MessageBox(" Level Crossing Bell Ack Already received! This button cannot be controlled! ","Level Crossing STATUS");
				return;
			}
			if ( !nSysVar.bLC5BELL ) 
			{
				str.Format(" Do you want to ring a %s LC bell?", strLCName);
				bConfirm = TRUE;
				symbol = 13;
				d1 = (BYTE)pSysObj->m_nID;
			}
		}
		break;
		
    case BtnC:                // OSS 버튼(Outer 신호기 정지) 
	case BtnD:				
		str  = pSysObj->m_szDrawName;
		nLen = str.Find(".");
		strncpy(szName, str, nLen);

		pRetObj = SearchPtrByName( OBJ_SIGNAL, szName );
		d1 = (BYTE)pRetObj->m_nID;

		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pSigInfo = (ScrInfoSignal*)pSignal->m_pData;

		}
		else
			return;

		if (pSigInfo->OSS)
		{
			str = "Outer Signal (OSS) 'N' ?";
			d2 = 0; 
			symbol = 0;
			dir = 0x09;
		}
		else
		{
			str = "Outer Signal (OSS) 'R' ?";
			d2 = 1;
			symbol = 1;
			dir = 0x0C;
		}

		if (bSendMsg = CheckConfirm( str, point, strtitle, symbol )) 
	        FireOleSendMessage( WS_OPMSG_OSS, wParam, lParam );
		return;

	case BtnNOUSED:			  // Nouse 버튼	
		str  = pSysObj->m_szDrawName;
		nLen = str.Find(".");
		strncpy(szName, str, nLen);

		pRetObj = SearchPtrByName( OBJ_SWITCH, szName );

		if (pRetObj) 
		{
			pSwitch = (CBmpSwitch*)pRetObj;
			pPntInfo = (ScrInfoSwitch*)pSwitch->m_pData;
		}
		else
			return;

        temp += str.Left(nLen);
		temp += "T.1";
		strcpy(szName, temp);

		pRetObj2 = SearchPtrByName( OBJ_TRACK, szName );
		d1 = (BYTE)pRetObj->m_nID;

		if (pRetObj2) 
		{
			pTrack = (CBmpTrack*)pRetObj2;
			pTrackInfo = (ScrInfoTrack*)pTrack->m_pData;
		}
		else
			return;


		if (!pTrackInfo->ROUTE || !pTrackInfo->TRACK) 
		{
			MessageBox(" This button cannot be controlled when routes had been set! ","BUTTON STATUS");
			return;
		}

		if (pPntInfo->NOUSED)
		{
			str = "This point will be used only to reverse position when routes are setting.";
			d2 = 0; 
			symbol = 0;
			dir = 0x09;
		    FireOleSendMessage( WS_OPMSG_POS_ON, wParam, lParam );
		}
		else
		{
			str = "This point will be used only to mormal position when routes are setting.";
			d2 = 1;
			symbol = 1;
			dir = 0x0C;
		    FireOleSendMessage( WS_OPMSG_POS_OFF, wParam, lParam );
		}

		return;
    case BtnLoop1:            // Loop track 폐쇄 버튼     
	case BtnLoop2:  
		str  = pSysObj->mName;
		nLen = str.Find(".");

		strncpy(szName, str, nLen);
		pRetObj = SearchPtrByName( OBJ_TRACK2, szName );
		d1 = (BYTE)pRetObj->m_nID;

		if (pRetObj) 
		{
			pTrack = (CBmpTrack*)pRetObj;
			pTrackInfo = (ScrInfoTrack*)pTrack->m_pData;
		}
		else
			return;


#ifdef _WINDOWS
		if (pTrackInfo->INHIBIT)
		{
			if ( m_iProject == _13Stations_PJT )
			{
				str = "Loop Track (LOS) 'OFF' ?";
			}
			else
			{
				str = "Loop Track (COS) 'OFF' ?";
			}
			d2 = 0; 
			dir = 0x0C;
			symbol = 10;
		}
		else
		{
			if ( m_iProject == _13Stations_PJT )
			{
				str = "Loop Track (LOS) 'ON' ?";
			}
			else
			{
				str = "Loop Track (COS) 'ON' ?";
			}
			d2 = 1;
			dir = 0x0C;
			symbol = 11;
		}
		if (bSendMsg = CheckConfirm( str, point, strtitle, symbol )) 
	        FireOleSendMessage( WS_OPMSG_LOS, wParam, lParam );
		return;
#else
		if (pTrackInfo->INHIBIT)
		{
			if ( m_iProject == _13Stations_PJT )
			{
				str = "Loop Track (LOS) 'OFF' ?";
			}
			else
			{
				str = "Loop Track (COS) 'OFF' ?";
			}
			d2 = 0; 
			dir = 0x0C;
			symbol = 10;
		    FireOleSendMessage( WS_OPMSG_LOS_OFF, wParam, lParam );
		}
		else
		{
			if ( m_iProject == _13Stations_PJT )
			{
				str = "Loop Track (LOS) 'ON' ?";
			}
			else
			{
				str = "Loop Track (COS) 'ON' ?";
			}
			d2 = 1;
			dir = 0x0C;
			symbol = 11;
			FireOleSendMessage( WS_OPMSG_LOS_ON, wParam, lParam );
		}
		return;
#endif
	case BtnAck:				// Alarm (signal, point, approach, block)
		if ( nSysVarExt.bSound2 ) 
		{
			str  = " Do you want to switch Alarm off' ?";
			bConfirm = TRUE;
			symbol = 15;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;
	case BtnPDB:				// Buzzer (power)
		if ( nSysVarExt.bSound3 ) 
		{
			str  = " Do you want to switch Buzzer off' ?";
			bConfirm = TRUE;
			symbol = 15;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnClose:				// Station open / close
		TScrobj *pRetObj2;
		strcpy(szName,"B2");
		pRetObj = SearchPtrByName( OBJ_SIGNAL, szName );
		strcpy(szName,"B4");
		pRetObj2 = SearchPtrByName( OBJ_SIGNAL, szName );

    	if (pRetObj && pRetObj2) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			
			CBmpSignal *pSignal2 = (CBmpSignal*)pRetObj2;
			ScrInfoBlock *pBlkInfo2 = (ScrInfoBlock*)pSignal2->m_pData;
			
			if ((pBlkInfo2->CA) && (pBlkInfo2->LCRIN) && (pBlkInfo2->CONFIRM) && (!pBlkInfo2->DEPARTIN)
				&& (pBlkInfo->CA) && (pBlkInfo->LCRIN) && (pBlkInfo->CONFIRM) && (!pBlkInfo->DEPARTIN))
			{}
			else
				return;
		}
		else
			return;	

		if ( nSysVar.bStation ) // bStation = 1 이면 close
		{
			str  = " The current Station 'OPEN'?";
			bConfirm = TRUE;
			symbol = 9;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " The current Station 'CLOSE'?";
			bConfirm = TRUE;
			symbol = 8;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKdn:              // Electric point key DOWN

		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 1)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContS) // ON 상태
				{
					// 2006.05.31  BR 요구사항에 의해 궤도회로 Failure시에는 Key를 제거할 수 있도록 함...
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox( " G9-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox( " DOWN-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
				}
				if (!nSysVar.bPKeyS) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G9-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " DOWN-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}

		if ( nSysVar.bPKeyContS )   // 1이면 LOCK
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G9 - EPK OFF";
			else
				strtitle = "DEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G9 - EPK ON";
			else
				strtitle = "DEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKup:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 2)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContN)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G8-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" UP-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
				}
				if (!nSysVar.bPKeyN) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G8-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " UP-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContN )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G8 - EPK OFF";
			else
				strtitle = "UEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G8 - EPK ON";
			else
				strtitle = "UEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKA:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 11)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContA)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G1-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" A-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G1-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " A-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}

		if ( nSysVar.bPKeyContA )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G1-EPK OFF";
			else
				strtitle = "AEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G1-EPK ON";
			else
				strtitle = "AEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKB:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 12)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContB)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G2-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" B-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G2-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " B-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContB )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G2-EPK OFF";
			else
				strtitle = "BEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G2-EPK ON";
			else
				strtitle = "BEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKC:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 13)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContC)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G3-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" C-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G3-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " C-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContC )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G3-EPK OFF";
			else
				strtitle = "CEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G3-EPK ON";
			else
				strtitle = "CEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKD:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 14)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContD)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G4-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" D-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G4-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " D-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContD )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G4-EPK OFF";
			else
				strtitle = "DEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G4-EPK ON";
			else
				strtitle = "DEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKE:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 15)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContE)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G5-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" E-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G5-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " E-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContE )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G5-EPK OFF";
			else
				strtitle = "EEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G5-EPK ON";
			else
				strtitle = "EEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKF:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 16)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContF)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G6-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" F-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G6-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " F-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContF )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G6-EPK OFF";
			else
				strtitle = "FEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G6-EPK ON";
			else
				strtitle = "FEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnAPKG:				// Electric point key UP
		for (index=1; index<=m_Engine.m_TableBase.InfoSwitch.nCount; index++) 
		{
			if (m_Engine.m_pSwitchInfoTable[index].nPointKeyNo == 17)
			{
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, index );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;

				// 2005.09.08 수정, 전철기 쇄정 상태에서는 on제어만 하지 못한다.  key가 빠져 있는 경우가 아니면 off제어는 할 수 있다..
				if (!nSysVar.bPKeyContG)
				{
					if (pSwitchInfo->ROUTELOCK /*|| !pSwitchInfo->TRACKLOCK*/)
					{
						if ( m_strStationName.Find("AKHAURA",0) == 0 )
						{
							MessageBox(" G7-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
						else
						{
							MessageBox(" G-Electric Point Locked! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
							return;
						}
					}
					//break;
				}
				if (!pSwitchInfo->WKEYKR) // 2005.09.08 Key가 빠져있으면 제어하지 못한다...
				{
					if ( m_strStationName.Find("AKHAURA",0) == 0 )
					{
						MessageBox( " G7-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
					else
					{
						MessageBox( " G-Electric Point key released! Cannot be operated! ","BUTTON STATUS");	// Key operation 불가 Message box 발생
						return;
					}
				}
			}
		}
		if ( nSysVar.bPKeyContG )   // 1이면 reverse
		{
			str  = " KEY Transmitter Lock?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G7-EPK OFF";
			else
				strtitle = "GEPK OFF";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		else
		{
			str  = " KEY Transmitter Release?";
			if ( m_strStationName.Find("AKHAURA",0) == 0 )
				strtitle = "G7-EPK ON";
			else
				strtitle = "GEPK ON";
			bConfirm = TRUE;
			symbol = 3;
			d1 = (BYTE)pSysObj->m_nID;
		}
		break;

	case BtnB1ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B1" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TGB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBGPR )
		{
			if (pBlkInfo->CA || pBlkInfo->LCR) 
			{
				MessageBox(" TGB! Now Using! ","BLOCK STATUS");	
				return;
			}

			else if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TGB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB2ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B2" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TCB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBCPR)
		{
			if (pBlkInfo->CA || pBlkInfo->LCR)
			{
				MessageBox(" TCB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TCB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB3ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B3" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			if((!pBlkInfo->SAXLACTIN && (!pBlkInfo->AXLOCC || !pBlkInfo->AXLDST)) || (pBlkInfo->SAXLACTIN && (!pBlkInfo->SAXLOCC || !pBlkInfo->SAXLDST)))
			{
				MessageBox(" TGB Block Axle Counter is not cleared.", "BLOCK STATUS");
				return;
			}

			str  = " TGB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBGPR)
		{
			if ((pBlkInfo->CA || pBlkInfo->LCR) && (m_strStationName.Find("CHINKI", 0) != 0))
			{
				MessageBox(" TGB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if((!pBlkInfo->SAXLACTIN && (!pBlkInfo->AXLOCC || !pBlkInfo->AXLDST)) || (pBlkInfo->SAXLACTIN && (!pBlkInfo->SAXLOCC || !pBlkInfo->SAXLDST)))
			{
				MessageBox(" TGB Block Axle Counter is not cleared.", "BLOCK STATUS");
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TGB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB4ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B4" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			if((!pBlkInfo->SAXLACTIN && (!pBlkInfo->AXLOCC || !pBlkInfo->AXLDST)) || (pBlkInfo->SAXLACTIN && (!pBlkInfo->SAXLOCC || !pBlkInfo->SAXLDST)))
			{
				MessageBox(" TGB Block Axle Counter is not cleared.", "BLOCK STATUS");
				return;
			}

			str  = " TCB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBCPR)
		{
			if ((pBlkInfo->CA || pBlkInfo->LCR) && (m_strStationName.Find("BHAIRAB", 0) != 0) && (m_strStationName.Find("CHINKI", 0) != 0))
			{
				MessageBox(" TCB! Now Using! ","BLOCK STATUS");	
				return;
			}

			if((!pBlkInfo->SAXLACTIN && (!pBlkInfo->AXLOCC || !pBlkInfo->AXLDST)) || (pBlkInfo->SAXLACTIN && (!pBlkInfo->SAXLOCC || !pBlkInfo->SAXLDST)))
			{
				MessageBox(" TCB Block Axle Counter is not cleared.", "BLOCK STATUS");
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				if ((pBlkInfo->LCR) && (m_strStationName.Find("BHAIRAB", 0) == 0))
				{
					str  = "Set Forced Train Arrival For TCB?";
				}
				else
				{
					str  = " TCB CLEAR REQUEST?";
				}

				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB11ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B11" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TGB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBGPR)
		{
			if (pBlkInfo->CA || pBlkInfo->LCR) 
			{
				MessageBox(" TGB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TGB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB12ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B12" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TCB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBCPR)
		{
			if (pBlkInfo->CA || pBlkInfo->LCR) 
			{
				MessageBox(" TCB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TCB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB13ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B13" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TGB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBGPR)
		{
			if (pBlkInfo->CA || pBlkInfo->LCR) 
			{
				MessageBox(" TGB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TGB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB14ClearReq:				// Block Button
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B14" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
			d2 = (BYTE)pRetObj->m_nID;
		}
		else
			return;
		
		if ( pBlkInfo->BLKRSTREQIN )
		{
			str  = " TCB CLEAR ACCEPT?";
			strtitle = "BLOCK Control";
			symbol = 14;
			if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
			{
				d1 = BLKRST_CMD_ACC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			else
			{
				d1 = BLKRST_CMD_DEC;
				FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
			}
			bSendMsg = 0;
		}
		else if (pBlkInfo->RBCPR)
		{
			if (pBlkInfo->CA || pBlkInfo->LCR) 
			{
				MessageBox(" TCB! Now Using! ","BLOCK STATUS");	
				return;
			}
			
			if ( !pBlkInfo->BLKRSTREQ )
			{
				str  = " TCB CLEAR REQUEST?";
				strtitle = "BLOCK Control";
				symbol = 14;
				if (bSendMsg = CheckConfirm( str, point, strtitle, symbol ))
				{
					d1 = BLKRST_CMD_REQ;
					FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
				}
			}
			bSendMsg = 0;
		}
		break;
	case BtnB1AXLRSTACC:
		d1 = (BYTE)pSysObj->m_nID;
		ndl = (short)(point.x);
		ndh = (short)(point.y);
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B1" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
		}
		else
			return;
		
		if ( pBlkInfo == NULL)
			return;
		else
		{
			if ( (pBlkInfo->SAXLACTIN && pBlkInfo->SAXLREQ) || (!pBlkInfo->SAXLACTIN && pBlkInfo->AXLREQ) )
			{
				CString strRealName = pSysObj->mName;
				
				if( strRealName.Find('@') > 0)
				{
					strRealName = strRealName.Left(strRealName.Find('@'));
				}
				str.Format(" RESET '%s' ?",strRealName); 
				strtitle = "AXLE Reset";
				symbol = 16;
				if ( !CheckConfirm( str, point, strtitle, symbol ) )
				{
					d1++;
				}
				if ( pBlkInfo->SAXLACTIN )
				{
					d2|=0x80;
				}
				FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam );
			}
			else
				MessageBox("Cannot reset now!", "AXL STATUS");
		}
		break;
	case BtnB2AXLRST:
		d1 = (BYTE)pSysObj->m_nID;
		ndl = (short)(point.x);
		ndh = (short)(point.y);

		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B2" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
		}
		else
			return;

		if ( (pBlkInfo->SAXLACTIN && !pBlkInfo->SAXLOCC) || (!pBlkInfo->SAXLACTIN && !pBlkInfo->AXLOCC) )
		{
			if ( pBlkInfo->SAXLACTIN )
			{
				d2|=0x80;
			}

			FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam);
		}
		else
			MessageBox("Cannot reset now!", "AXL STATUS");
		break;
	case BtnB3AXLRSTACC:
		d1 = (BYTE)pSysObj->m_nID;
		ndl = (short)(point.x);
		ndh = (short)(point.y);
		
		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B3" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
		}
		else
			return;
		
		if ( pBlkInfo == NULL)
			return;
		else
		{
			if ( (pBlkInfo->SAXLACTIN && pBlkInfo->SAXLREQ) || (!pBlkInfo->SAXLACTIN && pBlkInfo->AXLREQ) )
			{
				CString strRealName = pSysObj->mName;

				if( strRealName.Find('@') > 0)
				{
					strRealName = strRealName.Left(strRealName.Find('@'));
				}
				str.Format(" RESET '%s' ?",strRealName); 
				strtitle = "AXLE Reset";
				symbol = 16;
				if ( !CheckConfirm( str, point, strtitle, symbol ) )
				{
					d1++;
				}
				if ( pBlkInfo->SAXLACTIN )
				{
					d2|=0x80;
				}
				FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam );
			}
			else
				MessageBox("Cannot reset now!", "AXL STATUS");
		}
		break;
	case BtnB4AXLRST:
		d1 = (BYTE)pSysObj->m_nID;
		ndl = (short)(point.x);
		ndh = (short)(point.y);

		pRetObj = SearchPtrByName( OBJ_SIGNAL, "B4" );
		
		if (pRetObj) 
		{
			pSignal = (CBmpSignal*)pRetObj;
			pBlkInfo = (ScrInfoBlock*)pSignal->m_pData;
		}
		else
			return;

		if ( (pBlkInfo->SAXLACTIN && !pBlkInfo->SAXLOCC) || (!pBlkInfo->SAXLACTIN && !pBlkInfo->AXLOCC) )
		{
			if ( pBlkInfo->SAXLACTIN )
			{
				d2|=0x80;
			}

			FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam);
		}
		else
			MessageBox("Cannot reset now!", "AXL STATUS");
		break;
	case BtnTKAXLRST:
		ndl = (short)(point.x);
		ndh = (short)(point.y);
		
		pRetObj = SearchPtrByName( OBJ_TRACK2, cRealName );
		d1 = (BYTE)pRetObj->m_nID;
		
		if (pRetObj) 
		{
			pTrack = (CBmpTrack*)pRetObj;
			pTrackInfo = (ScrInfoTrack*)pTrack->m_pData;
		}
		else
			return;
		
		if ( pTrackInfo == NULL)
			return;
		else
		{
			if ( pTrackInfo->RESETBUF || m_strStationName == "ASHUGANJ")
			{
				str.Format(" RESET '%s' ?",cRealName); 
				strtitle = "AXLE Reset";
				symbol = 16;
				d2 = CheckConfirm( str, point, strtitle, symbol );
				if ( d2 == 0 )
				{
					d2 = 2;
				}
				FireOleSendMessage( WS_OPMSG_TRACK_RESET, wParam, lParam );
			}
			else
				MessageBox("Cannot reset now!", "AXL STATUS");
		}
		break;
	case BtnSys1:			// event
	case BtnSys2:			// event
	case BtnGnd:				// Ground fail
	case BtnS:				// event
	case BtnP:				// event
	case BtnB24:			// event
	case BtnFuse:			// event
	case BtnUps1:			// event
	case BtnUps2:			// event
	case BtnPower:			// event
	case BtnGen:			// event
	case BtnComm:			// event
		break;

	default:
		break;
	
	}

	if ( bConfirm ) 
	{
		bSendMsg = CheckConfirm( str, point,strtitle,symbol );
	}
	if ( bSendMsg ) 
	{
		ndl = (short)(point.x);
		ndh = (short)(point.y);
		FireOleSendMessage( WS_OPMSG_BUTTON, wParam, lParam );
	}
}

void CLSMCtrl::OnLButtonUp(UINT nFlags, CPoint point) 
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	OnMouseMove(nFlags, point);//////// 2012.08.31 클릭시 손모양이 없어지기때문에 추가..///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	return;
	COleControl::OnLButtonUp(nFlags, point);
}

void CLSMCtrl::OnMouseMove(UINT nFlags, CPoint point) 
{

//	if (GetCapture() != this) {
//		return;
//	}

	CLSMApp *pApp = (CLSMApp *)AfxGetApp();
	
	if( pApp->m_iMySystemNo == 3)
		return;

	if (m_pPressObj && (nFlags & MK_LBUTTON)) 
	{
		if (!m_pPressObj->PtInRegion( point )) 
		{
			m_pPressObj->m_IsPress = 0;
			InvalidateRgn( m_pPressObj, FALSE );
			m_pPressObj = NULL;
		}
	}
	for (short i=0; i<m_ScrObject.GetSize(); i++ ) 
	{
		TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
		TScrobj *pObj = obj->PtInArea( point );
		if (pObj) 
		{
            if ((pObj->m_cObjectType == (BYTE)OBJ_TRACK)) 
			{
				if ( pDataHead->LCCStatus.bSIMModeIO)
					SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
				else
					SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_CHECK));
//				SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
			}

			if((pObj->m_nID == BtnPas) || (pObj->m_nID == BtnSysRun)){}
			else
			{	
				if (m_cOption & OPTION_NOCONTROL) return;
				if ( m_bLOCK ) return;
			}
			if (m_bSysOptLOCK && !isAlarm(pObj->m_nID)) return;
			int bSig = 0;
			if (m_bStation) // Station Close
			{
				CString str = pObj->m_szDrawName;
				int b = str.Find('B');
				int x = str.Find('X');
				if (b == NULL && x == NULL && !m_bOptLOCK)
				{
					if ( ((CBmpSignal*)pObj)->IsAble() ) 
					{
						bSig = 1;
						SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
					}
				}
				else if ( ((pObj->m_cObjectType == (BYTE)OBJ_SYSPUSH) && 
					((!m_bSysOptLOCK || isAlarm(pObj->m_nID)) || (!m_bPasOut && (pObj->m_nID == BtnClose)) )
					) ) 
				{
					SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
				}
				else
					return;
			}

			if ((pObj->m_cObjectType == (BYTE)OBJ_SIGNAL) && !m_bOptLOCK)
			{
				if ( ((CBmpSignal*)pObj)->IsAble() ) 
					bSig = 1;
			}
			if (  (  (bSig || (pObj->m_cObjectType == (BYTE)OBJ_SWITCH)  ) && !m_bOptLOCK) 

				|| ( (pObj->m_cObjectType == (BYTE)OBJ_SYSPUSH) && 
				
				(  (!m_bSysOptLOCK || isAlarm(pObj->m_nID)) || (!m_bPasOut && (pObj->m_nID == BtnSysRun)) )
				
				) ) 
			{
				CString strPoint = pObj->mName;
				BOOL bIsExeptedXPoint = FALSE;
				if ( m_strStationName.Find("MYMENSINGH")==0 && (strPoint.Find("117X") ==0 || strPoint.Find("118X")==0))
					bIsExeptedXPoint = TRUE;
				CString strBIsIncludedInItsName = pObj->mName;
				if ( (pObj->m_cObjectType == (BYTE)OBJ_SWITCH && ( strBIsIncludedInItsName.Find('B') > 1 || strBIsIncludedInItsName.Find('X') > 1 )) && (bIsExeptedXPoint == FALSE));
				else
				{
					if ( pDataHead->LCCStatus.bSIMModeIO)
					{
						if ((pObj->m_cObjectType == (BYTE)OBJ_SWITCH) || (pObj->m_cObjectType == (BYTE)OBJ_SIGNAL))
						{
							SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
						}
					}
					else
					{
						SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_HAND));
					}
				}
			}
			return;
		}
	}
	COleControl::OnMouseMove(nFlags, point);
}

void CLSMCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	OnMouseMove(nFlags, point);//////// 2012.08.31 클릭시 손모양이 없어지기때문에 추가..///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _EIPSIM
	for (short i=0; i<m_ScrObject.GetSize(); i++ ) 
		{
			TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
			TScrobj *pObj = obj->PtInArea( point );

			if (pObj ) 
			{
				if((pObj->m_nID == BtnPas) || (pObj->m_nID == BtnSysRun)){}
				else
				{
					if (m_cOption & OPTION_NOCONTROL) return;
					if ( m_bLOCK ) return;
				}

     			if (m_bSysOptLOCK && !isAlarm(pObj->m_nID)) return;
				BYTE type = pObj->m_cObjectType;
				switch( type ) {
				case OBJ_SIGNAL :
					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK) {
						if ( ((CBmpSignal *)pObj)->IsAble() ) {
							DoSignalEvent( (CBmpSignal *)pObj, point );
						}
					}
					break;

				case OBJ_SWITCH :
					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK) {
						DoSwitchEvent( (CBmpSwitch *)pObj, point );
					}
					break;
				case OBJ_TRACK :
					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK) {
						DoTrackEvent( (CBmpTrack*)pObj, point );
					}
					break;

				case OBJ_SYSPUSH :
					if ( !(m_cOption &  OPTION_SINGLECLICK) ) {
						if (( !m_bSysOptLOCK || isAlarm(pObj->m_nID)) || (!m_bPasOut && (pObj->m_nID == BtnSysRun)) ) {
							DoSystemButtonEvent( pObj, point );
						}
					}
					break;
				}
                break;
			}
		}
#else
#endif
	return;
 	COleControl::OnLButtonDown(nFlags, point);
}

void CLSMCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{

//	if (GetCapture() == this) {
//		ReleaseCapture();

	CLSMApp *pApp = (CLSMApp *)AfxGetApp();
	
	if( pApp->m_iMySystemNo == 3 /*|| pDataHead->LCCStatus.bSIMModeIO*/ )
		return;

	if ( m_strStationName.Find("@StandardOf13") > 0 || m_strStationName.Find("MYMENSINGH_JN") == 0 )
	{
		m_iProject = _13Stations_PJT;
	}
	else if ( m_strStationName.Find("@StandardOfLC") > 0 || m_strStationName.Find("LAKSAM") == 0 )
	{
		m_iProject = _Laksam_PJT;
	}
	else if ( m_strStationName.Find("@Standard") > 0 || m_strStationName.Find("BHAIRAB_BAZAR") == 0 )
	{
		m_iProject = _Tongi_PJT;
	}

	for (short i=0; i<m_ScrObject.GetSize(); i++ ) 
		{
			TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
			TScrobj *pObj = obj->PtInArea( point );
			
			if (pObj ) 
			{
				if((pObj->m_nID == BtnPas) || (pObj->m_nID == BtnSysRun)){}
				else
				{
					if (m_cOption & OPTION_NOCONTROL) return;
					if ( m_bLOCK ) return;
				}

     			if (m_bSysOptLOCK && !isAlarm(pObj->m_nID)) return;
				BYTE type = pObj->m_cObjectType;
				CString str;
				CString strObjectName = pObj->mName;
				int b;
				BOOL bIsExeptedXPoint = FALSE;
				char *pName = pObj->mName;

				if (m_bStation) // Station Close
				{
					str = pObj->m_szDrawName;
					b = str.Find("B");
					if (!(m_cOption & OPTION_SINGLECLICK) && (b == NULL && !m_bOptLOCK)  && type == OBJ_SIGNAL)
					{
						if ( ((CBmpSignal*)pObj)->IsAble() ) 
							DoSignalEvent( (CBmpSignal *)pObj, point );
						break;
					}
					else if ( !(m_cOption &  OPTION_SINGLECLICK) )
					{
						if ( ((pObj->m_cObjectType == (BYTE)OBJ_SYSPUSH) && 
						((!m_bSysOptLOCK || isAlarm(pObj->m_nID)) || (!m_bPasOut && (pObj->m_nID == BtnClose)) )
						) )
						DoSystemButtonEvent( pObj, point );
						break;
					}
					else
						return;
				}
				switch( type ) {
				case OBJ_SIGNAL :
					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK) {
						if ( ((CBmpSignal *)pObj)->IsAble() ) {
							if ( pDataHead->LCCStatus.bSIMModeIO )
								FireOleSendMessage( WS_OPMSG_CONT_IOSIM, (WPARAM)type, (LPARAM)pName);
							else if ( pDataHead->nSysVar.bMode )
								MessageBox("You are on CTC mode!","LES");
							else
								DoSignalEvent( (CBmpSignal *)pObj, point );
						}
					}
					break;

				case OBJ_SWITCH :
					if ( m_strStationName.Find("MYMENSINGH")==0 && (strObjectName.Find("117X") ==0 || strObjectName.Find("118X")==0))
						bIsExeptedXPoint = TRUE;

					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK && strObjectName.Find('B') < 0 && (strObjectName.Find('X') < 0 || bIsExeptedXPoint == TRUE )) {
						if ( pDataHead->LCCStatus.bSIMModeIO )
							FireOleSendMessage( WS_OPMSG_CONT_IOSIM, (WPARAM)type, (LPARAM)pName);
						else if ( pDataHead->nSysVar.bMode )
							MessageBox("You are on CTC mode!","LES");
						else
							DoSwitchEvent( (CBmpSwitch *)pObj, point );
					}
					break;
				case OBJ_TRACK :
					str = pObj->m_szDrawName;
					b = str.Find("X");
					if (b > 0) 
					{
						return;
					}

					if (!(m_cOption & OPTION_SINGLECLICK) && !m_bOptLOCK) {
						if ( pDataHead->LCCStatus.bSIMModeIO )
							FireOleSendMessage( WS_OPMSG_CONT_IOSIM, (WPARAM)type, (LPARAM)pName);
						else if ( pDataHead->nSysVar.bMode )
							MessageBox("You are on CTC mode!","LES");
						else
							DoTrackEvent( (CBmpTrack*)pObj, point );
					}
					break;

				case OBJ_SYSPUSH :
					if ( !(m_cOption &  OPTION_SINGLECLICK) ) {
						if (( !m_bSysOptLOCK || isAlarm(pObj->m_nID)) || (!m_bPasOut && (pObj->m_nID == BtnSysRun)) ) {
							if ( pDataHead->LCCStatus.bSIMModeIO )
								return;
							else if ( pDataHead->nSysVar.bMode && (BYTE)pObj->m_nID != BtnLocalMode )
								MessageBox("You are on CTC mode!","LES");
							else
								DoSystemButtonEvent( pObj, point );
						}
					}
					break;
				}
                break;
			}
		}
	COleControl::OnLButtonDblClk(nFlags, point);
}

#include "SignalStop.h"

short CLSMCtrl::Signalstop( CString &str, CString &release, CString &restr, CPoint &point )
{
	short result = 0;
	CSignalStop dlg;
	dlg.m_Position = point;
	dlg.m_strText = str;
	dlg.m_retryText = restr;
	dlg.m_releaseText = release;
	dlg.m_nSelectType = 0;
	dlg.SetWindowText( str );
	
	if ( dlg.DoModal() == IDOK ) {
		result = dlg.m_nSelectType + 1;
	}
	return result;
}

#include "PointControl.h"

short CLSMCtrl::PointControl( CString &str, CString &move, CString &block, CPoint &point )
{
	short result = 0;
	CPointControl dlg;
	dlg.m_Position = point;
	dlg.m_strText = str;
	dlg.m_MoveText = move;
	dlg.m_BlockText = block;
	dlg.m_nSeletType = 0;
	dlg.SetWindowText( str );
	
	if ( dlg.DoModal() == IDOK ) {
		result = dlg.m_nSeletType + 1;
	}
	return result;
}


BOOL CLSMCtrl::CheckConfirm( CString &str, CPoint &point, CString &strwindow, int symbol )
{
	if (m_cOption & OPTION_NOCONFIRM) return TRUE;
	CConfirmDlg dlg( NULL);
    switch( symbol ) {
	  case 0 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALON);
		  break;
	  case 1 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALOFF);
		  break;
	  case 2 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_WARN);
		  break;
	  case 3 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SWITCH);
		  break;
	  case 4 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SYSTEM);
		  break;
	  case 5 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_OPERATE);
		  break;
	  case 6 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_DAY);
		  break;
	  case 7 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_NIGHT);
		  break;
	  case 8 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_LOCK);
		  break;
	  case 9 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_UNLOCK);
		  break;
	  case 10 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_TRACKON);
		  break;
	  case 11 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_TRACKOFF);
		  break;
      case 12 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_KEY);
		  if (str.Find("CTC MODE") > 0)
		  {
			  dlg.m_strOK = "ACCEPT";
			  dlg.m_strCancel = "DECLINE";
		  }
		  break;
      case 13 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_CA);
		  break;
      case 14 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_BLOCK);
		  break;
      case 15 : 
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_ALROFF);
		  break;
	  case 16:
		  dlg.hIcon = AfxGetApp()->LoadIcon(IDI_BLOCK);
		  dlg.m_strOK = "ACCEPT";
		  dlg.m_strCancel = "DECLINE";
		  break;
		}
	dlg.m_Position = point;
	dlg.m_strText = str;
	dlg.m_strWindow = strwindow;
	return ( dlg.DoModal() == IDOK );
}

void CLSMCtrl::OnUpdateDest(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(FALSE);		
}


/////////////////////////////////////////////////////////////////////////////
#include "../include/OPMsgCode.h"

long CLSMCtrl::OlePostMessage(long message, long wParam, long lParam) 
{
	// TODO: Add your dispatch handler code here
	union {
		long wData;
		struct {
			BYTE  d1;
			BYTE  d2;
			BYTE  d3;
			BYTE  d4;
		};
	};

	union {
		long lData;
		struct {
			short ndl;
			short ndh;
		};
	};

	char  *pbuf;
	short i;
	TScrobj *obj;
	CPoint point;

	switch ( message ) {
	case WS_OPMSG_CALLCHECKROUTE:	// 90 Call Check route in EipEmp class
		lData = wParam;
		ndl = m_Engine.CheckRoute( ndl );	// nRouteID
		ndh = 0;
		return lData;
//		break;
	case WS_OPMSG_GETSCRINFO:		// 98 Get ScrInfo class Table ptr
		return (long)m_pScrInfo;
		break;
	case WS_OPMSG_GETEMUPTR:		// 99
		return (long)&m_Engine;
		break;
	case WS_OPMSG_RESET: 			// 2		// 시스템 재기동
		d2 = WS_OPMSG_RESET;
		d1 = ~d2;
		FireOleSendMessage( WS_OPMSG_RESET, wData, lParam );
		break;
	case WS_OPMSG_SETTRAINNAME:		// 4		// 열차 번호 지정
		break;

	case WS_OPMSG_SIGNALDEST:		// 5		// main 신호 취급
		OperateSignalOn( wParam );
		break;

	case WS_OPMSG_SIGNALCANCEL:		// 6		// 신호 (폐색 포함)취소
		OperateSignalOff( wParam );
		break;
	case WS_OPMSG_BLOCK:            // 27       // Block control
		OperateBlockOn( wParam );
		break;

	case WS_OPMSG_PAS:
		wParam = 0;
        lParam = 0;
		if ( m_bPasOut ) 
		{
            FireOleSendMessage( WS_OPMSG_PAS_IN, wParam, lParam );
		}
		else
		{
            FireOleSendMessage( WS_OPMSG_PAS_OUT, wParam, lParam );
		}
		break;

	case WS_OPMSG_NOUSE:
		break;

	case WS_OPMSG_SWTOGGLE:			// 9		// 선로전환기 토글(toggle)
		OperateSwitch( wParam );
		break;

	case WS_OPMSG_BUTTON:			// 10		// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
		wData = wParam;
		obj = NULL;
		for (i=0; i<m_ScrObject.GetSize(); i++ ) 
		{
			obj = (TScrobj *)m_ScrObject.GetAt( i );
			if ( obj ) 
			{
				if ( d1 && (obj->m_cObjectType == OBJ_SYSPUSH) && ((BYTE)obj->m_nID == d1) ) 
					break;
				else 
					obj = NULL;
			}
		}
		if ( obj ) 
		{
			point.x = _nSFMViewXSize / 2;
			point.y = _nSFMViewYSize / 2;
			
			DoSystemButtonEvent( obj, point );
		}
		else 
		{
			pbuf = (char*)lParam;
			ndl = *(short*)pbuf;
			pbuf += sizeof(short);
			ndh = *(short*)pbuf;
			pbuf += sizeof(short);
			if ( OpMsgConfirm( *(const char**)pbuf, ndl, ndh ) ) 
			{
				 FireOleSendMessage( WS_OPMSG_BUTTON, wParam, lParam );
			}
		}
		break;

	case WS_OPMSG_MARKINGTRACK:		// 14		// 궤도 사용금지/해제(모터카, 사용금지, 단선)
		FireOleSendMessage( WS_OPMSG_MARKINGTRACK, wParam, lParam );
		break;
	case WS_OPMSG_FREESUBTRACK:		// 15		// 구분진로비상해정
		wData = wParam;
		d2 = ROUTELOCK_FREE;
		FireOleSendMessage( WS_OPMSG_FREESUBTRACK, wData, lParam );
		break;
	case WS_OPMSG_LOADFILE:			// 32		// SFM Load
		return OpMsgLoadFileInfo( wParam, lParam );		// sfm Name string, sfm Mode string.
		break;
	case WS_OPMSG_ASSIGNSCRINFO:	// 33		// Assign Info(Down.bin)
		return AssignScrInfo( lParam );
		break;
	case WS_OPMSG_UPDATESCREEN:		// 34		// Update VMem
		ScreenUpdate( lParam );
		break;
	case WS_OPMSG_SETLSMOPTION:	// 36		// LSM Option
		lData = wParam;
		SetLSMOption( ndl );
		break;
	case WS_OPMSG_TRKSEL:			// 37		// 궤도 선택
		break;
	case WS_OPMSG_OPERATESTATE:
		lData = lParam;
		m_bOptLOCK = ( ndl ) ? TRUE : FALSE;
		m_bLOCK = ( ndh ) ? TRUE : FALSE;
		break;
	case WS_OPMSG_SETDISPTIME:		// 60	//로그시간 표시 유무 지정
		lData = wParam;
		if ( ndl ) 
			m_bDispSaveTime = TRUE;
		else 
			m_bDispSaveTime = FALSE;
		break;
	case WS_OPMSG_AXL_RESET:
        FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam );
		break;
	case WS_OPMSG_TRACK_RESET:
        FireOleSendMessage( WS_OPMSG_AXL_RESET, wParam, lParam );
		break;
	case WS_OPMSG_BLOCK_CLEAR:
        FireOleSendMessage( WS_OPMSG_BLOCK_CLEAR, wParam, lParam );
		break;

	default:
		break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CLSMCtrl::OpMsgConfirm( const char *pstr, short x, short y)
{
	CString str = pstr;
	CPoint  point( x, y );
	CString strtitle;
	strtitle = "OP Confirm";
	return CheckConfirm( str, point,strtitle,5 );
}

BOOL CLSMCtrl::OpMsgConfirm( const char *pstr, long xy)
{
	union {
		long lData;
		struct {
			short dl;
			short dh;
		};
	};
	lData = xy;
	CString str = pstr;
	CPoint  point( dl, dh );
	CString strtitle;
	strtitle = "OP Confirm";
	return CheckConfirm( str, point,strtitle,5 );
}

/////////////////////////////////////////////////////////////////////////////

void CLSMCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	for (short i=0; i<m_ScrObject.GetSize(); i++ ) 
	{
		TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
		TScrobj *pObj = obj->PtInArea( point );

		if (pObj) 
		{
			BYTE type = pObj->m_cObjectType;
			switch( type ) 
			{
			case OBJ_TRACK :
					ScrInfoTrack *pTrackInfo = (ScrInfoTrack*)pObj->m_pData;
					if (pTrackInfo == NULL || pTrackInfo->INHIBIT == 1)
						return; 
					FireOperate( WS_OPMSG_TRKSEL, pObj->m_nID );
					return;
			}
		}
	}

	return;
	COleControl::OnRButtonDown(nFlags, point);
}

void CLSMCtrl::OnMove(int x, int y) 
{
	COleControl::OnMove(x, y);
	GetWindowRect( &m_WndRect );
	_WndRect = m_WndRect;
}

void CLSMCtrl::OnSize(UINT nType, int cx, int cy) 
{
	COleControl::OnSize(nType, cx, cy);
	GetWindowRect( &m_WndRect );
	_WndRect = m_WndRect;
}

void CLSMCtrl::OnRButtonDblClk(UINT nFlags, CPoint point) 
{
	if ( m_cOption & OPTION_VIEWINFO ) {
		for (short i=0; i<m_ScrObject.GetSize(); i++ ) {
			TScrobj *obj = (TScrobj*)m_ScrObject.GetAt( i );
			TScrobj *pObj = obj->PtInArea( point );
			if (pObj) {
				BYTE type = pObj->m_cObjectType;
				switch( type ) {
				case OBJ_TRACK :
					ViewObjectInfo( (CBmpTrack*)pObj );
					break;
				}
				break;
			}
		}
	}
	COleControl::OnRButtonDblClk(nFlags, point);
}

void CLSMCtrl::ViewObjectInfo( CBmpTrack* pTrack )
{
   CObjectInfoDlg* dialog = new CObjectInfoDlg;
	dialog->m_pCtrl = this;
	dialog->m_pTrack = pTrack;
	dialog->Create(IDD_DIALOG_INFO,this);
	dialog->ShowWindow(SW_SHOW);
	
	UpdateData(FALSE);
}

void ChangeMapping(CPoint &p1,CPoint &p2)	// NULL FUNCTION FOR LSM
{	
}

RECT  _WndRect;
void GlovalShowWindow( CWnd *pWnd, int x, int y, int wx, int wy ) {
	if ( x + wx > _WndRect.right - 10 ) {
		x = _WndRect.right - wx - 10;
	}
	if ( y + wy > _WndRect.bottom - 10 ) {
		y = _WndRect.bottom - wy - 10;
	}
	if ( x < 10 ) x = 10;
	if ( y < 10 ) y = 10;
	pWnd->MoveWindow( x, y, wx, wy );
	pWnd->ShowWindow( SW_SHOW );
}

void CLSMCtrl::ObjectModify( long nObject )
{
	FireOleSendMessage( WS_OPMSG_OBJECTMODIFY, 0, nObject );
}

BOOL CLSMCtrl::GetMenuStatus() 
{
	return m_bMenuStatus;
}

void CLSMCtrl::DisplayClock(BOOL bDisplayClock) 
{
	m_bDisplayClock = bDisplayClock;
}

void CLSMCtrl::DisplayBG(short iStationNo) 
{
	// TODO: Add your dispatch handler code here
	m_iDispBG = iStationNo;
}

void CLSMCtrl::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu) 
{
	COleControl::OnMenuSelect(nItemID, nFlags, hSysMenu);

#if 1
	// TODO: Add your message handler code here
		typedef struct tagRouteBasPtr 
	{
		WORD	nOfsSignal;
		WORD	nOfsTrack;
		WORD	nOfsRoute;
		WORD	nOfsAspect;
		WORD	nOfsBlockNormal;
		WORD	n3;
		WORD	n4;
		WORD	n5;
	} RouteBasPtr;

	union {
		long lData;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
		struct {
			short dl;
			short dh;
		};
	};

	lData = 0;
	int nID = nItemID - 0x1000;

	char szName[10] = "\0";

	if ( (m_listRouteMenu.GetCount() > (int)nID) && (0 <= nID) ) 
	{
		CString sigName = m_pLastSignal->mName;
		CString str = sigName;
		CString strRouteName;
		CString strButton = *m_listRouteMenu.GetAtIndex( nID );

		if ( strButton == "=" ) 
		{
			m_listRouteMenu.Purge();
			return;
		}

		int n = strButton.Find( ',' );
		int nRouteOfs = atoi( (char*)(LPCTSTR)strButton + n + 1 );

        int nn = 0, cs=0;
        nn = strButton.Find( '(' );
		if (strButton.GetAt(nn+1) == 'C')
			cs = 1;
		else if (strButton.GetAt(nn+1) == 'S')
			cs = 2;

// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
//		if (nn > 0)
//		    strButton = strButton.Left( nn );
//		else
		    strButton = strButton.Left( n );


		// 2006.10.19 Akhaura역의 58:52(M), 58:52(C) 진로 설정 제한을 위해 삽입함.		
// 		if ( m_strStationName.Find ( "BYPASS",0 )==0 && sigName == "58")
// 		{
// 			if (pDataHead != NULL)
// 			{
// 				SystemStatusType &nSysVar = pDataHead->nSysVar;
// 				if (strButton == "52")
// 				{
// 					if (!nSysVar.bMainRoute)     // main이 설정되어 있지 않음.  Callon만 가능
// 					{
// 						if (cs != 1)
// 							return;
// 					}
// 					else     // main이 설정되어 있음.  main만 가능
// 					{
// 						if (cs == 1)
// 							return;
// 					}
// 				}
// 			}
// 		}
/////////////////////////////////////////////////////////////////////////////
// 2013.10.29 갈릭 코멘트에 의해 오버랩 진로를 강제로 선택할 수 있도록 변경.
//		str += ":";
//		str += strButton;
		strRouteName = strButton;

// 		if (nn > 0)
// 		{
// 			if (cs == 1)
// 			    strRouteName += "(C)";
// 			else
// 			    strRouteName += "(S)";
// 		}
// 		else
// 			strRouteName += "(M)";
///////////////////////////////////////////////////////////////////////////////

		BYTE count, nSigID, sCount=0;

		strcpy(szName, sigName);
		CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByName( OBJ_SIGNAL, szName );
		nSigID = (BYTE)pSignal->m_nID;

		BYTE nRouteCount = m_Engine.m_pSignalInfoTable[ nSigID ].nRouteCount;

		if (nRouteCount < 6 || (strRouteName.Find('(') > 0 && strRouteName.Find(')') > 0))		// 20130809 왜 6개이하로 제한했는지 모르겠으나 shunt진로가 6개 이상인 것이 많기에 수정.
			str = strRouteName;
		else
		    str += "(M)";

		WORD wRouteNo = m_Engine.m_pSignalInfoTable[ nSigID ].wRouteNo;

		// 2006.10.19 Akhaura역 58신호기의 52 진로 방향 Main 진로를 막기 위해...
		if ( m_strStationName.Find ( "AKHA",0 )==0 && sigName == "52")
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRouteNo ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블

			CBmpTrack *pTrack = (CBmpTrack *)SearchPtrByID( OBJ_TRACK, pInfo[RT_OFFSET_FROM] );
			ScrInfoTrack *pTrkInfo = (ScrInfoTrack*)pTrack->m_pData;
			if (pTrkInfo->INHIBIT && cs==0 )
			{
				return;
			}
		}

		for(count=0; count<nRouteCount; count++)
		{
			UINT nInfoPtr = *(UINT*)&m_Engine.m_pRouteAddTable[ wRouteNo+count ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			BYTE *pInfo = &m_Engine.m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			BYTE nButtonID = pInfo[RT_OFFSET_BUTTON];

			if ( (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_CALLON) || (pInfo[ RT_OFFSET_ROUTESTATUS ] & RT_SHUNTSIGNAL) )
				continue;

			CBmpButton *pButton = (CBmpButton *)SearchPtrByID( OBJ_BUTTON, (short)nButtonID );
			if (strButton != pButton->m_szDrawName)
				continue;
                 
			sCount += 1;
			BYTE *p = pInfo + RT_OFFSET_SWITCH;
			unsigned short i, nCount = *p++;

			BYTE nTrkID = *p++;			//종착궤도
			BYTE nOverTime = *p++;		//보류쇄정시간

			BYTE nOverSigID = 0;
			BYTE nID, swpos;
			ScrInfoSwitch *pSwitchInfo = NULL;

			for (i=2; i<nCount; i+=2) 
			{
				nID = *p++;
				nOverSigID = *p++;
				swpos = nID & 0x80;
				nID &= 0x7f;

				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByID( OBJ_SWITCH, nID );
				pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
				if (pSwitchInfo->NOUSED == 1 && swpos == 0  && sCount>1)
				{
					if ( !(nRouteCount < 6 && cs > 0) )
					{
						str += '-';
						str += (sCount-1)+48;
					}
					break;
				}
			}
			if (pSwitchInfo != NULL)
			{
				if ( (pSwitchInfo->NOUSED == 0 && sCount>2) || (((pSwitchInfo != NULL) && (pSwitchInfo->NOUSED == 1)) && swpos == 0 && sCount>1) )
				{
					break;
				}
			}
		}

		m_pRouteLine = SearchRoutePos( str );
		if (m_pRouteLine==NULL && m_strStationName.Find ( "SHAIS",0 )==0 && strButton=="53")
		{
			str.Replace("53","45");
			m_pRouteLine = SearchRoutePos( str );
			m_RouteLineType = 1;
		}
		// 2006.05.15  실헷의 중간 shunt진로 (strButton=="55","26")  클라우라의 중간 shunt진로 (strButton=="16")를 그린다...
		// 2006.08.16  아카우라 중간 shunt진로 (strButton=="SDG3","SDG4","SDG5","SDG6","SDG7","SDG8","SDG9","SDG10","SDG14", "5","48" szName[0]==3)을 그린다...
		else if (m_pRouteLine==NULL && nRouteCount>=6 && (strButton=="55" || strButton=="26" || strButton=="12" || strButton=="14" || strButton=="16" || strButton=="18" 
			|| strButton=="SDG3" || strButton=="SDG4" || strButton=="SDG5" || strButton=="SDG6" || strButton=="SDG7" || strButton=="SDG8" || strButton=="SDG9" 
			|| strButton=="SDG10" || strButton=="SDG11" || strButton=="SDG12" || strButton=="SDG13" || strButton=="SDG14" || strButton=="SDG15" ||
			strButton=="5" || strButton=="48" || strButton=="61" || szName[0]=='3') )
		{
			str = strRouteName;
			m_pRouteLine = SearchRoutePos( str );
		}
		else
		{
			m_RouteLineType = 0;
		}
	}

#endif
}

void CLSMCtrl::OnUninitMenuPopup(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	m_pLastSignal->m_nPressTimer = 0;
	m_pRouteLine = NULL;

//	m_listRouteMenu.Purge();
}

CString CLSMCtrl::CheckRouteIndicatorCondition(CString strSigName)
{
	CString strCondition;
	CString strConditionSigName;
	CString strAspect;
	CString strSigBuf;
	char cSigNameBuf[5];
	CString strSigNameBuf;
	CString strSigAspectBuf;
	CString strPointBuf;
	char cPointNameBuf[5];
	CString strPointNameBuf;
	CString strPointDirectionBuf;
	BOOL bIsConditionOK = FALSE;
	BOOL bIsSigConditionOK = TRUE;
	BOOL bIsNonSigConditionOK = TRUE;
	BOOL bIsPointConditionOK = TRUE;

	for ( int i = 0; m_RouteIndicatorInfo.SigName[i] != ""; i++)
	{
// 		if ( strcmp(strSigName, m_RouteIndicatorInfo.SigName[i]) == 0 )
		if ( strSigName == m_RouteIndicatorInfo.SigName[i] )
		{
			strAspect = m_RouteIndicatorInfo.SigAspect[i];

			for (int k = 0; m_RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[k] != ""; k++)
			{
				strSigBuf = m_RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[k];
				strSigAspectBuf = strSigBuf.Right(1);
				strSigNameBuf = strSigBuf.Left(strSigBuf.GetLength()-1);
				strncpy( cSigNameBuf, (char*)(LPCTSTR)strSigNameBuf,4);
				
				CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByName( OBJ_SIGNAL, cSigNameBuf );
				ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
				
				if ( strSigAspectBuf == "M" && !pSignalInfo->SIN2 && !pSignalInfo->SIN3 )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "H" && !pSignalInfo->SIN2 )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "D" && !pSignalInfo->SIN1 )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "C" && !pSignalInfo->SIN3 )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "S" && !pSignalInfo->INU )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "A" && !pSignalInfo->SIN1 && !pSignalInfo->SIN2 && !pSignalInfo->SIN3 && !pSignalInfo->INU )
				{
					bIsSigConditionOK = FALSE;
					break;
				}
			}
			for (int l = 0; m_RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[l] != ""; l++)
			{
				strSigBuf = m_RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[l];
				strSigAspectBuf = strSigBuf.Right(1);
				strSigNameBuf = strSigBuf.Left(strSigBuf.GetLength()-1);
				strncpy( cSigNameBuf, (char*)(LPCTSTR)strSigNameBuf,4);
				
				CBmpSignal *pSignal = (CBmpSignal *)SearchPtrByName( OBJ_SIGNAL, cSigNameBuf );
				ScrInfoSignal *pSignalInfo = (ScrInfoSignal*)pSignal->m_pData;
				
				if ( strSigAspectBuf == "H" && pSignalInfo->SIN2 )
				{
					bIsNonSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "D" && pSignalInfo->SIN1 )
				{
					bIsNonSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "C" && pSignalInfo->SIN3 )
				{
					bIsNonSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "S" && pSignalInfo->INU )
				{
					bIsNonSigConditionOK = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "A" && ( pSignalInfo->SIN1 || pSignalInfo->SIN2 || pSignalInfo->SIN3 || pSignalInfo->INU ))
				{
					bIsNonSigConditionOK = FALSE;
					break;
				}
			}
			for (int j = 0; m_RouteIndicatorInfo.AspectCondition[i].PointCondition[j] != ""; j++)
			{
				strPointBuf = m_RouteIndicatorInfo.AspectCondition[i].PointCondition[j];
				strPointDirectionBuf = strPointBuf.Right(1);
				strPointNameBuf = strPointBuf.Left(strPointBuf.GetLength()-1);
				strncpy( cPointNameBuf, (char*)(LPCTSTR)strPointNameBuf,4);
				
				CBmpSwitch *pSwitch = (CBmpSwitch *)SearchPtrByName( OBJ_SWITCH, cPointNameBuf );
				ScrInfoSwitch *pSwitchInfo = (ScrInfoSwitch*)pSwitch->m_pData;
				
				if ( strPointDirectionBuf == "N" && !pSwitchInfo->KR_P )
				{
					bIsPointConditionOK = FALSE;
					break;
				}
				else if ( strPointDirectionBuf == "R" && !pSwitchInfo->KR_M )
				{
					bIsPointConditionOK = FALSE;
					break;
				}
			}

			if ( bIsSigConditionOK == TRUE && bIsNonSigConditionOK == TRUE && bIsPointConditionOK == TRUE)
			{
				bIsConditionOK = TRUE;
				break;
			}
		}
		
		bIsSigConditionOK = TRUE;
		bIsNonSigConditionOK = TRUE;
		bIsPointConditionOK = TRUE;
	}

	if ( bIsConditionOK )
		return strAspect;
	else
		return " ";
}
