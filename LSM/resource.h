//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LSM.rc
//
#define IDI_ABOUTDLL                    1
#define IDB_LSM                         1
#define IDD_ABOUTBOX_LSM                1
#define IDS_LSM                         1
#define IDS_LSM_PPG                     2
#define IDD_PROPPAGE_LSM                100
#define IDS_LSM_PPG_CAPTION             100
#define IDB_SYMINFO1                    132
#define IDB_SYMINFO                     202
#define IDC_RADIO_REROUTE               202
#define IDC_CURSOR_HAND                 203
#define IDC_RADIO_CAN                   203
#define IDD_DIALOG1                     204
#define IDD_DIALOG_SIGNALSELECT         204
#define IDC_STATIC_TEXT                 204
#define IDC_CURSOR_CHECK                205
#define IDC_COMBO1                      205
#define IDD_DIALOG_CONFIRM              206
#define IDC_STATIC_TRACK_NAME           206
#define IDR_MENU_DEST                   207
#define IDC_STATIC_TRACK_LOCK_ITEM      207
#define IDB_SYMINFO_8                   208
#define IDC_EDIT1                       208
#define IDC_EDIT_INFO                   208
#define IDD_DIALOG_TRACKLOCK            209
#define IDC_BUTTON_DELETE               209
#define IDD_DIALOG_INFO                 209
#define IDC_EDIT_TNI                    210
#define IDC_STATIC_ICON                 213
#define IDC_LIST1                       215
#define IDD_DIALOG_TNI                  219
#define IDC_RADIO_ROUTE                 219
#define IDC_RADIO_SIGCAN                220
#define IDB_BITMAP1                     221
#define IDB_BITMAPB                     221
#define IDC_STATIC_TEXT2                221
#define IDB_BITMAP2                     222
#define IDB_BITMAPP                     222
#define IDC_RADIO_POINT_BLOCK           222
#define IDB_BITMAP3                     223
#define IDB_BITMAPS                     223
#define IDC_RADIO_POINT_MOVE            223
#define IDC_STATIC_TEXT3                224
#define IDB_BITMAPLG                    227
#define IDI_SIGNALON                    229
#define IDI_SIGNALOFF                   231
#define IDI_SYSTEM                      232
#define IDI_SWITCH                      233
#define IDI_WARN                        234
#define IDI_OPERATE                     236
#define IDB_BITMAP4                     239
#define IDI_ICON1                       242
#define IDI_LOCK                        242
#define IDI_ICON2                       243
#define IDI_UNLOCK                      243
#define IDI_DAY                         248
#define IDI_NIGHT                       249
#define IDI_ALRON                       250
#define IDI_ALROFF                      251
#define IDB_SYMINFO7                    253
#define IDD_DIALOG_SIGSTOP              257
#define IDD_DIALOG_POINT_CONTROL        258
#define IDI_TRACKOFF                    263
#define IDI_TRACKON                     264
#define IDI_KEY                         265
#define IDI_CA                          267
#define IDI_BLOCK                       268
#define IDI_ICON3                       271
#define IDB_BITMAPAKASDG                275
#define ID_MENUITEM32769                32769
#define ID_MENUITEM32770                32770

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        277
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         225
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
