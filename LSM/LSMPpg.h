//===============================================================
//====                     LSMPpg.h                         ====
//===============================================================
// 파  일  명 : LSMPpg.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMctrl 클래스에서 사용되는 CLSMPropPage
//              factory와 quid를 초기화한다.
//
//===============================================================

class CLSMPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CLSMPropPage)
	DECLARE_OLECREATE_EX(CLSMPropPage)

// Constructor
public:
	CLSMPropPage();

// Dialog Data
	//{{AFX_DATA(CLSMPropPage)
	enum { IDD = IDD_PROPPAGE_LSM };
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CLSMPropPage)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
