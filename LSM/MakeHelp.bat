@echo off
REM -- First, make map file from resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by MSCR.HPJ. >hlp\MScr.hm
echo. >>hlp\MScr.hm
echo // Commands (ID_* and IDM_*) >>hlp\MScr.hm
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>hlp\MScr.hm
echo. >>hlp\MScr.hm
echo // Prompts (IDP_*) >>hlp\MScr.hm
makehm IDP_,HIDP_,0x30000 resource.h >>hlp\MScr.hm
echo. >>hlp\MScr.hm
echo // Resources (IDR_*) >>hlp\MScr.hm
makehm IDR_,HIDR_,0x20000 resource.h >>hlp\MScr.hm
echo. >>hlp\MScr.hm
echo // Dialogs (IDD_*) >>hlp\MScr.hm
makehm IDD_,HIDD_,0x20000 resource.h >>hlp\MScr.hm
echo. >>hlp\MScr.hm
echo // Frame Controls (IDW_*) >>hlp\MScr.hm
makehm IDW_,HIDW_,0x50000 resource.h >>hlp\MScr.hm
REM -- Make help for Project MSCR
start /wait hcrtf -x "MScr.hpj"
echo.
