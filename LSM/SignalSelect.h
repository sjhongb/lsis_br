// SignalSelect.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSignalSelect dialog

#include "resource.h"

class CSignalSelect : public CDialog
{
// Construction
public:
	CPoint m_Position;
	CSignalSelect(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSignalSelect)
	enum { IDD = IDD_DIALOG_SIGNALSELECT };
	int		m_nSelectType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSignalSelect)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSignalSelect)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
