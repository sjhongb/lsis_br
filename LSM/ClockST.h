//===============================================================
//====                     ClockST.h                         ====
//===============================================================
// 파  일  명 : ClockST.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : 화면에 날짜와 시간정보를 그려준다.
//
//===============================================================

#ifndef _CLOCKST_H_
#define _CLOCKST_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// Return values
#ifndef	CLOCKST_OK
#define	CLOCKST_OK					0
#endif
#ifndef	CLOCKST_INVALIDRESOURCE
#define	CLOCKST_INVALIDRESOURCE		1
#endif
#ifndef	CLOCKST_THREADKO
#define	CLOCKST_THREADKO			2
#endif

class CClockST : public CStatic
{
public:
	CClockST();
	virtual ~CClockST();

	DWORD Start(int nPaneID, int nBigID, int nSmallID, BOOL bAlternateDateFormat, BYTE *pVMEM);

	static short GetVersionI()		{return 13;}
	static LPCTSTR GetVersionC()	{return (LPCTSTR)_T("1.3");}

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClockST)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CClockST)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

private:
	BOOL GetBitmapAndPalette(UINT nIDResource, CBitmap& bitmap, CPalette& pal);
	void Refresh();
	void Clean();

	typedef struct _STRUCT_THRCLOCK
	{
		BOOL		bRun;
		int			nStep;
		LPVOID		pParent;
		HANDLE		hThrHandle;
		DWORD		dwThrId;

		_STRUCT_THRCLOCK::_STRUCT_THRCLOCK()
		{::ZeroMemory(this, sizeof(_STRUCT_THRCLOCK));}
	} STRUCT_THRCLOCK;

	STRUCT_THRCLOCK m_thrParam;
	static UINT thrClock(LPVOID pParam);

	typedef struct _STRUCT_CDCBUFFER
	{
		BOOL		bValid;
		CDC			dcMemory;
		CBitmap*	pOldBmp;

		_STRUCT_CDCBUFFER::_STRUCT_CDCBUFFER()
		{bValid = FALSE;}
	} STRUCT_CDCBUFFER;

	STRUCT_CDCBUFFER m_cdcBufClock;
	STRUCT_CDCBUFFER m_cdcBufBig;
	STRUCT_CDCBUFFER m_cdcBufSmall;

	BYTE		m_idxDigit[14];

	CBitmap		m_bmClock;
	BITMAP		m_bmInfoClock;
	CBitmap		m_bmBig;
	CBitmap		m_bmSmall;
	CPalette	m_palPalette;

	BOOL		m_bAlternateDateFormat;
	BYTE*		m_pVMEM;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif
