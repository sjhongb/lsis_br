#ifndef _CONTROL_H
#define _CONTROL_H

// Control.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CControl window

class CControl
{
// Construction
public:
	CControl();

// Attributes
public:

// Operations
public:

// Overrides

// Implementation
public:
	virtual ~CControl();

	// Generated message map functions
protected:
};

/////////////////////////////////////////////////////////////////////////////
#endif