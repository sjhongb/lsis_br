//===============================================================
//====                     KeyCheck.cpp                      ====
//===============================================================
// 파  일  명 : KeyCheck.cpp
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : Key와 Password를 체크한다.
//
//===============================================================

#include "stdafx.h"

#include "../include/scrobj.h"

char *DefKey1[] = {
"1>13<?WT7SJX?kY2SP=NIA<0o0<1EQ7?CPoJ10?P8=?LHOQKOB1>132AFnCl?O1=mDGVTL6=Jk>=OU7=ZK0NO0M1B?B5DKkOO7C048WM?IM<JN=I?IGJ<GP1oQO?I50<",
"4G0401PL0<0YOn6K<H0=1IY6M<R<Q>LBoMJTC02nBBKEJ=K1J?HO3<oM42<O80=?>N7J0Qm<JJ5GUk5MK?NJ5>YSF8TW6lO?31407V2SBU0E?>81041Um1<=071W1NV<",
">O<7<ZMLOLW17S0=MKUMn=BKK<2N8l50S===QClM=MI6UOXN3N6>NnM8Y8D?GK7P9O1SX>?PI>W=><E<ZQZ1?0K?PAE>UOERC5MRK==Mn013AYSJ?DAJ<SlT1RP0Po=<",
"2m=1T4LVJCMJOWM>6omD<>M0m>=<YJ>2OXnVJ3M0K5N<2C>DWP=?noONT=O24NN>AJoL7XO<Rl><=501YC<O?1NnFoPXL>1ML7>l0JQFoXA10ENYK5=4ACRYFS>W43<<",
"4M9F7>L?S34E<>F<1>U<Qo=EL<1L1>L1n0?E4<09=>l0CD<EL051m>T=OSZD1?ZLV>k>M9SD3>V<P>K=2TMD1=>AI=S>=FFCK<JlC=KoLIPC2?EoL=km<>9CJlTQmAo<",
"0X<C?KWP6CUmBHM0T3Q7>1OD1P>OBZKnQRI1RWO40AEEO9CR9n0Zn0P<=<N9<?QOLPJRkH8oI=2>J749I?2F15SK0o><7V<=YL40VF0G>N?TPDUVC==>S><K0K6LBRH<",
"3NCP0FH=>?1JM5>Ln?1DkFT<0DE?TEZkP>M8SR?RI>D1FE?N6<GNOE=HK>P>Y>004>Vo0E9LLMHT1EO6?>=SoK0Po>0LlEK0BZI1UEQ=Q>4P>01WJ>JJGEmWH6KB55N<",
"?J8JMI1O>8k50T96VU=<>DWVnLJ=o<1A7OS7LDKN0=?=PkOXJMKJ18=CI?K905L?JDn>R3GZV=MRCPZEkOCKE?NNLlV?>KOl3nO1U0RG>VLM<WOVn3N07=8nOOPBAPI<",
"4>I0=4Ro=1ONCSSU=1J=>0GlDIoJ>=BJ>oO0EM0o>YJH>V77F34D>P2U>L?EG0O0>F1=?C?RGmJN?M=0?J<<IRU3?4ZN?1X=JGoX?1RK?0PJJL1M?=JY?nHHK15=TKM<",
"1m=L?<JlmP?LFANWIAZKP<OZ1EN2<<9Z=A>9OHoMNBY8D=NX4K?01=8W?B=7YOPBSBX6I=MUMRPm5=7U1C<4=U<8XCW3N=LSGY1SJ>6R0D12<lB>mDV1S>KPPoRHQ?I<",
    NULL
};

void CheckOk() { return; }

extern CDC *_MyImageDC;
void CheckFail() {
	static int cnt = 0;
	cnt++;
	if ((cnt % 500) == 0) _MyImageDC++;
	return;
}

void (*CheckFun)() = CheckOk;

void KeyCheck3() {
	CheckFun();
}

extern TScrobj *_pStation;

char *make( char *s ) {
	static unsigned char b1[128],b2[129];
	strcpy((char*)b1,s);
	strcat((char*)b1,s);
	strcat((char*)b1,s);
	int i;
	int n = strlen( (char*)b1 );
	for (i=0; i<129; i++) b2[i] = 0;
	for (i=0; i<124; i++) {
		long *l = (long*)(b1+(i%n));
		long *m = (long*)(b2+i);
		*m += *l * *l * i;
		if ((i % n) == 0) *m <<= 1;
		*m -= b1[i%n];
	}
	for (i=0; i<128; i++) {
		if (b2[i] < 48) b2[i] = 48 + (b2[i]>>1);
		b2[i] = (b2[i] >> 2) + ' ';
		if (!isalnum(b2[i])) b2[i]= (b2[i]+0x10) & 0x7f;
	}
	return (char*)b2;
}

int KeyCheck() {

	char *b2 = make(_pStation->mName);
	int i=0; 	// '98.12.03
	while( DefKey1[i] ) {
		if ( strcmp(DefKey1[i],(char*)b2) ) i++;	// not like to ~
		else {                       			// same to ~
			 return 0;
		}
	}
	CheckFun = CheckFail;
	return 1;
}

#define MAX_PASS 64
void Encrypt(unsigned char *lpsz1, unsigned char *lpsz2)
{
	srand(5234);
	int nLen = strlen((char*)lpsz1);
	if (nLen != 0)
	{
		int i, j = 0, k = 0;
		unsigned int c = 0;
		for (i = 0; i<nLen; i++) {
			c += (unsigned int)lpsz1[i];
			c <<= 1;
		}
		for (i = MAX_PASS-1; i>=0; i--)
		{
			c = c + (lpsz1[j++] << (k+1)) + 1;
			if (j>=nLen) {
				j=0;
				k++;
			}
			lpsz2[i] = (char)(32 + (rand() * c % 94));
			if (lpsz2[i] == (unsigned char)0x22) lpsz2[i]++;
			if (lpsz2[i] == (unsigned char)'\\') lpsz2[i]++;
			if (lpsz2[i] == (unsigned char)'/') lpsz2[i]++;
			if (lpsz2[i] == (unsigned char)' ') lpsz2[i]++;
		}
		lpsz2[MAX_PASS] = '\0';
	}
}

char *PassWord[] = {
"zm*7f$v]DoFS.axuLjfJBdJ.jGF#f`(MxYnIN1$RL74:(.nYH,.g8(z8.z@56E>v",
"8W|itZ(,dg:+>I###T:Hl^|lL{0wNNBk&]];n'xL>szRfN6;n8r%zN2*:P:3.9VV",
",et7|rl4.qdkF9dANfr&Rd]*.SZmf>v'4w`-F=:D@!FVJZL#jjD]]^`b0N<s814t",
"H=h{,J|bNiX1V3lM$PX$|^0z`;DQ<x2E@{<1fE0>2].nvz&Q.v8y@&*f<$6q0%LT",
".O,Y^*br.'H]:Wl;T8]Xvpx,t%dy.8Tk:o8IDcdP6e`n|6:}l#:I.JvNBjv=&M(V",
"]9l-lN`B@}<5JQtG*#BVTjLjVkN]frn+Fs&MRkZ8(C6:]h`M#.@Sbp@@N@pM|A@6",
"|1z7ZRl00i&5T7:)<6h*r.(zj0Z#`r.Y]%vYR_&@NeDl,PV9:f${|tvTR0t52-Rf",
"BE^{]$|*4;:in3&_>>p&^.px`KlERPh5d](}ry]Xhov|L&`-R.^38]FFl|TsXAtX",
"VWD?D0Zl#qfs|s6{0z:HT$.@jc,Avd.wLGt%bQhd^{4,<]lOjTro]rPvx@vA.UXH",
"zY:%F`jT&ChIJo4S2$TD@$d>r!>czBheT!&I$Y@|x'x<]DdCp.N'vl2z#zh!Fiz:",
"4U`c$]><x10-H5l}ZRph8z`$V)N;|T>c&sfCLCxl8#6.x`lO*>jit^JVB<t=Z+`x",
"nSj}#,.Tt_.W.9#GFJVlL.v&Nk<+,vbu|;h}x)0T|wb|X,t[pvBeZdzd:`6o4u>(",
"4WZun<r:VmbODYHWLPD:P$blb-he*VF?@m0Ub{.zh'8p],$St6@}(J>FTn&;xI4$",
"HA&m:vb$vQNa>}lYfl.JV&Z^De|{f4:m60RK,[]ztYFnhTh9XP.)$l**JldC&9<`",
"R3l-hb6,*WLo6qle:.6|@.X2BsJ?dbr36't-.SffFQRFTn][0**=H*rBhDNeV?vD",
"<!2YT#xXb0h{FCNEr.tt,z$ZDYJ}.0(o#Cb-D_8(2s8j<pz}N&(cxL`$B8<3x%|>",
"L3B}.(^zN!@YbMX]zzNB#TV,z5(Gt<dCvmR_f?^DJuPZ`^j72hnyhxDZ@rB?45FR",
"L#HIr:v`rWb].7PU,:NLd*2VHGZkjP#I^#&%leJzvOP6.TTo8PHkLr@](Rx%&_`R",
",?`;Z(:VR',G8gdo$4J(&<n.F}HiXN@ML{Be^=:]fqr4Lz(Cjj`yDh2JX@b-v)|P",
"8_`kPn,..A<]l=zcLl&Fl]Jh43f0^Dvs21(EF_0`xG.p.@Vwt$.ux]Pn>D@-R;6]",
"4KJmlf.]V?>u07DqV@8&v*`(@?B;J8V9J_xe.yvfXQH6z0N#Ht]9bF2|^j${$Und",
"#q$Q,dP&:0^'Z90#&Jd>&.T.z!ZK#Bro^m.crApnB%.#(FdgF6@SV^`B&BLk8y(4",
"laXiF]xFB_rYF04InPlZHj(<P!$k#&p;@W0IjE<>$Mt(vP]mDZb96Rf,>`tOnOhJ",
"xojM<Dj||g6o.c8=8*6x0xbdP5BC(zHO8}t)Rg.06#0dFt,CN`05jF&P$dRaJO#V",
".'n'pt*.VCN;fy@A`($NNVV#$?XQ]|@MPk$5JEt`FITFdTtgZ|v5lF#lBt|Eb7Z>",
"$305NfL|n9Lmj1Z{Hj^P.*$<#3nMv861BM@5^5n^z1@4#]R+$h0+4|*DFh:=fQDp",
"4]:C.J|*ZCJOX=DC&*.B*D(.d7.ah<p]^%4[T36b#]LVd6<_d6bqbtvRL26cT-jV",
"#'fCRJ0XZ-,G.'.IhnnR<.tlF+*14<(%H5^CT38fjG`vt2$Qd6#C*HLf6BJO.wjH",
"X7][zL*R*)<q(uB}Hx&nDVFH2E8'lr.?6W.kd'|fVo]^>^(k@d6uv2&H>,0eh3pt",
"tmP0<$(#8!0I8oJw|bjlnPx(r-4iT`HoB[lor{r`HM2*j.N;fp<!ZjN:]`*c`'*T",

"2]!aL.!w.FLO]krM8K#nb$b7t+8H!6<>0.H!v=PJRVL]&}RoDI#*V8vz*J|?Fi^P",
"4Y@C8p&yF:4_Fgvs@S6PXJZ[d#prfDrj044':YT>p,B!NCXi$#n@.|2vhL<5t?n0",
"6[fm^0>a^jJubwxq*ox2^lvIjKpBB]hFx8`#ri^nl@XM<wV}.'J#*$|rpj(_6CXr",
"l-]Otrn9v42'v?H9]w.rT4D9ZwJl^j@rx>L)6'b8,tNo:g(Cl_8l`4lnPBF!dwhR",
"x]rO:v!A0Jh%BS`7p7#V.B$[`Ujh,nNN^j.Y6sVhfxl)LGvU03,V2v^242z#puN^",
"P0h1PZ&wHrP5,y0]D?`8$hPKP#D4r|Pz^Fx_.eZ2&Nb!Jk|y:7Dlh(N.rhnC@K^>",
"N[.7B<<)>^v;*k@CJGZ(h^x'63xJvBB2>`d?ru^T8!J%TU&WRs|!VV:R4bVKJs:&",
"Dk!]#HBo>F`%>Qj1X'l4t`.9(g,!]h*|@z2YFa(.pb(I.I67`s$z<FN4l0:)tu#d",
"rEBwFhR-Zr#Er_,iLM6n!>X?zkP*|`&&F@dU|U*xxz00lWPgty`b>H.p:0&%HYj:",
"hUhi&@Xs&Z60(yVWZaHz,tB{lAb*b(lFH02oPARRR4lSF!`{Xyf^$8BRH]h7r[Rx",
",k|uPNZ#Vt4g]ini]GFF(2L]>3^<.,<VdX^3NeB>^^R+tg@!(qztv:frPD&9d+&P",
"`[,OpBT;V.J}rODQx34:z08KL]L<HdTj.h2wzONd&zte<?ZAxGJD2JR2BvB[:)>p",
"xS.1`z4;nxT+$G`[>-|Flp*)$k*n!(].F|fst}>!>Z0w@98QR3Tx8>LFHbfIx[T8",
"(}xQfzL02L<YXm2U*W46R$(UpK(nvx6nd6hW6YzNl&*_:e25Z[Lftz<lLn(C!y:z",
":;0uhb]ClP<aB!RUPgn^]j];BE!@`,z0D!jQfY]jHdpO*=z=Ji0f|JN^L,T%]!b*",
"t_f-j.4M&&RsfGdk<m4B`4^i,C0tfPzdx,0]:odpXNZSZmLKdsdrF]R^fdTCD!t4",
">ATU|J^)HZPe.a&Uv?VFT^p1:sB2j,xtd&BYNQ:*rzVelyfUJSZBH:rb(hZg&}Tb",
"P0$qv.N0|t@A(Qx[Zw0&d|PcJ_t]^(,B&VLUD78V@JxspUV_Lid&PRB@J>4UD?nP",
"FGby4HH+RR4?j+8u!sb,,0#%.u,#XH.*ZjHY0K&4p!(_J[z5*)#,h,^FHz2cT7r*",
"JyBY0RJ{`X&W@-T}DIvh>B4Cju`#Nd^@z4v#n3LfnDtk|w,]>gX88jBTzJV'<Wj8",
"TiPg&VP#T8,5:[<3@mJNvvF{|u2#LdPXH<4+BA4pN^>;TW.-ZM,6@`NtbRP1LqL2",
"j;Ze48.EP$$e$?TM^MRt@R]0v+n#d:|z^82'@y`vT]bM.E0_!c$T8l8$r&D+0S<b",
",9X?lv&5FPRSB;d[!?tn.0>q(yNxxrzPv.>e@Mn#&X>Gz%FI6]48:&Fh.!^Uv+xJ",
"ZkbIh#(QTVDkBqLcD?*L@v&1:ENxndLf8VlYJ5jTX|`SNAVGJ=jDh:Tv`xNw4up.",
"&5X0TN8IVX&i<)Hk,o&Bvr0mD-RNvr@R|nhqVAnJXn,a:w23(sXdNp$*:jb3xEl$",
"Z'fm`46ifhBC2e:;fMv&lf*od{!DPVNNB,xYlkfPtD*)H1#qJ7V$zdH,lHtQ,IvN",
".y8s0F]=B6pOBGr%vK^Jp(:Mfo]f(><Rn&Z'X5.Fbr$Q>3&08mzZ6.N`$P`CB3@(",
"zqn5>8FW2$P3(?t9f3*NV!4WT-&<n&^l4t]KZ06hdt`S,0dwrA$P`*#@dpnUx#46",
"r+JkBdV_8zd5FoP=dKn:|J&S$MPz6JB82v4'Fgd@&(&k|iPa`a$vL`$*2t|aF!T#",
"t[@M.H(7PnLEZ7Tc8S$Prp|C>y*FRXxd2|!->%hhD2P0F0V'jep.$F>&FvpWtUd`",

"&#&eX|z_F(&?RWHe2%dJ0`>m.7]dX^*8,lV]X-v0,.ba2Mta:qN#X4*B]d63ha8r",
":QFuh:Le&xx?dG&+2K*Fx4X_Tmr0dhFFnTlyv9<8^:&E&!^!]uB&fDnH&<,!pO!.",
">{Rkx2>MH4!m8)*sHg&#>bvG$y,Z>D8.`j>[(W@PB!#!@!.%0czZD4^P::v[Zm|f",
"HSfk]!8m|zhmjw]cHY@^0Zz3$G^&fh#8DRTm<00X@L:U|c:5`=lt@D2XX<bi.'dv"

};

unsigned char stname[25];
void *passptr = NULL;

void *PassCheck() {
	char bf[MAX_PASS+1];
	Encrypt(stname, (unsigned char*)bf);
	for (int i=0; i<sizeof(PassWord)/sizeof(char*); i++) {
		if (!strcmp(bf,PassWord[i])) return passptr;
	}
	return NULL;
}
