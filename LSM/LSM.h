//===============================================================
//====                        LSM.h                         ====
//===============================================================
// 파  일  명 : LSM.h
// 작  성  자 : 전종준
// 작성  날짜 : 2004. 10. 1
// 버      젼 : 1.0
// 설      명 : CLSMApp와 DLL 등록을 수행한다.
//
//===============================================================

#include "afxctl.h" 
#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CLSMApp : See LSM.cpp for implementation.

class CLSMApp : public COleControlModule
{
public:
	int m_iNumberOfLC;
	int m_iNumberOfEPK;
	int m_iNumberOfHPK;
	int m_iMySystemNo;
	CString m_strLCName[6];
	BOOL InitInstance();
	int ExitInstance();
	void SetStationUpDnDirection(int iStationUpDn);
	void SetBlockInformation(CString strBlockName);
	void SetAxleInformation(CString strBlockName, CString strAxleName);
	void SetLCInformation(int iLCNum, CString strLCName);
	void SetEPKInformation();
	void SetHPKInformation();
	void SetStationName(CString strStationName);
	void SetFlagOfSyncWithLES();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;
