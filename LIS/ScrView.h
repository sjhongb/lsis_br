// ScrView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScrView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

//{{AFX_INCLUDES()
#include "LSM.h"
//}}AFX_INCLUDES

class CScrView : public CFormView
{
protected:
	CScrView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CScrView)

// Form Data
public:
	BOOL CreateScreenInfo();

	//{{AFX_DATA(CScrView)
	enum { IDD = IDD_DIALOG_SCR };
	CLSM	m_Screen;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScrView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnDraw(CDC* pDC);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CScrView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CScrView)
	afx_msg void OnClickLSMctrl();
	afx_msg void OnMouseDownLSMctrl(short Button, short Shift, long x, long y);
	afx_msg void OnOperateLSMctrl1(short nFunction, short nArgument);
	afx_msg void OnOleSendMessageLSMctrl1(long message, long wParam, long lParam);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CScrView *_pScrView;

/////////////////////////////////////////////////////////////////////////////
