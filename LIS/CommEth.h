
#if !defined(AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_)
#define AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommEth.h : header file
//
#include "..\Include\UserDataType.h"

/////////////////////////////////////////////////////////////////////////////
#define ID_COMM_WATCH	 200
#define ID_COMM_ERRMSG   201
#define ID_COMM_PRINTMSG 202

/////////////////////////////////////////////////////////////////////////////
#define COM_DLE ((BYTE)0x10)
#define COM_STX ((BYTE)0x02)
#define COM_ETX ((BYTE)0x03)
#define COM_LENGTH ((BYTE)0x2A)
#define COM_CRC_LENGTH ((BYTE)0x26)
#define COM_DATA_LENGTH ((BYTE)0x20)
#define COM_OP_IOSCAN ((BYTE)0x82)

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CCommEth command target

class CCommEth : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CCommEth();
	virtual ~CCommEth();
	BOOL SendData(BYTE* data, int len);

	void MakeCRC(BYTE* rcvBuffer, int rcvLen, BYTE* crcLow, BYTE* crcHigh );
	BOOL CheckCRC(BYTE* rcvBuffer, int rcvLen);

	union {
		unsigned short m_nCRC;
		struct {
			BYTE m_CRCLow;
			BYTE m_CRCHigh;
		};
	};
	
	COMCommandType m_PostDB;

	int  m_nCmdCounter;
// Overrides
public:
	BOOL CreateSocket(HWND hWnd, CString strSimConsoleIP, UINT nSimConsolePort, CString strSimulatorIP, UINT nSimulatorPort);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommEth)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CCommEth)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:

// 	void GetRxCRC16(BYTE c);
// 	void GetTxCRC16(BYTE c);
	
public:
	HWND	m_hParentWnd;

	CString m_strSimConsoleIP;
	CString m_strSimulatorIP;

	UINT	m_nSimConsolePort;
	UINT	m_nSimulatorPort;

	BYTE m_pDataQ[4096];
	BYTE m_pTxQ[4096];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_)
