// IOInfoView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIOInfoView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CIOInfoView : public CFormView
{
protected:
	CIOInfoView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CIOInfoView)

// Form Data
public:
	//{{AFX_DATA(CIOInfoView)
	enum { IDD = IDD_DIALOG_IOINFO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOInfoView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIOInfoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CIOInfoView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
