// MyTrack.cpp : implementation file
//

#include "stdafx.h"
//#include "wem.h"
#include "MyTrack.h"
#include "EITInfo.h"
#include "MemView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC( CSimObject, CObject )
IMPLEMENT_DYNAMIC( CMyTrack, CSimObject )

extern BOOL _bDebugMode;

CMyTrack::CMyTrack( CEITInfo *pInfo ) : CSimObject( pInfo )
{
	m_bAuto = FALSE;
	m_nID = 0;
	m_nGoDirection = 0;
	m_bIsCreateSimpleTrackDlg = FALSE;
}

extern CMemView *_pCMemView;

extern int _nRANDOMGENERATE;

void CMyTrack::Run()
{
	if (_bDebugMode)
	{
		ScrInfoTrack *pInfo = (ScrInfoTrack *)m_pData;
		if (m_bAuto) {
			ASSERT( m_nID>0 );
			if (m_nTimer>0) {
				m_nTimer--;
				if (m_nTimer == _pCMemView->m_nTrackingDelay / 2) 
				{
					BYTE nTrackID = m_pInfo->m_Engine.m_pTrackPrev[ m_nID * 2 + 1];
					CMyTrack *pTrack = NULL;
					if (_pCMemView && nTrackID) pTrack = _pCMemView->TrackObjectFind( nTrackID );
					if (pTrack) {
						if (pInfo->ROUTE) m_pInfo->m_Engine.m_pTrackPrev[ m_nID * 2 + 1] = 0;
						pTrack->SetAutoAccupancy();
					}
				}
				if (!m_nTimer) pInfo->TRACK = 1;
			}
			else m_nTimer = 0;
		}
		
		if (_nRANDOMGENERATE)
			pInfo->TRACK = rand() & 1;
	}
	else
	{
		ScrInfoTrack *pInfo = (ScrInfoTrack *)m_pData;

		if ( m_bIsCreateSimpleTrackDlg == FALSE )
		{
			m_SimpleTrackDlg.m_strName = m_strName;
			m_SimpleTrackDlg.m_pInfo = pInfo;
			m_SimpleTrackDlg.Create(IDD_DIALOG_TRACK_SIMPLE, NULL);
			
			m_bIsCreateSimpleTrackDlg = TRUE;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMyTrackDlg dialog


CMyTrackDlg::CMyTrackDlg( ScrInfoTrack *pInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CMyTrackDlg::IDD, pParent)
{
	m_pInfo = pInfo;
	if (m_pInfo) {
		m_bTrack = (pInfo->TRACK != 0);
		m_bDisturb = (pInfo->DISTURB != 0);
//		m_bRoute = (pInfo->ROUTE != 0);
//		m_bOverlap = (pInfo->OVERLAP != 0);
//		m_bInhibit = (pInfo->INHIBIT != 0);
//		m_bAlarm = (pInfo->ALARM != 0);
//		m_bTKRequest = (pInfo->TKREQUEST != 0);
//		m_bRouteDIR = (pInfo->ROUTEDIR != 0);
//		m_bEMREL = (pInfo->EMREL != 0);
	}
	//{{AFX_DATA_INIT(CMyTrackDlg)
	m_strName = _T("");
	m_strTime = _T("");
	m_bAuto = FALSE;
	//}}AFX_DATA_INIT
}


void CMyTrackDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyTrackDlg)

	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_DELAY, m_strTime);
	DDX_Check(pDX, IDC_CHECK_TRACK, m_bTrack);
	DDX_Check(pDX, IDC_CHECK_DISTURB, m_bDisturb);
	if ( m_bDisturb == FALSE )
	{
		m_bTrack = FALSE;
	}
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMyTrackDlg, CDialog)
	//{{AFX_MSG_MAP(CMyTrackDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTrackDlg message handlers

void CMyTrack::DialogOpen()
{
	if (_bDebugMode)
	{
		CMyTrackDlg dlg( (ScrInfoTrack*)m_pData );
		dlg.m_strName = m_strName;
		dlg.m_bAuto = m_bAuto;
		if (dlg.DoModal() == IDOK) 
		{
			m_bAuto = dlg.m_bAuto;
			if (dlg.m_bTrack == 0) m_nTimer = _pCMemView->m_nTrackingDelay;
		}
	}
	else
	{
		m_SimpleTrackDlg.ShowWindow(SW_SHOW);
	}
}

void CMyTrackDlg::OnOK()
{
	UpdateData( TRUE );
	if (m_pInfo) 
	{
		m_pInfo->TRACK = m_bTrack;
		m_pInfo->DISTURB = m_bDisturb;
//		m_pInfo->ROUTE = m_bRoute;
//		m_pInfo->OVERLAP = m_bOverlap;
//		m_pInfo->INHIBIT = m_bInhibit;
//		m_pInfo->ALARM = m_bAlarm;
//		m_pInfo->TKREQUEST = m_bTKRequest;
//		m_pInfo->ROUTEDIR = m_bRouteDIR;
//		m_pInfo->EMREL = m_bEMREL;
	}
	CDialog::OnOK();
}

void CMyTrack::SetAutoAccupancy()
{
	m_nTimer = _pCMemView->m_nTrackingDelay;
	ScrInfoTrack *pInfo = (ScrInfoTrack *)m_pData;
	pInfo->TRACK = 0;
	m_bAuto = TRUE;
}
/////////////////////////////////////////////////////////////////////////////
// CMySimpleTrackDlg dialog


CMySimpleTrackDlg::CMySimpleTrackDlg( CWnd* pParent /*=NULL*/)
	: CDialog(CMySimpleTrackDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMySimpleTrackDlg)
	m_strName = _T("");
	m_bCheckOccupy = FALSE;
	m_bCheckDisturb = FALSE;
	m_bCheckResetIn = FALSE;
	//}}AFX_DATA_INIT
}


void CMySimpleTrackDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMySimpleTrackDlg)
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_TRACK_OCCUPY, m_bCheckOccupy);
	DDX_Check(pDX, IDC_CHECK_TRACK_DISTURB, m_bCheckDisturb);
	DDX_Check(pDX, IDC_CHECK_TRACK_RESETIN, m_bCheckResetIn);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMySimpleTrackDlg, CDialog)
	//{{AFX_MSG_MAP(CMySimpleTrackDlg)
	ON_BN_CLICKED(IDC_CHECK_TRACK_OCCUPY, OnCheckTrackOccupy)
	ON_BN_CLICKED(IDC_CHECK_TRACK_DISTURB, OnCheckTrackDisturb)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_TRACK_RESETIN, OnCheckTrackResetin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySimpleTrackDlg message handlers

void CMySimpleTrackDlg::OnCheckTrackOccupy() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->TRACK = !m_bCheckOccupy;
}

void CMySimpleTrackDlg::OnCheckTrackDisturb() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->DISTURB = !m_bCheckDisturb;
	if ( m_bCheckDisturb )
	{
		m_bCheckOccupy = m_bCheckDisturb;
		m_pInfo->TRACK = !m_bCheckOccupy;
	}
	UpdateData(FALSE);
}

void CMySimpleTrackDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch (nIDEvent)
	{
	case UPDATE_ALL:
		UpdateTrackInfo();
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void CMySimpleTrackDlg::UpdateTrackInfo()
{
	m_bCheckOccupy = !m_pInfo->TRACK;
	m_bCheckDisturb = !m_pInfo->DISTURB;

	if(m_pInfo->RESETOUT)
	{
		m_bCheckOccupy = m_pInfo->TRACK = TRUE;
		m_bCheckDisturb = m_pInfo->DISTURB = TRUE;
	}

	UpdateData(FALSE);
}

BOOL CMySimpleTrackDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_bCheckOccupy = !m_pInfo->TRACK;
	m_bCheckDisturb = !m_pInfo->DISTURB;
	if (m_strName.Find('?') == 0)
	{
		GetDlgItem(IDC_CHECK_TRACK_OCCUPY)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_TRACK_DISTURB)->EnableWindow(FALSE);
	}
	UpdateData(FALSE);
	SetTimer(UPDATE_ALL,500,NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMySimpleTrackDlg::OnCheckTrackResetin() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->RESETIN = m_bCheckResetIn;
}
