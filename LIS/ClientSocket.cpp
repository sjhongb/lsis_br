// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "ClientSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSocket

CClientSocket::CClientSocket( CMemView* pServer )
{
	m_pServer = pServer;
//	m_pFile = new CSocketFile(this);
}

CClientSocket::~CClientSocket()
{
//	if (m_pFile != NULL)
//		delete m_pFile;
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSocket, CSocket)
	//{{AFX_MSG_MAP(CClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSocket member functions

void CClientSocket::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CSocket::OnReceive(nErrorCode);
	m_pServer->ProcessPendingRead(this);
}

IMPLEMENT_DYNAMIC(CClientSocket, CSocket)
