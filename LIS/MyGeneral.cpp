// MyGeneralDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "MyGeneral.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMyGeneral::CMyGeneral( CEITInfo *pInfo ) : CSimObject( pInfo )
{
	m_pDialog = NULL;
}

void CMyGeneral::Run()
{
}

void CMyGeneral::DialogOpen()
{
	CMyGeneralDlg dlg( (ScrInfoLamp*)m_pData );
	dlg.m_strName = m_strName;
	m_pDialog = &dlg;
	if (dlg.DoModal() == IDOK) {
	}
	m_pDialog = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CMyGeneralDlg dialog


CMyGeneralDlg::CMyGeneralDlg( ScrInfoLamp *pInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CMyGeneralDlg::IDD, pParent)
{
	m_pInfo = pInfo;
	if (m_pInfo) {
		m_bBIT0 = (pInfo->BitLamp != 0);
	}
	//{{AFX_DATA_INIT(CMyGeneralDlg)
	m_strName = _T("");
	//}}AFX_DATA_INIT
}


void CMyGeneralDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyGeneralDlg)
	DDX_Check(pDX, IDC_CHECK_BIT0, m_bBIT0);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMyGeneralDlg, CDialog)
	//{{AFX_MSG_MAP(CMyGeneralDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyGeneralDlg message handlers

void CMyGeneralDlg::OnOK()
{
	UpdateData( TRUE );
	if (m_pInfo) {
		m_pInfo->BitLamp = m_bBIT0;
	}
	CDialog::OnOK();
}
