#if !defined(AFX_MYSIGNAL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MYSIGNAL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MySignal.h : header file
//

#include "resource.h"
#include "simobj.h"
#include "../include/iolist.h"
#include "..\INCLUDE\EipEmu.h"	// Added by ClassView
#include "..\INCLUDE\CtcEmu.h"	// Added by ClassView

#define	SET_GREEN				1
#define	SET_YELLOW				2
#define	SET_DOUBLE_YELLOW		3
#define	SET_RED					4
#define	SET_CALLON				5
#define	SET_SHUNT				6
#define UPDATE_ALL				1
//#define  AZPR_TEST
//#define  SYT_TEST

/////////////////////////////////////////////////////////////////////////////
// CMySimpleSignalDlg dialog

class CMySimpleSignalDlg : public CDialog
{
	// Construction
public:
	void OnUpdateState();
	int m_nSetSignal;
	void ReceiveSignalInformation(ScrInfoSignal *pInfo, BYTE nType, BYTE nLamp, BYTE nLampExt);
	void RemoveNoNeededButton();
	BYTE m_nSignalLamp;
	BYTE m_nSignalLampExt;
	void MakeDisable();
	BYTE m_nSignalType;
	ScrInfoSignal *m_pInfo;
 	CMySimpleSignalDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_strName;

	
	// Dialog Data
	//{{AFX_DATA(CMySimpleSignalDlg)
	enum { IDD = IDD_DIALOG_SIGNAL_SIMPLE };
	BOOL m_bWantToControlTheLamp;
	BOOL m_bFailGreen;
	BOOL m_bFailYellow;
	BOOL m_bFailRed;
	BOOL m_bFail2ndYellow;
	BOOL m_bFailLeftIndicator;
	BOOL m_bFailRightIndicator;
	BOOL m_bFailRouteIndicator;
	BOOL m_bFailCallon;
	BOOL m_bFailShunt;
	BOOL m_bSetGreen;
	BOOL m_bSetYellow;
	BOOL m_bSetDoubleYellow;
	BOOL m_bSetRed;
	BOOL m_bSetLeftIndicator;
	BOOL m_bSetRightIndicator;
	BOOL m_bSetRouteIndicator;
	BOOL m_bSetCallon;
	BOOL m_bSetShunt;
	BOOL m_bWantToControlTheRelay;
	BOOL m_bPickUpLOR;
	BOOL m_bPickUpHHLOR;
	BOOL m_bPickUpCLOR;
	BOOL m_bPickUpSLOR;
	BOOL m_bPickUpDIRLOR;
	BOOL m_bPickUpALR;
	BOOL m_bPickUpDR;
	BOOL m_bPickUpHR;
	BOOL m_bPickUpHHR;
	BOOL m_bPickUpCR;
	BOOL m_bPickUpSR;
	BOOL	m_bAdjCond;
	BOOL	m_bPickUpCEKR;
	BOOL	m_bPickUpEKR;
	BOOL	m_bFailRedFilament;
	BOOL	m_bFailYellowFilament;
	BOOL	m_bFail2ndYellowFilament;
	BOOL	m_bFailGreenFilament;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySimpleSignalDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CMySimpleSignalDlg)
	afx_msg void OnOk2();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckLampControl();
	afx_msg void OnCheckRelayControl();
	afx_msg void OnCheckControlAlr();
	afx_msg void OnCheckControlCr();
	afx_msg void OnCheckControlDr();
	afx_msg void OnCheckControlHhr();
	afx_msg void OnCheckControlHr();
	afx_msg void OnCheckControlSr();
	afx_msg void OnCheckFailClor();
	afx_msg void OnCheckFailSlor();
	afx_msg void OnCheckFailHhlor();
	afx_msg void OnCheckFailIndicatorLor();
	afx_msg void OnCheckFailLor();
	afx_msg void OnCheckSetCallon();
	afx_msg void OnCheckSetGreen();
	afx_msg void OnCheckSetLeftIndicator();
	afx_msg void OnCheckSetRed();
	afx_msg void OnCheckSetRightIndicator();
	afx_msg void OnCheckSetRouteIndicator();
	afx_msg void OnCheckSetShunt();
	afx_msg void OnCheckSetYellow();
	afx_msg void OnCheckSetYellow2();
	afx_msg void OnCheckAnyButton();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheckAdjCond();
	afx_msg void OnCheckFailEkr();
	afx_msg void OnCheckFailCekr();
	afx_msg void OnCheckFailGFilament();
	afx_msg void OnCheckFailYFilament();
	afx_msg void OnCheckFailRFilament();
	afx_msg void OnCheckFailYyFilament();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMySimpleBlockDlg dialog

class CMySimpleBlockDlg : public CDialog
{
	// Construction
public:
	CMySimpleBlockDlg(CWnd* pParent = NULL);   // standard constructor
	void ReceiveSignalInformation(ScrInfoBlock *pInfo, BYTE nType);
	void OnUpdateState();
	void SelectTGBorTCB();
	void MakeDiable();
	BYTE m_nBlockType;
	ScrInfoBlock *m_pInfo;
	BOOL m_bIsTrainInBlockSection;
	CString m_strName;
	BOOL m_bSimInitialized;

	// Dialog Data
	//{{AFX_DATA(CMySimpleBlockDlg)
	enum { IDD = IDD_DIALOG_BLOCK_SIMPLE };
	BOOL	m_bCheckCA;
	BOOL	m_bCheckLCR;
	BOOL	m_bCheckBCB;
	BOOL	m_bCheckDEPART;
	BOOL	m_bCheckAUTO;
	BOOL	m_bCheckBGRREQ;
	BOOL	m_bCheckBGRACC;
	BOOL	m_bCheckBGRDEC;
	BOOL	m_bCheckRELEASE;
	BOOL	m_bCheckAXLOCC;
	BOOL	m_bCheckAXLDST;
	BOOL	m_bCheckAXLACC;
	BOOL	m_bCheckAXLDEC;
	BOOL	m_bCheckAXLREQ;
	BOOL	m_bCheckSAXLACC;
	BOOL	m_bCheckSAXLDEC;
	BOOL	m_bCheckSAXLDST;
	BOOL	m_bCheckSAXLOCC;
	BOOL	m_bCheckSAXLREQ;
	BOOL	m_bCheckSAXLACTIN;
	BOOL	m_bCheckRBGR;
	BOOL	m_bCheckSWEEP;
	BOOL	m_bCheckSWEEPACK;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySimpleBlockDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CMySimpleBlockDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheckAnyButton();
	afx_msg void OnCheckSweep();
	afx_msg void OnCheckSweepack();
	afx_msg void OnCheckResetReq();
	afx_msg void OnCheckResetAcc();
	afx_msg void OnCheckResetDec();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMySignal

class CEITInfo;
class CMySignal : public CSimObject
{
//    DECLARE_SERIAL( CMyPoint )
public:
	BOOL m_bIsCreateSimpleDlg;
	BOOL m_bIsCreateBlockDlg;
	void RunSignal(ScrInfoSignal *pInfo);
	void RunSimpleSignal(ScrInfoSignal *pInfo);
	void RunBlock(ScrInfoBlock *pInfo);
	void CheckSinalAspect(CString strSigName, ScrInfoSignal *pInfo);
	CString GetRouteIndicatorInfo(CString strSigName);
	CDialog * m_pDialog;
	CMySimpleSignalDlg m_SimpleDialog;
	CMySimpleBlockDlg m_BlockDlg;
	void DialogOpen();
	void Run();
	void ReceiveControlMessage(BYTE byteFunc);
	CMySignal( CEITInfo *pInfo );
	virtual ~CMySignal() {}

	BOOL m_bSLS;
    BOOL m_bManual;
    BYTE m_nSignalType;
	BYTE m_nSignalLamp;
	BYTE m_nSignalLampExt;
	ScrInfoBlock m_PrevBlockInfo;

//	WORD m_nCode;
//	WORD m_nHR, m_nLMR, m_nHRD;
//	IOBitlocSignal *m_pIO;
//	CSimObject *m_pTrack;
};

/////////////////////////////////////////////////////////////////////////////
// CMyBlockDlg dialog

class CMyBlockDlg : public CDialog
{
// Construction
public:
	int GetFaultFlag();
	void GetData();
	void SetData();
	CMyBlockDlg( ScrInfoBlock *pBlkInfo, BYTE nType, CWnd* pParent = NULL);   // standard constructor

	ScrInfoBlock *m_pInfo;
    BYTE m_nSignalType;

// Dialog Data
	//{{AFX_DATA(CMyBlockDlg)
	enum { IDD = IDD_DIALOG_BLOCK };
	int		m_nTimer;
	CString	m_strName;
	BOOL m_bManual;
	BOOL m_bCA;
	BOOL m_bLCR;
	BOOL m_bLCRIN;
	BOOL m_bLCROP;
	BOOL m_bCBB;
	BOOL m_bCBBIN;
	BOOL m_bCBBOP;
	BOOL m_bCOMPLETEIN;
	BOOL m_bCONFIRM;
	BOOL m_bASTART;
	BOOL m_bDEPART;
	BOOL m_bDEPARTIN;
	BOOL m_bARRIVE;
	BOOL m_bARRIVEIN;
	BOOL m_bCOMPLETE;
	BOOL m_bSTCLOSE;
	BOOL m_bSTCLOSEIN;
	BOOL m_bNBGR;
	BOOL m_bRBGR;
	BOOL m_bNBCR;
	BOOL m_bRBCR;
	BOOL m_bNBGPR;
	BOOL m_bRBGPR;
	BOOL m_bNBCPR;
	BOOL m_bRBCPR;
	BOOL m_bAXLDST;
	BOOL m_bAXLOCC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyBlockDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMyBlockDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMySignalDlg dialog

class CMySignalDlg : public CDialog
{
// Construction
public:
	int GetFaultFlag();
	void GetData();
	void SetData();
	CMySignalDlg( ScrInfoSignal *pInfo, BYTE nType, CWnd* pParent = NULL);   // standard constructor

	ScrInfoSignal *m_pInfo;
    BYTE m_nSignalType;

// Dialog Data
	//{{AFX_DATA(CMySignalDlg)
	enum { IDD = IDD_DIALOG_SIGNAL };
	int		m_nTimer;
	CString	m_strName;
	BOOL	m_bManual;
	BOOL m_bSOUT1;
	BOOL m_bSOUT2;
	BOOL m_bSOUT3;
	BOOL m_bSOUT3C;
	BOOL m_bSIN1;
	BOOL m_bSIN2;
	BOOL m_bSIN3;
	BOOL m_bSIN3O;
	BOOL m_bSIN3C;
	BOOL m_bSIN3CC;
	BOOL m_bINLEFT;
	BOOL m_bINRIGHT;
	BOOL m_bLEFT;
	BOOL m_bRIGHT;
	BOOL m_bLOR;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySignalDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMySignalDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYSIGNAL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_)
