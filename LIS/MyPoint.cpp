// MyPoint.cpp : implementation file
//

#include "stdafx.h"
#include "MyPoint.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern BOOL _bHPK;
extern BOOL _bDebugMode;

extern BOOL _bSIMModeSW;
extern BOOL _bSIMModeIO;

/////////////////////////////////////////////////////////////////////////////
typedef struct
{
	CString AspectSigCondition[5];
	CString NonAspectSigCondition[5];
	CString PointCondition[10];
	BOOL bAspectSigCondition[5];
	BOOL bNonAspectSigCondition[5];
	BOOL bPointCondition[10];
}RIAspectCondition;
typedef struct
{
	CString SigName[100];
	CString SigAspect[100];
	RIAspectCondition AspectCondition[100];
}RICondition;
/////////////////////////////////////////////////////////////////////////////
extern RICondition _RouteIndicatorInfo;

typedef struct
{
	CString strName;
	BOOL bInconsistency;
	BOOL bNKR;
	BOOL bRKR;
	BOOL bKeyRemoved;
}HPKINFO;
HPKINFO HPKR[10];
//IMPLEMENT_SERIAL (CMyPoint, CObject, 0)

CMyPoint::CMyPoint( CEITInfo *pInfo ) : CSimObject( pInfo )
{
//	m_nWR_N = m_nWR_R = m_nKR_N = m_nKR_R = 0;
	m_nDelay = 0;
	m_bFail = FALSE;
	m_bManual = FALSE;
    m_nWRODP = m_nWRODM = 0;
	m_bIsCreateSimplePointDlg = FALSE;

	m_pDialog = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CMyPointDlg dialog


CMyPointDlg::CMyPointDlg( ScrInfoSwitch *pInfo,BYTE nType,BYTE nCrank,CWnd* pParent /*=NULL*/)
	: CDialog(CMyPointDlg::IDD, pParent)
{
	m_pInfo = pInfo;
	//{{AFX_DATA_INIT(CMyPointDlg)
	m_strName = _T("");
	m_bFail = FALSE;
	m_bManual = FALSE;
	m_nTimer = 0;

	m_bKRP = FALSE;
	m_bKRM = FALSE;
	m_bWRP = FALSE;
	m_bWRM = FALSE;
	m_bWLR = FALSE;
	m_bWROM = FALSE;
    m_bWROP = FALSE;
	m_bWLRO = FALSE;
	m_bWKEYKR = FALSE;
	m_bREQP = FALSE;
	m_bREQM = FALSE;
	m_bNOUSED = FALSE;

	//}}AFX_DATA_INIT

	SetData();
	m_nSwitchType = nType;
	m_nPointKeyNo = nCrank;
}


void CMyPointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyPointDlg)
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_DELAY, m_nTimer);
	DDX_Check(pDX, IDC_CHECK_MANUAL, m_bManual);
    DDX_Check(pDX, IDC_CHECK_WKEYKR, m_bWKEYKR);
	DDX_Check(pDX, IDC_CHECK_NOUSED, m_bNOUSED);
	DDX_Check(pDX, IDC_CHECK_WROM, m_bWROM);
    DDX_Check(pDX, IDC_CHECK_WROP, m_bWROP);
	DDX_Check(pDX, IDC_CHECK_REQP, m_bREQP);
    DDX_Check(pDX, IDC_CHECK_REQM, m_bREQM);
	DDX_Check(pDX, IDC_CHECK_WRP, m_bWRP);
	DDX_Check(pDX, IDC_CHECK_WRM, m_bWRM);
	DDX_Check(pDX, IDC_CHECK_KRP, m_bKRP);
	DDX_Check(pDX, IDC_CHECK_KRM, m_bKRM);
	DDX_Check(pDX, IDC_CHECK_WLR, m_bWLR);
	DDX_Check(pDX, IDC_CHECK_WLRO, m_bWLRO);
	DDX_Check(pDX, IDC_CHECK_FAIL, m_bFail);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMyPointDlg, CDialog)
	//{{AFX_MSG_MAP(CMyPointDlg)
	ON_BN_CLICKED(ID_Update, OnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyPointDlg message handlers

BYTE _nSwitchDelay = 20;  // 40 까지 늘릴 수 있다...

extern int _nRANDOMGENERATE;

void CMyPoint::Run()
{
	if (!m_pData) return;

	ScrInfoSwitch *pInfo = (ScrInfoSwitch *)m_pData;

	if ( _bDebugMode )
	{
		CMyPointDlg *pDlg = NULL;
		if ( m_pDialog ) 
		{
			pDlg = (CMyPointDlg*)m_pDialog;
			if (pDlg->m_hWnd) 
			{
				CButton *pButton = (CButton *)pDlg->GetDlgItem( IDC_CHECK_FAIL );
				if (pButton) {
					m_bFail = pDlg->m_bFail = pButton->GetCheck();
				}
				pButton = (CButton *)pDlg->GetDlgItem( IDC_CHECK_MANUAL );
				if (pButton) {
					m_bManual = pDlg->m_bManual = pButton->GetCheck();
				}
			}
		}
		if (m_bManual) return;

		// 실헷역 처리..
		/*
		if ( (m_nPointKeyNo==11 && m_strName == "21") || (m_nPointKeyNo==12 && m_strName == "24") || (m_nPointKeyNo==13 && m_strName == "26") || 
			 (m_nPointKeyNo==14 && m_strName == "29") || (m_nPointKeyNo==15 && m_strName == "31") || (m_nPointKeyNo==16 && m_strName == "33") || 
			 (m_nPointKeyNo==17 && m_strName == "35") )
			pInfo->WKEYKR = 1;
		*/

	//	return;

	if (_bHPK )
		{
			pInfo->KEYOPER = 0;
		}

		// 초기값이 결정 된다...
		if (m_nSwitchType == 0x30)
		{
			pInfo->KR_P = 1;
			m_nDelay = _nSwitchDelay;
			
			pInfo->WKEYKR = !pInfo->KEYOPER;
		}
		else
		{
			if (pInfo->WR_P == pInfo->WR_M) 
			{
				pInfo->KR_P = pInfo->WR_P = 1;
				pInfo->KR_M = pInfo->WR_M = 0;
				m_nDelay = 0;
			}
		}

	// 전철기 정상 방향에서 상대 방향으로 전철기 제어 중 	
		if (pInfo->WRO_P != pInfo->WRO_M && pInfo->WLRO)   
		{
			pInfo->WR_P = pInfo->WRO_P;  // 전철기 제어 출력 을 표시 입력으로 받아치고 있음...
			pInfo->WR_M = pInfo->WRO_M;
		}

		pInfo->WLR = pInfo->WLRO;        // 전철기 제어 명령에 의해 전철기 쇄정 정보 update


		if (m_bFail) {
			pInfo->KR_P = pInfo->KR_M = 0;
		}
		else if ( pInfo->WLR ) 
		{
			if (pInfo->WR_P) 
			{
				pInfo->KR_M = 0; 
				if (!pInfo->KR_P) 
				{
					if (m_nDelay > 0) 
						m_nDelay--;
					if (!m_nDelay) 
					{
						if (m_nSwitchType != 0x30)
							pInfo->KR_P = 1;
					}
				}
			}
			else if (pInfo->WR_M) 
			{
				pInfo->KR_P = 0; 
				if (!pInfo->KR_M) 
				{
					if (m_nDelay < _nSwitchDelay) 
						m_nDelay++;
					if (m_nDelay == _nSwitchDelay) 
					{
						if (m_nSwitchType != 0x30)
							pInfo->KR_M = 1;
					}
				}
			}
		}
		if ( m_pDialog ) 
		{
			pDlg->SetData();
			pDlg->UpdateData( FALSE );
		}
	}
	else
	{
		if ( m_bIsCreateSimplePointDlg == FALSE )
		{
			m_SimpleDialog.m_strName = m_strName;
			m_SimpleDialog.m_iDelayTime = 20;
			m_SimpleDialog.m_bDelayTimeUpdate = TRUE;
			m_SimpleDialog.ReceivePointInformation((ScrInfoSwitch*)m_pData, m_nSwitchType, m_nPointKeyNo);
			m_SimpleDialog.Create(IDD_DIALOG_POINT_SIMPLE, NULL);

			m_bIsCreateSimplePointDlg = TRUE;
		}

		if ( m_nSwitchType == 0x10 || m_nSwitchType == 0x20 )
		{
			// 초기값이 결정 된다...
			if (pInfo->WR_P == pInfo->WR_M) 
			{
				pInfo->KR_P = pInfo->WR_P = 1;
				pInfo->KR_M = pInfo->WR_M = 0;
				m_nDelay = 0;
			}
			
			// 전철기 정상 방향에서 상대 방향으로 전철기 제어 중 	
			if (pInfo->WRO_P != pInfo->WRO_M && pInfo->WLRO)   
			{
				pInfo->WR_P = pInfo->WRO_P;  // 전철기 제어 출력 을 표시 입력으로 받아치고 있음...
				pInfo->WR_M = pInfo->WRO_M;
			}
			
			pInfo->WLR = pInfo->WLRO;        // 전철기 제어 명령에 의해 전철기 쇄정 정보 update
			
			if ( pInfo->WLR ) 
			{
				if (pInfo->WR_P) 
				{
					pInfo->KR_M = 0; 
					if (!pInfo->KR_P) 
					{
						if (m_nDelay > 0) 
							m_nDelay--;
						if (!m_nDelay) 
						{
							if (m_nSwitchType != 0x30)
								pInfo->KR_P = 1;
						}
					}
				}
				else if (pInfo->WR_M) 
				{
					pInfo->KR_P = 0; 
					if (!pInfo->KR_M) 
					{
						if (m_nDelay < m_SimpleDialog.m_iDelayTime) 
							m_nDelay++;
						if (m_nDelay == m_SimpleDialog.m_iDelayTime) 
						{
							if (m_nSwitchType != 0x30)
								pInfo->KR_M = 1;
						}
					}
				}
			}
		}
		else if ( m_nSwitchType == 0x30 )
		{
			int iIndexOfHPK = 0;
			for ( iIndexOfHPK = 0; iIndexOfHPK < 10; iIndexOfHPK++ )
			{
				if ( HPKR[iIndexOfHPK].strName == m_strName )
				{
					break;
				}
				else if ( HPKR[iIndexOfHPK].strName == "")
				{
					HPKR[iIndexOfHPK].strName = m_strName;
					break;
				}
			}

			if( HPKR[iIndexOfHPK].bInconsistency )
			{
				pInfo->KR_P = 0;
				pInfo->KR_M = 0;
			}
			else
			{
				pInfo->WR_P = pInfo->KR_P = HPKR[iIndexOfHPK].bNKR;
				pInfo->WR_M = pInfo->KR_M = HPKR[iIndexOfHPK].bRKR;
			}

			pInfo->WKEYKR = !HPKR[iIndexOfHPK].bKeyRemoved;
		}
	}
/*
	if (m_nSwitchType != 0x30)
	{
		pInfo->KR_P = pInfo->WR_P;
		pInfo->KR_M = pInfo->WR_M;
	}
*/
	if (_bSIMModeIO == TRUE )
	{
		CheckPointDirection(m_strName, pInfo);
	}

}

void CMyPoint::CheckPointDirection(CString strPointName, ScrInfoSwitch *pInfo)
{
	CString strPointBuf;
	CString strPointNameBuf;
	CString strPointDirectionBuf;

	for ( int i = 0; _RouteIndicatorInfo.SigName[i] != ""; i++)
	{
		for (int j = 0; _RouteIndicatorInfo.AspectCondition[i].PointCondition[j] != ""; j++)
		{
			strPointBuf = _RouteIndicatorInfo.AspectCondition[i].PointCondition[j];
			strPointDirectionBuf = strPointBuf.Right(1);
			strPointNameBuf = strPointBuf.Left(strPointBuf.GetLength()-1);
			
			if ( strPointNameBuf == m_strName )
			{
				if ( strPointDirectionBuf == "N" && !pInfo->KR_P )
				{
					_RouteIndicatorInfo.AspectCondition[i].bPointCondition[j] = FALSE;
					break;
				}
				else if ( strPointDirectionBuf == "R" && !pInfo->KR_M )
				{
					_RouteIndicatorInfo.AspectCondition[i].bPointCondition[j] = FALSE;
					break;
				}
				else
				{
					_RouteIndicatorInfo.AspectCondition[i].bPointCondition[j] = TRUE;
				}
			}
		}
	}
}


void CMyPoint::DialogOpen()
{
	if ( _bDebugMode )
	{
		CMyPointDlg dlg( (ScrInfoSwitch*)m_pData, m_nSwitchType, m_nPointKeyNo );
		dlg.m_strName = m_strName;
		dlg.m_bFail = m_bFail;
		dlg.m_bManual = m_bManual;

		m_pDialog = &dlg;
		if (dlg.DoModal() == IDOK) 
		{
			m_bFail = dlg.m_bFail;
			m_bManual = dlg.m_bManual;
		}
		m_pDialog = NULL;
	}
	else
	{
		m_SimpleDialog.ShowWindow(SW_SHOW);
	}
}

void CMyPointDlg::OnOK() 
{
	UpdateData( TRUE );

	if (m_pInfo) 
	{
		m_pInfo->KR_P = m_bKRP;
		m_pInfo->KR_M = m_bKRM;
		m_pInfo->WR_P = m_bWRP;
		m_pInfo->WR_M = m_bWRM;
		m_pInfo->WRO_P = m_bWROP;
		m_pInfo->WRO_M = m_bWROM;
		m_pInfo->WLR = m_bWLR;
		m_pInfo->WLRO = m_bWLRO;
		m_pInfo->WKEYKR = m_bWKEYKR;
		m_pInfo->REQUEST_P = m_bREQP;
		m_pInfo->REQUEST_M = m_bREQM;
		m_pInfo->NOUSED = m_bNOUSED;
	}
	CDialog::OnOK();
}

void CMyPointDlg::SetData()
{
//	if (m_hWnd) {
//		CButton *pButton = (CButton *)GetDlgItem( IDC_CHECK_FAIL );
//		if (pButton) {
//			m_bFail = pButton->GetCheck();
//		}
//	}
	if (m_pInfo) 
	{
		m_bWROM = (m_pInfo->WRO_M != 0);
		m_bWROP = (m_pInfo->WRO_P != 0);
		m_bWRM = (m_pInfo->WR_M != 0);
		m_bWRP = (m_pInfo->WR_P != 0);
		m_bKRM = (m_pInfo->KR_M != 0);
		m_bKRP = (m_pInfo->KR_P != 0);
		m_bWLR = (m_pInfo->WLR != 0);
		m_bWLRO = (m_pInfo->WLRO != 0);
        m_bWKEYKR = (m_pInfo->WKEYKR !=0);
		m_bREQP = (m_pInfo->REQUEST_P !=0) ;
		m_bREQM = (m_pInfo->REQUEST_M !=0) ;
		m_bNOUSED = (m_pInfo->NOUSED !=0) ;

//		m_nTimer = m_pInfo->TIMER;
	}
}

BOOL CMyPointDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
//m_cBTN_OK.SetIcon(IDI_OK, (int)BTNST_AUTO_DARKER);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMyPointDlg::OnUpdate() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_pInfo) 
	{
		m_pInfo->KR_P = m_bKRP;
		m_pInfo->KR_M = m_bKRM;
		m_pInfo->WR_P = m_bWRP;
		m_pInfo->WR_M = m_bWRM;
		m_pInfo->WRO_P = m_bWROP;
		m_pInfo->WRO_M = m_bWROM;
		m_pInfo->WLR = m_bWLR;
		m_pInfo->WLRO = m_bWLRO;
		m_pInfo->WKEYKR = m_bWKEYKR;
		m_pInfo->REQUEST_P = m_bREQP;
		m_pInfo->REQUEST_M = m_bREQM;
		m_pInfo->NOUSED = m_bNOUSED;
	}
}
/////////////////////////////////////////////////////////////////////////////
// CMySimplePointDlg dialog


CMySimplePointDlg::CMySimplePointDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMySimplePointDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMySimplePointDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CMySimplePointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMySimplePointDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_INCONSISTENCY, m_bInconsistency);
	DDX_Check(pDX, IDC_CHECK_NKR, m_bNKR);
	DDX_Check(pDX, IDC_CHECK_RKR, m_bRKR);
	DDX_Check(pDX, IDC_CHECK_NWPR, m_bNWPR);
	DDX_Check(pDX, IDC_CHECK_RWPR, m_bRWPR);
	DDX_Check(pDX, IDC_CHECK_HANDPOINTKEY_REMOVE, m_bHandPointKeyRemove);
	if(m_bDelayTimeUpdate)
	{
		DDX_Text(pDX, IDC_EDIT_Delay_Time, m_iDelayTime);
		m_bDelayTimeUpdate = FALSE;
	}
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMySimplePointDlg, CDialog)
	//{{AFX_MSG_MAP(CMySimplePointDlg)
	ON_BN_CLICKED(IDC_CHECK_INCONSISTENCY, OnButtonInconsistency)
	ON_BN_CLICKED(IDC_CHECK_HANDPOINTKEY_REMOVE, OnButtonHandpointkeyRemove)
	ON_BN_CLICKED(IDOK2, OnOk2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_RKR, OnButtonRkr)
	ON_BN_CLICKED(IDC_CHECK_NKR, OnButtonNkr)
	ON_BN_CLICKED(IDC_CHECK_NWPR, OnCheckNwpr)
	ON_BN_CLICKED(IDC_CHECK_RWPR, OnCheckRwpr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySimplePointDlg message handlers

void CMySimplePointDlg::OnButtonInconsistency() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bInconsistency)
	{
		m_pInfo->KR_P = FALSE;
		m_pInfo->KR_M = FALSE;
	}
	else
	{
		m_pInfo->KR_P = m_pInfo->WR_P;
		m_pInfo->KR_M = m_pInfo->WR_M;
	}

	int iIndexOfHPK = 0;
	for ( iIndexOfHPK = 0; iIndexOfHPK < 10; iIndexOfHPK++ )
	{
		if ( HPKR[iIndexOfHPK].strName == m_strName )
		{
			break;
		}
		else if ( HPKR[iIndexOfHPK].strName == "")
		{
			HPKR[iIndexOfHPK].strName = m_strName;
			break;
		}
	}
	HPKR[iIndexOfHPK].bInconsistency = !HPKR[iIndexOfHPK].bInconsistency;
}

void CMySimplePointDlg::OnButtonNkr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if ( m_byteType == 0x30 )
	{
		if (m_bNKR)
		{
			m_bRKR = FALSE;
			m_pInfo->WR_P = m_pInfo->KR_P = TRUE;
			m_pInfo->WR_M = m_pInfo->KR_M = FALSE;
		}
		else
		{
			m_pInfo->WR_P = m_pInfo->KR_P = FALSE;
		}

		int iIndexOfHPK = 0;
		for ( iIndexOfHPK = 0; iIndexOfHPK < 10; iIndexOfHPK++ )
		{
			if ( HPKR[iIndexOfHPK].strName == m_strName )
			{
				break;
			}
			else if ( HPKR[iIndexOfHPK].strName == "")
			{
				HPKR[iIndexOfHPK].strName = m_strName;
				break;
			}
		}
		HPKR[iIndexOfHPK].bNKR = m_bNKR;
		HPKR[iIndexOfHPK].bRKR = m_bRKR;
	}
	else
	{
		if (m_bNKR)
		{
			m_bRKR = FALSE;
			m_pInfo->KR_P = TRUE;
			m_pInfo->KR_M = FALSE;
		}
		else
		{
			m_pInfo->KR_P = FALSE;
		}
		
		if ((m_pInfo->WR_P == m_pInfo->KR_P) && (m_pInfo->WR_M == m_pInfo->KR_M))
		{
			m_bInconsistency = FALSE;
		}
		else
		{
			m_bInconsistency = TRUE;
		}
	}

	UpdateData(FALSE);
}

void CMySimplePointDlg::OnButtonRkr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if ( m_byteType == 0x30 )
	{
		if (m_bRKR)
		{
			m_bNKR = FALSE;
			m_pInfo->WR_M = m_pInfo->KR_M = TRUE;
			m_pInfo->WR_P = m_pInfo->KR_P = FALSE;
		}
		else
		{
			m_pInfo->WR_M = m_pInfo->KR_M = FALSE;
		}

		int iIndexOfHPK = 0;
		for ( iIndexOfHPK = 0; iIndexOfHPK < 10; iIndexOfHPK++ )
		{
			if ( HPKR[iIndexOfHPK].strName == m_strName )
			{
				break;
			}
			else if ( HPKR[iIndexOfHPK].strName == "")
			{
				HPKR[iIndexOfHPK].strName = m_strName;
				break;
			}
		}
		HPKR[iIndexOfHPK].bNKR = m_bNKR;
		HPKR[iIndexOfHPK].bRKR = m_bRKR;
	}
	else
	{
		if (m_bRKR)
		{
			m_bNKR = FALSE;
			m_pInfo->KR_M = TRUE;
			m_pInfo->KR_P = FALSE;
		}
		else
		{
			m_pInfo->KR_M = FALSE;
		}
		
		if ((m_pInfo->WR_P == m_pInfo->KR_P) && (m_pInfo->WR_M == m_pInfo->KR_M))
		{
			m_bInconsistency = FALSE;
		}
		else
		{
			m_bInconsistency = TRUE;
		}
	}
	
	UpdateData(FALSE);
}

void CMySimplePointDlg::OnButtonHandpointkeyRemove() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int iIndexOfHPK = 0;
	for ( iIndexOfHPK = 0; iIndexOfHPK < 10; iIndexOfHPK++ )
	{
		if ( HPKR[iIndexOfHPK].strName == m_strName )
		{
			break;
		}
		else if ( HPKR[iIndexOfHPK].strName == "")
		{
			HPKR[iIndexOfHPK].strName = m_strName;
			break;
		}
	}
	HPKR[iIndexOfHPK].bKeyRemoved = !HPKR[iIndexOfHPK].bKeyRemoved;
	m_pInfo->WKEYKR = !m_pInfo->WKEYKR;
}

void CMySimplePointDlg::OnOk2() 
{
	// TODO: Add your control notification handler code here
	m_bDelayTimeUpdate = TRUE;
	CDialog::OnOK();
}

BOOL CMySimplePointDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if ( m_byteType  == 0x30 )
	{
		GetDlgItem( IDC_CHECK_HANDPOINTKEY_REMOVE )->SetRedraw(TRUE);
		m_pInfo->WR_P = m_bNKR = TRUE;
		UpdateData(FALSE);
		OnButtonNkr();
	}
	else
	{
		GetDlgItem( IDC_CHECK_HANDPOINTKEY_REMOVE )->SetRedraw(FALSE);
	}

	SetTimer(UPDATE_ALL,500,NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMySimplePointDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch(nIDEvent)
	{
	case UPDATE_ALL:
		if(m_byteType == 0x30 && m_pInfo->KEYOPER)
		{
			GetDlgItem( IDC_CHECK_HANDPOINTKEY_REMOVE )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_NWPR )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_RWPR )->EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_HANDPOINTKEY_REMOVE )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_NWPR )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_RWPR )->EnableWindow(TRUE);
		}

		if ((m_pInfo->KR_M != m_pInfo->KR_P) && (m_pInfo->WR_P == m_pInfo->KR_P) && (m_pInfo->WR_M == m_pInfo->KR_M))
			m_bInconsistency = FALSE;

		m_bNKR = m_pInfo->KR_P;
		m_bRKR = m_pInfo->KR_M;
		m_bNWPR = m_pInfo->WR_P;
		m_bRWPR = m_pInfo->WR_M;
		
		UpdateData(FALSE);
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void CMySimplePointDlg::ReceivePointInformation(ScrInfoSwitch *pInfo, BYTE nType, BYTE nCrank)
{
	m_pInfo = pInfo;
	m_byteType = nType;
	m_byteCrank = nCrank;
}


void CMySimplePointDlg::OnCheckNwpr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	if (m_bNWPR)
	{
		m_bRWPR = FALSE;
		m_pInfo->WR_P = TRUE;
		m_pInfo->WR_M = FALSE;
	}
	else
	{
		m_pInfo->WR_P = FALSE;
	}
	
	UpdateData(FALSE);
}

void CMySimplePointDlg::OnCheckRwpr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	if (m_bRWPR)
	{
		m_bNWPR = FALSE;
		m_pInfo->WR_M = TRUE;
		m_pInfo->WR_P = FALSE;
	}
	else
	{
		m_pInfo->WR_M = FALSE;
	}
	
	UpdateData(FALSE);
}

BOOL CMySimplePointDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN)
			OnOk2();
		if(pMsg->wParam == VK_ESCAPE)
			return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}
