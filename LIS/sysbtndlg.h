// SysBtnDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSysBtnDlg dialog   2004. 11. 25
class CCommDriver;
class CSysBtnDlg : public CDialog
{
// Construction
public:
	void SetIOSIMMode();
	CSysBtnDlg(CCommDriver* pSerial, CWnd* pParent = NULL);   // standard constructor

	CCommDriver* m_pSerial;

// Dialog Data
	//{{AFX_DATA(CSysBtnDlg)
	enum { IDD = IDD_DIALOG_SYSTEMBUTTON };
    BOOL	m_bACTIVEON1;
    BOOL	m_bACTIVEON2;
    BOOL	m_bVPOR1;
    BOOL	m_bVPOR2;
    BOOL	m_bFUSE1;
    BOOL	m_bFUSE2;
	BOOL	m_bFUSEPR;
	BOOL	m_bMode;
	BOOL	m_bCTCRequest;
    BOOL	m_bFAN1;
    BOOL	m_bFAN2;
    BOOL	m_b24VINT;
    BOOL	m_b24VNEXT;
    BOOL	m_b24VSEXT;
    BOOL	m_bMAINPOWER;
    BOOL	m_bGENRUN;
    BOOL	m_bGENLOWFUEL;
    BOOL	m_bGENFAIL;
    BOOL	m_bUPS2FAIL;
    BOOL	m_bUPS1FAIL;
    BOOL	m_bCHARGEFAIL;
    BOOL	m_bLOWVOLTAGE;
    BOOL	m_bCHARGING;
    BOOL	m_bGNDFAIL;
    BOOL	m_bNKWEPRN;
    BOOL	m_bNKWEPRS;
    BOOL	m_bNKWEPRA;
    BOOL	m_bNKWEPRB;
    BOOL	m_bNKWEPRC;
    BOOL	m_bNKWEPRD;
    BOOL	m_bNKWEPRE;
    BOOL	m_bNKWEPRF;
    BOOL	m_bNKWEPRG;
	BOOL    m_bDIMMER;
	BOOL    m_bDefault1;
	BOOL    m_bDefault2;
	BOOL    m_bInitial;
	BOOL    m_bLC1NKXPR;
	BOOL    m_bLC1BELACK;
	BOOL	m_bLC2NKXPR;
	BOOL	m_bLC2BELACK;
	BOOL	m_bLC3NKXPR;
	BOOL	m_bLC3BELACK;
	BOOL	m_bLC4NKXPR;
	BOOL	m_bLC4BELACK;
	BOOL	m_bLC5NKXPR;
	BOOL	m_bLC5BELACK;
	BOOL    m_bHPK;
	BOOL    m_bSLS;
    BOOL    m_bPAS;
	BOOL	m_bFAN3;
	BOOL	m_bFAN4;
	BOOL	m_bFUSE3;
	BOOL	m_bFUSE4;
	BOOL	m_bMCCR1;
	BOOL	m_bMCCR2;
	BOOL	m_bMCCR1Online;
	BOOL	m_bMCCR2Online;
	BOOL	m_bBlockUp;
	BOOL	m_bBlockDown;
	BOOL	m_bBlockMid;
	BOOL	m_bSIMModeSW;
	BOOL	m_bSIMModeIO;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSysBtnDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSysBtnDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonFan1();
	afx_msg void OnButtonCtc();
	afx_msg void OnButtonUps1();
	afx_msg void OnButtonDcExt();
	afx_msg void OnButtonCbi1();
	afx_msg void OnButtonFan2();
	afx_msg void OnButtonUps2();
	afx_msg void OnButtonMccr1();
	afx_msg void OnButtonCbi2();
	afx_msg void OnButtonFuseC();
	afx_msg void OnButtonGenFail();
	afx_msg void OnButtonMccr1Online();
	afx_msg void OnButtonCbiOnline();
	afx_msg void OnButtonFuseR();
	afx_msg void OnButtonGenRun();
	afx_msg void OnButtonMccr2();
	afx_msg void OnButtonMainPower();
	afx_msg void OnButtonB24In();
	afx_msg void OnButtonMccr2Online();
	afx_msg void OnButtonLowVoltage();
	afx_msg void OnButtonBatteryCharging();
	afx_msg void OnButtonGround();
	afx_msg void OnButtonLc1Barrier();
	afx_msg void OnButtonLc1BellAck();
	afx_msg void OnButtonLc2Barrier();
	afx_msg void OnButtonLc2BellAck();
	afx_msg void OnButtonLc3Barrier();
	afx_msg void OnButtonLc3BellAck();
	afx_msg void OnButtonLc4Barrier();
	afx_msg void OnButtonLc4BellAck();
	afx_msg void OnButtonLc5Barrier();
	afx_msg void OnButtonLc5BellAck();
	afx_msg void OnButtonUepkHepkKey();
	afx_msg void OnButtonDepkIepkKey();
	afx_msg void OnButtonAepkKey();
	afx_msg void OnButtonBepkKey();
	afx_msg void OnButtonCepkKey();
	afx_msg void OnButtonDepkKey();
	afx_msg void OnButtonEepkKey();
	afx_msg void OnButtonFepkKey();
	afx_msg void OnButtonGepkKey();
	afx_msg void OnButtonAllHpkKey();
	afx_msg void OnButtonBlockMiddle();
	afx_msg void OnButtonBlockCommUp();
	afx_msg void OnButtonBlockCommDown();
	afx_msg void OnButtonBlockCommMid();
	afx_msg void OnButtonCtcRequest();
	afx_msg void OnCheckSimmodeSw();
	afx_msg void OnCheckSimmodeIO();
	afx_msg void OnButtonFan3();
	afx_msg void OnButtonFan4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
