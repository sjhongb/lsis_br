#if !defined(AFX_SHAREDMEMORY_H__FAA0E6FE_7557_401D_8D5C_516A380DA13E__INCLUDED_)
#define AFX_SHAREDMEMORY_H__FAA0E6FE_7557_401D_8D5C_516A380DA13E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SharedMemory.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SharedMemory document

class SharedMemory : public CDocument
{
protected:
	DECLARE_DYNCREATE(SharedMemory)

// Attributes
public:
	SharedMemory();           // protected constructor used by dynamic creation
    char SharedMemoryFile[25];
    char SharedMemoryName[25];
	LPVOID SharedMemoryAddress;
	HANDLE SharedMemoryMapFile;

    char SharedControlFile[25];
    char SharedControlName[25];
	LPVOID SharedControlAddress;
	HANDLE SharedControlMapFile;

	unsigned char m_creater;

// Operations
	void Serialize();
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SharedMemory)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	void SharedMemoryInit();
	virtual ~SharedMemory();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(SharedMemory)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHAREDMEMORY_H__FAA0E6FE_7557_401D_8D5C_516A380DA13E__INCLUDED_)
