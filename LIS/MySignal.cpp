// MySignal.cpp : implementation file
//

#include "stdafx.h"
#include "../include/eipemu.h"
#include "MySignal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern BOOL _bDebugMode;
extern BOOL _bSLS;
extern BOOL _bSIMModeSW;
extern BOOL _bSIMModeIO;

/////////////////////////////////////////////////////////////////////////////
typedef struct
{
	CString AspectSigCondition[5];
	CString NonAspectSigCondition[5];
	CString PointCondition[10];
	BOOL bAspectSigCondition[5];
	BOOL bNonAspectSigCondition[5];
	BOOL bPointCondition[10];
}RIAspectCondition;
typedef struct
{
	CString SigName[100];
	CString SigAspect[100];
	RIAspectCondition AspectCondition[100];
}RICondition;
/////////////////////////////////////////////////////////////////////////////
extern RICondition _RouteIndicatorInfo;

CMySignal::CMySignal( CEITInfo *pInfo ) : CSimObject( pInfo )
{
	m_bSLS = FALSE;
    m_bManual = FALSE;
//    m_nSignalType = pInfo->m_pSignalInfoTable[].m_nSignalType
	m_pDialog = NULL;
	m_bIsCreateSimpleDlg = FALSE;
	m_bIsCreateBlockDlg = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CMySignalDlg dialog


CMySignalDlg::CMySignalDlg( ScrInfoSignal *pInfo, BYTE nType, CWnd* pParent /*=NULL*/)
	: CDialog(CMySignalDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMySignalDlg)
	m_nTimer = 0;
	m_strName = _T("");
	m_bManual = FALSE;
	m_bSOUT1 = FALSE;
	m_bSOUT2 = FALSE;
	m_bSOUT3 = FALSE;
	m_bSOUT3C = FALSE;
	m_bSIN1 = FALSE;
	m_bSIN2 = FALSE;
	m_bSIN3 = FALSE;
	m_bSIN3O = FALSE;
	m_bSIN3C = FALSE;
	m_bSIN3CC = FALSE;
	m_bINLEFT = FALSE;
	m_bINRIGHT = FALSE;
	m_bLEFT = FALSE;
	m_bRIGHT = FALSE;
	m_bLOR = FALSE;
	//}}AFX_DATA_INIT

	m_pInfo = pInfo;
    m_nSignalType = nType;
	SetData();
}


void CMySignalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMySignalDlg)
	DDX_Text(pDX, IDC_EDIT_TIMER, m_nTimer);
	DDX_Text(pDX, IDC_EDIT_SIGNAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_MANUAL, m_bManual);
	DDX_Check(pDX, IDC_CHECK_SIN1, m_bSIN1);
	DDX_Check(pDX, IDC_CHECK_SIN2, m_bSIN2);
	DDX_Check(pDX, IDC_CHECK_SIN3, m_bSIN3);
	DDX_Check(pDX, IDC_CHECK_SIN3O, m_bSIN3O);
	DDX_Check(pDX, IDC_CHECK_SIN3C, m_bSIN3C);
	DDX_Check(pDX, IDC_CHECK_SIN3CC, m_bSIN3CC);
	DDX_Check(pDX, IDC_CHECK_SOUT1, m_bSOUT1);
	DDX_Check(pDX, IDC_CHECK_SOUT2, m_bSOUT2);
	DDX_Check(pDX, IDC_CHECK_SOUT3, m_bSOUT3);
	DDX_Check(pDX, IDC_CHECK_SOUT3C, m_bSOUT3C);
	DDX_Check(pDX, IDC_CHECK_INLEFT, m_bINLEFT);
	DDX_Check(pDX, IDC_CHECK_INRIGHT, m_bINRIGHT);
	DDX_Check(pDX, IDC_CHECK_LEFT, m_bLEFT);
	DDX_Check(pDX, IDC_CHECK_RIGHT, m_bRIGHT);
	DDX_Check(pDX, IDC_CHECK_LOR, m_bLOR);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMySignalDlg, CDialog)
	//{{AFX_MSG_MAP(CMySignalDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySignalDlg message handlers


// 제어명령에 따른 변화는 없고 단지 Data의 변화에 따라 반응한다...
void CMySignal::Run()
{
	if (!m_pData) return;

	BOOL bSignal = FALSE;
	ScrInfoSignal *pInfo = (ScrInfoSignal *)m_pData;
	ScrInfoBlock *pBlkInfo = (ScrInfoBlock *)pInfo;

	int nFlag = 0;
    int n = m_strName.Find( 'B' );
	if (!n)
	{
		if(_bDebugMode)
		{
			CMyBlockDlg *pDlg = NULL;
			if ( m_pDialog ) 
			{
				pDlg = (CMyBlockDlg*)m_pDialog;
				if (pDlg->m_hWnd) 
				{
					CButton *pButton = (CButton *)pDlg->GetDlgItem( IDC_CHECK_MANUAL );
					if (pButton) 
					{
						m_bManual = pDlg->m_bManual = pButton->GetCheck();
					}
				}
			}
			
			BYTE stype = m_nSignalType & 0xF0;
			if (stype == 0x60) return;   // Stop sign,  LOS  sign ...
			
			BYTE nDir = (m_nSignalType & SIGNALTYPE_DIR_BIT) >> 2;  // Left -> 1
			
			if ( stype == BLOCKTYPE_INTER_OUT || stype == BLOCKTYPE_INTER_IN)
			{
				if (m_bManual) 
				{
					return;
				}
			}
			else if ( stype == BLOCKTYPE_AUTO_OUT ) //TGB
			{
				if (nDir)  
				{
					ScrInfoBlock *pB2Info = (ScrInfoBlock *)pB2;    //B3 정보 update
					if (pB2Info != NULL)
					{
						pBlkInfo->LCRIN = pB2Info->LCR;
						pBlkInfo->CBBIN = pB2Info->CBB;
						pBlkInfo->ARRIVEIN = pB2Info->ARRIVE;
						pBlkInfo->COMPLETEIN = pB2Info->COMPLETE;
						pBlkInfo->CAACK = pB2Info->CAACK;
					}
				}
				else       
				{
					ScrInfoBlock *pB4Info = (ScrInfoBlock *)pB4;
					if (pB4Info != NULL)
					{
						pBlkInfo->LCRIN = pB4Info->LCR;
						pBlkInfo->CBBIN = pB4Info->CBB;
						pBlkInfo->ARRIVEIN = pB4Info->ARRIVE;
						pBlkInfo->COMPLETEIN = pB4Info->COMPLETE;
						pBlkInfo->CAACK = pB4Info->CAACK;
					}
				}
				
				if (pBlkInfo->RBGR || pBlkInfo->RBGPR)
				{
					pBlkInfo->RBGPR = 1;
					pBlkInfo->NBGPR = 0;
				}
				if (pBlkInfo->NBGR || pBlkInfo->NBGPR) 
				{
					pBlkInfo->NBGPR = 1;
					pBlkInfo->RBGPR = 0;
				}
				if (!pBlkInfo->NBGPR && !pBlkInfo->RBGPR) 
				{
					pBlkInfo->NBGPR = 1;
					pBlkInfo->RBGPR = 0;
				}
				
				// RBCR 처리
				if (pBlkInfo->RBCR || pBlkInfo->RBCPR)
				{
					pBlkInfo->RBCPR = 1;
					pBlkInfo->NBCPR = 0;
				}
				if (pBlkInfo->NBCR || pBlkInfo->NBCPR) 
				{
					pBlkInfo->NBCPR = 1;
					pBlkInfo->RBCPR = 0;
				}
				if (!pBlkInfo->NBCPR && !pBlkInfo->RBCPR) 
				{
					pBlkInfo->NBCPR = 1;
					pBlkInfo->RBCPR = 0;
				}
			}
			else if ( stype == BLOCKTYPE_AUTO_IN ) //TCB
			{
				if (nDir)  
				{
					ScrInfoBlock *pB1Info = (ScrInfoBlock *)pB1;  //B4  정보 update
					if (pB1Info != NULL)
					{
						pBlkInfo->CA = pB1Info->CA;
						pBlkInfo->LCRIN = pB1Info->LCR;
						pBlkInfo->CBBIN = pB1Info->CBB;
						pBlkInfo->DEPARTIN = pB1Info->DEPART;
						pBlkInfo->COMPLETEIN = pB1Info->COMPLETE;
					}
				}
				else       
				{
					ScrInfoBlock *pB3Info = (ScrInfoBlock *)pB3;
					if (pB3Info != NULL)
					{
						pBlkInfo->CA = pB3Info->CA;
						pBlkInfo->LCRIN = pB3Info->LCR;
						pBlkInfo->CBBIN = pB3Info->CBB;
						pBlkInfo->DEPARTIN = pB3Info->DEPART;
						pBlkInfo->COMPLETEIN = pB3Info->COMPLETE;
					}
				}
				
				if (pBlkInfo->RBCR || pBlkInfo->RBCPR)
				{
					pBlkInfo->RBCPR = 1;
					pBlkInfo->NBCPR = 0;
				}
				if (pBlkInfo->NBCR || pBlkInfo->NBCPR) 
				{
					pBlkInfo->NBCPR = 1;
					pBlkInfo->RBCPR = 0;
				}
				if (!pBlkInfo->NBCPR && !pBlkInfo->RBCPR) 
				{
					pBlkInfo->NBCPR = 1;
					pBlkInfo->RBCPR = 0;
				}
				
				// RBGR 처리...
				if (pBlkInfo->RBGR || pBlkInfo->RBGPR)
				{
					pBlkInfo->RBGPR = 1;
					pBlkInfo->NBGPR = 0;
				}
				if (pBlkInfo->NBGR || pBlkInfo->NBGPR) 
				{
					pBlkInfo->NBGPR = 1;
					pBlkInfo->RBGPR = 0;
				}
				if (!pBlkInfo->NBGPR && !pBlkInfo->RBGPR) 
				{
					pBlkInfo->NBGPR = 1;
					pBlkInfo->RBGPR = 0;
				}
			}
			if ( m_pDialog ) 
			{
				pDlg->UpdateData( TRUE );
				pDlg->GetData();
			}
		}
		else
		{
			RunBlock(pBlkInfo);
			m_PrevBlockInfo = *pBlkInfo;
		}
	}
	else
	{
		if(_bDebugMode)
		{
			RunSignal(pInfo);
		}
		else
		{
			RunSimpleSignal(pInfo);
		}
	}
}

void CMySignal::RunSignal(ScrInfoSignal *pInfo)
{
	CMySignalDlg *pDlg = NULL;
	if ( m_pDialog ) 
	{
		pDlg = (CMySignalDlg*)m_pDialog;
		if (pDlg->m_hWnd) 
		{
			CButton *pButton = (CButton *)pDlg->GetDlgItem( IDC_CHECK_MANUAL );
			if (pButton) 
			{
				m_bManual = pDlg->m_bManual = pButton->GetCheck();
			}
		}
	}
	
	if (m_bManual) return;
	
	BYTE stype = m_nSignalType & 0xF0;
	BYTE nDir = (m_nSignalType & SIGNALTYPE_DIR_BIT) >> 2;  // Left -> 1
	
	// Simulation을 위해 출력을 입력으로 대치한다...
	pInfo->SIN1 = pInfo->SOUT1;
	pInfo->SIN2 = pInfo->SOUT2;
	pInfo->LM_CLOR = pInfo->SIN3 = pInfo->SOUT3;
	if (pInfo->LEFT || pInfo->RIGHT)
		pInfo->INLEFT = 1;
	
	//pInfo->INLEFT = pInfo->LEFT;
	//pInfo->INRIGHT = pInfo->RIGHT;
	pInfo->INU = pInfo->U;
	// pInfo->ROUTE는 좀 더 정확한 이해를 해야 할 것 같다....
	// 신호기에 대한 전체 입력이 1일 경우에 비로서 0이된다... 
	//pInfo->ROUTE = !pInfo->SIN1 || !pInfo->SIN2 || !pInfo->SIN3 || !pInfo->INU;
// 	pInfo->LM_YL = 0;
// 	pInfo->LM_RL = 0;
// 	pInfo->LM_GL = 0;
	//		pInfo->LM_Y1L = 0;
	// 주부심 단심이 정상임... --> 강제적으로 모든 신호기에 대해 1로 설정한다.
	//		pInfo->LM_LMR = 1;   
	if (pInfo->SIN3 || pInfo->SIN2 || pInfo->SIN1)
	{
		int debug = 1;
	}
	// 신호기 현시별로 simulation을 처리한다.... --> SIN3가 신호기 현시의 가장 LOW한 단계임...
	pInfo->LM_LOR = 1;
	
	if (m_nSignalLamp == 0x09)   // Red + Shunt signal --> Shunt On 시 Red Off 되어야 한다....
	{
		if (pInfo->INU)
		{
			pInfo->LM_SLOR = 1;
			pInfo->LM_LOR = 0;
		}
		int debug = 1;
	}
	else if ( (m_nSignalType & 0x0f8) == 0x0b8 ) // Ground shunt임을 나타냄...     debug을 위해...
	{	
		pInfo->LM_LOR = 0;
		int debug = 1;
	}
	
	
	if (stype == 0xB0)  // shunt signal
	{
		if (pInfo->INU)
		{
			pInfo->LM_LOR = 0;
		}
	}
	
	if ( m_pDialog )
	{
		pDlg->UpdateData( TRUE );
		pDlg->GetData();
	}
}

void CMySignal::RunSimpleSignal(ScrInfoSignal *pInfo)
{

	if(m_bIsCreateSimpleDlg == FALSE)
	{
		m_SimpleDialog.m_strName = m_strName;
		m_SimpleDialog.ReceiveSignalInformation( pInfo, m_nSignalType, m_nSignalLamp, m_nSignalLampExt);
		m_SimpleDialog.Create(IDD_DIALOG_SIGNAL_SIMPLE, NULL);
		
		m_bIsCreateSimpleDlg = TRUE;
	}

	if ( m_bSLS != _bSLS)
	{
		m_bSLS = _bSLS;
		m_SimpleDialog.m_bPickUpALR = !m_bSLS;
		m_SimpleDialog.OnUpdateState();
	}

	if ( m_SimpleDialog.m_bWantToControlTheLamp )
	{
		switch ( m_SimpleDialog.m_nSetSignal )
		{
		case SET_GREEN:
			pInfo->SOUT1 = 1;
			pInfo->SOUT2 = 1;
			pInfo->U = 0;
			break;
		case SET_YELLOW:
			pInfo->SOUT2 = 1;
			pInfo->U = 0;
			break;
		case SET_DOUBLE_YELLOW:
			pInfo->SOUT2 = 1;
			pInfo->SOUT3 = 1;
			pInfo->U = 0;
			break;
		case SET_CALLON:
			pInfo->SE3 = 1;
			pInfo->SOUT3 = 1;
			pInfo->CS =1;
			pInfo->U = 0;
			break;
		case SET_SHUNT:
			pInfo->SE4 = 1;
			pInfo->U = 1;
			pInfo->CS = 1;
			break;
		case SET_RED:
			pInfo->SOUT1 = 0;
			pInfo->SOUT2 = 0;
			pInfo->SOUT3 = 0;
			pInfo->U = 0;
		}

		pInfo->SIN1 = pInfo->SOUT1;
		pInfo->SIN2 = pInfo->SOUT2;
		pInfo->SIN3 = pInfo->SOUT3;
		pInfo->INU = pInfo->U;
		
		if (m_SimpleDialog.m_bSetLeftIndicator | m_SimpleDialog.m_bSetRouteIndicator)
		{
			pInfo->LEFT |= m_SimpleDialog.m_bSetLeftIndicator | m_SimpleDialog.m_bSetRouteIndicator;
		}
		if (m_SimpleDialog.m_bSetRightIndicator)
		{
			pInfo->RIGHT |= m_SimpleDialog.m_bSetRightIndicator;
		}
		
		m_SimpleDialog.OnUpdateState();
		pInfo->LM_LOR = m_SimpleDialog.m_bPickUpLOR;
		pInfo->LM_SLOR = m_SimpleDialog.m_bPickUpSLOR;
		pInfo->LM_CLOR = ((m_SimpleDialog.m_bPickUpCLOR | m_SimpleDialog.m_bPickUpHHLOR) & pInfo->SOUT3);

// 		pInfo->LM_EKR = m_SimpleDialog.m_bPickUpEKR;
// 		pInfo->LM_CEKR = m_SimpleDialog.m_bPickUpCEKR;

		pInfo->INLEFT = (m_SimpleDialog.m_bPickUpDIRLOR & (pInfo->LEFT | pInfo->RIGHT));
	}
	else
	{
		m_SimpleDialog.OnUpdateState();
		pInfo->SIN1 = pInfo->SOUT1;
		pInfo->SIN2 = pInfo->SOUT2;
		pInfo->SIN3 = pInfo->SOUT3;
		pInfo->INU = pInfo->U;
	}

	if (_bSIMModeIO == TRUE )
	{
		CheckSinalAspect(m_strName, pInfo);

		CString strAspect = GetRouteIndicatorInfo(m_strName);

		if ((strAspect != "" || m_SimpleDialog.m_bSetLeftIndicator || m_SimpleDialog.m_bSetRouteIndicator) && !m_SimpleDialog.m_bFailLeftIndicator && strAspect != "R")
		{
			pInfo->LEFT = TRUE;
			pInfo->INLEFT = TRUE;
		}
		else
		{
			pInfo->LEFT = FALSE;
			pInfo->INLEFT = FALSE;
		}
		if ((strAspect != "" || m_SimpleDialog.m_bSetRightIndicator) && !m_SimpleDialog.m_bFailRightIndicator && strAspect != "L")
		{
			pInfo->RIGHT = TRUE;
			pInfo->INRIGHT = TRUE;
		}
		else
		{
			pInfo->RIGHT = FALSE;
			pInfo->INRIGHT = FALSE;
		}
	}
}

void CMySignal::CheckSinalAspect(CString strSigName, ScrInfoSignal *pInfo)
{
	CString strSigBuf;
	CString strSigNameBuf;
	CString strSigAspectBuf;

	for( int i = 0; _RouteIndicatorInfo.SigName[i] != ""; i++)
	{
		for ( int k = 0; _RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[k] != ""; k++)
		{
			strSigBuf = _RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[k];
			strSigAspectBuf = strSigBuf.Right(1);
			strSigNameBuf = strSigBuf.Left(strSigBuf.GetLength()-1);

			if ( strSigNameBuf == m_strName )
			{
				if ( strSigAspectBuf == "M" && !pInfo->SIN2 && !pInfo->SIN3 )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "H" && !pInfo->SIN2 )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "D" && !pInfo->SIN1 )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "C" && !pInfo->SIN3 )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "S" && !pInfo->INU )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else if ( strSigAspectBuf == "A" && !pInfo->SIN1 && !pInfo->SIN2 && !pInfo->SIN3 && !pInfo->INU )
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = FALSE;
					break;
				}
				else
				{
					_RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] = TRUE;
				}
			}
		}
		for (int l = 0; _RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[l] != ""; l++)
		{
			strSigBuf = _RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[l];
			strSigAspectBuf = strSigBuf.Right(1);
			strSigNameBuf = strSigBuf.Left(strSigBuf.GetLength()-1);
			
			if ( strSigAspectBuf == "H" && pInfo->SIN2 )
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = FALSE;
				break;
			}
			else if ( strSigAspectBuf == "D" && pInfo->SIN1 )
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = FALSE;
				break;
			}
			else if ( strSigAspectBuf == "C" && pInfo->SIN3 )
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = FALSE;
				break;
			}
			else if ( strSigAspectBuf == "S" && pInfo->INU )
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = FALSE;
				break;
			}
			else if ( strSigAspectBuf == "A" && ( pInfo->SIN1 || pInfo->SIN2 || pInfo->SIN3 || pInfo->INU ))
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = FALSE;
				break;
			}
			else
			{
				_RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[l] = TRUE;
			}
		}
	}
}

CString CMySignal::GetRouteIndicatorInfo(CString strSigName)
{
	BOOL bAspectConditionOK = FALSE;

	for(int i = 0; _RouteIndicatorInfo.SigName[i] != ""; i++)
	{
		if ( strSigName == _RouteIndicatorInfo.SigName[i])
		{
			bAspectConditionOK = TRUE;

			for(int k = 0; k < 10; k++)
			{
				if ( k < 5 )
				{
					if ( _RouteIndicatorInfo.AspectCondition[i].bAspectSigCondition[k] == FALSE )
					{
						bAspectConditionOK = FALSE;
						break;
					}
					if ( _RouteIndicatorInfo.AspectCondition[i].bNonAspectSigCondition[k] == FALSE )
					{
						bAspectConditionOK = FALSE;
						break;
					}
				}
				if ( _RouteIndicatorInfo.AspectCondition[i].bPointCondition[k] == FALSE )
				{
					bAspectConditionOK = FALSE;
					break;
				}
			}

			if ( bAspectConditionOK == TRUE )
			{
				return _RouteIndicatorInfo.SigAspect[i];
			}
		}
	}
	return "";
}


void CMySignal::RunBlock(ScrInfoBlock *pInfo)
{
	if(m_bIsCreateBlockDlg == FALSE)
	{
		m_BlockDlg.m_strName = m_strName;
		m_BlockDlg.ReceiveSignalInformation( pInfo, m_nSignalType );
		m_BlockDlg.Create(IDD_DIALOG_BLOCK_SIMPLE, NULL);
		
		m_bIsCreateBlockDlg = TRUE;
	}

	if ((m_nSignalType & 0xF0) == BLOCKTYPE_AUTO_OUT)
	{
		if (pInfo->NBGR && !m_PrevBlockInfo.NBGR)
		{
			m_BlockDlg.m_bIsTrainInBlockSection = FALSE;
		}
		else if (pInfo->RBGR && !m_PrevBlockInfo.RBGR)
		{
			m_BlockDlg.m_bIsTrainInBlockSection = TRUE;
		}
		pInfo->NBGPR = !m_BlockDlg.m_bIsTrainInBlockSection;
		pInfo->RBGPR = m_BlockDlg.m_bIsTrainInBlockSection;

		if ( pInfo->CBBOP )
		{
			pInfo->LCRIN = FALSE;
			pInfo->CAACK = FALSE;
			pInfo->ARRIVEIN = FALSE;
			
			m_BlockDlg.m_bCheckCA = FALSE;
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
		if ( pInfo->CBBIN )
		{
			pInfo->LCRIN = FALSE;
			pInfo->CAACK = FALSE;
			pInfo->ARRIVEIN = FALSE;
			
			m_BlockDlg.m_bCheckCA = FALSE;
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
		if ( pInfo->DEPART && !pInfo->ARRIVEIN && !pInfo->CBBIN )
		{
			if (pInfo->SAXLACTIN )
			{
				m_BlockDlg.m_bCheckSAXLOCC = TRUE;
				pInfo->SAXLOCC = FALSE;
			}
			else
			{
				m_BlockDlg.m_bCheckAXLOCC = TRUE;
				pInfo->AXLOCC = FALSE;
			}
		}
		if ( pInfo->ARRIVEIN )
		{
			if (pInfo->SAXLACTIN )
			{
				m_BlockDlg.m_bCheckSAXLOCC = FALSE;
				pInfo->SAXLOCC = TRUE;
			}
			else
			{
				m_BlockDlg.m_bCheckAXLOCC = FALSE;
				pInfo->AXLOCC = TRUE;
			}
		}

		if(pInfo->SAXLACTIN)
		{
			if ( !m_PrevBlockInfo.SAXLRST && pInfo->SAXLRST && !pInfo->SAXLOCC )
			{
				m_BlockDlg.m_bCheckSAXLOCC = FALSE;
				m_BlockDlg.m_bCheckSAXLDST = FALSE;
				m_BlockDlg.m_bCheckSAXLREQ = FALSE;
				m_BlockDlg.m_bCheckSAXLACC = FALSE;
				m_BlockDlg.m_bCheckSAXLDEC = FALSE;
				pInfo->SAXLOCC = TRUE;
				pInfo->SAXLDST = TRUE;
				pInfo->SAXLREQ = FALSE;
				pInfo->SAXLACC = FALSE;
				pInfo->SAXLDEC = FALSE;
				pInfo->SAXLRST = FALSE;
				
				CString strMsg;
				strMsg.Format("Super Block Axle related with %s was reset!!", m_strName);
				MessageBox(NULL, strMsg, "AXLE STATUS", MB_OK | MB_ICONINFORMATION | MB_TOPMOST );
			}
		}
		else
		{
			if ( !m_PrevBlockInfo.AXLRST && pInfo->AXLRST && !pInfo->AXLOCC )
			{
				m_BlockDlg.m_bCheckAXLOCC = FALSE;
				m_BlockDlg.m_bCheckAXLDST = FALSE;
				m_BlockDlg.m_bCheckAXLREQ = FALSE;
				m_BlockDlg.m_bCheckAXLACC = FALSE;
				m_BlockDlg.m_bCheckAXLDEC = FALSE;
				pInfo->AXLOCC = TRUE;
				pInfo->AXLDST = TRUE;
				pInfo->AXLREQ = FALSE;
				pInfo->AXLACC = FALSE;
				pInfo->AXLDEC = FALSE;
				pInfo->AXLRST = FALSE;
				
				CString strMsg;
				strMsg.Format("Block Axle related with %s was reset!!", m_strName);
				MessageBox(NULL, strMsg, "AXLE STATUS", MB_OK | MB_ICONINFORMATION | MB_TOPMOST );
			}
		}

		if(!pInfo->SWEEPING && pInfo->SWEEPINGACK)
		{
			pInfo->SWEEPINGACK = FALSE;
			pInfo->BLKRSTREQ = FALSE;
			pInfo->BLKRSTACC = FALSE;
			pInfo->BLKRSTREQIN = FALSE;
			pInfo->BLKRSTACCIN = FALSE;
		}
	}
	else if ((m_nSignalType & 0XF0) == BLOCKTYPE_AUTO_IN)
	{
		if (pInfo->NBCR && !m_PrevBlockInfo.NBCR)
		{
			m_BlockDlg.m_bIsTrainInBlockSection = FALSE;
		}
		else if (pInfo->RBCR && !m_PrevBlockInfo.RBCR)
		{
			m_BlockDlg.m_bIsTrainInBlockSection = TRUE;
		}
		pInfo->NBCPR = !m_BlockDlg.m_bIsTrainInBlockSection;
		pInfo->RBCPR = m_BlockDlg.m_bIsTrainInBlockSection;

		if ( pInfo->CBBOP )
		{
			pInfo->LCRIN = FALSE;
			pInfo->CA = FALSE;
			pInfo->DEPARTIN = FALSE;
			
			m_BlockDlg.m_bCheckCA = FALSE;
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
		if ( pInfo->CBBIN )
		{
			pInfo->LCRIN = FALSE;
			pInfo->CA = FALSE;
			pInfo->DEPARTIN = FALSE;
			
			m_BlockDlg.m_bCheckCA = FALSE;
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
		if ( pInfo->DEPARTIN )
		{
			if ( pInfo->SAXLACTIN )
			{
				m_BlockDlg.m_bCheckSAXLOCC = TRUE;
				pInfo->SAXLOCC = FALSE;
			}
			else
			{
				m_BlockDlg.m_bCheckAXLOCC = TRUE;
				pInfo->AXLOCC = FALSE;
			}
		}
		if ( pInfo->ARRIVE )
		{
			if ( pInfo->SAXLACTIN )
			{
				m_BlockDlg.m_bCheckSAXLOCC = FALSE;
				pInfo->SAXLOCC = TRUE;
			}
			else
			{
				m_BlockDlg.m_bCheckAXLOCC = FALSE;
				pInfo->AXLOCC = TRUE;
			}
		}

		if(pInfo->SAXLACTIN)
		{
			if ( !m_PrevBlockInfo.SAXLRST && pInfo->SAXLRST && !pInfo->SAXLOCC )
			{
				m_BlockDlg.m_bCheckSAXLOCC = FALSE;
				m_BlockDlg.m_bCheckSAXLDST = FALSE;
				pInfo->SAXLOCC = TRUE;
				pInfo->SAXLDST = TRUE;
				pInfo->SAXLRST = FALSE;
				
				CString strMsg;
				strMsg.Format("Super Block Axle related with %s was reset!!", m_strName);
				MessageBox(NULL, strMsg, "AXLE STATUS", MB_OK | MB_ICONINFORMATION | MB_TOPMOST );
			}
		}
		else
		{
			if ( !m_PrevBlockInfo.AXLRST && pInfo->AXLRST && !pInfo->AXLOCC )
			{
				m_BlockDlg.m_bCheckAXLOCC = FALSE;
				m_BlockDlg.m_bCheckAXLDST = FALSE;
				pInfo->AXLOCC = TRUE;
				pInfo->AXLDST = TRUE;
				pInfo->AXLRST = FALSE;

				CString strMsg;
				strMsg.Format("Block Axle related with %s was reset!!", m_strName);
				MessageBox(NULL, strMsg, "AXLE STATUS", MB_OK | MB_ICONINFORMATION | MB_TOPMOST );
			}
		}

		if(pInfo->SWEEPING && pInfo->SWEEPINGACK)
		{
			pInfo->BLKRSTREQ = FALSE;
			pInfo->BLKRSTACC = FALSE;
			pInfo->BLKRSTREQIN = FALSE;
			pInfo->BLKRSTACCIN = FALSE;
			
			pInfo->SWEEPING = FALSE;
		}
	}
	else if ((m_nSignalType & 0XF0) == BLOCKTYPE_INTER_OUT)
	{
		if ( pInfo->CBBIN )
		{
			pInfo->LCRIN = FALSE;
			pInfo->DEPART = FALSE;
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
	}
	else if ((m_nSignalType & 0XF0) == BLOCKTYPE_INTER_IN)
	{
		if ( pInfo->CBBIN )
		{
			pInfo->LCRIN = FALSE;
			pInfo->DEPARTIN = FALSE;
			
			m_BlockDlg.m_bCheckLCR = FALSE;
			m_BlockDlg.m_bCheckDEPART = FALSE;
		}
	}
}

void CMySignal::DialogOpen()
{
    int n = m_strName.Find( 'B' );
	if (!n)
	{
		if (_bDebugMode)
		{
			CMyBlockDlg dlg( (ScrInfoBlock*)m_pData, m_nSignalType );
			dlg.m_strName = m_strName;
			dlg.m_bManual = m_bManual;
			m_pDialog = &dlg;
			if (dlg.DoModal() == IDOK) {
				m_bManual = dlg.m_bManual;
			}
			m_pDialog = NULL;
		}
		else
		{
			m_BlockDlg.ShowWindow(SW_SHOW);
		}
	}
	else
	{
		if (_bDebugMode)
		{
			CMySignalDlg dlg( (ScrInfoSignal*)m_pData, m_nSignalType );
			dlg.m_strName = m_strName;
			dlg.m_bManual = m_bManual;
			m_pDialog = &dlg;
			if (dlg.DoModal() == IDOK) {
				m_bManual = dlg.m_bManual;
			}
			m_pDialog = NULL;
		}
		else
		{
			m_SimpleDialog.ShowWindow(SW_SHOW);
		}
	}
}

void CMySignal::ReceiveControlMessage(BYTE byteFunc)
{
	if ((m_nSignalType & 0XF0) == BLOCKTYPE_AUTO_IN || (m_nSignalType & 0XF0) == BLOCKTYPE_AUTO_OUT)
	{
		m_BlockDlg.m_bSimInitialized = TRUE; 
	}
}

void CMySignalDlg::OnOK() 
{
	GetData();
	CDialog::OnOK();
}

void CMySignalDlg::SetData()
{
	if (m_pInfo) 
	{
		m_bSOUT3 = (m_pInfo->SOUT3 != 0);
		m_bSOUT2 = (m_pInfo->SOUT2 != 0);
		m_bSOUT1 = (m_pInfo->SOUT1 != 0);
		m_bSIN3 = (m_pInfo->SIN3 != 0);
		m_bSIN2 = (m_pInfo->SIN2 != 0);
		m_bSIN1 = (m_pInfo->SIN1 != 0);
		m_bINLEFT = (m_pInfo->INLEFT != 0);
		m_bINRIGHT = (m_pInfo->INRIGHT != 0);
		m_bLEFT = (m_pInfo->LEFT != 0);
		m_bRIGHT = (m_pInfo->RIGHT != 0);
		m_bLOR = (m_pInfo->LM_LOR != 0);
	}
//	UpdateData( FALSE );
}

void CMySignalDlg::GetData()
{
	UpdateData( TRUE );
	if (m_pInfo) 
	{
        BYTE stype = m_nSignalType & 0xF0;
        if ( stype == BLOCKTYPE_INTER_OUT ) 
		{

        }
        else 
		{
		    m_pInfo->SOUT3 = m_bSOUT3;
		    m_pInfo->SOUT2 = m_bSOUT2;
		    m_pInfo->SOUT1 = m_bSOUT1;
		    m_pInfo->SIN3 = m_bSIN3;
		    m_pInfo->SIN2 = m_bSIN2;
		    m_pInfo->SIN1 = m_bSIN1;
			m_pInfo->INLEFT = m_bINLEFT;
			m_pInfo->INRIGHT = m_bRIGHT;
			m_pInfo->LEFT = m_bLEFT;
			m_pInfo->RIGHT = m_bRIGHT;
			m_pInfo->LM_LOR = m_bLOR;
        }
//	    m_pInfo->REQUEST = m_bREQUEST;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMyBlockDlg dialog


CMyBlockDlg::CMyBlockDlg( ScrInfoBlock *pBlkInfo, BYTE nType, CWnd* pParent /*=NULL*/)
	: CDialog(CMyBlockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMyBlockDlg)
	m_nTimer = 0;
	m_strName = _T("");
	m_bManual = FALSE;
	m_bCA = FALSE;
	m_bLCR = FALSE;
	m_bLCRIN = FALSE;
	m_bLCROP = FALSE;
	m_bCBB = FALSE;
	m_bCBBIN = FALSE;
	m_bCBBOP = FALSE;
	m_bCOMPLETEIN = FALSE;
	m_bCONFIRM = FALSE;
    m_bASTART = FALSE;
	m_bDEPART = FALSE;
	m_bDEPARTIN = FALSE;
	m_bARRIVE = FALSE;
	m_bARRIVEIN = FALSE;
// 	m_bARRIVETR = FALSE;
	m_bCOMPLETE = FALSE;
	m_bSTCLOSE = FALSE;
	m_bSTCLOSEIN = FALSE;
	m_bNBGR = FALSE;
	m_bRBGR = FALSE;
	m_bNBCR = FALSE;
	m_bRBCR = FALSE;
	m_bNBGPR = FALSE;
	m_bRBGPR = FALSE;
	m_bNBCPR = FALSE;
	m_bRBCPR = FALSE;
	//}}AFX_DATA_INIT


	m_pInfo = pBlkInfo;

	if (!m_pInfo)
	{
		m_bAXLOCC = (pBlkInfo->AXLOCC != 0);
		m_bAXLDST = (pBlkInfo->AXLDST != 0);
	}
    m_nSignalType = nType;
	SetData();
}


void CMyBlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyBlockDlg)
	DDX_Text(pDX, IDC_EDIT_TIMER, m_nTimer);
	DDX_Text(pDX, IDC_EDIT_SIGNAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_MANUAL, m_bManual);
	DDX_Check(pDX, IDC_CHECK_CA, m_bCA);
	DDX_Check(pDX, IDC_CHECK_LCR, m_bLCR);
	DDX_Check(pDX, IDC_CHECK_LCRIN, m_bLCRIN);
	DDX_Check(pDX, IDC_CHECK_LCROP, m_bLCROP);
	DDX_Check(pDX, IDC_CHECK_CBB, m_bCBB);
	DDX_Check(pDX, IDC_CHECK_CBBIN, m_bCBBIN);
	DDX_Check(pDX, IDC_CHECK_CBBOP, m_bCBBOP);
	DDX_Check(pDX, IDC_CHECK_COMPLETEIN, m_bCOMPLETEIN);
	DDX_Check(pDX, IDC_CHECK_CONFIRM, m_bCONFIRM);
	DDX_Check(pDX, IDC_CHECK_ASTART, m_bASTART);
	DDX_Check(pDX, IDC_CHECK_DEPART, m_bDEPART);
	DDX_Check(pDX, IDC_CHECK_DEPARTIN, m_bDEPARTIN);
	DDX_Check(pDX, IDC_CHECK_ARRIVE, m_bARRIVE);
	DDX_Check(pDX, IDC_CHECK_ARRIVEIN, m_bARRIVEIN);
// 	DDX_Check(pDX, IDC_CHECK_ARRIVETR, m_bARRIVETR);
	DDX_Check(pDX, IDC_CHECK_COMPLETE, m_bCOMPLETE);
	DDX_Check(pDX, IDC_CHECK_STCLOSE, m_bSTCLOSE);
	DDX_Check(pDX, IDC_CHECK_STCLOSEIN, m_bSTCLOSEIN);
	DDX_Check(pDX, IDC_CHECK_NBGR, m_bNBGR);
	DDX_Check(pDX, IDC_CHECK_RBGR, m_bRBGR);
	DDX_Check(pDX, IDC_CHECK_NBCR, m_bNBCR);
	DDX_Check(pDX, IDC_CHECK_RBCR, m_bRBCR);
	DDX_Check(pDX, IDC_CHECK_NBGPR, m_bNBGPR);
	DDX_Check(pDX, IDC_CHECK_RBGPR, m_bRBGPR);
	DDX_Check(pDX, IDC_CHECK_NBCPR, m_bNBCPR);
	DDX_Check(pDX, IDC_CHECK_RBCPR, m_bRBCPR);
	DDX_Check(pDX, IDC_CHECK_AXLOCC, m_bAXLOCC);
	DDX_Check(pDX, IDC_CHECK_AXLDST, m_bAXLDST);
	if ( m_bAXLDST == FALSE )
	{
		m_bAXLOCC = FALSE;
	}
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMyBlockDlg, CDialog)
	//{{AFX_MSG_MAP(CMyBlockDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyBlockDlg message handlers

void CMyBlockDlg::OnOK() 
{
	GetData();
	CDialog::OnOK();
}

void CMyBlockDlg::SetData()
{
	if (m_pInfo) 
	{
		m_bCA = (m_pInfo->CA != 0);
		m_bLCR = (m_pInfo->LCR != 0);
		m_bLCRIN = (m_pInfo->LCRIN != 0);
		m_bLCROP = (m_pInfo->LCROP != 0);
		m_bCBB = (m_pInfo->CBB != 0);
		m_bCBBIN = (m_pInfo->CBBIN != 0);
		m_bCBBOP = (m_pInfo->CBBOP != 0);
		m_bCOMPLETEIN = (m_pInfo->COMPLETEIN != 0);
		m_bCONFIRM = (m_pInfo->CONFIRM != 0);
		m_bASTART = (m_pInfo->ASTART != 0);
		m_bDEPART = (m_pInfo->DEPART != 0);
		m_bDEPARTIN = (m_pInfo->DEPARTIN != 0);
		m_bARRIVE = (m_pInfo->ARRIVE != 0);
		m_bARRIVEIN = (m_pInfo->ARRIVEIN != 0);
		m_bCOMPLETE = (m_pInfo->COMPLETE != 0);
		m_bSTCLOSEIN = (m_pInfo->SAXLACTIN != 0);
		m_bNBGR = (m_pInfo->NBGR != 0);
		m_bRBGR = (m_pInfo->RBGR != 0);
		m_bNBCR = (m_pInfo->NBCR != 0);
		m_bRBCR = (m_pInfo->RBCR != 0);
		m_bNBGPR = (m_pInfo->NBGPR != 0);
		m_bRBGPR = (m_pInfo->RBGPR != 0);
		m_bNBCPR = (m_pInfo->NBCPR != 0);
		m_bRBCPR = (m_pInfo->RBCPR != 0);
		m_bAXLOCC = (m_pInfo->AXLOCC != 0);
		m_bAXLDST = (m_pInfo->AXLDST != 0);
	}
//	UpdateData( FALSE );
}

void CMyBlockDlg::GetData()
{
	UpdateData( TRUE );
	if (m_pInfo) 
	{
        BYTE stype = m_nSignalType & 0xF0;

	    m_pInfo->CA = m_bCA;
	    m_pInfo->LCR = m_bLCR;
	    m_pInfo->LCRIN = m_bLCRIN;
	    m_pInfo->LCROP = m_bLCROP;
	    m_pInfo->CBB = m_bCBB;
	    m_pInfo->CBBIN = m_bCBBIN;
		m_pInfo->CBBOP = m_bCBBOP;
		m_pInfo->COMPLETEIN = m_bCOMPLETEIN;
		m_pInfo->CONFIRM = m_bCONFIRM;
		m_pInfo->ASTART = m_bASTART;
		m_pInfo->DEPART = m_bDEPART;
		m_pInfo->DEPARTIN = m_bDEPARTIN;
		m_pInfo->ARRIVE = m_bARRIVE;
		m_pInfo->ARRIVEIN = m_bARRIVEIN;
	    m_pInfo->COMPLETE = m_bCOMPLETE;
	    m_pInfo->SAXLACT = m_bSTCLOSE;
	    m_pInfo->NBGR = m_bNBGR;
		m_pInfo->RBGR = m_bRBGR;
		m_pInfo->NBCR = m_bNBCR;
		m_pInfo->RBCR = m_bRBCR;
		m_pInfo->NBGPR = m_bNBGPR;
		m_pInfo->RBGPR = m_bRBGPR;
		m_pInfo->NBCPR = m_bNBCPR;
		m_pInfo->RBCPR = m_bRBCPR;
		m_pInfo->AXLOCC = m_bAXLOCC;
		m_pInfo->AXLDST = m_bAXLDST;
//	    m_pInfo->REQUEST = m_bREQUEST;
	}
}
/////////////////////////////////////////////////////////////////////////////
// CMySimpleSignalDlg dialog


CMySimpleSignalDlg::CMySimpleSignalDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMySimpleSignalDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMySimpleSignalDlg)
	m_bWantToControlTheLamp = TRUE;
	m_bWantToControlTheRelay = FALSE;
	m_bFailGreen = FALSE;
	m_bFailYellow = FALSE;
	m_bFailRed = FALSE;
	m_bFail2ndYellow = FALSE;
	m_bFailLeftIndicator = FALSE;
	m_bFailRightIndicator = FALSE;
	m_bFailRouteIndicator = FALSE;
	m_bFailCallon = FALSE;
	m_bFailShunt = FALSE;
	m_bSetGreen = FALSE;
	m_bSetYellow = FALSE;
	m_bSetDoubleYellow = FALSE;
	m_bSetRed = FALSE;
	m_bSetLeftIndicator = FALSE;
	m_bSetRightIndicator = FALSE;
	m_bSetRouteIndicator = FALSE;
	m_bSetCallon = FALSE;
	m_bSetShunt = FALSE;
	m_bPickUpLOR = TRUE;
	m_bPickUpHHLOR = FALSE;
	m_bPickUpCLOR = FALSE;
	m_bPickUpSLOR = FALSE;
	m_bPickUpDIRLOR = FALSE;
	m_bPickUpALR = FALSE;
	m_bPickUpDR = FALSE;
	m_bPickUpHR = FALSE;
	m_bPickUpHHR = FALSE;
	m_bPickUpCR = FALSE;
	m_bPickUpSR = FALSE;
	m_bAdjCond = TRUE;
	m_bPickUpCEKR = FALSE;
	m_bPickUpEKR = TRUE;
	m_nSetSignal = 0;
	m_bFailRedFilament = FALSE;
	m_bFailYellowFilament = FALSE;
	m_bFail2ndYellowFilament = FALSE;
	m_bFailGreenFilament = FALSE;
	//}}AFX_DATA_INIT
}


void CMySimpleSignalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMySimpleSignalDlg)
	DDX_Check(pDX, IDC_CHECK_LAMP_CONTROL, m_bWantToControlTheLamp);
	DDX_Check(pDX, IDC_CHECK_RELAY_CONTROL, m_bWantToControlTheRelay);
	DDX_Check(pDX, IDC_CHECK_FAIL_GREEN, m_bFailGreen);
	DDX_Check(pDX, IDC_CHECK_FAIL_YELLOW, m_bFailYellow);
	DDX_Check(pDX, IDC_CHECK_FAIL_RED, m_bFailRed);
	DDX_Check(pDX, IDC_CHECK_FAIL_YELLOW2, m_bFail2ndYellow);
	DDX_Check(pDX, IDC_CHECK_FAIL_LEFT_INDICATOR, m_bFailLeftIndicator);
	DDX_Check(pDX, IDC_CHECK_FAIL_RIGHT_INDICATOR, m_bFailRightIndicator);
	DDX_Check(pDX, IDC_CHECK_FAIL_ROUTE_INDICATOR, m_bFailRouteIndicator);
	DDX_Check(pDX, IDC_CHECK_FAIL_CALLON, m_bFailCallon);
	DDX_Check(pDX, IDC_CHECK_FAIL_SHUNT, m_bFailShunt);
	DDX_Check(pDX, IDC_CHECK_SET_GREEN, m_bSetGreen);
	DDX_Check(pDX, IDC_CHECK_SET_YELLOW, m_bSetYellow);
	DDX_Check(pDX, IDC_CHECK_SET_YELLOW2, m_bSetDoubleYellow);
	DDX_Check(pDX, IDC_CHECK_SET_RED, m_bSetRed);
	DDX_Check(pDX, IDC_CHECK_SET_LEFT_INDICATOR, m_bSetLeftIndicator);
	DDX_Check(pDX, IDC_CHECK_SET_RIGHT_INDICATOR, m_bSetRightIndicator);
	DDX_Check(pDX, IDC_CHECK_SET_ROUTE_INDICATOR, m_bSetRouteIndicator);
	DDX_Check(pDX, IDC_CHECK_SET_CALLON, m_bSetCallon);
	DDX_Check(pDX, IDC_CHECK_SET_SHUNT, m_bSetShunt);
	DDX_Check(pDX, IDC_CHECK_FAIL_LOR, m_bPickUpLOR);
	DDX_Check(pDX, IDC_CHECK_FAIL_HHLOR, m_bPickUpHHLOR);
	DDX_Check(pDX, IDC_CHECK_FAIL_CLOR, m_bPickUpCLOR);
	DDX_Check(pDX, IDC_CHECK_FAIL_SLOR, m_bPickUpSLOR);
	DDX_Check(pDX, IDC_CHECK_FAIL_INDICATOR_LOR, m_bPickUpDIRLOR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_ALR, m_bPickUpALR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_DR, m_bPickUpDR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_HR, m_bPickUpHR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_HHR, m_bPickUpHHR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_CR, m_bPickUpCR);
	DDX_Check(pDX, IDC_CHECK_CONTROL_SR, m_bPickUpSR);
	DDX_Check(pDX, IDC_CHECK_ADJ_COND, m_bAdjCond);
	DDX_Check(pDX, IDC_CHECK_FAIL_CEKR, m_bPickUpCEKR);
	DDX_Check(pDX, IDC_CHECK_FAIL_EKR, m_bPickUpEKR);
	DDX_Text(pDX, IDC_EDIT_SIGNAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_FAIL_R_FILAMENT, m_bFailRedFilament);
	DDX_Check(pDX, IDC_CHECK_FAIL_Y_FILAMENT, m_bFailYellowFilament);
	DDX_Check(pDX, IDC_CHECK_FAIL_YY_FILAMENT, m_bFail2ndYellowFilament);
	DDX_Check(pDX, IDC_CHECK_FAIL_G_FILAMENT, m_bFailGreenFilament);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMySimpleSignalDlg, CDialog)
	//{{AFX_MSG_MAP(CMySimpleSignalDlg)
	ON_BN_CLICKED(IDOK2, OnOk2)
	ON_BN_CLICKED(IDC_CHECK_LAMP_CONTROL, OnCheckLampControl)
	ON_BN_CLICKED(IDC_CHECK_RELAY_CONTROL, OnCheckRelayControl)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_ALR, OnCheckControlAlr)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_CR, OnCheckControlCr)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_DR, OnCheckControlDr)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_HHR, OnCheckControlHhr)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_HR, OnCheckControlHr)
	ON_BN_CLICKED(IDC_CHECK_CONTROL_SR, OnCheckControlSr)
	ON_BN_CLICKED(IDC_CHECK_FAIL_CLOR, OnCheckFailClor)
	ON_BN_CLICKED(IDC_CHECK_FAIL_SLOR, OnCheckFailSlor)
	ON_BN_CLICKED(IDC_CHECK_FAIL_HHLOR, OnCheckFailHhlor)
	ON_BN_CLICKED(IDC_CHECK_FAIL_INDICATOR_LOR, OnCheckFailIndicatorLor)
	ON_BN_CLICKED(IDC_CHECK_FAIL_LOR, OnCheckFailLor)
	ON_BN_CLICKED(IDC_CHECK_SET_CALLON, OnCheckSetCallon)
	ON_BN_CLICKED(IDC_CHECK_SET_GREEN, OnCheckSetGreen)
	ON_BN_CLICKED(IDC_CHECK_SET_LEFT_INDICATOR, OnCheckSetLeftIndicator)
	ON_BN_CLICKED(IDC_CHECK_SET_RED, OnCheckSetRed)
	ON_BN_CLICKED(IDC_CHECK_SET_RIGHT_INDICATOR, OnCheckSetRightIndicator)
	ON_BN_CLICKED(IDC_CHECK_SET_ROUTE_INDICATOR, OnCheckSetRouteIndicator)
	ON_BN_CLICKED(IDC_CHECK_SET_SHUNT, OnCheckSetShunt)
	ON_BN_CLICKED(IDC_CHECK_SET_YELLOW, OnCheckSetYellow)
	ON_BN_CLICKED(IDC_CHECK_SET_YELLOW2, OnCheckSetYellow2)
	ON_BN_CLICKED(IDC_CHECK_FAIL_GREEN, OnCheckAnyButton)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_ADJ_COND, OnCheckAdjCond)
	ON_BN_CLICKED(IDC_CHECK_FAIL_EKR, OnCheckFailEkr)
	ON_BN_CLICKED(IDC_CHECK_FAIL_CEKR, OnCheckFailCekr)
	ON_BN_CLICKED(IDC_CHECK_FAIL_G_FILAMENT, OnCheckFailGFilament)
	ON_BN_CLICKED(IDC_CHECK_FAIL_Y_FILAMENT, OnCheckFailYFilament)
	ON_BN_CLICKED(IDC_CHECK_FAIL_R_FILAMENT, OnCheckFailRFilament)
	ON_BN_CLICKED(IDC_CHECK_FAIL_LEFT_INDICATOR, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_RED, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_RIGHT_INDICATOR, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_YELLOW, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_YELLOW2, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_ROUTE_INDICATOR, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_CALLON, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_SHUNT, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_FAIL_YY_FILAMENT, OnCheckFailYyFilament)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySimpleSignalDlg message handlers

void CMySimpleSignalDlg::ReceiveSignalInformation(ScrInfoSignal *pInfo, BYTE nType, BYTE nLamp, BYTE nLampExt)
{
	m_pInfo = pInfo;
    m_nSignalType = nType;
	m_nSignalLamp = nLamp;
	m_nSignalLampExt = nLampExt;
}

void CMySimpleSignalDlg::OnOk2() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}

BOOL CMySimpleSignalDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_bPickUpHHLOR = m_pInfo->LM_CLOR;
	m_bPickUpCLOR = m_pInfo->LM_CLOR;
	m_bPickUpSLOR = m_pInfo->LM_SLOR;
// 	m_bPickUpEKR = m_pInfo->LM_EKR;
// 	m_bPickUpCEKR = m_pInfo->LM_CEKR;
	m_bPickUpDIRLOR = ( m_pInfo->INLEFT | m_pInfo->INRIGHT );
	m_bPickUpALR = m_pInfo->LM_ALR;
	m_bPickUpDR = m_pInfo->SOUT1;
	m_bPickUpHR = m_pInfo->SOUT2;
	m_bPickUpHHR = m_pInfo->SOUT3;
	m_bPickUpCR = m_pInfo->SOUT3;
	m_bPickUpSR = m_pInfo->U;
	m_bAdjCond = m_pInfo->ADJCOND;
	RemoveNoNeededButton();
	UpdateData(FALSE);
	MakeDisable();
	SetTimer(UPDATE_ALL, 500, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMySimpleSignalDlg::MakeDisable()
{
	if(m_bWantToControlTheLamp)
	{
		GetDlgItem(IDC_CHECK_FAIL_GREEN)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_YELLOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_RED)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_YELLOW2)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_G_FILAMENT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_Y_FILAMENT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_R_FILAMENT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_YY_FILAMENT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_LEFT_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_RIGHT_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_ROUTE_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_CALLON)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_SHUNT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_GREEN)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_YELLOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_YELLOW2)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_RED)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_LEFT_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_RIGHT_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_ROUTE_INDICATOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_CALLON)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SET_SHUNT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_LOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_HHLOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_CLOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_SLOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_INDICATOR_LOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_EKR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_CEKR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_ALR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_DR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_HR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_HHR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_CR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_SR)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_CHECK_FAIL_GREEN)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_YELLOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_RED)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_YELLOW2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_G_FILAMENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_Y_FILAMENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_R_FILAMENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_YY_FILAMENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_LEFT_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_RIGHT_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_ROUTE_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_CALLON)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_SHUNT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_GREEN)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_YELLOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_YELLOW2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_RED)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_LEFT_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_RIGHT_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_ROUTE_INDICATOR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_CALLON)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SET_SHUNT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_LOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_HHLOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_CLOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_SLOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_INDICATOR_LOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_EKR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_FAIL_CEKR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTROL_ALR)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_DR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTROL_HR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTROL_HHR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTROL_CR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_CONTROL_SR)->EnableWindow(TRUE);
	}
}

void CMySimpleSignalDlg::OnCheckLampControl() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_bWantToControlTheRelay = !m_bWantToControlTheLamp;
	UpdateData(FALSE);
	MakeDisable();
}

void CMySimpleSignalDlg::OnCheckRelayControl() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_bWantToControlTheLamp = !m_bWantToControlTheRelay;
	m_bFailGreen = FALSE;
	m_bFailYellow = FALSE;
	m_bFail2ndYellow = FALSE;
	m_bFailRed = FALSE;
	m_bFailGreenFilament = FALSE;
	m_bFailYellowFilament = FALSE;
	m_bFail2ndYellowFilament = FALSE;
	m_bFailRedFilament = FALSE;
	m_bFailLeftIndicator = FALSE;
	m_bFailRightIndicator = FALSE;
	m_bFailRouteIndicator = FALSE;
	m_bFailCallon = FALSE;
	m_bFailShunt = FALSE;
	m_bSetGreen = FALSE;
	m_bSetYellow = FALSE;
	m_bSetDoubleYellow = FALSE;
	m_bSetRed = FALSE;
	m_bSetLeftIndicator = FALSE;
	m_bSetRightIndicator = FALSE;
	m_bSetRouteIndicator = FALSE;
	m_bSetCallon = FALSE;
	m_bSetShunt = FALSE;
	m_nSetSignal = FALSE;
	UpdateData(FALSE);
	MakeDisable();
}

void CMySimpleSignalDlg::OnCheckControlAlr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckControlCr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bPickUpCR)
	{
		m_bPickUpCLOR = TRUE;
	}
}

void CMySimpleSignalDlg::OnCheckControlDr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckControlHhr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bPickUpHHR)
	{
		m_bPickUpHHLOR = TRUE;
		m_bPickUpCEKR = TRUE;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckControlHr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckControlSr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bPickUpSR)
	{
		m_bPickUpSLOR = TRUE;
	}
}

void CMySimpleSignalDlg::OnCheckFailClor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailSlor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailHhlor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailIndicatorLor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailLor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckSetCallon() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetCallon)
	{
		m_bSetGreen = FALSE;
		m_bSetYellow = FALSE;
		m_bSetDoubleYellow = FALSE;
		m_bSetRed = FALSE;
		m_bSetShunt = FALSE;

		m_nSetSignal = SET_CALLON;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckSetGreen() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetGreen)
	{
		m_bSetYellow = FALSE;
		m_bSetDoubleYellow = FALSE;
		m_bSetRed = FALSE;
		m_bSetCallon = FALSE;
		m_bSetShunt = FALSE;

		m_nSetSignal = SET_GREEN;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckSetLeftIndicator() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetLeftIndicator)
		m_bSetRightIndicator = FALSE;
	else
		m_pInfo->LEFT = FALSE;
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckSetRed() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetRed)
	{
		m_bSetGreen = FALSE;
		m_bSetYellow = FALSE;
		m_bSetDoubleYellow = FALSE;
		m_bSetCallon = FALSE;
		m_bSetShunt = FALSE;
		m_bPickUpSR =FALSE;

		m_nSetSignal = SET_RED;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
	OnCheckAnyButton();
}

void CMySimpleSignalDlg::OnCheckSetRightIndicator() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetRightIndicator)
		m_bSetLeftIndicator = FALSE;
	else
		m_pInfo->RIGHT = FALSE;
	UpdateData(FALSE);
	OnCheckAnyButton();
}

void CMySimpleSignalDlg::OnCheckSetRouteIndicator() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (!m_bSetRouteIndicator)
		m_pInfo->LEFT = FALSE;
	OnCheckAnyButton();
}

void CMySimpleSignalDlg::OnCheckSetShunt() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetShunt)
	{
		m_bSetGreen = FALSE;
		m_bSetYellow = FALSE;
		m_bSetDoubleYellow = FALSE;
		m_bSetRed = FALSE;
		m_bSetCallon = FALSE;

		m_nSetSignal = SET_SHUNT;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckSetYellow() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetYellow)
	{
		m_bSetGreen = FALSE;
		m_bSetDoubleYellow = FALSE;
		m_bSetRed = FALSE;
		m_bSetCallon = FALSE;
		m_bSetShunt = FALSE;

		m_nSetSignal = SET_YELLOW;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnCheckSetYellow2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (m_bSetDoubleYellow)
	{
		m_bSetGreen = FALSE;
		m_bSetYellow = FALSE;
		m_bSetRed = FALSE;
		m_bSetCallon = FALSE;
		m_bSetShunt = FALSE;

		m_nSetSignal = SET_DOUBLE_YELLOW;
	}
	else
	{
		m_nSetSignal = NULL;
	}
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::RemoveNoNeededButton()
{
	BOOL bHasRed = m_nSignalLamp & SG_LAMP_R;
	BOOL bHasYellow = m_nSignalLamp & SG_LAMP_Y;
	BOOL bHasGreen = m_nSignalLamp & SG_LAMP_G;
	BOOL bHasShunt = m_nSignalLamp & SG_LAMP_SHUNT;
	BOOL bHasCallon = m_nSignalLamp & SG_LAMP_CALL;
	BOOL bHas2ndYellow = m_nSignalLamp & SG_LAMP_SY;
	BOOL bHasLeftIndicator = m_nSignalLamp & SG_LAMP_LL;
	BOOL bHasRightIndicator = m_nSignalLamp & SG_LAMP_LR;
	BOOL bHasRouteIndicator = m_nSignalLampExt & SG_LAMP_EXT_RI;
	if (!bHasRed)
	{
		GetDlgItem(IDC_CHECK_FAIL_RED)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_RED)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_LOR)->SetRedraw(FALSE);
	}
	if (!bHasYellow)
	{
		GetDlgItem(IDC_CHECK_FAIL_YELLOW)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_YELLOW)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_HR)->SetRedraw(FALSE);
	}
	if (!bHasGreen)
	{
		GetDlgItem(IDC_CHECK_FAIL_GREEN)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_GREEN)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_DR)->SetRedraw(FALSE);
	}
	if (!bHasShunt)
	{
		GetDlgItem(IDC_CHECK_CONTROL_SR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_SHUNT)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_SLOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_SHUNT)->SetRedraw(FALSE);
	}
	if (!bHasCallon)
	{
		GetDlgItem(IDC_CHECK_FAIL_CALLON)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_CALLON)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_CLOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_CR)->SetRedraw(FALSE);
	}
	if (!bHas2ndYellow)
	{
		GetDlgItem(IDC_CHECK_FAIL_YELLOW2)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_YELLOW2)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_HHLOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_CONTROL_HHR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_FAIL_CEKR)->SetRedraw(FALSE);
	}
	if (!bHasLeftIndicator)
	{
		GetDlgItem(IDC_CHECK_FAIL_LEFT_INDICATOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_LEFT_INDICATOR)->SetRedraw(FALSE);
	}
	if (!bHasRightIndicator)
	{
		GetDlgItem(IDC_CHECK_FAIL_RIGHT_INDICATOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_RIGHT_INDICATOR)->SetRedraw(FALSE);
	}
	if (!bHasRouteIndicator)
	{
		GetDlgItem(IDC_CHECK_FAIL_ROUTE_INDICATOR)->SetRedraw(FALSE);
		GetDlgItem(IDC_CHECK_SET_ROUTE_INDICATOR)->SetRedraw(FALSE);
	}
	if(!bHasLeftIndicator && !bHasRightIndicator && !bHasRouteIndicator)
	{
		GetDlgItem(IDC_CHECK_FAIL_INDICATOR_LOR)->SetRedraw(FALSE);
	}
}


void CMySimpleSignalDlg::OnCheckAnyButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	UpdateData(FALSE);
}


void CMySimpleSignalDlg::OnUpdateState()
{
	if ( m_bWantToControlTheLamp)
	{
		if ( (m_pInfo->SOUT1 && m_pInfo->SOUT2) || m_nSetSignal == SET_GREEN )
			m_bPickUpLOR = !m_bFailGreen;
		else if ( (!m_pInfo->SOUT1 && m_pInfo->SOUT2) || m_nSetSignal == SET_YELLOW )
			m_bPickUpLOR = !m_bFailYellow;
		else 
			m_bPickUpLOR = !m_bFailRed;

		if ( (m_pInfo->SOUT1 && m_pInfo->SOUT2) || m_nSetSignal == SET_GREEN )
			m_bPickUpEKR = !m_bFailGreenFilament;
		else if ( (!m_pInfo->SOUT1 && m_pInfo->SOUT2) || m_nSetSignal == SET_YELLOW )
			m_bPickUpEKR = !m_bFailYellowFilament;
		else 
			m_bPickUpEKR = !m_bFailRedFilament;

		if ( ( m_pInfo->SOUT3 && !m_pInfo->CS ) || m_nSetSignal == SET_DOUBLE_YELLOW )
		{
			m_bPickUpHHLOR = !m_bFail2ndYellow;
		}

		if ( ( m_pInfo->SOUT3 && !m_pInfo->CS ) || m_nSetSignal == SET_DOUBLE_YELLOW )
		{
			m_bPickUpCEKR = !m_bFail2ndYellowFilament;
		}

		else if ( ( m_pInfo->SOUT3 && m_pInfo->CS ) || m_nSetSignal == SET_CALLON )
		{
			m_bPickUpCLOR = !m_bFailCallon;
		}
		else if (  ( m_pInfo->U && m_pInfo->CS ) || m_nSetSignal == SET_SHUNT )
		{
			m_bPickUpSLOR = !m_bFailShunt;
		}
		else
		{
			m_bPickUpCLOR = FALSE;
			m_bPickUpSLOR = FALSE;
			m_bPickUpHHLOR = FALSE;
			m_bPickUpCEKR = FALSE;
		}
		
		if ( m_pInfo->LEFT && (m_pInfo->SOUT2 || m_pInfo->SOUT3 || m_pInfo->U) || m_bSetLeftIndicator )
			m_bPickUpDIRLOR = !m_bFailLeftIndicator;
		else if ( m_pInfo->RIGHT && (m_pInfo->SOUT2 || m_pInfo->SOUT3 || m_pInfo->U) || m_bSetRightIndicator )
			m_bPickUpDIRLOR = !m_bFailRightIndicator;
		else if ( m_bSetRouteIndicator )
			m_bPickUpDIRLOR = !m_bFailRouteIndicator;
		else
			m_bPickUpDIRLOR = FALSE;

		if ( m_bFailRouteIndicator )
			m_bPickUpDIRLOR = FALSE;

		m_bPickUpALR = m_pInfo->LM_ALR;
 		m_bPickUpDR = m_pInfo->SOUT1;
 		m_bPickUpHR = m_pInfo->SOUT2;
 		m_bPickUpHHR = m_pInfo->SOUT3;
 		m_bPickUpCR = m_pInfo->SOUT3;

		if ( m_nSetSignal == NULL || m_nSetSignal == SET_SHUNT )
		{
			m_bPickUpSR = m_pInfo->U;
		}
	}
	else
	{
		if ( !m_bPickUpHHR )
			m_bPickUpHHLOR = FALSE;
		if ( !m_bPickUpCR )
			m_bPickUpCLOR = FALSE;
		if ( !m_bPickUpSR )
			m_bPickUpSLOR = FALSE;
		
		m_pInfo->LM_ALR = m_bPickUpALR;
		m_pInfo->SOUT1 = m_bPickUpDR;
		m_pInfo->SOUT2 = m_bPickUpHR;
		m_pInfo->SOUT3 = (m_bPickUpHHR | m_bPickUpCR);
		m_pInfo->CS = m_pInfo->SE3 = m_bPickUpCR;
		m_pInfo->U = m_pInfo->SE4 = m_bPickUpSR;
		m_pInfo->LEFT = m_pInfo->RIGHT = m_bPickUpDIRLOR;

		if ( !(m_nSignalLamp & SG_LAMP_Y) && !m_bPickUpDR )
		{
			m_pInfo->SOUT2 = m_bPickUpDR;
		}

		if ( m_bPickUpALR )
		{
			m_bPickUpLOR = FALSE;
			m_bPickUpCLOR = FALSE;
			m_bPickUpSLOR = FALSE;
			m_bPickUpHHLOR = FALSE;
			m_bPickUpDIRLOR = FALSE;
			m_bPickUpEKR = FALSE;
			m_bPickUpCEKR = FALSE;
			m_pInfo->U = FALSE;
		}


		m_pInfo->LM_LOR = m_bPickUpLOR;
		m_pInfo->LM_CLOR = m_bPickUpHHLOR | m_bPickUpCLOR;
		m_pInfo->LM_SLOR = m_bPickUpSLOR;
// 		m_pInfo->LM_EKR = m_bPickUpEKR;
// 		m_pInfo->LM_CEKR = m_bPickUpCEKR;

		m_pInfo->INLEFT = m_bPickUpDIRLOR;

		UpdateData(FALSE);
	}
	
	m_bAdjCond = m_pInfo->ADJCOND;
	UpdateData(FALSE);
}

void CMySimpleSignalDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch (nIDEvent)
	{
	case UPDATE_ALL:
		OnUpdateState();
		UpdateData(FALSE);
		break;
	}
	
	CDialog::OnTimer(nIDEvent);
}
/////////////////////////////////////////////////////////////////////////////
// CMySimpleBlockDlg dialog


CMySimpleBlockDlg::CMySimpleBlockDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMySimpleBlockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMySimpleBlockDlg)
	m_bCheckCA = FALSE;
	m_bCheckLCR = FALSE;
	m_bCheckBCB = FALSE;
	m_bCheckDEPART = FALSE;
	m_bCheckBGRREQ = FALSE;
	m_bCheckBGRACC = FALSE;
	m_bCheckBGRDEC = FALSE;
	m_bCheckAXLOCC = FALSE;
	m_bCheckAXLDST = FALSE;
	m_bCheckAXLACC = FALSE;
	m_bCheckAXLDEC = FALSE;
	m_bCheckAXLREQ = FALSE;
	m_bCheckSAXLACC = FALSE;
	m_bCheckSAXLDEC = FALSE;
	m_bCheckSAXLDST = FALSE;
	m_bCheckSAXLOCC = FALSE;
	m_bCheckSAXLREQ = FALSE;
	m_bCheckSAXLACTIN = FALSE;
	m_bCheckRBGR = FALSE;
	m_strName = _T("");
	m_bIsTrainInBlockSection = FALSE;
	m_bSimInitialized = FALSE;
	m_bCheckSWEEP = FALSE;
	m_bCheckSWEEPACK = FALSE;
	//}}AFX_DATA_INIT
}


void CMySimpleBlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMySimpleBlockDlg)
	DDX_Check(pDX, IDC_CHECK_CA, m_bCheckCA);
	DDX_Check(pDX, IDC_CHECK_LCR, m_bCheckLCR);
	DDX_Check(pDX, IDC_CHECK_BCB, m_bCheckBCB);
	DDX_Check(pDX, IDC_CHECK_DEPART, m_bCheckDEPART);
	DDX_Check(pDX, IDC_CHECK_AUTO, m_bCheckAUTO);
	DDX_Check(pDX, IDC_CHECK_BGR_REQ, m_bCheckBGRREQ);
	DDX_Check(pDX, IDC_CHECK_BGR_ACC, m_bCheckBGRACC);
	DDX_Check(pDX, IDC_CHECK_BGR_DEC, m_bCheckBGRDEC);
	DDX_Check(pDX, IDC_CHECK_RELEASE, m_bCheckRELEASE);
	DDX_Check(pDX, IDC_CHECK_AXLOCC, m_bCheckAXLOCC);
	DDX_Check(pDX, IDC_CHECK_AXLDST, m_bCheckAXLDST);
	DDX_Check(pDX, IDC_CHECK_AXLACC, m_bCheckAXLACC);
	DDX_Check(pDX, IDC_CHECK_AXLDEC, m_bCheckAXLDEC);
	DDX_Check(pDX, IDC_CHECK_AXLREQ, m_bCheckAXLREQ);
	DDX_Check(pDX, IDC_CHECK_SAXLACC, m_bCheckSAXLACC);
	DDX_Check(pDX, IDC_CHECK_SAXLDEC, m_bCheckSAXLDEC);
	DDX_Check(pDX, IDC_CHECK_SAXLDST, m_bCheckSAXLDST);
	DDX_Check(pDX, IDC_CHECK_SAXLOCC, m_bCheckSAXLOCC);
	DDX_Check(pDX, IDC_CHECK_SAXLREQ, m_bCheckSAXLREQ);
	DDX_Check(pDX, IDC_CHECK_SAXLACTIN, m_bCheckSAXLACTIN);
	DDX_Text(pDX, IDC_EDIT_BLOCKNAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_RBGR, m_bCheckRBGR);
	DDX_Text(pDX, IDC_EDIT_BLOCKNAME, m_strName);
	DDX_Check(pDX, IDC_CHECK_SWEEP, m_bCheckSWEEP);
	DDX_Check(pDX, IDC_CHECK_SWEEPACK, m_bCheckSWEEPACK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMySimpleBlockDlg, CDialog)
	//{{AFX_MSG_MAP(CMySimpleBlockDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_AXLDST, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_AXLOCC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_AXLACC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_AXLDEC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_AXLREQ, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLDST, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLOCC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLACTIN, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLACC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLDEC, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_SAXLREQ, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_CA, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_BCB, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_DEPART, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_AUTO, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_RELEASE, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_LCR, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_RBGR, OnCheckAnyButton)
	ON_BN_CLICKED(IDC_CHECK_BGR_REQ, OnCheckResetReq)
	ON_BN_CLICKED(IDC_CHECK_BGR_ACC, OnCheckResetAcc)
	ON_BN_CLICKED(IDC_CHECK_BGR_DEC, OnCheckResetDec)
	ON_BN_CLICKED(IDC_CHECK_SWEEP, OnCheckSweep)
	ON_BN_CLICKED(IDC_CHECK_SWEEPACK, OnCheckSweepack)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySimpleBlockDlg message handlers

BOOL CMySimpleBlockDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SelectTGBorTCB();
	SetTimer(UPDATE_ALL, 500, NULL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMySimpleBlockDlg::ReceiveSignalInformation(ScrInfoBlock *pInfo, BYTE nType)
{
	m_pInfo = pInfo;
    m_nBlockType = nType;
}

void CMySimpleBlockDlg::SelectTGBorTCB()
{
	if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_OUT)
	{
		GetDlgItem( IDC_STATIC_TGB_CONTROL )->SetWindowText( "TCB Control" );
		GetDlgItem( IDC_CHECK_CA )->SetWindowText( "CA ACK" );
		GetDlgItem( IDC_CHECK_LCR )->SetWindowText( "LCG" );
		GetDlgItem( IDC_CHECK_BCB )->SetWindowText( "BRB" );
		GetDlgItem( IDC_CHECK_DEPART )->SetWindowText( "ARRIVE" );
		GetDlgItem( IDC_CHECK_AUTO )->SetWindowText("AUTO LCG");
		GetDlgItem( IDC_CHECK_BGR_REQ)->SetWindowText( "BCR REQ" );
		GetDlgItem( IDC_CHECK_BGR_ACC)->SetWindowText( "BCR ACC" );
		GetDlgItem( IDC_CHECK_BGR_DEC)->SetWindowText( "BCR DEC" );
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_IN)
	{
		GetDlgItem( IDC_CHECK_RBGR )->SetWindowText( "RBCR" );
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_INTER_OUT)
	{
		GetDlgItem( IDC_STATIC_TGB_CONTROL )->SetWindowText( "TCB Control" );
		GetDlgItem( IDC_CHECK_BCB )->SetWindowText( "BRB" );
	}
}

void CMySimpleBlockDlg::OnUpdateState()
{
	if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_OUT)
	{
		m_pInfo->CAACK = m_bCheckCA;
		m_pInfo->LCRIN = m_bCheckLCR;
		m_pInfo->COMPLETEIN = m_pInfo->COMPLETE = !m_bCheckLCR;
		m_pInfo->CBBIN = m_bCheckBCB;
		m_pInfo->ARRIVEIN = m_bCheckDEPART;

		m_pInfo->AXLREQ = m_bCheckAXLREQ;

		if(m_pInfo->AXLREQ)
		{
			if(m_pInfo->AXLACC)
			{
//				m_bCheckAXLREQ = m_pInfo->AXLREQ = FALSE;

				m_pInfo->AXLRST = TRUE;
			}
			else if(m_pInfo->AXLDEC)
			{
				m_bCheckAXLREQ = m_pInfo->AXLREQ = FALSE;
			}
		}

		if(m_pInfo->SAXLACTIN)
		{
			m_pInfo->SAXLREQ = m_bCheckSAXLREQ;
			
			if(m_pInfo->SAXLREQ)
			{
				if(m_pInfo->SAXLACC)
				{
//					m_bCheckSAXLREQ = m_pInfo->SAXLREQ = FALSE;
					
					m_pInfo->SAXLRST = TRUE;
				}
				else if(m_pInfo->SAXLDEC)
				{
					m_bCheckSAXLREQ = m_pInfo->SAXLREQ = FALSE;
				}
			}
		} 
		
		if ( !m_pInfo->RBGPR && m_pInfo->NBGPR)
		{
			m_bCheckRBGR = FALSE;
		}
		else if(m_pInfo->RBGPR && !m_pInfo->NBGPR)
		{
			m_bCheckRBGR = TRUE;
		}

		if(m_pInfo->SWEEPING)
		{
			m_bCheckSWEEP = TRUE;
		}
		else
		{
			m_bCheckSWEEP = FALSE;
		}

		if(m_pInfo->SWEEPINGACK)
		{
			m_bCheckSWEEPACK = TRUE;
		}
		else
		{
			m_bCheckSWEEPACK = FALSE;
		}
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_IN)
	{
		m_pInfo->CA = m_bCheckCA;
		m_pInfo->LCRIN = m_bCheckLCR;
		m_pInfo->COMPLETEIN = m_pInfo->COMPLETE = !m_bCheckLCR;
		m_pInfo->DEPARTIN = m_bCheckDEPART;
		m_pInfo->CBBIN = m_bCheckBCB;
		if (m_bCheckDEPART)
			m_bIsTrainInBlockSection = TRUE;

		if(m_pInfo->AXLREQ)
		{
			m_pInfo->AXLACC = m_bCheckAXLACC;
			m_pInfo->AXLDEC = m_bCheckAXLDEC;
		}
		else
		{
			m_bCheckAXLACC = m_pInfo->AXLACC = FALSE;
			m_bCheckAXLDEC = m_pInfo->AXLDEC = FALSE;
		}

		if(m_pInfo->SAXLACTIN)
		{
			if(m_pInfo->SAXLREQ)
			{
				m_pInfo->SAXLACC = m_bCheckSAXLACC;
				m_pInfo->SAXLDEC = m_bCheckSAXLDEC;
			}
			else
			{
				m_bCheckSAXLACC = m_pInfo->SAXLACC = FALSE;
				m_bCheckSAXLDEC = m_pInfo->SAXLDEC = FALSE;
			}
		}

		if ( !m_pInfo->RBCPR && m_pInfo->NBCPR)
		{
			m_bCheckRBGR = FALSE;
		}
		else if(m_pInfo->RBCPR && !m_pInfo->NBCPR)
		{
			m_bCheckRBGR = TRUE;
		}
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_INTER_OUT )
	{
		m_pInfo->LCRIN = m_bCheckLCR;
		m_pInfo->COMPLETEIN = m_pInfo->COMPLETE = !m_bCheckLCR;
		m_pInfo->CBBIN = m_bCheckBCB;
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_INTER_IN )
	{
		m_pInfo->LCR = m_bCheckLCR;
		m_pInfo->CBBIN = m_bCheckBCB;
		m_pInfo->COMPLETEIN = m_pInfo->COMPLETE = !m_bCheckLCR;
	}

	if ( m_bCheckBCB || m_pInfo->CBBOP )
		m_bIsTrainInBlockSection = FALSE;
	if (!m_bCheckCA && !m_bCheckLCR)
	{
		m_bCheckBCB = FALSE;
	}
	if (m_bCheckAXLDST)
	{
		m_bCheckAXLOCC = TRUE;
		m_pInfo->AXLOCC = FALSE;
	}
	if (m_bCheckSAXLDST)
	{
		m_bCheckSAXLOCC = TRUE;
		m_pInfo->SAXLOCC = FALSE;
	}

// 	if ( !m_pInfo->BLKRSTREQ )
// 	{
// 		m_bCheckBGRACC = FALSE;
// 		m_bCheckBGRDEC = FALSE;
// 		m_pInfo->BLKRSTACCIN = FALSE;
// 		m_pInfo->BLKRSTDECIN = FALSE;
// 	}

// 	if ( m_pInfo->BLKRSTDEC || m_pInfo->BLKRSTACC)
// 	{
// 		m_bCheckBGRREQ = FALSE;
// 		m_pInfo->BLKRSTREQIN = FALSE;
// 	}

	if(m_pInfo->BLKRSTREQIN || m_pInfo->BLKRSTREQ)
	{
		m_bCheckBGRREQ = TRUE;
	}
	else
	{
		m_bCheckBGRREQ = FALSE;
	}

	if(m_pInfo->BLKRSTDECIN || m_pInfo->BLKRSTDEC)
	{
		m_bCheckBGRDEC = TRUE;
	}
	else
	{
		m_bCheckBGRDEC = FALSE;
	}

	if(m_pInfo->BLKRSTACCIN || m_pInfo->BLKRSTACC)
	{
		m_bCheckBGRACC = TRUE;
	}
	else
	{
		m_bCheckBGRACC = FALSE;
	}

	if (m_bSimInitialized)	
	{
		m_bCheckCA = FALSE;
		m_bCheckLCR = FALSE;
		m_bCheckBCB = FALSE;
		m_bCheckDEPART = FALSE;
		m_bCheckAUTO = FALSE;
		m_bCheckBGRREQ = FALSE;
		m_bCheckAXLOCC = FALSE;
		m_bCheckAXLDST = FALSE;
		m_bCheckBGRREQ = FALSE;
		m_bCheckBGRACC = FALSE;
		m_bCheckBGRDEC = FALSE;
		m_bCheckSWEEP = FALSE;
		m_bCheckSWEEPACK = FALSE;

		m_bSimInitialized = FALSE;
	}
}

void CMySimpleBlockDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch (nIDEvent)
	{
	case UPDATE_ALL:
		OnUpdateState();
		MakeDiable();
		UpdateData(FALSE);
		break;
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CMySimpleBlockDlg::OnCheckAnyButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->AXLOCC = !m_bCheckAXLOCC;
	m_pInfo->AXLDST = !m_bCheckAXLDST;

	m_pInfo->SAXLOCC = !m_bCheckSAXLOCC;
	m_pInfo->SAXLDST = !m_bCheckSAXLDST;

	m_pInfo->SAXLACTIN = m_bCheckSAXLACTIN;

 	m_pInfo->BLKRSTREQIN = m_bCheckBGRREQ;
 	m_pInfo->BLKRSTACCIN = m_bCheckBGRACC;
 	m_pInfo->BLKRSTDECIN = m_bCheckBGRDEC;

	
	if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_IN)
	{
		if(m_bCheckRBGR)
		{
			m_pInfo->RBCR = 1;
			m_pInfo->NBCR = 0;
		}
		else
		{
			m_pInfo->RBCR = 0;
			m_pInfo->NBCR = 1;
		}
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_OUT)
	{
		if(m_bCheckRBGR)
		{
			m_pInfo->RBGR = 1;
			m_pInfo->NBGR = 0;
		}
		else
		{
			m_pInfo->RBGR = 0;
			m_pInfo->NBGR = 1;
		}
	}

	if(m_bCheckAUTO)
	{
		if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_IN)
		{
			m_pInfo->LCRIN = TRUE;
			m_pInfo->LCROP = TRUE;
			m_pInfo->LCR = TRUE;
			m_pInfo->COMPLETE = FALSE;
			m_pInfo->COMPLETEIN = FALSE;
			m_pInfo->DEPARTIN = TRUE;
			if(m_strName != "B11" && m_strName != "B12")
			{
				m_pInfo->AXLOCC = FALSE;
			}
			m_bCheckLCR = TRUE;
			m_bCheckDEPART = TRUE;
			m_bCheckAUTO = FALSE;
			m_bIsTrainInBlockSection = TRUE;
		}
		else if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_OUT)
		{
			m_pInfo->LCRIN = TRUE;
			m_pInfo->LCROP = TRUE;
			m_pInfo->LCR = TRUE;
			m_pInfo->COMPLETE = FALSE;
			m_pInfo->COMPLETEIN = FALSE;
			m_bCheckLCR = TRUE;
			m_bCheckAUTO = FALSE;
		}
	}
	if(m_bCheckRELEASE)
	{
		m_pInfo->LCR = FALSE;
		m_pInfo->LCROP = FALSE;
		m_pInfo->DEPART = FALSE;
		m_pInfo->DEPARTIN = FALSE;
		m_pInfo->ARRIVE = FALSE;
		m_pInfo->ARRIVEIN = FALSE;
		m_pInfo->AXLOCC = TRUE;
		m_pInfo->AXLDST = TRUE;
		m_pInfo->COMPLETE = TRUE;
		m_pInfo->COMPLETEIN = TRUE;

		m_pInfo->AXLREQ = FALSE;
		m_pInfo->AXLACC = FALSE;
		m_pInfo->AXLDEC = FALSE;

		m_pInfo->SAXLACTIN = FALSE;
		m_pInfo->SAXLREQ = FALSE;
		m_pInfo->SAXLACC = FALSE;
		m_pInfo->SAXLDEC = FALSE;

		m_pInfo->SAXLOCC = FALSE;
		m_pInfo->SAXLDST = FALSE;

		m_bCheckCA = FALSE;
		m_bCheckLCR = FALSE;
		m_bCheckDEPART = FALSE;
		m_bCheckAUTO = FALSE;
		m_bCheckAXLOCC = FALSE;
		m_bCheckAXLDST = FALSE;
		m_bCheckRELEASE = FALSE;
		m_bIsTrainInBlockSection = FALSE;
	}
}

void CMySimpleBlockDlg::MakeDiable() 
{
	if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_OUT)
	{
		if ( !m_pInfo->CA || m_pInfo->CAACK )
			GetDlgItem( IDC_CHECK_CA )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_CA )->EnableWindow(TRUE);

		if ( !m_pInfo->LCR || m_pInfo->LCRIN )
			GetDlgItem( IDC_CHECK_LCR )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_LCR )->EnableWindow(TRUE);

		if ( m_pInfo->LCRIN && !m_pInfo->ARRIVEIN)
			GetDlgItem( IDC_CHECK_BCB )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_BCB )->EnableWindow(TRUE);

		if ( !m_pInfo->DEPART )
			GetDlgItem( IDC_CHECK_DEPART )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_DEPART )->EnableWindow(TRUE);

		if (m_pInfo->LCR)
			GetDlgItem( IDC_CHECK_AUTO )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_AUTO )->EnableWindow(TRUE);

		GetDlgItem( IDC_CHECK_AXLACC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLDEC )->EnableWindow(FALSE);
		
		if( !m_pInfo->AXLOCC )
		{
			GetDlgItem( IDC_CHECK_AXLREQ )->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_AXLREQ )->EnableWindow(FALSE);
		}
		
		GetDlgItem( IDC_CHECK_SAXLACC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLDEC )->EnableWindow(FALSE);

		if( m_pInfo->SAXLACTIN & !m_pInfo->SAXLOCC )
		{
			GetDlgItem( IDC_CHECK_SAXLREQ )->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_SAXLREQ )->EnableWindow(FALSE);
		}
	
		if ( m_pInfo->BLKRSTREQ )
		{
			GetDlgItem( IDC_CHECK_BGR_REQ )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_BGR_ACC )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_BGR_DEC )->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_BGR_REQ )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_BGR_ACC )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_BGR_DEC )->EnableWindow(FALSE);
		}

		GetDlgItem( IDC_CHECK_SWEEP )->EnableWindow(FALSE);
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_AUTO_IN)
	{
		if ( m_pInfo->CA )
			GetDlgItem( IDC_CHECK_CA )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_CA )->EnableWindow(TRUE);

		if ( m_pInfo->LCRIN || !m_pInfo->AXLOCC)
			GetDlgItem( IDC_CHECK_LCR )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_LCR )->EnableWindow(TRUE);

		if ( (m_pInfo->LCRIN && !m_pInfo->LCR) || m_pInfo->DEPART || !m_pInfo->AXLOCC)
			GetDlgItem( IDC_CHECK_BCB )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_BCB )->EnableWindow(TRUE);

		if ( m_pInfo->DEPARTIN || !m_pInfo->LCR)
			GetDlgItem( IDC_CHECK_DEPART )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_DEPART )->EnableWindow(TRUE);

		if ( m_bCheckLCR )
			GetDlgItem( IDC_CHECK_AUTO )->EnableWindow(FALSE);
		else
			GetDlgItem( IDC_CHECK_AUTO )->EnableWindow(TRUE);
		
		if( !m_pInfo->AXLOCC && m_pInfo->AXLREQ )
		{
			GetDlgItem( IDC_CHECK_AXLACC )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_AXLDEC )->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_AXLACC )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_AXLDEC )->EnableWindow(FALSE);
		}
		
		GetDlgItem( IDC_CHECK_AXLREQ )->EnableWindow(FALSE);
		
		if( !m_pInfo->SAXLOCC && m_pInfo->SAXLREQ )
		{
			GetDlgItem( IDC_CHECK_SAXLACC )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_SAXLDEC )->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem( IDC_CHECK_SAXLACC )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_SAXLDEC )->EnableWindow(FALSE);
		}
		
		GetDlgItem( IDC_CHECK_SAXLREQ )->EnableWindow(FALSE);

		if ( m_pInfo->BLKRSTREQ )
		{
			GetDlgItem( IDC_CHECK_BGR_REQ )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_BGR_ACC )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_BGR_DEC )->EnableWindow(TRUE);
		}

		if ( !m_pInfo->BLKRSTREQ && !m_pInfo->BLKRSTREQIN )
		{
			GetDlgItem( IDC_CHECK_BGR_REQ )->EnableWindow(TRUE);
			GetDlgItem( IDC_CHECK_BGR_ACC )->EnableWindow(FALSE);
			GetDlgItem( IDC_CHECK_BGR_DEC )->EnableWindow(FALSE);
		}
		
		GetDlgItem( IDC_CHECK_SWEEPACK )->EnableWindow(FALSE);
	}
	else if ((m_nBlockType & 0xF0) == BLOCKTYPE_INTER_OUT || (m_nBlockType & 0xF0) == BLOCKTYPE_INTER_IN)
	{
		GetDlgItem( IDC_CHECK_CA )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_DEPART )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AUTO )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLOCC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLDST )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLACC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLDEC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_AXLREQ )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLOCC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLDST )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLACTIN )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLACC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLDEC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_SAXLREQ )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_BGR_REQ )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_BGR_ACC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_BGR_DEC )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHECK_RBGR )->EnableWindow(FALSE);
	}	
}

void CMySimpleSignalDlg::OnCheckAdjCond() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->ADJCOND = m_bAdjCond;
}

void CMySimpleBlockDlg::OnCheckResetReq() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->BLKRSTREQIN = m_bCheckBGRREQ;
}

void CMySimpleBlockDlg::OnCheckResetAcc() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->BLKRSTACCIN = m_bCheckBGRACC;
}

void CMySimpleBlockDlg::OnCheckResetDec() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->BLKRSTDECIN = m_bCheckBGRDEC;
}

void CMySimpleBlockDlg::OnCheckSweep() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->SWEEPING = m_bCheckSWEEP;
}

void CMySimpleBlockDlg::OnCheckSweepack() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_pInfo->SWEEPINGACK = m_bCheckSWEEPACK;
}

void CMySimpleSignalDlg::OnCheckFailEkr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailCekr() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailGFilament() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailYFilament() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailRFilament() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CMySimpleSignalDlg::OnCheckFailYyFilament() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}
