// ScrView.cpp : implementation file
//

#include "stdafx.h"
#include "LIS.h"

#include "LISDoc.h"
#include "ScrView.h"

#include "EITInfo.h"
#include "MemView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScrView

IMPLEMENT_DYNCREATE(CScrView, CFormView)

CScrView::CScrView()
	: CFormView(CScrView::IDD)
{
	//{{AFX_DATA_INIT(CScrView)
	//}}AFX_DATA_INIT
}

CScrView::~CScrView()
{
}

void CScrView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScrView)
//	DDX_Control(pDX, IDC_LSMCTRL, m_Screen);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CScrView, CFormView)
	//{{AFX_MSG_MAP(CScrView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScrView diagnostics

#ifdef _DEBUG
void CScrView::AssertValid() const
{
	CFormView::AssertValid();
}

void CScrView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CScrView message handlers
extern char _StationName[];
extern char _MetrixMode[];

CScrView *_pScrView = NULL;
void CScrView::OnInitialUpdate() 
{
////	SetScrollSizes( MM_TEXT, CSize(2048,1436) );
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	DWORD dwStyle = WS_CHILD;
    CRect rect;
	GetClientRect( &rect );

 	m_Screen.Create( _StationName, dwStyle, rect, this, IDC_LSMCTRL ); 

	if ( !IsWindow(m_Screen.m_hWnd) ) {
		MessageBox( "SCREEN CREATE FAILED", MB_OK );
	}
	else {
		_pScrView = this;
	}
}

extern CEITInfo *_pEITInfo;

BOOL CScrView::CreateScreenInfo()
{
	BOOL bRet = FALSE;

	if (!_pEITInfo) return bRet;
	if (!IsWindow(m_Screen)) return bRet;

	m_Screen.ShowWindow( SW_SHOW );
	long wParam = (long)&_StationName[0];
	long lParam = (long)&_MetrixMode[0];
	char *errstr = (char*)m_Screen.OlePostMessage( WS_OPMSG_LOADFILE, wParam, lParam );
	if ( errstr ) {
		AfxMessageBox( errstr );
	}
	else {
		m_Screen.OlePostMessage( WS_OPMSG_SETLSMOPTION, 0x10, 0);	// Set ViewInfo bit
		m_Screen.MoveWindow( 0, 0, 2560, 1600 );
		m_Screen.ShowWindow( SW_SHOW );

		bRet = TRUE;
	}

	return bRet;
}

BEGIN_EVENTSINK_MAP(CScrView, CFormView)
    //{{AFX_EVENTSINK_MAP(CScrView)
	ON_EVENT(CScrView, IDC_LSMCTRL, 2 /* OleSendMessage */, OnOleSendMessageLSMctrl1, VTS_I4 VTS_I4 VTS_I4)
	ON_EVENT(CScrView, IDC_LSMCTRL, 1 /* Operate */, OnOperateLSMctrl1, VTS_I2 VTS_I2)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

extern CMemView * _pCMemView;

// LSM의 제어를 Simulation을 위해 MemView class의 Operate을 호출한다....
// Simulation 처리를 하지 않을 경우에는 수행하지 않는다...
// LSM의 FireOperate에 대응한다....
void CScrView::OnOperateLSMctrl1(short nFunction, short nArgument) 
{
	// TODO: Add your control notification handler code here
	if (!_pCMemView) return;
	_pCMemView->Operate( (long)nFunction, (long)nArgument, 0L );
}

// 사용하지 않는다....
void CScrView::OnOleSendMessageLSMctrl1(long message, long wParam, long lParam) 
{
	// TODO: Add your control notification handler code here
	if (!_pCMemView) return;
    _pCMemView->Operate( message, wParam, lParam );

}

char *binfile = "SIM.BIN";

void CScrView::OnDraw(CDC* pDC) 
{
	// TODO: Add your specialized code here and/or call the base class
		
}

void CScrView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnPrepareDC(pDC, pInfo);
}

void CScrView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
//	CSize size( 1280, 1024 );
	CSize size( 2560, 1600 );
	SetScrollSizes( MM_TEXT, size /*GetDocument( )->GetDocSize( )*/ );
	ResizeParentToFit( );   // Default bShrinkOnly argument
}
