// StationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "StationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStationDlg dialog


CStationDlg::CStationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStationDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CStationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStationDlg)
	DDX_Control(pDX, IDC_LIST_STATION, m_ctrlStationList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStationDlg, CDialog)
	//{{AFX_MSG_MAP(CStationDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_STATION, OnDblclkListStation)
	ON_WM_CREATE()
	ON_BN_DOUBLECLICKED(IDC_BUTTON_SEL_MAKE, OnDoubleclickedButtonSelMake)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStationDlg message handlers

BOOL CStationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	m_sImg.Create(IDB_BITMAP_DB, 16, 1, RGB(255,255,255));
	m_ctrlStationList.SetImageList(&m_sImg, LVSIL_SMALL);

	char szPath[256];
	::GetCurrentDirectory(256, szPath);

	CWinApp* pApp = AfxGetApp();
	m_strSelectedStation = pApp->GetProfileString("Config", "LastSelected");

	CString strPath;
	strPath = szPath;

	CFileFind	fileFind;
	BOOL		bIsFind;
	CString		strFileName;

	bIsFind = fileFind.FindFile(strPath + "\\*.*");

	int	row = 0;
	while(bIsFind)
	{
		bIsFind = fileFind.FindNextFile();
		strFileName = fileFind.GetFileName();

		if(fileFind.IsDirectory())
		{
			if(strFileName != "." && strFileName != "..")
			{
				if(strFileName == m_strSelectedStation)
				{
					m_ctrlStationList.InsertItem(LVIF_TEXT|LVIF_STATE, row, strFileName, LVIS_SELECTED, LVIS_SELECTED, 0, 0);

					
				}
				else
				{
					m_ctrlStationList.InsertItem(row, strFileName, 0);
				}
			}
		}

		row++;
	}

	fileFind.Close();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CStationDlg::OnOK()
{
	POSITION pos = m_ctrlStationList.GetFirstSelectedItemPosition();
	int iItem = m_ctrlStationList.GetNextSelectedItem(pos);
	
	m_strSelectedStation = m_ctrlStationList.GetItemText(iItem, 0);
	
	CWinApp* pApp = AfxGetApp();
	pApp->WriteProfileString("Config", "LastSelected", m_strSelectedStation);

	CDialog::OnOK();
}
 

void CStationDlg::OnDblclkListStation(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;

	OnOK();
}

int CStationDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	SetParent(GetDesktopWindow());
	lpCreateStruct->dwExStyle |= WS_EX_APPWINDOW;

	return 0;
}

void CStationDlg::OnDoubleclickedButtonSelMake() 
{
	// TODO: Add your control notification handler code here
	((CLISApp*)AfxGetApp())->m_bMakeMode = TRUE;

	POSITION pos = m_ctrlStationList.GetFirstSelectedItemPosition();
	while(pos != NULL)
	{
		int iItem = m_ctrlStationList.GetNextSelectedItem(pos);
		CString strSelStation = m_ctrlStationList.GetItemText(iItem, 0);

		m_listSelectedStation.AddTail(strSelStation);
	}

	CDialog::OnOK();
}
