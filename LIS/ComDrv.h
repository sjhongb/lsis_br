#ifndef _ERCCOM_H
#define _ERCCOM_H
// ErcCom.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCommDriver thread

class CCommDriver : public CWinThread
{
	DECLARE_DYNCREATE(CCommDriver)
protected:
public:
	BOOL m_bExit;
	CString NameOf();
	int WriteBlock( char *buffer, int size );
	void RxProc(unsigned int &size);
	CCommDriver();           // protected constructor used by dynamic creation

// Attributes
public:

    int         m_nMode;
    BOOL        m_bCheckCTS;

	HANDLE		m_idComDev ;
	BYTE		m_bPort;
	BOOL		m_bOpenComm, m_fXonXoff, m_fLocalEcho, m_fAutoWrap;
	BYTE		m_bByteSize, m_bFlowCtrl, m_bParity, m_bStopBits;
	DWORD		m_dwBaudRate;
	HANDLE		m_hPostEvent, m_hWatchThread, m_hWatchEvent;
	DWORD		m_dwThreadID;
//	BOOL	    m_bIsConnect;
    BOOL        m_bExitDone;

	OVERLAPPED  m_osWrite, m_osRead ;


	BYTE m_pRxBuffer[128];
	BYTE m_pTxBuffer[128];

	BYTE m_pIO_OutBuffer[256];
	BYTE m_pIO_InBuffer[256];
	BOOL m_bRxReady;

	int m_nrxptr, m_nrxcap, m_nrxlen;
	unsigned short m_nrxcrc, m_nchkcrc;

	BYTE m_nIsDLE;
	BYTE m_nrxcnt;

	int m_nTxBuffSize;

    char m_szPort[5];
	unsigned char* m_pReadBuff;
	unsigned char* m_pTempBuff;

	CWnd *m_pClient;
// Operations
public:

    int WaitSerial();
	BOOL  SetupDCB();
	BOOL  CloseModem();							// Close CommPort
	BOOL  OpenModem();							// Open CommPort, 모뎀 초기화 ("AT-SSE=1")
	DWORD ReadCommData(unsigned char*, DWORD);	// 데이타 수신
	void SetClient( CWnd *pWnd ) { m_pClient = pWnd; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommDriver)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

// Implementation
protected:
public:
	virtual ~CCommDriver();

	// Generated message map functions
	//{{AFX_MSG(CCommDriver)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

protected:
	// Generated message map functions

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//--------------------------------------------------------
//--------------------------------------------------------
#define		FC_DTRDSR				0x01
#define		FC_RTSCTS				0x02
#define		FC_XONXOFF				0x04
#define		ASCII_XON				0x11
#define		ASCII_XOFF				0x13

/*
class AdaComm {
   int TxBuffSize;
   int BlockLen;
public:
   virtual ~AdaComm();
	DWORD WINAPI CommWatchProc(LPVOID) ;

private :

public:
	BOOL WriteCommData(unsigned char*, DWORD);	// 데이타 송신
};
*/
extern unsigned char RxImage[];
#define EM_DATARECEIVED WM_USER+100

#endif
