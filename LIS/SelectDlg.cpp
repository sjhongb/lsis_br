// SelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LIS.h"
#include "SelectDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDlg dialog


CSelectDlg::CSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectDlg)
	m_EndMin = _T("");
	m_EndDate = _T("");
	m_EndHour = _T("");
	m_StartHour = _T("");
	m_StartMin = _T("");
	m_StartDate = _T("");
	m_bFault = TRUE;
	m_bHistory = TRUE;
	m_bOthers = TRUE;
	m_bSignal = TRUE;
	m_bSwitch = TRUE;
	m_bTrack = TRUE;
	//}}AFX_DATA_INIT
}

void CSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectDlg)
	DDX_Text(pDX, IDC_ENDMIN, m_EndMin);
	DDX_Text(pDX, IDC_ENDYEAR, m_EndDate);
	DDX_Text(pDX, IDC_ENDHOUR, m_EndHour);
	DDX_Text(pDX, IDC_STARTHOUR, m_StartHour);
	DDX_Text(pDX, IDC_STARTMIN, m_StartMin);
	DDX_Text(pDX, IDC_STARTYEAR, m_StartDate);
	DDX_Check(pDX, IDC_LOGFAULT, m_bFault);
	DDX_Check(pDX, IDC_LOGHISTORY, m_bHistory);
	DDX_Check(pDX, IDC_OTHERS, m_bOthers);
	DDX_Check(pDX, IDC_SIGNAL, m_bSignal);
	DDX_Check(pDX, IDC_SWITCH, m_bSwitch);
	DDX_Check(pDX, IDC_TRACK, m_bTrack);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectDlg)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SSYEAR, OnDeltaposSsyear)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SEYEAR, OnDeltaposSeyear)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SSHOUR, OnDeltaposSshour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SEHOUR, OnDeltaposSehour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SSMIN, OnDeltaposSsmin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SEMIN, OnDeltaposSemin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDlg message handlers

BOOL CSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_End = CTime::GetCurrentTime();
	CTimeSpan span = CTimeSpan( 0,0,30,0 );
	m_Start = m_End - span;

	UpdateString();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectDlg::UpdateString()
{
	m_StartDate = m_Start.Format("%Y-%m-%d");
	m_EndDate = m_End.Format("%Y-%m-%d");
	m_StartHour = m_Start.Format("%H");
	m_EndHour = m_End.Format("%H");
	m_StartMin = m_Start.Format("%M");
	m_EndMin = m_End.Format("%M");

	UpdateData( FALSE );
}

void CSelectDlg::OnDeltaposSsyear(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( pNMUpDown->iDelta, 0, 0, 0 );
	m_Start += span;
	if (m_Start > m_End) m_Start = m_End;
	UpdateString();
	*pResult = 0;
}

void CSelectDlg::OnDeltaposSeyear(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( pNMUpDown->iDelta, 0, 0, 0 );
	m_End += span;
	if (m_Start > m_End) m_End = m_Start;
	UpdateString();
	*pResult = 0;
}

void CSelectDlg::OnDeltaposSshour(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( 0, pNMUpDown->iDelta, 0, 0 );
	m_Start += span;
	if (m_Start > m_End) m_Start = m_End;
	UpdateString();
	*pResult = 0;
}

void CSelectDlg::OnDeltaposSehour(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( 0, pNMUpDown->iDelta, 0, 0 );
	m_End += span;
	if (m_Start > m_End) m_End = m_Start;
	UpdateString();
	*pResult = 0;
}

void CSelectDlg::OnDeltaposSsmin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( 0, 0, pNMUpDown->iDelta, 0 );
	m_Start += span;
	if (m_Start > m_End) m_Start = m_End;
	UpdateString();
	*pResult = 0;
}

void CSelectDlg::OnDeltaposSemin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CTimeSpan span = CTimeSpan( 0, 0, pNMUpDown->iDelta, 0 );
	m_End += span;
	if (m_Start > m_End) m_End = m_Start;
	UpdateString();
	*pResult = 0;
}
