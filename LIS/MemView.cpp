// MemView.cpp : implementation file

#include "stdafx.h"

//#include "WEM.h"
#include "MemView.h"
#include "EITInfo.h"
#include "ScrView.h"
#include "LISInfoView.h"
#include "comdrv.h"
#include "SharedMemory.h"
#include "MainFrm.h"
#include "LIS.h"
#include "..\Include\Scrobj.h"

extern CCommDriver *_pSerial;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMemView dialog

#include "MyPoint.h"
#include "MySignal.h"
#include "MyTrack.h"
#include "MyGeneral.h"
#include "SysBtnDlg.h"


BYTE m_pVMEM[2048];   //VMEM의 선언...

int _nRANDOMGENERATE;
extern BOOL _bSIMModeSW;
extern BOOL _bSIMModeIO;

class CVirtualIO {
public:
	BYTE m_pVIO[256];	// Virtual In Out
						// 4BYTE(at CARD) * 8(at RACK) * 8(at SYSTEM)
						// total 2048 bits, 11 bit wide range
	void SetBit( short loc,  short data );
	short GetBit( short loc );
};

void CVirtualIO::SetBit( short loc, short data ) 
{
	loc &= 0x7ff;
	short addr = loc >> 3;
	BYTE bitloc = 1 << (loc & 7);
	if (data) m_pVIO[ addr ] |= bitloc;
	else m_pVIO[ addr ] &= ~bitloc;
}

short CVirtualIO::GetBit( short loc ) 
{
	loc &= 0x7ff;
	short addr = loc >> 3;
	BYTE bitloc = 1 << (loc & 7);
	if (m_pVIO[addr] & bitloc) return 1;
	return 0;
}

//CCom32 *_pCom;
extern int _PortNo;

CMemView::CMemView(CWnd* pParent /*=NULL*/)
	: CDialog(CMemView::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMemView)
	m_CTC = _T("");
	m_bBlock = FALSE;
	m_bRandom = FALSE;
	m_strStatus = _T("");
    m_nMode = SIMMODE_SIM;      // 0
	m_nComPort = -1;
	m_strPortAddress = _T("");
	m_bPortValue = FALSE;
	m_bPrevCTCReq = FALSE;
	//}}AFX_DATA_INIT
    m_nComPort = _PortNo;
	m_nTimer = 0;
	m_bOnSim = FALSE;
	RemoveMySim();

	m_szMsgQue[1] = 0;

	m_nTrackingDelay = 40;		// 4sec

	m_pSocket = NULL;
	m_connectionSocket = NULL;

	m_hEventRun = CreateEvent(NULL, TRUE, FALSE, "LES_LIS_SHM_SYNC");
	ASSERT(m_hEventRun);
}

CMemView::~CMemView()
{
//2001.04.18 jin
	if ( m_pSocket != NULL ) {
		delete m_pSocket;
		m_pSocket = NULL;
	}
//
	RemoveMySim();
}

extern BYTE _nSwitchDelay;
void CMemView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMemView)
//	DDX_Control(pDX, IDC_COMBO_SIGNAL, m_comboSignal);
//	DDX_Control(pDX, IDC_COMBO_TRACK, m_comboTrack);
//	DDX_Control(pDX, IDC_COMBO_SWITCH1, m_comboSwitch);
//	DDX_Control(pDX, IDC_COMBO_ROUTE, m_comboRoute);
	DDX_Control(pDX, IDC_LIST_SCR, m_ScrIO);
	DDX_Control(pDX, IDC_LIST_TRACK, m_TRACK);
	DDX_Control(pDX, IDC_LIST_SIGNAL, m_SIGNAL);
	DDX_Control(pDX, IDC_LIST_POINT, m_POINT);
	DDX_Control(pDX, IDC_LIST_BLOCK, m_BLOCK);
//	DDX_Control(pDX, IDC_LIST_LINK, m_LINK);
//	DDX_Control(pDX, IDC_LIST_OUT, m_OUT);
//	DDX_Control(pDX, IDC_LIST_IN, m_IN);
//	DDX_Text(pDX, IDC_EDIT2, m_CTC);
//	DDX_Check(pDX, IDC_CHECK_BLOCK, m_bBlock);
//	DDX_Text(pDX, IDC_EDIT_TRACKDELAY, m_nTrackingDelay);
//	DDX_Check(pDX, IDC_CHECK_RND, m_bRandom);
//	DDX_Text(pDX, IDC_EDIT_STATUS, m_strStatus);
//	DDX_Radio(pDX, IDC_RADIO_MODE, m_nMode);
//	DDX_CBIndex(pDX, IDC_COMBO_COM, m_nComPort);
//	DDX_Text(pDX, IDC_EDIT_SWDELAY, _nSwitchDelay);
//	DDX_Text(pDX, IDC_EDIT_PORTADDR, m_strPortAddress);
//	DDX_Check(pDX, IDC_CHECK_PORTVALUE, m_bPortValue);
	//}}AFX_DATA_MAP
}


//#define EM_DATARECEIVED 1111

BEGIN_MESSAGE_MAP(CMemView, CDialog)
	//{{AFX_MSG_MAP(CMemView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_OPER, OnButtonOper)
	ON_BN_CLICKED(ID_VIEW_SYSBTN, OnButtonSys)
	ON_BN_CLICKED(IDCLOSE, OnClose)
	ON_BN_CLICKED(ID_VIEW_SYSBTN2, OnViewSysbtn2)
	ON_BN_CLICKED(ID_VIEW_IOLIST, OnViewIolist)
	ON_BN_CLICKED(IDC_BUTTON_CTC, OnButtonCtc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemView message handlers

BOOL CMemView::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

//	m_VirtualMem.LoadList();
	CString strRet;
	char buf[100];
	GetPrivateProfileString("SYSTEM", "STATION_FILENAME", "", buf , 100 ,"./Station.ini");	
	strRet = buf;

	/*
	if ( strRet == "YES" ) {
		m_nShared = TRUE;
		CString m_strmem = _T("SharedLES"); 
		m_SMles.Init(m_strmem,2048); 
		m_strmem = _T("SharedCommand");
		m_SMlesCommand.Init(m_strmem,20); 
		m_nTimer = SetTimer( 1, 1000, NULL );
	} else {
		m_nShared = FALSE;
	}
	*/
	m_nTimer = SetTimer( 1, 50, NULL );

//	CWEMApp *pApp = (CWEMApp*)AfxGetApp();
//	pApp->SetClient( this );
//	_pCom = &pApp->m_Com;
	OnButtonOper();

	m_pSocket = new CListeningSocket(this);
	if ( m_pSocket->Create( 700 ) )
	{
		if (m_pSocket->Listen())
			return TRUE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// W(+N)	1
// KR-N		7	WR-N
// KR-R		8	WR-R
// WR-N		5	W(+N)
// WR-R		6	W(+N)

extern char _StationName[];
extern char _WorkPath[];

extern CEITInfo *_pEITInfo;
CMemView * _pCMemView = NULL;



ScrInfoBlock *pB1;
ScrInfoBlock *pB2;
ScrInfoBlock *pB11;
ScrInfoBlock *pB12;
ScrInfoBlock *pB3;
ScrInfoBlock *pB4;

void CMemView::LoadRelayInfo()
{
//	m_IN.DeleteAllItems();
//	m_OUT.DeleteAllItems();
	m_CtcDlg.CreateDlg( m_CtcCmd );

	LV_COLUMN lvc;
	LV_COLUMN lvc_Track;
	LV_COLUMN lvc_Signal;
	LV_COLUMN lvc_Point;
	LV_COLUMN lvc_Block;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	lvc.iSubItem = 0;
	lvc.pszText = "POINT NAME";
//  	lvc.cx = 142;
	lvc.fmt = LVCFMT_LEFT;
	lvc_Block = lvc_Point = lvc_Signal = lvc_Track = lvc;
	lvc_Signal.pszText = "SIGNAL NAME";
	lvc_Track.pszText = "TRACK NAME";
	lvc_Block.pszText = "BLOCK NAME";

//	m_IN.InsertColumn(0,&lvc);
//	m_OUT.InsertColumn(0,&lvc);
	m_ScrIO.InsertColumn(0,&lvc);
//	m_LINK.InsertColumn(0,&lvc);
	m_TRACK.InsertColumn(0,&lvc_Track);
	m_SIGNAL.InsertColumn(0,&lvc_Signal);
	m_POINT.InsertColumn(0,&lvc_Point);
	m_BLOCK.InsertColumn(0,&lvc_Block);

// 	lvc.iSubItem = 1;
// 	lvc.pszText = "ADDR";
// 	lvc.cx = 45;
// 	lvc.fmt = LVCFMT_CENTER;
// 	m_IN.InsertColumn(1,&lvc);
// 	m_OUT.InsertColumn(1,&lvc);
// 	m_ScrIO.InsertColumn(1,&lvc);
// 	m_LINK.InsertColumn(1,&lvc);
// 	m_TRACK.InsertColumn(1,&lvc);
// 
// 	lvc.iSubItem = 2;
// 	lvc.pszText = "TF";
// 	lvc.cx = 20;
// 	lvc.fmt = LVCFMT_CENTER;
// 	m_IN.InsertColumn(2,&lvc);
// 	m_OUT.InsertColumn(2,&lvc);
// 	m_ScrIO.InsertColumn(2,&lvc);
// 	m_LINK.InsertColumn(2,&lvc);
// 	m_TRACK.InsertColumn(2,&lvc);

// insert items

	LV_ITEM lvi;
	LV_ITEM lvi_Track;
	LV_ITEM lvi_Signal;
	LV_ITEM lvi_Point;
 	LV_ITEM lvi_Block;
	int i, n_in = 0, n_out = 0, n_link = 0, n_track = 0;

	if (_pEITInfo)  //EITInfo class의 pointer....
	{
		CMyTrack *track;
		CMySignal *signal;
		CMyPoint *point;
//		CMySignal *block;
		CMyGeneral *pScrLamp;

// 전체의 ScrInfo정보를 가지고 온다... ---> Daynamic data에 대한 정보...
		CScrInfo *pScrInfo = _pEITInfo->m_pScrInfo;

		short nTrack = 0, nSignal = 0, nSwitch = 0, nLamp = 0, nBlock = 0;
		BYTE nDir, nSigType;

// VMEM 초기화...  --> 여기서 초기화를 해도 각각의 object 정보 처리 시 update된다.
		memset( m_pVMEM, 0, 2048 );   
		int nListLoc = 0;
		int nListForPoint = 0;
		int nListLocForTC = 0;
		int nListLocForSig = 0;
		int nListLocForBlk = 0;

// 각 Dynamic data의 정보를 가지고 해당 
		CObject *pObj;
		CObject *pTrackObj;
		CObject *pSignalObj;
		CObject *pPointObj;
		CObject *pBlockObj;
		pSignalObj = NULL;
		pPointObj = NULL;
		pTrackObj = NULL;
		pBlockObj = NULL;
		for (i=0; i<_pEITInfo->m_nScrInfoCount; i++) 
		{
			CScrInfo *pInfo = pScrInfo + i;
			ScrInfoTrack *pTrack;
			ScrInfoSwitch *pPoint;

			lvi.mask = LVIF_TEXT | LVIF_STATE;
			
			lvi.iItem = i;
			lvi.iSubItem = 0;
			
			lvi.pszText = (char*)(LPCTSTR)pInfo->m_strName;
			lvi.stateMask = LVIS_STATEIMAGEMASK;
			lvi.state = INDEXTOSTATEIMAGEMASK(1);
			switch ( pInfo->m_nType ) 
			{
			case SCRINFO_TRACK :
				track = new CMyTrack(_pEITInfo);
				track->m_nID = nTrack + 1;
				pObj = (CObject*)track;
				nTrack++;
				if (pInfo->m_strName.Find('?')==0)
					break;
				pTrackObj = (CObject *)track;
				pTrack = (ScrInfoTrack *)pInfo->m_pEipData;
				pTrack->TRACK = 1;		// 궤도 여자 상태
				pTrack->ROUTE = 1;		// 궤도 해정 상태
				pTrack->DISTURB = 1;

				lvi_Track.mask = LVIF_TEXT | LVIF_STATE;
				
				lvi_Track.iItem = i;
				lvi_Track.iSubItem = 0;
				
				lvi_Track.pszText = (char*)(LPCTSTR)pInfo->m_strName;
				lvi_Track.stateMask = LVIS_STATEIMAGEMASK;
				lvi_Track.state = INDEXTOSTATEIMAGEMASK(1);
			
				break;

			case SCRINFO_SIGNAL :
				signal = new CMySignal(_pEITInfo);
				pObj = (CObject*)signal;
				nSignal++;
				if (pInfo->m_strName.Find('-')>0)
					break;
                signal->m_nSignalType = _pEITInfo->m_pSignalInfoTable[nSignal].nSignalType;
				signal->m_nSignalLamp = _pEITInfo->m_pSignalInfoTable[nSignal].m_nLamp;
				signal->m_nSignalLampExt = _pEITInfo->m_pSignalInfoTable[nSignal].m_nLampExt;
				
				nDir = (signal->m_nSignalType & SIGNALTYPE_DIR_BIT) >> 2;
				nSigType = signal->m_nSignalType & 0xF0;

				if(nSigType == BLOCKTYPE_AUTO_IN || nSigType == BLOCKTYPE_AUTO_OUT || nSigType == BLOCKTYPE_INTER_IN || nSigType == BLOCKTYPE_INTER_OUT)
				{
					pBlockObj = (CObject *)signal;
					lvi_Block.mask = LVIF_TEXT | LVIF_STATE;
					
					lvi_Block.iItem = i;
					lvi_Block.iSubItem = 0;
					
					lvi_Block.pszText = (char*)(LPCTSTR)pInfo->m_strName;
					lvi_Block.stateMask = LVIS_STATEIMAGEMASK;
					lvi_Block.state = INDEXTOSTATEIMAGEMASK(1);
				}
				else
				{
					pSignalObj = (CObject *)signal;
					lvi_Signal.mask = LVIF_TEXT | LVIF_STATE;
					
					lvi_Signal.iItem = i;
					lvi_Signal.iSubItem = 0;
					
					lvi_Signal.pszText = (char*)(LPCTSTR)pInfo->m_strName;
					lvi_Signal.stateMask = LVIS_STATEIMAGEMASK;
					lvi_Signal.state = INDEXTOSTATEIMAGEMASK(1);
				}
				
				if (!nDir)
				{
					if (nSigType == BLOCKTYPE_AUTO_OUT)
					{
						pB1 = (ScrInfoBlock *)pInfo->m_pEipData;
						pB1->AXLOCC = 1;
						pB1->AXLDST = 1;
					}
					else if (nSigType== BLOCKTYPE_AUTO_IN)
					{
						pB2 = (ScrInfoBlock *)pInfo->m_pEipData;
						pB2->AXLOCC = 1;
						pB2->AXLDST = 1;
					}
				}
				else
				{
					if (nSigType == BLOCKTYPE_AUTO_OUT)
					{
					    pB3 = (ScrInfoBlock *)pInfo->m_pEipData;
						pB3->AXLOCC = 1;
						pB3->AXLDST = 1;
					}
					else if (nSigType == BLOCKTYPE_AUTO_IN)
					{
					    pB4 = (ScrInfoBlock *)pInfo->m_pEipData;
						pB4->AXLOCC = 1;
						pB4->AXLDST = 1;
					}
				}
				break;

			case SCRINFO_SWITCH :
				lvi_Point.mask = LVIF_TEXT | LVIF_STATE;
				
				lvi_Point.iItem = i;
				lvi_Point.iSubItem = 0;
				
				lvi_Point.pszText = (char*)(LPCTSTR)pInfo->m_strName;
				lvi_Point.stateMask = LVIS_STATEIMAGEMASK;
				lvi_Point.state = INDEXTOSTATEIMAGEMASK(1);

				point = new CMyPoint(_pEITInfo);
				pObj = (CObject*)point;
				pPointObj = (CObject *)point;
				pPoint = (ScrInfoSwitch *)pInfo->m_pEipData;
				pPoint->BLOCK = 1;
				nSwitch++;
                point->m_nSwitchType = _pEITInfo->m_pSwitchInfoTable[nSwitch].nSwitchType;
                point->m_nPointKeyNo = _pEITInfo->m_pSwitchInfoTable[nSwitch].nPointKeyNo;
				break;

			case SCRINFO_LAMP :
				pScrLamp = new CMyGeneral(_pEITInfo);
				pObj = (CObject*)pScrLamp;
				nLamp++;
				break;

			case SCRINFO_BUTTON :
				pObj = NULL;
				break;
			}

// Graphic object data를 object array에 추가하고, data 정보를 설정한다...
 			if (pObj) 
			{
				((CSimObject*)pObj)->m_strName = pInfo->m_strName; // Object name 저장 
				((CSimObject*)pObj)->m_pData = pInfo->m_pEipData;  // Dynamic data의 pointer 저장
				
				m_MySim.AddHead( pObj );
				CString str;
				str.Format("%d", nListLoc );
				m_ScrIO.InsertItem(&lvi);
				m_ScrIO.SetItemText( nListLoc,1,str);
				m_ScrIO.SetItemData( nListLoc, (DWORD)pObj);
				nListLoc++;
			}
			if (pPointObj)
			{
				((CSimObject*)pPointObj)->m_strName = pInfo->m_strName; // Object name 저장 
				((CSimObject*)pPointObj)->m_pData = pInfo->m_pEipData;  // Dynamic data의 pointer 저장
				
				m_MyPointSim.AddHead( pPointObj );
				CString str;
				str.Format("%d", nListForPoint );
				m_POINT.InsertItem(&lvi_Point);
				m_POINT.SetItemText( nListForPoint,1,str);
				m_POINT.SetItemData( nListForPoint, (DWORD)pPointObj);
				nListForPoint++;
			}
			if (pTrackObj)
			{
				((CSimObject*)pTrackObj)->m_strName = pInfo->m_strName; // Object name 저장 
				((CSimObject*)pTrackObj)->m_pData = pInfo->m_pEipData;  // Dynamic data의 pointer 저장
				
				m_MyTrackSim.AddHead( pTrackObj );
				CString str;
				str.Format("%d", nListLocForTC );
				m_TRACK.InsertItem(&lvi_Track);
				m_TRACK.SetItemText( nListLocForTC,1,str);
				m_TRACK.SetItemData( nListLocForTC, (DWORD)pTrackObj);
				nListLocForTC++;
			}
			if (pSignalObj)
			{
				((CSimObject*)pSignalObj)->m_strName = pInfo->m_strName; // Object name 저장 
				((CSimObject*)pSignalObj)->m_pData = pInfo->m_pEipData;  // Dynamic data의 pointer 저장
				
				m_MySignalSim.AddHead( pSignalObj );
				CString str;
				str.Format("%d", nListLocForSig );
				m_SIGNAL.InsertItem(&lvi_Signal);
				m_SIGNAL.SetItemText( nListLocForSig,1,str);
				m_SIGNAL.SetItemData( nListLocForSig, (DWORD)pSignalObj);
				nListLocForSig++;
			}
			if (pBlockObj)
			{
				((CSimObject*)pBlockObj)->m_strName = pInfo->m_strName; // Object name 저장 
				((CSimObject*)pBlockObj)->m_pData = pInfo->m_pEipData;  // Dynamic data의 pointer 저장
				
				m_MyBlockSim.AddHead( pBlockObj );
				CString str;
				str.Format("%d", nListLocForBlk );
				m_BLOCK.InsertItem(&lvi_Block);
				m_BLOCK.SetItemText( nListLocForBlk,1,str);
				m_BLOCK.SetItemData( nListLocForBlk, (DWORD)pBlockObj);
				nListLocForBlk++;
			}
			pObj = NULL;
			pTrackObj = NULL;
			pSignalObj = NULL;
			pPointObj = NULL;
			pBlockObj = NULL;
		}

// Create Route Combo
// 		CMyStrList &listRoute = _pEITInfo->m_strRoute;
//		POSITION pos;
// 		for ( pos = listRoute.GetHeadPosition(); pos != NULL;) 
// 		{
// 			CString &str = *(CString*)listRoute.GetNext( pos );
// 			m_comboRoute.AddString( str );
// 		}
// 
// Create Signal Combo
// 		CMyStrList &listSignal = _pEITInfo->m_strSignal;
// 		for ( pos = listSignal.GetHeadPosition(); pos != NULL;) 
// 		{
// 			CString &str = *(CString*)listSignal.GetNext( pos );
// 			m_comboSignal.AddString( str );
// 		}
// 
// Create Track Combo
// 		CMyStrList &listTrack = _pEITInfo->m_strTrack;
// 		for ( pos = listTrack.GetHeadPosition(); pos != NULL;) 
// 		{
// 			CString &str = *(CString*)listTrack.GetNext( pos );
// 			m_comboTrack.AddString( str );
// 		}
// 
// Create Switch Combo
// 		CMyStrList &listSwitch = _pEITInfo->m_strSwitch;
// 		for ( pos = listSwitch.GetHeadPosition(); pos != NULL;) 
// 		{
// 			CString &str = *(CString*)listSwitch.GetNext( pos );
// 			m_comboSwitch.AddString( str );
// 		}

		UpdateData( FALSE );
	}

	m_bOnSim = TRUE;

	_pCMemView = this;
}

extern SharedMemory *_pShared;
extern unsigned int OnSGTimerTick;
//
void CMemView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	/*
	long lParam;
	*/
	char m_szComMsg[16];
	BYTE nShareControl = 0;
	m_byteFunction = m_szMsgQue[1];

	if ( m_bOnSim && _pEITInfo ) 
	{
		//OtherStateType &OtherState = *(OtherStateType *)&m_pVMEM[0x0f];

        if ( m_nMode == SIMMODE_SIM ) 
		{
			/*
			if ( m_nShared == TRUE ) {
				m_SMlesCommand.Lock();
				memcpy ( &m_szComMsg , m_SMlesCommand.GetData(), 16);
				memcpy ( &lParam , &m_szComMsg[1] , 2 );
				Operate ( (long) m_szComMsg[0] , lParam ,0 );
				memset ( m_SMlesCommand.GetData() , 0, 16);
				m_SMlesCommand.Unlock();
			}
			*/

//TRACE ("m_szMsgQue = [%d:%d:%d] \n", m_szMsgQue[0],m_szMsgQue[1],m_szMsgQue[2]);
#if 0
			if (m_MsgSequence == m_szMsgQue[0])   // SIM으로 부터 제어가 없는 경우 Shared Control이 있는지 검색하여 있으면 제어한다..
			{
				if ((LPSTR)_pShared->SharedControlAddress != NULL && m_szMsgQue[1] != 1)
				{
					memcpy (&m_szMsgQue[1], (LPSTR)_pShared->SharedControlAddress, 15);
					if (m_szMsgQue[1])
					{
						nShareControl = 1;
						memcpy (&m_szComMsg[1], &m_szMsgQue[1], 15);
						m_szComMsg[0] = 0x41;
					}
				}
			}
#endif
			m_MsgSequence = m_szMsgQue[0];

            CEipEmu &m_Engine = _pEITInfo->m_Engine;

			m_Engine.ProcessConfigList();			
			m_Engine.UpdateAXLStatusByConfig();
 
			if(m_CtcCmd[2] != 0)
			{
				m_Engine.ProcessCtcCommand( m_CtcCmd );
			}

			if ( _bSIMModeIO )
			{
 				GetInput();
 				SetOutput();
			}
			else 
			{
				m_Engine.Operate( m_szMsgQue );
				m_Engine.RunInterlock( m_szMsgQue );
			}

			m_Engine.MakeCtcElementImage(m_CtcBuffer);

			m_CtcDlg.UpdateElementData(m_CtcBuffer);

            // Signal에 대한 Timer를 표시한다.
			CString str;
			str.Format("ON Timer cnt: %d", OnSGTimerTick);
			StatusView( str );

			// LSM 화면의 변화를 Update한다....
	        if (_pScrView && IsWindow(_pScrView->m_Screen) ) {
		        _pScrView->m_Screen.ScreenUpdate( (long)m_pVMEM );
	        }


			// 제어 Update....
			if (nShareControl)
				memcpy(&m_pVMEM[m_Engine.m_TableBase.InfoGeneralIO.nStart + m_Engine.m_TableBase.InfoGeneralIO.nSize], m_szComMsg, 16);


			if ((LPSTR)_pShared->SharedMemoryAddress != NULL)
			{
				// 2005.10.12  시간을 Update 한다.. --> 이것은 SharedMemory를 사용할 때 EIS로 전송하기 위한 것이다.
				SYSTEMTIME systime;
				GetLocalTime( &systime );
				
				m_pVMEM[OFFSET_TIME_SEC]	= BYTE( systime.wSecond );
				m_pVMEM[OFFSET_TIME_MIN]	= BYTE( systime.wMinute );
				m_pVMEM[OFFSET_TIME_HOUR]	= BYTE( systime.wHour );
				m_pVMEM[OFFSET_TIME_DAY]	= BYTE( systime.wDay );
				m_pVMEM[OFFSET_TIME_MON]	= BYTE( systime.wMonth );
				m_pVMEM[OFFSET_TIME_YEAR]	= BYTE( systime.wYear - 2000 );

				memcpy ((LPSTR)_pShared->SharedMemoryAddress, m_pVMEM, 2048);
				PulseEvent(m_hEventRun);
			}

			// 명령 Message buffer를 저장한다.
	        BYTE *m_pMsgBuffer = &m_pVMEM[ m_Engine.m_TableBase.InfoGeneralIO.nStart + m_Engine.m_TableBase.InfoGeneralIO.nSize ];
			m_pMsgBuffer[8] = 0;

            // 각 Object별로 동작 상태를 Update한다.
			// 연동결과가 이 routine에서 update되고 화면에 표시되기 위해 준비 되어진다.
            RunMySim();

			/*
			if ( m_nShared == TRUE ) {
				m_SMles.Lock();
				memcpy ((BYTE*)m_SMles.GetData() , m_pVMEM, 2048);
				m_SMles.Unlock();
			}
			*/

			//OtherState.bSW_IGCPR = OtherState.bSW_OGCPR;
        }
        else if ( m_nMode == SIMMODE_IOSIM ) 
		{
	        if ( _pSerial && _pSerial->m_bRxReady ) // For IO Simulation
			{       
			    _pEITInfo->DataUpdate( 2, _pSerial->m_pIO_OutBuffer, TRUE );	// OUT  IPM에서 PC
			    _pEITInfo->DataUpdate( 1, _pSerial->m_pIO_InBuffer, TRUE );	    // IN	PC에서 IPM


			    if (_pScrView && IsWindow(_pScrView->m_Screen) ) {
				    _pScrView->m_Screen.ScreenUpdate( (long)m_pVMEM );
			    }
			    _pSerial->m_bRxReady = FALSE;
	        }


	        if (_pScrView && IsWindow(_pScrView->m_Screen) ) {
		        _pScrView->m_Screen.ScreenUpdate( (long)m_pVMEM );
	        }

            RunMySim();
			//OtherState.bSW_IGCPR = OtherState.bSW_OGCPR;

			ViewPortValue();
        }
	}
	else
		CDialog::OnTimer(nIDEvent);
}

void CMemView::GetInput() 
{
	CMainFrame* pWnd;
	pWnd = (CMainFrame*)AfxGetMainWnd();
	// 연동장치의 FDOM(352~479)/FDBM(672~735)은 Simulator의 FDIM이 된다.
	for ( int i = 352; i < 480; i++)
	{
		int nBitPos = _pEITInfo->m_pIOInfo->m_pAssignTable[i].BitPos;
		int nByte = nBitPos/8;
		int nBit = nBitPos%8;
		BYTE byteFlagOfBitForVMEM = 0x00;
		BYTE byteFlagOfBitForFDIM = 0x00;
		int nFByte = i/8-44;
		int nFBit = i%8;
		
		byteFlagOfBitForVMEM = GetFlagOfBit(nBit);
		byteFlagOfBitForFDIM = GetFlagOfBit(nFBit);

		if ( pWnd->INPUT.Data[nFByte] & byteFlagOfBitForFDIM )
		{
			m_pVMEM[nByte] |= byteFlagOfBitForVMEM;
		}
		else
		{
			m_pVMEM[nByte] &= ~byteFlagOfBitForVMEM;
		}
	}
	for ( int k = 672; k < 736; k++)
	{
		int nBitPos = _pEITInfo->m_pIOInfo->m_pAssignTable[k].BitPos;
		int nByte = nBitPos/8;
		int nBit = nBitPos%8;
		BYTE byteFlagOfBitForVMEM = 0x00;
		BYTE byteFlagOfBitForFDIM = 0x00;
		int nFByte = k/8-68;
		int nFBit = k%8;
		
		byteFlagOfBitForVMEM = GetFlagOfBit(nBit);
		byteFlagOfBitForFDIM = GetFlagOfBit(nFBit);
		
		if ( pWnd->INPUT.Data[nFByte] & byteFlagOfBitForFDIM )
		{
			m_pVMEM[nByte] |= byteFlagOfBitForVMEM;
		}
		else
		{
			m_pVMEM[nByte] &= ~byteFlagOfBitForVMEM;
		}
	}
}

void CMemView::SetOutput() 
{
	CMainFrame* pWnd;
	pWnd = (CMainFrame*)AfxGetMainWnd();
	// 연동장치의 0번카드는 SYSTEM FDOM(0 ~ 31port)이므로 Simulator의 1번부터 8번카드(32~255port)가 FDOM이 된다.
	for ( int i = 32; i < 288; i++)
	{
		int nBitPos = _pEITInfo->m_pIOInfo->m_pAssignTable[i].BitPos;
		int nByte = nBitPos/8;
		int nBit = nBitPos%8;
		BYTE byteFlagOfBitForVMEM = 0x00;
		BYTE byteFlagOfBitForFDOM = 0x00;
		int nFByte = i/8-4;
		int nFBit = i%8;
		
		byteFlagOfBitForVMEM = GetFlagOfBit(nBit);
		byteFlagOfBitForFDOM = GetFlagOfBit(nFBit);
		
		if ( m_pVMEM[nByte] & byteFlagOfBitForVMEM )
		{
			pWnd->OUTPUT.Data[nFByte] |= byteFlagOfBitForFDOM;
		}
		else
		{
			pWnd->OUTPUT.Data[nFByte] &= ~byteFlagOfBitForFDOM;
		}
	}
}

BYTE CMemView::GetFlagOfBit(int i) 
{
	BYTE byteFlag = 0x00;

	switch(i)
	{
	case 0:
		byteFlag = 0x01;
		break;
	case 1:
		byteFlag = 0x02;
		break;
	case 2:
		byteFlag = 0x04;
		break;
	case 3:
		byteFlag = 0x08;
		break;
	case 4:
		byteFlag = 0x10;
		break;
	case 5:
		byteFlag = 0x20;
		break;
	case 6:
		byteFlag = 0x40;
		break;
	case 7:
		byteFlag = 0x80;
		break;
	}

	return byteFlag;
}

CSimObject * CMemView::SearchMySim(CString & str)
{
	POSITION pos = m_MySim.GetHeadPosition();
	CSimObject *obj;
	while (pos) {
		obj = (CSimObject*)m_MySim.GetNext( pos );
		if (obj->m_strName == str) return obj;
	}
	return NULL;
}

void CMemView::RunMySim()
{
	POSITION pos = m_MySim.GetHeadPosition();
	CSimObject *obj;
	while (pos) 
	{
		obj = (CSimObject*)m_MySim.GetNext( pos );
		if (obj)
		{
			if ( m_byteFunction == WS_OPMSG_RESET )
			{
				obj->ReceiveControlMessage(m_byteFunction);
			}
			obj->Run();
		}
	}
}

void CMemView::RemoveMySim()
{
	POSITION pos = m_MySim.GetHeadPosition();
	while (pos) {
		CObject *obj = m_MySim.GetNext( pos );
		if (obj) delete obj;
	}
	m_MySim.RemoveAll();
}

WORD *_tbl_p;

WORD getd() {
	WORD d = *_tbl_p++;
	return d;
}

void Err( char *s ) {
	return;	//	MessageBox( NULL, s, "FAIL CHECK", MB_OK);
}

void CMemView::OnHide() 
{
	// TODO: Add your control notification handler code here
	ShowWindow( SW_HIDE );
}

void CMemView::OnViewIolist() 
{
	// TODO: Add your control notification handler code here
	if (_pEITInfo) {
		_pEITInfo->m_pIOInfo->ShowWindow( SW_SHOW );
	}
}

	DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;
  	UnitStatusType &UnitState = *(UnitStatusType *)&m_pVMEM[OFFSET_UNIT_STATUS];

void CMemView::OnButtonOper() 
{
	// TODO: Add your control notification handler code here

    if (UnitState.bSysRun)
	{
		m_szMsgQue[1] = WS_OPMSG_RESET;
	}
	else
	{
		m_szMsgQue[1] = WS_OPMSG_START;
	}

}

void CMemView::OnButtonRoute() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
	int n = m_comboRoute.GetCurSel();
	*(short*)&m_szMsgQue[2] = n+1;
	if (m_bBlock) {
//		m_szMsgQue[1] = OPMSG_BLOCK;
	}
//	else if (m_bTTB) {
//		m_szMsgQue[1] = WS_OPMSG_TTB;
//	}
	else {
//		m_szMsgQue[1] = OPMSG_ROUTE;
	}
}

void CMemView::OnButtonRoutecan() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
	int n = m_comboRoute.GetCurSel();
	*(short*)&m_szMsgQue[2] = n+1;
	if (m_bBlock) {
//		m_szMsgQue[1] = OPMSG_BLOCKCAN;
	}
//	else if (m_bTTB) {
//		m_szMsgQue[1] = WS_OPMSG_TTBCANCEL;
//	}
	else {
//		m_szMsgQue[1] = OPMSG_ROUTECAN;
	}
}

void CMemView::OnButtonSw() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
	int n = m_comboSwitch.GetCurSel();
	m_szMsgQue[2] = (char)(n+1);
	m_szMsgQue[1] = WS_OPMSG_SWTOGGLE;
}

CMyTrack * CMemView::TrackObjectFind( BYTE nTrkID )
{
	CMyTrack *pTrack = NULL;
	POSITION pos;
	for ( pos = m_MySim.GetHeadPosition(); pos != NULL; ) {
		CSimObject *pObj = (CSimObject *)m_MySim.GetNext( pos );
		if (pObj->IsKindOf( RUNTIME_CLASS( CMyTrack ))) {
			pTrack = (CMyTrack *)pObj;
			if (pTrack->m_nID == nTrkID) return pTrack;
		}
	}
	return NULL;
}

void CMemView::OnButtonInfo() 
{
	if ( _pEITInfo ) {
		CLISInfoView dlg( _pEITInfo );
		dlg.DoModal();
	}
}
extern BOOL _bDlgLoad;
extern BOOL _bDefault1;
void CMemView::OnButtonSys() 
{
	if ( _bDlgLoad ) {
		m_pSysBtnDlg->SetFocus();
		return;
	}
	if (_bDefault1)
	{
		_bDefault1 = FALSE;
	}
	else
	{
		_bDefault1 = TRUE;
	}

	m_pSysBtnDlg = NULL;
	m_pSysBtnDlg = new CSysBtnDlg(_pSerial);
	m_pSysBtnDlg->Create(IDD_DIALOG_SYSTEMBUTTON);
	m_pSysBtnDlg->ShowWindow( SW_SHOWMINIMIZED);
	m_pSysBtnDlg->ShowWindow( SW_HIDE);
	
	CLISApp *pApp = (CLISApp *)AfxGetApp();
	if(pApp->m_bIOSIM)
	{
		m_pSysBtnDlg->SetIOSIMMode();

		// 두번 세팅 안되도록.
		pApp->m_bIOSIM = FALSE;
	}
	
	SystemStatusType *nSysVar=(SystemStatusType*)&m_pVMEM[OFFSET_SYS_VAR];
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
	nSysVar->bN3 = 1;
}
// LSM에서 제어한 명령을 처리한다....
BYTE nOpCyc = 0;
void CMemView::Operate(long message, long wParam, long lParam)  //(short nFunction, short nArgument)
{
    short nFunction = (short)message;
    short nArgument = (short)wParam;

	if (nFunction == WS_OPMSG_TRKSEL) 
	{
		CMyTrack *pTrack = TrackObjectFind( (BYTE)nArgument );

		// 여기서 바로 상태 변화를 수행한다...
		if (pTrack && pTrack->m_pData) 
		{
			ScrInfoTrack *pInfo = (ScrInfoTrack *)pTrack->m_pData;
			pInfo->TRACK ^= 1;

		}
	}
	else if (nFunction == WS_OPMSG_CONT_IOSIM)
	{
		char* cName = (char*)lParam;
		CString strObj;
		strObj.Format("%s",cName);

		if ( wParam == OBJ_SWITCH)
		{
			strObj.Remove('A');
		}
		else if ( wParam == OBJ_TRACK)
		{
			strObj = strObj.Left(strObj.Find('.'));
		}

		switch(wParam)
		{
		case OBJ_SIGNAL:
			if (strObj.Find('B') == 0)
				m_BLOCK.OnLButtonDblClkByLSM(strObj);
			else
				m_SIGNAL.OnLButtonDblClkByLSM(strObj);
			break;
		case OBJ_SWITCH:
			m_POINT.OnLButtonDblClkByLSM(strObj);
			break;
		case OBJ_TRACK:
			m_TRACK.OnLButtonDblClkByLSM(strObj);
			break;
		}
	}
	else 
	{
		*(short*)&m_szMsgQue[2] = nArgument;
		m_szMsgQue[1] = (char)nFunction;
        if (nFunction == WS_OPMSG_SETTRAINNAME) {
            memcpy( m_szMsgQue + 3, (char*)lParam, 5 );
        }
	}
	m_szMsgQue[0] = '@' + (nOpCyc++ & 0x0f);
/*
	switch (nFunction) {
	case OPMSG_START	: opClearAllRoute();				break;
	case OPMSG_ROUTE	: opRoute( nArgument );		break;
	case OPMSG_ROUTECAN	: opRouteCan( nArgument );	break;
	case OPMSG_BLOCK	: opBlock( nArgument );		break;
	case OPMSG_BLOCKCAN	: opBlockCan( nArgument );	break;
	case OPMSG_TTB		: opTTB( nArgument );		break;
	case OPMSG_TTBCAN	: opTTBCan( nArgument );		break;
	case OPMSG_SWTGL :
	case OPMSG_SWPOS :
	case OPMSG_SWNEG :
		opSwitch( *(pMsg+1) ); break;
	}
*/
}

void CMemView::OnCheckRnd() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
    _nRANDOMGENERATE = m_bRandom;
}

#define CTCID_LOCAL 6
#define CTCID_CTC   7
#define CTCID_CSSB  8
#define CTCID_CPB   9
#define CTCID_ERRB  10
#define CTCID_TTB   11
#define CTCID_TTCB  12

#define CTCTYPE_SIGNAL 1
#define CTCTYPE_TRACK  2
#define CTCTYPE_SWITCH 4

void CMemView::OnButtonSigdest() 
{
	// TODO: Add your control notification handler code here
    BYTE nSignalID, nTrackID;
	UpdateData( TRUE );
	nSignalID = (BYTE)m_comboSignal.GetCurSel() + 1;
	nTrackID = (BYTE)m_comboTrack.GetCurSel() + 1;
    union {
        short nCtcPtr;
        struct {
            BYTE nID;
            BYTE nPort;
        };
    };
 //   nCtcPtr = FindCtcPtr( nSignalID, CTCTYPE_SIGNAL );
 //   CtcCommand( nPort, nID );
 //   nCtcPtr = FindCtcPtr( nTrackID, CTCTYPE_TRACK );
 //   CtcCommand( nPort, nID );
}

void CMemView::OnButtonCpb() 
{
	// TODO: Add your control notification handler code here
    BYTE nSwitchID;
	UpdateData( TRUE );
	nSwitchID = (BYTE)m_comboSwitch.GetCurSel() + 1;
    union {
        short nCtcPtr;
        struct {
            BYTE nID;
            BYTE nPort;
        };
    };
//	nCtcPtr = FindCtcPtr( nSwitchID, CTCTYPE_SWITCH );
//	CtcCommand( 0, CTCID_CPB );
//	CtcCommand( nPort, nID );
}

void CMemView::OnButtonTtb() 
{
	// TODO: Add your control notification handler code here
    BYTE nSignalID;
	UpdateData( TRUE );
	nSignalID = (BYTE)m_comboSignal.GetCurSel() + 1;
    union {
        short nCtcPtr;
        struct {
            BYTE nID;
            BYTE nPort;
        };
    };
//     nCtcPtr = FindCtcPtr( nSignalID, CTCTYPE_SIGNAL );
//     CtcCommand( 0, CTCID_TTB );
//     CtcCommand( nPort, nID );
}

void CMemView::OnButtonTtcb() 
{
	// TODO: Add your control notification handler code here
    BYTE nSignalID;
	UpdateData( TRUE );
	nSignalID = (BYTE)m_comboSignal.GetCurSel() + 1;
    union {
        short nCtcPtr;
        struct {
            BYTE nID;
            BYTE nPort;
        };
    };
//     nCtcPtr = FindCtcPtr( nSignalID, CTCTYPE_SIGNAL );
//     CtcCommand( 0, CTCID_TTCB );
//     CtcCommand( nPort, nID );
}

void CMemView::OnButtonCssb() 
{
	// TODO: Add your control notification handler code here
    BYTE nSignalID;
	UpdateData( TRUE );
	nSignalID = (BYTE)m_comboSignal.GetCurSel() + 1;
    union {
        short nCtcPtr;
        struct {
            BYTE nID;
            BYTE nPort;
        };
    };
//     nCtcPtr = FindCtcPtr( nSignalID, CTCTYPE_SIGNAL );
//     CtcCommand( 0, CTCID_CSSB );
//     CtcCommand( nPort, nID );
}

void CMemView::OnRadioMode() 
{
	// TODO: Add your control notification handler code here
    if (m_nMode != 0) {
        m_nMode = 0;
        _pSerial->m_nMode = 0;
        _pSerial->CloseModem();
    }
}

void CMemView::OnRadioIosim() 
{
	// TODO: Add your control notification handler code here
    if (m_nMode != 1) {
        m_nMode = 1;
        _pSerial->m_nMode = 1;
        _pSerial->OpenModem();
    }
}

void CMemView::OnRadioCtc() 
{
	// TODO: Add your control notification handler code here
    if (m_nMode != 2) {
        m_nMode = 2;
        _pSerial->m_nMode = 2;
        _pSerial->OpenModem();
    }
}

// short CMemView::FindCtcPtr( short nObjectID, short nType )
// {
//     IOBitMatchInfoType	*m_pAssignTable = _pEITInfo->m_pIOInfo->m_pAssignTable;
//     short nDTSOutBase = MAX_IOBITMAP + 128 * 6;
//     union {
//         short nCtcPtr;
//         struct {
//             BYTE nID;
//             BYTE nPort;
//         };
//     };
// 
//     for (short i = 0;  i<4; i++) {
//         short no = nDTSOutBase + i * 64;
//         for (short j=0; j<64; j++) {
//             nCtcPtr = *(short*)&m_pAssignTable[no];
//             if (nPort == nType && nID == (BYTE)nObjectID ) {
//                 nPort = (BYTE)i;
//                 nID = (BYTE)j;
//                 return nCtcPtr;
//             }
//             no++;
//         }
//     }
//     return 0;
// }

// short CMemView::CtcCommand( short nPort, short nID )
// {
//     CString str;
//     str.Format( "CTCOUT PORT:%d ID:%d", nPort, nID );
//     StatusView( str );
// 
//     BYTE bf[16];
//     bf[0] = 0x40 + nPort;
//     bf[1] = 0xF0;
//     bf[2] = m_nSeqNo++;
//     bf[3] = 0x9D;   // Set Output Command
//     bf[4] = 0x07;   // count
//     bf[5] = 0x80 + nID;
// 
//     BYTE BCC = 0;
//     for (short i=0; i<6; i++) {
//         BCC += bf[i];
//     }
//     bf[i] = BCC;
// 
//     //WriteBlock( bf, 7 );
// 
//     return 0;
// }

void CMemView::StatusView( CString &str )
{
    for (short i=0; i<2; i++) {
        m_strStatusLine[i] = m_strStatusLine[i+1];
    }
    m_strStatusLine[i] = str;
    m_strStatus = m_strStatusLine[0];
    for (i=0; i<2; i++) {
        m_strStatus += "\r\n";
        m_strStatus += m_strStatusLine[i+1];
    }
	UpdateData( FALSE );
}


// Combo box의 Select에 의해 port number가 설정된다....
void CMemView::OnSelchangeComboCom() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
    _PortNo = m_nComPort;
}


void CMemView::ProcessPendingAccept()
{
	CClientSocket* pSocket = new CClientSocket(this);

	if (m_pSocket->Accept(*pSocket))
	{
//		pSocket->Init();	
		m_connectionSocket = pSocket;
	}
	else
		delete pSocket;
}

void CMemView::ProcessPendingRead(CClientSocket* pSocket)
{
	BYTE nData[4];
	CMyTrack *pTrack;
	ScrInfoTrack *pInfo;
	pSocket->Receive( &nData, 4 );
	switch ( nData[0] ) {
	case 1 : // Track = 1
		pTrack = TrackObjectFind( nData[1] );
		if (pTrack && pTrack->m_pData) {
			pInfo = (ScrInfoTrack *)pTrack->m_pData;
			pInfo->TRACK = 1;
		}
		break;
	case 2 : // Track = 0
		pTrack = TrackObjectFind( nData[1] );
		if (pTrack && pTrack->m_pData) {
			pInfo = (ScrInfoTrack *)pTrack->m_pData;
			pInfo->TRACK = 0;
		}
		break;
	}
}

void CMemView::ViewPortValue()
{
	CWnd *pWnd = GetDlgItem( IDC_EDIT_PORTADDR );
	if ( !pWnd ) return;
	pWnd->GetWindowText( m_strPortAddress );
	char *p;
	short nAddr = (short)strtol( m_strPortAddress, &p, 16 );
	short nBitLoc = 1 << ( nAddr & 7 );
	nAddr >>= 3;

	BYTE cOutData = _pSerial->m_pIO_OutBuffer[ nAddr ];
	m_bPortValue = cOutData & nBitLoc;
	pWnd = GetDlgItem( IDC_CHECK_PORTVALUE );
	if ( pWnd ) {
		CButton *pButton = (CButton*)pWnd;
		pButton->SetCheck( m_bPortValue ? 1 : 0 );
		pButton->Invalidate();
	}
}


extern int _nSysBtnIndex[];
extern int _nSysCount;

extern BOOL	_bACTIVEON1;
extern BOOL	_bACTIVEON2;
extern BOOL	_bVPOR1;
extern BOOL	_bVPOR2;
extern BOOL	_bFUSE1;
extern BOOL	_bFUSE2;
extern BOOL	_bFUSE3;
extern BOOL	_bFUSE4;
extern BOOL _bFUSEPR;
extern BOOL	_bFAN1;
extern BOOL	_bFAN2;
extern BOOL	_bFAN3;
extern BOOL	_bFAN4;
extern BOOL	_b24VINT;
extern BOOL	_b24VNEXT;
extern BOOL	_b24VSEXT;
extern BOOL	_bMAINPOWER;
extern BOOL	_bGENRUN;
extern BOOL	_bGENLOWFUEL;
extern BOOL	_bGENFAIL;
extern BOOL	_bUPS2FAIL;
extern BOOL	_bUPS1FAIL;
extern BOOL	_bCHARGEFAIL;
extern BOOL	_bLOWVOLTAGE;
extern BOOL	_bCHARGING;
extern BOOL	_bGNDFAIL;
extern BOOL	_bNKWEPRN;
extern BOOL	_bNKWEPRS;
extern BOOL	_bNKWEPRA;
extern BOOL	_bNKWEPRB;
extern BOOL	_bNKWEPRC;
extern BOOL	_bNKWEPRD;
extern BOOL	_bNKWEPRE;
extern BOOL	_bNKWEPRF;
extern BOOL	_bNKWEPRG;
extern BOOL _bDIMMER;
extern BOOL _bLC1NKXPR;
extern BOOL _bLC1BELACK;
extern BOOL _bLC2NKXPR;
extern BOOL _bLC2BELACK;
extern BOOL _bLC3NKXPR;
extern BOOL _bLC3BELACK;
extern BOOL _bLC4NKXPR;
extern BOOL _bLC4BELACK;
extern BOOL _bLC5NKXPR;
extern BOOL _bLC5BELACK;
extern BOOL _bPAS;
extern BOOL _bSLS;
extern BOOL	_bMode;
extern BOOL _bCTCRequest;
extern BOOL _bLC3NKXPR;
extern BOOL _bLC3BELACK;
extern BOOL _bMCCR1;
extern BOOL _bMCCR2;
extern BOOL _bMCCR1Online;
extern BOOL _bMCCR2Online;
extern BOOL _bBlockUp;
extern BOOL _bBlockDown;
extern BOOL _bBlockMid;



extern int _nIndex[];
extern int _nIndexCount;

// 시스템버튼 값 셋팅위해.. 
void CMemView::SetSystemButton()
{
    SystemStatusType *nSysVar= &((DataHeaderType*)m_pVMEM)->nSysVar;
	SystemStatusTypeExt &nSysVarExt = ((DataHeaderType*)m_pVMEM)->nSysVarExt;
	LCCStatusType &nLCCVar = ((DataHeaderType*)m_pVMEM)->LCCStatus;
	
////
	_bSLS = nSysVar->bN3;
// if ( _bPAS)
// {
// 	nSysVar->bN1 = 1;
// }
// else
// {
// 	nSysVar->bN1 = 0;
// }
// 
// if ( _bSLS)
// {
// 	nSysVar->bN3 = 1;
// }
// else
// {
// 	nSysVar->bN3 = 0;
// }

////
if ( _bACTIVEON1 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0001;
	nSysVar->bSYS1 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0001;
	nSysVar->bSYS1 = 0;
}
////
if ( _bACTIVEON2 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0002;
	nSysVar->bSYS2 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0002;
	nSysVar->bSYS2 = 0;
}
////
if ( _bVPOR1 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0004;
	nSysVar->bSYS1Good = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0004;
	nSysVar->bSYS1Good = 0;
}
////
if ( _bVPOR2 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0008;
	nSysVar->bSYS2Good = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0008;
	nSysVar->bSYS2Good = 0;
}
////
if ( _bFUSE1 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0010;
	nSysVar->bFuse1 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0010;
	nSysVar->bFuse1 = 0;
}
////
if ( _bFUSE2 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0020;
	nSysVar->bFuse2 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0020;
	nSysVar->bFuse2 = 0;
}

if ( _bFUSE3 ) 
{
	nSysVarExt.bFuse3 = 1;
} 
else 
{
	nSysVarExt.bFuse3 = 0;
}
////
if ( _bFUSE4 ) 
{
	nSysVarExt.bFuse4 = 1;
} 
else 
{
	nSysVarExt.bFuse4 = 0;
}
////
if ( _bFAN1 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0040;
	nSysVar->bFan1 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0040;
	nSysVar->bFan1 = 0;
}
////
if ( _bFAN2 ) 
{
	_pSerial->m_pIO_InBuffer[4] |= 0x0080;
	nSysVar->bFan2 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[4] &= ~0x0080;
	nSysVar->bFan2 = 0;
}
////
if ( _bFAN3 ) 
{
//	_pSerial->m_pIO_InBuffer[4] |= 0x0080;
	nSysVarExt.bFan3 = 1;
} 
else 
{
//	_pSerial->m_pIO_InBuffer[4] &= ~0x0080;
	nSysVarExt.bFan3 = 0;
}
////
if ( _bFAN4 ) 
{
//	_pSerial->m_pIO_InBuffer[4] |= 0x0080;
	nSysVarExt.bFan4 = 1;
} 
else 
{
//	_pSerial->m_pIO_InBuffer[4] &= ~0x0080;
	nSysVarExt.bFan4 = 0;
}

/*  Sylhet 시험시만 사용한다.
if ( _bFAN3 ) 
{
	nSysVar->bPKeyN = 1;
} 
else 
{
	nSysVar->bPKeyN = 0;
}
////
if ( _bFAN4 ) 
{
	nSysVar->bPKeyS = 1;
} 
else 
{
	nSysVar->bPKeyS = 0;
}
*/

////
if ( _b24VINT ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0001;
	nSysVar->bB24PWR = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0001;
	nSysVar->bB24PWR = 0;
}
////
if ( _b24VNEXT ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0002;
	nSysVar->bExtPowerN = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0002;
	nSysVar->bExtPowerN = 0;
}
////
if ( _b24VSEXT ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0004;
	nSysVar->bExtPowerS = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0004;
	nSysVar->bExtPowerS = 0;
}
////
if ( _bMAINPOWER ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0008;
	nSysVar->bPowerFail = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0008;
	nSysVar->bPowerFail = 0;
}
////
if ( _bGENRUN ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0010;
	nSysVar->bGenRun = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0010;
	nSysVar->bGenRun = 0;
}
////
if ( _bGENLOWFUEL ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0020;
	nSysVar->bGenLowFuel = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0020;
	nSysVar->bGenLowFuel = 0;
}
////
if ( _bGENFAIL ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0040;
	nSysVar->bGen = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0040;
	nSysVar->bGen = 0;
}
////
if ( _bUPS2FAIL ) 
{
	_pSerial->m_pIO_InBuffer[5] |= 0x0080;
	nSysVar->bUPS2 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[5] &= ~0x0080;
	nSysVar->bUPS2 = 0;
}
////
if ( _bUPS1FAIL ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0001;
	nSysVar->bUPS1 = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0001;
	nSysVar->bUPS1 = 0;
}
////
if ( _bCHARGEFAIL ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0002;
	nSysVar->bCharge = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0002;
	nSysVar->bCharge = 0;
}
////
if ( _bLOWVOLTAGE ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0004;
	nSysVar->bLowVoltage = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0004;
	nSysVar->bLowVoltage = 0;
}
////
if ( _bCHARGING ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0008;
	nSysVar->bCharging= 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0008;
	nSysVar->bCharging = 0;
}
////
if ( _bGNDFAIL ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0010;
	nSysVar->bGround = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0010;
	nSysVar->bGround = 0;
}
////
if ( _bNKWEPRN ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0020;
	nSysVar->bPKeyN = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0020;
	nSysVar->bPKeyN = 0;
}
////
if ( _bNKWEPRS ) 
{
	_pSerial->m_pIO_InBuffer[6] |= 0x0040;
	nSysVar->bPKeyS = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[6] &= ~0x0040;
	nSysVar->bPKeyS = 0;
}
////
if ( _bNKWEPRA )
{
	nSysVarExt.bPKeyA = 1;
}
else
{
	nSysVarExt.bPKeyA = 0;
}
if ( _bNKWEPRB )
{
	nSysVarExt.bPKeyB = 1;
}
else
{
	nSysVarExt.bPKeyB = 0;
}
if ( _bNKWEPRC )
{
	nSysVarExt.bPKeyC = 1;
}
else
{
	nSysVarExt.bPKeyC = 0;
}
if ( _bNKWEPRD )
{
	nSysVarExt.bPKeyD = 1;
}
else
{
	nSysVarExt.bPKeyD = 0;
}
if ( _bNKWEPRE )
{
	nSysVarExt.bPKeyE = 1;
}
else
{
	nSysVarExt.bPKeyE = 0;
}
if ( _bNKWEPRF )
{
	nSysVarExt.bPKeyF = 1;
}
else
{
	nSysVarExt.bPKeyF = 0;
}
if ( _bNKWEPRG )
{
	nSysVarExt.bPKeyG = 1;
}
else
{
	nSysVarExt.bPKeyG = 0;
}

// if ( _bDIMMER ) 
// {
// 	_pSerial->m_pIO_InBuffer[6] |= 0x0080;
// 	nSysVar->bIDayNight = 1;
// } 
// else 
// {
// 	_pSerial->m_pIO_InBuffer[6] &= ~0x0080;
// 	nSysVar->bIDayNight = 0;
// }

if ( _bLC1NKXPR ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0001;
	nSysVarExt.bLC1NKXPR = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0001;
	nSysVarExt.bLC1NKXPR = 0;
}
if ( _bLC1BELACK ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0002;
	nSysVarExt.bLC1BELACK = 1;
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0002;
	nSysVarExt.bLC1BELACK = 0;
}

// nSysVar->bMode = _bMode;
if (m_bPrevCTCReq == TRUE && nSysVar->bCTCRequest == FALSE)
{
	_bCTCRequest = FALSE;
}
nSysVar->bCTCRequest = _bCTCRequest;
nSysVarExt.bLC2NKXPR = _bLC2NKXPR;
nSysVarExt.bLC2BELACK = _bLC2BELACK;
nSysVarExt.bLC3NKXPR = _bLC3NKXPR;
nSysVarExt.bLC3BELACK = _bLC3BELACK;
nSysVarExt.bLC4NKXPR = _bLC4NKXPR;
nSysVarExt.bLC4BELACK = _bLC4BELACK;
nSysVar->bLC5NKXPR = _bLC5NKXPR;
nSysVar->bLC5BELACK = _bLC5BELACK;
nLCCVar.bLCC1Alive = _bMCCR1;
nLCCVar.bLCC2Alive = _bMCCR2;
nLCCVar.bLCC1Active = _bMCCR1Online;
nLCCVar.bLCC2Active = _bMCCR2Online;
nSysVar->bUpModem = _bBlockUp;
nSysVar->bDnModem = _bBlockDown;
nSysVar->bMidModem = _bBlockMid;
nSysVar->bFusePR = _bFUSEPR;
nLCCVar.bSIMModeSW = _bSIMModeSW;
nLCCVar.bSIMModeIO = _bSIMModeIO;
m_bPrevCTCReq = nSysVar->bCTCRequest;

////
/*
// For sylhet station.
#ifdef SYT_TEST

if ( _bNKWEPRA ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0004;

	CString PointName = "21";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
//	PointName = "101";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 1;
//	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0004;

	CString PointName = "21";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL)
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
//	PointName = "101";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 0;
//	}
}
if ( _bNKWEPRB ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0008;

	CString PointName = "32";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
//	PointName = "101";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 1;
//	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0008;

	CString PointName = "32";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
//	PointName = "101";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 0;
//	}
}
if ( _bNKWEPRC ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0010;

	CString PointName = "25";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
//	PointName = "106";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 1;
//	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0010;

	CString PointName = "25";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
//	PointName = "106";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 0;
//	}
}
if ( _bNKWEPRD ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0020;

	CString PointName = "35";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
//	PointName = "121";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 1;
//	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0020;

	CString PointName = "35";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
//	PointName = "121";
//	PointObj = SearchMySim(PointName);
//	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
//	{
//		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
//		pInfo->WKEYKR = 0;
//	}
}
if ( _bNKWEPRE ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0040;

	CString PointName = "31";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
	PointName = "125";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0040;

	CString PointName = "31";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
	PointName = "125";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
}
if ( _bNKWEPRF ) 
{
	_pSerial->m_pIO_InBuffer[7] |= 0x0080;

	CString PointName = "33";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
	PointName = "127";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[7] &= ~0x0080;

	CString PointName = "33";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
	PointName = "127";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
}
if ( _bNKWEPRG ) 
{
	_pSerial->m_pIO_InBuffer[8] |= 0x0010;

	CString PointName = "35";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
	PointName = "129";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 1;
	}
} 
else 
{
	_pSerial->m_pIO_InBuffer[8] &= ~0x0010;

	CString PointName = "35";
	CSimObject *PointObj = SearchMySim(PointName);
	if (PointObj != NULL )
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
	PointName = "129";
	PointObj = SearchMySim(PointName);
	if (PointObj != NULL)  // Sylhet 역임을 알수 있다.
	{
		ScrInfoSwitch *pInfo = (ScrInfoSwitch *)PointObj->m_pData;
		pInfo->WKEYKR = 0;
	}
}
#endif
*/
/*
	if ( _bN1 ) {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[0] / 8;
			int pos = _nSysBtnIndex[0] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[0] / 8;
			int pos = _nSysBtnIndex[0] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
	
	if ( _bN2 ) {
		if ( _nSysBtnIndex[1] > -1 ) {
			int idx = _nSysBtnIndex[1] / 8;
			int pos = _nSysBtnIndex[1] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[1] / 8;
			int pos = _nSysBtnIndex[1] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
	
	if ( _bN3 ) {
		if ( _nSysBtnIndex[2] > -1 ) {
			int idx = _nSysBtnIndex[2] / 8;
			int pos = _nSysBtnIndex[2] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[2] / 8;
			int pos = _nSysBtnIndex[2] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
	
	if ( _bBatc ) {
		if ( _nSysBtnIndex[3] > -1 ) {
			int idx = _nSysBtnIndex[3] / 8;
			int pos = _nSysBtnIndex[3] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[3] / 8;
			int pos = _nSysBtnIndex[3] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
	
	if ( _bBatd ) {
		if ( _nSysBtnIndex[4] > -1 ) {
			int idx = _nSysBtnIndex[4] / 8;
			int pos = _nSysBtnIndex[4] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[4] / 8;
			int pos = _nSysBtnIndex[4] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
	if ( _bSouth ) {
		if ( _nSysBtnIndex[5] > -1 ) {
			int idx = _nSysBtnIndex[5] / 8;
			int pos = _nSysBtnIndex[5] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[5] / 8;
			int pos = _nSysBtnIndex[5] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bNorth ) {
		if ( _nSysBtnIndex[6] > -1 ) {
			int idx = _nSysBtnIndex[6] / 8;
			int pos = _nSysBtnIndex[6] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[6] / 8;
			int pos = _nSysBtnIndex[6] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bAcr ) {
		if ( _nSysBtnIndex[7] > -1 ) {
			int idx = _nSysBtnIndex[7] / 8;
			int pos = _nSysBtnIndex[7] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[7] / 8;
			int pos = _nSysBtnIndex[7] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bInvt ) {
		if ( _nSysBtnIndex[8] > -1 ) {
			int idx = _nSysBtnIndex[8] / 8;
			int pos = _nSysBtnIndex[8] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[8] / 8;
			int pos = _nSysBtnIndex[8] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}


	if ( _bFuse ) {
		if ( _nSysBtnIndex[9] > -1 ) {
			int idx = _nSysBtnIndex[9] / 8;
			int pos = _nSysBtnIndex[9] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[9] / 8;
			int pos = _nSysBtnIndex[9] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bSysr1 ) {
		if ( _nSysBtnIndex[10] > -1 ) {
			int idx = _nSysBtnIndex[10] / 8;
			int pos = _nSysBtnIndex[10] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[10] / 8;
			int pos = _nSysBtnIndex[10] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bSysr2 ) {
		if ( _nSysBtnIndex[11] > -1 ) {
			int idx = _nSysBtnIndex[11] / 8;
			int pos = _nSysBtnIndex[11] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[11] / 8;
			int pos = _nSysBtnIndex[11] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bVpor1 ) {
		if ( _nSysBtnIndex[12] > -1 ) {
			int idx = _nSysBtnIndex[12] / 8;
			int pos = _nSysBtnIndex[12] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[12] / 8;
			int pos = _nSysBtnIndex[12] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bVpor2 ) {
		if ( _nSysBtnIndex[13] > -1 ) {
			int idx = _nSysBtnIndex[13] / 8;
			int pos = _nSysBtnIndex[13] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[13] / 8;
			int pos = _nSysBtnIndex[13] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bB24pwr ) {
		if ( _nSysBtnIndex[14] > -1 ) {
			int idx = _nSysBtnIndex[14] / 8;
			int pos = _nSysBtnIndex[14] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[14] / 8;
			int pos = _nSysBtnIndex[14] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bB24pwr1 ) {
		if ( _nSysBtnIndex[15] > -1 ) {
			int idx = _nSysBtnIndex[15] / 8;
			int pos = _nSysBtnIndex[15] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[15] / 8;
			int pos = _nSysBtnIndex[15] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bB24pwr2 ) {
		if ( _nSysBtnIndex[16] > -1 ) {
			int idx = _nSysBtnIndex[16] / 8;
			int pos = _nSysBtnIndex[16] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[16] / 8;
			int pos = _nSysBtnIndex[16] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bB24pwr3 ) {
		if ( _nSysBtnIndex[17] > -1 ) {
			int idx = _nSysBtnIndex[17] / 8;
			int pos = _nSysBtnIndex[17] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[17] / 8;
			int pos = _nSysBtnIndex[17] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bB24pwr4 ) {
		if ( _nSysBtnIndex[18] > -1 ) {
			int idx = _nSysBtnIndex[18] / 8;
			int pos = _nSysBtnIndex[18] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[18] / 8;
			int pos = _nSysBtnIndex[18] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}

	if ( _bCtcReqsw ) {
		if ( _nSysBtnIndex[19] > -1 ) {
			int idx = _nSysBtnIndex[19] / 8;
			int pos = _nSysBtnIndex[19] % 8;
			_pSerial->m_pIO_InBuffer[idx] |= 1 << pos;
		}
	}
	else {
		if ( _nSysBtnIndex[0] > -1 ) {
			int idx = _nSysBtnIndex[19] / 8;
			int pos = _nSysBtnIndex[19] % 8;
			int temp = (1 << pos) ^ 0xff;
			_pSerial->m_pIO_InBuffer[idx] &= temp;
		}
	}
}

*/
}

void CMemView::OnClose() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}
void CMemView::OnViewSysbtn2() 
{
	// TODO: Add your control notification handler code here
	if ( _bDlgLoad ) {
		m_pSysBtnDlg->SetFocus();
		return;
	}
	m_pSysBtnDlg = NULL;
	m_pSysBtnDlg = new CSysBtnDlg(_pSerial);
	m_pSysBtnDlg->Create(IDD_DIALOG_SYSTEMBUTTON);
	m_pSysBtnDlg->ShowWindow( SW_SHOW );

	_bDlgLoad = TRUE;
}

void CMemView::OnButtonCtc() 
{
	// TODO: Add your control notification handler code here
	m_CtcDlg.ShowWindow(SW_SHOW);
}
