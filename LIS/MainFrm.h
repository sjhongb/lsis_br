// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////
//#include "logdlg.h"
#include "CommEth.h"
#include "..\Include\UserDataType.h"

#define SEND_DATA 100

class CLogList;

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();
	CCommEth m_pSimulatorComm;

// Attributes
public:
	CLogList *m_pLogList;

// Operations
public:

	typedef struct {
		union {
			BYTE Card[4];
			struct {
				BYTE bPort1		:1;
				BYTE bPort2		:1;
				BYTE bPort3		:1;
				BYTE bPort4		:1;
				BYTE bPort5		:1;
				BYTE bPort6		:1;
				BYTE bPort7		:1;
				BYTE bPort8		:1;
				BYTE bPort9		:1;
				BYTE bPort10	:1;
				BYTE bPort11	:1;
				BYTE bPort12	:1;
				BYTE bPort13	:1;
				BYTE bPort14	:1;
				BYTE bPort15	:1;
				BYTE bPort16	:1;
				BYTE bPort17	:1;
				BYTE bPort18	:1;
				BYTE bPort19	:1;
				BYTE bPort20	:1;
				BYTE bPort21	:1;
				BYTE bPort22	:1;
				BYTE bPort23	:1;
				BYTE bPort24	:1;
				BYTE bPort25	:1;
				BYTE bPort26	:1;
				BYTE bPort27	:1;
				BYTE bPort28	:1;
				BYTE bPort29	:1;
				BYTE bPort30	:1;
				BYTE bPort31	:1;
				BYTE bPort32	:1;
			};
		};
	} CardType;
	
	typedef struct {
		union {
			BYTE Data[32];
			struct {
				CardType FCard[8];
			};
		};
	} FDIOM;
	
	FDIOM OUTPUT;
	FDIOM INPUT;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
//	CToolBar    m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
