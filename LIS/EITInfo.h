// EITInfo.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEITInfo document

#include "../include/strlist.h"
#include "../include/scrinfo.h"
#include "../include/iolist.h"

#include "../include/EipEmu.h"

typedef struct {
	USHORT	nID;
	CString strName;
} CTCELEM;

/////////////////////////////////////////////////////////////////////////////
typedef struct
{
	CString AspectSigCondition[5];
	CString NonAspectSigCondition[5];
	CString PointCondition[10];
	BOOL bAspectSigCondition[5];
	BOOL bNonAspectSigCondition[5];
	BOOL bPointCondition[10];
}RIAspectCondition;
typedef struct
{
	CString SigName[100];
	CString SigAspect[100];
	RIAspectCondition AspectCondition[100];
}RICondition;
/////////////////////////////////////////////////////////////////////////////

class CEITInfo : public CDocument
{
protected:
	CEITInfo();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CEITInfo)

// Attributes
public:
	void MakeDownloadFile();
	void CreateAuthorizationCode();
	CString & strDumpOfButton( BYTE nID );
	CString & strDumpOfSwitch( BYTE nID );
	CString & strDumpOfSignal( BYTE nID );
	CString & strDumpOfTrack( BYTE nID );
    CString & strDumpOfRoute( WORD wID );

    CString & strDumpOnlyID( BYTE nID );

	void TableDump(CDumpContext& dc);
	short SignalStrToTypeID( CString &str );
	void DataUpdate( WORD inout, BYTE *buffer, BOOL bIosim = FALSE );
	short MakeTrackAtSwitchTable( CString &item, BYTE *pApproach, BOOL bAlarmAdd = FALSE );
	WORD GetBitLocation( CString &str );
	BYTE GetFunctionCode( CString &str );
	void CreateEquation( CString &str );
	BYTE GetStrToSwID( CString &str );
	int m_nTableSize;
	int m_nScrMemSize;
	CEipEmu m_Engine;

    void CEITInfo::CreateCRC();

	void CreateLockTable( CString pLines[], WORD nCount, WORD &wRouteNo );
    void CreateBlockTable( CString pLines[], WORD nCount, WORD &wRouteNo );
	BOOL m_bInverseLR;
	BOOL SwitchFindAtTrack( BYTE nSwID, BYTE *pTrack, BYTE nMainTrkID );
	void SignalProc( BYTE nID, BYTE nNextID );
	void opSwitch( BYTE nSwID );
	BYTE RouteToSignal( short nRoute );
	void CreateIOInfo();
	void CreateRouteIndicatorInfo();
	void CreateScrInfo();
	void CreateCTCInfo( CMyStrList &list );
	void CreateConfigList( CMyStrList &list );

	CMyStrList m_strTrack, m_strSignal, m_strSwitch, m_strButton, m_strLamp;
	CMyStrList m_strRoute;
	CMyStrList m_ItemInfo;

	CString m_strInfoObjectSize;
	CString m_strLCName[5];
	CDumpContext *m_pDeb;

	RICondition m_RouteIndicatorInfo;

#include "../include/ROMAREA.H"

	BYTE m_pSignalDirection[ 256 ];

// IO Assign table
//	BYTE m_pIOAssignTable[4096];
//
// 인접궤도 정보  (Node = Branch + 2)
//
//      A === B === C               A,C, A-C
//
//      A === B === C				A,C,D, (B), A-C,A-D
//			  |==== D
//
//      A === B === C               A,C,D, (B), A-C,D-C
//      D ===/
//
//      A ===B1,B2 === C			A,B,C,D, (B1)(B2), A-C,D-C,A-E,D-E
//      D ==/   |== E
//
//      A === B1 === C				A,B,C,D, (B1)(B2), A-C,D-C,A-C,E-C
//      D ==B2/  
//      E ==/
//

	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장.
	UINT m_nInfoPtr;

	short m_nScrInfoCount;
	CScrInfo *m_pScrInfo;
	CIOList *m_pIOInfo;
	short m_nTrack, m_nSignal, m_nSwitch, m_nButton, m_nLamp, m_nRoute;

	CList<CTCELEM, CTCELEM> m_listElement;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEITInfo)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEITInfo();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CEITInfo)
	afx_msg void OnCreatedownfile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CEITInfo *_pEITInfo;

