// LISInfoView.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "LISInfoView.h"

#include "eitinfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLISInfoView dialog


CLISInfoView::CLISInfoView( CEITInfo *pInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CLISInfoView::IDD, pParent)
{
	m_pInfo = pInfo;
	//{{AFX_DATA_INIT(CLISInfoView)
	m_nRoute = m_pInfo->m_nRoute;
	m_nTrack = m_pInfo->m_nTrack;
	m_nSignal = m_pInfo->m_nSignal;
	m_nSwitch = m_pInfo->m_nSwitch;
	m_strInfoSize = m_pInfo->m_strInfoObjectSize;
	//}}AFX_DATA_INIT
}


void CLISInfoView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLISInfoView)
	DDX_Text(pDX, IDC_EDIT_INFO_NROUTE, m_nRoute);
	DDX_Text(pDX, IDC_EDIT_INFO_NTRACK, m_nTrack);
	DDX_Text(pDX, IDC_EDIT_INFO_NSIGNAL, m_nSignal);
	DDX_Text(pDX, IDC_EDIT_INFO_NSWITCH, m_nSwitch);
	DDX_Text(pDX, IDC_EDIT_OBJECTSIZE, m_strInfoSize);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLISInfoView, CDialog)
	//{{AFX_MSG_MAP(CLISInfoView)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLISInfoView message handlers
