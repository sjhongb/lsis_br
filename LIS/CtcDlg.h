#if !defined(AFX_CTCDLG_H__583CF533_5BC8_4ED3_AE12_FA9B7D66307F__INCLUDED_)
#define AFX_CTCDLG_H__583CF533_5BC8_4ED3_AE12_FA9B7D66307F__INCLUDED_

#include "..\INCLUDE\EipEmu.h"	// Added by ClassView
#include "..\INCLUDE\CtcEmu.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CtcDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCtcDlg dialog

class CCtcDlg : public CDialog
{
// Construction
public:
	void CreateDlg(BYTE *pCTCCmd);
	BYTE m_cCmdID;
	WORD m_wElemID;
	BYTE *m_pCTCCmd;
	void UpdateElementData(BYTE *pCtcBuffer);
	CCtcDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCtcDlg)
	enum { IDD = IDD_DIALOG_CTC };
	CEdit	m_ctrlValueEdit;
	CComboBox	m_ctrlCmdCombo;
	CListCtrl	m_ctrlElemList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCtcDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCtcDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedListElem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeComboCommand();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnButtonSend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bUpdate;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTCDLG_H__583CF533_5BC8_4ED3_AE12_FA9B7D66307F__INCLUDED_)
