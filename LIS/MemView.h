#if !defined(AFX_MEMVIEW_H__35415EC2_A3CF_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MEMVIEW_H__35415EC2_A3CF_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MemView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMemView dialog
#include "resource.h"
#include "simobj.h"
#include "MyList.h"

#include "ClientSocket.h"
#include "ListeningSocket.h"
#include "CtcDlg.h"
#include "..\INCLUDE\EipEmu.h"	// Added by ClassView

class CSysBtnDlg;	

class CSimObject;
class CMyTrack;

class CMemView : public CDialog
{
// Construction
public:
	void SetSystemButton();		
	CSysBtnDlg *m_pSysBtnDlg;
	void ViewPortValue();
	void ProcessPendingRead(CClientSocket* pSocket);
	void ProcessPendingAccept();
	CListeningSocket* m_pSocket;
	CSocket* m_connectionSocket;

	BYTE m_nSeqNo;
	void StatusView( CString &str );
// 	short CtcCommand( short nPort, short nID );
// 	short FindCtcPtr( short nID, short nType );
	void Operate(long message, long wParam, long lParam);   //(short nFunction, short nArgument);
	CMyTrack * TrackObjectFind( BYTE nTrkID );

	char m_szMsgQue[16];
	char m_MsgSequence;
	BYTE m_byteFunction;

	WORD m_ax, m_bx, m_si;
	BOOL m_bOnSim;
	void RemoveMySim();
	void RunMySim();
	CSimObject * SearchMySim( CString &str );
	CObList m_MySim;
	CObList m_MyPointSim;
	CObList m_MyTrackSim;
	CObList m_MySignalSim;
	CObList m_MyBlockSim;

	CCtcDlg	m_CtcDlg;
	BYTE m_CtcBuffer[2048];
	BYTE m_CtcCmd[4];

	void LoadRelayInfo();
	CMemView(CWnd* pParent = NULL);   // standard constructor
	~CMemView();   // standard destructor

//	BYTE m_pCode[0x10000];
//	UINT m_nCodeSize;
	
//	BYTE R_AL, R_AH, R_BL, R_BH, R_CL, R_CH, R_DL, R_DH;
	WORD *m_pCodeTable;
	UINT m_nCodePageNo;
	UINT m_nPageSize;

	WORD *m_pTimerTable;
	UINT m_nTimerCount;

	UINT m_nTimer;

	void SetOutput();
	void GetInput();
	BYTE GetFlagOfBit( int i );

// Dialog Data
	//{{AFX_DATA(CMemView)
	enum { IDD = IDD_DIALOG_LIS };
	CComboBox	m_comboSignal;
	CComboBox	m_comboTrack;
	CComboBox	m_comboSwitch;
	CComboBox	m_comboRoute;
	CMyList	m_ScrIO;
	CMyList	m_TRACK;
	CMyList m_SIGNAL;
	CMyList m_BLOCK;
	CMyList m_POINT;
	CMyList	m_LINK;
	CMyList	m_OUT;
	CMyList	m_IN;
	CString	m_CTC;
	BOOL	m_bBlock;
	short m_nTrackingDelay;
	BOOL	m_bRandom;
	CString	m_strStatus;
	int		m_nMode;
	int		m_nComPort;
	CString	m_strPortAddress;
	BOOL	m_bPortValue;
	//}}AFX_DATA

    CString m_strStatusLine[4];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMemView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HANDLE m_hEventRun;

	// Generated message map functions
	//{{AFX_MSG(CMemView)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonOper();
	afx_msg void OnButtonSys();
	afx_msg void OnClose();
	afx_msg void OnViewSysbtn2();
	afx_msg void OnButtonCtc();
	//}}AFX_MSG
	afx_msg void OnHide();
	afx_msg void OnButtonRoute();
	afx_msg void OnButtonRoutecan();
	afx_msg void OnButtonSw();
	afx_msg void OnButtonInfo();
	afx_msg void OnCheckRnd();
	afx_msg void OnButtonCpb();
	afx_msg void OnButtonTtb();
	afx_msg void OnButtonTtcb();
	afx_msg void OnButtonCssb();
	afx_msg void OnRadioMode();
	afx_msg void OnRadioIosim();
	afx_msg void OnRadioCtc();
	afx_msg void OnButtonSigdest();
	afx_msg void OnSelchangeComboCom();
	DECLARE_MESSAGE_MAP()
public:
	BOOL m_bPrevCTCReq;
	void OnViewIolist();

};

#define SIMMODE_SIM     0
#define SIMMODE_IOSIM   1
#define SIMMODE_CTC     2

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEMVIEW_H__35415EC2_A3CF_11D2_BA70_00C06C003C49__INCLUDED_)
