// LISDoc.cpp : implementation of the CLISDoc class
//

#include "stdafx.h"
#include "LIS.h"

#include "LISDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Global variables

/////////////////////////////////////////////////////////////////////////////
// Global variables

char _StationName[80];
char _MetrixMode[20];
char _HStationName[80];
char *GetHStationName() {return &_HStationName[0];}

//char StationName[80];
//char HStationName[80];
//char *GetHStationName() {return &HStationName[0];}

/////////////////////////////////////////////////////////////////////////////
// CLISDoc

IMPLEMENT_DYNCREATE(CLISDoc, CDocument)

BEGIN_MESSAGE_MAP(CLISDoc, CDocument)
	//{{AFX_MSG_MAP(CLISDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLISDoc construction/destruction

CLISDoc::CLISDoc()
{
	// TODO: add one-time construction code here
}

CLISDoc::~CLISDoc()
{
}

BOOL CLISDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CLISDoc serialization

void CLISDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}

	// Calling the base class COleDocument enables serialization
	//  of the container document's COleClientItem objects.
//	COleDocument::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CLISDoc diagnostics

#ifdef _DEBUG
void CLISDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLISDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLISDoc commands


BOOL CLISDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	// TODO: Add your specialized code here and/or call the base class
	return FALSE;
	return CDocument::CanCloseFrame(pFrame);
}
