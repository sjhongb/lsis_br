// CtcDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "EITInfo.h"
#include "CtcDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CEITInfo *_pEITInfo;

/////////////////////////////////////////////////////////////////////////////
// CCtcDlg dialog


CCtcDlg::CCtcDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCtcDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCtcDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bUpdate = FALSE;
	m_cCmdID = 0x00;
	m_wElemID = 0x0000;
}


void CCtcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCtcDlg)
	DDX_Control(pDX, IDC_EDIT_VALUE, m_ctrlValueEdit);
	DDX_Control(pDX, IDC_COMBO_COMMAND, m_ctrlCmdCombo);
	DDX_Control(pDX, IDC_LIST_ELEM, m_ctrlElemList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCtcDlg, CDialog)
	//{{AFX_MSG_MAP(CCtcDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_ELEM, OnItemchangedListElem)
	ON_CBN_SELCHANGE(IDC_COMBO_COMMAND, OnSelchangeComboCommand)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCtcDlg message handlers

BOOL CCtcDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_ctrlCmdCombo.AddString("0x21 Signal | SLS ON");
	m_ctrlCmdCombo.AddString("0x22 Signal | SLS OFF");
	m_ctrlCmdCombo.AddString("0x23 Signal | Day");
	m_ctrlCmdCombo.AddString("0x24 Signal | Night");
	m_ctrlCmdCombo.AddString("0x41 Block | CA");
	m_ctrlCmdCombo.AddString("0x42 Block | LCR/LCG");
	m_ctrlCmdCombo.AddString("0x43 Block | BCB/BRB");
	m_ctrlCmdCombo.AddString("0x51 Point M. | Normal");
	m_ctrlCmdCombo.AddString("0x52 Point M. | Reverse");
	m_ctrlCmdCombo.AddString("0x53 Point M. | Block Set");
	m_ctrlCmdCombo.AddString("0x54 Point M. | Block Rel.");
	m_ctrlCmdCombo.AddString("0x61 Signal | Stop");
	m_ctrlCmdCombo.AddString("0x62 Signal | Return");
	m_ctrlCmdCombo.AddString("0x71 Route | Main Set");
	m_ctrlCmdCombo.AddString("0x72 Route | Main Rel.");
	m_ctrlCmdCombo.AddString("0x73 Route | Shunt Set");
	m_ctrlCmdCombo.AddString("0x74 Route | Shunt Rel.");
	m_ctrlCmdCombo.AddString("0x75 Route | Call-on Set");
	m_ctrlCmdCombo.AddString("0x76 Route | Call-on Rel.");
	m_ctrlCmdCombo.AddString("0x81 Track | LOS ON");
	m_ctrlCmdCombo.AddString("0x82 Track | LOS OFF");
	m_ctrlCmdCombo.AddString("0x91 L/C | Call");
	m_ctrlCmdCombo.AddString("0xa1 OSS | Stop");
	m_ctrlCmdCombo.AddString("0xa2 OSS | Return");

	LV_COLUMN lvcol;
	char *colName[4] = {"ID","NAME","HEX","BITS"};
    int  colWidth[4] = { 30,150,40,80 };
	
    for(int i = 0; i < 4; i++)
    {
        lvcol.mask	= LVCF_FMT|LVCF_SUBITEM|LVCF_TEXT|LVCF_WIDTH;
        lvcol.fmt	= LVCFMT_LEFT;
        lvcol.pszText	= colName[i];
        lvcol.cx		= colWidth[i];

        m_ctrlElemList.InsertColumn(i, &lvcol);
    }
	
	DWORD style = LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_ONECLICKACTIVATE; 
	m_ctrlElemList.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LPARAM(style));

	i = 0;
	LV_ITEM lvitem;
	int nItem;
	CString strIdx;
	CTCELEM elem;
	POSITION pos = _pEITInfo->m_listElement.GetHeadPosition();
	while(pos != NULL)
	{
		elem = _pEITInfo->m_listElement.GetNext(pos);

		strIdx.Format("%d", elem.nID);

		lvitem.mask = LVIF_TEXT;
		lvitem.iItem = i++;
		lvitem.iSubItem = 0;
		lvitem.pszText = (char*)(LPCTSTR)strIdx;

		nItem = m_ctrlElemList.InsertItem(&lvitem);
		m_ctrlElemList.SetItemText(nItem, 1, (LPCTSTR)elem.strName);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCtcDlg::UpdateElementData(BYTE *pCtcBuffer)
{
	if(!m_bUpdate)
	{
		return;
	}

	BYTE cHex;
	CString strHex,strBits;

	WORD wCount = *(WORD*)pCtcBuffer;
	pCtcBuffer += 2;

	for(int i = 0; i < wCount ; i++)
	{
		cHex = *pCtcBuffer++;

		strHex.Format("%02X", cHex);
		strBits.Format("%d%d%d%d %d%d%d%d", (cHex>>7 & 1), (cHex>>6 & 1), (cHex>>5 & 1), (cHex>>4 & 1), (cHex>>3 & 1), (cHex>>2 & 1), (cHex>>1 & 1), (cHex & 1));

		m_ctrlElemList.SetItemText(i, 2, strHex);
		m_ctrlElemList.SetItemText(i, 3, strBits);
	}
}

void CCtcDlg::OnItemchangedListElem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	if ((pNMListView->uChanged & LVIF_STATE) && (pNMListView->uNewState & LVNI_SELECTED))
	{
		CString strElemID;
		strElemID = m_ctrlElemList.GetItemText(pNMListView->iItem, 0);

		m_wElemID = (WORD)atoi(strElemID);

		strElemID.Format("0x%04X", m_wElemID);
		m_ctrlValueEdit.SetWindowText(strElemID);
	}
	
	*pResult = 0;
}

void CCtcDlg::OnSelchangeComboCommand() 
{
	// TODO: Add your control notification handler code here
	CString strCmd;
	int i = m_ctrlCmdCombo.GetCurSel();
	m_ctrlCmdCombo.GetLBText(i, strCmd);

	strCmd = strCmd.Mid(2, 2);

	char *strStop;
	m_cCmdID = (BYTE)strtol(strCmd, &strStop, 16);
}

void CCtcDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	if(bShow)
	{
		m_bUpdate = TRUE;
	}
	else
	{
		m_bUpdate = FALSE;
	}
	
}

void CCtcDlg::OnButtonSend() 
{
	// TODO: Add your control notification handler code here
	*(WORD*)m_pCTCCmd	= m_wElemID;
	m_pCTCCmd[2]		= m_cCmdID;
}

void CCtcDlg::CreateDlg(BYTE *pCTCCmd)
{
	m_pCTCCmd = pCTCCmd;

	Create(IDD_DIALOG_CTC);
}
