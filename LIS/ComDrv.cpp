/////////////////////////////////////////////////////////////////////////////
//
//	Copyright (c) 2004 < (주) LG산전 >
//
//	Module Name:
//		ComDrv.cpp
//
//	Abstract:
//		IOS 시리얼 통신 관련
//
//	Author:
//		<omani> (xxxx@lgis.com) 2004.4.9
//
//	Revision History:
//
//	Notes:
//
/////////////////////////////////////////////////////////////////////////////

// ErcCom.cpp : implementation file
//

#include "stdafx.h"
#include "ComDrv.h"
#include "memview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommDriver

IMPLEMENT_DYNCREATE(CCommDriver, CWinThread)

CCommDriver::CCommDriver()
{
	m_nTxBuffSize	= 2048;
	m_nrxptr		= 0;
	m_nrxcap		= 0;
	m_nrxlen		= 0;
	m_nrxcrc		= 0;
	m_nrxcnt		= 0;
	m_nIsDLE		= 0;
	m_pClient		= NULL;
    m_bCheckCTS		= FALSE;        // IOSIM = FALSE,
    m_nMode			= 0;
    m_idComDev		= (HANDLE) -1;
    m_bExit			= FALSE;
    m_bOpenComm		= FALSE;
    m_bExitDone		= FALSE;
    m_bAutoDelete	= FALSE;
}

CCommDriver::~CCommDriver()
{
	if (m_bOpenComm) CloseModem();

	if (m_pReadBuff) delete [] m_pReadBuff;
	if (m_pTempBuff) delete [] m_pTempBuff;
}

int _PortNo = 0;
#define bfsize 4096
BOOL CCommDriver::InitInstance()
{
	m_pReadBuff = new unsigned char[bfsize];
	memset(m_pReadBuff, '\0',bfsize);
	
	m_pTempBuff = new unsigned char[bfsize<<1];

//            if ( m_nMode == 1 /*SIMMODE_IOSIM*/ ) {
    OpenModem();

    return TRUE;
}

BOOL CCommDriver::OpenModem()
{
    if (m_nMode == 0)		// SIM MODE
		return FALSE;  

    else if (m_nMode == 1)	// IOSIM MODE
	{
    	//m_dwBaudRate		= CBR_115200;
    	m_dwBaudRate		= CBR_38400;
    }
    else if (m_nMode == 2)	// CTCSIM MODE
	{
    	m_dwBaudRate		= CBR_9600;
        m_bCheckCTS = TRUE;        // CTC
    }

    if ( m_bOpenComm ) 
		CloseModem();

 	m_idComDev			= (HANDLE) -1 ;
	m_bOpenComm			= FALSE ;
	m_fLocalEcho		= FALSE ;
	m_fAutoWrap			= TRUE ;


	m_bPort				= _PortNo;   // Port Number Assign....

//	m_dwBaudRate		= CBR_115200;
//	m_dwBaudRate		= CBR_128000;
//CBR_115200
//CBR_128000
	m_bByteSize			= 8 ;
	m_bFlowCtrl			= 0; //FC_XONXOFF ;
//	m_bFlowCtrl = FC_RTSCTS;
	m_bParity			= NOPARITY ;
	m_bStopBits			= TWOSTOPBITS ;
	m_fXonXoff			= FALSE ;

	m_osWrite.Offset	= 0 ;
	m_osWrite.OffsetHigh = 0 ;
	m_osRead.Offset		= 0 ;
	m_osRead.OffsetHigh	= 0 ;

   // create I/O event used for overlapped reads / writes

   m_osRead.hEvent = CreateEvent( NULL,    // no security
                                  TRUE,    // explicit reset req
                                  FALSE,   // initial event reset
                                  NULL ) ; // no name
   if (m_osRead.hEvent == NULL)
   {
//      LocalFree( npTTYInfo ) ;
//      return ( -1 ) ;
	   return FALSE;
   }

   m_osWrite.hEvent = CreateEvent( NULL,    // no security
                                   TRUE,    // explicit reset req
                                   FALSE,   // initial event reset
                                   NULL ) ; // no name
   if (NULL == m_osWrite.hEvent)
   {
//      CloseHandle( m_osWrite.hEvent ) ;
//      LocalFree( npTTYInfo ) ;
//      return ( -1 ) ;
	   return FALSE;
   }

//   LoadInfo();
//	if (OpenModem()) _Comm1 = this;
	BOOL			fRetVal;
//	HANDLE			hCommWatchThread;
	COMMTIMEOUTS 	CommTimeOuts;

	wsprintf( m_szPort,"\\\\.\\COM%d", m_bPort + 1);


// CommPort Open
	if ((m_idComDev = CreateFile(m_szPort,                    // Device file name (COM4 이상은  \\.\COM5)
								GENERIC_READ | GENERIC_WRITE, // file access mode
								0,                            // share mode
								NULL,                         // no security attrs
								OPEN_EXISTING,                // Creation method
								//FILE_ATTRIBUTE_NORMAL |     // Flags and Attributes
								FILE_FLAG_NO_BUFFERING |
								FILE_ATTRIBUTE_ARCHIVE |
							//	FILE_ATTRIBUTE_SYSTEM |
								FILE_ATTRIBUTE_COMPRESSED,    
							//	FILE_FLAG_OVERLAPPED, // overlapped I/O
								NULL) ) == (HANDLE) -1 )

//  if((m_idComDev  = fopen("COM3","w+b")) == (HANDLE) -1)
        return FALSE;

	else
	{
      // get any early notifications
		SetCommMask( m_idComDev, EV_RXCHAR |EV_RING ) ;             // COM Monitor event setup

	  // setup device buffers
		SetupComm( m_idComDev, m_nTxBuffSize, m_nTxBuffSize);		// ===========  Setup I/ O Buffer

      // purge any information in the buffer
		PurgeComm( m_idComDev, PURGE_TXABORT | PURGE_RXABORT |
							  PURGE_TXCLEAR | PURGE_RXCLEAR ) ;

      // set up for overlapped I/O
		CommTimeOuts.ReadIntervalTimeout			= 10;	// 10 ms;  0xFFFFFFFF ;
		CommTimeOuts.ReadTotalTimeoutMultiplier		= 1;
		CommTimeOuts.ReadTotalTimeoutConstant		= 10000 ;
		CommTimeOuts.WriteTotalTimeoutMultiplier	= 0 ;
		CommTimeOuts.WriteTotalTimeoutConstant		= 0 ;	//1000 ;
		SetCommTimeouts( m_idComDev, &CommTimeOuts ) ;
	}


	fRetVal = SetupDCB() ;
//    fRetVal = 0;

	if (fRetVal) 
	{
		m_bOpenComm = TRUE ;
//		if (NULL == (hCommWatchThread = CreateThread(NULL, 0, &(::CommWatchProc),0,0,&dwThreadID)))	{
//			m_bOpenComm = FALSE ;
//			CloseHandle( m_idComDev ) ;
//			fRetVal = FALSE ;
//		}
//		else {
//			m_dwThreadID = dwThreadID ;
//			m_hWatchThread = hCommWatchThread ;
			EscapeCommFunction( m_idComDev, SETDTR );
//		}

	    if (!SetCommMask( m_idComDev, EV_RXCHAR))
		    return FALSE;

	}
	else 
	{
		m_bOpenComm = FALSE ;
		CloseHandle( m_idComDev ) ;
	}
//    BlockLen = 4 * 4 + 6;

	return fRetVal;
}

int CCommDriver::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	if (m_bOpenComm) CloseModem();

	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCommDriver, CWinThread)
	//{{AFX_MSG_MAP(CCommDriver)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommDriver message handlers

BOOL CCommDriver::CloseModem()
{
	m_bOpenComm = FALSE ;

   // disable event notification and wait for thread to halt
	SetCommMask( m_idComDev, 0 ) ;
   // block until thread has been halted
//	while(m_dwThreadID != 0);
   // drop DTR
	EscapeCommFunction( m_idComDev, CLRDTR ) ;

   // purge any outstanding reads/writes and close device handle
	PurgeComm( m_idComDev, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR );


	CloseHandle( m_idComDev );
    CloseHandle( m_osRead.hEvent ) ;
    CloseHandle( m_osWrite.hEvent ) ;


//	m_bIsConnect = FALSE;

//    npTelinfo->dwThreadID = 0 ;
//    if (npTelinfo->hWatchThread) TerminateThread(npTelinfo->hWatchThread,0);
//	npTelinfo->hWatchThread = NULL ;
    return TRUE;
}

BOOL CCommDriver::SetupDCB()
{
	BOOL       fRetVal ;
	BYTE       bSet ;
	DCB        dcb ;

	dcb.DCBlength = sizeof( DCB ) ;

	GetCommState( m_idComDev, &dcb ) ;

	dcb.BaudRate = m_dwBaudRate ;
	dcb.ByteSize = m_bByteSize ;
	dcb.Parity = m_bParity ;
	dcb.StopBits = m_bStopBits ;

   // setup hardware flow control

	bSet = (BYTE) ((m_bFlowCtrl & FC_DTRDSR) != 0) ;
	dcb.fOutxDsrFlow = bSet ;
	if (bSet)
		dcb.fDtrControl = DTR_CONTROL_HANDSHAKE ;
	else
		dcb.fDtrControl = DTR_CONTROL_ENABLE ;


	bSet = (BYTE) ((m_bFlowCtrl & FC_RTSCTS) != 0) ;
	dcb.fOutxCtsFlow = bSet ;
	if (bSet)
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE ;
	else
		dcb.fRtsControl = RTS_CONTROL_ENABLE ;


   // setup software flow control
/*
	bSet = (BYTE) ((m_bFlowCtrl & FC_XONXOFF) != 0) ;

	dcb.fInX = dcb.fOutX = bSet ;
	dcb.XonChar = ASCII_XON ;
	dcb.XoffChar = ASCII_XOFF ;
	dcb.XonLim = 100 ;
	dcb.XoffLim = 100 ;
*/
	dcb.fInX = dcb.fOutX = FALSE ;

	// other various settings
	dcb.fBinary = TRUE ;
	dcb.fParity = TRUE ;

	fRetVal = SetCommState( m_idComDev, &dcb ) ;

	return ( fRetVal ) ;
}

/*
BOOL CCommDriver::WriteCommData(unsigned char* lpBuffer , DWORD dwBytesToWrite)
{
	BOOL        fWriteStat ;
	DWORD       dwBytesWritten ;
	//DWORD       dwErrorFlags;
	DWORD   	dwError;
	COMSTAT     ComStat;

	fWriteStat = WriteFile( m_idComDev, (unsigned char*)lpBuffer, dwBytesToWrite,
	                       &dwBytesWritten, NULL);//&(m_osWrite) ) ;
	return TRUE;
}
*/

DWORD CCommDriver::ReadCommData(unsigned char* lpBuffer, DWORD nMaxLength )
{
	BOOL       fReadStat ;
	DWORD      dwLength;
//	DWORD	   temp;
	COMSTAT		ComStat;
	static UINT LengthSum = 0;
	DWORD		dwErrorFlags;

	int count = 0;


	ClearCommError(m_idComDev, &dwErrorFlags, &ComStat);
//	dwLength = min((DWORD)nMaxLength, ComStat.cbInQue);
	dwLength = (DWORD)nMaxLength;

	if (dwLength > 0)
	{
		fReadStat = ReadFile( m_idComDev, (unsigned char*)lpBuffer, dwLength, &dwLength, NULL); //&m_osRead);
		/*LengthSum +=dwLength;
		if(LengthSum > MAX_TRANS_BUFF_SIZE)
			PurgeComm( m_idComDev,PURGE_RXCLEAR ) ;
		  */
	}

    return dwLength;
}

/*
 *  Crc calculation stuff
 */


//CRC CCITT
unsigned short CRCtab[256] = {
	0x0000,	0x1189,	0x2312,	0x329b,	0x4624,	0x57ad,	0x6536,	0x74bf,
	0x8c48,	0x9dc1,	0xaf5a,	0xbed3,	0xca6c,	0xdbe5,	0xe97e,	0xf8f7,
	0x1081,	0x0108,	0x3393,	0x221a,	0x56a5,	0x472c,	0x75b7,	0x643e,
	0x9cc9,	0x8d40,	0xbfdb,	0xae52,	0xdaed,	0xcb64,	0xf9ff,	0xe876,
	0x2102,	0x308b,	0x0210,	0x1399,	0x6726,	0x76af,	0x4434,	0x55bd,
	0xad4a,	0xbcc3,	0x8e58,	0x9fd1,	0xeb6e,	0xfae7,	0xc87c,	0xd9f5,
	0x3183,	0x200a,	0x1291,	0x0318,	0x77a7,	0x662e,	0x54b5,	0x453c,
	0xbdcb,	0xac42,	0x9ed9,	0x8f50,	0xfbef,	0xea66,	0xd8fd,	0xc974,
	0x4204,	0x538d,	0x6116,	0x709f,	0x0420,	0x15a9,	0x2732,	0x36bb,
	0xce4c,	0xdfc5,	0xed5e,	0xfcd7,	0x8868,	0x99e1,	0xab7a,	0xbaf3,
	0x5285,	0x430c,	0x7197,	0x601e,	0x14a1,	0x0528,	0x37b3,	0x263a,
	0xdecd,	0xcf44,	0xfddf,	0xec56,	0x98e9,	0x8960,	0xbbfb,	0xaa72,
	0x6306,	0x728f,	0x4014,	0x519d,	0x2522,	0x34ab,	0x0630,	0x17b9,
	0xef4e,	0xfec7,	0xcc5c,	0xddd5,	0xa96a,	0xb8e3,	0x8a78,	0x9bf1,
	0x7387,	0x620e,	0x5095,	0x411c,	0x35a3,	0x242a,	0x16b1,	0x0738,
	0xffcf,	0xee46,	0xdcdd,	0xcd54,	0xb9eb,	0xa862,	0x9af9,	0x8b70,
	0x8408,	0x9581,	0xa71a,	0xb693,	0xc22c,	0xd3a5,	0xe13e,	0xf0b7,
	0x0840,	0x19c9,	0x2b52,	0x3adb,	0x4e64,	0x5fed,	0x6d76,	0x7cff,
	0x9489,	0x8500,	0xb79b,	0xa612,	0xd2ad,	0xc324,	0xf1bf,	0xe036,
	0x18c1,	0x0948,	0x3bd3,	0x2a5a,	0x5ee5,	0x4f6c,	0x7df7,	0x6c7e,
	0xa50a,	0xb483,	0x8618,	0x9791,	0xe32e,	0xf2a7,	0xc03c,	0xd1b5,
	0x2942,	0x38cb,	0x0a50,	0x1bd9,	0x6f66,	0x7eef,	0x4c74,	0x5dfd,
	0xb58b,	0xa402,	0x9699,	0x8710,	0xf3af,	0xe226,	0xd0bd,	0xc134,
	0x39c3,	0x284a,	0x1ad1,	0x0b58,	0x7fe7,	0x6e6e,	0x5cf5,	0x4d7c,
	0xc60c,	0xd785,	0xe51e,	0xf497,	0x8028,	0x91a1,	0xa33a,	0xb2b3,
	0x4a44,	0x5bcd,	0x6956,	0x78df,	0x0c60,	0x1de9,	0x2f72,	0x3efb,
	0xd68d,	0xc704,	0xf59f,	0xe416,	0x90a9,	0x8120,	0xb3bb,	0xa232,
	0x5ac5,	0x4b4c,	0x79d7,	0x685e,	0x1ce1,	0x0d68,	0x3ff3,	0x2e7a,
	0xe70e,	0xf687,	0xc41c,	0xd595,	0xa12a,	0xb0a3,	0x8238,	0x93b1,
	0x6b46,	0x7acf,	0x4854,	0x59dd,	0x2d62,	0x3ceb,	0x0e70,	0x1ff9,
	0xf78f,	0xe606,	0xd49d,	0xc514,	0xb1ab,	0xa022,	0x92b9,	0x8330,
	0x7bc7,	0x6a4e,	0x58d5,	0x495c,	0x3de3,	0x2c6a,	0x1ef1,	0x0f78
};

/*
 * updcrc macro derived from article Copyright (C) 1986 Stephen Satchell.
 *  NOTE: First srgument must be in range 0 to 255.
 *        Second argument is referenced twice.
 *
 * Programmers may incorporate any or all code into their programs,
 * giving proper credit within the source. Publication of the
 * source routines is permitted so long as proper credit is given
 * to Stephen Satchell, Satchell Evaluations and Chuck Forsberg,
 * Omen Technology.
 */

#define updcrc(cp, crc) ( crc16tab[((crc >> 8) & 255)] ^ (crc << 8) ^ cp)

#define crc_CCITT( c, s ) ( CRCtab[(s^c)&0xFF] ^ s>>8 )

BYTE RxImage[2148];

int CheckCount = 0;

#define RXMAXSIZE 1024+10
#define MSTX ((unsigned char)0x7E)
#define METX ((unsigned char)0xFE)
unsigned char debbf[512];


void CCommDriver::RxProc( unsigned int &size ) {
//	SymbolInfo = SaveInfo;
//	CheckCount = 0;
	BYTE *bf = m_pReadBuff;

	int l = size;
	if (l > 510) l = 510;
	memset(debbf,0,512);
	memcpy(debbf,bf,l);

	WORD nCrc = 0;
	while (size > 36 && !(*bf == MSTX) /*(bf+36) == METX*/ ) {
		bf++;
		size--;
	}
	m_pReadBuff = bf;

	int error = 1;
	if (*bf == MSTX && *(bf+1) == 'D' && size > 35) {
		error = 2;
		bf++;
		BYTE c;
		for (UINT i=1; i < 34; i++) {
			c = *bf++;
			nCrc = crc_CCITT( c, nCrc );
		}
		c = *bf++;
		nCrc = crc_CCITT( c, nCrc );
		c = *bf++;
		nCrc = crc_CCITT( c, nCrc );

		if (nCrc == 0) {
			memcpy( m_pRxBuffer, m_pReadBuff + 2, 32 );
			m_bRxReady = TRUE;
			error = 0;
		}
	}

	if ( error ) {
		bf = m_pReadBuff;
	}

	bf = m_pReadBuff;



	unsigned char c;
	RxImage[ m_nrxptr++ ] = 0;
	while (size) {
		c = *bf++;
		size--;
		RxImage[ m_nrxptr++ ] = c;
		if ( m_nrxptr > 100 ) {
			m_nrxptr = 0;
		}
	}
}

//BYTE pSW[4] = { 0x80, 0x80, 0x80, 0x80 };
int CCommDriver::Run() 
{
	ASSERT_VALID(this);

	// for tracking the idle time state
	BOOL bIdle = TRUE;
	LONG lIdleCount = 0;

	// acquire and dispatch messages until a WM_QUIT message is received.
	for (;;)
	{
		// phase1: check to see if we can do idle work
		while ( !m_bExit ) 
		{
			while (bIdle &&
				!::PeekMessage(&m_msgCur, NULL, NULL, NULL, PM_NOREMOVE))
			{
				// call OnIdle while in bIdle state
				if (!OnIdle(lIdleCount++))
					bIdle = FALSE; // assume "no idle" state
			}
			WaitSerial();
		}

		m_bExitDone = TRUE;
		return FALSE;


		return ExitInstance();

		// phase2: pump messages while available
		do
		{
			// pump message, but quit on WM_QUIT
			if (!PumpMessage())
				return ExitInstance();

			// reset "no idle" state after pumping "normal" message
			if (IsIdleMessage(&m_msgCur))
			{
				bIdle = TRUE;
				lIdleCount = 0;
			}

		} while (::PeekMessage(&m_msgCur, NULL, NULL, NULL, PM_NOREMOVE));

	}

	ASSERT(FALSE);  // not reachable
}

/*
while ( bDoingBackgroundProcessing ) 
{ 
    MSG msg;
    while ( ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) ) 
    { 
        if ( !PumpMessage( ) ) 
        { 
            bDoingBackgroundProcessing = FALSE; 
            ::PostQuitMessage( ); 
            break; 
        } 
    } 
    // let MFC do its idle processing
    LONG lIdle = 0;
    while ( AfxGetApp()->OnIdle(lIdle++ ) )
        ;  
    // Perform some background processing here 
    // using another call to OnIdle
}
*/  

#define SIMIORXBLKSIZE 256
#define SIMIORXBLKSIZE_S 32

// 통신이 발생하는 부분...
extern CMemView * _pCMemView;		
int CCommDriver::WaitSerial() 
{
	DWORD       dwEvtMask ;
//	OVERLAPPED  os ;
	unsigned int  nLength;
	unsigned int  DataSize=0;
//	unsigned char TempBuffSize[5];

	unsigned int	str_len = 0;
//	char		BuffFlag;
	unsigned char* pdest=NULL;
	unsigned int	TempSize=0;
	unsigned int	CmpSize=0;
	unsigned int	i=0;


	int ptr=0;
	int tempDataSize=0;
	int HdrSearch=0;

    if ( _pCMemView ) 
	{
		_pCMemView->SetSystemButton();		
	}


    if ( m_bOpenComm ) 
	{

//	    if (!SetCommMask( m_idComDev, EV_RXCHAR))
//		    return FALSE;
//	    m_bIsConnect = TRUE;

	    memset( m_pRxBuffer, 0, 32 );
	    memset( m_pTxBuffer, 0, 32 );

		// 송수신 Buffer 초기화...
	    memset( m_pIO_InBuffer, 0, 256 );
	    memset( m_pIO_OutBuffer, 0, 256 );

	    char pCapBf[2048];
	    int nCapPos = 0;

	    m_bRxReady = 1;
	    while ( m_bOpenComm )
	    {
		    dwEvtMask = 0 ;
		    PurgeComm( m_idComDev, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR ) ;

		    WaitCommEvent( m_idComDev, &dwEvtMask, NULL ); // Data receive Event를 기다린다...

		    if (dwEvtMask & EV_RXCHAR)
		    {
				//////////////////////////////////////////////////////////////////////////////////////////
				//  For compile IO Simulation
				//////////////////////////////////////////////////////////////////////////////////////////
                if ( m_nMode == 1 /*SIMMODE_IOSIM*/ ) 
				{
			        nLength = ReadCommData((m_pReadBuff + str_len), 1024 );
					char dbf[512];
					int l = str_len;
					if ( l > 500 ) 
						l = 500;

					memcpy( dbf, m_pReadBuff, l );

			        str_len += nLength;
			        if ( str_len ) 
					{
				        char *p = (char*)m_pReadBuff;

				        while ( m_bOpenComm && str_len ) 
						{
					        char c = *p++;
					        int i;
					        str_len--;
					        pCapBf[ nCapPos++ ] = c;

					        if ( nCapPos > 3 ) 
							{
						        if (nCapPos == SIMIORXBLKSIZE+6) 
								{
							        if (!memcmp( pCapBf+SIMIORXBLKSIZE+3, "END", 3 )) 
									{
								        memcpy( m_pIO_OutBuffer, pCapBf+3, SIMIORXBLKSIZE );  // 받은 Data를 OutBuffer에 저장..
								        m_bRxReady = 1;
								        
								        char bf[SIMIORXBLKSIZE+6];

								        bf[0] = 'S';  // Start Position...


										if ( _pCMemView ) 
										{
											_pCMemView->SetSystemButton();		
										}

								        memcpy( bf+1, m_pIO_InBuffer, SIMIORXBLKSIZE_S+6 );  // 전송 할 Data를 구성한다..

										WORD nCrc = 0;
										BYTE c;
										for (i=0; i<SIMIORXBLKSIZE_S+1; i++) 
										{
											c = bf[i];
											nCrc = crc_CCITT( c, nCrc );
										}
										*(WORD *)&bf[SIMIORXBLKSIZE_S+1] = nCrc;

										WriteBlock( bf, SIMIORXBLKSIZE_S+3 );   // IPM으로 Data를 전송한다....

										nCapPos = 0;
									}

									else // END를 만날 때까지 Read한 Data를 계속 모은다...
									{
										for (i=3; i<SIMIORXBLKSIZE+3; i++) 
										{
											if (!memcmp( pCapBf+i, "STX", 3)) 
												break;
										}
										nCapPos -= i;
									}
									nCapPos = 0;
								}
							}

							else if ( nCapPos == 3 )  // 첫 Data...
							{
								if (memcmp( pCapBf, "STX", 3 )) 
								{
									nCapPos = 0;
								}
							}

							else 
							{
								if (pCapBf[0] != 'S') 
								{
									memcpy(pCapBf, pCapBf+1, nCapPos);
									nCapPos--;
								}
							}
						}
						
					}
						else 
						{
							str_len = 0;
						}
					}
    //////////////////////////////////////////////////////////////////////////////////////////
    //  For compile IO Simulation end
    //////////////////////////////////////////////////////////////////////////////////////////
    //			Sleep(5);

				else 
				{
			    }
		    }
	    }

    }
	return TRUE;
}


int CCommDriver::WriteBlock( char *lpBuffer, int dwBytesToWrite )
{
	BOOL        fWriteStat ;
	DWORD       dwBytesWritten ;
//	DWORD       dwErrorFlags;
//	DWORD   	dwError;
//	COMSTAT     ComStat;
//	char        szError[ 10 ] ;

	EscapeCommFunction( m_idComDev, SETRTS ) ;


//	fWriteStat = WriteFile( m_idComDev, (unsigned char*)lpBuffer, dwBytesToWrite,
//	                       &dwBytesWritten, NULL);//&(m_osWrite) ) ;

	int size;              
	while ( dwBytesToWrite ) // Write해야 할 Data가 존재하면...
	{
		if ( m_bCheckCTS ) 
		{
			DWORD nModemStatus = 0;
			while ( !(nModemStatus & MS_CTS_ON) ) 
			{
				BOOL bResult = GetCommModemStatus( m_idComDev, &nModemStatus );

				if (!bResult) 
					nModemStatus = 0;
			}
		}

		size = dwBytesToWrite;
//		if (size > 64) size = 64;
		fWriteStat = WriteFile( m_idComDev, (unsigned char*)lpBuffer, size,
	                       &dwBytesWritten, NULL);//&(m_osWrite) ) ;

		size = dwBytesWritten;
		lpBuffer += size;
		dwBytesToWrite -= size;
	}

	EscapeCommFunction( m_idComDev, CLRRTS ) ;
	EscapeCommFunction( m_idComDev, CLRRTS ) ;

	return 0;
}


CString CCommDriver::NameOf()
{
	CString strName = "COMM";
	return strName;
}

