// ListeningSocket.h : header file
//

#include <Afxsock.h>
/////////////////////////////////////////////////////////////////////////////
// CListeningSocket command target

#ifndef __LSTNSOCK_H__
#define __LSTNSOCK_H__

class CMemView;

class CListeningSocket : public CSocket
{
	DECLARE_DYNAMIC(CListeningSocket);
// Attributes
public:

// Operations
public:
	CListeningSocket( CMemView *pServer );
	virtual ~CListeningSocket();

	CMemView *m_pServer;
// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListeningSocket)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CListeningSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////
#endif