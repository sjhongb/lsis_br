// SysBtnDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LIS.h"
#include "SysBtnDlg.h"
#include "comdrv.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSysBtnDlg dialog

/////////////////////////////////////////////////////////////////////////////
// CSysBtnDlg message handlers
extern CCommDriver *_pSerial;
extern int _nSysBtnIndex[];
extern int _nSysCount;

extern BOOL _bSIMModeSW;
extern BOOL _bSIMModeIO;
extern BOOL	_bACTIVEON1;
extern BOOL	_bACTIVEON2;
extern BOOL	_bVPOR1;
extern BOOL	_bVPOR2;
extern BOOL	_bFUSE1;
extern BOOL	_bFUSE2;
extern BOOL	_bFUSE3;
extern BOOL	_bFUSE4;
extern BOOL _bFUSEPR;
extern BOOL	_bFAN1;
extern BOOL	_bFAN2;
extern BOOL	_bFAN3;
extern BOOL	_bFAN4;
extern BOOL	_b24VINT;
extern BOOL	_b24VNEXT;
extern BOOL	_b24VSEXT;
extern BOOL	_bMAINPOWER;
extern BOOL	_bGENRUN;
extern BOOL	_bGENLOWFUEL;
extern BOOL	_bGENFAIL;
extern BOOL	_bUPS2FAIL;
extern BOOL	_bUPS1FAIL;
extern BOOL	_bCHARGEFAIL;
extern BOOL	_bLOWVOLTAGE;
extern BOOL	_bCHARGING;
extern BOOL	_bGNDFAIL;
extern BOOL	_bNKWEPRN;
extern BOOL	_bNKWEPRS;
extern BOOL	_bNKWEPRA;
extern BOOL	_bNKWEPRB;
extern BOOL	_bNKWEPRC;
extern BOOL	_bNKWEPRD;
extern BOOL	_bNKWEPRE;
extern BOOL	_bNKWEPRF;
extern BOOL	_bNKWEPRG;
extern BOOL	_bDIMMER;
extern BOOL _bDefault1;
extern BOOL _bDefault2;
extern BOOL _bInitial;
extern BOOL _bLC1NKXPR;
extern BOOL _bLC1BELACK;
extern BOOL _bLC2NKXPR;
extern BOOL _bLC2BELACK;
extern BOOL _bLC3NKXPR;
extern BOOL _bLC3BELACK;
extern BOOL _bLC4NKXPR;
extern BOOL _bLC4BELACK;
extern BOOL _bLC5NKXPR;
extern BOOL _bLC5BELACK;
extern BOOL _bHPK;
extern BOOL _bSLS;
extern BOOL _bPAS;
extern BOOL	_bMode;
extern BOOL _bCTCRequest;
extern BOOL _bLC3NKXPR;
extern BOOL _bLC3BELACK;
extern BOOL _bMCCR1;
extern BOOL _bMCCR2;
extern BOOL _bMCCR1Online;
extern BOOL _bMCCR2Online;
extern BOOL _bBlockUp;
extern BOOL _bBlockDown;
extern BOOL _bBlockMid;

extern BOOL _bDlgLoad;

CSysBtnDlg::CSysBtnDlg(CCommDriver* pSerial, CWnd* pParent /*=NULL*/)
	: CDialog(CSysBtnDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSysBtnDlg)
	m_bACTIVEON1 = FALSE;
	m_bACTIVEON2 = FALSE;
	m_bVPOR1 = FALSE;
	m_bVPOR2 = FALSE;
	m_bFUSE1 =FALSE;
	m_bFUSE2 = FALSE;
	m_bFUSEPR = FALSE;
	m_bFAN1 = FALSE;
	m_bFAN2 = FALSE;
	m_b24VINT = FALSE;
	m_b24VNEXT = FALSE;
	m_b24VSEXT = FALSE;
	m_bMAINPOWER = FALSE;
	m_bGENRUN = FALSE;
	m_bGENLOWFUEL = FALSE;
	m_bGENFAIL = FALSE;
	m_bUPS2FAIL = FALSE;
	m_bUPS1FAIL = FALSE;
	m_bCHARGEFAIL = FALSE;
	m_bLOWVOLTAGE = FALSE;
	m_bCHARGING = FALSE;
	m_bGNDFAIL = FALSE;
	m_bNKWEPRN = FALSE;
	m_bNKWEPRS = FALSE;
	m_bNKWEPRA = FALSE;
	m_bNKWEPRB = FALSE;
	m_bNKWEPRC = FALSE;
	m_bNKWEPRD = FALSE;
	m_bNKWEPRE = FALSE;
	m_bNKWEPRF = FALSE;
	m_bNKWEPRG = FALSE;
	m_bDIMMER = FALSE;
	m_bDefault1 = FALSE;
	m_bDefault2 = FALSE;
	m_bInitial = FALSE;
	m_bLC1NKXPR = FALSE;
	m_bLC1BELACK = FALSE;
	m_bLC2NKXPR = FALSE;
	m_bLC2BELACK = FALSE;
	m_bLC3NKXPR = FALSE;
	m_bLC3BELACK = FALSE;
	m_bLC4NKXPR = FALSE;
	m_bLC4BELACK = FALSE;
	m_bLC5NKXPR = FALSE;
	m_bLC5BELACK = FALSE;
	m_bHPK = FALSE;
    m_bSLS = TRUE;
	m_bPAS = FALSE;
	m_bFAN3 = FALSE;
	m_bFAN4 = FALSE;
	m_bFUSE3 = FALSE;
	m_bFUSE4 = FALSE;
	m_bLC3NKXPR = FALSE;
	m_bLC3BELACK = FALSE;
	m_bMode = FALSE;
	m_bCTCRequest = FALSE;
	m_bMCCR1 = FALSE;
	m_bMCCR1Online = FALSE;
	m_bMCCR2 = FALSE;
	m_bMCCR2Online = FALSE;
	m_bBlockUp = TRUE;
	m_bBlockDown = TRUE;
	m_bBlockMid = TRUE;

	//}}AFX_DATA_INIT
	m_pSerial = pSerial;
}


void CSysBtnDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSysBtnDlg)
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_SIMMODE_SW, _bSIMModeSW);
	DDX_Check(pDX, IDC_CHECK_SIMMODE_IO, _bSIMModeIO);
/*	DDX_Check(pDX, IDC_CHECK_ACTIVEON1, m_bACTIVEON1);
	DDX_Check(pDX, IDC_CHECK_ACTIVEON2, m_bACTIVEON2);
	DDX_Check(pDX, IDC_CHECK_VPOR1, m_bVPOR1);
	DDX_Check(pDX, IDC_CHECK_VPOR2, m_bVPOR2);
	DDX_Check(pDX, IDC_CHECK_FUSE1, m_bFUSE1);
	DDX_Check(pDX, IDC_CHECK_FUSE2, m_bFUSE2);
	DDX_Check(pDX, IDC_CHECK_FAN1, m_bFAN1);
	DDX_Check(pDX, IDC_CHECK_FAN2, m_bFAN2);
	DDX_Check(pDX, IDC_CHECK_24VINT, m_b24VINT);
	DDX_Check(pDX, IDC_CHECK_24VNEXT, m_b24VNEXT);
	DDX_Check(pDX, IDC_CHECK_24VSEXT, m_b24VSEXT);
	DDX_Check(pDX, IDC_CHECK_MAINPOWER, m_bMAINPOWER);
	DDX_Check(pDX, IDC_CHECK_GENRUN, m_bGENRUN);
	DDX_Check(pDX, IDC_CHECK_UPS2FAIL, m_bUPS2FAIL);
	DDX_Check(pDX, IDC_CHECK_UPS1FAIL, m_bUPS1FAIL);
	DDX_Check(pDX, IDC_CHECK_CHARGEFAIL, m_bCHARGEFAIL);
	DDX_Check(pDX, IDC_CHECK_LOWVOLTAGE, m_bLOWVOLTAGE);
	DDX_Check(pDX, IDC_CHECK_CHARGING, m_bCHARGING);
	DDX_Check(pDX, IDC_CHECK_GNDFAIL, m_bGNDFAIL);
	DDX_Check(pDX, IDC_CHECK_NKWEPRN, m_bNKWEPRN);
	DDX_Check(pDX, IDC_CHECK_NKWEPRS, m_bNKWEPRS);
	DDX_Check(pDX, IDC_CHECK_NKWEPRA, m_bNKWEPRA);
	DDX_Check(pDX, IDC_CHECK_NKWEPRB, m_bNKWEPRB);
	DDX_Check(pDX, IDC_CHECK_NKWEPRC, m_bNKWEPRC);
	DDX_Check(pDX, IDC_CHECK_NKWEPRD, m_bNKWEPRD);
	DDX_Check(pDX, IDC_CHECK_NKWEPRE, m_bNKWEPRE);
	DDX_Check(pDX, IDC_CHECK_NKWEPRF, m_bNKWEPRF);
	DDX_Check(pDX, IDC_CHECK_NKWEPRG, m_bNKWEPRG);
    DDX_Check(pDX, IDC_CHECK_DIMMER, m_bDIMMER);
	DDX_Check(pDX, IDC_CHECK_DEFAULT1, m_bDefault1);
	DDX_Check(pDX, IDC_CHECK_DEFAULT2, m_bDefault2);
	DDX_Check(pDX, IDC_CHECK_INITIAL, m_bInitial);
	DDX_Check(pDX, IDC_CHECK_bLC1NKXPR, m_bLC1NKXPR);
	DDX_Check(pDX, IDC_CHECK_bLC1BELACK, m_bLC1BELACK);
	DDX_Check(pDX, IDC_CHECK_HPK, m_bHPK);
	DDX_Check(pDX, IDC_CHECK_SLS, m_bSLS);
	DDX_Check(pDX, IDC_CHECK_PAS, m_bPAS);
	DDX_Check(pDX, IDC_CHECK_FAN3, m_bFAN3);
	DDX_Check(pDX, IDC_CHECK_FAN4, m_bFAN4);
	DDX_Check(pDX, IDC_CHECK_FUSE3, m_bFUSE3);
	DDX_Check(pDX, IDC_CHECK_FUSE4, m_bFUSE4);
*/
}


BEGIN_MESSAGE_MAP(CSysBtnDlg, CDialog)
	//{{AFX_MSG_MAP(CSysBtnDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_FAN1, OnButtonFan1)
	ON_BN_CLICKED(IDC_BUTTON_CTC, OnButtonCtc)
	ON_BN_CLICKED(IDC_BUTTON_UPS1, OnButtonUps1)
	ON_BN_CLICKED(IDC_BUTTON_DC_EXT, OnButtonDcExt)
	ON_BN_CLICKED(IDC_BUTTON_CBI1, OnButtonCbi1)
	ON_BN_CLICKED(IDC_BUTTON_FAN2, OnButtonFan2)
	ON_BN_CLICKED(IDC_BUTTON_UPS2, OnButtonUps2)
	ON_BN_CLICKED(IDC_BUTTON_MCCR1, OnButtonMccr1)
	ON_BN_CLICKED(IDC_BUTTON_CBI2, OnButtonCbi2)
	ON_BN_CLICKED(IDC_BUTTON_FUSE_C, OnButtonFuseC)
	ON_BN_CLICKED(IDC_BUTTON_GEN_FAIL, OnButtonGenFail)
	ON_BN_CLICKED(IDC_BUTTON_MCCR1_ONLINE, OnButtonMccr1Online)
	ON_BN_CLICKED(IDC_BUTTON_CBI_ONLINE, OnButtonCbiOnline)
	ON_BN_CLICKED(IDC_BUTTON_FUSE_R, OnButtonFuseR)
	ON_BN_CLICKED(IDC_BUTTON_GEN_RUN, OnButtonGenRun)
	ON_BN_CLICKED(IDC_BUTTON_MCCR2, OnButtonMccr2)
	ON_BN_CLICKED(IDC_BUTTON_MAIN_POWER, OnButtonMainPower)
	ON_BN_CLICKED(IDC_BUTTON_B24_IN, OnButtonB24In)
	ON_BN_CLICKED(IDC_BUTTON_MCCR2_ONLINE, OnButtonMccr2Online)
	ON_BN_CLICKED(IDC_BUTTON_LOW_VOLTAGE, OnButtonLowVoltage)
	ON_BN_CLICKED(IDC_BUTTON_BATTERY_CHARGING, OnButtonBatteryCharging)
	ON_BN_CLICKED(IDC_BUTTON_GROUND, OnButtonGround)
	ON_BN_CLICKED(IDC_BUTTON_LC1_BARRIER, OnButtonLc1Barrier)
	ON_BN_CLICKED(IDC_BUTTON_LC1_BELL_ACK, OnButtonLc1BellAck)
	ON_BN_CLICKED(IDC_BUTTON_LC2_BARRIER, OnButtonLc2Barrier)
	ON_BN_CLICKED(IDC_BUTTON_LC2_BELL_ACK, OnButtonLc2BellAck)
	ON_BN_CLICKED(IDC_BUTTON_LC3_BARRIER, OnButtonLc3Barrier)
	ON_BN_CLICKED(IDC_BUTTON_LC3_BELL_ACK, OnButtonLc3BellAck)
	ON_BN_CLICKED(IDC_BUTTON_LC4_BARRIER, OnButtonLc4Barrier)
	ON_BN_CLICKED(IDC_BUTTON_LC4_BELL_ACK, OnButtonLc4BellAck)
	ON_BN_CLICKED(IDC_BUTTON_LC5_BARRIER, OnButtonLc5Barrier)
	ON_BN_CLICKED(IDC_BUTTON_LC5_BELL_ACK, OnButtonLc5BellAck)
	ON_BN_CLICKED(IDC_BUTTON_UEPK_HEPK_KEY, OnButtonUepkHepkKey)
	ON_BN_CLICKED(IDC_BUTTON_DEPK_IEPK_KEY, OnButtonDepkIepkKey)
	ON_BN_CLICKED(IDC_BUTTON_AEPK_KEY, OnButtonAepkKey)
	ON_BN_CLICKED(IDC_BUTTON_BEPK_KEY, OnButtonBepkKey)
	ON_BN_CLICKED(IDC_BUTTON_CEPK_KEY, OnButtonCepkKey)
	ON_BN_CLICKED(IDC_BUTTON_DEPK_KEY, OnButtonDepkKey)
	ON_BN_CLICKED(IDC_BUTTON_EEPK_KEY, OnButtonEepkKey)
	ON_BN_CLICKED(IDC_BUTTON_FEPK_KEY, OnButtonFepkKey)
	ON_BN_CLICKED(IDC_BUTTON_GEPK_KEY, OnButtonGepkKey)
	ON_BN_CLICKED(IDC_BUTTON_ALL_HPK_KEY, OnButtonAllHpkKey)
	ON_BN_CLICKED(IDC_BUTTON_BLOCK_COMM_UP, OnButtonBlockCommUp)
	ON_BN_CLICKED(IDC_BUTTON_BLOCK_COMM_DOWN, OnButtonBlockCommDown)
	ON_BN_CLICKED(IDC_BUTTON_BLOCK_COMM_MID, OnButtonBlockCommMid)
	ON_BN_CLICKED(IDC_BUTTON_CTC_REQUEST, OnButtonCtcRequest)
	ON_BN_CLICKED(IDC_CHECK_SIMMODE_SW, OnCheckSimmodeSw)
	ON_BN_CLICKED(IDC_CHECK_SIMMODE_IO, OnCheckSimmodeIO)
	ON_BN_CLICKED(IDC_BUTTON_FAN3, OnButtonFan3)
	ON_BN_CLICKED(IDC_BUTTON_FAN4, OnButtonFan4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSysBtnDlg::OnOK() 
{
//	CDialog::OnOK();
	

// 	UpdateData(TRUE);
// 
//     _bACTIVEON1 = m_bACTIVEON1;
//     _bACTIVEON2 = m_bACTIVEON2;
//     _bVPOR1 = m_bVPOR1;
//     _bVPOR2 = m_bVPOR2;
//     _bFUSE1 = m_bFUSE1;
//     _bFUSE2 = m_bFUSE2;
//     _bFUSE3 = m_bFUSE3;
//     _bFUSE4 = m_bFUSE4;
// 	_bFUSEPR = m_bFUSEPR;
// 	_bMode = m_bMode;
// 	_bAXLFail = m_bAXLFail;
// 	_bLC2NKXPR = m_bLC2NKXPR;
// 	_bLC2BELACK = m_bLC2BELACK;
//     _bFAN1 = m_bFAN1;
//     _bFAN2 = m_bFAN2;
//     _bFAN3 = m_bFAN3;
//     _bFAN4 = m_bFAN4;
//     _b24VINT = m_b24VINT;
//     _b24VNEXT = m_b24VNEXT;
//     _b24VSEXT = m_b24VSEXT;
//     _bMAINPOWER = m_bMAINPOWER;
//     _bGENRUN = m_bGENRUN;
//     _bGENLOWFUEL = m_bGENLOWFUEL;
//     _bGENFAIL = m_bGENFAIL;
//     _bUPS2FAIL = m_bUPS2FAIL;
//     _bUPS1FAIL = m_bUPS1FAIL;
//     _bCHARGEFAIL = m_bCHARGEFAIL;
//     _bLOWVOLTAGE = m_bLOWVOLTAGE;
//     _bCHARGING = m_bCHARGING;
//     _bGNDFAIL = m_bGNDFAIL;
//     _bDIMMER = m_bDIMMER;
// 	_bMCCR1 = m_bMCCR1;
// 	_bMCCR2 = m_bMCCR2;
// 	_bMCCR1Online = m_bMCCR1Online;
// 	_bMCCR2Online = m_bMCCR2Online;
// 	_bBlockComm = m_bBlockComm;
// 
// 	_bDefault1 = m_bDefault1;
// 	_bDefault2 = m_bDefault2;
// 	_bInitial = m_bInitial;
// 	_bLC1NKXPR = m_bLC1NKXPR;
// 	_bLC1BELACK = m_bLC1BELACK;
// 	_bNKWEPRN = m_bNKWEPRN;
// 	_bNKWEPRS = m_bNKWEPRS;
// 	_bNKWEPRA = m_bNKWEPRA;
// 	_bNKWEPRB = m_bNKWEPRB;
// 	_bNKWEPRC = m_bNKWEPRC;
// 	_bNKWEPRD = m_bNKWEPRD;
// 	_bNKWEPRE = m_bNKWEPRE;
// 	_bNKWEPRF = m_bNKWEPRF;
// 	_bNKWEPRG = m_bNKWEPRG;
// 	_bHPK = m_bHPK;
// 	_bSLS = m_bSLS;
// 	_bPAS = m_bPAS;
// 	_bInitial = m_bInitial;
// 
// 
// 	KillTimer(100);
  	DestroyWindow();
}


void CSysBtnDlg::PostNcDestroy() 
{
	delete this;
	_bDlgLoad = FALSE;
	
	CDialog::PostNcDestroy();
}


 void CSysBtnDlg::OnTimer(UINT nIDEvent) 
{
#if 0
 	UpdateData(TRUE);
	DataHeaderType* pHead = (DataHeaderType*)m_pVMEM;
	if(pHead->nSysVar.bPKeyContA || pHead->nSysVar.bPKeyContN)
		GetDlgItem(IDC_BUTTON_AEPK_UEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_AEPK_UEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContB || pHead->nSysVar.bPKeyContS)
		GetDlgItem(IDC_BUTTON_BEPK_DEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_BEPK_DEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContC)
		GetDlgItem(IDC_BUTTON_CEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_CEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContD)
		GetDlgItem(IDC_BUTTON_DEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_DEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContE)
		GetDlgItem(IDC_BUTTON_EEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_EEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContF)
		GetDlgItem(IDC_BUTTON_FEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_FEPK_KEY)->EnableWindow(FALSE);
	if(pHead->nSysVar.bPKeyContG)
		GetDlgItem(IDC_BUTTON_GEPK_KEY)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_GEPK_KEY)->EnableWindow(FALSE);
	if(pHead->CrossState.bLC1KXLR)
		GetDlgItem(IDC_BUTTON_LC1_BARRIER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_LC1_BARRIER)->EnableWindow(FALSE);
	if(pHead->CrossState.bLC2KXLR)
		GetDlgItem(IDC_BUTTON_LC2_BARRIER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_LC2_BARRIER)->EnableWindow(FALSE);
	if(pHead->CrossState.bLC3KXLR)
		GetDlgItem(IDC_BUTTON_LC3_BARRIER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_LC3_BARRIER)->EnableWindow(FALSE);
	if(pHead->CrossState.bLC4KXLR)
		GetDlgItem(IDC_BUTTON_LC4_BARRIER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_LC4_BARRIER)->EnableWindow(FALSE);
	if(pHead->nSysVar.bLC5KXLR)
		GetDlgItem(IDC_BUTTON_LC5_BARRIER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON_LC5_BARRIER)->EnableWindow(FALSE);
#endif
// 
//     _bACTIVEON1 = m_bACTIVEON1;
//     _bACTIVEON2 = m_bACTIVEON2;
//     _bVPOR1 = m_bVPOR1;
//     _bVPOR2 = m_bVPOR2;
//     _bFUSE1 = m_bFUSE1;
//     _bFUSE2 = m_bFUSE2;
//     _bFUSE3 = m_bFUSE3;
//     _bFUSE4 = m_bFUSE4;
// 	_bFUSEPR = m_bFUSEPR;
// 	_bMode = m_bMode;
// 	_bAXLFail = m_bAXLFail;
// 	_bLC2NKXPR = m_bLC2NKXPR;
// 	_bLC2BELACK = m_bLC2BELACK;
//     _bFAN1 = m_bFAN1;
//     _bFAN2 = m_bFAN2;
//     _bFAN3 = m_bFAN3;
//     _bFAN4 = m_bFAN4;
//     _b24VINT = m_b24VINT;
//     _b24VNEXT = m_b24VNEXT;
//     _b24VSEXT = m_b24VSEXT;
//     _bMAINPOWER = m_bMAINPOWER;
//     _bGENRUN = m_bGENRUN;
//     _bGENLOWFUEL = m_bGENLOWFUEL;
//     _bGENFAIL = m_bGENFAIL;
//     _bUPS2FAIL = m_bUPS2FAIL;
//     _bUPS1FAIL = m_bUPS1FAIL;
//     _bCHARGEFAIL = m_bCHARGEFAIL;
//     _bLOWVOLTAGE = m_bLOWVOLTAGE;
//     _bCHARGING = m_bCHARGING;
//     _bGNDFAIL = m_bGNDFAIL;
//     _bNKWEPRN = m_bNKWEPRN;
//     _bNKWEPRS = m_bNKWEPRS;
//     _bNKWEPRA = m_bNKWEPRA;
//     _bNKWEPRB = m_bNKWEPRB;
//     _bNKWEPRC = m_bNKWEPRC;
//     _bNKWEPRD = m_bNKWEPRD;
//     _bNKWEPRE = m_bNKWEPRE;
//     _bNKWEPRF = m_bNKWEPRF;
//     _bNKWEPRG = m_bNKWEPRG;
// 	_bDIMMER = m_bDIMMER;
// 	_bDefault1 = m_bDefault1;
// 	_bDefault2 = m_bDefault2;
// 	_bLC1NKXPR = m_bLC1NKXPR;
// 	_bLC1BELACK = m_bLC1BELACK;
// 	_bHPK = m_bHPK;
// 	_bSLS = m_bSLS;
// 	_bPAS = m_bPAS;
// 	_bInitial = m_bInitial;
// 	_bMCCR1 = m_bMCCR1;
// 	_bMCCR2 = m_bMCCR2;
// 	_bMCCR1Online = m_bMCCR1Online;
// 	_bMCCR2Online = m_bMCCR2Online;
// 	_bBlockComm = m_bBlockComm;
// 
// 	CDialog::OnTimer(nIDEvent);
	OnCheckSimmodeIO();
}

BOOL CSysBtnDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (_bInitial)
	{
// 		m_bDefault2 = FALSE;
		m_bDefault1 = FALSE;

// 		m_bACTIVEON1 = FALSE;
// 		m_bACTIVEON2 = FALSE;
// 		m_bVPOR1 = FALSE;
// 		m_bVPOR2 = FALSE;
// 		m_bFUSE1 = FALSE;
// 		m_bFUSE2 = FALSE;
// 		m_bFUSE3 = FALSE;
// 		m_bFUSE4 = FALSE;
// 		m_bFUSEPR = FALSE;
// 		m_bFAN1 = FALSE;
// 		m_bFAN2 = FALSE;
// 		m_bFAN3 = FALSE;
// 		m_bFAN4 = FALSE;
// 		m_b24VINT = FALSE;
// 		m_b24VNEXT = FALSE;
// 		m_b24VSEXT = FALSE;
// 		m_bMAINPOWER = FALSE;
// 		m_bGENRUN = FALSE;
// 		m_bGENLOWFUEL = FALSE;
// 		m_bGENFAIL = FALSE;
// 		m_bUPS2FAIL = FALSE;
// 		m_bUPS1FAIL = FALSE;
// 		m_bCHARGEFAIL = FALSE;
// 		m_bLOWVOLTAGE = FALSE; 
// 		m_bCHARGING = FALSE;
// 		m_bGNDFAIL = FALSE;
// 		m_bNKWEPRN = FALSE;
// 		m_bNKWEPRS = FALSE;
// 		m_bNKWEPRA = FALSE;
// 		m_bNKWEPRB = FALSE;
// 		m_bNKWEPRC = FALSE;
// 		m_bNKWEPRD = FALSE;
// 		m_bNKWEPRE = FALSE;
// 		m_bNKWEPRF = FALSE;
// 		m_bNKWEPRG = FALSE;
// 		m_bDIMMER = FALSE;
// 		m_bLC1NKXPR = FALSE;
// 		m_bLC1BELACK = FALSE;
// 		m_bHPK = FALSE;
// 		m_bSLS = FALSE;
// 		m_bPAS = FALSE;
// 		m_bMode = FALSE;
// 		m_bAXLFail = FALSE;
// 		m_bLC2NKXPR = FALSE;
// 		m_bLC2BELACK = FALSE;
// 		m_bMCCR1 = FALSE;
// 		m_bMCCR1Online = FALSE;
// 		m_bMCCR2 = FALSE;
// 		m_bMCCR2Online = FALSE;
// 		m_bBlockComm = FALSE;
		_bACTIVEON1 = FALSE;
		_bVPOR1 = FALSE;
		_bACTIVEON2 = FALSE;
		_bVPOR2 = FALSE;
		_bFUSE1 = FALSE;
		_bFUSE2 = FALSE;
		_bFUSE3 = FALSE;
		_bFUSE4 = FALSE;
		_bFUSEPR = FALSE;
		_bFAN1 = FALSE;
		_bFAN2 = FALSE;
		_bFAN3 = FALSE;
		_bFAN4 = FALSE;
		_b24VINT = FALSE;
		_b24VNEXT = FALSE;
		_b24VSEXT = FALSE;
		_bMAINPOWER = FALSE;
		_bGENRUN = FALSE;
		_bGENLOWFUEL = FALSE;
		_bGENFAIL = FALSE;
		_bUPS1FAIL = FALSE;
		_bUPS2FAIL = FALSE;
		_bCHARGEFAIL = FALSE;
		_bLOWVOLTAGE = FALSE;
		_bCHARGING = FALSE;
		_bGNDFAIL = FALSE;
		_bNKWEPRA = FALSE;
		_bNKWEPRB = FALSE;
		_bNKWEPRC = FALSE;
		_bNKWEPRD = FALSE;
		_bNKWEPRE = FALSE;
		_bNKWEPRF = FALSE;
		_bNKWEPRG = FALSE;
		_bNKWEPRN = FALSE;
		_bNKWEPRS = FALSE;
		_bDIMMER = FALSE;
		_bLC1NKXPR = FALSE;
		_bLC1BELACK = FALSE;
		_bLC2NKXPR = FALSE;
		_bLC2BELACK = FALSE;
		_bLC3NKXPR = FALSE;
		_bLC3BELACK = FALSE;
		_bLC4NKXPR = FALSE;
		_bLC4BELACK = FALSE;
		_bLC5NKXPR = FALSE;
		_bLC5BELACK = FALSE;
		_bHPK = FALSE;
		_bSLS = FALSE;
		_bPAS = FALSE;
		_bMode = FALSE;
		_bCTCRequest = FALSE;
		_bMCCR1 = FALSE;
		_bMCCR1Online = FALSE;
		_bMCCR2 = FALSE;
		_bMCCR2Online = FALSE;
		_bBlockUp = TRUE;
		_bBlockDown = TRUE;
		_bBlockMid = TRUE;
	}	
	else if (_bDefault1)
	{
//		m_bDefault2 = FALSE;
//		m_bDefault1 = TRUE;

//      m_bACTIVEON1 = TRUE;
//      m_bVPOR1 = TRUE;
// 		m_bVPOR2 = TRUE;
//      m_bFUSE1 = FALSE;
//      m_bFAN1 = TRUE;
//      m_bFUSE2 = FALSE;
// 		m_bFUSEPR = FALSE;
//      m_bFAN2 = TRUE;
//      m_bFAN3 = TRUE;
//      m_bFAN4 = TRUE;
//      m_b24VINT = TRUE;
//      m_b24VNEXT = TRUE;
//      m_bMAINPOWER = TRUE;
//      m_bGENRUN = FALSE;
//      m_bGENLOWFUEL = TRUE;
//      m_bGENFAIL = FALSE;
//      m_bUPS2FAIL = TRUE;
// 		m_bUPS1FAIL = TRUE;
// 		m_bCHARGEFAIL = TRUE;
// 		m_bLOWVOLTAGE = FALSE;
// 		m_bCHARGING = TRUE;
// 		m_bGNDFAIL = FALSE;
// 		m_bNKWEPRN = TRUE;
// 		m_bNKWEPRS = TRUE;
// 		m_bNKWEPRA = TRUE;
// 		m_bNKWEPRB = TRUE;
// 		m_bNKWEPRC = TRUE;
// 		m_bNKWEPRD = TRUE;
// 		m_bNKWEPRE = TRUE;
// 		m_bNKWEPRF = TRUE;
// 		m_bNKWEPRG = TRUE;
// 		m_bLC1NKXPR = FALSE;
// 		m_bHPK = TRUE;
// 		m_bSLS = TRUE;
// 		m_bMode = FALSE;
// 		m_bAXLFail = FALSE;
// 		m_bLC1NKXPR = FALSE;
// 		m_bMCCR1 = TRUE;
// 		m_bMCCR1Online = TRUE;
// 		m_bMCCR2 = TRUE;
// 		m_bMCCR2Online = FALSE;
// 		m_bBlockComm = FALSE;
		_bACTIVEON1 = TRUE;
		_bVPOR1 = TRUE;
		_bVPOR2 = TRUE;
		_bFUSE1 = FALSE;
		_bFUSE2 = FALSE;
		_bFUSE3 = FALSE;
		_bFUSE4 = FALSE;
		_bFUSEPR = FALSE;
		_bFAN1 = TRUE;
		_bFAN2 = TRUE;
		_bFAN3 = TRUE;
		_bFAN4 = TRUE;
		_b24VINT = TRUE;
		_b24VNEXT = TRUE;
		_b24VSEXT = TRUE;
		_bMAINPOWER = TRUE;
		_bGENRUN = FALSE;
		_bGENLOWFUEL = TRUE;
		_bGENFAIL = FALSE;
		_bUPS1FAIL = TRUE;
		_bUPS2FAIL = TRUE;
		_bCHARGEFAIL = TRUE;
		_bLOWVOLTAGE = FALSE;
		_bCHARGING = TRUE;
		_bGNDFAIL = FALSE;
		_bNKWEPRA = TRUE;
		_bNKWEPRB = TRUE;
		_bNKWEPRC = TRUE;
		_bNKWEPRD = TRUE;
		_bNKWEPRE = TRUE;
		_bNKWEPRF = TRUE;
		_bNKWEPRG = TRUE;
		_bNKWEPRN = TRUE;
		_bNKWEPRS = TRUE;
		_bLC1NKXPR = FALSE;
		_bLC2NKXPR = FALSE;
		_bLC3NKXPR = FALSE;
		_bLC4NKXPR = FALSE;
		_bLC5NKXPR = FALSE;
		_bHPK = TRUE;
		_bSLS = TRUE;
		_bMode = FALSE;
		_bCTCRequest = FALSE;
		_bMCCR1 = TRUE;
		_bMCCR1Online = TRUE;
		_bMCCR2 = TRUE;
		_bMCCR2Online = FALSE;
		_bBlockUp = TRUE;
		_bBlockDown = TRUE;
		_bBlockMid = TRUE;
	}
// 	else if (_bDefault2)
// 	{
// 		m_bDefault1 = FALSE;
// //		m_bDefault2 = TRUE;
// 
//         m_bACTIVEON2 = TRUE;
//         m_bVPOR2 = TRUE;
//         m_bFAN3 = TRUE;
//         m_bFAN4 = TRUE;
//         m_bFUSE2 = TRUE;
// 		m_bFUSEPR = TRUE;
//         m_bFAN2 = TRUE;
//         m_bFUSE1 = TRUE;
//         m_bFAN1 = TRUE;
//         m_b24VINT = TRUE;
//         m_b24VNEXT = TRUE;
//         m_bMAINPOWER = TRUE;
//         m_bGENRUN = TRUE;
//         m_bGENLOWFUEL = TRUE;
//         m_bGENFAIL = TRUE;
//         m_bUPS2FAIL = TRUE;
// 		m_bUPS1FAIL = TRUE;
// 		m_bCHARGEFAIL = TRUE;
// 		m_bLOWVOLTAGE = TRUE;
// 		m_bCHARGING = TRUE;
// 		m_bGNDFAIL = TRUE;
// 		m_bNKWEPRN = TRUE;
// 		m_bNKWEPRS = TRUE;
// 		m_bNKWEPRA = TRUE;
// 		m_bNKWEPRB = TRUE;
// 		m_bNKWEPRC = TRUE;
// 		m_bNKWEPRD = TRUE;
// 		m_bNKWEPRE = TRUE;
// 		m_bNKWEPRF = TRUE;
// 		m_bNKWEPRG = TRUE;
// 		m_bLC1NKXPR = FALSE;
// 		m_bHPK = TRUE;
// 		m_bSLS = TRUE;
// 		m_bMode = FALSE;
// 		m_bAXLFail = TRUE;
// 		m_bLC2NKXPR = FALSE;
// 		m_bMCCR1 = TRUE;
// 		m_bMCCR1Online = FALSE;
// 		m_bMCCR2 = TRUE;
// 		m_bMCCR2Online = TRUE;
// 		m_bBlockComm = TRUE;
// 	}
// 	else
// 	{
// 		m_bACTIVEON1 = _bACTIVEON1;
// 		m_bACTIVEON2 = _bACTIVEON2;
// 		m_bVPOR1 = _bVPOR1;
// 		m_bVPOR2 = _bVPOR2;
// 		m_bFUSE1 = _bFUSE1;
// 		m_bFUSE2 = _bFUSE2;
// 		m_bFUSE3 = _bFUSE3;
// 		m_bFUSE4 = _bFUSE4;
// 		m_bFUSEPR = _bFUSEPR;
// 		m_bFAN1 = _bFAN1;
// 		m_bFAN2 = _bFAN2;
// 		m_bFAN3 = _bFAN3;
// 		m_bFAN4 = _bFAN4;
// 		m_b24VINT = _b24VINT;
// 		m_b24VNEXT = _b24VNEXT;
// 		m_b24VSEXT = _b24VSEXT;
// 		m_bMAINPOWER = _bMAINPOWER;
// 		m_bGENRUN = _bGENRUN;
// 		m_bGENLOWFUEL = _bGENLOWFUEL;
// 		m_bGENFAIL = _bGENFAIL;
// 		m_bUPS2FAIL = _bUPS2FAIL;
// 		m_bUPS1FAIL = _bUPS1FAIL;
// 		m_bCHARGEFAIL = _bCHARGEFAIL;
// 		m_bLOWVOLTAGE = _bLOWVOLTAGE; 
// 		m_bCHARGING = _bCHARGING;
// 		m_bGNDFAIL = _bGNDFAIL;
// 		m_bNKWEPRN = _bNKWEPRN;
// 		m_bNKWEPRS = _bNKWEPRS;
// 		m_bNKWEPRA = _bNKWEPRA;
// 		m_bNKWEPRB = _bNKWEPRB;
// 		m_bNKWEPRC = _bNKWEPRC;
// 		m_bNKWEPRD = _bNKWEPRD;
// 		m_bNKWEPRE = _bNKWEPRE;
// 		m_bNKWEPRF = _bNKWEPRF;
// 		m_bNKWEPRG = _bNKWEPRG;
// 		m_bDIMMER = _bDIMMER;
// 		m_bLC1NKXPR = _bLC1NKXPR;
//         m_bLC1BELACK = _bLC1BELACK;
// 		m_bHPK = _bHPK;
// 		m_bSLS = _bSLS;
// 		m_bPAS = _bPAS;
// 		m_bMode = _bMode;
// 		m_bAXLFail = _bAXLFail;
// 		m_bLC2NKXPR = _bLC2NKXPR;
// 		m_bLC2BELACK = _bLC2BELACK;
// 		m_bMCCR1 = _bMCCR1;
// 		m_bMCCR2 = _bMCCR2;
// 		m_bMCCR1Online = _bMCCR1Online;
// 		m_bMCCR2Online = _bMCCR2Online;
// 		m_bBlockComm = _bBlockComm;
// 
// 		m_bDefault1 = _bDefault1;
// 		m_bDefault2 = _bDefault2;
// 		m_bInitial = _bInitial;
// 	}

	
// 	UpdateData(FALSE);

	CLISApp *pApp = (CLISApp *)AfxGetApp();

	int iNumberOfEPK = pApp->GetProfileInt("LSM", "NumberOfEPK", 0);
	int iNumberOfLC = pApp->GetProfileInt("LSM", "NumberOfLC", 0);
	CString strProjectName = pApp->GetProfileString("LSM", "ProjectName", "");
	CString strLCName[6];

	CString strNameOfLC;
	for (int i = 1; i < 6; i++)
	{
		strNameOfLC.Format("LC%d",i);
		
		strLCName[i] = pApp->GetProfileString("LSM", strNameOfLC, "FALSE");

	}

	CString strBuffer;

	switch (iNumberOfLC)
	{
	case 0:
		GetDlgItem(IDC_BUTTON_LC1_BARRIER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_LC1_BELL_ACK)->EnableWindow(FALSE);
	case 1:
		GetDlgItem(IDC_BUTTON_LC2_BARRIER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_LC2_BELL_ACK)->EnableWindow(FALSE);
	case 2:
		GetDlgItem(IDC_BUTTON_LC3_BARRIER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_LC3_BELL_ACK)->EnableWindow(FALSE);
	case 3:
		GetDlgItem(IDC_BUTTON_LC4_BARRIER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_LC4_BELL_ACK)->EnableWindow(FALSE);
	case 4:
		GetDlgItem(IDC_BUTTON_LC5_BARRIER)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_LC5_BELL_ACK)->EnableWindow(FALSE);
	}
	
	switch (iNumberOfLC)
	{
	case 5:
		strBuffer.Format("%s OPEN",strLCName[5]);
		GetDlgItem(IDC_BUTTON_LC5_BARRIER)->SetWindowText(strBuffer);
		strBuffer.Format("%s ACK",strLCName[5]);
		GetDlgItem(IDC_BUTTON_LC5_BELL_ACK)->SetWindowText(strBuffer);
	case 4:
		strBuffer.Format("%s OPEN",strLCName[4]);
		GetDlgItem(IDC_BUTTON_LC4_BARRIER)->SetWindowText(strBuffer);
		strBuffer.Format("%s ACK",strLCName[4]);
		GetDlgItem(IDC_BUTTON_LC4_BELL_ACK)->SetWindowText(strBuffer);
	case 3:
		strBuffer.Format("%s OPEN",strLCName[3]);
		GetDlgItem(IDC_BUTTON_LC3_BARRIER)->SetWindowText(strBuffer);
		strBuffer.Format("%s ACK",strLCName[3]);
		GetDlgItem(IDC_BUTTON_LC3_BELL_ACK)->SetWindowText(strBuffer);
	case 2:
		strBuffer.Format("%s OPEN",strLCName[2]);
		GetDlgItem(IDC_BUTTON_LC2_BARRIER)->SetWindowText(strBuffer);
		strBuffer.Format("%s ACK",strLCName[2]);
		GetDlgItem(IDC_BUTTON_LC2_BELL_ACK)->SetWindowText(strBuffer);
	case 1:
		strBuffer.Format("%s OPEN",strLCName[1]);
		GetDlgItem(IDC_BUTTON_LC1_BARRIER)->SetWindowText(strBuffer);
		strBuffer.Format("%s ACK",strLCName[1]);
		GetDlgItem(IDC_BUTTON_LC1_BELL_ACK)->SetWindowText(strBuffer);
	}
	
	switch (iNumberOfEPK)
	{
	case 2:
		GetDlgItem(IDC_BUTTON_AEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_BEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_DEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_EEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_FEPK_KEY)->EnableWindow(FALSE);
		break;
	case 3:
		GetDlgItem(IDC_BUTTON_DEPK_KEY)->EnableWindow(FALSE);
	case 4:
		GetDlgItem(IDC_BUTTON_EEPK_KEY)->EnableWindow(FALSE);
	case 5:
		GetDlgItem(IDC_BUTTON_FEPK_KEY)->EnableWindow(FALSE);
	case 6:
		GetDlgItem(IDC_BUTTON_GEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_UEPK_HEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_DEPK_IEPK_KEY)->EnableWindow(FALSE);
	}

	if ( iNumberOfEPK == 3 && strProjectName == "StandardOfLC")
	{
		GetDlgItem(IDC_BUTTON_UEPK_HEPK_KEY)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_DEPK_IEPK_KEY)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_AEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_BEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CEPK_KEY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_EEPK_KEY)->EnableWindow(TRUE);
	}

	if ( _bSIMModeSW )
	{
		GetDlgItem(IDC_CHECK_SIMMODE_SW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SIMMODE_IO)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_CHECK_SIMMODE_SW)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SIMMODE_IO)->EnableWindow(FALSE);
	}
	
	//SetTimer(100, 50, NULL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




void CSysBtnDlg::OnButtonFan1() 
{
	// TODO: Add your control notification handler code here
//	m_bFAN1 = !m_bFAN1;
	_bFAN1 = !_bFAN1;
}

void CSysBtnDlg::OnButtonCtc() 
{
	// TODO: Add your control notification handler code here
	_bMode = !_bMode;
}

void CSysBtnDlg::OnButtonUps1() 
{
	// TODO: Add your control notification handler code here
	_bUPS1FAIL = !_bUPS1FAIL;
}

void CSysBtnDlg::OnButtonDcExt() 
{
	// TODO: Add your control notification handler code here
	_b24VNEXT = !_b24VNEXT;
	_b24VSEXT = _b24VNEXT;
}

void CSysBtnDlg::OnButtonCbi1() 
{
	// TODO: Add your control notification handler code here
	_bVPOR1 = !_bVPOR1;
}

void CSysBtnDlg::OnButtonFan2() 
{
	// TODO: Add your control notification handler code here
	_bFAN2 = !_bFAN2;
}

void CSysBtnDlg::OnButtonFan3() 
{
	// TODO: Add your control notification handler code here
	_bFAN3 = !_bFAN3;
}

void CSysBtnDlg::OnButtonFan4() 
{
	// TODO: Add your control notification handler code here
	_bFAN4 = !_bFAN4;
}

void CSysBtnDlg::OnButtonUps2() 
{
	// TODO: Add your control notification handler code here
	_bUPS2FAIL = !_bUPS2FAIL;
}

void CSysBtnDlg::OnButtonMccr1() 
{
	// TODO: Add your control notification handler code here
	_bMCCR1 = !_bMCCR1;
}

void CSysBtnDlg::OnButtonCbi2() 
{
	// TODO: Add your control notification handler code here
	_bVPOR2 = !_bVPOR2;
}

void CSysBtnDlg::OnButtonFuseC() 
{
	// TODO: Add your control notification handler code here
	_bFUSE1 = !_bFUSE1;
	_bFUSE4 = _bFUSE3 = _bFUSE2 = _bFUSE1;
}

void CSysBtnDlg::OnButtonGenFail() 
{
	// TODO: Add your control notification handler code here
	_bGENFAIL = !_bGENFAIL;
}

void CSysBtnDlg::OnButtonMccr1Online() 
{
	// TODO: Add your control notification handler code here
	_bMCCR1Online = !_bMCCR1Online;
	if ( _bMCCR1Online == TRUE && _bMCCR2Online == TRUE )
	{
		_bMCCR2Online = FALSE;
	}
}

void CSysBtnDlg::OnButtonCbiOnline() 
{
	// TODO: Add your control notification handler code here
	if ( _bACTIVEON1 == FALSE && _bACTIVEON2 == FALSE )
	{
		_bACTIVEON1 = TRUE;
	}
	else if ( _bACTIVEON1 == TRUE && _bACTIVEON2 == FALSE )
	{
		_bACTIVEON1 = FALSE;
		_bACTIVEON2 = TRUE;
	}
	else if ( _bACTIVEON1 == FALSE && _bACTIVEON2 == TRUE )
	{
		_bACTIVEON2 = FALSE;
	}
	else
	{
		_bACTIVEON1 = _bACTIVEON2 = FALSE;
	}
}

void CSysBtnDlg::OnButtonFuseR() 
{
	// TODO: Add your control notification handler code here
	_bFUSEPR = !_bFUSEPR;
}

void CSysBtnDlg::OnButtonGenRun() 
{
	// TODO: Add your control notification handler code here
	_bGENRUN = !_bGENRUN;
}

void CSysBtnDlg::OnButtonMccr2() 
{
	// TODO: Add your control notification handler code here
	_bMCCR2 = !_bMCCR2;
}

void CSysBtnDlg::OnButtonMainPower() 
{
	// TODO: Add your control notification handler code here
	_bMAINPOWER = !_bMAINPOWER;
}

void CSysBtnDlg::OnButtonB24In() 
{
	// TODO: Add your control notification handler code here
	_b24VINT = !_b24VINT;
}

void CSysBtnDlg::OnButtonMccr2Online() 
{
	// TODO: Add your control notification handler code here
	_bMCCR2Online = !_bMCCR2Online;
	if ( _bMCCR1Online == TRUE && _bMCCR2Online == TRUE )
	{
		_bMCCR1Online = FALSE;
	}

}

void CSysBtnDlg::OnButtonLowVoltage() 
{
	// TODO: Add your control notification handler code here
	_bLOWVOLTAGE = !_bLOWVOLTAGE;
}

void CSysBtnDlg::OnButtonBatteryCharging() 
{
	// TODO: Add your control notification handler code here
	_bCHARGING = !_bCHARGING;
}

void CSysBtnDlg::OnButtonGround() 
{
	// TODO: Add your control notification handler code here
	_bGNDFAIL = !_bGNDFAIL;
}

void CSysBtnDlg::OnButtonLc1Barrier() 
{
	// TODO: Add your control notification handler code here
	_bLC1NKXPR = !_bLC1NKXPR;
}

void CSysBtnDlg::OnButtonLc1BellAck() 
{
	// TODO: Add your control notification handler code here
	_bLC1BELACK = !_bLC1BELACK;
}

void CSysBtnDlg::OnButtonLc2Barrier() 
{
	// TODO: Add your control notification handler code here
	_bLC2NKXPR = !_bLC2NKXPR;
}

void CSysBtnDlg::OnButtonLc2BellAck() 
{
	// TODO: Add your control notification handler code here
	_bLC2BELACK = !_bLC2BELACK;
}

void CSysBtnDlg::OnButtonLc3Barrier() 
{
	// TODO: Add your control notification handler code here
	_bLC3NKXPR = !_bLC3NKXPR;
}

void CSysBtnDlg::OnButtonLc3BellAck() 
{
	// TODO: Add your control notification handler code here
	_bLC3BELACK = !_bLC3BELACK;
}

void CSysBtnDlg::OnButtonLc4Barrier() 
{
	// TODO: Add your control notification handler code here
	_bLC4NKXPR = !_bLC4NKXPR;
}

void CSysBtnDlg::OnButtonLc4BellAck() 
{
	// TODO: Add your control notification handler code here
	_bLC4BELACK = !_bLC4BELACK;
}

void CSysBtnDlg::OnButtonLc5Barrier() 
{
	// TODO: Add your control notification handler code here
	_bLC5NKXPR = !_bLC5NKXPR;
}

void CSysBtnDlg::OnButtonLc5BellAck() 
{
	// TODO: Add your control notification handler code here
	_bLC5BELACK = !_bLC5BELACK;
}

void CSysBtnDlg::OnButtonUepkHepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRN = !_bNKWEPRN;
}

void CSysBtnDlg::OnButtonDepkIepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRS = !_bNKWEPRS;
}

void CSysBtnDlg::OnButtonAepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRA = !_bNKWEPRA;
}

void CSysBtnDlg::OnButtonBepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRB = !_bNKWEPRB;
}

void CSysBtnDlg::OnButtonCepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRC = !_bNKWEPRC;
}

void CSysBtnDlg::OnButtonDepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRD = !_bNKWEPRD;
}

void CSysBtnDlg::OnButtonEepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRE = !_bNKWEPRE;
}

void CSysBtnDlg::OnButtonFepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRF = !_bNKWEPRF;
}

void CSysBtnDlg::OnButtonGepkKey() 
{
	// TODO: Add your control notification handler code here
	_bNKWEPRG = !_bNKWEPRG;
}

void CSysBtnDlg::OnButtonAllHpkKey() 
{
	// TODO: Add your control notification handler code here
	_bHPK = !_bHPK;
}

void CSysBtnDlg::OnButtonBlockCommUp() 
{
	// TODO: Add your control notification handler code here
	_bBlockUp = !_bBlockUp;
}

void CSysBtnDlg::OnButtonBlockCommDown() 
{
	// TODO: Add your control notification handler code here
	_bBlockDown = !_bBlockDown;
}

void CSysBtnDlg::OnButtonBlockCommMid() 
{
	// TODO: Add your control notification handler code here
	_bBlockMid = !_bBlockMid;
}

void CSysBtnDlg::OnButtonCtcRequest() 
{
	// TODO: Add your control notification handler code here
	_bCTCRequest = TRUE;
}

void CSysBtnDlg::OnCheckSimmodeSw() 
{
	// TODO: Add your control notification handler code here
	_bSIMModeSW = TRUE;
	_bSIMModeIO = FALSE;
	GetDlgItem(IDC_CHECK_SIMMODE_SW)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_SIMMODE_IO)->EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CSysBtnDlg::OnCheckSimmodeIO() 
{
	// TODO: Add your control notification handler code here
	_bSIMModeSW = FALSE;
	_bSIMModeIO = TRUE;
	GetDlgItem(IDC_CHECK_SIMMODE_SW)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_SIMMODE_IO)->EnableWindow(FALSE);
	UpdateData(FALSE);
}

void CSysBtnDlg::SetIOSIMMode()
{
	SetTimer(100, 1000, NULL);
}
