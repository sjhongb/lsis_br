// ClientSocket.h : header file
//
#include <Afxsock.h>
/////////////////////////////////////////////////////////////////////////////
// CClientSocket command target

#ifndef __CLNTSOCK_H__
#define __CLNTSOCK_H__

class CMemView;

class CClientSocket : public CSocket
{
	DECLARE_DYNAMIC(CClientSocket);
// Attributes
public:
	CMemView* m_pServer;
//	CSocketFile* m_pFile;

// Operations
public:
	CClientSocket( CMemView* pServer );
	virtual ~CClientSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClientSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClientSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////
#endif