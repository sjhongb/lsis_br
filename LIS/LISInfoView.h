// LISInfoView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLISInfoView dialog

class CEITInfo;

class CLISInfoView : public CDialog
{
// Construction
public:
	CLISInfoView( CEITInfo *pInfo, CWnd* pParent = NULL);   // standard constructor

	CEITInfo *m_pInfo;
// Dialog Data
	//{{AFX_DATA(CLISInfoView)
	enum { IDD = IDD_DIALOG_LISINFO };
	int m_nRoute;
	int m_nTrack;
	int m_nSignal;
	int m_nSwitch;
	CString m_strInfoSize;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLISInfoView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLISInfoView)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
