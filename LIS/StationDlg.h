#if !defined(AFX_STATIONDLG_H__17D2ECC0_200C_4397_8640_BAE11014E96F__INCLUDED_)
#define AFX_STATIONDLG_H__17D2ECC0_200C_4397_8640_BAE11014E96F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StationDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStationDlg dialog

class CStationDlg : public CDialog
{
// Construction
public:
	CString m_strSelectedStation;
	CStringList m_listSelectedStation;
	CStationDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CStationDlg)
	enum { IDD = IDD_DIALOG_STATION };
	CListCtrl	m_ctrlStationList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStationDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStationDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkListStation(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDoubleclickedButtonSelMake();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CImageList m_sImg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATIONDLG_H__17D2ECC0_200C_4397_8640_BAE11014E96F__INCLUDED_)
