// EITInfo.cpp : implementation file
//

#include "stdafx.h"
#include "LIS.h"

#include "../include/strlist.h"
#include "EITInfo.h"
#include "ScrView.h"

#include "../include/logic.h"
#include "../include/parser.h"
#include "../include/CtcEmu.h"
#include "mytrack.h"

#include "CRCCCITT.h"
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
extern int _nComRecordSize;
extern int _nSfmRecordSize;
extern char _StationName[];

RICondition _RouteIndicatorInfo;

/////////////////////////////////////////////////////////////////////////////
// CEITInfo

IMPLEMENT_DYNCREATE(CEITInfo, CDocument)

BYTE _pNoTni[256];

CEITInfo::CEITInfo()
{
	_pEITInfo = NULL;

	m_pScrInfo = NULL;
	m_pIOInfo = NULL;
//	m_bInverseLR = FALSE;
	m_bInverseLR = TRUE;	// for 부곡

	short nSizeTrack = SIZE_INFO_TRACK;
	short nSizeSignal = SIZE_INFO_SIGNAL;
	short nSizeSwitch = SIZE_INFO_SWITCH;
	short nSizeLamp = SIZE_INFO_LAMP;
	m_strInfoObjectSize.Format( "TK:%d SG:%d SW:%d IO:%d", nSizeTrack, nSizeSignal, nSizeSwitch, nSizeLamp );
	for ( int k = 0; k < 100; k++)
	{
		for (int i = 0; i < 10; i++)
		{
			if( i < 5 )
			{
				m_RouteIndicatorInfo.AspectCondition[k].bAspectSigCondition[i] = TRUE;
				m_RouteIndicatorInfo.AspectCondition[k].bNonAspectSigCondition[i] = TRUE;
			}
			m_RouteIndicatorInfo.AspectCondition[k].bPointCondition[i] = TRUE;
		}
	}
}

BOOL CEITInfo::OnNewDocument()
{
	return FALSE;

	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CEITInfo::~CEITInfo()
{
	if (m_pScrInfo) delete [] m_pScrInfo;
	if (m_pIOInfo) delete [] m_pIOInfo;

	_pEITInfo = NULL;
}


BEGIN_MESSAGE_MAP(CEITInfo, CDocument)
	//{{AFX_MSG_MAP(CEITInfo)
	ON_COMMAND(ID_CREATEDOWNFILE, OnCreatedownfile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEITInfo diagnostics

#ifdef _DEBUG
void CEITInfo::AssertValid() const
{
	CDocument::AssertValid();
}

void CEITInfo::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEITInfo serialization

void CEITInfo::CreateCRC() {
	UINT RouteCount = *(UINT*)&m_pRouteAddTable[ 0 ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
	memset( m_pRouteCrcTable, 0, sizeof(m_pRouteCrcTable) );
    for (UINT i=0; i<RouteCount; i++) {
        CRC_CCITT Crc;
        char *p = (char*)&m_pInfoMem[ m_pRouteAddTable[i+1].nRouteOffset ];		// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
        short size = (short)m_pRouteSizeTable[i];
        for (short j=0; j<size; j++) {
            Crc.AddChar( *p++ );
        }
        m_pRouteCrcTable[i] = Crc.GetCRC();
    }
}

void invfwrite( void *dest, int size, int count, FILE *fp ) {
	char *p = (char *)dest;
	while (count) {
		int blksize = size;
		char *s = p + blksize - 1;
		while (blksize) {
			fwrite( s, 1, 1, fp );
			s--;
			blksize--;
		}
		p += size;
		count--;
	}
}

void CEITInfo::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		return;
	}

	CString docname = GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "DEB" );
	docname.ReleaseBuffer( );
	CFile dumpfile;
	CFileException e;
	if( dumpfile.Open( docname, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
		m_pDeb = new CDumpContext( &dumpfile );
	}
	else
		m_pDeb = NULL;

//		WORD pRouteTblSize[512];
    UINT nRouteSize;	// 20140516 sjhong - InfoPtr을 WORD -> UINT로 확장
	WORD nTblMax = 0;

	char bf[4096];
	CString token;
	int ontable = 0;
	int GroupNo;
			
	// 20140516 sjhong - RouteAddTable의 크기가 WORD --> UINT로 확장되었으나, 이 값은 진로 개수 카운트와 진로 인덱스의 용도로 동시에 사용되므로 예외적으로 WORD형으로 사용한다.
	WORD &wRouteNo = *(WORD*)&m_pRouteAddTable[ 0 ];
	wRouteNo = 0;
	m_nInfoPtr = 0;

    memset(&m_TableBase, 0 , sizeof(TableBaseInfoType));
	memset(m_pInfoMem, 0, sizeof(m_pInfoMem));			// 연동도표 정보 테이블
	memset(m_pSignalInfoTable, 0, sizeof(m_pSignalInfoTable));
	memset(m_pRouteAddTable, 0, sizeof(m_pRouteAddTable));
	memset(m_pRouteCrcTable, 0, sizeof(m_pRouteCrcTable));
	memset(m_pRouteSizeTable, 0, sizeof(m_pRouteSizeTable));
	memset(m_pInfoGeneral, 0, sizeof(m_pInfoGeneral));
	memset(m_pSwitchInfoTable, 0, sizeof(m_pSwitchInfoTable));

	m_nRoute = 0;

	CMyStrList m_ItemRoute;
	CMyStrList *pList = NULL;
	CString item;
	BYTE major=0, minor=0;



	while ( ar.ReadString( bf,4095 ) ) 
	{
		/*
        for (int i=0; bf[i]; i++)
		{
			bf[i] |= 0x80;
		}
		*/
        for (int i=0; bf[i]; i++)
		{
			bf[i] &= 0x7f;
		}

		CString str = bf;
		if (bf[0] == '@') {
			m_ItemInfo.AddTail( str );
		}
		else 
		{
			CLogicParser parser( bf );
			parser.GetToken( token );
			if (!strcmp( token, "TRACK:" )) {
				pList = &m_strTrack; goto loadit;
			}
			else if (!strcmp( token, "SIGNAL:" )) {
				pList = &m_strSignal; goto loadit;
			}
			else if (!strcmp( token, "SWITCH:" )) {
				pList = &m_strSwitch; goto loadit;
			}
			else if (!strcmp( token, "BUTTON:" )) {
				pList = &m_strButton; goto loadit;
			}
			else if (!strcmp( token, "LAMP:" )) {
				pList = &m_strLamp; goto loadit;
			}
			else if (!strcmp( token, "VERSION:" )) 
			{
				parser.GetToken( token );
				ASSERT( token == "{" );
				parser.GetToken( token );
				CLogicParser line( (char*)(LPCTSTR)token, "." );

				line.GetToken( item );
				major = item.GetAt(0) - 48;

				line.GetToken( item );
				line.GetToken( item );
				minor = item.GetAt(0) - 48;

				continue;
			}
			else {
				m_ItemRoute.AddTail( str );
				continue;
			}
loadit:
			parser.GetToken( token );
			ASSERT( token == "{" );
			parser.GetToken( token );
			CLogicParser line( (char*)(LPCTSTR)token, "," );

			while ( line.GetToken( item ) ) 
			{
				if (item != ",") 
				{
					int n = item.Find( '.' );
					if (n>0) 
						item = item.Left( n );

					pList->CheckAdd( item, 1 );
				}
			}
		}
	}
	CreateScrInfo();

	if ( major<16  &&  minor<16 )
	{
		m_pCommonTrack[255] = 0;
		m_pCommonTrack[255] |= major;
		m_pCommonTrack[255] <<= 4;
		m_pCommonTrack[255] |= minor;
	}

	POSITION pos;
	CString pstrSubItems[20];

	//char lbf[768];
	char lbf[4096];   //for AKR
					  // 20130807 [1200]이었다가 Laksam역에서 버퍼가 넘쳐서 2000으로 수정.
					  // 20140409 다시 4096으로 확장.
    ontable = 0;

	for ( pos = m_ItemRoute.GetHeadPosition(); pos != NULL; ) {
		CString *pStr = (CString *)m_ItemRoute.GetNext( pos );
		strcpy( lbf, *pStr );
		char *p = lbf;
		while ( *p == ' ' || *p == '\t' ) p++;
		CLogicParser parser( p, "{}, " );

		if (!ontable) {
			parser.GetToken( token );
			if (!strcmp( token, "ROUTE:" )) {
				ontable = 1;
				GroupNo = 0;
				m_nRoute++;
			}
			else if (!strcmp( token, "BLOCK:" )) 
			{
				ontable = 3;
				GroupNo = 0;
				m_nRoute++;
			}
		}
		else if ( ontable == 1 || ontable == 3 ) {
			parser.GetToken( token );
			parser.GetToken( token );
			if ( ontable == 1 ) {
				parser.GetToken( token );
				parser.GetToken( token );
				m_strRoute.AddTail( token );
			}
			else {
				parser.GetToken( token );
				parser.GetToken( token );
				m_strRoute.AddTail( token );
			}
			ontable = 0;
		}
	}

	short isOpen = 0;
    ontable = 0;

	CMyStrList listCTC;
	CMyStrList listConfig;

	short nEqNo = 0;
	for ( pos = m_ItemRoute.GetHeadPosition(); pos != NULL; ) 
	{
		CString *pStr = (CString *)m_ItemRoute.GetNext( pos );

		CLogicParser parser( (char*)(LPCTSTR)*pStr,"{}" );
		if (!ontable) 
		{
			parser.GetToken( token );
			if (!strcmp( token, "ROUTE:" )) {
				ontable = 1;
				GroupNo = 0;
				m_nRoute++;
			}
			else if (!strcmp( token, "POINT:" )) {
				ontable = 2;
				GroupNo = 0;
			}
			else if (!strcmp( token, "BLOCK:" )) {
				ontable = 3;
				GroupNo = 0;
			}
			else if (!strcmp( token, "LC:" )) {
				ontable = 4;
				GroupNo = 0;
			}
			else if (!strcmp( token, "LOGIC:" )) {
				ontable = 5;
				GroupNo = 0;
			}
			else if (!strcmp( token, "CTCTABLE:" )) {
				ontable = 6;
				GroupNo = 0;
			}
			else if (!strcmp( token, "CONFIG:" )) {
				ontable = 9;
				GroupNo	= 0;
			}
		}

		if ( ontable == 1 )		// Route 
		{	
			// 20140407 sjhong - 대항진로와 같은 긴 라인 처리를 위하여 줄바꿈의 경우에도 처리하도록 수정
			int lineex = 0;	
			while( parser.GetToken( token ) && ontable ) 
			{
				if (!strcmp( token, "{" )) 
				{
					if ( isOpen == 0 ) 
					{
						isOpen = 1;
						lineex = 0;
						pstrSubItems[ GroupNo ] = "";
					}
					else 
					{
						isOpen++;
					}
				}
				else if (!strcmp( token, "}" )) 
				{
					lineex = 1;

					isOpen--;
					ASSERT( isOpen >= 0 );
					if ( isOpen == 0 ) 
					{
						// 진로테이블은 항상 15줄 이상이 되어야 한다. (15 or 16줄)
						ASSERT( GroupNo == 15 || GroupNo == 16 );

						if ( GroupNo ) 
						{
							ASSERT( m_nInfoPtr < sizeof(m_pInfoMem) );
							nRouteSize = m_nInfoPtr;

							CreateLockTable( pstrSubItems, GroupNo, wRouteNo );

							nRouteSize = m_nInfoPtr - nRouteSize;
							ASSERT ( nRouteSize < 1024 );
							m_pRouteSizeTable[ wRouteNo - 1 ] = (short)nRouteSize;

							if (nTblMax < nRouteSize)
								nTblMax = nRouteSize;
						}

						ontable = 0;
						GroupNo = 0;
					}
				}
				else 
				{
					// 20140407 sjhong - 대항진로와 같은 긴 라인 처리를 위하여 줄바꿈의 경우에도 처리하도록 수정
                    //ASSERT( pstrSubItems[ GroupNo ] == "" );
                    pstrSubItems[ GroupNo ] += token;
				}
			}

			// 20140407 sjhong - 대항진로와 같은 긴 라인 처리를 위하여 줄바꿈의 경우에도 처리하도록 수정
            if(lineex)
			{
				GroupNo++;
				pstrSubItems[ GroupNo ] = "";
			}
		}
		else if ( ontable == 2 )   // Point
		{
			BYTE nTrkID = 0;

			while( parser.GetToken( token ) && ontable ) 
			{
				if (!strcmp( token, "}" )) {
					ontable = 0;
				}
				else if (!strcmp( token, "{" )) {
				}
				else 
				{
					CParser parLine;
					parLine.Create( (char*)(LPCTSTR)token, "," );
					parLine.gettokenl();

					CString strSwitch = parLine.gettokenl();
					int nSwID = m_strSwitch.FindIndex( strSwitch ) + 1;
					ASSERT( nSwID );

					CString pointType = parLine.gettokenl();
					if (pointType == "HANDSW")
					{
						m_pSwitchInfoTable[nSwID].nSwitchType = 0x30;
					}
					else if (pointType == "DOUBLE")
					{
						m_pSwitchInfoTable[nSwID].nSwitchType = 0x20;
					}
					else
					{
						m_pSwitchInfoTable[nSwID].nSwitchType = 0x10;
					}

					for (BYTE nn=0; nn<10; nn++)
					{
						CString strPointKeyType = parLine.gettokenl();
						if (strPointKeyType == "")
							break;

						// 궤도일 경우
						if((strPointKeyType.GetAt(0) == 'W') || (strPointKeyType.Find('T') == 1))
						{
							nTrkID = m_strTrack.FindIndex( strPointKeyType ) + 1;
							m_pTrackInfoTable[ nTrkID ].bSWITCHTRACK = 1;
								
							continue;
						}
						else 
						{
							m_pSwitchInfoTable[nSwID].nPointKeyNo = atoi(strPointKeyType);
							break;
						}
					}					
				}
			}
		}
		else if ( ontable == 3 )	// Block 
		{
			int lineex = 1;
			while( parser.GetToken( token ) && ontable ) 
			{
				if (!strcmp( token, "{" )) 
				{
					if ( isOpen == 0 ) 
					{
						isOpen = 1;
						lineex = 0;
					}
					else {
						isOpen++;
					}
				}
				else if (!strcmp( token, "}" )) 
				{
					isOpen--;
					ASSERT( isOpen >= 0 );
					if ( isOpen == 0 ) 
					{
						if ( GroupNo ) 
						{
							ASSERT( m_nInfoPtr < sizeof(m_pInfoMem) );
							nRouteSize = m_nInfoPtr;
							CreateBlockTable( pstrSubItems, GroupNo, wRouteNo );
							nRouteSize = m_nInfoPtr - nRouteSize;
							ASSERT ( nRouteSize < 128 );
							m_pRouteSizeTable[ wRouteNo-1 ] = (short)nRouteSize;

							if (nTblMax < nRouteSize)
								nTblMax = nRouteSize;
						}
						ontable = 0;
						GroupNo = 0;
					}
				}
				else 
				{
                    ASSERT( pstrSubItems[ GroupNo ] == "" );
                    pstrSubItems[ GroupNo ] = token;
					lineex = 1;
				}
			}
            if ( lineex ) GroupNo++;
            pstrSubItems[ GroupNo ] = "";			
		}
		else if ( ontable == 4 ) // Level Crossing
		{
			while( parser.GetToken( token ) && ontable ) 
			{
				if (!strcmp( token, "}" )) {
					ontable = 0;
				}
				else if (!strcmp( token, "{" )) {
				}
				else {
					CParser parLine;
					parLine.Create( (char*)(LPCTSTR)token, "," );
					CString LCtype = parLine.gettokenl();

					for (BYTE nn=0; nn<10; nn++)
					{
						BOOL bNotInTrack = FALSE;

						CString strTrack = parLine.gettokenl();
						if (strTrack == "")
							break;

						if(strTrack.GetAt(0) == '^')
						{
							bNotInTrack = TRUE;

							strTrack = strTrack.Mid(1);
						}

						int nTrID = m_strTrack.FindIndex( strTrack ) + 1;
						ASSERT( nTrID );

						int iFlagForLC = 0;

						for (; iFlagForLC<6;iFlagForLC++)
						{
							if ( m_strLCName[iFlagForLC] == LCtype)
								break;
						}
						switch(iFlagForLC)
						{
						case 0:
							m_pTrackInfoTable[nTrID].bLC1 = 1;
							break;
						case 1:
							m_pTrackInfoTable[nTrID].bLC2 = 1;
							break;
						case 2:
							m_pTrackInfoTable[nTrID].bLC3 = 1;
							break;
						case 3:
							m_pTrackInfoTable[nTrID].bLC4 = 1;
							break;
						case 4:
							m_pTrackInfoTable[nTrID].bLC5 = 1;
							break;
						default:
							break;
						}

						if(bNotInTrack)
						{
							m_pTrackInfoTable[nTrID].bLCNOTINTRK = 1;
						}
					}					
				}
			}
		}
		else if ( ontable == 5 ) 
		{
			while( parser.GetToken( token ) && ontable ) 
			{
				if (!strcmp( token, "}" )) {
					m_pInfoMem[ m_nInfoPtr++ ] = 0;			// End of Eq
					ontable = 0;
				}
				else if (!strcmp( token, "{" )) {
					m_pInfoGeneral[ nEqNo++ ] = m_nInfoPtr;
				}
				else {
					CreateEquation( *pStr );
					break;
				}
			}
		}
		else if ( ontable == 6 ) 
		{
			int bEnd = 0;
			do 
			{
				CString *pStr = (CString *)m_ItemRoute.GetNext( pos );
				CLogicParser parser( (char*)(LPCTSTR)*pStr,"{}" );

				if ( *pStr ) 
				{
					if ( *pStr != "CTCTABLE_END:" ) 
					{
						listCTC.AddTail( *pStr );
					}
					else 
						bEnd = 1;
				}
				else 
					bEnd = 1;
			} while ( !bEnd );

			ontable = 0;
		}
		else if ( ontable == 9 )
		{
			int bEnd = 0;
			do 
			{
				CString *pStr = (CString *)m_ItemRoute.GetNext( pos );
				CLogicParser parser( (char*)(LPCTSTR)*pStr,"{}" );
				
				if ( *pStr ) 
				{
					if ( *pStr != "CONFIG_END:" ) 
					{
						listConfig.AddTail( *pStr );
					}
					else 
						bEnd = 1;
				}
				else 
					bEnd = 1;
			} while ( !bEnd );

			ontable = 0;
		}
	}

	CFile* f = ar.GetFile();
	SetPathName(f->GetFilePath());

	ASSERT( m_nInfoPtr < sizeof(m_pInfoMem) );

    CreateCRC();

    char *s = (char*)m_TableBase.LGInfo;
    strcpy( s, "LGEIE520, Electronic Interlock System V1.0\r\n");
    strcat( s, "by LGIS, CO., LTD \r\n ");

    CTime t = CTime::GetCurrentTime();
    CString strCreateTime = t.Format( "%Y-%m-%d, %H:%M:%S" );

    sprintf( s + strlen(s), "(CREATED AT %s)", strCreateTime );

	m_TableBase.InfoMem.nStart = 0;
	m_TableBase.InfoMem.nSize = m_nInfoPtr - m_TableBase.InfoMem.nStart;

	UINT nByteLoc = 0;
	m_TableBase.InfoRouteAddTable.nStart = 1;//nByteLoc;
	m_TableBase.InfoRouteAddTable.nSize = 2;//nByteLoc - m_TableBase.InfoRouteAddTable.nStart;
	m_TableBase.InfoSwitchOnTrackTable.nStart = 3;//nByteLoc;
	m_TableBase.InfoSwitchOnTrackTable.nSize = 4;//nByteLoc - m_TableBase.InfoSwitchOnTrackTable.nStart;
	m_TableBase.InfoSignalInfoTable.nStart = 5;//nByteLoc;
	m_TableBase.InfoSignalInfoTable.nSize = 6;//nByteLoc - m_TableBase.InfoSignalInfoTable.nStart;

	CreateIOInfo();
	CreateRouteIndicatorInfo();
	CreateCTCInfo( listCTC );
	CreateConfigList( listConfig );

/*
// Create Binary down file
	FILE *fout = fopen( "DOWN.BIN", "wb" );

	fwrite( &m_nInfoPtr, sizeof(m_nInfoPtr), 1, fout );
	fwrite( m_pInfoMem, m_nInfoPtr, 1, fout );

	fputs( "InfoSignal", fout );
	fwrite( m_pInfoSignal, sizeof(m_pInfoSignal), 1, fout );
	fputs( "InfoTrack", fout );
	fwrite( m_pInfoTrack, sizeof(m_pInfoTrack), 1, fout );
	fputs( "InfoSwitch", fout );
	fwrite( m_pInfoSwitch, sizeof(m_pInfoSwitch), 1, fout );
	fputs( "InfoGeneral", fout );
	fwrite( m_pInfoGeneral, sizeof(m_pInfoGeneral), 1, fout );
	fputs( "RouteAddTable", fout );
	fwrite( m_pRouteAddTable, sizeof(m_pRouteAddTable), 1, fout );
	fputs( "SwitchOnTrackTable", fout );
	fwrite( m_pSwitchOnTrackTable, sizeof(m_pSwitchOnTrackTable), 1, fout );
	fputs( "SignalInfoTable", fout );
	fwrite( m_pSignalInfoTable, sizeof(m_pSignalInfoTable), 1, fout );

	fclose( fout );
*/
	m_Engine.LoadTable( &m_TableBase );

	// Global 변수 m_pVMEM을 member 변수로 pointer할당...
	m_Engine.m_pVMEM = m_pVMEM;

    m_Engine.m_pMsgBuffer = &m_pVMEM[ m_Engine.m_TableBase.InfoGeneralIO.nStart + m_Engine.m_TableBase.InfoGeneralIO.nSize ];
    m_Engine.m_nTNIBase = m_Engine.m_TableBase.InfoGeneralIO.nStart + m_Engine.m_TableBase.InfoGeneralIO.nSize + 16;

	memcpy(m_Engine.m_pCtcElementTable, m_pIOInfo->m_pCtcElementTable, sizeof(m_pIOInfo->m_pCtcElementTable));
	memcpy(m_Engine.m_pConfigListTable, m_pIOInfo->m_pConfigListTable, sizeof(m_pIOInfo->m_pConfigListTable));

	m_nTableSize =
		sizeof(m_TableBase) +
		sizeof(m_pInfoMem) +
		sizeof(m_pInfoGeneral) +
		sizeof(m_pRouteAddTable) +
		sizeof(m_pRouteCrcTable) +
		sizeof(m_pRouteSizeTable) +
		sizeof(m_pSwitchOnTrackTable) +
		sizeof(m_pSignalInfoTable) +
		sizeof(m_pSwitchInfoTable) +
        sizeof(m_pTrackInfoTable) +
		sizeof(m_pCommonTrack);

	ASSERT( m_nTableSize == m_Engine.m_nTableSize );
    USHORT nIOTSize	= sizeof(m_pIOInfo->m_pAssignTable);
	USHORT nCTCSize	= sizeof(m_pIOInfo->m_pCtcElementTable);
	USHORT nCfgSize = sizeof(m_pIOInfo->m_pConfigListTable);
	
    m_TableBase.DownInfo.nMemSize = m_nTableSize + nIOTSize + nCTCSize + nCfgSize;

	CreateAuthorizationCode();

    CRC_CCITT mCrc;
    s = (char*)&m_TableBase;
    s += 2;
	int i;
    for (i = 2; i<m_nTableSize; i++) 
	{
        mCrc.AddChar( *s++ );
    }

    s = (char*)m_pIOInfo->m_pAssignTable;
    for (i = 0; i<nIOTSize; i++) 
	{
        mCrc.AddChar( *s++ );
    }

	s = (char*)m_pIOInfo->m_pCtcElementTable;
    for (i = 0; i<nCTCSize; i++) 
	{
        mCrc.AddChar( *s++ );
    }

	s = (char*)m_pIOInfo->m_pConfigListTable;
    for (i = 0; i<nCfgSize; i++) 
	{
        mCrc.AddChar( *s++ );
    }

    m_TableBase.DownInfo.nCRC = mCrc.GetCRC();

//2000-11-19}

//	FILE *fp = fopen( "LISINFO.TXT", "wt" );
//#define info_out(a1) (fprintf(fp, #a1 " \t = %4.4X %4.4X\n",nAddr,sizeof(a1)),nAddr+=sizeof(a1))
	char pdumy[20];
#define info_out(a1) ( *m_pDeb << #a1 " \t = " << itoa(nAddr,pdumy,16) << " \t " << sizeof(a1) << "\n" ,nAddr += (UINT)sizeof(a1))
	UINT nAddr = 0;
	info_out(m_TableBase);
	info_out(m_pInfoMem);
	info_out(m_pInfoGeneral);
	info_out(m_pRouteAddTable);
	info_out(m_pRouteCrcTable);
	info_out(m_pRouteSizeTable);
	info_out(m_pSwitchOnTrackTable);
	info_out(m_pSignalInfoTable);
	info_out(m_pSwitchInfoTable);
	info_out(m_pTrackInfoTable);
	info_out(m_pCommonTrack);

    info_out(m_pIOInfo->m_pAssignTable);
	info_out(m_pIOInfo->m_pCtcElementTable);
	info_out(m_pIOInfo->m_pConfigListTable);

	if ( m_pDeb ) 
	{
/*
		for (int loc=0; loc<2048;loc++)
		{
			WORD nInterBitAddr = m_pIOInfo->m_pAssignTable[loc].BitPos;
if (loc > 72)
{
	int deb = 1;
}
		}
*/
		*m_pDeb << "\n";
		*m_pDeb << " bNOTRACK        0x0001\n";
		*m_pDeb << " bBLKLTRACK      0x0002\n";
		*m_pDeb << " bBLKRTRACK      0x0004\n";
		*m_pDeb << " bSWITCHTRACK    0x0020\n";
		*m_pDeb << " bDESTTRACK      0x0040\n";
		*m_pDeb << " bALARM          0x0080\n";
		*m_pDeb << " bLC1            0x0100\n";
		*m_pDeb << " bLC2            0x0200\n";
		*m_pDeb << " bLC3            0x0400\n";
		*m_pDeb << " bLC4            0x0800\n";
		*m_pDeb << " bLC5            0x1000\n\n";

//		nTNICount = 0;
		for (i=1; i<=256; i++) {
			CString *pStr = m_strTrack.GetAtIndex(i-1);
			if ( pStr ) {
				char bf[256];

				sprintf( bf, "%3d [%s]\t%4.4X ", i, *pStr, m_pTrackInfoTable[i] );

				if(m_pTrackInfoTable[i].bNOTRACK)		strcat( bf, " NOTRACK      ");
				if(m_pTrackInfoTable[i].bBLKLTRACK)		strcat( bf, " BLKLTRACK    ");
				if(m_pTrackInfoTable[i].bBLKRTRACK)		strcat( bf, " BLKRTRACK    ");
				if(m_pTrackInfoTable[i].bSWITCHTRACK)	strcat( bf, " SWITCHTRACK  ");
				if(m_pTrackInfoTable[i].bDESTTRACK)		strcat( bf, " DESTTRACK    ");
				if(m_pTrackInfoTable[i].bALARM)			strcat( bf, " ALARM        ");
				if(m_pTrackInfoTable[i].bLC1)			strcat( bf, " LC1          ");
				if(m_pTrackInfoTable[i].bLC2)			strcat( bf, " LC2          ");
				if(m_pTrackInfoTable[i].bLC3)			strcat( bf, " LC3          ");
				if(m_pTrackInfoTable[i].bLC4)			strcat( bf, " LC4          ");
				if(m_pTrackInfoTable[i].bLC5)			strcat( bf, " LC5          ");

				strcat( bf, "\n" );

				*m_pDeb << bf;
//				*m_pDeb << i << " [" << *pStr << "] " << m_pTrackInfoTable[i] << "\n";
//				if ( m_pTrackInfoTable[i] & TI_HASTNI ) nTNICount++;
			}
			else break;
		}

		TableDump( *m_pDeb );
		dumpfile.Close();
		delete m_pDeb;
	}

	if (_pScrView) {
		if ( !_pScrView->CreateScreenInfo() ) {
		}
	}
}

void CEITInfo::MakeDownloadFile() {

//2000-11-19 if (0) {
	char BinName[256];
    USHORT nIOTSize = sizeof(m_pIOInfo->m_pAssignTable);
	USHORT nCTCSize = sizeof(m_pIOInfo->m_pCtcElementTable);
	USHORT nCfgSize = sizeof(m_pIOInfo->m_pConfigListTable);

	strcpy( BinName, _StationName );
	strcat( BinName, ".BIN" );
	FILE *fout = fopen( BinName, "wb" );			// Bin
	fwrite( &m_TableBase, m_nTableSize, 1, fout );
	fwrite( m_pIOInfo->m_pAssignTable, nIOTSize, 1, fout );   //8*8*32*2
	fwrite( m_pIOInfo->m_pCtcElementTable, nCTCSize, 1, fout ); // 2048
	fwrite( m_pIOInfo->m_pConfigListTable, nCfgSize, 1, fout ); // 128

	//ASSERT( m_nTableSize + nIOTSize == 0x8000 );
	ASSERT( m_nTableSize + nIOTSize + nCTCSize + nCfgSize == 0x24B08 );// 20140516 Size 확장.

// 오브젝트 명칭 테이블을 추가
	fwrite( &m_nTrack, sizeof(short), 1, fout );
	fwrite( &m_nSignal, sizeof(short), 1, fout );
	fwrite( &m_nSwitch, sizeof(short), 1, fout );
	fwrite( &m_nButton, sizeof(short), 1, fout );
	fwrite( &m_nLamp, sizeof(short), 1, fout );
    m_strTrack.ExportToFileFixed16( fout );
    m_strSignal.ExportToFileFixed16( fout );
    m_strSwitch.ExportToFileFixed16( fout );
    m_strButton.ExportToFileFixed16( fout );
    m_strLamp.ExportToFileFixed16( fout );
    fclose( fout );

/////////////////////////////////////////
// for MOTOROLA CPU, Byte reversing
/////////////////////////////////////////
	char RevBinName[256];
	strcpy( RevBinName, _StationName );
	strcat( RevBinName, ".REV" );
	fout = fopen( RevBinName, "wb" );			// Donw Bin

	invfwrite( &m_TableBase.DownInfo.nCRC, 2, 1, fout);
	invfwrite( &m_TableBase.DownInfo.nReserved, 2, 1, fout);
	invfwrite( &m_TableBase.DownInfo.nMemSize, 4, 1, fout);
	invfwrite( &m_TableBase.InfoMem, 4, 2, fout);
	invfwrite( &m_TableBase.InfoHeader, 2, 16, fout);
	fwrite( &m_TableBase.nSwitchDelay, 216, 1, fout );


	//fwrite( &m_pInfoMem, sizeof(m_pInfoMem), 1, fout );
	UINT RouteCount = *(UINT*)&m_pRouteAddTable[ 0 ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
	UINT nOfsWordCount = sizeof(RouteBasPtr)/sizeof(WORD);
	UINT nWriteSize = 0;
	BYTE *pInfo = m_pInfoMem;
	UINT nSize;
    for (UINT i=0; i<RouteCount; i++) 
	{

		UINT nInfoPtr = *(UINT*)&m_pRouteAddTable[ i+1 ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
        BYTE *p = (BYTE*)&m_pInfoMem[nInfoPtr];
		RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)p;

		ASSERT( p == (pInfo+nWriteSize) );
        nSize = m_pRouteSizeTable[i];
		nWriteSize += nSize;

		// Offset 값은 WORD 이므로 Byte Order 변환하여 파일에 쓴다.
		invfwrite(p, 2, nOfsWordCount, fout );

		// 잔여 데이터 크기 및 다음 작업 위치를 조정한다.
		nSize -= (2 * nOfsWordCount);
		p += (2 * nOfsWordCount);

		// 20140603 sjhong - 진로 정보의 경우 nOfsSignal에 값이 있으나, 폐색 정보의 경우 해당 부분을 사용하지 않는다. 따라서 예외처리가 필요함. 
		if(pRouteBasPtr->nOfsSignal > 0)
		{
			// nOfsSignal offset 위치 앞의 데이터를 먼저 파일에 쓴다.
			fwrite(p, pRouteBasPtr->nOfsSignal - (2 * nOfsWordCount), 1, fout);
			
			// 잔여 데이터 크기 및 포인터 위치를 조정한다.
			nSize -= (pRouteBasPtr->nOfsSignal - (2 * nOfsWordCount));
			p += (pRouteBasPtr->nOfsSignal - (2 * nOfsWordCount));
			
			// 대항 진로 개수 + 1 만큼을 Byte Order 변환하여 파일에 쓴다.
			WORD wConflictRouteCount = *(WORD*)p;
			invfwrite(p, 2, wConflictRouteCount + 1, fout);
			
			// 잔여 데이터 크기 및 포인터 위치를 조정한다.
			nSize -= (2 * (wConflictRouteCount + 1));
			p += (2 * (wConflictRouteCount + 1));
		}
			
		// 나머지 데이터를 파일에 쓴다.
		fwrite(p, nSize, 1, fout);
    }

	nSize = sizeof(m_pInfoMem) - nWriteSize;
	if ( nSize > 0 ) 
	{
		fwrite( pInfo + nWriteSize, nSize, 1, fout );
	}


	invfwrite( &m_pInfoGeneral, 2, sizeof(m_pInfoGeneral)/2, fout );
	invfwrite( &m_pRouteAddTable, 4, sizeof(m_pRouteAddTable)/4, fout );	// 20140516 sjhong - nInfoPtr을 WORD --> UINT로 확장
	invfwrite( &m_pRouteCrcTable, 2, sizeof(m_pRouteCrcTable)/2, fout );
	invfwrite( &m_pRouteSizeTable, 2, sizeof(m_pRouteSizeTable)/2, fout );
	fwrite( &m_pSwitchOnTrackTable, sizeof(m_pSwitchOnTrackTable), 1, fout );

	for (int nS = 0; nS<128; nS++) 
	{
		fwrite( &m_pSignalInfoTable[nS], 6, 1, fout );
		invfwrite( &m_pSignalInfoTable[nS].wRouteNo, 2, 1, fout );
		invfwrite( &m_pSignalInfoTable[nS].wCheckResult, 2, 1, fout );	// 20150206 sjhong - Check Result용 영역으로 초기 데이터는 비어있다.
		fwrite( &m_pSignalInfoTable[nS].m_nLamp, 22, 1, fout );
	}

	fwrite( &m_pSwitchInfoTable, sizeof(m_pSwitchInfoTable), 1, fout );
    fwrite( &m_pTrackInfoTable, sizeof(m_pTrackInfoTable), 1, fout );
	fwrite( &m_pCommonTrack, sizeof(m_pCommonTrack), 1, fout );

	// Assign table write
	invfwrite( m_pIOInfo->m_pAssignTable, 2, nIOTSize/2, fout );   //8*8*32*2
	// CTC table write
	invfwrite( m_pIOInfo->m_pCtcElementTable, 2, nCTCSize/2, fout );   //2048
	// Config table write
	invfwrite( m_pIOInfo->m_pConfigListTable, 2, nCfgSize/2, fout );   //128

// 오브젝트 명칭 테이블을 추가
	invfwrite( &m_nTrack, sizeof(short), 1, fout );
	invfwrite( &m_nSignal, sizeof(short), 1, fout );
	invfwrite( &m_nSwitch, sizeof(short), 1, fout );
	invfwrite( &m_nButton, sizeof(short), 1, fout );
	invfwrite( &m_nLamp, sizeof(short), 1, fout );
    m_strTrack.ExportToFileFixed16( fout );
    m_strSignal.ExportToFileFixed16( fout );
    m_strSwitch.ExportToFileFixed16( fout );
    m_strButton.ExportToFileFixed16( fout );
    m_strLamp.ExportToFileFixed16( fout );
	fclose( fout );

// Create CRC for Motorola binary file
    CRC_CCITT mCrc;
	
	FILE *frev = fopen( RevBinName, "r+b" );
	BYTE d;
	d = fgetc( frev );
	d = fgetc( frev );
    for (i = 2; i < m_TableBase.DownInfo.nMemSize; i++) {
		d = fgetc( frev );
        mCrc.AddChar( d );
    }

    unsigned short nCrc = mCrc.GetCRC();
	fseek( frev, 0L, SEEK_SET );
	invfwrite( &nCrc, sizeof(unsigned short), 1, frev);
	fclose( frev );
/////////////////////////////////////////

// convert BIN file to TORNADO s file
	char SName[256];
	strcpy( SName, _StationName );
	strcat( SName, ".s" );

	FILE *fin = fopen( RevBinName, "rb" );
	fout = fopen( SName, "wt" );

	fprintf( fout, "/* CREATED BY LSIS Co.,LTD. */\n\n	.globl binArrayStart\n	.globl binArrayEnd\n\n" );
	fprintf( fout, "    .data\n\n");
	fprintf( fout, "binArrayStart:\n" );

	int lines = 0;
	long nFileSize = _filelength( fileno( fin ) );
	while ( nFileSize > 0 ) 
	{
		nFileSize--;
		d = fgetc( fin );
		if ( lines == 0 ) fprintf( fout,"    .byte " );

		fprintf( fout, "0x%2.2X", d );
		lines++;

		if (lines < 16 && nFileSize) fprintf( fout,"," );
		else {
			lines = 0;
			fprintf( fout,"\n" );
		}
	}

	fprintf( fout, "binArrayEnd:\n" );
	fclose( fin );
	fclose( fout );
}


CString GetSwitchName( CString &str ) 
{
	if (str == "") return str;
	char bf[32];
	strcpy( bf, str );
	char *p = bf;
// YN 2001.6.23 영문으로 시작되는 Switch처리. ( 109A -> B09A )	
	while (*p && !isdigit(*p)) p++;
	if ( *p ) {
// E
		while (*p) {
			if (!isdigit(*p)) {
				*p = 0;
				break;
			}
			p++;
		}
	}
	CString name = bf;
	return bf;
}


void CEITInfo::CreateBlockTable( CString pLines[], WORD nCount, WORD &wRouteNo )
{	
	static BYTE nOldSignalID = 0xff;

	BYTE nSignalType;

	CString strBlockName = "";
	CString strSameRouteNum = "";
	CString item = "";
	CString sname = "";
	CString strDestTrack;

	CLogicParser line;
	CParser parLine;

	BYTE nID, nSigID;

	CString strSignal;

    BYTE nRouteStatus = 0;
	wRouteNo++;	// one base


// 0 : // 신호기 종류
	parLine.Create( (char*)(LPCTSTR)pLines[ 0 ], "," );	// GRP 0
	CString strSignalType = parLine.gettokenl();
    nSignalType = (BYTE)SignalStrToTypeID( strSignalType );

	strBlockName = parLine.gettokenl();   //RouteName

	// Block의 경우 SignalTrackID에 RouteNo를 저장한다... 
	nSigID = m_strSignal.FindIndex( strBlockName ) + 1;		// one base
	m_pSignalInfoTable[ nSigID ].wRouteNo = wRouteNo;

	m_pRouteAddTable[ wRouteNo ].nRouteOffset = m_nInfoPtr;	
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)&m_pInfoMem[ m_nInfoPtr ];
	
	BYTE *pBlkInfoMem = &m_pInfoMem[ m_nInfoPtr ];
	UINT nInfoPtrBase = m_nInfoPtr;		// 20140516 sjhong - InfoPtr를 WORD --> UINT로 확장

	// Route Table 처리 시 이 정보가 블록 Info임을 구분하기 위한 정보
	pBlkInfoMem[ RT_OFFSET_ROUTESTATUS ] = BLOCK_IDENTIFIER;

// 1 : // Block Tracks
	CString strBlkTrack = pLines[ 1 ];			// GROUP 1
	line.Create( (char*)(LPCTSTR)strBlkTrack, ":" );

	BYTE OffsetBlkT = RT_OFFSET_BLKTRACK1;
	BYTE nItemCount = 0;

	line.GetToken( item );
	while((item != "") && (nItemCount < MAX_BLKTRACK_QTY))
	{
		if(item != ":")
		{
			nID = m_strTrack.FindIndex( item ) + 1;         // 출발점, 신호기가 있는 궤도
			ASSERT(nID);

			pBlkInfoMem[ OffsetBlkT++ ] = (BYTE)nID;			// offset 0

			nItemCount++;
		}
		
		line.GetToken( item );
	}
	
	if(strBlockName == "B1" || strBlockName == "B2")
	{
		pBlkInfoMem[ RT_OFFSET_BLKDIR ] = BLOCKTYPE_DIR_LEFT;
	}
	else if(strBlockName == "B3" || strBlockName == "B4")
	{
		pBlkInfoMem[ RT_OFFSET_BLKDIR ] = BLOCKTYPE_DIR_RIGHT;
	}
	else if(strBlockName == "B11" || strBlockName == "B12")
	{
		pBlkInfoMem[ RT_OFFSET_BLKDIR ] = BLOCKTYPE_DIR_MID;
	}

// 2 : // From Signal
	strSignal = pLines[ 2 ];			// GROUP 2
	nSigID = m_strSignal.FindIndex( strSignal ) + 1;		// one base
	*m_pDeb << strDumpOfSignal(nSigID) << "\n";
	nID = nSigID;
	pBlkInfoMem[ RT_OFFSET_SIGNAL ] = (BYTE)nID;		// offset 2


//  3 : // Dest
	item = pLines[ 3 ];				// GROUP 3
	*m_pDeb << pLines[3] << "\n";

	if (item != "") 
	{
		nID = m_strButton.FindIndex( item ) + 1;
		ASSERT( nID > 0 );
	}
	else 
		nID = 0;

	pBlkInfoMem[ RT_OFFSET_BUTTON ] = (BYTE)nID;		// offset 3, dest button

//  4 : // Prohibit signal  -- 현재는 최대 5개 까지 저장 가능...
	line.Create( (char*)(LPCTSTR)pLines[ 4 ], "," );	// GROUP 4
	*m_pDeb << pLines[4] << "\n";

	BYTE OffsetBlkPrevSig = RT_OFFSET_BLKPREVSIG1;
	nItemCount = 0;

	line.GetToken( item );
	while((item != "") && (nItemCount < MAX_BLKPREVSIG_QTY)) 
	{
		if (item != ",") 
		{
			nSigID = m_strSignal.FindIndex( item ) + 1;		
			ASSERT( nSigID );

			pBlkInfoMem[ OffsetBlkPrevSig++ ] = (BYTE)nSigID;

			nItemCount++;
		}

		line.GetToken( item );
	}

//  5 : // Prohibit block
	item = pLines[ 5 ];				// GROUP 5
	*m_pDeb << pLines[5] << "\n";

	if (item != "") 
	{
		nID = m_strSignal.FindIndex( item ) + 1;
		ASSERT( nID > 0 );
	}
	else
	{
		nID = 0;
	}

	pBlkInfoMem[ RT_OFFSET_PROHIBITBLK ] = (BYTE)nID;

	//  6 : // Communication Channel
	line.Create( (char*)(LPCTSTR)pLines[ 6 ], ", " );		// GROUP 6
	*m_pDeb << pLines[6] << "\n";

	nCount = 0;
	while ( line.GetToken( item ) ) 
	{
		if (item != ",")
		{
			if(item == "PORT1")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 1;
			}
			else if(item == "PORT2")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 2;
			}
			else if(item == "PORT3")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 3;
			}
			else if(item == "PORT4")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 4;
			}
			else if(item == "PORT1_BD")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 5;
			}
			else if(item == "PORT2_BD")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 6;
			}
			else if(item == "PORT3_BD")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 7;
			}
			else if(item == "PORT4_BD")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 8;
			}
			else if(item == "NOPORT")
			{
				pBlkInfoMem[ RT_OFFSET_MAINBLKPORT + nCount++ ] = 0;
			}
		}

		if(nCount == 2)
		{
			break;
		}
	}

	if(nCount == 0)
	{
		if(pBlkInfoMem[RT_OFFSET_BLKDIR] == BLOCKTYPE_DIR_LEFT)
		{
			pBlkInfoMem[RT_OFFSET_MAINBLKPORT]	= 1;
			pBlkInfoMem[RT_OFFSET_SUBBLKPORT]	= 3;
		}
		else if(pBlkInfoMem[RT_OFFSET_BLKDIR] == BLOCKTYPE_DIR_RIGHT)
		{
			pBlkInfoMem[RT_OFFSET_MAINBLKPORT]	= 2;
			pBlkInfoMem[RT_OFFSET_SUBBLKPORT]	= 4;
		}
	}

	//  7 : // Home Arrival Track
	line.Create( (char*)(LPCTSTR)pLines[ 7 ], ", " );		// GROUP 7
	*m_pDeb << pLines[7] << "\n";

	line.GetToken( item );
	if (item != "") 
	{	
		nID = m_strTrack.FindIndex( item ) + 1;         // Home Signal 내방 첫 궤도
		ASSERT(nID);
		
		pBlkInfoMem[ RT_OFFSET_BLKARRIVETK ] = (BYTE)nID;	
	}

	m_nInfoPtr = nInfoPtrBase + RT_OFFSET_SUBBLKPORT + 1;
}


void CEITInfo::CreateLockTable(CString pLines[], WORD nCount, WORD &wRouteNo) 
{	
	static BYTE nOldSignalID = 0xff;

	BYTE nSignalType;
    BYTE nFromTrackID = 0;
	BYTE nDestTrackID = 0;
	BYTE nMultiDestTrackID = 0;
	BYTE nStayDestTrackID = 0;
	UINT nInfoPtr;

	BYTE *pInfoCountPtr = NULL;
	BYTE nDelay;
	CString strRoute = "";
	CString strSameRouteNum = "";
	CString item = "";
	CString sname = "";
	CString strDestTrack;

	short n;

	CLogicParser line;
	CParser parLine;

	BYTE nID, nSigID, nOverTrkID, nOverTime;

	CString strSignal;

	BYTE pTrack[64] = {0,};
	BYTE pRoute[64] = {0,};
	WORD pSignal[512] = {0,};	// byte -> word, 128 --> 512
	BYTE pSwitch[64] = {0,};
	BYTE pFlank[64] = {0,};
	BYTE pLamp[64] = {0,};	// 20140625 sjhong - 마주보는 신호기 조건 처리

    BYTE nRouteStatus = 0;
	wRouteNo++;	// one base

// 0 : // 신호기 종류
	item = pLines[ 0 ];		// GROUP 0
	*m_pDeb << "\n\n=========" << item << "=========\n";

	sname = item.Left(2);

	parLine.Create( (char*)(LPCTSTR)pLines[ 0 ], "," );	// GRP 0 
	CString strSignalType = parLine.gettokenl();
	strRoute = parLine.gettokenl();   //RouteName

    nSignalType = (BYTE)SignalStrToTypeID( strSignalType );

  	strSameRouteNum = parLine.gettokenl();   //RouteName

	m_pRouteAddTable[ wRouteNo ].nRouteOffset = m_nInfoPtr;
	RouteBasPtr *pRouteBasPtr = (RouteBasPtr *)&m_pInfoMem[ m_nInfoPtr ];
	BYTE *pInfoMem = &m_pInfoMem[ m_nInfoPtr ];
	UINT nInfoPtrBase = m_nInfoPtr;	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장

// 1 : // Signal
	strSignal = pLines[ 2 ];			// GROUP 2
	nSigID = m_strSignal.FindIndex( strSignal ) + 1;	// one base
	pInfoMem[ RT_OFFSET_SIGNAL ] = (BYTE)nSigID;		// offset 2
	*m_pDeb << strDumpOfSignal(nSigID) << "\n";

	// 20140904 sjhong - Direction 정보를 백업
	BYTE nRouteDirection = ((m_pSignalInfoTable[ nSigID ].nSignalType & SIGNALTYPE_DIR_BIT) >> 2);   // 좌,우행 결정...  Left -> 1

    if ( nSignalType == SIGNALTYPE_CALLON )
	{
		nRouteStatus |= RT_CALLON;
		m_pSignalInfoTable[ nSigID ].nSignalType |= SIGNALTYPE_YUDO_BIT;        // 유도신호기
    }
    else if ( nSignalType == SIGNALTYPE_SHUNT )
	{
		nRouteStatus |= RT_SHUNTSIGNAL;
		m_pSignalInfoTable[ nSigID ].nSignalType |= SIGNALTYPE_YUDO_BIT;        // 유도신호기
	}
    else if ( nSignalType == SIGNALTYPE_GSHUNT )
	{
		nRouteStatus |= RT_SHUNTSIGNAL;
		m_pSignalInfoTable[ nSigID ].nSignalType = SIGNALTYPE_GSHUNT;
		m_pSignalInfoTable[ nSigID ].nSignalType |= SIGNALTYPE_YUDO_BIT;        // 유도신호기
	}
    else if ( nSignalType == SIGNALTYPE_ISHUNT ) 
	{
		nRouteStatus |= RT_SHUNTSIGNAL;
		m_pSignalInfoTable[ nSigID ].nSignalType = SIGNALTYPE_ISHUNT;
		m_pSignalInfoTable[ nSigID ].nSignalType |= SIGNALTYPE_YUDO_BIT;        // 유도신호기
	}
    else if ( nSignalType == SIGNALTYPE_ISTART ) 
	{
		m_pSignalInfoTable[ nSigID ].nSignalType = SIGNALTYPE_ISHUNT;
	}

	// 20140904 sjhong - Direction 정보를 복구
	if(nRouteDirection)
	{
		m_pSignalInfoTable[ nSigID ].nSignalType |= SIGNALTYPE_DIR_BIT;
	}

// 2 : // From-To Track
	CString strFromTo = pLines[ 1 ];			// GROUP 1
	line.Create( (char*)(LPCTSTR)strFromTo, ": " );	// 20140515 sjhong - 종착 궤도가 2개인 경우의 파싱을 위해 공백(space)를 delimeter로 추가

	line.GetToken( item );
	nFromTrackID = m_strTrack.FindIndex( item ) + 1;// 출발점, 신호기가 있는 궤도
	pInfoMem[ RT_OFFSET_FROM ] = (BYTE)nFromTrackID;// offset 0

	line.GetToken( item );	// ':'

	line.GetToken( item );
	nDestTrackID = m_strTrack.FindIndex( item ) + 1;// 도착점, 버튼이 있는 궤도
	pInfoMem[ RT_OFFSET_TO ] = (BYTE)nDestTrackID;	// offset 1

	// 20140515 sjhong - 종착 궤도가 여러개인 경우에 대한 처리 추가
	BYTE nMultiDestCount = 0;

	line.GetToken( item );
	if(item == "AND")	// LKM, MYN역의 다중 종착 궤도 처리
	{
		while(line.GetToken( item ))
		{
			if(item == "AND")
			{
				continue;
			}

			nMultiDestTrackID = m_strTrack.FindIndex( item ) + 1;// 도착점, 버튼이 있는 궤도

			ASSERT( nMultiDestTrackID );

			pInfoMem[ RT_OFFSET_MULTIDEST ]++;
		}
	}
	else if(item == "OR")	// MYN역의 체류 해정 종착 궤도 처리
	{
		line.GetToken( item );

		nStayDestTrackID = m_strTrack.FindIndex( item ) + 1;// 도착점, 버튼이 있는 궤도

		ASSERT( nStayDestTrackID );

		pInfoMem[ RT_OFFSET_MULTIDEST ] = TRACK_STAY_DEST | nStayDestTrackID;
	}

//  3 : // Dest Button
	item = pLines[ 3 ];				// GROUP 3
	if (item != "") 
	{
		nID = m_strButton.FindIndex( item ) + 1;
		ASSERT( nID > 0 );
		*m_pDeb << strDumpOfButton(nID) << "\n";
	}
	else
	{
		nID = 0;
	}
	pInfoMem[ RT_OFFSET_BUTTON ] = (BYTE)nID;			// offset 3, dest button

// 시작 신호기가 같은 종류 Indexing
	if (nSigID != nOldSignalID) 
	{
		nOldSignalID = nSigID;
		m_pSignalInfoTable[ nSigID ].wRouteNo = wRouteNo;	// one base
	}
	m_pSignalInfoTable[ nSigID ].nRouteCount++;


	// 20130114 sjhong - 동일 Signal로부터 시작되는 진로에 대하여 Route Order 값을 저장함. (1 base)
	pInfoMem[RT_OFFSET_ROUTEORDER]	= m_pSignalInfoTable[ nSigID ].nRouteCount;

	// 20141227 sjhong - 동일 Signal로부터 시작되는 진로 중 Alphabet 인덱스가 붙은 경우 이 인덱스를 저장함. (CTC 제어를 위해)
	if(strRoute.ReverseFind('-') > 0)
	{
		pInfoMem[RT_OFFSET_ROUTETAG] =	strRoute.GetAt(strRoute.GetLength() - 1); 
	}
	
	nRouteStatus |= atoi(strSameRouteNum);
	pInfoMem[RT_OFFSET_ROUTESTATUS] = nRouteStatus;	// offset 4, Route Status

	// offset 5 ~ 8, Depend signal
	for(BYTE cSigOffset = RT_OFFSET_DEPENDSIG_BEGIN ; cSigOffset <= RT_OFFSET_DEPENDSIG_END ; cSigOffset++)
	{
		pInfoMem[cSigOffset] = 0; 
	}

	pInfoMem[RT_OFFSET_PREVSIGNAL] = 0;				// offset 5, Previous signal
	pInfoMem[RT_OFFSET_APPRCH_TIME] = 0;				// offset 16, Approach Timer
	pInfoMem[RT_OFFSET_BERTH_TIME] = 0;				// offset 15, Berth Timer

	for(BYTE cTrkOffset = RT_OFFSET_APPRCHTK_BEGIN ; cTrkOffset <= RT_OFFSET_APPRCHTK_END ; cTrkOffset++)
	{
		pInfoMem[cTrkOffset] = 0;
	}

	BYTE &refnRouteStatus = pInfoMem[ RT_OFFSET_ROUTESTATUS ];		

//  4 : // Switch lock
// COUNT SW1 SW2 SG3 SW3 00 SG4 SW4 00  최종은 역순으로
// COUNT TRK TM SW4 SG4 SW3 SG3 00 SW2 SW1

// COUNT SG SW SG SW 00 SW 00 SW
//
	line.Create( (char*)(LPCTSTR)pLines[ 4 ], ", " );		// GROUP 4
	*m_pDeb << pLines[4] << "\n";

	nOverTime = 0;
	nInfoPtr = 0;
	pInfoCountPtr = &pSwitch[ nInfoPtr++ ];
	*pInfoCountPtr = 0;

	BOOL bIsOutOfRouteSW = FALSE;

	while ( line.GetToken( item ) ) 
	{
		if (item != ",") 
		{
			if (isdigit(item.GetAt(0))) 
			{
				nOverTime = atoi(item);
				continue;
			}

			short l = item.GetLength();

			if(item == "AND")
			{
				ASSERT(nInfoPtr - 2 >= 0);
				ASSERT( pSwitch[ nInfoPtr ] == 0 );
				pSwitch[ nInfoPtr - 2 ] |= SWT_COMBO_MASTER;
				pSwitch[ nInfoPtr ] |= SWT_COMBO_SLAVE;

				continue;	// 다음 토큰을 가져온다.
			}
			else if (item.GetAt(0)=='&') 
			{	// DETECTION ONLY	        
				pSwitch[ nInfoPtr++ ] |= SWT_DETECT_ONLY;
				(*pInfoCountPtr)++;
				item = item.Right( l-1 );
			}
			else if (item.GetAt(0)=='*') 
			{	// FORCED MOVING
				pSwitch[ nInfoPtr++ ] |= SWT_FORCED_MOVE;
				(*pInfoCountPtr)++;
				item = item.Right( l-1 );
			}
			else 
			{
				nInfoPtr++;
				(*pInfoCountPtr)++;
			}

			pSwitch[ nInfoPtr++ ] |= GetStrToSwID( item );
			(*pInfoCountPtr)++;

			CString posstr;
			BYTE posid;
            posid = pSwitch[nInfoPtr-1] & 0x7F; 
			if (pSwitch[nInfoPtr-1] & 0x80)
			{
				posstr.Format( "[R]");
			}
			else
			{
				posstr.Format( "[N]");
			}
			*m_pDeb << strDumpOnlyID(pSwitch[nInfoPtr-2]) << strDumpOfSwitch(posid) << posstr;
		}
	}


// 5 : // Signal lock

	line.Create( (char*)(LPCTSTR)pLines[ 5 ], "," );		// GROUP 5
	*m_pDeb << "\n" << pLines[5] << "\n";

 	// 20140515 sjhong - 대항진로 Table을 BYTE(1byte) --> WORD(2bytes) 로 확장한다.
	WORD *pConflictRouteCount	= pSignal;		// size 값도 2Bytes로 확장
	WORD *pConflictRoute		= pSignal + 1;

	*pConflictRouteCount = 0;	// 초기화

	while( line.GetToken(item) ) 
	{
		// 20140407 대항진로에 진로 번호에 스페이스가 있으면 제거함.
		item.Replace(" ", "");

		if(item != ",")
		{
			WORD wConflictRouteID = m_strRoute.FindIndex( item ) + 1;

			ASSERT( wConflictRouteID );
			*pConflictRoute = wConflictRouteID;

			pConflictRoute++;			// 대항 진로 저장 위치 이동 (+2bytes)
			(*pConflictRouteCount)++;	// 대항 진로 개수 증가
		}
	}

	for(WORD wPos = 1 ; wPos <= *pConflictRouteCount ; wPos++)
	{
		*m_pDeb << strDumpOfRoute(pSignal[wPos]);
    }

// Overlap 해정궤도는 Track에서 강제적으로 정해 즐 수 있다.
// 6 : // Track
	line.Create( (char*)(LPCTSTR)pLines[ 6 ], "," );		// GROUP 6
	*m_pDeb << "\n" << pLines[6] << "\n";
//
	BYTE nSignalClearOnlyTrackID=0, nTrackCount=0;
	nOverTrkID = 0;
	pInfoCountPtr = &pTrack[ 0 ];
	*pInfoCountPtr = 0;
	while ( line.GetToken( item ) ) 
	{
 		if (item != ",") 
		{
			if (item.GetAt(0) == '&') 
			{		// 오버랩 해정 궤도만 먼저 찾음, 뒤에 전철기 Overlap 해정 시간에 이용한다.
				item = item.Right( item.GetLength() - 1 );
				nID = m_strTrack.FindIndex( item ) + 1;
				ASSERT( nID );

				if (nSignalType != SIGNALTYPE_INBLOCK)
                    nOverTrkID = nID;

    			//pTrack[ ++(*InfoCountPtr) ] = (BYTE)nID;
			}
	        if (item.GetAt(0) == '$')  // 항상 궤도정보(pLines[6]) 끝에 위치 할 것이다.
			{
				item = item.Right( item.GetLength() - 1 );
				nID = m_strTrack.FindIndex( item ) + 1;
				ASSERT( nID );

			    nSignalClearOnlyTrackID = nID | 0x80;
			}
			nTrackCount++;
		}
	}

	nInfoPtr = MakeTrackAtSwitchTable( pLines[ 6 ], pTrack + 1 );

	if ( pTrack[nTrackCount*2-1] == (nSignalClearOnlyTrackID & 0x7f) ) 
        pTrack[nTrackCount*2-1] = nSignalClearOnlyTrackID;

	pTrack[ 0 ] = (BYTE)nInfoPtr;
/*
	for (ii=1; ii<=pTrack[0];ii++)
	{
		if (pTrack[ii] != 0)
	        *m_pDeb << strDumpOfTrack(pTrack[ii]);
    }
*/

// 7 : // Route
	line.Create( (char*)(LPCTSTR)pLines[ 7 ], "," );		// GROUP 7
	*m_pDeb << "\n" << pLines[7] << "\n";
//
	pInfoCountPtr = &pRoute[ 0 ];
	*pInfoCountPtr = 0;
	pRoute[1] = 0;
	while ( line.GetToken( item ) ) 
	{
		if (item != ",") 
		{
			nID = m_strTrack.FindIndex( item ) + 1;
			ASSERT( nID );

// 중복 ID가 있으면 에러처리
			for (WORD i = 0; i < *pInfoCountPtr; i++) 
			{
				ASSERT ( pRoute[i+1] != nID );
			}
//
			pRoute[ ++(*pInfoCountPtr) ] = (BYTE)nID;
			pRoute[ (*pInfoCountPtr)+1 ] = 0;
		}
	}

	if (nDestTrackID) 
	{
		m_pTrackInfoTable[ nDestTrackID ].bDESTTRACK = 1;

		short find = 0;
		for (short i=0; i<*pTrack; i++) 
		{	// 철사쇄정에 있는가?
			if (nDestTrackID == pTrack[i+1]) {
				find = 1;
				break;
			}
		}


//입환신호기 진로의 경우 진로에 도착점 추가
		if (nRouteStatus & RT_SHUNTSIGNAL || nRouteStatus & RT_CALLON)
		{
			find = 1;
		}
//
		if (find) 
		{	// 철사쇄정에 있고 진로에 없으면 ROUTE에 추가
			for (short i=0; i<*pRoute; i++) 
			{	// 진로쇄정에 있는가?
				if (nDestTrackID == pRoute[i+1]) 
				{
					find = 0;
					break;
				}
			}	
			if (find) 
			{
				pRoute[ ++(*pInfoCountPtr) ] = nDestTrackID;
			}
		}
	}

	for (int ii=1; ii<=pRoute[0];ii++)
	{
	    *m_pDeb << strDumpOfTrack(pRoute[ii]);
    }

// 오버랩 해정 궤도 - 진로의 마지막 궤도를 지정 -> 도착점 궤도로 변경
	if (!nOverTrkID) 
	{
		n = pRoute[0];
		if (nSignalType != SIGNALTYPE_INBLOCK)
		{
		    if (n>0) 
				nOverTrkID = nDestTrackID;
		}
	}

	// 해당 신호기의 첫 진로에서 첫 궤도의 값을 저장한다.
	if( m_pSignalInfoTable[ nSigID ].nRouteCount == 1 )
	{
		m_pSignalInfoTable[ nSigID ].nFirstTrackID = pRoute[1];
	}

// 철사쇄정을 읽은후 스위치 중에 철사 쇄정에 포함되지 않은 스위치를 찾아 오버랩으로 처리함.
// 항상 뒤쪽에 있을 것.

// OLD
// COUNT SW1 SW2 SG3 SW3 00 SG4 SW4 00
// 최종은 역순으로
// COUNT TRK TM SW4 SG4 SW3 SG3 00 SW2 SW1
// NEW
// COUNT SG SW SG SW 00 SW 00 SW
// COUNT TRK TM 00 SW4 SG4 00 SW3 00 SW2 00 SW1 FF(OVR) 
	m_nInfoPtr = nInfoPtrBase + RT_OFFSET_SWITCH;

	pInfoCountPtr = &m_pInfoMem[ m_nInfoPtr++ ];
	*pInfoCountPtr = 0;
	m_pInfoMem[ m_nInfoPtr++ ] = nOverTrkID;
	(*pInfoCountPtr)++;
	m_pInfoMem[ m_nInfoPtr++ ] = nOverTime;
	(*pInfoCountPtr)++;

	for ( n=pSwitch[0]; n>0; ) 
	{
		nID = pSwitch[ n-- ];
		m_pInfoMem[ m_nInfoPtr++ ] = (BYTE)nID;
		(*pInfoCountPtr)++;
		BYTE nOverlap = pSwitch[ n-- ];
		if (nOverlap) 
		{	// 조건부 쇄정
 			if(!SwitchFindAtTrack(nID, pRoute, nDestTrackID)) 
			{	// 진로(신호제어)중에 속한 스위치인가?
				if(!(nOverlap & SWT_COMBO_SLAVE))
				{
					nOverlap |= SWT_IN_OVERLAP;
				}
			}
			
			m_pInfoMem[ m_nInfoPtr++ ] = (BYTE)nOverlap;
			(*pInfoCountPtr)++;
		}
		else 
		{
			m_pInfoMem[ m_nInfoPtr++ ] = 0;
			(*pInfoCountPtr)++;
		}
	}

// signal copy
	// 20140515 sjhong - 대항진로 Table을 BYTE(1byte) --> WORD(2bytes) 로 확장한다.
	pRouteBasPtr->nOfsSignal = m_nInfoPtr - nInfoPtrBase;
	nInfoPtr = (*pSignal + 1) * sizeof(WORD);
	memcpy(&m_pInfoMem[ m_nInfoPtr ], pSignal, nInfoPtr);

	m_nInfoPtr += nInfoPtr;

// track copy
	pRouteBasPtr->nOfsTrack = m_nInfoPtr - nInfoPtrBase;
	nInfoPtr = (*pTrack) + 1;
	memcpy(&m_pInfoMem[ m_nInfoPtr ], pTrack, nInfoPtr );
	m_nInfoPtr += nInfoPtr;

// Route copy
	pRouteBasPtr->nOfsRoute = m_nInfoPtr - nInfoPtrBase;
	nInfoPtr = (*pRoute) + 1;
	memcpy(&m_pInfoMem[ m_nInfoPtr ], pRoute, nInfoPtr );
	m_nInfoPtr += nInfoPtr;

// Approach Track & Timer
	nID = 0;
	line.Create( (char*)(LPCTSTR)pLines[ 8 ], "," );		// GROUP 6
	*m_pDeb << "\n" << pLines[8] << "\n";

	BYTE cOffset = RT_OFFSET_APPRCHTK_BEGIN;

	while( line.GetToken(item) && (cOffset <= RT_OFFSET_APPRCHTK_END) ) 
	{
		if (item != ",") 
		{
			nID = m_strTrack.FindIndex( item ) + 1;
			if (nID != 0)
			{
				pInfoMem[cOffset++] = (BYTE)nID;
			}

			*m_pDeb << strDumpOfTrack(nID);
		}
	}

	line.Create( (char*)(LPCTSTR)pLines[ 9 ], "," );		// GROUP 9
	*m_pDeb << "\n" << pLines[9] << "\n";

	line.GetToken( item );	// APPROACH LOCK TIME
	if(!item.IsEmpty())
	{
		item.TrimRight("Ss \t");
		nDelay = atoi(item);
		if(nDelay > 0)
		{
			pInfoMem[RT_OFFSET_APPRCH_TIME] = (BYTE)nDelay;
		}

		*m_pDeb << strDumpOnlyID(nDelay);
	}
	else
	{
		if (nSignalType == SIGNALTYPE_CALLON || nSignalType == SIGNALTYPE_SHUNT || nSignalType == SIGNALTYPE_GSHUNT || nSignalType == SIGNALTYPE_ISHUNT || nSignalType == SIGNALTYPE_ISTART)
		{	
			pInfoMem[RT_OFFSET_APPRCH_TIME] = (BYTE)30;
		}
		else
		{
			pInfoMem[RT_OFFSET_APPRCH_TIME] = (BYTE)120;
		}
	}

	if (nSignalType == SIGNALTYPE_CALLON || nSignalType == SIGNALTYPE_SHUNT || nSignalType == SIGNALTYPE_GSHUNT || nSignalType == SIGNALTYPE_ISHUNT || nSignalType == SIGNALTYPE_ISTART)
	{
		line.GetToken( item );	// ','
		line.GetToken( item );	// TRACK BERTH TIME
		if(!item.IsEmpty())
		{
			item.TrimRight("Ss \t");
			nDelay = atoi(item);
			if(nDelay >= 0)	// BERTH TIME은 0일수도 있다.
			{
				pInfoMem[RT_OFFSET_BERTH_TIME] = (BYTE)(nDelay & 0x7F) | TRACK_BERTH_MASK;
			}
			
			*m_pDeb << strDumpOnlyID(nDelay);
		}
		else
		{
			pInfoMem[RT_OFFSET_BERTH_TIME] = (BYTE)30 | TRACK_BERTH_MASK;
		}
	}

// 10 : Aspect
	pRouteBasPtr->nOfsAspect = m_nInfoPtr - nInfoPtrBase;
	item = pLines[10];			// GROUP 10, Aspect
	*m_pDeb << "\n" << pLines[10] << "\n";

	CParser aspect( item, ",+" );
	CString token;
	BYTE nAspect = 0;
	BYTE &nAspectCnt = m_pInfoMem[ m_nInfoPtr++ ];

	nAspectCnt = 0;
	while ( aspect.GetToken( token ) ) 
	{
		if ( token == "," ) 
		{
			m_pInfoMem[ m_nInfoPtr++ ] = nAspect;
			nAspectCnt++;
			nAspect = 0;
		}
		else if ( token == "+" ) 
		{
		}
		else if ( token == "R" ) nAspect |= ASPECT_R;

		else if ( token == "C" ) nAspect |= ASPECT_CO;	// R 은 오프시킴.
//
		else if ( token == "YY" ) 
		{
			nAspect |= ASPECT_Y;
			nAspect |= ASPECT_SY;
		}
		else if ( token == "SY" )
		{
			//nAspect |= ASPECT_R;
			nAspect |= ASPECT_SY;
		}
		else if ( token == "Y" ) nAspect |= ASPECT_Y;
		else if ( token == "G" ) nAspect |= ASPECT_G;
		else if ( token == "S" ) 
		{
			nAspect |= ASPECT_SH;
		}
		else if ( token == "LI(L)" ) 
		{
			//refnRouteStatus |= RT_LEFT;
			nAspect |= ASPECT_LL;
		}
		else if ( token == "LI(M)" || token == "LI(R)") 
		{
			//refnRouteStatus |= RT_RIGHT;
			nAspect |= ASPECT_LR;
		}
		else if ( token == "LI(F)" ) 
		{
			refnRouteStatus |= RT_FREESHUNT;
			nAspect |= ASPECT_LL;
		}
		else {
//			ASSERT( 0 );
		}
	}
	m_pInfoMem[ m_nInfoPtr++ ] = nAspect;
	nAspectCnt++;

	for (ii=0; ii<nAspectCnt;ii++)
	{
	    *m_pDeb << strDumpOnlyID(m_pInfoMem[m_nInfoPtr-nAspectCnt+ii]);
    }


// 11 : Ahead signal
	CParser ahead( pLines[ 11 ],"," );
	*m_pDeb << "\n" << pLines[11] << "\n";

	BYTE nAhead = 0;
	BYTE nAheadSigID = 0;
	CString strLast;
	int nSig = 0;
	int nAheadCnt = 0;
	while ( ahead.GetToken( token ) ) 
	{
		if ( token == "," ) 
		{
			strLast = "";
			m_pInfoMem[ m_nInfoPtr++ ] = nAhead;
			nAheadCnt++;
			nAhead = 0;
			continue;
		}
		else if ( token == "AT" ) 
		{
			nAheadSigID = m_strSignal.FindIndex( strLast ) + 1;
			ASSERT( nAheadSigID > 0 );
// AND CONDITION
/*
			if ( nAhead != 0 ) 
			{
				nAhead |= AHEAD_EXT;
				m_pInfoMem[ m_nInfoPtr++ ] = nAhead;
				m_pInfoMem[ m_nInfoPtr++ ] = nAheadSigID;
				nAhead = 0;
			}
*/
			nSig--;
		}
		else if ( token == "OR" ) 
		{
		}
		else if ( token == "R" ) nAhead |= AHEAD_R;
		else if ( token == "Y" || token == "YY" || token == "SY" ) nAhead |= AHEAD_Y;
		else if ( token == "G" ) nAhead |= AHEAD_G;
		else if ( token == "Y+LI(L)" ) 
		{
			nAhead |= AHEAD_Y;
			nAhead |= AHEAD_LL;
		}
		else if ( token == "Y+LI(R)") 
		{
			nAhead |= AHEAD_Y;
			nAhead |= AHEAD_LR;
		}
		else if ( token == "Y+LI" )
		{
			nAhead |= AHEAD_LL;
			nAhead |= AHEAD_LR;
		}
		else if ( token == "Y+LI(M)")
		{
			nAhead |= AHEAD_Y;
			nAhead |= AHEAD_LM;
		}
		else if( token == "G+LI(M)" ) 
		{
			nAhead |= AHEAD_G;
			nAhead |= AHEAD_LM;
		}
		else {
			nSig++;
		}
		strLast = token;
	}
	m_pInfoMem[ m_nInfoPtr++ ] = nAhead;
	nAheadCnt++;
	ASSERT( nSig == 0 );
	ASSERT( nAspectCnt == nAheadCnt );

	for (ii=0; ii<nAheadCnt;ii++)
	{
	    *m_pDeb << strDumpOnlyID(m_pInfoMem[m_nInfoPtr-nAheadCnt+ii]);
    }


// 13: // 연속진로의 신호기
	line.Create( (char*)(LPCTSTR)pLines[ 12 ], "," );		// GROUP 12
//
	line.GetToken( item );
	BYTE cItemCount = 0;

	while (item != "") 
	{
        BOOL bIsBlock = FALSE;

		switch(item.GetAt(0))
		{
		case '#':
            item = item.Right( item.GetLength() - 1 );
			nID = m_strSignal.FindIndex( item ) + 1;
			ASSERT( nID > 0 );

			m_pSignalInfoTable[ nSigID ].nSGContSigID = (BYTE)nID;
			break;

		case '!':	// 신호를 낼수 없는 조건의 폐색(Block) ID
			item = item.Right(item.GetLength() - 1);
			nID = m_strSignal.FindIndex( item ) + 1;
			ASSERT( nID > 0 );

			pInfoMem[ RT_OFFSET_CONFLICTBLK ] = (BYTE)nID;
			break;
			
		case 'N':
			break;

		case '&':	// 중계신호기지만, 주신호와 함께 현시되지 않는 신호기.
			item = item.Right(item.GetLength() - 1);
			nID = m_strSignal.FindIndex( item ) + 1;
			ASSERT( nID > 0 );
			ASSERT( cItemCount != 0 );

			pInfoMem[RT_OFFSET_DEPENDSIG_BEGIN + cItemCount - 1] = (BYTE)nID | SIG_DEPEND_SET_ONLY; 
			break;

		default:
			nID = m_strSignal.FindIndex( item ) + 1;
			ASSERT( nID > 0 );

			if(cItemCount == 0)
			{
				pInfoMem[RT_OFFSET_PREVSIGNAL] = (BYTE)nID;	// 전방 신호기 ID
			}
			else
			{
				pInfoMem[RT_OFFSET_DEPENDSIG_BEGIN + cItemCount - 1] = (BYTE)nID;
			}
			break;
		}

    	line.GetToken( item );
        if(item != ",") 
		{
			break;
		}

    	line.GetToken( item );
		cItemCount++;
	}

// 14: // LevelCrossing 
	line.Create( (char*)(LPCTSTR)pLines[ 13 ], "," );		// GROUP 13
	while ( line.GetToken( token ) ) 
	{
		int iNumberForFlag = 0;
		for(; iNumberForFlag < 5;iNumberForFlag++)
		{
			if (( m_strLCName[iNumberForFlag] == "" || m_strLCName[iNumberForFlag] == token ) && token != "," )
			{
				m_strLCName[iNumberForFlag] = token;
				break;
			}
		}

		switch(iNumberForFlag)
		{
		case 0 :
			pInfoMem[RT_OFFSET_LEVELCROSS] |= FLAG_LC1;
			break;
		case 1 :
			pInfoMem[RT_OFFSET_LEVELCROSS] |= FLAG_LC2;
			break;
		case 2 :
			pInfoMem[RT_OFFSET_LEVELCROSS] |= FLAG_LC3;
			break;
		case 3 :
			pInfoMem[RT_OFFSET_LEVELCROSS] |= FLAG_LC4;
			break;
		case 4 :
			pInfoMem[RT_OFFSET_LEVELCROSS] |= FLAG_LC5;
			break;
		default:
			break;
		}
	}

// 15: // Flank protection 
	pRouteBasPtr->nOfsFlank = m_nInfoPtr - nInfoPtrBase;

	CParser flank( pLines[ 14 ],"," );
	memset (pFlank, NULL, sizeof(pFlank));

	BOOL bIsOnlyRouteSetFlank = FALSE;

	BYTE nCopySize = 1;
	while ( flank.GetToken( item ) ) 
	{
		if (item == ",") 
			continue;

        if (item.GetAt(0) == '=') 
		{
            item = item.Right( item.GetLength() - 1 );
        }

		// 20131119 sjhong - 최초 Signal Clear 이후에는 Flank Protection 처리 안하는 기능 추가
		if (item.GetAt(0) == '$')
		{
			item = item.Right( item.GetLength() - 1 );

			bIsOnlyRouteSetFlank = TRUE;
		}
		else
		{
			bIsOnlyRouteSetFlank = FALSE;
		}

		nID = m_strTrack.FindIndex( item ) + 1;
		ASSERT( nID );

// 중복 ID가 있으면 에러처리
		for (short i = 1; i<nCopySize; i++) 
		{
			ASSERT ( pFlank[i++] != nID );
		}

		// 20131119 sjhong - 최초 Signal Clear 이후에는 Flank Protection 처리 안하는 기능 추가
		pFlank[ nCopySize++ ] = (BYTE)(nID | (bIsOnlyRouteSetFlank ? 0x80 : 0x00));

		flank.GetToken(item);

		if (item == "AT")
		{
			flank.GetToken( item );
			pFlank[ nCopySize++ ] = GetStrToSwID( item );

			flank.GetToken( item );
			if(item == "AND")
			{
				pFlank[ nCopySize++ ] = '&';

				flank.GetToken( item );
				pFlank[ nCopySize++ ] = GetStrToSwID( item );
			}
			else if(item == "OR")
			{
				pFlank[ nCopySize++ ] = '|';
				
				flank.GetToken( item );
				pFlank[ nCopySize++ ] = GetStrToSwID( item );
			}
			else
			{
				pFlank[nCopySize++] = 0;
				pFlank[nCopySize++] = 0;
			}
		}
		else if ((item == ",") || (item == ""))
		{
			pFlank[nCopySize++] = 0;
			pFlank[nCopySize++] = 0;
			pFlank[nCopySize++] = 0;
			continue;
		}
	}

	if (nCopySize>1)
	{
	    pFlank[0] = nCopySize;
	}
	else
	{
	    pFlank[0] = 0;
	}
	
	memcpy (&m_pInfoMem[m_nInfoPtr], pFlank, nCopySize);
	m_nInfoPtr += (nCopySize);

// 16: // Lamp check protection 
	pRouteBasPtr->nOfsLamp = m_nInfoPtr - nInfoPtrBase;
	
	CParser lamp( pLines[ 15 ],"," );
	memset (pLamp, NULL, sizeof(pLamp));
	
	nCopySize = 1;

	while( lamp.GetToken( item ) )
	{
		if (item == ",")
		{
			continue;
		}
		
		nID = m_strSignal.FindIndex( item ) + 1;
		ASSERT( nID );
		
		// 중복 ID가 있으면 에러처리
		for (short i = 1 ; i < nCopySize ; i++)
		{
			ASSERT( pLamp[i++] != nID );
		}
		
		pLamp[ nCopySize++ ] = nID;
	}
	
	if(nCopySize > 1)
	{
		pLamp[0] = nCopySize;
	}
	else
	{
		pLamp[0] = 0;
	}

	memcpy (&m_pInfoMem[m_nInfoPtr], pLamp, nCopySize);
	m_nInfoPtr += (nCopySize);
}


#define MAX_OPERATE_CMD_SIZE 16

CEITInfo *_pEITInfo = NULL;
extern BYTE m_pVMEM[];

CString *pTR[256];
CString *pSG[256];
CString *pSW[256];

BYTE pSwitchDirection[128];


void CEITInfo::CreateScrInfo()
{
	memset( _pNoTni, sizeof(_pNoTni), 0 );

	m_nTrack = m_strTrack.GetCount();
	m_nSignal = m_strSignal.GetCount();
	m_nSwitch = m_strSwitch.GetCount();
	m_nButton = m_strButton.GetCount();
	m_nLamp = m_strLamp.GetCount();

	m_nScrInfoCount = m_nTrack + m_nSignal + m_nSwitch + m_nButton + m_nLamp;
	m_pScrInfo = new CScrInfo[m_nScrInfoCount];  // 전체 Object별 Data를 Heap영역에 저장한다.

	UINT nByteLoc = 0;
	short nNo = 0;

	// 임시 Buffer정의...
	POSITION pos;
	CString *pStr;
	CScrInfo *pInfo;  

	memset( m_pCommonTrack, 0, sizeof(m_pCommonTrack) );
	memset( m_pTrackInfoTable, 0, sizeof(m_pTrackInfoTable) );

// Header에 대한 정보...
	m_TableBase.InfoHeader.nStart = nByteLoc;  // 0
	nByteLoc += sizeof DataHeaderType;
	m_TableBase.InfoHeader.nSize = nByteLoc - m_TableBase.InfoHeader.nStart;

// 여기서 부터 각 Object별 Data정보를 저장한다...
	m_TableBase.InfoTrack.nStart = nByteLoc;

// Dynamic data의 정보를 가지는 m_pScrInfo에 해당되는 정보를 모두 저장한다... 
// 궤도, 신호기, 전철기 등의 순으로 저장...
	short nTrack = 0;
	for ( pos = m_strTrack.GetHeadPosition(); pos != NULL; ) 
	{
		pStr = (CString*)m_strTrack.GetNext( pos );  // pStr --> 이름 저장을 위한 Buffer
		pTR[ ++nTrack ] = pStr;

		pInfo = &m_pScrInfo[ nNo++ ];     // m_pScrInfo에 각 Object별 data를 저장한다... 
		pInfo->m_strName = *pStr;         // Object의 String name
		pInfo->m_nType = SCRINFO_TRACK;   // Object의 Type정보
		pInfo->m_nID = nTrack;            // Object별 ID - element id에 해당.
		pInfo->m_nMemOffset = nByteLoc;   // Object data의 index
		pInfo->m_pEipData = &m_pVMEM[nByteLoc];  // 실제 Object Data 저장을 위한 Buffer의 pointer

		nByteLoc += SIZE_INFO_TRACK;      // index를 증가 시킴.
		ASSERT( nByteLoc < 2048 );
	}

	m_TableBase.InfoTrack.nCount = nTrack;

	m_TableBase.InfoSignal.nStart = nByteLoc;
	short nSignal = 0;
	for ( pos = m_strSignal.GetHeadPosition(); pos != NULL; ) 
	{
		pStr = (CString*)m_strSignal.GetNext( pos );
		pSG[ ++nSignal ] = pStr;

		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nType = SCRINFO_SIGNAL;
		pInfo->m_nID = nSignal;
		pInfo->m_nMemOffset = nByteLoc;
		pInfo->m_pEipData = &m_pVMEM[nByteLoc];

		nByteLoc += SIZE_INFO_SIGNAL;
		ASSERT( nByteLoc < 2048 );
	}
	m_TableBase.InfoSignal.nCount = nSignal;

	m_TableBase.InfoSwitch.nStart = nByteLoc;
	short nSwitch = 0;
	for ( pos = m_strSwitch.GetHeadPosition(); pos != NULL; ) 
	{
		pStr = (CString*)m_strSwitch.GetNext( pos );
		pSW[ ++nSwitch ] = pStr;

		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nType = SCRINFO_SWITCH;
		pInfo->m_nID = nSwitch;
		pInfo->m_nMemOffset = nByteLoc;
		pInfo->m_pEipData = &m_pVMEM[nByteLoc];

		nByteLoc += SIZE_INFO_SWITCH;
		ASSERT( nByteLoc < 2048 );
	}

	m_TableBase.InfoSwitch.nCount = nSwitch;

	m_TableBase.InfoGeneralIO.nStart = nByteLoc;  // 역 공통 표시등을 나타낸다,
	short nLamp = 0;
	for ( pos = m_strLamp.GetHeadPosition(); pos != NULL; ) 
	{
		++nLamp;
		pStr = (CString*)m_strLamp.GetNext( pos );

		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nType = SCRINFO_LAMP;
		pInfo->m_nID = nLamp;
		pInfo->m_nMemOffset = nByteLoc;
		pInfo->m_pEipData = &m_pVMEM[nByteLoc];

		nByteLoc += SIZE_INFO_LAMP;
		ASSERT( nByteLoc < 2048 );
	}

	m_TableBase.InfoGeneralIO.nSize = nByteLoc - m_TableBase.InfoGeneralIO.nStart;

	short nButton = 0;
	for ( pos = m_strButton.GetHeadPosition(); pos != NULL; ) 
	{
		++nButton;
		pStr = (CString*)m_strButton.GetNext( pos );

		pInfo = &m_pScrInfo[ nNo++ ];
		pInfo->m_strName = *pStr;
		pInfo->m_nType = SCRINFO_BUTTON;
		pInfo->m_nID = nButton;
		pInfo->m_nMemOffset = -1;   // 실제 Object Data가 없으므로 Offset를 -1로 함.
		pInfo->m_pEipData = NULL;	// Button에 대한 실제 Object Data는 없다.
	}

	ASSERT( nNo == m_nScrInfoCount );

	_pEITInfo = this;
	m_nScrMemSize = nByteLoc;   // 현재까지 계산된 size가 모든 Object data의 총 size임.

    // 실제 Object data만을 저장할 buffer.
	memset( m_pVMEM, 0, MAX_MSGBLOCK_SIZE );

// Create Track Geographic Info...
// 연동에서 사용하는 참조용, Link용 Data 생성...
	short nTrkID;
	CString token;

	//POSITION pos1;
	//CString *pStr1;

	memset(m_pSwitchOnTrackTable, 0, sizeof(m_pSwitchOnTrackTable));

	for ( pos = m_ItemInfo.GetHeadPosition(); pos != NULL; ) 
	{
		pStr = (CString*)m_ItemInfo.GetNext( pos );
		CParser line( *pStr, "=," );
		line.GetToken( token );

		if ( token == "@TRACK" ) 
		{
			line.GetToken( token );
			ASSERT( token == "=" );
			line.GetToken( token );
			
			if ( token.Find( '@' ) > 0 ) 
			{	
				CParser nn( token, "@" );
				nn.GetToken( token );
			}

			nTrkID = m_strTrack.FindIndex( token ) + 1;

            if ( token.GetAt(0) == 'A' )
			{
				CString TName = token.Left(2);
				if (TName == "A1")
				{
					m_pTrackInfoTable[ nTrkID ].bBLKLTRACK = 1;
				}
				else
				{
					m_pTrackInfoTable[ nTrkID ].bBLKRTRACK = 1;
				}
			}

			// Track의 정보Bit를 입력한다...
			/*
            if ( token.GetAt(0) == 'B' )
			{
				m_pTrackInfoTable[ nTrkID ] |= TI_ENDTRACK;

                // 선로전환기명중 영문자로 시작하는것의 Track명이 영문'B'로 시작할 경우 NotEndTrack.
				for ( pos1 = m_strSwitch.GetHeadPosition(); pos1 != NULL; ) 
				{
					pStr1 = (CString*)m_strSwitch.GetNext( pos1 );
					CString SName = *pStr1;
					if( SName.Left(1) == 'B' )
					{
						SName += 'T';
						if ( token == SName ) 
							m_pTrackInfoTable[ nTrkID ] &= ~TI_ENDTRACK;						
					}
				}

				CString TName = token.Right(2);
				TName = TName.Left(1);

                // 전철기 궤도...   xxxAT, xxxBT
				if( TName == 'W')
				{
					m_pTrackInfoTable[ nTrkID ] &= ~TI_ENDTRACK;
				}
			}
			*/
            if ( token.GetAt(0) == '?' )
			{
                m_pTrackInfoTable[ nTrkID ].bNOTRACK = 1;
			}
        }
		else if ( token == "@NODE" ) 
		{
			line.GetToken( token );
			ASSERT( token == "=" );
			short loc = 4;
			while (line.GetToken( token )) 
			{
				if (token == ",") continue;

				CString strSW2 = "";
				if ( token.Find( '@' ) < 0 ) 
				{	// double slip
					strSW2 = token;
					line.GetToken( token );
					line.GetToken( token );
				}

				CParser nn( token, "@" );
				CString name, node;
				nn.GetToken( name );
				nn.GetToken( node );
				nn.GetToken( node );
				short nID;

				if (node.GetAt(0) == 'B') 
				{
					name = GetSwitchName( name );

					nID = m_strSwitch.FindIndex( name ) + 1;
					ASSERT( nID > 0 );
					ASSERT( nID < 128 );

                    if ( node.GetLength() >= 2 && node.GetAt(1) == '+' ) {
                        pSwitchDirection[ nID ] = 1;        // --> plus
                    }
                    else {
                        pSwitchDirection[ nID ] = 0;        // --> minus
                    }

					BYTE *pb = (BYTE*)&m_pSwitchOnTrackTable[nID];
					BYTE trid = (BYTE)nTrkID;
					if (!*pb) *pb = trid;
					else if (*pb != trid) 
					{
						if ( !*(pb+1) ) *(pb+1) = trid;
						else
							ASSERT( *(pb+1) == trid );
					}
					if (strSW2 != "") 
					{		// double slip
						name = GetSwitchName( strSW2 );
						
						nID = m_strSwitch.FindIndex( name ) + 1;
						ASSERT( nID > 0 );
						pb = (BYTE*)&m_pSwitchOnTrackTable[nID];
						if (!*pb) *pb = trid;
						else if (*pb != trid) {
							if (!*(pb+1) ) *(pb+1) = trid;
							else ASSERT( *(pb+1) == trid );
						}
					}
				}
				else 
				{
					if (name.GetAt(0) != '_' && name.GetAt(0) != '?') 
					{
						nID = m_strTrack.FindIndex( name ) + 1;
						ASSERT( nID > 0 );
//XX						m_pTrackNode[ ptrTrack + loc ] = (BYTE)nID + 1;
					}
					else {
//XX						m_pTrackNode[ ptrTrack + loc ] = 0;
// 종단 궤도
//						if (token == "_@X")
//                          m_pTrackInfoTable[ nTrkID ] |= TI_ENDTRACK;

					}
					nID = 0;
					if (node == "C") nID = 4;
					if (node == "N") nID = 8;
					if (node == "R") nID = 12;
//XX					m_pTrackNode[ ptrTrack + loc + 1] = (BYTE)nID;
					loc += 4;
				}
			}
		}
		else if ( token.Left(8) == "@SIGNAL:" ) 
		{
			token = token.Right(2);
			short loc = -1;
			BYTE cnr;
			BOOL bIsBufStartOrNot = FALSE;

			// loc, cnr의 각 값의 의미는.....
			if (token == "C1") { loc = 6; cnr = 4; }
			else if (token == "C2") { loc = 7; cnr = 4; }
			else if (token == "N1") { loc = 10; cnr = 8; }
			else if (token == "N2") { loc = 11; cnr = 8; }
			else if (token == "R1") { loc = 14; cnr = 12; }
			else if (token == "R2") { loc = 15; cnr = 12; }
			ASSERT( loc > 0 );

			line.GetToken( token );
			ASSERT( token == "=" );
			line.GetToken( token );

			short nSigID = m_strSignal.FindIndex( token ) + 1;
			if ( token.Find("-1") > 0 )
			{
				bIsBufStartOrNot = TRUE;
			}							// 가짜 신호기.
			ASSERT( nSigID > 0 );

			m_pSignalInfoTable[ nSigID ].nSignalTrackID = (BYTE)nTrkID;  //????
			line.GetToken( token );
			line.GetToken( token );
			ASSERT( m_pSignalInfoTable[ nSigID ].nSignalType == 0 );

			if ( bIsBufStartOrNot )
			{
				token = "BUFSTART";
			}							// 가짜 신호기.
            m_pSignalInfoTable[ nSigID ].nSignalType = (BYTE)SignalStrToTypeID( token );

/*
			if ((token == "단선연동출발폐색") || (token == "복선연동출발폐색")) {
				m_pSignalInfoTable[ nSigID ].nSignalType = BLOCKTYPE_INTER_OUT;
			}
			if (token == "구내폐색신호기") {
				m_pSignalInfoTable[ nSigID ].nSignalType = SIGNALTYPE_INBLOCK;
			}
*/

			BYTE cLR = 0;
			line.GetToken( token );
			line.GetToken( token );

 			if (token.GetAt(0) == 'L') 
 				cLR = SIGNALTYPE_DIR_BIT;

			BYTE sig_n = 0;
			BYTE nSigLamp = 0;
			BYTE nSigLampExt = 0;

			BYTE conflict_count = 12;
			while ( line.GetToken( token ) ) 
			{
				if ( token == "YR") {
					sig_n = 0;
					nSigLamp = SG_LAMP_Y + SG_LAMP_R;
				}
				else if ( token == "R" && m_pSignalInfoTable[nSigID].nSignalType == SIGNALTYPE_IN) {
					nSigLamp |= SG_LAMP_LR;
				}
				else if ( token == "R" && m_pSignalInfoTable[nSigID].nSignalType != SIGNALTYPE_IN) {
					sig_n = 0;
					nSigLamp = SG_LAMP_R;
				}
				else if ( token == "GR" ) {
					sig_n = 1;
					nSigLamp = SG_LAMP_G + SG_LAMP_R;
				}
				else if ( token == "GYR" ) {
					sig_n = 2;
					nSigLamp = SG_LAMP_Y + SG_LAMP_G + SG_LAMP_R;
				}
				else if ( token == "C" ) {
					nSigLamp |= SG_LAMP_CALL;
				}
				else if ( token == "S" ) {
					nSigLamp |= SG_LAMP_SHUNT;
				}
				else if ( token == "SY" ) {
					nSigLamp |= SG_LAMP_SY;
				}
				else if ( token == "L" ) {
					nSigLamp |= SG_LAMP_LL;
				}
				else if ( token == "RR" ) {
					nSigLamp |= SG_LAMP_LR;
				}
				else if ( token == "LR" ) {
					nSigLamp |= SG_LAMP_LL;
					nSigLamp |= SG_LAMP_LR;
				}
 				else if ( token == "RI") {
 					nSigLampExt |= SG_LAMP_EXT_RI;
 				}
				else if ( token == 'A' ) {
					m_pSignalInfoTable[ nSigID ].nSGContSigID = 0xff;
				}

				// 2007.7.18  대항신호기 R 검지를 위해 추가 함.....
// 				else if ( token == ',' || token == 'G' || token == "SY" || token == "RI")
// 					continue;
// 				else if (token == "B1" || token == "B2" || token == "B3" || token == "B4" || token == "B11" || token == "B12")
// 				{
// 					BYTE nConflictSigID = m_strSignal.FindIndex( token ) + 1;
// 					ASSERT( nConflictSigID > 0 );
// 					m_pSignalInfoTable[ nSigID ].pApproach[11] = nConflictSigID;
// 				}
// 				else
// 				{
// 					BYTE nConflictSigID = m_strSignal.FindIndex( token ) + 1;
// 					ASSERT( nConflictSigID > 0 );
// 					m_pSignalInfoTable[ nSigID ].pApproach[conflict_count++] = nConflictSigID;
// 				}
			}
			m_pSignalInfoTable[ nSigID ].m_nLamp = nSigLamp;
			m_pSignalInfoTable[ nSigID ].m_nLampExt = nSigLampExt;
			m_pSignalInfoTable[ nSigID ].nSignalType |= (sig_n | cLR);


		}
		else if ( token == "@SWITCH_PAIR" ) 
		{
// 실제 쌍동 관계인 더블슬립 분기에 대해
//  상대방의 쇄정 궤도를 지정
			line.GetToken( token );
			ASSERT( token == "=" );
			CString sw1, sw2;
			line.GetToken( sw1 );
			line.GetToken( token );
			ASSERT( token == "," );
			line.GetToken( sw2 );
			short nSw1, nSw2;
			nSw1 = m_strSwitch.FindIndex( sw1 ) + 1;
			ASSERT( nSw1 > 0 );
			nSw2 = m_strSwitch.FindIndex( sw2 ) + 1;
			ASSERT( nSw2 > 0 );
			BYTE *pSw1Trk = (BYTE*)&m_pSwitchOnTrackTable[nSw1];
			BYTE *pSw2Trk = (BYTE*)&m_pSwitchOnTrackTable[nSw2];
			ASSERT( *(pSw1Trk+1) == 0 );	//	1개의 궤도만 있어야 함.
			ASSERT( *(pSw2Trk+1) == 0 );
			*(pSw1Trk+1) = *pSw2Trk;
			*(pSw2Trk+1) = *pSw1Trk;
		}
		else if ( token == "@TRACK_PAIR" ) 
		{
// 동일 착점 궤도로 지정
			line.GetToken( token );
			ASSERT( token == "=" );
			CString tr1, tr2;
			line.GetToken( tr1 );
			line.GetToken( token );
			ASSERT( token == "," );
			line.GetToken( tr2 );
			short nTr1, nTr2;
			nTr1 = m_strTrack.FindIndex( tr1 ) + 1;
			ASSERT( nTr1 > 0 );
			nTr2 = m_strTrack.FindIndex( tr2 ) + 1;
			ASSERT( nTr2 > 0 );
			ASSERT(m_pCommonTrack[nTr1] == 0);
			ASSERT(m_pCommonTrack[nTr2] == 0);
			m_pCommonTrack[nTr1] = (BYTE)nTr2;
			m_pCommonTrack[nTr2] = (BYTE)nTr1;
		}
        else if ( token == "@TRACK_JOIN" ) {
        }
        else if ( token == "@TRACK_BLOCK" ) 
		{
			line.GetToken( token );
			ASSERT( token == "=" );
			CString tr1, tr2;
			line.GetToken( tr1 );
			line.GetToken( token );
			ASSERT( token == "," );
			line.GetToken( tr2 );
			short nTr1, nTr2;
			nTr1 = m_strTrack.FindIndex( tr1 ) + 1;
			ASSERT( nTr1 > 0 );
			nTr2 = m_strTrack.FindIndex( tr2 ) + 1;
			ASSERT( nTr2 > 0 );
			ASSERT( nTr1 < nTr2 );
        }
        else if ( token == "@TRACK_NOTEND" ) 
		{
			line.GetToken( token );
			ASSERT( token == "=" );
			CString tr;
			line.GetToken( tr );
			short nTr;
			nTr = m_strTrack.FindIndex( tr ) + 1;
//			m_pTrackInfoTable[ nTr ] &= ~TI_ENDTRACK;
		}
        else if ( token == "@TRACK_NOTTNI" ) 
		{
			line.GetToken( token );
			ASSERT( token == "=" );
			CString tr;
			line.GetToken( tr );
			short nTr;
			nTr = m_strTrack.FindIndex( tr ) + 1;
//			m_pTrackInfoTable[ nTr ] &= ~TI_HASTNI;
			_pNoTni[ nTr ] = 1;
//
		}
		else if ( token.Left(6) == "@SPEED" ) {
        }
		else if ( token.Left(5) == "@LAMP" ) {
        }
		else {
			ASSERT( token.GetAt(0) == ';' );
		}
	}
}

void CEITInfo::CreateIOInfo()
{
	CString docname = GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);

	char pNameIOM[80];
	strcpy( pNameIOM, p );
	strcpy( p+l+1, "RDF" );
	strcpy( pNameIOM + l + 1, "IOM" );
	docname.ReleaseBuffer( );
	m_pIOInfo = new CIOList( docname, &m_ItemInfo );

    // TRACK, SIGNAL, SWITCH
	m_pIOInfo->m_pSignal = &m_strSignal;
	m_pIOInfo->m_pTrack = &m_strTrack;
	m_pIOInfo->m_pSwitch = &m_strSwitch;
	m_pIOInfo->m_pGeneral = &m_strLamp;
 
	m_pIOInfo->m_nOffsetSignal = m_TableBase.InfoSignal.nStart;
	m_pIOInfo->m_nOffsetSwitch = m_TableBase.InfoSwitch.nStart;
	m_pIOInfo->m_nOffsetTrack = m_TableBase.InfoTrack.nStart;
	m_pIOInfo->m_nOffsetGeneral = m_TableBase.InfoGeneralIO.nStart;
	m_pIOInfo->m_nOffsetSystemInput = m_TableBase.InfoGeneralIO.nStart + m_TableBase.InfoGeneralIO.nSize + 0x40;

    // example = 0x287 + 0x40 = 0x2C7

	m_pIOInfo->m_pIOCardInfo = &m_TableBase.IOCardInfo[0];
	m_pIOInfo->Create( IDD_DIALOG_IOLIST );
	memcpy( m_TableBase.RealID, m_pIOInfo->GetRealAddressPtr(), sizeof(m_TableBase.RealID) /*64*/ );
}
/////////////////////////////////////////////////////////////////////////////
#define RII_FILE_VERSION  "ROUTE INDICATOR INFO FILE VER1.0"

class CFileBuffer {
	char *m_pFileBuffer;
	long m_lReadPtr;
	long m_lSize;
public:
	CFileBuffer() { m_pFileBuffer = NULL;}
	~CFileBuffer() {
		if(m_pFileBuffer != NULL)
			delete [] m_pFileBuffer;
	}
	BOOL Load( LPCTSTR pFileName );
	BOOL ReadString( CString &str );
	BOOL ReadString( char *buffer, short size );
};

BOOL CFileBuffer::Load( LPCTSTR pFileName ) {
	FILE *pFile = fopen( pFileName, "rb" );
	if (!pFile) return 1;
	m_lSize = _filelength( fileno( pFile ) );
	m_pFileBuffer = new char[m_lSize];
	fread( m_pFileBuffer, m_lSize, 1, pFile );
	fclose( pFile );
	m_lReadPtr = 0L;

	return 0;
}

BOOL CFileBuffer::ReadString( CString &str ) {
	char buffer[2048];
	char c;
	short dest = 0;
	while ( m_lReadPtr < m_lSize ) {
		c = m_pFileBuffer[ m_lReadPtr++ ];
		if (dest && c == '\r') {
			if ( m_pFileBuffer[ m_lReadPtr ] == '\n')
				m_lReadPtr++;
			break;
		}
		else {
			buffer[dest++] = c;
		}
	}
	buffer[dest] = 0;
	if (dest) str = buffer;
	return dest;
}

BOOL CFileBuffer::ReadString( char *buffer, short size ) 
{
	char c;
	short dest = 0;

	memset (buffer, NULL, size);
	while ( m_lReadPtr < m_lSize ) 
	{
		c = m_pFileBuffer[ m_lReadPtr++ ];
		if (dest && c == '\r') 
		{
			if ( m_pFileBuffer[ m_lReadPtr ] == '\n')
				m_lReadPtr++;
			break;
		}
		else {
			buffer[dest++] = c;
		}
	}
	buffer[dest] = 0;
	return dest;
}

void CEITInfo::CreateRouteIndicatorInfo()
{
	CString docname = GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);

	strcpy( p+l+1, "RII");
	docname.ReleaseBuffer( );

	CFileBuffer ar;
	if (ar.Load( docname )) 
		return;
	
	char * cap;
	CString str;
	char bf[2048];
	int i = 0;
	
	ar.ReadString(bf, 2048);
	str = bf;
	
	if( strcmp((char*)(LPCTSTR)str, RII_FILE_VERSION) )	
	{
		AfxMessageBox((LPCTSTR)"DATA 오류\"*.RII\" 입니다.");
		return ;
	}
	
	while ( ar.ReadString(bf, 2048) ) 
	{
		str = bf;
		
		CParser is( (char*)(LPCTSTR)str, "\t\r\n ,():[]" );
		cap = is.gettokenl();
		
		char * token;
		CString strBuf;
		if ( *cap && (*cap != ';') ) 
		{
			m_RouteIndicatorInfo.SigName[i] = cap;
			int iNonSigConditionCount = 0;
			int iSigConditionCount = 0;
			int iPointConditionCount = 0;
			
			m_RouteIndicatorInfo.SigAspect[i] = is.gettokenl();
			
			while( token = is.gettokenl(), *token ) 
			{
				strBuf = token;
				
				if ( strBuf.Find('M') > 0 || strBuf.Find('H') > 0 || strBuf.Find('D') > 0 || strBuf.Find('C') > 0 || strBuf.Find('S') > 0 || strBuf.Find('A') > 0 )
				{
					if ( strBuf.Find('!') == 0)
					{
						m_RouteIndicatorInfo.AspectCondition[i].NonAspectSigCondition[iNonSigConditionCount++] = strBuf.Right(strBuf.GetLength()-1);
					}
					else
					{
						m_RouteIndicatorInfo.AspectCondition[i].AspectSigCondition[iSigConditionCount++] = strBuf;
					}
				}
				else if ( strBuf.Find('N') > 0 || strBuf.Find('R') > 0 )
				{
					m_RouteIndicatorInfo.AspectCondition[i].PointCondition[iPointConditionCount++] = strBuf;
				}
			}
			i++;
		}
	}

	_RouteIndicatorInfo = m_RouteIndicatorInfo;
}

void CEITInfo::CreateCTCInfo( CMyStrList &list )
{
// Create CTC Info

    CString str;
    WORD *pTable = (WORD*)(m_pIOInfo->m_pCtcElementTable);

	char szListBuffer[256];
	if ( m_pDeb ) 
		*m_pDeb << "*** Create DTS Info Table ***\n";

	WORD &nCount = *pTable++;
	nCount = 0;
	POSITION pos;
	for ( pos = list.GetHeadPosition(); pos != NULL; ) 
	{
		CString *pStr = (CString*)list.GetNext( pos );
		if ( pStr && *pStr != "" && pStr->GetAt(0) != ';' ) 
		{
			CRemoteLinkTableType &Link = *(CRemoteLinkTableType*)(pTable++);
			int nNo = nCount++;

			CParser par( *pStr, " " );
			int nID = atoi( par.gettokenl() );
//ASSERT( nID == nNo );
			CString strName = par.gettokenl();

			CTCELEM elem;
			elem.nID	 = nID; 
			elem.strName = strName;
			m_listElement.AddTail(elem);

			int n;
			if ( strName.Find( "MODE" ) == 0 ) {
				Link.m_nType = CTC_LK_MODE;
			}
			else if ( strName.Find( "POWER_E" ) == 0 ) {
				Link.m_nType = CTC_LK_POWER_EXT;
			}
			else if ( strName.Find( "POWER" ) == 0 ) {
				Link.m_nType = CTC_LK_POWER;
			}
			else if ( strName.Find( "COMMON" ) == 0 ) {
				Link.m_nType = CTC_LK_COMMON;
			}
			else if ( strName.Find( "KT_UPDN" ) == 0 ) 
			{
				Link.m_nType = CTC_LK_KT_UPDN;
			}
			else if ( strName.Find( "KT_ABCD" ) == 0 ) 
			{
				Link.m_nType = CTC_LK_KT_ABCD;
			}
			else if ( strName.Find( "CBI" ) == 0 ) {
				Link.m_nType = CTC_LK_CBI;
			}
			else if ( strName.Find( "MCCR" ) == 0 ) {
				Link.m_nType = CTC_LK_MCCR;
			}
			else if ( strName.Find( "ADJCOMM" ) == 0 ) {
				Link.m_nType = CTC_LK_ADJCOMM;
			}
			else if ( strName.Find( "CALLON_L" ) == 0 ) {
				Link.m_nType = CTC_LK_CALLON_LSB;
			}
			else if ( strName.Find( "CALLON_M" ) == 0 ) {
				Link.m_nType = CTC_LK_CALLON_MSB;
			}
			else if ( strName.Find( "ERR_L" ) == 0 ) {
				Link.m_nType = CTC_LK_ERR_LSB;
			}
			else if ( strName.Find( "ERR_M" ) == 0 ) {
				Link.m_nType = CTC_LK_ERR_MSB;
			}
			else if ( strName.Find( "AXLB2_L" ) == 0) {
				Link.m_nType = CTC_LK_AXLB2_LSB;
			}
			else if ( strName.Find( "AXLB2_M" ) == 0 ) {
				Link.m_nType = CTC_LK_AXLB2_MSB;
			}
			else if ( strName.Find( "AXLB4_L" ) == 0) {
				Link.m_nType = CTC_LK_AXLB4_LSB;
			}
			else if ( strName.Find( "AXLB4_M" ) == 0 ) {
				Link.m_nType = CTC_LK_AXLB4_MSB;
			}
			else if ( strName.Find( "AXL" ) == 0 ) {
				Link.m_nType = CTC_LK_AXL;
			}
			else if ( (n = strName.Find( "ARROW_" )) == 0 ) {
				str = strName.Mid(n + strlen("ARROW_"));
				Link.m_nType = CTC_LK_ARROW;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "CA_" )) == 0 ) {
				str = strName.Mid(n + strlen("CA_"));
				Link.m_nType = CTC_LK_CA;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "LCR_" )) == 0 ) {
				str = strName.Mid(n + strlen("LCR_"));
				Link.m_nType = CTC_LK_LCR;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "CBB_" )) == 0 ) {
				str = strName.Mid(n + strlen("CBB_"));
				Link.m_nType = CTC_LK_CBB;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "BCR_" )) == 0 ) {
				str = strName.Mid(n + strlen("BCR_"));
				Link.m_nType = CTC_LK_BCR;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "SW_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("SW_"));
				Link.m_nType = CTC_LK_SW;
				Link.m_nObjNo = m_strSwitch.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "MSW_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("MSW_"));
				Link.m_nType = CTC_LK_MSW;
				Link.m_nObjNo = m_strSwitch.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "SG_E_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("SG_E_"));
				Link.m_nType = CTC_LK_SG_EXT;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "SG_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("SG_"));
				Link.m_nType = CTC_LK_SG;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "TK_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("TK_"));
				Link.m_nType = CTC_LK_TK;
				Link.m_nObjNo = m_strTrack.FindIndex( str ) + 1;
			}			
			else if ( (n = strName.Find( "LC_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("LC_"));
				Link.m_nType = CTC_LK_LC;
				Link.m_nObjNo = atoi(str);
			}
			else if ( (n = strName.Find( "OSS_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("OSS_"));
				Link.m_nType = CTC_LK_OSS;
				Link.m_nObjNo = m_strSignal.FindIndex( str ) + 1;
			}
			else if ( (n = strName.Find( "RO_" )) == 0 ) 
			{
				str = strName.Mid(n + strlen("RO_"));

				CString strStartSig, strEndBtn;
				char cTag = 0;
				
				n = str.Find('_');
				strStartSig = str.Mid(0, n);

				str = str.Mid(n + 1);

				n = str.Find('_');

				if(n == -1)
				{
					strEndBtn = str;
				}
				else
				{
					strEndBtn = str.Mid(0, n);

					cTag = str.GetAt(n + 1);
				}

				// 진로명에 U/M/L이 있는 경우 BUTTON명에 맞게 가공한다.
				if(strEndBtn != "UT" && (strEndBtn.GetAt(0) == 'U' || strEndBtn.GetAt(0) == 'M' || strEndBtn.GetAt(0) == 'L'))
				{
					strEndBtn = strEndBtn.Mid(1) + "/" + strEndBtn.GetAt(0);
				}

				BYTE nStartSigID = m_strSignal.FindIndex( strStartSig ) + 1;
				BYTE nEndBtnID = m_strButton.FindIndex( strEndBtn ) + 1;

				WORD wRouteNo;
				DWORD nRouteCount = *(DWORD*)&m_pRouteAddTable[ 0 ];	// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				DWORD nInfoPtr = 0;										// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
				BYTE *pInfo = NULL;

				for(WORD i = 0 ; i < nRouteCount ; i++)
				{
					nInfoPtr = *(DWORD*)&m_pRouteAddTable[ i + 1 ];		// 20140516 sjhong - InfoPtr을 WORD --> DWORD로 확장
					pInfo = (BYTE*)&m_pInfoMem[nInfoPtr];
					
					if(nStartSigID == pInfo[RT_OFFSET_SIGNAL] && nEndBtnID == pInfo[RT_OFFSET_BUTTON])
					{
						if(cTag != 0 && cTag != pInfo[RT_OFFSET_ROUTETAG])
						{
							continue;
						}

						wRouteNo = i + 1;

						Link.m_nType	= 0x80 | HIBYTE(wRouteNo);
						Link.m_nObjNo	= LOBYTE(wRouteNo);
						break;
					}
				}
			}

			if ( m_pDeb ) 
			{
				sprintf( szListBuffer, "%-30s.%4d %3X %3X\r\n",	*pStr, nNo, Link.m_nType, Link.m_nObjNo );
				*m_pDeb << szListBuffer;
			}
		}
	}

	if ( m_pDeb ) 
		*m_pDeb << "*** Create DTS Info Table DONE ***\n\n";
	
}

void CEITInfo::CreateConfigList( CMyStrList &list )
{
// Create Configuration Info

    CString str;
    WORD *pTable = (WORD*)(m_pIOInfo->m_pConfigListTable);

	char szListBuffer[256];
	if ( m_pDeb ) 
		*m_pDeb << "*** Create Config List Table ***\n";

	WORD &nCount = *pTable++;
	nCount = 0;
	POSITION pos;
	for ( pos = list.GetHeadPosition(); pos != NULL; ) 
	{
		CString *pStr = (CString*)list.GetNext( pos );
		if ( pStr && *pStr != "" && pStr->GetAt(0) != ';' ) 
		{
			CRemoteLinkTableType &Link = *(CRemoteLinkTableType*)(pTable++);
			int nNo = nCount++;

			CParser par( *pStr, " " );
			CString strType = par.gettokenl();
			CString strValue = par.gettokenl();

			if(strType == "SINGLE_TRACK")
			{
				Link.m_nType	= CONFIG_SINGLE_TRACK;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "AXLCNT_TRACK")
			{
				Link.m_nType	= CONFIG_AXLCNT_TRACK;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "B1B2_AXLSYNC") 
			{
				Link.m_nType	= CONFIG_B1B2_AXLSYNC;

				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B3B4_AXLSYNC") 
			{
				Link.m_nType	= CONFIG_B3B4_AXLSYNC;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "B11B12_AXLSYNC") 
			{
				Link.m_nType	= CONFIG_B11B12_AXLSYNC;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B13B14_AXLSYNC") 
			{
				Link.m_nType	= CONFIG_B13B14_AXLSYNC;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "CTC_ZONENO")
			{
				int i;
				
				Link.m_nType	= CONFIG_CTC_ZONENO;

				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "CTC_STATIONNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_CTC_STATIONNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "SUPER_BLOCK")
			{
				Link.m_nType	= CONFIG_SUPER_BLOCK;

				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B2_SELF_AXLRST")
			{
		 		Link.m_nType	= CONFIG_B2_SELF_AXLRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B4_SELF_AXLRST")
			{
				Link.m_nType	= CONFIG_B4_SELF_AXLRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "REVERSE_LAYOUT")
			{
				Link.m_nType	= CONFIG_REVERSE_LAYOUT;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "B1B2_NOAXL") 
			{
				Link.m_nType	= CONFIG_B1B2_NOAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B3B4_NOAXL") 
			{
				Link.m_nType	= CONFIG_B3B4_NOAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "B11B12_NOAXL") 
			{
				Link.m_nType	= CONFIG_B11B12_NOAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B13B14_NOAXL") 
			{
				Link.m_nType	= CONFIG_B13B14_NOAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if(strType == "B1B2_SWAPAXL") 
			{
				Link.m_nType	= CONFIG_B1B2_SWAPAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B3B4_SWAPAXL") 
			{
				Link.m_nType	= CONFIG_B3B4_SWAPAXL;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "GPS_ZONENO")
			{
				int i;
				
				Link.m_nType	= CONFIG_GPS_ZONENO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "GPS_STATIONNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_GPS_STATIONNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "GPS_SERVERNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_GPS_SERVERNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "UP_STATIONNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_UP_STATIONNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "DN_STATIONNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_DN_STATIONNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "MD_STATIONNO")
			{
				int i;
				
				Link.m_nType	= CONFIG_MD_STATIONNO;
				
				for(i = 0 ; i < strValue.GetLength() ; i++)
				{
					Link.m_nObjNo *= 10;
					Link.m_nObjNo += (strValue.GetAt(i) - '0');
				}
			}
			else if (strType == "B1B2_SELF_BLKRST")
			{
				Link.m_nType	= CONFIG_B1B2_SELF_BLKRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B3B4_SELF_BLKRST")
			{
				Link.m_nType	= CONFIG_B3B4_SELF_BLKRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B11B12_SELF_BLKRST")
			{
				Link.m_nType	= CONFIG_B11B12_SELF_BLKRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "B13B14_SELF_BLKRST")
			{
				Link.m_nType	= CONFIG_B13B14_SELF_BLKRST;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "USE_MMCR_NET")
			{
				Link.m_nType	= CONFIG_USE_MMCR_NET;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "USE_MCCR_NET")
			{
				Link.m_nType	= CONFIG_USE_MCCR_NET;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "USE_MODEM_BKUP")
			{
				Link.m_nType	= CONFIG_USE_MODEM_BKUP;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
 			else if (strType == "USE_TRAIN_SWEEP")
 			{
 				Link.m_nType	= CONFIG_BLK_TRAIN_SWEEP;
 				
 				if(strValue.CompareNoCase("YES") == 0)
 				{
 					Link.m_nObjNo = 1;
 				}
 				else if(strValue.CompareNoCase("NO") == 0)
 				{
 					Link.m_nObjNo = 0;
 				}
 			}
			else if (strType == "UP_NEWCBI_IFC")
			{
				Link.m_nType	= CONFIG_UP_NEWCBI_IFC;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "DN_NEWCBI_IFC")
			{
				Link.m_nType	= CONFIG_DN_NEWCBI_IFC;
				
				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}
			else if (strType == "BLK_DEV_MODE")
			{
				Link.m_nType	= CONFIG_BLK_DEV_MODE;

				if(strValue.CompareNoCase("YES") == 0)
				{
					Link.m_nObjNo = 1;
				}
				else if(strValue.CompareNoCase("NO") == 0)
				{
					Link.m_nObjNo = 0;
				}
			}

			if ( m_pDeb ) 
			{
				sprintf( szListBuffer, "%-30s.%4d %3X %3X\r\n", *pStr, nNo, Link.m_nType, Link.m_nObjNo );
				*m_pDeb << szListBuffer;
			}
		}
	}

	if ( m_pDeb ) 
	{
		*m_pDeb << "*** Create Config List Table DONE ***\n\n";
	}
}

BOOL CEITInfo::CanCloseFrame(CFrameWnd* pFrame) 
{
	// TODO: Add your specialized code here and/or call the base class
	return FALSE;
	return CDocument::CanCloseFrame(pFrame);
}


/*
 궤도의 쇄정상태

  진로 설정시 쇄정
  진로 취소시 모두 해정

 신호기 쇄정상태
  진로 설정시 쇄정
  신호기가 속해 있는 궤도 낙하시 접근 쇄정
*/

BOOL CEITInfo::SwitchFindAtTrack( BYTE nSwID, BYTE *pTrack, BYTE nMainTrkID )
{
	nSwID &= 0x7f;
	BYTE *pSwitchNodePtr = &m_pSwitchOnTrackTable[ nSwID ].pData[0];

	for (short i=0; i<sizeof(SwitchOnTrackTableType); i++) 
	{
		BYTE trid = pSwitchNodePtr[i];
		if (trid) 
		{
			for (short j=1; j<=*pTrack; j++) 
			{
				BYTE cmpid = *(pTrack+j);
				if (trid == cmpid) 
					return TRUE;

				if (cmpid == nMainTrkID) 
					break;		// 본선 도착 궤도 이후는 찾지않음
			}
		}
	}
	return FALSE;
}

extern CMemView *_pCMemView;

// 2000.6.20 추가
// Allow Long Switch Name
CString GetSwitchNameFromNR( CString &str ) {
	if ( isdigit(str.GetAt(4)))
		return str.Mid(2,3);
	return str.Mid(2,2);
}
// 2000.6.20 end


BYTE CEITInfo::GetStrToSwID( CString &str )
{
	BYTE swpos;
	if ( str.GetAt(0) == 'N') swpos = 0;
	else swpos = 0x80;
//2000.6.21	CString item = str.Mid( 2, 2 );
	CString item = GetSwitchNameFromNR( str );
//
	short nID = m_strSwitch.FindIndex( item ) + 1;
	ASSERT( nID );
	nID |= swpos;
	return (BYTE)nID;
}

void CEITInfo::CreateEquation( CString &str )
{
	char cInverse = 0;
	WORD nBitLoc;
	CLogicParser line( (char*)(LPCTSTR)str );
	CString strOp, strArg1, strArg2;
	line.GetToken( strOp );
	line.GetToken( strArg1 );
	if ( strArg1 == "!") {
		cInverse = 1;
		line.GetToken( strArg1 );
	}

	line.GetToken( strArg2 );
	m_pInfoMem[ m_nInfoPtr++ ] = GetFunctionCode( strOp );
	nBitLoc = GetBitLocation( strArg1 );
	if (cInverse) nBitLoc |= 0x8000;
	*(WORD*)&m_pInfoMem[ m_nInfoPtr++ ] = nBitLoc;
	m_nInfoPtr++;
}

char szFunction[] = "LDA  STA  AND  IOR  XOR  NOT  MOV  ";

BYTE CEITInfo::GetFunctionCode( CString &str )
{
	char *p = strstr(szFunction, str);
	BYTE code = 0;
	if (p) {
		code = ( p - szFunction ) / 5 + 1;
	}
	return code;
}

WORD CEITInfo::GetBitLocation( CString &str )
{
	WORD nBitLoc = 0;
	char *pStr = (char*)(LPCTSTR)str;
	CParser line( pStr, "." );
	CString strName, strMember;
	line.GetToken( strName );
	line.GetToken( strMember );
	ASSERT( strMember == "." );
	line.GetToken( strMember );
	CString strType = strName.Left(2);
	strName = ((char*)(LPCTSTR)strName)+2;
	short nID = 0;
	if (strType == "TK") {
		nID = m_strTrack.FindIndex( strName ) + 1;
		ASSERT( nID > 0 );
		nBitLoc = (m_TableBase.InfoTrack.nStart + (nID-1) * sizeof(ScrInfoTrack)) << 3;
		if ( strMember == "TRACK" ) nBitLoc += 1;
		else if ( strMember == "ROUTE" ) nBitLoc += 2;
		else if ( strMember == "REQUEST" ) nBitLoc += 3;
		else {
			ASSERT( 0 );
		}
	}
	else if (strType == "SW") {
		nID = m_strSwitch.FindIndex( strName ) + 1;
		ASSERT( nID > 0 );
		nBitLoc = (m_TableBase.InfoSwitch.nStart + (nID-1) * sizeof(ScrInfoSwitch)) << 3;
		if ( strMember == "FREE" ) nBitLoc += 5;
		else if ( strMember == "ROUTELOCK" ) nBitLoc += (8+1);
		else {
			ASSERT( 0 );
		}
	}
	else if (strType == "SG") {
		nID = m_strSignal.FindIndex( strName ) + 1;
		ASSERT( nID > 0 );
		nBitLoc = (m_TableBase.InfoSignal.nStart + (nID-1) * sizeof(ScrInfoSignal)) << 3;
		if ( strMember == "FREE" ) {
			nBitLoc += (8 + 5);
		}
		else {
			ASSERT( 0 );
		}
	}
	else {
		ASSERT( 0 );
	}
	return nBitLoc;
}

short CEITInfo::MakeTrackAtSwitchTable( CString &str, BYTE *pApproach, BOOL bAlarmAdd )
{
	BYTE nID;
	WORD nInfoPtr = 0;
	CLogicParser line;
	line.Create( (char*)(LPCTSTR)str, "," );
	CString item;

	while ( line.GetToken( item ) ) 
	{
		if (item != ",") 
		{
			if (item.GetAt(0) == '&' || item.GetAt(0) == '$') {		// 오버랩 해정 궤도
				item = item.Right( item.GetLength() - 1 );
			}
			short n = item.Find( '@' );
			short swcount = 0;

			CString strTrk = item;
			if ( n > 0 ) 
			{	// 분기조건
				strTrk = item.Left( n );
			}

			nID = m_strTrack.FindIndex( strTrk ) + 1;
			ASSERT( nID );
// 중복 ID가 있으면 에러처리

			BYTE *p = pApproach;
			for (short i = 0; i < nInfoPtr; ) {
				ASSERT ( p[i] != nID);
				while (p[i]) i++;
				i++;
			}

			pApproach[nInfoPtr++] = (BYTE)nID;

            if (bAlarmAdd) 
			{
                m_pTrackInfoTable[ nID ].bALARM = 1;
            }

            if (n>0) 
			{
				item = item.Right( item.GetLength() - n - 1);

				while (item != "") 
				{
					short p = item.Find( ']' );
					CString strSw = item.Left(p+1);
					char c = *(char*)(LPCTSTR)strSw; 
					ASSERT( c == 'N' || c == 'R' );
					item = item.Right( item.GetLength() - p - 1);
					pApproach[nInfoPtr++] = GetStrToSwID( strSw );
//E
				}

			    pApproach[nInfoPtr++] = 0;
			}
			else
			{
			    pApproach[nInfoPtr++] = 0;
			}
		}
	}

	pApproach[nInfoPtr++] = 0;
	return nInfoPtr;
}

// Inverse 처리
// addr 의 bit 13 이 sign bit 로 1 이면 해당 정보를 반전한다.
//
// !!! 주의 : 추후에 VMEM 의 사용 번지가 0x2000 / 8 = 0x400 을 넘지 않도록 할것 !!!
// -> 넘으면 알고리즘 변경 필요...
//
void BitMove( BYTE *dest, WORD daddr, BYTE *source, WORD saddr ) {
	BYTE *s, *d;
	int nInverse = saddr & 0x2000;
	nInverse ^= daddr & 0x2000;
// mask
	saddr &= 0x1FFF;
	daddr &= 0x1FFF;

	s = source + (saddr >> 3);
	d = dest + (daddr >> 3);
	BYTE sbit = 1 << ( saddr & 7 );
	BYTE dbit = 1 << ( daddr & 7 );
	if ( (*s & sbit && !nInverse) || (!(*s & sbit) && nInverse) ) {	// 1
		*d |= dbit;
	}
	else {				// 0
		*d &= ~dbit;
	}
}


void CEITInfo::DataUpdate( WORD inout, BYTE *buffer, BOOL bIosim )
{
	WORD w;

	WORD *p = (WORD*)m_pIOInfo->m_pAssignTable;

	int direct = inout;  //In, Out 방향 결정 --> 1이면 EIP에서 PC, 2이면 PC에서 LIS
	if (bIosim) 
		direct ^= 3;

	for (int i=0; i<2048; i++) // 1 RACK
	{		
		w = p[i];
		WORD type = w >> 14;

		if ( type == inout ) 
		{
			if ( direct == 1 ) {	// IN
				BitMove( m_pVMEM, w & 0x3fff, buffer, i );
			}
			else if ( direct == 2 ) {
				BitMove( buffer, i, m_pVMEM, w & 0x3fff );
			}
		}
	}
}

char *SigTypeStr[] = {
	"TCB",
	"TGB",
	"OC",
	"OG",
	"HOME",	
	"BLOCK",
	"START",
	"ISTART",
	"SHUNT",
	"GSHUNT",
	"ISHUNT",
	"OHOME",
	"ASTART",
	"CALLON",
	"OCALLON",
	"REPEAT",
	"STOP",
	"BUFSTART"
};

short SigTypeID[] = {
    BLOCKTYPE_AUTO_IN,      //	0x10 //Tokenless 장내
    BLOCKTYPE_AUTO_OUT,     //	0x20 //Tokenless 출발
    BLOCKTYPE_INTER_IN,     //	0x30 //Token 장내
    BLOCKTYPE_INTER_OUT,    //  0x40 //Token 출발
    SIGNALTYPE_IN,          //	0x80
    SIGNALTYPE_INBLOCK,     //	0x10 //구내폐색
    SIGNALTYPE_OUT,         //  0xA0
    SIGNALTYPE_ISTART,      //  0x70
    SIGNALTYPE_SHUNT,       //	0xB0
    SIGNALTYPE_GSHUNT,       //	0x90	20140418 sjhong - 수정
    SIGNALTYPE_ISHUNT,      //	0x50
    SIGNALTYPE_OHOME,		//	0xC0 
	SIGNALTYPE_ASTART,		//  0xD0
	SIGNALTYPE_CALLON,		//  0xE0
	SIGNALTYPE_CALLON,		//  0xE0
	SIGNALTYPE_MPASS,		//  0xF0	// 원방신호기
	SIGNALTYPE_STOP,		//  0x60	// STOP sign,  LOS sign ...
	SIGNALTYPE_BUFSTART		//  0x00	// 가짜 신호기.
};


short CEITInfo::SignalStrToTypeID( CString &str )
{
    CString sname = str;
//    if (sname.Left(8) == "구내폐색") sname = "구내폐색";
    for (short i = 0; i < sizeof(SigTypeStr) / sizeof(char *); i++ ) {
        if ( sname == SigTypeStr[ i ] )
            return SigTypeID[ i ];
    }
    return 0;
}

BOOL CEITInfo::OnOpenDocument(LPCTSTR lpszPathName) 
{
	SetPathName( lpszPathName );
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

void CEITInfo::TableDump(CDumpContext& dc)
{
	char pBuffer[512];
	dc << "m_TableBase\n";
	dc << "m_pInfoMem\n";
	dc << "m_pInfoGeneral\n";

// IO Card Info bits
	dc << "\n IO Card Info bits\n";
	dc << "Strat at offset 0x2E to 0x3D, total 16 bytes, max 64 IO cards\n";
	dc.HexDump( "\tIO defines :", m_TableBase.IOCardInfo, 16, 16 );
	dc << "Strat at offset 0x3E to 0x6D, total 64 bytes, REAL ID\n";
	dc.HexDump( "\tREAL ID :", m_TableBase.RealID, 64, 16 );
	dc << "\n Interlock Table Dump\n";


	BYTE n, /*nDestTrackID,*/ nSignalID;


	for ( nSignalID = 1; nSignalID <= m_strSignal.GetCount(); nSignalID++ ) 
	{
		WORD wRouteNo = m_pSignalInfoTable[ nSignalID ].wRouteNo;

		dc << "\nSIGNAL = " << strDumpOfSignal( nSignalID ) << "\n";

		dc.HexDump( "SignalInfo :", (BYTE*)&m_pSignalInfoTable[ nSignalID ], 10, 10 );

		// 2007.7.18 추가 
//		dc.HexDump( "Approach   :", (BYTE*)&m_pSignalInfoTable[ nSignalID ].pApproach, 21, 21 );

		BYTE nRouteCount = m_pSignalInfoTable[ nSignalID ].nRouteCount;
		dc << "\tROUTES : " << nRouteCount << "\n";

		for (n=0; n<nRouteCount; n++) 
		{
			UINT nInfoPtr = *(UINT*)&m_pRouteAddTable[ wRouteNo ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
			CString str;
			str.Format( " = 0x%4.4X", nInfoPtr );
			dc << "\tROUTE : " << wRouteNo << str << "\n";
			BYTE *pInfo = &m_pInfoMem[nInfoPtr];			// 연동도표 정보 테이블
			dc << "\t\t";
			char bf[80];
			sprintf( bf , "%s", strDumpOfTrack( pInfo[RT_OFFSET_FROM] ));
			sprintf( pBuffer, "Rt: %10s-%-10s", bf, strDumpOfTrack( pInfo[RT_OFFSET_TO] ));
			dc << pBuffer;
			sprintf( pBuffer, " Sg:%-10s", strDumpOfSignal( pInfo[RT_OFFSET_SIGNAL] ));
			dc << pBuffer;
			sprintf( pBuffer, " Bt:%-10s", strDumpOfButton( pInfo[RT_OFFSET_BUTTON] ));
			dc << pBuffer;
			sprintf( pBuffer, " St:0x%02X", pInfo[RT_OFFSET_ROUTESTATUS] );
			dc << pBuffer;
//#define RT_SHUNTSIGNAL	0x40
//#define RT_HASYUDO		0x80
			BYTE cRtState = pInfo[RT_OFFSET_ROUTESTATUS];
			sprintf( pBuffer, "(%c%c) ", cRtState & 0x80 ? 'U' : ' ', cRtState & 0x40 ? 'S' : ' ' );
			dc << pBuffer;
			sprintf( pBuffer, " Pre:%-6s", strDumpOfSignal( pInfo[RT_OFFSET_PREVSIGNAL] ));
			dc << pBuffer;
			sprintf( pBuffer, " Dep1:%-6s", strDumpOfSignal( pInfo[RT_OFFSET_DEPENDSIG1] & SIGNALID_MASK));
			dc << pBuffer;
			sprintf( pBuffer, " Dep2:%-6s", strDumpOfSignal( pInfo[RT_OFFSET_DEPENDSIG2] & SIGNALID_MASK ));
			dc << pBuffer;
			sprintf( pBuffer, " Dep3:%-6s", strDumpOfSignal( pInfo[RT_OFFSET_DEPENDSIG3] & SIGNALID_MASK ));
			dc << pBuffer;
			sprintf( pBuffer, " Dep4:%-6s", strDumpOfSignal( pInfo[RT_OFFSET_DEPENDSIG4] & SIGNALID_MASK ));
			dc << pBuffer << "\n";
			sprintf( pBuffer, " LC:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_LEVELCROSS]) );
			dc << pBuffer;
			sprintf( pBuffer, " RtOd:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_ROUTEORDER] ));
			dc << pBuffer;
			sprintf( pBuffer, " MtDt:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_MULTIDEST] ));
			dc << pBuffer;
			sprintf( pBuffer, " RtTg:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_ROUTETAG] ));
			dc << pBuffer << "\n";
			sprintf( pBuffer, " BerT:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_BERTH_TIME] ));
			dc << pBuffer;
			sprintf( pBuffer, " AprT:%-6s", strDumpOnlyID( pInfo[RT_OFFSET_APPRCH_TIME] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr1:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK1] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr2:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK2] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr3:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK3] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr4:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK4] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr5:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK5] ));
			dc << pBuffer;
			sprintf( pBuffer, " Apr6:%-6s", strDumpOfTrack( pInfo[RT_OFFSET_APPRCHTK6] ));
			dc << pBuffer << "\n";

			int ptr = RT_OFFSET_SWITCH;
			int count = (int)pInfo[ ptr ] + 1;
			dc.HexDump( "\t\tSwitch   :", (BYTE*)&pInfo[ ptr ], count, count );
			ptr += count;

			count = *(WORD*)&pInfo[ ptr ] + 1;
			dc.HexDump( "\t\tSignal   :", (BYTE*)&pInfo[ ptr ], (count * 2), (count * 2));
			ptr += (count * 2);

			count = (int)pInfo[ ptr ] + 1;
			dc.HexDump( "\t\tTrackLock:", (BYTE*)&pInfo[ ptr ], count, count );
			ptr += count;
			count = (int)pInfo[ ptr ] + 1;
			dc.HexDump( "\t\tRouteLock:", (BYTE*)&pInfo[ ptr ], count, count );

			wRouteNo++;
		}
		dc << "END OF ROUTES\n";
	}

	dc << "m_pRouteAddTable\n";
	dc << "m_pRouteCrcTable\n";
	dc << "m_pRouteSizeTable\n";
	dc << "m_pSwitchOnTrackTable\n";
	dc << "m_pSignalInfoTable\n";
	dc << "m_pSwitchInfoTable\n";
	dc << "m_pTrackInfoTable\n";
	dc << "m_pCommonTrack\n";
    dc << "m_pIOInfo->m_pAssignTable\n";
	dc << "m_pIOInfo->m_pCtcElementTable\n";
	dc << "m_pIOInfo->m_pConfigListTable\n";

// ScrInfo Object Address Dump
	UINT addr = 0;
	int no;
	char bf[256];
	CString *pStr;
	dc << "\nTRACK\n";
	for ( no = 0; no < m_strTrack.GetCount(); no++ ) {
		pStr = m_strTrack.GetAtIndex(no);
		sprintf( bf, "%3d %3.3X %s\n", no, addr, *pStr);
		dc << bf;
		addr += SIZE_INFO_TRACK;
	}
	dc << "\nSIGNAL\n";
	for ( no = 0; no < m_strSignal.GetCount(); no++ ) {
		pStr = m_strSignal.GetAtIndex(no);
		sprintf( bf, "%3d %3.3X %s\n", no, addr, *pStr);
		dc << bf;
		addr += SIZE_INFO_SIGNAL;
	}
	dc << "\nSWITCH\n";
	for ( no = 0; no < m_strSwitch.GetCount(); no++ ) {
		pStr = m_strSwitch.GetAtIndex(no);
		sprintf( bf, "%3d %3.3X %s\n", no, addr, *pStr);
		dc << bf;
		addr += SIZE_INFO_SWITCH;
	}
	dc << "\nLAMP\n";
	for ( no = 0; no < m_strLamp.GetCount(); no++ ) {
		pStr = m_strLamp.GetAtIndex(no);
		sprintf( bf, "%3d %3.3X %s\n", no, addr, *pStr);
		dc << bf;
		addr += SIZE_INFO_LAMP;
	}

}

CString & MakeStringAndID( CString &pStr, BYTE nID ) {
	static CString str;
	str.Format( "%s(%2.2X)", pStr, nID );
	return str;
}

CString & MakeStringAndRouteID( CString &pStr, WORD wRouteID)
{
	static CString str;
	str.Format( "%s(%2.2X)", pStr, wRouteID );
	return str;
}
CString strNoID = "(0)";
CString & CEITInfo::strDumpOfTrack( BYTE nID )
{
	if (nID == 0) return strNoID;
	CString *pStr = m_strTrack.GetAtIndex( nID - 1 );
	return MakeStringAndID( *pStr, nID );
}
CString & CEITInfo::strDumpOfSignal( BYTE nID )
{
	if (nID == 0) return strNoID;
	CString *pStr = m_strSignal.GetAtIndex( nID - 1 );
	return MakeStringAndID( *pStr, nID );
}
CString & CEITInfo::strDumpOfSwitch( BYTE nID )
{
	if (nID == 0) return strNoID;
	CString *pStr = m_strSwitch.GetAtIndex( nID - 1 );
	return MakeStringAndID( *pStr, nID );
}
CString & CEITInfo::strDumpOfButton( BYTE nID )
{
	if (nID == 0) return strNoID;
	CString *pStr = m_strButton.GetAtIndex( nID - 1 );
	return MakeStringAndID( *pStr, nID );
}
CString & CEITInfo::strDumpOfRoute( WORD wID )
{
	if (wID == 0) return strNoID;
	CString *pStr = m_strRoute.GetAtIndex( wID - 1 );
	return MakeStringAndRouteID( *pStr, wID );
}
CString & CEITInfo::strDumpOnlyID( BYTE nID )
{
	if (nID == 0) return strNoID;
	static CString str;
	str.Format( "[%2.2X]", nID );
	return str;
}


BYTE _ACodeStr[32];
BYTE _ACode[16];
BYTE _KeyCode[16] = {
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
	0x08, 0x84,
};
BYTE _DecodedCode[16];


void ACodeGenByte( BYTE d ) {
	BYTE Temp = d;
	_asm mov  dl,8
Crc_Loop:
	BYTE *pd = _ACode + 15;
	_asm mov   esi,pd
	_asm clc
	_asm mov   ecx,16
Sftloop:
	_asm rcr   BYTE PTR [esi],1
	_asm dec   esi
	_asm loop  Sftloop
    _asm jnc   Next1
	_asm mov   al,Temp
	_asm xor   al,1
	_asm mov   Temp,al
Next1:
	_asm rcr   Temp,1
	_asm jnc   Next

	pd = _ACode + 15;
	BYTE *ps = _KeyCode + 15;
	_asm mov   esi,pd
	_asm mov   edi,ps
	_asm mov   ecx,16
Xorloop:
	_asm mov   al,[esi]
	_asm xor   al,[edi]
	_asm mov   [esi],al
	_asm dec   esi
	_asm dec   edi
	_asm loop  Xorloop
Next:
	_asm dec   dl
	_asm jnz   Crc_Loop
}

void CodeToStr( BYTE *dest, BYTE *src ) {
	unsigned long a = *(long*)src;
	BYTE check = 0;
	for (int i = 0; i < 6; i++) {
		if ( i==5 )
			a ^= check;
		BYTE d = (BYTE)(a & 0x3F);
		check ^= d;
		if ( d < 10 ) {
			d += '0';
		}
		else {
			d -= 10;
			if ( d < 27 ) {
				d += '@';
			}
			else {
				d -= 27;
				d += '`';
			}
		}
		*dest++ = d;
		a >>= 6;
	}
}

BOOL StrToCode( BYTE *dest, BYTE *src ) {
	unsigned long a = 0;
	BYTE check = 0;
	for (int i = 0; i < 6; i++) {
		BYTE d = *src++;
		if ( d < '0' ) return FALSE;
		if ( d <= '9' ) {
			d -= '0';
		}
		else {
			if ( d < '@' ) return FALSE;
			if ( d <= 'Z' ) {
				d -= '@';
				d += 10;
			}
			else {
				if ( d < '`' ) return FALSE;
				d -= '`';
				d += 37;
			}
		}
		if ( i==5 ) {
			d ^= check;
			if ( d & 0xC )
				return FALSE;
		}
		a |= (d << ( i * 6 ));
		check ^= d;
	}
	*(long*)dest = a;
	return TRUE;
}

void MakeCodeString()
{
//	memset( _ACode, 0, sizeof(_ACode) );
//	BYTE *pBase = (BYTE*)&m_TableBase;
//    BYTE *p = pBase + 2;
//	for (int i=0; i<56; i++) {		// 56 = 16 * 3 + 8
//		ACodeGenByte( (BYTE)i );
//	}
	int i;
	BYTE *p = _ACode;
	BYTE *d = _ACodeStr;
	for (i=0; i<4; i++) {
		if (i) *d++ = '-';
		CodeToStr( d, p );
		p += 4;
		d += 6;
	}
	*d = 0;

	d = _ACodeStr;
	p = _DecodedCode;
	for (i=0; i<4; i++) {
		if (i) d++;
		StrToCode( p, d );
		p += 4;
		d += 6;
	}

	for (i=0; i<16; i++) {
		if ( _ACode[i] == _DecodedCode[i] ) {
		}
		else {
			_ACode[i] = _DecodedCode[i];
		}
	}
}

void CEITInfo::CreateAuthorizationCode()
{
	memset( _ACode, 0, sizeof(_ACode) );

    BYTE *p = (BYTE*)&m_TableBase.DownInfo.nMemSize;

	for (UINT i=0 ; i< 58 ; i++) {		// 56 = 16 * 3 + 8
		ACodeGenByte( *p++ );
	}

	UINT RouteCount = *(UINT*)&m_pRouteAddTable[ 0 ];	// 20140516 sjhong - InfoPtr을 WORD --> UINT로 확장
    for (i=0; i<RouteCount; i++) {
        WORD nCrc = m_pRouteCrcTable[i];
		ACodeGenByte( nCrc & 0xff );
		ACodeGenByte( nCrc >> 8 );
    }

	MakeCodeString();
	if ( m_pDeb ) {
		CString str = '"';
		str += _ACodeStr;
		str += '"';
		*m_pDeb << "Authorization Code = " << str << "\n\n";
	}
	memcpy( (BYTE*)m_TableBase.ACode, _ACode, 16 );
}


void CEITInfo::OnCreatedownfile() 
{
	// TODO: Add your command handler code here
	MakeDownloadFile();
}
