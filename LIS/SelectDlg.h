// SelectDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectDlg dialog

class CSelectDlg : public CDialog
{
// Construction
public:
	void UpdateString();
	CTime m_Start, m_End;
	CSelectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectDlg)
	enum { IDD = IDD_DIALOGLISTPRINT };
	CString	m_EndMin;
	CString	m_EndDate;
	CString	m_EndHour;
	CString	m_StartHour;
	CString	m_StartMin;
	CString	m_StartDate;
	BOOL	m_bFault;
	BOOL	m_bHistory;
	BOOL	m_bOthers;
	BOOL	m_bSignal;
	BOOL	m_bSwitch;
	BOOL	m_bTrack;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSsyear(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSeyear(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSshour(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSehour(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSsmin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSemin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
