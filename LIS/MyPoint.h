#if !defined(AFX_MYPOINT_H__C4C8CD44_A487_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MYPOINT_H__C4C8CD44_A487_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyPoint.h : header file
//

#include "resource.h"
#include "simobj.h"
#include "../include/iolist.h"
#include "..\INCLUDE\EipEmu.h"	// Added by ClassView
//#include "BtnST.h"

#define UPDATE_ALL 1

/////////////////////////////////////////////////////////////////////////////
// CMySimplePointDlg dialog

class CMySimplePointDlg : public CDialog
{
	// Construction
public:
	BYTE m_byteCrank;
	BYTE m_byteType;
	BOOL m_bInconsistency;
	BOOL m_bNWPR;
	BOOL m_bRWPR;
	BOOL m_bNKR;
	BOOL m_bRKR;
	BOOL m_bHandPointKeyRemove;
	BOOL m_bDelayTimeUpdate;
	int  m_iDelayTime;
	ScrInfoSwitch *m_pInfo;
	CMySimplePointDlg( CWnd* pParent = NULL);   // standard constructor
	void ReceivePointInformation(ScrInfoSwitch *pInfo, BYTE nType, BYTE nCrank);
	
	// Dialog Data
	//{{AFX_DATA(CMySimplePointDlg)
	enum { IDD = IDD_DIALOG_POINT_SIMPLE };
	CString	m_strName;
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySimplePointDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CMySimplePointDlg)
	afx_msg void OnButtonInconsistency();
	afx_msg void OnButtonHandpointkeyRemove();
	afx_msg void OnOk2();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonRkr();
	afx_msg void OnButtonNkr();
	afx_msg void OnCheckNwpr();
	afx_msg void OnCheckRwpr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////
class CEITInfo;
class CMyPoint : public CSimObject
{
//    DECLARE_SERIAL( CMyPoint )
public:
	BOOL m_bIsCreateSimplePointDlg;
	CDialog * m_pDialog;
	CMySimplePointDlg m_SimpleDialog;
	BOOL m_bFail;
    BOOL m_bManual;
	void DialogOpen();
	BYTE m_nDelay;
	BYTE m_nSwitchType;
	BYTE m_nPointKeyNo;

    BYTE m_nWRODP, m_nWRODM; 
	BOOL m_bWKEYKR;

	void Run();
	void CheckPointDirection(CString strPointName, ScrInfoSwitch *pInfo);
	CMyPoint( CEITInfo *pInfo);
	virtual ~CMyPoint() {}

//	WORD m_nWR, m_nWLP;
//	WORD m_nWR_N, m_nWR_R, m_nKR_N, m_nKR_R;
//	IOBitlocSwitch *m_pIO;
	CSimObject *m_pTrack;
};

/////////////////////////////////////////////////////////////////////////////
// CMyPointDlg dialog

class CMyPointDlg : public CDialog
{
// Construction
public:
	void SetData();
	CMyPointDlg( ScrInfoSwitch *pInfo, BYTE nType, BYTE nCrank, CWnd* pParent = NULL);   // standard constructor

	ScrInfoSwitch *m_pInfo;
	BYTE m_nSwitchType;
	BYTE m_nPointKeyNo;
	BYTE m_nHPKrelease;

// Dialog Data
	//{{AFX_DATA(CMyPointDlg)
	enum { IDD = IDD_DIALOG_POINT };
	CString	m_strName;
	BOOL m_bFail;
	int  m_nTimer;
	BOOL m_bManual;
    BOOL m_bWROP;
    BOOL m_bWROM;
    BOOL m_bWRP;
    BOOL m_bWRM;
    BOOL m_bKRP;
    BOOL m_bKRM;
    BOOL m_bWLR;
    BOOL m_bWLRO;
	BOOL m_bWKEYKR;
	BOOL m_bREQP;
	BOOL m_bREQM;
	BOOL m_bNOUSED;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyPointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMyPointDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYPOINT_H__C4C8CD44_A487_11D2_BA70_00C06C003C49__INCLUDED_)
