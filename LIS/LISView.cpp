// LISView.cpp : implementation of the CLISView class
//

#include "stdafx.h"
#include "LIS.h"

//#include "LISDoc.h"
#include "EITInfo.h"
#include "LISView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLISView

IMPLEMENT_DYNCREATE(CLISView, CView)

BEGIN_MESSAGE_MAP(CLISView, CView)
	//{{AFX_MSG_MAP(CLISView)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLISView construction/destruction

CLISView::CLISView()
{
}

CLISView::~CLISView()
{
}

BOOL CLISView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLISView drawing

void CLISView::OnDraw(CDC* pDC)
{
	CEITInfo* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
}

void CLISView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
}

/////////////////////////////////////////////////////////////////////////////
// CLISView printing

BOOL CLISView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLISView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLISView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CLISView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
}

/////////////////////////////////////////////////////////////////////////////
// CLISView diagnostics

#ifdef _DEBUG
void CLISView::AssertValid() const
{
	CView::AssertValid();
}

void CLISView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CEITInfo* CLISView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEITInfo)));
	return (CEITInfo*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLISView message handlers

