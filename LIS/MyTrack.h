#if !defined(AFX_MYTRACK_H__C4C8CD43_A487_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MYTRACK_H__C4C8CD43_A487_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyTrack.h : header file
//

#include "resource.h"
#include "simobj.h"
#include "../include/iolist.h"

#define UPDATE_ALL 1
/////////////////////////////////////////////////////////////////////////////
// CMySimpleTrackDlg dialog

class CMySimpleTrackDlg : public CDialog
{
	// Construction
public:
	ScrInfoTrack *m_pInfo;
	CString	m_strName;
	void UpdateTrackInfo();
	CMySimpleTrackDlg(CWnd* pParent = NULL);   // standard constructor
	
	// Dialog Data
	//{{AFX_DATA(CMySimpleTrackDlg)
	enum { IDD = IDD_DIALOG_TRACK_SIMPLE };
	BOOL	m_bCheckResetIn;
	BOOL	m_bCheckOccupy;
	BOOL	m_bCheckDisturb;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySimpleTrackDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CMySimpleTrackDlg)
	afx_msg void OnCheckTrackOccupy();
	afx_msg void OnCheckTrackDisturb();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckTrackResetin();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//////////////////////////////////////////////////////////////////
class CEITInfo;
class CMyTrack : public CSimObject
{
//    DECLARE_SERIAL( CMyPoint )
public:
	void SetAutoAccupancy();
	void DialogOpen();
	void Run();
	CMyTrack( CEITInfo *pInfo );
	virtual ~CMyTrack() {}

	BYTE m_nID;
	short m_nTimer;
	short m_nGoDirection;
	BOOL m_bAuto;
	BOOL m_bIsCreateSimpleTrackDlg;
	CMySimpleTrackDlg m_SimpleTrackDlg;

//	IOBitlocTrack *m_pIO;
//	CSimObject *m_pSwitch;
//	CMyTrack *m_pC;
//	CMyTrack *m_pN;
//	CMyTrack *m_pR;
DECLARE_DYNAMIC(CMyTrack)
};

/////////////////////////////////////////////////////////////////////////////
// CMyTrackDlg dialog

class CMyTrackDlg : public CDialog
{
// Construction
public:
	CMyTrackDlg( ScrInfoTrack *pInfo, CWnd* pParent = NULL);   // standard constructor

	ScrInfoTrack *m_pInfo;

// Dialog Data
	//{{AFX_DATA(CMyTrackDlg)
	enum { IDD = IDD_DIALOG_TRACK };
	CString	m_strName;
	CString	m_strTime;
	BOOL	m_bTrack;
	BOOL	m_bAuto;
	BOOL	m_bDisturb;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyTrackDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMyTrackDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTRACK_H__C4C8CD43_A487_11D2_BA70_00C06C003C49__INCLUDED_)
