#if !defined(AFX_MYLIST_H__D4E54842_A3E5_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MYLIST_H__D4E54842_A3E5_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyList window

class CMyList : public CListCtrl
{
// Construction
public:
	int m_nIsBitBlock;
	CMyList();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyList)
	//}}AFX_VIRTUAL

// Implementation
public:
	void Update();
	virtual ~CMyList();
	void OnLButtonDblClkByLSM(CString strObjName);
	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyList)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYLIST_H__D4E54842_A3E5_11D2_BA70_00C06C003C49__INCLUDED_)
