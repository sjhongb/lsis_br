// MyList.cpp : implementation file
//

#include "stdafx.h"
//#include "WEM.h"
#include "MyList.h"

#include "SimObj.h"
#include "MemView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyList

CMyList::CMyList()
{
	m_nIsBitBlock = 0;
}

CMyList::~CMyList()
{
}


BEGIN_MESSAGE_MAP(CMyList, CListCtrl)
	//{{AFX_MSG_MAP(CMyList)
	ON_WM_LBUTTONDOWN()
	ON_WM_DRAWITEM()
	ON_WM_NCCALCSIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyList message handlers

LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
	static const _TCHAR szThreeDots[] = _T("...");

	int nStringLen = lstrlen(lpszLong);

	if(nStringLen == 0 ||
		(pDC->GetTextExtent(lpszLong, nStringLen).cx + nOffset) <= nColumnLen)
	{
		return(lpszLong);
	}

	static _TCHAR szShort[MAX_PATH];

	lstrcpy(szShort,lpszLong);
	int nAddLen = pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

	for(int i = nStringLen-1; i > 0; i--)
	{
		szShort[i] = 0;
		if((pDC->GetTextExtent(szShort, i).cx + nOffset + nAddLen)
			<= nColumnLen)
		{
			break;
		}
	}

	lstrcat(szShort, szThreeDots);
	return(szShort);
}

#define OFFSET_FIRST	2
#define OFFSET_OTHER	6

#define VMEMIDMAX 32768
extern BYTE m_pVMEM[];

void CMyList::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	COLORREF m_clrText;
	COLORREF m_clrTextBk;
	COLORREF m_clrBkgnd;

	m_clrText = ::GetSysColor(COLOR_WINDOWTEXT);
	m_clrTextBk = ::GetSysColor(COLOR_WINDOW);
	m_clrBkgnd = ::GetSysColor(COLOR_WINDOW);

//	CListCtrl& ListCtrl=GetListCtrl();
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem(lpDrawItemStruct->rcItem);
	UINT uiFlags = ILD_TRANSPARENT;

//	CImageList* pImageList;
	int nItem = lpDrawItemStruct->itemID;
	BOOL bFocus = (GetFocus() == this);
	COLORREF clrTextSave, clrBkSave;
	COLORREF clrImage = m_clrBkgnd;
	static _TCHAR szBuff[MAX_PATH];
	LPCTSTR pszText;

// get item data

	LV_ITEM lvi;
	lvi.mask = LVIF_TEXT | LVIF_STATE;
	lvi.iItem = nItem;
	lvi.iSubItem = 0;
	lvi.pszText = szBuff;
	lvi.cchTextMax = sizeof(szBuff);
	lvi.stateMask = 0xFFFF;		// get all state flags
	GetItem(&lvi);

	BOOL bSelected = (bFocus || (GetStyle() & LVS_SHOWSELALWAYS)) && lvi.state & LVIS_SELECTED;
	bSelected = bSelected || (lvi.state & LVIS_DROPHILITED);

// set colors if item is selected

	CRect rcAllLabels;
	GetItemRect(nItem, rcAllLabels, LVIR_BOUNDS);

	CRect rcLabel;
	GetItemRect(nItem, rcLabel, LVIR_LABEL);

	rcAllLabels.left = rcLabel.left;
//	if (m_bClientWidthSel && rcAllLabels.right<m_cxClient)
//		rcAllLabels.right = m_cxClient;

	if (bSelected)
	{
		clrTextSave = pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		clrBkSave = pDC->SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));

		pDC->FillRect(rcAllLabels, &CBrush(::GetSysColor(COLOR_HIGHLIGHT)));
	}
	else
		pDC->FillRect(rcAllLabels, &CBrush(m_clrTextBk));

// set color and mask for the icon
/*
	if (lvi.state & LVIS_CUT)
	{
		clrImage = m_clrBkgnd;
		uiFlags |= ILD_BLEND50;
	}
	else if (bSelected)
	{
		clrImage = ::GetSysColor(COLOR_HIGHLIGHT);
		uiFlags |= ILD_BLEND50;
	}
*/
// draw state icon
/*
	UINT nStateImageMask = lvi.state & LVIS_STATEIMAGEMASK;
	if (nStateImageMask)
	{
		int nImage = (nStateImageMask>>12) - 1;
//		pImageList = ListCtrl.GetImageList(LVSIL_STATE);
		pImageList = GetImageList(LVSIL_STATE);
		if (pImageList)
		{
			pImageList->Draw(pDC, nImage,
				CPoint(rcItem.left, rcItem.top), ILD_TRANSPARENT);
		}
	}

// draw normal and overlay icon
	CRect rcIcon;
//	ListCtrl.GetItemRect(nItem, rcIcon, LVIR_ICON);
	GetItemRect(nItem, rcIcon, LVIR_ICON);

//	pImageList = ListCtrl.GetImageList(LVSIL_SMALL);
	pImageList = GetImageList(LVSIL_SMALL);
	if (pImageList)
	{
		UINT nOvlImageMask=lvi.state & LVIS_OVERLAYMASK;
		if (rcItem.left<rcItem.right-1)
		{
			ImageList_DrawEx(pImageList->m_hImageList, lvi.iImage,
					pDC->m_hDC,rcIcon.left,rcIcon.top, 16, 16,
					m_clrBkgnd, clrImage, uiFlags | nOvlImageMask);
		}
	}

// draw item label
*/
	GetItemRect(nItem, rcItem, LVIR_LABEL);
	rcItem.right -= 1; //m_cxStateImageOffset;

	pszText = MakeShortString(pDC, szBuff,
				rcItem.right-rcItem.left, 2*OFFSET_FIRST);

	rcLabel = rcItem;
	rcLabel.left += OFFSET_FIRST;
	rcLabel.right -= OFFSET_FIRST;

	pDC->DrawText(pszText,-1,rcLabel,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
// draw labels for extra columns

	LV_COLUMN lvc;
	lvc.mask = LVCF_FMT | LVCF_WIDTH;
				   
	DWORD memptr = GetItemData(nItem);
	char bf[256];
	for(int nColumn = 1; GetColumn(nColumn, &lvc); nColumn++)
	{
		rcItem.left = rcItem.right;
		rcItem.right += lvc.cx;

		if (nColumn == 1) {
			int nRetLen = GetItemText(nItem, nColumn,
							szBuff, sizeof(szBuff));
			if (nRetLen == 0) 
				continue;
			pszText = MakeShortString(pDC, szBuff,
					rcItem.right - rcItem.left, 2*OFFSET_OTHER);
		}
		else {
			DWORD addr = GetItemData(nItem);
			if (addr >= 0 && addr < VMEMIDMAX) {
				BYTE &p = m_pVMEM[addr];
				sprintf(bf,"%2.2X",p);
				pszText = bf;
			}
			else pszText = "*";
		}

		UINT nJustify = DT_LEFT;

		if(pszText == szBuff)
		{
			switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
			{
//			case LVCFMT_LEFT:
//				nJustify = DT_LEFT;
//				break;
			case LVCFMT_RIGHT:
				nJustify = DT_RIGHT;
				break;
			case LVCFMT_CENTER:
				nJustify = DT_CENTER;
				break;
			default:
				break;
			}
		}

		rcLabel = rcItem;
		rcLabel.left += OFFSET_OTHER;
		rcLabel.right -= OFFSET_OTHER;

		pDC->DrawText(pszText, -1, rcLabel,
			nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
	}

// draw focus rectangle if item has focus

	if (lvi.state & LVIS_FOCUSED && bFocus)
		pDC->DrawFocusRect(rcAllLabels);

// set original colors if item was selected

	if (bSelected)
	{
        pDC->SetTextColor(clrTextSave);
		pDC->SetBkColor(clrBkSave);
	}
	
//	CListCtrl::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

extern CMemView * _pCMemView;
void CMyList::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	UINT uFlags = 0;
	int nHitItem = HitTest(point, &uFlags);

	if (uFlags & LVHT_ONITEM)
	{
		// double click works only if we don't have state icons,
		// or if we are in icon or small icon view
//		if (!m_bStateIcons || GetViewType() == LVS_ICON ||
//			GetViewType()==LVS_SMALLICON)
//		{
//			CheckItem(nHitItem);
//		}

//		if (m_nIsBitBlock) {
//			DWORD offset = GetItemData( nHitItem );
//			m_pVMEM[offset] ^= 1;
//			Invalidate( FALSE );
//		}

		if (_pCMemView) {
			DWORD offset = GetItemData( nHitItem );
			CSimObject *pObj = (CSimObject*)offset;
			if (pObj->IsKindOf(RUNTIME_CLASS(CSimObject))) {
				pObj->DialogOpen();
			}
	//		m_pVMEM[offset] ^= 1;
			Invalidate( FALSE );
		}
	}
	
	CListCtrl::OnLButtonDown(nFlags, point);
}

void CMyList::OnLButtonDblClkByLSM(CString strObjName)
{
	DWORD offset;
	CSimObject *pObj = NULL;
	for (int k = 0; k < GetItemCount();k++)
	{
		offset = GetItemData(k);
		pObj = (CSimObject*)offset;
		if( pObj->m_strName == strObjName )
			break;
	}

	if (pObj->IsKindOf(RUNTIME_CLASS(CSimObject))) {
		pObj->DialogOpen();
	}
}

void CMyList::Update()
{
	if(!IsWindowVisible()) return;
	CRect rect;
	GetClientRect( &rect );
//	Invalidate( TRUE );
	rect.left = rect.right - 30;
	InvalidateRect( &rect, FALSE );
}

void CMyList::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp) 
{
	// TODO: Add your message handler code here and/or call default
	ModifyStyle(WS_HSCROLL, 0);

	
	CListCtrl::OnNcCalcSize(bCalcValidRects, lpncsp);
}
