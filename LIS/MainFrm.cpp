// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "LIS.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern BOOL _bSIMModeIO;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	memset(&INPUT, 0, sizeof(INPUT));
	memset(&OUTPUT, 0, sizeof(OUTPUT));
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	int nSimConsolePort = 5100;
	int nSimulatorPort = 5100;
	CString strSimConsoleIP = "192.168.10.11";
	CString strSimulatorIP = "192.168.10.10";

	BOOL bCreateSocket = FALSE;
	
	bCreateSocket = m_pSimulatorComm.CreateSocket(m_hWnd, strSimConsoleIP, nSimConsolePort, strSimulatorIP, nSimulatorPort);

	if ( bCreateSocket )
	{
		SetTimer( SEND_DATA, 250, NULL );
	}
	else
	{
		TRACE("\nSocket ���� ����...\n\n");
	}

	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
// 	if (!m_wndToolBar.Create(this) ||
// 		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
// 	{
// 		TRACE0("Failed to create toolbar\n");
// 		return -1;      // fail to create
// 	}
// 	if (!m_wndStatusBar.Create(this) ||
// 		!m_wndStatusBar.SetIndicators(indicators,
// 		  sizeof(indicators)/sizeof(UINT)))
// 	{
// 		TRACE0("Failed to create status bar\n");
// 		return -1;      // fail to create
// 	}
// 
// 	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
// 	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
// 		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
// 
// 	// TODO: Delete these three lines if you don't want the toolbar to
// 	//  be dockable
// 	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
// 	EnableDocking(CBRS_ALIGN_ANY);
// 	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CMDIFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	if ( nIDEvent == SEND_DATA && _bSIMModeIO == TRUE )
	{
		m_pSimulatorComm.SendData(&OUTPUT.Data[0], COM_LENGTH);
	}

	CMDIFrameWnd::OnTimer(nIDEvent);
}

BOOL CMainFrame::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	COMCommandType *DestTable;
	UINT commandID = (UINT)wParam;
	
	switch (commandID) 
	{
	case ID_COMM_WATCH:			
		DestTable = (COMCommandType *)lParam;
		memcpy(&INPUT.Data[0], DestTable->pData, sizeof(INPUT));
		return TRUE;
		
	default:
		break;
	}

	return CMDIFrameWnd::OnCommand(wParam, lParam);
}
