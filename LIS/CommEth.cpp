// CommEth.cpp : implementation file
//

#include "stdafx.h"
#include "LIS.h"
#include "CommEth.h"
// #include "CRCCCITT.h"
// #include "LESView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommEth

CCommEth::CCommEth()
{
	m_hParentWnd = 0;
	m_nCmdCounter = 0;
}

CCommEth::~CCommEth()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CCommEth, CAsyncSocket)
	//{{AFX_MSG_MAP(CCommEth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CCommEth member functions

void CCommEth::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString strRcvIP;
	UINT nRcvPort;

	BYTE	szBuffer[4096] = {0,};
	UINT	nRecvSize = 0, nSentSize = 0;

	nRecvSize = ReceiveFrom(szBuffer, 4096, strRcvIP, nRcvPort, 0);

	if ( !(nRcvPort == m_nSimulatorPort && strRcvIP == m_strSimulatorIP))
	{
		return;
	}

	CLISApp *pApp = (CLISApp*)AfxGetApp();
	BYTE *p = (BYTE*)szBuffer;
	BOOL bCallParent = FALSE;

	if ( szBuffer[0] == COM_DLE && szBuffer[1] == COM_STX && szBuffer[2] == 0x00 && szBuffer[3] == (COM_DATA_LENGTH + 2) && szBuffer[5] == COM_OP_IOSCAN )
	{
		BOOL bCrcOK = CheckCRC(&szBuffer[0], COM_CRC_LENGTH);

		if ( bCrcOK == TRUE )
		{
			if ( m_hParentWnd )
			{
				memcpy(&m_pDataQ[0], &szBuffer[6], COM_DATA_LENGTH);
				m_PostDB.Port = m_nSimConsolePort;
				m_PostDB.Length = nRecvSize;
				m_PostDB.pData = &m_pDataQ[0];

// 				SendMessage(m_hParentWnd, WM_COMMAND, ID_COMM_WATCH, (LPARAM)&m_PostDB);					
				PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_WATCH, (LPARAM)&m_PostDB);					
			}
		}
		else
		{
			szBuffer[0] = 0;
//			AfxMessageBox("CRC error!");
			PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_ERRMSG, NULL);
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

BOOL CCommEth::CreateSocket(HWND hWnd, CString strSimConsoleIP, UINT nSimConsolePort, CString strSimulatorIP, UINT nSimulatorPort)
{
	m_hParentWnd = hWnd;
	m_strSimConsoleIP	= strSimConsoleIP;
	m_nSimConsolePort	= nSimConsolePort;
	m_strSimulatorIP	= strSimulatorIP;
	m_nSimulatorPort	= nSimulatorPort;

	return Create(m_nSimConsolePort, SOCK_DGRAM, FD_READ|FD_WRITE, m_strSimConsoleIP);
}

BOOL CCommEth::SendData(BYTE* data, int len)
{
	int nDataLength = 6;

	memset(m_pTxQ, 0, COM_LENGTH );
	m_pTxQ[0] = COM_DLE;
	m_pTxQ[1] = COM_STX;
	m_pTxQ[2] = 0x00;
	m_pTxQ[3] = 34;
	m_pTxQ[4] = m_nCmdCounter;
	m_pTxQ[5] = 0x83;

	for(; nDataLength < COM_CRC_LENGTH;nDataLength++)
	{
		m_pTxQ[nDataLength] = *data++;
	}

	MakeCRC(&m_pTxQ[0], COM_CRC_LENGTH, &m_CRCLow, &m_CRCHigh);

	// CRC1
	m_pTxQ[nDataLength++] = m_CRCLow;
	// CRC2
	m_pTxQ[nDataLength++] = m_CRCHigh;

	m_pTxQ[nDataLength++] = COM_DLE;
	m_pTxQ[nDataLength++] = COM_ETX;
	
	if ( nDataLength == COM_LENGTH )
	{
		SendTo(&m_pTxQ[0], COM_LENGTH, m_nSimulatorPort, m_strSimulatorIP);

		if (m_nCmdCounter == 0xFF)
			m_nCmdCounter = 0x01;
		else
			m_nCmdCounter++;
	}

	return TRUE;
}

void CCommEth::MakeCRC(BYTE* sndBuffer, int rcvLen, BYTE* crcLow, BYTE* crcHigh )
{

	USHORT crct = 0;
	for (USHORT lenIdx = 0; lenIdx < rcvLen; lenIdx++)
	{
		crct = (USHORT)((crct & 0xff00) | (crct ^ (USHORT)(sndBuffer[lenIdx] & 0xff)));

		for (BYTE relenIdx = 0; relenIdx < 8; relenIdx++)
		{
			if ((crct & 0x0001) > 0)
			{
				crct = (USHORT)((crct >> 1) ^ 0xa001);
			}
			else
			{
				crct = (USHORT)(crct >> 1);
			}
		}
	}

	*crcLow = (BYTE)(crct & 0x00ff);
	*crcHigh = (BYTE)((crct >> 8) & 0xff);
}

BOOL CCommEth::CheckCRC(BYTE* rcvBuffer, int rcvlen)
{
	bool crcCheck = false;
	
	BYTE cmpCrcBuffer[2] = {0,};
	
	MakeCRC(rcvBuffer, rcvlen, &cmpCrcBuffer[0], &cmpCrcBuffer[1]);
	
	if ((cmpCrcBuffer[0] == rcvBuffer[rcvlen]) &&
		(cmpCrcBuffer[1] == rcvBuffer[rcvlen + 1]))
		crcCheck = true;
	
	return crcCheck;
}



