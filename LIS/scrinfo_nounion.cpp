// scrinfo.cpp :  implementation of the CScrInfo class
//

#include "stdafx.h"
#include "scrinfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

//BYTE CMessageInfo::m_uFilter = LOG_MSG_ALL;
//BYTE *CScrInfo::m_pOldVMEM = NULL;
//BYTE *CScrInfo::m_pCurVMEM = NULL;

/////////////////////////////////////////////////////////////////////////////

CScrInfo::CScrInfo()
{
	m_nMemOffset = -1;
	m_nID = -1;
    m_pEipData = NULL;
}

/*
void CScrInfo::Save( FILE *fp )
{
    short n = m_strName.GetLength();
    fwrite( &n, sizeof( short ), 1, fp );
    fwrite( (LPCTSTR)m_strName, n, 1, fp );
    fwrite( &m_nMemOffset, sizeof( short ), 1, fp );
    fwrite( &m_nType, sizeof( short ), 1, fp );
    fwrite( &m_nID, sizeof( short ), 1, fp );
}

int CScrInfo::Load( FILE *fp )
{
    short n;
    fread( &n, sizeof( short ), 1, fp );
    char *bf = new char[n+2];
    fread( bf, n, 1, fp );
    bf[n] = 0;
    m_strName = bf;
    delete [] bf;
    fread( &m_nMemOffset, sizeof( short ), 1, fp );
    fread( &m_nType, sizeof( short ), 1, fp );
    fread( &m_nID, sizeof( short ), 1, fp );
    return 0;
}
*/

BYTE ScrInfoSignal::GetSignalControlState( int nSignalType )
{
	BYTE nControlState = 0;
	switch ( nSignalType ) {
	case 3:	// 5 현시
		if ( SE3 && SOUT3 && !SOUT2 && !SOUT1 )	// Y1 Lamp
			nControlState |= 1;
	case 1:	// 3 현시
	case 2:	// 4 현시
		if ( SE3 && SOUT3 && !(SOUT2 && SOUT1) ) // Y Lamp
			nControlState |= 8;
		if ( !SE3 ) {// R Lamp
			nControlState |= 4;
		}
		else {
			if (!SOUT3) {
				if ( SOUT2 ) nControlState |= 8;
				if ( SOUT1 ) nControlState |= 1;
			}
		}
		if ( SE3 && SE2 && SOUT3 && SOUT2 ) // G Lamp
			nControlState |= 2;
		break;
	case 0:	// 2 현시
	default:
		if ( !SE3 ) // R Lamp
			nControlState |= 4;
		else  // G Lamp
			nControlState |= 2;
		break;
	}
	if (nControlState == 0) nControlState = 4;
	return nControlState;
}

BYTE ScrInfoSignal::GetSignalLampState()
{
	BYTE LMRBit = 0;
	BYTE *pData = (BYTE*)this;
	BYTE bLMR = pData[3];
	for (int loc=0; loc < 4; loc++) {
		LMRBit <<= 1;
		if ( bLMR & 3 )	LMRBit |= 1;
		bLMR >>= 2;
	}
	return LMRBit;
}
