#if !defined(AFX_MYGENERALL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_)
#define AFX_MYGENERAL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyGeneral.h : header file
//

#include "resource.h"
#include "simobj.h"
#include "../include/iolist.h"

class CEITInfo;
class CMyGeneral : public CSimObject
{
//    DECLARE_SERIAL( CMyPoint )
public:
	CDialog * m_pDialog;
	void DialogOpen();
	void Run();
	CMyGeneral( CEITInfo *pInfo );
	virtual ~CMyGeneral() {}

//	IOBitlocSignal *m_pIO;
};

/////////////////////////////////////////////////////////////////////////////

// CMyGeneralDlg dialog

class CMyGeneralDlg : public CDialog
{
// Construction
public:
	CMyGeneralDlg( ScrInfoLamp *pInfo, CWnd* pParent = NULL );   // standard constructor

	ScrInfoLamp *m_pInfo;
// Dialog Data
	//{{AFX_DATA(CMyGeneralDlg)
	enum { IDD = IDD_DIALOG_GENERAL };
	BOOL	m_bBIT0;
	CString	m_strName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyGeneralDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMyGeneralDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_MYGENERAL_H__C4C8CD42_A487_11D2_BA70_00C06C003C49__INCLUDED_)
