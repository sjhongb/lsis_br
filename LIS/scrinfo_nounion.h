// ScrInfo.h
//

#ifndef _SCRINFO_H
#define _SCRINFO_H

/////////////////////////////////////////////////////////////////////////////

#define MAX_MSGBLOCK_SIZE			2048 
#define MAX_OPERATE_CMD_SIZE		16
//2000.6.20 #define MAX_OPERATE_CMD_CODESIZE	10
//2000.6.20 #define MAX_OPERATE_CMD_UNITCODE	6
#define MAX_OPERATE_CMD_CODESIZE	8
#define MAX_OPERATE_CMD_UNITCODE	8

/////////////////////////////////////////////////////////////////////////////

#define SCRINFO_TRACK	1
#define SCRINFO_SIGNAL	2
#define SCRINFO_SWITCH	3
#define SCRINFO_BUTTON	4
#define SCRINFO_LAMP	5
#define SCRINFO_SYSTEM	99

/////////////////////////////////////////////////////////////////////////////

/*
class CScrInfo 
{
public:
// VMEM Information data
	CString m_strName;
	BYTE *m_pEipData;
	short m_nMemOffset;
	short m_nType;
	short m_nID;
// VMEM compare data
//	int   m_nOldPos;
//	int   m_nNewPos;

//public:
//	static BYTE *m_pOldVMEM;
//	static BYTE *m_pCurVMEM;

public:
	CScrInfo();
};
*/

#ifdef _WINDOWS
#include "stdio.h"
#endif

typedef unsigned char BYTE;

class CScrInfo {
// attribute
public:
//	int Load( FILE *fp );
//	void Save( FILE *fp );
//2000-11-21	CString m_strName;
	char m_szName[32];
	BYTE *m_pEipData;
	short m_nMemOffset;
	short m_nType;
	short m_nID;

// Constructor
public:
	CScrInfo();
};

/////////////////////////////////////////////////////////////////////////////
// Field Bit position of SystemStatusType struct.
typedef enum SysVarNoType {
// Byte 0
	BitNO_CTC = 64,     // Base Address = VMEM[8];
	BitNo_CTCREQ,
	BitNo_LOCAL,
	BitNo_LOCALREQ,
	BitNo_SYS1,
	BitNo_SYS1GOOD,
	BitNo_SYS2,
	BitNo_SYS2GOOD,
// Byte 1
	BitNo_N1,
	BitNo_N1GOOD,
	BitNo_N2,
	BitNo_B24PWR,
	BitNo_UPS1,
	BitNo_UPS2,
	BitNo_FUSE,
	BitNo_C,
// Byte 2
	BitNo_D,
	BitNo_S,
	BitNo_P,
	BitNo_Sound1,		// 접근 
	BitNo_Sound2,		// S,P 부저(경고) 
	BitNo_Sound3,		// Power 경고
	BitNo_Sound4,		// 폐색
	BitNo_Sound5,
// Byte 3
	BitNo_SysRun,
	BitNo_Reserved1,
	BitNo_SysFail,
	BitNo_StartUp,
	BitNo_SubRequest,
	BitNo_Active,
    BitNo_ComUsed,
	BitNo_ComFail
};

/////////////////////////////////////////////////////////////////////////////

// Base Address 0x08
typedef struct {
//	BYTE pVar[4];
//	struct {
		BYTE bCTC		:1;
		BYTE bCTCReq	:1;
		BYTE bLOCAL		:1;
		BYTE bLOCALReq	:1;
		BYTE bSYS1 		:1;
		BYTE bSYS1Good	:1;
		BYTE bSYS2 		:1;
		BYTE bSYS2Good	:1;

		BYTE bN1   		:1;
		BYTE bN3	   	:1;
		BYTE bN2  		:1;
		BYTE bB24PWR  	:1;
		BYTE bUPS1 		:1;
		BYTE bUPS2		:1;
		BYTE bFUSE 		:1;
		BYTE bC			:1;

		BYTE bD			:1;     // 0x0A
		BYTE bS    		:1;
		BYTE bP    		:1;
        BYTE bSound1    :1;		// 접근 
        BYTE bSound2    :1;		// S,P 부저(경고) 
        BYTE bSound3    :1;		// Power 경고
        BYTE bSound4    :1;		// 폐색
        BYTE bSound5    :1;		// 

        BYTE bSysRun	:1;		// 0/fail-flashing, 1/normal
		BYTE reserved1  :1;
        BYTE bSysFail   :1;		// bSysGood 과 같음. 장애 발생시 Set(Cpu 동작중의 장애 발생시 포함) 
		BYTE bStartUp 	:1;		// 
        BYTE bSubRequest:1;		// 자기계가 서브로 전환할려고 상대계에 요구
        BYTE bActive    :1;		// 
        BYTE bComUsed   :1;
        BYTE bComFail   :1;
//	} bit;
} SystemStatusType;

/////////////////////////////////////////////////////////////////////////////

typedef struct {
//	BYTE pVar[1];
//	struct {
		BYTE bOpt1Main	:1;
		BYTE bOpt2Main	:1;
		BYTE bOpt1Fail	:1;
		BYTE bOpt2Fail	:1;
		BYTE bTimeError	:1;
		BYTE bSystemOn	:1;	// desk computer on
		BYTE bSystemOff	:1;	// desk computer off
		BYTE bSpare		:1;
//	} bit;
} OPStatusType;

/////////////////////////////////////////////////////////////////////////////

typedef struct {
//	BYTE pVar[1];
//	struct {
		BYTE bCom1CardFail	:1;
		BYTE bCom2CardFail	:1;
		BYTE bDts1CardFail	:1;
		BYTE bDts2CardFail	:1;
		BYTE res_sysrun	:4;
//	} bit;
} UnitStateType;

/////////////////////////////////////////////////////////////////////////////
/*
#define ID_BUTTON_CTC	0x0E0
#define ID_BUTTON_LOCAL	0X0E1
#define ID_BUTTON_SYS1	0X0E2
#define ID_BUTTON_SYS2	0X0E3
#define ID_BUTTON_N1	0X0E4
#define ID_BUTTON_N2	0X0E5
#define ID_BUTTON_UPS	0X0E6
#define ID_BUTTON_ACR	0X0E7
#define ID_BUTTON_FUSE	0X0E8
#define ID_BUTTON_C		0X0E9
#define ID_BUTTON_D		0X0EA
#define ID_BUTTON_S		0X0EB
#define ID_BUTTON_P		0X0EC
*/

typedef enum SysButtonIDTypes { 
// Id = 0xE0
	BtnCtc = 0x0E0,			// event
	BtnLocal,				// event
	BtnEMLocal,				// event
	BtnS,					// event
	BtnP,					// event
	BtnFuse,				// event
	BtnUps1,					// event
	BtnSysRun,				// event, System Run
// ID = 0xe8
	BtnSys1, 
	BtnSys2,
	BtnN1, 
	BtnN2, 
	BtnC, 
	BtnD, 
	BtnUps2, 
	BtnBls,
// ID = 0xf0
	BtnOpt1 = 0x0F0,		// User event
	BtnOpt2,
	BtnB24Pwr,
	BtnN3,
	BtnEnd 
};

/////////////////////////////////////////////////////////////////////////////
typedef struct {
//	union {
//		BYTE pData[32];			// 16->32, 2000.3.19 
//		struct {
			BYTE nMsgHeader;	// 부계:0x05, 주계0x0A
			BYTE nDataVer;		// 
			BYTE nSec;
			BYTE nMin;
			BYTE nHour;
			BYTE nDate;
			BYTE nMonth;
			BYTE nYear;
// Offset 0x08 - 0x0b (4 Byte)
			SystemStatusType nSysVar;
// Offset 0x0c
			BYTE Cycle;
// Offset 0x0d (1 Byte)
			OPStatusType nOPVar;	// Make status for EID SW(Not control system).
// Offset 0x0e, 0x0f
			UnitStateType	UnitState;	// SysRunState
			BYTE OtherState;
//			BYTE reserved[2];		// SysRunState, OtherState
// 추가 2000.3.19
// Offset 0x10
			BYTE IOC1Status[8];	// rack 1 - 8	D0:IOC, D1-D7:Card 1-7
			BYTE IOC2Status[8];	// 
//		} bit;
//	};
} DataHeaderType;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
//	union {
//		BYTE data;
//		struct {
//			BYTE BitIN		:1;
//			BYTE BitINTERNAL:7;
//		} bitio;
//		struct {
			BYTE ALARM		:1;	// 0 접근
// internal
			BYTE TRACK		:1; // 1
			BYTE ROUTE		:1;	// 2 1:NORMAL, 0/LOCK(GRREN)
			BYTE TKREQUEST	:1; // 3
			BYTE OVERLAP	:1; // 4
			BYTE ROUTEDIR	:1;	// 5 
// in
			BYTE TRPWRDN	:1;	// 사용금지 ( 단전 )
			BYTE INHIBIT	:1;	// 사용금지 (기타 사용금지)
//		} bit;
//	};
//	union {
//		BYTE TIMER;
//		struct {
//			BYTE TIMERL		: 4;
//			BYTE TIMERH		: 4;
//		};
//	};

//	BYTE TRAIN_ID[6];
} ScrInfoTrack;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
//	union {
//		BYTE pData[2];
//		struct {
//			BYTE BitIN		:4;
//			BYTE BitLOCK	:2;
//			BYTE BitOUT		:2;
//			BYTE BitINTERNAL:8;
//		};
//		struct {
// in
			BYTE KR_P		:1;     //0
			BYTE KR_M		:1;     //1
			BYTE WR_P		:1;     //2
			BYTE WR_M		:1;     //3
			BYTE WLR		:1;     //4
// out
			BYTE FREE		:1;		//5 1=전환가능 상태, 0 쇄정
			BYTE WRO_M		:1;     //6
			BYTE WRO_P		:1;     //7
// internal
			BYTE BACKROUTE  :1;     //8
			BYTE ROUTELOCK	:1;     //9
			BYTE REQUEST_P	:1;     //10
			BYTE REQUEST_M	:1;     //11
			BYTE ON_TIMER	:1;     //12

            BYTE WLRO       :1;     //13
            BYTE SWFAIL     :1;     //14
            BYTE TRACKLOCK  :1;     //15
//		} bit;
//	};
//	BYTE TIMER;	
} ScrInfoSwitch;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
			BYTE BLKREQOUT  :1;     // OUT  SD, NOT OF REQUEST
			BYTE BLKTKIN	:1;     // IN   TK
			BYTE BLKENDIN	:1;     // IN   SA
                                    // 장내
            BYTE BLKREQIN   :1;     // IN
			BYTE BLKTKOUT   :1;     // OUT
            BYTE BLKENDOUT  :1;     // OUT

            BYTE BLKROUTE   :1;
            BYTE BLKTKLAMP  :1;

            BYTE dumy1      :4;
            BYTE BLKINSGON  :1;

            BYTE BitINTERNAL2:8;
} ScrInfoBlock;

typedef struct {
// out
			BYTE SOUT1			:1;     //0 S1
			BYTE SOUT2			:1;     //1 S2
			BYTE SOUT3			:1;     //2 S3
			BYTE U			:1;     //3
// in
			BYTE SIN1		:1;     //4
			BYTE SIN2		:1;     //5
			BYTE SIN3		:1;     //6
			BYTE INU		:1;     //7
//=====================================
// OFFSET 1
// 필요한 제어신호, 실제 출력은 G,Y,YY로 나감.
			BYTE SE1		:1;     //8
			BYTE SE2	    :1;     //9
			BYTE SE3		:1;     //10
			BYTE SE4		:1;     //11

			BYTE ROUTE		:1;		//12 LM5
// internal
			BYTE FREE		:1;		//13	; 신호 현시 상태, 0-쇄정
			BYTE ENABLED	:1;		//14	; 신호현시 상태에서 전방 궤도가 낙하하여 신호가 정지한 경우 세트
			BYTE TTB		:1;     //15
//=====================================
// OFFSET 2
            BYTE _REQG      :1;     //16
			BYTE _LMYE		:1;     //17
			BYTE _LMY1E		:1;     //18
			BYTE REQUEST	:1;     //19
			BYTE ON_TIMER	:1;     //20
            BYTE LMRCHECK   :1;     //21
            BYTE LMRTIMER   :1;     //22
            BYTE SGFAIL     :1;		//23 0/normal, 1/fail
//=====================================
// OFFSET 3

            BYTE LM_YM      :1;     //24
            BYTE LM_YA      :1;     //25
            BYTE LM_RM      :1;     //26
            BYTE LM_RA      :1;     //27
            BYTE LM_GM      :1;     //28
            BYTE LM_GA      :1;     //29
            BYTE LM_Y1M     :1;     //30
            BYTE LM_Y1A     :1;     //31
//		} bit;
//	};
//	BYTE TIMER;
	BYTE DESTTRACK;

	BYTE GetSignalControlState( int nSignalType );
	BYTE GetSignalLampState();

} ScrInfoSignal;

/////////////////////////////////////////////////////////////////////////////
typedef struct {
//	union {
//		BYTE data;
//		struct {
			BYTE BitLamp	:1;
//		} bit;
//	};
} ScrInfoLamp;

/////////////////////////////////////////////////////////////////////////////
#define SIZE_INFO_TRACK		sizeof(ScrInfoTrack)
#define SIZE_INFO_SIGNAL	sizeof(ScrInfoSignal)
#define SIZE_INFO_SWITCH	sizeof(ScrInfoSwitch)
#define SIZE_INFO_LAMP		sizeof(ScrInfoLamp)

/////////////////////////////////////////////////////////////////////////////
typedef struct {
	union {
		BYTE data;
		struct {
			BYTE SHIFT_LEFT	:1;
			BYTE SHIFT_RIGHT	:1;
		} bit;
	};
} ScrInfoTrkNode;

/////////////////////////////////////////////////////////////////////////////
#endif