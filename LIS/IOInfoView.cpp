// IOInfoView.cpp : implementation file
//

#include "stdafx.h"
#include "lis.h"
#include "IOInfoView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIOInfoView

IMPLEMENT_DYNCREATE(CIOInfoView, CFormView)

CIOInfoView::CIOInfoView()
	: CFormView(CIOInfoView::IDD)
{
	//{{AFX_DATA_INIT(CIOInfoView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CIOInfoView::~CIOInfoView()
{
}

void CIOInfoView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIOInfoView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIOInfoView, CFormView)
	//{{AFX_MSG_MAP(CIOInfoView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIOInfoView diagnostics

#ifdef _DEBUG
void CIOInfoView::AssertValid() const
{
	CFormView::AssertValid();
}

void CIOInfoView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIOInfoView message handlers
