//=======================================================
//==              Jsdxf.h
//=======================================================
//	파 일 명 :  Jsdxf.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef PNTTYPE
#define PNTTYPE
typedef struct {
    double x,y;
} point;
#endif

#define VAL_SIZE 31
#define LAY_SIZE 8
#define TAG_SIZE 8
#define BLK_SIZE 8
#define TST_SIZE 10        // text style
#define TEXT_SIZE 255

class CBlock;
class LINE;
class ARC;
class ATEXT;
class LOCATOR;

class CDXFObject;

class ENTITIES {
		    // 0 SECTION
		    // 2 ENTITIES
   int page;
   friend LOCATOR;
protected:
   int ni;
   CBlock *ihead;
   CBlock *ilast;
   int nl;
   LINE *lhead;
   LINE *llast;
   int na;
   ARC *ahead;
   ARC *alast;
   int nt;
   ATEXT *thead;
   ATEXT *tlast;
		    // 0 ENDSEC
		    // 0 EOF
public:
   ENTITIES();
};

#define FILEBUFSIZE 32768
class COMP;
class DXFile : public ENTITIES {
	FILE *m_pFile;
	char *m_pHeaderBuffer;
	char *m_pObjectBuffer;
    char *m_pFileBuffer;
    long m_lReadPtr;
	long m_lHeaderSize;
	long m_lEntitySize;
	long m_lObjectSize;
	long m_lFileSize;
    char fname[80];
	char m_DxfCode[128];
	char m_DxfValue[256];

public:
	FILE *m_pOutFile;

	char m_szSheetType[256];
	char m_szSheetNo[256];
	char m_szProjectTitle[256];

	int m_nHandle, m_nReadHandle;
protected:
	int dxferror,line,dxfcode;
////	char bf[101];
// WARNING MAX CHAR SIZE 255...!!!!
	char bf[256];

	friend COMP;
public:
	char * CopyOneLine();
	int FindCode0();
	long CopyAndFindEntityLocation();
	long FindEntityLocation();
	long ReadToBuffer();
    DXFile(char*);
	~DXFile();
	char *extfname(char*);
	char *readbf();
	int read0();
	void readcode();
	void err(int);
	int  ReadFromDXF();
	void *rINSERT();
	void *rLINE();
	void *rARC();
	void *rATEXT();
	void rOTHER();
//   void packwrite();
   void freeclass();
//   int  packread();
   void WriteToDXF();

    void out(int,int);
    void out(int,double);

   void out(int,char);
   void out(int,char*);
   void picklhead(void **);
   void pickihead(void **);
   void pickahead(void **);
   void pickthead(void **);
   void InsertMark(point &p, char*);
   CBlock *FindBlock(point &p);
   void fMakeOn(char *fn, char *szType = "");
   void fMakeOff();
   void fAddText(char *s, double x, double y, char *szLayer = NULL );
   void fAddText(char *s, double x, double y, double h, int nVert = 0, char *szLayer = NULL );
   void fAddInsert(char *s, double x, double y);
   void fAddInsert(char *s, double x, double y, double, double);
   void fAddLine(double, double, double, double, int lType = 0);
   void fAddRect(double, double, double, double);
};

class CDxfCode {
public:
	CDxfCode();
	CDxfCode( int, char * );
	~CDxfCode();
	int m_nCode;
	char m_szData[120];
	CDxfCode *m_pNext;
};

class CDXFObject {
//protected:
public:
	CDxfCode *m_pAnyCode;

	char m_szName[16];
	char m_cHandle[8];
	char layer[LAY_SIZE+1];   // 8        "0"
	char style[TST_SIZE+1];
	char value[TEXT_SIZE+1];

//LINE
	point sp;        // 10,20
	point ep;        // 11,21
//ARC
	double r;           // 40	, for text, h
	double sa;          // 50
	double ea;          // 51
//TEXT
	int halign;     // 72
	int halign2;     // 73

	friend DXFile;
public:
	CDXFObject *m_pNext;
	CDXFObject *GetNext() { return m_pNext; }

	CDXFObject();
	~CDXFObject();

	void pack(DXFile*,int&,char*);
	void unpack(DXFile *d);
	void getsp(point&);
	void getep(point&);
	char *getlayer() {return layer;};

	void AddAnyCode( int nCode, char *pBf );
};

class CAttribute : public CDXFObject {
	char m_cHandle[8];
	char layer[LAY_SIZE+1];   // 8        "<REF>", "0"
	char color;      // 62       5       NONE 255
	point p;         // 10,20
	double texth;       // 40       2
	char value[VAL_SIZE+1];  // 1
	char tag[TAG_SIZE+1];     // 2        "<REF>"
	char attr;       // 70
	char style[TST_SIZE+1];   // 7        ROMANS
	double halign;     // 72       4
	double halign74;     // 72       4
	point ap;	    // 11
	friend CBlock;

public:
	CAttribute *m_pNext;
	CAttribute();
	~CAttribute();
   void pack(DXFile*,int&,char*);
   void unpack(DXFile *d);
   char *gettag();
   char *getvalue();
   void setvalue(char *);
   char *getlayer() {return layer;};
   int getcolor() {return color; };
};

class CBlock : public CDXFObject {
	char m_cHandle[8];
	char m_cAHandle[8];
	char layer[LAY_SIZE+1];   // 8
	char hasatt;     // 66       1
	char block[BLK_SIZE+1];   // 2
	point p;         // 10,20
	double m_dzdir;       // 41       -1      NONE 0
	CAttribute *att;     //

   friend DXFile;
   friend LOCATOR;
   friend COMP	;

public:
	char m_szRack[32];
	char m_szPos[32];
	char m_szContact[32];
	char m_szType[32];
	char m_szRef[32];

   CBlock *m_pNext;
   CBlock();
   void pack(DXFile*,int&,char*);
   void writeblocki(CBlock *head,FILE *wf);
   void readblocki(CBlock *&h,CBlock *&l,int n,FILE *rf);
   void unpack(DXFile *d);
   void getp(point&);
   void getattval(char*,char*,char*,int &,int &,int &);
    void AttChange(char *tag, char *s);
    void AttColorChange(char *tag, short color);
   void RefMake(char *blname, int dy, char *ext);
   char *getlayer() {return layer;};
   char *getblock() {return block;};
   void sort(CBlock **h, CBlock **t);
   void insert(CBlock *&head, CBlock *&last, CBlock *now);
	int  getposcolor();
	int  getrefcolor();
};

class LINE : public CDXFObject {
	friend DXFile;
public:
	char *m_pLineType;
	LINE();
	void unpack(DXFile *d);
};

class ARC : public CDXFObject {
public:
   ARC();
//   void pack(DXFile*,int&,char*);
   void unpack(DXFile *d);
   void get2p(point&,point&);
};

class ATEXT : public CDXFObject {
	friend DXFile;
public:
	ATEXT();
	void unpack(DXFile *d);
};

template <class T>
void insert(T*&head, T*&last, T*now) {
   if (!head) head = now;
   else last -> m_pNext = now;
   last = now;
   now -> m_pNext = NULL;
}

#define UP_INS 1
#define UP_POS 2
#define UP_REF 3

#define TEXTHEIGHT 1.1
extern DXFile MyDxf;

/*
class LOCATOR {
   char dxfname[80];            // DXF file name.
	int fatal, AutoType, AutoSeq, AutoIDF;
	point ip;

public:
	LOCATOR();
	int  NRfind(char *);
   void dxfrename();
   void run(char *,int);
   void prerun(char *,int);
   void err(char*,char*);
   void err(CBlock*,char*,char*);
	void CheckAutoFile(char *fname);
};


*/