//=======================================================
//==              SetMar.cpp
//=======================================================
//	파 일 명 :  SetMar.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "SetMar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetMar dialog


CSetMar::CSetMar(CWnd* pParent /*=NULL*/)
	: CDialog(CSetMar::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetMar)
	m_Top = 0;
	m_Left = 0;
	m_Right = 0;
	m_Bottom = 0;
	m_cTotalPage = TRUE;
	m_iStartPage = 0;
	m_iTotPage = 0;
	m_bCoverPage = FALSE;
	//}}AFX_DATA_INIT
	m_iTableType = 1;
}


void CSetMar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetMar)
	DDX_Text(pDX, IDC_EDIT3, m_Top);
	DDV_MinMaxUInt(pDX, m_Top, 0, 100);
	DDX_Text(pDX, IDC_EDIT1, m_Left);
	DDV_MinMaxUInt(pDX, m_Left, 0, 100);
	DDX_Text(pDX, IDC_EDIT2, m_Right);
	DDV_MinMaxUInt(pDX, m_Right, 0, 100);
	DDX_Text(pDX, IDC_EDIT4, m_Bottom);
	DDV_MinMaxUInt(pDX, m_Bottom, 0, 100);
	DDX_Check(pDX, IDC_CHK_TOTALPAGE, m_cTotalPage);
	DDX_Text(pDX, IDC_EDIT5, m_iStartPage);
	DDX_Text(pDX, IDC_EDIT6, m_iTotPage);
	DDX_Check(pDX, IDC_CHK_COVER, m_bCoverPage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetMar, CDialog)
	//{{AFX_MSG_MAP(CSetMar)
	ON_BN_CLICKED(IDC_RADIO_TYPE1, OnRadioType1)
	ON_BN_CLICKED(IDC_RADIO_TYPE2, OnRadioType2)
	ON_BN_CLICKED(IDC_RADIO_TYPE3, OnRadioType3)
	ON_BN_CLICKED(IDC_RADIO_TYPE4, OnRadioType4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetMar message handlers

void CSetMar::OnRadioType1() 
{
	// TODO: Add your control notification handler code here
	m_iTableType = 1;
}

void CSetMar::OnRadioType2() 
{
	// TODO: Add your control notification handler code here
	m_iTableType = 2;
}

void CSetMar::OnRadioType3() 
{
	// TODO: Add your control notification handler code here
	m_iTableType = 3;	
}

void CSetMar::OnRadioType4() 
{
	// TODO: Add your control notification handler code here
	m_iTableType = 4;		
}

BOOL CSetMar::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if ( m_iTableType == 1 ) ((CButton*)GetDlgItem(IDC_RADIO_TYPE1))->SetCheck(TRUE);
	else if ( m_iTableType == 2 ) ((CButton*)GetDlgItem(IDC_RADIO_TYPE2))->SetCheck(TRUE);
	else if ( m_iTableType == 3 ) ((CButton*)GetDlgItem(IDC_RADIO_TYPE3))->SetCheck(TRUE);
	else if ( m_iTableType == 4 ) ((CButton*)GetDlgItem(IDC_RADIO_TYPE4))->SetCheck(TRUE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


