//=======================================================
//==              Jsutil.cpp
//=======================================================
//	파 일 명 :  Jsutil.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"

#include <ctype.h>
#include "jsutil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void swap(int &a, int &b) {
   int c;
   c = a;
   a = b;
   b = c;
}

void swap(short &a, short &b) {
   short c;
   c = a;
   a = b;
   b = c;
}

void swap(unsigned &a, unsigned &b) {
   int c;
   c = a;
   a = b;
   b = c;
}

void swap(long &a, long &b) {
   long c;
   c = a;
   a = b;
   b = c;
}

void pntswap(point &a, point &b) {
   point c;
   c = a;
   a = b;
   b = c;
}

int pntcmp(point &p1,point &p2) {
   return ((p1.x==p2.x) && (p1.y==p2.y));
}

char *stripp(char *s) {
   static char bf[20];
   char *d = bf;
   if (*s == '(') s++;
   while(*s && *s !=')' && *s != '.') *d++ = *s++;
   *d = 0;
   d--;
   while(d>bf && isdigit(*d)) *d-- = 0;
   return bf;
}

char *gettoken(char *&p , char *szDel ) {
	static char t[128];
	char *d = t;
	while(*p && strchr( szDel, *p )) p++;
	while(*p && !strchr( szDel, *p) ) *d++ = *p++;
	*d = 0;
	return t;
}

char *gettoken( char *&p ) {
	return gettoken( p, " " );
}

char *gettoken1(char *&p) {
   static char t[128];
   char *d = t;
   while(*p==' ') p++;
   while(*p && *p!=' ' && *p!=':' && *p!=';' && *p != '\n') *d++ = *p++;
   if (*p == ':') *d++ = *p++;
   *d = 0;
   return t;
}

char cLOCH(int x) {
   char c;
   c = x+'A';
   if (c>='I') c++;
   return c;
}

void MoveStr(char *d, const char *s, int n) {
   strncpy(d,s,n);
   *(d+n) = 0;
};
