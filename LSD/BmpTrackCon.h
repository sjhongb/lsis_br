//=======================================================
//====               BmpTrackCon.h                   ==== 
//=======================================================
//  파  일  명: BmpTrackCon.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: SVI 화면에 트랙을 표시한다.
//              
//=======================================================

#ifndef _BMPTRACKCON_H
#define _BMPTRACKCON_H

/////////////////////////////////////////////////////////////////////////////
// CBmpTrackCon document
#include "../Include/bmpinfo.h"
class CBmpLevelCross;
class CBmpSignal;
class CBmpButton;
class CBmpEtcObj;

class CLSDDoc;

class CBmpTrackCon : public CObject
{
protected:

public:
	float  m_fScaleX, m_fScaleY;
	short  m_nDotPerCell;

	CLSDDoc * GetDocument();
	void OnResetState();
	short LoadFileInfo(LPCTSTR pFileName) ;
	void SetFreeRouteNo(int no);
	void SetFileName(CString filename);
	//void SetConfig( BYTE config );
	void Save();
	void Open();
	int IsModified();
	int GetCount();
	void Serialize(CArchive& ar);
	void FindJoinTrack();
	void ChangeSwitchState(CBmpTrack *track);
	CBmpTrack *TrackFind(CBmpTrack* T, int n, int& node);
	void Destory();
	CString GetProgramVersion();
	void AddObject(TScrobj *pBmpinfo);
	void Initial();
	void Draw(CDC* pDC, const CRect& rcBounds, const CRect& rcInvalid);
	CBmpTrackCon( CWnd *wnd, short nDotPerCell = 10 );
	virtual ~CBmpTrackCon();

public:
	CString sFileName;
	CWnd  *m_pParentOwner;
	CBmpTrack *pTrack;
	CObArray	mTrackList;
	UINT type;	
	BOOL  Modified;
	BOOL IsFileOpen; // 화일을 open 했었는가?
	int				m_iFreeRouteNo;
	short m_nTTBID;
	char m_pszError[1024];
	void ChangeSVIFile( CString strFileName , CString strFileName1);
};

#endif