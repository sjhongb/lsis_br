//=======================================================
//==              TrkEdit.h
//=======================================================
//	파 일 명 :  TrkEdit.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  연동도표 수정 다이알로그 - 궤도
//
//=======================================================

#include "../include/StrList.h"

class CTrackEdit : public CDialog
{
// Construction
public:
	CTrackEdit(CString &name, CMyStrList *trk, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTrackEdit)
	enum { IDD = IDD_TRACKEDIT };
	CComboBox	m_ListTrack;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrackEdit)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTrackEdit)
	afx_msg void OnTrackdel();
	afx_msg void OnTrackins();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString* m_pName;
	CMyStrList* m_pTrack;
};
