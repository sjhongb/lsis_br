//=======================================================
//==              Station.h
//=======================================================
//	파 일 명 :  Station.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CStationInfo : public CDialog
{
// Construction
	CLSDDoc *pDocument;
public:
	CStationInfo(CLSDDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CStationInfo)
	enum { IDD = IDD_STATIONINFO };
	CString	m_FromStation;
	CString	m_LineName;
	CString	m_NextStation;
	CString	m_PrevStation;
	int		m_nType;
	int		m_nDir;
	int		m_nUpDn;
	CString	m_StationName;
	CString	m_ToStation;
	CString	m_Kilo;
	int		m_Type;
	BOOL	m_bViewAll;
	BOOL	m_bOverlapCK;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStationInfo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStationInfo)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
