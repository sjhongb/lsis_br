//=======================================================
//==              PointLockEdit.h
//=======================================================
//	파 일 명 :  PointLockEdit.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#if !defined(AFX_POINTLOCKEDIT_H__83A23F39_AB47_4F78_9DBF_1E2DC526F433__INCLUDED_)
#define AFX_POINTLOCKEDIT_H__83A23F39_AB47_4F78_9DBF_1E2DC526F433__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PointLockEdit.h : header file
//

#include "../include/StrList.h"

/////////////////////////////////////////////////////////////////////////////
// CPointLockEdit dialog

class CPointLockEdit : public CDialog
{
// Construction
public:
	CPointLockEdit(CString &name, CMyStrList *sw, CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CPointLockEdit)
	enum { IDD = IDD_POINTLOCKEDIT };
		// NOTE: the ClassWizard will add data members here
	CComboBox	m_ListSwitch;
	CComboBox	m_ListSignal;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPointLockEdit)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPointLockEdit)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	afx_msg void OnLockSwitchdel();
	afx_msg void OnLockSignaldel();
	afx_msg void OnLockSwitchins();
	afx_msg void OnLockSignalins();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString* m_pName;
	CMyStrList* m_pSignal;
	CMyStrList* m_pSwitch;
	CMyStrList Point;
	CMyStrList OverlapPoint;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POINTLOCKEDIT_H__83A23F39_AB47_4F78_9DBF_1E2DC526F433__INCLUDED_)

