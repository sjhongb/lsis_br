//=======================================================
//==              TableFrm.cpp
//=======================================================
//	파 일 명 :  TableFrm.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "TableFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableFrame

IMPLEMENT_DYNCREATE(CTableFrame, CMDIChildWnd)

//=======================================================
//
//  함 수 명 :  CTableFrame
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CTableFrame::CTableFrame()
{
}

//=======================================================
//
//  함 수 명 :  ~CTableFrame
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CTableFrame::~CTableFrame()
{
}


BEGIN_MESSAGE_MAP(CTableFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTableFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableFrame message handlers

//=======================================================
//
//  함 수 명 :  OnCreate
//  함수출력 :  int
//  함수입력 :  LPCREATESTRUCT lpCreateStruct
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  프레임 생성시 초기화 작업
//
//=======================================================
int CTableFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_TableToolBar.InitToolBar(this)) return -1;
	m_TableToolBar.SetWindowText("Graph information");
	// TODO: Add your specialized creation code here

	m_bmToolbarHi.LoadBitmap( IDB_TABLETOOL );
	m_TableToolBar.SetBitmap( (HBITMAP)m_bmToolbarHi);

	m_TableToolBar.SetBarStyle(m_TableToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY); // | CBRS_SIZE_DYNAMIC);

	m_TableToolBar.EnableDocking(CBRS_ALIGN_ANY);

	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_TableToolBar);
	return 0;
}

#define COMBOBOX_WIDTH  180	//콤보박스 가로크기
#define COMBOBOX_HEIGHT 20	//콤보박스 세로크기

//=======================================================
//
//  함 수 명 :  InitToolBar
//  함수출력 :  BOOL
//  함수입력 :  CWnd* pParentWnd
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  툴바를 초기화한다.
//
//=======================================================
BOOL CTableToolBar::InitToolBar(CWnd* pParentWnd)
{
	// 스타일을 정해놓고
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_SIZE_DYNAMIC;
	//툴바를 만든후에 툴바에 비트맵 을 로드한다.
	if (!Create(pParentWnd,dwStyle) ||
		!LoadToolBar(IDR_SHEETTOOLBAR))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}	


	//콤보박스를 툴바에 위치하게 만든다.
	CRect rect(-COMBOBOX_WIDTH, -COMBOBOX_HEIGHT, 0, 0);
	if (!m_btnHide.Create("Hide Modify",
		WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 
		rect, this,
		IDC_HIDE))
	{
		return FALSE;
	}
	SetBarStyle(GetBarStyle() | CBRS_ALIGN_TOP | CBRS_TOOLTIPS | CBRS_FLYBY);
	SetButtonInfo(1, IDC_HIDE, TBBS_SEPARATOR, COMBOBOX_WIDTH);

	if (m_btnHide.m_hWnd != NULL)
	{
		CRect rect;
		GetItemRect(1, rect);
		m_btnHide.SetWindowPos(NULL, rect.left + 10, rect.top + 5, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOCOPYBITS);
		m_btnHide.ShowWindow(SW_SHOW);
	}

	return TRUE;
}

