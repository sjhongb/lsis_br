//=======================================================
//==              dotchange.h
//=======================================================
//	파 일 명 :  dotchange.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CDotChange : public CDialog
{
// Construction
public:
	CDotChange(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDotChange)
	enum { IDD = IDD_DOT_CHANGE };
	int		mdot;
	int		m_iOffSetX;
	int		m_iOffSetY;
	BOOL	m_bReverse;
	int		m_nXSize;
	int		m_nYSize;
	int		m_nSystemY;
	BOOL	m_bSmallFont;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDotChange)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDotChange)
	virtual void OnOK();
	afx_msg void OnReverse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
