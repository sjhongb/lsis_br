//=======================================================
//==              TableFrm.h
//=======================================================
//	파 일 명 :  TableFrm.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  1.01
//	설    명 :  연동도표를 보여주는 뷰의 프레임
//
//=======================================================

class CTableToolBar: public CToolBar
{
public:
    CButton m_btnHide;

    BOOL InitToolBar(CWnd* pParentWnd);
};

class CTableFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CTableFrame)
protected:
	CTableFrame();           // protected constructor used by dynamic creation

// Attributes
public:
	CTableToolBar m_TableToolBar;
	CBitmap    m_bmToolbarHi;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTableFrame)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTableFrame();

	// Generated message map functions
	//{{AFX_MSG(CTableFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
