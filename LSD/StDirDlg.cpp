//=======================================================
//==              StDirDlg.cpp
//=======================================================
//	파 일 명 :  StDirDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "StDirDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjDirectDlg dialog


CObjDirectDlg::CObjDirectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjDirectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjDirectDlg)
	m_sNext = _T("");
	m_sOrg = _T("");
	m_bReverse = FALSE;
	m_bNoInfo = FALSE;
	//}}AFX_DATA_INIT
}


void CObjDirectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjDirectDlg)
	DDX_Text(pDX, IDC_STATION_NEXT, m_sNext);
	DDX_Text(pDX, IDC_STATION_ORG, m_sOrg);
	DDX_Check(pDX, IDC_CHECK_REVERSE, m_bReverse);
	DDX_Check(pDX, IDC_CHK_NOINFO, m_bNoInfo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CObjDirectDlg, CDialog)
	//{{AFX_MSG_MAP(CObjDirectDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjDirectDlg message handlers
