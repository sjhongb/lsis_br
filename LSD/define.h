// 신호기 타입 정의 
#define _HOME         0
#define	_START        1
#define	_GSHUNT       2
#define	_ASTART       3
#define	_OHOME        4
#define	_TOKENLESSC   5
#define	_TOKENLESSG   6
#define	_OC			  7
#define	_OG		      8
#define	_LOS		  9
#define	_FREE        10
#define	_STOP        11
#define	_ISTART      12
#define	_ISHUNT      13
#define	_SHUNT       14
#define	_CALLON      15
#define	_OCALLON     16
#define	_PSTART      17
#define	_ANY		 99
// 버튼 타입 정의
#define _BSTART       0
#define _BSHUNT       1
#define _BCOMMON      2
#define _BISTART      3
#define _BSDG	      4

#define PSMODE0		  0
#define PSMODE1		  1
#define PSMODE2		  2
#define PSMODE3		  3
#define PSMODE4		  4
#define PSMODE5		  5
#define PSMODE6		  6
#define PSMODE7		  7
#define PSMODE8		  8
#define PSMODE9		  9
#define PSMODE10	 10
#define PSMODE11	 11
#define PSMODE12	 12
#define PSMODE13	 13
#define PSMODE14	 14
#define PSMODE15	 15

#define _C            1
#define _N            2
#define _R            3

#define DN			  1
#define UP			  2

#define ETCOBJTYPE_STATIONNAME		1   // 역명
#define ETCOBJTYPE_LEFTARROWTL		2	// 인접역명 좌
#define ETCOBJTYPE_RIGHTARROWTL		3	// 인접역명 우
#define ETCOBJTYPE_LGCI         	4	// LG로고
#define ETCOBJTYPE_COUNTER       	5	// 계수기
#define ETCOBJTYPE_OPERATPANNEL    	6	// 조작반
#define ETCOBJTYPE_TEXT         	7	// Text
#define ETCOBJTYPE_MINISTATIONNAME  8	// 미니역
#define ETCOBJTYPE_PLATFORM     	9	// 조작반
#define ETCOBJTYPE_WARNINGL   		11	// LG로고
#define ETCOBJTYPE_WARNINGR   		12	// LG로고
#define ETCOBJTYPE_LEFTARROWTEXT	13	// 인접역명 좌
#define ETCOBJTYPE_RIGHTARROWTEXT	14	// 인접역명 우

#define ETCOBJTYPE_SYSBUTTON		20  // System 버튼
#define ETCOBJTYPE_OPTIONBUTTON	    21  // Option 버튼
#define ETCOBJTYPE_BLOCKBUTTON	    22  // Block 버튼
#define ETCOBJTYPE_PAS              23  // PAS 버튼
#define ETCOBJTYPE_TOGGLE          	24  // Toggle 버튼
#define ETCOBJTYPE_DIMMING          25  // Dimming 버튼
#define ETCOBJTYPE_RGB		        26	// RGB
#define ETCOBJTYPE_OSS		        27	// OSS 버튼
#define ETCOBJTYPE_EPK		        28	// APK 버튼   Hand Point 제어 버튼 남/북
#define ETCOBJTYPE_LEVELCROSS     	29	// 건널목
#define ETCOBJTYPE_LCA		     	30	// 건널목
#define ETCOBJTYPE_BCBL		        31	// OSS 버튼
#define ETCOBJTYPE_BCBR		        32	// OSS 버튼
#define ETCOBJTYPE_FLEVELCROSS     	33	// 건널목
#define ETCOBJTYPE_BRIDGE     	    34	// 다리(교량)
#define ETCOBJTYPE_LAMP_RT			35	// LDK, Block Signal Fail
#define ETCOBJTYPE_AXLE_COUNTER		36	// AXLE COUNTER


#define MAXDESTSIGNAL				50
#define MAXSTACKCNT				  2000