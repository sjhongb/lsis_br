//=======================================================
//====               BitmapClient.h                  ==== 
//=======================================================
//  파  일  명: BitmapClient.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.21
//  버      전: 2.01
//  설      명: MID의 배경 그림 (LG 산전 CI )을 표시해준다 
//              
//=======================================================

/////////////////////////////////////////////////////////////////////////////
// CBitmapClient window

class CBitmapClient : public CWnd
{
// Construction
public:
	CBitmapClient();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapClient)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBitmapClient();

	// Generated message map functions
protected:
	CBitmap m_bmp;
	//{{AFX_MSG(CBitmapClient)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
