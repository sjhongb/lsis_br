//=======================================================
//==              StOpDlg.h
//=======================================================
//	파 일 명 :  StOpDlg.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CObjOperateDlg : public CDialog
{
// Construction
public:
	CObjOperateDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CObjOperateDlg)
	enum { IDD = IDD_OBJOPERATE };
	int		m_nBlockLeft;
	int		m_nBlockRight;
	int		m_nType;
	int		m_nRC;
	BOOL	m_bRCLeft;
	BOOL	m_bRCRight;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjOperateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CObjOperateDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
