// ObjView.cpp : implementation file
//

#include "stdafx.h"
#include "LSD.h"
#include "ObjView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjView

IMPLEMENT_DYNCREATE(CObjView, CScrollView)

CObjView::CObjView()
{
}

CObjView::~CObjView()
{
}


BEGIN_MESSAGE_MAP(CObjView, CScrollView)
	//{{AFX_MSG_MAP(CObjView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjView drawing

void CObjView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

void CObjView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CObjView diagnostics

#ifdef _DEBUG
void CObjView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CObjView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CObjView message handlers
