//=======================================================
//====               Button.cpp                      ==== 
//=======================================================
//  파  일  명: Button.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 압구에 관한 정보 및 처리를 담당한다.
//              
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "Button.h"
#include "ButtonDl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(CMyButton, CGraphObject, 0)

CMyButton::CMyButton(int t, int cx, int cy) : CGraphObject(CRect(CPoint(cx,cy), CSize(5,5)),"b") {
	SetName("");
	m_nShape = B_NAME_B;
	m_nLR = 0;
	m_nType = 0;
	nB.y = GridSize * 2;
	m_nLine = 0;
	UpdateRect();
	Init();	
}

void CMyButton::Draw(CDC* pDC , COLORREF cColor) {
	if (!isValid()) return;
	CPen newPen;
	CBrush *oldbr;
	CBrush black(cColor);
	CString strLineNo;
	CPoint p1(x,y), p2(x,y);

	if ( m_nLine == -1 || m_nLine == 0 ) strLineNo = "B";
	else strLineNo.Format("%d",m_nLine);
    if( newPen.CreatePen( PS_SOLID, 1, cColor ) )
    {
        CPen* pOldPen = pDC->SelectObject( &newPen );

		int r = GridSize - 1;
		CRect cir(x-r,y-r,x+r,y+r);
		switch (m_nType) {
		case 0 :
			pDC->Ellipse(&cir);
			pDC->DrawText(strLineNo,cir,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			break;
		case 1 :
			oldbr = pDC->SelectObject(&black);
			pDC->Ellipse(&cir);
			pDC->DrawText(strLineNo,cir,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject(oldbr);
			break;
		case 2 :
			if (m_nLR) {
				p1.y -= r;
				p2.y += r;
			}
			else {
				p1.y += r;
				p2.y -= r;
			}
			pDC->Chord(cir,p2,p1);
			oldbr = pDC->SelectObject(&black);
			pDC->Chord(cir,p1,p2);
			pDC->DrawText(strLineNo,cir,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject(oldbr);
			break;
		case 3 :
			pDC->Ellipse(&cir);
			strLineNo = "I";
			pDC->DrawText(strLineNo,cir,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			break;
		case 4 :
			pDC->Ellipse(&cir);
			strLineNo = "S";
			pDC->DrawText(strLineNo,cir,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			break;
		}
/*
		if (m_nLine) {
			int dir;
			if (m_nLR) {
				dir = 1;
			}
			else {
				dir = -1;
			}
			CPoint pa(x - GridSize * dir, y);
			DrawAngle(pDC,pa,dir);
			if (m_nLine == 2) {
				pa.x -= GridSizeH * dir;
				DrawAngle(pDC,pa,dir);
			}
		}
*/
		if (m_nShape & B_NAME_B)
			TextOutAtPointCenter( pDC, nB, Name );
        pDC->SelectObject( pOldPen );
    }
}

void CMyButton::PropertyChange(CWnd *wnd) {
	CMyButtonDlg dlg(this);
	dlg.DoModal();
	Init();
}

BOOL CMyButton::isNoGrid(CBlip *pBlip)
{
	if (m_pBlips[0] == pBlip) return TRUE;
	else return FALSE;
}

void CMyButton::Init() {
	m_pBlips[0] = &nB;
	m_pBlips[1] = NULL;
	m_nHandle = 1;
	nB.Visible(m_nShape & B_NAME_B);
}

void CMyButton::Read(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar >> m_nShape >> m_nType >> m_nLR >> m_nLine;
}

void CMyButton::Write(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar << m_nShape << m_nType << m_nLR << m_nLine;
}


void CMyButton::DrawAngle(CDC *pDC, CPoint p, int dir)
{
	pDC->MoveTo(p.x - dir * GridSize, p.y + GridSizeH);
	pDC->LineTo(p);
	pDC->LineTo(p.x - dir * GridSize, p.y - GridSizeH);
}
