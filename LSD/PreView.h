#if !defined(AFX_PREVIEW_H__13AB90F7_8BDD_4D41_8E71_9C9087C8AF13__INCLUDED_)
#define AFX_PREVIEW_H__13AB90F7_8BDD_4D41_8E71_9C9087C8AF13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreView.h : header file
//

#include "Track.h"
/////////////////////////////////////////////////////////////////////////////
// CPreView view

class CPreView : public CView
{
protected:
	CPreView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPreView)

// Attributes
public:
	CTrack *pObj;
	int m_nZoomFactor;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPreView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CPreView)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREVIEW_H__13AB90F7_8BDD_4D41_8E71_9C9087C8AF13__INCLUDED_)
