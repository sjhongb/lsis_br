//=======================================================
//==              mycdc.h
//=======================================================
//	파 일 명 :  mycdc.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _MYCDC_H
#define _MYCDC_H

class CMyDc : public CDC {
public:
	void MyLPtoDP( double &x, double &y );
	void MyLPtoDP( LPPOINT lpPoint );
	BOOL Rectangle( LPCRECT lpRect );
	int DrawText( const CString& str, LPRECT lpRect, UINT nFormat );
	virtual int DrawText( LPCTSTR lpszString, int nCount, LPRECT lpRect, UINT nFormat );
	BOOL LineTo( int x, int y );
};

#endif