//=======================================================
//==              StDirDlg.h
//=======================================================
//	파 일 명 :  StDirDlg.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CObjDirectDlg : public CDialog
{
// Construction
public:
	CObjDirectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CObjDirectDlg)
	enum { IDD = IDD_OBJDIRECT };
	CString	m_sNext;
	CString	m_sOrg;
	BOOL	m_bReverse;
	BOOL	m_bNoInfo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjDirectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CObjDirectDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
