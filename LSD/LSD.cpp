//=======================================================
//==              LSD.cpp
//=======================================================
//	파 일 명 :  LSD.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "LSDDoc.h"
#include "LSDView.h"
#include "TableDoc.h"
#include "TablView.h"
#include "TableFrm.h"
#include "SetMar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLSDApp

BEGIN_MESSAGE_MAP(CLSDApp, CWinApp)
	//{{AFX_MSG_MAP(CLSDApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_SETMARGIN, OnSetmargin)
	ON_COMMAND(ID_PASSCHANGE, OnPasschange)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLSDApp construction

CLSDApp::CLSDApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CLSDApp object

CLSDApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLSDApp initialization

int GridSize, GridSizeH;
int mLeft, mRight, mTop, mBottom;
BOOL mTotPage;
int mLogX, mLogY;

BOOL CLSDApp::InitInstance()
{
	// CG: This code was inserted by the Password Dialog Component
	GridSize = 10;   // 5mm
	GridSizeH = GridSize / 2;
	m_iTotPage = 50;
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;

	pDocTemplate = new CMultiDocTemplate(
		IDR_LSDTYPE,
		RUNTIME_CLASS(CLSDDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CLSDView));
	AddDocTemplate(pDocTemplate);

	pDocTemplate = new CMultiDocTemplate(
		IDR_ILTTYPE,
		RUNTIME_CLASS(CTableDoc),
		RUNTIME_CLASS(CTableFrame), // custom MDI child frame
		RUNTIME_CLASS(CTableView));
	AddDocTemplate(pDocTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
// DON'T display a new MDI child window during startup!!!
   cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	ReadDefProf();

	AfxEnableControlContainer();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	CString GetProgramVersion();
// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CListBox	m_cVersionHistoryBox;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_LIST_VERSION, m_cVersionHistoryBox);
	//}}AFX_DATA_MAP
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strVersion;
	strVersion = GetProgramVersion();
	GetDlgItem(IDC_STATIC_VERSION)->SetWindowText(strVersion);
	m_cVersionHistoryBox.AddString("2007.04.06 2.0.0.22 ILD 생성 항목 수정(예외)");
	m_cVersionHistoryBox.AddString("2007.04.04 2.0.0.21 교량 객체 추가");
	m_cVersionHistoryBox.AddString("2007.02.27 2.0.0.20 아카우라 T&C 수정");
	m_cVersionHistoryBox.AddString("2007.02.27 2.0.0.19 아카우라 션트 신호기 LI 표시 삭제");
	m_cVersionHistoryBox.AddString("2006.11.27 2.0.0.18 진로 취소시 오버랩 카운트 동시에 감소");
	m_cVersionHistoryBox.AddString("2006.10.18 2.0.0.17 :63 현시 예외처리");
	m_cVersionHistoryBox.AddString("2006.10.17 2.0.0.16 Start 신호기 에 전방신호기로 Start 신호기 가 있을경우 _PSTART 이나 진로가 없는 Start 의 경우 일반 START 로 취급토록 수정");
	m_cVersionHistoryBox.AddString("2006.10.17 2.0.0.15 AKA 역 도착신호기 63 현시 확인 예외처리");
	m_cVersionHistoryBox.AddString("2006.10.13 2.0.0.14 SDG 방향 CALL ON 진로시 마지막T 제거되는 문제 수정");
	m_cVersionHistoryBox.AddString("2006.10.13 2.0.0.13 AKB 역 58:52(M), 58:56(M) 진로 현시 예외처리");
	m_cVersionHistoryBox.AddString("2006.10.12 2.0.0.12 방향별 진로 탐색 취소시 반대방향으로 전철기 강제 전환토록 수정");
	m_cVersionHistoryBox.AddString("2006.10.12 2.0.0.11 N방향 진로 탐색 오류 수정");
	m_cVersionHistoryBox.AddString("2006.10.11 2.0.0.10 AKA역 연동도표 대항진로 항목 라인을 14줄로 확장");
	m_cVersionHistoryBox.AddString("2006.10.11 2.0.0.8  AKA 비트맵 표시 ");
	m_cVersionHistoryBox.AddString("2006.09.27 2.0.0.7  오버랩 Release Time 30S 로 수");
	m_cVersionHistoryBox.AddString("2006.09.20 2.0.0.6  TGB,TCB 진로 탐색 로직 수정");
	m_cVersionHistoryBox.AddString("2006.09.19 2.0.0.5  LevelCross 명칭 인식 로직수정");
	m_cVersionHistoryBox.AddString("2006.09.19 2.0.0.4  SHUNT 신호기 현시 로직수정");
	m_cVersionHistoryBox.AddString("2006.09.19 2.0.0.3  프랭크 탐색 로직수정");
	m_cVersionHistoryBox.AddString("2006.09.15 2.0.0.2  AboutBox 에 Version 정보 추가");
	m_cVersionHistoryBox.AddString("2004.02.11 2.0.0.1  최초 릴리즈.");
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CString CAboutDlg::GetProgramVersion()
{
    HRSRC hRsrc = FindResource(NULL, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	CString strVersion;
	strVersion = "0.0.0.0";
    if (hRsrc != NULL)
    {
        HGLOBAL hGlobalMemory = LoadResource(NULL, hRsrc);
        if (hGlobalMemory != NULL)
        {
            void *pVersionResouece = LockResource(hGlobalMemory); 
            void *pVersion;
            UINT uLength;
            // 아래줄에 041204B0는 리소스 파일(*.rc)에서 가져옴.
            // 프로젝트 리소스 파일을 참고하세요(어느부분 참고인지는 밑에 나와있음)
            if( VerQueryValue(pVersionResouece, "StringFileInfo\\040904b0\\FileVersion", &pVersion, &uLength) != 0 )
            {
               // 1, 0, 0, 1로 되어 있는 부분에서 숫자 부분만 가져옴.
                int anVersion[4] = {0,};
                char* pcTemp = strtok( (char *)pVersion, "," );
                for(int inxVersion=0; inxVersion<4; inxVersion++)
                {
                    if(pcTemp  != NULL)
                    {
                        anVersion[inxVersion] = atoi(pcTemp );
                        if(inxVersion != 3)
                        {
                            pcTemp = strtok(NULL, ",");
                        }
                    }
                }
                strVersion.Format("%d.%d.%d.%d", anVersion[0], anVersion[1], anVersion[2], anVersion[3]);
                //AfxMessageBox(strVersion);
            }
            FreeResource(hGlobalMemory);
        }
    }
	return strVersion;
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CLSDApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CLSDApp commands
int CLSDApp::CalcPrintScale(CDC *pDC, int doc_size_x )
{
	int dotsperinch, xsize, scale;
	dotsperinch = pDC->GetDeviceCaps(LOGPIXELSX);
	xsize = pDC->GetDeviceCaps(HORZSIZE);
	scale = (dotsperinch * (xsize - mLeft - mRight)) / 420 * 1650 / doc_size_x ;  // 600 DPI * 420mm	-> PRSCALE = 400

	mLogX =	mLeft * 1000 / scale * dotsperinch / 254;
	mLogY = mTop * 1000 / scale * dotsperinch / 254;
	return scale;
}

int CLSDApp::CalcPrintScaleTable(CDC *pDC)
{
	int dotsperinch, xsize, scale;
	dotsperinch = pDC->GetDeviceCaps(LOGPIXELSX);
	xsize = pDC->GetDeviceCaps(HORZSIZE);

	scale = (dotsperinch * (xsize - mLeft - mRight)) / 600;  // 600 DPI * 420mm	-> PRSCALE = 400
	mLogX =	mLeft * 1000 / scale * dotsperinch / 254;
	mLogY = mTop * 1000 / scale * dotsperinch / 254;
	return scale;
}

void CLSDApp::OnSetmargin() 
{
	// TODO: Add your command handler code here
	CSetMar dlg;
	dlg.m_Left = mLeft;
	dlg.m_Right = mRight;
	dlg.m_Top = mTop;
	dlg.m_Bottom = mBottom;
	dlg.m_cTotalPage = m_TotPage;
	dlg.m_iStartPage = m_iStartPage;
	dlg.m_iTableType = m_iTableType;
	dlg.m_iTotPage = m_iTotPage;
	if (dlg.DoModal() == IDOK) {
		mLeft = dlg.m_Left;
		mRight = dlg.m_Right;
		mTop = dlg.m_Top;
		mBottom = dlg.m_Bottom;
		m_TotPage = dlg.m_cTotalPage;
		m_iStartPage = dlg.m_iStartPage;
		m_iTableType = dlg.m_iTableType;
		m_iTotPage = dlg.m_iTotPage;
		WriteDefProf();
	}
}

BOOL CLSDApp::WriteDefProf()
{
	WriteProfileInt("PRINTER","LEFTMARGIN",mLeft);
	WriteProfileInt("PRINTER","RIGHTMARGIN",mRight);
	WriteProfileInt("PRINTER","TOPMARGIN",mTop);
	WriteProfileInt("PRINTER","BOTTOMMARGIN",mBottom);
	WriteProfileInt("PRINTER","TOTPAGE",m_TotPage);
	WriteProfileInt("PRINTER","STARTPAGE",m_iStartPage);
	WriteProfileInt("PRINTER","TABLETYPE",m_iTableType);
	return TRUE;
}

BOOL CLSDApp::ReadDefProf() {
	mLeft			= GetProfileInt("PRINTER","LEFTMARGIN",0);
	mRight			= GetProfileInt("PRINTER","RIGHTMARGIN",0);
	mTop			= GetProfileInt("PRINTER","TOPMARGIN",0);
	mBottom			= GetProfileInt("PRINTER","BOTTOMMARGIN",0);
	m_TotPage		= GetProfileInt("PRINTER","TOTPAGE",0);
	m_iStartPage	= GetProfileInt("PRINTER","STARTPAGE",0);
	m_iTableType	= GetProfileInt("PRINTER","TABLETYPE",0);
	return TRUE;
}

void CLSDApp::OnPasschange() 
{
	// TODO: Add your command handler code here
}

