//=======================================================
//==              TableRow.h
//=======================================================
//	파 일 명 :  TableRow.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  연동도표의 한행
//
//=======================================================

#ifndef _TABLEROW_H
#define _TABLEROW_H

#include "../include/StrList.h"
#include "Cell.h"

#define W_NUMBER	30
#define W_TYPE		80
#define W_NAME		120 //150 //80
#define W_NO		70  //80
#define W_SIGNAL	50 //50
#define W_BUTTON	50 //50
#define W_LOCK		570 //570 //640
#define W_TRACK		160 //150
#define W_POINTLOCK	180
#define W_SASPECT   100
#define W_AHEADSIG  90
#define W_CLEAR     180 //180
#define W_OCCUPY    60
#define W_TIME      40
#define W_ROUTE		0
#define W_HOLD		140
#define W_ADDCOND   80 //70  //80
#define W_DELAY		50
#define W_PTYPE		100
#define W_PNAME		100
#define W_PNO		300
#define W_PLOCK		1430
#define W_PKEY		400
#define W_TCOCCSEQ  120
#define W_TCOCC     70 //100 //70
#define W_TCTIME    90 //50 //70
#define W_OVLAP     80 //100 //60

extern CString __editdev21;

class CCell;
class CTableDoc;
class CRow : public CObject {
	CTableDoc *m_pDoc;

	CCell *m_pCell[100];
	int m_nMaxCells;

public:
	virtual void Dump(CDumpContext& dc) const ;
	void InsertScissors(CMyStrList& Sci);
	CString DestTrack();
	void Sort();
	BOOL isSwitch(CString &str);
	void ResetView();
	BOOL RouteCompare(CRow* row);
	CString MakeID();
	static CPoint m_Org;
	static int m_nTextH, m_nPage;
	int m_iHeight;

	int GetHeight() { return m_iHeight; }
	int GetMaxRows();
	void IncHeight( int n = 1 );
	BOOL CheckFrank(CRow *row);
	BOOL CheckFrankPoint(CRow *row , CString strCheckPoint );
	BOOL CheckStartShareStartTrack(CRow *row);
	BOOL CheckOverlap(CRow *row , CString* strTrack);
	BOOL CheckPoint(CRow *row);
	BOOL CheckTrack(CRow *row);
	BOOL CheckIncludeTrack(CRow *row);
	BOOL CheckStationException(CRow *row = NULL);
	BOOL CheckException(CRow *row);
	BOOL CheckOverlapTrack(CRow *row);
	BOOL CheckContinue(CRow *row);
	BOOL CheckContinueShuntSignal(CRow *row);
	BOOL CheckContinueSignal(CRow *row);
	BOOL CheckDestTrack(CRow *row);

protected:
	DECLARE_SERIAL(CRow);

	BOOL m_bValid; //, m_bDelete;
	int  m_nRowType;
	int  m_nLineNo;
	int  m_nSubType;

	int m_nType;
public:
	int  GetApproachRealCount();
	BOOL Modify(CString &mod);
	BOOL CheckAreaAndEdit(CTableDoc* pDoc, CPoint point, CString &dif);
	enum RowSubType { NONE, _HOME, _START, _GSHUNT, _ASTART, _OHOME, _TOKENLESSC, _TOKENLESSG,
					     _OC, _OG, _LOS, _FREE, _STOP, _ISTART,_ISHUNT,_SHUNT, _CALLON, _OCALLON , _PSTART,_POINT,_CROSSING ,_BLOCK};
	enum RowType { ROWNONE = 0x100, ROWSIGNAL, ROWPOINT, ROWCROSSING ,ROWBLOCKROUTE  };
	BOOL SwitchInclude(CRow *row);
	BOOL isSignal() { return m_nType != _BLOCK && m_nType != _POINT && m_Button != "TTB" && m_Button != ""; }
	int NR();
	void Draw(CDC *_pDC, BOOL viewall = TRUE);
	void DrawHeader(CDC *pDC, CPoint p);
	CRow(CString& com, CString& str, CString& pre, CString& Aspect, CString& Path ,CRow *&cYUDO, CObList *cObTTB, CTableDoc *pDoc = NULL);
	CRow(CString& str, CTableDoc *pDoc = NULL);
	CRow(CTableDoc *pDoc = NULL);
	int SigTypeCheck(CString strToken);
	CString GetLevelCrossName( CString strLCName );
	char m_cUD;
	CString GetSwitchNameFromNR( CString &str ) ;
	CString m_Number, m_Type, m_No , m_RouteName, m_RouteCnt, m_TrackFromTo, m_Signal, m_Button , m_Apk , m_Time ,m_Delay, m_Aspect ,m_Aspect_Table, m_AheadSignal, m_AheadSignal_Table , m_AddCond , m_TcTime , m_TcOCC ,m_TcOCC_F, m_Dir , m_OverLap;
	CMyStrList m_LockSwitch, m_CallOnLockSwitch, m_LockSignal, m_Track, m_TrackClear, m_FrankTrack, m_Track2, m_CallonTrack , m_CallonTempTrack , m_Route, m_Approach , m_Occupy , m_TcOCCSeq ;

	CCommonCell m_cNumber, m_cType, m_cNo, m_cName, m_cSignal, m_cButton, m_cDelay, m_cSAspect , m_cAheadSig  , m_cTime, m_cAddCond, m_cLNo , m_cTcTime , m_cTcOCC , m_cOverLap;
	CLockCell  m_cLock;
	CPointLockCell m_cPointLock;
	CTrackCell m_cTrack,  m_cOccupy;
	CTrackClearCell m_cTrackClear;
	CApproachCell m_cHold;
	CTcOCCCell m_cTcOCCSeq;
	CRouteCell m_cRoute;
	CString    m_sNextSignal;
	CString    m_DependSignal;
	CString    m_GShuntSignal;
	int        m_iFreeShuntCk;
	CString    m_BlockSignal;
	CString    m_LevelCross;
	CString    m_sNextSignal2;
    CString    m_pStrPath;
	CString    strTempButton;
	int m_nValue;
	int iTemp;

	enum RowItemType { LOCKSWITCH, LOCKSIGNAL, LOCKTRACK, FREEROUTE, APPROACHTRACK, APPROACHTIME };
	BOOL CheckReferanceItem( CString &Item, CRow::RowItemType itemType, BOOL bInChecked = TRUE );

	void Init();

	void Locate() {
	CCell::SetCurrent(m_Org);
	for ( int i=0; i<m_nMaxCells; i++ ) {
		CCell *pCell = m_pCell[i];
		pCell->LocateRight();
	}
	m_Org.y -= (m_nTextH * m_iHeight);
}

public:
	virtual void Serialize(CArchive &ar);
	BOOL isValid() { return m_bValid; }
	static void SetPos(int x, int y, int h) {
		m_Org.x = x;
		m_Org.y = y;
		m_nTextH = h;
		CCell::m_nTextH = h;
		m_nPage = 0;
	}

friend BOOL operator < (CRow &a, CRow &b);
friend CTableDoc;
};

#endif