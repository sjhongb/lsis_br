//=======================================================
//==              Rparse.cpp
//=======================================================
//	file name   : Rparse.cpp
//  creator     : Do.Young.Kim
//	create date :  2004-10-11
//	version     :
//	contents    :
//
//=======================================================

#include <iostream.h>
#include <fstream.h>
#include <strstrea.h>
#include <string.h>
#include "stdafx.h"
#include "LSD.h"
#include "LSDDoc.h"
#include "LSDView.h"
#include "Track.h"
#include "Signal.h"
#include "MyObject.h"
#include "RouteLine.h"
#include "Define.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define INVALID_POS	(CPoint(10000,0))

int		giCallOnCk		= 0;
int		giShuntCk		= 0;
int		giStartCk		= 0;
int		giStartShuntCk	= 0;
short	giCallONPushCnt	= 0;
short	giShuntPushCnt	= 0;
short	giHomeCntUP		= 0;
short	giHomeCntDN		= 0;
BOOL	gbOuterFindCk	= FALSE;																	// Starter 신호기 에서 션트 있는신호기 가 OHOME 을 넘어서서 진로 체크할때 OHOME 을 찾았는지 여부를 체크 한다.
CSignal *gsBlockSignal;

char	*pcSigTypeStr[] = {
		"HOME",		    // 0
		"START",		// 1
		"GSHUNT",		// 2
		"ASTART",		// 3
		"OHOME",		// 4
		"TCB",			// 5
		"TGB",			// 6
		"OC",			// 7
		"OG",			// 8
		"LOS",			// 9
		"FREE",			//10
		"STOP",			//11
		"ISTART",		//12
		"ISHUNT",		//13
		"SHUNT",		//14
		"CALLON",		//15
		"OCALLON"		//16
};

class StationDB;
class CSwitch : public CGraphObject 
{
	char    m_cNR;
	BOOL    m_bUnique;
	BOOL    m_bKeyLock;
protected:
	DECLARE_SERIAL(CSwitch);
public:
	CTrack *m_pParent;
	CString m_strPairName;
	int		m_iBranchVect;

	CSwitch(CTrack *parent = NULL, char cNR = ' ' ) 
	{
		Name			= parent->m_sBranchName;
		m_pParent		= parent;
		m_cNR			= cNR;
		m_bUnique		= TRUE;
		m_bKeyLock		= FALSE;
		m_iBranchVect	= parent->m_iBranchVect;
	}

	CSwitch(CString strName, char cNR = ' ' ) 
	{
		Name			= strName;
		m_pParent		= NULL;
		m_cNR			= cNR;
		m_bUnique		= TRUE;
		m_bKeyLock		= FALSE;
		m_iBranchVect	= 0;
	}

	char       GetNR()    { return m_cNR; };
	char const *NameOf()  { return "Point"; };
	CString    &strName() { return Name; };

	friend class StationDB;
};

IMPLEMENT_SERIAL(CSwitch, CGraphObject, 0)

extern CString strStationName;
class RouteInfo {
    int			m_iStack[MAXSTACKCNT];
    int			m_iStackPtr;
    int			m_iTrackCount;
    int			m_iRouteCount;
	BOOL		m_bMainLine;
	CObject		*Tracks[MAXSTACKCNT];
	CMyStrList	m_mstrFlankTC;
	CMyStrList	m_mstrSW;

public:
	int		OutPathList( ostream &os, CString &ttl );
	int		GetDestCnt( CString strDestButton );
	CString MakeRouteType( ostream &os , CString strButton , int iSigType );
	CString ButtonName;
	CString GetSwitchNameFromNR( CString &str );
    void	Init() {
			m_iTrackCount	= 0; 
			m_iRouteCount	= 0;
			m_iStackPtr		= 0;
			ClearFrankTC();
    }

    void CheckAdd(CObject *tno) 
	{
		int i;
		int iCheckResult = 0;
		if ( tno->IsKindOf(RUNTIME_CLASS(CSwitch)) ) {
			CSwitch* t = (CSwitch*)tno;		
			for (i=0; i<m_iTrackCount; i++) {					
				CGraphObject *obj = (CGraphObject *)Tracks[i];
				if (obj != NULL) {
					if ( obj->IsKindOf(RUNTIME_CLASS(CSwitch))  ) {
						CSwitch* p = (CSwitch*)obj;		
						if ( (p->Name == t->Name) && (p->GetNR() == t->GetNR()) && (p->m_strPairName == t->m_strPairName)) {
							iCheckResult = 1;
						}
					}
				}
			}
		}
		if ( iCheckResult == 0 ) {
			Tracks[m_iTrackCount++] = tno;
		}
    }

    void Add(CObject *tno) 
	{
			Tracks[m_iTrackCount++] = tno;
    }

	CString *GetFrankTail()
	{
		if ( m_mstrFlankTC.GetCount() > 0 ) return (CString*)m_mstrFlankTC.GetTail();
		else return NULL;
	}
	
	void RemoveFrankTC ( CString strTC ) 
	{
		POSITION pos1,pos2;
		CString  *pstrTemp,strTemp;

		pos1 = pos2 = NULL;

		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			pos2 = pos1;
			pstrTemp = (CString*)m_mstrFlankTC.GetNext( pos1 );
			strTemp	 = *pstrTemp;
			if ( strTemp == strTC ) m_mstrFlankTC.RemoveAt(pos2);
		}

	}
    
	CString GetFlankSimpleSwitchName( CString strFlankTrack )
	{
		if ( strFlankTrack.Find( "AT" )> 0 ) {
			strFlankTrack.Replace("A]" ,"]");
			strFlankTrack.Replace("B]" ,"]");
		}	
		return strFlankTrack;
	}
	
	CString GetFlankSimpleTrackName( CString strFlankTrack )
	{
		if ( strFlankTrack.Find( "AT" )> 0 ) {
			strFlankTrack = strFlankTrack.Left(strFlankTrack.Find( "_AT" ));
		}
		else {
			strFlankTrack = strFlankTrack;
		}
		return strFlankTrack;
	}

	void AddFrankTC( CString strTC ) 
	{
	
		POSITION pos1,pos2;
		CString	 *pstrTemp,strTemp,strComp1,strComp2;

		pos1 = NULL;
		strComp1 = strTC;
		strComp1 = GetFlankSimpleSwitchName( strTC );
		strComp1.Replace("=" ,"");

		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; ) {
			pos2 = pos1;
			pstrTemp = (CString*)m_mstrFlankTC.GetNext( pos1 );
			strTemp  = *pstrTemp;
			
			strComp2 = GetFlankSimpleSwitchName( strTemp );
			strComp2.Replace("=" ,"");	
			
			if ( ( strTemp.Right(2) != strTC.Right(2) ) && ( strComp1 == strComp2)) {
				m_mstrFlankTC.RemoveAt(pos2);
				strTC = GetFlankSimpleTrackName( strTC );
			}
		}

		strTC = "=" + strTC;
		m_mstrFlankTC.CheckAdd ( strTC );
    }

    void ClearFrankTC(  ) 
	{
		m_mstrFlankTC.Purge();
    }

	int SameTrackCheck ( CString strTrack1 , CString strTrack2 )
	{
		CString strTemp1,strTemp2;
		int iTrackCount , iRet , iTemp1,iTemp2;
		iRet = 0;
		iTrackCount = GetTrackCount();

		if ( strTrack1 == "" || strTrack2 == "" )  return TRUE; 
		if ( strTrack1.Find("*",0) > -1 ) strTrack1 = strTrack1.Right(strTrack1.GetLength()-1);
		if ( strTrack2.Find("*",0) > -1 ) strTrack2 = strTrack2.Right(strTrack2.GetLength()-1);

		iTemp1 = strTrack1.Find('.');
		if ( iTemp1 > 0 ) strTrack1 = strTrack1.Left(iTemp1);

		iTemp2 = strTrack2.Find('.');
		if ( iTemp2 > 0 ) strTrack2 = strTrack2.Left(iTemp2);

		if ( strTrack1 == strTrack2 ) return TRUE;
		else return FALSE;

	}
	void RebuildFrankTC ( )
	{
		POSITION pos1,pos2;
		CString* pstr1,*pstr2,*pstr3;
		CString  str1,str2,str3,str4,str5,track1,track2,sw1,sw2,point2,strNR,strNR2,strTC2,strSwitch,strSwitch2;
		CString  strTemp1,strTemp2,strSwitch3,strPairSw1,strPairSw2,strFrank1;
		CString  LinkedTrack[10][2];
		CString  FrankTrack[20][2];
		CMyStrList temp_FlankTC;
		CMyStrList temp_FlankTC2;
		CSwitch* p;
		CGraphObject *obj;
		int iSWCheck;

		int  i,j,k,iTemp1,iTemp2,iSameTrackCk,iLinkedTrackCnt,iFlankCnt;

		for ( i = 0 ; i < 20 ; i++ ){
			FrankTrack[i][0] = "";
			FrankTrack[i][1] = "0";		
		}

		iFlankCnt = m_mstrFlankTC.GetCount();

		for( i=0; i<iFlankCnt; i++)
		{
			pstr1 = (CString*)m_mstrFlankTC.GetAtIndex( i );
			str1 = *pstr1;
			FrankTrack[i][0] = str1;
			FrankTrack[i][1] = "0";
		}		

		// 동일 트랙에 전철기가 둘있을경우 두 전철기 에 관련한 프랭크 리스트를 만든다. 
		iLinkedTrackCnt = 0;

		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			if ( pos1 != NULL ) {
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			str1 = GetFlankSimpleSwitchName( str1 );
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
				sw1 = "";
				iTemp1 = str1.Find("[",0);
				if ( iTemp1 > 0 ) {
					sw1 = str1.Mid ( iTemp1+1,2 );
				}
			} else {
				track1 = str1;
			} 

			for( pos2 = m_mstrFlankTC.GetHeadPosition(); pos2 != NULL; )
			{
				pstr2 = (CString*)m_mstrFlankTC.GetNext( pos2 );
				str2 = *pstr2;
				str2 = GetFlankSimpleSwitchName( str2 );
				iTemp2 = str2.Find("_AT_",0);
				if ( iTemp2 > 0 ) {
					track2 = str2.Left ( iTemp2 );
					sw2 = "";
					iTemp2 = str2.Find("[",0);
					if ( iTemp2 > 0 ) {
						sw2 = str2.Mid ( iTemp2+1,2 );
					}
				} else {
					track2 = str2;
				} 

				if ( (track1 == track2) && (sw1 != sw2) && ( pos1 != pos2 ) && pos1 != NULL && pos2 != NULL) {
					LinkedTrack[ iLinkedTrackCnt ][0] = GetFlankSimpleSwitchName( str1 );
					LinkedTrack[ iLinkedTrackCnt ][1] = GetFlankSimpleSwitchName( str2 );
					iLinkedTrackCnt++;
				}
			}
			}
		}

		for( int m=0; m<iFlankCnt; m++ )
		{
			str1 = FrankTrack[m][0];
			FrankTrack[m][0] = GetFlankSimpleSwitchName( str1 );
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
			} else {
				track1 = str1;
			} 
			iTemp1 = str1.Find("[",0);
			if ( iTemp1 > 0 ) {
				point2 = str1.Mid(iTemp1-1,5);
			} else {
				point2 = "";
			}

			for (i=0; i<m_iTrackCount; i++) {					
				CGraphObject *obj = (CGraphObject *)Tracks[i];
				if (obj != NULL) {
					if ( obj->IsKindOf(RUNTIME_CLASS(CSwitch))  ) {
						CSwitch* p = (CSwitch*)obj;		
						strNR = p->GetNR();
						strSwitch = p->Name;
						if ( strSwitch.Find("A") > 0 ) strPairSw1 = "A";
						else if ( strSwitch.Find("B") > 0 ) strPairSw1 = "B";
						else strPairSw1 = "";
						strSwitch.Replace("A","");
						strSwitch.Replace("B","");
						strNR2 = str1.Mid(str1.Find("[")-1,1);
						strSwitch2 = str1.Mid(str1.Find("[")+1,2);
						if ( str1.Mid(str1.Find("]")-1,1) == "A" ) strPairSw2 = "A";
						else if ( str1.Mid(str1.Find("]")-1,1) == "B" ) strPairSw2 = "B";
						else strPairSw2 = "";
						if ( strSwitch == strSwitch2 ){
							if ( strNR != strNR2 ) {
								FrankTrack[m][1] = "1";
									for ( j = 0 ; j < iLinkedTrackCnt ; j ++ ) {
										if ( LinkedTrack[j][0] == FrankTrack[m][0] || LinkedTrack[j][1] == FrankTrack[m][0]) {
											for ( k = 0 ; k < iFlankCnt ; k ++ ) {
												if ( FrankTrack[k][0] == LinkedTrack[j][0] ) {
													FrankTrack[k][1] = "1";
												}
										
												if ( FrankTrack[k][0] == LinkedTrack[j][1] ) {
													FrankTrack[k][1] = "1";
												}										
											}
										}
									}

							} else {
								for ( j = 0 ; j < iLinkedTrackCnt ; j ++ ) {
									if ( LinkedTrack[j][0] == FrankTrack[m][0] || LinkedTrack[j][1] == FrankTrack[m][0]) {
										FrankTrack[m][1] = "1";
								
									}
								}						
							}
						} 
					}
				}
			}
		}

		temp_FlankTC.Purge();
		for ( i = 0 ; i < iFlankCnt ; i ++ ) {
			if ( FrankTrack[i][1] == "0" ) {
				strFrank1 = GetFlankSimpleSwitchName( FrankTrack[i][0] );
				temp_FlankTC.AddTail ( strFrank1 );
			}
		}

		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;
		iFlankCnt = m_mstrFlankTC.GetCount();

		for ( i = 0 ; i < 10 ; i++ ){
			FrankTrack[i][0] = "";
			FrankTrack[i][1] = "0";		
		}
		
		for( i=0; i<iFlankCnt; i++)
		{
			pstr1 = (CString*)m_mstrFlankTC.GetAtIndex( i );
			str1 = *pstr1;
			FrankTrack[i][0] = str1;
			FrankTrack[i][1] = "0";
		}		
		// 진로내 트랙이 프랭크일경우 제거
		temp_FlankTC.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			iSameTrackCk = 0;
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
			} else {
				track1 = str1;
			} 
			
			for (int i=0; i<m_iTrackCount; i++) {					
				CGraphObject *obj = (CGraphObject *)Tracks[i];
				if (obj != NULL) {
					if ( obj->IsKindOf(RUNTIME_CLASS(CTrack))  ) {
						CTrack* T = (CTrack*)obj;
						track2 = T->Name;
						track1.Replace("=","");
						if ( SameTrackCheck ( track1 , track2 ) == TRUE ) {
							iSameTrackCk = 1;
							break;
						}
					}
				}
			}
			
			if ( iSameTrackCk == 0 ) {
				temp_FlankTC.AddTail ( str1 );
			}
		}

		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;

/*		// 프랭크 트랙의 조건의 전철기가 프랭크 트랙내에 있을때 전철기 제거 한다. 
		temp_FlankTC.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
				iTemp2 = str1.Find("[",0);
				str2 = str1.Mid(iTemp2+1,2);

				if ( track1.Find(str2,0) > 0 ) {
					temp_FlankTC.AddTail ( track1 );
				} else {
					temp_FlankTC.AddTail ( str1 );
				}
				
			} else {
				track1 = str1;
				temp_FlankTC.AddTail ( track1 );
			} 
		}
		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;
*/		


		// 쇄정이 걸린 전철기가 있는 트랙은 프랭크 트랙의 조건에서 제거한다. - 이원희 IDEA !!!
		temp_FlankTC.Purge();
		m_mstrSW.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			track1 = str1;
			iSWCheck = 0;
			for (i=0; i<m_iTrackCount; i++) { // 전철기 리스트를 정리한다. 
				obj = (CGraphObject *)Tracks[i];
				if (obj != NULL) {
					if (obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
						p = (CSwitch*)obj;
						str4 = p->GetNR()+p->strName().Left(2);
						m_mstrSW.AddTail(str4);

					}
				}
			}
			iTemp1 = str1.Find("[",0);
			str3 = str1.Mid(iTemp1-1,1);
			str1 = str1.Mid(iTemp1+1,2);

			str1 = str3 + str1;
			// 정리된 전철기와 프랭크 트랙의 전철기가 겹치면 패스 !!
			pos2 = m_mstrSW.GetHeadPosition();
			for( pos2 = m_mstrSW.GetHeadPosition(); pos2 != NULL; ){
				pstr2 = (CString*)m_mstrSW.GetNext( pos2 );
				str2 = *pstr2;
				if (( str1.Left(1) != str2.Left(1)) && ( str1.Right(2) == str2.Right(2)) ) {
					iSWCheck = 1;
				}			
			}

			if ( iSWCheck == 0 ) {
				temp_FlankTC.AddTail ( track1 );
			}
		}
		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;
		temp_FlankTC2 = temp_FlankTC;
	
		// 동일 트랙의 경우 쇄정이 잇는 전철기가 없는경우가 우선  !!!
		m_mstrSW.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;

			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
			} else {
				track1 = str1;
			} 

			iSWCheck = 0;
			for (i=0; i<m_iTrackCount; i++) { // 전철기 리스트를 정리한다. 
				obj = (CGraphObject *)Tracks[i];
				if (obj != NULL) {
					if (obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
						p = (CSwitch*)obj;
						str4 = p->GetNR()+p->strName().Left(2);
						m_mstrSW.AddTail(str4);

					}
				}
			}

			iTemp2 = temp_FlankTC.GetCount();


			for ( i = 0 ; i < iTemp2 ; i ++ ) {
				iSameTrackCk = 0;
				pstr2 = (CString*)temp_FlankTC.GetAtIndex( i );
				str2 = *pstr2;
				if ( str1 == str2 ) continue;
				iTemp1 = str2.Find("_AT_",0);
				if ( iTemp1 > 0 ) {
					track2 = str2.Left ( iTemp1 );
				} else {
					track2 = str2;
				} 
				iTemp1 = str2.Find("[",0);
				if ( iTemp1 > 0 ) {
					str4 = str2.Mid(iTemp1-1,1);
					str2 = str4+str2.Mid(iTemp1+1,2);
				} else continue;

				if ( track1 == track2 ) {
					pos2 = m_mstrSW.GetHeadPosition();
					for( pos2 = m_mstrSW.GetHeadPosition(); pos2 != NULL; ){
						pstr3 = (CString*)m_mstrSW.GetNext( pos2 );
						str3 = *pstr3;
						if ( str2.Right(3) == str3.Right(3)) {
							iSWCheck = 1;
							if ( iSWCheck == 1 ) {
								temp_FlankTC.RemoveAtIndex(i);
								iTemp2--;
								break;
							}
/*
							for (j=0; j<m_iTrackCount; j++) { // 전철기 리스트를 정리한다. 
								tobj = (CGraphObject *)Tracks[j];
								if (tobj != NULL) {
									if (tobj->IsKindOf(RUNTIME_CLASS(CTrack))) {
										t = (CTrack*)tobj;
										str5 = t->Name;
										iTemp5 = str5.Find(".");
										if ( iTemp5 > 0 ) str5 = str5.Left(iTemp5);
										
										str5 = "="+str5;
										if ( str5 == track2 ) {
											iSameTrackCk = 1;
										}
									}
								}
							}
							*/
						}			
					}
					//if ( iSWCheck == 1 && iSameTrackCk ==1) {
					//	temp_FlankTC.RemoveAtIndex(i);
					//	iTemp2--;
					//}
				}
			}
		}
		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;
		
		// 같은 트랙은 제거 한다. 
		temp_FlankTC.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			iSameTrackCk = 0;
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
			} else {
				track1 = str1;
			} 
			iTemp2 = temp_FlankTC.GetCount();
			int iTemp3,iTemp4;

			for ( i = 0 ; i < iTemp2 ; i ++ ) {
				pstr2 = (CString*)temp_FlankTC.GetAtIndex( i );
				str2 = *pstr2;
				iTemp1 = str2.Find("_AT_",0);
				if ( iTemp1 > 0 ) {
					track2 = str2.Left ( iTemp1 );
				} else {
					track2 = str2;
				} 
				if ( track1 == track2 ) {
					iSameTrackCk = 1;
					iTemp3 = str1.Find("[",0);
					iTemp4 = str2.Find("[",0);
					if ( (iTemp3 > 0) && (iTemp4 > 0) ) {
						if ((str1 != str2) && (str1.Mid(iTemp3+1,2) == str2.Mid(iTemp4+1,2))) {
							temp_FlankTC.RemoveAtIndex(i);
							iSameTrackCk =0;
							iTemp2--;
							str1 = str1.Left(iTemp3-5);
						}
					}
				}
			}

			if ( iSameTrackCk == 0 ) {
				temp_FlankTC.CheckAdd ( str1 );				
			}
		}
		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;


		// 프랭크 트랙의 조건의 전철기가 프랭크 트랙내에 있을때 전철기 제거 한다. 
		temp_FlankTC.Purge();
		for( pos1 = m_mstrFlankTC.GetHeadPosition(); pos1 != NULL; )
		{
			pstr1 = (CString*)m_mstrFlankTC.GetNext( pos1 );
			str1 = *pstr1;
			iTemp1 = str1.Find("_AT_",0);
			if ( iTemp1 > 0 ) {
				track1 = str1.Left ( iTemp1 );
				iTemp2 = str1.Find("[",0);
				str2 = str1.Mid(iTemp2+1,2);

				if ( track1.Find(str2,0) > 0 && track1.Find("W2122T",0) < 0 ) {
					temp_FlankTC.AddTail ( track1 );
				} else {
					temp_FlankTC.AddTail ( str1 );
				}
				
			} else {
				track1 = str1;
				temp_FlankTC.AddTail ( track1 );
			} 
		}
		m_mstrFlankTC.Purge();
		m_mstrFlankTC = temp_FlankTC;
	}

	void RemoveFrankTail()
	{
		m_mstrFlankTC.RemoveTail();
	}

    void Push() 
	{
		m_iStack[m_iStackPtr++] = m_iTrackCount;
    }

    void Pop() 
	{
		m_iTrackCount = m_iStack[--m_iStackPtr];
    }

    void RemoveTail() 
	{
		m_iTrackCount --;
    }

    void Out( ostream &os, int mode , int  callon = 0);
    int GetRouteCount() { return m_iRouteCount; }
    int GetTrackCount() { return m_iTrackCount; }
	CObject * GetTracks(int index) 
	{
		return Tracks[index];
	}
};

CObList	*gpTrackPath;
BOOL	gbCapture, gbStart;
CString gstrDest, gstrDestSignal[MAXDESTSIGNAL];;
int     giDestSignalCnt[MAXDESTSIGNAL];
int     giDestSignal;
int		_giOverLapCnt;
int		_Lastn;

CSignal *_FirstSignal		= NULL;		// 출발 신호기 
CSignal *_SecondSignal		= NULL;		// 도착 신호기 
CSignal *_GShuntSignal1		= NULL;
CSignal *_GShuntSignal2		= NULL;
CSignal *_GShuntSignal3		= NULL;
int		_iGShuntCnt			= 0;

CTrack	*_LastRouteTrack	= NULL;

CObList _TrackPathList;

void AddPointNode(CPoint &p) { //2007.4.10 이함수에서 메모리릭이 발생하고 있다. - 확인이 필요함.
	
	if (gpTrackPath && gbCapture && gbStart) 
		gpTrackPath->AddTail((CObject*)new CPoint(p));

	_TrackPathList.AddTail((CObject*)new CPoint(p));
}

void AddNode(CGraphObject *obj) {
	if (obj->IsKindOf(RUNTIME_CLASS(CTrack))) {
		CTrack *t = (CTrack*)obj;
		int n1,n2;
		if ( _LastRouteTrack ) {
			if ( t->Connect(_LastRouteTrack,n1,n2) ) {		// t1-Last:n2, t-Now:n1
				if (_Lastn >= 0)
					_LastRouteTrack->DrawVector(_Lastn, n2);
				_Lastn = n1;								// 0:C, 1:N, 2:R
			}
		}
		_LastRouteTrack = t;
	}
	else if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) {
		if ( !_LastRouteTrack || !_FirstSignal ) {
			AddPointNode(*obj);
		    TRACE("ADD POINT NODE 1[%d,%d]\n",obj->x,obj->y);
		} else {
			int endnode = ( _Lastn ) ? 0 : 1;
			_LastRouteTrack->DrawVector(_Lastn, endnode);
		}
	}
	else {
		AddPointNode(*obj);
		    TRACE("ADD POINT NODE 2[%d,%d]\n",obj->x,obj->y);
	}
}

void LptoDp( CPoint &p );
void ChangeMapping(CPoint &p1,CPoint &p2);	

CString GetSwitchName( CString &str ) {
	if (str == "") return str;
	int l = str.GetLength();
	if (l>5) return str;
	if ( isalpha( str.GetAt( l-1 )) ) {
		return str.Left( l-1 );
	}
	return str;
}

int RouteInfo::GetDestCnt( CString strDestButton )
{
	BOOL bFindDest;
	int iret;
	iret = 0;
	bFindDest = FALSE;
	for ( int i = 0 ; i <= giDestSignal ; i++ ) {
		if ( gstrDestSignal[i] == strDestButton ) {
			bFindDest = TRUE;
			iret = giDestSignalCnt[i];
		}
	}

	return iret; 
}

//=======================================================
//
//  함 수 명 :  MakeRouteType
//  함수출력 :  CString
//  함수입력 :  strButton
//  작 성 자 :  전
//  작성날짜 :  2004-10-11
//  버    전 :  1.0
//  설    명 :  버튼명을 받아서 진로의 타입이 메인진지, 콜온인지,션트인지 구분해서 리턴한다.
//
//=======================================================
CString RouteInfo::MakeRouteType( ostream &os , CString strButton , int iSigType )
{
	CString strRet;
	CString strType;

	strRet = strButton; 

	switch ( iSigType ) {
	case _SHUNT		: strType = "(S)"; break;
	case _CALLON	: strType = "(C)"; break;
	case _OCALLON	: strType = "(C)"; break;
	default			: strType = "(M)"; break;
	}

	os  << strType;
	strRet = strRet + strType;

	return strRet;
}

void RouteInfo::Out( ostream &os, int mode ,int callon) 
{
	BOOL bNormalSwitch		= TRUE;
	BOOL bNormalRoute		= TRUE;
	int  i					= 0;
	int  iIND				= 0; 
	int  iCeckOverLapPoint	= 0;
	int  iIStartPoint		= 0;
	int  iButtonLine		= 0;		// 버튼이 메인라인인지 루프라인인지의 여부
	int  iButtonLineFind	= 0;
	int  iFirstSW			= 0;
	int  iFreeShuntCk		= 0;
	int	 iPassRYIStart		= 0;		// SRY가 아닌 RY type의 ISTART를 통과했을때 ASTART의 현시여부에 관계없이 START를 진행시키기위해 사용
	CString strButton;					// 버튼 번호만 가지고 있는놈 41
	CString strButtonAll;				// 버튼 관련 모든 정보 표시된놈 41(M)-1;
	CString strOverLapPoint;
	CString strType;
	CString strTemp;
	CString strDependSW;				// Depend Sw 를 저장 하는 string; depend sw 는 진로의 전철기 출력 후 나중에 출력 한다. 
	int  iDependSwOutCheck = 0;			// DependSw 를 출력 했는지 여부 - 0 출력 안함 1 - 출력 함 
	int  iOverLap = 0;
	BYTE index=0;

	strDependSW = "";
    TRACE("ROUTE OUT - Mode %d \n",mode);
	_TrackPathList.RemoveAll();
	giCallONPushCnt = 0;


	for (i=0; i<m_iTrackCount; i++) {  // 동일 궤도명에서 정반위가 혼재하는 궤도를 삭제한다.
		CGraphObject *obj = (CGraphObject *)Tracks[i];
		if (obj && obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
			CSwitch* p = (CSwitch*)obj;
			char NR = p->GetNR();
			CString name = GetSwitchName( p->strName() );
			for (int j=0; j<m_iTrackCount; j++) {
				CGraphObject *obj1 = (CGraphObject *)Tracks[j];
				if (obj1 && obj1->IsKindOf(RUNTIME_CLASS(CSwitch))) {
					CSwitch* p1 = (CSwitch*)obj1;
				}
			}
		}
	}

	if (mode == PSMODE0 || mode == PSMODE7 || mode == PSMODE9 || mode == PSMODE11) {
		gbCapture = TRUE;
		gbStart = TRUE;
		
		if ( _FirstSignal ) {
			CPoint point = *_FirstSignal;
			AddPointNode( point );
		    TRACE("ADD POINT NODE 3[%d,%d]\n",point.x,point.y);
		}

		_Lastn = -1;
		_LastRouteTrack = NULL;
		m_iRouteCount++;
		if (mode == PSMODE11 ) os << "[BLOCK] ";
		else {
			os << "[ROUTE] ";
			CString* strTC;
			CString strTC2;
			CString strSwitch;
			CString strSwitch2;
			CString strNR;
			CString strNR2;
			int iFTCnt = m_mstrFlankTC.GetCount();
			int iSameSwitchCk = 0;

			RebuildFrankTC (); // 프랭크 트랙을 재 정리 한다. 
			iFTCnt = m_mstrFlankTC.GetCount();

			if ( iFTCnt > 0 ) {					
				for ( int r = 0 ; r < iFTCnt ; r ++ )
				{
					strTC = m_mstrFlankTC.GetAtIndex( r );
					strTC2 = *strTC;
					iSameSwitchCk = 0;
					for (i=0; i<m_iTrackCount; i++) {					
						CGraphObject *obj = (CGraphObject *)Tracks[i];
						if (obj != NULL) {
							if ( obj->IsKindOf(RUNTIME_CLASS(CSwitch))  ) {
								CSwitch* p = (CSwitch*)obj;		
								strNR = p->GetNR();
								strSwitch = p->Name;
								strSwitch.Replace("A","");
								strSwitch.Replace("B","");
								strNR2 = strTC2.Mid(strTC2.Find("[")-1,1);
								strSwitch2 = strTC2.Mid(strTC2.Find("[")+1,2);
								if ( strSwitch == strSwitch2 &&  strNR != strNR2){
									iSameSwitchCk = 1;
								}
							}
						}
					}

					if ( iSameSwitchCk == 0 ){
						os << "F:";
						os << strTC2;
						os << " ";
					}
				}
			}
		}

		_iGShuntCnt = 0;
		_GShuntSignal1 = NULL;
		_GShuntSignal2 = NULL;
		_GShuntSignal3 = NULL;

		for (i=0; i<m_iTrackCount; i++) {
			CGraphObject *obj = (CGraphObject *)Tracks[i];
			if (obj == NULL) {
				if ( _FirstSignal->m_nType == _HOME   ) os << "MAIN ";
				if ( _FirstSignal->m_nType == _CALLON ) os << "CALLON ";
				if ( _FirstSignal->m_nType == _SHUNT  ) os << "SHUNT ";
				if ( _FirstSignal->m_nType == _ISHUNT  ) os << "SHUNT ";
				if ( _FirstSignal->m_nType == _GSHUNT  ) os << "SHUNT ";

			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) {
				strButton = obj->Name;										// 전방신호기 설정용으로 버튼명을 저장한다. 	
				iButtonLineFind = strButton.Find('.',0);
					if ( GetDestCnt(((CMyButton*)obj)->Name) == 0 ) {
						strType.Format("_%02d",((CMyButton*)obj)->m_nLine); // (CMyButton*)obj)->m_nLine 을 OverLap 에 따른 진로 구분으로 사용한다.  진로수가 한개인경우 -X 를 표시하지 않는다. 
					} else {
						strType.Format("-%d_%02d",GetDestCnt(((CMyButton*)obj)->Name),((CMyButton*)obj)->m_nLine); // (CMyButton*)obj)->m_nLine 을 OverLap 에 따른 진로 구분으로 사용한다. 
					}

					if ( ((CMyButton*)obj)->m_nType == _BISTART ) {
						strType= "_99";
						((CMyButton*)obj)->m_nLine = 99;
					}

					if ( ((CMyButton*)obj)->m_nType == _BSDG ) {
						strType= "_98";
						((CMyButton*)obj)->m_nLine = 98;
					}

					// 아카우라 바이패스 역을 위한 예외처리 - 도저히 방법이 없다.
					if ( iButtonLineFind > 0  ) strButton = strButton.Left(iButtonLineFind);


					if ( strStationName.Find ( "BYP",0 ) >= 0 ) {
						if ( strButton == "58R" ) {
							if ( strType == "-2_00" ) {
								strButton= "58L";
								strType = "_00";
							}
						}
					}

					if ( strType == "-1_00" )
						strType = "-A_00";
					else if ( strType == "-2_00" )
						strType = "-B_00";
					else if ( strType == "-3_00" )
						strType = "-C_00";
					else if ( strType == "-4_00" )
						strType = "-D_00";
					else if ( strType == "-5_00" )
						strType = "-E_00";
					else if ( strType == "-6_00" )
						strType = "-F_00";
					else if ( strType == "-7_00" )
						strType = "-G_00";
					else if ( strType == "-8_00" )
						strType = "-H_00";
					else if ( strType == "-9_00" )
						strType = "-I_00";

					
					if ( iIStartPoint == 0 ) os << "B:" << strButton;
					else os << "B:" << strButton;
					strButtonAll = MakeRouteType( os , strButton , _FirstSignal->m_nType );
					os << strType << " ";
					strButtonAll = strButtonAll + strType;

					if (((CMyButton*)obj)->m_nLine < 97 ) {
						AddNode(obj);
						AddPointNode( INVALID_POS );
						TRACE("ADD POINT NODE 4[%d,%d]\n",obj->x,obj->y);
						gbCapture = FALSE;
					}
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
				CSwitch* p = (CSwitch*)obj;
				// 아카우라 바이패스 역을 위한 예외처리 - 도저히 방법이 없다.
				if ( strStationName.Find ( "BYP",0 ) >= 0 ) {
					if ( p->Name == "132" ) {
						p->m_strPairName = "*";
					}
				}
				if ( strStationName.Find ( "BHAIRAB",0 ) >= 0 ) {
					if ( p->Name == "38" ) {
						p->m_strPairName = "*";
					}
				}


				if (p->GetNR() != 'N') {
					bNormalSwitch = FALSE;
					if ( iFirstSW == 1 && iCeckOverLapPoint == 0 ) iFirstSW = 2;
				}
				if ( _FirstSignal->m_nLR == 0 ) {
					if ( iCeckOverLapPoint == 1 && ( p->m_iBranchVect == 1 || p->m_iBranchVect == 4 )) {
						if ( strOverLapPoint == GetSwitchNameFromNR( p->strName() ) ) {
							iOverLap = 3;
							if ( p->m_strPairName == "*" ) os << "*";
							else os << "&";
						} else {
							strTemp = p->strName();
							if ( strTemp.Find('B') < 0 ) {
								os << "*";
								iOverLap = 2;
							} else {
								if ( p->m_strPairName == "*" ) os << "*";
								else os << "&";
								iOverLap = 3;
							}
						}
					} else if ( iCeckOverLapPoint == 1 ) {
						if ( p->m_strPairName == "*" ) os << "*";
						else os << "&";
						iOverLap = 3;
						strOverLapPoint = GetSwitchNameFromNR( p->strName() );
					}
				} else if ( _FirstSignal->m_nLR == 1 ) {
					if ( iCeckOverLapPoint == 1 && ( p->m_iBranchVect == 7 || p->m_iBranchVect == 6 )) {
						if ( strOverLapPoint == GetSwitchNameFromNR( p->strName() ) ) {
							if ( p->m_strPairName == "*" ) os << "*";
							else os << "&";
							iOverLap = 3;
						} else { 
							strTemp = p->strName();
							os << "*";
							iOverLap = 2;
						}
					} else if ( iCeckOverLapPoint == 1 ) {
						if ( p->m_strPairName == "*" )
							os << "*";
						else
							os << "&";
						iOverLap = 3;
						strOverLapPoint = GetSwitchNameFromNR( p->strName() );
					}
				} 
				os << p->GetNR() << "[" << p->strName() << "] ";
				if ( iFirstSW == 0 ) iFirstSW = 1;
				
				CString strDepPoint1,strDepPoint2,strCheckOverLap;
				int iFind;

				iFind = p->m_pParent->m_sBranchPower.Find('@',0);
				if ( p->m_pParent->m_sBranchPower.GetLength() > 24 ) {
					strCheckOverLap = p->m_pParent->m_sBranchPower.Mid( 25 , 1 );
				}
				if ( iFind > 0 ) {
					strDepPoint1 = p->m_pParent->m_sBranchPower.Mid( iFind , p->m_pParent->m_sBranchPower.GetLength() - iFind);
					iFind = strDepPoint1.Find('@',1);
					if ( iFind > 0 ) {
						strDepPoint2 = strDepPoint1.Mid( iFind , strDepPoint1.GetLength() - iFind);
						strDepPoint1 = strDepPoint1.Left(iFind);
					}
				}

				if ( strDepPoint1 != "" ) {
					if ( iOverLap != 0 && strCheckOverLap == "T" ) {
					} else {
						if ( strDepPoint1.GetAt(1) == 'N' && p->GetNR() == 'N' ) {
							if ( iOverLap == 2 ) os << "*";
							else if ( iOverLap == 3 ) os << "*";
							os << strDepPoint1.GetAt(3) << "[" << strDepPoint1.Mid(4, strDepPoint1.GetLength() - 4 ) << "] ";
						}
						if ( strDepPoint1.GetAt(1) == 'R' && p->GetNR() == 'R' ) {
							if ( iOverLap == 2 ) os << "*";
							else if ( iOverLap == 3 ) os << "*";
							os << strDepPoint1.GetAt(3) << "[" << strDepPoint1.Mid(4, strDepPoint1.GetLength() - 4 ) << "] ";
						}
					}
				}
				if ( strDepPoint2 != "" ) {
					if ( iOverLap != 0 && strCheckOverLap == "T" ) {
					} else {
						if ( strDepPoint2.GetAt(1) == 'N' && p->GetNR() == 'N' ) {
							if ( iOverLap == 2 ) os << "*";
							else if ( iOverLap == 3 ) os << "*";
							os << strDepPoint2.GetAt(3) << "[" << strDepPoint2.Mid(4, strDepPoint2.GetLength() - 4 ) << "] ";
						}
						if ( strDepPoint2.GetAt(1) == 'R' && p->GetNR() == 'R' ) {
							if ( iOverLap == 2 ) os << "*";
							else if ( iOverLap == 3 ) os << "*";
							os << strDepPoint2.GetAt(3) << "[" << strDepPoint2.Mid(4, strDepPoint2.GetLength() - 4 ) << "] ";
						}
					}
				}
				p->m_strPairName = " ";
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CSignal))) {
				CSignal* s = (CSignal*)obj;
				if (s->m_nType == _OHOME)	{	// 구내 폐색 신호 
						os <<  "S:";	 
						os << obj->Name << " ";
				} 
				else {
					if (s->m_nType == _TOKENLESSG || s->m_nType == _TOKENLESSC) {
					 if ( s->m_nType == _TOKENLESSC ) os << "R:";	 
					 else os << "Q:";	 
					}
					else if (s->m_nType == _OG || s->m_nType == _OC) {
					 if ( s->m_nType == _OC ) os << "R:";	 
					 else os << "Q:";	 
					}
					else if ( s->m_nType == _ISTART) {
						 os << "I:";
						 iIStartPoint = 1;
						 if ( s->m_nLamp == 1 )
							 iPassRYIStart = 1;
					} 
					else if ( s->m_nType == _PSTART) {
						 os << "P:";
						 iCeckOverLapPoint = 1;
						 s->m_nType = _START;
					} else if (s->m_nType == _GSHUNT ) {
						if ( s->m_nLamp == 11 || s->m_nILR == 5 ) os << "G:";
						else { 
							os << "U:";
							_iGShuntCnt ++;
							if ( _iGShuntCnt == 1 ) _GShuntSignal1 = s;
							else if ( _iGShuntCnt == 2 ) _GShuntSignal2 = s;
							else if ( _iGShuntCnt == 3 ) _GShuntSignal3 = s;
							else _iGShuntCnt = 3;
						}
					} else if ( s->m_nType == _START && s->m_nILR == 5 ) {
						os << "G:";
					} else {
						 os << "E:";
						 iCeckOverLapPoint = 1;
					}			// 동일 방향의 Next 신호(장내/출발에 대한 다음 출발신호)
					os << obj->Name << " ";

					if ( iCeckOverLapPoint == 1 && iDependSwOutCheck == 0 ){
						os << strDependSW;
						iDependSwOutCheck = 1;
					}

				}
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CLevelCross))) { // 객체가 건널목일경우
				if ( _FirstSignal->m_nType != _START || __editdev19.Find("-BCI-") < 0)				// 2012.07.23 BCI역 새로운 케이스..overlap은 넣지만 LC는 제거.
					os << "L:" << obj->Name << " ";

			}
			else {
				index++;
				AddNode(obj);
				if ( obj->Name == ""  || (obj->Name.GetAt(0) == '?' && index!=1) ) {
					AddPointNode( INVALID_POS );
					TRACE("ADD POINT NODE 5[%d,%d]\n",obj->x,obj->y);
				}
				gbStart = TRUE;
				if (obj->Name == "" /*|| obj->Name.GetAt(0) == '?'*/) {
					if (i==0) {
						os << ButtonName;
						if (obj->Name.GetAt(0) == '?') {
							os << obj->Name;
						}
						os << " ";
					}
					else {
						CGraphObject *o = (CGraphObject *)Tracks[m_iTrackCount-1];
						if (o && o->IsKindOf(RUNTIME_CLASS(CMyButton))) {
							if (obj->Name.GetAt(0) == '?') {
								os << obj->Name;
							}
							os << " ";
						}
						else {
							if (i+1 < m_iTrackCount) {
								CGraphObject *o = (CGraphObject *)Tracks[i+1];
								if (o->IsKindOf(RUNTIME_CLASS(CMyButton))) os << o->Name << " ";
							}
						}
					}
				}
				else {
					if ( iCeckOverLapPoint >= 1 ) {
						os << "#";
					}
					os << obj->Name << " ";
				}
			}
		}

		if ( _FirstSignal->m_nType == _SHUNT && _FirstSignal->m_nLamp == 11 && strButton.Left(1) == "S" ) {
			os << "T:FREE ";
			iFreeShuntCk = 1;
		}

		if ( _FirstSignal->m_nType == _SHUNT && _FirstSignal->m_nILR == 5 && strButton.Left(1) == "S" ) {
			os << "T:FREE ";
			iFreeShuntCk = 1;
		}

		os << "\r\n";
	}
	else {
		if (mode == PSMODE1 || mode == PSMODE6) {
			os << "[PREROUTE] ";
			_SecondSignal = NULL;
		}
		else{
			os << "[HOLDROUTE] ";
			_SecondSignal = NULL;
		}
		for (i=m_iTrackCount-1; i>=0; i--) {
			CGraphObject *obj = (CGraphObject *)Tracks[i];
			if (obj == NULL) {
				os << "@ ";
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) {
				strButton = obj->Name; // 전방신호기 설정용으로 버튼명을 저장한다. 	
				iButtonLineFind = strButton.Find('.',0);
				if ( iButtonLineFind > 0  ) strButton = strButton.Left(iButtonLineFind);

				os << "B:" << strButton;
				os << "\r\n";
				strButton = strButton; // 전방신호기 설정용으로 버튼명을 저장한다. 
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
				CSwitch* p = (CSwitch*)obj;
				os << p->GetNR() << "[" << p->strName() << "] ";
			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CLevelCross))) { // 객체가 건널목일경우 
				os << "L:" << obj->Name << " ";

			}
			else if (obj->IsKindOf(RUNTIME_CLASS(CSignal))) {
				CSignal* s = (CSignal*)obj;
				if (s->m_nType == _OHOME)		// 구내 폐색 신호 
					 os << "S:";	 
				else if (s->m_nType == _TOKENLESSG || s->m_nType == _TOKENLESSC) {	// 폐색 출발 신호
					 if ( s->m_nType == _TOKENLESSC ) os << "R:";	 

					 else os << "Q:";	 
				}
				else if (s->m_nType == _OG || s->m_nType == _OC) {	// 폐색 출발 신호
					 if ( s->m_nType == _OC ) os << "R:";	 
					 else os << "Q:";	 
				}
				else if (s->m_nType == _ISTART ) 
					 os << "I:";
				else if (s->m_nType == _PSTART ) 
					 os << "P:";
				else if (s->m_nType == _GSHUNT )
					 if ( s->m_nLamp == 11 ) os << "G:";
					 else { 
						os << "U:";
					 }
				else os << "E:";			// 동일 방향의 Next 신호(장내/출발에 대한 다음 출발신호)
				os << obj->Name << " ";
			}
			else {
				if (obj->Name == "" || obj->Name.GetAt(0) == '?') {
					if (i==0) {
						if (obj->Name.GetAt(0) == '?') {
				
							os << obj->Name;
						}
						os << " ";
					}
					else {
						CGraphObject *o = (CGraphObject *)Tracks[m_iTrackCount-1];
						if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) {
							os << o->Name;
							if (obj->Name.GetAt(0) == '?') {
								os << obj->Name;
							}
							os << " ";
						}
					}
				}
				else os << obj->Name << " ";
			}
		}
		os << "\r\n";
	}

    if ( iFirstSW == 1 ) m_bMainLine = TRUE;
	else m_bMainLine = FALSE;

	if (_TrackPathList.GetCount()) {
		CString ttl = "[PATH]";

		iIND = OutPathList( os, ttl );
		os << "\r\n";
        _TrackPathList.RemoveAll();

	}

	if ( _SecondSignal != NULL )
	{
		if ( iIND == 0 || ( ((_FirstSignal->m_nShape & B_STRAIGHTLINE) == B_STRAIGHTLINE) && _FirstSignal->m_strStraightSignal == _SecondSignal->Name) ) m_bMainLine = TRUE;
		else m_bMainLine = FALSE;
	}
	else
	{
		if ( iIND == 0 ) m_bMainLine = TRUE;
		else m_bMainLine = FALSE;
	}

	if (mode == PSMODE0 || mode == PSMODE7 || mode == PSMODE9 || mode == PSMODE11) {
		if ( _FirstSignal ) {
			os << "[ASPECT]";
			if ( _FirstSignal->m_nType == _ASTART   ) os << " G";
			if ( _FirstSignal->m_nType == _HOME     ) {
				iButtonLine = atoi(strButtonAll.Right(1));
				if ( m_bMainLine == TRUE && ( iIND == 0 || ((_FirstSignal->m_nShape & B_STRAIGHTLINE) == B_STRAIGHTLINE) ) && _SecondSignal != NULL) { // 0 : 메인라인 1: 루프라인 2: 사이딩 라인 
					if (( _FirstSignal->m_nLamp == 0 || _FirstSignal->m_nLamp == 1 || _FirstSignal->m_nLamp == 3) && ( _SecondSignal->m_nLamp == 0 || _SecondSignal->m_nLamp == 2 || _SecondSignal->m_nLamp == 3 ) ) { 
						if ( _FirstSignal->m_nILR == 4 )
						{
							if ( _FirstSignal->m_nLamp == 3)
								os << " Y+LI(M) ";
							else
								os << " Y+LI(M),G+LI(M) ";
						}
						else
						{
							if ( _FirstSignal->m_nLamp == 3)
								os << " Y ";
							else
								os << " Y,G ";
						}
						if ( _FirstSignal->m_nLamp != 3)
						{
							os << "[ ";
/*
							if ( _iGShuntCnt > 0 ) {
								if ( _iGShuntCnt >= 1 ) {
									if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 2 ) {
									if ( _GShuntSignal2->m_nLamp == 12 ) os << ", "<< _GShuntSignal2->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 3 ) {
									if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
								}
							}
*/
							os << strButton;
							if ( m_bMainLine == TRUE ) {
								if ( _SecondSignal->m_nLamp == 0 ) { 
									os << " AT R OR Y";
									if ( _SecondSignal->m_nILR == 4 ) {
										os << "+LI(M)";
									}
								}
								else if ( _SecondSignal->m_nLamp == 1 ) os << " AT R";
								else if ( _SecondSignal->m_nLamp == 2 ) {
									os << " AT R OR Y";
									if ( _SecondSignal->m_nILR == 4 ) {
										os << "+LI(M)";
									}
								}
								else if ( _SecondSignal->m_nLamp == 3 ) os << " AT R";
								else os << " AT R";
							}
							else os << " AT R";
							os << ",";
							os << strButton;
							if ( m_bMainLine == TRUE ) {						
								if ( _SecondSignal->m_nLamp == 0 ) os << " AT G";
								else if ( _SecondSignal->m_nLamp == 1 ) os << " AT Y";
								else if ( _SecondSignal->m_nLamp == 2 ) os << " AT G";
								else if ( _SecondSignal->m_nLamp == 3 ) os << " AT Y";
								else os << " AT Y";
								if ( _SecondSignal->m_nILR == 4 ) {
										if ( m_bMainLine == TRUE && iIND == 0) os << "+LI(M)";
										else os << "+LI(L)";
								}

							}
							else {
								os << " AT Y";
								if ( _SecondSignal->m_nILR == 4 ) {
										os << "+LI(L)";
								}
							}
							os << " ]";
						}
					} // m_nLamp == 0 or 1
					else {
						if ( _FirstSignal->m_nLamp == 6 ) {
							if ( _FirstSignal->m_nILR == 4) {
								os << " G+LI(M) ";
							} else {
								os << " G";
							}
						} else if ( _FirstSignal->m_nLamp == 7 ) {
								os << " Y,G ";							
								os << "[ ";
								os << strButton;
								//if ( m_bMainLine == TRUE ) {
									if ( _SecondSignal->m_nLamp == 0 ) { 
										os << " AT R OR Y";
										if ( _SecondSignal->m_nILR == 4 ) {
											os << "+LI(M)";
										}
									}
									else if ( _SecondSignal->m_nLamp == 1 ) os << " AT R";
									else if ( _SecondSignal->m_nLamp == 2 ) {
										os << " AT R OR Y";
										if ( _SecondSignal->m_nILR == 4 ) {
											os << "+LI(M)";
										}
									}
									else if ( _SecondSignal->m_nLamp == 3 ) os << " AT R";
									else os << " AT R";
								//}
								//else os << " AT R";
								os << ",";
								os << strButton;
								//if ( m_bMainLine == TRUE ) {						
									if ( _SecondSignal->m_nLamp == 0 ) os << " AT G";
									else if ( _SecondSignal->m_nLamp == 1 ) os << " AT Y";
									else if ( _SecondSignal->m_nLamp == 2 ) os << " AT G";
									else if ( _SecondSignal->m_nLamp == 3 ) os << " AT Y";
									else os << " AT Y";
									if ( _SecondSignal->m_nILR == 4 ) {
										if ( m_bMainLine == TRUE && iIND == 0) os << "+LI(M)";
										else os << "+LI(L)";
									}
								//}
								//else {
								//	os << " AT Y";
								//	if ( _SecondSignal->m_nILR == 4 ) {
								//		os << "+LI(L)";
								//	}
								//}
							os << " ]";
						} else {
							if ( _FirstSignal->m_nILR == 4) {
								os << " Y+LI(M) ";
							} else {
								os << " Y";
							}
						}
/*
						if ( _iGShuntCnt > 0 ) {
							os << " [ ";
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
							os << "] ";
						}
*/
					}
				} // Main Line
				else {
					if ( _FirstSignal->m_nLamp == 7 ){
						os << " Y,G ";							
						os << "[ ";
						os << strButton;
						//if ( m_bMainLine == TRUE ) {
							if ( _SecondSignal->m_nLamp == 0 ) { 
								os << " AT R OR Y";
								if ( _SecondSignal->m_nILR == 4 ) {
									os << "+LI(M)";
								}
							}
							else if ( _SecondSignal->m_nLamp == 1 ) os << " AT R";
							else if ( _SecondSignal->m_nLamp == 2 ) {
								os << " AT R OR Y";
								if ( _SecondSignal->m_nILR == 4 ) {
									os << "+LI(M)";
								}
							}
							else if ( _SecondSignal->m_nLamp == 3 ) os << " AT R";
							else os << " AT R";
						//}
						//else os << " AT R";
						os << ",";
						os << strButton;
						//if ( m_bMainLine == TRUE ) {						
							if ( _SecondSignal->m_nLamp == 0 ) os << " AT G";
							else if ( _SecondSignal->m_nLamp == 1 ) os << " AT Y";
							else if ( _SecondSignal->m_nLamp == 2 ) os << " AT G";
							else if ( _SecondSignal->m_nLamp == 3 ) os << " AT Y";
							else os << " AT Y";
							if ( _SecondSignal->m_nILR == 4 ) {
								if ( m_bMainLine == TRUE && iIND == 0) os << "+LI(M)";
								else os << "+LI(L)";
							}
						//}
						//else {
						//	os << " AT Y";
						//	if ( _SecondSignal->m_nILR == 4 ) {
						//		os << "+LI(L)";
						//	}
						//}
						os << " ]";

					} else {
						if ( _FirstSignal->m_nILR == 0 ) os << " Y";
						if ( _FirstSignal->m_nILR == 1 ) os << " Y+LI(L)";
						if ( _FirstSignal->m_nILR == 2 ) os << " Y+LI(R)"; 
						if ( _FirstSignal->m_nILR == 3 ) {
							if ( iIND == 1 && _FirstSignal->m_nLR == 0 ) os << " Y+LI(L)";
							if ( iIND == 1 && _FirstSignal->m_nLR == 1 ) os << " Y+LI(R)"; 
							if ( iIND == -1 && _FirstSignal->m_nLR == 0 ) os << " Y+LI(R)"; 
							if ( iIND == -1 && _FirstSignal->m_nLR == 1 ) os << " Y+LI(L)";
							if ( iIND == 0 ) os << " Y";  // 메인신호도 LI 를 켜기로 했다. 
						}
						if ( _FirstSignal->m_nILR == 4 ) {
							os << " Y+LI(L)";
						}
/*
						if ( _iGShuntCnt > 0 ) {
							os << " [ ";
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
							os << "] ";
						}
*/
					}
				}

			} // HOME

			if ( _FirstSignal->m_nType == _OHOME    ) { 	
				switch ( _FirstSignal->m_nLamp ) {
					case 0  : os << " YY,G "; break; // "4Lamp (Y R Y G)"
					case 1  : os << " YY "	; break; // "3Lamp (Y R Y)"
					case 2  : os << " SY "	; break; // "2Lamp (Y R )"
					case 3  : os << " Y "	; break; // "2Lamp (R Y )"
					case 4  : os << " G "	; break; // "3Lamp (Y R G )"
					case 5  : os << " G "	; break; // "3Lamp (Y R G )"
					default : os << " YY,G "; break;
				}	
				if ( _SecondSignal != NULL ) { // HOME 으로 가정한다. 
					os << "[ ";
					os << strButton ;
					if ( _SecondSignal->m_nILR == 1 ) os << " AT Y+LI(L)"; // Left
					if ( _SecondSignal->m_nILR == 2 ) os << " AT Y+LI(R)"; // Right
					if ( _SecondSignal->m_nILR == 3 ) os << " AT Y+LI";    // Both
					if ( _SecondSignal->m_nILR == 4 ) os << " AT Y+LI(L)"; // Character
					if (( _FirstSignal->m_nLamp == 1 || _FirstSignal->m_nLamp == 3 ) && _SecondSignal->m_nILR == 4) os << " OR Y+LI(M)";
					if ( _SecondSignal->m_nLamp == 5 ) os << " AT Y";
					if ( _SecondSignal->m_nLamp == 7 ) os << " AT Y OR G";
					if ( _FirstSignal->m_nLamp == 0 ) {
						os << " , " << strButton;
						switch ( _SecondSignal->m_nLamp ) {
						case 0  :  if ( _SecondSignal->m_nILR == 4 ) os << " AT Y+LI(M) OR G+LI(M) ]"; // "4Lamp (C R Y G)"
								   else os << " AT Y OR G ]";
								   break;
						case 1  :  if ( _SecondSignal->m_nILR == 4 ) os << " AT Y+LI(M) OR G+LI(M) ]"; // "4Lamp (S R Y G)"
								   else os << " AT Y OR G ]";
								   break;
						default :  if ( _SecondSignal->m_nILR == 4 ) os << " AT Y+LI(M) ]";
								   else os << " AT Y ]";
								   break;
						}
					} else {
						os << " ]";
					}
				}
			}
			if ( _FirstSignal->m_nType == _OCALLON  ) {
				switch ( _FirstSignal->m_nLamp ) {
				case 3  : os << " Y [ "  << strButton << " AT R ]"; break;
				default : os << " SY [ " << strButton << " AT R ]"; break;
				}
			}
			if ( _FirstSignal->m_nType == _CALLON   ) {

				iButtonLine = atoi(strButtonAll.Right(1));
				if ( m_bMainLine == TRUE && iIND == 0 ) { // 0 : 메인라인 1: 루프라인 2: 사이딩 라인 
					if ( _FirstSignal->m_nILR == 4 ) os << " R+C+LI(M)"; 
					else os << " R+C"; 
				} else {
					if ( _FirstSignal->m_nILR == 1 ) os << " R+C+LI(L)";
					if ( _FirstSignal->m_nILR == 2 ) os << " R+C+LI(R)"; 
					if ( _FirstSignal->m_nILR == 3 ) {
						if ( iIND == 1 && _FirstSignal->m_nLR == 0 ) os << " R+C+LI(L)";				
						if ( iIND == 1 && _FirstSignal->m_nLR == 1 ) os << " R+C+LI(R)"; 
						if ( iIND == -1 && _FirstSignal->m_nLR == 0 ) os << " R+C+LI(R)"; 
						if ( iIND == -1 && _FirstSignal->m_nLR == 1 ) os << " R+C+LI(L)";				

					}
					if ( _FirstSignal->m_nILR == 4 ) {
						os << " R+C+LI(L)";
					}
				}
			}
			if ( _FirstSignal->m_nType == _SHUNT  ) {
/*// 10 역의 션트 신호기
				if (  _FirstSignal->m_nLamp < 10 ) {

					iButtonLine = atoi(strButtonAll.Right(1));

					if ( m_bMainLine == TRUE && iIND == 0 ) { // 0 : 메인라인 1: 루프라인 2: 사이딩 라인 
						if ( giShuntCk == 2 ) os << " S+LI(M)"; // 메인신호도 LI 를 켜기로 했다. // 홈을 거친 션트 
						else {
							if ( _FirstSignal->m_nILR == 5 && strButtonAll.Find("SDG") >= 0 ){ 
								if ( bNormalSwitch == TRUE || _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) os << " S+LI(F)";
								else os << " S";

//								os << " S+LI(L)";
							}
							else os << " S";
						}
					} else {
						if ( _FirstSignal->m_nILR == 1 ) os << " S+LI(L)";
						else if ( _FirstSignal->m_nILR == 2 ) os << " S+LI(R)"; 
						else if ( _FirstSignal->m_nILR == 3 ) {
							if ( iIND == 1  && _FirstSignal->m_nLR == 0 ) os << " S+LI(L)";				
							if ( iIND == 1  && _FirstSignal->m_nLR == 1 ) os << " S+LI(R)";	
							if ( iIND == -1 && _FirstSignal->m_nLR == 0 ) os << " S+LI(R)";	
							if ( iIND == -1 && _FirstSignal->m_nLR == 1 ) os << " S+LI(L)";				

						} 
						else if ( _FirstSignal->m_nILR == 4 ){
							os << " S+LI(L)";
						} 
						else if ( _FirstSignal->m_nILR == 5 && strButtonAll.Find("SDG") >= 0 ){ 
							if ( bNormalSwitch == TRUE || _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) os << " S+LI(F)";
							else os << " S";
						}
						else {
							os << " S"; 				
						}

					}
				} else {
					if ( iFreeShuntCk == 1 ) {
							if ( bNormalSwitch == TRUE ||  _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) os << " S+LI(F)";
							else os << " S";

//						os << " S+LI(L)";


					} else if ( _FirstSignal->m_nILR == 5 && strButtonAll.Find("SDG") >= 0 ){ 
							if ( bNormalSwitch == TRUE ||  _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) os << " S+LI(F)";
							else os << " S";
						//os << " S+LI(L)";
					} else {			
*/			
			 // 아카우라의 션트 신호기 : 알롬 때문에 R+S 였다가 조C 의견 반영 아카우라도 10역과 같이 R검지 안한다. 2007.03.08
				if (  _FirstSignal->m_nLamp < 10 ) { // Home 혹은 Start 에 붙어 있는 Shunt 신호기
					if ( ( bNormalSwitch == TRUE || _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) && ( iFreeShuntCk == 1 || _FirstSignal->m_nILR == 5 ) && strButtonAll.Find("SDG") >= 0 ){ // AKA 역의 59,57 신호기는 전철기가 Normal 이 아니어도 Free 등이 켜진다.
							//os << " R+S+LI(F)"; // Shunt 신호기는 LI를 현시 하지 않는다. 2006.09.19
							os << " R+S+LI(F)"; // 알롬 때문에 R+S 였다가 조C 의견 반영 아카우라도 10역과 같이 R검지 안한다. 2007.03.08
					} else {
						//os << " R+S"; // Shunt 신호기는 LI를 현시 하지 않는다. 2006.09.19
						if ( _FirstSignal->m_nILR == 1 && ((_FirstSignal->m_iDiry == -1 && iIND == 1) || (_FirstSignal->m_iDiry == 1 && iIND == -1))) 
							os << " S+LI(L)";
						else if ( _FirstSignal->m_nILR == 2 && ((_FirstSignal->m_iDiry == 1 && iIND == 1) || (_FirstSignal->m_iDiry == -1 && iIND == -1))) 
							os << " S+LI(R)"; 
						else if ( _FirstSignal->m_nILR == 4 && iIND == 0)
							os << " S+LI(M)";
						else if ( _FirstSignal->m_nILR == 4 && iIND != 0)
						{
							if  (((_FirstSignal->m_nShape & B_STRAIGHTLINE) == B_STRAIGHTLINE) && (_FirstSignal->m_strStraightSignal != ""))
							{
								int whereS = strButtonAll.Find("(S)",0);
								CString BufStr = strButtonAll.Left(whereS);
								
								if ( _FirstSignal->m_strStraightSignal == BufStr )
									os << " S+LI(M)";
								else
									os << " S+LI(L)";
							}
							else
								os << " S+LI(L)";
						}
						else 
							os << " S"; // 알롬 때문에 R+S 였다가 조C 의견 반영 아카우라도 10역과 같이 R검지 안한다. 2007.03.08
					}
				} else { // Ground Shunt 신호기 m_nLamp 가 10 보다 크다
					if ( ( bNormalSwitch == TRUE || _FirstSignal->Name == "57" || _FirstSignal->Name == "59" ) && ( iFreeShuntCk == 1 || _FirstSignal->m_nILR == 5 ) && strButtonAll.Find("SDG") >= 0 ){  // AKA 역의 59,57 신호기는 전철기가 Normal 이 아니어도 Free 등이 켜진다.
							os << " S+LI(F)";
					} else {
						os << " S"; 				
					}
				}
				if ( _SecondSignal != NULL ) {
					if ( _SecondSignal->m_nType == _ISTART ) {
						if ( _SecondSignal->m_nLamp == 3 ) {
						}
						if ( _SecondSignal->m_nLamp == 4 ) {
						}
					}
				}

			}
			if ( _FirstSignal->m_nType == _START    ) {
				if ( _FirstSignal->m_nLamp == 0 ) { 
					if ( _FirstSignal->m_nILR == 4 ) {
						if ( m_bMainLine == TRUE ) os << " Y+LI(M),G+LI(M)";
						else os << " Y+LI(L)";
					} else {
						if ( m_bMainLine == TRUE ) os << " Y,G";
						else os << " Y";
					}

					if ( m_bMainLine == TRUE ) {
				    os << " [ ";
/*
						if ( _iGShuntCnt > 0 ) {
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
						}
*/
					os << strButton ;
					os << " AT R";
					os << ",";
					os << strButton ;
					os << " AT G ]";
					}
				}
				else if ( _FirstSignal->m_nLamp == 1  )
				{
					if ( _FirstSignal->m_nILR == 4 )
						os << " Y+LI(L)";
					else
						os << " Y";
				}

				else if ( _FirstSignal->m_nLamp == 2  ) {
					if ( _SecondSignal != NULL ) {
						if ( _SecondSignal->m_nType == _ISTART ) {
							if ( _SecondSignal->m_nLamp == 3 ) {
								os << " Y";
							}
							else if ( _SecondSignal->m_nLamp == 4 ) {
								os << " G";
								os << " [ ";
								os << _SecondSignal->Name;
								os << " AT G ]";
							}
							else {
								os << " Y,G";
								os << " [ ";
/*
								if ( _iGShuntCnt > 0 ) {
									if ( _iGShuntCnt >= 1 ) {
										if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 2 ) {
										if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 3 ) {
										if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
									}
								}
*/
								os << strButton ;
								os << " AT R";
								os << ",";
								os << strButton ;
								os << " AT G ]";
							}
						} else if ( _SecondSignal->m_nType == _ASTART ){
							if ( iIND == 0 ) {
								os << " G";
								os << " [ ";
/*
								if ( _iGShuntCnt > 0 ) {
									if ( _iGShuntCnt >= 1 ) {
										if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 2 ) {
										if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 3 ) {
										if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
									}
								}
*/
								os << _SecondSignal->Name;
								os << " AT G ]";
							} else {
								os << " Y";
								os << " [ ";
								os << _SecondSignal->Name;
								os << " AT G ]";						
							}
					
						} else {
							os << " Y,G";
							os << " [ ";
/*
							if ( _iGShuntCnt > 0 ) {
								if ( _iGShuntCnt >= 1 ) {
									if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 2 ) {
									if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 3 ) {
									if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
								}
							}
*/
							os << strButton ;
							os << " AT R";
							os << ",";
							os << strButton ;
							os << " AT G ]";					
						}
					} else {
/*
						if ( _iGShuntCnt > 0 ) {
							os << " [ ";
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
							os << "]";
						}
*/
					}
				}

				else if ( _FirstSignal->m_nLamp == 3  ) { // "3Lamp (S R Y)"
					if ( _SecondSignal != NULL ) {
						if ( _SecondSignal->m_nType == _ISTART ) {
							switch ( _SecondSignal->m_nLamp ) {
							case  3 : os << " Y"; break;
							case  4 : os << " Y"; break;
							default : os << " Y"; break;
							}
							os << " [ ";
							os << _SecondSignal->Name;
							if ( _SecondSignal->m_nLamp == 1 ||  _SecondSignal->m_nLamp == 3 ) {
								os << " AT Y ]";
							}
							else if ( _SecondSignal->m_nLamp == 0 || _SecondSignal->m_nLamp == 2) {
								os << " AT Y OR G]";
							}
							else {
									os << " AT G ]";
							}
						}
						else if ( _SecondSignal->m_nType == _ASTART ){
							if ( iPassRYIStart == 1)
							{
								if ( _FirstSignal->m_nILR == 1 && ((_FirstSignal->m_iDiry == -1 && iIND == 1) || (_FirstSignal->m_iDiry == 1 && iIND == -1))) os << " Y+LI(L)";
								else if ( _FirstSignal->m_nILR == 2 && ((_FirstSignal->m_iDiry == 1 && iIND == 1) || (_FirstSignal->m_iDiry == -1 && iIND == -1))) os << " Y+LI(R)"; 
								else os << " Y"; 
							}
							else
							{
								if ( _FirstSignal->m_nILR == 1 && ((_FirstSignal->m_iDiry == -1 && iIND == 1) || (_FirstSignal->m_iDiry == 1 && iIND == -1))) os << " Y+LI(L) [ ";
								else if ( _FirstSignal->m_nILR == 2 && ((_FirstSignal->m_iDiry == 1 && iIND == 1) || (_FirstSignal->m_iDiry == -1 && iIND == -1))) os << " Y+LI(R) [ "; 
								else os << " Y [ "; 
/*
								if ( _iGShuntCnt > 0 ) {
									if ( _iGShuntCnt >= 1 ) {
										if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 2 ) {
										if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
									}
									if ( _iGShuntCnt >= 3 ) {
										if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
									}
								}
*/
								os << _SecondSignal->Name << " AT G ]";
							}
						} 
						else if ( _SecondSignal->m_nType == _START ){
							os << " Y";
							os << " [ ";
/*
							if ( _iGShuntCnt > 0 ) {
								if ( _iGShuntCnt >= 1 ) {
									if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 2 ) {
									if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
								}
								if ( _iGShuntCnt >= 3 ) {
									if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
								}
							}
*/
							os << _SecondSignal->Name;
							if ( _SecondSignal->m_nLamp == 1 ||  _SecondSignal->m_nLamp == 3 ) {
								os << " AT Y ]";
							}
							else if ( _SecondSignal->m_nLamp == 0 || _SecondSignal->m_nLamp == 2) {
								os << " AT Y OR G]";
							}
							else {
									os << " AT G ]";
							}
						} 
					} else {
						os << " Y";
/*
						if ( _iGShuntCnt > 0 ) {
							os << " [ ";
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
							os << "]";
						}
*/

					}
				}
				else if ( _FirstSignal->m_nLamp == 4 || _FirstSignal->m_nLamp == 8 ){
					if ( _SecondSignal->m_nType == _ISTART ) {
						if ( _SecondSignal->m_nLamp == 3 ) {
							os << " G";
						}
						else if ( _SecondSignal->m_nLamp == 4 ) {
							os << " G";
						} 
						else if ( _SecondSignal->m_nLamp == 8 ) {
							os << " G";
							os << " [ ";
							os << _SecondSignal->Name;
							os << " AT G ]";						
						} else {
							os << " G";
						}
					}
					else if ( _SecondSignal->m_nType == _ASTART ){
						os << " G";
						os << " [ ";
/*
						if ( _iGShuntCnt > 0 ) {
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
						}
*/
						os << _SecondSignal->Name;
						os << " AT G ]";						
					} 
					else if ( _SecondSignal->m_nType == _START ){
						os << " G";
/*
						if ( _iGShuntCnt > 0 ) {
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
						}
*/
						os << " [ ";
						os << _SecondSignal->Name;
						if ( _SecondSignal->m_nLamp == 0 || _SecondSignal->m_nLamp == 2) {
							os << " AT G]";
						}
						else {
								os << " AT G ]";
						}
					} 					
					else {
						os << " G";
/*
						if ( _iGShuntCnt > 0 ) {
							os << " [ ";
							if ( _iGShuntCnt >= 1 ) {
								if ( _GShuntSignal1->m_nLamp == 12 ) os << _GShuntSignal1->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 2 ) {
								if ( _GShuntSignal2->m_nLamp == 12 ) os <<", "<< _GShuntSignal2->Name << " AT ~R ";
							}
							if ( _iGShuntCnt >= 3 ) {
								if ( _GShuntSignal3->m_nLamp == 12 ) os <<", "<< _GShuntSignal3->Name << " AT ~R ";
							}
							os << "]";
						}
*/
					}
				}
				else if ( _FirstSignal->m_nLamp == 6  ){
					if ( _FirstSignal->Name == "58" ) {
						if ( strButton == "52" || strButton == "10T") {
							os << " YY,G [ 52 AT Y+LI(L),52 AT Y+LI(M) ] ";
						}
						if ( strButton == "56" ) {
							os << " YY+LI(L),G+LI(L) [ 56 AT R,56 AT G ]";
						}
					}
				} 
				else if ( _FirstSignal->m_nLamp == 7  ){
					if ( _FirstSignal->m_nILR == 4 )
						os << " G+LI(L)";
					else
						os << " G";
				}
				else {
					if ( _SecondSignal != NULL && _SecondSignal->m_nType == _ISTART ) {
						if ( _SecondSignal->m_nLamp == 3 ) {
							os << " [ ";
							os << _SecondSignal->Name;
							os << " AT Y ]";
						}
						if ( _SecondSignal->m_nLamp == 4 ) {
							os << " [ ";
							os << _SecondSignal->Name;
							os << " AT G ]";
						}
					}
				}
			}
			os <<"\r\n";
			os <<"[ENDROUTE]\r\n";
			if ( _SecondSignal != NULL ) {
				if ( _SecondSignal->m_nType == _ISTART || _SecondSignal->m_nType == _ISHUNT ) _SecondSignal = NULL;
			}
		}
	} // if (mode == PSMODE0 || mode == PSMODE7 || mode == PSMODE9) {

	if ( mode == PSMODE9 ) mode = PSMODE7;
	ClearFrankTC();
}

// Global variable definition
#define MAXBRENCHPOWER 256
CString strStationName;
class StationDB {
	CLSDDoc *m_pDoc;
    int m_iDirection;  //신호기의  방향 
	int sigtype;
    int ObjCount;
	RouteInfo Route;
	RouteInfo Block;
	CObList m_Switchs;
	CObList m_Cross;
	int iBtnCnt;


	BOOL m_bOneRoute;
	CString m_TrkName, m_BtnName;
	CObList *m_pNodeList;
	BOOL Capture;
	CString m_strTrackJoin[50];
	int m_iTrackJoinCnt;
	BOOL m_bFind;
	BOOL m_bMainLine;
	BOOL m_bFirstFlag;
	CString m_strDependSWN;
	CString m_strDependSWR;
	int Is_IStartAtFirst;		//SRY 타입의 ISTART신호기가 DB생성후 START로 돌아가는 것을 막기 위해 추가로 선언하여 사용

	// 오버랩 궤도가 몇개 있는지의 여부 - 방글라데시 

public:
    StationDB(CLSDDoc* pDoc, BOOL one = FALSE, char *tname = "", char *btn = "", CObList *path = NULL);
	~StationDB();
    void RootParse( ostream &os );
    void Parse( CTrack* id, int cnr, ostream &os, int mode, BOOL bNext = FALSE );
	int  ParseMode0( CTrack *T, int cnr, ostream &os, int mode, int flag = 0);
	int  ParseMode1( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode2( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode6( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode7( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode8( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode9( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode10( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode11( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode12( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode13( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode14( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseMode15( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseCNR1( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseCNR2( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseCNR3( CTrack *T, int cnr, ostream &os, int mode );
	int  ParseCheckMainLine( CTrack *T, CSignal*sig, int cnr, ostream &os, int mode );
	void AddDestCnt( CString strDestButton );
	void DelDestCnt( CString strDestButton );
	int  GetDestCnt( CString strDestButton );
	void TrackListOutPut( ostream &os );
	void InfoListOutPut( ostream &os );
    int GetSignal();
	CTrack *TrackFind(CTrack* T, int n, int& node);
	CMyButton *ButtonFind(CTrack* T);
	CLevelCross *LevelCrossFind(CTrack* T);
	CSwitch *HandPointFind( CTrack *T );
	CSwitch *CommonTPointFind( CTrack *T );
	CSwitch *DependPointFind( CTrack *T );
	BOOL isMatched(CMyButton *btn);
	CSwitch* AddPoint(CTrack *T, char nr , char force = ' ');
	void TrackFrankCheck ();
	void GetDependPoint ( CTrack * track , CString & strDependN, CString & strDependR );
	void CheckTrackFrank(CTrack *T , int iDir , CString strDepSignalN = "" , CString strDepSignalR = "");
	CLevelCross* AddCross( CString strName, int cx, int cy ,CString strTrackName);
	int RouteCancleCk( CTrack *T , CString fSignal , CString sSignal );
	BOOL SigFind(CTrack *T, int &cnt, int type, CSignal*& sig , BOOL bDir = TRUE , BOOL bReverse = FALSE );
	int SameTrackCheck ( CString strTrack1 , CString strTrack2 );
	CString ChangeChar(CString str , CString str1, int iIndex , int iLength = MAXBRENCHPOWER );
	CString ChangestrPower(CString strPower);
};

StationDB::StationDB(CLSDDoc *pDoc, BOOL one, char *tname, char *btn, CObList *path) {
	m_pDoc = pDoc;
	m_bOneRoute = one;
	m_TrkName = tname;
	m_BtnName = btn;
	if (one) gpTrackPath = path;
	else gpTrackPath = NULL;
	strStationName = m_pDoc->m_StationName;
}

StationDB::~StationDB() 
{
	POSITION	pos;

	// 전철기 리스트 제거 

	CSwitch		*item;

	for( pos = m_Switchs.GetHeadPosition(); pos != NULL; )
	{
        item = (CSwitch *)m_Switchs.GetNext( pos );
		delete item;
	}
	m_Switchs.RemoveAll();

	// 건널목 리스트 제거 

	CLevelCross		*LCross;

	for( pos = m_Cross.GetHeadPosition(); pos != NULL; )
	{
        LCross = (CLevelCross *)m_Cross.GetNext( pos );
		delete LCross;
	}
	m_Cross.RemoveAll();
}

CSwitch* StationDB::AddPoint(CTrack *T, char cNR , char force) 
{
	CSwitch *point = new CSwitch(T,cNR);
	POSITION pos;
	CSwitch *item;

	CString& name = point->Name;

	BOOL find = FALSE;
	for( pos = m_Switchs.GetHeadPosition(); pos != NULL; )
	{
        item = (CSwitch *)m_Switchs.GetNext( pos );
		if (item->Name == name) {
			if (item->m_cNR == cNR) {
				delete point;
				if ( force == '*' ) {
					item->m_strPairName = "*";
				}
				return item;
			}
			find = TRUE;
		}
	}
	if (find) point->m_bUnique = FALSE;

	m_Switchs.AddTail(point);

	if ( force == '*' ) {
		point->m_strPairName = "*";
	}
	return point;
}

//=======================================================
//
//  함 수 명 :  GetDependPoint
//  함수출력 :  void
//  함수입력 :  
//  작 성 자 :  김도영
//  작성날짜 :  2005-1-30
//  버    전 :
//  설    명 :  의존성 있는 전철기를 얻어온다. 
//              
//
//=======================================================
void StationDB:: GetDependPoint ( CTrack * track , CString & strDependN, CString & strDependR )
{

	 strDependN = "";
	 strDependR = "";
	 CString strDepPoint1,strDepPoint2;
	 int iFind;

	 iFind = track->m_sBranchPower.Find('@',0);
	 if ( iFind > 0 ) {
		strDepPoint1 = track->m_sBranchPower.Mid( iFind , track->m_sBranchPower.GetLength() - iFind);
		iFind = strDepPoint1.Find('@',1);
		if ( iFind > 0 ) {
			strDepPoint2 = strDepPoint1.Mid( iFind , strDepPoint1.GetLength() - iFind);
			strDepPoint1 = strDepPoint1.Left(iFind);
			if ( strDepPoint2.Left(2) == "@N" ) strDependN = strDepPoint2.Right(strDepPoint2.GetLength()-3);
			if ( strDepPoint2.Left(2) == "@R" ) strDependR = strDepPoint2.Right(strDepPoint2.GetLength()-3);

		}

		if ( strDepPoint1.Left(2) == "@N" ) strDependN = strDepPoint1.Right(strDepPoint1.GetLength()-3);
		if ( strDepPoint1.Left(2) == "@R" ) strDependR = strDepPoint1.Right(strDepPoint1.GetLength()-3);
	 }
}

void StationDB::TrackFrankCheck ()
{
	CString strDependPointN;
	CString strDependPointR;
	int i , iTrackCount , iDir;

	iTrackCount = Route.GetTrackCount();

	for (i=0; i<iTrackCount-1; i++) {  
		CGraphObject *obj = (CGraphObject*) Route.GetTracks(i);
		if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) break;
		if ( _FirstSignal->m_nType == _SHUNT || _FirstSignal->m_nType == _CALLON ) {
			if (obj->IsKindOf(RUNTIME_CLASS(CMyButton))) break;
		}
		if (obj && obj->IsKindOf(RUNTIME_CLASS(CSwitch))) {
			CSwitch* p = (CSwitch*)obj;
			CTrack* T = p->m_pParent;
			char NR = p->GetNR();

			if ( T->m_bKeyLock == TRUE ) continue;

			TRACE ( "TRACK FLANK CHECK %s, %d\n",p->Name,iDir);
			if ( NR == 'N' ) {
				if ( p->Name.Find("A") > 0 || p->Name.Find("B") > 0 ) continue;
				else iDir = _R;
			}
			if ( NR == 'R' ) {
				iDir = _N;
			}

			GetDependPoint ( T , strDependPointN , strDependPointR );
			if ( strDependPointN != "" ) m_strDependSWN = strDependPointN;
			if ( strDependPointR != "" ) m_strDependSWR = strDependPointR;

			CheckTrackFrank ( T , iDir , m_strDependSWN , m_strDependSWR );
		}
	}
}

int StationDB::SameTrackCheck ( CString strTrack1 , CString strTrack2 )
{
	CString strTemp1;
	CString strTemp2;
	int iTrackCount , iRet , iTemp1,iTemp2;
	iRet = 0;
	iTrackCount = Route.GetTrackCount();
	strTrack1.Replace("=","");
	strTrack2.Replace("=","");
	if ( strTrack1 == "" || strTrack2 == "" )  return TRUE; // 비교할 대상이 없을때는 TRUE
	if ( strTrack1.Find("*",0) > -1 ) strTrack1 = strTrack1.Right(strTrack1.GetLength()-1);
	if ( strTrack2.Find("*",0) > -1 ) strTrack2 = strTrack2.Right(strTrack2.GetLength()-1);

	iTemp1 = strTrack1.Find('.');
	if ( iTemp1 > 0 ) strTrack1 = strTrack1.Left(iTemp1);

	iTemp2 = strTrack2.Find('.');
	if ( iTemp2 > 0 ) strTrack2 = strTrack2.Left(iTemp2);

	if ( strTrack1 == strTrack2 ) return TRUE;
	else return FALSE;

}


//=======================================================
//
//  함 수 명 :  CheckTrackFrank
//  함수출력 :  void
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2005-1-30
//  버    전 :
//  설    명 :  궤도 프랭크 프로텍션을 검지한다. 
//              검지된 궤도는 TC 조건에 포함이 된다. 
//
//=======================================================

void StationDB::CheckTrackFrank( CTrack* T , int iDir , CString strDepSignalN , CString strDepSignalR )
{

	CTrack  *SearchStartTrack,*tempTrack1,*tempTrack2;
	CTrack  *SaveTrack1 = NULL; // 전철기가 있어서 임시로 저장 한 궤도 
	CTrack  *SaveTrack2 = NULL; // 전철기가 있어서 임시로 저장 한 궤도 
	CSignal * tempSignal1;		// 발견된 신호기 
	int     iSearchStartDir;
	int     iOldDir;
	int     iNode;				// 연결된 궤도의 노드
	int     iTemp1;				// 궤도 명에서 . 을 찾기 위한 변수 
	int     iFrankTrackCnt = 0; // 전철기를 발견한 이후부터 신호기를 만날때 까지의 궤도 갯수.
	BOOL bSigFind;
	BOOL bExit = FALSE;
	BOOL bHandPoint = FALSE;
	CString strOldBrName;
	CString strBrName1;  // 검색시작한 전첳기
	CString strBrName2;  // 최종 으로 기록되는 전철기 
	CString strBrName3;  // 최종 으로 기록되는 전철기를 한박자 늦게 가지고 있는다. 전철기 다음에 프랭크 트랙 없이 신호 기 발견될때 되돌리기 위함.
	CString strTraceTemp;  // 검색시작한 전철기
	CString strTemp1;  // 검색시작한 전철기

	bSigFind = FALSE;
	SearchStartTrack = T; // 프랭크 진로 탐색하기 시작한 트랙 저장
	
	tempTrack1 = SearchStartTrack;
	iSearchStartDir  = iDir;
	iOldDir = iDir;

	if ( iSearchStartDir == 1 ) strTraceTemp = "C";
	if ( iSearchStartDir == 2 ) strTraceTemp = "N";
	if ( iSearchStartDir == 3 ) strTraceTemp = "R";

	TRACE ( " -> FLANK CHECK <- \n");
	TRACE ( "%s   %s%s\n",SearchStartTrack->Name , strTraceTemp,SearchStartTrack->m_sBranchName) ;

	while ( bExit == FALSE ) {
		// 궤도의 끝일때
		if ( bExit == FALSE )  {
			strOldBrName = tempTrack1->m_sBranchName;

			tempTrack2 = TrackFind( tempTrack1, iDir, iNode);
		}

		if ( tempTrack2 == NULL || tempTrack2->Name.Left(1) == '?') {
			bExit = TRUE;
		} else {
		}

		CString *pstrTemp;
		CString strTemp;
		if ( bExit == FALSE ) {
			// 신호기를 찾았을때
			if ( SigFind( tempTrack2 , iNode , _ANY , tempSignal1 ,FALSE ,FALSE)) {
				TRACE ( "Signal Find : %s\n",tempSignal1->Name ) ;		
					
				pstrTemp = Route.GetFrankTail();
				
				if ( pstrTemp != NULL) {
					strTemp = *pstrTemp;
					if ( strTemp.Find( "AT" )> 0 ) {
						strTemp = strTemp.Left(strTemp.Find( "_AT" ));
					}
					else {
						strTemp = strTemp;
					}
					if ( SameTrackCheck( strTemp , tempTrack2->Name) == TRUE ) Route.RemoveFrankTail();
				}


				if ( SaveTrack1 != NULL ) {
					tempTrack1 = SaveTrack1;

					tempTrack2 = NULL;
					iDir = _R; // N 방향은 끝났으니까. R 방향으로 검색을 한다. 
					//tempTrack2 = TrackFind( SaveTrack2, iDir, iNode);
					strBrName2.Replace('N','R');
					SaveTrack1 = NULL;
					bSigFind = TRUE;					
				} else {
					tempTrack2 = tempTrack1;
					if ( SaveTrack2 != NULL ) {
						tempTrack1 = SaveTrack2;
					}

					bSigFind = TRUE;
					bExit = TRUE;
				}
			}
			// 검색된 트랙에 전철기가 있을때 그러나 HandPoint 전철기는 제외한다.
			if ( bSigFind == FALSE || (bSigFind == TRUE && bExit == TRUE && SaveTrack2 != NULL )) {
				bHandPoint = tempTrack2->m_bKeyLock;
				if ( tempTrack2->m_sBranchName != "" && bHandPoint == FALSE ) {
					if ( strOldBrName.Left(2) == tempTrack2->m_sBranchName.Left(2) ) {
						if ( iOldDir == _N && iNode == _R ) {
							break;
						}
					}

					strBrName3 = strBrName2;
					strBrName2 = tempTrack2->m_sBranchName;
					if ( iNode == _C ) {
						SaveTrack1 = tempTrack2;
						TRACE ( "TRACK SAVE %s\n",SaveTrack1->Name);
						strBrName2 = "N[" +strBrName2;
						iDir = _N;
					}
					else if ( iNode == _N ) {
						strBrName2 = "N[" +strBrName2;
						iDir = _C;
					}
					else if ( iNode == _R ) {
						strBrName2 = "R[" +strBrName2;
						iDir = _C;				
					}
					if ( iDir == 1 ) strTraceTemp = "C";
					if ( iDir == 2 ) strTraceTemp = "N";
					if ( iDir == 3 ) strTraceTemp = "R";
					TRACE ( "%s   %s%s\n",tempTrack2->Name , strTraceTemp, tempTrack2->m_sBranchName) ;		
				} else {
					if ( iNode == _C ) iDir = _N;
					else if ( iNode == _N ) iDir = _C;
					else if ( iNode == _R ) iDir = _C;							
				}
			}

		}

		if ( tempTrack2 != NULL && tempTrack2->Name.Left(1) != '?' ) {
			if ( ((SameTrackCheck( tempTrack1->Name , SearchStartTrack->Name) == FALSE && bExit == FALSE) || (iNode == _R || iNode == _N )) && (bSigFind == FALSE||(bSigFind == TRUE && bExit == TRUE && SaveTrack2 != NULL))) {
			//if ( ((SameTrackCheck( tempTrack1->Name , tempTrack2->Name) == FALSE && bExit == FALSE) || (iNode == _R || iNode == _N )) && bSigFind == FALSE) {
				if ( SameTrackCheck( SearchStartTrack->Name , tempTrack2->Name) == FALSE && tempTrack2->Name.Find("DT") < 0 && tempTrack2->Name.Find("UT") < 0) {  // 한번은 건너뛴다. //2012.09.10 Frank Protection 수정.
						iTemp1 = tempTrack2->Name.Find('.');
						if ( iTemp1 > 0 ) strTemp1 = tempTrack2->Name.Left(iTemp1);
						else strTemp1 = tempTrack2->Name;
						if ( bHandPoint == FALSE && strBrName2 != "") {
							strTemp1 = strTemp1 + "_AT_" +  strBrName2 + "]";
						}
						TRACE ( "Frank Track ADD : %s\n",strTemp1);
						iFrankTrackCnt++;
						if ( iFrankTrackCnt < 2 ) { // 프랭크 트랙은 검색 방향으로 1개만 인정한다. 2006.09.18 - 조신영 C 의견
							if ((strStationName.Find ( "AKHA",0 ) >= 0 ) && (strTemp1.Find("W131T") >=0 )) { // 알롬 요구로 수정 2007.03.06
							} else {
								Route.AddFrankTC ( strTemp1 );					
							}
						}
				}
			}
			SaveTrack2 = tempTrack1;
			tempTrack1 = tempTrack2;
		}
		bSigFind = FALSE;
	}
}


//=======================================================
//
//  함 수 명 :  AddCross
//  함수출력 :  CLevelCross*
//  함수입력 :  CTrack *T
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  건널목을 연동 도표에 추가 한다. 
//
//=======================================================
CLevelCross* StationDB::AddCross( CString strName, int cx, int cy , CString strTrackName ) 
{
	CLevelCross *lcross = new CLevelCross( strName, cx, cy );

	POSITION pos;
	CLevelCross *item;
	BOOL bfind = FALSE;

	for ( pos = m_Cross.GetHeadPosition(); pos != NULL; )
	{
		item = (CLevelCross *)m_Cross.GetNext( pos );
		if ( item->Name == strName ) {
			item->m_LCTrack.CheckAdd(strTrackName);
			bfind = TRUE;
		}
	}

	if ( bfind == FALSE ) {
			lcross->m_LCTrack.AddTail(strTrackName);
			m_Cross.AddTail(lcross);
	}

	return lcross;
}

int StationDB::RouteCancleCk( CTrack *T , CString fSignal , CString sSignal )
{

	CString     brTemp,brTemp2,brTemp3,brTemp4,brTemp5,brTemp6,brTemp7;
	int iGap;
	iGap = 0;

	T->m_sBranchPower = ChangeChar( T->m_sBranchPower , "E" , MAXBRENCHPOWER );
	if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(2,1) == "N" ) {
		iGap = 0;
	} else if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(2,1) == "R" ) {
		iGap = 3;
	} else {
		return -1;
	}
	brTemp = T->m_sBranchPower.Mid(8+iGap,3);
	if ( brTemp != "   " ) {
		brTemp.TrimLeft();
		brTemp.TrimRight();
	}
	brTemp2 = T->m_sBranchPower.Mid(14+iGap,3);
	if ( brTemp2 != "   " ) {
		brTemp2.TrimLeft();
		brTemp2.TrimRight();
	}
	brTemp3 = T->m_sBranchPower.Mid(30+iGap,3);
	if ( brTemp3 != "   " ) {
		brTemp3.TrimLeft();
		brTemp3.TrimRight();
	}
	brTemp4 = T->m_sBranchPower.Mid(36+iGap,3);
	if ( brTemp4 != "   " ) {
		brTemp4.TrimLeft();
		brTemp4.TrimRight();
	}
	brTemp5 = T->m_sBranchPower.Mid(42+iGap,3);
	if ( brTemp5 != "   " ) {
		brTemp5.TrimLeft();
		brTemp5.TrimRight();
	}
	brTemp6 = T->m_sBranchPower.Mid(48+iGap,3);
	if ( brTemp6 != "   " ) {
		brTemp6.TrimLeft();
		brTemp6.TrimRight();
	}
	brTemp7 = T->m_sBranchPower.Mid(54+iGap,3);
	if ( brTemp7 != "   " ) {
		brTemp7.TrimLeft();
		brTemp7.TrimRight();
	}
	if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(2,1) == "R" && 
		(( brTemp  != "   " && ( fSignal == brTemp  || sSignal == brTemp || "S"+fSignal == brTemp  || "D"+ sSignal == brTemp  )) || 
		 ( brTemp2 != "   " && ( fSignal == brTemp2 || sSignal == brTemp2|| "S"+fSignal == brTemp2 || "D"+ sSignal == brTemp2 )) ||
		 ( brTemp3 != "   " && ( fSignal == brTemp3 || sSignal == brTemp3|| "S"+fSignal == brTemp3 || "D"+ sSignal == brTemp3 )) ||
		 ( brTemp4 != "   " && ( fSignal == brTemp4 || sSignal == brTemp4|| "S"+fSignal == brTemp4 || "D"+ sSignal == brTemp4 )) ||
		 ( brTemp5 != "   " && ( fSignal == brTemp5 || sSignal == brTemp5|| "S"+fSignal == brTemp5 || "D"+ sSignal == brTemp5 )) ||
		 ( brTemp6 != "   " && ( fSignal == brTemp6 || sSignal == brTemp6|| "S"+fSignal == brTemp6 || "D"+ sSignal == brTemp6 )) ||
		 ( brTemp7 != "   " && ( fSignal == brTemp7 || sSignal == brTemp7|| "S"+fSignal == brTemp7 || "D"+ sSignal == brTemp7 ))) ) 
	{
		return _R;
	} 
	else if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(2,1) == "N" && 
		(( brTemp  != "   " && ( fSignal == brTemp  || sSignal == brTemp || "S"+fSignal == brTemp  || "D"+ sSignal == brTemp  )) || 
		 ( brTemp2 != "   " && ( fSignal == brTemp2 || sSignal == brTemp2|| "S"+fSignal == brTemp2 || "D"+ sSignal == brTemp2 )) ||
		 ( brTemp3 != "   " && ( fSignal == brTemp3 || sSignal == brTemp3|| "S"+fSignal == brTemp3 || "D"+ sSignal == brTemp3 )) ||
		 ( brTemp4 != "   " && ( fSignal == brTemp4 || sSignal == brTemp4|| "S"+fSignal == brTemp4 || "D"+ sSignal == brTemp4 )) ||
		 ( brTemp5 != "   " && ( fSignal == brTemp5 || sSignal == brTemp5|| "S"+fSignal == brTemp5 || "D"+ sSignal == brTemp5 )) ||
		 ( brTemp6 != "   " && ( fSignal == brTemp6 || sSignal == brTemp6|| "S"+fSignal == brTemp6 || "D"+ sSignal == brTemp6 )) ||
		 ( brTemp7 != "   " && ( fSignal == brTemp7 || sSignal == brTemp7|| "S"+fSignal == brTemp7 || "D"+ sSignal == brTemp7 ))) ) 
	{
		return _N;
	}
	else 
	{
		return -1;	
	}
		
	return -1;
}

BOOL StationDB::SigFind(CTrack *T, int &cnr, int type, CSignal*& sig , BOOL bDir , BOOL bReverse ) 
{
	if ( bReverse == TRUE ) bDir = TRUE; // bReverse 인자가 존재할경우 bDir 인자는 무시된다.

	switch (cnr) {  // 첫번째 신호기 
		case 1:	sig = &(T->m_cSignalC[0]); break;
		case 2:	sig = &(T->m_cSignalN[0]); break;
		case 3:	sig = &(T->m_cSignalR[0]); break;
	}

	
	if (sig->isValid() && (sig->m_nType == type || type == _ANY)) {
		if ( bDir == FALSE ) return TRUE;
		else if ( bReverse == FALSE ) {
			if ( m_iDirection-1 == sig->m_nLR) return TRUE;
		}
		else if ( bReverse == TRUE ) {
			if ( m_iDirection-1 != sig->m_nLR) return TRUE;		
		}
	}

	switch (cnr) {  // 두번째 신호기
		case 1:	sig = &(T->m_cSignalC[1]); break;
		case 2:	sig = &(T->m_cSignalN[1]); break;
		case 3:	sig = &(T->m_cSignalR[1]); break;
	}
	if (sig->isValid() && (sig->m_nType == type || type == _ANY) ) {
		if ( bDir == FALSE ) return TRUE;
		else if ( bReverse == FALSE ) {
			if ( m_iDirection-1 == sig->m_nLR) return TRUE;
		}
		else if ( bReverse == TRUE ) {
			if ( m_iDirection-1 != sig->m_nLR) return TRUE;		
		}
	}
	
	return FALSE;
}

short _Depth          = 0;
int   _giCallonCheck  = 1;  // 메인진로및 콜온진로를 체크 한다. 하나의 진로에 대해서 참인동안 계속 루프를 돈다. 1: 메인진로 2: 콜온진로 0 : 더이상 없음 

int StationDB::ParseCNR3( CTrack *T, int cnr, ostream &os, int mode ) 
{
	TRACE("PARCE CNR3 - TRACK : %s\n" , T->Name);
	CString     brname;
	CString     brTemp,brTemp2,brTemp3,brTemp4,brTemp5,brTemp6,brTemp7;
	CString     fSignal;
	CString     sSignal;
	CTrack		*track;
	CSwitch		*point;
	CMyButton	*btn	= NULL;
	int			cnr1;
	int			node;
	CSignal		*sig	= NULL;

	brname  = T->m_sBranchName;

	fSignal = _FirstSignal->Name;
	if ( _SecondSignal == NULL ) sSignal = "";
	else sSignal = _SecondSignal->Name;

        if (mode == PSMODE2 || mode == PSMODE6) {
			TRACE ("ROUTE OUT1\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			return TRUE;
		}
			if (mode == PSMODE0 || mode == PSMODE6) {
			cnr1 = 1;
			if (SigFind(T,cnr1,4,sig)) {													// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
				if (sigtype <= _OCALLON) {  // ORG if (sigtype < 2) {
				    Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				}
			}
		}
		track = TrackFind(T,_C,node);

		if ( track == NULL ) {
			HandPointFind( T );
		} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
			HandPointFind( T );
		}

	    if (track) {
			if (brname != "") {
				if ( RouteCancleCk( T , fSignal , sSignal ) == _R )
				{		
					if ( Route.GetRouteCount() > 0 ) {
						TRACE("ROUTE CHECK CANCLE : R DIRECTION1\n");
						if ( T->m_bKeyLock != 1 ) {
							_giOverLapCnt--;
							DelDestCnt(gstrDest);
						}
						mode = -1;
						return -1;
					}
				}
				point = AddPoint(T,'R');
				Route.Add( point );
				TRACE("ADD1 POINT R: %s \n" , point->Name);
				CommonTPointFind(T);
			}
			Parse(track,node,os,mode);
			return -1;
		}
		else { /*if (mode & 2)*/ 
			if ( mode == PSMODE3 ) {
				TRACE ("ROUTE OUT2-1\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				return TRUE;			
			} else {		
				btn =	ButtonFind(T);
				if ( btn || mode == PSMODE7 ) {
					TRACE ("ROUTE OUT2\n");
					TrackFrankCheck ();
					Route.Out(os,mode);
				} else {
					TRACE("ROUTE CHECK CANCLE : TRACK FNISH\n");
					_giOverLapCnt--;
					if ( _SecondSignal != NULL ) {
						if ( _SecondSignal->m_nType == _ISTART || _SecondSignal->m_nType == _ISHUNT ) _SecondSignal = NULL;
					}
				DelDestCnt(gstrDest);
				mode = -1;
				return -1;
			}
		}
		}
	    return TRUE;
}

int StationDB::ParseCNR2( CTrack *T, int cnr, ostream &os, int mode ) 
{
	
	// 오버랩 궤도의 갯수는 MODE7 일경우 CNR2 에 들어오는 횟수를 세면 된다. 
	TRACE("PARCE CNR2 - TRACK : %s\n" , T->Name);
	CString     brname;
	CString     brTemp,brTemp2,brTemp3,brTemp4,brTemp5,brTemp6,brTemp7;
	CString     fSignal;
	CString     sSignal;
	CTrack		*track;
	CSwitch		*point;
	CMyButton	*btn	= NULL;
	int			cnr1;
	int			node;
	CSignal		*sig	= NULL;

	brname  = T->m_sBranchName;
	fSignal = _FirstSignal->Name;
	if ( _SecondSignal == NULL ) sSignal = "";
	else sSignal = _SecondSignal->Name;

		if (mode == PSMODE0 || mode == PSMODE6) {
			cnr1 = 1;
			if (SigFind(T,cnr1,4,sig)) {													// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
				if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
				    Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				}
			}
		}

		if (mode == PSMODE3 ) {
			if ( _FirstSignal->m_nType == _START ) {
				TRACE ("ROUTE OUT3\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				return TRUE;

			}
			if ( _FirstSignal->m_nType == _HOME ) {
				cnr1 = 2;
				if (SigFind(T,cnr1,4,sig)) {											// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
					if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
							Route.Add(sig);
							TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					}
				}	
			}
		}	

		track = TrackFind(T,_C,node);

		if ( track == NULL ) {
			HandPointFind( T );
		} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
			HandPointFind( T );
		}


	    if (track) {

			if (brname != "") {
				if ( RouteCancleCk( T , fSignal , sSignal ) == _N ) {
					if ( Route.GetRouteCount() > 0 ) {
						TRACE("ROUTE CHECK CANCLE : N DIRECTION\n");

						point = AddPoint(T,'R','*');
						Route.CheckAdd( point );

						_giOverLapCnt--;
						DelDestCnt(gstrDest);
					}			
				}
				else {
					if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
						point = AddPoint(T,'N','*');
					} else {
						point = AddPoint(T,'N');
					}
					Route.CheckAdd( point );
					Parse(track,node,os,mode);
				}
			}
			else Parse(track,node,os,mode);

		}
		else/* if (mode & 2 )*/ {
			if (mode == PSMODE3){
				TRACE ("ROUTE OUT5-1\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				return TRUE;			
			} else {
				btn =	ButtonFind(T);
				if ( btn || mode == PSMODE7 ) {
					TRACE ("ROUTE OUT5\n");
					TrackFrankCheck ();
					Route.Out(os,mode);
					return TRUE;
				} else {
					TRACE("ROUTE CHECK CANCLE : TRACK FNISH\n");
					_giOverLapCnt--;
					if ( _SecondSignal != NULL ) {
						if ( _SecondSignal->m_nType == _ISTART || _SecondSignal->m_nType == _ISHUNT ) _SecondSignal = NULL;
					}
					DelDestCnt(gstrDest);
					mode = -1;
					return -1;
				}
			}
		} 
		return TRUE;

}

//=======================================================
//
//  함 수 명 :  ParseCNR1
//  함수출력 :
//  함수입력 :
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int StationDB::ParseCNR1( CTrack *T, int cnr, ostream &os, int mode ) 
{
	TRACE("PARCE CNR1 - TRACK : %s\n" , T->Name);
	CString     brname;
	CString     brTemp,brTemp2,brTemp3,brTemp4,brTemp5,brTemp6,brTemp7;
	CString     fSignal;
	CString     sSignal;
	CTrack		*track;
	CSignal		*sig;
	CSwitch		*point;
	CMyButton	*btn	= NULL;
	int			cnr1;
	int			node;
	int         ihandpointck;
	CString     strDependN;
	CString     strDependR;

	ihandpointck = 0;
	sig		= NULL;
	track   = NULL;
	brname  = T->m_sBranchName;
	fSignal = _FirstSignal->Name;
	if ( _SecondSignal == NULL ) sSignal = "";
	else sSignal = _SecondSignal->Name;


	GetDependPoint ( T , strDependN, strDependR );

	if ( T->m_sBranchPower.GetLength() > 2 && T->m_sBranchPower.Mid(2,1) == "R" && mode == PSMODE3 ) {		
		if ( T->m_bKeyLock == TRUE ) {
			ihandpointck = 1;
		}
	}


	if (brname != "" && ihandpointck != 1/*&& T->m_bKeyLock != TRUE*/ ) {  // 전철기가 있더라도 KeyLock 전철기가 아닐경우		
			TRACE("PARCE CNR1 - POINT FIND : %s \n" , brname);
			if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) track = TrackFind(T,_R,node);
		    else track = TrackFind(T,_N,node);

			if ( track == NULL ) {
				HandPointFind( T );
			} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
				HandPointFind( T );
			}

			if (track) {
                if ( mode == PSMODE7 && ( _FirstSignal->m_nType == _CALLON || _FirstSignal->m_nType == _SHUNT ) ) {
					giCallONPushCnt ++;
				}

				if ( giCallONPushCnt == 0 ) {
					TRACE("PARCE CNR1 - TRACK FIND N : %s   ROUTE PUSH \n", track->Name);
					Route.Push();
				}

				if (mode == PSMODE0 ) {
				cnr1 = _N;
					if (SigFind(T,cnr1,_OHOME,sig)) {											// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
						if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
						    Route.Add(sig);
							TRACE("ADD  SIGNAL : %s \n" , sig->Name);
						}
					}	
				}

				if (mode & 2 && mode == PSMODE7 ) {
					if ( RouteCancleCk( T , fSignal , sSignal ) == _N ) {
						if ( Route.GetRouteCount() > 0 ) {
							TRACE("ROUTE CHECK CANCLE : N DIRECTION\n");

							point = AddPoint(T,'R','*');
							Route.CheckAdd( point );

							_giOverLapCnt--;
							DelDestCnt(gstrDest);
						}
					}
					else {
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) point = AddPoint(T,'R');
						else {
							if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
								point = AddPoint(T,'N','*');
							} else {
								point = AddPoint(T,'N');
							}
//							point->m_strPairName = "*";						// overlap, 안전측선
						}
						Route.Add( point );
//						point->m_strPairName = "";						// overlap, 안전측선
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) TRACE("ADD  POINT R-: %s \n" , point->Name);
						else TRACE("ADD  POINT N-: %s \n" , point->Name);
						Parse(track,node,os,mode);
					}

					if ( track &&  ( _FirstSignal->m_nType == _OCALLON) || ( _FirstSignal->m_nType == _HOME) ) {
						Route.Pop();
						TRACE("PARCE CNR 1 - TRACK FIND R : %s \n", track->Name);
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) track = TrackFind(T,_N,node);
					    else track = TrackFind(T,_R,node);

						if ( track == NULL ) {
							HandPointFind( T );
						} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
							HandPointFind( T );
						}

						_giOverLapCnt ++ ;
						AddDestCnt(gstrDest);
						if ( track == NULL ) {
							_giOverLapCnt--;
							DelDestCnt(gstrDest);
							return -1;
						}
						TRACE("PARCE CNR 1 : ROUTE POP \n");
						if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
							if ( Route.GetRouteCount() > 0 ) {
								TRACE("ROUTE CHECK CANCLE : R DIRECTION2\n");
								if ( T->m_bKeyLock != 1 ) {
									_giOverLapCnt--;
									DelDestCnt(gstrDest);
								}
								mode = -1;
								return -1;
							}
						}
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) {
							point = AddPoint(T,'N','*');
							Route.Add( point );
						}
						else {
							point = AddPoint(T,'R');
							Route.Add( point );
							CommonTPointFind(T);
						}// overlap, 안전측선
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) TRACE("ADD  POINT N: %s \n" , point->Name);
						else TRACE("ADD  POINT R!: %s \n" , point->Name);
						mode = PSMODE9;
						HandPointFind( track );
						Route.Add(track);
						TRACE("ADD  TRACK : %s \n" , track->Name);
						Parse(track,node,os,mode);
						return -1;
					}
				} else {  // 모드7이 아닐때
					if ( RouteCancleCk( T , fSignal , sSignal ) == _N ) {
						if ( Route.GetRouteCount() > 0 ) {
							TRACE("ROUTE CHECK CANCLE : N DIRECTION\n");//

							point = AddPoint(T,'R','*');
							Route.CheckAdd( point );
							
							_giOverLapCnt--;
							DelDestCnt(gstrDest);
						}
					}
					else {
						if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) point = AddPoint(T,'R');
						else {
							if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
								point = AddPoint(T,'N','*');
							} else {
								point = AddPoint(T,'N');
							}
						}
						Route.Add( point );
						TRACE("ADD  POINT N: %s \n" , point->Name);
						Parse(track,node,os,mode);
						Route.Pop();
						_giOverLapCnt = 0;
					}
				}
			} else if (mode & 2 && mode == PSMODE7) {
				if (mode == PSMODE7) {
					track = TrackFind(T,_R,node);

					if ( track == NULL ) {
						HandPointFind( T );
					} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
						HandPointFind( T );
					}

					if ( track /*&& track->m_sBranchName == ""*/ && ( _FirstSignal->m_nType == _HOME ) ) {
						TRACE("PARSECNR1_3 - ROUTEPUSH");
						Route.Push();
					}
					if ( RouteCancleCk( T , fSignal , sSignal ) == _N ) {
						if ( Route.GetRouteCount() > 0 ) {
							TRACE("ROUTE CHECK CANCLE : N DIRECTION\n");

							point = AddPoint(T,'R','*');
							Route.CheckAdd( point );

							_giOverLapCnt--;
							DelDestCnt(gstrDest);
						}
					}
					else {
						if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
							point = AddPoint(T,'N','*');
						} else {
							point = AddPoint(T,'N');
						}

						Route.Add( point );														// overlap, 안전측선
						TRACE("ADD  POINT N: %s \n" , point->Name);
						TrackFrankCheck ();
						Route.Out(os,mode);
					}
					if ( track && ( _FirstSignal->m_nType == _HOME ) ) {
						Route.Pop();
						_giOverLapCnt ++ ;
						AddDestCnt(gstrDest);
						if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
							if ( Route.GetRouteCount() > 0 ) {
								TRACE("ROUTE CHECK CANCLE : R DIRECTION3\n");
								if ( T->m_bKeyLock != 1 ) {
									_giOverLapCnt--;
									DelDestCnt(gstrDest);
								}
								mode = -1;
								return -1;
							}
						}
						point = AddPoint(T,'R');
						Route.Add( point );														// overlap, 안전측선
						TRACE("ADD  POINT R@: %s \n" , point->Name);
						CommonTPointFind(T);
						mode = PSMODE9;
						HandPointFind( track );
						Route.Add(track);
						TRACE("ADD  TRACK : %s \n" , track->Name);
						Parse(track,node,os,mode);
					}
				} else {
					TrackFrankCheck ();
					Route.Out(os,mode);
				}
				if ( mode == PSMODE9 ) mode = PSMODE7;
				return TRUE;
			}

			if (mode == PSMODE0 || mode == PSMODE9) {
				if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) track = TrackFind(T,_N,node);
				else track = TrackFind(T,_R,node);

				//track = TrackFind(T,_R,node);

				if ( track == NULL ) {
					HandPointFind( T );
				} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
					HandPointFind( T );
				}

				if (track) {
					cnr1 = 3;
					if (SigFind(T,cnr1,4,sig)) {											// R 궤도 끝단의 구내자동폐색신호기를 찾는다.
						if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
						    Route.Add(sig);
                            TRACE("ADD  SIGNAL : %s \n" , sig->Name);
						}
					}
					if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
						if ( Route.GetRouteCount() > 0 ) {
							TRACE("ROUTE CHECK CANCLE : R DIRECTION4\n");
							if ( T->m_bKeyLock != 1 ) {
								_giOverLapCnt--;
								DelDestCnt(gstrDest);
							}
							mode = -1;
							return -1;
						}						
					}
					if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) {
						point = AddPoint(T,'N');
						Route.Add( point );
					}
					else {
						point = AddPoint(T,'R');
						 Route.Add( point );
						 CommonTPointFind(T);
					}
					if ( T->m_sBranchPower.GetLength() > 3 && T->m_sBranchPower.Mid(4,1) == "R" ) TRACE("ADD  POINT N: %s \n" , point->Name);
					else TRACE("ADD  POINT R#: %s \n" , point->Name);
					Parse(track,node,os,mode);
				}
			}
	} else { 
		if ( brname != "" /*&& T->m_bKeyLock == TRUE*/ ) {
				if ( RouteCancleCk( T , fSignal , sSignal ) == _N ) {
					if ( Route.GetRouteCount() > 0 ) {
						TRACE("ROUTE CHECK CANCLE : N DIRECTION\n");

						point = AddPoint(T,'R','*');
						Route.CheckAdd( point );

						_giOverLapCnt--;
						DelDestCnt(gstrDest);
					}
				}
				else {
					if ( RouteCancleCk( T , fSignal , sSignal ) == _R ) {
						point = AddPoint(T,'N','*');
					} else {
						point = AddPoint(T,'N');
					}
					Route.CheckAdd( point );														// overlap, 안전측선
				}
		}
		TRACE("PARCE CNR1 - POINT NOT FIND\n");
		if (mode == PSMODE0 || mode == PSMODE6) {
			cnr1 = 2;
			if (SigFind(T,cnr1,4,sig)) {												// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
				if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
					Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				}
			}
		}

		if (mode == PSMODE3 ) {
			if ( _FirstSignal->m_nType == _HOME ) {
				cnr1 = 1;
				if (SigFind(T,cnr1,4,sig)) {											// N 궤도 끝단의 구내자동폐색신호기를 찾는다.
					if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
						Route.Add(sig);
						TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					}
				}	
			}
		}

		track = TrackFind(T,_N,node);

		if ( track == NULL ) {
			HandPointFind( T );
		} else if ( SameTrackCheck ( T->Name , track->Name ) == FALSE ) {
			HandPointFind( T );
		}


		if (track && _FirstSignal->m_nType == _START ) {
			if ( mode != PSMODE3 ) {
				Parse(track,node,os,mode);
				return -1;
			} else {
				TRACE ("ROUTE OUT9\n");
//				Route.CheckAdd(T);
				TrackFrankCheck ();
				Route.Out(os,mode);
				return TRUE;			
			}
		}
			
		if (track) {
			Parse(track,node,os,mode);
		} else {
			if ( mode == PSMODE3 || mode == PSMODE9 ) {
				TRACE ("ROUTE OUTA-1\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				return TRUE;			
			} else {
				btn =	ButtonFind(T);
				if ( btn || mode == PSMODE7 ) {
					TRACE ("ROUTE OUTA\n");
					TrackFrankCheck ();
					Route.Out(os,mode);
					return TRUE;
				} else {
					TRACE("ROUTE CHECK CANCLE : TRACK FNISH\n");
					_giOverLapCnt--;
					if ( _SecondSignal != NULL ) {
						if ( _SecondSignal->m_nType == _ISTART || _SecondSignal->m_nType == _ISHUNT ) _SecondSignal = NULL;
					}
					DelDestCnt(gstrDest);
					mode = -1;
					return -1;
				}
			}
		}

	}
	return TRUE;
}

int StationDB::ParseCheckMainLine( CTrack *T, CSignal*sig, int cnr, ostream &os, int mode ) 
{
	//	장내신호의 착점이 발견된 경우 연속하는 출발진로가 있는가? // -> 반대방향의 출발진로도 동일하게 처리 되도록 수정. 97.6.17
	//  연속하는 출발 진로가 있거나 신고기가 있는 트랙이 메인 라인일경우에 PSMODE7 그외의 경우 PSMODE15
	int			cnr1;

	cnr1 = (cnr >= 2) ? 1 : 2;																// C : N
	m_bFind = SigFind(T, cnr1, 1, sig);														// 출발 신호 검사
	if ( !m_bFind ) m_bFind = SigFind(T, cnr, 1, sig);										// 출발 신호 검사

	mode = PSMODE7;

	if ( m_bFind ) {																		// 신호가 있고 검색 진로가 장내 또는 출발인 경우
		Route.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		Route.Add(NULL);
		TRACE("ADD  NULL  \n");
	} else {
		if (T->isMainLine()) {															   // 출발신호가 없어도 도착선이면 종료
			Route.Add(NULL);
			TRACE("ADD  NULL  \n");
		} else mode = PSMODE15;
	}

	return mode;
}

int StationDB::ParseMode15( CTrack *T, int cnr, ostream &os, int mode ) 
{
	TRACE("ParseMode15\n");
	int			cnr1;
	CSignal		*sig	= NULL;

//	HandPointFind( T ); // OMA
	Route.Add(T);
	TRACE("ADD  TRACK6 : %s \n" , T->Name);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}


	m_bFind = FALSE;																		//	장내신호의 착점이 발견된 경우 연속하는 출발진로가 있는가? // -> 반대방향의 출발진로도 동일하게 처리 되도록 수정. 97.6.17
	if (sigtype <= _OCALLON) { // ORG if (sigtype < 2) {
		cnr1 = (cnr >= 2) ? 1 : 2;														// C : N
		m_bFind = SigFind(T, cnr1, 1, sig);												// 출발 신호 검사
		if ( !m_bFind ) m_bFind = SigFind(T, cnr, 1, sig);									// 출발 신호 검사
	}

	if ( m_bFind ) {																		// 신호가 있고 검색 진로가 장내 또는 출발인 경우
		Route.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		Route.Add(NULL);
		TRACE("ADD  NULL  \n");
		mode = PSMODE7;
	} else {
		if (T->isMainLine()) {															// 출발신호가 없어도 도착선이면 종료
			Route.Add(NULL);
			TRACE("ADD  NULL  \n");
			mode = PSMODE7;
		}
	}
	return mode;
}

int StationDB::ParseMode14( CTrack *T, int cnr, ostream &os, int mode )  // 블록 버튼 용 진로 탐색 로직 
{
	mode = PSMODE11;

	TRACE("PARSE MODE 12 : %s\n" , T->Name);
	int			cnr1;
	int			node;
	int         iTcbFind;
	CSignal		*sig	= NULL;
	CTrack		*track	= NULL;

	cnr1 = (cnr == 2) ? 1 : 2;														// C : N

	iTcbFind = SigFind(T, cnr1, _OC, sig);												// 출발 신호 검사
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr, _OC, sig);											// 출발 신호 검사
	}
    if ( iTcbFind ) {
		Route.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}

	m_bFind = SigFind(T, cnr1, _ASTART, sig);												// 출발 신호 검사
	if ( !m_bFind ) {
		m_bFind = SigFind(T, cnr, _ASTART, sig);											// 출발 신호 검사
	}

	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
	Route.Add(sig);
	TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		if (sig->m_nType == _ASTART) {
			TRACE ("ROUTE OUTB\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		}
	} else {
//		HandPointFind( T ); //OMA
		Route.Add(T);
		TRACE("ADD   TRACK  : %s\n" , T->Name);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

		track = TrackFind(T,cnr1,node);
		cnr = node;
		ParseMode12 ( track, cnr, os, mode );
	}

	_Depth--;
	return -1;

}
int StationDB::ParseMode13( CTrack *T, int cnr, ostream &os, int mode )  // 블록 버튼 용 진로 탐색 로직 
{
	mode = PSMODE11;

	TRACE("PARSE MODE 13 : %s\n" , T->Name);
	int			cnr1;
	int			node;
	int         iTcbFind;
	int         iOhomeFind;
	CSignal		*sig	= NULL;
	CTrack		*track	= NULL;


	cnr1 = (cnr == 2) ? 1 : 2;														// C : N

	iTcbFind = SigFind(T, cnr1, _OG, sig);												// 출발 신호 검사
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr, _OG, sig);											// 출발 신호 검사
	}
    if ( iTcbFind ) {
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}
	
	iOhomeFind = SigFind(T, cnr1, _OHOME, sig);												// 출발 신호 검사
	if ( !iOhomeFind ) {
		iOhomeFind = SigFind(T, cnr, _OHOME, sig);											// 출발 신호 검사
	}
    if ( iOhomeFind ) {
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}

	m_bFind = SigFind(T, cnr1, _ASTART, sig);												// 출발 신호 검사
	if ( !m_bFind ) {
		m_bFind = SigFind(T, cnr, _ASTART, sig);											// 출발 신호 검사
	}


	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		if (sig->m_nType == _ASTART) {
			track = TrackFind(T,cnr1,node);
			cnr1 = node;
			ParseMode11 ( track, cnr, os, mode );
			Block.Out(os,mode);
			_Depth--;
			return -1;
		}
	} else {
//		HandPointFind( T ); //OMA
		Route.Add(T);
		Block.Add(T);
		TRACE("ADD   TRACK  : %s\n" , T->Name);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

		track = TrackFind(T,cnr1,node);
		ParseMode10 ( track, cnr, os, mode );
	}

	_Depth--;
	return -1;
}

int StationDB::ParseMode12( CTrack *T, int cnr, ostream &os, int mode )  // 블록 버튼 용 진로 탐색 로직 
{
	mode = PSMODE11;

	TRACE("PARSE MODE 12 : %s\n" , T->Name);
	int			cnr1;
	int			node;
	int         iTcbFind;
	CSignal		*sig	= NULL;
	CSignal		*sig2	= NULL;
	CTrack		*track	= NULL;
	BOOL		bTempFind;

	cnr1 = (cnr == 2) ? 1 : 2;														// C : N

//	Route.Add(T);
//	TRACE("ADD   TRACK  : %s\n" , T->Name);

	iTcbFind = SigFind(T, cnr1, _TOKENLESSC, sig);												// 출발 신호 검사
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr, _TOKENLESSC, sig);											// 출발 신호 검사
	}
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr1, _OC, sig);											// 출발 신호 검사
	}
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr, _OC, sig);											// 출발 신호 검사
	}

    if ( iTcbFind ) {
		Route.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}

	m_bFind = SigFind(T, cnr1, _HOME, sig , FALSE);												// HOME 신호기를 만나면 진로 탐색을 취소한다.
	if ( !m_bFind ) {
		m_bFind = SigFind(T, cnr, _HOME, sig , FALSE);											// HOME 신호 검사
	}


	bTempFind = SigFind(T, cnr1, _ASTART, sig2);												// 출발 신호 검사
	if ( !bTempFind ) {
		bTempFind = SigFind(T, cnr, _ASTART, sig2);											// 출발 신호 검사

	}

	if ( m_bFind == TRUE && bTempFind == TRUE ) { 
		sig = sig2;
		m_bFind = TRUE;
	}
	else if ( m_bFind == FALSE && bTempFind == TRUE ) {
		sig = sig2;
		m_bFind = TRUE;
	}

	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
		Route.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		if (sig->m_nType == _ASTART) {
			TRACE ("ROUTE OUTB\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		}
		if (sig->m_nType == _HOME) {
			TRACE ("ROUTE OUTBB\n");
			TrackFrankCheck ();
//			if ( _Depth == 1 ) { // 첫 궤도에서 신호기를 발견했다면 트랙이 Route에 Add 되지 않는다. 
				Route.CheckAdd(T);
				TRACE("ADD   TRACK  : %s\n" , T->Name);
//			}
			Route.Out(os,mode);
			_Depth--;
			return -1;
		}
	} else {
		Route.CheckAdd(T);
		TRACE("ADD   TRACK  : %s\n" , T->Name);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

		track = TrackFind(T,cnr1,node);
		if ( track != NULL ) {
			cnr = node;
			ParseMode12 ( track, cnr, os, mode );
		}
	}

	_Depth--;
	return -1;

}

int StationDB::ParseMode11( CTrack *T, int cnr, ostream &os, int mode )  // 블록 버튼 용 진로 탐색 로직 
{
	mode = PSMODE11;

	TRACE("PARSE MODE 11 : %s\n" , T->Name);
	int			cnr1;
	int			node;
	int			iPushFlag;
	iPushFlag = 0;
	CSignal		*sig	= NULL;
	CTrack		*track	= NULL;
	CTrack		*track2	= NULL;
	int         icnr1,icnr2;
	icnr1 = 1;
	icnr2 = 2;

	m_bFind = SigFind(T, icnr1, _START, sig);											// 출발 신호 검사
	if ( !m_bFind ) {
		m_bFind = SigFind(T, icnr2, _START, sig);											// 출발 신호 검사
	}

	if ( m_bFind == 0 ) {
		m_bFind = SigFind(T, icnr1, _ISTART, sig);											// 출발 신호 검사
		if ( !m_bFind ) {
			m_bFind = SigFind(T, icnr2, _ISTART, sig);											// 출발 신호 검사
		}
	}

	if ( T->m_sBranchName !="" && cnr !=3 && cnr !=2/* &&  T->m_bKeyLock != TRUE*/ ){
		Route.Push();
		TRACE("PARSEMODE11 - ROUTEPUSH\n");
		iPushFlag = 1;
	}

	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		return -1;
	} else {

		if ( cnr == 1 ) cnr1 = 2;
		else if ( cnr == 2 ) cnr1 = 1;
		else if ( cnr == 3 ) cnr1 = 1;

		track = TrackFind(T,cnr1,node);
	
		cnr = node;

		if ( track ) {
			cnr = node;
			ParseMode11 ( track, cnr, os, mode );
		}
		if ( iPushFlag == 1 ) { 
			iPushFlag = 0;
			Route.Pop();
			track = TrackFind(T,3,node);
			if ( track ) {
				cnr = node;
				if ( ParseMode11 ( track, cnr , os, mode ) == -1 ) return -1;
			}
		}
		return -1;
	}

	return -1;

	track = TrackFind(T,cnr1,node);
	
	if ( track ) {
		cnr = node;
		ParseMode11 ( track, cnr, os, mode );
	}

	return -1;
}

int StationDB::ParseMode10( CTrack *T, int cnr, ostream &os, int mode )  // 블록 버튼 용 진로 탐색 로직 
{
	mode = PSMODE11;

	TRACE("PARSE MODE 10 : %s\n" , T->Name);
	int			cnr1;
	int			node;
	int         iTcbFind;
	int         iOhomeFind;
	CSignal		*sig	= NULL;
	CSignal		*sig2	= NULL;
	CTrack		*track	= NULL;
	BOOL		bTempFind;


	cnr1 = (cnr == 2) ? 1 : 2;														// C : N

	//Route.Add(T);
	//Block.Add(T);
	//TRACE("ADD   TRACK  : %s\n" , T->Name);

	iTcbFind = SigFind(T, cnr1, _TOKENLESSG, sig);												// 출발 신호 검사
	if ( !iTcbFind ) {
		iTcbFind = SigFind(T, cnr, _TOKENLESSG, sig);											// 출발 신호 검사
	}
    if ( iTcbFind ) {
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}
	
	iOhomeFind = SigFind(T, cnr1, _OHOME, sig);												// 출발 신호 검사
	if ( !iOhomeFind ) {
		iOhomeFind = SigFind(T, cnr, _OHOME, sig);											// 출발 신호 검사
	}
    if ( iOhomeFind ) {
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
	}

	m_bFind = SigFind(T, cnr1, _HOME, sig , FALSE);												// HOME 신호기를 만나면 진로 탐색을 취소한다.
	if ( !m_bFind ) {
		m_bFind = SigFind(T, cnr, _HOME, sig , FALSE);											// HOME 신호 검사
	}

	bTempFind = SigFind(T, cnr1, _ASTART, sig2);												// 출발 신호 검사
	if ( !bTempFind ) {
		bTempFind = SigFind(T, cnr, _ASTART, sig2);											// 출발 신호 검사

	}

	if ( m_bFind == TRUE && bTempFind == TRUE ) { 
		sig = sig2;
		m_bFind = TRUE;
	}
	else if ( m_bFind == FALSE && bTempFind == TRUE ) {
		sig = sig2;
		m_bFind = TRUE;
	}

	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
		Route.Add(sig);
		Block.Add(sig);
		TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		if (sig->m_nType == _ASTART) {
			track = TrackFind(T,cnr1,node);
			cnr1 = node;
			ParseMode11 ( track, cnr, os, mode );
			Block.Out(os,mode);
			_Depth--;
			return -1;
		}
		if (sig->m_nType == _HOME) {
			track = TrackFind(T,cnr1,node);
			cnr1 = node;
//			if ( _Depth == 1 ) { // 첫 궤도에서 신호기를 발견했다면 트랙이 Route에 Add 되지 않는다. 
				Route.CheckAdd(T);
				Block.CheckAdd(T);
				TRACE("ADD   TRACK  : %s\n" , T->Name);
//			}
			ParseMode11 ( track, cnr, os, mode );
			Block.Out(os,mode);
			_Depth--;
			return -1;
		}
	} else {
//		HandPointFind( T ); //OMA
		Route.CheckAdd(T);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

		Block.CheckAdd(T);
		TRACE("ADD   TRACK  : %s\n" , T->Name);

		track = TrackFind(T,cnr1,node);
		if ( track != NULL ) {
			ParseMode10 ( track, cnr, os, mode );
		}
	}

	_Depth--;
	return -1;
}

int StationDB::ParseMode9( CTrack *T, int cnr, ostream &os, int mode )  // START 신호기의 TRACK OCCUPY 생성용 
{
	TRACE("ParseMode9\n");
	int			cnr1;
	CSignal		*sig	= NULL;

	
	cnr1 = (cnr >= 2) ? 1 : 2;														// C : N
	m_bFind = SigFind(T, cnr1, _ASTART, sig);												// 출발 신호 검사
	if ( !m_bFind ) {
		m_bFind = SigFind(T, cnr, _ASTART, sig);											// 출발 신호 검사
	}

	if ( m_bFind && sigtype != _HOME ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
	Route.Add(sig);
	TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		if (sigtype == _ASTART) {
			TRACE ("ROUTE OUTC\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		}
	}

	mode = PSMODE7;
	return mode;
}

int StationDB::ParseMode8( CTrack *T, int cnr, ostream &os, int mode )  // START 신호기의 TRACK OCCUPY 생성용 
{
	TRACE("PARSE MODE 8 : %s\n" , T->Name);
	CSignal		*sig	= NULL;
//	HandPointFind( T ); //OMA
	Route.Add(T);
	TRACE("ADD   TRACK  : %s\n" , T->Name);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

	TRACE ("ROUTE OUTD\n");
	TrackFrankCheck ();
	if( _FirstSignal->m_nType != _SHUNT || giShuntCk != 4 || Is_IStartAtFirst != 1 )
		Route.Out(os,mode);															// 2012.07.23  ISHUNT는 [HOLDROUTE]사용안함!
	TRACE("ROUTE OUT    : MODE : %d\n\n" , mode);
	mode = PSMODE0;
	return -1;
}

int StationDB::ParseMode7( CTrack *T, int cnr, ostream &os, int mode ) 
{
	int			cnr1;
	CSignal		*sig	= NULL;
	CTrack		*track	= NULL;

	if (sigtype == _HOME  ) {	// HOME 신호기의 진로
		cnr1 = (cnr >= 2) ? 1 : 2;														// C : N
		m_bFind = SigFind(T, cnr1, _ASTART, sig);	   									// 출발 신호 검사
		if ( !m_bFind ) {
			m_bFind = SigFind(T, cnr, _ASTART, sig);									// 출발 신호 검사
		}
		if ( !m_bFind ) {
			if ( SigFind(T, cnr1, _START, sig) == TRUE ) {
				if ((sig->m_nShape & B_OVERLAP_CK_STOP) != 0)
					m_bFind = TRUE;
			};
		}
		if ( !m_bFind ) {
			if ( SigFind(T, cnr1, _ISTART, sig) == TRUE ) {
				if ((sig->m_nShape & B_OVERLAP_CK_STOP) != 0)
					m_bFind = TRUE;
			};
		}
		if ( !m_bFind ) {
			if ( SigFind(T, cnr1, _GSHUNT, sig) == TRUE ) {
				if ((sig->m_nShape & B_OVERLAP_CK_STOP) != 0)
					m_bFind = TRUE;
			};
		}
	} else if ( sigtype == _OCALLON  ) { 												// 출발 진로 신호  // ORG } else if (sigtype == 1) {															// 출발 진로 신호
//		cnr1 = (cnr >= 2) ? 1 : 2;														// C : N
		if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;

		m_bFind = SigFind(T, cnr, _START, sig , FALSE , TRUE);	   										// 출발 신호 검사
		if ( !m_bFind ) {
			m_bFind = SigFind(T, cnr1, _START, sig , FALSE , TRUE);									// 출발 신호 검사
		}
		if ( !m_bFind ) { // AKHAURA BYPASS 구간의 A48 신호기의 진로 탐색을 위해서 추가 
			m_bFind = SigFind(T, cnr1, _OHOME, sig , FALSE , TRUE);									// 출발 신호 검사
		}

	} else if ( sigtype == _CALLON ) { 													// 출발 진로 신호  // ORG } else if (sigtype == 1) {															// 출발 진로 신호
		if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;
		m_bFind = SigFind(T, cnr1, _ASTART, sig);	   										// 출발 신호 검사
	} else if ( sigtype == _SHUNT ) { 													// 출발 진로 신호  // ORG } else if (sigtype == 1) {															// 출발 진로 신호
		if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;
		
		if ( giShuntCk == 4 ) { // START 에 포함된 션트 이면..
			// 2007년 09월 13일 아카우라역 폐색취급후 출발션트 진로 가능하도록 ( 대항에서 폐색 제거)
			
			m_bFind = SigFind(T, cnr1, _TOKENLESSC, sig , TRUE);	   									// 출발 신호 검사
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr, _TOKENLESSC, sig , TRUE);									// 출발 신호 검사
			}
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr1, _OC, sig , TRUE);									// 출발 신호 검사
			}
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr, _OC, sig , TRUE);									// 출발 신호 검사
			}
			if ( m_bFind == TRUE ) {
				Route.Add(sig);
			}
			
		} else {
				m_bFind = TRUE;			
		}
	} else if ( sigtype == _START ) { 													// 출발 진로 신호  // ORG } else if (sigtype == 1) {															// 출발 진로 신호
		if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;
//		if ( _FirstSignal->m_nLamp == 0 || _FirstSignal->m_nLamp == 1 ) {
//			m_bFind = SigFind(T, cnr1, _OHOME, sig , FALSE);	   									// 출발 신호 검사
//			if ( !m_bFind ) {
//				m_bFind = SigFind(T, cnr, _OHOME, sig , FALSE);									// 출발 신호 검사
//			}	
//		} else {
//			2012.07.23  BCI역 8,10,12신호기 overlap확인결과 다른 신호기와 동일하게 취급하기로 함.
			gbOuterFindCk = SigFind(T, cnr1, _OHOME, sig , FALSE);	   									// 출발 신호 검사
			if ( !gbOuterFindCk ) {
				gbOuterFindCk = SigFind(T, cnr, _OHOME, sig , FALSE);									// 출발 신호 검사
			}	

			if ( gbOuterFindCk == TRUE ) Route.RemoveTail();

/*			// START 신호기는 BLOCK 을 보지 않기로 했다. 2006.09.15
			gsBlockSignal = NULL;
			m_bFind = SigFind(T, cnr1, _TOKENLESSC, sig , TRUE);	   									// 출발 신호 검사
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr, _TOKENLESSC, sig , TRUE);									// 출발 신호 검사
			} else {
				gsBlockSignal = sig;
			}
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr1, _OC, sig , TRUE);									// 출발 신호 검사
			} else {
			    gsBlockSignal = sig;
			}
			if ( !m_bFind ) {
				m_bFind = SigFind(T, cnr, _OC, sig , TRUE);									// 출발 신호 검사
			} else {
			    gsBlockSignal = sig;			
			}
			if ( gsBlockSignal != NULL ) Route.Add(gsBlockSignal);
*/
//		}

	}

	TRACE("PARSE MODE 7  : SIGNAL FIND %d \n",m_bFind);
	int iTemp;
	
	if ( m_bFind ) {																    // 다음 진로(Next Route)에 대한 신호 찾음
		if (sigtype == _OCALLON ) {
			TRACE ("ROUTE OUTE\n");
			iTemp = Route.GetTrackCount();
			CGraphObject* item = (CGraphObject *)Route.GetTracks( iTemp-1 );
			if (item->IsKindOf(RUNTIME_CLASS(CTrack))) {
			} else {
				Route.RemoveTail();
			}
			Route.RemoveTail();
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		} else if (sigtype == _CALLON ) {
			Route.RemoveTail();
			TRACE ("ROUTE OUTF\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		} else if (sigtype == _HOME ) {
			TRACE ("ROUTE OUTF\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		} else if (sigtype == _SHUNT ) {
			TRACE ("ROUTE OUTF\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			_Depth--;
			return -1;
		} else if (sigtype == _START ) {
			if ( _FirstSignal->m_nLamp == 0 || _FirstSignal->m_nLamp == 1 ) {
				Route.RemoveTail();
			} else {
				if ( _SecondSignal->m_nType != _PSTART ) {
					Route.Add(sig);
				}
			}
			TRACE ("ROUTE OUTF\n");
			TrackFrankCheck ();
			Route.Out(os,mode);
			gbOuterFindCk = FALSE;
			_Depth--;
			return -1;
		}
	}

	return mode;
}

int StationDB::ParseMode6( CTrack *T, int cnr, ostream &os, int mode ) 
{
	TRACE("ParseMode6\n");
	CSignal		*sig	= NULL;
	if (SigFind(T,cnr,0,sig)) { 
		TRACE ("ROUTE OUTG\n");
		TrackFrankCheck ();
		Route.Out(os,mode);
		_Depth--;
		return -1;
	}

//	HandPointFind( T ); //OMA
	Route.Add(T);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

	TRACE("ADD  TRACK8 : %s \n" , T->Name);
	return mode;
}

int StationDB::ParseMode2( CTrack *T, int cnr, ostream &os, int mode ) 
{
	TRACE("ParseMode2\n");
	CSignal		*sig	= NULL;
	if (SigFind(T,cnr,0,sig)) {
		TRACE ("ROUTE OUTH\n");
		TrackFrankCheck ();
		Route.Out(os,mode);
		_Depth--;
		return -1;
	}
//	HandPointFind( T ); //OMA
	Route.Add(T);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

	TRACE("ADD  TRACK9 : %s \n" , T->Name);
	return mode;
}

int StationDB::ParseMode1( CTrack *T, int cnr, ostream &os, int mode )  
{
	TRACE("ParseMode1\n");
	if ( T->isMainLine() ) {																
//		HandPointFind( T ); //OMA
		Route.Add(T);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

		TRACE("ADD  TRACK10 : %s \n" , T->Name);
		Route.Add(NULL);	
		TRACE("ADD  NULL  \n");
	}
	return mode;
}

int StationDB::ParseMode0( CTrack *T, int cnr, ostream &os, int mode ,int flag) // HOME 신호기를 만났을때 
{
	TRACE("PARSE MODE 0 : %s\n" , T->Name);
	CMyButton	*btn	= NULL;
	CSignal		*sig	= NULL;
	BOOL        bSigFind;
	int			cnr1;
	giShuntPushCnt = 0;

	Route.Add(T);
	CLevelCross	*lcro	= NULL;
	lcro =	LevelCrossFind(T);
	CString strTemp;
	if ( lcro ) {
		if (T->m_nShape & B_MAIN) {
			strTemp = "HT-"+T->Name;
		} else {
			strTemp = T->Name;
		}
		Route.Add(lcro);
		AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
		TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
	}

	TRACE("ADD  TRACK11 : %s \n" , T->Name);

	btn =	ButtonFind(T);

	if ( btn && (btn->m_nShape & B_CONT)) btn = NULL;

	if ( btn ) {
		gstrDest = btn->Name;
		AddDestCnt( btn->Name );

		if (btn->m_nShape & B_CONT) {													// 착점 버튼이 연속 진로인 경우 다음 진로를 처리하고 버튼처리
			/* // 원래는 Serise Course 의경우 루트를 하나 만들고 이어서 계속 루트를 만드나 AKA 역의 SDG 를 위하여 무시하는 것으로 변경 	
			Route.Push();
			TRACE("ParseMode0 - ROUTEPUSH ");
			Parse(T, cnr, os, mode, TRUE);
			Route.Pop();
			*/
		}

		if (btn->m_nShape & B_NOCALLON) {
			if ( _FirstSignal->m_nType == _OCALLON || _FirstSignal->m_nType == _CALLON ) {// 아카우라 바이패스 역의 58신호기는 START 로도 OUTER 신호기로도 동작한다. 
				TRACE("ROUTE CHECK CANCLE : NO CALLON ROUTE\n");
				_giOverLapCnt--;
				if ( _SecondSignal != NULL ) {
					if ( _SecondSignal->m_nType == _ISTART || _SecondSignal->m_nType == _ISHUNT ) _SecondSignal = NULL;
				}
				DelDestCnt(gstrDest);
				mode = -1;
				return -1;
			}
		}


		Route.Add(btn);
		TRACE("ADD  BUTTON : %s \n" , btn->Name);


		if ( T->Name.Find('?') != -1 /*|| _FirstSignal->m_bF1 == TRUE*/ ) {
			    if ( cnr == _C ) cnr1 = _C;
				else cnr1 = _N;

				if ( _FirstSignal->m_nLamp == 11 || _FirstSignal->m_nILR == 5) {
					bSigFind = SigFind(T,cnr1,_GSHUNT,sig , TRUE, TRUE);							
					if ( bSigFind ) {
						if ( sig->m_nLamp == 11 || sig-> m_nILR == 5 ) {
							Route.Add(sig);
						}
					} else {
						bSigFind = SigFind(T,cnr1,_START,sig , TRUE, TRUE);							
						if ( bSigFind ) {
							if ( sig-> m_nILR == 5 ) {
								Route.Add(sig);
							}
						}
					}

				}
				if ( sigtype != _SHUNT && sigtype != _CALLON ) {
					TRACE("ROUTE CHECK CANCLE : SDG TRACK\n");
					if ( T->m_bKeyLock != 1 ) {
						_giOverLapCnt--;
						DelDestCnt(gstrDest);
					}
					mode = -1;
					return -1;
				}


			TrackFrankCheck ();
			Route.Out(os,mode);
			mode = -1;
			return -1;
		}

		if (sigtype == _HOME) {																// 장내신호일경우
		    if ( cnr == _C ) cnr1 = _N;
			else cnr1 = _C;

			bSigFind = SigFind(T,cnr1,_START,sig);
			if ( bSigFind ) {
				Route.Add(sig);
				mode = PSMODE7;																	// 신호 검색
			}

			_SecondSignal = sig;
			Route.Add(NULL);
		} else if ( sigtype == _OHOME || sigtype == _OCALLON ) {
			cnr1 = 1;
			bSigFind = SigFind(T,cnr1,0,sig);
			if ( bSigFind ) {
				Route.Add(sig);
				TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				_SecondSignal = sig;
			} else {
				cnr1 = 2;
				bSigFind = SigFind(T,cnr1,0,sig);
				if ( bSigFind ) {
					Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					_SecondSignal = sig;
				}
			}
			if ( sigtype == _OHOME ) {
				TRACE ("ROUTE OUTI\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				_Depth--;
				return -1;
			} else if ( sigtype == _OCALLON ) {
				mode = PSMODE7;
			}
		}else if (  sigtype == _CALLON ) {
			    if ( cnr == _C ) cnr1 = _N;
				else cnr1 = _C;

				bSigFind = SigFind(T,cnr1,_GSHUNT,sig, FALSE , FALSE);	
				if ( bSigFind ) {
					Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				}

				bSigFind = SigFind(T,cnr1,_START,sig);
				if ( bSigFind ) {
					Route.Add(sig);
				}
				Route.Add(NULL);
				TrackFrankCheck ();
				Route.Out(os,mode);
				_Depth--;
				return -1;

		}else if (  sigtype == _SHUNT ) {
			    if ( cnr == _C ) cnr1 = _N;
				else cnr1 = _C;
					bSigFind = SigFind(T,cnr1,_START,sig);
					if ( bSigFind ) {
						Route.Add(sig);
					} else {
						bSigFind = SigFind(T,cnr1,_SHUNT,sig);
						if ( bSigFind ) {
							Route.Add(sig);
						} else {
							bSigFind = SigFind(T,cnr1,_LOS,sig);
							if ( bSigFind ) {
								Route.Add(sig);
							} else {
								bSigFind = SigFind(T,cnr1,_ASTART,sig);							
								if ( bSigFind ) {
									Route.Add(sig);
								} else {
									if ( _FirstSignal->m_nLamp == 11 ) {
										bSigFind = SigFind(T,cnr1,_SHUNT,sig , TRUE, TRUE);							
										if ( bSigFind ) {
											Route.Add(sig);
										}
									}
								}
							}
						}
					}
				mode = PSMODE7;																	// 신호 검색
				Route.Add(NULL);
		}else if (  sigtype == _ASTART ) {
				cnr1 = 1;	
				bSigFind = SigFind(T,cnr1,6,sig);
				if ( bSigFind == FALSE ) {
					bSigFind = SigFind(T,cnr1,_OG,sig);
				}
				if ( bSigFind ) {
					Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					_SecondSignal = sig;
				} else {
					cnr1 = 2;
					bSigFind = SigFind(T,cnr1,6,sig);
					if ( bSigFind == FALSE ) {
						bSigFind = SigFind(T,cnr1,_OG,sig);
					}
					if ( bSigFind ) {
						Route.Add(sig);
						TRACE("ADD  SIGNAL : %s \n" , sig->Name);
						_SecondSignal = sig;
					}
				}

				gsBlockSignal = NULL;
				m_bFind = SigFind(T, cnr1, _TOKENLESSC, sig , TRUE);	   									// 출발 신호 검사
				if ( !m_bFind ) {
					m_bFind = SigFind(T, cnr, _TOKENLESSC, sig , TRUE);									// 출발 신호 검사
				} else {
					gsBlockSignal = sig;
				}
				if ( !m_bFind ) {
					m_bFind = SigFind(T, cnr1, _OC, sig , TRUE);									// 출발 신호 검사
				} else {
					gsBlockSignal = sig;
				}
				if ( !m_bFind ) {
					m_bFind = SigFind(T, cnr, _OC, sig , TRUE);									// 출발 신호 검사
				} else {
					gsBlockSignal = sig;			
				}

				if ( gsBlockSignal != NULL ) Route.Add(gsBlockSignal);

				TRACE ("ROUTE OUTJ\n");
				TrackFrankCheck ();
				Route.Out(os,mode);
				_Depth--;
				return -1;
		}else if (  sigtype == _START ) {
				if ( cnr == _C ) cnr1 = _N;
				else cnr1 = _C;
				bSigFind = SigFind(T,cnr1,_ISTART,sig);
				if ( bSigFind ) {
					Route.Add(sig);
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					mode = PSMODE0;																	// 신호 검색
				} 	else {				
					bSigFind = SigFind(T,cnr1,_START,sig);
					if ( bSigFind ) {
						if ( (sig->m_nShape & B_NOROUTE_ALL) == FALSE ) sig->m_nType = _PSTART; //_PSTART
						Route.Add(sig);
						_SecondSignal = sig;
						TRACE("ADD  SIGNAL : %s \n" , sig->Name);
						mode = PSMODE7;																	// 신호 검색
					} else {
						bSigFind = SigFind(T,cnr1,_ASTART,sig);
						if ( bSigFind ) {
							Route.Add(sig);
							//if ( _SecondSignal == NULL ) _SecondSignal = sig; // 아카우라 역 47 신호기 진로에 문제 있어서 수정 Second Sig 가 남아 있어서 71쪽 진로에 63신호기가 ahead로 들어감
							_SecondSignal = sig;
							TRACE("ADD  SIGNAL : %s \n" , sig->Name);
							mode = PSMODE7;																	// 신호 검색
						} else {
							bSigFind = SigFind(T,cnr1,_HOME,sig);
							if ( bSigFind ) {
								Route.Add(sig);
								if ( _SecondSignal == NULL ) _SecondSignal = sig;
								TRACE("ADD  SIGNAL : %s \n" , sig->Name);
								mode = PSMODE7;																	// 신호 검색
							} else {
								mode = PSMODE0;																	// 신호 검색
							}
						}
					}
				}
		} else {
			mode = PSMODE7;															// 출발
		}
	} else if (sigtype == _HOME){
			// 2006.2.1 Shunt 검색 추가
	    if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;

		bSigFind = SigFind(T,cnr1,_GSHUNT,sig, FALSE , FALSE);	
		if ( bSigFind ) {
			Route.Add(sig);
			TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		}
	} else if (sigtype == _CALLON){
			// 2006.2.1 Shunt 검색 추가
	    if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;

		bSigFind = SigFind(T,cnr1,_GSHUNT,sig, FALSE , FALSE);	
		if ( bSigFind ) {
			Route.Add(sig);
			TRACE("ADD  SIGNAL : %s \n" , sig->Name);
		}
	} else if ( sigtype == _OG) {
		mode = PSMODE7;																	// 신호 검색
	} else if ( sigtype == _START ) {
//				cnr1 = _N;
	    if ( cnr == _C ) cnr1 = _N;
		else cnr1 = _C;

				bSigFind = SigFind(T,cnr1,_GSHUNT,sig);/*, FALSE , FALSE);	*/
				if ( bSigFind ) {
					Route.Add(sig);
				TRACE("ADD  SIGNAL : %s \n" , sig->Name);
				}
//				cnr1 = _N;
				bSigFind = SigFind(T,cnr1,_ISTART,sig);
				if ( bSigFind ) {
					Route.Add(sig);
					_SecondSignal = sig;
					TRACE("ADD  SIGNAL : %s \n" , sig->Name);
					mode = PSMODE0;
				} else {
					bSigFind = SigFind(T,cnr1,_START,sig);
					if ( bSigFind ) {
						if ( (sig->m_nShape & B_NOROUTE_ALL) == FALSE ) sig->m_nType = _PSTART; //_PSTART
						Route.Add(sig);
						if ( _SecondSignal == NULL ) _SecondSignal = sig;
						TRACE("ADD  SIGNAL : %s \n" , sig->Name);
						mode = PSMODE7;																	// 신호 검색
					}				
				}
	} else if ( sigtype == _SHUNT ) {
			    if ( cnr == _C ) cnr1 = _N;
				else cnr1 = _C;
				bSigFind = SigFind(T,cnr1,_ISTART,sig);
				if ( bSigFind ) {
					Route.Add(sig);
					_SecondSignal = sig;
					TRACE("ROUTE CHECK : FIND ISTART\n");
					mode = PSMODE0; // -> 이건 위두라인의 반대..
				} else mode = PSMODE0;
	}
//	HandPointFind( T ); //OMA
	return mode;
}

//=======================================================
//
//  함 수 명 :  HandPointFind
//  함수출력 :  CSwitch
//  함수입력 :  CTrack *T
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  0.9
//  설    명 :  신호기가 있는 출발 트랙의 Hand thrown point 를 찾아서 리턴한다.
//
//=======================================================
CSwitch *StationDB::HandPointFind( CTrack *T )
{
	CSwitch * point = NULL;
	CTrack * track = NULL;
	CTrack * trackbackup = T;
	int cnr = _C;
	int iTrackCount;
	BOOL bCheck = TRUE;
	CString strTemp1,strTemp2;
	if ( _FirstSignal->m_nType == _SHUNT  || 
		 _FirstSignal->m_nType == _ISHUNT || 
		 _FirstSignal->m_nType == _GSHUNT || 
		 _FirstSignal->m_nType == _ISTART ||
		 (_FirstSignal->m_nType == _START && giStartCk == 1)) return point;
	iTrackCount = Route.GetTrackCount();
	strTemp1 = T->Name;
	DelStringMultiNo( strTemp1 );

//	POSITION pos = m_pDoc->m_objects.GetHeadPosition();
	POSITION pos = m_pDoc->m_objects.GetTailPosition();

	while (pos != NULL) {
//		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetPrev(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack)) && pObj->isValid()) {
			track = (CTrack *)pObj;
			strTemp2 = track->Name;
			DelStringMultiNo( strTemp2 );

			if ( strTemp1 == strTemp2 ) {
				if ( track->m_sBranchName != "" ) {
					if ( track->m_bKeyLock == TRUE ) {
						point = AddPoint(track,'N');
						////////////////////////////////////2012.08.20 Hand Point 미리 당겨오던 것을 @standard에서는 사용 안함!
						if ( ((point->Name.Find("25") < 0) || (point->Name.Find("26") < 0)) && strStationName.Find("@Standard") < 0)
							Route.CheckAdd(point);
					}
				}			
			}
		}
	}
	return point;
}

//=======================================================
//
//  함 수 명 :  CommonTPointFind
//  함수출력 :  CSwitch
//  함수입력 :  CTrack *T
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  0.9
//  설    명 :  동일 트랙에 여러개의 전철기가 있을경우에 해당 전철기를 찾아서 리턴한다.
//
//=======================================================
CSwitch *StationDB::CommonTPointFind( CTrack *T )
{
	CSwitch * point = NULL;
	CTrack * track = NULL;
	CTrack * trackbackup = T;
	int cnr = _N;
	int node;
	BOOL bCheck = TRUE;
	CString strTemp1,strTemp2;
	int m_iOrgVector;
	int m_iNewVector;

	m_iOrgVector = T->m_iBranchVect;
	while ( bCheck ) {
		track = TrackFind(T,cnr,node);

		if ( track != NULL ) {
			strTemp1 = track->Name;
			strTemp2 = T->Name;
			m_iNewVector = track->m_iBranchVect;
			DelStringMultiNo( strTemp1 );
			DelStringMultiNo( strTemp2 );
			if ( strTemp1 != strTemp2 ) bCheck = FALSE;
			else {		
				if ( track->m_sBranchName != "" ) {
					if ( track->m_bKeyLock != TRUE ) {
						point = AddPoint(track,'N');
// 2013.01.15 special case 삭제.( 모양에 따른 예외처리. >> 차후 T&C때 확정 필요)
//						if ( m_iNewVector == 2 && m_iOrgVector == 3 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 3 && m_iOrgVector == 2 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 1 && m_iOrgVector == 4 )
//						{
//							if ( !(__editdev19.Find("-KQC-") > 0 && point->Name == "21A"))
//								point->m_strPairName = "*";
//						}
//						if ( m_iNewVector == 4 && m_iOrgVector == 1 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 6 && m_iOrgVector == 7 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 7 && m_iOrgVector == 6 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 4 && m_iOrgVector == 7 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 7 && m_iOrgVector == 4 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 5 && m_iOrgVector == 8 ) point->m_strPairName = "*";
//						if ( m_iNewVector == 8 && m_iOrgVector == 5 ) point->m_strPairName = "*";
						Route.CheckAdd(point);
						TRACE("ADD11 POINT N: %s \n" , point->Name);
					}
				}

				if ( node == 2 ) cnr = 1;
				if ( node == 1 ) cnr = 2;
				T = track;
			}
		} else bCheck = FALSE;
	}

	return point;
}

//=======================================================
//
//  함 수 명 :  CommonTPointFind
//  함수출력 :  CSwitch
//  함수입력 :  CTrack *T
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  0.9
//  설    명 :  동일 트랙에 여러개의 전철기가 있을경우에 해당 전철기를 찾아서 리턴한다.
//
//=======================================================
CSwitch *StationDB::DependPointFind( CTrack *T )
{
	CSwitch * point = NULL;
	CTrack * track = NULL;
	CTrack * trackbackup = T;
	int cnr = _N;
	int node;
	BOOL bCheck = TRUE;
	CString strTemp1,strTemp2;

	while ( bCheck ) {
		track = TrackFind(T,cnr,node);

		if ( track != NULL ) {
			strTemp1 = track->Name;
			strTemp2 = T->Name;

			DelStringMultiNo( strTemp1 );
			DelStringMultiNo( strTemp2 );
			if ( strTemp1 != strTemp2 ) bCheck = FALSE;
			else {		
				if ( track->m_sBranchName != "" ) {
					if ( track->m_bKeyLock != TRUE ) {
						point = AddPoint(track,'N');
						Route.Add(point);
						TRACE("ADD10 POINT N: %s \n" , point->Name);
					}
				}

				if ( node == 2 ) cnr = 1;
				if ( node == 1 ) cnr = 2;
				T = track;
			}
		} else bCheck = FALSE;
	}

	return point;
}

int StationDB::GetDestCnt( CString strDestButton )
{
	BOOL bFindDest;
	int iret;
	iret = 0;
	bFindDest = FALSE;
	for ( int i = 0 ; i <= giDestSignal ; i++ ) {
		if ( gstrDestSignal[i] == strDestButton ) {
			bFindDest = TRUE;
			iret = giDestSignalCnt[i];
		}
	}

	return iret; 
}

void StationDB::DelDestCnt( CString strDestButton )
{

	BOOL bFindDest;
	bFindDest = FALSE;
	for ( int i = 0 ; i <= giDestSignal ; i++ ) {
		if ( gstrDestSignal[i] == strDestButton ) {
			giDestSignalCnt[i]--;
			bFindDest = TRUE;
		}
	}

/*	if ( bFindDest == FALSE ) {
		giDestSignal--;
		gstrDestSignal[giDestSignal] = strDestButton;
		giDestSignalCnt[giDestSignal] = 0;
	}
*/
}

void StationDB::AddDestCnt( CString strDestButton )
{

	BOOL bFindDest;
	bFindDest = FALSE;
	for ( int i = 0 ; i <= giDestSignal ; i++ ) {
		if ( gstrDestSignal[i] == strDestButton ) {
			giDestSignalCnt[i]++;
			bFindDest = TRUE;
		}
	}

	if ( bFindDest == FALSE ) {
		giDestSignal++;
		gstrDestSignal[giDestSignal] = strDestButton;
		giDestSignalCnt[giDestSignal] = 0;
	}
}

void StationDB::Parse( CTrack *T, int cnr, ostream &os, int mode, BOOL bNext ) 
{

	TRACE ("PARSE : TRACK %s , CNR %d , MODE %d, DEPTH %d , NEXT %d \n",T->Name,cnr,mode,_Depth,bNext);
	if (_Depth++ > 200) return;																// 파싱 깊이가 200 보다 크면 리턴 

	CSignal		*sig	= NULL;
	CMyButton	*btn	= NULL;
	CSwitch		*point  = NULL;
	m_bFind = FALSE;
	CString strTemp;
	if ( bNext == FALSE ) {
		switch ( mode ) {
		case PSMODE0  : mode = ParseMode0  ( T, cnr, os, mode ); break;
		case PSMODE1  : mode = ParseMode1  ( T, cnr, os, mode ); break;
		case PSMODE2  : mode = ParseMode2  ( T, cnr, os, mode ); break;
		case PSMODE6  : mode = ParseMode6  ( T, cnr, os, mode ); break;
		case PSMODE8  : mode = ParseMode8  ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE9  : mode = ParseMode9  ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE10 : mode = ParseMode10 ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE11 : mode = ParseMode11 ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE12 : mode = ParseMode12 ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE13 : mode = ParseMode13 ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE14 : mode = ParseMode14 ( T, cnr, os, mode ); break;  // 처음 시작은 PSMODE3 또는 PSMODE8 인데 PSMODE3 은 뒤로 계속가면서 PSMODE8 은 자기 진로만 HOLDROUTE 에 추가 한다. 
		case PSMODE15 : mode = ParseMode15 ( T, cnr, os, mode ); break;
		default       : if (mode == -1 ) return;
						else {							if ( _FirstSignal->m_nType == _START && gbOuterFindCk == TRUE ) {
							} else {
								if ( _FirstSignal->m_nType == _SHUNT && giShuntCk == 4 ) {
								} else {
									Route.Add(T);
//									HandPointFind( T ); //OMA
									CLevelCross	*lcro	= NULL;
									lcro =	LevelCrossFind(T);
									if ( mode == PSMODE3 && _Depth > 1 ) {
									} else {
									if ( lcro ) {
										if (T->m_nShape & B_MAIN) {
											strTemp = "HT-"+T->Name;
										} else {
											strTemp = T->Name;
										}
										Route.Add(lcro);
										AddCross(lcro->Name,lcro->x,lcro->y,strTemp);
										TRACE("ADD  LEVEL CROSS : %s \n" , lcro->Name);
									}
									}
									TRACE("ADD  TRACK12 : %s \n" , T->Name);
								}
							}
						}
		}

		if ( mode == -1) return;
		
		if (/*!m_bFind &&*/ (mode == PSMODE7)) {												// 장내, 출발 신호에 대한 도착점 이후 진로의 신호 검색
			mode = ParseMode7( T, cnr, os, mode );
			if (mode == -1 ) return;
		}
	}

    switch (cnr) {
	case 1 :  mode = ParseCNR1( T, cnr, os, mode );break; 											// from Cs
	case 2 :  mode = ParseCNR2( T, cnr, os, mode );break;											// from N
	case 3 :  mode = ParseCNR3( T, cnr, os, mode );break;											// from R
    }


	if ( mode == PSMODE9 ) mode = PSMODE7;
	_Depth--;
}

// 궤도명 가져오기.
CString GetIntName( CString &str ) {
	if (str == "") return str;
	char bf[32];
	int j, i, m_strlen;
	j = i = m_strlen = 0;
	strcpy( bf, str );
	char *p = bf;
	while (*p && !isdigit(*p)){
		p++;
		i++;
	}
	if ( *p ) {
		while (*p) {
			if (!isdigit(*p)) {
				*p = 0;
				break;
			}
			j++;
			p++;
		}
	}
	CString name = bf;
	name = name.Mid(i,j);
	m_strlen = strlen(str);
	if( m_strlen == i && i > 1 )
		name = "";
	strcpy( bf, name );	
	return bf;
}

void StationDB::InfoListOutPut( ostream &os ) 
{
	// 도면 정보를 출력한다. 
	os << ";[INFO01] " << m_pDoc->m_StationName <<"\r\n";
	os << ";[INFO02] " << __editdev1  <<"\r\n";
	os << ";[INFO03] " << __editdev2  <<"\r\n";
	os << ";[INFO04] " << __editdev3  <<"\r\n";
	os << ";[INFO05] " << __editdev4  <<"\r\n";
	os << ";[INFO06] " << __editdev5  <<"\r\n";
	os << ";[INFO07] " << __editdev6  <<"\r\n";
	os << ";[INFO08] " << __editdev7  <<"\r\n";
	os << ";[INFO09] " << __editdev8  <<"\r\n";
	os << ";[INFO10] " << __editdev9  <<"\r\n";
	os << ";[INFO11] " << __editdev10 <<"\r\n";
	os << ";[INFO12] " << __editdev11 <<"\r\n";
	os << ";[INFO13] " << __editdev12 <<"\r\n";
	os << ";[INFO14] " << __editdev13 <<"\r\n";
	os << ";[INFO15] " << __editdev14 <<"\r\n";
	os << ";[INFO16] " << __editdev15 <<"\r\n";
	os << ";[INFO17] " << __editdev16 <<"\r\n";
	os << ";[INFO18] " << __editdev17 <<"\r\n";
	os << ";[INFO19] " << __editdev18 <<"\r\n";
	os << ";[INFO20] " << __editdev19 <<"\r\n";
	os << ";[INFO21] " << __editdev20 <<"\r\n";
	os << ";[INFO22] " << __editdev21 <<"\r\n";
}

void StationDB::TrackListOutPut( ostream &os ) 
{

	POSITION	pos;
	CTrack		*track; 
	CSignal		*signal;

	pos = m_pDoc->m_objects.GetHeadPosition();

	while (pos != NULL) 
	{
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack)) && pObj->isValid() ) 
		{
			track = (CTrack*)pObj;
			if ( track->m_sBranchPower.GetLength() > 2 ) 
			{
				if ( track->m_sBranchPower.Mid(3,1) == "L" )  
				{	
					os << "@TRACK=" << track->Name << "@L\r\n";
				}
				else 
				{
					os << "@TRACK=" << track->Name << "\r\n";
				}
			} 
			else 
			{
				os << "@TRACK=" << track->Name << "\r\n";
			}

			
			signal = &track->m_cSignalC[0];												// 신호기 출력
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:C1="; 
				signal->Dump( os ); 
			}
			signal = &track->m_cSignalC[1];
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:C2="; 
				signal->Dump( os ); 
			}
			signal = &track->m_cSignalN[0];
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:N1="; 
				signal->Dump( os ); 
			}
			signal = &track->m_cSignalN[1];
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:N2="; 
				signal->Dump( os ); 
			}
			signal = &track->m_cSignalR[0];
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:R1="; 
				signal->Dump( os ); 
			}
			signal = &track->m_cSignalR[1];
			if ( signal->isValid() ) 
			{ 
				os << "@SIGNAL:R2="; 
				signal->Dump( os ); 
			}

			if (track->m_sBranchName != "") 
			{	// 분기가 있을경우	C,N,R									// 접속 노드를 찾음
				track->m_pConnect[0] = TrackFind(track, _C, track->m_nConnect[0]); 
				track->m_pConnect[1] = TrackFind(track, _N, track->m_nConnect[1]);
				track->m_pConnect[2] = TrackFind(track, _R, track->m_nConnect[2]);
				os << "@NODE=" << track->m_sBranchName;
				os << "@B";
				if ( track->m_iBranchVect == 1) 
					os << "+1,";
				else if ( track->m_iBranchVect == 2) 
					os << "+2,";
				else if ( track->m_iBranchVect == 3) 
					os << "+3,";
				else if ( track->m_iBranchVect == 4) 
					os << "+4,";
				else if ( track->m_iBranchVect == 5) 
					os << "+5,";
				else if ( track->m_iBranchVect == 6) 
					os << "+6,";
				else if ( track->m_iBranchVect == 7) 
					os << "+7,";
				else if ( track->m_iBranchVect == 8) 
					os << "+8,";
				else 
					os << "+0,";
				
				track->NodeDump( os, 0 );
				os << ",";
				track->NodeDump( os, 1 );
				os << ",";
				track->NodeDump( os, 2 );
				os << "\r\n";
			} 
			else 
			{						// 분기가 없을경우 C,N
				track->m_pConnect[0] = TrackFind(track, _C, track->m_nConnect[0]); 
				track->m_pConnect[1] = TrackFind(track, _N, track->m_nConnect[1]);
				os << "@NODE=";
				track->NodeDump( os, 0 );
				os << ",";
				track->NodeDump( os, 1 );
				os << "\r\n";					
			}
		} 
	} 
}

void StationDB::RootParse( ostream &os ) 
{

	POSITION	pos;
	POSITION	pos2;
	CTrack		*track; 
    CTrack		*NextTrack = NULL; 
	CSignal		*signal;
	CString		TBtemp[50],	   Temp[50],		pFTN[50],       Blank[50];
	CString		TBinteger[50], Tempinteger[50], TrackBlock[50]; 

	int i, j, p, q, m_trackcount;
	int cnr = 0;
	int iStartCk = 0;
	int iGShuntCk = 0;
	Is_IStartAtFirst = 0;		//SRY 타입의 ISTART신호기가 DB생성후 START로 돌아가는 것을 막기 위해 추가로 선언하여 사용

	i = j = p = q = m_trackcount = 0;
	m_iTrackJoinCnt = 0;																	// 트랙 조인 카운트 초기화

	InfoListOutPut( os );

	if (!m_bOneRoute) TrackListOutPut( os );

	pos = m_pDoc->m_objects.GetHeadPosition();
	while (pos != NULL) {
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack)) && pObj->isValid() && (!m_bOneRoute || pObj->GetName() == m_TrkName)) {
			track = (CTrack*)pObj;
			track->m_bCheck = FALSE;    // For SCISSORS Check
			os << ";TRACK:" << track->Name << "\r\n";
			

			for (int s = 0; s<6; s++) {
				switch(s) {
				case 0:	signal = &track->m_cSignalC[0]; break;
				case 1:	signal = &track->m_cSignalC[1]; break;
				case 2:	signal = &track->m_cSignalN[0]; break;
				case 3:	signal = &track->m_cSignalN[1]; break;
				case 4:	signal = &track->m_cSignalR[0]; break;
				case 5:	signal = &track->m_cSignalR[1]; break;
				} // switch(s)

				if ((signal->m_nShape & B_NOROUTE_ALL) != 0 ) {
					continue;
				}
				
				if (signal->isValid() && ((signal->m_nType < _OCALLON ))  // ORG if (signal->isValid() && ((signal->m_nType < 4)||(signal->m_nType == 8))
					&& (!m_bOneRoute || signal->GetName() == m_BtnName)
					) {
					_FirstSignal = signal;
					m_iDirection = signal->m_nLR + 1;
					sigtype = signal->m_nType;

					if (m_iDirection == UP) {
						os << "UP ";
						Route.ButtonName = track->m_cButton[0].Name;
						if (Route.ButtonName == "") Route.ButtonName = track->m_cButton[1].Name;
					} else {
						os << "DN ";
						Route.ButtonName = track->m_cButton[1].Name;
						if (Route.ButtonName == "") Route.ButtonName = track->m_cButton[0].Name;
					}

					if ( giCallOnCk == 2 ) {       // Home 이나 Outer 를 한번 거쳤을경우 
						if		( sigtype == _HOME ) signal->m_nType = _CALLON;
						else if (sigtype == _OHOME ) signal->m_nType = _OCALLON;
						sigtype = signal->m_nType;
					} 

					if ( giCallOnCk == 4 ) {       // Home 이나 Outer 를 한번 거쳤을경우 
						if		( sigtype == _HOME ) signal->m_nType = _CALLON;
						else if (sigtype == _OHOME ) signal->m_nType = _OCALLON;
						sigtype = signal->m_nType;
					} 

					if ( giCallOnCk == 6 ) {       // Home 이나 Outer 를 한번 거쳤을경우 
						if (sigtype == _START ) signal->m_nType = _OCALLON;
						sigtype = signal->m_nType;
					} 

					if ( giShuntCk == 2 ) {       // Home 이나 Outer 를 한번 거쳤을경우 
						if		( sigtype == _HOME ) signal->m_nType = _SHUNT;
						sigtype = signal->m_nType;
					} 
					
					if ( giShuntCk == 4 ) {       // Home 이나 Outer 를 한번 거쳤을경우 
						if	( sigtype == _START || sigtype == _ISTART ){
							if ( signal->m_nLamp == 5 ) signal->m_nType = _GSHUNT;
							else
							{
								if (sigtype == _START)
									signal->m_nType = _SHUNT;
								else
									signal->m_nType = _ISHUNT;
							}
						}
						sigtype = signal->m_nType;
					} 

					os << pcSigTypeStr[sigtype];
					os << " " << signal->Name;
					os << "\r\n";

					NextTrack = TrackFind(track, s/2+1, cnr);
					
					for ( int k = 0 ; k < 50 ; k++ ){
						gstrDestSignal[k] = "";
						giDestSignalCnt[k] = 0;
					}

					giDestSignal = 0;  // 한신호기의 도착점의 갯수를 나타내는 카운터 
					_giOverLapCnt = 0;  // 오버랩 궤도의 갯수를 나타내는 m_iOverLapCnt 를 초기화한다. 

					switch ( sigtype ) {
					case _TOKENLESSG :  TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start TGB Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										Route.Init();
										Block.Init();
										Parse(track,s/2+1,os,PSMODE12);
										break;
					case _TOKENLESSC :  TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start TCB Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										Route.Init();
										Block.Init();
										Parse(track,s/2+1,os,PSMODE10);
										break;
					case _OG :  TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start OG Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										Route.Init();
										Block.Init();
										Parse(track,s/2+1,os,PSMODE14);
										break;
					case _OC :  TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start OC Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										Route.Init();
										Block.Init();
										Parse(track,s/2+1,os,PSMODE13);
										break;

					}


					if (NextTrack) {

						switch ( sigtype ) {
						case _HOME :	
										if ( signal->m_nLamp == 0 || signal->m_nLamp == 2 ) {
											if ( giCallOnCk == 0 ) giCallOnCk = 1;        // CALLON 을 만들기 위해 체크 하는 로직 
											else if ( giCallOnCk == 2 ) giCallOnCk = 0;   // CALLON 을 만들기 위해 체크 하는 로직 
										} else if ( signal->m_nLamp == 1 || signal->m_nLamp == 3 ) {
											if ( giShuntCk == 0 ) giShuntCk = 1;        // SHUNT 을 만들기 위해 체크 하는 로직 
											else if ( giShuntCk == 2 ) giShuntCk = 0;   // SHUNT 을 만들기 위해 체크 하는 로직 										
										} else if ( signal->m_nLamp == 4  ) {
											if ( giCallOnCk == 0 ) giCallOnCk = 3;        // CALLON 을 만들기 위해 체크 하는 로직 
											else if ( giCallOnCk == 4 ) giCallOnCk = 0;   // CALLON 을 만들기 위해 체크 하는 로직 
										}
										Route.Init();
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start HOME Signal  :  %s \n" , signal->Name);
										TRACE ("-------------------------------------------\n\n");
 										Parse(track,s/2+1,os,PSMODE3);
										break;
						case _OHOME :	if ( signal->m_nLamp != 5 ) { // 램프타입 5 RYG 에는 SY (아우터 콜온이 없다)
											if ( giCallOnCk == 0 ) giCallOnCk = 1;	    // OCALLON 을 만들기 위해 체크 하는 로직 
											else if ( giCallOnCk == 2 ) giCallOnCk = 0;   // OCALLON 을 만들기 위해 체크 하는 로직 
										}
										Route.Init();
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start OUTER HOME Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										if ( signal->m_nLamp == 2 ) break;
										Parse(track,s/2+1,os,PSMODE3);
										break;
						case _START : 	
										giStartCk = 0;
										Is_IStartAtFirst = 0;
										if ( signal->m_nLamp == 2 || signal->m_nLamp == 3 || signal->m_nLamp == 4 || signal->m_nLamp == 5 /*|| signal->m_nLamp == 6*/) {
											if ( giShuntCk == 0 ) giShuntCk = 3;        // SHUNT 을 만들기 위해 체크 하는 로직 
											else if ( giShuntCk == 3 ) giShuntCk = 0;   // SHUNT 을 만들기 위해 체크 하는 로직 										
										}

										if ( signal->m_nLamp == 6 ) {
											if ( giCallOnCk == 0 ) giCallOnCk = 5;	    // OCALLON 을 만들기 위해 체크 하는 로직 
											else if ( giCallOnCk ==6  ) giCallOnCk = 0;   // OCALLON 을 만들기 위해 체크 하는 로직 
										}
										
										Route.Init();
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start START Signal : %s \n" , signal->Name );
										TRACE ("\n-------------------------------------------\n");
										Parse(track,s/2+1,os,PSMODE8);	
										break;
						case _ISTART :	if ( signal->m_nLamp == 8 ) iStartCk = 0; 
							            else iStartCk = 1; 
										giStartCk = 1;
										Is_IStartAtFirst = 1;
										Route.Init();
										signal->m_nType = _START;
										if ( signal->m_nLamp == 2 || signal->m_nLamp == 3 || signal->m_nLamp == 4 || signal->m_nLamp == 5 || signal->m_nLamp == 8) {
											if ( giShuntCk == 0 ) giShuntCk = 3;        // SHUNT 을 만들기 위해 체크 하는 로직 
											else if ( giShuntCk == 3 ) giShuntCk = 0;   // SHUNT 을 만들기 위해 체크 하는 로직 										
										}

										// if ( signal->m_nLamp == 6 ) break;

										sigtype = signal->m_nType;
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start ISTART Signal : %s \n" , signal->Name );
										TRACE ("\n-------------------------------------------\n");
										Parse(track,s/2+1,os,PSMODE8);	
										break;
						case _CALLON :  Route.Init();
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start CALLON Signal  :  %s \n" , signal->Name);
										TRACE ("-------------------------------------------\n\n");
										Parse(track,s/2+1,os,PSMODE8);
										break;
						case _SHUNT :   Route.Init();
										giShuntPushCnt = 0;
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start SHUNT Signal  :  %s \n" , signal->Name);
										TRACE ("-------------------------------------------\n\n");
										Parse(track,s/2+1,os,PSMODE8);
										break;
						case _ISHUNT :  iStartCk = 1; 
							            Route.Init();
										signal->m_nType = _SHUNT;
										sigtype = _SHUNT;
										giShuntPushCnt = 0;
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start ISHUNT Signal  :  %s \n" , signal->Name);
										TRACE ("-------------------------------------------\n\n");
										Parse(track,s/2+1,os,PSMODE8); 
										break;
						case _GSHUNT :   Route.Init();
										iGShuntCk = 1;
										signal->m_nType = _SHUNT;
										sigtype = _SHUNT;
										giShuntPushCnt = 0;
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start GSHUNT Signal  :  %s \n" , signal->Name);
										TRACE ("-------------------------------------------\n\n");
										Parse(track,s/2+1,os,PSMODE8); 
										break;
						case _OCALLON : Route.Init();
										TRACE ("ROUTE  INITIAL \n");
										TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start OCALLON Signal  : %s \n", signal->Name);
										TRACE ("-------------------------------------------\n\n");
										Parse(track,s/2+1,os,PSMODE8);
										break;
						case _ASTART :  TRACE ("\n-------------------------------------------\n");
										TRACE ("Parsing Start ASTART Signal : %s \n" , signal->Name );
										TRACE ("-------------------------------------------\n\n");
										break;
						default :		break;
						}

						Route.Init();
						TRACE ("ROUTE  INITIAL \n");
						HandPointFind( track );
						Route.Add(track);
						TRACE("ADD  TRACK13 : %s \n" , track->Name);

						_Depth = 0;
						if ( sigtype == _OHOME && signal->m_nLamp == 2 ) {
						} else {
							Parse(NextTrack,cnr,os,PSMODE0);
						}
					}
					os << ";ROUTES = " << Route.GetRouteCount();
					os << "\r\n";

				} // if

				if ( sigtype == _SHUNT && iStartCk == 1 ) {
					 sigtype = _ISTART;
					signal->m_nType = _ISTART; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					iStartCk = 0;
					giShuntCk = 0;

				} else if ( sigtype == _SHUNT && signal->m_nLamp == 8 ) {
					 sigtype = _ISTART;
					signal->m_nType = _ISTART; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					iStartCk = 0;
					giShuntCk = 0;
				}

				if ( sigtype == _START && iStartCk == 1 ) {
					if ( signal->m_nLamp > 3 ) {
						sigtype = _ISHUNT;
						signal->m_nType = _ISHUNT; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					} else {
						sigtype = _ISTART;
						signal->m_nType = _ISTART; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
						iStartCk = 0;
					}
				}

				if ( sigtype == _SHUNT && iGShuntCk == 1 ) {
						sigtype = _GSHUNT;
						signal->m_nType = _GSHUNT; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
						iGShuntCk = 0;				
				}
				
				if ( sigtype == _CALLON  &&  giCallOnCk == 2) {
					 sigtype = _HOME;
					 signal->m_nType = _HOME; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giCallOnCk = 0;
				}

				if ( sigtype == _CALLON  &&  giCallOnCk == 4) {
					 sigtype = _HOME;
					 signal->m_nType = _HOME; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giCallOnCk = 0;
					 giShuntCk = 1;
				}

				if ( sigtype == _OCALLON &&  giCallOnCk == 2) {
					 sigtype = _OHOME;
					 signal->m_nType = _OHOME; // OCALLON 을 만들기 위해 OHOME 을 임시로 OCALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giCallOnCk = 0;
				}

				if ( sigtype == _OCALLON &&  giCallOnCk == 6) {
					 sigtype = _START;
					 signal->m_nType = _START; // OCALLON 을 만들기 위해 START 을 임시로 OCALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giCallOnCk = 0;
				}

				if ( sigtype == _SHUNT && giShuntCk == 2 ) {
					 sigtype = _HOME;
					 signal->m_nType = _HOME; // CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giShuntCk = 0;
				}

				if ( sigtype == _SHUNT || sigtype ==_GSHUNT&& giShuntCk == 4 ) {
					if ( Is_IStartAtFirst == 1 )
					{
						sigtype = _ISTART;
						signal->m_nType = _ISTART;
					}
					else
					{
						sigtype = _START;
						signal->m_nType = _START;
					}// CALLON 을 만들기 위해 HOME 을 임시로 CALLON 으로 만들었던것을 원래의 홈으로 되돌려 놓는다. 
					 giShuntCk = 0;
				}

				if ( giCallOnCk == 1 ) {					  // HOME 이나 OHOME 을 만 났을경우 한번더 LOOP 를 돌기 위해 객체를 한칸 뒤로 돌린다.
					s--;
					giCallOnCk = 2;
				}								
				if ( giCallOnCk == 3 ) {					  // HOME 이나 OHOME 을 만 났을경우 한번더 LOOP 를 돌기 위해 객체를 한칸 뒤로 돌린다.
					s--;
					giCallOnCk = 4;
				}								
				if ( giCallOnCk == 5 ) {					  // 아카우라 바이패스역의 58 신호기 가 START 로 정의 했으나 아카우라로 갈때는 OUTER 로 동작한다. 
					s--;
					giCallOnCk = 6;
				}								
				if ( giShuntCk == 1 ) {					  // HOME 이나 OHOME 을 만 났을경우 한번더 LOOP 를 돌기 위해 객체를 한칸 뒤로 돌린다.
					s--;
					giShuntCk = 2;
				}								
				if ( giShuntCk == 3 ) {					  // HOME 이나 OHOME 을 만 났을경우 한번더 LOOP 를 돌기 위해 객체를 한칸 뒤로 돌린다.
					s--;
					giShuntCk = 4;
				}								

			} // for
		}
	} // while
    ////////////////////////////////////////////////////////////////////////////////
	CLevelCross *lcro;
	CString* strLCTrack;
	CString  strLCroTrack;
	for( pos = m_Cross.GetHeadPosition(); pos != NULL; )
	{
        lcro = (CLevelCross*)m_Cross.GetNext( pos );
		os << "UP";
		os << " [CROSS] " << lcro->Name <<" ";
			for ( pos2 = lcro->m_LCTrack.GetHeadPosition() ; pos2 != NULL; )
			{
				strLCTrack = (CString*)lcro->m_LCTrack.GetAt(pos2);
				strLCroTrack = *strLCTrack;
				os << strLCroTrack <<" ";
				lcro->m_LCTrack.GetNext(pos2);
			}
		os << "\r\n";
	}
    /////////////////////////////////////////////////////////////////////////////////
	CString name1a, name, name2a, namew, namew2, strPower, strNameForSDG[50];  // 전철기 출력
	CSwitch *item;
	int		iCountSDG = 0;
	for( pos = m_Switchs.GetHeadPosition(); pos != NULL; )
	{
        item = (CSwitch*)m_Switchs.GetNext( pos );
		if (item->m_bUnique) {
			name1a = item->Name;
			name = GetSwitchName( name1a );
			int n = atoi(name);

			if (n>100) n -= 100;   // 추가, 3자리 선로전환기명 처리
			if (n < 24) os << "DN";
			else os << "DN";

			os << " [POINT] " << name <<" " ;

			strNameForSDG[iCountSDG++] = name;

			strPower = ChangestrPower( item->m_pParent->m_sBranchPower.Mid(1,1) );
			
			os << strPower << " " << item->m_pParent->Name;

			POSITION pos1;
			CSwitch *item1;
			for( pos1 = m_Switchs.GetHeadPosition(); pos1 != NULL; )
			{
				item1 = (CSwitch*)m_Switchs.GetNext( pos1 );
				if (pos != pos1 && item->m_bUnique) {
					namew = item1->Name;
					if (name1a == namew || name2a == namew) {
						item1->m_bUnique = FALSE;
					} else {
						namew2 = GetSwitchName( namew );
						if (name == namew2) {
							name2a = namew;
							os << " " << item1->m_pParent->Name;
							item1->m_bUnique = FALSE;
						}
					}
				} 
			}
			os << "\r\n";
		}
	}

	POSITION pos1;  // 분기궤도 중 진로에 포함되지 않는 선로전환기 명칭의 목록상 미등록 오류 처리.
	CString strTrackB, strSwitchB;
	BOOL find;
	pos = m_pDoc->m_objects.GetHeadPosition();
	while (pos != NULL) {
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack)) && pObj->isValid()) {
			track = (CTrack*)pObj;
			if (track->m_sBranchName != "" && !track->isDoubleSlip()) {
				strTrackB = track->m_sBranchName;
				find = FALSE;
				for( pos1 = m_Switchs.GetHeadPosition(); pos1 != NULL; )
				{
					item = (CSwitch*)m_Switchs.GetNext( pos1 );
					strSwitchB = item->Name;
					if ( strTrackB == strSwitchB ) {
						find = TRUE;						
						break;
					}
				}
				if ( !find )
				{
					name1a = GetSwitchName( strTrackB );

					BOOL bFindOneMoreTime = FALSE;
						
					for(int kk = 0; kk < iCountSDG+1; kk++)
					{
						if ( name1a.Find( strNameForSDG[kk] ) >= 0 )
						{
							bFindOneMoreTime = TRUE;
							break;
						}
					}
						
					if ( !bFindOneMoreTime )
					{
						int n = atoi(name1a);
						if (n>100) n -= 100;
						if (n < 51) os << "DN";
						else os << "DN";
						os << " [POINT] " << name1a << " " << track->Name << "\r\n";
					}
				}
			}
		}
	}
				

	if (m_bOneRoute) return; // scissors를 찾는다.
	pos = m_pDoc->m_objects.GetHeadPosition();

	while (pos != NULL) {
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack))) {
			CTrack* track = (CTrack*)pObj;
			if (track->m_sBranchName != "" && !track->m_bCheck) {
				CTrack *t1, *t2, *t3, *t4;
				int node;
				t1 = TrackFind(track,_N,node);		// N-N
				if (t1 && t1->m_sBranchName != "" && node == 2) {
					t2 = TrackFind(t1,_R,node);	// R-R
					if (t2 && node == 3) {
						t3 = TrackFind(t2,_N,node);		// N-N
						if (t3 && node == 2) {
							t4 = TrackFind(t3,_R,node);	// R-R
							if (t4 == track) {
								os << "SCISSORS:" 
									<< GetSwitchName( track->m_sBranchName )
									<< GetSwitchName( t1->m_sBranchName );
								os << "SCISSORS:" 
									<< GetSwitchName( t1->m_sBranchName )
									<< GetSwitchName( track->m_sBranchName ) ;
									os << "\r\n";

								t1->m_bCheck = t2->m_bCheck = t3->m_bCheck = TRUE;
							}
						}
					}
				}
			}
		}
	}
}


CTrack *StationDB::TrackFind(CTrack* T, int n, int& iNode)   // Track T 의 n(C,N,R) 방향에 접속된 T를 찾음.
{
	CTrack *track = NULL;
	CPoint  p;
	CString strJoinNode;
	CString strTrack;
	char   *pNode = NULL;
	char   *pjoin = NULL; // TRACK JOIN 위해 새로 추가 
	int    iJoinNode;

	iNode = 0;

	switch (n) {
	case _C: if ( T->nC.m_sBlipName != "" ) strJoinNode = T->nC.m_sBlipName;
		     else p = T->c;
		     break;
	case _N: if ( T->nN.m_sBlipName != "" ) strJoinNode = T->nN.m_sBlipName;
		     else p = T->n;
		     break;
	case _R: if ( T->nR.m_sBlipName != "" ) strJoinNode = T->nR.m_sBlipName;
		     else p = T->r;
		     break;
	}

	if ( strJoinNode != "" ) {                                // 연결 트랙이 없다면 
		iJoinNode = strJoinNode.Find( '@' );
		if ( iJoinNode < 0 ) return NULL;
		strTrack = strJoinNode.Left( iJoinNode );
		pNode = (char*)(LPCTSTR)strJoinNode + iJoinNode + 1;
		pjoin = (char*)(LPCTSTR)strJoinNode + iJoinNode + 2;
		if ( *pjoin == 'J' ) {                               // 연결트랙 입력 107T@CJ  C노드에 트랙  조인이다
			m_strTrackJoin[m_iTrackJoinCnt] = T->GetName();
			m_strTrackJoin[m_iTrackJoinCnt+1] = strTrack;
			m_iTrackJoinCnt = m_iTrackJoinCnt+2;
		}
	}

	

	POSITION pos = m_pDoc->m_objects.GetHeadPosition();
	while (pos != NULL) {
		CGraphObject* pObj = (CGraphObject*)m_pDoc->m_objects.GetNext(pos);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack)) && pObj->isValid()) {
			track = (CTrack*)pObj;
			if (T != track) {

				if ( pNode ) {
					if (track->Name == strTrack ) {
						if ( *pNode == 'C' ) iNode = 1;				
						else if ( *pNode == 'N' ) iNode = 2;				
						else if ( *pNode == 'R' ) iNode = 3;
						return track;  // 입력된걸로 리턴 
					}
				} else {
					if (track->c == p) iNode = 1;
					else if (track->n == p) iNode = 2;
					else if (track->r == p) iNode = 3;
					if (iNode) {

						if (																// 동일 분기명칭에서 N,R이 다르게 접속되면 무시함.
							((iJoinNode == 2) || (iJoinNode == 3)) && 
							((iNode == 2) || (iNode == 3)) &&
							(n != iNode)) {
							if ( GetSwitchName(T->m_sBranchName) != GetSwitchName( track->m_sBranchName) )
								return track;         // 찾아서 리턴 
							iNode = 0;
						}
						else return track;
					}
				}
			}
		}
	}
	return NULL;
}

CMyButton *StationDB::ButtonFind(CTrack* T) 
{
	CMyButton *btn;
	btn = &(T->m_cButton[0]);
	if (btn->isValid() && isMatched(btn)) return btn;
	btn = &(T->m_cButton[1]);
	if (btn->isValid() && isMatched(btn)) return btn;
	return NULL;
}

CLevelCross *StationDB::LevelCrossFind(CTrack* T) 
{
	CLevelCross *lcro;
	lcro = &(T->m_cLevelCross);
	if (lcro->isValid()) return lcro;
	return NULL;
}

BOOL StationDB::isMatched(CMyButton *btn) 
{
	if ( btn->m_nLR + 1 != m_iDirection) return FALSE;

	switch ( btn->m_nType ) {
		case _BSTART  : if ( sigtype <= _OCALLON && sigtype != _SHUNT ) return TRUE; break;
		case _BSHUNT  : if ( sigtype == _SHUNT ) return TRUE; break;
		case _BCOMMON : return TRUE; break;
		case _BISTART : return TRUE; break;
		case _BSDG    : return TRUE; break;
	}

	return FALSE;
}

CString StationDB::ChangestrPower(CString strPower) // EPK 가 여러개인 역의 경우 EPK값에 +10 을 해서 구분 한다.
{
	if      ( strPower == "1" ) strPower = "1";
	else if ( strPower == "2" ) strPower = "2"; 
	else if ( strPower == "A" ) strPower = "11"; 
	else if ( strPower == "B" ) strPower = "12"; 
	else if ( strPower == "C" ) strPower = "13"; 
	else if ( strPower == "D" ) strPower = "14"; 
	else if ( strPower == "E" ) strPower = "15"; 
	else if ( strPower == "F" ) strPower = "16"; 
	else if ( strPower == "G" ) strPower = "17"; 
	else if ( strPower == "H" ) strPower = "18"; 
	return strPower;
}

CString StationDB::ChangeChar(CString str , CString str1 , int iIndex , int iLength)
{
	int i,iStrLength;
	CString strFirst, strLast;
	iStrLength = str.GetLength();

/*
	if ( iStrLength > iLength ) {
		str = str.Left(iLength);
		strLast = str.Right(iStrLength-iLength);
	}
*/
	if ( iStrLength < iLength ) {
		for ( i =  iStrLength ; i < iLength ; i++ ) {
			str = str+" ";
		}
	}

	strFirst = str.Left(iIndex-1);
	if (( str.GetLength() - iIndex ) <= 0 ) strLast = "";
	else strLast = str.Mid(iIndex+str1.GetLength()-1,str.GetLength() - iIndex+1);

	str = strFirst + str1 + strLast;
	
	return str;
}
CString MakeFileName(CDocument *pDoc, char *ext) 
{
	CString docname = pDoc->GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, ext );
	docname.ReleaseBuffer( );
	return docname;
}
  
CString MakeTable(CLSDDoc *pDoc, char *tname, char *btn, CObList *path) 
{
	CString docname = pDoc->GetPathName();
	if (docname == "") return docname;
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "ILT" );
	docname.ReleaseBuffer( );

    giHomeCntUP		= 0;
    giHomeCntDN		= 0;

	if (tname && *tname) {
		StationDB stdb(pDoc, TRUE, tname, btn, path);
		ostrstream os;
		stdb.RootParse( os );
		CString str = os.str();
		os.rdbuf()->freeze( 0 );
		return str;
	} else {
		StationDB stdb(pDoc);
		ofstream os(docname, ios::binary);
		stdb.RootParse( os );
	}

	return docname;
}

int RouteInfo::OutPathList( ostream &os, CString &ttl )
{
	int iCheckFlag = 0;
	int oldY = 0;
	int iIND = 0; 
    POSITION pos = _TrackPathList.GetHeadPosition();
    CPoint *pLastPoint = NULL, *p1 = NULL, p;

	os << ttl;

    while ( pos ) {
        CPoint *pPoint = (CPoint*)_TrackPathList.GetNext( pos );
        if (pPoint->x != 10000) {
            if ( !pLastPoint ) {
				p = *pPoint;
				ChangeMapping( p );
                os << "[" << p.x << "," << p.y << "]";
            } else if ( *pLastPoint != *pPoint ) {
                if ( pLastPoint->y == pPoint->y ) {
                    p1 = pPoint;
                } else {
                    if (p1) {
						p = *p1;
						ChangeMapping( p );
                        os << "[" << p.x << "," << p.y << "]";
                    }
					p = *pPoint;
					ChangeMapping( p );
                    os << "[" << p.x << "," << p.y << "]";
                    p1 = NULL;
                }
            }
            pLastPoint = pPoint;
        } else break;

		if ( iCheckFlag == 0 ) {
			if      ( oldY !=0 && p.y != 0 && oldY < p.y ) iIND = 1;
			else if ( oldY !=0 && p.y != 0 && oldY > p.y ) iIND = -1;
			else if ( p.y == 0 ) iCheckFlag = 1;
			oldY = p.y;
		}
    }

    if (p1) {
		CPoint p = *p1;
		ChangeMapping( p );
        os << "[" << p.x << "," << p.y << "]";
    }
	return iIND;
}

CString RouteInfo::GetSwitchNameFromNR( CString &str ) // Allow Long Switch Name  전철기 명에서 +&N[12] 와 같은경우 문자를 다 제거 하고 12 만 리턴 한다. 
{  
	int iFirst, iLast, iStrLength;
	CString strTemp;

	iStrLength = str.GetLength();
	iFirst = 0;

	for ( int i = 0 ; i < iStrLength ; i ++ ) {
		strTemp = str.Mid(i,1);
		if ( strTemp < '0' || strTemp > '9' ) {
			iLast = i;
			break;
		}
		if ( i == iStrLength ) iLast = iStrLength;
	}
	return str.Mid(iFirst,iLast);
}



	