//=======================================================
//==              TableDoc.cpp
//=======================================================
//	파 일 명 :  TableDoc.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  연동도표를 생성한다.
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "TableDoc.h"
#include "TablView.h"
#include <direct.h>  // _mkdir 이용 위해 헤더 추가..
#include "TableRow.h"
#include "../include/parser.h"
#include "LSDDoc.h"
#include "define.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableDoc

IMPLEMENT_DYNCREATE(CTableDoc, CDocument)

//=======================================================
//
//  함 수 명 :  CTableDoc()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CTableDoc::CTableDoc()
{
//	m_pBitmap.LoadBitmap(IDB_LGLOGO);
//	m_pBitmap.LoadBitmap(IDB_SIGN5);
//	m_pBitmap3.LoadBitmap(IDB_SIGN4);
}

//=======================================================
//
//  함 수 명 :  OnNewDocument
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  새로운 문서작성시 처리 
//
//=======================================================
BOOL CTableDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

//=======================================================
//
//  함 수 명 :  ~CTableDoc
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CTableDoc::~CTableDoc()
{
	Purge();
//	m_pBitmap.DeleteObject();
//	m_pBitmap3.DeleteObject();
}

BEGIN_MESSAGE_MAP(CTableDoc, CDocument)
	//{{AFX_MSG_MAP(CTableDoc)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableDoc diagnostics

#ifdef _DEBUG
void CTableDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTableDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTableDoc serialization

//=======================================================
//
//  함 수 명 :  Serialize
//  함수출력 :  없음
//  함수입력 :  CArchive& ar
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표를 저장한다.
//
//=======================================================
void CTableDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
		CString str;
		while (ar.ReadString(str)) {
			m_Rows.AddTail((CObject*)new CString(str) );
		}
		CFile* f = ar.GetFile();

		SetPathName(f->GetFilePath());
		CreateTable();
	ReadModFile();
	ModifyTable();

		Export();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTableDoc commands

CString CTableDoc::GetProgramVersion()
{
    HRSRC hRsrc = FindResource(NULL, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	CString strVersion;
	strVersion = "0.0.0.0";
    if (hRsrc != NULL)
    {
        HGLOBAL hGlobalMemory = LoadResource(NULL, hRsrc);
        if (hGlobalMemory != NULL)
        {
            void *pVersionResouece = LockResource(hGlobalMemory); 
            void *pVersion;
            UINT uLength;
            // 아래줄에 041204B0는 리소스 파일(*.rc)에서 가져옴.
            // 프로젝트 리소스 파일을 참고하세요(어느부분 참고인지는 밑에 나와있음)
            if( VerQueryValue(pVersionResouece, "StringFileInfo\\040904b0\\FileVersion", &pVersion, &uLength) != 0 )
            {
               // 1, 0, 0, 1로 되어 있는 부분에서 숫자 부분만 가져옴.
                int anVersion[4] = {0,};
                char* pcTemp = strtok( (char *)pVersion, "," );
                for(int inxVersion=0; inxVersion<4; inxVersion++)
                {
                    if(pcTemp  != NULL)
                    {
                        anVersion[inxVersion] = atoi(pcTemp );
                        if(inxVersion != 3)
                        {
                            pcTemp = strtok(NULL, ",");
                        }
                    }
                }
                strVersion.Format("%d.%d.%d.%d", anVersion[0], anVersion[1], anVersion[2], anVersion[3]);
                //AfxMessageBox(strVersion);
            }
            FreeResource(hGlobalMemory);
        }
    }
	return strVersion;
}

//=======================================================
//
//  함 수 명 :  CreateTable
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표를 생성한다.
//
//=======================================================
void CTableDoc::CreateTable()
{
	POSITION pos;
	int i = 0;
	CString *str;
	CString Common, Preroute ,Aspect , Route, Path;
	CRow *row, *pLastRow = NULL, *pLastRow1 = NULL;

	CMyStrList SCISSORS;

	for( pos = m_Rows.GetHeadPosition(); pos != NULL; )
	{
		i++;
        str = (CString*)m_Rows.GetNext( pos );
		if (*str == "") continue;
		if (str->GetAt(0) == '@') {
			if (str->Left(7) == "@TRACK=") {
				char *p = (char*)(LPCTSTR)*str;
				CString tname = p + 7;												// omani str 에서 @TRACK= 을 제외한 트랙 이름만을 얻는다. 
				if ( tname.Find("@") > 0 ) {
					tname = tname.Left(tname.Find("@"));
				}
				m_StationObject.AddTrack( tname );									// omani 트랙 리스트에 트랙을 추가 한다. 
			}
			else if (str->Left(6) == "@LAMP=") {
				char *p = (char*)(LPCTSTR)*str;
				CString lname = p + 6;
				m_StationObject.AddLamp( lname );
			}
			else if (str->Left(8) == "@SIGNAL:") {									// omani 추가 
				CString sname;
				char *p = (char*)(LPCTSTR)*str;
				int iS1Pos,iS2Pos;
				sname = p;
				iS1Pos = sname.Find("=",9);
				iS1Pos++;
				iS2Pos = sname.Find(",",iS1Pos);
				iS2Pos = iS2Pos - iS1Pos;
				sname = sname.Mid(iS1Pos,iS2Pos);
				m_StationObject.AddSignal( sname );									// omani 추가 
			}
		}
		else if (str->GetAt(0) != ';') {
			CString token = str->Left(3);
			if (token == "UP " || token == "DN ") {
				if (str->GetAt(3) == '[') {
					row = new CRow(*str, this);
					if (row->isValid()) {
						row->m_cUD = token.GetAt(0);
						SortInsert(row);
					}
					else delete row;
				}
				else {
					Common = *str;													// omani Common 에 첫번째 줄을 넣는다. 
					Preroute = "";
				}
			}
			else {
                if ((str->Left(6) == "[PATH]")/* && pLastRow */) {
					Path = *str;
                    continue;
                }
				else if (str->Left(8) == "[ASPECT]") {
					Aspect = *str;
					continue;
				}
				else if (str->Left(7) == "[ROUTE]") { 
					Route = * str;
					continue;
				} 
				else if (str->Left(7) == "[BLOCK]") { 
					Route = * str;
					continue;
				} 
				else if (str->Left(10) == "[PREROUTE]") {
					Preroute = *str;
					continue;
				}
				else if (str->Left(11) == "[HOLDROUTE]") {
					Preroute = *str;
					continue;
				}
				else if (str->Left(9) == "SCISSORS:") {
					CString s = str->Right(4);
					SCISSORS.AddTail(s);
					continue;
				} 
				else if (str->Left(10) == "[ENDROUTE]") {
				
                pLastRow = NULL;

				int n = 0;
				CString temp;
				CString s;
		
				CRow *cYudo = NULL;
				CObList cObTTB;
				str = &Route;

				row = new CRow(Common, *str, Preroute, Aspect , Path ,cYudo, &cObTTB, this);
				if (row->isValid()) {
					SortInsert(row);
					if (cYudo) SortInsert(cYudo);
//                    pLastRow = row;
				}
				else delete row;
				}//(str->Left(9) == "[ENDROUTE]") {
			}
		} else {
			if (str->Left(9) == ";[INFO01]" ) 
			m_StationObject.m_StationName = str->Mid(9);
			if (str->Left(9) == ";[INFO02]" ) 
			m_StationObject.m_Edit01 = str->Mid(9);
			if (str->Left(9) == ";[INFO03]" ) 
			m_StationObject.m_Edit02 = str->Mid(9);
			if (str->Left(9) == ";[INFO04]" ) 
			m_StationObject.m_Edit03 = str->Mid(9);
			if (str->Left(9) == ";[INFO05]" ) 
			m_StationObject.m_Edit04 = str->Mid(9);
			if (str->Left(9) == ";[INFO06]" ) 
			m_StationObject.m_Edit05 = str->Mid(9);
			if (str->Left(9) == ";[INFO07]" ) 
			m_StationObject.m_Edit06 = str->Mid(9);
			if (str->Left(9) == ";[INFO08]" ) 
			m_StationObject.m_Edit07 = str->Mid(9);
			if (str->Left(9) == ";[INFO09]" ) 
			m_StationObject.m_Edit08 = str->Mid(9);
			if (str->Left(9) == ";[INFO10]" ) 
			m_StationObject.m_Edit09 = str->Mid(9);
			if (str->Left(9) == ";[INFO11]" ) 
			m_StationObject.m_Edit10 = str->Mid(9);
			if (str->Left(9) == ";[INFO12]" ) 
			m_StationObject.m_Edit11 = str->Mid(9);
			if (str->Left(9) == ";[INFO13]" ) 
			m_StationObject.m_Edit12 = str->Mid(9);
			if (str->Left(9) == ";[INFO14]" ) 
			m_StationObject.m_Edit13 = str->Mid(9);
			if (str->Left(9) == ";[INFO15]" ) 
			m_StationObject.m_Edit14 = str->Mid(9);
			if (str->Left(9) == ";[INFO16]" ) 
			m_StationObject.m_Edit15 = str->Mid(9);
			if (str->Left(9) == ";[INFO17]" ) 
			m_StationObject.m_Edit16 = str->Mid(9);
			if (str->Left(9) == ";[INFO18]" ) 
			m_StationObject.m_Edit17 = str->Mid(9);
			if (str->Left(9) == ";[INFO19]" ) 
			m_StationObject.m_Edit18 = str->Mid(9);
			if (str->Left(9) == ";[INFO20]" ) 
			m_StationObject.m_Edit19 = str->Mid(9);

		}

	}

	RemoveDuplicateRoute();
	ReadModFile();
	ModifyTable();
	InsertScissors(SCISSORS);
	CreateRouteCnt();
	CreateSignalLock();
	CreateSwitchTable();
	CreateCrossTable();	
	//연동도표가 완전히 생성된후 건널목과 블록 노트 생생
	/*
	CString strTemp;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		if ( row->m_Type == "ISTART" ) {
			row->m_LockSwitch.Purge();
//			row->m_LockSignal.Purge();
			row->m_Route.Purge();
			strTemp = *(CString *) row->m_TrackClear.GetAtIndex(0);
			row->m_Track.Purge();
			row->m_Occupy.Purge();
			row->m_TcOCCSeq.Purge();
			row->m_TcOCC = "";
			row->m_TrackClear.Purge();
			row->m_TrackClear.AddTail( strTemp );
		}
		CString *pstrTemp;
		CString strTemp;
		CString strTemp2;
		if (  row->m_Type == "ISHUNT") {
			pstrTemp = row->m_TcOCCSeq.GetAtIndex(0);
			strTemp2 = * pstrTemp;
			row->m_LockSwitch.Purge();
//			row->m_LockSignal.Purge();
			strTemp = *(CString *) row->m_TrackClear.GetAtIndex(0);
			row->m_Route.Purge();
			row->m_Track.Purge();
			row->m_Occupy.Purge();
			row->m_Occupy.AddTail(strTemp2);
			row->m_TcOCCSeq.Purge();
			row->m_TcOCC = "";
			row->m_TrackClear.Purge();
			row->m_TrackClear.AddTail( strTemp );
		}

	}
	*/

}

CString MakeFileName(CDocument *pDoc, char *ext);


//=======================================================
//
//  함 수 명 :  ReadModFile
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자가 수정한 내용을 읽어서 처리한다.
//
//=======================================================
void CTableDoc::ReadModFile()
{
	CString filename = MakeFileName(this, "AMD");

	CFile cfile;
	if (cfile.Open(filename,CFile::modeRead)) {
		char buf[512];
		CArchive ar( &cfile, CArchive::load, 512, buf );
		CString str;
		while (ar.ReadString(str)) {
			if (str != "" && str.GetAt(0) == '@') {
				m_Modifier.AddTail(str);
			}
		}
		ar.Close();
		cfile.Close();
	}
}

//=======================================================
//
//  함 수 명 :  SortInsert
//  함수출력 :  없음
//  함수입력 :  CRow *row
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  행을 소트하여 입력한다.
//
//=======================================================
void CTableDoc::SortInsert(CRow *row)
{

	POSITION pos, pos1;
	CRow *row1;
	// 바이패스 스테이션은  58:52(M),58:52(C),58:56(M) 순서로 정렬 되어야 한다.
	if ( m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
		if ( row->m_No == "58:56(M)" ) {
			for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
				pos = pos1;
				row1 = (CRow*)m_Items.GetNext( pos1 );

				if ( row1->m_No == "58:52(M)") {
					m_Items.InsertAfter( pos, row );
					return; // 바이패스역의 신호기 순서 때문
				}
			}
		}
	}

	if ( row->m_No == "58:52(C)" ) {
		for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
			pos = pos1;
			row1 = (CRow*)m_Items.GetNext( pos1 );

			if ( row1->m_No == "58:52(M)") {
				m_Items.InsertAfter( pos, row );
				return; // 바이패스역의 신호기 순서 때문
			}
		}
	}

	if ( row->m_No == "58:52(C)-1" ) {
		for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
			pos = pos1;
			row1 = (CRow*)m_Items.GetNext( pos1 );

			if ( row1->m_No == "58:52(C)") {
				m_Items.InsertAfter( pos, row );
				return; // 바이패스역의 신호기 순서 때문
			}
		}
	}
	if ( row->m_No == "60:58R(M)-1" ) {
		return;
	}
	if ( row->m_No == "58:52(M)-1" ) {
		return;
		for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
			pos = pos1;
			row1 = (CRow*)m_Items.GetNext( pos1 );

			if ( row1->m_No == "58:52(M)") {
				m_Items.InsertAfter( pos, row );
				return; // 바이패스역의 신호기 순서 때문
			}
		}
	}

	for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
		pos = pos1;
		row1 = (CRow*)m_Items.GetNext( pos1 );
		if (*row < *row1) {
			m_Items.InsertBefore( pos, row );
			return;
		}
	}

	m_Items.AddTail( row );
}


int HIDX(CString &str);

void CTableDoc::AllocArea(PageInfo *page)
{
	POSITION pos, pos1;
	CRow *row, *row1;

	CObList& items = m_Items;
	POSITION pos0;
	pos0 = NULL;

	int sz = page->hy;
	pos0 = items.FindIndex(page->sy);
	if (sz && pos0) {
		int i;
		for( pos = pos0, i = 0 ; pos != NULL && i<sz; i++) {
			row = (CRow*)items.GetNext( pos );
			row->ResetView();
		}
	}
	return;

	if (sz && pos0) {
		int i,j;
		for( pos = pos0, i = 0 ; pos != NULL && i<sz; i++) {
			row = (CRow*)items.GetNext( pos );
			row->ResetView();
		}
		for( pos = pos0, i = 0 ; pos != NULL && i<sz; i++) {
			row = (CRow*)items.GetNext( pos );
			CRow *pRow = row;
			if (pos && row->m_cSignal.isValid()) {
				pos1 = pos;
				CString name = row->m_Signal;
				CMyStrList list;
				list = row->m_Approach;
				j = i+1;
				while (pos1 && j++<sz) {
					row1 = (CRow*)items.GetNext( pos1 );
					if (name == row1->m_Signal) {
						if (list == row1->m_Approach) {
							pRow->m_cHold.IncHeight();
							row1->m_cHold.SetView(FALSE);
						}
						else {
							list = row1->m_Approach;
							pRow = row1;
						}
					}
					else break;
				}
			}
		}
		for( pos = pos0, i = 0 ; pos != NULL && i<sz; i++) {
			row = (CRow*)items.GetNext( pos );
			if (pos && row->m_cButton.isValid()) {
				pos1 = pos;
				CString name = row->m_Button;
				j = i+1;
				while (pos1 && j++<sz) {
					row1 = (CRow*)items.GetNext( pos1 );
					if (name == row1->m_Button) {
						row->m_cButton.IncHeight();
						row1->m_cButton.SetView(FALSE);
					}
					else break;
				}
			}
		}
		for( pos = pos0, i = 0 ; pos != NULL && i<sz; i++) {
			row = (CRow*)items.GetNext( pos );
			if (pos && row->m_cButton.isValid()) {
				pos1 = pos;
				CString name = row->m_RouteName;
				j = i+1;
				while (pos1 && j++<sz) {
					row1 = (CRow*)items.GetNext( pos1 );
					if (name == row1->m_RouteName) {
						row->m_cName.IncHeight();
						row1->m_cName.SetView(FALSE);
					}
					else break;
				}
			}
		}

	}
}

CString GetSwitchNameFromNR( CString & str );
/*
{  // Allow Long Switch Name  전철기 명에서 +&N[12] 와 같은경우 문자를 다 제거 하고 12 만 리턴 한다. 

	int iFirst, iLast;
	iFirst = str.Find("[",0);
	iLast  = str.Find("]",0);
	if ( iFirst > -1 ) iFirst = iFirst + 1;
	if ( ( iLast > -1)  && ( iFirst < iLast ) ) iLast = iLast - iFirst; 
	return str.Mid(iFirst,iLast);
}
*/
void CTableDoc::CreateSwitchTable()
{
	POSITION pos, pos1, pos2;
	CRow *row, *row1;
	CString strTemp1,strTemp2, * strTemp3;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		strTemp1 = row->m_Type;
		if ( strTemp1 != "POINT" ) continue;

			for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
				row1 = (CRow*)m_Items.GetNext( pos1 );
				for( pos2 = row1->m_LockSwitch.GetTailPosition(); pos2 != NULL; )
				{
					// 전철기 방향 검색 
					strTemp3 = (CString*)row1->m_LockSwitch.GetPrev( pos2 );
					if ( GetSwitchNameFromNR(  * strTemp3 )  == row->m_Signal ) {
	if ( row1->m_No == "A54:54(C)" ) {
		if ( row->m_No == "54:18(C)" ) {
			int a;
			a++;
		}		
	}
						row->m_LockSignal.CheckAdd( row1->m_No );
					}
				}
			}
	}

}

void CTableDoc::CreateCrossTable()
{
	POSITION pos, pos1;
	CRow *row, *row1;
	CString strTemp1,strTemp2,strTemp3;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		strTemp1 = row->m_Type;
		if ( strTemp1 != "CROSS" ) continue;

			for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
				row1 = (CRow*)m_Items.GetNext( pos1 );
				if ( row1->m_LevelCross  == row->m_Signal ) {
					if ( row1->m_Type != "CALLON" ) 
	if ( row1->m_No == "A54:54(C)" ) {
		if ( row->m_No == "54:18(C)" ) {
			int a;
			a++;
		}		
	}
					row->m_LockSignal.CheckAdd( row1->m_No );
				}
			}
	}

}

void CTableDoc::CreateRouteCnt()
{
	POSITION pos, pos1;
	CRow *row, *row1;
	CString strTemp1,strTemp2;
	int     iTemp1,iTemp2,iRouteCnt;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		if ( row->m_RouteName == "" ) continue;
		iRouteCnt = 0;
		strTemp1 = row->m_No;
		iTemp1 = strTemp1.Find(")",1);
		strTemp1 = strTemp1.Mid(0,iTemp1);
		if (row->isSignal() && row->m_RouteName.GetAt(0) != '-') {
			for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
				row1 = (CRow*)m_Items.GetNext( pos1 );
				strTemp2 = row1->m_No;
				iTemp2 = strTemp2.Find(")",1);
				strTemp2 = strTemp2.Mid(0,iTemp1);
				// 동일진로명 검색
				if ( strTemp1 == strTemp2 ) {
					iRouteCnt ++;
				}

			}
		}
		strTemp2 = row->m_No;
		if ( iRouteCnt > 1 && (strTemp2.Find(")-",0) == -1 ) ) strTemp1.Format("%d",iRouteCnt);
		else strTemp1 = "";
		row->m_RouteCnt = strTemp1;
	}
}

//=======================================================
//
//  function name    :  CreateSignalLock
//  return value     :  none
//  input value      :  none
//  creator          :  Do.Young.Kim
//  create date      :  2004-10-11
//  function version :  2.01
//  remark           :  create route prohibit
//
//=======================================================
void CTableDoc::CreateSignalLock()
{
	POSITION pos, pos1;
	CRow *row, *row1;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		if ( row->m_Type == "ISTART" ) continue;
		if ( row->m_Type == "ISHUNT" ) continue;
		if ( row->m_RouteName == "" ) continue;
		if (row->isSignal() && row->m_RouteName.GetAt(0) != '-') {
			for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
				row1 = (CRow*)m_Items.GetNext( pos1 );
				if ( row1->m_Type == "ISTART" ) continue;
				if ( row1->m_Type == "ISHUNT" ) continue;
				if (row != row1 && row->m_Signal != row1->m_Signal) {
					if (row1->m_RouteName != "" && row1->m_RouteName.GetAt(0) != '-')
						row->SwitchInclude( row1 );
				}
			}
		}
	}
}

//=======================================================
//
//  함 수 명 :  ModifyTable
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  읽어온 수정정보를 반영한다.
//
//=======================================================
void CTableDoc::ModifyTable()
{
	POSITION pos, pos1;
	CString* mod;
	CRow *row;
	for( pos = m_Modifier.GetHeadPosition(); pos != NULL; ) {
		mod = (CString*)m_Modifier.GetNext( pos );
		for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {
			row = (CRow*)m_Items.GetNext( pos1 );
			if (row->Modify( *mod )) break;
		}
	}
}

//=======================================================
//
//  함 수 명 :  UpdateModFile
//  함수출력 :  BOOL
//  함수입력 :  CString &dif
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자가 수정한내용을 현재의 연동도표에 반영한다.
//
//=======================================================
BOOL CTableDoc::UpdateModFile(CString &dif)
{
	int loc = dif.Find('$');
	int cr = dif.Find('\n');
	if (cr >=0) dif.SetAt(cr,' ');

	CString sub;
	if (loc >= 0) sub = dif.Left(loc);
	else sub = dif;
	CString filename = MakeFileName(this, "AMD");
	CFile cfile;
	if (cfile.Open(filename,CFile::modeCreate | CFile::modeWrite)) {
		BOOL find = FALSE;
		CString str;
		POSITION pos, pos1;
		CString* mod;
		for( pos = m_Modifier.GetHeadPosition(); pos != NULL; ) {
			pos1 = pos;
			mod = (CString*)m_Modifier.GetNext( pos );
			if (!find && mod->Find(sub) >=0 ) {
				if (loc > 0) {
					CString *newstr = new CString(dif);
					m_Modifier.SetAt(pos1,(CObject*)newstr);
					delete mod;
				}
				else {
					m_Modifier.RemoveAt(pos1);
					delete mod;
				}
				find = TRUE;
				str = dif;
			}
			else str = *mod;
			if (str.Find('$') > 0) {
				str += "\n";
				cfile.Write(str, str.GetLength());
			}
		}
		if (!find) {
			m_Modifier.AddTail(dif);
			dif += "\n";
			cfile.Write(dif, dif.GetLength());
		}
		cfile.Close();
		return TRUE;
	}
	return FALSE;
}

typedef struct {
	char pSignal[32];
	char pDest[32];
	char pTrack[32];
} RouteConfigType;

RouteConfigType *_RouteConf = NULL;

void CTableDoc::RemoveDuplicateRoute()
{
	CString docname = GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "RTS" );
	docname.ReleaseBuffer( );
	FILE *pFile = fopen( docname, "rt" );
	if ( pFile ) {
		_RouteConf = new RouteConfigType[100];
		int i = 0;
		char bf[128];
		CString token;
		while ( !feof(pFile) && i < 99 ) {
			fgets( bf, 127, pFile );
			if ( bf[0] != ';') {
				CParser line( bf );
				line.GetToken( token );
				strcpy( _RouteConf[ i ].pSignal, token );
				line.GetToken( token );
				strcpy( _RouteConf[ i ].pDest, token );
				line.GetToken( token );
				strcpy( _RouteConf[ i ].pTrack, token );
				i++;
			}
		}
		_RouteConf[ i ].pSignal[0] = 0;
		fclose( pFile );
	}
	POSITION pos, pos1;
	CRow *row;
	CString ID, oldID;
	CObList select;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		row = (CRow*)m_Items.GetNext( pos );
		ID = row->MakeID();
		if (ID == oldID) {
			select.AddTail((CObject*)row);
		}
		else {
			SelectOneRow( &select );
			select.AddTail((CObject*)row);
		}
		oldID = ID;
	}
	SelectOneRow( &select );
	for( pos = m_Items.GetHeadPosition(); pos != NULL; ) {
		pos1 = pos;
		row = (CRow*)m_Items.GetNext( pos );
		if (!row->isValid()) {
			m_Items.RemoveAt(pos1);
			delete row;
		}
	}
	// Remove Route Select Info
	if (_RouteConf) {
		delete [] _RouteConf;
		_RouteConf = NULL;
	}
}

void SelectOnRouteByTrackReferance( CObList *sel )
{
	if (!_RouteConf) return;

	CString stritem, strTrack;
	POSITION pos;

	pos = sel->GetHeadPosition();
	while(pos != NULL) {
		RouteConfigType *pRouteInfo = _RouteConf;
		CRow *r = (CRow*)sel->GetNext( pos );
		if (r->isValid()) {
			while ( pRouteInfo->pSignal[0] ) {
				if ( pRouteInfo->pSignal == r->m_Signal && pRouteInfo->pDest == r->m_Button ) {
					strTrack = pRouteInfo->pTrack;
					if ( r->CheckReferanceItem( strTrack, CRow::FREEROUTE, TRUE ) == FALSE ) {
						r->m_bValid = FALSE;
					}
				}
				pRouteInfo++;
			}
		}
	}
}

void CTableDoc::SelectOneRow(CObList *sel)
{
	if (sel->GetCount()>1) {	// 중복진로 있음
		SelectOnRouteByTrackReferance( sel );		/// 지정된 궤도를 포함하는 진로 선택
//		SelectOneRouteBy(sel,CRow::_HOME);		    // HOME    신호기 경합 검색
//		SelectOneRouteBy(sel,CRow::_START);			// STARTER 신호기 경합 검색
//		SelectOneRouteBy(sel,CRow::_ASTART);		// ASTART 신호기 경합 검색
//		SelectOneRouteBy(sel,CRow::_SHUNT);			// 입환 표지 경합 검색
		SelectOneRouteByTrackAndSwitchNR(sel);		// 궤도 수, 전철기 수, 정위 전철기수 검색
	}
	sel->RemoveAll();
}

BOOL CTableDoc::SelectOneRouteByTrackAndSwitchNR(CObList *sel)
{
	int v, min, max;
	POSITION pos, pos1;
	CRow *pMin, *pMax;

	pMin = pMax = NULL;  // 궤도 숫자가 적은 것을 검색(경합진로가 같은 루트인 경우).
	pos = sel->GetHeadPosition();
	while(pos != NULL) {
		CRow *r = (CRow*)sel->GetNext( pos );
		if (r->isValid()) {
			if (pMin == NULL) pMin = pMax = r;
			else {
				v   = r->m_Route.GetCount();
				min = pMin->m_Route.GetCount();
				max = pMax->m_Route.GetCount();
				if (min > v) pMin = r;
				if (max < v) pMax = r;
			}
		}
	}
	if (pMin == pMax) {

		pos = sel->GetHeadPosition(); // 전철기 숫자가 적은 궤도를 검색(궤도 숫자가 같은 루트인 경우)
		pMin = pMax = NULL;
		while(pos != NULL) {
			CRow *r = (CRow*)sel->GetNext( pos );
			if (r->isValid()) {
				if (pMin == NULL) pMin = pMax = r;
				else {
					v = r->m_LockSwitch.GetCount();
					min = pMin->m_LockSwitch.GetCount();
					max = pMax->m_LockSwitch.GetCount();
					if (min > v) pMin = r;
					if (max < v) pMax = r;
				}
			}
		}
		if (pMin == pMax) {

			pos = sel->GetHeadPosition(); // 정위 전철기가 많은 궤도를 검색(전철기 숫자가 같은 루트인 경우)
			pMin = pMax = NULL;
			min  = max  = 0;
			while(pos != NULL) {
				CRow *r = (CRow*)sel->GetNext( pos );
				if (r->isValid()) {
					v = 0;
					CMyStrList *lockswitch = &(r->m_LockSwitch);
					pos1 = lockswitch->GetHeadPosition();
					while (pos1 != NULL) {
						CString *str = (CString*)lockswitch->GetNext( pos1 );
						if (str->GetAt(0) == 'N') v++;
					}
					if (pMin == NULL) {
						pMin = pMax = r;
						min  = max  = v;
					}
					else {
						if (min > v) { pMin = r; min = v; }
						if (max < v) { pMax = r; max = v; }
					}
				}
			}
			if (min == max) return FALSE;
			else pMin = pMax;
		}
	}
	
	pos = sel->GetHeadPosition(); // 선별된 루트 이외의 궤도 삭제
	while(pos != NULL) {
		CRow *r = (CRow*)sel->GetNext( pos );
		if ((pMin != r) && r->isValid()) r->m_bValid = FALSE; 
	}
	return TRUE;
}

BOOL CTableDoc::SelectOneRouteBy(CObList *sel, CRow::RowSubType type) 
{
	int  n;
	POSITION pos, pos1;
	CRow *row;
	for( pos = sel->GetHeadPosition(); pos != NULL; ) {
		CRow *r = (CRow*)sel->GetNext( pos );
		if (r->isValid()) r->m_nValue = 0;
	}
	for( pos1 = m_Items.GetHeadPosition(); pos1 != NULL; ) {	// 경합진로 검사
		row = (CRow*)m_Items.GetNext( pos1 );					//  ** 자기자신의 진로를 거치는 다른 진로개수를 검사 **
		if (row->isValid() && row->m_nType == type) {
			for( pos = sel->GetHeadPosition(); pos != NULL; ) {
				CRow *r = (CRow*)sel->GetNext( pos );
				if (r->isValid())
					if (!r->RouteCompare(row)) break;
			}
		}
	}
	int min, max;
	BOOL first = TRUE;
	n = min = max = 0;
	for( pos = sel->GetHeadPosition(); pos != NULL; ) {
		CRow *r = (CRow*)sel->GetNext( pos );
		if (r->isValid()) {
			int v = r->m_nValue;
			if (first) {
				min = max = v;
				first = FALSE;
			}
			else {
				if (min>v) min = v;
				else if (max<v) max = v;
			}
			n++;
		}
	}
	if (min == max) {	
		if (n > 1) return FALSE;
		else return TRUE;
	}
	
	n = 0; // 경합진로 개수가 가장적은 진로를 제외한 다른 동일 진로 삭제
	for( pos = sel->GetHeadPosition(); pos != NULL; ) {
		CRow *r = (CRow*)sel->GetNext( pos );
		if (r->isValid()) {
			int v = r->m_nValue;
			if (v != min) r->m_bValid = FALSE;
			else n++;			// 최소단위의 경합진로를 가진 루트 수
		}
	}
	if (n > 1) return FALSE;	// 최소 단위의 경합 진로가 같은 동일 루트가 존재
	else return TRUE;			// 단 하나의 진로만 존재
}

//=======================================================
//
//  함 수 명 :  RemoveDeletedRow
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자가 삭제한 행을 처리한다.
//
//=======================================================
void CTableDoc::RemoveDeletedRow()
{
	POSITION pos, pos1;
	CRow *row;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; )
	{
		pos1 = pos;
        row = (CRow*)m_Items.GetNext( pos );
		if (row->m_RouteName.GetAt(0) == '-') {
			delete row;
			m_Items.RemoveAt(pos1);
		}
	}
}

void CTableDoc::Purge()
{
	POSITION pos;
	CString *item;
	for( pos = m_Rows.GetHeadPosition(); pos != NULL; )
	{
        item = (CString*)m_Rows.GetNext( pos );
		delete item;
	}
	m_Rows.RemoveAll();
	CRow *row;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; )
	{
        row = (CRow*)m_Items.GetNext( pos );
		delete row;
	}
	m_Items.RemoveAll();
	m_Modifier.Purge();
}

void CTableDoc::TableRegen(BOOL bHide)
{
	Purge();
	OnOpenDocument(GetPathName());
	if (bHide) RemoveDeletedRow();
}


void CTableDoc::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);		
}

void CTableDoc::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);		
}


void CTableDoc::InsertScissors(CMyStrList& Sci)
{
	POSITION pos;
	CRow *row;
	for( pos = m_Items.GetHeadPosition(); pos != NULL; )
	{
        row = (CRow*)m_Items.GetNext( pos );
		row->InsertScissors(Sci);
	}
}

#define RPP_FILE_VERSION  "ROUTE INFO FILE VER1.0"

int m_count, End, r;
CString temp, Out[200], ShuntSignal[100];


//=======================================================
//
//  함 수 명 :  BackupIDFFile
//  함수출력 :  없음
//  함수입력 :  CString strFileName
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  IDF 파일을 백업한다. 
//
//=======================================================
void CTableDoc::BackupIDFFile(CString strFileName)
{
	FILE* fp;
	if ( (fp = fopen( strFileName, "r" )) ) {
			char *s;
			int ch = '\\';
			s = strrchr( strFileName, ch );
			CString str = s;
			CString path;
			int len = strlen(strFileName) - strlen(str);
			path = strFileName.Left(len);

			CTime Time;
			CString Day;
			Time = CTime::GetCurrentTime();
			Day = Time.Format("%Y.%m.%d_%H시%M분%S초");

			CString BakDir = path;
			BakDir += "\\IDF File BackUp";
			
			char Backup[80];
			strcpy( Backup, BakDir );
			strcat( Backup, "\\IDF_" );
			strcat( Backup, Day );
			strcat( Backup, ".bak" );

			_mkdir ( BakDir );
			FILE *fpBak = fopen ( Backup, "w" );
			char bf[1024];
			while( (fgets( bf, 256, fp )) != NULL ) {
				fputs ( bf, fpBak );
			}

			fclose(fp);
			fclose(fpBak);
			r = m_count = End = 0;
	}
}

//=======================================================
//
//  함 수 명 :  MakeIDFFile
//  함수출력 :  없음 
//  함수입력 :  CString strFileName
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  IDF 파일을 작성한다.
//
//=======================================================
void CTableDoc::MakeIDFFile(CString strFileName)
{
	CFile			f;
	CFileException	e;
	POSITION		pos;
	CRow			*row;
	CString			temp1,temp2,rowNext,rowSignal,str;
	CString			strVersion;

	strVersion = GetProgramVersion();
	r = m_count = End = 0;

	if( f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) ) {
		CDumpContext dc( &f );
		CTime    time = CTime::GetCurrentTime();
		CString ttl = ";/////////////////////////////////////////////////////////////////////////"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
		        ttl = ";//         LSIS Interlocking equipment interlocking data file v3.0     //"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
				ttl = ";//                    Do not modify this file                          //"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
           ttl.Format(";//        Create date : %04dY %02dM %02dD %02dH %02dM %02dS                      //",time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
		        ttl = ";/////////////////////////////////////////////////////////////////////////"; dc << ttl << "\r\n";
		        ttl = ";LSD Version : " + strVersion ;dc << ttl << "\r\n";dc << "\r\n";


		dc << "VERSION:{" << __editdev20 << "}\r\n";
		m_StationObject.Sort();
		m_StationObject.Dump( dc );
		dc << "\r\n";

		CString *item;		
		for( pos = m_Rows.GetHeadPosition(); pos != NULL; )
		{
			item = (CString*)m_Rows.GetNext( pos );
			if (*item == "") continue;
			if (item->GetAt(0) == '@') {
				dc << *item << "\r\n";
			}
		}

		dc << "\r\n";

		int nOldType = 0;

		for( pos = m_Items.GetHeadPosition(); pos != NULL; )
		{
			row = (CRow*)m_Items.GetNext( pos );
			if ( nOldType != row->m_nRowType ) {
				if ( row->m_nRowType == CRow::ROWSIGNAL ) {
					dc << "\n; ROUTE TABLE\n\n";
				}
				else if ( row->m_nRowType == CRow::ROWPOINT ) {
					dc << "\n; POINT TABLE\n\n";
				}
				else if ( row->m_nRowType == CRow::ROWCROSSING ) {
					dc << "\n; LC TABLE\n\n";
				}
				else if ( row->m_nRowType == CRow::ROWBLOCKROUTE ) {
					dc << "\n; BLOCK TABLE\n\n";
				}
				nOldType = row->m_nRowType;
			}
			row->CheckStationException();
			row->Dump(dc);
		}
		f.Close();
	}
}

//=======================================================
//
//  함 수 명 :  Export()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  ILD 및 RPP 파일을 출력한다. 
//
//=======================================================
void CTableDoc::Export()
{
	CString			strFileName;
	CString			strFileName1;
	CFile			f;
	CFileException	e;
	POSITION		pos;
	CString ttl;
	CString			strVersion;
	CTime    time = CTime::GetCurrentTime();
	strFileName1 = strFileName = MakeFileName(this, "ILD");

	r = m_count = End = 0;

	BackupIDFFile( strFileName );
	MakeIDFFile( strFileName );
	strFileName = MakeFileName(this, "IDF");
	ChangeIDFFile( strFileName , strFileName1);

	strFileName1 = strFileName = MakeFileName(this, "RPP");
	strVersion = GetProgramVersion();

	if( f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
		CDumpContext dc( &f );
		CRow *row;

		dc << RPP_FILE_VERSION;
		dc << "\r\n";
		CString ttl = ";/////////////////////////////////////////////////////////////////////////"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
		        ttl = ";//         LSIS Interlocking equipment route data file v3.0            //"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
				ttl = ";//                    Do not modify this file                          //"; dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
           ttl.Format(";//        Create date : %04dY %02dM %02dD %02dH %02dM %02dS                      //",time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());dc << ttl << "\r\n";
		        ttl = ";//                                                                     //"; dc << ttl << "\r\n";
		        ttl = ";/////////////////////////////////////////////////////////////////////////"; dc << ttl << "\r\n";
		        ttl = ";LSD Version : " + strVersion;dc << ttl << "\r\n";dc << "\r\n";


		for( pos = m_Items.GetHeadPosition(); pos != NULL; )
		{
			row = (CRow*)m_Items.GetNext( pos );
            if (row->m_pStrPath) {
//                char *p = (char*)(LPCTSTR)*row->m_pStrPath;
//                p += 6;
//                CString path = p;
				CString path = row->m_pStrPath;
//				if ( row->m_Button.Find("(C)",0) > -1 || row->m_Button.Find("(S)",0) > -1 /*|| row->m_Button.Find(")-",0) > -1*/ || row->m_Type == "POINT" ) {
//					continue;
//				}
//                dc << row->m_Signal << ":" << row->m_Button/*.Mid(0,row->m_Button.Find("(",0))*/ << " " << path << "\r\n";
				  dc << row->m_No << " " << path << "\r\n";
            }
        }
		f.Close();
    }

	strFileName = MakeFileName(this, "PRL");
	ChangeIDFFile( strFileName , strFileName1);
}

void DrawRect(CDC *pDC, CRect &rect);

//=======================================================
//
//  함 수 명 :  DrawCover
//  함수출력 :  없음
//  함수입력 :  CDC *pDC
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표의 표지를 그린다. 
//
//=======================================================
void CTableDoc::DrawCover(CDC *pDC) // 연동도표의 외곽선을 그린다. 
{
	CString strTemp;
	CString strTemp2;
	CRect client;
	int gap = 30;
	pDC->GetClipBox(client);
	CRect rect = client;

	int hx = client.left-gap;
	int hy = client.bottom+gap;
	rect.top = rect.top - gap;
	rect.left = rect.left + gap;
	rect.right = rect.right - gap;
	rect.bottom = rect.bottom + gap;

	int y;
	int x;

	strTemp2 = m_StationObject.m_StationName;
	short l = strlen(strTemp2);
    short n = strTemp2.Find( '_' ); 

	for (;n > 0 ; n = strTemp2.Find( '_' ))
	{
		CString Temp = strTemp2.Right(l-n-1);
		strTemp2 = strTemp2.Left( n );
		strTemp2 += "  ";
		strTemp2 += Temp;
		l++;
	}
	short k = strTemp2.Find('@');
	if ( k > 0 )
		strTemp2 = strTemp2.Left(k);


	x = rect.left ; y = rect.bottom;
	strTemp.Format("%s STATION" ,strTemp2);

	if ( m_StationObject.m_Edit19.Find("BRLC-LS-") > 0 )
	{
		StrWrite( -1000, y+1300, "CONSTRUCTION OF DOUBLE LINE TRACK FROM LAKSAM TO CHINKI" , pDC , 60 , 1);
		StrWrite( -1020, y+1200, "ASTANA INCLUDING SIGNALLING ON DHAKA-CHITTAGONG MAIN LINE" , pDC , 60 , 1);
	}
	else if(m_StationObject.m_Edit19.Find("BR-LS-") > 0)
	{
		StrWrite( -1000, y+1300, "CONSTRUCTION OF DOUBLE LINE TRACK FROM TONGI TO BHAIRAB" , pDC , 60 , 1);
		StrWrite( -1012, y+1200, "BAZAR INCLUDING SIGNALLING ON DHAKA-CHITTAGONG MAIN LINE" , pDC , 60 , 1);
	}

	if ( strTemp.Find("SRINIDHI") > 0)
		StrWrite( -480, y+900,  strTemp , pDC , 100 , 1);
	else
		StrWrite( -600, y+900,  strTemp , pDC , 100 , 1);
	StrWrite( -400, y+800, "CONTROL TABLE" , pDC , 100 , 1);

//	if ( m_iTableType != 4 ) {
 //   CPen penLine; // Construct it, then initialize
//    penLine.CreatePen( PS_SOLID, 3, RGB(0,0,0) );
//    CPen* pOldPen = pDC->SelectObject( &penLine );
//	CRect rect( 450 , y+300, 920, y + 430);
//	DrawRect(pDC,rect);
//	if ( m_iTableType == 1 ) {
//		StrWrite( 490, y+410, "   FOR DRAFT " , pDC , 50 , 1);
//	} else if ( m_iTableType == 2 ) {
//		StrWrite( 490, y+410, "FOR APPROVAL" , pDC , 50 , 1);
//	} else if ( m_iTableType == 3 ) {
//		StrWrite( 490, y+410, "   TEST SHEET" , pDC , 50 , 1);
//	}
//	pDC->SelectObject( pOldPen );

//	pDC->MoveTo( 470 , y+350 );
//	pDC->LineTo( 900 , y+350 );

//	StrWrite( 495 , y+340, "BANGLADESH RAILWAY. COS/LG/10 STATION" , pDC , 20);
//	StrWrite( 495 , y+340, "BANGLADESH RAILWAY. COS/LG/AKHAURA JN STATION" , pDC , 17);
//	StrWrite( 495 , y+340, "BANGLADESH RAILWAY. Tonggi-Bazar Double Line" , pDC , 17);

//	}
}

//=======================================================
//
//  함 수 명 :  DrawPageOff
//  함수출력 :  없음
//  함수입력 :  CDC *pDC , int CurrPage , int TotalPage
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표의 외곽선 및 도각을 출력한다. 
//
//=======================================================
void CTableDoc::DrawPageOff(CDC *pDC , int CurrPage , int TotalPage) // 연동도표의 외곽선을 그린다. 
{
	CString strTemp;
	CString strTemp2;

	int gap = 30;
	CPoint pTemp1,pTemp2,pTemp3;
	CRect client;
	pDC->GetClipBox(client);
	CRect rect = client;

	int hx = client.left-(gap*2);
	int hy = client.bottom+gap;
	rect.top = rect.top - gap;
	rect.left = rect.left + (gap*2);
	rect.right = rect.right - gap;
	rect.bottom = rect.bottom + gap;

    CPen penLine; // Construct it, then initialize
    penLine.CreatePen( PS_SOLID, 3, RGB(0,0,0) );
    CPen* pOldPen = pDC->SelectObject( &penLine );
	DrawRect(pDC,rect);
	pDC->SelectObject( pOldPen );


	// 연동도표의 도각을 그린다. 
	if ( CurrPage > 1 ) 
	{
		int iTotalX = rect.right - rect.left ;
		int iTotalY = rect.top - rect.bottom ;

		if(m_StationObject.m_Edit19.Find("BRLC-LS-") > 0)
		{
			int x1 = (int) rect.left;
			int x2 = x1 + (int)(iTotalX * 0.1535);
			int x3 = x1 + (int)(iTotalX * 0.17425);
			int x4 = x1 + (int)(iTotalX * 0.20425);
			int x5 = x1 + (int)(iTotalX * 0.29100);
			int x6 = x1 + (int)(iTotalX * 0.30825); 
			int x7 = x1 + (int)(iTotalX * 0.33700); 
			int x8 = x1 + (int)(iTotalX * 0.37550); 
			int x9 = x1 + (int)(iTotalX * 0.42475); 
			int x10 = x1 + (int)(iTotalX * 0.48750); 
			int x11 = x1 + (int)(iTotalX * 0.53625); 
			int x12 = x1 + (int)(iTotalX * 0.59575);
			int x13 = x1 + (int)(iTotalX * 0.64450);
			int x14 = x1 + (int)(iTotalX * 0.81750);
			int x15 = x1 + (int)(iTotalX * 0.84600);
			int x16 = x1 + (int)(iTotalX * 0.97300); 
			int x17 = x1 + (int)iTotalX; 

			int y1 = (int)hy; 
			int y2 = y1 + (int)(iTotalY * 0.05484);
			int y3 = y1 + (int)(iTotalY * 0.13162);
			int y4 = y1 + (int)(iTotalY * 0.13500);
			int y11 = y1 + (int)(iTotalY * 0.01371);
			int y12 = y1 + (int)(iTotalY * 0.02742);
			int y13 = y1 + (int)(iTotalY * 0.04113);
			int y14 = y1 + (int)(iTotalY * 0.05484);
			int y15 = y1 + (int)(iTotalY * 0.06855);
			int y16 = y1 + (int)(iTotalY * 0.08226);
			int y17 = y1 + (int)(iTotalY * 0.09597);
			int y18 = y1 + (int)(iTotalY * 0.10968);
			int y21 = y1 + (int)(iTotalY * 0.01603);
			int y22 = y1 + (int)(iTotalY * 0.03332);
			int y23 = y1 + (int)(iTotalY * 0.05063);
			int y24 = y1 + (int)(iTotalY * 0.06159);
			int y31 = y1 + (int)(iTotalY * 0.02363);
			int y32 = y1 + (int)(iTotalY * 0.03755);
			int y33 = y1 + (int)(iTotalY * 0.10125);
			int y19 = (int)( hy-gap );

			CPen penLine;
			penLine.CreatePen( PS_SOLID, 3, RGB(0,0,0) );
			CPen* pOldPen = pDC->SelectObject( &penLine );
			DrawRect(pDC,rect);
			pDC->SelectObject( pOldPen );

			pTemp1.x = x1; pTemp1.y = y3; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y4; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y4; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y2; pDC->MoveTo( pTemp1 );
			pTemp2.x = x2; pTemp2.y = y2; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x2; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y11; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y11; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y12; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y12; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y13; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y13; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y14; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y14; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y15; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y15; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y16; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y16; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y17; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y17; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y18; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y18; pDC->LineTo( pTemp2 );

			pTemp1.x = x3; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x3; pTemp2.y = y18; pDC->LineTo( pTemp2 );

			pTemp1.x = x4; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x4; pTemp2.y = y18; pDC->LineTo( pTemp2 );

			pTemp1.x = x5; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x5; pTemp2.y = y18; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y21; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y21; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y22; pDC->MoveTo( pTemp1 );
			pTemp2.x = x14; pTemp2.y = y22; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y23; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y23; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y24; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x7; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x7; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x8; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x10; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x11; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y22; pDC->LineTo( pTemp2 );

			pTemp1.x = x12; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x12; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x12; pTemp1.y = y33; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y33; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x13; pTemp2.y = y22; pDC->LineTo( pTemp2 );

			pTemp1.x = x14; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x14; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x14; pTemp1.y = y31; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y31; pDC->LineTo( pTemp2 );

			pTemp1.x = x14; pTemp1.y = y32; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y32; pDC->LineTo( pTemp2 );

			pTemp1.x = x15; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x15; pTemp2.y = y32; pDC->LineTo( pTemp2 );

			pTemp1.x = x16; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x16; pTemp2.y = y32; pDC->LineTo( pTemp2 );

			// LS 로고 출력
			HDC hdc = pDC->GetSafeHdc();
			HDC mDC;
			
			HBITMAP LSLOGO;
			HBITMAP CRMJVLOGO;
			HBITMAP RANKENLOGO;
			HBITMAP CHINALOGO;
			HBITMAP MAXLOGO;
			HBITMAP CANARAILLOGO;
			HBITMAP SMECLOGO;
			HBITMAP DBLOGO;
			HBITMAP DAINICHILOGO;
			HBITMAP ACELOGO;
			
			HINSTANCE hInst = AfxGetInstanceHandle();
			
			LSLOGO			= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_LS_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			CRMJVLOGO		= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_CRMJV_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			RANKENLOGO		= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_RANKEN_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			CHINALOGO		= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_CHINA_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			MAXLOGO			= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_MAX_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			CANARAILLOGO	= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_CANARAIL_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			SMECLOGO		= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_SMEC_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			DBLOGO			= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_DB_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			DAINICHILOGO	= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_DAINICHI_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			ACELOGO			= (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_ACE_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);
			
			mDC=CreateCompatibleDC(hdc);

			SelectObject(mDC, LSLOGO);
			StretchBlt(hdc,x1+8,y2-40,77,30,mDC,0,0,804,233,SRCCOPY);
			SelectObject(mDC, CRMJVLOGO);
			StretchBlt(hdc,x6+5,y3-55,48,36,mDC,0,0,58,45,SRCCOPY);
			SelectObject(mDC, RANKENLOGO);
			StretchBlt(hdc,x8+15,y3-39,64,26,mDC,0,0,96,39,SRCCOPY);
			SelectObject(mDC, CHINALOGO);
			StretchBlt(hdc,x8+17,y3-75,62,30,mDC,0,0,153,92,SRCCOPY);
			SelectObject(mDC, MAXLOGO);
			StretchBlt(hdc,x6+5,y24+10,80,31,mDC,0,0,150,57,SRCCOPY);
			SelectObject(mDC,CANARAILLOGO);
			StretchBlt(hdc,x10+5,y3-39,90,31,mDC,0,0,350,91,SRCCOPY);
			SelectObject(mDC,SMECLOGO);
			StretchBlt(hdc,x10+10,y3-85,90,31,mDC,0,0,642,212,SRCCOPY);
			SelectObject(mDC,DBLOGO);
			StretchBlt(hdc,x11+10,y3-85,100,31,mDC,0,0,87,28,SRCCOPY);
			SelectObject(mDC,DAINICHILOGO);
			StretchBlt(hdc,x10+5,y23-25,130,40,mDC,0,0,1348,345,SRCCOPY);
			SelectObject(mDC,ACELOGO);
			StretchBlt(hdc,x11+40,y23-20,80,50,mDC,0,0,119,82,SRCCOPY);
			
			// 사인 출력
			
			HBITMAP Sign1;
			HBITMAP Sign2;
			
			Sign1 = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_SIGN3));
			
			mDC=CreateCompatibleDC(hdc);
			SelectObject(mDC,Sign1);
			StretchBlt(hdc,x7+3,y21+3,80,20,mDC,0,0,449,176,SRCCOPY);
			
			Sign2 = LoadBitmap(hInst,MAKEINTRESOURCE(IDB_SIGN4));
			
			mDC=CreateCompatibleDC(hdc);
			SelectObject(mDC,Sign2);
			StretchBlt(hdc,x8+10,y21+3,90,20,mDC,0,0,70,18,SRCCOPY);

						// 글자 출력
		
			StrWrite( x1+20, y3-7, "N O T E S:" , pDC , 23, 1);

			StrWrite( x1+5, y18-7, "ALL DIMENSIONS ARE IN METRIC SCALE. DO NOT SCALE" , pDC , 14);
			StrWrite( x1+5, y17-4, "FROM THE DRAWING. WRITTEN DIMENSIONS WILL" , pDC , 14);
			StrWrite( x1+5, y16-1, "PREVAIL IN ALL CASES UNLESS VERIRIED BY THE SITE" , pDC , 14);
			StrWrite( x1+5, y15+2, "CONDITION." , pDC , 14);

			StrWrite( x1+90, y13, "LS INDUSTRIAL SYSTEMS Co., LTD." , pDC , 14);
			StrWrite( x1+50, y12-5, "LS tower, 1026-6 Hogye-dong, Dongan-gu," , pDC , 14);
			StrWrite( x1+50, y11, "Anyang-si, Gyeonggi-do, Korea" , pDC , 14);

			StrWrite( x4+10, y3-7, "REVISIONS" , pDC , 23, 1);
			StrWrite( x2+3, y18-3, "MKD" , pDC , 19, 1);
			StrWrite( x3+10, y18-3, "DATE" , pDC , 19, 1);
			StrWrite( x4+30, y18-3, "DESCRIPTION" , pDC , 19, 1);
			StrWrite( x5+8, y18-3, "BY" , pDC , 19, 1);

			StrWrite( x2+18, y17-2, "A" , pDC , 19, 1);
		//	StrWrite( x3+10, y7-4, "B" , pDC , 19, 1);
		//	StrWrite( x3+10, y6-4, "C" , pDC , 19, 1);
		//	StrWrite( x3+10, y5-4, "D" , pDC , 19, 1);

			strTemp.Format("%s",m_StationObject.m_Edit10);
			StrWrite( x3+2, y17-6, strTemp , pDC , 11);
			strTemp.Format("%s",m_StationObject.m_Edit12);
			StrWrite( x4+10, y17-5, strTemp , pDC , 14);
			strTemp.Format("%s",m_StationObject.m_Edit11);
			StrWrite( x5, y17-5, strTemp , pDC , 14);

			StrWrite( x7-12, y3-30, "Joint Venture of" , pDC , 14);
			StrWrite( x9-30, y3-12, "Chengdu Ranken Railway" , pDC , 14);
			StrWrite( x9-30, y3-27, "Construction Co. Ltd." , pDC , 14);
			StrWrite( x9-30, y3-42, "China Railway Materials" , pDC , 14);
			StrWrite( x9-30, y3-57, "Import & Export Co. Ltd." , pDC , 14);
			StrWrite( x7+30, y15+17, "MAX Automobile Products Ltd." , pDC , 14);

			StrWrite( x7+5, y24-2, "DRAWN BY" , pDC , 14, 1);
			StrWrite( x8+10, y24-2, "CHECKED BY" , pDC , 14, 1);
			StrWrite( x9+20, y24-2, "PREPARED BY" , pDC , 14, 1);
			StrWrite( x6+3, y23-7, "NAME" , pDC , 14, 1);
			StrWrite( x6+3, y22-8, "SIGNATURE" , pDC , 10, 1);
			StrWrite( x6+3, y21-5, "DATE" , pDC , 14, 1);

			strTemp.Format("%s",m_StationObject.m_Edit17);
			StrWrite( x7+10, y23-7, strTemp , pDC , 14);
			strTemp.Format("%s",m_StationObject.m_Edit18);
			StrWrite( x7+10, y21-6, strTemp , pDC , 11);
			strTemp.Format("%s",m_StationObject.m_Edit15);
			StrWrite( x8+25, y23-7, strTemp , pDC , 14);
			strTemp.Format("%s",m_StationObject.m_Edit16);
			StrWrite( x8+23, y21-6, strTemp , pDC , 11);

			StrWrite( x11-15, y3-15, "in Joint Venture with" , pDC , 14);
			StrWrite( x10+10, y15+5, "& in association with" , pDC , 14);

			StrWrite( x10+7, y22-10, "APPROVED" , pDC , 19, 1);
			StrWrite( x10+40, y21, "BY" , pDC , 19, 1);

			StrWrite( x12+50, y3-4, "Bangladesh Railway" , pDC , 40, 1);

			StrWrite( x12+60, y33-15, "DHAKA-CHITTAGONG RAILWAY DEVELOPMENT PROJECT" , pDC , 19);

			StrWrite( x12+45, y33-45, "TRACK DOUBLING BETWEEN LAKSAM AND CHINKI ASTANA" , pDC , 19);
			StrWrite( x12+190, y33-65, "DCRDP/LCDL/W-1" , pDC , 19);
			StrWrite( x12+160, y33-85, "JBIC LOAN NO. BD-P56" , pDC , 19);

			StrWrite( x12+7, y22-10, "APPROVED" , pDC , 19, 1);
			StrWrite( x12+40, y21, "BY" , pDC , 19, 1);

			StrWrite( x14+35, y3-4, "DRAWING TITLE" , pDC , 40, 1);
			strTemp2 = m_StationObject.m_StationName;
			short l = strlen(strTemp2);
			short n = strTemp2.Find( '_' ); 
			
			for (;n > 0 ; n = strTemp2.Find( '_' ))
			{
				CString Temp = strTemp2.Right(l-n-1);
				strTemp2 = strTemp2.Left( n );
				strTemp2 += " ";
				strTemp2 += Temp;
				l++;
			}
			short k = strTemp2.Find('@');
			strTemp2 = strTemp2.Left(k);
			
			strTemp.Format("%s STATION",strTemp2);
			StrWrite( x14+10, y33-20, strTemp , pDC , 30, 1);
			StrWrite( x14+75, y33-55, "CONTROL TABLE" , pDC , 30, 1);

			StrWrite( x14+13, y32-4, "Scale" , pDC , 14, 1);
			StrWrite( x15+85, y32-4, "AS APPLICABLE" , pDC , 14, 1);
			StrWrite( x16+5, y32-6, "SHEET NO" , pDC , 10, 1);
			StrWrite( x14+5, y31-11, "DWG. No." , pDC , 12, 1);

			strTemp.Format("%s",m_StationObject.m_Edit19);
			if ( strlen(strTemp) == 24 )						// 역명 이니셜이 3자리
				StrWrite( x15+5, y31-8, strTemp , pDC , 20, 1);
			else												// 역명 이니셜이 4자리
				StrWrite( x15-2, y31-8, strTemp, pDC, 20, 1);
			if ( m_bTotalPage == TRUE )
			{
				if ( CurrPage > 2 )
				{
					if ((TotalPage - 3) > 9 )
						strTemp.Format("%02d/%d",CurrPage-2,TotalPage-3);
					else
						strTemp.Format(" %d/ %d",CurrPage-2,TotalPage-3);
				}
				else
					strTemp.Format(" 1/ 2");
			}
			else 
			{
				if ( CurrPage > 2 )
					strTemp.Format("%03d",CurrPage+m_iStartPage);
				else
					strTemp.Format("%03d",CurrPage-1+m_iStartPage);
			}
			StrWrite( x16+2, y31-8,  strTemp , pDC , 20, 1);
			

/*			strTemp.Format("%s",m_StationObject.m_Edit18);
			StrWrite( x4, y8-5, strTemp , pDC , 14);
			strTemp.Format("%s",m_StationObject.m_Edit12);
			StrWrite( x5+5, y8-5, strTemp , pDC , 17);
			strTemp.Format("%s",m_StationObject.m_Edit17);
			StrWrite( x6, y8-7, strTemp , pDC , 13);
			StrWrite( x17+70, y34-1, "Drawing No." , pDC , 14);
			if ( CurrPage > 2 )
				strTemp.Format("%s",m_StationObject.m_Edit19);
			else
			{
				CString strDrawingNumber = m_StationObject.m_Edit19;
				strDrawingNumber = strDrawingNumber.Left(strlen(strDrawingNumber) - 1);
				strDrawingNumber = strDrawingNumber + '2';
				strTemp.Format("%s",strDrawingNumber);
			}
			StrWrite( x17+5, y33-15, strTemp , pDC , 17);
			StrWrite( x17+70, y32-1, "Sheet No." , pDC , 14);
			if ( m_bTotalPage == TRUE )
			{
				if ( CurrPage > 2 )
				{
					if ((TotalPage - 3) > 9 )
						strTemp.Format("%02d/%d",CurrPage-2,TotalPage-3);
					else
						strTemp.Format(" %d/ %d",CurrPage-2,TotalPage-3);
				}
				else
					strTemp.Format(" 1/ 2");
			}
			else 
			{
				if ( CurrPage > 2 )
					strTemp.Format("%03d",CurrPage+m_iStartPage);
				else
					strTemp.Format("%03d",CurrPage-1+m_iStartPage);
			}
			StrWrite( x17+70, y31-10,  strTemp , pDC , 30, 1);
*/
		}
		else if(m_StationObject.m_Edit19.Find("BR-LS-") > 0)
		{
			int x1 = (int) rect.left;
			int x2 = x1 + (int)(iTotalX * 0.01784);
			int x3 = x1 + (int)(iTotalX * 0.05315);
			int x4 = x1 + (int)(iTotalX * 0.06716);
			int x5 = x1 + (int)(iTotalX * 0.10496);
			int x6 = x1 + (int)(iTotalX * 0.19493); 
			int x7 = x1 + (int)(iTotalX * 0.21878); 
			int x8 = x1 + (int)(iTotalX * 0.24635); 
			int x9 = x1 + (int)(iTotalX * 0.36822); 
			int x10 = x1 + (int)(iTotalX * 0.40391); 
			int x11 = x1 + (int)(iTotalX * 0.47646); 
			int x12 = x1 + (int)(iTotalX * 0.58583);
			int x13 = x1 + (int)(iTotalX * 0.74378);
			int x14 = x1 + (int)(iTotalX * 0.78102);
			int x15 = x1 + (int)(iTotalX * 0.82417);
			int x16 = x1 + (int)(iTotalX * 0.86908); 
			int x17 = x1 + (int)(iTotalX * 0.91384);
			int x18 = x1 + (int)iTotalX; 

			int y1 = (int)hy; 
			int y2 = y1 + (int)(iTotalY * 0.01468);
			int y3 = y1 + (int)(iTotalY * 0.02936);
			int y4 = y1 + (int)(iTotalY * 0.04404);
			int y5 = y1 + (int)(iTotalY * 0.05872);
			int y6 = y1 + (int)(iTotalY * 0.07340); 
			int y7 = y1 + (int)(iTotalY * 0.08808); 
			int y8 = y1 + (int)(iTotalY * 0.10276); 
			int y9 = y1 + (int)(iTotalY * 0.11744); 
			int y11 = y1 + (int)(iTotalY * 0.01357); 
			int y12 = y1 + (int)(iTotalY * 0.02714); 
			int y13 = y1 + (int)(iTotalY * 0.04071); 
			int y14 = y1 + (int)(iTotalY * 0.05260); 
			int y15 = y1 + (int)(iTotalY * 0.06881); 
			int y16 = y1 + (int)(iTotalY * 0.08502); 
			int y17 = y1 + (int)(iTotalY * 0.10123); 
			int y18 = y1 + (int)(iTotalY * 0.11744); 
			int y21 = y1 + (int)(iTotalY * 0.01749); 
			int y22 = y1 + (int)(iTotalY * 0.03498); 
			int y23 = y1 + (int)(iTotalY * 0.05247); 
			int y24 = y1 + (int)(iTotalY * 0.06996); 
			int y25 = y1 + (int)(iTotalY * 0.11744); 
			int y31 = y1 + (int)(iTotalY * 0.026235); 
			int y32 = y1 + (int)(iTotalY * 0.03498); 
			int y33 = y1 + (int)(iTotalY * 0.061215); 
			int y34 = y1 + (int)(iTotalY * 0.06996); 
			int y35 = y1 + (int)(iTotalY * 0.11744); 
			int y00 = y1 + (int)(iTotalY * 0.13992); 
			int y19 = (int)( hy-gap );
/*		
			int x1 = (int) rect.left;
			int x2 = x1 + (int)(iTotalX * 0.3670);
			int x3 = x1 + (int)(iTotalX * 0.4177);
			int x4 = x1 + (int)(iTotalX * 0.4943);
			int x5 = x1 + (int)(iTotalX * 0.6742);
			int x6 = x1 + (int)(iTotalX * 0.6926); 
			int x7 = x1 + (int)(iTotalX * 0.7314);  // 0.7314 -> 재우시가 준 도각 
			int x8 = x1 + (int)(iTotalX * 0.8338); 
			int x9 = x1 + (int)(iTotalX * 0.8863); 
			int x10 = x1 + (int)(iTotalX * 0.9427); 
			int x11 = x1 + (int)iTotalX; 

			int y1 = (int)hy; 
			int y2 = y1 + (int)(iTotalY * 0.0181);
			int y3 = y1 + (int)(iTotalY * 0.0362);
			int y4 = y1 + (int)(iTotalY * 0.0544);
			int y5 = y1 + (int)(iTotalY * 0.0733);
			int y6 = y1 + (int)(iTotalY * 0.1001); 
			int y7 = y1 + (int)(iTotalY * 0.1112); 
			int y8 = y1 + (int)(iTotalY * 0.1443); 
			int y9 = (int)( hy-gap );
*/
			CPen penLine; // Construct it, then initialize
			penLine.CreatePen( PS_SOLID, 3, RGB(0,0,0) );
			CPen* pOldPen = pDC->SelectObject( &penLine );
			DrawRect(pDC,rect);
			pDC->SelectObject( pOldPen );

			pTemp1.x = x1; pTemp1.y = y2; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y2; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y3; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y4; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y4; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y5; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y6; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y6; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y7; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y7; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y8; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y8; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y9; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x1; pTemp1.y = y00; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x2; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x3; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x3; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x4; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x4; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x5; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x5; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x7; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x7; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x8; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x8; pTemp1.y = y13; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y13; pDC->LineTo( pTemp2 );

			pTemp1.x = x8; pTemp1.y = y14; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y14; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y9; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y11; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y11; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y12; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y12; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y13; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y13; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y14; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y14; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y15; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y15; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y16; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y16; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y17; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y17; pDC->LineTo( pTemp2 );

			pTemp1.x = x10; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y13; pDC->LineTo( pTemp2 );

			pTemp1.x = x10; pTemp1.y = y14; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y17; pDC->LineTo( pTemp2 );

			pTemp1.x = x11; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x12; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x12; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x13; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y21; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y21; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y22; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y22; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y23; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y23; pDC->LineTo( pTemp2 );

			pTemp1.x = x13; pTemp1.y = y24; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x14; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x14; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x15; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x15; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x16; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x16; pTemp2.y = y24; pDC->LineTo( pTemp2 );

			pTemp1.x = x17; pTemp1.y = y1; pDC->MoveTo( pTemp1 );
			pTemp2.x = x17; pTemp2.y = y00; pDC->LineTo( pTemp2 );

			pTemp1.x = x17; pTemp1.y = y31; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y31; pDC->LineTo( pTemp2 );

			pTemp1.x = x17; pTemp1.y = y32; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y32; pDC->LineTo( pTemp2 );

			pTemp1.x = x17; pTemp1.y = y33; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y33; pDC->LineTo( pTemp2 );

			pTemp1.x = x17; pTemp1.y = y34; pDC->MoveTo( pTemp1 );
			pTemp2.x = x18; pTemp2.y = y34; pDC->LineTo( pTemp2 );

/*
			pTemp1.x = x1; pTemp1.y = y6; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y6; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y5; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y4; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y4; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y3; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y3; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y2; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y2; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y7; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y7; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y8; pDC->MoveTo( pTemp1 );
			pTemp2.x = x11; pTemp2.y = y8; pDC->LineTo( pTemp2 );

			pTemp1.x = x2; pTemp1.y = y6; pDC->MoveTo( pTemp1 );
			pTemp2.x = x2; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x3; pTemp1.y = y6; pDC->MoveTo( pTemp1 );
			pTemp2.x = x3; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x4; pTemp1.y = y6; pDC->MoveTo( pTemp1 );
			pTemp2.x = x4; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x5; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x5; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x6; pTemp1.y = y8; pDC->MoveTo( pTemp1 );
			pTemp2.x = x6; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x7; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x7; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x8; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x8; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x9; pTemp1.y = y5; pDC->MoveTo( pTemp1 );
			pTemp2.x = x9; pTemp2.y = y1; pDC->LineTo( pTemp2 );

			pTemp1.x = x10; pTemp1.y = y3; pDC->MoveTo( pTemp1 );
			pTemp2.x = x10; pTemp2.y = y1; pDC->LineTo( pTemp2 );
*/
			// LS 로고 출력

			HDC hdc = pDC->GetSafeHdc();
			HDC mDC;
			HBITMAP MyBitmap;

			HINSTANCE hInst = AfxGetInstanceHandle();

			MyBitmap = (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_LS_LOGO), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);

			mDC=CreateCompatibleDC(hdc);
			SelectObject(mDC,MyBitmap);
			StretchBlt(hdc,x8+5,y13-22,42,20,mDC,0,0,854,233,SRCCOPY);

			// 사인 출력

			HBITMAP Sign1;
			HBITMAP Sign2;

			Sign1 = (HBITMAP)LoadImage(hInst,MAKEINTRESOURCE(IDB_SIGN3), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);

			mDC=CreateCompatibleDC(hdc);
			SelectObject(mDC,Sign1);
			StretchBlt(hdc,x7+3,y8-23,51,20,mDC,0,0,450,177,SRCCOPY);

			Sign2 = (HBITMAP)LoadImage(hInst,MAKEINTRESOURCE(IDB_SIGN4), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_COPYFROMRESOURCE | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT);

			mDC=CreateCompatibleDC(hdc);
			SelectObject(mDC,Sign2);
			StretchBlt(hdc,x10+60,y11-19,70,18,mDC,0,0,70,18,SRCCOPY);
/*
			CBitmap pBitmap;
			CBitmap pBitmap3;

			pBitmap.LoadBitmap(IDB_SIGN3);
			pBitmap3.LoadBitmap(IDB_SIGN4);

			CDC cdc;
			cdc.CreateCompatibleDC(pDC);
			cdc.SelectObject(&pBitmap);
		//	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&dc,0,0,SRCCOPY);
			pDC->StretchBlt(x1+1080,y9+23,72,36,&cdc,0,0,109,54,SRCCOPY);

			CDC ddc;
			ddc.CreateCompatibleDC(pDC);
			ddc.SelectObject(&pBitmap3);
			pDC->StretchBlt(x1+1830,y9+23,94,103,&ddc,0,0,132,139,SRCCOPY);

			pBitmap.DeleteObject();
			pBitmap3.DeleteObject();
*/
			// 글자 출력
		
			StrWrite( x1+5, y00-7, "REMARKS" , pDC , 23, 1);
			StrWrite( x5+40, y00-7, "REVISIONS" , pDC , 23, 1);
			StrWrite( x9-90, y00-7, "CONTRACTOR" , pDC , 23, 1);
			StrWrite( x11+15, y00-7, "ENGINEER/CONSULTANT" , pDC , 19, 1);
			StrWrite( x12+110, y00-7, "APPROVAL" , pDC , 23, 1);
			StrWrite( x13+15, y00-7, "EMPLOYER:BANGLADESH RAILWAY" , pDC , 21, 1);
			StrWrite( x17+10, y00-7, "DRAWING TITLE" , pDC , 23, 1);

			StrWrite( x3+2, y9-7, "MKD" , pDC , 12, 1);
			StrWrite( x4+20, y9-4, "DATE" , pDC , 19, 1);
			StrWrite( x5+40, y9-4, "DESCRIPTION" , pDC , 19, 1);
			StrWrite( x6+15, y9-4, "BY" , pDC , 19, 1);
			StrWrite( x7+10, y9-4, "SIGN." , pDC , 19, 1);

			StrWrite( x3+10, y8-4, "A" , pDC , 19, 1);
		//	StrWrite( x3+10, y7-4, "B" , pDC , 19, 1);
		//	StrWrite( x3+10, y6-4, "C" , pDC , 19, 1);
		//	StrWrite( x3+10, y5-4, "D" , pDC , 19, 1);

			strTemp.Format("%s",m_StationObject.m_Edit18);
			StrWrite( x4, y8-5, strTemp , pDC , 14);
			strTemp.Format("%s",m_StationObject.m_Edit12);
			StrWrite( x5+5, y8-5, strTemp , pDC , 17);
			strTemp.Format("%s",m_StationObject.m_Edit17);
			StrWrite( x6, y8-7, strTemp , pDC , 13);

			StrWrite( x8+10, y9-8, "CHINA RAILWAY GROUP LIMITED,(CREC)" , pDC , 15);
			StrWrite( x8+10, y9-25, "Registered Office" , pDC , 15);
			StrWrite( x8+10, y9-42, "1,Xinghuo Road Fenghal District, Beijing, China" , pDC , 11);
			StrWrite( x8+10, y9-57, "Bangladesh office." , pDC , 12);
			StrWrite( x8+10, y9-72, "House no.20 (Green Palace), Road no. 129," , pDC , 12);
			StrWrite( x8+10, y9-87, "Gulshan-1, Dhaka-1212, Bangladesh." , pDC , 12);
			StrWrite( x8+50, y14, "SUB - CONTRACTOR" , pDC , 19, 1);
			StrWrite( x8+50, y12+10, "LS INDUSTRIAL SYSTEMS Co., LTD." , pDC , 14);
			StrWrite( x8+30, y11+10, "LS tower, 1026-6 Hogye-dong, Dongan-gu," , pDC , 12);
			StrWrite( x8+30, y11-5, "Anyang-si, Gyeonggi-do, Korea" , pDC , 12);

			StrWrite( x10+10, y9-8, "Submitted" , pDC , 15);
			StrWrite( x9+30, y17-7, "Name" , pDC , 15);
			StrWrite( x9+5, y16-7, "Designation" , pDC , 13);
			StrWrite( x9+10, y15-7, "Signature" , pDC , 15);
			StrWrite( x10, y14-1, "Prepared By" , pDC , 15);
			StrWrite( x9+30, y13-3, "Name" , pDC , 15);
			StrWrite( x9+5, y12-3, "Designation" , pDC , 13);
			StrWrite( x9+10, y11-3, "Signature" , pDC , 15);

			StrWrite( x10+60, y13-3, "Y.C.KIM" , pDC , 17);
			StrWrite( x10+20, y12-3, "PROJECT MANAGER" , pDC , 17);

			StrWrite( x11+20, y9-5, "SMEC" , pDC , 25, 1);
			StrWrite( x11+10, y9-30, "SMEC INTERNATIONAL PTY LTD.A" , pDC , 15);
			StrWrite( x11+10, y9-50, "AUSTRALIA ,in association with" , pDC , 15);
			StrWrite( x11+10, y9-70, "Maunsell/AECOM," , pDC , 15);
			StrWrite( x11+10, y9-90, "No. 5 Institute of China Railway," , pDC , 15);
			StrWrite( x11+10, y9-109, "Balaji Railroad Systems Ltd. India" , pDC , 13);
			StrWrite( x11+10, y9-127, "ACE Consultants Ltd., Bangladesh" , pDC , 15);
			StrWrite( x11+10, y9-146, "House 13/A, Road-83, Gulshan-2, Dhaka-1212," , pDC , 11);
			StrWrite( x11+10, y9-161, "Bangladesh." , pDC , 15);

			StrWrite( x13+10, y9-10, "BANGLADESH RAILWAY SECTOR IMPROVEMENT PROJECT-1" , pDC , 13);
			StrWrite( x13+10, y9-30, "Construction of Doulble Line Track from Tongi to Bhairab Bazar" , pDC , 13);
			StrWrite( x13+10, y9-50, "Including Signalling of Dhaka-Chittagong Main Line." , pDC , 13);
			StrWrite( x14+10, y24-7, "Prepared By" , pDC , 15);
			StrWrite( x15+10, y24-7, "Checked By" , pDC , 15);
			StrWrite( x16+10, y24-7, "Approved By" , pDC , 15);
			StrWrite( x13+30, y23-7, "Name" , pDC , 15);
			StrWrite( x13+10, y22-7, "Signature" , pDC , 15);
			StrWrite( x13+30, y21-7, "Date" , pDC , 15);

			strTemp2 = m_StationObject.m_StationName;
			short l = strlen(strTemp2);
			short n = strTemp2.Find( '_' ); 

			for (;n > 0 ; n = strTemp2.Find( '_' ))
			{
				CString Temp = strTemp2.Right(l-n-1);
				strTemp2 = strTemp2.Left( n );
				strTemp2 += " ";
				strTemp2 += Temp;
				l++;
			}
			short k = strTemp2.Find('@');
			if ( k > 0 )
				strTemp2 = strTemp2.Left(k);

			strTemp.Format("%s STATION",strTemp2);
			if ( strTemp.Find("SRINIDHI") > 0)
				StrWrite( x17+33, y9-20, strTemp , pDC , 16);
			else
				StrWrite( x17+10, y9-20, strTemp , pDC , 16);
			StrWrite( x17+40, y9-40, "CONTROL TABLE" , pDC , 20);
			StrWrite( x17+70, y34-1, "Drawing No." , pDC , 14);
			if ( CurrPage > 2 )
			{
				strTemp.Format("%s",m_StationObject.m_Edit19);
			}
			else
			{
				CString strDrawingNumber = m_StationObject.m_Edit19;
				strDrawingNumber = strDrawingNumber.Left(strlen(strDrawingNumber) - 1);
				strDrawingNumber = strDrawingNumber + '2';
				strTemp.Format("%s",strDrawingNumber);
			}
			StrWrite( x17+5, y33-15, strTemp , pDC , 17);
			StrWrite( x17+70, y32-1, "Sheet No." , pDC , 14);
			if ( m_bTotalPage == TRUE )
			{
				if ( CurrPage > 2 )
				{
					if ((TotalPage - 3) > 9 )
						strTemp.Format("%02d/%d",CurrPage-2,TotalPage-3);
					else
						strTemp.Format(" %d/ %d",CurrPage-2,TotalPage-3);
				}
				else
					strTemp.Format(" 1/ 2");
			}
			else 
			{
				if ( CurrPage > 2 )
					strTemp.Format("%03d",CurrPage+m_iStartPage);
				else
					strTemp.Format("%03d",CurrPage-1+m_iStartPage);
			}
			StrWrite( x17+70, y31-10,  strTemp , pDC , 30, 1);

/*
			StrWrite( x1+190, y6-55, "LS INDUSTRIAL SYSTEMS Co.,Ltd" , pDC , 40);
			StrWrite( x2+40, y6-10, "DATE" , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit01);
			StrWrite( x2+15, y5-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit04);
			StrWrite( x2+15, y4-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit07);
			StrWrite( x2+15, y3-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit10);
			StrWrite( x2+15, y2-5, strTemp , pDC , 16);
			StrWrite( x3+65, y6-10, "AUTH BY" , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit02);
			StrWrite( x3+15, y5-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit05);
			StrWrite( x3+15, y4-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit08);
			StrWrite( x3+15, y3-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit11);
			StrWrite( x3+15, y2-5, strTemp , pDC , 16);
			StrWrite( x4+120, y6-10, "REVISION DESCRIPTION" , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit03);
			StrWrite( x4+15, y5-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit06);
			StrWrite( x4+15, y4-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit09);
			StrWrite( x4+15, y3-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit12);
			StrWrite( x4+15, y2-5, strTemp , pDC , 16);
			StrWrite( x5+17, y5-10, "D" , pDC , 16);
			StrWrite( x5+17, y4-10, "C" , pDC , 16);
			StrWrite( x5+17, y3-10, "B" , pDC , 16);
			StrWrite( x5+17, y2-10, "A" , pDC , 16);
			StrWrite( x6+17, y4-8, "DESIGNED" , pDC , 12);
			strTemp.Format("%s",m_StationObject.m_Edit17);
			StrWrite( x7+15, y4-5, strTemp , pDC , 16);
			StrWrite( x6+17, y3-8, "CHECKED" , pDC , 12);
			strTemp.Format("%s",m_StationObject.m_Edit15);
			StrWrite( x7+15, y3-5, strTemp , pDC , 16);
			StrWrite( x6+17, y2-8, "APPROVED" , pDC , 12);
			strTemp.Format("%s",m_StationObject.m_Edit13);
			StrWrite( x7+15, y2-5, strTemp , pDC , 16);
			StrWrite( x8+50, y5-10, "DATE" , pDC , 12);
			strTemp.Format("%s",m_StationObject.m_Edit18);
			StrWrite( x8+15, y4-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit16);
			StrWrite( x8+15, y3-5, strTemp , pDC , 16);
			strTemp.Format("%s",m_StationObject.m_Edit14);
			StrWrite( x8+15, y2-5, strTemp , pDC , 16);
			StrWrite( x9+8, y3-8, "SCALE" , pDC , 10);
			StrWrite( x9+50, y3-30, "NONE" , pDC , 20);
			StrWrite( x10+8, y3-8, "SHEET No." , pDC , 10);
	
			if ( m_bTotalPage == TRUE ) strTemp.Format("%d/%d",CurrPage-1,TotalPage-1);
			else strTemp.Format("%03d",CurrPage-1+m_iStartPage);

			StrWrite( x10+50, y3-30,  strTemp , pDC , 20);
			StrWrite( x9+17, y5-10, "DRAWING No." , pDC , 12);
			strTemp.Format("%s",m_StationObject.m_Edit19);
			StrWrite( x9+17, y5-30, strTemp , pDC , 20);
		//	StrWrite( x6+60, y8-10, "BANGLADESH RAILWAY. COS/LG/10 STATION" , pDC , 30);
		//	StrWrite( x6+60, y8-10, "BANGLADESH RAILWAY. COS/LG/AKHAURA JN STATION" , pDC , 25);
			StrWrite( x6+60, y8-10, "BANGLADESH RAILWAY. Tonggi-Bazar Double Line" , pDC , 25);
			StrWrite( x6+17, y7-10, "DRAWING TITLE" , pDC , 12);

			strTemp2 = m_StationObject.m_StationName;
			short l = strlen(strTemp2);
			short n = strTemp2.Find( '_' ); 

			for (;n > 0 ; n = strTemp2.Find( '_' ))
			{
				CString Temp = strTemp2.Right(l-n-1);
				strTemp2 = strTemp2.Left( n );
				strTemp2 += "  ";
				strTemp2 += Temp;
				l++;
			}

			strTemp.Format("%s STATION CONTROL TABLE",strTemp2);
			StrWrite( x6+150, y7-20,  strTemp , pDC , 25);
*/
/*
			CDC cdc;
			cdc.CreateCompatibleDC(pDC);
		//	cdc.SelectObject(&m_pBitmap);
		//	pDC->StretchBlt(x1+110,y6-100,50,50,&cdc,0,0,120,120,SRCCOPY);
			cdc.SelectObject(&m_pBitmap3);
			pDC->StretchBlt(x1+1870,y9+23,94,103,&cdc,0,0,94,103,SRCCOPY);
*/
		}
	}
}


//=======================================================
//
//  함 수 명 :  StrWrite
//  함수출력 :  없음
//  함수입력 :  int x, int y, CString str , CDC* pDC , int iSzie , int iDevid 
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면에 글자를 출력한다.
//
//=======================================================
void CTableDoc::StrWrite(int x, int y, CString str , CDC* pDC , int iSzie , int iDevid)
{
   	CFont m_Font;
	CFont * oldfont;

	if ( iDevid == 0 ) m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	else			   m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"Arial Black");

	oldfont = pDC->SelectObject(&m_Font);

	pDC->SelectObject(&m_Font);				
	pDC->TextOut( x, y, str );

	pDC->SelectObject(oldfont);
	m_Font.DeleteObject();
}

void CTableDoc::ChangeIDFFile( CString strFileName , CString strFileName1)
{
	CFile m_readFile;
	CFile m_saveFile;
	if (m_readFile.Open( strFileName1, CFile::modeRead , NULL) == FALSE) 
	{
		AfxMessageBox("readFile open fail");
		return;
	}
	if (m_saveFile.Open( strFileName, CFile::modeCreate | CFile::modeWrite, NULL) == FALSE)
	{
		AfxMessageBox("saveFile open fail");
		return;
	}

	unsigned char buffer;
	BOOL beof = TRUE; 
	BOOL bCommentCK = FALSE;

	while (beof){
		if ( m_readFile.Read(&buffer,1) == 0 ) beof = FALSE;
		else {
			if ( buffer == 59 ) bCommentCK = TRUE;
			if ( buffer != 13 && buffer != 10 ) {
				if ( bCommentCK == FALSE ) buffer = buffer | 0x80;
			} else {
				bCommentCK = FALSE;
			}
			m_saveFile.Write(&buffer,1);
		}
	}
}
