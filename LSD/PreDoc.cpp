// PreDoc.cpp : implementation file
//

#include "stdafx.h"
#include "LSD.h"
#include "PreDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreDoc

IMPLEMENT_DYNCREATE(CPreDoc, CDocument)

CPreDoc::CPreDoc()
{
}

BOOL CPreDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CPreDoc::~CPreDoc()
{
}


BEGIN_MESSAGE_MAP(CPreDoc, CDocument)
	//{{AFX_MSG_MAP(CPreDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreDoc diagnostics

#ifdef _DEBUG
void CPreDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPreDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPreDoc serialization

void CPreDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPreDoc commands
