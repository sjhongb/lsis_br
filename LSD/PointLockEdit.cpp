//=======================================================
//==              PointLockEdit.cpp
//=======================================================
//	파 일 명 :  PointLockEdit.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "lsd.h"
#include "PointLockEdit.h"
#include "../include/StrList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPointLockEdit dialog

CPointLockEdit::CPointLockEdit(CString &name, CMyStrList *sw, CWnd* pParent /*=NULL*/)
	: CDialog(CPointLockEdit::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLockEdit)
	//}}AFX_DATA_INIT
	POSITION pos;
	CString * strTemp;
	CString strTemp2;
	int nIndex = 0;
	pos = sw->GetHeadPosition(); 
	while(pos != NULL) {
		strTemp = sw->GetAtIndex( nIndex );
		if (( strTemp->Find("&",0) > -1 ) || (strTemp->Find("*",0) > -1)) OverlapPoint.AddTail( *strTemp );
		else Point.AddTail( *strTemp );
		sw->GetNext(pos);
		nIndex++;
	}

	m_pName = &name;
	m_pSwitch = sw;
}


void CPointLockEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLockEdit)
	DDX_Control(pDX, IDC_LIST_LOCKSWITCH, m_ListSwitch);
	DDX_Control(pDX, IDC_LIST_LOCKSIGNAL, m_ListSignal);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPointLockEdit, CDialog)
	//{{AFX_MSG_MAP(CLockEdit)
	ON_BN_CLICKED(IDC_LOCK_SWITCHDEL, OnLockSwitchdel)
	ON_BN_CLICKED(IDC_LOCK_SIGNALDEL, OnLockSignaldel)
	ON_BN_CLICKED(IDC_LOCK_SWITCHINS, OnLockSwitchins)
	ON_BN_CLICKED(IDC_LOCK_SIGNALINS, OnLockSignalins)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLockEdit message handlers

BOOL CPointLockEdit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(*m_pName);

	POSITION pos;
	CString* str;
	
	for( pos = Point.GetHeadPosition(); pos != NULL; )
	{
        str = (CString*)Point.GetNext( pos );
		m_ListSwitch.AddString(*str);
	}
	for( pos = OverlapPoint.GetHeadPosition(); pos != NULL; )
	{
        str = (CString*)OverlapPoint.GetNext( pos );
		m_ListSignal.AddString(*str);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MyStrAdd(CComboBox &combo);
void MyStrDel(CComboBox &combo);

/*
void MyStrAdd(CComboBox &combo) {
	CString str, stra, strd;
	combo.GetWindowText(str);
	if (str == "") return;
	if (str.GetAt(0) == '-') str = str.Right(str.GetLength()-1);
	stra = "+"+str;
	strd = "-"+str;
	int index = combo.FindString(-1,strd);
	if (index != CB_ERR) {
		combo.DeleteString(index);
		combo.InsertString(index,str);
	}
	else if (combo.FindString(-1,str) == CB_ERR && combo.FindString(-1,stra) == CB_ERR) {
		combo.AddString(stra);
	}
}

void MyStrDel(CComboBox &combo) {
	CString str;
	int index = combo.GetCurSel();
	if (index < 0) return;
	combo.GetLBText(index,str);
	if (str.GetAt(0) != '-') {
		combo.DeleteString(index);
		if (str.GetAt(0) != '+') {
			str = "-" + str;
			combo.InsertString(index,str);
		}
	}
}
*/
void CPointLockEdit::OnLockSwitchdel() 
{
	// TODO: Add your control notification handler code here
	MyStrDel(m_ListSwitch);
}

void CPointLockEdit::OnLockSignaldel() 
{
	// TODO: Add your control notification handler code here
	MyStrDel(m_ListSignal);
}

void CPointLockEdit::OnLockSwitchins() 
{
	// TODO: Add your control notification handler code here
	MyStrAdd(m_ListSwitch);
}

void CPointLockEdit::OnLockSignalins() 
{
	// TODO: Add your control notification handler code here
	MyStrAdd(m_ListSignal);
}

void CPointLockEdit::OnOK() 
{
	// TODO: Add extra validation here
	Point.Purge();
	OverlapPoint.Purge();
	m_pSwitch->Purge();

	int i,n;
	CString str;
	n = m_ListSwitch.GetCount();
	for (i=0; i<n; i++) {
		m_ListSwitch.GetLBText(i,str);
		Point.AddTail(str);
		m_pSwitch->AddTail(str);
	}
	n = m_ListSignal.GetCount();
	for (i=0; i<n; i++) {
		m_ListSignal.GetLBText(i,str);
		OverlapPoint.AddTail(str);
		m_pSwitch->AddTail(str);
	}
	CDialog::OnOK();
}
