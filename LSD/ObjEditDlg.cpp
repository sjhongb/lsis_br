//=======================================================
//==              ObjEditDlg.cpp
//=======================================================
//	파 일 명 :  ObjEditDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "ObjEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjEditDlg dialog


CObjEditDlg::CObjEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjEditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjEditDlg)
	m_strText = _T("");
	m_strPara1 = _T("");
	m_strPara2 = _T("");
	//}}AFX_DATA_INIT
}


void CObjEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjEditDlg)
	DDX_Text(pDX, IDC_EDIT1, m_strText);
	DDX_Text(pDX, IDC_EDIT7, m_strPara1);
	DDX_Text(pDX, IDC_EDIT8, m_strPara2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CObjEditDlg, CDialog)
	//{{AFX_MSG_MAP(CObjEditDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjEditDlg message handlers

void CObjEditDlg::OnOK() 
{
	// TODO: Add extra validation here
	int  period = m_strText.Find('.');
	if ( period >= 0 ) {
		if ((m_strText.GetAt( period ) == 0x00) || (period == 0) || (m_strText.GetLength() <= period)) {
			AfxMessageBox("데이터 입력 오류입니다. 확인 및 재입력 후 '확인' 버튼을 눌러주십시오");
			return;
		}
	}
	CDialog::OnOK();
}

BOOL CObjEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
