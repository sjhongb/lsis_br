//=======================================================
//==              Signal.h
//=======================================================
//	파 일 명 :  Signal.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _SIGNAL_H
#define _SIGNAL_H

///////////////////////////////////////////////////////////////////////////////
//       a ------c------ b	c = m_Base
//			    |
//				 |
//		 	      d
///////////////////////////////////////////////////////////////////////////////

#include "GraphObj.h"

#define B_NAME_B		  0x1
#define B_NAME_S		  0x2
#define B_REVERSE		  0x4
#define B_OVERLAP_CK_STOP 0x8
#define B_NOROUTE		  0x80
#define B_ROUTE_LEFT	  0x100
#define B_ROUTE_RIGHT	  0x200
#define B_NOROUTE_ALL	  0x400
#define B_STRAIGHTLINE	  0x800
#define B_MASKBLOCKADE	  0x0f0
#define B_NONEBLOCKADE	  0

class CSignalDlg;
class StationDB;
class ostream;
class CSignal : public CGraphObject {
public:

	int m_nShape;
 	CBlip s;
	CBlip nB, nS;

protected:

	DECLARE_SERIAL(CSignal);

public:

	int  GetBlockadeMode() { return ((m_nShape & B_MASKBLOCKADE) >> 4); }
	BOOL isNoGrid(CBlip *pBlip);
	BOOL ButtonAt(CPoint point);
	void DrawDot(CDC *pDC, CPoint p);
	void Dump( ostream &os );
	void DrawRlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawGlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawYlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawSlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawClamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawLIlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int & iIndDis);
	void DrawIndlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis);
	void DrawFreelamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis); 
	int  m_nType, m_nLamp, m_nRoute, m_nUD, m_nLR;
	int  m_nILR; // 진로 표시등 (0: NONE 1: Left, 2: Right)
	int  m_iDiry;															   //입환 신호 모양 (상하) bmpinfo 상용 
	BOOL m_bF1, m_bF2;
	CString m_strStraightSignal;

	CSignal(int t=0, int cx=0, int cy=0);
	void	Init();
	void	Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));
	virtual CRect GetLampRect();
	virtual void UpdateRect();
	virtual void PropertyChange(CWnd *wnd);
	virtual void Read(CArchive &ar);
	virtual void Write(CArchive &ar);
	virtual const char *NameOf() { return "SIGNAL"; }
	virtual BOOL isValid(){ return Name != ""; }
	friend	CSignalDlg;
	friend	StationDB;
};

#endif