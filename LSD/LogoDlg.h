//=======================================================
//==              LogoDlg.h
//=======================================================
//	파 일 명 :  LogoDlg.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#if !defined(AFX_LOGODLG_H__A30704D8_28AC_4EAF_B67B_A9C83A6733D7__INCLUDED_)
#define AFX_LOGODLG_H__A30704D8_28AC_4EAF_B67B_A9C83A6733D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLogoDlg dialog

class CLogoDlg : public CDialog
{
// Construction
public:
	CLogoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogoDlg)
	enum { IDD = IDD_LOGODLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogoDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGODLG_H__A30704D8_28AC_4EAF_B67B_A9C83A6733D7__INCLUDED_)
