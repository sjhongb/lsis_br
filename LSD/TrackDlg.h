//=======================================================
//==              TrackDlg.h
//=======================================================
//	파 일 명 :  TrackDlg.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  궤도 회로의 정보를 입력하는 다이알로그
//
//=======================================================

#include "Track.h"
#define MAXBRENCHPOWER 256


//{{AFX_INCLUDES()
#include "gdiole.h"
//}}AFX_INCLUDES

class CTrackDlg : public CDialog
{
// Construction
	CTrack *m_pObject;
public:
	void UpdateDoubleSlipItem();
	CString ChangeChar(CString str , CString str1, int iIndex , int iLength = MAXBRENCHPOWER );
	CTrackDlg(CGraphObject *obj, CWnd* pParent = NULL);   // standard constructor
    CFrameWnd m_Frame;
    CCreateContext pContext;

// Dialog Data
	//{{AFX_DATA(CTrackDlg)
	enum { IDD = IDD_TRACK };
	CString	m_sName;
	BOOL	m_bOpen1;
	BOOL	m_bOpen2;
	BOOL	m_bOpen3;
	CString	m_sSigNameC1;
	CString	m_sSigNameC2;
	CString	m_sSigNameN1;
	CString	m_sSigNameN2;
	CString	m_sSigNameR1;
	CString	m_sSigNameR2;
	CString	m_sPointName;
	CString	m_sPointName2;
	CString	m_sLevelCrossName;
	BOOL	m_bName1;
	BOOL	m_bName2;
	BOOL	m_bName3;
	CString	m_sButtonName1;
	CString	m_sButtonName2;
	BOOL	m_bMain;
	BOOL	m_bCend1;
	BOOL	m_bCend2;
	BOOL	m_bCend3;
	BOOL	m_bIn1;
	BOOL	m_bIn2;
	BOOL	m_bIn3;
	BOOL	m_bOut1;
	BOOL	m_bOut2;
	BOOL	m_bOut3;
	BOOL	m_bTend1;
	BOOL	m_bTend2;
	BOOL	m_bTend3;
	BOOL	m_bNoTrack;
	BOOL	m_bDoubleSlip;
	BOOL	m_bTend4;
	BOOL	m_bOpen4;
	BOOL	m_bCend4;
	BOOL	m_bBrPosDown;
	BOOL	m_bBrPosLeft;
	BOOL	m_bBrPosRight;
	BOOL	m_bBrPosUp;
	CGDIOLE	m_Gdi1;
	BOOL	m_bKeyLock;
	BOOL	m_bDifTrack;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrackDlg)
	protected:
		virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTrackDlg)
	virtual void OnOK();
	afx_msg void OnCheckDoubleSlip();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioPwr1();
	afx_msg void OnRadioPwr2();
	afx_msg void OnRadioPwr3();
	afx_msg void OnRadioPwr4();
	afx_msg void OnChkNoroute();
	afx_msg void OnChkNoroute2();
	afx_msg void OnChkLosr();
	afx_msg void OnChkRdirfirst();
	afx_msg void OnChangeTrackPoint();
	afx_msg void OnChkHidtrack();
	afx_msg void OnRadioPwr5();
	afx_msg void OnRadioPwr6();
	afx_msg void OnRadioPwr7();
	afx_msg void OnRadioPwr8();
	afx_msg void OnRadioPwr9();
	afx_msg void OnRadioPwr10();
	afx_msg void OnChkMainline();
	afx_msg void OnChkPointnouse();
	afx_msg void OnChkLinkedPoint();
	afx_msg void OnChkLinkedPoint2();
	afx_msg void OnChkDestinationTrackUS1();
	afx_msg void OnChkDestinationTrackUS2();
	afx_msg void OnChkDestinationTrackUS3();
	afx_msg void OnChkDestinationTrackDS1();
	afx_msg void OnChkDestinationTrackDS2();
	afx_msg void OnChkDestinationTrackDS3();
	afx_msg void OnCheckOl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
