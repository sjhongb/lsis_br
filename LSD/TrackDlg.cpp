//=======================================================
//==              TrackDlg.cpp
//=======================================================
//	파 일 명 :  TrackDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "TrackDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
// CTrackDlg dialog

CTrackDlg::CTrackDlg(CGraphObject *obj, CWnd* pParent /*=NULL*/)
	: CDialog(CTrackDlg::IDD, pParent)
{
	m_pObject = (CTrack*)obj;
	//{{AFX_DATA_INIT(CTrackDlg)
	m_sName = CString(m_pObject->GetName());
	m_bOpen1 = FALSE;
	m_bOpen2 = FALSE;
	m_bOpen3 = FALSE;
	m_sSigNameC2 = _T("");
	m_sSigNameN1 = _T("");
	m_sSigNameN2 = _T("");
	m_sSigNameR1 = _T("");
	m_sSigNameR2 = _T("");
	m_bName1 = FALSE;
	m_bName2 = FALSE;
	m_bName3 = FALSE;
	m_sButtonName1 = _T("");
	m_sButtonName2 = _T("");
	m_bMain = FALSE;
	m_bCend1 = FALSE;
	m_bCend2 = FALSE;
	m_bCend3 = FALSE;
	m_bIn1 = FALSE;
	m_bIn2 = FALSE;
	m_bIn3 = FALSE;
	m_bOut1 = FALSE;
	m_bOut2 = FALSE;
	m_bOut3 = FALSE;
	m_bTend1 = FALSE;
	m_bTend2 = FALSE;
	m_bTend3 = FALSE;
	m_bNoTrack = FALSE;
	m_bDoubleSlip = FALSE;
	m_bTend4 = FALSE;
	m_bOpen4 = FALSE;
	m_bCend4 = FALSE;
	m_bBrPosDown = FALSE;
	m_bBrPosLeft = FALSE;
	m_bBrPosRight = FALSE;
	m_bBrPosUp = FALSE;
	m_bKeyLock = FALSE;
	m_bDifTrack = FALSE;
	//}}AFX_DATA_INIT
	m_bName1 = (m_pObject->m_nShape & B_NAME_C) != 0;
	m_bName2 = (m_pObject->m_nShape & B_NAME_N) != 0;
	m_bName3 = (m_pObject->m_nShape & B_NAME_R) != 0;
	m_bOpen1 = (m_pObject->m_nShape & B_OPEN_C) != 0;
	m_bOpen2 = (m_pObject->m_nShape & B_OPEN_N) != 0;
	m_bOpen3 = (m_pObject->m_nShape & B_OPEN_R) != 0;
	m_bCend1 = (m_pObject->m_nShape & B_CEND_C) != 0;
	m_bCend2 = (m_pObject->m_nShape & B_CEND_N) != 0;
	m_bCend3 = (m_pObject->m_nShape & B_CEND_R) != 0;
	m_bTend1 = (m_pObject->m_nShape & B_TEND_C) != 0;
	m_bTend2 = (m_pObject->m_nShape & B_TEND_N) != 0;
	m_bTend3 = (m_pObject->m_nShape & B_TEND_R) != 0;
	m_bIn1	 = (m_pObject->m_nShape & B_IN_C  ) != 0;
	m_bIn2   = (m_pObject->m_nShape & B_IN_N  ) != 0;
	m_bIn3   = (m_pObject->m_nShape & B_IN_R  ) != 0;
	m_bOut1  = (m_pObject->m_nShape & B_OUT_C ) != 0;
	m_bOut2  = (m_pObject->m_nShape & B_OUT_N ) != 0;
	m_bOut3  = (m_pObject->m_nShape & B_OUT_R ) != 0;

	m_bMain = (m_pObject->m_nShape & B_MAIN) != 0;
	m_bNoTrack = (m_pObject->m_nShape & B_NOTRACK) != 0;
	m_bDoubleSlip = (m_pObject->m_nShape & B_DOUBLE_SLIP) != 0;
	m_bDifTrack = (m_pObject->m_nShape & B_DIF_TRACK) != 0;	
	m_bOpen4 = (m_pObject->m_nShape & B_OPEN_E) != 0;
	m_bCend4 = (m_pObject->m_nShape & B_CEND_E) != 0;
	m_bTend4 = (m_pObject->m_nShape & B_TEND_E) != 0;

	m_bBrPosRight =	( m_pObject->m_nShape & B_BRANCH_POS_RIGHT ) != 0;
	m_bBrPosUp	  =	( m_pObject->m_nShape & B_BRANCH_POS_UP ) != 0;
	m_bBrPosLeft  =	( m_pObject->m_nShape & B_BRANCH_POS_LEFT ) != 0;
	m_bBrPosDown  =	( m_pObject->m_nShape & B_BRANCH_POS_DOWN ) != 0;
}


void CTrackDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrackDlg)
	DDX_Text(pDX, IDC_TRACKNAME, m_sName);
	DDV_MaxChars(pDX, m_sName, 20);
	DDX_Check(pDX, IDC_TRACK_OPEN1, m_bOpen1);
	DDX_Check(pDX, IDC_TRACK_OPEN2, m_bOpen2);
	DDX_Check(pDX, IDC_TRACK_OPEN3, m_bOpen3);
	DDX_Check(pDX, IDC_TRACK_NAME1, m_bName1);
	DDX_Check(pDX, IDC_TRACK_NAME2, m_bName2);
	DDX_Check(pDX, IDC_TRACK_NAME3, m_bName3);
	DDX_Check(pDX, IDC_TRACK_MAIN, m_bMain);
	DDX_Check(pDX, IDC_TRACK_CEND1, m_bCend1);
	DDX_Check(pDX, IDC_TRACK_CEND2, m_bCend2);
	DDX_Check(pDX, IDC_TRACK_CEND3, m_bCend3);
	DDX_Check(pDX, IDC_TRACK_IN1, m_bIn1);
	DDX_Check(pDX, IDC_TRACK_IN2, m_bIn2);
	DDX_Check(pDX, IDC_TRACK_IN3, m_bIn3);
	DDX_Check(pDX, IDC_TRACK_OUT1, m_bOut1);
	DDX_Check(pDX, IDC_TRACK_OUT2, m_bOut2);
	DDX_Check(pDX, IDC_TRACK_OUT3, m_bOut3);
	DDX_Check(pDX, IDC_TRACK_TEND1, m_bTend1);
	DDX_Check(pDX, IDC_TRACK_TEND2, m_bTend2);
	DDX_Check(pDX, IDC_TRACK_TEND3, m_bTend3);
	DDX_Check(pDX, IDC_NOTRACK, m_bNoTrack);
	DDX_Check(pDX, IDC_CHECK_DOUBLE_SLIP, m_bDoubleSlip);
	DDX_Check(pDX, IDC_DBLSLIP_ENDTRACK, m_bTend4);
	DDX_Check(pDX, IDC_DBLSLIP_NOINSULATE, m_bOpen4);
	DDX_Check(pDX, IDC_DBLSLIP_ENDLINE, m_bCend4);
	DDX_Check(pDX, IDC_BRPOS_DOWN, m_bBrPosDown);
	DDX_Check(pDX, IDC_BRPOS_LEFT, m_bBrPosLeft);
	DDX_Check(pDX, IDC_BRPOS_RIGHT, m_bBrPosRight);
	DDX_Check(pDX, IDC_BRPOS_UP, m_bBrPosUp);
	DDX_Check(pDX, IDC_CHECK1, m_bKeyLock);
	DDX_Check(pDX, IDC_DIF_TRACK, m_bDifTrack);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_TRACK_POINT, m_pObject->m_sBranchName);
	DDX_Text(pDX, IDC_TRACK_LEVELCORSS, m_pObject->m_cLevelCross.Name);
	DDX_Text(pDX, IDC_TRACK_SNAMEC1, m_pObject->m_cSignalC[0].Name);
	DDX_Text(pDX, IDC_TRACK_SNAMEC2, m_pObject->m_cSignalC[1].Name);
	DDX_Text(pDX, IDC_TRACK_SNAMEN1, m_pObject->m_cSignalN[0].Name);
	DDX_Text(pDX, IDC_TRACK_SNAMEN2, m_pObject->m_cSignalN[1].Name);
	DDX_Text(pDX, IDC_TRACK_SNAMR1, m_pObject->m_cSignalR[0].Name);
	DDX_Text(pDX, IDC_TRACK_SNAMR2, m_pObject->m_cSignalR[1].Name);
	DDX_Text(pDX, IDC_TRACK_BUTTON1, m_pObject->m_cButton[0].Name);
	DDX_Text(pDX, IDC_TRACK_BUTTON2, m_pObject->m_cButton[1].Name);
	DDX_Text(pDX, IDC_JOIN_C, m_pObject->nC.m_sBlipName);
	DDX_Text(pDX, IDC_JOIN_N, m_pObject->nN.m_sBlipName);
	DDX_Text(pDX, IDC_JOIN_R, m_pObject->nR.m_sBlipName);
}


BEGIN_MESSAGE_MAP(CTrackDlg, CDialog)
	//{{AFX_MSG_MAP(CTrackDlg)
	ON_BN_CLICKED(IDC_CHECK_DOUBLE_SLIP, OnCheckDoubleSlip)
	ON_BN_CLICKED(ID_RADIO_PWR1, OnRadioPwr1)
	ON_BN_CLICKED(ID_RADIO_PWR2, OnRadioPwr2)
	ON_BN_CLICKED(ID_RADIO_PWR3, OnRadioPwr3)
	ON_BN_CLICKED(ID_RADIO_PWR4, OnRadioPwr4)
	ON_BN_CLICKED(IDC_CHK_NOROUTE, OnChkNoroute)
	ON_BN_CLICKED(IDC_CHK_NOROUTE2, OnChkNoroute2)
	ON_BN_CLICKED(IDC_CHK_LOSR, OnChkLosr)
	ON_BN_CLICKED(IDC_CHK_RDIRFIRST, OnChkRdirfirst)
	ON_EN_CHANGE(IDC_TRACK_POINT, OnChangeTrackPoint)
	ON_BN_CLICKED(IDC_CHK_HIDTRACK, OnChkHidtrack)
	ON_BN_CLICKED(ID_RADIO_PWR5, OnRadioPwr5)
	ON_BN_CLICKED(ID_RADIO_PWR6, OnRadioPwr6)
	ON_BN_CLICKED(ID_RADIO_PWR7, OnRadioPwr7)
	ON_BN_CLICKED(ID_RADIO_PWR8, OnRadioPwr8)
	ON_BN_CLICKED(ID_RADIO_PWR9, OnRadioPwr9)
	ON_BN_CLICKED(ID_RADIO_PWR10, OnRadioPwr10)
	ON_BN_CLICKED(IDC_CHK_MAINLINE, OnChkMainline)
	ON_BN_CLICKED(IDC_CHK_POINTNOUSE, OnChkPointnouse)
	ON_BN_CLICKED(IDC_CHK_LINKED_POINT, OnChkLinkedPoint)
	ON_BN_CLICKED(IDC_CHK_LINKED_POINT2, OnChkLinkedPoint2)
///////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
//	ON_BN_CLICKED(IDC_CHK_US1, OnChkDestinationTrackUS1)
//	ON_BN_CLICKED(IDC_CHK_US2, OnChkDestinationTrackUS2)
// 	ON_BN_CLICKED(IDC_CHK_US3, OnChkDestinationTrackUS3)
// 	ON_BN_CLICKED(IDC_CHK_DS1, OnChkDestinationTrackDS1)
// 	ON_BN_CLICKED(IDC_CHK_DS2, OnChkDestinationTrackDS2)
// 	ON_BN_CLICKED(IDC_CHK_DS3, OnChkDestinationTrackDS3)
/////////////////////////////////////////////////////////////
	ON_BN_CLICKED(IDC_CHECK_OL, OnCheckOl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrackDlg message handlers

void CTrackDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	m_pObject->m_bKeyLock = m_bKeyLock;
	CString strDepPoint,strDepSignal;

	ChangeChar( m_pObject->m_sBranchPower , "E" , MAXBRENCHPOWER );
	if ( m_bKeyLock == TRUE ) m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"0",1);

	if (m_bDoubleSlip) {
		if (GetTokenTrackName(m_pObject->m_sBranchName.GetBuffer(1), NULL) != 2) {
			AfxMessageBox("입력 오류! \n더블 슬립 형태는 전철기명을 모두 기입하여야 합니다."
						  "\n입력전철기명은 안각, 외각순으로 기입하고 구분은 \',\'으로 합니다. \n다시 입력하십시오." );
			GetDlgItem(IDC_TRACK_POINT)->SetFocus();
			return;
		}

		m_pObject->m_nShape |= B_DOUBLE_SLIP;
	}
	else m_pObject->m_nShape &= ~B_DOUBLE_SLIP;
	if ( m_bDifTrack ) m_pObject->m_nShape |= B_DIF_TRACK;
	else m_pObject->m_nShape &= ~ B_DIF_TRACK;
	
	m_pObject->SetName((char*)(LPCTSTR)m_sName);

#define GetCheckOnOff( a, b ) if (a) m_pObject->m_nShape |= b; else m_pObject->m_nShape &= ~b;

	if (m_bName1) m_pObject->m_nShape |= B_NAME_C;
	else m_pObject->m_nShape &= ~B_NAME_C;
	if (m_bName2) m_pObject->m_nShape |= B_NAME_N;
	else m_pObject->m_nShape &= ~B_NAME_N;
	if (m_bName3) m_pObject->m_nShape |= B_NAME_R;
	else m_pObject->m_nShape &= ~B_NAME_R;

	if (m_bOpen1) m_pObject->m_nShape |= B_OPEN_C;
	else m_pObject->m_nShape &= ~B_OPEN_C;
	if (m_bOpen2) m_pObject->m_nShape |= B_OPEN_N;
	else m_pObject->m_nShape &= ~B_OPEN_N;
	if (m_bOpen3) m_pObject->m_nShape |= B_OPEN_R;
	else m_pObject->m_nShape &= ~B_OPEN_R;

	if (m_bCend1) m_pObject->m_nShape |= B_CEND_C;
	else m_pObject->m_nShape &= ~B_CEND_C;
	if (m_bCend2) m_pObject->m_nShape |= B_CEND_N;
	else m_pObject->m_nShape &= ~B_CEND_N;
	if (m_bCend3) m_pObject->m_nShape |= B_CEND_R;
	else m_pObject->m_nShape &= ~B_CEND_R;

	if (m_bTend1) m_pObject->m_nShape |= B_TEND_C;
	else m_pObject->m_nShape &= ~B_TEND_C;
	if (m_bTend2) m_pObject->m_nShape |= B_TEND_N;
	else m_pObject->m_nShape &= ~B_TEND_N;
	if (m_bTend3) m_pObject->m_nShape |= B_TEND_R;
	else m_pObject->m_nShape &= ~B_TEND_R;

	if (m_bIn1) m_pObject->m_nShape |= B_IN_C;
	else m_pObject->m_nShape &= ~B_IN_C;
	if (m_bIn2) m_pObject->m_nShape |= B_IN_N;
	else m_pObject->m_nShape &= ~B_IN_N;
	if (m_bIn3) m_pObject->m_nShape |= B_IN_R;
	else m_pObject->m_nShape &= ~B_IN_R;

	if (m_bOut1) m_pObject->m_nShape |= B_OUT_C;
	else m_pObject->m_nShape &= ~B_OUT_C;
	if (m_bOut2) m_pObject->m_nShape |= B_OUT_N;
	else m_pObject->m_nShape &= ~B_OUT_N;
	if (m_bOut3) m_pObject->m_nShape |= B_OUT_R;
	else m_pObject->m_nShape &= ~B_OUT_R;

	if (m_bMain) m_pObject->m_nShape |= B_MAIN;
	else m_pObject->m_nShape &= ~B_MAIN;
	if (m_bNoTrack) m_pObject->m_nShape |= B_NOTRACK;
	else m_pObject->m_nShape &= ~B_NOTRACK;

	if (m_bOpen4) m_pObject->m_nShape |= B_OPEN_E;
	else m_pObject->m_nShape &= ~B_OPEN_E;
	if (m_bCend4) m_pObject->m_nShape |= B_CEND_E;
	else m_pObject->m_nShape &= ~B_CEND_E;
	if (m_bTend4) m_pObject->m_nShape |= B_TEND_E;
	else m_pObject->m_nShape &= ~B_TEND_E;

    GetCheckOnOff( m_bBrPosRight, B_BRANCH_POS_RIGHT )
    GetCheckOnOff( m_bBrPosUp, B_BRANCH_POS_UP )
    GetCheckOnOff( m_bBrPosLeft, B_BRANCH_POS_LEFT )
    GetCheckOnOff( m_bBrPosDown, B_BRANCH_POS_DOWN )

	GetDlgItem(IDC_EDT_DEPENDSIGNALN)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,8);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,8);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,11);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,11);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN2)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,14);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,14);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR2)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,17);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,17);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN3)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,30);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,30);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR3)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,33);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,33);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN4)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,36);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,36);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR4)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,39);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,39);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN5)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,42);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,42);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR5)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,45);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,45);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN6)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,48);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,48);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR6)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,51);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,51);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALN7)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,54);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,54);	
	}

	GetDlgItem(IDC_EDT_DEPENDSIGNALR7)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,57);	
	} else {
		strDepSignal = "   ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,57);	
	}

	GetDlgItem(IDC_EDT_LINKEDPOINT)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,21);	
	} else {
		strDepSignal = "    ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,21);	
	}

	GetDlgItem(IDC_EDT_LINKEDPOINT2)->GetWindowText( strDepSignal );
    if ( strDepSignal !="" ) {
		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,221);	
	} else {
		strDepSignal = "    ";
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,221);	
	}

	GetDlgItem(IDC_EDT_DESTINATION_TRACK_US1)->GetWindowText( strDepSignal );
	int nBufferSize = strDepSignal.GetLength();
	for ( ; nBufferSize < 150 ; nBufferSize++)
	{
		strDepSignal = strDepSignal + " ";
	}
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,60);
///////////////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_US1)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,61);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,61);	
// 	}
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_US2)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,71);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,71);	
// 	}
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_US3)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,81);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,81);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_US1)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,66);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,66);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_US2)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,76);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,76);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_US3)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,86);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,86);	
// 	}
// 	
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS1)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,91);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,91);	
// 	}
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS2)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,101);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,101);	
// 	}
// 	GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS3)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,111);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,111);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_DS1)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,96);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,96);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_DS2)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,106);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,106);	
// 	}
// 	GetDlgItem(IDC_EDT_REFERENCE_POINT_DS3)->GetWindowText( strDepSignal );
//     if ( strDepSignal !="" ) {
// 		if ( strDepSignal.GetLength() == 1 ) strDepSignal = "   " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 2 ) strDepSignal = "  " + strDepSignal;
// 		if ( strDepSignal.GetLength() == 3 ) strDepSignal = " " + strDepSignal;
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,116);	
// 	} else {
// 		strDepSignal = "    ";
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,strDepSignal,116);	
// 	}
////////////////////////////////////////////////////////////////////////////////////////////////////

	GetDlgItem(IDC_EDT_DEPENDPOINTN)->GetWindowText( strDepPoint );
    if ( strDepPoint !="" ) {
		m_pObject->m_sBranchPower = m_pObject->m_sBranchPower + "@N_"+ strDepPoint;	
	}

	GetDlgItem(IDC_EDT_DEPENDPOINTR)->GetWindowText( strDepPoint );
    if ( strDepPoint !="" ) {
		m_pObject->m_sBranchPower = m_pObject->m_sBranchPower + "@R_"+ strDepPoint;	
	}

	CDialog::OnOK();
}

BOOL CTrackDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
     CWnd* pFrameWnd = this;
	 CString strPoint;
	 CString strDepPoint1,strDepPoint2;
	 int iFind;

	 iFind = m_pObject->m_sBranchPower.Find('@',0);
	 if ( iFind > 0 ) {
		strDepPoint1 = m_pObject->m_sBranchPower.Mid( iFind , m_pObject->m_sBranchPower.GetLength() - iFind);
		m_pObject->m_sBranchPower = m_pObject->m_sBranchPower.Left(iFind);

		iFind = strDepPoint1.Find('@',1);
		if ( iFind > 0 ) {
			strDepPoint2 = strDepPoint1.Mid( iFind , strDepPoint1.GetLength() - iFind);
			strDepPoint1 = strDepPoint1.Left(iFind);
			if ( strDepPoint2.Left(2) == "@N" ) GetDlgItem(IDC_EDT_DEPENDPOINTN)->SetWindowText( strDepPoint2.Right(strDepPoint2.GetLength()-3) );
			if ( strDepPoint2.Left(2) == "@R" ) GetDlgItem(IDC_EDT_DEPENDPOINTR)->SetWindowText( strDepPoint2.Right(strDepPoint2.GetLength()-3) );

		}

		if ( strDepPoint1.Left(2) == "@N" ) GetDlgItem(IDC_EDT_DEPENDPOINTN)->SetWindowText( strDepPoint1.Right(strDepPoint1.GetLength()-3) );
		if ( strDepPoint1.Left(2) == "@R" ) GetDlgItem(IDC_EDT_DEPENDPOINTR)->SetWindowText( strDepPoint1.Right(strDepPoint1.GetLength()-3) );
	 }


	 m_bKeyLock = m_pObject->m_bKeyLock;
	 m_pObject->m_sBranchPower = ChangeChar( m_pObject->m_sBranchPower , "E" , MAXBRENCHPOWER );

	 if ( m_pObject->m_sBranchPower.Mid(1,1) == "1" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR1))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "2" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR2))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "A" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR3))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "B" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR4))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "C" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR5))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "D" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR6))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "E" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR7))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "F" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR8))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "G" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR9))->SetCheck(TRUE);
	 } else if ( m_pObject->m_sBranchPower.Mid(1,1) == "H" ){
		 ((CButton*)GetDlgItem(ID_RADIO_PWR10))->SetCheck(TRUE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(2,1) == "N" ) {
		 ((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->SetCheck(TRUE);
	 } else if (m_pObject->m_sBranchPower.Mid(2,1) == "R" ){
		 ((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->SetCheck(TRUE);
	 }else {
		((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(3,1) == "L" ) {
		((CButton*)GetDlgItem(IDC_CHK_LOSR))->SetCheck(TRUE);
	 } else {
		((CButton*)GetDlgItem(IDC_CHK_LOSR))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(4,1) == "R" ) {
		((CButton*)GetDlgItem(IDC_CHK_RDIRFIRST))->SetCheck(TRUE);
	 } else {
		((CButton*)GetDlgItem(IDC_CHK_RDIRFIRST))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(5,1) == "H" ) {
		((CButton*)GetDlgItem(IDC_CHK_HIDTRACK))->SetCheck(TRUE);
	 } else {
		((CButton*)GetDlgItem(IDC_CHK_HIDTRACK))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(6,1) == "M" ) { // Main 
		((CButton*)GetDlgItem(IDC_CHK_MAINLINE))->SetCheck(TRUE);
	 } else {
		((CButton*)GetDlgItem(IDC_CHK_MAINLINE))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(7,1) == "N" ) { // Main 
		((CButton*)GetDlgItem(IDC_CHK_POINTNOUSE))->SetCheck(TRUE);
	 } else {
		((CButton*)GetDlgItem(IDC_CHK_POINTNOUSE))->SetCheck(FALSE);
	 }

	 if ( m_pObject->m_sBranchPower.Mid(8,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN)->SetWindowText(m_pObject->m_sBranchPower.Mid(8,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(11,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR)->SetWindowText(m_pObject->m_sBranchPower.Mid(11,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(14,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN2)->SetWindowText(m_pObject->m_sBranchPower.Mid(14,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(17,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR2)->SetWindowText(m_pObject->m_sBranchPower.Mid(17,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(25,1) == "T" ) { // True T/F
		 ((CButton*)GetDlgItem(IDC_CHECK_OL))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHECK_OL))->SetCheck(FALSE);
	 }
	 
	 if ( m_pObject->m_sBranchPower.Mid(30,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN3)->SetWindowText(m_pObject->m_sBranchPower.Mid(30,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(33,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR3)->SetWindowText(m_pObject->m_sBranchPower.Mid(33,3));
	 }
	 
	 if ( m_pObject->m_sBranchPower.Mid(36,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN4)->SetWindowText(m_pObject->m_sBranchPower.Mid(36,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(39,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR4)->SetWindowText(m_pObject->m_sBranchPower.Mid(39,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(42,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN5)->SetWindowText(m_pObject->m_sBranchPower.Mid(42,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(45,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR5)->SetWindowText(m_pObject->m_sBranchPower.Mid(45,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(48,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN6)->SetWindowText(m_pObject->m_sBranchPower.Mid(48,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(51,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR6)->SetWindowText(m_pObject->m_sBranchPower.Mid(51,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(54,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALN7)->SetWindowText(m_pObject->m_sBranchPower.Mid(54,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(57,3) != "   " ) { // Main 
		GetDlgItem(IDC_EDT_DEPENDSIGNALR7)->SetWindowText(m_pObject->m_sBranchPower.Mid(57,3));
	 }

	 if ( m_pObject->m_sBranchPower.Mid(20,1) == "P" ) { // Main 
		 ((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(21,4) != "   " ) { // Main 
		 GetDlgItem(IDC_EDT_LINKEDPOINT)->SetWindowText(m_pObject->m_sBranchPower.Mid(21,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(220,1) == "P" ) { // Main 
		 ((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT2))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT2))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(221,4) != "   " ) { // Main 
		 GetDlgItem(IDC_EDT_LINKEDPOINT2)->SetWindowText(m_pObject->m_sBranchPower.Mid(221,4));
	 }

	 int nBufferLength = 150;
	 CString strBuffer = m_pObject->m_sBranchPower.Mid(60,150);
	 for( int nCount = 150; nCount > 0; nCount--)
	 {
		 if(strBuffer.Right(1) == ' ')
		 {
			 strBuffer = strBuffer.Left(nCount-1);
		 }
	 }
	 GetDlgItem(IDC_EDT_DESTINATION_TRACK_US1)->SetWindowText(strBuffer);
	 /*//////////////////////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
	 if ( m_pObject->m_sBranchPower.Mid(60,1) == "U" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_US1))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_US1))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(61,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_US1)->SetWindowText(m_pObject->m_sBranchPower.Mid(61,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(66,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_US1)->SetWindowText(m_pObject->m_sBranchPower.Mid(66,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(70,1) == "U" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_US2))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_US2))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(71,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_US2)->SetWindowText(m_pObject->m_sBranchPower.Mid(71,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(76,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_US2)->SetWindowText(m_pObject->m_sBranchPower.Mid(76,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(80,1) == "U" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_US3))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_US3))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(81,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_US3)->SetWindowText(m_pObject->m_sBranchPower.Mid(81,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(86,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_US3)->SetWindowText(m_pObject->m_sBranchPower.Mid(86,4));
	 }
	 
	 if ( m_pObject->m_sBranchPower.Mid(90,1) == "D" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_DS1))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_DS1))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(91,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS1)->SetWindowText(m_pObject->m_sBranchPower.Mid(91,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(96,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_DS1)->SetWindowText(m_pObject->m_sBranchPower.Mid(96,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(100,1) == "D" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_DS2))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_DS2))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(101,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS2)->SetWindowText(m_pObject->m_sBranchPower.Mid(101,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(106,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_DS2)->SetWindowText(m_pObject->m_sBranchPower.Mid(106,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(110,1) == "D" ) { 
		 ((CButton*)GetDlgItem(IDC_CHK_DS3))->SetCheck(TRUE);
	 } else {
		 ((CButton*)GetDlgItem(IDC_CHK_DS3))->SetCheck(FALSE);
	 }
	 if ( m_pObject->m_sBranchPower.Mid(111,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_DESTINATION_TRACK_DS3)->SetWindowText(m_pObject->m_sBranchPower.Mid(111,4));
	 }
	 if ( m_pObject->m_sBranchPower.Mid(116,4) != "   " ) { 
		 GetDlgItem(IDC_EDT_REFERENCE_POINT_DS3)->SetWindowText(m_pObject->m_sBranchPower.Mid(116,4));
	 }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

	 GetDlgItem(IDC_TRACK_POINT)->GetWindowText( strPoint );


	if ( strPoint != "" ) {
		GetDlgItem(IDC_STATIC_POINT)->SetWindowText( m_pObject->m_sBranchName );
		GetDlgItem(IDC_EDT_DEPENDPOINTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDT_DEPENDPOINTR)->EnableWindow(TRUE);
	} else {
		GetDlgItem(IDC_STATIC_POINT)->SetWindowText( m_pObject->m_sBranchName );
		GetDlgItem(IDC_EDT_DEPENDPOINTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDT_DEPENDPOINTR)->EnableWindow(FALSE);
	}

	 UpdateData(FALSE);
	 UpdateDoubleSlipItem();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTrackDlg::UpdateDoubleSlipItem()
{
	if (m_bDoubleSlip) {
		GetDlgItem(IDC_TRACK_NAME1)->EnableWindow(FALSE);
		GetDlgItem(IDC_DBL_FLAG_TITLE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DBLSLIP_ENDTRACK)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DBLSLIP_NOINSULATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DBLSLIP_ENDLINE)->ShowWindow(SW_SHOW);
	}
	else {
		GetDlgItem(IDC_TRACK_NAME1)->EnableWindow(TRUE);
		GetDlgItem(IDC_DBL_FLAG_TITLE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DBLSLIP_ENDTRACK)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DBLSLIP_NOINSULATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DBLSLIP_ENDLINE)->ShowWindow(SW_HIDE);
	}
}

void CTrackDlg::OnCheckDoubleSlip() 
{
	
}


CString CTrackDlg::ChangeChar(CString str , CString str1 , int iIndex , int iLength)
{
	int i,iStrLength;
	CString strFirst, strLast;
	iStrLength = str.GetLength();

	if ( iStrLength > iLength ) str = str.Left(iLength);

	if ( iStrLength < iLength ) {
		for ( i =  iStrLength ; i < iLength ; i++ ) {
			str = str+" ";
		}
	}

	strFirst = str.Left(iIndex);
	if (( str.GetLength() - iIndex ) <= 0 ) strLast = "";
	else strLast = str.Mid(iIndex+str1.GetLength(),str.GetLength() - iIndex);

	str = strFirst + str1 + strLast;
	
	return str;
}

void CTrackDlg::OnRadioPwr1() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"1",1);
}

void CTrackDlg::OnRadioPwr2() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"2",1);
}

void CTrackDlg::OnRadioPwr3() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"A",1);
}

void CTrackDlg::OnRadioPwr4() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"B",1);
}

void CTrackDlg::OnRadioPwr5() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"C",1);	
}

void CTrackDlg::OnRadioPwr6() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"D",1);	
}

void CTrackDlg::OnRadioPwr7() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"E",1);	
}

void CTrackDlg::OnRadioPwr8() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"F",1);	
}

void CTrackDlg::OnRadioPwr9() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"G",1);	
}

void CTrackDlg::OnRadioPwr10() 
{
	m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"H",1);	
}

void CTrackDlg::OnChkNoroute() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->GetCheck() == TRUE ) {
		if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->GetCheck() == TRUE ) {
			((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->SetCheck(FALSE);
		}
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"N",2);
	}
	else {
		if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->GetCheck() == FALSE ) {
			m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",2);
		}	
	}
}

void CTrackDlg::OnChkNoroute2() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE2))->GetCheck() == TRUE ) {
		if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->GetCheck() == TRUE ) {
			((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->SetCheck(FALSE);
		}
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"R",2);
	}
	else {
		if (((CButton*)GetDlgItem(IDC_CHK_NOROUTE))->GetCheck() == FALSE ) {
			m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",2);
		}	
	}
}


void CTrackDlg::OnChkLinkedPoint() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"P",20);
	}
	else if (((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT))->GetCheck() == FALSE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",20);	
	}	
}

void CTrackDlg::OnChkLinkedPoint2() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT2))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"P",220);
	}
	else if (((CButton*)GetDlgItem(IDC_CHK_LINKED_POINT2))->GetCheck() == FALSE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",220);	
	}	
}
///////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap표현방식변경.
// void CTrackDlg::OnChkDestinationTrackUS1() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_US1))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"U",60);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_US1))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",60);	
// 	}	
// }
// void CTrackDlg::OnChkDestinationTrackUS2() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_US2))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"U",70);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_US2))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",70);	
// 	}	
// }
// void CTrackDlg::OnChkDestinationTrackUS3() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_US3))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"U",80);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_US3))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",80);	
// 	}	
// }
// void CTrackDlg::OnChkDestinationTrackDS1() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_DS1))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"D",90);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_DS1))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",90);	
// 	}	
// }
// void CTrackDlg::OnChkDestinationTrackDS2() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_DS2))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"D",100);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_DS2))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",100);	
// 	}	
// }
// void CTrackDlg::OnChkDestinationTrackDS3() 
// {
// 	if (((CButton*)GetDlgItem(IDC_CHK_DS3))->GetCheck() == TRUE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"D",110);
// 	}
// 	else if (((CButton*)GetDlgItem(IDC_CHK_DS3))->GetCheck() == FALSE ) {
// 		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",110);	
// 	}	
// }
////////////////////////////////////////////////////////////////////////////////////////////////

void CTrackDlg::OnChkLosr() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_LOSR))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"L",3);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower," ",3);
	}	
}


void CTrackDlg::OnChkRdirfirst() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_RDIRFIRST))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"R",4);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"N",4);
	}		
}

void CTrackDlg::OnChkHidtrack() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_HIDTRACK))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"H",5);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"S",5);
	}			
}

void CTrackDlg::OnChkMainline() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_MAINLINE))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"M",6);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"L",6);
	}				
}

void CTrackDlg::OnChkPointnouse() 
{
	if (((CButton*)GetDlgItem(IDC_CHK_POINTNOUSE))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"U",7);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"N",7);
	}					
}

void CTrackDlg::OnCheckOl() 
{
	if (((CButton*)GetDlgItem(IDC_CHECK_OL))->GetCheck() == TRUE ) {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"T",25);
	}
	else {
		m_pObject->m_sBranchPower = ChangeChar(m_pObject->m_sBranchPower,"F",25);
	}						
}

void CTrackDlg::OnChangeTrackPoint() 
{
	CString strPoint, strDepN, strDepR;

	GetDlgItem(IDC_TRACK_POINT)->GetWindowText( strPoint );

	if ( strPoint != "" ) {
		GetDlgItem(IDC_STATIC_POINT)->SetWindowText( strPoint );
		GetDlgItem(IDC_EDT_DEPENDPOINTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDT_DEPENDPOINTR)->EnableWindow(TRUE);
	} else {
		GetDlgItem(IDC_STATIC_POINT)->SetWindowText( strPoint );
		GetDlgItem(IDC_EDT_DEPENDPOINTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDT_DEPENDPOINTR)->EnableWindow(FALSE);
	}
}





