//=======================================================
//==              RouteLine.h
//=======================================================
//	파 일 명 :  RouteLine.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _ROUTELINE_H
#define _ROUTELINE_H

#include <iostream.h>
#include <fstream.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
#define MAX_POINT_LIST		100
#define MAX_STACK_LIST		100

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CRouteLine : public CObject 
{
protected:
	DECLARE_SERIAL(CRouteLine);

protected:
	CString m_sRoute;
	CPoint  m_Points[MAX_POINT_LIST];
	UINT m_nCount;

protected:
	UINT m_nStackArray[MAX_STACK_LIST];
	UINT m_nStack;

public:
	CRouteLine();
	CRouteLine(CPoint *&p, UINT count, const char *s="");
	~CRouteLine();

	void Out( ostream &os );
	void Init();
	void SetRoute(const char *s);
	BOOL AddPoint(CPoint &p)
	{
		return AddPoint(p.x, p.y);
	}
	BOOL AddPoint(POINT initPt);
	BOOL AddPoint(SIZE	initSize);
	BOOL AddPoint(int initX, int initY);
	void Push();
	void Pop();

	BOOL m_bValid; 
public:
	virtual void Serialize(CArchive &ar);
	BOOL isValid() { return m_bValid; }
	virtual const char *NameOf() { return "ROUELINE"; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // _ROUTELINE_H

