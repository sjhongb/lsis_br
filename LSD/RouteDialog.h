//=======================================================
//==              RouteDialog.h
//=======================================================
//	파 일 명 :  RouteDialog.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CRouteDialog : public CDialog
{
// Construction
public:
	void UpdateRoute( CString &pStrRoute );
	CRouteDialog(CWnd* pParent = NULL);   // standard constructor
	BOOL SetTrans(int iVal);
// Dialog Data
	//{{AFX_DATA(CRouteDialog)
	enum { IDD = IDD_DIALOG_ROUTE };
	CListCtrl	m_ListRoute;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRouteDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRouteDialog)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
