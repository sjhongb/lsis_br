//=======================================================
//====               ButtonDl.cpp                    ==== 
//=======================================================
//  파  일  명: ButtonDl.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 압구에 관한 정보를 입력하는 화면 
//              
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "ButtonDl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyButtonDlg dialog

CMyButtonDlg::CMyButtonDlg(CGraphObject *obj, CWnd* pParent /*=NULL*/)
	: CDialog(CMyButtonDlg::IDD, pParent)
{
	m_pObject = (CMyButton*)obj;
	//{{AFX_DATA_INIT(CMyButtonDlg)
	m_sName = CString(m_pObject->GetName());
	m_bName1 = FALSE;
	m_nLine = 0;
	m_bCont = FALSE;
	m_bNoCallon = FALSE;
	//}}AFX_DATA_INIT
	m_bName1 = (m_pObject->m_nShape & B_NAME_B) != 0;
	m_bCont = (m_pObject->m_nShape & B_CONT) != 0;
	m_bNoCallon = (m_pObject->m_nShape & B_NOCALLON) != 0;
}

void CMyButtonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyButtonDlg)
	DDX_Control(pDX, IDC_COMBO_LINE, m_cLineNo);
	DDX_Text(pDX, IDC_BUTTONNAME, m_sName);
	DDV_MaxChars(pDX, m_sName, 20);
	DDX_Check(pDX, IDC_BUTTON_NAME1, m_bName1);
	DDX_Check(pDX, IDC_BUTTON_CONT, m_bCont);
	DDX_Check(pDX, IDC_BUTTON_NOCALLON, m_bNoCallon);
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_BUTTONRADIO1, m_pObject->m_nType);
	DDX_Radio(pDX, IDC_BUTTONDIR2, m_pObject->m_nLR);
}

BEGIN_MESSAGE_MAP(CMyButtonDlg, CDialog)
	//{{AFX_MSG_MAP(CMyButtonDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyButtonDlg message handlers

void CMyButtonDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	m_pObject->m_nLine = m_cLineNo.GetCurSel();
	if (m_bName1) m_pObject->m_nShape |= B_NAME_B;
	else m_pObject->m_nShape &= ~B_NAME_B;
	if (m_bCont) m_pObject->m_nShape |= B_CONT;
	else m_pObject->m_nShape &= ~B_CONT;
	if (m_bNoCallon) m_pObject->m_nShape |= B_NOCALLON;
	else m_pObject->m_nShape &= ~B_NOCALLON;

	CDialog::OnOK();
}

BOOL CMyButtonDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_cLineNo.SetCurSel(m_pObject->m_nLine);
	return TRUE;
}
