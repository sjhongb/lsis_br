// PreView.cpp : implementation file
//

#include "stdafx.h"
#include "LSD.h"
#include "PreView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreView

IMPLEMENT_DYNCREATE(CPreView, CView)

CPreView::CPreView()
{
}

CPreView::~CPreView()
{
}


BEGIN_MESSAGE_MAP(CPreView, CView)
	//{{AFX_MSG_MAP(CPreView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreView drawing

void CPreView::OnDraw(CDC* pDC)
{
	// TODO: add draw code here
	pObj->Draw( pDC );
}

/////////////////////////////////////////////////////////////////////////////
// CPreView diagnostics

#ifdef _DEBUG
void CPreView::AssertValid() const
{
	CView::AssertValid();
}

void CPreView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPreView message handlers

void CPreView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	Invalidate();
	// TODO: Add your specialized code here and/or call the base class
	m_nZoomFactor = 50;
}

BOOL CPreView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	return TRUE;
	//return CView::PreTranslateMessage(pMsg);
}

void CPreView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	CView::OnPrepareDC(pDC, pInfo);

	// TODO: Add your specialized code here and/or call the base class

	CRect rect,rEct;
	int x,y,dx,dy;
	rect = pObj->GetRect();
	GetClientRect(&rEct);

	pDC->SetMapMode(MM_TEXT);
	dx = pDC->GetDeviceCaps(LOGPIXELSX);
	dy = pDC->GetDeviceCaps(LOGPIXELSY);

	x = ((rect.left - rect.right)/2)+rect.right - rEct.right/2;
	y = (rect.bottom - rect.top)/2+rect.top - rEct.bottom/2;

	pDC->SetWindowOrg(x,y);
}
