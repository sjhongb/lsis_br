//=======================================================
//==              Develop.cpp
//=======================================================
//	파 일 명 :  Develop.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  도면 장성자의 정보를 처리한다.
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "Develop.h"
#include "LSDDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Develop dialog

Develop::Develop(CWnd* pParent /*=NULL*/)
	: CDialog(Develop::IDD, pParent)
{
	//{{AFX_DATA_INIT(Develop)
	m_EditDev1 = _T("");
	m_EditDev2 = _T("");
	m_EditDev3 = _T("");
	m_EditDev4 = _T("");
	m_EditDev5 = _T("");
	m_EditDev6 = _T("");
	m_EditDev7 = _T("");
	m_EditDev8 = _T("");
	m_EditDev9 = _T("");
	m_EditDev10 = _T("");
	m_EditDev11 = _T("");
	m_EditDev12 = _T("");
	m_EditDev13 = _T("");
	m_EditDev14 = _T("");
	m_EditDev15 = _T("");
	m_EditDev16 = _T("");
	m_EditDev17 = _T("");
	m_EditDev18 = _T("");
	m_EditDev19 = _T("");
	m_EditDev20 = _T("");
	//}}AFX_DATA_INIT

}


void Develop::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Develop)
	DDX_Text(pDX, IDC_EditDEV1, m_EditDev1);
	DDX_Text(pDX, IDC_EditDEV2, m_EditDev2);
	DDX_Text(pDX, IDC_EditDEV3, m_EditDev3);
	DDX_Text(pDX, IDC_EditDEV4, m_EditDev4);
	DDX_Text(pDX, IDC_EditDEV5, m_EditDev5);
	DDX_Text(pDX, IDC_EditDEV6, m_EditDev6);
	DDX_Text(pDX, IDC_EditDEV7, m_EditDev7);
	DDX_Text(pDX, IDC_EditDEV8, m_EditDev8);
	DDX_Text(pDX, IDC_EditDEV9, m_EditDev9);
	DDX_Text(pDX, IDC_EditDEV10, m_EditDev10);
	DDX_Text(pDX, IDC_EditDEV11, m_EditDev11);
	DDX_Text(pDX, IDC_EditDEV12, m_EditDev12);
	DDX_Text(pDX, IDC_EditDEV13, m_EditDev13);
	DDX_Text(pDX, IDC_EditDEV14, m_EditDev14);
	DDX_Text(pDX, IDC_EditDEV15, m_EditDev15);
	DDX_Text(pDX, IDC_EditDEV16, m_EditDev16);
	DDX_Text(pDX, IDC_EditDEV17, m_EditDev17);
	DDX_Text(pDX, IDC_EditDEV18, m_EditDev18);
	DDX_Text(pDX, IDC_EditDEV19, m_EditDev19);
	DDX_Text(pDX, IDC_EditDEV20, m_EditDev20);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Develop, CDialog)
	//{{AFX_MSG_MAP(Develop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Develop message handlers

void Develop::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	__editdev1 = m_EditDev1; 
	__editdev2 = m_EditDev2; 
	__editdev3 = m_EditDev3; 
	__editdev4 = m_EditDev4; 
	__editdev5 = m_EditDev5; 
	__editdev6 = m_EditDev6; 
	__editdev7 = m_EditDev7; 
	__editdev8 = m_EditDev8; 
	__editdev9 = m_EditDev9; 
	__editdev10 = m_EditDev10; 
	__editdev11 = m_EditDev11; 
	__editdev12 = m_EditDev12; 
	__editdev13 = m_EditDev13; 
	__editdev14 = m_EditDev14; 
	__editdev15 = m_EditDev15; 
	__editdev16 = m_EditDev16; 
	__editdev17 = m_EditDev17; 
	__editdev18 = m_EditDev18; 
	__editdev19 = m_EditDev19; 
	__editdev20 = m_EditDev20; 	
	CDialog::OnOK();
}

BOOL Develop::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CTime cTime = CTime::GetCurrentTime();

    m_EditDev1 = __editdev1;
	m_EditDev2 = __editdev2;
	m_EditDev3 = __editdev3;
	m_EditDev4 = __editdev4;
	m_EditDev5 = __editdev5;
	m_EditDev6 = __editdev6;
	m_EditDev7 = __editdev7;	
    m_EditDev8 = __editdev8;
	m_EditDev9 = __editdev9;
	m_EditDev10 = __editdev10;
	m_EditDev11 = __editdev11;
	m_EditDev12 = __editdev12;
	m_EditDev13 = __editdev13;
	m_EditDev14 = __editdev14;	
    m_EditDev15 = __editdev15;
	m_EditDev16 = __editdev16;
	m_EditDev17 = __editdev17;
	m_EditDev18 = __editdev18;
	m_EditDev19 = __editdev19;
	m_EditDev20 = __editdev20;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
