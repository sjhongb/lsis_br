//=======================================================
//==              GraphObj.h
//=======================================================
//	파 일 명 :  GraphObj.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _GRAPHOBJ_H
#define _GRAPHOBJ_H

#include "Blip.h"

extern int GridSize, GridSizeH;

class CLSDDoc;
class CLSDView;
class TScrobj;

class CGraphObject : public CObject, public CBlip {
protected:
	DECLARE_SERIAL(CGraphObject);
	CBlip *m_pBlips[20];

	CRgn m_Rgn;

public:

	TScrobj *m_pBmpInfo;
	BOOL	m_bIsBmpMode;

	CString Name;
	CRect m_position;
	CLSDDoc *m_pDocument;
	CLSDView *m_pView;
	int m_nHandle;

	virtual void Dump(CDumpContext& dc);

public:
	virtual const char *NameOf() { return "GRAPHOBJ"; }
	virtual CRect GetRect() { 
		UpdateRect();
		return m_position; 
	}
	virtual int  BlipType(CBlip* Handle);	// 0/Track(N,C,R), 2/Buttons(Base), 3/Signals, 4/Names
	virtual void Offset(const CPoint point, CLSDView* pView = NULL);
	void MoveHandleTo(CBlip* Handle, CPoint point, CLSDView* pView);
	virtual CRect GetHandleRect(int nHandleID, CLSDView* pView);
	virtual CBlip* HitTest(CPoint point, CLSDView* pView, BOOL bSelected);
	virtual void Remove();
	virtual CGraphObject* Clone(CLSDDoc* pDoc);
	virtual CGraphObject* SaveClone(CLSDDoc* pDoc);
	virtual void Invalidate();
	virtual CPoint GetHandle(int nHandle);
	virtual int GetHandleCount();
	virtual BOOL Intersects(const CRect& rect);
	virtual void UpdateRect();
	virtual void PropertyChange(CWnd *wnd) {};
	virtual void Init() {}
	CGraphObject() {
		m_pView = NULL;
		m_nHandle = 0;
		m_pBmpInfo = NULL;
	}
	CGraphObject(const CRect& position, char *n = "") {
		m_position = position;
		m_pDocument = NULL;
		m_pView = NULL;
		Name = CString(n);
		m_nHandle = 0;
		m_pBmpInfo = NULL;
	}
	void SetName(char *n) { 
		Name = CString(n);
	}
	char *GetName() { return (char*)(LPCTSTR)Name; }
	void DrawName(CDC *pDC);

// Operations
	virtual void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));
	virtual void DrawTracker(CDC *pDC, TrackerState state);
	virtual void Read(CArchive &ar) {}
	virtual void Write(CArchive &ar) {}
	friend CArchive& operator >> (CArchive &ar, CGraphObject &obj) {
		obj.Read(ar);
		return ar; 
	}
	friend CArchive& operator << (CArchive &ar, CGraphObject &obj) { 
		obj.Write(ar);
		return ar; 
	}
	virtual ~CGraphObject();
	virtual void Serialize(CArchive &ar);
};

void swap(CPoint &a, CPoint &b);
void TextOutAtPointCenter( CDC *pDC, CPoint &point, CString &string ,COLORREF cColor = RGB(0,0,0));

#endif