//=======================================================
//==              STOBJECT.CPP
//=======================================================
//	파 일 명 :  STOBJECT.CPP
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "Track.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "StObject.h"

void CStationObject::Dump(CDumpContext& dc) const
{
	dc << "TRACK:{";
	m_Track.Dump( dc );
	dc << "}\n";
	//  신호기 목록 순서 알고리즘에 따라 TableDoc.cpp -> Export()에서 Dump()를 대신함.
	dc << "SIGNAL:{";
	m_Signal.Dump( dc );
	dc << "}\n";

	dc << "SWITCH:{";
	m_Switch.Dump( dc );
	dc << "}\n";

	dc << "BUTTON:{";
	m_ArrivedButton.Dump( dc );
	dc << "}\n";

//	if ( m_Lamp.GetCount() > 0 ) {
//		dc << "LAMP:{";
//		m_Lamp.Dump( dc );
//		dc << "}\n";
//	}
}

void CStationObject::AddSwitch( CString &str )
{
	m_Switch.CheckAdd( str, TRUE );
}

void CStationObject::AddSignal( CString &str )
{
	int a;
	if ( str.Find("11") > 0 ) {
	a = 1;
	}
	m_Signal.CheckAdd( str, TRUE );
}

void CStationObject::AddCross( CString &str )
{
	m_Cross.CheckAdd( str, TRUE );
}

void CStationObject::AddTrack( CString &str )
{
	CString newstr = str;
	short n = str.Find('.');
	if (n>0) newstr = str.Left( n );
		m_Track.CheckAdd( newstr, TRUE );
}

void CStationObject::AddButton( CString &str )
{
	int iTemp;
	iTemp = str.Find("(",0);  // 버튼명의 (M),(M)-1,(C),(C)-1... 을 제거하기 위함 

	m_ArrivedButton.CheckAdd( str.Left(iTemp) , TRUE );
}

void CStationObject::AddLamp( CString &str )
{
	m_Lamp.CheckAdd( str, TRUE );
}

void CStationObject::Sort()
{
	m_Switch.Sort( 0 );
	m_Signal.Sort( 0 );
	m_Track.Sort( 0 );
	m_ArrivedButton.Sort( 0 );
}
