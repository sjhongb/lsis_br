//=======================================================
//==              mycdc.cpp
//=======================================================
//	파 일 명 :  mycdc.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "mycdc.h"

#include "jsdxf.h"
#include "tablview.h"

#define DXFSCALE (1.0/7.1)

BOOL CMyDc::Rectangle( LPCRECT lpRect ) {
	if ( MyDxf.m_pOutFile ) {
		double w = (double)(lpRect->right - lpRect->left);
		double h = (double)(lpRect->top - lpRect->bottom);
		double x = lpRect->left;
		double y = lpRect->top;
		MyLPtoDP( x, y );
		MyDxf.fAddRect( x, y, w * DXFSCALE, h * DXFSCALE );
	}
	return CDC::Rectangle( lpRect );
}

int CMyDc::DrawText( const CString& str, LPRECT lpRect, UINT nFormat ) {
	return CMyDc::DrawText((LPCTSTR)str, str.GetLength(), lpRect, nFormat);
//	return CDC::DrawText( str, lpRect, nFormat );
}

int CMyDc::DrawText( LPCTSTR lpszString, int nCount, LPRECT lpRect, UINT nFormat ) {
	if ( MyDxf.m_pOutFile ) {
		double x, y;
		UINT mode = CDC::GetTextAlign();
		int al = 0;
		if ( mode == TA_UPDATECP ) {
			CPoint p = GetCurrentPosition();
			x = p.x;
			y = p.y;
			al = 1000;
		}
		else {
			x = (double)(lpRect->right + lpRect->left) / 2;
			y = (double)(lpRect->top + lpRect->bottom) / 2;
		}
		MyLPtoDP( x, y );
		char *bf = strdup( lpszString );
		char *dest = bf;
		char *src = (char*)lpszString;
		while (*src) {
			*dest++ = *src++; 
			if ( *src == '&' || *src == '*') {
				if (*(src-1) == '&' || *(src-1) == '*') src++;
			}
		}
		*dest = 0;
		MyDxf.fAddText( bf, x, y - TEXTHEIGHT/2, al + TEXTHEIGHT );
		free( bf );
	}
	return CDC::DrawText( lpszString, nCount, lpRect, nFormat );
}
/*
UINT CMyDc::SetTextAlign( UINT nFlags ) {
	if ( MyDxf.m_pOutFile ) {
		m_nFlags = nFlags;
	}
	return CDC::SetTextAlign( nFlags );
}
*/

void CMyDc::MyLPtoDP( LPPOINT lpPoint )
{
	TRACE("함수 : BOOL CMyDc::Rectangle( LPCRECT lpRect ) {\n");
	CPoint p = GetWindowOrg();
	lpPoint->x -= p.x;
	lpPoint->y -= p.y;
}

void CMyDc::MyLPtoDP( double &x, double &y )
{
	TRACE("함수 : void CMyDc::MyLPtoDP( double &x, double &y )\n");
	CPoint p = GetWindowOrg();
	x = (x - p.x) * DXFSCALE + 13;
	y = (y - p.y + PAPERY ) * DXFSCALE - 5;
}

BOOL CMyDc::LineTo( int x, int y ) {
	if ( MyDxf.m_pOutFile ) {
		CPoint p = GetCurrentPosition();
		double ax = x;
		double ay = y;
		double bx = p.x;
		double by = p.y;
		MyLPtoDP( ax, ay );
		MyLPtoDP( bx, by );
		MyDxf.fAddLine( ax, ay+TEXTHEIGHT*0.4, bx, by+TEXTHEIGHT*0.4, 1 );	//1=DASHED
	}
	return CDC::LineTo( x, y );
}
/*
CPoint CMyDc::MoveTo( int x, int y ) {
	if ( MyDxf.m_pOutFile ) {
		CPoint p = GetCurrentPosition();
		double ax = x;
		double ay = y;
		double bx = p.x;
		double by = p.y;
		MyLPtoDP( ax, ay );
		MyLPtoDP( bx, by );
		MyDxf.fAddLine( ax, ay+TEXTHEIGHT*0.4, bx, by+TEXTHEIGHT*0.4, 1 );	//1=DASHED
	}
	return CDC::MoveTo( x, y );
}
*/