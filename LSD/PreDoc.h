#if !defined(AFX_PREDOC_H__003E1EF3_17D8_45BC_A946_0DB81AD06E63__INCLUDED_)
#define AFX_PREDOC_H__003E1EF3_17D8_45BC_A946_0DB81AD06E63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPreDoc document

class CPreDoc : public CDocument
{
protected:

// Attributes
public:
	CPreDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPreDoc)

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPreDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CPreDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREDOC_H__003E1EF3_17D8_45BC_A946_0DB81AD06E63__INCLUDED_)
