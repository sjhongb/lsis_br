//=======================================================
//==              ObjDlg.h
//=======================================================
//	파 일 명 :  ObjDlg.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CObjDlg : public CDialog
{
	CString *m_pResult;
// Construction
public:
	CObjDlg(CString &result, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CObjDlg)
	enum { IDD = IDD_OBJECTINS };
	CString	m_Selection;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CObjDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
