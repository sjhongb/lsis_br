//=======================================================
//==              MyObject.cpp
//=======================================================
//	파 일 명 :  MyObject.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
// 
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "LSDDoc.h"
#include "MyObject.h"
#include "../include/ScrInfo.h"
#include "ObjSetTextDlg.h"
#include "ObjEditDlg.h"
#include "StDirDlg.h"
#include "StOpDlg.h"
#include "../include/BmpInfo.h"

CString m_strStationName;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
const char *stringSystemButton[] = {
	"I 4 232 68 G 32836 R", 
	"II 4 233 70 G 32838 R", 
//	"RUN 5 231 88 G 32912 R",
	"PWR 5 234 0 G 32855 R",
	"B24IN 5 243 0 G 32843 R",
	"DC-EX 5 243 0 G 32878 R",
	"UPS1 5 241 0 G 32844 R",
	"UPS2 5 239 0 G 32845 R", 
	"FUSE[C] 5 229 0 G 32874 R", 
	"FUSE2[R] 5 301 0 G 32886 R",
	"FAN1 5 229 0 G 32870 R", 
	"FAN2 5 229 0 G 32871 R", 
	"GEN 5 235 0 G 32865 R", 
	"AXL 5 251 0 G 32870 R",
	NULL
};

const char *stringOptionButton[] = {
	"Alarm 4 244 1 G 32852 R", 
	"Buzzer 4 245 1 G 32853 R",
	"Battery 4 242 0 G 32876 R", 
	"Comm 4 230 0 G 32868 R",
	"Signal 4 227 0 G 32849 R",
	"Switch 4 228 0 G 32850 R",
	"Ground 4 246 0 G 32834 R",
	"MCCR1 4 253 0 G 32862 R",
	"MCCR2 4 254 0 G 32863 R",
	NULL
};

const char *stringBlockButton[] = {
	"CA 4 245 0 G 0 R", 
	"LCR 4 246 0 G 0 R", 
	NULL
};

const char *stringLampButton[] = {
	"SLS 4 226 1 G 32842 R", 
	"ATB 4 238 1 G 32866 R", 
	NULL
};

// NAME ( X, Y ) TYPE, CTRLID, NAME WIDTH, ONID ONCOLOR FLID FLCOLOR

typedef struct OpStateType {
   unsigned short BlockLeft		: 2;
   unsigned short BlockRight	: 2; 
   unsigned short Type			: 2;
   unsigned short RC			: 2;
   unsigned short RCLeft		: 1;
   unsigned short RCRight		: 1;
   unsigned short dumy			: 6;
} DataState;

/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CMyObject, CGraphObject, 0)

/////////////////////////////////////////////////////////////////////////////

CMyObject::CMyObject(CString name, int cx, int cy, int index) : CGraphObject(CRect(CPoint(cx,cy), CSize(5,5)),"b") {
	Name = name;
	if (name == "OSS-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "BLOCK-CLEAR-BUTTONL") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "BLOCK-CLEAR-BUTTONR") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "AXLE-COUNTER") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}									// 2012.08.27 Axle Counter 추가
	else if (name == "WARNINGBOARD(L)") {
		m_Front.x = cx;
		m_Front.y = cy;
		m_Rear.x = m_Front.x;
		m_Rear.y = m_Front.y;
	}
	else if (name == "WARNINGBOARD(R)") {
		m_Front.x = cx;
		m_Front.y = cy;
		m_Rear.x = m_Front.x;
		m_Rear.y = m_Front.y;
	}
	else if (name == "EPK-BUTTON" || name == "APK-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "NIGHT/DAY") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "TOGGLE-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "LAMP-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "PAS") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "CONTROL-PANNEL") {
		m_Front.x = cx-GridSize * 2;
		m_Front.y = cy-GridSize * 2;
		m_Rear.x = cx+GridSize * 2;
		m_Rear.y = cy+GridSize * 2;
	}
	else if (name == "ROAD_WARNER") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "GATE_LODGE") {
		m_Front.x = cx-GridSize * 2;
		m_Front.y = cy-GridSize * 2;
		m_Rear.x = cx+GridSize * 2;
		m_Rear.y = cy+GridSize * 2;
	}
	else if (name == "X_MARK") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "LINE-NAME") {
		m_Front.x = cx-GridSize * 8;
		m_Front.y = cy-GridSize * 2;
		m_Rear.x = cx+GridSize * 8;
		m_Rear.y = cy+GridSize * 2;
	}
	else if (name == "TOKENBLOCK(LEFT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 0;
	}
	else if (name == "TOKENBLOCK(RIGHT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 1;
	}
	else if (name == "TOKENLESSBLOCK(LEFT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 0;
	}
	else if (name == "TOKENLESSBLOCK(RIGHT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 1;
	}
	else if (name == "DIRECTION(LEFT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 0;
	}
	else if (name == "DIRECTION(RIGHT)") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 3;
		m_nShape = 0;
	}
	else if (name == "PLATFORM" || name == "HOME") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "LEVELCROSS") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 5;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 5;
	}
	else if (name == "FLEVELCROSS") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 5;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 5;
	}
	else if (name == "LCA") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "INTERLOCKING-NAME") {
		m_Front.x = cx-GridSize * 12;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 12;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "STATION-NAME") {
		m_Front.x = cx-GridSize * 20;
		m_Front.y = cy-GridSize * 3;
		m_Rear.x = cx+GridSize * 20;
		m_Rear.y = cy+GridSize * 3;
	}
	else if (name == "DN-UP") {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 1;
	}
	else if ( !strncmp( name.Left(4), "TEXT" ,4 ) ) {
		m_Front.x = cx-GridSize * 5;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 5;
		m_Rear.y = cy+GridSize * 1;
		m_nShape = 0x8000;
	}
	else if (name == "OPTION-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "LAMP-BUTTON") {
		m_Front.x = cx-GridSize * 1;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 1;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (name == "BLOCK-BUTTON") {
		m_Front.x = cx-GridSize * 3;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 3;
		m_Rear.y = cy+GridSize * 1;
	}
	else if (Name.GetLength() > 3) {
		m_Front.x = cx-GridSize * 3;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 3;
		m_Rear.y = cy+GridSize * 1;
	}
	else {
		m_nShape = 0x8000;
		m_Front.x = cx-GridSize * 2;
		m_Front.y = cy-GridSize * 1;
		m_Rear.x = cx+GridSize * 2;
		m_Rear.y = cy+GridSize * 1;
	}
	if (name == "SYSTEM-BUTTON") {
		char s1[64];
		strcpy( s1, stringSystemButton[ index ] );
		char *s2 = s1;
		while ( *s2 ) {
			if ( *s2 == ' ' ) {
				*s2 = 0;
				s2++;
				break;
			}
			s2++;
		}
		m_Front.m_sBlipName = s1;
		m_Rear.m_sBlipName = s2;
	}
	if (name == "OPTION-BUTTON") {
		char s1[64];
		strcpy( s1, stringOptionButton[ index ] );
		char *s2 = s1;
		while ( *s2 ) {
			if ( *s2 == ' ' ) {
				*s2 = 0;
				s2++;
				break;
			}
			s2++;
		}
		m_Front.m_sBlipName = s1;
		m_Rear.m_sBlipName = s2;
	}
	if (name == "BLOCK-BUTTON") {
		char s1[64];
		strcpy( s1, stringBlockButton[ index ] );
		char *s2 = s1;
		while ( *s2 ) {
			if ( *s2 == ' ' ) {
				*s2 = 0;
				s2++;
				break;
			}
			s2++;
		}
		m_Front.m_sBlipName = s1;
		m_Rear.m_sBlipName = s2;
	}
	if (name == "LAMP-BUTTON") {
		char s1[64];
		strcpy( s1, stringLampButton[ index ] );
		char *s2 = s1;
		while ( *s2 ) {
			if ( *s2 == ' ' ) {
				*s2 = 0;
				s2++;
				break;
			}
			s2++;
		}
		m_Front.m_sBlipName = s1;
		m_Rear.m_sBlipName = s2;
	}
	Init();
	UpdateRect();
}

int  CMyObject::GetID()
{
	return(m_nShape & 0x07ff);
}

void DrawTextRect(CDC *pDC, char *sitem, CRect &rect, int mode, int dr = 0 , int iLine = 0);
void DrawArrow(CDC *pDC, CPoint a1, CPoint a2, int w, int ed);
void CMyObject::Draw(CDC* pDC , COLORREF cColor) {

	if ( !m_pDocument || m_pDocument->m_bSFMMode ) {
		Init();				// for sfm drawing
		return;
	}

	if (!m_bActive) return;
	Init();
	
	int iTemp;

	int oldmode = pDC->SetTextAlign(TA_LEFT);
	CRect r = m_position;

	CPen newPen;
	CBrush *oldbr;
	CBrush black(cColor);


	r.DeflateRect(2,2);
	r.NormalizeRect();
	int h = (r.top - r.bottom) / 2;
	if (h < 0) h = -h;
	int fy = h;
	int fx = h * 2 / 3;

	CLSDDoc *pDoc = m_pDocument;
	if (Name == "CONTROL-PANNEL") {
		
		int iUD;
		
		if ( m_Rear.m_sBlipName == "UP" ) iUD = -1;
		else iUD = 1;
		
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		DrawTextRect(pDC, "S  B", m_position, 2, 1);
		if ( iUD == 1 ) {
			pDC->MoveTo(m_position.left + (int)((m_position.right - m_position.left)/5) , m_position.bottom + ((int)((m_position.top - m_position.bottom)/5))*iUD);
			pDC->LineTo(m_position.right - (int)((m_position.right - m_position.left)/5) , m_position.bottom + ((int)((m_position.top - m_position.bottom)/5))*iUD);
			CRect cir(m_position.left-((m_position.left-m_position.right)/2)-2,m_position.bottom + ((int)((m_position.top - m_position.bottom)/5)-8)*iUD,m_position.left-((m_position.left-m_position.right)/2)+2,m_position.bottom + ((int)((m_position.top - m_position.bottom)/5)-4)*iUD);
			oldbr = pDC->SelectObject(&black);
			pDC->Ellipse(&cir);
		} else {
			pDC->MoveTo(m_position.left + (int)((m_position.right - m_position.left)/5) , m_position.top + ((int)((m_position.top - m_position.bottom)/5))*iUD);
			pDC->LineTo(m_position.right - (int)((m_position.right - m_position.left)/5) , m_position.top + ((int)((m_position.top - m_position.bottom)/5))*iUD);
			CRect cir(m_position.left-((m_position.left-m_position.right)/2)-2,m_position.top + ((int)((m_position.top - m_position.bottom)/5)-8)*iUD,m_position.left-((m_position.left-m_position.right)/2)+2,m_position.top + ((int)((m_position.top - m_position.bottom)/5)-4)*iUD);		
			oldbr = pDC->SelectObject(&black);
			pDC->Ellipse(&cir);
		}
		pDC->SelectObject(oldbr);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "ROAD_WARNER") {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);
			
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			
			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("LCW",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );
			
			Font1.DeleteObject();
		}
	}
	else if (Name == "GATE_LODGE") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		DrawTextRect(pDC, "", m_position, 2, 1);
		CPen pen2;
		pen2.CreatePen( PS_SOLID,2,cColor);
		pDC->SelectObject(&pen2);
		pDC->MoveTo(m_position.left + (int)((m_position.right - m_position.left)/8) , m_position.bottom + ((int)((m_position.top - m_position.bottom)/5)));
		pDC->LineTo(m_position.right - (int)((m_position.right - m_position.left)/8) , m_position.bottom + ((int)((m_position.top - m_position.bottom)/5)));
		pDC->MoveTo((m_position.left+m_position.right)/2 , m_position.bottom + ((int)((m_position.top - m_position.bottom)/5)));
		pDC->LineTo((m_position.left+m_position.right)/2 , m_position.bottom + ((int)((m_position.top - m_position.bottom)*4/5)));
		CRect cir((m_position.left*3 +m_position.right)/4 - 6,(m_position.bottom + m_position.top)/2 - 6,(m_position.left*3 +m_position.right)/4 + 6,(m_position.bottom + m_position.top)/2 + 6);
		CRect cir2((m_position.left +m_position.right*3)/4 - 6,(m_position.bottom + m_position.top)/2 - 6,(m_position.left +m_position.right*3)/4 + 6,(m_position.bottom + m_position.top)/2 + 6);
		oldbr = pDC->SelectObject(&black);
		pDC->Ellipse(&cir);
		pDC->Ellipse(&cir2);
		pDC->SelectObject(oldbr);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "X_MARK") {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);
			
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			
			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("X",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );
			
			Font1.DeleteObject();
		}
	}
	else if (Name == "LINE-NAME") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
    	CFont Font;
		Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		CString n = pDoc->m_LineName;
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 1);
	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );

	}
	else if (Name == "TOKENBLOCK(LEFT)" || Name == "TOKENBLOCK(RIGHT)") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

    	CFont Font;
		Font.CreateFont(fy/2,fx/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		CPoint p, p1, p2;
		CString n;
		p.y = p1.y = p2.y = (r.top + r.bottom) / 2;
		int iLeft;

		BOOL bLeft = (Name == "TOKENBLOCK(LEFT)");
		if ( m_nShape & MYOBJ_REVERSE ) bLeft = !bLeft;

		if ( bLeft ) {
			p1.x = r.left;
			p2.x = r.left + 30;
			p.x = r.right;
			iLeft = 1;
		}
		else {
			p.x = r.left;
			p1.x = r.right;
			p2.x = r.right - 30;
			iLeft = -1;
		}

		MakeDrawingText();
		
		n = m_strText2 + "\n" + m_strText1;
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 0);

		pDC->MoveTo(p);
		pDC->LineTo(p1);
		pDC->MoveTo(p.x,p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y-(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y-(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y-(int)(r.Height()/10));
		pDC->MoveTo(p.x+((int)(r.Height()/5)*iLeft),p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y-(int)(r.Height()/10));

		DrawArrow(pDC,p2,p1,5,0);

	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "DIRECTION(LEFT)" || Name == "DIRECTION(RIGHT)") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

    	CFont Font;
		Font.CreateFont(fy/2,fx/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		CPoint p, p1, p2;
		CString n;
		p.y = p1.y = p2.y = (r.top + r.bottom) / 2;
		int iLeft;

		BOOL bLeft = (Name == "DIRECTION(LEFT)");
		if ( m_nShape & MYOBJ_REVERSE ) bLeft = !bLeft;

		if ( bLeft ) {
			p1.x = r.left;
			p2.x = r.left + 30;
			p.x = r.right;
			iLeft = 1;
		}
		else {
			p.x = r.left;
			p1.x = r.right;
			p2.x = r.right - 30;
			iLeft = -1;
		}

		MakeDrawingText();
		
		n = "   \n" + m_strText1;
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 0);


		pDC->MoveTo(p);
		pDC->LineTo(p1);
		DrawArrow(pDC,p2,p1,5,0);

	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "TOKENLESSBLOCK(LEFT)" || Name == "TOKENLESSBLOCK(RIGHT)") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

    	CFont Font;
		Font.CreateFont(fy/2,fx/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		CPoint p, p1, p2;
		CString n;
		p.y = p1.y = p2.y = (r.top + r.bottom) / 2;
		int iLeft;

		BOOL bLeft = (Name == "TOKENLESSBLOCK(LEFT)");
		if ( m_nShape & MYOBJ_REVERSE ) bLeft = !bLeft;

		if ( bLeft ) {
			p1.x = r.left;
			p2.x = r.left + 30;
			p.x = r.right;
			iLeft = 1;
		}
		else {
			p.x = r.left;
			p1.x = r.right;
			p2.x = r.right - 30;
			iLeft = -1;
		}

		MakeDrawingText();
		
		n = m_strText2 + "\n" + m_strText1;
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 0);

		pDC->MoveTo(p);
		pDC->LineTo(p1);
		pDC->MoveTo(p.x,p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y-(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y-(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x+((int)(r.Height()/5)*iLeft),p.y-(int)(r.Height()/10));
		pDC->MoveTo(p.x+((int)(r.Height()/5)*iLeft),p.y+(int)(r.Height()/10));
		pDC->LineTo(p.x,p.y-(int)(r.Height()/10));

		DrawArrow(pDC,p2,p1,5,0);

	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "PLATFORM" || Name == "HOME") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		DrawTextRect(pDC, (char *)(LPCTSTR)m_Rear.m_sBlipName, m_position, 2, 1);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "LEVELCROSS") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		CRect rect;
		int iy1,iy2,itemp;

		pDC->MoveTo(m_position.left,m_position.bottom);
		pDC->LineTo(m_position.left,m_position.top);
		pDC->MoveTo(m_position.right,m_position.bottom);
		pDC->LineTo(m_position.right,m_position.top);

		iy1 = m_position.bottom;
		iy2 = m_position.top;

		if ( iy1 > iy2 ) { 
			itemp = iy2;
			iy2 = iy1;
			iy1 = itemp;
		}

		for ( int i = iy1 ; i < iy2 ; i = i + 30 ) {
			rect.bottom = i;
			rect.top = i+20;
			rect.left = m_position.left + 5;
			rect.right = m_position.right - 5;
			pDC->Rectangle(rect);
		}

		pDC->SelectObject( pOldPen );
	}
	else if (Name == "FLEVELCROSS") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		CRect rect;
		int iy1,iy2,itemp;

		pDC->MoveTo(m_position.left,m_position.bottom);
		pDC->LineTo(m_position.left,m_position.top);
		pDC->MoveTo(m_position.right,m_position.bottom);
		pDC->LineTo(m_position.right,m_position.top);

		iy1 = m_position.bottom;
		iy2 = m_position.top;

		if ( iy1 > iy2 ) { 
			itemp = iy2;
			iy2 = iy1;
			iy1 = itemp;
		}

		for ( int i = iy1 ; i < iy2 ; i = i + 30 ) {
			rect.bottom = i;
			rect.top = i+20;
			rect.left = m_position.left + 5;
			rect.right = m_position.right - 5;
			pDC->Rectangle(rect);
		}

		pDC->SelectObject( pOldPen );
	}
	else if (Name == "INTERLOCKING-NAME") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

    	CFont Font;
		Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		int type = pDoc->m_nType & 0xf;
		CString n;
		if (type == 1) n = "전 기 연 동 장 치";
		else if (type == 0) n = "전 자 연 동 장 치";
		else n = "LSEIS520-III";
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 0);
	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "STATION-NAME") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		int hx = (r.left + r.right) / 2;
		int hy = (r.top + r.bottom) / 2;

		CFont Font1,Font2;
		CFont *oldFont;
		
		Font1.CreateFont(30,20,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
		Font2.CreateFont(25,16,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
		oldFont=(CFont*)pDC->SelectObject(&Font1);

		pDC->SetBkMode(TRANSPARENT);
		CString title = pDoc->m_StationName; // LSD 타이틀 
		int old = pDC->SetTextAlign( TA_CENTER );
		int myspace[] = { 30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30 };

		CRect rect = r;
		rect.bottom = r.bottom + h;
		pDC->ExtTextOut(hx, hy + h, ETO_OPAQUE, rect, title, (LPINT)myspace);

		pDC->MoveTo(r.left,hy);
		pDC->LineTo(r.right,hy);

		pDC->SelectObject(&Font2);
		pDC->TextOut(hx,hy - 15,pDoc->m_Kilo);
		pDC->TextOut(hx-150,hy - 15,"(",1);
		pDC->TextOut(hx+150,hy - 15,")",1);
		pDC->SelectObject(oldFont);
		pDC->SetTextAlign(old);

		Font1.DeleteObject();
		Font2.DeleteObject();

		pDC->SelectObject( pOldPen );
	}
	else if (Name == "DN-UP") {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		int hx = (r.left + r.right) / 2;
		int hy = (r.top + r.bottom) / 2;
		CFont Font3;
		CFont *oldFont;
		CString strFrontName = (char *)(LPCTSTR)m_Front.m_sBlipName;
		Font3.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
		oldFont=pDC->SelectObject(&Font3);
		CPoint cl(r.left + fx * 3,hy);
		CPoint cc(hx,hy);
		CPoint cr(r.right - fx * 3,hy);
		pDC->MoveTo(cl);
		pDC->LineTo(cr);
		pDC->MoveTo(cc.x,r.top);
		pDC->LineTo(cc.x,r.bottom);
		int old = pDC->SetTextAlign( TA_CENTER );
		if ( strFrontName == "DN/UP")
		{
			pDC->TextOut(cl.x - fx * 3 / 2, cl.y+fy/2, "DN", 2);
			pDC->TextOut(cr.x + fx * 3 / 2, cr.y+fy/2, "UP", 2);
		}
		else
		{
			pDC->TextOut(cl.x - fx * 3 / 2, cl.y+fy/2, "UP", 2);
			pDC->TextOut(cr.x + fx * 3 / 2, cr.y+fy/2, "DN", 2);
		}
		pDC->SetTextAlign(old);
		pDC->SelectObject(oldFont);
		Font3.DeleteObject();
		cl = CPoint(hx - fx * 2,hy);
		cr = CPoint(hx + fx * 2,hy);
		DrawArrow(pDC,cl,cc,fy / 3,0);
		DrawArrow(pDC,cr,cc,fy / 3,0);
		pDC->SelectObject( pOldPen );
	}
	else if (Name == "SYSTEM-BUTTON") {
		if ( pDoc->m_bOnViewSystemButton ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		// 시스템 버튼의 텍스트 출력 코드 삽입부(사각 모서리를 이용한 박스형 문자 출력)
			CFont Font;
			Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
			DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, r, 2, 1);
			pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
		}
	} 
	else if (Name == "OPTION-BUTTON") {
		if ( pDoc->m_bOnViewSystemButton ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		// 시스템 버튼의 텍스트 출력 코드 삽입부(사각 모서리를 이용한 박스형 문자 출력)
			CFont Font;
			Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
			DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, r, 2, 1);
			pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
		}
	} 
	else if (Name == "BLOCK-BUTTON") {
		if ( pDoc->m_bOnViewSystemButton ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		// 시스템 버튼의 텍스트 출력 코드 삽입부(사각 모서리를 이용한 박스형 문자 출력)
			CFont Font;
			Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
			DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, r, 2, 1);
			pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
		}
	} 
	else if (Name == "LAMP-BUTTON") {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText((char *)(LPCTSTR)m_Front.m_sBlipName,m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
/*
		if ( pDoc->m_bOnViewSystemButton ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			pDC->Ellipse(m_position.left+(GridSize*2),m_position.top,m_position.right-(GridSize*2),m_position.bottom);
			pDC->DrawText((char *)(LPCTSTR)m_Front.m_sBlipName,m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );
		}
*/
	} 
	else if ( Name == "SMALL-STATION-NAME" ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		iTemp = (m_position.bottom-m_position.top)/2;
		if ( m_Rear.m_sBlipName == "UP" ) {
			pDC->MoveTo(m_position.left,m_position.top+iTemp);
			pDC->LineTo(m_position.left,m_position.bottom);
			pDC->LineTo(m_position.right,m_position.bottom);
			pDC->LineTo(m_position.right,m_position.top+iTemp);
		} else {
			pDC->MoveTo(m_position.left,m_position.bottom-iTemp);
			pDC->LineTo(m_position.left,m_position.top);
			pDC->LineTo(m_position.right,m_position.top);
			pDC->LineTo(m_position.right,m_position.bottom-iTemp);		
		}
		DrawTextRect( pDC, (char *)(LPCTSTR)m_Front.m_sBlipName , m_position, 2, 0);

		pDC->SelectObject( pOldPen );
	}
	else if ( Name == "LSCI" || Name == "LGCI") {
			if ( pDoc->m_bOnViewSystemButton ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left,m_position.top,m_position.right,m_position.bottom);
			pDC->DrawText("LS",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );
		}
	}
	else if ( Name == "BRIDGE" ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		CRect tempRect;
		// 교량 윗부분
		pDC->MoveTo(m_position.left-10,m_position.top);
		pDC->LineTo(m_position.left,m_position.top+10);
		pDC->LineTo(m_position.right,m_position.top+10);
		pDC->LineTo(m_position.right+10,m_position.top);
		// 교량 아래부분		
		pDC->MoveTo(m_position.left-10,m_position.bottom);
		pDC->LineTo(m_position.left,m_position.bottom-10);
		pDC->LineTo(m_position.right,m_position.bottom-10);
		pDC->LineTo(m_position.right+10,m_position.bottom);
		
		tempRect.left = m_position.left;
		tempRect.right = m_position.right;
		tempRect.bottom = m_position.bottom;
		tempRect.top = m_position.bottom + 20;
		
		pDC->DrawText(m_Rear.m_sBlipName,tempRect,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
		pDC->SelectObject( pOldPen );
	}
	else if ( Name == "AXLE-COUNTER" ) {		// 2012.08.27 Axle Counter추가
		CFont Font1;
		CRect tempRect;
		Font1.CreateFont(11,10,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
		pDC->SelectObject(&Font1);
		
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );
		
		pDC->Rectangle(m_position.left, m_position.top, m_position.right, m_position.bottom);
		pDC->Ellipse(m_position.left+(GridSize+4),m_position.top+(GridSize),m_position.right-(GridSize+4),m_position.bottom-(GridSize));
		pDC->MoveTo((m_position.left+m_position.right)/2-15,(m_position.top+m_position.bottom)/2);
		pDC->LineTo((m_position.left+m_position.right)/2+15,(m_position.top+m_position.bottom)/2);
		
		tempRect.left = m_position.left;
		tempRect.right = m_position.right;
		tempRect.bottom = m_position.bottom - 40;
		tempRect.top = m_position.bottom - 40;
		
		pDC->DrawText(m_Front.m_sBlipName,tempRect,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
		pDC->SelectObject( pOldPen );
		
		Font1.DeleteObject();
	}
	else if ( Name == "LCA" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("LCA",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "WARNINGBOARD(L)" ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
		
			int r = GridSize - 1, r1, r2;
			int dir = 1;
			CRect cir(x-r,y-r,x+r,y+r);
	
			CPoint c = m_position.CenterPoint();

			pDC->MoveTo(c+CPoint(0,GridSizeH));
			pDC->LineTo(c+CPoint(0,-GridSizeH));
			pDC->MoveTo(c);

			r1 = r - 2;
			r2 = r1/2 + 1;
			c.x += (GridSizeH + r1) * dir;
			pDC->LineTo(c);

			r1 = r * 2 / 9;
			c.x += dir;			
			CPoint c1 = c + CPoint(dir*r,r);
			CPoint c2 = c;
			c2.x -= r * dir;
			CPoint a1, a2;

			a1 = CPoint(c2.x,c1.y);
			a2 = CPoint(c1.x,c2.y);

			cir.SetRect(c.x-r,c.y-r,c.x+r,c.y+r);
			pDC->Ellipse(&cir);


			cir.SetRect(c.x-r1,c.y-(r1*4),c.x+r1,c.y+(r1*4));
			pDC->MoveTo((c.x-(r1*3)*dir),c.y+(r1*3));
			pDC->LineTo((c.x+(r1*3)*dir),c.y-(r1*3));

			pDC->MoveTo((c.x+(r1*3)*dir),c.y+(r1*3));
			pDC->LineTo((c.x-(r1*3)*dir),c.y-(r1*3));

			pDC->SelectObject( pOldPen );
	}
	else if ( Name == "WARNINGBOARD(R)" ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
		
			int r = GridSize - 1, r1, r2;
			int dir = -1;
			CRect cir(x-r,y-r,x+r,y+r);

			CPoint c = m_position.CenterPoint();
			
			pDC->MoveTo(c+CPoint(0,GridSizeH));
			pDC->LineTo(c+CPoint(0,-GridSizeH));
			pDC->MoveTo(c);

			r1 = r - 2;
			r2 = r1/2 + 1;
			c.x += (GridSizeH + r1) * dir;
			pDC->LineTo(c);

			r1 = r * 2 / 9;
			c.x += dir;			
			CPoint c1 = c + CPoint(dir*r,r);
			CPoint c2 = c;
			c2.x -= r * dir;
			CPoint a1, a2;

			a1 = CPoint(c2.x,c1.y);
			a2 = CPoint(c1.x,c2.y);

			cir.SetRect(c.x-r,c.y-r,c.x+r,c.y+r);
			pDC->Ellipse(&cir);


			cir.SetRect(c.x-r1,c.y-(r1*4),c.x+r1,c.y+(r1*4));
			pDC->MoveTo((c.x-(r1*3)*dir),c.y+(r1*3));
			pDC->LineTo((c.x+(r1*3)*dir),c.y-(r1*3));

			pDC->MoveTo((c.x+(r1*3)*dir),c.y+(r1*3));
			pDC->LineTo((c.x-(r1*3)*dir),c.y-(r1*3));

			pDC->SelectObject( pOldPen );	}
	else if ( Name == "PAS" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			int iTemp,iTemp2;
			pDC->Rectangle(m_position);
			iTemp = (int)((m_position.Width()-4)/5);
			iTemp2 = (int)((m_position.Height()-4)/5);
			pDC->Ellipse(m_position.left+2+iTemp,m_position.top+(iTemp2*2),m_position.left+2+(iTemp*4),m_position.top+(iTemp2*5));
			pDC->MoveTo(m_position.left+(int)(m_position.Width()/2),m_position.top+(iTemp2*2));
			pDC->LineTo(m_position.left+(int)(m_position.Width()/2),m_position.top+iTemp2);
			pDC->LineTo(m_position.left+(int)(m_position.Width()/2)+iTemp,m_position.top+iTemp2);
			pDC->SelectObject( pOldPen );
		}
	}
	else if ( Name == "RGB" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			int iTemp;
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			pDC->Rectangle(m_position);
			iTemp = (int)((m_position.Width()-4)/3);
			pDC->Ellipse(m_position.left+2,m_position.top+2,m_position.left+2+iTemp,m_position.bottom-2);
			pDC->Ellipse(m_position.left+2+iTemp,m_position.top+2,m_position.left+2+(iTemp*2),m_position.bottom-2);
			pDC->Ellipse(m_position.left+2+(iTemp*2),m_position.top+2,m_position.left+2+(iTemp*3),m_position.bottom-2);
			pDC->SelectObject( pOldPen );
		}
	}
	else if ( Name == "NIGHT/DAY" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("N/D",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "OSS-BUTTON" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("OSS",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "BLOCK-CLEAR-BUTTONL" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("BCBL",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "BLOCK-CLEAR-BUTTONR" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("BCBR",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "EPK-BUTTON" || Name == "APK-BUTTON") {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("EPK",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "TOGGLE-BUTTON" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			CFont Font1;
			Font1.CreateFont(11,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
			pDC->SelectObject(&Font1);

			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );

			pDC->Ellipse(m_position.left+(GridSize-5),m_position.top+(GridSize-5),m_position.right-(GridSize-5),m_position.bottom-(GridSize-5));
			pDC->Ellipse(m_position.left+(GridSize-2),m_position.top+(GridSize-2),m_position.right-(GridSize-2),m_position.bottom-(GridSize-2));
			
			//DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, m_position , 2, 0);
			pDC->DrawText("COS",m_position,DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			pDC->SelectObject( pOldPen );

			Font1.DeleteObject();
		}
	}
	else if ( Name == "COUNTER" ) {
		if ( pDoc->m_bOnViewSystemButton ) {
			newPen.CreatePen( PS_SOLID, 1, cColor );
			CPen* pOldPen = pDC->SelectObject( &newPen );
			pDC->Rectangle(m_position);
			DrawTextRect(pDC, (char *)(LPCTSTR)m_Front.m_sBlipName, r, 2, 1);
		}
	}
	else if ( Name == "OPERBUTTON" ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

		DrawTextRect(pDC, "OP BUTTON", m_position, 2, 1);
		pDC->SelectObject( pOldPen );
	}
	else if ( !strncmp( Name.Left(4), "TEXT", 4 ) ) {
		newPen.CreatePen( PS_SOLID, 1, cColor );
		CPen* pOldPen = pDC->SelectObject( &newPen );

    	CFont Font;
		Font.CreateFont(fy,fx,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	    CFont *oldFont=(CFont*)pDC->SelectObject(&Font);
		CString n = Name.Right( Name.GetLength() - 5 );
		DrawTextRect(pDC, (char *)(LPCTSTR)n, r, 2, 0);
	    pDC->SelectObject(oldFont);
		pDC->SelectObject( pOldPen );
	}
	pDC->SetTextAlign(oldmode);
}

void CMyObject::MakeDrawingText() {
	CLSDDoc *pDoc = m_pDocument;
	CString s1 = m_Front.m_sBlipName;
	CString s2 = m_Rear.m_sBlipName;
	if (Name == "TOKENBLOCK(LEFT)") {
		if (s1 == "") s1 = pDoc->m_FromStation;
		if (s2 == "") s2 = pDoc->m_PrevStation;
	}
	else if (Name == "TOKENLESSBLOCK(LEFT)") {
		if (s1 == "") s1 = pDoc->m_FromStation;
		if (s2 == "") s2 = pDoc->m_PrevStation;
	}
	else if (Name == "DIRECTION(LEFT)") {
		if (s1 == "") s1 = pDoc->m_FromStation;
		if (s2 == "") s2 = pDoc->m_PrevStation;
	}
	else {
		if (s1 == "") s1 = pDoc->m_ToStation;
		if (s2 == "") s2 = pDoc->m_NextStation;
	}

	int j, k, l;
	m_nType = (m_nShape & 0x3FFFFFFF) >> 1;
	if (m_nType == 0) m_nType = 1;
	for(j=0; j<m_nType; j++) {
		k = s1.Find(',');
		l = s2.Find(',');
		if(k >= 0) {
			m_strText1 = s1.Left(k);
			CString imsi1 = s1.Mid(k+1);
			if (imsi1 != "") {
				s1 = imsi1;
			}
		}
		else if (s1 != "") m_strText1 = s1;	
		if(l >= 0) {
			m_strText2 = s2.Left(l);
			CString imsi2 = s2.Mid(l+1);
			if (imsi2 != "") {
				s2 = imsi2;
			}
		}
		else if (s2 != "") m_strText2 = s2;	
	}
}

extern int   OFFSET_X;
extern int   OFFSET_Y;

void CMyObject::Init() {
	char bf[64];
	int iWidth;
	int iHeight = 0;
	WORD nOnID, nOnColor, nFlashID, nFlashColor;
	char *str;

	m_pBlips[0] = &m_Front;
	m_pBlips[1] = &m_Rear;
	m_pBlips[2] = NULL;

	m_nHandle = 2;
	m_Front.Visible(TRUE);
	m_Rear.Visible(TRUE);
	Visible(TRUE);
    // new CBmpEtcObj 로 생성한것만 SFM 파일로 저장됨, 
	if ( !m_pDocument ) return;
	CLSDDoc *pDoc = m_pDocument;
	if ( Name == "STATION-NAME" ) {
		int type = ETCOBJTYPE_STATIONNAME /* 1 */;	// 역명
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		p.x = ( p.x + OFFSET_X);  // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)pDoc->m_StationName, type, id );
		}
	} 
	else  if (Name == "SMALL-STATION-NAME"){
		int type = ETCOBJTYPE_MINISTATIONNAME /* 6 */;	// 미니역명
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X); // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}
	if ( Name == "PLATFORM"  || Name == "HOME") {
		int type = ETCOBJTYPE_PLATFORM /* 1 */;	// 역명
		int id   = 0; 
		iWidth = abs(m_position.left - m_position.right);
		iHeight = abs(m_position.top - m_position.bottom); 
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X);  // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id,iWidth,iHeight);
		}
		else {
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id,iWidth,iHeight);
		}
	} 
	if ( Name == "LEVELCROSS" ) {
		int type;
		int id   = 0; 
		type = ETCOBJTYPE_LEVELCROSS /* 1 */;	// 역명
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X); // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
			iWidth = m_Front.y - m_Rear.y;
			iWidth = abs((int)(iWidth / GridSize));
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			iWidth = m_Front.y - m_Rear.y;
			iWidth = abs((int)(iWidth / GridSize));
		}
		char *s = bf;
		//iWidth = atoi( s );
		while ( *s && *s != ' ' ) s++;
		id = atoi( ++s );
		while ( *s && *s != ' ' ) s++;
		s++;
		if ( isdigit(*s) || *s == '.' ) {
			nOnID = atoi( s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nOnColor = (WORD)*s++;
			nFlashID = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nFlashColor = (WORD)*s;
			if ( m_pBmpInfo ) { 
				((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
				((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
			} else { 
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
										 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	} 
	if ( Name == "FLEVELCROSS" ) {
		int type;
		int id   = 0; 
		type = ETCOBJTYPE_FLEVELCROSS /* 1 */;	// 역명
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X); // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
			iWidth = m_Front.y - m_Rear.y;
			iWidth = abs((int)(iWidth / GridSize));
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			iWidth = m_Front.y - m_Rear.y;
			iWidth = abs((int)(iWidth / GridSize));
		}
		char *s = bf;
		//iWidth = atoi( s );
		while ( *s && *s != ' ' ) s++;
		id = atoi( ++s );
		while ( *s && *s != ' ' ) s++;
		s++;
		if ( isdigit(*s) || *s == '.' ) {
			nOnID = atoi( s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nOnColor = (WORD)*s++;
			nFlashID = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nFlashColor = (WORD)*s;
			if ( m_pBmpInfo ) { 
				((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
				((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
			} else { 
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
										 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	} 
	if ( Name == "LCA" ) {
		int type;
		int id   = 0; 
		type = ETCOBJTYPE_LCA /* 1 */;	// 역명
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X); // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
		char *s = bf;
		iWidth = atoi( s );
		while ( *s && *s != ' ' ) s++;
		id = atoi( ++s );
		while ( *s && *s != ' ' ) s++;
		s++;
		if ( isdigit(*s) || *s == '.' ) {
			nOnID = atoi( s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nOnColor = (WORD)*s++;
			nFlashID = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nFlashColor = (WORD)*s;
			if ( m_pBmpInfo ) { 
				((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
				((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
			} else { 
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
										 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	} 
	else  if (Name == "LSCI" || Name == "LGCI"){
		int type = ETCOBJTYPE_LGCI /* 8 */;	// LG산전 CI
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "LSCI";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}
	else  if (Name == "BRIDGE"){
		int type = ETCOBJTYPE_BRIDGE /* 34 */;	// 아카우라 바이패스역의 교량 (TITAS BRIDGE)
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		p.x = m_position.left;
//		p.y = m_position.top;

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);

		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Rear.m_sBlipName);
			iWidth = m_Front.x - m_Rear.x;
			iWidth = abs((int)(iWidth / GridSize));
			m_pBmpInfo->m_iWith = iWidth;
		} else {
			m_Front.m_sBlipName = "BRIDGE";
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			iWidth = 0;
			iWidth = m_Front.x - m_Rear.x;
			iWidth = abs((int)(iWidth / GridSize));
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_Front.m_sBlipName, (char*)(LPCTSTR)m_Rear.m_sBlipName, type, id, iWidth, iHeight,
										 nOnID, nOnColor, nFlashID, nFlashColor );

//			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}

		
//		if ( m_pBmpInfo ) {
//			m_pBmpInfo->mPos = p;
//		}
//		else {
//			m_Front.m_sBlipName = "BRIDGE";
//			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
//		}	
	}
	else  if (Name == "WARNINGBOARD(L)"){
		int type = ETCOBJTYPE_WARNINGL /* 8 */;	// LG산전 CI
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		//p.x = ( p.x + OFFSET_X); // no reverse // REVERSE 관련 수정
		//p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "WARNINGBOARD";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}
	else  if (Name == "WARNINGBOARD(R)"){
		int type = ETCOBJTYPE_WARNINGR /* 8 */;	// LG산전 CI
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "WARNINGBOARD";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}
	else  if ( !strncmp( Name.Left(4), "TEXT" ,4 )){
		int type = ETCOBJTYPE_TEXT /* 8 */;	// LG산전 CI
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "TEXT";
			m_Front.m_sBlipName = Name.Mid(5,Name.GetLength());
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}
	else  if (Name == "PAS"){
		int type = ETCOBJTYPE_PAS;	// PAS 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
	}
	else  if (Name == "RGB"){
		int type = ETCOBJTYPE_RGB;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
	}
	else  if (Name == "NIGHT/DAY"){
		int type = ETCOBJTYPE_DIMMING;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
			
	}
	else  if (Name == "OSS-BUTTON"){
		int type = ETCOBJTYPE_OSS;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
			
	}
	else  if (Name == "BLOCK-CLEAR-BUTTONL"){
		int type = ETCOBJTYPE_BCBL;	// Block Clear Button L 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}		
	}
	else  if (Name == "BLOCK-CLEAR-BUTTONR"){
		int type = ETCOBJTYPE_BCBR;	// BlockClearButton R
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
		char *s = bf;
		iWidth = atoi( s );
		while ( *s && *s != ' ' ) s++;
		id = atoi( ++s );
		while ( *s && *s != ' ' ) s++;
		s++;
		if ( isdigit(*s) || *s == '.' ) {
			nOnID = atoi( s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nOnColor = (WORD)*s++;
			nFlashID = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nFlashColor = (WORD)*s;
			if ( m_pBmpInfo ) { 
				((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
				((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
			} else { 
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
					nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}		
	}
	else  if (Name == "AXLE-COUNTER")
	{
		int type = ETCOBJTYPE_AXLE_COUNTER;	
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
		char *s = bf;
		iWidth = atoi( s );
		while ( *s && *s != ' ' ) s++;
		id = atoi( ++s );
		while ( *s && *s != ' ' ) s++;
		s++;
		if ( isdigit(*s) || *s == '.' ) {
			nOnID = atoi( s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nOnColor = (WORD)*s++;
			nFlashID = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			nFlashColor = (WORD)*s;
			if ( m_pBmpInfo ) { 
				((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
				((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
				((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
			} else { 
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
					nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}		
	}
	else  if (Name == "EPK-BUTTON" || Name == "APK-BUTTON"){
		int type = ETCOBJTYPE_EPK;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
			
	}
	else  if (Name == "TOGGLE-BUTTON"){
		int type = ETCOBJTYPE_TOGGLE;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnID = nOnID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nOnColor = nOnColor;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashID = nFlashID;
					((CBmpEtcObj*) m_pBmpInfo)->m_nFlashColor = nFlashColor;
				} else { 
					m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
				}
			}
	}
	else  if (Name == "COUNTER"){
		int type = ETCOBJTYPE_COUNTER;	// RGB 키 
		int id   = 0; 
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
			strcpy(m_pBmpInfo->m_szDrawName,m_Front.m_sBlipName);
			strcpy( bf, m_Rear.m_sBlipName );
		} else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			iWidth = 0;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
		}
			char *s = bf;
			while ( *s && *s != ',' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ',' ) s++;
			s++;
			iWidth = atoi( s );
				if ( m_pBmpInfo ) { 
					((CBmpEtcObj*) m_pBmpInfo)->m_nID = id;
					((CBmpEtcObj*) m_pBmpInfo)->mWidth = iWidth;
				} else { 
					m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
				}
	}
    else  if (Name == "CONTROL-PANNEL"){
		int type = ETCOBJTYPE_OPERATPANNEL /* 6 */;	// 미니역명
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "CONTROL-PANNEL";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}                 
    else  if (Name == "ROAD_WARNER"){
		int type = ETCOBJTYPE_ROAD_WARNER /* 37 */;
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "ROAD_WARNER";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}                 
    else  if (Name == "GATE_LODGE"){
		int type = ETCOBJTYPE_GATE_LODGE /* 38 */;
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "GATE_LODGE";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}                 
    else  if (Name == "X_MARK"){
		int type = ETCOBJTYPE_X_MARK /* 39 */;
		int id   = 0; 
		CPoint p = m_position.CenterPoint();
		
		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_Front.m_sBlipName = "X_MARK";
			m_pBmpInfo = new CBmpEtcObj(p,(char*)(LPCTSTR)m_Front.m_sBlipName,(char*)(LPCTSTR)m_Rear.m_sBlipName,type,id);
		}	
	}                 
	else if( Name == "TOKENBLOCK(LEFT)" ) {			
		int type = ETCOBJTYPE_LEFTARROWTEXT;	// 기점,인접역명 (좌)
		int id;
		if ( m_nShape & MYOBJ_REVERSE ) {
			//type = ETCOBJTYPE_RIGHTARROWTEXT;
			id   = 0; 
		} else {
			id   = 230; 
		}

		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		MakeDrawingText();
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32869, 82 );
		}
	}
	else if( Name == "TOKENBLOCK(RIGHT)" ) {
		int type = ETCOBJTYPE_RIGHTARROWTEXT;	// 기점,인접역명 (우)
		int id;
		if ( m_nShape & MYOBJ_REVERSE ) {
			//type = ETCOBJTYPE_RIGHTARROWTEXT;
			id   = 0; 
		} else {
			id   = 230; 
		}
		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);			
		MakeDrawingText();
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32868, 82 );
		}
	}
	else if( Name == "TOKENLESSBLOCK(LEFT)" ) {			
		int type = ETCOBJTYPE_LEFTARROWTL;	// 기점,인접역명 (좌)
		if ( m_nShape & MYOBJ_REVERSE ) type = ETCOBJTYPE_RIGHTARROWTL;
		int id;

		if ( m_nShape & MYOBJ_REVERSE ) 
		{
			//type = ETCOBJTYPE_RIGHTARROWTEXT;
			id   = 0; 
		} 
		else 
		{
			id   = 230; 
		}

		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		MakeDrawingText();

		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else 
		{
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32869, 82 );
		}

		if ( m_nShape & MYOBJ_NOINFO )
		{
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32852, 82 );
		}
}
	else if( Name == "TOKENLESSBLOCK(RIGHT)" ) {
		int type = ETCOBJTYPE_RIGHTARROWTL;	// 기점,인접역명 (우)
		if ( m_nShape & MYOBJ_REVERSE ) type = ETCOBJTYPE_LEFTARROWTL;
		int id;

		if ( m_nShape & MYOBJ_REVERSE ) {
			//type = ETCOBJTYPE_RIGHTARROWTEXT;
			id   = 0; 
		} else {
			id   = 230; 
		}

		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);			
		MakeDrawingText();
		
	if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32868, 82 );
		}
		
		if ( m_nShape & MYOBJ_NOINFO )
		{
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32852, 82 );
		}
	}
	else if( Name == "DIRECTION(LEFT)" ) {			
		int type = ETCOBJTYPE_LEFTARROWTEXT;	// 기점,인접역명 (좌)
		int id;

		if ( m_nShape & MYOBJ_REVERSE ) {
			id   = 0; 
		} else {
			id   = 0; 
		}

		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);
		MakeDrawingText();
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else 
		{
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32869, 82 );
		}
	}
	else if( Name == "DIRECTION(RIGHT)" ) {
		int type = ETCOBJTYPE_RIGHTARROWTEXT;	// 기점,인접역명 (우)
		int id;

		if ( m_nShape & MYOBJ_REVERSE ) {
			id   = 0; 
		} else {
			id   = 0; 
		}

		CPoint p = m_position.CenterPoint();
		ChangeMapping(p);			
		MakeDrawingText();
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			m_pBmpInfo = new CBmpEtcObj( p, (char*)(LPCTSTR)m_strText1, (char*)(LPCTSTR)m_strText2, type, id, 100, 0, 0, 71, 32868, 82 );
		}
	}
	else  if( Name == "SYSTEM-BUTTON" ) {	// system button
		int type;
		int id   = GetID(); 
		int no = id - BtnPas;
		iWidth = 0;
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				type = ETCOBJTYPE_SYSBUTTON /* 4 */ ;	// 시스템 버튼
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
			}
			else {
                type = ETCOBJTYPE_SYSBUTTON;
				while ( *s && *s != '.' ) s++;
				s++;
				char *str2 = s;
				while ( *s && *s != ' ' ) s++;
				*s++ = 0;
				m_pBmpInfo = new CBmpEtcObj( p, str, str2, type, id, iWidth );
			}
		}
	}
	else  if( Name == "OPTION-BUTTON" ) {	// system button
		int type;
		int id   = GetID(); 
		int no = id - BtnPas;
		iWidth = 0;
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				type = ETCOBJTYPE_OPTIONBUTTON /* 4 */ ;	// 시스템 버튼
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	}
	else  if( Name == "BLOCK-BUTTON" ) {	// system button
		int type;
		int id   = GetID(); 
		int no = id - BtnPas;
		iWidth = 0;
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				type = ETCOBJTYPE_BLOCKBUTTON /* 4 */ ;	// 시스템 버튼
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	}
	else  if( Name == "LAMP-BUTTON" ) {	// system button
		int type;
		int id   = GetID(); 
		int no = id - BtnPas;
		iWidth = 0;
		CPoint p = m_position.CenterPoint();

		p.x = ( p.x + OFFSET_X); // no reverse
		p.y = ( -p.y + OFFSET_Y);
		if ( m_pBmpInfo ) {
			m_pBmpInfo->mPos = p;
		}
		else {
			str = (char*)(LPCTSTR)m_Front.m_sBlipName;
			nOnID = 0; nOnColor = 0; nFlashID = 0; nFlashColor = 0;
			strcpy( bf, m_Rear.m_sBlipName );
			char *s = bf;
			iWidth = atoi( s );
			while ( *s && *s != ' ' ) s++;
			id = atoi( ++s );
			while ( *s && *s != ' ' ) s++;
			s++;
			if ( isdigit(*s) || *s == '.' ) {
				type = ETCOBJTYPE_TOGGLE /* 4 */ ;	// 시스템 버튼
				nOnID = atoi( s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nOnColor = (WORD)*s++;
				nFlashID = atoi( ++s );
				while ( *s && *s != ' ' ) s++;
				s++;
				nFlashColor = (WORD)*s;
				m_pBmpInfo = new CBmpEtcObj( p, str, str, type, id, iWidth, iHeight,
											 nOnID, nOnColor, nFlashID, nFlashColor );
			}
		}
	}

}

void CMyObject::DrawAngle(CDC *pDC, CPoint p, int dir)
{
	pDC->MoveTo(p.x - dir * GridSize, p.y + GridSizeH);
	pDC->LineTo(p);
	pDC->LineTo(p.x - dir * GridSize, p.y - GridSizeH);
}

void CMyObject::UpdateRect()
{
	m_position = m_Front.GetRect();
	m_position |= m_Rear.GetRect();
	m_position.NormalizeRect();
}

void CMyObject::GetSize(CMyObject& obj)
{
	CSize sz;
	sz.cx = obj.m_Front.x - obj.m_Rear.x;
	sz.cy = obj.m_Front.y - obj.m_Rear.y;
	int x = (m_Front.x + m_Rear.x) / 2;
	int y = (m_Front.y + m_Rear.y) / 2;
	m_Front.x = x - sz.cx / 2;
	m_Front.y = y - sz.cy / 2;
	m_Rear.x = x + sz.cx / 2;
	m_Rear.y = y + sz.cy / 2;
	UpdateRect();
}

//=======================================================
//
//  함 수 명 :  PropertyChange
//  함수출력 :  없음
//  함수입력 :  CWnd *wnd
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :
//
//=======================================================
void CMyObject::PropertyChange(CWnd *wnd) {
	if (Name == "TOKENBLOCK(LEFT)" || Name == "TOKENBLOCK(RIGHT)") {
		CObjDirectDlg dlg;
		dlg.m_sOrg = m_Front.m_sBlipName;
		dlg.m_sNext = m_Rear.m_sBlipName;
		dlg.m_bReverse = (m_nShape & MYOBJ_REVERSE) != 0;
		dlg.DoModal();
		m_Front.m_sBlipName = dlg.m_sOrg;
		m_Rear.m_sBlipName = dlg.m_sNext;
		if ( dlg.m_bReverse ) {
			m_nShape |= MYOBJ_REVERSE;
		}
		else {
			m_nShape &= ~MYOBJ_REVERSE;
		}
	}
	else if (Name == "TOKENLESSBLOCK(LEFT)" || Name == "TOKENLESSBLOCK(RIGHT)") {
		CObjDirectDlg dlg;
		dlg.m_sOrg = m_Front.m_sBlipName;
		dlg.m_sNext = m_Rear.m_sBlipName;
		dlg.m_bReverse = (m_nShape & MYOBJ_REVERSE) != 0;
		dlg.m_bNoInfo = (m_nShape & MYOBJ_NOINFO) != 0;
		dlg.DoModal();
		m_Front.m_sBlipName = dlg.m_sOrg;
		m_Rear.m_sBlipName = dlg.m_sNext;
		if ( dlg.m_bReverse ) {
			m_nShape |= MYOBJ_REVERSE;
		}
		else {
			m_nShape &= ~MYOBJ_REVERSE;
		}
		if ( dlg.m_bNoInfo ) {
			m_nShape |= MYOBJ_NOINFO;
		}
		else {
			m_nShape &= ~MYOBJ_NOINFO;
		}
	}
	else if (Name == "DIRECTION(LEFT)" || Name == "DIRECTION(RIGHT)") {
		CObjDirectDlg dlg;
		dlg.m_sOrg = m_Front.m_sBlipName;
		dlg.m_sNext = m_Rear.m_sBlipName;
		dlg.m_bReverse = (m_nShape & MYOBJ_REVERSE) != 0;
		dlg.DoModal();
		m_Front.m_sBlipName = dlg.m_sOrg;
		m_Rear.m_sBlipName = dlg.m_sNext;
		if ( dlg.m_bReverse ) {
			m_nShape |= MYOBJ_REVERSE;
		}
		else {
			m_nShape &= ~MYOBJ_REVERSE;
		}
	}
	else if ( !strncmp(Name.Left(4), "TEXT", 4) ) {
		CObjSetTextDlg	Dlg;
		Dlg.m_strText = Name;
		if ( Dlg.DoModal() == IDOK ) {
			Name = Dlg.m_strText;
		}
	}
	else {
		CObjEditDlg	Dlg;
		Dlg.m_strText = Name;
		Dlg.m_strPara1 = m_Front.m_sBlipName;
		Dlg.m_strPara2 = m_Rear.m_sBlipName;
		if ( Dlg.DoModal() == IDOK ) {
			Name = Dlg.m_strText;
			m_Front.m_sBlipName = Dlg.m_strPara1;
			m_Rear.m_sBlipName = Dlg.m_strPara2;
		}
	}
}

void CMyObject::Serialize(CArchive &ar) {
	if (ar.IsStoring()) {
		x = m_nShape;
		CGraphObject::Serialize(ar);
	}
	else {
		CGraphObject::Serialize(ar);
		m_nShape = x;
	}
}

int CMyObject::GetOutStringILD( CString &str )
{
	if (Name == "SYSTEM-BUTTON") {
		str = m_Front.m_sBlipName;

		int i = 0;
		while ( stringSystemButton[i] ) {
			char s1[64];
			strcpy( s1, stringSystemButton[i] );
			char *s2 = s1;
			while ( *s2 ) {
				if ( *s2 == ' ' ) {
					*s2 = 0;
					s2++;
					break;
				}
				s2++;
			}
			if ( str == s1 ) return 0;
			i++;
		}
		return 1;
	}

	if (Name == "OPTION-BUTTON") {
		str = m_Front.m_sBlipName;

		int i = 0;
		while ( stringOptionButton[i] ) {
			char s1[64];
			strcpy( s1, stringOptionButton[i] );
			char *s2 = s1;
			while ( *s2 ) {
				if ( *s2 == ' ' ) {
					*s2 = 0;
					s2++;
					break;
				}
				s2++;
			}
			if ( str == s1 ) return 0;
			i++;
		}
		return 1;
	}
	return 0;
}
