//=======================================================
//==              Track.h
//=======================================================
//	파 일 명 :  Track.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  궤도의 정보를 저장하고 관리하는 클래스
//
//=======================================================

#ifndef _TRACK_H
#define _TRACK_H

/*
       c ------p------ n	c = m_Base
			    |
				 |
		 	      r
*/

#include "Blip.h"
#include "GraphObj.h"
#include "Signal.h"
#include "Button.h"
#include "LevelCross.h"
#include "../include/bmpinfo.h"

#define B_NAME_C			0x0001
#define B_NAME_N			0x0002
#define B_NAME_R			0x0004
#define B_NAME_E			0x0008
#define B_OPEN_C			0x0010
#define B_OPEN_N			0x0020
#define B_OPEN_R			0x0040
#define B_OPEN_E			0x0080
#define B_BLOCKSIGNALFAIL	0x0100

#define B_MAIN				0x0200
#define B_NOTRACK			0x0400
#define B_DOUBLE_SLIP		0x0800

#define B_BRANCH_POS_RIGHT	0x1000
#define B_BRANCH_POS_UP		0x2000
#define B_BRANCH_POS_LEFT	0x4000
#define B_BRANCH_POS_DOWN	0x8000

#define B_CEND_C		0x00010000
#define B_CEND_N		0x00020000
#define B_CEND_R		0x00040000
#define B_CEND_E		0x00080000
#define B_TEND_C		0x00100000
#define B_TEND_N		0x00200000
#define B_TEND_R		0x00400000
#define B_TEND_E		0x00800000
#define B_IN_C			0x01000000
#define B_IN_N			0x02000000
#define B_IN_R			0x04000000
#define B_DIF_TRACK		0x08000000
#define B_OUT_C			0x10000000
#define B_OUT_N			0x20000000
#define B_OUT_R			0x40000000
#define B_NO_MAIN_ROUTE	0x80000000

class CTrackDlg;
class StationDB;
class CSwitch;
class ostream;

class CLine {
public:
	POINT * GetRgnPoint();
	CPoint a,b;
	CLine(CPoint ia, CPoint ib) {
		a = ia;
		b = ib;
	}
	CLine(int ax, int ay, int bx, int by) {
		a.x = ax;
		a.y = ay;
		b.x = bx;
		b.y = by;
	}
	void Draw(CDC *pDC);
};

class CTrack : public CGraphObject {
	int m_nShape;
	CBlip nC, nN, nR; 
	CSignal m_cSignalC[2], m_cSignalN[2], m_cSignalR[2];
	CMyButton m_cButton[2];
	CLevelCross m_cLevelCross;
	CString m_sBranchName;
	CString m_sLevelCross;
	CString m_sBranchNameItems[4];
	CPoint pC,pN,pR;
	CRect br;
	CObList m_Lines;
protected:
	DECLARE_SERIAL(CTrack);
public:
	CString m_sBranchPower;
	CString m_sSpare;
	void NodeDump( ostream &os, int nNode );
	void CreateDefaultSignalPosDo(CSignal &Sig, CBlip &ref);
	void CreateDefaultSignalPos();
	void DrawDoubleSlip(CDC* pDC);
	
	CRgn * GetRgn();
	BOOL isValid();

	BOOL m_bCheck;
	BOOL m_bKeyLock;

	void DrawLines(CDC *pDC);
	void AddLines(CLine *line);
	void RemoveLines();
	void DrawVector(int n1, int n2);
	BOOL Connect(CTrack* t, int &n1, int &n2);
 	CBlip c, n, r, l, p;

	int AtSignal(CPoint point);
	char * GetButtonName(int cnr2);
	virtual void UpdateRect();
	CRect DrawHatch(CDC *pDC, BOOL bIsDrawText=TRUE);
	virtual CGraphObject* Clone(CLSDDoc* pDoc);
	virtual CGraphObject* SaveClone(CLSDDoc* pDoc);

	virtual BOOL Intersects(const CRect& rect) {
		return CGraphObject::Intersects(rect);
	}
	void DrawParen( CDC *pDC, CPoint p, int dir );
	CPoint ClipPoint(CPoint &a, CPoint &b);
	BOOL   GetClipPoint( int node, CPoint a, CPoint b, CPoint &cp1, CPoint &cp2, CPoint &cp3 );
	void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));

	virtual void PropertyChange(CWnd *wnd);
	CTrack(int t = 0, int cx = 0, int cy = 0);
	~CTrack() {
		RemoveLines();
	}
	virtual void Read(CArchive &ar);
	virtual void Write(CArchive &ar);
	CBlip *PtInBlip(CPoint point);
	void DrawSegment(CDC *pDC, CPoint a, CPoint b, int wh, BOOL horiz = FALSE);
	void Init();
	virtual const char *NameOf() { return "TRACK"; }
	BOOL isMainLine();
	BOOL isDoubleSlip();
	BOOL isNoGrid(CBlip *pBlip);
	virtual void Serialize(CArchive &ar);
friend CTrackDlg;
friend StationDB;
friend CSwitch;
friend CTrack;

// For database construct only	

	BOOL m_iBranchVect;		// TRUE:(+), FALSE(-)
	CTrack *m_pDoubleSlip;
	CTrack *m_pConnect[3];
	int m_nConnect[3];
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
int GetTokenTrackName(const char *s, CString *obj);
int strcmpTName(const char *s1, const char *s2);
void DelStringLastSpace(CString &str);
void DelStringMultiNo(CString &s , BOOL bPOINT = FALSE); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T

/////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif