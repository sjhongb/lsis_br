//=======================================================
//==              Signal.cpp
//=======================================================
//	파 일 명 :  Signal.cpp
//  작 성 자 :  omani
//	작성날짜 :  2004-10-11
//	버    전 :  1.0
//	설    명 :  신호기 관련 처리를 담당
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "Signal.h"
#include "SignalDl.h"
#include "Define.h"
#include <iostream.h>
#include <fstream.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern short	giHomeCntUP;
extern short	giHomeCntDN;

IMPLEMENT_SERIAL(CSignal, CGraphObject, 0)

class MyDC {
	CDC *m_pDC;
public:
	int cx;
	int cy;
	int dir;
	int m_nLampSize;

	MyDC( CDC *pDC ) { m_pDC = pDC; }
	void DrawDot( CPoint p ) { m_pDC->Ellipse(p.x-1,p.y-1,p.x+1,p.y+1); }
	void DrawLamp( BYTE xy );
	void DrawRoute( BYTE lr );
};

#define LAMP_LINE_H		1
#define LAMP_LINE_13	2
#define LAMP_LINE_V		4
#define LAMP_LINE_24	8
#define LAMP_SHUNT		100
#define LAMP_CALLON		101

void MyDC::DrawLamp( BYTE xy ) {
	int dx = dir * m_nLampSize;
	if ( xy >= LAMP_SHUNT ) {	// call on, shunt
		dx = dx * 8 / 10;
		int h = m_nLampSize * 8 / 17;
		if ( xy == LAMP_CALLON ) {
			m_pDC->MoveTo(cx + dx, cy);
			m_pDC->LineTo(cx, cy - h);
			m_pDC->LineTo(cx, cy + h);
			m_pDC->LineTo(cx + dx, cy);
		}
		else {
			m_pDC->MoveTo(cx, cy);
			m_pDC->LineTo(cx + dx, cy - h);
			m_pDC->LineTo(cx + dx, cy + h);
			m_pDC->LineTo(cx, cy);
		}
	}
	else {
		int r = m_nLampSize / 2, r1;
		CRect cir;
		CPoint c( cx + r * dir, cy );
		cir.SetRect( c.x-r, c.y-r, c.x+r, c.y+r );
			
		m_pDC->Ellipse(&cir);
		r1 = r * 2 / 3;
		cir.SetRect(c.x-r1,c.y-r1,c.x+r1,c.y+r1);

	    // 1 -  2 /  4 |  8 \;
		if ( xy & 1 ) {		// -
			m_pDC->MoveTo(c.x-r,c.y);
			m_pDC->LineTo(c.x+r,c.y);
		}
		if ( xy & 2 ) {     // /
			m_pDC->MoveTo(c.x+r1,c.y+r1);
			m_pDC->LineTo(c.x-r1,c.y-r1);
		}
		if ( xy & 4 ) {	// |
			m_pDC->MoveTo(c.x-1,c.y+r);
			m_pDC->LineTo(c.x-1,c.y-r);
			m_pDC->MoveTo(c.x+1,c.y+r);
			m_pDC->LineTo(c.x+1,c.y-r);
		}
		if ( xy & 8 ) {	// \ Y
			m_pDC->MoveTo(c.x-r1,c.y+r1);
			m_pDC->LineTo(c.x+r1,c.y-r1);
		}
	}
	cx += dx;
};

void MyDC::DrawRoute( BYTE lr ) {
	int dx = dir * m_nLampSize * 7 / 10;
	if ( lr & 1 ) {	// left
		m_pDC->MoveTo(cx, cy);
		m_pDC->LineTo(cx + dx, cy + dx );
	}
	if ( lr & 2 ) {	// right
		m_pDC->MoveTo(cx, cy);
		m_pDC->LineTo(cx + dx, cy - dx );
	}
};

extern char *pcSigTypeStr[];


CSignal::CSignal(int t, int cx, int cy) : CGraphObject(CRect(CPoint(cx,cy), CSize(5,5)),"s")
{
	x = cx;
	y = cy;
	ASSERT_VALID(this);
	SetName("");
	m_nShape = B_NAME_B;
	m_nLamp = m_nType = m_nUD = m_nLR = m_nILR = 0;
	s = CBlip(CPoint(cx,cy + GridSize * 2));
	m_nRoute = 1;
	m_bF1 = m_bF2 = FALSE;
	nB.y = GridSize * 2;
	nS.x = GridSize;
	nS.y = GridSize * 2;
	UpdateRect();
	Init();
	m_iDiry=0;
}

void CSignal::DrawRlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis) 
{
	CRect cir;
	int r = GridSize - 1;
  	int r1 = r * 2 / 9;

	cir.SetRect(c.x+(int)((r*iPos-iIndDis)*iDir),c.y-r,c.x+(int)((r*(iPos+2)-iIndDis)*iDir),c.y+r);
	pDC->Ellipse(&cir);

	pDC->MoveTo(c.x+((r*(iPos+1)-iIndDis)*iDir)+r1,c.y+(r1*4));
	pDC->LineTo(c.x+((r*(iPos+1)-iIndDis)*iDir)+r1,c.y-(r1*4));
	pDC->MoveTo(c.x+((r*(iPos+1)-iIndDis)*iDir)-r1,c.y+(r1*4));
	pDC->LineTo(c.x+((r*(iPos+1)-iIndDis)*iDir)-r1,c.y-(r1*4));
}
void CSignal::DrawGlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis)  
{
	CRect cir;
	int r = GridSize - 1;
  	int r1 = r * 2 / 9;

	cir.SetRect(c.x+(int)((r*iPos-iIndDis)*iDir),c.y-r,c.x+(int)((r*(iPos+2)-iIndDis)*iDir),c.y+r);
	pDC->Ellipse(&cir);

	pDC->MoveTo(c.x+((r*(iPos+1)-iIndDis)*iDir)-r,c.y);
	pDC->LineTo(c.x+((r*(iPos+1)-iIndDis)*iDir)+r,c.y);						
}
void CSignal::DrawYlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis)  
{
	CRect cir;
	int r = GridSize - 1;
  	int r1 = r * 2 / 9;

	cir.SetRect(c.x+(int)((r*iPos-iIndDis)*iDir),c.y-r,c.x+(int)((r*(iPos+2)-iIndDis)*iDir),c.y+r);
	pDC->Ellipse(&cir);

	pDC->MoveTo((c.x+((r*(iPos+1)-iIndDis)-(r1*3))*iDir),c.y+(r1*3));
	pDC->LineTo((c.x+((r*(iPos+1)-iIndDis)+(r1*3))*iDir),c.y-(r1*3));
}
void CSignal::DrawSlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis)  
{
	if ( iPos == 1 ) iPos = -1;
	CRect cir;
	int r = GridSize - 1;
	c.x += iDir;			
	CPoint c1 = c + CPoint(iDir*r,m_iDiry*r);
	CPoint c2 = c;
	c2.x -= r * iDir;
	c2.y -= r * m_iDiry;
	CPoint a1, a2;
	if (iDir * m_iDiry > 0) {
		a1 = CPoint(c2.x,c1.y);
		a2 = CPoint(c1.x,c2.y);
	}
	else {
		a2 = CPoint(c2.x,c1.y);
		a1 = CPoint(c1.x,c2.y);
	}
	r *= 2;
	cir.SetRect(c2.x-r,c2.y-r,c2.x+r,c2.y+r);
	pDC->Pie(cir,a2,a1);
}
void CSignal::DrawClamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis) 
{
	CRect cir;
	int r = GridSize - 1;
  	int r1 = r * 2 / 9;

	CString strTemp;
	cir.SetRect(c.x+(int)((r*iPos-iIndDis)*iDir),c.y-r,c.x+(int)((r*(iPos+2)-iIndDis)*iDir),c.y+r);
	pDC->Ellipse(&cir);
	strTemp = "C";
	c.x = c.x+(int)((r*(iPos+1)-iIndDis)*iDir);
	TextOutAtPointCenter( pDC, c, strTemp );
}
void CSignal::DrawLIlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int & iIndDis) 
{
	CRect cir;
	int r = GridSize - 1;
  	int r1 = r * 2 / 4;
	int iLR;
	if ( m_nLR == 0 ) iLR = -1;
	if ( m_nLR == 1 ) iLR = 1;

	if (( m_nLR == 0 && m_nILR == 1 ) || ( m_nLR == 1 && m_nILR == 2 ))  {
		iIndDis = 0;
/*
		pDC->MoveTo(c.x+((r*iPos)*iDir)+(r*2),c.y);
		pDC->LineTo(c.x+((r*iPos)*iDir)+(r*2),c.y-r);

		pDC->MoveTo(c.x+((r*iPos)*iDir)+(r*2),c.y-r);
		pDC->LineTo(c.x+((r*iPos)*iDir)+r,c.y-r);

		cir.SetRect(c.x+((r*iPos)*iDir)+r,c.y-r-r1,c.x+((r*iPos)*iDir)+(r*2),c.y-r+r1);
		pDC->Ellipse(&cir);
*/


		pDC->MoveTo(c.x+(((r*iPos))+(r1))*iDir,c.y);
		pDC->LineTo(c.x+(((r*iPos))+(r1))*iDir,c.y-r);
		pDC->LineTo(c.x+(((r*iPos))*iLR+(((r1+r1))*iLR)*iLR)*iDir,c.y-r);
		cir.SetRect(c.x+(((r*iPos))*iLR+(((r1+r1))*iLR)*iLR)*iDir,c.y-r-r1,c.x+(((r*iPos))*iLR+(((r1+r1+r1+r1))*iLR)*iLR)*iDir,c.y-r+r1);
		pDC->Ellipse(&cir);
/*
		pDC->MoveTo(c.x-(((r*iPos)*iDir)+(r1*iDir))*iDir,c.y);
		pDC->LineTo(c.x-(((r*iPos)*iDir)+(r1*iDir))*iDir,c.y-r);
		pDC->LineTo(c.x-(((r*iPos)*iDir)*iLR+(((r1+r1)*iDir)*iLR)*iLR)*iDir,c.y-r);
		cir.SetRect(c.x-(((r*iPos)*iDir)*iLR+(((r1+r1)*iDir)*iLR)*iLR)*iDir,c.y-r-r1,c.x-(((r*iPos)*iDir)*iLR+(((r1+r1+r1+r1)*iDir)*iLR)*iLR)*iDir,c.y-r+r1);
		pDC->Ellipse(&cir);
		*/
/*		cir.SetRect(c.x+((int)(r*(iPos-(0.5*iDir*iLR)))*iDir*iLR)+(r1*iDir*iLR),c.y-r-r1,c.x+((int)(r*(iPos-(1.5*iDir*iLR)))*iDir*iLR)+(r1*iDir*iLR),c.y-r+r1);
		pDC->Ellipse(&cir);
		pDC->MoveTo(c.x+((r*iPos)*iDir)+(r1*iDir*iLR),c.y-r);
		pDC->LineTo(c.x+((int)(r*(iPos-(0.5*iDir*iLR)))*iDir)+(r1*iDir*iLR),c.y-r);
*/
	}

	if (( m_nLR == 0 && m_nILR == 2 ) || ( m_nLR == 1 && m_nILR == 1 ))  {
		iIndDis = 0;
		pDC->MoveTo(c.x+(((r*iPos))+(r1))*iDir,c.y);
		pDC->LineTo(c.x+(((r*iPos))+(r1))*iDir,c.y+r);
		pDC->LineTo(c.x+(((r*iPos))*iLR+(((r1+r1))*iLR)*iLR)*iDir,c.y+r);
		cir.SetRect(c.x+(((r*iPos))*iLR+(((r1+r1))*iLR)*iLR)*iDir,c.y+r+r1,c.x+(((r*iPos))*iLR+(((r1+r1+r1+r1))*iLR)*iLR)*iDir,c.y+r-r1);
		pDC->Ellipse(&cir);
/*		cir.SetRect(c.x+((int)(r*(iPos-(0.5*iDir*iLR)))*iDir)+(r1*iDir*iLR),c.y+r+r1,c.x+((int)(r*(iPos-(1.5*iDir*iLR)))*iDir)+(r1*iDir*iLR),c.y+r-r1);
		pDC->Ellipse(&cir);
		pDC->MoveTo(c.x+((r*iPos)*iDir*iLR)+(r1*iDir*iLR),c.y+r);
		pDC->LineTo(c.x+((int)(r*(iPos-(0.5*iDir*iLR)))*iDir)+(r1*iDir*iLR),c.y+r);
		*/
	}

	if  ( m_nILR == 3 )  {
		iIndDis = 0;
		pDC->MoveTo(c.x+(((r*iPos))+(r1))*iDir,c.y-r);
		pDC->LineTo(c.x+(((r*iPos))+(r1))*iDir,c.y+r);
		pDC->LineTo(c.x+(((r*iPos))*iLR+(((r1+r1))))*iDir,c.y+r);
		pDC->MoveTo(c.x+(((r*iPos))+(r1))*iDir,c.y-r);
		pDC->LineTo(c.x+(((r*iPos))*iLR+(((r1+r1))))*iDir,c.y-r);
		cir.SetRect(c.x+(((r*iPos))*iLR+(((r1+r1))))*iDir,c.y-r-r1,c.x+(((r*iPos))+(((r1+r1+r1+r1))))*iDir,c.y-r+r1);
		pDC->Ellipse(&cir);
		cir.SetRect(c.x+(((r*iPos))*iLR+(((r1+r1))))*iDir,c.y+r+r1,c.x+(((r*iPos))+(((r1+r1+r1+r1))))*iDir,c.y+r-r1);
		pDC->Ellipse(&cir);


/*
		pDC->MoveTo(c.x+((r*(iPos+1-1))*iDir)-r,c.y-r);
		pDC->LineTo(c.x+((r*(iPos+1-1))*iDir)-r,c.y+r);

		cir.SetRect(c.x+((int)(r*(iPos+1.5-1))*iDir)-r,c.y-r-r1,c.x+(((int)(r*(iPos+1.5-1))+(r1*(iPos+1-1)))*iDir)-r,c.y-r+r1);
		pDC->Ellipse(&cir);
		pDC->MoveTo(c.x+((r*(iPos+1-1))*iDir)-r,c.y-r);
		pDC->LineTo(c.x+((int)(r*(iPos+1.5-1))*iDir)-r,c.y-r);

		cir.SetRect(c.x+((int)(r*(iPos+1.5-1))*iDir)-r,c.y+r-r1,c.x+(((int)(r*(iPos+1.5-1))+(r1*(iPos+1-1)))*iDir)-r,c.y+r+r1);
		pDC->Ellipse(&cir);
		pDC->MoveTo(c.x+((r*(iPos+1-1))*iDir)-r,c.y+r);
		pDC->LineTo(c.x+((int)(r*(iPos+1.5-1))*iDir)-r,c.y+r);
*/
	}
}
void CSignal::DrawIndlamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis) 
{
	CRect cir;
	int r = GridSize - 1;
	int r1 = r * 2 / 9;
	if ( m_nILR == 4 )  {
		cir.SetRect(c.x+((r*(iPos+1)-iIndDis)*iDir),c.y-r,c.x+((r*iPos-iIndDis)*iDir),c.y+r);
		pDC->Rectangle(&cir);
	}

}

void CSignal::DrawFreelamp(CDC* pDC , COLORREF cColor, CPoint c , int iDir, int iPos , int iIndDis) 
{
	CRect cir;
	int r = GridSize - 1;
	int r1 = r * 2 / 9;
	cir.SetRect(c.x+((r*(iPos+1)-iIndDis)*iDir),c.y-r,c.x+((r*iPos-iIndDis)*iDir),c.y+r);
	pDC->Rectangle(&cir);
}

void CSignal::Draw(CDC* pDC , COLORREF cColor) {
	if (!isValid()) return;

	CString strTemp;
	CPen	newPen;
	CBrush	black(cColor), *oldbr;

	int	sx, dir;
	int r = GridSize - 1, r1, r2;
	int iIndDis = r*2;	

    if( newPen.CreatePen( PS_SOLID, 1, cColor ) )
    {
        CPen* pOldPen = pDC->SelectObject( &newPen );

        // Draw with the pen

		//m_nLR 방향:  왼쪽 0  오른쪽 1
		dir = m_nLR ? 1 : -1;		// 0:LEFT, 1:RIGHT
		if ( m_nUD == 1 ) dir = dir * -1;
		sx = GridSize * dir;

        // Draw button

		CRect cir(x-r,y-r,x+r,y+r);
		pDC->Ellipse(&cir);								       // 신호기 아래 버튼 그리는 부분

        // Draw post
		pDC->MoveTo(s+CPoint(0,GridSizeH));
		pDC->LineTo(s+CPoint(0,-GridSizeH));
		pDC->MoveTo(s);
	
		CPoint c = s;
		CPoint p;
		if (m_nRoute > 1) {	// 장내
			r1 = r - 2;
			r2 = r1/2 + 1;
			c.x += (GridSizeH + r1) * dir;
			pDC->LineTo(c);
			if (m_nRoute > 3 || m_nType >= 2) {
				pDC->Rectangle(c.x-r1,c.y-r1,c.x+r1,c.y+r1);
				CString route;
				route.Format("%d",m_nRoute);
				TextOutAtPointCenter( pDC, c, route );
			}
			else {
				pDC->Ellipse(c.x-r1,c.y-r1,c.x+r1,c.y+r1);
				oldbr = pDC->SelectObject(&black);
				DrawDot(pDC,c);
				DrawDot(pDC,c+CPoint(0,r2));
				DrawDot(pDC,c+CPoint(0,-r2));
				DrawDot(pDC,c+CPoint(r2,0));
				DrawDot(pDC,c+CPoint(-r2,0));
				pDC->SelectObject(oldbr);
			}
			int r2 = 0;
		}
		else {
			int r2 = 0;
			r1 = r;
			c.x += dir * GridSize * 2;
			pDC->LineTo(c);
		}
		m_iDiry = (y < c.y) ? 1 : -1;
		if ((m_nType <= _ISTART)) {
			if (m_nType == _HOME ) {

				pDC->MoveTo(c.x+((r*1-r1)*dir),c.y);
				pDC->LineTo(c.x+((r*3)*dir),c.y);

				switch ( m_nLamp ) {
				case 0 : DrawClamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 9 , iIndDis);
						 break;
				case 1 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 9 , iIndDis);
						 break;
				case 2 : DrawClamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 3 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 4 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawClamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 9 , iIndDis);
						 break;
				case 5 : DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 break;
				case 6 : DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 break;
				case 7 : DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 8 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawClamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 9 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 11 , iIndDis);
						 break;
				}				
			}

			if (m_nType == _START || m_nType == _ISTART) {
				switch ( m_nLamp ) {
				case 0 : DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);		//2012add
						 DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 1 : DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);		//2012add
						 DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 if ( m_nILR == 5 ) {
							DrawFreelamp( pDC , cColor, c , dir, 5 , iIndDis);
						 }
						 break;
				case 2 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);		//2012add
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 3 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);		//2012add
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 if ( m_nILR == 5 ) {
							DrawFreelamp( pDC , cColor, c , dir, 7 , iIndDis);
						 }
						 break;
				case 4 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawLIlamp( pDC , cColor, c , dir, 1 , iIndDis);		//2012add
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 break;
				case 5 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 break;
				case 6 : c.x -= dir * GridSize * 2;
					     DrawLIlamp( pDC , cColor, c , dir, 0, iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 1 , iIndDis);
					     DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 7 , iIndDis);
						 break;
				case 7 : DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawIndlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 if ( m_nILR == 5 ) {
							DrawFreelamp( pDC , cColor, c , dir, 5 , iIndDis);
						 }
						 break;
				case 8 : DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
						 DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
						 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
						 break;
				}
			}

			if (m_nType == _ASTART ) {
					cir.SetRect(c.x-r,c.y-r,c.x+r,c.y+r);
					pDC->Ellipse(&cir);
				    
					r1 = r * 2 / 9;

					cir.SetRect(c.x+((r*3)*dir),c.y-r,c.x+(r*dir),c.y+r);
					pDC->Ellipse(&cir);
				    cir.SetRect(c.x-r1,c.y-(r1*4),c.x+r1,c.y+(r1*4));

					pDC->MoveTo(c.x+r1,c.y+(r1*4));
					pDC->LineTo(c.x+r1,c.y-(r1*4));
					pDC->MoveTo(c.x-r1,c.y+(r1*4));
					pDC->LineTo(c.x-r1,c.y-(r1*4));

					pDC->MoveTo(c.x+((r*2)*dir)-r,c.y);
					pDC->LineTo(c.x+((r*2)*dir)+r,c.y);

					pDC->MoveTo((c.x-((int)(r*1.7)-(r1*2))*dir),c.y+(r1*2));
					pDC->LineTo((c.x-((int)(r*1.7)+(r1*2))*dir),c.y-(r1*2));				

			}

			if (m_nType == _OHOME ) {
				switch ( m_nLamp ) {
					case 0 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
							 DrawYlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
							 DrawGlamp( pDC , cColor, c , dir, 7 , iIndDis);
							 break;
					case 1 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
						     DrawYlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 DrawYlamp( pDC , cColor, c , dir, 5 , iIndDis);
							 break;
					case 2 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
						     DrawYlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 break;
					case 3 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 break;
					case 4 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
						     DrawYlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
							 break;
					case 5 : DrawLIlamp( pDC , cColor, c , dir, 0 , iIndDis);
						     DrawRlamp( pDC , cColor, c , dir, 1 , iIndDis);
						     DrawYlamp( pDC , cColor, c , dir, 3 , iIndDis);
							 DrawGlamp( pDC , cColor, c , dir, 5 , iIndDis);
							 break;

				}
			}

			if (m_nType == _LOS ) {
					cir.SetRect(c.x-r,c.y-r,c.x+r,c.y+r);
					pDC->Ellipse(&cir);

					r1 = r * 2 / 9;

					pDC->MoveTo(c.x+r1,c.y+(r1*4));
					pDC->LineTo(c.x+r1,c.y-(r1*4));
					pDC->MoveTo(c.x-r1,c.y+(r1*4));
					pDC->LineTo(c.x-r1,c.y-(r1*4));

			}

			if (m_nType == _FREE ) {

					pDC->MoveTo(c.x+((r*1-r1)*dir),c.y);
					pDC->LineTo(c.x+((r*2)*dir),c.y);

					DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
					DrawFreelamp( pDC , cColor, c , dir, 4 , iIndDis);
			}

			if (m_nType == _STOP ) {

					cir.SetRect(c.x+(int)((r*1)*dir),c.y-r,c.x,c.y+r);
					pDC->Rectangle(&cir);
			}

			if (m_nType == _TOKENLESSG ) {
			 		c.x += (r + r1 + 2) * dir;
			 		cir.SetRect(c.x-r1*5,c.y-r1*2,c.x+r1*5,c.y+r1*2);
			 		pDC->Rectangle(cir);
			 		CString nameTTB = "TGB";
			 		TextOutAtPointCenter( pDC, c, nameTTB );
			}

			if (m_nType == _TOKENLESSC ) {
			 		c.x += (r + r1 + 2) * dir;
			 		cir.SetRect(c.x-r1*5,c.y-r1*2,c.x+r1*5,c.y+r1*2);
			 		pDC->Rectangle(cir);
			 		CString nameTTB = "TCB";
			 		TextOutAtPointCenter( pDC, c, nameTTB );
			}

			if (m_nType == _OG ) {
			 		c.x += (r + r1 + 2) * dir;
			 		cir.SetRect(c.x-r1*5,c.y-r1*2,c.x+r1*5,c.y+r1*2);
			 		pDC->Rectangle(cir);
			 		CString nameTTB = "OG";
			 		TextOutAtPointCenter( pDC, c, nameTTB );
			}

			if (m_nType == _OC ) {
			 		c.x += (r + r1 + 2) * dir;
			 		cir.SetRect(c.x-r1*5,c.y-r1*2,c.x+r1*5,c.y+r1*2);
			 		pDC->Rectangle(cir);
			 		CString nameTTB = "OC";
			 		TextOutAtPointCenter( pDC, c, nameTTB );
			}

			if (m_nType == _SHUNT || m_nType == _GSHUNT ) {
				if ( m_nLamp == 10 ) {
					DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
				} else if ( m_nLamp == 11 ) {
					pDC->MoveTo(c.x+((r*1-r1)*dir),c.y);
					pDC->LineTo(c.x+((r*2)*dir),c.y);
					DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
					DrawFreelamp( pDC , cColor, c , dir, 4 , iIndDis);
				} else if ( m_nLamp == 12 ) {
					DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
					DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
				    if ( m_nILR == 5 ) {
						DrawFreelamp( pDC , cColor, c , dir, 5 , iIndDis);
					}
					else
					{
						DrawIndlamp( pDC , cColor, c , dir, 5 , iIndDis);
					}

				} else if ( m_nLamp == 13 ) {
					pDC->MoveTo(c.x+((r*3-r1)*dir),c.y);
					pDC->LineTo(c.x+((r*4)*dir),c.y);
					DrawSlamp( pDC , cColor, c , dir, 1 , iIndDis);
					DrawRlamp( pDC , cColor, c , dir, 3 , iIndDis);
				    if ( m_nILR == 5 ) {
						DrawFreelamp( pDC , cColor, c , dir, 5 , iIndDis);
					} 
				}

			}

		}

		if (m_nShape & B_NAME_B)
			TextOutAtPointCenter( pDC, nB, Name );
		if (m_nShape & B_NAME_S)
			TextOutAtPointCenter( pDC, nS, Name );


		CBlip::Draw( pDC );
        pDC->SelectObject( pOldPen );
    }
}

void CSignal::PropertyChange(CWnd *wnd) {
	CSignalDlg dlg(this);
	dlg.DoModal();
	Init();
}

void CSignal::Init() {
	m_pBlips[0] = &s;
	m_pBlips[1] = &nB;
	m_pBlips[2] = &nS;
	m_pBlips[3] = NULL;
	m_nHandle = 3;
	nB.Visible(m_nShape & B_NAME_B);
	nS.Visible(m_nShape & B_NAME_S);
}

BOOL CSignal::isNoGrid(CBlip *pBlip)
{
	if (m_pBlips[0] == pBlip) return TRUE;
	else if (m_pBlips[1] == pBlip) return TRUE;
	else if (m_pBlips[2] == pBlip) return TRUE;
	else return FALSE;
}

void CSignal::UpdateRect()
{
	CGraphObject::UpdateRect();
	m_position |= GetLampRect();
}

CRect CSignal::GetLampRect()
{
	CPoint c = s;
	if (m_nLR) c.x += GridSize*3;
	else c.x -= GridSize*3;
	int r = GridSize - 1;
	CRect cir(x-r,y-r,x+r,y+r);
	return CRect(c.x-r,c.y-r,c.x+r,c.y+r) | cir;
}

void CSignal::Read(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar >> m_nShape >> m_nType >> m_nLamp >> m_nRoute >> m_nUD >> m_nLR >> m_nILR;
	ar >> m_bF1 >> m_bF2 >> m_strStraightSignal;
	Init();
}

void CSignal::Write(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar << m_nShape << m_nType << m_nLamp << m_nRoute << m_nUD << m_nLR << m_nILR;
	ar << m_bF1 << m_bF2 << m_strStraightSignal;
}


void CSignal::DrawDot(CDC *pDC, CPoint p)
{
	pDC->Ellipse(p.x-1,p.y-1,p.x+1,p.y+1);
}

BOOL CSignal::ButtonAt(CPoint point)
{
	CRect r = GetRect();
	r.NormalizeRect();
	return r.PtInRect(point);
}

void CSignal::Dump( ostream &os )
{
	
	CString strSigType = pcSigTypeStr[m_nType];

	if ( strSigType == "GSHUNT" ) strSigType = "SHUNT";
	os	<< Name  << ","	<< strSigType;

	if ( m_nLR == 0 ) os << ",L";
	if ( m_nLR == 1 ) os << ",R";

	switch ( m_nType ) {
	case _HOME		:	if ( m_nLamp == 0 ) os << ",GYR,C";
						if ( m_nLamp == 1 ) os << ",GYR,S";
						if ( m_nLamp == 2 ) os << ",YR,C";
						if ( m_nLamp == 3 ) os << ",YR,S";
						if ( m_nLamp == 4 ) os << ",YR,C,S";
						if ( m_nLamp == 5 ) os << ",YR";
						if ( m_nLamp == 6 ) os << ",GR";
						if ( m_nLamp == 7 ) os << ",GYR";
						if ( m_nLamp == 8 ) os << ",GYR,C,S";
						if ( m_nILR == 1 )  os << ",L";
						if ( m_nILR == 2 )  os << ",R";
						if ( m_nILR == 3 )  os << ",LR";
						if ( m_nILR == 4 )  os << ",RI";                      
						break;
	case _START		:	if ( m_nLamp == 0 ) os << ",GYR";
						if ( m_nLamp == 1 ) os << ",YR";
						if ( m_nLamp == 2 ) os << ",GYR,S";
						if ( m_nLamp == 3 ) os << ",YR,S";
						if ( m_nLamp == 4 ) os << ",GR,S";
//						if ( m_nLamp == 5 ) os << ",YR,S";
						if ( m_nLamp == 5 ) os << ",R,S";
						if ( m_nLamp == 6 ) os << ",GYR,SY";
						if ( m_nLamp == 7 ) os << ",GR";
						if ( m_nILR == 1 )  os << ",L";			//20140424 시뮬레이터 구분을 위해 수정.
						if ( m_nILR == 2 )  os << ",R";
						if ( m_nILR == 3 )  os << ",LR";
						if ( m_nILR == 4 )  os << ",RI";                      
						break;
	case _SHUNT		:	if ( m_nLamp == 0 ) os << ",S";
						if ( m_nLamp == 1 ) os << ",S";
						if ( m_nLamp == 2 ) os << ",R,S";
						if ( m_nLamp == 3 ) os << ",R,S";
						break;
	case _GSHUNT	:	if ( m_nLamp == 12 ) os << ",R,S";
						else os << ",S";
						if ( m_nILR == 4 )  os << ",RI";                      
						break;
	case _ASTART	:	os << ",GR";
						break;
	case _ISTART	:	if ( m_nLamp == 0 ) os << ",GYR";
						if ( m_nLamp == 1 ) os << ",YR";
						if ( m_nLamp == 2 ) os << ",GYR,S";
						if ( m_nLamp == 3 ) os << ",YR,S";
						if ( m_nLamp == 4 ) os << ",GR,S";
						if ( m_nLamp == 5 ) os << ",R,S";
						if ( m_nLamp == 6 ) os << ",GYR,SY";
						if ( m_nLamp == 7 ) os << ",GR";
						if ( m_nLamp == 8 ) os << ",GR,S";
						break;
	case _OHOME		:	if ( m_nLamp == 0 ) os << ",GYR,SY";
						if ( m_nLamp == 1 ) os << ",YR,SY";
						if ( m_nLamp == 2 ) os << ",R,SY";
						if ( m_nLamp == 3 ) os << ",YR";
						if ( m_nLamp == 4 ) os << ",GR,SY";
						if ( m_nLamp == 5 ) os << ",GYR";
						break;
	case _TOKENLESSC:	os << ",G";
						break;
	case _TOKENLESSG:	os << ",G";
						break;
	case _OC		:	os << ",G";
						break;
	case _OG		:	os << ",G";
						break;
	case _CALLON	:	os << ",C";
						break;
	case _OCALLON	:	os << ",C";
						break;
	}
	if ( m_bF2 == TRUE )  os << ",A";
	// 아카우라역 대항진로 항목에 대항신호기 R검지 요청에 따라 추가 
	if ( s.m_sBlipName != "" ) {
		os << ",";
		os << s.m_sBlipName;
	}
	os	<< "\n";
}
