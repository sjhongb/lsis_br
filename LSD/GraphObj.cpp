//=======================================================
//==              GraphObj.cpp
//=======================================================
//	파 일 명 :  GraphObj.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "GraphObj.h"
#include "LSDDoc.h"
#include "LSDView.h"

IMPLEMENT_SERIAL(CGraphObject, CObject, 0)

CGraphObject::~CGraphObject() {
	if ( m_pBmpInfo ) delete m_pBmpInfo;
}

void CGraphObject::Dump(CDumpContext& dc)
{
	dc << Name << "(" << x << "," << y << ")\n";
}

void CGraphObject::DrawName(CDC* pDC) {
}

void swap(CPoint &a, CPoint &b) {
	CPoint c = a;
	a = b;
	b = c;
}

#include "track.h"
void CGraphObject::DrawTracker(CDC *pDC, TrackerState state)
{
	ASSERT_VALID(this);
	switch (state) {
	case normal : break;
	case selected :
	case active :
		if (!IsKindOf(RUNTIME_CLASS(CTrack))) {
			CRect r = GetRect();
			pDC->LPtoDP( r );
			int nSaveDC = pDC->SaveDC();
			pDC->SetMapMode(MM_TEXT);
			r.NormalizeRect();
			pDC->DPtoLP( r );
			pDC->RestoreDC( nSaveDC );
		}

		int nHandleCount = GetHandleCount();
		for (int nHandle = 0; nHandle < nHandleCount; nHandle++) {
			if (m_pBlips[nHandle] && m_pBlips[nHandle]->isValid())
			m_pBlips[nHandle]->DrawTracker(pDC, state);
		}
		break;
	}
}

void CGraphObject::Draw(CDC *pDC , COLORREF cColor)
{
	ASSERT_VALID(this);
	for (int nHandle = 0; nHandle < m_nHandle; nHandle++) {
		if (m_pBlips[nHandle]->isValid()) {
			m_pBlips[nHandle]->Draw(pDC , cColor );
		}
	}
}

CBlip *CGraphObject::HitTest(CPoint point, CLSDView* pView, BOOL bSelected)
{
	ASSERT_VALID(this);
	ASSERT(pView != NULL);
	if (bSelected) {
		for (int n = 0; n < m_nHandle; n++) {
			if (m_pBlips[n] && m_pBlips[n]->isValid()) {
				CBlip *bl = m_pBlips[n]->HitTest(point, pView, bSelected);
				if (bl)	{
					bl->m_pParent = this;
					return bl;
				}
			}
		}
	}
		if (point.x >= m_position.left && point.x < m_position.right &&
			point.y >= m_position.top && point.y < m_position.bottom)
			return this;

	return NULL;
}

BOOL CGraphObject::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);
	CRect fixed = m_position;
	fixed.NormalizeRect();
	CRect rectT = rect;
	rectT.NormalizeRect();
	return !(rectT & fixed).IsRectEmpty();
}

int CGraphObject::GetHandleCount()
{
	ASSERT_VALID(this);
	return m_nHandle;	
}

CPoint CGraphObject::GetHandle(int nHandle)
{
	ASSERT_VALID(this);
	CPoint p = *m_pBlips[nHandle-1];
	return p;
}


void CGraphObject::UpdateRect() {
	if (m_nHandle) {
		m_position = CBlip::GetRect();
		for (int i=0; i<m_nHandle; i++) {
			CBlip *bl = m_pBlips[i];
			if (bl != NULL && bl->isValid()) {
				m_position |= bl->GetRect();
			}
		}
		m_position.NormalizeRect();
		if (m_position.bottom == m_position.top) m_position.InflateRect(0,2);
		if (m_position.left == m_position.bottom) m_position.InflateRect(2,0);
	}
}

void CGraphObject::Invalidate()
{
	ASSERT_VALID(this);
	m_pDocument->UpdateAllViews(NULL, HINT_UPDATE_DRAWOBJ, this);	
}

CGraphObject* CGraphObject::Clone(CLSDDoc* pDoc)
{
	ASSERT_VALID(this);

	CRuntimeClass* pClassRef = GetRuntimeClass();
	CGraphObject* pClone = (CGraphObject*)pClassRef->CreateObject();

	
	BYTE * pBuf = (BYTE *)new char [4096];
	CMemFile file( pBuf, 4096, 256 );

	CArchive ar1(&file,CArchive::store);
	Serialize(ar1);
	ar1.Close();
	CMemFile file1( pBuf, 4096, 256 );
	file1.SetLength(4096);
	CArchive ar2(&file1,CArchive::load);
	pClone->Serialize(ar2);
	pClone->Active(TRUE);
	ar2.Close();

	ASSERT_VALID(pClone);

	if (pDoc != NULL) {
		pDoc->Add(pClone);
			TRACE("Normal Add\n");
	}
	return pClone;
}

CGraphObject* CGraphObject::SaveClone(CLSDDoc* pDoc)
{
	ASSERT_VALID(this);

	CRuntimeClass* pClassRef = GetRuntimeClass();
	CGraphObject* pClone = (CGraphObject*)pClassRef->CreateObject();

	BYTE * pBuf = (BYTE *)new char [4096];
	CMemFile file( pBuf, 4096, 256 );

	CArchive ar1(&file,CArchive::store);
	Serialize(ar1);
	Sleep(10);
	ar1.Close();
	CMemFile file1( pBuf, 4096, 256 );
	file1.SetLength(4096);
	CArchive ar2(&file1,CArchive::load);
	pClone->Serialize(ar2);
	Sleep(10);
	pClone->Active(TRUE);
	ar2.Close();

	ASSERT_VALID(pClone);

	if (pDoc != NULL) {
		pDoc->CloneAdd(pClone);
	}
	return pClone;
}

void CGraphObject::Remove()
{
	delete this;
}

CRect CGraphObject::GetHandleRect(int nHandleID, CLSDView* pView)
{
	ASSERT_VALID(this);
	ASSERT(pView != NULL);

	CRect rect;
	CPoint point = GetHandle(nHandleID);
	pView->DocToClient(point);
	rect.SetRect(point.x-3, point.y-3, point.x+3, point.y+3);
	pView->ClientToDoc(rect);
	return rect;
}

void CGraphObject::MoveHandleTo(CBlip *Handle, CPoint point, CLSDView* pView)
{
	if (*Handle == point) return;
	if (pView == NULL) Invalidate();
	else pView->InvalObj(this);
	Handle->Offset(point, pView);
	UpdateRect();
	if (pView == NULL) Invalidate();
	else {
		pView->InvalObj(this);
		pView->GetDocument()->SetModifiedFlag();
	}
}

void CGraphObject::Offset(const CPoint point, CLSDView* pView)
{
	if (point.x == x && point.y == y) return;
	if (pView != NULL) pView->InvalObj(this);
	CBlip::Offset( point );
	for (int i=0; i<m_nHandle; i++) {
		CBlip *bl = m_pBlips[i];
		if (bl != NULL) bl->Offset(point);
	}
	UpdateRect();
	if (pView != NULL) {
		pView->InvalObj(this);
		pView->GetDocument()->SetModifiedFlag();
	}
}

void CGraphObject::Serialize(CArchive &ar) {
	CObject::Serialize(ar);
	int i;
	Init();
	if (ar.IsStoring()) {
		CBlip::Write(ar);
		ar << Name;
		for (i=0; i<m_nHandle; i++) {
			if (m_pBlips[i] != NULL) ar << *m_pBlips[i];
		}
	}
	else {
		CBlip::Read(ar);
		CString s;
		ar >> s;
		i = s.Find(' ');
		if (i > 0) Name = s.Left(i);
		else Name = s;
		for (i=0; i<m_nHandle; i++) {
			if (m_pBlips[i]) ar >> *m_pBlips[i];
		}
		Init();
	}
	UpdateRect();
}


int  CGraphObject::BlipType(CBlip* Handle)	// 0/Track(N,C,R), 2/Buttons(Base), 3/Signals, 4/Names
{
	return 0;
}

void TextOutAtPointCenter( CDC *pDC, CPoint &point, CString &string, COLORREF cColor) {
	int dx = 1;
	CSize sz = pDC->GetViewportExt();

	CRect rect;
	if (sz.cx < 0) {
		CSize sz = pDC->GetTextExtent( string );
		int w = sz.cx;
		rect.left = point.x - 30 + w;
		rect.right = point.x + 30 + w;
		rect.top = point.y + 10;
		rect.bottom = point.y - 10;
	}
	else {
		rect.left = point.x - 30;
		rect.right = point.x + 30;
		rect.top = point.y + 10;
		rect.bottom = point.y - 10;
	}
	pDC->DrawText( string, rect, DT_CENTER | DT_NOCLIP | DT_SINGLELINE | DT_VCENTER );
}
