//=======================================================
//==              LockEdit.cpp
//=======================================================
//	파 일 명 :  LockEdit.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "../include/StrList.h"
#include "LockEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLockEdit dialog


CLockEdit::CLockEdit(CString &name, CMyStrList *sw, CWnd* pParent /*=NULL*/)
	: CDialog(CLockEdit::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLockEdit)
	//}}AFX_DATA_INIT
	m_pName = &name;
	m_pSwitch = sw;
}


void CLockEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLockEdit)
	DDX_Control(pDX, IDC_LIST_LOCKSWITCH, m_ListSwitch);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLockEdit, CDialog)
	//{{AFX_MSG_MAP(CLockEdit)
	ON_BN_CLICKED(IDC_LOCK_SWITCHDEL, OnLockSwitchdel)
	ON_BN_CLICKED(IDC_LOCK_SWITCHINS, OnLockSwitchins)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLockEdit message handlers

BOOL CLockEdit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(*m_pName);

	POSITION pos;
	CString* str;
	for( pos = m_pSwitch->GetHeadPosition(); pos != NULL; )
	{
        str = (CString*)m_pSwitch->GetNext( pos );
		m_ListSwitch.AddString(*str);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MyStrAdd(CComboBox &combo);
void MyStrDel(CComboBox &combo);

void MyStrAdd(CComboBox &combo) {
	CString str, stra, strd;
	combo.GetWindowText(str);
	if (str == "") return;
	if (str.GetAt(0) == '-') str = str.Right(str.GetLength()-1);
	stra = "+"+str;
	strd = "-"+str;
	int index = combo.FindString(-1,strd);
	if (index != CB_ERR) {
		combo.DeleteString(index);
		combo.InsertString(index,str);
	}
	else if (combo.FindString(-1,str) == CB_ERR && combo.FindString(-1,stra) == CB_ERR) {
		combo.AddString(stra);
	}
}

void MyStrDel(CComboBox &combo) {
	CString str;
	int index = combo.GetCurSel();
	if (index < 0) return;
	combo.GetLBText(index,str);
	if (str.GetAt(0) != '-') {
		combo.DeleteString(index);
		if (str.GetAt(0) != '+') {
			str = "-" + str;
			combo.InsertString(index,str);
		}
	}
}

void CLockEdit::OnLockSwitchdel() 
{
	MyStrDel(m_ListSwitch);
}

void CLockEdit::OnLockSwitchins() 
{
	MyStrAdd(m_ListSwitch);
}

void CLockEdit::OnOK() 
{
	m_pSwitch->Purge();

	int i,n;
	CString str;
	n = m_ListSwitch.GetCount();
	for (i=0; i<n; i++) {
		m_ListSwitch.GetLBText(i,str);
		m_pSwitch->AddTail(str);
	}
	CDialog::OnOK();
}
