//=======================================================
//====               BmpTrackCon.cpp                 ==== 
//=======================================================
//  파  일  명: BmpTrackCon.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: SVI 화면에 트랙을 표시한다.
//              
//=======================================================

#include "stdafx.h"
#include <strstrea.h>
#include <io.h>

#include "LSD.h"
#include "../include/Parser.h"
#include "BmpTrackCon.h"
#include "LSDView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileBuffer

class CFileBuffer {
	char *m_pFileBuffer;
	long m_lReadPtr;
	long m_lSize;
public:
	CFileBuffer() {}
	~CFileBuffer() {
		delete [] m_pFileBuffer;
	}
	BOOL Load( LPCTSTR pFileName );
	BOOL ReadString( CString &str );
};
BOOL CFileBuffer::Load( LPCTSTR pFileName ) {
	FILE *pFile = fopen( pFileName, "rb" );
	if (!pFile) return 1;
	m_lSize = _filelength( fileno( pFile ) );
	m_pFileBuffer = new char[m_lSize];
	fread( m_pFileBuffer, m_lSize, 1, pFile );
	fclose( pFile );
	m_lReadPtr = 0L;


	long crc = 0L;
	for (long i=0; i<m_lSize; i++) {
		char d = m_pFileBuffer[i];

	}
	return 0;
}

BOOL CFileBuffer::ReadString( CString &str ) {
	char buffer[2048];
	char c;
	short dest = 0;
	while ( m_lReadPtr < m_lSize ) {
		c = m_pFileBuffer[ m_lReadPtr++ ];
		if (dest && c == '\r') {
			if ( m_pFileBuffer[ m_lReadPtr ] == '\n')
				m_lReadPtr++;
			break;
		}
		else {
			buffer[dest++] = c;
		}
	}
	buffer[dest] = 0;
	if (dest) str = buffer;
	return dest;
}
//////////////////////////////////////////////////////////////////////

////////////////  extern ///////////
#define SFM_FILE_VERSION  "SFM EDIT FILE VER1.0"
extern CDC *_MyImageDC;
extern CDC *_MyImageDC1;
extern CDC *_MyImageDC2;

extern UINT  CELLSIZE_Y;
extern int   OFFSET_X;
extern int   OFFSET_Y;
extern int   REVERSE;
extern int	 STATIONUPDN;

/////////////////////////////////////////////////////////////////////////////
// CBmpTrackCon

CBmpTrackCon::CBmpTrackCon(CWnd * wnd, short nDotPerCell ) {
	m_pParentOwner = wnd;
	IsFileOpen = 0;
	m_iFreeRouteNo = -1;
	Modified=0 ;//1

	m_nDotPerCell = nDotPerCell;
	if( m_nDotPerCell == 10 )	{			
		m_fScaleX = 0.5f;
		m_fScaleY = 1.0f;
	}
	else if( m_nDotPerCell == 8 )	{
		m_fScaleX = 0.4f;
		m_fScaleY = 0.8f;
	}
}

CBmpTrackCon::~CBmpTrackCon()
{
	Destory();	
}

void CBmpTrackCon::SetFileName(CString filename)
{
	CString docname = filename;
	int l = docname.Find('.');
	sFileName = docname.Left(l);
	sFileName += ".SFM";	
}

/*
void CBmpTrackCon::SetConfig( BYTE config )
{
	TScrobj *obj;
	CBmpTrack *track = NULL;
	int count = mTrackList.GetSize();
	for (int i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );
		if( obj->m_cObjectType == (BYTE)OBJ_TRACK )	{
			track = (CBmpTrack*)obj;
			track->SetObjectConfig( config );
		}
	}
}

*/

void CBmpTrackCon::OnResetState()
{	
/*
	CBitmap bitmapAKASDG;
	CDC *memDC;
	bitmapAKASDG.LoadBitmap(IDB_BITMAPAKASDG);
	memDC->CreateCompatibleDC(m_pDC);
	bitmapAKASDG.CreateCompatibleBitmap( m_pDC, 0,0 );
	memDC->SelectObject( bitmapAKASDG );
	m_pDC->BitBlt( 0,0,800,600, memDC, 0,0,SRCCOPY);
*/
	
	if (_MyImageDC) {
		delete	_MyImageDC;
		_MyImageDC = NULL;
	}

	if (_MyImageDC1) {
		delete	_MyImageDC1;
		_MyImageDC1 = NULL;
	}

	if (_MyImageDC2) {
		delete	_MyImageDC2;
		_MyImageDC2 = NULL;
	}

	if (!_MyImageDC) {
		CBitmap Symbols;
		BOOL t = Symbols.LoadBitmap( IDB_SYMINFO7 );
		if (t) {
			CDC *pDC = m_pParentOwner->GetDC();
			_MyImageDC = new CDC;
			_MyImageDC->CreateCompatibleDC( pDC );
			_MyImageDC->SelectObject( &Symbols );
			Symbols.DeleteObject();
			m_pParentOwner->ReleaseDC( pDC );
		}
	}

	if (!_MyImageDC1) {
		CBitmap Symbols;
		BOOL t = Symbols.LoadBitmap( IDB_SYMINFO7 );
		if (t) {
			CDC *pDC = m_pParentOwner->GetDC();
			_MyImageDC1 = new CDC;
			if (_MyImageDC1->CreateCompatibleDC( pDC )) {
				_MyImageDC1->SelectObject( &Symbols );
			}
			else {
				delete _MyImageDC1;
				_MyImageDC1 = NULL;
			}
			Symbols.DeleteObject();
			m_pParentOwner->ReleaseDC( pDC );
		}
	}

	if (!_MyImageDC2) {
		CBitmap AKASDG;
		BOOL t = AKASDG.LoadBitmap( IDB_BITMAPAKASDG );
		if (t) {
			CDC *pDC = m_pParentOwner->GetDC();
			_MyImageDC2 = new CDC;
			if (_MyImageDC2->CreateCompatibleDC( pDC )) {
				_MyImageDC2->SelectObject( &AKASDG );
			}
			else {
				delete _MyImageDC2;
				_MyImageDC2 = NULL;
			}
			AKASDG.DeleteObject();
			m_pParentOwner->ReleaseDC( pDC );
		}
	}

}

void CBmpTrackCon::Initial()	// 모든 데이타 입력 완료 상태에서 호출 
{
	IsFileOpen = 0;
	Modified=0;//1;
	OnResetState();

	Save();
	FindJoinTrack();	
}

void CBmpTrackCon::AddObject(TScrobj *obj)
{	
	int size = mTrackList.GetSize();
	if ( size == 93 ) {
		size = 0;
	}
	mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)obj );
	Modified = 0 ;//1;
}

void CBmpTrackCon::Destory()
{
	TScrobj *obj;
	if(IsFileOpen)	{
		int count = mTrackList.GetSize();
		for (int i=0; i<count; i++ ) {
			obj = (TScrobj*)mTrackList.GetAt( i );			
			delete obj;
		}		
	}
	mTrackList.RemoveAll();	

	if (_MyImageDC) {
		delete	_MyImageDC;
		_MyImageDC = NULL;
	}
	if (_MyImageDC1) {
		delete	_MyImageDC1;
		_MyImageDC1 = NULL;
	}
	if (_MyImageDC2) {
		delete	_MyImageDC2;
		_MyImageDC2 = NULL;
	}
}

void CBmpTrackCon::FindJoinTrack()
{
	int no, node=0;
	CBmpTrack *t;
	TScrobj * obj;
	CBmpTrack *track = NULL;

	int count = mTrackList.GetSize();
	for (int i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );			
		if( obj->m_cObjectType == (BYTE)OBJ_TRACK )	{
			track = (CBmpTrack*)obj;
			for( no = 0 ; no<3 ; no++)	{
				t = TrackFind(track,no+1,node);
				if(t)
					track->SetJoinTrack(no,t,node);	
			}		
		}
	}		

	//  좌표 변화 
	count = mTrackList.GetSize();
	for (i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );
		obj->ConvertPos();
	}
}


int strcmpTrackName(const char *s1, const char *s2)
{
	CString t1, t2;

	t1 = s1;
	t2 = s2;
	int ispariod = t1.Find('.');
	if (ispariod >= 0) t1.SetAt(ispariod, 0x00);
	ispariod = t2.Find('.');
	if (ispariod >= 0) t2.SetAt(ispariod, 0x00);
	return ( strcmp(t1.GetBuffer(1), t2.GetBuffer(1)) );
}

CBmpTrack *CBmpTrackCon::TrackFind(CBmpTrack* T, int n, int& node) {  // Track T 의 n(C,N,R) 방향에 접속된 T를 찾음.
	CBmpTrack *track = NULL;
	TScrobj * obj;
	CPoint p;
	switch (n) {
	case 1: p = T->C; break;
	case 2: p = T->N; break;
	case 3: p = T->R; break;
	}
	node = 0;
	int count = mTrackList.GetSize();
	for (int i=0; i<count; i++ ) {
		obj = (TScrobj*)mTrackList.GetAt( i );			
		if( obj->m_cObjectType == (BYTE)OBJ_TRACK )	{
			track = (CBmpTrack*)obj;		
			if (T != track) {
				if (track->C == p) node = 1;
				else if (track->N == p) node = 2;
				else if (track->R == p) node = 3;
				if (node) {	
					if(!strcmpTrackName(T->mName,track->mName))
						return track;				
					node = 0;				
				}
			}
		}
	}
	return NULL;
}


void CBmpTrackCon::ChangeSwitchState(CBmpTrack *track)
{
	int node = (int)track->GetSwitchState();			
	CBmpTrack *t = NULL;
	for(int i = 1 ; i < 4 ; i++) {
		t = TrackFind(track,i,node);
		if(t)	{
		}	
	}
	Modified = 0;//1;
}

int CBmpTrackCon::GetCount()
{
	return mTrackList.GetSize();
}
int CBmpTrackCon::IsModified()
{
	return 0;//Modified;
}

void CBmpTrackCon::Open()
{
	if( Modified && GetCount() > 0 ) {
		if(AfxMessageBox("현재 정보를 저장하시겠습니까?",MB_ICONQUESTION | MB_YESNO ) == IDYES)
			Save();
	}
	char FileName[100];
	strcpy(FileName,"*.SFM");
	CFileDialog dlg( TRUE , "SFM", FileName, 
	                 OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
					  "SVI FILE (*.sfm)|*.sfm||", NULL );
	char sPath[100];
	GetCurrentDirectory(100, sPath);
	dlg.m_ofn.lpstrInitialDir=sPath;
	if (dlg.DoModal() != IDOK)  return ;
	strcpy(FileName,dlg.GetPathName());
	LoadFileInfo(FileName);
}

short CBmpTrackCon::LoadFileInfo(LPCTSTR pFileName) 
{
	CFileBuffer ar;
	if (ar.Load( pFileName )) return 1;

	CBmpTrack *pTrack;

	CString str;
	ar.ReadString( str );
	if( strcmp((char*)(LPCTSTR)str, SFM_FILE_VERSION) )	{
		AfxMessageBox((LPCTSTR)"버젼이 틀린 파일 입니다 확인하시고 다시 하여주십시요");
		return 1;
	}
	if(IsFileOpen)	{		// 파일을 OPEN 했으면 new 했기 때문에 
		int count = mTrackList.GetSize();
		for (int i=0; i<count; i++ ) {
			TScrobj *obj = (TScrobj*)mTrackList.GetAt( i );
			if (obj) delete obj;
		}
	}
	IsFileOpen = 1;	
	mTrackList.RemoveAll();
	CString		Name; 			
	CPoint		NamePos;
	UINT		Jeulyun;
	int			a , b, c, d, e, f, g, h, blockadetype;
	CPoint		p1, p2 ,p3;
	UINT nodetype;					
	pTrack = NULL;
	char * cap;

	while ( ar.ReadString(str) ) {
		CParser is( (char*)(LPCTSTR)str, "\t\r\n ,()" );
		cap = is.gettokenl();
		if(! strcmp(cap, "#[HEADER]"))	{
			ar.ReadString(str);
			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );			
			a = atoi(is1.gettokenl());			
			b = atoi(is1.gettokenl());
			c = atoi(is1.gettokenl());
			m_nDotPerCell = (UINT)a;
			REVERSE = b;
			STATIONUPDN = c;
			if( m_nDotPerCell == 10 )	{			
				m_fScaleX = 0.5f;
				m_fScaleY = 1.0f;
			}
			else if( m_nDotPerCell == 8 )	{
				m_fScaleX = 0.4f;
				m_fScaleY = 0.8f;
			}
			OnResetState();
		}
		else if(! strcmp(cap, "#[TRACK]"))
		{				
			if(pTrack)	{
				pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 
				mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );
			}
			pTrack = new CBmpTrack();			
			pTrack->m_bIsFileOpenPos=1;						// 1 : file load position  , 0 : des position
			pTrack->Init();		
			ar.ReadString(str);
			CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );
			char *s = is1.gettokenl();							
			pTrack->SetName( s );
			p1.x = atoi(is1.gettokenl());
			p1.y = atoi(is1.gettokenl());				
			p2.x = atoi(is1.gettokenl());
			p2.y = atoi(is1.gettokenl());				
			pTrack->SetNamePos(p1, p2);

			p1.x = atoi(is1.gettokenl());
			p1.y = atoi(is1.gettokenl());
			p2.x = atoi(is1.gettokenl());
			p2.y = atoi(is1.gettokenl());				
			p3.x = atoi(is1.gettokenl());
			p3.y = atoi(is1.gettokenl());				
			pTrack->SetCNRPnt(p1,p2,p3);

			Jeulyun = atoi(is1.gettokenl());			
			a  = atoi(is1.gettokenl());
			if(a) pTrack->SetDoubleSlip();				
		}
		else if(! strcmp(cap,"#[ETCOBJ]") ) {
				if(pTrack)	{
					pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 			
					AddObject(pTrack);							// 트랙 추가 
				}
				pTrack = NULL;
				ar.ReadString(str);				
				CParser is1( (char*)(LPCTSTR)str, "\t\r\n ,()" );
				Name = is1.gettokenl();							
				p1.x = atoi(is1.gettokenl());
				p1.y = atoi(is1.gettokenl());				
				a = atoi(is1.gettokenl());				// type
				b = atoi(is1.gettokenl());				// id				
				CString Name2 = is1.gettokenl();		//  인접역명
				c = atoi(is1.gettokenl());				// width
				CBmpEtcObj	*etc;
				etc = new CBmpEtcObj(p1,(char*)(LPCTSTR)Name,(char*)(LPCTSTR)Name2,a,b,c);
				AddObject(etc);							// 트랙 추가 
		}
		else if(! strncmp(cap,"[CELL]",6) ) {
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}											
			p1.x =  atoi(is.gettokenl());
			p1.y =  atoi(is.gettokenl());
			p2.x =  atoi(is.gettokenl());
			p2.y =  atoi(is.gettokenl());				
			nodetype=  atoi(is.gettokenl());
			pTrack->AddPos(p1,p2,nodetype);
		}
		else if(! strcmp(cap,"[SIGNAL]") ) {					
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}												
			Name = is.gettokenl();						
			p1.x = atoi(is.gettokenl());
			p1.y = atoi(is.gettokenl());
			p2.x = atoi(is.gettokenl());
			p2.y = atoi(is.gettokenl());								
			p3.x = atoi(is.gettokenl());
			p3.y = atoi(is.gettokenl());
			h = atoi(is.gettokenl());
			a = atoi(is.gettokenl());
			b = atoi(is.gettokenl());
			c = atoi(is.gettokenl());
			d = atoi(is.gettokenl());
			e = atoi(is.gettokenl());
			f = atoi(is.gettokenl());
			g = atoi(is.gettokenl());
				blockadetype = atoi(is.gettokenl());
			if(Name == "NONE") Name.Empty();				
			pTrack->SetSignal(p2,p1,(char*)(LPCTSTR)Name,p3,h,a , b, (UINT)c, d, e,(UINT)f,g, blockadetype);
		}
		else if(! strcmp(cap,"[SWITCH]") ) {					
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}								
			Name = is.gettokenl();							
			p1.x = atoi(is.gettokenl());
			p1.y = atoi(is.gettokenl());					
			p2.x = atoi(is.gettokenl());
			p2.y = atoi(is.gettokenl());				
			pTrack->SetSwitch(p1,(char*)(LPCTSTR)Name,p2);
		}
		else if(! strcmp(cap,"[BUTTON]") ){					
			if(! pTrack) {
				AfxMessageBox((LPCTSTR)"파일 형태가 틀립니다.  확인하고 다시 하여 주십시요");
				break;
			}								
			Name = is.gettokenl();							
			p1.x = atoi(is.gettokenl());
			p1.y = atoi(is.gettokenl());
			a = atoi(is.gettokenl());
			char *p = is.gettokenl();
			int nButtonType = atoi(p);
			pTrack->SetButton( p1, (char*)(LPCTSTR)Name, a, nButtonType );
		}			
	}	
	if(pTrack)	{
		pTrack->SetNoJeulyun(Jeulyun);				// 절연 정보 셋팅 
		mTrackList.SetAtGrow( mTrackList.GetSize(), (CObject*)pTrack );
	}
	FindJoinTrack( );	
	return 0;
}

void CBmpTrackCon::Save() 
{
	if( GetCount() <= 0 ) return;	
	CFile f;
	f.Open(sFileName,CFile::modeCreate|CFile::modeWrite);
	CArchive ar(&f,CArchive::store);		
	Serialize(ar);
	ar.Close();
	f.Close();

	CString sFileName1;
	sFileName1 = sFileName.Left(sFileName.GetLength()-4);
	sFileName1 = sFileName1 + ".SVI";
	ChangeSVIFile( sFileName1 , sFileName );

}

void CBmpTrackCon::Serialize(CArchive& ar)
{
	char ver[22];
	char en[2];
	strcpy(en,"\r\n");
	CTime    time = CTime::GetCurrentTime();
	TScrobj * obj;
	CString strHead;
	CString	strVersion;

	strHead.Format(";//        Create date : %04dY %02dM %02dD %02dH %02dM %02dS                      //\r\n",time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
	strVersion = ";LSD Version : " + GetProgramVersion()+ "\r\n";
	if(ar.IsStoring())		{
		strcpy(ver,SFM_FILE_VERSION);
		ar.WriteString(SFM_FILE_VERSION);
		ar.Write(en,2);

		ar.WriteString(";\r\n");
		ar.WriteString(";/////////////////////////////////////////////////////////////////////////\r\n");
		ar.WriteString(";//                                                                     //\r\n");
		ar.WriteString(";//    Screen File Format for BR    , @ 2004-1-28 by LSIS.Co.,Ltd..     //\r\n");
		ar.WriteString(";//                                                                     //\r\n");
		ar.WriteString(";//                    Do not modify this file                          //\r\n");
		ar.WriteString(";//                                                                     //\r\n");
		ar.WriteString(strHead);
		ar.WriteString(";//                                                                     //\r\n");
		ar.WriteString(";/////////////////////////////////////////////////////////////////////////\r\n");
		ar.WriteString(strVersion);
		CString dot;
		ar.WriteString("#[SIZE]");
		ar.Write(en,2);

		short x = 1, y = 1;
		CLSDView *pView = (CLSDView *)m_pParentOwner;
		if (pView) {
			x = pView->m_nSFMViewXSize;
			y = pView->m_nSFMViewYSize;
		};

		dot.Format("%d , %d", x, y );
		ar.WriteString(dot);
		ar.Write(en,2);

		ar.WriteString("#[HEADER]");
		ar.Write(en,2);

		dot.Format("%d , %d , %d ", m_nDotPerCell, REVERSE, STATIONUPDN );
		ar.WriteString(dot);
		ar.Write(en,2);

		int count = mTrackList.GetSize();
		for (int i=0; i<count; i++ ) {
			obj = (TScrobj*)mTrackList.GetAt( i );						
			obj->Serialize(ar);					
		}		
		ar.Write("[END]",5);
	}
	Modified = 0;	
}


void CBmpTrackCon::SetFreeRouteNo(int no)
{
	m_iFreeRouteNo = no;
}

CLSDDoc * CBmpTrackCon::GetDocument()
{
	CLSDView *pView = (CLSDView *)m_pParentOwner;
	if (pView) {
		return pView->GetDocument();
	}
	return NULL;
}

CString CBmpTrackCon::GetProgramVersion()
{
    HRSRC hRsrc = FindResource(NULL, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	CString strVersion;
	strVersion = "0.0.0.0";
    if (hRsrc != NULL)
    {
        HGLOBAL hGlobalMemory = LoadResource(NULL, hRsrc);
        if (hGlobalMemory != NULL)
        {
            void *pVersionResouece = LockResource(hGlobalMemory); 
            void *pVersion;
            UINT uLength;
            // 아래줄에 041204B0는 리소스 파일(*.rc)에서 가져옴.
            // 프로젝트 리소스 파일을 참고하세요(어느부분 참고인지는 밑에 나와있음)
            if( VerQueryValue(pVersionResouece, "StringFileInfo\\040904b0\\FileVersion", &pVersion, &uLength) != 0 )
            {
               // 1, 0, 0, 1로 되어 있는 부분에서 숫자 부분만 가져옴.
                int anVersion[4] = {0,};
                char* pcTemp = strtok( (char *)pVersion, "," );
                for(int inxVersion=0; inxVersion<4; inxVersion++)
                {
                    if(pcTemp  != NULL)
                    {
                        anVersion[inxVersion] = atoi(pcTemp );
                        if(inxVersion != 3)
                        {
                            pcTemp = strtok(NULL, ",");
                        }
                    }
                }
                strVersion.Format("%d.%d.%d.%d", anVersion[0], anVersion[1], anVersion[2], anVersion[3]);
                //AfxMessageBox(strVersion);
            }
            FreeResource(hGlobalMemory);
        }
    }
	return strVersion;
}

void CBmpTrackCon::ChangeSVIFile( CString strFileName , CString strFileName1)
{
	CFile m_readFile;
	CFile m_saveFile;
	if (m_readFile.Open( strFileName1, CFile::modeRead , NULL) == FALSE) 
	{
		AfxMessageBox("readFile open fail");
		return;
	}
	if (m_saveFile.Open( strFileName, CFile::modeCreate | CFile::modeWrite, NULL) == FALSE)
	{
		AfxMessageBox("saveFile open fail");
		return;
	}

	unsigned char buffer;
	BOOL beof = TRUE; 
	BOOL bCommentCK = FALSE;

	while (beof){
		if ( m_readFile.Read(&buffer,1) == 0 ) beof = FALSE;
		else {
			if ( buffer == 59 ) bCommentCK = TRUE;
			if ( buffer != 13 && buffer != 10 ) {
				if ( bCommentCK == FALSE ) buffer = buffer | 0x80;
			} else {
				bCommentCK = FALSE;
			}
			m_saveFile.Write(&buffer,1);
		}
	}
}
