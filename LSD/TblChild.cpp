// ChildFrm.cpp : implementation of the CTableChildFrm class
//

#include "stdafx.h"
#include "LSD.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrm

IMPLEMENT_DYNCREATE(CTableChildFrm, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTableChildFrm, CMDIChildWnd)
	//{{AFX_MSG_MAP(CTableChildFrm)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrm construction/destruction

CTableChildFrm::CTableChildFrm()
{
	// TODO: add member initialization code here
	
}

CTableChildFrm::~CTableChildFrm()
{
}

BOOL CTableChildFrm::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CMDIChildWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrm diagnostics

#ifdef _DEBUG
void CTableChildFrm::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CTableChildFrm::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTableChildFrm message handlers
