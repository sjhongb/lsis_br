//=======================================================
//====               Blip.h                          ==== 
//=======================================================
//  파  일  명: Blip.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.19
//  버      전: 2.01
//  설      명: 트랙 신호기등 좌표값을 가지는 객체의 속성
//              
//=======================================================


#ifndef _BLIP_H
#define _BLIP_H

extern int GridSize, GridSizeH;

class CLSDView;
class CGraphObject;

class CBlip : public CPoint {
protected:
	BOOL m_bVisible, m_bActive, m_bSelect;
public:
	CString       m_sBlipName;
	CGraphObject *m_pParent;
	int           m_iBlipSize;
public:
	virtual CBlip* HitTest(CPoint point, CLSDView* pView, BOOL bSelected);
	enum TrackerState { normal, selected, active };
	virtual CRect GetRect() {
		return CRect(x-m_iBlipSize,y-m_iBlipSize,x+m_iBlipSize,y+m_iBlipSize);
	}
	virtual void Offset(const CPoint point, CLSDView* pView = NULL);
	virtual CPoint MoveTo(CPoint point);
	void SetName(char *n) { m_sBlipName = n; }
	int MoveToX(int nx);

	CBlip(char *n) {
		m_sBlipName = n;
		x = 0; y = 0;
		m_bVisible = TRUE;
		m_bActive = FALSE;
		m_bSelect = FALSE;
		m_pParent = NULL;
		m_iBlipSize = 10;
	}
	CBlip() {
		m_sBlipName = "";
		x = 0; y = 0;
		m_bVisible = TRUE;
		m_bActive = FALSE;
		m_bSelect = FALSE;
		m_pParent = NULL;
		m_iBlipSize = 10;
	}
	CBlip(CPoint &point, char *n = "") {
		m_sBlipName = n;
		x = point.x; 
		y = point.y;
		m_bVisible = TRUE;
		m_bActive = FALSE;
		m_bSelect = FALSE;
		m_pParent = NULL;
		m_iBlipSize = 10;
	}
	virtual void Show(CDC* pDC = NULL) {
		if (m_bVisible && pDC) Draw(pDC);
	}
	virtual void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0)) {}
	virtual void DrawTracker(CDC *pDC, TrackerState state);
	virtual const char *NameOf() { return "BLIP"; }
	virtual void Visible(BOOL f) { m_bVisible = f; }
	virtual void Active(BOOL f) { m_bActive = f; }
	virtual void Select(BOOL t = FALSE) { m_bSelect = t; }
	virtual BOOL isValid() { return m_bVisible; }
	virtual void PropertyChange(CWnd *wnd) {};
	virtual void Read(CArchive &ar) {
		ar >> x >> y >> m_sBlipName;
	}
	virtual void Write(CArchive &ar) {
		ar << x << y << m_sBlipName;
	}
	void GridPoint(CPoint &point) {
		point.x = ((point.x + GridSizeH)/GridSize)*GridSize;
		point.y = ((point.y + GridSizeH)/GridSize)*GridSize;
	}
	friend CArchive& operator >> (CArchive &ar, CBlip &obj) {
		obj.Read(ar);
		return ar; 
	}
	friend CArchive& operator << (CArchive &ar, CBlip &obj) { 
		obj.Write(ar);
		return ar; 
	}
};

#endif