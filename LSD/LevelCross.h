//=======================================================
//==              LevelCross.h
//=======================================================
//	파 일 명 :  LevelCross.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  1.01
//	설    명 :  건널목 관련 처리를 담당
//
//=======================================================

#ifndef _LevelCross_H
#define _LevelCross_H

#include "GraphObj.h"
#include "../include/StrList.h"

#define B_NAME_B 0x1
#define B_NAME_S 0x2
#define B_CONT   0x4
#define B_NOCALLON  0x8

class StationDB;

class CLevelCross : public CGraphObject {
	int m_nShape;
	CBlip nL;
	BOOL    m_bUnique;
protected:
	DECLARE_SERIAL(CLevelCross);
public:
	int m_nType, m_nLR, m_nLine;
	CMyStrList	m_LCTrack;
public:
	void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));
	CLevelCross( CString strName = "" , int cx = 0, int cy = 0);
	virtual void Read(CArchive &ar);
	virtual void Write(CArchive &ar);
	virtual const char *NameOf() { return "LevelCross"; }
	virtual BOOL isValid(){ return Name != ""; }
	BOOL isNoGrid(CBlip *pBlip);
	void Init();
friend StationDB;
};

#endif