//=======================================================
//==              ObjSetTextDlg.cpp
//=======================================================
//	파 일 명 :  ObjSetTextDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "ObjSetTextDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjSetTextDlg dialog


CObjSetTextDlg::CObjSetTextDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjSetTextDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjSetTextDlg)
	m_strText = _T("");
	//}}AFX_DATA_INIT
}


void CObjSetTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjSetTextDlg)
	DDX_Text(pDX, IDC_EDIT1, m_strText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CObjSetTextDlg, CDialog)
	//{{AFX_MSG_MAP(CObjSetTextDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjSetTextDlg message handlers

void CObjSetTextDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_strText = "TEXT." + m_strText;
	m_strText.Replace(" ","_");
	UpdateData(FALSE);
	CDialog::OnOK();
}

BOOL CObjSetTextDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	int  period = m_strText.Find('.');
	m_strText = m_strText.Mid(period+1,m_strText.GetLength());
	m_strText.Replace("_"," ");
	UpdateData(FALSE);
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
