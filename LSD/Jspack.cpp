//=======================================================
//==              Jspack.cpp
//=======================================================
//	파 일 명 :  Jspack.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================


#include "stdafx.h"

#define DXF_TEMPLETE_A3 ".\\DWGBASE12.DXF"
#define DXF_TEMPLETE_A2 ".\\DWGBASE12.DXF"

//#define DEBUG
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <io.h>

//#include <dir.h>
//#include "mydir.h"
//#include "dxfproj.h"
//#include "dxfinfo.h"
//#include "jsproj.h"
#include "jsdxf.h"
#include "jsutil.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MYDEB
#ifdef MYDEB
FILE *debfp = NULL;
int debstop = 0;
#endif


//CBlock *CBlock::start = NULL;
CBlock *INSroot;
DXFile *_DXF;

CDxfCode::CDxfCode() {
	m_pNext = NULL;
	m_nCode = 0;
	m_szData[0] = 0;
}

CDxfCode::CDxfCode( int nCode, char *pBf ) {
	m_pNext = NULL;
	m_nCode = nCode;
	strcpy( m_szData, pBf );
}

CDxfCode::~CDxfCode() {
	if ( m_pNext ) delete m_pNext;
	m_pNext = NULL;
}

CDXFObject::CDXFObject() {
	memset(this,0,sizeof(CDXFObject));
	m_pAnyCode = NULL;
	halign2 = 0;
}

CDXFObject::~CDXFObject()
{
	TRACE("함수 : CDxfCode::CDxfCode() {\n");
	if ( m_pAnyCode ) delete m_pAnyCode;
}

void CDXFObject::AddAnyCode( int nCode, char *pBf ) {
	CDxfCode *pCode = new CDxfCode( nCode, pBf );
	if ( m_pAnyCode == NULL ) {
		m_pAnyCode = pCode;
	}
	else {
		CDxfCode *pLast = m_pAnyCode;
		while ( pLast->m_pNext != NULL ) {
			pLast = pLast->m_pNext;
		}
		pLast->m_pNext = pCode;
	}
}

void CDXFObject::unpack( DXFile *pDxf ) {
	CDxfCode *pCode = m_pAnyCode;
	while ( pCode ) {
		pDxf->out( pCode->m_nCode, pCode->m_szData );
		pCode = pCode->m_pNext;
	}
}

ARC::ARC() {
   memset(this,0,sizeof(ARC));
}
ATEXT::ATEXT() {
   memset(this,0,sizeof(ATEXT));
}

LINE::LINE() {
   memset(this,0,sizeof(LINE));
   m_pLineType = NULL;
}

CBlock::CBlock() {
   memset(this,0,sizeof(CBlock));
   m_dzdir = 0.0;
}

CAttribute::CAttribute() {
	memset(this,0,sizeof(CAttribute));
	m_pNext = NULL;
	halign = 0.0;     // 72       4
	halign74 = 0.0;     // 72       4
}

CAttribute::~CAttribute() {
}

ENTITIES::ENTITIES() {
   page = ni = 0;
   ihead = ilast = NULL;
   nl = 0;
   lhead = llast = NULL;
   na = 0;
   ahead = alast = NULL;
   nt = 0;
   thead = tlast = NULL;
}

DXFile::DXFile(char *fn) {
    _DXF = this;
    line = 0;
    dxfcode = -1;

	m_lReadPtr = 0;

	m_lHeaderSize = 0L;
	m_lObjectSize = 0L;
	m_pHeaderBuffer = NULL;
	m_pObjectBuffer = NULL;
	m_pFileBuffer = NULL;

	m_nReadHandle = 0;
	m_nHandle = 0x5000;

	m_pFile = NULL;
    if (fn) {
		strcpy(fname,fn);
		if (!*fn) {
			m_pFile = fopen( DXF_TEMPLETE_A3, "rb" );
			if (m_pFile) {
				ReadToBuffer();
				FindEntityLocation();
			}
		}
	}
	else
		strcpy(fname,"");

	m_pOutFile = NULL;
}

DXFile::~DXFile() {
//	if (m_pHeaderBuffer) delete [] m_pHeaderBuffer;
//	if (m_pObjectBuffer) delete [] m_pObjectBuffer;
	if (m_pFileBuffer) delete [] m_pFileBuffer;

	if (m_pFile) fclose( m_pFile );
}

char *DXFile::extfname(char *ext) {
   static char fn[80];
   strcpy(fn,fname);
   strcat(fn,".");
   strcat(fn,ext);
   return fn;
}

#define FORMAT 1
#define SEQUENCE 2
#define NODXF 3
char *estr[] = { "",
		 "Bad format",
		 "Bad sequence",
		 "Not DXF file",
		 "" };

void DXFile::err(int eno) {
   printf("\n%4d : %s %s",line,bf,estr[eno]);
   dxfcode = 0;
//   exit(eno);

#ifdef MYDEB
   if ( debfp ) fclose( debfp );
   debfp = NULL;
   debstop = 1;
#endif
}

char *DXFile::readbf() {
	char *p,*q;
	line++;
	p = CopyOneLine();
//	strupr(bf);
    if (p) {
       q = p+strlen(p)-1;
       *q-- = 0;     // CR(\n) remove
       *q-- = 0;     // CR(\r) remove
       if (*p != ' ') while(q>p && *q==' ') *q--=0;
    }
    return p;
}

int DXFile::read0() {
    if (!dxfcode) return 1;
    line++;
	CopyOneLine();
//	strupr(bf);
	if (!strcmp(bf,"100\r\n")) {
		readbf();
		line++;
		CopyOneLine();
		strupr(bf);
	}
// 2002.3.25.IM 추가
	if (!strcmp(bf,"  6\r\n")) {
		readbf();
		line++;
		CopyOneLine();
		strupr(bf);
	}
//
	if (!strncmp(bf,"  0",3)) readbf();
	else {
		err(FORMAT);
		readbf();
		printf( "\n[%s]", bf );
		readbf();
		printf( "\n[%s]", bf );
		readbf();
		printf( "\n[%s]", bf );
		readbf();
		printf( "\n[%s]", bf );
	}
    return 1;
}

void DXFile::readcode() {
    line++;
    CopyOneLine();
	if (!strcmp(bf,"102\r\n")) {
		dxfcode = 102;
		readbf();
		readbf();
		readbf();
		readbf();
		readbf();
		line++;
		CopyOneLine();
	}
	if (!strcmp(bf,"100\r\n")) {
		dxfcode = 100;
		readbf();
		line++;
		CopyOneLine();
	}
//	strupr(bf);
		dxfcode = atoi(bf);
		if (readbf() == NULL) dxfcode = 0;
}

//void DXFile::ReadHandle() {
//	dxfcode = 5;
//	char buf[10] = "0x";
//	readbf();
//	char *string;
//	m_nReadHandle = strtol(bf,&string,16);
//	line++;
//	fgets(bf,99,f);
//}

void icount() {
   CBlock *x; int n=0;
   x = INSroot;
   while(x) {
      n++;
      x = x->m_pNext;
   }
	printf("[%d]",n);
}

int DXFile::ReadFromDXF() {
	int status = 0;

#ifdef MYDEB
    if ( debfp ) fclose( debfp );
	if ( debstop == 0 ) {
		debfp = fopen( "LOCDEB.TXT", "w+b" );
		fprintf( debfp, "%s\n", fname );
	}
#endif

	if (m_pFile = fopen(extfname("DXF"),"rb"), m_pFile ) {

		ReadToBuffer();

	int nLast = 0;
		if (CopyAndFindEntityLocation()) {
				readcode();
				readcode();
//		if (dxfcode==2 && !strcmp(bf,"ENTITIES")) {
			while (read0()) {
				nLast = 0;
				int x = 0;
				if (line > 0xC00) {
					line = line + x;
				}
				if (!strcmp(bf,"LINE")) 
					rLINE();
				else if (!strcmp(bf,"POLYLINE")) 
					rOTHER();
				else if (!strcmp(bf,"LWPOLYLINE")) 
					rOTHER();
				else if (!strcmp(bf,"ATTDEF")) 
					rOTHER();
				else if (!strcmp(bf,"CIRCLE")) 
					rOTHER();
/*
				else if (!strcmp(bf,"ELLIPSE")) 
					rOTHER();
				else if (!strcmp(bf,"HATCH")) 
					rOTHER();
				else if (!strcmp(bf,"VERTEX")) 
					rOTHER();
*/
				else if (!strcmp(bf,"INSERT")) {
					nLast = 1;
					while (!strcmp(bf,"INSERT")) {
						rINSERT();
					}
//					printf( "\nDXFCODE=%d", dxfcode );
					if (dxfcode) readcode();
//					printf( "\nBF=%s", bf );
					if (!strcmp(bf,"SECTION")) {
						status = 1;
						break;
					}
				}
				else if (!strcmp(bf,"ARC")) rARC();
				else if (!strcmp(bf,"TEXT")) rATEXT();
				else if (!strcmp(bf,"POINT")) rOTHER();
				else if (!strcmp(bf,"VIEWPORT"))
					rOTHER();
				else if (!strcmp(bf,"ENDSEC")) {

//					long cr = ftell( f );
//					m_lObjectSize = m_lFileSize - cr;
//					m_pObjectBuffer = m_pFileBuffer + cr;
					status = 1;
					break;
				}
//				else if (!strcmp(bf,"EOF")) {
//					break;
//				}
				else {
					err(FORMAT);
					break;
				}
			}
		} else err(NODXF);
		fclose(m_pFile);
		m_pFile = NULL;
	}
	INSroot = ihead;

#ifdef MYDEB
   if ( debfp ) fclose( debfp );
   debfp = NULL;
#endif
	return status;
}

template <class T>
void writeblock(T *head, FILE *wf) {
	char *s;
	int cnt = 0;
	while (head) {
		cnt++;
#ifdef DEBUG
	 printf("%3d.%p",++n,head);
#endif
		s = head->getlayer();
		if (strcmp(s,"0") && strcmp(s,"REF")) {

	 printf("..ERROR PCK..");
//	 exit(0);
//	 myexit("..ERROR PCK..");

	 return;
		}
		fwrite(head,sizeof(T),1,wf);
		head = head->m_pNext;
	}
    cnt = cnt;
}

void CBlock::writeblocki(CBlock *head,FILE *wf) {
   while (head) {
      fwrite(head,sizeof(CBlock),1,wf);
      if (head->att) writeblock(head->att,wf);
      head = head->m_pNext;
   }
}

/*
void DXFile::packwrite() {
   FILE *wf;
   if (wf=fopen(extfname("PCK"),"wb"),wf) {
#ifdef DEBUG
	  printf("\nLINE : %d  ARC : %d  CBlock : %d",nl,na,ni);
#endif
	  fwrite(this,sizeof(ENTITIES),1,wf);
#ifdef DEBUG
	  printf("\nLINE");
#endif
	  if (lhead) writeblock((LINE*)lhead,wf);
#ifdef DEBUG
	  printf("\nARC");
#endif
	  if (ahead) writeblock((ARC*)ahead,wf);
#ifdef DEBUG
	  printf("\nTEXT");
#endif
	  if (thead) writeblock((ATEXT*)thead,wf);
#ifdef DEBUG
	  printf("\nINSERT");
#endif
	  if (ihead) ((CBlock*)ihead)->writeblocki((CBlock*)ihead,wf);
	  fprintf(wf,"DXF PACK FILE");
	  fclose(wf);
   }
}
*/

void DXFile::freeclass() {
	LINE *ll,*l = (LINE*)lhead;
	while (l) {
		ll = l;
		l = (LINE*)l->GetNext();
		delete ll;
	}
	ARC *aa,*a = (ARC*)ahead;
	while (a) {
		aa = a;
		a = (ARC*)a->GetNext();
		delete aa;
	}
	ATEXT *xx,*x = (ATEXT*)thead;
	while (x) {
		xx = x;
		x = (ATEXT*)x->GetNext();
		delete xx;
	}
	CBlock *ii,*i = (CBlock*)ihead;
	CAttribute *tt,*t;
	while (i) {
		ii = i;
		t = i->att;
		while (t) {
			tt = t;
			t = t->m_pNext;
			delete tt;
		}
		i = i->m_pNext;
		delete ii;
	}
	lhead = llast = NULL;
	ahead = alast = NULL;
	thead = tlast = NULL;
	ihead = ilast = NULL;
}

void *DXFile::rINSERT() {
#ifdef MYDEB
	if ( debfp ) fprintf( debfp, "INSERT {\n" );
#endif
   CBlock *ENT = new CBlock;
   ENT->pack(this,dxfcode,bf);
   if (!strcmp(ENT->block,"CROSS")||!strcmp(ENT->block,"ERRBOX")||!strcmp(ENT->block,"CADSHEET")) {
      delete ENT;
      ENT = NULL;
   }
   else {
      ENT->insert(ihead,ilast,ENT);
      ni++;
   }
   return ENT;
}

void *DXFile::rLINE() {
#ifdef MYDEB
	if ( debfp ) fprintf( debfp, "LINE {\n" );
#endif
   LINE *ENT = new LINE;
   ENT->pack(this,dxfcode,bf);
   insert(lhead,(LINE*)llast,ENT);
   nl++;
   return ENT;
}

void DXFile::rOTHER() {
   while(readcode(),dxfcode) {
   }
}

void *DXFile::rARC() {
#ifdef MYDEB
	if ( debfp ) fprintf( debfp, "ARC {\n" );
#endif
   ARC *ENT = new ARC;
   ENT->pack(this,dxfcode,bf);
   insert(ahead,(ARC*)alast,ENT);
   na++;
   return ENT;
}

void *DXFile::rATEXT() {
#ifdef MYDEB
	if ( debfp ) fprintf( debfp, "TEXT {\n" );
#endif
   ATEXT *ENT = new ATEXT;
   strcpy( ENT->m_szName, "TEXT" );
   ENT->pack(this,dxfcode,bf);
   insert(thead,(ATEXT*)tlast,ENT);
   nt++;
   return ENT;
}

void CDXFObject::pack(DXFile*d,int &code,char *bf) {
	char *pp;
	while(d->readcode(),code) {
		switch (code) {
		case 1  : 
//			strcpy(value,bf); 
			strncpy(value,bf,255); 
			break;
		case 5  : strcpy(m_cHandle,bf); break;
		case 7  : strcpy(style,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 10 : sp.x = strtod(bf,&pp); break;
		case 20 : sp.y = strtod(bf,&pp); break;
		case 11 : ep.x = strtod(bf,&pp); break;
		case 21 : ep.y = strtod(bf,&pp); break;
		case 40 : r = strtod(bf,&pp); break;
		case 72 : halign = atoi(bf); break;
// ARC
		case 50 : sa = strtod(bf,&pp); break;
		case 51 : ea = strtod(bf,&pp); break;
		default :
			AddAnyCode( code, bf );
			break;
		}
	}
}

/*
void ATEXT::pack(DXFile*d,int &code,char *bf) {
	while(d->readcode(),code) {
		switch (code) {
		case 1  : strcpy(value,bf); break;
		case 5  : strcpy(m_cHandle,bf); break;
		case 7  : strcpy(style,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 10 : sp.x = atoi(bf); break;
		case 20 : sp.y = atoi(bf); break;
		case 11 : ep.x = atoi(bf); break;
		case 21 : ep.y = atoi(bf); break;
		case 40 : h = atoi(bf); break;
		case 72 : hal = atoi(bf); break;
		}
	}
}

void ARC::pack(DXFile*d,int &code,char *bf) {
	while(d->readcode(),code) {
		switch (code) {
		case 5  : strcpy(m_cHandle,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 10 : p.x = atoi(bf); break;
		case 20 : p.y = atoi(bf); break;
		case 40 : r = atoi(bf); break;
		case 50 : sa = atoi(bf); break;
		case 51 : ea = atoi(bf); break;
		}
	}
}

void LINE::pack(DXFile *d, int &code, char *bf) {
	while(d->readcode(),code) {
		switch (code) {
		case 5  : strcpy(m_cHandle,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 10 : sp.x = atoi(bf); break;
		case 20 : sp.y = atoi(bf); break;
		case 11 : ep.x = atoi(bf); break;
		case 21 : ep.y = atoi(bf); break;
		}
	}
}
*/
void CAttribute::pack(DXFile *d, int &code, char *bf) {
	char *pp;
	while(d->readcode(),code) {
		switch (code) {
		case 5  : strcpy(m_cHandle,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 62 : color = atoi(bf); break;
		case 10 : p.x = strtod(bf,&pp); break;
		case 20 : p.y = strtod(bf,&pp); break;
		case 40 : texth = strtod(bf,&pp); break;
		case 1  :
			strcpy(value,bf); break;
		case 2  : strcpy(tag,bf); break;
		case 70 : attr = atoi(bf); break;
		case 7  : strcpy(style,bf); break;
		case 72 : halign = strtod(bf,&pp); break;
		case 74 : halign74 = strtod(bf,&pp); break;
		case 11 : ap.x = strtod(bf,&pp); break;
		case 21 : ap.y = strtod(bf,&pp); break;
		default :
			AddAnyCode( code, bf );
			break;
		}
	}
	if (!*style) strcpy(style,"ROMANS");
	if (!strcmp(bf,"ATTRIB")) {
		m_pNext = new CAttribute;
		m_pNext->pack(d,code,bf);
	}
	else m_pNext = NULL;
}

void CBlock::pack(DXFile *d, int &code, char *bf) {
	int	_ISSHEET = 0;
	char *pp;
	while(d->readcode(),code) {
		switch (code) {
		case 5  : strcpy(m_cHandle,bf); break;
		case 8  : strcpy(layer,bf); break;
		case 66 : hasatt = atoi(bf); break;
		case 2  :
			strcpy(block,bf);
			if (!strcmp(bf,"CADSHEET")) {
			_ISSHEET = 1;
			}
		break;
		case 10 : p.x = strtod(bf,&pp); break;
		case 20 : p.y = strtod(bf,&pp); break;
		case 41 : m_dzdir = strtod(bf,&pp); break;
		default :
			AddAnyCode( code, bf );
			break;
		}
	}
	if (hasatt) {
		if (!strcmp(bf,"ATTRIB")) {
			att = new CAttribute;
			att->pack(d,code,bf);
		}
		if (!strcmp(bf,"SEQEND")) {
			d->readcode();
			strcpy(m_cAHandle,bf);
		}
		else d->err(SEQUENCE);
	}

	CAttribute *a, *pLast = NULL;
	CAttribute *pAttrRack = NULL;
	CAttribute *pAttrVal2 = NULL;
	a = att;
	while (a) {
		if (!strcmp(a->tag, "POS")) {
//			if ( !strcmp( block, "ENPRN" ) || !strcmp( block, "ENPRR" ) ) {
			if ( !strncmp( block, "ENPR", 4 ) ) {
				strcpy(a->tag,"NVAL");
				if ( !strcmp( block, "ENPRF" ) ) {
					a->ap.y += 1;
					pAttrVal2 = new CAttribute;
					strcpy(pAttrVal2->tag, "VAL2" );
					strcpy(pAttrVal2->value, "");
					point p = a->ap;
					p.y += 9;
					pAttrVal2->p = p;
					pAttrVal2->ap = p;
					pAttrVal2->attr = a->attr;
					pAttrVal2->halign = a->halign;
					pAttrVal2->color = a->color;
					strcpy(pAttrVal2->layer, a->layer);
					strcpy(pAttrVal2->style, a->style);
					pAttrVal2->texth = a->texth;
				}
			}
			else if ( !strncmp( block, "EQ", 2 ) ) {
				strcpy(a->tag,"NVAL");
				a->p = p;
				a->p.y -= 2.0;
				a->ap = a->p;
				a->halign = 4;	// align center
			}
		}
		if (!strcmp(a->tag, "VAL")) {
			if ( !strncmp( block, "EQ", 2 ) ) {
				strcpy(a->tag,"NPOS");
			}
// ENPRN ENPRR ENPRF ENPRC...
			else if ( !strncmp( block, "ENPR", 4 ) ) {
				strcpy(a->tag,"NPOS");
				pAttrRack = new CAttribute;
				strcpy(pAttrRack->tag, "RACK" );
				strcpy(pAttrRack->value, "");
				pAttrRack->p = a->ap;
				a->ap.y -= 1.8;
				pAttrRack->ap = pAttrRack->p;
				pAttrRack->attr = a->attr;
				pAttrRack->halign = a->halign;
				pAttrRack->color = a->color;
				strcpy(pAttrRack->layer, a->layer);
				strcpy(pAttrRack->style, a->style);
				pAttrRack->texth = a->texth;
			}
		}

		pLast = a;
		a = a->m_pNext;
	}
	if ( pLast ) {
		pLast->m_pNext = pAttrRack;
		if (pAttrRack) pAttrRack->m_pNext = pAttrVal2;
		else if (pAttrVal2) delete pAttrVal2;
	}
	else {
		if (pAttrRack) delete pAttrRack;
		if (pAttrVal2) delete pAttrVal2;
	}
}

void CBlock::AttChange(char *tag, char *s) {
	CAttribute *a, *pLast;
	a = att;
	while (a) {
		if (!strcmp(a->tag,tag)) {
			strcpy(a->value,s);
			return;
		}
		pLast = a;
		a = a->m_pNext;
	}
}

void CBlock::AttColorChange(char *tag, short color) {
	CAttribute *a;
	a = att;
	while (a) {
		if (!strcmp(a->tag,tag)) {
			a->color = (char)color;
			break;
		}
		a = a->m_pNext;
	}
}

void CBlock::RefMake(char *blname, int dy, char *ext) {
	CBlock *i, *r=NULL;
	CAttribute *a;
	double x,y,dx;
	char nref[13];

	x = p.x;
	y = p.y + dy;
	i = INSroot;
	while (i) {
		if (!strcmp(blname,i->block) && i->p.y==y) {
			if (r) {
				if ((dx>abs((int)(i->p.x - x)))) {
					r = i;
					dx = abs((int)(i->p.x - x));
				}
			}
			else {
				r = i;
				dx = abs((int)(i->p.x - x));
				if (dx>100) r = NULL;
			}
		}
		i = i->m_pNext;
	}
	if (r) {
		a = att;
		*nref = 0;
		while(a) {
			if (!strcmp(a->gettag(),"REF")) {
				strcpy(nref,a->getvalue());
				break;
			}
			a = a->m_pNext;
		}
		if (ext) strcat(nref,ext);
		r->AttChange("REF",nref);
	}
}

template <class T>
void readblock(T*&h,int n,FILE *rf) {
   T *l = NULL, *e;
   int i;
   for (i=0; i<n; i++) {
      e = new T;
      fread(e,sizeof(T),1,rf);
#ifdef DEBUG
      printf("[%3d.%s",i+1,e->getlayer());
#endif
      if (!h) h = e;
      else l->m_pNext = e;
      l = e;
   };
}

void CBlock::readblocki(CBlock *&h,CBlock *&l,int n,FILE *rf) {
   CBlock *e;
   CAttribute *a;
   int i;
   l = NULL;
   for (i=0; i<n; i++) {
      e = new CBlock;
      fread(e,sizeof(CBlock),1,rf);
#ifdef DEBUG
      printf("I.%s",e->getlayer());
#endif
      if (e->att) {
	 a = e->att = new CAttribute;
	 fread(a,sizeof(CAttribute),1,rf);
#ifdef DEBUG
      printf("A.%s",a->getlayer());
#endif
	 while(a->m_pNext) {
	    a = a->m_pNext = new CAttribute;
	    fread(a,sizeof(CAttribute),1,rf);
#ifdef DEBUG
      printf("A.%s",a->getlayer());
#endif
	 }
      }
      if (!h) h = e;
      else l->m_pNext = e;
      l = e;
   };
}

template <class T>
void writedxfent(T *e, DXFile *d) {
   while(e) {
      e->unpack(d);
      e = (T*)e->m_pNext;
   }
}

void DXFile::WriteToDXF() {
//   struct ffblk ffblk;
//   int  none;
   char newname[80];
   char bakname[80];
   strcpy(newname,extfname("DXF"));
   strcpy(bakname,extfname("DKF"));

//   none = findfirst(bakname,&ffblk,0);
//   if (!none) remove(bakname);
//   rename(newname,bakname);

   m_pOutFile = fopen(newname,"wb");

   if (m_pHeaderBuffer)
	   fwrite( m_pHeaderBuffer, 1, m_lHeaderSize, m_pOutFile );
/*
  out(0,"BLOCK");
  out(8,"0");
  out(2,"CROSS");
  out(70,"     0");
  out(10,"0");
  out(20,"0");
  out(30,"0");
  out(3,"CROSS");
  out(1,"");
  out(0,"LINE");
  out(5,"F01F");
  out(8,"GUIDE");
  out(10,"-2");
  out(20,"2");
  out(30,"0");
  out(11,"2");
  out(21,"-2");
  out(31,"0");
  out(0,"LINE");
  out(5,"F021");
  out(8,"GUIDE");
  out(10,"2");
  out(20,"2");
  out(30,"0");
  out(11,"-2");
  out(21,"-2");
  out(31,"0");
  out(0,"ENDBLK");
  out(5,"F023");
  out(8,"0");
  out(0,"ENDSEC");
*/
   DXFile *t = this;
   out(0,"SECTION");
   out(2,"ENTITIES");
   writedxfent((CBlock*)ihead,t);
   writedxfent((LINE*)lhead,t);
   writedxfent((ARC*)ahead,t);
   writedxfent((ATEXT*)thead,t);
   out(0,"ENDSEC");

   if (m_pObjectBuffer)
	   fwrite( m_pObjectBuffer, 1, m_lObjectSize, m_pOutFile );

   else out(0,"EOF");
   fclose( m_pOutFile );
}

void DXFile::fMakeOn(char *fn, char *szType ) {

	strcpy( m_szSheetType, szType );
	strcpy( m_szSheetNo, fn );
	printf( "\nCreate file : %s", fn );

//   char fndxf[80];
//2002.3.17.ADD TO PROJECT   PROJ->AddToPro(fn);
//	strcpy(fname,"C:\\EIP3\\DES\\");
//   strcat(fname,fn);
   strcpy(fname,fn);
   strcat(fname,".DXF");
   m_pOutFile = fopen(fname,"wb");

	fwrite( m_pHeaderBuffer, 1, m_lHeaderSize, m_pOutFile );

   out(0,"SECTION");
   out(2,"ENTITIES");
}

void DXFile::fMakeOff() {
//	fAddText( PROJ->m_szProjectTitle, 347.0, 37.5, 2 );
//	fAddText( m_szSheetType, 332.0, 29.5, 2 );
	fAddText( m_szProjectTitle, 354.0, 37.5, 2 );
	fAddText( m_szSheetType, 354.0, 29.5, 2 );
	int l = strlen( m_szSheetNo );
	char *szNo = m_szSheetNo + l - 4;
	fAddText( szNo, 401.0, 8.75, 1.5 );

    out(0,"ENDSEC");
	fwrite( m_pObjectBuffer, 1, m_lObjectSize, m_pOutFile );

// 다음줄 2002.3.17 복구함...
/*
  0
SECTION
  2
ENTITIES
  0
INSERT
  5
431
  8
0
  2
BORDER
 10
0.0
 20
0.0
 30
0.0
  0
ENDSEC
*/
    out(0,"SECTION");
    out(2,"ENTITIES");
    out(0,"INSERT");
    out(5,"431  ");		// 2002.4.2. code 5 는 4 글자 이상... !!! 주의...
    out(8, "0");
    out(2,"BORDER");
    out(10,"0.0");
    out(20,"0.0");
    out(30,"0.0");
    out(0,"ENDSEC");

    out(0,"EOF");
    fclose( m_pOutFile );
	 m_pOutFile = NULL;
}

void ATEXT::unpack(DXFile *d) {
   d->out(0,"TEXT");

   d->out(5,m_cHandle);

   d->out(8,layer);
   d->out(10,sp.x);
   d->out(20,sp.y);
//   d->out(30,int(0));
   d->out(40,r);
   d->out(1,value);
   if (*style) d->out(7,style);
   d->out(72,halign);
   d->out(11,ep.x);
   d->out(21,ep.y);
   d->out(73,halign2);
//   d->out(31,int(0));
}

void ARC::unpack(DXFile *d) {
   d->out(0,"ARC");

   d->out(5,m_cHandle);

   d->out(8,layer);
   d->out(10,sp.x);
   d->out(20,sp.y);
 //  d->out(30,int(0));
   d->out(40,r);
   d->out(50,sa);
   d->out(51,ea);
}

void LINE::unpack(DXFile *d) {
   d->out(0,"LINE");

   d->out(5,m_cHandle);

   d->out(8,layer);

   if ( m_pLineType ) {
	   d->out( 6, m_pLineType );
   }

   d->out(10,sp.x);
   d->out(20,sp.y);
//   d->out(30,int(0));
   d->out(11,ep.x);
   d->out(21,ep.y);

//   d->out(31,int(0));
}

void CAttribute::unpack(DXFile *d) {
	d->out(0,"ATTRIB");
	d->out(5,m_cHandle);
	d->out(8 ,layer);
	// change POS color to 9
	if (!strcmp(tag,"POS") && color != 4) color = 9;
	//
	if (color) d->out(62,color);
	d->out(10,p.x);
	d->out(20,p.y);
//	d->out(30,int(0));
	d->out(40,texth);
	d->out(1,value);
	d->out(2,tag);
	d->out(70,attr);
	d->out(7,style);
	if ( halign != 0.0 )
		d->out(72,halign);
	if ( halign74 != 0.0 )
		d->out(74,halign74);
	d->out(11,ap.x);
	d->out(21,ap.y);
//	d->out(31,int(0));
	CDXFObject::unpack( d );
	if (m_pNext) m_pNext->unpack(d);
}

void CBlock::unpack(DXFile *d) {
	d->out(0,"INSERT");
	d->out(5,m_cHandle);
	d->out(8 ,layer);
	if (hasatt) d->out(66,hasatt);
	d->out(2 ,block);
	d->out(10,p.x);
	d->out(20,p.y);
//	d->out(30,int(0));
	CDXFObject::unpack( d );
	if ( m_dzdir ) d->out( 41, m_dzdir );
	if (hasatt) {
		att->unpack(d);
		d->out(0,"SEQEND");
		d->out(5,m_cAHandle);
		d->out(8,"0");
	}
}

void DXFile::out(int n,double d){
   fprintf( m_pOutFile,"%3d\r\n",n);
   fprintf( m_pOutFile,"%5.3f\r\n",d);
}

void DXFile::out(int n,int i){
   fprintf( m_pOutFile,"%3d\r\n",n);
   fprintf( m_pOutFile,"%d\r\n",i);
}

void DXFile::out(int n,char c){
   fprintf( m_pOutFile,"%3d\r\n",n);
   fprintf( m_pOutFile,"%6d\r\n",c);
}

void DXFile::out(int n,char*s){
   fprintf( m_pOutFile,"%3d\r\n",n);
//   if (n == 5 && (*s == 0 || !strcmp(s,"0"))) {
   if (n == 5) {
		sprintf(s,"%X",m_nHandle++);
   }
   fprintf( m_pOutFile,"%s\r\n",s);
}

//void DXFile::out5(){
//	char s[16];
//	sprintf(s,"%X",m_nHandle++);
//    fprintf(wf,"  5\n%s\n", s);
//	fprintf(wf,"100\nAcDbEntity\n");
//}

void DXFile::picklhead(void **p){*p = lhead;}
void DXFile::pickihead(void **p){*p = ihead;}
void DXFile::pickahead(void **p){*p = ahead;}
void DXFile::pickthead(void **p){*p = thead;}

void CDXFObject::getsp(point &p) { p = sp; }
void CDXFObject::getep(point &p) { p = ep; }

void CBlock::getp(point &ip) { ip = p; }

void CBlock::getattval(char *name,char *ref,char *pos,int &mir,int &rot, int &poscolor) {
	CAttribute *a;
	if (!strcmp(block,"EMNPRC")||!strcmp(block,"ESRRC")||!strcmp(block,"EPBRC") ) {
		mir = 0;
		if (att) {
			a = att;
			while (a) {
				if (!strcmp(a->gettag(),"PSN")) {
					if (p.x > a->p.x) mir = 0;
					else mir = -1;
					break;
				}
				a = a->m_pNext;
			}
		}
	}
	else mir = (int)m_dzdir;
	rot = 0;
	strcpy(name,block);
	*ref = *pos = 0;
	if (att) {
		a = att;
		while(a) {
			if (!strcmp(a->gettag(),"REF"))
				strcpy(ref,a->getvalue());
			else if (!strcmp(a->gettag(),"POS")) {
				strcpy(pos,a->getvalue());
				poscolor = a->getcolor();
			}
			else if (!strcmp(a->gettag(),"NPOS")) {
				strcpy( m_szPos, a->getvalue() );
				poscolor = a->getcolor();
			}
			else if (!strcmp(a->gettag(),"RACK")) {
				strcpy( m_szRack, a->getvalue() );
			}
			else if (!strcmp(a->gettag(),"NVAL")) {
				strcpy( m_szContact, a->getvalue() );
			}
			else if (!strcmp(a->gettag(),"TYPE")) {
				strcpy( m_szType, a->getvalue() );
			}
			else if (!strcmp(a->gettag(),"VAL")) {
				strcpy( m_szContact, a->getvalue() );
			}
			a = a->m_pNext;
		}
	}
}

int CBlock::getposcolor() {
	CAttribute *a;
	if (att) {
		a = att;
		while(a) {
			if (!strcmp(a->gettag(),"POS")) return a->getcolor();
			a = a->m_pNext;
		}
	}
	return -1;
}

int CBlock::getrefcolor() {
	CAttribute *a;
	if (att) {
		a = att;
		while(a) {
			if (!strcmp(a->gettag(),"REF")) return a->getcolor();
			a = a->m_pNext;
		}
	}
	return -1;
}

char *CAttribute::gettag() { return tag; }

char *CAttribute::getvalue() { return value; }

void CAttribute::setvalue(char *iv) {
   strcpy(value,iv);
}

#define PI 3.141592654
point polar(point p, double r, double a) {
   point rp;
   rp.x = (int)(p.x + r * 1.0 * cos((double)a * PI / 180.0) + 0.1);
   rp.y = (int)(p.y + r * 1.0 * sin((double)a * PI / 180.0) + 0.1);
   return rp;
}

void ARC::get2p(point &p1,point &p2) {
   p1 = polar(sp,r,sa);
   p2 = polar(sp,r,ea);
}

char *cross[] = {
   "  0",      //0
   "CBlock",   //1
   "  5",      //2
   "",		   //3
   "  8",      //2
   "GUIDE",    //3
   "  2",      //4
   "CROSS",    //5
   " 10",      //6
   "220",      //7
   " 20",      //8
   "210",      //9
   " 30",      //10
   "0" };      //11
/*
<V14>
  0
CBlock
  5
1D
100
AcDbEntity
  8
0
100
AcDbBlockReference
  2
CROSS
 10
142
 20
111
 30
0

<V12>
  0
CBlock
  5
96C
  8
0
  2
CROSS
 10
80.0
 20
190.0
 30
0.0
*/
/*
  0
BLOCK
  8
0
  2
CROSS
 70
     0
 10
0
 20
0
 30
0
  3
CROSS
  1

  0
LINE
  5
F01F
  8
GUIDE
 10
-2
 20
2
 30
0
 11
2
 21
-2
 31
0
  0
LINE
  5
F021
  8
GUIDE
 10
2
 20
2
 30
0
 11
-2
 21
-2
 31
0
  0
ENDBLK
  5
F023
  8
0
*/
void DXFile::InsertMark(point &p, char *insname) {
//	return;
   CBlock *i = new CBlock;
   strcpy(i->layer,"GUIDE");
   strcpy(i->block,insname);

	sprintf(i->m_cHandle,"%X",m_nHandle++ + 0xF000);

   i->p = p;
   i->att = NULL;
   i->insert(ihead,(CBlock*)ilast,i);
   ni++;
}

/*
  0
SECTION
  2
ENTITIES
  0
TEXT
  8
0
 10
474
 20
180
 30
0
 40
2
  1
CENTER
  7
ROMANS
 72
     1
 11
480
 21
180
 31
0
  0
TEXT
  8
0
 10
468
 20
189
 30
0
 40
2
  1
MIDDLE CENTER
  7
ROMANS
 72
     1
 11
480
 21
190
 31
0
 73
     2
  0
TEXT
  8
0
 10
475
 20
199
 30
0
 40
2
  1
MIDDLE
  7
ROMANS
 72
     4
 11
480
 21
200
 31
0
  0
TEXT
  8
0
 10
480
 20
210
 30
0
 40
2
  1
STANDARD
  7
ROMANS
  0
ENDSEC
  0
EOF
*/

double _deftexth = TEXTHEIGHT;

void DXFile::fAddText(char *s, double x, double y, char *szLayer) {
   ATEXT t;
   int l;
   strcpy(t.value,s);
   if (szLayer)
	   strcpy(t.layer, szLayer);
   else
	   strcpy(t.layer,"0");
   strcpy(t.style,"ROMANS");

	sprintf(t.m_cHandle,"%X",m_nHandle++);

   l = strlen(s);
   t.sp.x = x-l;   t.ep.x = x+l;
   t.sp.y = t.ep.y = y;
   t.r = _deftexth;
   t.halign = 5;
   t.unpack(this);
}

void DXFile::fAddText(char *s, double x, double y, double h, int nVert, char *szLayer) {
	ATEXT t;
	double l;
	strcpy(t.value,s);
	if (szLayer)
		strcpy(t.layer, szLayer);
	else
		strcpy(t.layer,"0");
	strcpy(t.style,"ROMANS");

	sprintf(t.m_cHandle,"%X",m_nHandle++);

	int almode = (int)h / 1000;
	int sz = (int)h % 1000;
	switch (almode) {
	case 0:
		l = (double)strlen(s);
		t.r = h;
		l = l * h / 3;	// W = H * 2 / 3
//		t.halign = 5;
		t.halign = 1;
		t.halign2 = 1;
		if ( !nVert ) {
			t.sp.x = x-l;   t.ep.x = x;
			t.sp.y = y;
			t.ep.y = y - _deftexth / 3.0;
		}
		else {
			t.sp.y = y-l;   t.ep.y = y+l;
			t.sp.x = t.ep.x = x;
		}
		break;
//	case 1:
//		t.r = 2;
//		l = sz / 2;
//		t.halign = 5;
//		break;
	default:
		t.halign = almode - 1;
		l = sz / 2;
		t.r = _deftexth;
		if ( almode == 1 ) {
			t.sp.x = x;   t.ep.x = x;
		}
		else {
			if ( !nVert ) {
				t.sp.x = x-l;   t.ep.x = x+l;
			}
			else {
				t.sp.y = y-l;   t.ep.y = y+l;
			}
		}
		if ( !nVert ) {
			t.sp.y = t.ep.y = y - _deftexth / 3.0;
		}
		else {
			t.sp.x = t.ep.x = x;
		}
		break;
	}
	t.unpack(this);
}

void DXFile::fAddInsert(char *s, double x, double y) {
   CBlock t;
   t.hasatt=0;
   t.m_dzdir = 0.0;
   t.att=NULL;
   strcpy(t.block,s);
   strcpy(t.layer,"0");

	sprintf(t.m_cHandle,"%X",m_nHandle++);

   t.p.x = x; t.p.y = y;
   t.unpack(this);
}

void DXFile::fAddInsert(char *s, double x, double y, double xs, double ys) {
   CBlock t;
   t.hasatt=0;
   t.m_dzdir = 0.0;
   t.att=NULL;
   strcpy(t.block,s);
   strcpy(t.layer,"0");

	sprintf(t.m_cHandle,"%X",m_nHandle++);

   t.p.x = x; t.p.y = y;
   t.unpack(this);
   out(41,xs);
   out(42,ys);
}

char *pLineTypes[] = {
	NULL,
	"DASHED",
};

void DXFile::fAddLine(double x1, double y1, double x2, double y2, int lType ) {
	LINE t;
	t.sp.x = x1; t.sp.y = y1;
	t.ep.x = x2; t.ep.y = y2;
	strcpy(t.layer,"0");
	sprintf(t.m_cHandle,"%X",m_nHandle++);
	if ( lType > 1 ) lType = 0;
	t.m_pLineType = pLineTypes[ lType ];
	t.unpack(this);
}

void DXFile::fAddRect(double x, double y, double w, double h) {
	fAddLine( x, y, x + w, y );
	fAddLine( x + w, y, x + w, y - h );
	fAddLine( x, y, x, y - h );
	fAddLine( x, y  - h, x + w, y - h );
}

CBlock *DXFile::FindBlock(point &p) {
   CBlock *i;
   i = (CBlock *)ihead;
   while (i) {
      if (p.x == i->p.x && p.y == i->p.y) break;
      i = i->m_pNext;
   }
   return i;
}


void CBlock::insert(CBlock *&head, CBlock *&last, CBlock *now) {
   CBlock *i, *l = NULL;
   i = head;
   if (i) {
      while (i) {
	 if(i->p.x > now->p.x) break;
	 else if(i->p.x == now->p.x && i->p.y < now->p.y) break;
	 l = i;
	 i = i->m_pNext;
      }
      if (i) {
	 if (!l) head = now;
	 else l->m_pNext = now;
	 now->m_pNext = i;
      } else {
	 last = l->m_pNext = now;
	 last->m_pNext = NULL;
      }
   }
   else {
      head = last = now;
      last -> m_pNext = NULL;
   }
}

long DXFile::ReadToBuffer()
{
	TRACE("함수 : void CDXFObject::AddAnyCode( int nCode, char *pBf ) {\n");
	m_lHeaderSize =	m_lEntitySize =	m_lObjectSize = 0;
    m_lReadPtr = 0;

	m_lFileSize = filelength( fileno( m_pFile ) );
	m_pFileBuffer = new char[m_lFileSize + 1];

	char *dest = m_pFileBuffer;
/*
	long nBeRead = m_lFileSize;
	long nRead = 0L;
	while (nBeRead > 0) {
		long nr = FILEBUFSIZE;
		if (nr < nBeRead) nr = nBeRead;
		nRead = fread( dest, 1, nr, f );
		nBeRead -= nRead;
		dest += nRead;
	}
*/
	m_lFileSize = fread( dest, 1, m_lFileSize, m_pFile );
	m_pFileBuffer[m_lFileSize] = '\0';
	return m_lFileSize;
}

long DXFile::CopyAndFindEntityLocation()
{
	TRACE("함수 : long DXFile::CopyAndFindEntityLocation()\n");
	if (m_lReadPtr >= m_lFileSize) return 0;
	char *p = m_pFileBuffer;
	char *nextloc;
	m_lHeaderSize = 0;
	nextloc = strstr( p, "  0\r\nSECTION\r\n  2\r\nENTITIES\r\n" );
	if (nextloc ==  NULL) return 0;
	m_lHeaderSize = nextloc - p;

//2002.3.12
	if (m_lHeaderSize == 0) {
		DXFile dxf( DXF_TEMPLETE_A3 );

		dxf.m_pFile = fopen( DXF_TEMPLETE_A3, "rb" );

		dxf.ReadToBuffer();
		char *ext = dxf.m_pFileBuffer;
		char *extnextloc;
		extnextloc = strstr( ext, "  0\r\nSECTION\r\n  2\r\nENTITIES\r\n" );
		long m_lExtHeaderSize = extnextloc - ext;

		long size = m_lFileSize + m_lExtHeaderSize;
		char *temp = new char[size];
		memcpy( temp, dxf.m_pFileBuffer, m_lExtHeaderSize );
		memcpy( temp + m_lExtHeaderSize, m_pFileBuffer + m_lHeaderSize, m_lFileSize - m_lHeaderSize);
		m_lFileSize += (m_lExtHeaderSize - m_lHeaderSize);
		delete [] m_pFileBuffer;
		m_pFileBuffer = temp;
		nextloc = temp + m_lExtHeaderSize;
		m_lHeaderSize = m_lExtHeaderSize;
	}

	m_lReadPtr = m_lHeaderSize;
	m_pHeaderBuffer = m_pFileBuffer;

	m_pObjectBuffer = strstr( nextloc, "  0\r\nSECTION\r\n  2\r\nOBJECTS\r\n" );
	if (m_pObjectBuffer) {
		m_lObjectSize = m_lFileSize - (m_pObjectBuffer	- nextloc + m_lHeaderSize);
	}
	else
		m_lObjectSize = 0L;
	return 1;
}

long DXFile::FindEntityLocation()
{
	TRACE("함수 : long DXFile::FindEntityLocation()\n");
	if (m_lReadPtr >= m_lFileSize) return 0;
	char *p = m_pFileBuffer;
	char *nextloc;
	m_lHeaderSize = 0;
	nextloc = strstr( p, "  0\r\nSECTION\r\n  2\r\nENTITIES\r\n" );
	if (nextloc ==  NULL) return 0;
	m_lHeaderSize = nextloc - p;
	m_lReadPtr = m_lHeaderSize;
	m_pHeaderBuffer = m_pFileBuffer;

	m_pObjectBuffer = strstr( nextloc, "  0\r\nSECTION\r\n  2\r\nOBJECTS\r\n" );
	if (m_pObjectBuffer) {
		m_lObjectSize = m_lFileSize - (m_pObjectBuffer	- nextloc + m_lHeaderSize);
	}
	else
		m_lObjectSize = 0L;
	return 1;
}

int DXFile::FindCode0()
{
	TRACE("함수 : int DXFile::FindCode0()\n");
	do {
		line++;
		if (!CopyOneLine()) return 0;
//		strupr(bf);
	} while (strcmp(bf,"  0\r\n"));
	line++;
	if (!CopyOneLine()) return 0;
//	strupr(bf);
	return 1;
}

char * DXFile::CopyOneLine()
{
	TRACE("함수 : char * DXFile::CopyOneLine()\n");
	if (m_lReadPtr >= m_lFileSize) return 0;
	char *p = m_pFileBuffer + m_lReadPtr;
	char *nextloc;
	nextloc = strstr( p, "\r\n" );
	if (nextloc ==  NULL) nextloc = strstr( p, "\r\n" );
	long len;
	if (nextloc) {
		len = nextloc - p + 2;
		strncpy( bf, p, len );
	}
	else len = 0;
	bf[len] = '\0';
	m_lReadPtr += len;

#ifdef MYDEB
	if (debfp) fwrite( bf, strlen(bf), 1, debfp );
#endif
	return bf;
}

