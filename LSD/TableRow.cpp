//=======================================================
//==              TableRow.cpp
//=======================================================
//	FileName :  TableRow.cpp
//  Creater  :  DoYoung Kim
//	Date     :  2004-10-11
//	Version  :  2.01
//	Comment  :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "TableRow.h"
#include "TableDoc.h"
#include "../include/Parser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(CRow, CObject, 0)

CPoint CRow::m_Org;
int CRow::m_nTextH, CRow::m_nPage;
extern void DelStringMultiNo(CString &s , BOOL bPOINT = FALSE); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T

int HIDX(CString &str) { // signal priority 
	if (str.Left(2) == "OH") return 1;  //1 outer
	if (str.Left(2) == "OC") return 2;  //2 outer callon
	if (str.Left(2) == "HO") return 3;  //3 home
	if (str.Left(2) == "CA") return 4;  //4 home callon
	if (str.Left(2) == "ST") return 5;  //5 start
	if (str.Left(2) == "SH") return 6;  // shunt
	if (str.Left(2) == "AS") return 7;  // advanced start
	if (str.Left(2) == "TO") return 8;  // token XXXX
	if (str.Left(2) == "PO") return 9;  // point
	return 0;
};

//=======================================================
//
//  함 수 명 :  Serialize
//  함수출력 :  None
//  함수입력 :  CArchive &ar
//  작 성 자 :  DoYoung Kim
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  Serialize
//
//=======================================================
void CRow::Serialize(CArchive &ar) {
	CObject::Serialize(ar);
	if (ar.IsStoring()) { } 
	else { }
}

CRow::CRow(CTableDoc *pDoc) {
    m_pStrPath	= "";
	m_pDoc		= pDoc;
	m_bValid	= FALSE;
	Init();
}

int CRow::SigTypeCheck(CString strToken)
{
	int m_iSigType = -1;

	if		(strToken == "HOME")		m_iSigType = _HOME;
	else if (strToken == "START")		m_iSigType = _START;
	else if (strToken == "ISTART")		m_iSigType = _ISTART;
	else if (strToken == "ISHUNT")		m_iSigType = _ISHUNT;
	else if (strToken == "GSHUNT")		m_iSigType = _GSHUNT;
	else if (strToken == "SHUNT")		m_iSigType = _SHUNT;
	else if (strToken == "ASTART")		m_iSigType = _ASTART;
	else if (strToken == "OHOME")		m_iSigType = _OHOME;
	else if (strToken == "TCB")			m_iSigType = _TOKENLESSC;
	else if (strToken == "TGB")			m_iSigType = _TOKENLESSG;
	else if (strToken == "OC")			m_iSigType = _OC;
	else if (strToken == "OG")			m_iSigType = _OG;
	else if (strToken == "CALLON")		m_iSigType = _CALLON;
	else if (strToken == "OCALLON")		m_iSigType = _OCALLON;

	return m_iSigType;
}

void CRow::Init() {

	m_iHeight = 17;	// default row height

	if (m_nRowType == ROWPOINT) {
		void *pContainer[] = {
			&m_cSignal,
			&m_cTrack,
			&m_cLock,
			&m_cButton
		};
		void *pData[] = {
			&m_Signal,
			&m_Track2,
			&m_LockSignal,
			&m_Apk,

		};
		int pWidth[] = {
			W_PNAME,
			W_PNO,
			W_PLOCK,
			W_PKEY,
		};

		m_nMaxCells = sizeof(pContainer)/sizeof(void*);
		for ( int i=0; i<m_nMaxCells; i++ ) {
			m_pCell[i] = (CCell*)pContainer[i];
			CCell *pCell = (CCell*)pContainer[i];
			pCell->SetData( (CObject*)pData[i] );
			pCell->SetWidth( pWidth[i] );
			pCell->m_pRow = this;
		}
	} else if (m_nRowType == ROWCROSSING) {
		void *pContainer[] = {
			&m_cSignal,
			&m_cTrack,
			&m_cLock,
		};
		void *pData[] = {
			&m_Signal,
			&m_Track2,
			&m_LockSignal,

		};
		int pWidth[] = {
			W_PNAME,
			W_PNO,
			W_PLOCK + W_PKEY,
		};

		m_nMaxCells = sizeof(pContainer)/sizeof(void*);
		for ( int i=0; i<m_nMaxCells; i++ ) {
			m_pCell[i] = (CCell*)pContainer[i];
			CCell *pCell = (CCell*)pContainer[i];
			pCell->SetData( (CObject*)pData[i] );
			pCell->SetWidth( pWidth[i] );
			pCell->m_pRow = this;
		}
	} else { 
		strTempButton = m_Button;
		iTemp = strTempButton.Find("(",0);				// delete (M),(M)-1,(C),(C)-1 at interlocking table
		strTempButton = strTempButton.Left(iTemp);

		void *pContainer[] = {
			&m_cNumber,
			&m_cType,
			&m_cNo,
			&m_cSignal,
			&m_cButton,
			&m_cName,
			&m_cLock,
			&m_cPointLock,
			&m_cSAspect,
			&m_cAheadSig,
			&m_cTrackClear,
			&m_cOccupy,
			&m_cTime,
			&m_cHold,
			&m_cDelay,
			&m_cTcOCCSeq,
//			&m_cTcOCC,
			&m_cTcTime,
			&m_cOverLap,
			&m_cAddCond,
			&m_cLNo,
		};
		void *pData[] = {
			&m_Number,
			&m_Type,
			&m_No,
			&m_RouteName,
			&m_Signal,
			&strTempButton,
			&m_LockSignal,
			&m_LockSwitch,
			&m_Aspect_Table,
			&m_AheadSignal_Table,
			&m_Track,
			&m_Occupy,
			&m_Time,
			&m_Approach,
			&m_Delay,
			&m_TcOCCSeq,
//			&m_TcOCC,
			&m_OverLap,
			&m_TcTime,
			&m_AddCond,
			&m_No,
		};
		int pWidth[] = {
			W_NUMBER,
			W_TYPE,
			W_NO,
			W_NAME,
			W_SIGNAL,
			W_BUTTON,
			W_LOCK,
			W_POINTLOCK,
			W_SASPECT,
			W_AHEADSIG,
			W_TRACK,
			W_OCCUPY,
			W_TIME,
			W_HOLD,
			W_DELAY,
			W_TCOCCSEQ,
//			W_TCOCC,
			W_OVLAP,
			W_TCTIME,
			W_ADDCOND,
			W_NO,
		};

		m_nMaxCells = sizeof(pContainer)/sizeof(void*);

		for ( int i=0; i<m_nMaxCells; i++ ) {
			m_pCell[i] = (CCell*)pContainer[i];
			CCell *pCell = (CCell*)pContainer[i];
			pCell->SetData( (CObject*)pData[i] );
			pCell->SetWidth( pWidth[i] );
			pCell->m_pRow = this;
		}	
	
	}
}

CRow::CRow(CString& com, CString& linestr, CString& pre, CString& Aspect, CString& Path, CRow *&cYUDO, CObList *cObTTB,
		   CTableDoc *pDoc  )
{

	int		nLimitOverlapSwitch		= 20;	// Document 또는 App 에서 가져와야 함.
	int		nUsedOverlapSwitch		= 0;
	int     iSignalCnt				= 0;	// 진로방향의 신호기 갯수  ILT 에서 E: 으로 나온 신호기의 갯수를 센다. 
	CString striSignalName;
	CString str						= linestr;
	CString token;
	BOOL	bIsFirstOverlapSwitch	= FALSE;
	
	m_iFreeShuntCk	= 0;
	m_bValid		= FALSE;
	m_pDoc			= pDoc;
	striSignalName	= "";


	if (str.Left(7) != "[ROUTE]" && str.Left(7) != "[BLOCK]") return;

	CMyStrList route, track, ctrack , ctrack2 ,outt, maint;
	CParser cline(com);
	cline.GetToken( token );
	m_cUD = token.GetAt(0);
	cline.GetToken( token );

	m_nRowType = ROWSIGNAL;
	m_nSubType = SigTypeCheck( token );
	if ( m_nSubType == -1 ) return;
	m_Type  = token;   // 신호기 명칭

	cline.GetToken( m_Signal );

	CMyStrList point, ypoint , cpoint;

	CMyStrList ExtSignals, ExtSignalsOrder;
	CString ttbname[10];
	int ttbno = 0;

	CString firstT,lastT;
	CString sTname;
	CString sTname2;
	CString sTnameTemp;
	CMyStrList hold;
	CMyStrList FrankTC;
	BOOL first = TRUE, first1 = TRUE;

	Path = Path.Mid(6);
    m_pStrPath = Path;

	if (str.Left(7) == "[BLOCK]"){
		m_nRowType = ROWBLOCKROUTE;
		m_nSubType = _BLOCK;
		CParser line(str);
		line.GetToken( token );

		m_No = m_Signal;

		while (line.GetToken( token )) {
			char c = token.GetAt(0);
			if (c == 'Q' && token.GetAt(1) == ':') {
				CString s1 = token.Mid(2,token.GetLength()-2);
				m_LockSignal.CheckAdd(s1);
			} else if (c == 'G' && token.GetAt(1) == ':') {
				// G: 는 무시
			} else if (c == 'L' && token.GetAt(1) == ':') {
				// L: 는 무시
			} else if (c == 'R' && token.GetAt(1) == ':') {
				CString s1 = token.Mid(2,token.GetLength()-2);
				m_LockSignal.CheckAdd(s1);
			} else if (c == 'E' && token.Find("E:")>=0) { 
				iSignalCnt ++;
				CString s1 = token.Mid(2,token.GetLength()-2);
				if ( iSignalCnt > 1 ) {
					m_LockSwitch.CheckAdd(s1);
				} else {
					m_Button = s1;
					m_AheadSignal = s1;
				}
			} else if (c == 'P' && token.Find("P:")>=0) { 
				iSignalCnt ++;
				CString s1 = token.Mid(2,token.GetLength()-2);
				if ( iSignalCnt > 1 ) {
					m_LockSwitch.CheckAdd(s1);
				} else {
					m_Button = s1;
					m_AheadSignal = s1;
				}
			} else if (c == 'I' && token.Find("I:")>=0) { 
				iSignalCnt ++;
				CString s1 = token.Mid(2,token.GetLength()-2);
				striSignalName = s1;
				if ( iSignalCnt > 1 ) {
					m_LockSwitch.CheckAdd(s1);
				} else {
					m_Button = s1;
					m_AheadSignal = s1;
				}
			} else {
				if ( m_Type == "TCB" || m_Type == "OC") {
					if ( m_TrackFromTo != "" ) {
						m_TrackFromTo = m_TrackFromTo + ":" + token;
					} else m_TrackFromTo = token;
				} else {
					if ( m_TrackFromTo != "" ) {
						m_TrackFromTo = token + ":" + m_TrackFromTo;
					} else m_TrackFromTo = token;				
				}
			}
		}
		m_bValid = TRUE;
		return;
	}

	if (pre.Left(10) == "[PREROUTE]") {
		CString p1,p2, tstr;
		CParser line11(pre);
		line11.GetToken( token );
		int n = 0;
		while (line11.GetToken( token )) {
			char c = token.GetAt(0);
			if (tstr != token) {
				if (c == '@') {
					n = 1;
				}
				else if (n == 0 && isSwitch(token)) { 
				}
				else {
					if (n == 0) {
						p1 += token + " ";
					}
					else {
						char c = token.GetAt(0);
						p2 += token + " ";
					}
					tstr = token;
				}
			}
		}
		CParser line1(p2);
		n = 0;
		while (line1.GetToken( token )) {
			char c = token.GetAt(0);
			if (isSwitch(token)) { 
				point.CheckAdd(token, TRUE);
				cpoint.CheckAdd(token, TRUE);
			}
			else if (c == 'B' && token.GetAt(1) == ':') {
				n = 1;
			}
			else if (c == 'S' && token.GetAt(1) == ':') {
			}
			else {
				sTname = token;
				DelStringMultiNo(sTname); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T
				if (sTname != lastT) {
					if (first1) {
						firstT = sTname;
					}
					first1 = FALSE;
				}
			}
		}
		p2 = p1 + p2;

		CParser line2(p2);
		while (line2.GetToken( token )) {
			char c = token.GetAt(0);
			if (isSwitch(token)) { 
			}
			else if (c == 'B' && token.GetAt(1) == ':') {
			}
			else if (c == 'S' && token.GetAt(1) == ':') {
			}
			else {
				sTname = token;
				DelStringMultiNo(sTname); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T

				if (sTname != lastT) {
					if ( sTname.Find('?') < 0 ) {
						hold.CheckIns(sTname);
					}
				}
			}
		}
	}
	else if (pre.Left(11) == "[HOLDROUTE]") {
		CParser line1(pre);
		line1.GetToken( token );
		CString tstr;
		while (line1.GetToken( token )) {
			if (tstr != token) {
				char c = token.GetAt(0);
				if (isSwitch(token)) { 
				}
				else if (c == 'B' && token.GetAt(1) == ':') {
				}
				else if (c == 'L' && token.Find("L:")>=0) { 
					CString s1 = token.Mid(2,token.GetLength());
					m_LevelCross =  s1;
				}
				else if (c == 'S' && token.GetAt(1) == ':') {
					CString s1 = token.Mid(2,token.GetLength());
					m_DependSignal =  s1;
				}
				else {
					sTname = token;
					DelStringMultiNo(sTname); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T
					if (sTname != lastT) {
						if ( sTname.Find('?') < 0 ) {
							hold.CheckIns(sTname);
							tstr = token;
						}
					}
				}
			}
		}
	}

	if (Aspect.Left(8) == "[ASPECT]") {
		CParser line1(Aspect);
		CString tstr;
		int iAheadCheckFlag;  // 전방신호기 에 해당하는 부분에 들어가면 1 전방신호기 부분이 끝나면 3 자신현시일경우 0
		iAheadCheckFlag = 0;
		while (line1.GetToken( token )) {
			if ( token == "[" ) {
				iAheadCheckFlag = 1;
			}
			if ( token == "]" ) {
				iAheadCheckFlag = 3;
			}
			if ( iAheadCheckFlag == 3 ) {
				m_AheadSignal = tstr;
				m_AheadSignal_Table = tstr;
				if ( m_AheadSignal_Table.Find("LI(M)") > 0 )  {
					m_AheadSignal_Table.Replace ( "LI(M)","M" );
				}
				if ( m_AheadSignal_Table.Find("LI(R)") > 0 )  {
					m_AheadSignal_Table.Replace ( "LI(R)","L" );
				}
				if ( m_AheadSignal_Table.Find("LI(L)") > 0 )  {
					m_AheadSignal_Table.Replace ( "LI(L)","L" );
				}
				if ( m_Aspect_Table.Find("LI(F)") > 0 )  {
					m_Aspect_Table.Replace ( "LI(F)","F" );
					m_Aspect.Replace ( "LI(F)","LI(L)" );
				}
				if ( m_AheadSignal_Table.Find("LI") > 0 )  {
					m_AheadSignal_Table.Replace ( "LI","L" );
				}
				//if ( m_AheadSignal.Find("LI(M)") > 0 )  {
				//	m_AheadSignal.Replace ( "(M)","(L)" );
				//}
			}
			if ( iAheadCheckFlag == 2 ) {
				if ( tstr == "" ) tstr = token;
				else tstr = tstr + " " + token;
			}
			if ( iAheadCheckFlag == 1 ) iAheadCheckFlag = 2;
			CString strTemp;
			if ( iAheadCheckFlag == 0 ) {
				m_Aspect = token;
				m_Aspect_Table = m_Aspect;
				strTemp = m_Aspect;

					if ( m_Aspect_Table.Find("LI(M)") > 0 )  {
						m_Aspect_Table.Replace ( "LI(M)","M" );
					}
					if ( m_Aspect_Table.Find("LI(R)") > 0 )  {
						m_Aspect_Table.Replace ( "LI(R)","L" );
					}
					if ( m_Aspect_Table.Find("LI(L)") > 0 )  {
						m_Aspect_Table.Replace ( "LI(L)","L" );
					}
					if ( m_Aspect_Table.Find("LI(F)") > 0 )  {
						m_Aspect_Table.Replace ( "LI(F)","F" );
						m_Aspect.Replace ( "LI(F)","LI(L)" );
					}
					if ( m_Aspect_Table.Find("LI") > 0 )  {
						m_Aspect_Table.Replace ( "LI","L" );
					}
					//if ( m_Aspect.Find("LI(M)") > 0 )  {
					//	m_Aspect.Replace ( "(M)","(L)" );
					//}
			}
		}
	}

	CParser line(str);
	line.GetToken( token );

	int n = 0;
	CString sNextSignal;
	CString sDependSignal;
	BOOL bFirstNextSignal = TRUE;
	BOOL bSecondNextSignal = TRUE;
	BOOL bReversePoint = FALSE;
	BOOL bLastPoint = FALSE;
	iSignalCnt = 0; // 진로방향의 신호기 갯수  ILT 에서 E: 으로 나온 신호기의 갯수를 센다. 
	m_GShuntSignal = "";
	while (line.GetToken( token )) {
		
		char c = token.GetAt(0);
		if (isSwitch(token)) { 
			bLastPoint = FALSE;
			if (!bIsFirstOverlapSwitch || (bIsFirstOverlapSwitch && (nUsedOverlapSwitch < nLimitOverlapSwitch))) {
				if (n == 0)	{
					point.CheckAdd(token, TRUE);
					cpoint.CheckAdd(token, TRUE);				
					ypoint.CheckAdd(token, TRUE);
					if (token.GetAt(0) == 'R') {
						bReversePoint = TRUE;
					}
					if (token.GetAt(1) == 'N') {
						bLastPoint = TRUE;
					}

				}
				else if ( m_nSubType == _CALLON ) {
					cpoint.CheckAdd(token, TRUE);				
					if (token.GetAt(1) == 'N') {
					}

				}
				else if ( m_nSubType == _SHUNT ) {
					cpoint.CheckAdd(token, TRUE);				
					if (token.GetAt(1) == 'N') {
					}

				}
				else if (m_nSubType == _HOME || m_nSubType ==_OCALLON || m_nSubType ==_START ) {
					point.CheckAdd(token, TRUE);
					cpoint.CheckAdd(token, TRUE);				
					if (token.GetAt(1) == 'N') {
						if ( m_nSubType ==_HOME ) {
							bLastPoint = TRUE;
						}
					}

				} 				
				nUsedOverlapSwitch++;
			}
		}
		else if (c == 'B' && token.Find("B:")>=0) { 
			int loc = token.Find('_');
			m_Button = token.Mid(2,loc-2);
			m_nLineNo = atoi(token.Mid(loc+1,2));
			if ( m_nLineNo == 99 ) {
				n = 0;
			} else {
				n = 1;
				if ( !bIsFirstOverlapSwitch ) {
					bIsFirstOverlapSwitch = TRUE;
					nUsedOverlapSwitch = 0;
				}
			}
		}
		else if (c == 'C' && token.Find("C:")>=0) { 
			n = 1;
			if ( !bIsFirstOverlapSwitch ) {
				bIsFirstOverlapSwitch = TRUE;
				nUsedOverlapSwitch = 0;
			}
		}
		else if (c == 'L' && token.Find("L:")>=0) { 
				CString s1 = token.Mid(2,token.GetLength());
				m_LevelCross =  s1;

				if (( m_nSubType == _CALLON || m_nSubType == _SHUNT ) && bIsFirstOverlapSwitch == TRUE ) {
					m_LevelCross = "";
				}
		}
		else if (c == 'S' && token.Find("S:")>=0) { // Depend Signal

			CString s1 = token.Mid(2,token.GetLength());
			m_DependSignal =  s1;
			if ( bSecondNextSignal && !bFirstNextSignal) {
				sDependSignal = s1;
				bSecondNextSignal = FALSE;
			}
			if ( bFirstNextSignal ) {
				sNextSignal = s1;
				bFirstNextSignal = FALSE;
			}
			ExtSignals.AddTail(s1);
			ExtSignalsOrder.AddTail(s1);
			ttbname[ttbno] = s1;
			ttbno++;
		}
		else if (c == 'Q' && token.Find("Q:")>=0) { //token.GetAt(1) == ':') {	// 신호(장외폐색)
			CString s1 = token.Mid(2,token.GetLength()-2);
			if ( bFirstNextSignal ) {
				sNextSignal = s1;
				bFirstNextSignal = FALSE;
			}
			m_BlockSignal = s1;
			ExtSignals.AddTail(s1);
			ExtSignalsOrder.AddTail(s1);
		}
		else if (c == 'R' && token.Find("R:")>=0) { //token.GetAt(1) == ':') {	// 신호(장외폐색)
			CString s1 = token.Mid(2,token.GetLength()-2);
			m_BlockSignal = s1;
		}
		else if (c == 'E' && token.Find("E:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			iSignalCnt ++;
			CString s1 = token.Mid(2,token.GetLength()-2);
			if ( bFirstNextSignal ) {
			    sNextSignal = s1;
				bFirstNextSignal = FALSE;
			}
			ExtSignalsOrder.AddTail(s1);
		}

		else if (c == 'U' && token.Find("U:")>=0) {
			   CString s1 = token.Mid(2,token.GetLength()-2);
			   m_AddCond = "%G"+s1 + "\n" + m_AddCond;
			   if ( m_GShuntSignal !="" ) s1 = "," + s1;
			   m_GShuntSignal = m_GShuntSignal + s1;
		}

		else if (c == 'G' && token.Find("G:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			CString s1 = token.Mid(2,token.GetLength()-2);
		    sNextSignal = s1;
			m_DependSignal = m_Signal;
		}

		else if (c == 'T' && token.Find("T:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			if ( sNextSignal == "" ) sNextSignal = m_Signal; // G: 를 거치디 않았을때 
			m_DependSignal = m_Signal;
			m_iFreeShuntCk = 1;

		}
		else if (c == 'P' && token.Find("P:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			iSignalCnt ++;
			CString s1 = token.Mid(2,token.GetLength()-2);
			if ( bFirstNextSignal ) {
			    sNextSignal = s1;

				m_DependSignal = s1;
				bFirstNextSignal = FALSE;
			}
			ExtSignalsOrder.AddTail(s1);
		}
		else if (c == 'I' && token.Find("I:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			CString s1 = token.Mid(2,token.GetLength()-2);
			striSignalName = s1;

			if ( bFirstNextSignal ) {
			    sNextSignal = s1;

				m_DependSignal =  s1;
				bFirstNextSignal = FALSE;
			}
		}
		else if (c == 'F' && token.Find("F:")>=0) { //token.GetAt(1) == ':') {	// 신호(기타)
			CString s1 = token.Mid(2,token.GetLength()-2);
			FrankTC.CheckIns(s1);
		}
		else if (token == "MAIN") {
			n = 2;
		} 
		else if (token == "CALLON") {
			n = 3;
		} 
		else if (token == "SHUNT") {
			n = 3;
		} 
		else {
			sTname = token;
			sTname2 = sTname;
			DelStringMultiNo(sTname); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T			
			DelStringMultiNo(sTname2,TRUE); // 둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T

			if (sTname != lastT) {
				if (n == 0) {
					if (first) {
						if (first1) firstT = sTname;
					}
					else {
						if ( sTname.Find('?') < 0 ) route.CheckAdd(sTname);
						if ( sTname.Find('?') < 0 ) track.CheckTrackAdd(sTname);
						if ( sTname.Find('?') < 0 ) ctrack.CheckAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack2.CheckAdd(sTname2);				
					}
					first = FALSE;
					if ( sTname.Find('?') < 0 )	lastT = sTname;
				}
				else if ( n == 1 && iSignalCnt == 1 ) {
						if ( sTname.Find('?') < 0 ) track.CheckTrackAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack.CheckAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack2.CheckAdd(sTname2);				
				} 
				else if ( n == 2 && iSignalCnt == 1 ) {
						if ( sTname.Find('?') < 0 ) track.CheckTrackAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack.CheckAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack2.AddTail(sTname2);				
				}
				else if ( n == 3 && iSignalCnt == 1 ) {
						if ( sTname.Find('?') < 0 ) ctrack.CheckAdd(sTname);				
						if ( sTname.Find('?') < 0 ) ctrack2.CheckAdd(sTname2);				
				}
				else if (n == 1 && m_nSubType == _HOME) {
					if ( sTname.Find('?') < 0 ) maint.AddTail(sTname);
				}
			} else {
				if ( sTname.Find('?') < 0 ) ctrack2.CheckAdd(sTname2);				
			}
		}

	}

	CString *strff;
	POSITION posl;
	if ( (m_pDoc->m_StationObject.m_StationName.Find ( "AKHA",0 ) < 0) && (m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) < 0) ) {
		if ( bLastPoint == TRUE ) {
			posl = track.GetTailPosition();
			strff = (CString*)track.GetNext(posl);
			sTname = *strff;
			if ( sTname.Find('W') >0 && !(sTname == "#W36B37BT" && m_pDoc->m_StationObject.m_StationName.Find ( "BHAI",0) > 0 )) {
				sTname = "$"+sTname;
				track.RemoveTail();
				track.CheckAdd(sTname);
				// 2012.07.20조건이 불명확 하여 일단 예외문 처리.
			}
		}
	}

	m_TrackClear = track; 
	CString * strTT;
	for ( int m = 0; m < m_TrackClear.GetCount(); m++ ) {
		strTT = m_TrackClear.GetAtIndex(m);
	}
	m_CallonTrack = ctrack;
	m_CallonTempTrack = ctrack2;
	m_Route = route;
	
	int FrankTCCnt = FrankTC.GetCount();
	int k;
	CString* strFrankTC;
	CString strFTC;
	if ( FrankTCCnt > 0 ) {
		for ( k = 0 ; k < FrankTCCnt ; k++ ){
			strFrankTC = (CString *)FrankTC.GetAtIndex(k);
			strFTC = *strFrankTC;
			if ((m_pDoc->m_StationObject.m_StationName.Find ( "AKHA",0 ) >= 0 ) && (strFTC.Find("W131T") >=0 )) { // 알롬 요구로 수정 2007.03.06
			} else {
				m_FrankTrack.AddTail ( strFTC ); 
				m_Track.CheckIns( strFTC , TRUE);
			}
		}
	}

	if ( striSignalName != "" ) {
		m_AddCond = "%D"+striSignalName+"\n";
	} //else m_AddCond = "";

	if		(m_nSubType == _HOME) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		m_Approach = hold;
		m_Delay = "120S";
		m_TcTime = "120S";
	}else if (m_nSubType == _START) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		m_Approach = hold;
		m_Delay = "120S";
		m_TcTime = "120S";
	}else if (m_nSubType == _ASTART) {
		m_AddCond = m_AddCond + "%B"+ "\n";
	}else if (m_nSubType == _ISTART) {
		m_AddCond = m_AddCond + "%A"+ "\n";
		m_AddCond = m_AddCond + "%C"+ "\n";		// 2012.07.23 무조건 들어가게..
		m_Approach = hold;
		m_Delay = "120S";
	}else if (m_nSubType == _ISHUNT) {
		m_AddCond = m_AddCond + "%A"+ "\n";
		m_AddCond = m_AddCond + "%C"+ "\n";		// 2012.07.23 무조건 들어가게..
		m_TrackClear.RemoveTail();
		m_Occupy = hold;
	}else if (m_nSubType == _OHOME) {
		m_AddCond = m_AddCond + "%H\n %C\n";
		m_Delay = "";
	}else if (m_nSubType == _TOKENLESSC) {
		m_Approach = hold;
		m_Delay = "120S";
		m_TcTime = "120S";
	}else if (m_nSubType == _TOKENLESSG) {
		m_Approach = hold;
		m_Delay = "120S";
		m_TcTime = "120S";
	}else if (m_nSubType == _OC) {
		m_Approach = hold;
		m_Delay = "120S";
		m_TcTime = "120S";
	}else if (m_nSubType == _OG) {
		m_Approach = hold;
		m_Delay = "30S";
		m_TcTime = "120S";
	}else if (m_nSubType == _CALLON) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		CString *stringTemp = (CString*)m_TrackClear.GetTail();
		if (stringTemp->Find('W'))
		    m_TrackClear.RemoveTail();
		m_Occupy = hold;
		m_Time = "30S";
		m_Approach = hold;
		m_Delay = "30S";
		m_TcTime = "120S";
	}else if (m_nSubType == _SHUNT) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		CString *stringTemp = (CString*)m_TrackClear.GetTail();
		if (stringTemp->Find('W'))
		    m_TrackClear.RemoveTail();
		m_Occupy = hold;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	기존 션트신호에서 TRACK CIRCUIT의 TIME부분을 항상 30S로 만드는 것을 출발궤도가 SDG일경우 건너뛰기로함 2012.07.24	////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		int istr_leng = strlen(str);
		CString str_buf = str.Right(istr_leng - 8);		// 앞에 [ROUTE] 삭제
		int iWhereIsSpace = str_buf.Find(' ',0);
		CString str_buf2 = str_buf.Left(iWhereIsSpace);

		if ( str_buf2.Find("F:",0) >= 0 )
		{
			str_buf2 = str_buf.Right(istr_leng - 9 - strlen(str_buf2));		// 앞에서부터 하나씩 읽고 삭제.
			iWhereIsSpace = str_buf2.Find(' ',0);
			str_buf2 = str_buf2.Left(iWhereIsSpace);
		}

		if ( str_buf2.Find("SDG",0) < 0 )
			m_Time = "30S";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		m_Approach = hold;
		m_Delay = "30S";
		m_TcTime = "120S";
	}else if (m_nSubType == _GSHUNT) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		CString *stringTemp = (CString*)m_TrackClear.GetTail();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	단궤도 진로의 경우 TRACK CIRCUIT의 CLEAR부분에 표시 안하던 것을 표시 하기로 함		 2012.07.24						////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		CString str_buffer = str;
		int iNumberOfSpace = 0;

		for( ;str_buffer.Find(' ',0) >= 0; )
		{
			str_buffer = str_buffer.Right(strlen(str_buffer) - str_buffer.Find(' ',0) - 1);
			iNumberOfSpace++;
		}

		if (stringTemp->Find('W') && iNumberOfSpace > 6)
		    m_TrackClear.RemoveTail();
		m_Occupy = hold;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	기존 션트신호에서 TRACK CIRCUIT의 TIME부분을 항상 30S로 만드는 것을 출발궤도가 SDG일경우 건너뛰기로함 2012.07.24	////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		int istr_leng = strlen(str);
		CString str_buf = str.Right(istr_leng - 8);		// 앞에 [ROUTE] 삭제
		int iWhereIsSpace = str_buf.Find(' ',0);
		CString str_buf2 = str_buf.Left(iWhereIsSpace);

		if ( str_buf2.Find("F:",0) >= 0 )
		{
			str_buf2 = str_buf.Right(istr_leng - 9 - strlen(str_buf2));		// 앞에서부터 하나씩 읽고 삭제.
			iWhereIsSpace = str_buf2.Find(' ',0);
			str_buf2 = str_buf2.Left(iWhereIsSpace);
		}

		if ( str_buf2.Find("SDG",0) < 0 )
			m_Time = "30S";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if ( m_iFreeShuntCk != 1 ) m_Approach = hold;
		else m_Approach.Purge();
		m_Delay = "30S";
		m_TcTime = "120S";
	}else if (m_nSubType == _OCALLON) {
		m_AddCond = m_AddCond + "%C"+ "\n";
		m_Occupy = hold;
		m_Time = "30S";
		m_Approach = hold;
		m_Delay = "30S";
		m_TcTime = "120S";
	}

	int iTemp;
	iTemp = pDoc->m_LevelCrossing.GetRealCount();
	//Don't chack levelcrossing at CallOn signal
	if ( m_LevelCross !="" ) {
		pDoc->m_LevelCrossing.CheckAdd(m_LevelCross);
		m_AddCond = m_AddCond + GetLevelCrossName( m_LevelCross );
	}

	if ( iTemp < pDoc->m_LevelCrossing.GetRealCount() ) {
		pDoc->m_StationObject.m_strLC = pDoc->m_StationObject.m_strLC + m_LevelCross + " ";
	}

	m_TrackClear += outt;
	m_Route += maint;
	m_TrackClear += maint;
	m_LockSwitch = point;
	m_CallOnLockSwitch = cpoint;
	m_bValid = TRUE;


	if (m_Button != "") {
		CString sButton = m_Button;
		// 본선 궤도의 경우 UP/DN 을 붙임.
		// 순수하게 숫자로만 구성된 도착버튼.  ex) 12
		BOOL bIsMainTrack = TRUE;
		char *p = (char*)(LPCTSTR)m_Button;
		while (*p) {
			if ( !isdigit(*p) ) {
				bIsMainTrack = FALSE;
				break;
			}
			p++;
		}
		if ( m_pDoc ) 
			m_pDoc->m_StationObject.AddButton( sButton );
	}

	
	m_No = m_Signal;
	m_No = m_No + ":" + m_Button;

	// No Track Circuit  "X?_XT"
	if (firstT != "" && firstT.Find('?')>=0) {
		int l = firstT.Find('?');
		m_RouteName = firstT.Right(firstT.GetLength()-1) + ":";
		m_TrackFromTo = firstT + ":";

	}
	else {
		m_RouteName = firstT + ":";
		m_TrackFromTo = firstT + ":";
	}
	if (lastT != "" && lastT.Find('?')>=0) {
		int l = lastT.Find('?');
		m_RouteName += lastT.Right(lastT.GetLength()-1);
		
//		if ( lastT.Find("W") >= 0 ) {
//			m_TrackFromTo = m_TrackFromTo + "&" + lastT.Right(lastT.GetLength()-1);
//		} else {
		m_TrackFromTo += lastT;
//		}
		int i,iRouteCnt,iTemp,iTempRouteCnt;
		CString strTemp;
		if ( (m_nSubType != _OHOME && m_nSubType != _ASTART) ) {

//			if (( firstT.Find('?') < 0) ) m_TcOCCSeq.CheckAdd(firstT);
			iRouteCnt = m_Route.GetCount();
			iTempRouteCnt = iRouteCnt;

			iTemp = 0;  // 2006.6.19 TC OCCUPIED AND FREED In SEQUENCE 는 출발T + 2개의 트랙으로 한다. -> 기존 과 반대의 개념
			if ( iTemp < 0 ) iTemp = 0;
//			if ( iRouteCnt < 3 ) {

//			if (( firstT.Find('?') < 0 ) ) m_TcOCCSeq.CheckAdd(firstT);
			if (( firstT.Find('?') < 0 ) && m_nSubType != _CALLON && m_nSubType != _SHUNT && m_nSubType != _GSHUNT) m_TcOCCSeq.CheckAdd(firstT);
//			}
//			if ( iRouteCnt > 2 ) iRouteCnt = 2;

			strTemp = *m_Route.GetAtIndex(0);
			m_TcOCCSeq.CheckAdd(strTemp);
			if ( iRouteCnt == 1 ) m_TcOCC_F = " ";/*m_TcOCC_F = *m_Route.GetAtIndex(0);*/
			else if ( iRouteCnt > 1 ) m_TcOCC_F = *m_Route.GetAtIndex(1);

//			if ( iRouteCnt > 1 ) {
				for ( i = iTemp ; i < iRouteCnt ; i++ ) {
					strTemp = *m_Route.GetAtIndex(i);
					m_TcOCCSeq.CheckAdd(strTemp);
				}
//			if ( iTempRouteCnt = 2 ) m_TcOCCSeq.RemoveTail();

//				if ( m_RouteName.Find(":SDG",0) == -1 ) {
					m_TcOCCSeq.RemoveTail();
//				}
//			}  else {
//				m_TcOCCSeq += m_Route;
//				if ( m_RouteName.Find(":SDG",0) == -1 ) {
//					m_TcOCCSeq.RemoveTail();
//				}
//			}
			m_TcOCC = "";
/*
			iTemp = iRouteCnt-3;  // 2006.6.19 TC OCCUPIED AND FREED In SEQUENCE 는 출발T + 2개의 트랙으로 한다. -> 기존 과 반대의 개념
			if ( iTemp < 0 ) iTemp = 0;
			if ( iRouteCnt < 3 ) {
				if (( firstT.Find('?') < 0 ) ) m_TcOCCSeq.CheckAdd(firstT);
			}

			if ( iRouteCnt > 1 ) {
				for ( i = iTemp ; i < iRouteCnt ; i++ ) {
					strTemp = *m_Route.GetAtIndex(i);
					m_TcOCCSeq.CheckAdd(strTemp);
				}
				if ( m_RouteName.Find(":SDG",0) == -1 ) {
					m_TcOCCSeq.RemoveTail();
				}
			}  else {
				m_TcOCCSeq += m_Route;
				if ( m_RouteName.Find(":SDG",0) == -1 ) {
					m_TcOCCSeq.RemoveTail();
				}
			}
			m_TcOCC = "";
*/
		}
	}
	else {
		m_RouteName += lastT;
//		if ( lastT.Find("W") >= 0 ) {
//			m_TrackFromTo = m_TrackFromTo + "&" + lastT;
//		} else {
				m_TrackFromTo += lastT;
//		}

		int i,iRouteCnt,iTemp,iTempRouteCnt;
		CString strTemp;
		if ( (m_nSubType != _OHOME /*&& m_nSubType != _ASTART*/) ) {

			iRouteCnt = m_Route.GetCount();
			iTempRouteCnt = iRouteCnt;

			iTemp = 0;  // 2006.6.19 TC OCCUPIED AND FREED In SEQUENCE 는 출발T + 2개의 트랙으로 한다. -> 기존 과 반대의 개념
			if ( iTemp < 0 ) iTemp = 0;
//			if ( iRouteCnt < 3 ) {
			if (( firstT.Find('?') < 0 ) && m_nSubType != _CALLON && m_nSubType != _SHUNT && m_nSubType != _GSHUNT) m_TcOCCSeq.CheckAdd(firstT);
//			}
//			if ( iRouteCnt > 2 ) iRouteCnt = 3;

			if ( iRouteCnt == 1 ) {
				m_TcOCC_F = *m_Route.GetAtIndex(0);
			} else {
				strTemp = *m_Route.GetAtIndex(0);
				m_TcOCCSeq.CheckAdd(strTemp);
			}
			//if ( iRouteCnt == 1 ) m_TcOCC_F = " ";/*m_TcOCC_F = *m_Route.GetAtIndex(0);*/
			if ( iRouteCnt > 1 ) m_TcOCC_F = *m_Route.GetAtIndex(1);

//			if ( iRouteCnt > 1 ) {
				for ( i = iTemp ; i < iRouteCnt ; i++ ) {
					strTemp = *m_Route.GetAtIndex(i);
					m_TcOCCSeq.CheckAdd(strTemp);
				}
//			if ( iTempRouteCnt == 2 ) m_TcOCCSeq.RemoveTail();

				if ( m_No.Find(":SDG",0) == -1 ) {
					if ( firstT.Find("SDG",0) == -1 || iRouteCnt != 1 || m_Type != "GSHUNT")	//특이 케이스 ( 궤도 1개짜리 진로 ) 2012.07.24
						m_TcOCCSeq.RemoveTail();
				}
//			}  else {
//				m_TcOCCSeq += m_Route;
//				if ( m_RouteName.Find(":SDG",0) == -1 ) {
//					m_TcOCCSeq.RemoveTail();
//				}
//			}
				if ( m_No.Find(":SDG",0) == -1 ) {
					if ((m_RouteName.Find("SDG",0) < 0) || iRouteCnt != 1 || m_Type != "GSHUNT" )		//특이 케이스 ( 궤도 1개짜리 진로 ) 2012.07.24
						m_TcOCC = lastT;
//					else
//						int k = 0;
				}

/*			
			iTemp = iRouteCnt-3;
			if ( iTemp < 0 ) iTemp = 0;
			if ( iRouteCnt < 3 ) {
				if (( firstT.Find('?') < 0 ) ) m_TcOCCSeq.CheckAdd(firstT);
			}
			if ( iRouteCnt > 1 ) {
				for ( i = iTemp ; i < iRouteCnt ; i++ ) {
					strTemp = *m_Route.GetAtIndex(i);
					m_TcOCCSeq.CheckAdd(strTemp);
				}
				if ( m_RouteName.Find(":SDG",0) == -1 ) {
					m_TcOCCSeq.RemoveTail();
				}
			}  else {
				m_TcOCCSeq += m_Route;
				if ( m_RouteName.Find(":SDG",0) == -1 ) {
					m_TcOCCSeq.RemoveTail();
				}
			}
				m_TcOCC = lastT;
*/
		}
	}

	if ( nUsedOverlapSwitch > 0 ) {
		if ( m_TcOCC != "" && m_nSubType != _CALLON && m_nSubType != _SHUNT ) {
//			m_TcOCC = m_TcOCC + "\nAFTER 60S";
			if ( m_TcOCC != "" ) {
				if ((m_pDoc->m_StationObject.m_StationName.Find ( "AKHA",0 ) >= 0 ) || (m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0)) {
					m_OverLap = "^^^^^^" + m_TcOCC + "\n^^^^AFTER^^30";
				} else {
					m_OverLap = "^^^^^^" + m_TcOCC + "\n^^^^AFTER^^60";
				}
			}
			m_TcOCC = m_TcOCC_F;// + "\n^^"+ m_TcOCC + "^^AFTER 30S";
		} else {
			if ( m_TcOCC != "" ) {
				m_OverLap = "^^^^^^" + m_TcOCC + "\n  ";
			}
			m_TcOCC = m_TcOCC_F;// + "\n^^"+ m_TcOCC + "^^AFTER 30S";
		}
	} else {
		if ( m_TcOCC != "" ) {
			m_OverLap = "^^^^^^" + m_TcOCC + "\n  ";
		}
		m_TcOCC = m_TcOCC_F;

	}
	
	m_sNextSignal = sNextSignal;

	Init();

	if (m_pDoc) {
		if ( m_pDoc->m_StationObject.m_Signal.FindIndex( m_Signal ) == -1 )
		m_pDoc->m_StationObject.AddSignal( m_Signal );
		POSITION pos;
		CString* s;\
		pos = ExtSignals.GetHeadPosition(); 
		while(pos != NULL) {
			s = (CString*)ExtSignals.GetNext( pos );
		    if ( m_pDoc->m_StationObject.m_Signal.FindIndex( *s ) == -1 )
			m_pDoc->m_StationObject.AddSignal( *s );
		}
	}

	int TCCnt = m_TrackClear.GetCount();
	CString *strTCTemp;
	CString strTC;
	m_Track.Purge();

	if ( TCCnt > 0 ) {
		for ( k = 0 ; k < TCCnt ; k++ ){
			strTCTemp = (CString *)m_TrackClear.GetAtIndex(k);
			strTC = *strTCTemp;
			m_Track.CheckAdd ( strTC ); 
		}
	}

	if ( FrankTCCnt > 0 ) {
		for ( k = 0 ; k < FrankTCCnt ; k++ ){
			strFrankTC = (CString *)FrankTC.GetAtIndex(k);
			strFTC = *strFrankTC;
			m_Track.CheckAdd( strFTC ); 
		}
	}
	CheckStationException( );
}

CRow::CRow(CString& str, CTableDoc *pDoc)
{
    m_pStrPath = "";
	m_pDoc = pDoc;

	CString token;
	m_bValid = FALSE;
	if (str.Mid(3,7) == "[POINT]") {
		m_nRowType = ROWPOINT;
		m_nSubType = _POINT;
		m_Type = "POINT";
		CParser line(str);
		line.GetToken( token );
		m_cUD = token.GetAt(0);
		line.GetToken( token );	// [POINT]
		line.GetToken( m_Signal );
		line.GetToken( m_Button );		
		
		if		( m_Button == "0"  ) m_Apk = "HPK";
		else if ( m_Button == "1"  ) {
			if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYP",0 ) >= 0 ) {
				m_Apk = "133EPK";
			} else {
				m_Apk = "DEPK";
			}
		} else if ( m_Button == "2"  ) {
			if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYP",0 ) >= 0 ) {
				if ( m_Signal == "131" ) {
					m_Apk = "GEPK";
				} else {
					m_Apk = "132EPK";
				}
			} else {
				m_Apk = "UEPK";
			}
		} else if ( m_Button == "11" ) m_Apk = "AEPK";
		else if ( m_Button == "12" ) m_Apk = "BEPK";
		else if ( m_Button == "13" ) m_Apk = "CEPK";
		else if ( m_Button == "14" ) m_Apk = "DEPK";
		else if ( m_Button == "15" ) m_Apk = "EEPK";
		else if ( m_Button == "16" ) m_Apk = "FEPK";
		else if ( m_Button == "17" ) m_Apk = "GEPK";
		else if ( m_Button == "18" ) m_Apk = "HEPK";
		else if ( m_Button == "19" ) m_Apk = "IEPK";
		else if ( m_Button == "20" ) m_Apk = "JEPK";
		else m_Apk = "?EPK";

		while (line.GetToken( token )) {
			if ( m_Button != "0" ) {
				m_Track.AddTail(token); // omani 아래 추가후 삭제 
			    DelStringMultiNo(token , FALSE);
				m_Track2.CheckAdd(token); // omani 아래 추가후 삭제 
			}
		}
		
		if ( m_Button == "0" ) {
			m_RouteName = "HANDSW"; 
		}
		else if (m_Track.GetCount()==1) {
			m_RouteName = "SINGLE";
		}
		else if (m_Track.GetCount()==2) {
			m_RouteName = "DOUBLE";
		}
		m_bValid = TRUE;
		if (m_pDoc)
			m_pDoc->m_StationObject.AddSwitch( m_Signal );
	}

	if (str.Mid(3,7) == "[CROSS]") {
		m_nRowType = ROWCROSSING;
		m_nSubType = _CROSSING;
		m_Type = "CROSS";
		CParser line(str);
		line.GetToken( token );
		m_cUD = token.GetAt(0);
		line.GetToken( token );
		line.GetToken( m_Signal );

		while (line.GetToken( token )) {
			    if ( token.Find ("HT-",0) == -1 ) m_Track.AddTail(token); // omani 아래 추가후 삭제 
			DelStringMultiNo(token , FALSE);
				if ( token.Find ("HT-",0) == -1 ) m_Track2.CheckAdd(token); // omani 아래 추가후 삭제 
		}
		m_RouteName = "CROSS";		
		m_bValid = TRUE;
		
		if (m_pDoc)
			m_pDoc->m_StationObject.AddCross( m_Signal );
	}

	Init();
}

struct TitleTag {
	char *ttl;
	int w,s,h;
	int GetWidth();
} Title;

int TitleTag::GetWidth() {
	if ( w != -1 ) return w;
	TitleTag *pStruc = this;
	int nLevel = s + 1;
	int nSubWidth = 0;
	pStruc++;
	while ( pStruc->s >= nLevel ) {
		if ( pStruc->s == nLevel )
			nSubWidth += pStruc->GetWidth();
		pStruc++;
	}
	return nSubWidth;
}

TitleTag TableTitle[] = {
	{"ROUTE",						-1,												0, 1},
	{"NO",							W_NUMBER,										1, 3},
	{"TYPE",						W_TYPE,											1, 3},
	{"NAME",						W_NO,											1, 3},
	{"TRACK\nCIRCUIT",				W_NAME,											1, 2},
	{"FROM:TO",						W_NAME,											3, 1},
	{"SIGNAL",						W_SIGNAL + W_BUTTON,							1, 2},
	{" START ",						W_SIGNAL,										3, 1},
	{"END",							W_BUTTON,										3, 1},

	{"ROUTE PROHIBIT",				W_LOCK,											0, 4},

	{"POINT SET,LOCKED AND DETECTED\n[OVERLAP]",	W_POINTLOCK,							0, 4},

	{"SIGNAL CONTROL",				-1,												0, 1},
	{"SIGNAL\nASPECT",				W_SASPECT,										1, 3},
	{"AHEAD\nSIGNAL",				W_AHEADSIG,										1, 3},

	{"TRACK CIRCUIT",				-1,												0, 1},
	{"CLEAR",						W_TRACK,										1, 3},
	{"OCCUPIED",					W_OCCUPY,										1, 3},
	{"TIME\n(Sec)",					W_TIME,											1, 3},

	{"APPROACH LOCKING",			-1,												0, 1},
	{"APPROACH\nTRACK",				W_HOLD,											1, 3},
	{"CANCEL\nTIME\n(Sec)",			W_DELAY,										1, 3},

	{"ROUTE RELEASE",			    -1,   											0, 1},
//	{"NORMALIZATION INITIATED",								W_TCOCCSEQ+W_TCOCC, 	1, 1},
	{"TC OCCUPIED AND\nCLEARED IN\nSEQUENCE",W_TCOCCSEQ,							1, 3},
//	{"TC\nOCCUPIED",	  W_TCOCC,													2, 2},
//	{"RELEASED",		 W_OVLAP+W_TCTIME, 	1, 1},
	{"TC OCCUPIED\n(OVERLAP\nRELEASE)\n(Sec)",W_OVLAP,								1, 3},
	{"TIME AFTER\nEMERGENCY\nRELEASE\n(Sec)",W_TCTIME,								1, 3},
	{"\nADDITIONAL\nCONDITION\n ",		W_ADDCOND ,									0, 4},
	{"\nROUTE\nNAME\n ",					W_NO ,									0, 4},
	{"", -1, -1, 0 }
};

TitleTag TableTitleSwitch[] = {
	{"POINT",						W_PTYPE,										0, 4},

	{"LOCKED BY",					-1,   											0, 2},
	{"TC OCCUPIED",					W_PNO,											1, 2},
	{"ROUTE SET",					W_PLOCK,										1, 2},
	{"KEY TRANSMITTER",				W_PKEY,											1, 2},
	{"", -1, -1, 0 }
};

TitleTag TableTitleCross[] = {
	{"LEVEL CROSS",					W_PTYPE,										0, 4},

	{"LOCKED BY",					-1,   											0, 2},
	{"TC OCCUPIED",					W_PNO,											1, 2},
	{"ROUTE SET",					W_PLOCK + W_PKEY,								1, 2},
	{"", -1, -1, 0 }
};

void CRow::DrawHeader(CDC *pDC, CPoint p)
{
	CCommonCell ttl;
	CString str;
	int size = 0;
	TitleTag *pTag;
	
	if ( m_nRowType == ROWPOINT ) {
		size = sizeof(TableTitleSwitch) / sizeof(TitleTag) - 1;
		pTag = TableTitleSwitch;
	}
	else if ( m_nRowType == ROWCROSSING ) {
		size = sizeof(TableTitleCross) / sizeof(TitleTag) - 1;
		pTag = TableTitleCross;
	}
	else if ( m_nRowType == ROWSIGNAL ) {
		size = sizeof(TableTitle) / sizeof(TitleTag) - 1;
		pTag = TableTitle;
	}
	CPoint pPos[10];
	CCell::SetCurrent(p);
	for (int i=0; i<size; i++) {
		TitleTag *t = &pTag[i];
		str = "@B";
		str += t->ttl;
		ttl.SetData(&str);
		ttl.m_Size.cy = t->h;
		int w = t->GetWidth();
		ttl.SetWidth(w);
		CPoint temp = CCell::m_Current;
		ttl.LocateRight();
		if ( t->s < (t+1)->s ) {	// up level
			pPos[t->s] = CCell::m_Current;
			CCell::m_Current = temp; 
			ttl.LocateDown();
		}
		else if ( t->s > (t+1)->s ) {	// down level
			CCell::m_Current = pPos[(t+1)->s];
		}
		else {
		}
		ttl.Draw((CDC*)pDC);
	}
}


void CRow::Draw(CDC *_pDC, BOOL viewall)
{
	CDC *pDC = (CDC*)_pDC;

	for ( int i=0; i<m_nMaxCells; i++ ) {
		CCell *pCell = m_pCell[i];
		pCell->Draw(pDC , viewall);
	}
	int h = GetMaxRows();
	if ( m_iHeight < h )	IncHeight( h - m_iHeight );
}

BOOL _cmp(char* p1, char* p2) {
	while (*p1 && *p2) {
		if (isdigit(*p1) && isdigit(*p2)) {
				int n1, n2;
				n1 = atoi(p1);
				n2 = atoi(p2);
				if (n1 != n2) return n1 < n2;
		} else if (*p1 != *p2) return *p1 < *p2;
		p1++;
		p2++;
	}
	return *p2;
}

BOOL operator < (CRow &a, CRow &b) {
	BOOL result = TRUE;

	int n1, n2;

	n1 = HIDX(a.m_Type);
	n2 = HIDX(b.m_Type);
	
	if ( a.m_nRowType != b.m_nRowType ) {
		return a.m_nRowType < b.m_nRowType;
	}

	if ( a.m_cUD == 'D' && b.m_cUD == 'U' ) return FALSE ;
	if ( a.m_cUD == 'U' && b.m_cUD == 'D' ) return TRUE ;

	if (a.m_Signal.GetLength() < b.m_Signal.GetLength() ) return TRUE;
	else if ( a.m_Signal.GetLength() == b.m_Signal.GetLength() ){
		if (a.m_Signal < b.m_Signal ) return TRUE;
		else if ( a.m_Signal == b.m_Signal ) {
			if ( n2 > n1 ) return TRUE;
			else if (a.m_nLineNo < b.m_nLineNo ) return TRUE;
			else return FALSE; 
		} else return FALSE;
	} 
	else return FALSE;

}

int CRow::NR()
{
	char c = m_Signal.GetAt(0);
	if (m_nSubType == _POINT || m_nSubType == _SHUNT || m_nSubType == _ASTART) {
		int n = atoi(m_Signal);
		// 3자리 신호기명 처리
		if (n>100) n -= 100;
		if (n < 51) return 0;
		else return 1;
	}
	else {
		// 주신호의 경우
		// 1,3 -> 0		2,4 -> 1
		if (isalpha(c)) c = m_RouteName.GetAt(0);
		// 3자리 신호기명 처리
		// ORG	if (isdigit(m_Signal.GetAt(1))) c = m_Signal.GetAt(1);
		if (c & 1) return 0;
		else return 1;
	}
	return 0;
}

BOOL CRow::CheckReferanceItem( CString &Item, CRow::RowItemType itemType, BOOL bInChecked )
{
	BOOL bRet = FALSE;
	int  nFind = -1;

	switch ( itemType ) {
	case LOCKSWITCH:
		nFind = m_LockSwitch.FindIndex( Item );
		break;
	case LOCKSIGNAL:
		nFind = m_LockSignal.FindIndex( Item );
		break;
	case LOCKTRACK:
		nFind = m_Track.FindIndex( Item );
		break;
	case FREEROUTE:
		nFind = m_Route.FindIndex( Item );
		break;
	case APPROACHTRACK:
		nFind = m_Approach.FindIndex( Item );
		break;
	case APPROACHTIME:
	default:
		break;
	}
	if ( (bInChecked && (nFind >= 0)) || (!bInChecked && (nFind < 0)) ) 
		bRet = TRUE;

	return bRet;
}

CString CRow::GetSwitchNameFromNR( CString &str ) 
{  // Allow Long Switch Name  전철기 명에서 +&N[12] 와 같은경우 문자를 다 제거 하고 12 만 리턴 한다. 
	int iFirst, iLast;
	iFirst = str.Find("[",0);
	iLast  = str.Find("]",0);
	if ( iFirst > -1 ) iFirst = iFirst + 1;
	if ( ( iLast > -1)  && ( iFirst < iLast ) ) iLast = iLast - iFirst; 
	return str.Mid(iFirst,iLast);
}

BOOL CRow::CheckDestTrack(CRow *row)
{
	CString stt, stt1;
	CString temp1, temp2 , temp3, temp4;
	int  itemp1, itemp3;

	stt1 = row->m_TrackFromTo;
	stt  = m_TrackFromTo;

	itemp1 = stt.Find(":",0);
	itemp3 = stt1.Find(":",0);

	temp2 = stt.Mid(itemp1+1,stt.GetLength());
	temp4 = stt1.Mid(itemp3+1,stt1.GetLength());

	if ( temp2 == temp4 ) {
		if ( ( row->m_Type == "SHUNT" || row->m_Type == "CALLON" ) && ( m_Type == "SHUNT" || m_Type == "CALLON" ) ) {
			if ( row->m_cUD == m_cUD ) {
				return FALSE;
			}
			else {
				return TRUE;
			}
		} else {
			return FALSE;
		}
	}
	return FALSE;
}

BOOL CRow::CheckContinueSignal(CRow *row)
{
	int a = 0;
	if ( m_No =="8:2(C)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="2:A1(M)"){
			a++;
		}
	}
	
	CString stt, stt1;
	CString temp1, temp2 , temp3, temp4;
	int  itemp1, itemp2, itemp3, itemp4;

	stt1 = row->m_No;
	stt  = m_No;

	itemp1 = stt.Find(":",0);
	itemp2 = stt.Find("(",0);
	itemp3 = stt1.Find(":",0);
	itemp4 = stt1.Find("(",0);

	temp1 = stt.Mid(0,itemp1);
	temp2 = stt.Mid(itemp1+1,itemp2-(itemp1+1));
	temp3 = stt1.Mid(0,itemp3);
	temp4 = stt1.Mid(itemp3+1,itemp4-(itemp3+1));

	// 아카우라 바이패스 역의 58 신호기를 58L,58R 로 표시하면서 연속된 진로를 찾지 못하기 때문에
	// 연속된 진로를 찾을때는  58L 과 58R 을 58로 바꾸어 찾는다.
	if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
		if ( temp1 == "58L" || temp1 == "58R" ) temp1 = "58";
		if ( temp2 == "58L" || temp2 == "58R" ) temp2 = "58";
		if ( temp3 == "58L" || temp3 == "58R" ) temp3 = "58";
		if ( temp4 == "58L" || temp4 == "58R" ) temp4 = "58";
	}

	if (( ( stt1.Find("(M)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) ) 
	   || ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(M)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(M)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(M)",0) > 0 ) && ( row->m_cUD == m_cUD) )){ // 둘다 메인 진로인 경우 
		if ( temp2 == temp3 ) {
			return FALSE;
		}
		if ( temp1 == temp4 ) {
			return FALSE;
		}
	}
	return TRUE;

}

BOOL CRow::CheckContinueShuntSignal(CRow *row) // 사이스타 간지 42-8 , 8-4T 진로를 위하여.....
{
	int a = 0;
	if ( m_No =="A1:1(C)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="1:51(S)"){
			a++;
		}
	}
	
	CString stt, stt1;
	CString temp1, temp2 , temp3, temp4;
	int  itemp1, itemp2, itemp3, itemp4;

	stt1 = row->m_No;
	stt  = m_No;

	itemp1 = stt.Find(":",0);
	itemp2 = stt.Find("(",0);
	itemp3 = stt1.Find(":",0);
	itemp4 = stt1.Find("(",0);

	temp1 = stt.Mid(0,itemp1);
	temp2 = stt.Mid(itemp1+1,itemp2-(itemp1+1));
	temp3 = stt1.Mid(0,itemp3);
	temp4 = stt1.Mid(itemp3+1,itemp4-(itemp3+1));

	if(   ( ( stt1.Find("(M)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) ) 
	   || ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(M)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(M)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(M)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) ) // 2007.09.14 일 아카우라 A1:1(C)-6 번과 1:SDG3 가 나지 않는 문제 때문에 주석처리함 콜온,콜온,션트,션트 가능하도록
	   || ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) ) // 2007.09.14 일 아카우라 A1:1(C)-6 번과 1:SDG3 가 나지 않는 문제 때문에 주석처리함 콜온,콜온,션트,션트 가능하도록
		){ // 둘다 메인 진로인 경우 
		if ( temp2 == temp3 ) {
			return FALSE;
		}
		if ( temp1 == temp4 ) {
			return FALSE;
		}
	}
	return TRUE;

}

BOOL CRow::CheckContinue(CRow *row)
{
	int a = 0;
	if ( m_No =="1:49(M)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="45:53(M)"){
			a++;
		}
	}
	if ( m_No =="45:53(M)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="1:49(M)"){
			a++;
		}
	}
	
	CString stt, stt1, stb1;
	CString temp1, temp2 , temp3, temp4, temp5;
	int  itemp1, itemp2, itemp3, itemp4, itemp5;

	stt1 = row->m_No;
	stt  = m_No;
	stb1 = row->m_Button;

	itemp1 = stt.Find(":",0);
	itemp2 = stt.Find("(",0);
	itemp3 = stt1.Find(":",0);
	itemp4 = stt1.Find("(",0);
	itemp5 = stb1.Find("(",0);

	temp1 = stt.Mid(0,itemp1);
	temp2 = stt.Mid(itemp1+1,itemp2-(itemp1+1));
	temp3 = stt1.Mid(0,itemp3);
	temp4 = stt1.Mid(itemp3+1,itemp4-(itemp3+1));
	temp5 = stb1.Left(itemp5);

	// 아카우라 바이패스 역의 58 신호기를 58L,58R 로 표시하면서 연속된 진로를 찾지 못하기 때문에
	// 연속된 진로를 찾을때는  58L 과 58R 을 58로 바꾸어 찾는다.
	if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
		if ( temp1 == "58L" || temp1 == "58R" ) temp1 = "58";
		if ( temp2 == "58L" || temp2 == "58R" ) temp2 = "58";
		if ( temp3 == "58L" || temp3 == "58R" ) temp3 = "58";
		if ( temp4 == "58L" || temp4 == "58R" ) temp4 = "58";
	}

/*
	if ( m_pDoc->m_StationObject.m_StationName.Find ( "KULAURA",0 ) > 0 ) 
	{
		if ( (m_No.Find(":49(M)",0 ) > 0) && (row->m_No == "45:53(M)") )
		{
			temp3 = "49";
		}
		if ( (m_No.Find(":47(M)",0 ) > 0) && (row->m_No == "45:53(M)") )
		{
			temp3 = "47";
		}
		if ( (m_No.Find(":51(M)",0 ) > 0) && (row->m_No == "45:53(M)") )
		{
			temp3 = "51";
		}

		if ( (m_No =="45:53(M)") && ( (stt1.Mid(1,6) == ":47(M)") || (stt1.Mid(1,6) == ":49(M)") || (stt1.Mid(1,6) == ":51(M)") ) )
		{
			temp5 = "45";
		}

	}
*/

	if ( CheckPoint( row ) == FALSE ) {
		return TRUE;
	}

	if (( ( stt1.Find("(M)",0) > 0 ) && ( stt.Find("(M)",0) > 0 ) && ( row->m_cUD == m_cUD) ) 
       // 2005 년 4월 29 일 콜온 - 콜온 콜온 - 션트 진로는 동시에 나지 못한다. 
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) ) // 2007.09.14 일 아카우라 A1:1(C)-6 번과 1:SDG3 가 나지 않는 문제 때문에 주석처리함
	   || ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(C)",0) > 0 ) && ( row->m_cUD == m_cUD) )
	   || ( ( stt1.Find("(C)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) ) ){
	   //|| ( ( stt1.Find("(S)",0) > 0 ) && ( stt.Find("(S)",0) > 0 ) && ( row->m_cUD == m_cUD) )){ // 둘다 메인 진로인 경우 
		if ( temp2 == temp3 ) {
			return FALSE;
		}
		if ( temp1 == temp4 || temp1 == temp5 ) {
			return FALSE;
		}
	}
	return TRUE;

}

BOOL CRow::CheckFrank(CRow *row)
{
	return FALSE;

	int a = 0;
	if ( m_No =="A1:1(C)-1"){  // 브레이크 테스트용 코드
		if ( row->m_No =="42:6(C)"){
			a++;
		}
	}

	CMyStrList* rsw = &(row->m_TrackClear);
	BOOL find;
	POSITION pos, pos1;
	CString *str, *str1;
	char* s, * s1;

	find = FALSE;
	for( pos = rsw->GetHeadPosition(); pos != NULL; )
	{
	    str = (CString*)rsw->GetNext( pos );
		s = (char*)(LPCTSTR)*str;
		if (*s == '+') s++;
		if (*s != '#' && *s != '$') continue;
		if (*s == '#' || *s == '$') s++;

		for( pos1 = m_TrackClear.GetHeadPosition(); pos1 != NULL; )
		{
			str1 = (CString*)m_TrackClear.GetNext( pos1 );
			s1 = (char*)(LPCTSTR)*str1;
			if (*s1 == '+') s1++;
			if (*s1 == '#' || *s1 == '$') s1++;
			if (!strcmp(s,s1)) {
				if ( CheckOverlap( row , str1 ) == TRUE )
				find = TRUE;
				break;
			}
		}
		if (find) break;
	}
	return find;
}

BOOL CRow::CheckOverlap(CRow *row , CString* strTrack )
{
	int a = 0;
	if ( m_No =="42:8(C)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="1:45(C)"){
			a++;
		}
	}

	POSITION pos , pos1;
	CString *str, *str1 , stt , stt2;
	CString tr1,tr2;
	CString strStt1,strStt2,strStt3;  // 전철기가 여러개 있는 궤도용
	char* s, * s1;
	int  iFind = 0;
	int  iFind1 = 0;
	int  iFind2 = 0;
	int  iFindCheck = 0;
    // 22 번전철기도 21 번 트랙에 포함되면서 이부분 로직에 문제 있음.....
	// 확인 .... 필수...


	// OUTER CALLON 에 프랭크 없음
	if ( row->m_Type == "OCALLON" ) return FALSE;
	// 메인진로는 전철기 조건에 의함
	stt2 = row->m_No;
	if (stt2.Find("M",0) >=0 ) return FALSE;
	// 진로의 방향이 같으면 프랭크 아님
    //if ( row->m_cUD == m_cUD ) return FALSE;
	// 전철기가 있는 궤도가 아니면 프랭크 아님 
	tr1 ="";
	tr2 ="";
	stt = *strTrack;
	iFind1 = strTrack->Find("W",0);
	if ( iFind1 < 0 ) return FALSE;
	stt = stt.Mid(strTrack->Find("W",0)+1, strTrack->Find(".",0) - strTrack->Find("W",0)-2 );

	// 메인 진로의 경우 전철기 조건을 따름 
	stt2 = m_No;
	if ( stt.GetLength() > 2 ) {
		if ( stt.GetLength() > 5 ) strStt3 = stt.Mid(4,2);
		if ( stt.GetLength() > 3 ) strStt2 = stt.Mid(2,2);
		if ( stt.GetLength() > 1 ) strStt1 = stt.Mid(0,2);
	}
	else strStt1 = stt;
	//if (stt2.Find("M",0) >=0 ) {
	//	if ( CheckFrankPoint ( row , stt) == TRUE ) return FALSE;
	//}
	if ( stt.GetLength() > 2 ) {
		if ( stt.GetLength() > 5 ) if ( CheckFrankPoint ( row , strStt3) == TRUE ) iFindCheck = TRUE;
		if ( stt.GetLength() > 3 ) if ( CheckFrankPoint ( row , strStt2) == TRUE ) iFindCheck = TRUE;
		if ( stt.GetLength() > 1 ) if ( CheckFrankPoint ( row , strStt1) == TRUE ) iFindCheck = TRUE;
		if ( iFindCheck == TRUE ) return FALSE;
	} else {
		if ( CheckFrankPoint ( row , strStt1) == TRUE ) return FALSE;
	}
	
	// 같은이름 전철기의 방향을 확인해서 프랭크 인지 체크 한다. 
	CMyStrList* rsw = &(row->m_CallOnLockSwitch);

	for( pos = m_LockSwitch.GetHeadPosition(); pos != NULL; )
	{
		// 전철기 방향 검색 
		str1 = (CString*)m_LockSwitch.GetNext( pos );
		s1 = (char*)(LPCTSTR)*str1;
		if (*s1 == '+') s1++;
		// OUTER CALLON 의 경우 오버랩 진로 까지 확인 그외의 경우는 메인을 관통할때만 
		if ( m_Type != "OCALLON" ) if (*s1 == '&' || *s1 == '*' ) continue;
		if (*s1 == '&' || *s1 == '*' ) s1++;
//		if ( GetSwitchNameFromNR( *str1 ) == stt ) tr1 = s1;
		tr1 = s1;
		for ( pos1 = rsw->GetHeadPosition(); pos1 != NULL; )
		{
			// 전철기 방향 검색 
			str = (CString*)rsw->GetNext( pos1 );
			s = (char*)(LPCTSTR)*str;
			if (*s == '+') s++;
			if (*s != '&' && *s != '*') continue;
			if (*s == '&' || *s == '*') s++;
//			if ( GetSwitchNameFromNR( *str ) == stt ) tr2 = s;
			if ( GetSwitchNameFromNR( *str ) == GetSwitchNameFromNR( *str1 ) ) {
				tr2 = s;
				if ( tr1.Left(1) == "R" && tr2.Left(1) == "N" ) return TRUE;
			}
		}
		
	}
/*
	for ( pos1 = rsw->GetHeadPosition(); pos1 != NULL; )
	{
		// 전철기 방향 검색 
		str = (CString*)rsw->GetNext( pos1 );
		s = (char*)(LPCTSTR)*str;
		if (*s == '+') s++;
		if (*s != '&' && *s != '*') continue;
		if (*s == '&' || *s == '*') s++;
		if ( GetSwitchNameFromNR( *str ) == stt ) tr2 = s;
	}
*/
	if ( tr1.Left(1) == "R" && tr2.Left(1) == "N" ) return TRUE;
	else return FALSE;
}

BOOL CRow::CheckFrankPoint(CRow *row , CString strCheckPoint )
{
	return FALSE;

	int a = 0;
	if ( m_No =="42:8(C)"){  // 브레이크 테스트용 코드
		if ( row->m_No =="1:45(C)"){
			a++;
		}
	}

	CMyStrList* rsw = &(row->m_CallonTempTrack);
	BOOL find;
	POSITION pos, pos1;
	CString *str, *str1;
	char* s, * s1;

	find = FALSE;
	for( pos = rsw->GetHeadPosition(); pos != NULL; )
	{
	    str = (CString*)rsw->GetNext( pos );
		s = (char*)(LPCTSTR)*str;
		if (*s == '+') s++;
//		if (*s == '&' || *s == '*') continue;
		if (*s == '#' || *s == '$') continue;
		for( pos1 = m_CallonTempTrack.GetHeadPosition(); pos1 != NULL; )
		{
			str1 = (CString*)m_CallonTempTrack.GetNext( pos1 );
			s1 = (char*)(LPCTSTR)*str1;
			if (*s1 == '+') s1++;
//			if (*s1 == '&' || *s1 == '*') s1++;
			if (*s1 == '#' || *s1 == '$') s1++;
			if (!strcmp(s,s1)) {
				find = TRUE;
				break;
			}
		}
		if (find) break;
	}
	if (!find) return FALSE;
	return TRUE;
}

BOOL CRow::CheckPoint( CRow *row )
{
	POSITION pos , pos1;
	CString *str, *str1, stt;
	char* s, * s1;
	int  iPointCnt1 = 0;
	int  iPointCnt2 = 0;
	BYTE  iFind = 0;
	BYTE  iFind1, iFind2;
	CMyStrList* rsw = &(row->m_CallOnLockSwitch);

	for( pos = m_LockSwitch.GetHeadPosition(); pos != NULL; )
	{
		iPointCnt1++;
		str1 = (CString*)m_LockSwitch.GetNext( pos );
		s1 = (char*)(LPCTSTR)*str1;

		iFind1=0;
		if (*s1 == '&' || *s1 == '*') 
		{
			iFind1 = 1;
			s1++;

			if ( m_Type == "CALLON" && row->m_Type == "CALLON" ) {
				if ( iFind == 0 ) iFind = 1;
				continue;
			}

		}

		for ( pos1 = rsw->GetHeadPosition(); pos1 != NULL; )
		{
			iPointCnt2++;
			str = (CString*)rsw->GetNext( pos1 );
			s = (char*)(LPCTSTR)*str;
			stt = row->m_No;
			iFind2=0;
			if (*s == '&' || *s == '*')  
			{
				iFind2 = 1;

				s++;
				
				if ( (m_Type == "START" || m_Type == "CALLON" || m_Type == "SHUNT") && row->m_Type == "CALLON" ) 
				{						
					if ( iFind == 0 ) iFind = 1;
					continue;
				}
				
			}

			if (  GetSwitchNameFromNR( *str ) == GetSwitchNameFromNR( *str1 )   )
			{ 				
				if (!strcmp(s,s1))  // 방향이 같으면....
				{
					if ( iFind == 0 ) iFind = 1;
				} 
				else 
				{
					iFind = 2;
					break;
				}
			} else {

			}
		}
	}


//	if ( iFind != 1 ) return FALSE;
	if ( iFind == 2 ) return FALSE;
	else {
		return TRUE;
	}
}

BOOL CRow::CheckOverlapTrack(CRow *row)
{
	CMyStrList* rsw = &(row->m_CallonTempTrack);
	BOOL find;

	POSITION pos, pos1;
	CString *str, *str1;
	char* s, * s1;

	find = FALSE;
	for( pos = rsw->GetHeadPosition(); pos != NULL; )
	{
	    str = (CString*)rsw->GetNext( pos );
		s = (char*)(LPCTSTR)*str;
		if (*s == '+') s++;
		if (*s == '#' || *s == '$') {
			s++;
		}
//		else continue; // 오버랩과 오버랩이 겹치는 경우도 대항진로로 취급한다. 

		for( pos1 = m_CallonTempTrack.GetHeadPosition(); pos1 != NULL; )
		{
			str1 = (CString*)m_CallonTempTrack.GetNext( pos1 );
			s1 = (char*)(LPCTSTR)*str1;
			if (*s1 == '+') s1++;
			if (*s1 == '#' || *s1 == '$') {
				s1++;
//				continue; // 오버랩과 오버랩이 겹치는 경우도 대항진로로 취급한다.
			} 

			if (!strcmp(s,s1)) {
				find = TRUE;
				break;
			}
		}
		if (find) break;
	}
	if (!find) return FALSE;
	return TRUE;
}

BOOL CRow::CheckStartShareStartTrack(CRow *row)
{
	CString strTrackFrom1,strTrackFrom2;
	if ( ( row->m_Type == "HOME" || row->m_Type == "ASTART" ) &&  ( m_Type == "HOME" || m_Type == "ASTART" )) {
		strTrackFrom1 = row->m_TrackFromTo;
		strTrackFrom2 = m_TrackFromTo;
		if ( strTrackFrom1.Find(":",0) > 0 ) strTrackFrom1 = strTrackFrom1.Left(strTrackFrom1.Find(":",0));
		if ( strTrackFrom2.Find(":",0) > 0 ) strTrackFrom2 = strTrackFrom2.Left(strTrackFrom2.Find(":",0));
		if ( strTrackFrom1 == strTrackFrom2 ) return TRUE;
		else return FALSE;
	} else {
		return FALSE;
	}
}

BOOL CRow::CheckStationException( CRow *row )
{
	CString strTemp;
	CString* pstrTemp;
	POSITION pos;

	if ( m_pDoc->m_StationObject.m_StationName.Find ( "KUL",0 ) >= 0 ) {

		if ( m_No == "42:16(S)" ) {
			m_Aspect = "S";
			m_Aspect_Table.Replace ( "+M","" );
			return TRUE;
		}
		if ( row != NULL ) {
			if ( m_No.Find(":53(M)") > 0 && row->m_No == "45:53(M)" && row != NULL )  return TRUE;
			if ( row->m_No.Find(":53(M)") > 0 && m_No == "45:53(M)" && row != NULL )  return TRUE;	
			if ( m_No.Find("47(M)") > 0 && __editdev21 !="1" && row->m_No == "45:53(S)" && row != NULL ) {
				row->m_LockSignal.CheckAdd(m_No);
				m_LockSignal.CheckAdd(row->m_No);
				return TRUE;
			}
			if ( m_No.Find("49(M)") > 0 && __editdev21 !="1" && row->m_No == "45:53(S)" && row != NULL ) {
				row->m_LockSignal.CheckAdd(m_No);
				m_LockSignal.CheckAdd(row->m_No);
				return TRUE;
			}
			if ( m_No.Find("51(M)") > 0 && __editdev21 !="1" && row->m_No == "45:53(S)" && row != NULL ) {
				row->m_LockSignal.CheckAdd(m_No);
				m_LockSignal.CheckAdd(row->m_No);
				return TRUE;

			}
		}
	}

	if ( m_pDoc->m_StationObject.m_StationName.Find ( "SYL",0 ) >= 0 ) {
		if ( m_No == "1:55(S)" ) {
			m_Aspect = "S";
			m_Aspect_Table.Replace ( "+M","" );
			return TRUE;
		}
		if ( m_No == "42:44(S)" ) {
			m_Aspect = "S";
			m_Aspect_Table.Replace ( "+M","" );
			return TRUE;
		}
		if ( m_No == "18:2(S)" ) {
			m_AheadSignal = "2 AT G";
			m_AheadSignal_Table = "2 AT G*";
			return TRUE;
		}
		for( pos = m_Track.GetHeadPosition(); pos != NULL; )
		{
			pstrTemp = (CString*)m_Track.GetNext( pos );
			strTemp = *pstrTemp;
			if ( strTemp.Find ( "=W3536T_AT_") >= 0 ) {
				strTemp = "=W3536T";
				m_Track.GetPrev( pos );
				m_Track.RemoveAt(pos);
				m_Track.AddTail( strTemp );
			}
		}
		for( pos = m_FrankTrack.GetHeadPosition(); pos != NULL; )
		{
			pstrTemp = (CString*)m_FrankTrack.GetNext( pos );
			strTemp = *pstrTemp;
			if ( strTemp.Find ( "=W3536T_AT_") >= 0 ) {
				strTemp = "=W3536T";
				m_Track.GetPrev( pos );
				m_FrankTrack.RemoveAt(pos);
				if ((m_pDoc->m_StationObject.m_StationName.Find ( "AKHA",0 ) >= 0 ) && (strTemp.Find("W131T") >=0 )) { // 알롬 요구로 수정 2007.03.06
				} else {
					m_FrankTrack.AddTail( strTemp );
				}
			}
		}
	}

	if ( m_pDoc->m_StationObject.m_StationName.Find ( "AZA",0 ) >= 0 ) {
		if ( m_No == "1:41(M)-1" ) {
			m_Aspect = "Y+LI(M)";
			m_Aspect_Table = "Y+M";
			m_AheadSignal = "41 AT R OR Y+LI(L)";
			m_AheadSignal_Table = "41 AT R OR Y+L";
			return TRUE;
		}
	}

	if ( m_pDoc->m_StationObject.m_StationName.Find ( "AKHA",0 ) >= 0 ) {
		if ( m_No == "6:2(S)" ) {
			m_AheadSignal = "2 AT G";
			m_AheadSignal_Table = "2 AT G*";
			strTemp = "B2";
			m_BlockSignal=strTemp;

//			return TRUE;
		}
		if ( m_No == "49:71(S)" ) {
			m_AheadSignal = "71 AT G";
			m_AheadSignal_Table = "71 AT G*";
			return TRUE;
		}
		if ( m_No.Find("63(M)",0) > 0 ) {
			m_AheadSignal = "63 AT Y OR G";
			m_AheadSignal_Table = "63 AT Y OR G";
			if ( m_No == "51:63(M)" ) {
				strTemp = "52:16(M)";
				m_LockSignal.CheckAddTail(strTemp);
				return TRUE;
			}
			return TRUE;
		}

//		if ( m_No.Find("1:SDG3(C)",0) >= 0 ) {
//			m_LockSignal.RemoveAtIndex(15);
//		}

//		if ( m_No.Find("1:SDG3(S)",0) >= 0 ) {
//			m_LockSignal.RemoveAtIndex(15);
//		}

//		if ( m_No.Find("A1:1(C)-6",0) >= 0 ) {
//			m_LockSignal.RemoveAtIndex(17);
//			m_LockSignal.RemoveAtIndex(17);
//		}

		if ( m_No.Find("61:63(S)",0) > 0 ) {
			m_AheadSignal = "63 AT Y OR G";
			m_AheadSignal_Table = "";
			return TRUE;
		}
		if ( m_No == "50:SDG6(S)" ) {
			m_sNextSignal = "";
			m_DependSignal = "";
			return TRUE;
		}
		if ( m_No == "50:SDG7(S)" ) {
			m_sNextSignal = "";
			m_DependSignal = "";
			return TRUE;
		}


		if ( m_No == "52:16(M)" ) {
			strTemp = "51:63(M)";
			m_LockSignal.CheckAddTail(strTemp);
			m_Aspect = "Y+LI(M)";
			m_Aspect_Table = "Y+M";
			return TRUE;
		}


		if ( m_No == "54:18(M)" ) {
			m_Aspect = "Y+LI(M)";
			m_Aspect_Table = "Y+M";
			return TRUE;
		}

		if ( m_No == "54:18(M)-1" ) {
			m_Aspect = "Y+LI(M)";
			m_Aspect_Table = "Y+M";
			return TRUE;
		}

		if ( m_No != "1:SDG3(C)" && m_No !="1:SDG3(S)" && m_No !="4:2(M)" && m_No !="4:2(S)") {
			for( pos = m_Track.GetHeadPosition(); pos != NULL; )
			{
				pstrTemp = (CString*)m_Track.GetNext( pos );
				strTemp = *pstrTemp;
				if ( strTemp.Find ( "=W103104110T") >= 0 ) {
					strTemp = "=W103104110T_AT_N[103]";
					if ( pos != 0x00 ) { // 제일 마지막 엘리먼트의 POS 에는 0x00 이 들어간다.
						m_Track.GetPrev( pos );
						m_Track.RemoveAt(pos);
					} else {
						m_Track.RemoveTail();
					}

					m_Track.AddTail( strTemp );
				}
			}
		}

		if ( m_No != "1:SDG3(C)" && m_No !="1:SDG3(S)" && m_No !="4:2(M)" && m_No !="4:2(S)" ) {
			for( pos = m_FrankTrack.GetHeadPosition(); pos != NULL; )
			{
				pstrTemp = (CString*)m_FrankTrack.GetNext( pos );
				strTemp = *pstrTemp;
				if ( strTemp.Find ( "=W103104110T") >= 0 ) {
					strTemp = "=W103104110T_AT_N[103]";
					if ( pos != 0x00 ) { // 제일 마지막 엘리먼트의 POS 에는 0x00 이 들어간다.
						m_FrankTrack.GetPrev( pos );
						m_FrankTrack.RemoveAt(pos);
					} else {
						m_FrankTrack.RemoveTail();
					}

					m_FrankTrack.AddTail( strTemp );
				}
			}
		}
	}
/*	if ( m_pDoc->m_StationObject.m_StationName.Find ( "BHAIRAB",0 ) >= 0 )
	{
		if ( m_No.Find ( "3:47(M)",0 ) >= 0 ) 
		{
			for( pos = m_FrankTrack.GetHeadPosition(); pos != NULL; )
			{
				pstrTemp = (CString*)m_FrankTrack.GetNext( pos );
				strTemp = *pstrTemp;
				if ( strTemp.Find ( "=W2122T") >= 0 ) {
					strTemp = "=W2122T_AT_N[21]";
					if ( pos != 0x00 ) { // 제일 마지막 엘리먼트의 POS 에는 0x00 이 들어간다.
						m_FrankTrack.GetPrev( pos );
						m_FrankTrack.RemoveAt(pos);
					} else {
						m_FrankTrack.RemoveTail();
					}

					m_FrankTrack.AddTail( strTemp );
				}
			}
		}
	}
*/
	if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYP",0 ) >= 0 ) {

		if ( m_No == "63:67(M)" ) {
			m_Approach.RemoveAll();
			m_Delay="";
		}

		if ( m_No == "67:69(M)" ) {
			strTemp = "*N[133]";
			m_LockSwitch.CheckAddHead(strTemp);
			m_LockSwitch.RemoveString("N[133]");
		}

		if ( m_No == "58:52(M)" ) {
			m_Aspect = "YY, G";
			m_Aspect_Table = "YY, G";
			m_AheadSignal = "52 AT Y, 52 AT Y+LI(M)";
			m_AheadSignal_Table = "52 AT Y, 52 AT Y+M ";
			m_LockSignal.RemoveString("60:58L(M)");
//kkk			m_LockSignal.RemoveAtIndex(3); // 60:58R 진로가 나면 58:56진로가 안나기 때문
//			return TRUE;
		}
		if ( m_No == "58:52(C)" ) {
			m_Delay = "";
			m_Occupy.RemoveTail();
			m_Time = "";
			m_LockSignal.RemoveString("60:58R(M)");
			m_LockSignal.RemoveString("60:58R(M)-1");
//kkk			m_LockSignal.RemoveAtIndex(3); // 60:58R 진로가 나면 58:56진로가 안나기 때문
//			return TRUE;
		}
		if ( m_No == "58:56(M)" ) {
			m_Aspect = "YY+LI(L)";
			m_Aspect_Table = "YY+L";
			m_AheadSignal = "56 AT R OR G";
			m_AheadSignal_Table = "^^^^^^^^^^^^56 AT R^^OR G";
			m_LockSignal.RemoveString("60:58R(M)");
			m_LockSignal.RemoveString("60:58R(M)-1");
//kkk			m_LockSignal.RemoveAtIndex(4); // 60:58R 진로가 나면 58:56진로가 안나기 때문
			strTemp = "A65:65(C)";
			m_LockSignal.CheckAddTail(strTemp);
			strTemp = "B4";
			m_BlockSignal=strTemp;
//			return TRUE;
		}
		if ( m_No == "60:58L(M)" ) {
			m_Aspect = "Y";
			m_Aspect_Table = "Y";

			m_Track.RemoveTail();
			m_FrankTrack.RemoveAtIndex(0);
			strTemp = "=14T";
			m_Track.CheckTrackAdd(strTemp);
			m_FrankTrack.AddTail(strTemp);

			m_AheadSignal = "58 AT G OR YY";
			m_AheadSignal_Table = "^^^^^^^^^^^^58 AT G^^OR YY";
//			m_LockSignal.RemoveString("58:52(M)");

//			return TRUE;
		}
		if ( m_No == "60:58R(M)" ) {
			m_LockSwitch.RemoveAtIndex(2);

			m_Track.RemoveTail();
			m_FrankTrack.RemoveAtIndex(0);
			strTemp = "=14T";
			m_Track.CheckTrackAdd(strTemp);
			m_FrankTrack.AddTail(strTemp);

			m_AheadSignal = "58 AT G OR YY OR SY";
			m_AheadSignal_Table = "^^^^^^^^^^^^58 AT G^^OR YY^^OR SY";

			m_LockSignal.RemoveString("58:52(C)");
//kkk			m_LockSignal.RemoveAtIndex(4); // 60:58R 진로가 나면 58:56진로가 안나기 때문
//			return TRUE;
		}

		if ( m_No == "60:58R(M)-1" ) {
			m_LockSwitch.RemoveAtIndex(2);

			m_Track.RemoveTail();
			m_FrankTrack.RemoveAtIndex(0);
			strTemp = "=14T";
			m_Track.CheckTrackAdd(strTemp);
			m_FrankTrack.AddTail(strTemp);

			m_AheadSignal = "58 AT G OR YY OR SY";
			m_AheadSignal_Table = "^^^^^^^^^^^^58 AT G^^OR YY^^OR SY";

			m_LockSignal.RemoveString("58:52(C)");
//kkk			m_LockSignal.RemoveAtIndex(4); // 60:58R 진로가 나면 58:56진로가 안나기 때문
//			return TRUE;
		}

		if ( m_No == "67:69(M)" ) {
			strTemp = "15T";
			m_Track.CheckTrackAdd(strTemp);
			m_TrackClear.CheckTrackAdd(strTemp);
//			return TRUE;
		}
		if ( m_No == "69:15T(M)" ) {
			m_LockSignal.RemoveAll();
			m_BlockSignal.Empty();
		}

	}

	return FALSE;
}

BOOL CRow::CheckException(CRow *row)
{
	if ( m_No.Find(":53(M)") > 0 && row->m_No == "45:53(M)" )  return TRUE;
	if ( row->m_No.Find(":53(M)") > 0 && m_No == "45:53(M)" )  return TRUE;	

	return FALSE;
}

BOOL CRow::CheckIncludeTrack(CRow *row)
{
	CMyStrList	*rsw = &(row->m_CallonTempTrack);
	BOOL		find;
	POSITION	pos, pos1;
	CString		*str, *str1;
	char		*s, *s1;
	int			iTrackCount1,iTrackCount2;
	int			iSig1,iSig2;

	find = FALSE;
	
	iSig1 = atoi(row->m_Signal.Right(1));
	iSig2 = atoi(m_Signal.Right(1));
	iSig1 = iSig1 % 2;
	iSig2 = iSig2 % 2;

	if ( iSig1 != iSig2 ) return FALSE;

	iTrackCount1 = m_CallonTempTrack.GetCount();
	iTrackCount2 = rsw->GetCount();

	if ( iTrackCount1 > iTrackCount2 ) {	
		for( pos = rsw->GetHeadPosition(); pos != NULL; )
		{
			find = FALSE;
			str = (CString*)rsw->GetNext( pos );
			s = (char*)(LPCTSTR)*str;
			if (*s == '+') s++;
			if (*s == '#' || *s == '$') continue;  // 오버랩 진로등은 확인하지 않는다.

			for( pos1 = m_CallonTempTrack.GetHeadPosition(); pos1 != NULL; )
			{
				str1 = (CString*)m_CallonTempTrack.GetNext( pos1 );
				s1 = (char*)(LPCTSTR)*str1;
				if (*s1 == '+') s1++;
				if (*s1 == '#' || *s1 == '$') continue; // 오버랩 진로등은 확인하지 않는다.

				if (!strcmp(s,s1)) {
					find = TRUE;
					break;
				}
			}
			if (find == FALSE) break;
		}
	} else {
		for( pos = m_CallonTempTrack.GetHeadPosition(); pos != NULL; )
		{
			find = FALSE;
			str = (CString*)m_CallonTempTrack.GetNext( pos );
			s = (char*)(LPCTSTR)*str;
			if (*s == '+') s++;
			if (*s == '#' || *s == '$') continue;  // 오버랩 진로등은 확인하지 않는다.

			for( pos1 = rsw->GetHeadPosition(); pos1 != NULL; )
			{
				str1 = (CString*)rsw->GetNext( pos1 );
				s1 = (char*)(LPCTSTR)*str1;
				if (*s1 == '+') s1++;
				if (*s1 == '#' || *s1 == '$') continue; // 오버랩 진로등은 확인하지 않는다.

				if (!strcmp(s,s1)) {
					find = TRUE;
					break;
				}
			}
			if (find == FALSE) break;
		}	
	}
	if (find==FALSE) return FALSE;
	return TRUE;
}

BOOL CRow::CheckTrack(CRow *row)
{
	CMyStrList	*rsw = &(row->m_CallonTempTrack);
	BOOL		find;
	POSITION	pos, pos1;
	CString		*str, *str1;
	char		*s, *s1;

	find = FALSE;
	for( pos = rsw->GetHeadPosition(); pos != NULL; )
	{
	    str = (CString*)rsw->GetNext( pos );
		s = (char*)(LPCTSTR)*str;
		if (*s == '+') s++;
		if (*s == '#' || *s == '$') s++;

		for( pos1 = m_CallonTempTrack.GetHeadPosition(); pos1 != NULL; )
		{
			str1 = (CString*)m_CallonTempTrack.GetNext( pos1 );
			s1 = (char*)(LPCTSTR)*str1;
			if (*s1 == '+') s1++;
			if (*s1 == '#' || *s1 == '$') {
				s1++;
			}
			if (!strcmp(s,s1)) {
				find = TRUE;
				break;
			}
		}
		if (find) break;
	}
	if (!find) return FALSE;
	return TRUE;
}

BOOL CRow::SwitchInclude(CRow *row) // 대항진로 생성 하는 루틴 // 인자로 들어오는것이 비교되는 놈임 
{
	int a = 0;

	if ( m_No == "A54:54(C)" ) {
		if ( row->m_No == "54:18(C)" ) {
			a++;
		}		
	}
	if (!row->isSignal())					return FALSE;
	if ( CheckStationException ( row ) == TRUE )	return FALSE;
	
	if ( CheckTrack( row ) == TRUE  ) {							// 겹치는 궤도가 있고
		a++;
		if ( CheckIncludeTrack (row) == TRUE ) {				// 겹치는 궤도가 있으나 진로상의 궤도가 다른진로상의 궤도에 모두 포함될경우 대항진로 아님
			return FALSE;
		} else if ( CheckPoint( row ) == TRUE ) {				// 전철기의 방향이 같거나
			a++;
		} else if ( CheckOverlapTrack( row ) == TRUE && __editdev21 !="1" ) {		// 전철기의 방향이 같지 않아도 오버랩 궤도를 거쳐가는 진로이거나 
			a++;
		} else if ( CheckFrank( row ) == TRUE ) {				// 프랭크 프로텍션에 걸리면 대항진로
			a++;
		} else if ( CheckDestTrack( row ) == TRUE ) {			// 그러나 오버랩 목적T 가 같으면 대항진로
			a++;
		} else if ( CheckContinueShuntSignal( row ) == FALSE ) {

	if ( m_No == "A54:54(C)" ) {
		if ( row->m_No == "54:18(C)" ) {
			a++;
		}		
	}
			m_LockSignal.CheckAdd(row->m_No);
			row->m_LockSignal.CheckAdd(m_No);
			return FALSE;
		}  else {
			return FALSE;
		}
	} else {
		if ( CheckContinueSignal( row ) == FALSE ) {
			a++; 
		} else if ( CheckStartShareStartTrack ( row ) == TRUE ) {
			a++;
		} else {
			return FALSE;
		}
	}

	if ( CheckContinue( row ) == FALSE ) {
		return FALSE; 
	}															// 검색된 대항진로중 연속된 진로는 대항진로에서 제거 
	if ( m_No == "A54:54(C)" ) {
		if ( row->m_No == "54:18(C)" ) {
			a++;
		}		
	}
	row->m_LockSignal.CheckAdd(m_No);
	m_LockSignal.CheckAdd(row->m_No);

	return TRUE;
}

CString MakeFileName(CDocument *pDoc, char *ext);

BOOL CRow::CheckAreaAndEdit(CTableDoc* pDoc, CPoint point, 	CString &dif)
{
	if (m_nRowType == _POINT) {
	}
	else {
		CString name = MakeID();
		if (m_cLock.CheckAreaAndEdit(6,name,point, dif)) return TRUE;
		if (m_cPointLock.CheckAreaAndEdit(7,name,point, dif)) return TRUE;
		if (m_cTrack.CheckAreaAndEdit(10,name,point, dif)) return TRUE;
		if (m_cTrackClear.CheckAreaAndEdit(10,name,point, dif)) return TRUE;
		//if (m_cRoute.CheckAreaAndEdit(9,name,point, dif)) return TRUE;
		if (m_cHold.CheckAreaAndEdit(13,name,point, dif)) return TRUE;
		if (m_cTcOCCSeq.CheckAreaAndEdit(15,name,point, dif)) return TRUE;
		
	}
	return FALSE;
}

BOOL CRow::Modify(CString &mod)
{
	int loc = mod.Find("@");
	CString token, m1 = mod.Right(mod.GetLength() - (loc+1));
	CParser m2(m1);
	m2.GetToken( token );
	m2.GetToken( token );
	CString name = MakeID();
	if (name != token) return FALSE;
	CString col = mod.Mid(1,2);
	while (m2.GetToken( token )) {
		if (col == "NO" && m_No.GetAt(0) != '-') {
			m_No = "-" + m_No;
		}
		else if (col == "RO") {
			m_LockSignal.Modify(token);
		}
		else if (col == "PO") {
			m_LockSwitch.Modify(token , 1);
		}
		else if (col == "TC") {
			m_Track.Modify(token);
		}
		else if (col == "AP") {
			m_Approach.Modify(token);
		}
		else if (col == "TO") {
			m_TcOCCSeq.Modify(token);
		}

	}
	return TRUE;
}

CString CRow::MakeID() // ILT 의 트랙 에디터를 열었을때 타이틀 바에 표시되는 내용을 생성한다. 
{
	char* s = (char*)(LPCTSTR)m_No;
	if (*s == '-') s++;
	CString name = s;
	return name;
}

BOOL CRow::RouteCompare(CRow* row)
{
	if (m_RouteName == row->m_RouteName) return FALSE;
	CMyStrList &route = row->m_Route;
	POSITION pos;
	for (pos = route.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)route.GetNext( pos );
		if (!m_Route.CheckAdd(*str, FALSE)) m_nValue++;
	}
	return TRUE;
}

void CRow::ResetView()
{
/*	m_cType.ResetView();
	m_cDelay.ResetView();
	m_cSignal.ResetView();
	m_cHold.ResetView();
	m_cButton.ResetView();
	m_cName.ResetView();
*/
}

BOOL CRow::isSwitch(CString &str)
{
	char c = str.GetAt(0);
	if (str.GetLength()>1 && (c == 'N' || c == 'R' || c== '&' || c== '*' ) && ( str.GetAt(1) == '[' || str.GetAt(2) == '[') ) {
		int l = str.GetLength();
		if ( isalpha( str.GetAt(l-2) ) )
			str = str.Left(l-2) + ']';
		return TRUE;
	}
	return FALSE;
}

CString CRow::DestTrack()
{
	CString str;
	char *s = (char*)(LPCTSTR)m_RouteName;
	if (*s) {
		s++;
		while (*s) {
			if (*s == ':') break;
			s++;
		}
		if (*s == ':') str = (s+1);
	}
	return str;
}

void CRow::InsertScissors(CMyStrList& Sci)
{
	if (m_nSubType == _POINT) return;

	CMyStrList strListTemp;
	CString strTemp,strTemp1;
	int iCheck;
	POSITION pos, pos1, pos2;
	iCheck = 0;

	for (pos = m_LockSwitch.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)m_LockSwitch.GetNext( pos );
		strTemp = str->Mid(0,str->GetLength());
		for (pos1 = Sci.GetHeadPosition(); pos1 != NULL; ) {
			CString *str1 = (CString*)Sci.GetNext( pos1 );
			CString s = str1->Left(2);
			if (str->GetAt(0) == 'R' && str->Mid(2,2) == s) {
				CString ns = "N[" + str1->Mid(2,2);
				ns += "]";
				for (pos2 = m_LockSwitch.GetHeadPosition(); pos2 != NULL; ) {
					CString *str3 = (CString*)m_LockSwitch.GetNext( pos2 );
					strTemp1 = *str3;
					if ( strTemp1 == ns ) iCheck = 1;
				}
				if ( iCheck == 0 ) strListTemp.CheckPointAdd( ns );
			}
		}
		strListTemp.CheckPointAdd( strTemp );
	}

	m_LockSwitch = strListTemp;
}

void CRow::Dump(CDumpContext& dc) const
{

//--------------------------------------------------------  접근쇄정 트랙이 없을경우 딜레이 시간을 표시하지 않는다. 
	POSITION	pos;
	CString*	str;
	CString     strOut;
	CString     strTemp;
	int         iRealCount;
	int         iTemp;
	
	iRealCount	 = 0;										// Dump 했을때 최종적으로 씌여진 진짜 객체의 카운트
	short		cont = 0;
	for( pos = m_Approach.GetHeadPosition(); pos != NULL; )
	{

        str = (CString*)m_Approach.GetNext( pos );
		strOut = *str;
		if ( strOut.Left(1) == "-" ) {
			cont = 0;
			continue;										//omani 연동도표에서 삭제된 객체일경우 출력하지 않는다. 
		}
		iRealCount ++;
	}

//--------------------------------------------------------- 접근쇄정 트랙이 없을경우 딜레이 시간을 표시하지 않는다. 
	if (m_nSubType == _POINT) {
		dc << "POINT:{";
		dc << m_Type << ", ";
		dc << m_Signal << ", ";
		dc << m_RouteName << ", ";
		m_Track.Dump(dc); dc << ", ";
		//m_Route.Dump(dc); dc << ", ";
		dc << m_Button;
		dc << "}\n";
	}
	else if (m_nSubType == _CROSSING) {
		dc << "LC:{";
		dc << m_Signal << ", ";
		m_Track.Dump(dc);
		dc << "}\n";
	}
	else if (m_nSubType == _BLOCK) {
		dc << "BLOCK:{\n\t{";
		dc << m_Type <<  "," << m_No << "}\n\t{";
		if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
			if ( m_No == "B2" ||m_No == "B4") {
				dc << "}\n\t{";
			} else {
				strTemp = m_TrackFromTo;
				strTemp.Replace("#","");
				dc << strTemp; 
				if ( strTemp.Find(":",0) <0 ) dc << ":" <<strTemp; 
				dc << "}\n\t{";
			}
		} else {
			strTemp = m_TrackFromTo;
			strTemp.Replace("#","");
			dc << strTemp; 
			if ( strTemp.Find(":",0) <0 ) dc << ":" <<strTemp; 
			dc << "}\n\t{";
		}

		if ( m_pDoc->m_StationObject.m_StationName.Find ( "AKH",0 ) >= 0 ) {
			if ( m_No == "B11" || m_No == "B12" ) {
				dc << "}\n\t{";
			} else {
				if ( m_Type == "TCB" || m_Type == "OC") {
					dc << strTemp.Left(strTemp.Find(":",0)-1) << "}\n\t{";
				} else {
					dc << strTemp.Mid(strTemp.Find(":",0)+1 , strTemp.GetLength()-(strTemp.Find(":",0)+2)) << "}\n\t{";
				}
			}
		} else if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
			if ( m_No == "B4" ) {
				dc << "A65" << "}\n\t{";
			} else {
				if ( m_Type == "TCB" || m_Type == "OC") {
					dc << strTemp.Left(strTemp.Find(":",0)-1) << "}\n\t{";
				} else {
					dc << strTemp.Mid(strTemp.Find(":",0)+1 , strTemp.GetLength()-(strTemp.Find(":",0)+2)) << "}\n\t{";
				}
			}
		} else {
			if ( m_Type == "TCB" || m_Type == "OC") {
				dc << strTemp.Left(strTemp.Find(":",0)-1) << "}\n\t{";
			} else {
				dc << strTemp.Mid(strTemp.Find(":",0)+1 , strTemp.GetLength()-(strTemp.Find(":",0)+2)) << "}\n\t{";
			}
		}
		//dc << m_Signal << "}\n\t{";
		if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
			if ( m_No == "B2" ) {
				dc << "69" << "}\n\t{";
			} else {
				dc << m_Button << "}\n\t{";
			}
		} else {
			dc << m_Button << "}\n\t{";
		}
		/*m_LockSwitch.Dump(dc);*/		dc << "}\n\t{";
		if ( m_pDoc->m_StationObject.m_StationName.Find ( "BYPA",0 ) >= 0 ) {
			if ( m_No == "B1" || m_No == "B2") {
				dc << "}\n\t{";
			} else {
				m_LockSignal.Dump(dc);		dc << "}\n\t{"; //B1
			}
		} else {
			m_LockSignal.Dump(dc);		dc << "}\n\t{"; //B1
		}
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n\t{";
		dc << "}\n";
		dc << "}\n";
	} 
	else {
		iTemp = m_Button.Find("(",0); // 버튼뒤에 붙는 (M),(M)-1,(C),(C)-1 등을 제거한다.
		dc << "ROUTE:{\n\t{";
		dc << m_Type <<  ",";
		dc << m_No;
		if ( m_RouteCnt != "" ) dc << "," << m_RouteCnt;
		dc << "}\n\t{";
		dc << m_TrackFromTo << "}\n\t{";
		dc << m_Signal << "}\n\t{";
		dc << m_Button.Left(iTemp) << "}\n\t{";
		m_LockSwitch.Dump(dc);		
		dc << "}\n\t{";
		m_LockSignal.Dump(dc);		
		// 블록 조건을 대항진로 항목에 넣는다 
		if ( m_LockSignal.GetCount() > 0 && m_BlockSignal !="" ) {
			dc << ","; 
		}
		if ( m_BlockSignal !="" && m_BlockSignal != m_sNextSignal) {	// Double Line시 출발 블럭을 넣지 않도록 수정.
			dc << m_BlockSignal; 		
		}
		// 블록 조건을 대항진로 항목에 넣는다 
		dc << "}\n\t{";
//		if ( (m_Button.Find("(S)",0) >= 0 ) && m_Route.GetCount() == 1 )
//			m_Route.Dump(dc);
//		else
			m_TrackClear.Dump(dc);		
		//궤도 하나짜리 SHUNT진로가 전방궤도 무시하고 진로를 생성하는 경우를 막기위해서 추가.
		dc << "}\n\t{";
		m_Route.Dump(dc);			dc << "}\n\t{";
		if ( m_Delay == "" ) {
			m_Occupy.Dump(dc);		dc << "}\n\t{";
				dc << m_Time;		dc << "}\n\t{";
		} else {
			m_Approach.Dump(dc);		dc << "}\n\t{";
			if ( iRealCount > 0 ) {
				dc << m_Delay;				dc << "}\n\t{";
			} else {
				if ( m_iFreeShuntCk == 1 ) {
					dc << m_Delay;				dc << "}\n\t{";
				} else dc << "}\n\t{";
			}
		}

		dc << m_Aspect << "}\n\t{";
		dc << m_AheadSignal << "}\n\t{";
		if ( m_Type == "START" && m_AheadSignal.Find(" AT",0) >= 0 && m_DependSignal != "" )
			dc << m_AheadSignal.Left(m_AheadSignal.Find(' '));
		else
			dc << m_sNextSignal;
		if ( m_sNextSignal != "" )  {						// 전방신호기가 있는경우만 후방 신호기를 출력한다. 방글라에서는 홈신호기만 해당한다. 
			if ( m_DependSignal != "" ) {
				dc << "," << m_DependSignal; 
			}
			if ( m_GShuntSignal != "" ) dc << "," << m_GShuntSignal;
			dc << "}\n\t{";
		} else if ( /*m_TrackFromTo.Find("SDG") > 0 ||*/ m_sNextSignal == "" ) {
			if ( m_DependSignal !="" ) {
				dc << "N," << m_DependSignal; // Next 신호기 가 없는경우 "N," 을 붙인후 Depend 신호기를 나열한다. 2006.3.28
			}
			if ( m_GShuntSignal != "" ) {
				if ( m_sNextSignal == "" && m_DependSignal =="" ) dc << "N"; // Next 신호기 가 없는경우 "N," 을 붙인후 Depend 신호기를 나열한다. 2006.3.28
				dc << "," << m_GShuntSignal;
			}
			dc << "}\n\t{"; 
		} else dc << m_GShuntSignal << "}\n\t{";
		dc << m_LevelCross;
		dc <<"}\n\t{";
		m_FrankTrack.Dump(dc);
		dc <<"}\n";
		dc << "}\n";
	}
}

void CRow::Sort()
{
	m_LockSwitch.Sort( 0 );
	m_LockSignal.Sort( 0 );
	m_Track.Sort( 0 );
	m_Route.Sort( 0 );
	m_Approach.Sort( 0 );
}

int CRow::GetApproachRealCount()
{
	
	POSITION	pos;
	CString*	str;
	CString     strOut;
	int         iRealCount;
	
	iRealCount	 = 0;							// Dump 했을때 최종적으로 씌여진 진차 객체의 카운트
	short		cont = 0;
	for( pos = m_Approach.GetHeadPosition(); pos != NULL; )
	{

        str = (CString*)m_Approach.GetNext( pos );
		strOut = *str;
		if ( strOut.Left(1) == "-" ) {
			cont = 0;
			continue;  //omani 연동도표에서 삭제된 객체일경우 출력하지 않는다. 
		}
		iRealCount ++;
	}
	
	return iRealCount;
}

int CRow::GetMaxRows()
{
	int nMax = 0;
	for ( int i=0; i<m_nMaxCells; i++ ) {
		CCell *pCell = m_pCell[i];
		int r = pCell->GetRows();
		if ( r > nMax ) nMax = r;
	}
	return nMax;
}

void CRow::IncHeight( int n )
{
	m_iHeight += n;
	for ( int i=0; i<m_nMaxCells; i++ ) {
		CCell *pCell = m_pCell[i];
		for ( int j = 0; j < n; j++ ) {
			pCell->IncHeight();
		}
	}
}

CString CRow::GetLevelCrossName( CString strLCName )
{
	// Level Cross 의 명칭을 LC1 혹은 1LC 모두 수용
	CString strRetVal;
	CString strTemp,strLCNo;
	int iTemp,iLength;

	strTemp = strLCName;
	iTemp = strTemp.Find("LC",0);
	iLength = strTemp.GetLength();
	if ( iTemp == 0 ) {
		strLCNo = strTemp.Right(iLength-2);
	} else {
		strLCNo = strTemp.Left(iLength-2);
	}

	strRetVal = " %LC" + strLCNo + "\n";
	return strRetVal;
}

