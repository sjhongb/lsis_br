//=======================================================
//====               Cell.cpp                        ==== 
//=======================================================
//  파  일  명: Cell.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 연동도표 화면 표시의 기본단위 
//              
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "LockEdit.h"
#include "PointLockEdit.h"
#include "TrkEdit.h"
#include "Cell.h"
#include "TableRow.h"
#include "../include/parser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define USER_LEFTTHICK   0x10000
#define USER_RIGHTTHICK  0x20000

#define MYYELOW RGB(64,255,0)

CPoint CCell::m_Org, CCell::m_Current;
int CCell::m_nTextH;

//=======================================================
//
//  함 수 명 :  Draw
//  함수출력 :  없음
//  함수입력 :  CMyDc* pDC, BOOL viewall
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표에 기본 셀을 그린다.
//
//=======================================================
void CCell::Draw(CDC* pDC, BOOL /* viewall */) {
	if (m_bView) {
		CRect r = *this;
		pDC->Rectangle(r);
	}
}

//=======================================================
//
//  함 수 명 :  DrawTextRect
//  함수출력 :  없음
//  함수입력 :  CDC *_pDC, char *sitem, CRect &rect, int mode, int dr = 0
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면의 사각형 영역에 글자를 표시한다.
//
//=======================================================
void DrawTextRect(CDC *_pDC, char *sitem, CRect &rect, int mode, int dr = 0, int iLine = 0) 
{
	CDC *pDC = (CDC*)_pDC;
	int AlMode = DT_VCENTER | DT_SINGLELINE | DT_NOCLIP;
	CRect r = rect;
	int iTemp = iLine; // 헤더와 헤더가 아닌 부분을 구분 - 중간 선을 그리기 위해 
	if (dr == 1) {
		CBrush *oldbr = NULL;
		CBrush br;
		if (sitem && *sitem == '@') {
			COLORREF color = RGB(255,255,255);
			iTemp = 0;
			switch(sitem[1]) {
			case 'R': color = RGB(255,192,192); break;
			case 'Y': color = RGB(255,255,0); break;
			case 'B': color = RGB(224,224,255); break;
			}
			br.CreateSolidBrush(color);
			oldbr = pDC->SelectObject(&br);
		} /*else iTemp = 0;*/
		CRect r = rect;
		pDC->Rectangle(r);
		if (mode & USER_LEFTTHICK) {
			pDC->MoveTo(rect.left+1,rect.top);
			pDC->LineTo(rect.left+1,rect.bottom);
		} else if (mode & USER_RIGHTTHICK) {
			pDC->MoveTo(rect.right-1,rect.top);
			pDC->LineTo(rect.right-1,rect.bottom);
		}

		if (oldbr) {
			pDC->SelectObject(oldbr);
			br.DeleteObject();
		}
	}

	int count = 0;
	CString str[10];
	switch (mode & 3) {
	case 1	:	AlMode |= DT_RIGHT;
				r.right -= 4;
				break;
	case 2	:	AlMode |= DT_CENTER;
				break;
	default :	AlMode |= DT_LEFT;
				r.left += 4;
				break;
	}

	pDC->SetBkMode(TRANSPARENT);
	if (sitem && *sitem) {
		char *bf = strdup(sitem);
		char *s = strtok(bf,",,\n");
		while (s && count<10) {
			if (*s == '@') {
				str[count++] = s+2;
			} else {
				str[count++] = s;
			}
			s = strtok(NULL,"\n");
		}
		delete bf;
		int cy;
		int h = (- rect.bottom + rect.top) / count;
		cy = rect.top - h / 2;
		for (int i=0; i<count; i++) {
			r.top = cy + h/2;
			r.bottom = cy - h/2;
			// 표시변환
			
			if ( str[i] == "OHOME" ) { 
				str[i] = "Outer";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "OCALLON" ) { 
			    str[i] = "Outer 1Y";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "CALLON" ) {
				str[i] = "\n\n\n\n\n\n\n\n\n\nHome\nCallon";		AlMode &= ~DT_SINGLELINE ;
			} else if ( str[i] == "SHUNT" ) {
				str[i] = "Shunt";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "START" ) {
				str[i] = "Starter";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "ISTART" ) {
				str[i] = "Starter*";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "ISHUNT" ) {
				str[i] = "Shunt*";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "GSHUNT" ) {
				str[i] = "Shunt";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else if ( str[i] == "ASTART" ) {
				str[i] = "\n\n\n\n\n\n\n\n\n\nAdvanced\nStarter";	AlMode &= ~DT_SINGLELINE ;
			} else if ( str[i] == "HOME" ) {
				str[i] = "Home";
				AlMode |= DT_VCENTER ;
				AlMode |= DT_CENTER ;
				AlMode |= DT_NOCLIP ;
			} else {
				AlMode |= DT_SINGLELINE ;
			}

/*
			if ( str[i] == "OHOME" ) { 
				str[i] = "Outer";
			} else if ( str[i] == "OCALLON" ) { 
			    str[i] = "Outer SY";
			} else if ( str[i] == "CALLON" ) {
				str[i] = "Home\nCallon";
			} else if ( str[i] == "SHUNT" ) {
				str[i] = "Shunt";
			} else if ( str[i] == "START" ) {
				str[i] = "Starter";
			} else if ( str[i] == "ISTART" ) {
				str[i] = "Starter*";
			} else if ( str[i] == "ISHUNT" ) {
				str[i] = "Shunt*";
			} else if ( str[i] == "GSHUNT" ) {
				str[i] = "Shunt";
			} else if ( str[i] == "ASTART" ) {
				str[i] = "Advanced\nStarter";
			} else if ( str[i] == "HOME" ) {
				str[i] = "Home";
			} else {
				AlMode |= DT_SINGLELINE;
			}
			AlMode |= DT_SINGLELINE;
*/
			
			str[i].Replace("_"," ");
			if ( str[i].Find(" AT ",0) > 0 && str[i].Find(" OR ",0) > 0 && strlen(str[i]) > 12 )
			{
				str[i] = "^^^^" + str[i].Right(strlen(str[i]));
				str[i].Replace(" OR", "^^   OR");
			}
			if ( str[i].Find('^') != -1 ) {
				AlMode &= ~DT_SINGLELINE;
			}
			str[i].Replace("\^","\n");

//			CFont headerFont;
//			CFont * oldfont;
			if ( str[i].Find("30S") >= 0 )
				str[i].Replace("30S","30");
			if ( str[i].Find("60S") >= 0 )
				str[i].Replace("60S","60");
			if ( str[i].Find("120S") >= 0 )
				str[i].Replace("120S","120");
			
//			headerFont.CreateFont(16,8,0,0,FW_BOLD,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림");
			
//			oldfont = pDC->SelectObject(&headerFont);
			
			pDC->DrawText(str[i], &r, AlMode );

//			pDC->SelectObject(oldfont);
			
//			headerFont.DeleteObject();

			if ( iTemp== 1 ) pDC->MoveTo(r.left,r.bottom);  // 신호기 현시등 한 셀내에서 선을 그려야 할경우 선을 그린다. 
			if ( iTemp== 1 ) pDC->LineTo(r.right,r.bottom); // 신호기 현시등 한 셀내에서 선을 그려야 할경우 선을 그린다. 
			cy -= h;
		}
	}
}

//=======================================================
//
//  함 수 명 :  Draw
//  함수출력 :  없음
//  함수입력 :  CMyDc* pDC , BOOL
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  기본 셀을 화면에 그린다.
//
//=======================================================						   
void CCommonCell::Draw(CDC* pDC , BOOL) {
	if (!m_bView) return;
	char *sitem = (char*)(LPCSTR)*m_pStr;
	char c = *sitem;
	if (c == '-') {
		COLORREF old;
		CCell::Draw(pDC);
		old = pDC->SetTextColor(MYYELOW);
		sitem++;
		pDC->DrawText(sitem, strlen(sitem), this, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER | DT_CENTER);
		pDC->SetTextColor(old);
	}
	else DrawTextRect(pDC, sitem, *this, 2, 1 , 1);
}

void CSplitCell::Draw(CDC* pDC, BOOL /* viewall */) {
	if (!m_bView) return;
	CCell::Draw(pDC);
	if (!m_pData->GetCount()) return;

	POSITION pos;

	CString strl;
	int ww = m_Size.cx;

	CRect rr = *this;

	CSize cir = pDC->GetTextExtent("=");
	int cx,cy;
	int h = cir.cy * 15 / 10;
	int by = rr.top - h / 2;
	int row = 0;
	int old = pDC->SetTextAlign(TA_UPDATECP);

	int nRowNo = 0;
	for( pos = m_pData->GetHeadPosition(); pos != NULL; )
	{
		CString pstr = *(CString*)m_pData->GetNext( pos );
		cx = rr.left + cir.cx;

		CString str;
		CParser line( pstr );
		while ( line.GetToken( str ) ) {
			CSize tinf = pDC->GetTextExtent( str );
			if ( cx + tinf.cx > rr.left + ww ) {
				row++;
				cx = rr.left + cir.cx;
			}
			cy = by - h * row;
			pDC->MoveTo(cx,cy);
			pDC->DrawText( str, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			cx += tinf.cx + cir.cx;		// 2002.7.5. 1/2 -> 1
		}
		row++;

		ASSERT( nRowNo < 128 );
		int & fixrow = m_pHeight[nRowNo];
		if ( fixrow < row ) {
			fixrow = row;
		}
		else if ( fixrow > row ) {
			row = fixrow;
		}

		cy = by - h * row;
		pDC->MoveTo(rr.left,cy+2);
		pDC->LineTo(rr.right,cy+2);

		nRowNo++;
		m_nRows = nRowNo;
	}

	if ( m_pRow && row > m_pRow->m_iHeight ) {
		m_pRow->IncHeight( row - m_pRow->m_iHeight );
	}

	pDC->SetTextAlign(old);
}

//=======================================================
//
//  함 수 명 :  GetSwitchNameFromNR
//  함수출력 :  CString
//  함수입력 :  CString &str
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  Allow Long Switch Name  전철기 명에서
//              +&N[12] 와 같은경우 문자를 다 제거 하고 
//              12 만 리턴 한다. 
//
//=======================================================
CString GetSwitchNameFromNR( CString &str ) 
{
	int iFirst, iLast;
	iFirst = str.Find("[",0);
	iLast  = str.Find("]",0);
	if ( iFirst > -1 ) iFirst = iFirst + 1;
	if ( ( iLast > -1)  && ( iFirst < iLast ) ) iLast = iLast - iFirst; 
	return str.Mid(iFirst,iLast);
}

void CLockCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[14], colsa[14];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		if ( ww > 1400 )
			ww = 1300;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			if ( pstr == "" ) continue;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString st = strl + "  " + str;
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx + 200 > ww && rows<15) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 15) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		if ( rows == 0 ) rows = 1;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		int iCountProhibitRoute = 0;
		int iCountForLastComma = 0;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
			CString pstr = *(CString*)m_pData->GetNext( pos );
			if ( pstr == "" ) continue;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			iCountProhibitRoute++;
		}

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			if ( pstr == "" ) continue;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			iCountForLastComma++;
			CString now;
			if (iCountForLastComma == iCountProhibitRoute)
				now = " " + str;
			else
				now = " " + str + " , ";
			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
		pDC->SetTextAlign(old);
	}
}

//=======================================================
//
//  함 수 명 :  Draw
//  함수출력 :  없음
//  함수입력 :  CMyDc* pDC, BOOL viewall
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  연동도표에 전철기 쇄정을 표시한다. 
//
//=======================================================
void CPointLockCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		CRect r = *this;
		int d = 10;
		POSITION pos;
									// N[21]
		r.left += 5;
		r.right = r.left+d*3;
		int y = (top+bottom)/2;
		r.top = y + d;
		r.bottom = y - d;
		CString last,s;

		int left = r.left;
		int iOverLapPointCheck = 0 ;   // 오버랩 전철기 & 붙은거 확인 하는 플래그 .. 처음 진입시 [ 표시 끝날때 ] 표시

		int n = 0; 
		int iTemp = 0;
		for( pos = m_pDataT->GetHeadPosition(); pos != NULL; )
		{
			CString &pstr = *(CString*)m_pDataT->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			s = GetSwitchNameFromNR( str );
			if (s != last) {
				n++;
				last = s;
			}
		}
		last = "";
		if (n < 5) {
			for( pos = m_pDataT->GetHeadPosition(); pos != NULL; )
			{
				CString &pstr = *(CString*)m_pDataT->GetNext( pos );
				CString str;
				char c = pstr.GetAt(0);
				if (!viewall && c == '-') continue;
				if (c == '+' || c == '-' ) str = pstr.Right(pstr.GetLength()-1);
				else {
					c = ' ';
					str = pstr;
				}
				if (( str.Find("*",0) > -1 ) || ( str.Find("&",0) > -1 )||( str.Find("#",0) > -1 )) {
					if ( iOverLapPointCheck == 0 ) {
						iOverLapPointCheck = 1;
					}
					iTemp = 1;
				} else iTemp = 0;

				s = GetSwitchNameFromNR( str );
				if ( iOverLapPointCheck == 1 ) {
					pDC->DrawText("[", r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					
					r.left += d*3+2;
					r.right += d*3+2;
					
					iOverLapPointCheck = 2;

				} 

				if (s != last) {
					if (str.GetAt(iTemp) == 'R') {
						//pDC->Ellipse(r);
						s = s + "R";
					} else {
						s = s + "N";
					}
					COLORREF old;
//					if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
					if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
					else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);

					pDC->DrawText(s, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					if (c != ' ') pDC->SetTextColor(old);
					r.left += d*3+2;
					r.right += d*3+2;
					last = s;
				}
			}
			if ( iOverLapPointCheck == 2 ) {
					if ( n == 4 )
					{
						r.left -= 7;
					}
					pDC->DrawText("]", r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);			
			}
		}
		else {
			n++;
			n /= 2;
			int rows = 2, cols = 0;
			int row = 0;
			CSize cir = pDC->GetTextExtent("8]");
			int h = (- r.bottom + r.top) / rows + 5;
			int by = (r.bottom + r.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
			row = 0;
			cols = 0;

			r.top = by;
			r.bottom = by - h;

			CRect r1 = r;
			for( pos = m_pDataT->GetHeadPosition(); pos != NULL; )
			{
				CString &pstr = *(CString*)m_pDataT->GetNext( pos );
				CString str;
				char c = pstr.GetAt(0);
				if (!viewall && c == '-') continue;
				if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
				else {
					c = ' ';
					str = pstr;
				}

				if (( str.Find("*",0) > -1 ) || ( str.Find("&",0) > -1 )||( str.Find("#",0) > -1 )) {
					if ( iOverLapPointCheck == 0 ) {
						iOverLapPointCheck = 1;
					}
					iTemp = 1;
				} else iTemp = 0;

				s = GetSwitchNameFromNR( str );

				if ( iOverLapPointCheck == 1 ) {
					pDC->DrawText("[", r1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					
					r1.left += d*3+2;
					r1.right += d*3+2;
					
					iOverLapPointCheck = 2;

				} 

				if (s != last) {
					if (str.GetAt(iTemp) == 'R') {
						s = s +"R";
						//pDC->Ellipse(r1);
					} else {
						s = s + "N";
					}
					COLORREF old;
//					if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
					if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
					else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
					pDC->DrawText(s, r1, DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_NOCLIP);
					if (c != ' ') pDC->SetTextColor(old);
					r1.left += d*3+2;
					r1.right += d*3+2;
					last = s;
					cols++;
					if (/*!row &&*/ ( cols == 4 ) ) {
						cols = 0;
						row++;
						r = r1;
						r1.left = left;
						r1.right = left + d*3;
						r1.top -= h;
						r1.bottom -= h;
					}
				}
			}
			if ( iOverLapPointCheck == 2 ) {
					pDC->DrawText("]", r1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);			
			}
		}
	}
}

void CTrackCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[10], colsa[10];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			str.Replace("#","");
			CString st = strl + "  " + str;
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx + 50 > ww && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 11) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		if ( rows == 0 ) rows = 1;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			str.Replace("#","");
			CString now = " " + str;
			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
		pDC->SetTextAlign(old);
	}
}

void CTrackClearCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[10], colsa[10];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			str.Replace("#","");
			CString st = strl + "  " + str;
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx + 50 > ww && strlen(str) <= 4 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else if (sz.cx + 100 > ww && strlen(str) <= 9 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else if (sz.cx + 100 > ww && strlen(str) > 9 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 11) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		if ( rows == 0 ) rows = 1;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		int iReversePoint;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		int iCountTrackClear = 0;
		int iCountForLastComma = 0;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString pstr = *(CString*)m_pData->GetNext( pos );
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			iCountTrackClear++;

		}
		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			str.Replace("#","");
			str.Replace("_","");
			if ( str.Find("R[") > 0 ) {	
				iReversePoint = 1;
			} else {
				iReversePoint = 0;
			}
			str.Replace("TAT","T@");
			str.Replace("[","");
			str.Replace("]","");
			
			CString now;
			iCountForLastComma++;

			if( iCountForLastComma == iCountTrackClear )
				now = " " + str;
			else
				now = " " + str + " , ";

			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);

			int d = 10;
			CRect r = *this;


			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);

/*			if ( iReversePoint == 1 ) {	
				r.left = cx+sz1.cx;
				r.right = cx + 4;
				r.top = cy;
				r.bottom = cy + 4;
				pDC->Ellipse(r);
			}
*/
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
		pDC->SetTextAlign(old);
	}
}

void CApproachCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[10], colsa[10];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if ( c == '?' ) pstr = pstr.Right(pstr.GetLength()-1);
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString st = strl + "  " + str;
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx + 50 > ww && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 11) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		int iCountApproachTrack = 0;
		int iCountForLastComma = 0;
		CString strApproachTrack[5];

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
			CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if ( c == '?' ) pstr = pstr.Right(pstr.GetLength()-1);
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			strApproachTrack[iCountApproachTrack] = str;
			iCountApproachTrack++;
		}
		for ( int jj = 0; jj < iCountApproachTrack; jj++)
		{
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			CString now;
			iCountForLastComma++;

			if( iCountApproachTrack == iCountForLastComma )
				now = " " + strApproachTrack[iCountApproachTrack-jj-1];
			else
				now = " " + strApproachTrack[iCountApproachTrack-jj-1] + " , ";

			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
			//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
/*		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
			CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if ( c == '?' ) pstr = pstr.Right(pstr.GetLength()-1);
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString now = " " + str;
			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
			//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
*/
		pDC->SetTextAlign(old);
	}
}

void CTcOCCCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[10], colsa[10];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString st = strl + "  " + str;
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx + 80 > ww && strlen(str) <= 6 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else if (sz.cx + 100 > ww && strlen(str) <= 9 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else if (sz.cx + 130 > ww && strlen(str) > 9 && rows<11) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 11) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		int iCountTCOCCSeq = 0;
		int iCountForLastComma = 0;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
			CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			iCountTCOCCSeq++;
		}
		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
			CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString now;
			iCountForLastComma++;
			if ( iCountForLastComma == iCountTCOCCSeq )
				now = " " + str;
			else
				now = " " + str + " , ";

			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
			//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
		pDC->SetTextAlign(old);
	}
}

void CRouteCell::Draw(CDC* pDC, BOOL viewall) {
	if (m_bView) {
		CCell::Draw(pDC);
		if (!m_pData->GetCount()) return;
		POSITION pos;

		int wa[12], colsa[12];
		int rows = 0, cols = 0;
		CString strl;
		int ww = m_Size.cx;

		CRect rr = *this;

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString &pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			cols++;
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString st = strl + "(" + str + " )";
			CSize sz = pDC->GetTextExtent(st);
			if (sz.cx  + 50 > ww  && rows<13) {
				colsa[rows] = cols;
				wa[rows] = (pDC->GetTextExtent(strl)).cx;
				rows++;
				strl = "";
				cols = 0;
			}
			else strl = st;
		}
		if (cols && rows < 13) {
			colsa[rows] = cols;
			wa[rows] = (pDC->GetTextExtent(strl)).cx;
			rows++;
		}

		CSize cir = pDC->GetTextExtent("8]");
		int cx,cy;
		int h = (- rr.bottom + rr.top) / rows;
		int by = (rr.bottom + rr.top) / 2 + h * (rows-1) / 2 + cir.cy * 1 / 2 + 1;
		int row = 0;
		cols = 0;
		int old = pDC->SetTextAlign(TA_UPDATECP);

		for( pos = m_pData->GetHeadPosition(); pos != NULL; )
		{
		    CString pstr = *(CString*)m_pData->GetNext( pos );
			CString str;
			char c = pstr.GetAt(0);
			if (!viewall && c == '-') continue;
			if (!cols) {
				cx = rr.left + 2;
				cy = by - h * row;
				pDC->MoveTo(cx,cy);
			}
			if (c == '+' || c == '-') str = pstr.Right(pstr.GetLength()-1);
			else {
				c = ' ';
				str = pstr;
			}
			CString now = "(" + str + ")";
			CSize sz1 = pDC->GetTextExtent(now);
			COLORREF old;
//			if (c == '+') old = pDC->SetTextColor(RGB(255,0,0));
			if (c == '+') old = pDC->SetTextColor(RGB(0,0,0));
			else if (c == '-') continue;//old = pDC->SetTextColor(MYYELOW);
			pDC->DrawText(now, rr, DT_SINGLELINE | DT_NOCLIP | DT_VCENTER);
			if (c != ' ') pDC->SetTextColor(old);
			cols++;
			if (cols >= colsa[row]) {
				row++;
				cols = 0;
			}
		}
		pDC->SetTextAlign(old);
	}
}

CString CLockCell::CellEdit(CString& name)
{
	CMyStrList DataT, DataS;
	DataS = *m_pData;
	CLockEdit dlg(name, m_pData );
	dlg.DoModal();
	CString Difference = "@RO " + name;
	DataS.Compare(Difference, m_pData);
	return Difference;
}

CString CPointLockCell::CellEdit(CString& name)
{
	CMyStrList DataT;
	DataT = *m_pDataT;
	CPointLockEdit dlg(name, m_pDataT);
	dlg.DoModal();
	CString Difference = "@PO " + name;
	DataT.Compare(Difference, m_pDataT);
	return Difference;
}

CString CTrackCell::CellEdit(CString& name)
{
	CMyStrList Data;
	Data = *m_pData;
	CTrackEdit dlg(name, m_pData);
	dlg.DoModal();
	CString Difference = "@TR " + name;
	Data.Compare(Difference, m_pData);
	return Difference;
}

CString CTrackClearCell::CellEdit(CString& name)
{
	CMyStrList Data;
	Data = *m_pData;
	CTrackEdit dlg(name, m_pData);
	dlg.DoModal();
	CString Difference = "@TC " + name;
	Data.Compare(Difference, m_pData);
	return Difference;
}

CString CApproachCell::CellEdit(CString& name)
{
	CMyStrList Data;
	Data = *m_pData;
	CTrackEdit dlg(name, m_pData);
	dlg.DoModal();
	CString Difference = "@AP " + name;
	Data.Compare(Difference, m_pData);
	return Difference;
}

CString CTcOCCCell::CellEdit(CString& name)
{
	CMyStrList Data;
	Data = *m_pData;
	CTrackEdit dlg(name, m_pData);
	dlg.DoModal();
	CString Difference = "@TO " + name;
	Data.Compare(Difference, m_pData);
	return Difference;
}

CString CRouteCell::CellEdit(CString& name)
{
	CMyStrList Data;
	CTrackEdit dlg(name, m_pData);
	dlg.DoModal();
	CString Difference = "@" + name;
	Data.Compare(Difference, m_pData);
	return Difference;
}

typedef struct TitleTag {
	char *ttl;
	int w,s,h;
} Title;

extern TitleTag TableTitle[];

BOOL CCell::CheckAreaAndEdit(int n, CString& name, CPoint point, CString& dif)
{
	if (left<point.x && right>point.x && top>point.y && bottom<point.y) {
		CString ttl = name;
		dif = CellEdit(ttl);
		return TRUE;
	}
	return FALSE;
}

