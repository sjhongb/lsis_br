//=======================================================
//==              SetMar.h
//=======================================================
//	파 일 명 :  SetMar.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class CSetMar : public CDialog
{
// Construction
public:
	CSetMar(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetMar)
	enum { IDD = IDD_SETMARGIN };
	UINT	m_Top;
	UINT	m_Left;
	UINT	m_Right;
	UINT	m_Bottom;
	BOOL	m_cTotalPage;
	int		m_iStartPage;
	int		m_iTotPage;
	BOOL	m_bCoverPage;
	//}}AFX_DATA
	int     m_iTableType;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetMar)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetMar)
	afx_msg void OnRadioType1();
	afx_msg void OnRadioType2();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioType3();
	afx_msg void OnRadioType4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
