//=======================================================
//====               ButtonDl.h                      ==== 
//=======================================================
//  파  일  명: ButtonDl.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 압구에 관한 정보를 입력하는 화면 
//              
//=======================================================

#include "Button.h"

class CMyButtonDlg : public CDialog
{
// Construction
	CMyButton *m_pObject;
public:
	CMyButtonDlg(CGraphObject *obj, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMyButtonDlg)
	enum { IDD = IDD_BUTTON };
	CComboBox	m_cLineNo;
	CString	m_sName;
	int		m_Radio;
	int		m_nLR;
	BOOL	m_bName1;
	int		m_nLine;
	BOOL	m_bCont;
	BOOL	m_bNoCallon;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyButtonDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMyButtonDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
