//=======================================================
//==              Develop.h
//=======================================================
//	파 일 명 :  Develop.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

class Develop : public CDialog
{
// Construction
public:
	Develop(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Develop)
	enum { IDD = IDD_DEVELOP };
	CString	m_EditDev1;
	CString	m_EditDev2;
	CString	m_EditDev3;
	CString	m_EditDev4;
	CString	m_EditDev5;
	CString	m_EditDev6;
	CString	m_EditDev7;
	CString	m_EditDev8;
	CString	m_EditDev9;
	CString	m_EditDev10;
	CString	m_EditDev11;
	CString	m_EditDev12;
	CString	m_EditDev13;
	CString	m_EditDev14;
	CString	m_EditDev15;
	CString	m_EditDev16;
	CString	m_EditDev17;
	CString	m_EditDev18;
	CString	m_EditDev19;
	CString	m_EditDev20;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Develop)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(Develop)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

