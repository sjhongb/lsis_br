//=======================================================
//==              TableDoc.h
//=======================================================
//	파 일 명 :  TableDoc.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  연동도표 처리 도큐먼트 
//
//=======================================================

#include "TableRow.h"
#include "StObject.h"

typedef struct {
	CString Flag;
	CString szFromSignal;
	CString szToSignal;	
}SignalTable;

class PageInfo;
class CTableDoc : public CDocument
{
protected:
	DECLARE_DYNCREATE(CTableDoc)

// Attributes
public:
	CSize m_ScreenSize;
	CSize m_PaperSize;
	CBitmap m_pBitmap;
	CBitmap m_pBitmap3;

	void Export();
	void DrawPageOff(CDC *pDC , int CurrPage , int TotalPage); // 연동도표의 외곽선을 그린다. 
	void DrawCover(CDC *pDC);
	void InsertScissors(CMyStrList& Sci);
	void TableRegen(BOOL bHide = FALSE);
	void Purge();
	void RemoveDeletedRow();
	CString GetProgramVersion();
	BOOL SelectOneRouteByTrackAndSwitchNR(CObList *sel);
	BOOL SelectOneRouteBy(CObList *sel, CRow::RowSubType type);
	void SelectOneRow(CObList *sel);
	void RemoveDuplicateRoute();
	BOOL UpdateModFile(CString &dif);
	void ReadModFile();
	void ModifyTable();
	void CreateSignalLock();
	void CreateRouteCnt();
	void CreateSwitchTable();
	void CreateCrossTable();
	CTableDoc();           // protected constructor used by dynamic creation
	void AllocArea(PageInfo *page);
	void SortInsert(CRow *row);
	void CreateTable();
	void BackupIDFFile(CString strFileName);
	void MakeIDFFile(CString strFileName);
	void StrWrite(int x, int y, CString str , CDC* pDC ,int iSzie = 10 , int iDevid = 0 );
	void ChangeIDFFile(CString strFileName , CString strFileName1);
	CObList m_Rows;
	CObList m_Items;
	CMyStrList m_Modifier;
	CMyStrList m_LevelCrossing;
	CStationObject m_StationObject;

	BOOL m_bTotalPage;
	int  m_iStartPage;
	int  m_iTableType;
	BOOL m_bCoverPage;

	SignalTable m_Signaltable[500];

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTableDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTableDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTableDoc)
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
