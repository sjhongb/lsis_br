//=======================================================
//==              ObjDlg.cpp
//=======================================================
//	파 일 명 :  ObjDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "ObjDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjDlg dialog


CObjDlg::CObjDlg(CString &result, CWnd* pParent /*=NULL*/)
	: CDialog(CObjDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjDlg)
	m_Selection = _T("");
	//}}AFX_DATA_INIT
	result = "";
	m_pResult = &result;
}


void CObjDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjDlg)
	DDX_CBString(pDX, IDC_COMBO1, m_Selection);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CObjDlg, CDialog)
	//{{AFX_MSG_MAP(CObjDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjDlg message handlers

void CObjDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	*m_pResult = m_Selection;
	CDialog::OnOK();
}
