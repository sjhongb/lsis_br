//=======================================================
//====               BitmapClient.cpp                ==== 
//=======================================================
//  파  일  명: BitmapClient.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.21
//  버      전: 2.01
//  설      명: MID의 배경 그림 (LG 산전 CI )을 표시해준다 
//              
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "BitmapClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitmapClient

CBitmapClient::CBitmapClient()
{
	VERIFY(m_bmp.LoadBitmap(IDB_BAKLOGO)); 
}

CBitmapClient::~CBitmapClient()
{
}


BEGIN_MESSAGE_MAP(CBitmapClient, CWnd)
	//{{AFX_MSG_MAP(CBitmapClient)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CBitmapClient message handlers

void CBitmapClient::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
              
	RedrawWindow(NULL, NULL,
 		RDW_INVALIDATE|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN);    
	return ;  
}

BOOL CBitmapClient::OnEraseBkgnd(CDC* pDC) 
{
	CWnd::OnEraseBkgnd(pDC);

	BITMAP bm ;
	CDC dcMem ;

	VERIFY(m_bmp.GetObject(sizeof(bm), (LPVOID)&bm));

	dcMem.CreateCompatibleDC(pDC);
	CBitmap* pOldBMP = (CBitmap*) dcMem.SelectObject(&m_bmp);

	CRect rect;
	GetClientRect(rect);

	int i,j,h,k,iX,iY;

	iX = rect.Width();
	iY = rect.Height();
	h = (int)((float)iX/(float)bm.bmWidth);
	k = (int)((float)iY/(float)bm.bmHeight);

	
	for ( i = 0 ; i < h+1 ; i++ ) {
	for ( j = 0 ; j < k+1 ; j++ ) {
	
		pDC->BitBlt(i*bm.bmWidth,													// centered
		j*bm.bmHeight,
		bm.bmWidth,
		bm.bmHeight,
		&dcMem,
		0, 0,
		SRCCOPY);
	}
	}

   dcMem.SelectObject(pOldBMP) ;
   
   return TRUE;
}

