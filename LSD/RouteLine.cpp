//=======================================================
//==              RouteLine.cpp
//=======================================================
//	파 일 명 :  RouteLine.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "RouteLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CRouteLine, CObject, 0)

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CRouteLine::CRouteLine()
{
	m_nCount = 0;
	m_nStack = 0;
}
CRouteLine::CRouteLine(CPoint *&p, UINT count, const char *s)
{
	m_nStack = 0;
	m_nCount = count;
	for(UINT i=0; i<m_nCount; i++) {
		m_Points[i] = p[i];
	}
	m_sRoute = s;
}
CRouteLine::~CRouteLine()
{
	Init();
}

void CRouteLine::Out( ostream &os )
{
	if ((m_sRoute != "") && m_nCount) {
		os << '\n' << m_sRoute << "\n{";
		for(UINT i=0; i<m_nCount; i++) {
			if (i % 10) os << "\n     ";
			os << '(' << m_Points[i].x << ',' << m_Points[i].y << ')';
		}
		os << "\n}";
	}
	else if (m_sRoute != "") {
		os << "\n@ROUTEFAIL:" << m_sRoute;
	}
}
void CRouteLine::Init()
{
	m_nCount = 0;
	m_nStack = 0;
}
void CRouteLine::SetRoute(const char *s)
{
	m_sRoute = s;
	Init();
}
BOOL CRouteLine::AddPoint(POINT initPt)
{
	if (m_nCount < MAX_POINT_LIST) {
		m_Points[m_nCount] = initPt;
		return TRUE;
	}
	return FALSE;
}
BOOL CRouteLine::AddPoint(SIZE initSize)
{
	if (m_nCount < MAX_POINT_LIST) {
		m_Points[m_nCount] = initSize;
		return TRUE;
	}
	return FALSE;
}
BOOL CRouteLine::AddPoint(int initX, int initY)
{
	if (m_nCount < MAX_POINT_LIST) {
		m_Points[m_nCount] = CPoint(initX, initY);
		return TRUE;
	}
	return FALSE;
}

void CRouteLine::Push()
{
	if (m_nStack < MAX_STACK_LIST) {
		m_nStackArray[m_nStack++] = m_nCount;
	}
	else AfxMessageBox("Warnning!!\n \"CRouteLline\" class Stack Overflow !"); 
}
void CRouteLine::Pop()
{
	if (m_nStack) {
		m_nCount = m_nStackArray[--m_nStack];
	}
	else AfxMessageBox("Warnning!!\n \"CRouteLline\" class Stack Minimum flow !"); 
}

void CRouteLine::Serialize(CArchive &ar)
{
	if (ar.IsStoring()) {
	}
	else {
	}
}
