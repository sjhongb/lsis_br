//=======================================================
//==              SignalDl.h
//=======================================================
//	파 일 명 :  SignalDl.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "Signal.h"
#include "Define.h"

class CSignalDlg : public CDialog
{
// Construction
	CSignal *m_pObject;
public:
	void EnableWindowBlockadeItem(BOOL bShow, int nBlockadeType = 0 );
	void EnableWindowRouteIndicator(BOOL bShow , int iSignal = _HOME);
	CSignalDlg(CGraphObject *obj, CWnd* pParent = NULL);
	void SetLamp( int iType );
// Dialog Data
	//{{AFX_DATA(CSignalDlg)
	enum { IDD = IDD_SIGNAL };
	CComboBox	m_Lamp;
	CString	m_sName;
	int		m_nRoutes;
	int		m_Radio;
	int		m_nUD;
	int		m_nLR;
	BOOL	m_bF2;
	BOOL	m_bName1;
	BOOL	m_bName2;
	BOOL	m_bNoRoute;
	BOOL	m_bNoRouteAll;
	BOOL    m_bSlant;
	int		m_nBlockadeType;
	int		m_nILR;
	BOOL	m_bF1;
	BOOL	m_bOverlapStop;
	BOOL	m_bVirtualStraightLine;
	CString strStraightSignal;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSignalDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSignalDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSigRadioInBlockade();
	afx_msg void OnSigRadioOutBlockade();
	afx_msg void OnSigRadioInSignal();
	afx_msg void OnSigRadioShuntPG();
	afx_msg void OnSigRadioShuntSignal();
	afx_msg void OnSigRadioOutSignal();
	afx_msg void OnSigRadioInStationBlockade();
	afx_msg void OnSigRadioThroughSignal();
	afx_msg void OnSigRadioFarSignal();
	afx_msg void OnHomesignaldir1();
	afx_msg void OnHomesignaldir2();
	afx_msg void OnSignalradio1();
	afx_msg void OnHomesignaldir4();
	afx_msg void OnHomesignaldir3();
	afx_msg void OnHomesignaldir5();
	afx_msg void OnHomesignaldir6();
	afx_msg void OnSigRadioLos();
	afx_msg void OnSigRadioFree();
	afx_msg void OnSigRadioIntstart();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
