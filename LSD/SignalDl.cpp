//=======================================================
//==              SignalDl.cpp
//=======================================================
//	파 일 명 :  SignalDl.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "SignalDl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSignalDlg dialog

CSignalDlg::CSignalDlg(CGraphObject *obj, CWnd* pParent /*=NULL*/)
	: CDialog(CSignalDlg::IDD, pParent)
{
	m_pObject = (CSignal*)obj;
	//{{AFX_DATA_INIT(CSignalDlg)
	m_sName = CString(m_pObject->GetName());
	m_bName1 = FALSE;
	m_bName2 = FALSE;
	m_bNoRoute = FALSE;
	m_bNoRouteAll = FALSE;
	m_bOverlapStop = FALSE;
	m_bVirtualStraightLine = FALSE;
	m_nBlockadeType = -1;
	m_nILR = -1;
	m_bF1 = FALSE;
	m_bF2 = FALSE;
	//}}AFX_DATA_INIT
	m_bName1 = (m_pObject->m_nShape & B_NAME_B) != 0;
	m_bName2 = (m_pObject->m_nShape & B_NAME_S) != 0;
	m_bNoRoute = (m_pObject->m_nShape & B_NOROUTE) != 0;
	m_bNoRouteAll = (m_pObject->m_nShape & B_NOROUTE_ALL) != 0;
	m_bOverlapStop = (m_pObject->m_nShape & B_OVERLAP_CK_STOP ) !=0;
	m_bVirtualStraightLine = (m_pObject->m_nShape & B_STRAIGHTLINE ) != 0;
	m_nBlockadeType = ((m_pObject->m_nShape & B_MASKBLOCKADE) >> 4) & 3;
	strStraightSignal = m_pObject->m_strStraightSignal;
}

void CSignalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSignalDlg)
	DDX_Control(pDX, IDC_SIGNALLAMP, m_Lamp);
	DDX_Text(pDX, IDC_SIGNALNAME, m_sName);
	DDV_MaxChars(pDX, m_sName, 20);
	DDX_Check(pDX, IDC_SIGNAL_NAME1, m_bName1);
	DDX_Check(pDX, IDC_SIGNAL_NAME2, m_bName2);
	DDX_Check(pDX, IDC_NOROUTE, m_bNoRoute);
	DDX_Check(pDX, IDC_NOROUTE_ALL, m_bNoRouteAll);
	DDX_Check(pDX, IDC_CHECK_SLANT, m_bSlant);
	DDX_Radio(pDX, IDC_SIG_RADIO_TYPE1, m_nBlockadeType);
	DDX_Check(pDX, IDC_CHK_ISTART, m_pObject->m_bF1);
	DDX_Check(pDX, IDC_CHK_HOMEBLOCK, m_pObject->m_bF2);
	DDX_Check(pDX, IDC_OVERLAP_STOP, m_bOverlapStop);
	DDX_Check(pDX, IDC_VIRTUAL_STRAIGHT_LINE, m_bVirtualStraightLine);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_SIGNALROUTE, m_pObject->m_nRoute);
	DDX_Radio(pDX, IDC_SIGNALRADIO1, m_pObject->m_nType);
	DDX_Radio(pDX, IDC_SIGNALDIR2, m_pObject->m_nLR);
	DDX_Radio(pDX, IDC_HOMESIGNALDIR1, m_pObject->m_nILR);
}


BEGIN_MESSAGE_MAP(CSignalDlg, CDialog)
	//{{AFX_MSG_MAP(CSignalDlg)
	ON_BN_CLICKED(IDC_SIG_RADIO_INBLOCKADE, OnSigRadioInBlockade)
	ON_BN_CLICKED(IDC_SIG_RADIO_OUTBLOCKADE, OnSigRadioOutBlockade)
	ON_BN_CLICKED(IDC_SIGNALRADIO1, OnSigRadioInSignal)
	ON_BN_CLICKED(IDC_SIGNALRADIO2, OnSigRadioShuntPG)
	ON_BN_CLICKED(IDC_SIGNALRADIO3, OnSigRadioShuntSignal)
	ON_BN_CLICKED(IDC_SIGNALRADIO4, OnSigRadioOutSignal)
	ON_BN_CLICKED(IDC_SIGNALRADIO5, OnSigRadioInStationBlockade)
	ON_BN_CLICKED(IDC_SIGNALRADIO6, OnSigRadioThroughSignal)
	ON_BN_CLICKED(IDC_SIGNALRADIO7, OnSigRadioFarSignal)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR1, OnHomesignaldir1)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR2, OnHomesignaldir2)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR4, OnHomesignaldir4)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR3, OnHomesignaldir3)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR5, OnHomesignaldir5)
	ON_BN_CLICKED(IDC_HOMESIGNALDIR6, OnHomesignaldir6)
	ON_BN_CLICKED(IDC_SIG_RADIO_LOS, OnSigRadioLos)
	ON_BN_CLICKED(IDC_SIG_RADIO_FREE, OnSigRadioFree)
	ON_BN_CLICKED(IDC_SIG_RADIO_INTSTART, OnSigRadioIntstart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSignalDlg message handlers

void CSignalDlg::OnOK() 
{
	UpdateData();
	CString strProhibit;
	if (m_pObject->m_nType == _GSHUNT) m_pObject->m_nLamp = m_Lamp.GetCurSel() + 10;
	else m_pObject->m_nLamp = m_Lamp.GetCurSel();
	if (m_bName1) m_pObject->m_nShape |= B_NAME_B;
	else m_pObject->m_nShape &= ~B_NAME_B;
	if (m_bName2) m_pObject->m_nShape |= B_NAME_S;
	else m_pObject->m_nShape &= ~B_NAME_S;
	if (m_nBlockadeType >= 0) m_pObject->m_nShape |= m_nBlockadeType << 4;
	else m_pObject->m_nShape &= ~B_MASKBLOCKADE;
	m_pObject->m_nShape &= ~(0x0c);
	if (m_bNoRoute) m_pObject->m_nShape |= B_NOROUTE;
	else m_pObject->m_nShape &= ~B_NOROUTE;
	if (m_bNoRouteAll) m_pObject->m_nShape |= B_NOROUTE_ALL;
	else m_pObject->m_nShape &= ~B_NOROUTE_ALL;
	if (m_bOverlapStop ) m_pObject->m_nShape |= B_OVERLAP_CK_STOP;
	else m_pObject->m_nShape &= ~B_OVERLAP_CK_STOP;
	if (m_bVirtualStraightLine ) m_pObject->m_nShape |= B_STRAIGHTLINE;
	else m_pObject->m_nShape &= ~B_STRAIGHTLINE;
	if ( m_bSlant == TRUE ) m_pObject->m_nUD = 1;
	GetDlgItem(IDC_EDT_PROHIBIT)->GetWindowText(strProhibit);
	m_pObject->s.m_sBlipName = strProhibit;
	GetDlgItem(IDC_STRAIGHT_SIGNAL)->GetWindowText(strStraightSignal);
	m_pObject->m_strStraightSignal = strStraightSignal;
	CDialog::OnOK();
}

//=======================================================
//
//  함 수 명 :  OnInitDialog()
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  다이알로그를 초기화한다.
//
//=======================================================
BOOL CSignalDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString strProhibit;

	if ((m_pObject->m_nType == _HOME || m_pObject->m_nType == _START || m_pObject->m_nType == _ISTART || m_pObject->m_nType == _OHOME || m_pObject->m_nType == _GSHUNT)) {
		EnableWindowBlockadeItem(TRUE);
		SetLamp(m_pObject->m_nType);
	}
	else 
		EnableWindowBlockadeItem(TRUE);

	if ( m_pObject->m_nType == _HOME || m_pObject->m_nType == _START || m_pObject->m_nType == _OHOME || m_pObject->m_nType == _GSHUNT) {
		EnableWindowRouteIndicator( FALSE );	
        EnableWindowRouteIndicator( TRUE , m_pObject->m_nType );
	} else {
        EnableWindowRouteIndicator( FALSE );	
	}

	if ( m_pObject->m_nUD ==1 ) m_bSlant = TRUE;

	strProhibit = m_pObject->s.m_sBlipName;
	GetDlgItem(IDC_EDT_PROHIBIT)->SetWindowText(strProhibit);
	strStraightSignal = m_pObject->m_strStraightSignal;
	GetDlgItem(IDC_STRAIGHT_SIGNAL)->SetWindowText(strStraightSignal);

	UpdateData (FALSE);
	return TRUE;
}

void CSignalDlg::OnSigRadioInBlockade() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator( FALSE );
}

void CSignalDlg::OnSigRadioOutBlockade() 
{
	EnableWindowBlockadeItem(FALSE);	
	EnableWindowRouteIndicator( FALSE );
}

void CSignalDlg::OnSigRadioInSignal() 
{
	EnableWindowRouteIndicator( TRUE );
	EnableWindowBlockadeItem( TRUE );
	SetLamp(_HOME);
}

void CSignalDlg::OnSigRadioShuntPG() 
{
	EnableWindowBlockadeItem(TRUE);
	EnableWindowRouteIndicator( FALSE );	
	SetLamp(_GSHUNT);
}

void CSignalDlg::OnSigRadioShuntSignal() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator( FALSE );		
}

void CSignalDlg::OnSigRadioOutSignal() 
{
	EnableWindowBlockadeItem(TRUE);
	EnableWindowRouteIndicator( FALSE );	
	EnableWindowRouteIndicator( TRUE , _START);		
	SetLamp( _START);
}

void CSignalDlg::OnSigRadioInStationBlockade() 
{
	EnableWindowBlockadeItem(TRUE);
	EnableWindowRouteIndicator(FALSE);
	EnableWindowRouteIndicator(TRUE , _OHOME);
	SetLamp( _OHOME);
}

void CSignalDlg::OnSigRadioThroughSignal() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator(FALSE);	
}

void CSignalDlg::OnSigRadioFarSignal() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator(FALSE);	
}

void CSignalDlg::OnSigRadioLos() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator(FALSE);		
}

//=======================================================
//
//  함 수 명 :  EnableWindowRouteIndicator
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기 속성인 인디케이터를 활성화/비활성화 한다.
//
//=======================================================
void CSignalDlg::EnableWindowRouteIndicator(BOOL bShow , int iSignal)
{	
	if ( iSignal == _HOME || iSignal == _OHOME || iSignal == _GSHUNT ) GetDlgItem(IDC_HOMESIGNALDIR1)->EnableWindow( bShow );
	if ( iSignal == _HOME || iSignal == _START || iSignal == _OHOME ) GetDlgItem(IDC_HOMESIGNALDIR2)->EnableWindow( bShow );
	if ( iSignal == _HOME || iSignal == _START || iSignal == _OHOME ) GetDlgItem(IDC_HOMESIGNALDIR3)->EnableWindow( bShow );
	if ( iSignal == _HOME || iSignal == _START || iSignal == _OHOME ) GetDlgItem(IDC_HOMESIGNALDIR4)->EnableWindow( bShow );
	if ( iSignal == _HOME || iSignal == _START || iSignal == _GSHUNT ) GetDlgItem(IDC_HOMESIGNALDIR5)->EnableWindow( bShow );
	
}

//=======================================================
//
//  함 수 명 :  EnableWindowBlockadeItem
//  함수출력 :  없음
//  함수입력 :  BOOL bShow, int nBlockadeType
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  
//
//=======================================================
void CSignalDlg::EnableWindowBlockadeItem(BOOL bShow, int nBlockadeType)
{
	GetDlgItem(IDC_SIGNALLAMP)->EnableWindow( bShow );
}

//=======================================================
//
//  함 수 명 :  OnHomesignaldir1()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기의 인디케이터를 없음으로 설정한다.
//
//=======================================================
void CSignalDlg::OnHomesignaldir1() 
{
	m_pObject->m_nILR = 0; // 없음
}

//=======================================================
//
//  함 수 명 :  OnHomesignaldir2()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기의 인디케이터를 왼쪽으로 설정한다.
//
//=======================================================
void CSignalDlg::OnHomesignaldir2() 
{
	m_pObject->m_nILR = 1; // 왼쪽
}

//=======================================================
//
//  함 수 명 :  OnHomesignaldir3()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기의 인디케이터를 오른쪽으로 설정한다.
//
//=======================================================
void CSignalDlg::OnHomesignaldir3() 
{
	m_pObject->m_nILR = 2; // 오른쪽
}

//=======================================================
//
//  함 수 명 :  OnHomesignaldir4()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기의 인디케이터를 양쪽으로 설정한다.
//
//=======================================================
void CSignalDlg::OnHomesignaldir4() 
{
	m_pObject->m_nILR = 3; // 양쪽
}

//=======================================================
//
//  함 수 명 :  OnHomesignaldir5()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  홈신호기의 속성중 문자표시기를 추가한다.
//
//=======================================================
void CSignalDlg::OnHomesignaldir5() 
{
	m_pObject->m_nILR = 4; // 문자표시기
}

void CSignalDlg::OnHomesignaldir6() 
{
	m_pObject->m_nILR = 5; // 문자표시기
}

//=======================================================
//
//  함 수 명 :  SetLamp()
//  함수출력 :  없음
//  함수입력 :  int iType
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  램프 설정을 한다.
//
//=======================================================
void CSignalDlg::SetLamp( int iType ) 
{
	m_Lamp.ResetContent();

	if ( iType == _HOME ) {
		m_Lamp.AddString( "4Lamp (C R Y G)" );
		m_Lamp.AddString( "4Lamp (S R Y G)" );
		m_Lamp.AddString( "3Lamp (C R Y)" );
		m_Lamp.AddString( "3Lamp (S R Y)" );
		m_Lamp.AddString( "5Lamp (S C R Y)" );
		m_Lamp.AddString( "2Lamp (R Y)" );
		m_Lamp.AddString( "2Lamp (R G)" );
		m_Lamp.AddString( "3Lamp (R Y G)" );
		m_Lamp.AddString( "5Lamp (S C R Y G)" );
	} else if ( iType == _OHOME ) {
		m_Lamp.AddString( "4Lamp (Y R Y G)" );
		m_Lamp.AddString( "3Lamp (Y R Y)" );		
		m_Lamp.AddString( "2Lamp (Y R )" );		
		m_Lamp.AddString( "2Lamp (R Y )" );		
		m_Lamp.AddString( "3Lamp (Y R G)" );		
		m_Lamp.AddString( "3Lamp (R Y G)" );		
	} else if ( iType == _START ) {
		m_Lamp.AddString( "3Lamp (R Y G)" );
		m_Lamp.AddString( "2Lamp (R Y)" );
		m_Lamp.AddString( "4Lamp (S R Y G)" );
		m_Lamp.AddString( "3Lamp (S R Y)" );
		m_Lamp.AddString( "3Lamp (S R G)" );
		m_Lamp.AddString( "2Lamp (S R)" );
		m_Lamp.AddString( "4Lamp (Y R Y G)" );
		m_Lamp.AddString( "2Lamp (R G)" );
	} else if ( iType == _ISTART ) {
		m_Lamp.AddString( "3Lamp (R Y G)" );
		m_Lamp.AddString( "2Lamp (R Y)" );
		m_Lamp.AddString( "4Lamp (S R Y G)" );
		m_Lamp.AddString( "3Lamp (S R Y)" );
		m_Lamp.AddString( "3Lamp (S R G)" );
		m_Lamp.AddString( "2Lamp (S R)" );
		m_Lamp.AddString( "4Lamp (Y R Y G)" );
		m_Lamp.AddString( "2Lamp (R G)" );
		m_Lamp.AddString( "3Lamp (*S R G)" );
	} else if ( iType == _GSHUNT ) {
		m_Lamp.AddString( "1Lamp (S)" );
		m_Lamp.AddString( "2Lamp (S F)" );
		m_Lamp.AddString( "3Lamp (S R)" );
	}

	if ( iType == _GSHUNT ) { 
		m_Lamp.SetCurSel(m_pObject->m_nLamp-10);
	} else {
		m_Lamp.SetCurSel(m_pObject->m_nLamp);
	}
}



void CSignalDlg::OnSigRadioFree() 
{
	EnableWindowBlockadeItem(FALSE);
	EnableWindowRouteIndicator(FALSE);			
}

void CSignalDlg::OnSigRadioIntstart() 
{
	EnableWindowBlockadeItem(TRUE);
	EnableWindowRouteIndicator( FALSE );	
	EnableWindowRouteIndicator( TRUE , _ISTART);		
	SetLamp( _ISTART);
}
