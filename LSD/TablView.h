//=======================================================
//==              TablView.h
//=======================================================
//	파 일 명 :  TablView.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.02
//	설    명 :  연동도표를 보여주는 뷰 
//
//=======================================================

#ifndef _TABLEVIEW_H         
#define _TABLEVIEW_H

#define MAXCOL	100
#define PAPERX  2355		// 2970 //4200	//8000	//4000
//#define PAPERX  2355		// 2970 //4200	//8000	//4000
#define PAPERY  1600		// 970	//2970	//1700
//#define PAPERY  29100		// 970	//2970	//1700
#define SCREENX 2355		// 2970 //4200	//8000	//4000
//#define SCREENX 2355		// 2970 //4200	//8000	//4000
//#define SCREENY 9100		// 970	//2970	//1700
#define SCREENY 29700		// 970	//2970	//1700
/////////////////////////////////////////////////////////////////////////////
// CTableView view
typedef struct ColInfotag {
	char *Head;
	int  Width, Type;
} ColInfo;

CString sPrintTime();

class PageInfo : public CObject {
public:
	CPoint base;
	BOOL m_nActivePageNo;
	int sy, hy;

	int m_nType;
	int m_iPageNo;

	PageInfo(PageInfo &info) {
		sy = info.sy;
		hy = info.hy;
		base = info.base;
		m_nActivePageNo = info.m_nActivePageNo;
		m_nType = info.m_nType;
		m_iPageNo = info.m_iPageNo;
	}
	PageInfo() {
		m_nActivePageNo = 0; 
		sy = 0;
		hy = 0;
		m_nType = 0;
		m_iPageNo = 0;
	}

};

class CTableView : public CScrollView
{
protected:

	CSize	m_cDocSize;
	CPoint	m_TablePos;
	CRect	m_Area;

	int		m_iCursorRow;
	int		m_iRows;
	int		m_iCols;
	int		m_iPages;
	int		m_iHeight;
	int		m_iCharWidth;
	int		m_iCrRow;
	int		m_iCrCol;
	int		m_iPageNo;

	BOOL	m_bIsFixed;
	BOOL	m_bViewAll;

	DECLARE_DYNCREATE(CTableView)

public:

	CButton*	m_pHide;
	POSITION	m_posPage;
	CObList		m_PageInfo;
	int			m_iTotPage;
	int			m_iZoomFactor;

	CTableView();           // protected constructor used by dynamic creation
	int			CalcPageAlign();
	int			GetHeight();
	void		PurgePageInfo();
	void		SetViewWindowSize();
	void		DocToClient(CRect &rect);
	void		DocToClient(CPoint &point);
	void		StrWrite(int x, int y, CString str, CDC* pDC, int iSzie = 10, int iDevid = 0 );

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTableView)
	public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DrawItem(CDC *pDC, int c, int r, int cx, int cy);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTableView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CTableView)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRegen();
	afx_msg void OnHide();
	afx_msg void OnEditIolist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
#endif
