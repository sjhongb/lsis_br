//=======================================================
//==              StObject.h
//=======================================================
//	파 일 명 :  StObject.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _STOBJECT_H
#define _STOBJECT_H

#include "../include/strlist.h"

class CStationObject {
public:
	void Sort();
	void AddTrack( CString &str );
	void AddSignal( CString &str );
	void AddSwitch( CString &str );
	void AddButton( CString &str );
	void AddLamp( CString &str );
	void AddCross( CString &str );
	CMyStrList m_Signal, m_Track, m_Switch, m_ArrivedButton, m_Lamp , m_Cross;
	CString m_StationName;
	CString m_Edit01,m_Edit02,m_Edit03,m_Edit04,m_Edit05;
	CString m_Edit06,m_Edit07,m_Edit08,m_Edit09,m_Edit10;
	CString m_Edit11,m_Edit12,m_Edit13,m_Edit14,m_Edit15;
	CString m_Edit16,m_Edit17,m_Edit18,m_Edit19;
	CString m_strLC,m_strBLK;
	virtual void Dump(CDumpContext& dc) const;
};

#endif