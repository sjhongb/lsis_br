//=======================================================
//====               Blip.cpp                        ==== 
//=======================================================
//  파  일  명: Blip.cpp
//  작  성  자: 김도영
//  작성  날짜: 2004.5.19
//  버      전: 2.01
//  설      명: 트랙 신호기등 좌표값을 가지는 객체의 속성
//              
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "blip.h"
#include "LSDView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void DrawRect(CDC *pDC,CRect &rect);
void DrawCircle(CDC *pDC,CRect &rect);

//=======================================================
//
//  함 수 명 :  DrawTracker
//  함수출력 :  없음
//  함수입력 :  CDC *pDC, TrackerState state
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면에 트랙커를 그린다.
//
//=======================================================
void CBlip::DrawTracker(CDC *pDC, TrackerState state) {
	if (m_bVisible) {
		CPen BluePen, *pOldPen = NULL, RedPen ;
		RedPen.CreatePen( PS_SOLID , 1, RGB(255,0,0)  );
		if (m_bActive) {
			CRect r = GetRect();
			pDC->LPtoDP( r );

			int nSaveDC = pDC->SaveDC();
			pDC->SetMapMode(MM_TEXT);
			r.NormalizeRect();
			pDC->DPtoLP( r );
			pDC->DrawFocusRect( r );

			pDC->RestoreDC( nSaveDC );
		}
		else {
			if( BluePen.CreatePen( PS_SOLID, 1, RGB(0,255,0) ) )
				pOldPen = pDC->SelectObject( &BluePen );
			if (pOldPen) {
				CRect r = GetRect();
				pOldPen = pDC->SelectObject( &RedPen );
				DrawCircle (pDC,r);
				pOldPen = pDC->SelectObject( &BluePen );
				pDC->SelectObject( pOldPen );
			}
		}
		pDC->SetBkMode(OPAQUE);
		if (m_sBlipName.GetLength() == 1) {
			CPoint p( x, y );
			TextOutAtPointCenter( pDC, p, m_sBlipName ,RGB(0,255,0) );
		}

	}
}

//=======================================================
//
//  함 수 명 :  MoveTo
//  함수출력 :  CPoint
//  함수입력 :  CPoint point
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  좌표를 point 만큼 이동한결과를 리턴한다.
//
//=======================================================
CPoint CBlip::MoveTo(CPoint point)
{
	CPoint rel;
	rel.x = point.x - x;
	rel.y = point.y - y;
	x = point.x;
	y = point.y;
	return rel;
}

//=======================================================
//
//  함 수 명 :  MoveToX
//  함수출력 :  int
//  함수입력 :  int nx
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  x좌표를 이동한다.
//
//=======================================================
int CBlip::MoveToX(int nx)
{
	int dx;
	dx = nx - x;
	x = nx;
	return dx;
}

void CBlip::Offset(const CPoint point, CLSDView* pView)
{
	x += point.x;
	y += point.y;
	if (m_pParent && pView) pView->InvalObj(m_pParent);
}

//=======================================================
//
//  함 수 명 :  HitTest
//  함수출력 :
//  함수입력 :
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
CBlip* CBlip::HitTest(CPoint point, CLSDView* pView, BOOL bSelected)
{
	CRect r = GetRect();
	if (r.PtInRect(point)) return this;
	return NULL;
}
