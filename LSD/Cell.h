//=======================================================
//====               Cell.cpp                        ==== 
//=======================================================
//  파  일  명: Cell.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 연동도표 화면 표시의 기본단위 
//              
//=======================================================

#ifndef _CELL_H
#define _CELL_H

#include "../include/StrList.h"

class CRow;
class CCell : public CRect {
protected:
public:
	CRow *m_pRow;
	BOOL CheckAreaAndEdit(int n, CString& name, CPoint point, CString& dif);
	static CPoint m_Org, m_Current;
	static int m_nTextH;
	BOOL m_bView;
	CSize m_Size;
	int m_nRows;
	CCell() {
		m_pRow = NULL;
		m_bView = TRUE;
		m_nRows = 0;
	}
public:
	static void SetOrg(CPoint p) {
		m_Org = p;
		m_Current = p;
	}
	static void SetCurrent(CPoint p) {
		m_Current = p;
	}
	int GetRows() { return m_nRows; }
	void SetView(BOOL t) {
		m_bView = t;
	}
	void ResetView() {
		m_bView = TRUE;
		m_Size.cy = 1;
	}
	void Locate() {
		left = m_Current.x;
		top = m_Current.y;
		right = left + m_Size.cx;
		bottom = top - m_Size.cy * m_nTextH;
	}
	void LocateRight() {
		Locate();
		m_Current.x = right;
	}
	void LocateDown() {
		Locate();
		m_Current.y = bottom;
	}
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CSize GetSize() {
		return CSize(100, 17);  // 셀테두리의 기본 크기  // 셀테두리 넓이 , 셀테두리 높이 m_iHeight
	}
	virtual void SetWidth(int w) {
		m_Size.cx = w;
	}
	void IncHeight() {
		m_Size.cy ++;
	}
	BOOL isValid() { return m_bView; }
	virtual CString CellEdit(CString& name) { 
		return name;
	}
	virtual void SetData( void *pObj ) {
	}
};

class CCommonCell : public CCell {
	CString* m_pStr;
public:
	CCommonCell() {}
	void SetData(void *pStr) {
		m_pStr = (CString *)pStr;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC *pDC, BOOL viewall = TRUE);
};

class CSplitCell : public CCell {
	CMyStrList* m_pData;
public:
	int m_pHeight[128];
	int m_nRows;
public:
	CSplitCell() {
		memset( m_pHeight, 0, sizeof( m_pHeight ) );
		m_nRows = 0;
	}
	void SetData( void* pdata) {
		m_pData = (CMyStrList*)pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC *pDC, BOOL viewall = TRUE);
};

class CLockCell : public CCell {
	CMyStrList* m_pData;
public:
	CLockCell() {}
	void SetData( void* pdata ) {
		m_pData = (CMyStrList*) pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CPointLockCell : public CCell {
	CMyStrList* m_pDataT;

public:
	CPointLockCell() {}
	void SetData( void* pdataT) {
		m_pDataT = (CMyStrList*) pdataT;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CTrackCell : public CCell {
	CMyStrList* m_pData;
public:
	CTrackCell() {}
	void SetData( void* pdata) {
		m_pData = (CMyStrList* )pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CTrackClearCell : public CCell {
	CMyStrList* m_pData;
public:
	CTrackClearCell() {}
	void SetData( void* pdata) {
		m_pData = (CMyStrList* )pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CApproachCell : public CCell {
	CMyStrList* m_pData;
public:
	CApproachCell() {}
	void SetData( void* pdata) {
		m_pData = (CMyStrList* )pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CTcOCCCell : public CCell {
	CMyStrList* m_pData;
public:
	CTcOCCCell() {}
	void SetData( void* pdata) {
		m_pData = (CMyStrList* )pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};

class CRouteCell : public CCell {
	CMyStrList* m_pData;
public:
	CRouteCell() {}
	void SetData(CMyStrList* pdata) {
		m_pData = (CMyStrList* )pdata;
		m_Size = GetSize();
	}
public:
	virtual void Draw(CDC* pDC, BOOL viewall = TRUE);
	virtual CString CellEdit(CString& name);
};
#endif