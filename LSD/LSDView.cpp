//=======================================================
//==              LSDView.cpp
//=======================================================
//	파 일 명 :  LSDView.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "LSDDoc.h"
#include "LSDView.h"
#include "Track.h"
#include "Signal.h"
#include "MyObject.h"
#include "Station.h"
#include "Develop.h"
#include "TableDoc.h"
#include "TablView.h"
#include "ObjDlg.h"
#include "DotChange.h"
#include "../include/Parser.h"

#define DV 100

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLSDView

IMPLEMENT_DYNCREATE(CLSDView, CScrollView)

BEGIN_MESSAGE_MAP(CLSDView, CScrollView)
	//{{AFX_MSG_MAP(CLSDView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_TRACKINS, OnTrackins)
	ON_COMMAND(ID_SIGNALINS, OnSignalins)
	ON_COMMAND(ID_BUTTONINS, OnButtonins)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CHAR()
	ON_COMMAND(ID_TRACKDEL, OnTrackdel)
	ON_WM_SIZE()
	ON_COMMAND(ID_STATIONINFO, OnStationinfo)
	ON_COMMAND(ID_TESTMODE, OnTestmode)
	ON_COMMAND(ID_SELECTALL, OnSelectall)
	ON_COMMAND(ID_EXPORT, OnExport)
	ON_COMMAND(ID_VIEW_SYSTEM_OBJ, OnViewSystemObj)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SYSTEM_OBJ, OnUpdateViewSystemObj)
	ON_COMMAND(ID_CHANGE_DOT, OnChangeDot)
	ON_COMMAND(ID_RUN_MSTVIEW, OnRunMstview)
	ON_COMMAND(ID_SFMVIEW, OnSfmview)
	ON_UPDATE_COMMAND_UI(ID_ZOOMIN, OnUpdateZoomin)
	ON_UPDATE_COMMAND_UI(ID_ZOOMOUT, OnUpdateZoomout)
	ON_COMMAND(ID_ZOOMIN, OnZoomin)
	ON_COMMAND(ID_ZOOMOUT, OnZoomout)
	ON_COMMAND(ID_DEVELOP, OnDevelop)
	ON_COMMAND(ID_UNDO, OnUndo)
	ON_COMMAND(ID_COPY, OnCopy)
	ON_COMMAND(ID_PASTE, OnPaste)
	ON_COMMAND(ID_CUT, OnCut)
	ON_WM_ERASEBKGND()
	ON_UPDATE_COMMAND_UI(ID_PRINT, OnUpdatePrint)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW, OnUpdatePreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLSDView construction/destruction

extern const char *stringSystemButton[];
extern const char *stringOptionButton[];
extern const char *stringBlockButton[];
extern const char *stringLampButton[];

////////////////  extern     bmpdraw.cpp
extern int _UseSmallText;
extern float VIEW_XRATE;			    // x * VIEW_XRATE 
extern float VIEW_YRATE;			    // y * VIEW_XRATE
extern int _nSFMViewXSize; 
extern int   OFFSET_X;
extern int   OFFSET_Y;
extern int   REVERSE;
extern UINT  CELLSIZE_Y; 
extern UINT  CELLSIZE_X; 
extern int mLogX, mLogY;
extern int _nDotPerCell;
extern SymbolBitmapInfo *SymbolInfo;
extern SymbolBitmapInfo SymbolInfo10[];

BOOL CheckMode = FALSE;
enum SelectMode { none, netSelect, move, size };
SelectMode selectMode = none;
CBlip* nDragHandle;
CPoint onGridLastPoint;
CPoint noGridLastPoint;

CLSDView::CLSDView()
{
	// TODO: add construction code here
	m_bDragMode      = FALSE;
	m_bGrid          = TRUE;
	m_nGridSize      = GridSize;
	m_nViewMode      = VIEWMODE_NORMAL;
	m_TablePos       = CPoint(0,0);
	m_nSFMViewXSize  = 2048;
	m_nSFMViewYSize  = 768;
	m_nSystemYPos    = 25;
	m_nZoomFactor    = 100;
	m_rtZoomFactor   = 100;
	m_nCheckPathNo   = 0;
	m_DesCX, m_DesCY = 0;
	m_pRouteDialog   = NULL;
}

CLSDView::~CLSDView()
{
	RemoveCheckPath();
	PurgePageInfo();
	if (m_pRouteDialog) {
		delete m_pRouteDialog;
		m_pRouteDialog = NULL;
	}
	if (m_pTrackDoc){
//		delete m_pTrackDoc;
	}
}

void CLSDView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	// TODO: Add your specialized code here and/or call the base class
	SetViewWindowSize();
	CLSDDoc* pDoc = GetDocument();
	pDoc->m_iUnDoCnt[0][0] = 0;
	pDoc->m_iUnDoCnt[0][1] = 0;
	pDoc->m_objbak1.RemoveAll();
	pDoc->m_objbak2.RemoveAll();
	pDoc->m_objbak3.RemoveAll();
	m_iSelectObjCnt = 0;
	m_pTrackDoc = new CBmpTrackCon( this, pDoc->m_DotInfo.nDot );
}

BOOL CLSDView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLSDView drawing


void CLSDView::OnDraw(CDC* pDC)
{
	GetDocument()->m_bSFMMode = m_nViewMode == VIEWMODE_SCREEN;
	CheckMode = (m_nViewMode == VIEWMODE_ROUTE);
	if ( CheckMode ) OnTestDraw(pDC);
	else DrawPage(pDC);
}

void CLSDView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLSDView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CLSDView diagnostics

#ifdef _DEBUG
void CLSDView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CLSDView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CLSDDoc* CLSDView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLSDDoc)));
	return (CLSDDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLSDView message handlers

void CLSDView::GridPoint(CPoint &point) {
	int x = point.x;
	int y = point.y;
	if (x>0) x += m_nGridSize/2;
	else x -= m_nGridSize/2;
	if (y>0) y += m_nGridSize/2;
	else y -= m_nGridSize/2;
	x /= m_nGridSize;
	y /= m_nGridSize;
	x *= m_nGridSize;
	y *= m_nGridSize;
	point.x = x;
	point.y = y;
}

void CLSDView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if (!m_bActive) return;
	if ( m_nViewMode == VIEWMODE_SCREEN && m_nZoomFactor != 100 ) return; //YN >> 2001.6.13 LSD 상에서 zoom 작업을 했을경우만 sfm 상에서 수정불가.( ADD Code )

	CPoint        local = point;
	CBlip        *Handle;
	CGraphObject *pObj;

	ClientToDoc(local);
	GridPoint(local);
	selectMode = none;
	Select(NULL);

	pObj = GetDocument()->ObjectAt(local);

	if (pObj != NULL) {
		m_iSelectObjCnt = 1;
		GetDocument()->SaveItem( pObj , 3 , m_iSelectObjCnt);	
		Handle = pObj->HitTest(local, this, TRUE);
		if (Handle) {
			if (strcmp(Handle->NameOf(),"BLIP")) Handle->PropertyChange(this);
			else pObj->PropertyChange(this);
		}
		GetDocument()->SetModifiedFlag();
		InvalidateRect(NULL,FALSE);
	}
}

void CLSDView::ViewRouteCheck(CPoint  local) 
{
	CGraphObject* pObj;
	int cnr;

	pObj = GetDocument()->ObjectAtButton(local, cnr);

	if (pObj != NULL) {
		if (m_CheckTrack != (CTrack*)pObj) {
			m_nPathNo = 0;
			RemoveCheckPath();
			m_CheckTrack = (CTrack*)pObj;
			m_nCheckCNR = cnr;
			CreateCheckPath();
		} else {
			m_nPathNo++;
			if ( m_nPathNo >= m_nLastPathNo ) m_nPathNo = 0;
			Invalidate();
		}
	} else {
		m_CheckTrack = NULL;
		Invalidate();
	}
}

BOOL CLSDView::CheckObectSizeChange(CPoint  local) 
{
	CGraphObject* pObj;
	BOOL bRetCode;
	CString strObjName;
	CString strObjType;

	if (m_selection.GetCount() == 1) {
		pObj = (CGraphObject*)m_selection.GetHead();
	    strObjName = pObj->NameOf();

		if ( strObjName == "MyObject"){
			strObjType = ((CMyObject*)pObj)->Name;
			if ( strObjType == "LSCI" || strObjType == "LGCI") return FALSE;
//			if ( strObjType == "BRIDGE") return FALSE;
			if ( strObjType == "AXLE-COUNTER") return FALSE;
			if ( strObjType == "LCA") return FALSE;
			if ( strObjType == "WARNINGBOARD(L)") return FALSE;
			if ( strObjType == "WARNINGBOARD(R)") return FALSE;
			if ( strObjType == "COUNTER") return FALSE;
			if ( strObjType == "SYSTEM-BUTTON") return FALSE;
			if ( strObjType == "OPTION-BUTTON") return FALSE;
			if ( strObjType == "BLOCK-BUTTON") return FALSE;
			if ( strObjType == "PAS") return FALSE;
			if ( strObjType == "TOGGLE-BUTTON") return FALSE;
			if ( strObjType == "NIGHT/DAY") return FALSE;
			if ( strObjType == "RGB") return FALSE;
			if ( strObjType == "OSS-BUTTON") return FALSE;
			if ( strObjType == "EPK-BUTTON" || strObjType == "APK-BUTTON") return FALSE;
			if ( strObjType == "BLOCK-CLEAR-BUTTONL") return FALSE;
			if ( strObjType == "BLOCK-CLEAR-BUTTONR") return FALSE;
			if ( strObjType == "ROAD_WARNER") return FALSE;
			if ( strObjType == "GATE_LODGE") return FALSE;
			if ( strObjType == "X_MARK") return FALSE;
		}
		nDragHandle = pObj->HitTest(local, this, TRUE);
		if (nDragHandle != NULL && nDragHandle != pObj) {
			bRetCode = TRUE;
			m_iSelectObjCnt = 1;
		    m_iModiFlag = 1;
		    GetDocument()->SaveItem( pObj , 3 , m_iSelectObjCnt);
		} else {
			bRetCode = FALSE;
		}
	}
	return bRetCode;
}

BOOL CLSDView::CheckObectMoveChange(CPoint  local , UINT nFlags) 
{
	CGraphObject* pObj;
	BOOL bRetCode;
	POSITION pos;

	pObj = GetDocument()->ObjectAt(local);

	if (pObj != NULL) {
		if (!IsSelected(pObj)) 	Select(pObj, (nFlags & MK_CONTROL)!=0);
		if ((nFlags & MK_SHIFT)!=0) CloneSelection();
			bRetCode = TRUE;
			m_iSelectObjCnt = 1;
			pos = m_selection.GetTailPosition();
			GetDocument()->SaveItem( pObj , 3 ,m_iSelectObjCnt);
			if ( m_selection.GetCount() > 1 ) {
				while (pos!=NULL) {
					pObj = (CGraphObject*)m_selection.GetPrev(pos);
					GetDocument()->SaveItem( pObj , 3 ,m_iSelectObjCnt);
					m_iSelectObjCnt++;
				}
			}
		    m_iModiFlag = 1;
	} else {
		bRetCode = FALSE;	
	}

	return bRetCode;
}

void CLSDView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (!m_bActive) return;
	if ( m_nViewMode == VIEWMODE_SCREEN && m_nZoomFactor != 100 )return;    //LSD 상에서 zoom 작업을 했을경우만 sfm 상에서 수정불가.( ADD Code )

	CPoint  local = point;
	ClientToDoc(local);

	selectMode = none;

	GetDocument()->m_testFlag = 1;
	if ( m_nViewMode == VIEWMODE_ROUTE ) { ViewRouteCheck(local); return; }

	noGridLastPoint = local;
	GridPoint( local );

	if      ( CheckObectSizeChange( local ) == TRUE ) selectMode = size;
	else if ( CheckObectMoveChange( local , nFlags) == TRUE ) selectMode = move;
	else { Select(NULL);  selectMode = netSelect; }

	onGridLastPoint = local;

	SetCapture();
	c_nDownFlags = nFlags;
	c_down = point;
	c_last = point;
}

void CLSDView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (GetCapture() != this) {
		return;
	}
	CLSDDoc* pDoc = GetDocument();

	if (selectMode == netSelect) {
		CClientDC dc(this);
		CRect rect(c_down.x,c_down.y,c_last.x,c_last.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);
		rect.SetRect (c_down.x,c_down.y,point.x,point.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);

		c_last = point;
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		return;
	}

	CPoint local = point;
	ClientToDoc(local);
	CPoint nogriddelta = (CPoint)(local - noGridLastPoint);
	noGridLastPoint = local;
	GridPoint( local );
	CPoint delta = (CPoint)(local - onGridLastPoint);
	
	POSITION pos = m_selection.GetHeadPosition();
	while (pos!=NULL) {
		CGraphObject* pObj = (CGraphObject*)m_selection.GetNext(pos);
		CRect position = pObj->m_position;
		if (selectMode == move) {
			m_iModiFlag = 2;
			pObj->Offset(delta, this);
		}
		else if (nDragHandle != NULL) {

			BOOL bNoGrid = FALSE;
			if (pObj->IsKindOf(RUNTIME_CLASS(CTrack))) {
				bNoGrid = ((CTrack*)pObj)->isNoGrid( nDragHandle );
			}
			else if (pObj->IsKindOf(RUNTIME_CLASS(CSignal))) {
				bNoGrid = ((CSignal*)pObj)->isNoGrid( nDragHandle );
			}
			if ( bNoGrid ) {
				pObj->MoveHandleTo(nDragHandle, nogriddelta, this);
				m_iModiFlag = 2;

			}else { 
				pObj->MoveHandleTo(nDragHandle, delta, this);
			    m_iModiFlag = 2;
			}
		}
	}
	onGridLastPoint = local;
	if (selectMode == size) {
		c_last = point;
		return;
	}
	c_last = point;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

void CLSDView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (GetCapture() == this) {
		if (selectMode == netSelect) {
			CClientDC dc(this);
			CRect rect(c_down.x,c_down.y,c_last.x,c_last.y);
			rect.NormalizeRect();
			dc.DrawFocusRect(rect);
			
			SelectWithinRect(rect, TRUE);
		}
		else if (selectMode != none) {
			GetDocument()->UpdateAllViews( this );
		}
	}
	ReleaseCapture();
	if ( m_iModiFlag == 1) {
		GetDocument()->DeleteItem();
		m_iModiFlag = 0;
	} else if ( m_iModiFlag == 2) {
	
	};
    selectMode = none;
}

void CLSDView::DrawGrid(CDC *pDC)
{

	CRect client;
	pDC->GetClipBox(client);
	CRect rect = client;
	rect.NormalizeRect();

	CPen newPen; // 가운데 라인
	newPen.CreatePen( PS_SOLID, 1, RGB(98,146,204) );
    CPen* pOldPen = pDC->SelectObject( &newPen );

	CPoint p1( rect.left, rect.top ), p2( rect.right, rect.bottom );
	GridPoint( p1 );
	GridPoint( p2 );

	CPen gridPen;
	gridPen.CreatePen( PS_SOLID, 1, RGB(206,212,255) );
    pDC->SelectObject( &gridPen );

	for (int y=p1.y; y<(p2.y+m_nGridSize); y+=m_nGridSize) {
		for (int x=p1.x; x<(p2.x+m_nGridSize); x+=m_nGridSize) {
			pDC->MoveTo(x,(p1.y-m_nGridSize));
  	        pDC->LineTo(x,(p2.y+m_nGridSize));
		}
		pDC->MoveTo((p1.x-m_nGridSize),y);
  	    pDC->LineTo((p2.x+m_nGridSize),y);
	}

	pDC->SelectObject( &newPen );

	pDC->MoveTo(0,rect.top);
	pDC->LineTo(0,rect.bottom);
	pDC->MoveTo(rect.left,0);
	pDC->LineTo(rect.right,0);
    pDC->SelectObject( pOldPen );
}

void CLSDView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	m_pMenuPoint = point;
	ClientToDoc(m_pMenuPoint);
	GridPoint(m_pMenuPoint);
	CRect rect;
	GetWindowRect( &rect );
	rect.OffsetRect( point );
	CMenu menu;
	menu.CreatePopupMenu();
	menu.InsertMenu(0, MF_STRING, ID_TRACKINS, "TRACK INS");
	menu.InsertMenu(1, MF_STRING, ID_TRACKDEL, "DELETE");
	menu.InsertMenu(2, MF_STRING, ID_BUTTONINS, "INSERT");
	menu.TrackPopupMenu(TPM_LEFTALIGN + TPM_RIGHTBUTTON, rect.left, rect.top, this);
	CScrollView::OnRButtonUp(nFlags, point);
}

void CLSDView::OnTrackins() 
{
	CLSDDoc* pDoc = GetDocument();
	CRect rect;

	if ( m_pMenuPoint.x < 0 || m_pMenuPoint.y < 0 ) { 
		m_pMenuPoint.x = 100;
	    m_pMenuPoint.y = 60;
	    ClientToDoc(m_pMenuPoint);
	    GridPoint(m_pMenuPoint);
	    GetWindowRect( &rect );
	}


	CGraphObject* pObj = new CTrack(0, m_pMenuPoint.x, m_pMenuPoint.y);
	pDoc->Add( pObj );
	TRACE("add 1\n");
	InvalObj( pObj );
}

void CLSDView::OnSignalins() 
{
	CLSDDoc* pDoc = GetDocument();
	GridPoint(m_pMenuPoint);
	pDoc->Add( new CSignal(0, m_pMenuPoint.x, m_pMenuPoint.y) );
	Invalidate();
}

void CLSDView::OnButtonins() 
{
	CLSDDoc* pDoc = GetDocument();
	CRect	rect;	
	CString objname;
	CObjDlg dlg(objname);
	dlg.DoModal();

	if ( m_pMenuPoint.x < 0 || m_pMenuPoint.y < 0 ) { 
		m_pMenuPoint.x = 100;
	    m_pMenuPoint.y = 60;
	    ClientToDoc(m_pMenuPoint);
	    GridPoint(m_pMenuPoint);
	    GetWindowRect( &rect );
	}

	if (objname != "") {
		if ((objname == "TOKENBLOCK(LEFT)") || (objname == "TOKENBLOCK(RIGHT)") ) {
			CString s;
			if (objname == "TOKENBLOCK(LEFT)") s = pDoc->m_FromStation;
			else s = pDoc->m_ToStation;
			
			int n, find =0;
			do {
				n = s.Find(',');
				if (n >= 0) {
					CString imsi = s.Mid(n+1);
					s = imsi;
					find++;
				}
			}while( n != -1 );
			for(n=0; n<=find; n++) {
				CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
				((CMyObject*)pObj)->m_nType = n+1;
				((CMyObject*)pObj)->m_nShape |= (n+1) << 1;
				if ( ((CMyObject*)pObj)->m_Front.m_sBlipName == "" )
				{
					((CMyObject*)pObj)->m_Front.m_sBlipName = objname; 
					((CMyObject*)pObj)->m_Rear.m_sBlipName  = "Km.0.00";
				}
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
			}
		}
		else if ((objname == "TOKENLESSBLOCK(LEFT)") || (objname == "TOKENLESSBLOCK(RIGHT)") ) {
			CString s;
			if (objname == "TOKENLESSBLOCK(LEFT)") {
				s = pDoc->m_FromStation;
			} else { 
				s = pDoc->m_ToStation;
			}
			
			int n, find =0;
			do {
				n = s.Find(',');
				if (n >= 0) {
					CString imsi = s.Mid(n+1);
					s = imsi;
					find++;
				}
			}while( n != -1 );
			for(n=0; n<=find; n++) {
				CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
				((CMyObject*)pObj)->m_nType = n+1;
				((CMyObject*)pObj)->m_nShape |= (n+1) << 1;
				if ( s == "" )
				{
					((CMyObject*)pObj)->m_Front.m_sBlipName = objname; 
					((CMyObject*)pObj)->m_Rear.m_sBlipName  = "Km.0.00";
				}
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
			}
		}
		else if ((objname == "DIRECTION(LEFT)") || (objname == "DIRECTION(RIGHT)") ) {
			CString s;
			if (objname == "DIRECTION(LEFT)") {
				s = pDoc->m_FromStation;
			} else { 
				s = pDoc->m_ToStation;
			}
			
			int n, find =0;
			do {
				n = s.Find(',');
				if (n >= 0) {
					CString imsi = s.Mid(n+1);
					s = imsi;
					find++;
				}
			}while( n != -1 );
			for(n=0; n<=find; n++) {
				CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
				((CMyObject*)pObj)->m_nType = n+1;
				((CMyObject*)pObj)->m_nShape |= (n+1) << 1;
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
			}
		}
		else if(objname == "SYSTEM-BUTTON") {
			CLSDDoc* pDoc = GetDocument();
			pDoc->m_bOnViewSystemButton = TRUE;
			int x = m_pMenuPoint.x;
			int y = m_pMenuPoint.y;

			int i = 0;
			while ( stringSystemButton[i] ) {
				CGraphObject* pObj = new CMyObject(objname, x, y, i);
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
				x += 100;
				i++;
			}
		}
		else if(objname == "OPTION-BUTTON") {
			CLSDDoc* pDoc = GetDocument();
			pDoc->m_bOnViewSystemButton = TRUE;
			int x = m_pMenuPoint.x;
			int y = m_pMenuPoint.y;

			int i = 0;
			while ( stringOptionButton[i] ) {
				CGraphObject* pObj = new CMyObject(objname, x, y, i);
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
				x += 100;
				i++;
			}
		}
		else if(objname == "BLOCK-BUTTON") {
			CLSDDoc* pDoc = GetDocument();
			pDoc->m_bOnViewSystemButton = TRUE;
			int x = m_pMenuPoint.x;
			int y = m_pMenuPoint.y;

			int i = 0;
			while ( stringBlockButton[i] ) {
				CGraphObject* pObj = new CMyObject(objname, x, y, i);
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
				x += 100;
				i++;
			}
		}
		else if(objname == "LAMP-BUTTON") {
			CLSDDoc* pDoc = GetDocument();
			pDoc->m_bOnViewSystemButton = TRUE;
			int x = m_pMenuPoint.x;
			int y = m_pMenuPoint.y;

			int i = 0;
			while ( stringLampButton[i] ) {
				CGraphObject* pObj = new CMyObject(objname, x, y, i);
				pDoc->Add( pObj );
				pObj->Active(TRUE);
				InvalObj( pObj );
				x += 100;
				i++;
			}
		}
		else if(objname == "RGB") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "RGB"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 231 0 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "PAS") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "PAS"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 224 1 G 32840 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "HOME" || objname == "PLATFORM") {

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "PLATFORM"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "12(3456.7)89";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "LEVELCROSS") {

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "LEVELCROSS"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 253 1 G 33121 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );

			objname = "LCA";
			pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "LCA"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 254 1 G 33122 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "FLEVELCROSS") {

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "FLEVELCROSS"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 253 1 G 33121 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "OSS-BUTTON") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "OSS"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 236 1 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "BLOCK-CLEAR-BUTTONL") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "BCBL"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 256 1 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "BLOCK-CLEAR-BUTTONR") {
			pDoc->m_bOnViewSystemButton = TRUE;
			
			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "BCBR"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 257 1 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "AXLE-COUNTER") {
			pDoc->m_bOnViewSystemButton = TRUE;
			
			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "AX"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 268 1 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}									// 2012.08.27 Axle Counter 추가
		else if(objname == "EPK-BUTTON" || objname == "APK-BUTTON") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "EPK"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 251 1 G 32882 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "TOGGLE-BUTTON") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "COS"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 249 1 G 0 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else if(objname == "NIGHT/DAY") {
			pDoc->m_bOnViewSystemButton = TRUE;

			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			((CMyObject*)pObj)->m_Front.m_sBlipName = "NIGHT/DAY"; 
			((CMyObject*)pObj)->m_Rear.m_sBlipName  = "5 225 1 G 32879 R";
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}
		else {
			if (objname == "TEXT") {
				objname += ".NEW_TEXT";
			}
			CGraphObject* pObj = new CMyObject(objname, m_pMenuPoint.x, m_pMenuPoint.y);
			pDoc->Add( pObj );
			pObj->Active(TRUE);
			InvalObj( pObj );
		}

	}
}

void CLSDView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CScrollView::OnChar(nChar, nRepCnt, nFlags);
}

void CLSDView::OnTrackdel() 
{
	if (!m_bActive) return;
	CLSDDoc* pDoc = GetDocument();
	if (m_selection.IsEmpty() == TRUE) return;
	CString Name;
	int iCnt = 0;
	int iDelCnt = 1;
	POSITION pos = m_selection.GetHeadPosition();
	iDelCnt = m_selection.GetCount();

	while (pos != NULL) {
		iCnt ++;
		CGraphObject* pObj = (CGraphObject*)m_selection.GetNext(pos);
		Remove(pObj);
		if (pObj->IsKindOf(RUNTIME_CLASS(CTrack))) {
			CTrack *trk = (CTrack*)pObj;					
			if ( trk->isDoubleSlip() ) {
				POSITION pos1 = m_selection.Find(trk->m_pDoubleSlip);
				if ( pos1 ) {
					if (pos == pos1) m_selection.GetNext(pos);
					m_selection.RemoveAt(pos1);
				}
				Remove((CGraphObject*)trk->m_pDoubleSlip);
				pDoc->Remove((CGraphObject*)trk->m_pDoubleSlip);
			}
		}


		Name = pObj->GetName();
		pDoc->Remove(pObj , iDelCnt );
		pDoc->MarkingToTrackOrders( Name.GetBuffer(1) );
	}

	InvalidateRect(NULL,FALSE);
}

void CLSDView::SetPageSize(CSize size)
{
	CClientDC dc(NULL);
	size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX),DV);
	size.cy = MulDiv(size.cy, dc.GetDeviceCaps(LOGPIXELSY),DV);
	SetScrollSizes(MM_TEXT, size);
	GetDocument()->UpdateAllViews(NULL, HINT_UPDATE_WINDOW, NULL);
}

void CLSDView::DocToClient(CRect &rect)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&rect);
}

void CLSDView::DocToClient(CPoint &point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&point);
}

void CLSDView::ClientToDoc(CRect &rect)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.DPtoLP(&rect);
}

void CLSDView::ClientToDoc(CPoint &point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.DPtoLP(&point);
}

void CLSDView::Select(CGraphObject *pObj, BOOL bAdd)
{
	if (!bAdd) {
		OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
		m_selection.RemoveAll();
	}
	if (pObj == NULL || IsSelected(pObj)) return;
    m_selection.AddTail(pObj);
	InvalObj(pObj);
}

void CLSDView::SelectWithinRect(CRect rect, BOOL bAdd)
{
	if (!bAdd) Select(NULL);
	ClientToDoc(rect);
	CObList* pObList = GetDocument()->GetObjects();
	POSITION pos = pObList->GetHeadPosition();
	while (pos!=NULL) {
		CGraphObject* pObj = (CGraphObject*)pObList->GetNext(pos);
		if (pObj->Intersects(rect)) Select(pObj, TRUE);
	}
}

void CLSDView::Deselect(CGraphObject *pObj)
{
	POSITION pos = m_selection.Find(pObj);
	if (pos != NULL) {
		InvalObj(pObj);
		m_selection.RemoveAt(pos);
	}
}


void CLSDView::CloneSelection()
{
	POSITION pos = m_selection.GetHeadPosition();
	while (pos != NULL) {
		CGraphObject* pObj = (CGraphObject*)m_selection.GetNext(pos);
		pObj->Clone(pObj->m_pDocument);
	}
}

void CLSDView::UpdateActiveItem()
{

}

void CLSDView::InvalObj(CGraphObject *pObj)
{
	CRect rect = pObj->GetRect();
	DocToClient(rect);
	rect.NormalizeRect();
	rect.InflateRect(10,10);
	InvalidateRect(rect, FALSE);
}

void CLSDView::Remove(CGraphObject *pObj)
{
	POSITION pos = m_selection.Find(pObj);
	if (pos != NULL) m_selection.RemoveAt(pos);
}

void CLSDView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch (lHint) {
	case HINT_UPDATE_WINDOW:
		Invalidate(FALSE);
		break;
	case HINT_UPDATE_DRAWOBJ:
		InvalObj((CGraphObject*)pHint);
		break;
	case HINT_UPDATE_SELECTION: 
		{
			CObList* pList = pHint != NULL ? (CObList*)pHint : &m_selection;
			POSITION pos = pList->GetHeadPosition();
			while (pos!=NULL) InvalObj((CGraphObject*)pList->GetNext(pos));
		}
		break;
	case HINT_DELETE_SELECTION:
		if (pHint != &m_selection) {
			CObList* pList = (CObList*)pHint;
			POSITION pos = pList->GetHeadPosition();
			while (pos!=NULL) {
				CGraphObject* pObj = (CGraphObject*)pList->GetNext(pos);
				InvalObj(pObj);
				Remove(pObj);
			}
		}
		break;
	}
}

void CLSDView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	CScrollView::OnActivateView(bActivate, pActivateView, pDeactiveView);
	if (m_bActive != bActivate) {
		if (bActivate) m_bActive = bActivate;
		if (!m_selection.IsEmpty()) OnUpdate(NULL, HINT_UPDATE_SELECTION, NULL);
		m_bActive = bActivate;
	}
}

void CLSDView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	CLSDDoc *pDoc = GetDocument();

	pDC->SetMapMode(MM_ANISOTROPIC);
	int x = pDC->GetDeviceCaps(LOGPIXELSX);
	int y = pDC->GetDeviceCaps(LOGPIXELSY);
	CPoint ptOrg;
	ptOrg.x = pDoc->GetSize().cx / 2;
	ptOrg.y = pDoc->GetSize().cy / 2;

	if ( m_nViewMode == VIEWMODE_SCREEN ) {
		x = y = pDoc->m_DotInfo.nDot * 10;
		x /= -2;
		y = -y;
		if ( REVERSE != 1 ) {
			ptOrg.x *= -1;
			ptOrg.y *= -1;
		}
		else {
			x = -x;
			y = -y;
		}
	}

	pDC->SetWindowExt( m_nZoomFactor, -m_nZoomFactor );
	pDC->SetViewportExt(x, y);
	pDC->OffsetWindowOrg(-ptOrg.x,ptOrg.y);
}

//=======================================================
//
//  함 수 명 :  OnSize
//  함수출력 :  없음
//  함수입력 :  UINT nType, int cx, int cy
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 사이즈가 변화 할때 처리
//
//=======================================================
void CLSDView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	UpdateActiveItem();
}

BOOL CLSDView::IsSelected(const CObject* pDocItem) const
{
	POSITION pos = m_selection.Find((CGraphObject*)pDocItem);
	return pos != NULL;
}

void CLSDView::OnStationinfo() 
{
	CStationInfo dlg(GetDocument());
	dlg.DoModal();
	GetDocument()->SetModifiedFlag();
	Invalidate();
}

void CLSDView::OnTestDraw(CDC *pDC)
{
	CLSDDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CDC dc;
	CDC* pDrawDC = pDC;
	CBitmap bitmap;
	CBitmap* pOldBitmap;

	CRect client;
	pDC->GetClipBox(client);
	CRect rect = client;
	DocToClient(rect);

	int nSaveDC = pDC->SaveDC();

	if (!pDC->IsPrinting()) {
		if (dc.CreateCompatibleDC(pDC)) {
			if (bitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height())) {
				OnPrepareDC(&dc,NULL);
				pDrawDC = &dc;
				dc.OffsetViewportOrg(-rect.left,-rect.top);
				pOldBitmap = dc.SelectObject(&bitmap);
				dc.SetBrushOrg(rect.left % 8,rect.top % 8);
				dc.IntersectClipRect(client);
				CFont font;
				font.CreateFont(14,10,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
				dc.SelectObject(&font);
			}
		}
	}

	CBrush brush;
	if (!brush.CreateSolidBrush(RGB(255,255,255))) return;
	brush.UnrealizeObject();
	pDrawDC->FillRect(client, &brush);

    pDoc->Draw( pDrawDC, this );

	if (m_CheckPath.GetCount()) {
		int nPathNo = 1;
		CPen newPen;
		newPen.CreatePen( PS_SOLID, 6, RGB(0,255,0) );
		CPen* pOldPen = pDrawDC->SelectObject( &newPen );
		POSITION pos = m_CheckPath.GetHeadPosition();
		BOOL first = TRUE;
		while ( pos != NULL )
		{
			CPoint *p = (CPoint *)m_CheckPath.GetNext( pos );
			if (first) {
				if ( m_nPathNo == 0 || m_nPathNo == nPathNo ) 
					pDrawDC->MoveTo(*p);
				first = FALSE;
			}
			else if (p->x != 10000) {
				if ( m_nPathNo == 0 || m_nPathNo == nPathNo )
					pDrawDC->LineTo(*p);
			}
			else {
				first = TRUE;
				nPathNo++;
			}
		}
		m_nLastPathNo = nPathNo;
	    pDrawDC->SelectObject( pOldPen );
	}

	if (pDrawDC != pDC) {
		pDC->SetViewportOrg(0,0);
		pDC->SetWindowOrg(0,0);
		pDC->SetMapMode(MM_TEXT);
		dc.SetViewportOrg(0,0);
		dc.SetWindowOrg(0,0);
		dc.SetMapMode(MM_TEXT);
		pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&dc,0,0,SRCCOPY);
		dc.SelectObject(pOldBitmap);
	}
	pDC->RestoreDC(nSaveDC);
}

void CLSDView::OnTestmode() 
{
	if ( m_nViewMode == VIEWMODE_ROUTE ) {
		m_nViewMode = VIEWMODE_NORMAL;
		if (m_pRouteDialog) {
			delete m_pRouteDialog;
			m_pRouteDialog = NULL;
		}
	}
	else {
		m_nViewMode = VIEWMODE_ROUTE;
		if (!m_pRouteDialog) {
			m_pRouteDialog = new CRouteDialog( this );
			m_pRouteDialog->Create( IDD_DIALOG_ROUTE );
		}
		if (m_pRouteDialog) {
			m_pRouteDialog->ShowWindow( SW_SHOW );
		}
	}

	SetViewWindowSize();
	Invalidate();
}

void CLSDView::RemoveCheckPath()
{
	POSITION pos = m_CheckPath.GetHeadPosition();
	BOOL first = TRUE;
	while ( pos != NULL )
	{
		CPoint *p = (CPoint *)m_CheckPath.GetNext( pos );
		delete p;
	}
	m_CheckPath.RemoveAll();
}

CString MakeTable(CLSDDoc *pDoc, char *tname = "", char *btn = "",  CObList *path = NULL);

void CLSDView::CreateCheckPath()
{
	CLSDDoc* pDoc = GetDocument();
	char* tname, *btn;
	tname = m_CheckTrack->GetName();
	btn = ((CTrack*)m_CheckTrack)->GetButtonName(m_nCheckCNR);
	CString strRoute = MakeTable(pDoc, tname, btn, &m_CheckPath);
	if ( m_pRouteDialog ) {
		m_pRouteDialog->UpdateRoute( strRoute );
	}
	Invalidate();
	return;
}


void CLSDView::OnSelectall() 
{
	CObList* pObList = GetDocument()->GetObjects();
	POSITION pos = pObList->GetHeadPosition();
	while (pos!=NULL) {
		CGraphObject* pObj = (CGraphObject*)pObList->GetNext(pos);
		Select(pObj, TRUE);
	}
}

void CLSDView::PurgePageInfo()
{
	POSITION pos;
	for (pos = m_PageInfo.GetHeadPosition(); pos != NULL; ) {
		PageInfo *page = (PageInfo*)m_PageInfo.GetNext(pos);
		delete page;
	}
	m_PageInfo.RemoveAll();
}

void CLSDView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	DrawPage(pDC,pInfo);	
}

/////////////////////////////////////////////////////////////////////////////
// CLSDView printing

BOOL CLSDView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CLSDView::DrawPage(CDC *pDC, CPrintInfo *pInfo)
{
	CLSDDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CDC dc;
	CDC* pDrawDC = pDC;
	CBitmap bitmap;
	CBitmap* pOldBitmap;

	CRect client;
	pDC->GetClipBox(client);
	CRect rect = client;
	m_Area = rect;
	DocToClient(rect);

	int nSaveDC = pDC->SaveDC();

	if (!pDC->IsPrinting()) {
		if (dc.CreateCompatibleDC(pDC)) {
			if (bitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height())) {
				OnPrepareDC(&dc,NULL);
				pDrawDC = &dc;
				dc.OffsetViewportOrg(-rect.left,-rect.top);
				pOldBitmap = dc.SelectObject(&bitmap);
			}
		}
	}
	else {
		pDC->SetMapMode(MM_ISOTROPIC);
		pDC->SetViewportOrg(0,0);
		int prscale = ((CLSDApp*)AfxGetApp())->CalcPrintScale(pDC, DocSize.cx );
		pDC->SetViewportExt( prscale, prscale );
		pDC->SetWindowExt( 100, -100 );
		pDC->SetWindowOrg(-DocSize.cx/2 - mLogX, DocSize.cy/2 + mLogY);
	}

	CBrush brush;
	if ( m_nViewMode != VIEWMODE_SCREEN ) {
		if (!brush.CreateSolidBrush(RGB(255,255,255))) return;
	}
	else {
		if (!brush.CreateSolidBrush(RGB(0,0,0))) return;
	}
	brush.UnrealizeObject();
	pDrawDC->FillRect(client, &brush);

	if (!pDC->IsPrinting()) {
		if ( m_nViewMode != VIEWMODE_SCREEN ) DrawGrid( pDrawDC );
		pDoc->DrawPageOff(pDrawDC,pInfo);
	}

    if (!(pInfo && pInfo->m_nCurPage != 1) ) pDoc->Draw( pDrawDC, this );

	if (pInfo) {
		pDoc->DrawOutline(pDrawDC,pInfo);
		if (pDC->IsPrinting()) {
/*			POSITION pos = m_PageInfo.FindIndex(pInfo->m_nCurPage-1);
			PageInfo *page = (PageInfo*)m_PageInfo.GetNext(pos);
			if (page->m_nActivePageNo == 1) {
				pDoc->DrawTableType(pDC, pInfo->m_nCurPage == 1);

			}
*/
		}
	}

	if ( m_nViewMode == VIEWMODE_SCREEN ) {
		_nDotPerCell = pDoc->m_DotInfo.nDot;
		SymbolInfo = SymbolInfo10;
		OnRunMstview();
		int nSaveDC = pDrawDC->SaveDC();
		pDrawDC->SetMapMode(MM_TEXT);
		CSize size = pDoc->GetSize();
		if ( REVERSE == 1 )
			pDrawDC->OffsetWindowOrg( size.cx / 2, -size.cy / 2 );
		else
			pDrawDC->OffsetWindowOrg( -size.cx / 2, size.cy / 2 );
		int count = m_pTrackDoc->mTrackList.GetSize();
		TScrobj * obj;
		for (int i=0; i<count; i++ ) {
			obj = (TScrobj*)m_pTrackDoc->mTrackList.GetAt( i );
			obj->Draw( pDrawDC );
		}
		pDrawDC->RestoreDC( nSaveDC );
	    if (!(pInfo && pInfo->m_nCurPage != 1) ) pDoc->Draw( pDrawDC, this );
	}

	if (pDrawDC != pDC) {
		pDC->SetViewportOrg(0,0);
		pDC->SetWindowOrg(0,0);
		pDC->SetMapMode(MM_TEXT);
		dc.SetViewportOrg(0,0);
		dc.SetWindowOrg(0,0);
		dc.SetMapMode(MM_TEXT);
		pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&dc,0,0,SRCCOPY);
		dc.SelectObject(pOldBitmap);
	}

	pDC->RestoreDC(nSaveDC);
}

/*
									------------------

		sz[0] |	sz[2]			P0		CIRCUIT		
		ha1	  |	hb1					 ----------------
			  |							h0
		------+------				==================
			  |
		sz[1] |	sz[3]					h1
		ha2	  |	hb2

									==================
		ha = ha1 + ha2
		hb = hb1 + hb2					h2
												 ----
													th
									------------------

  P0
		ha < h0  and  
		hb < h0  and  
		hb1 < h0 - th

  P0,P1
	P0 - Circutit only
	P1 - Table
		ha < h1  and  
		hb < h1  and  
		hb1 < h1 - th

	P0 - Circutit only + Table
	P1 - Table
		ha < h0 + h1  and  
		hb < h0 + h1  and  
		hb1 < h0 + h1 - th
		
*/

int CLSDView::CalcPageAlign(int *sz)
{
	int pages = 0;
	return pages;
}

void CLSDView::OnExport() 
{
	CLSDDoc* pDoc = GetDocument();
	pDoc->Export();	
}

void CLSDView::OnChangeDot() 
{
	CLSDDoc *pDocument = GetDocument();
	CDotChange dlg;
	int dot = 10 - pDocument->m_DotInfo.nDot;
	dlg.mdot = dot;

	DWORD nStatus =	pDocument->m_DotInfo.nDesInfoStatus;
	dlg.m_bReverse = nStatus & DESINFO_REVERSE;

	dlg.m_nXSize = pDocument->m_DotInfo.nViewX;
	dlg.m_nYSize = pDocument->m_DotInfo.nViewY;
	dlg.m_iOffSetX = pDocument->m_DotInfo.nMoveX;	//OFFSET_X;
	dlg.m_iOffSetY = pDocument->m_DotInfo.nMoveY;	//OFFSET_Y;

	dlg.m_nSystemY = m_nSystemYPos;
	dlg.m_bSmallFont = (_UseSmallText) ? TRUE : FALSE;

	if(dlg.DoModal() == IDOK)	{		
		dot = dlg.mdot;
		pDocument->m_DotInfo.nDot = 10-dot;

		if(dlg.m_bReverse) {
			REVERSE = -1;
			nStatus |= DESINFO_REVERSE;
		}
		else {
			REVERSE = 1;
			nStatus &= ~DESINFO_REVERSE;
		}

		pDocument->m_DotInfo.nViewX = dlg.m_nXSize;
		pDocument->m_DotInfo.nViewY = dlg.m_nYSize;
		pDocument->m_DotInfo.nMoveX = dlg.m_iOffSetX;	//OFFSET_X;
		pDocument->m_DotInfo.nMoveY = dlg.m_iOffSetY;	//OFFSET_Y;
		pDocument->m_DotInfo.nDesInfoStatus = nStatus;
		pDocument->ComputePageSize();
		OnInitialUpdate();

		m_nSystemYPos   = dlg.m_nSystemY;
		_UseSmallText   = (dlg.m_bSmallFont) ? 1 : 0;
	}
}

void CLSDView::OnRunMstview() 
{
	m_pTrackDoc->Destory();
	m_pTrackDoc->OnResetState();

	CLSDDoc* pDoc = GetDocument();	
	POSITION pos;
	pos = pDoc->m_objects.GetHeadPosition();

	CPoint p1;
	CPoint p2;		

	int  i;
	CClientDC dc(this);

	while ( pos != NULL )
	{
		CGraphObject *item = (CGraphObject *)pDoc->m_objects.GetNext( pos );
		if (item->IsKindOf(RUNTIME_CLASS(CTrack))) {
			CTrack *trk = (CTrack*)item;					
			m_pTrackDoc->AddObject( item->m_pBmpInfo );			
		}
		else if ( item->m_pBmpInfo ) {
			m_pTrackDoc->AddObject( item->m_pBmpInfo );			
		}
	}		
	
	m_pTrackDoc->SetFileName(pDoc->GetPathName());
	m_nXSize = m_nSFMViewXSize;
	_nSFMViewXSize = m_nXSize;
	m_nYSize = m_nSFMViewYSize;

	int count = m_pTrackDoc->mTrackList.GetSize();
	TScrobj * obj;
	for (i=0; i<count; i++ ) {
		obj = (TScrobj*)m_pTrackDoc->mTrackList.GetAt( i );
		obj->m_hObject = (HANDLE)-1;
		obj->ConvertPos();
	}

}

void CLSDView::OnViewSystemObj() 
{
	CLSDDoc* pDoc = GetDocument();
	pDoc->m_bOnViewSystemButton = (pDoc->m_bOnViewSystemButton) ? FALSE : TRUE;
	Invalidate(FALSE);
}

void CLSDView::OnUpdateViewSystemObj(CCmdUI* pCmdUI) 
{
	CLSDDoc* pDoc = GetDocument();
	int checked = (pDoc->m_bOnViewSystemButton) ? 1 : 0;
	pCmdUI->SetCheck( checked );
}

void CLSDView::OnUpdateZoomin(CCmdUI* pCmdUI)   //SFM보기일때 ZoomIn, Out 비활성화 
{
	pCmdUI->Enable(m_bCopyZI); 
}

void CLSDView::OnUpdateZoomout(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bCopyZO); 
}
void CLSDView::OnUpdatePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bCopyPrint); 
}

void CLSDView::OnUpdatePreview(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bCopyPreview); 	
}

void CLSDView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bCopyOpen); 		
}

void CLSDView::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bCopyNew); 			
}

void CLSDView::OnSfmview() 
{
	// LSD와 SFM보기 전환시 전환전의 화면유지. (ADD code)
	int nCX = GetScrollPos( SB_HORZ );
	int nCY = GetScrollPos( SB_VERT );

	if ( m_nViewMode == VIEWMODE_SCREEN ) {
		m_nViewMode = VIEWMODE_NORMAL;
		m_bCopyZI = TRUE;	// LSD보기일때 ZoomIn, Out 활성화 
		m_bCopyZO = TRUE;	// LSD보기일때 ZoomIn, Out 활성화 
		m_bCopyPrint = TRUE;
		m_bCopyPreview = TRUE;
		m_bCopyOpen = TRUE;
		m_bCopyNew = TRUE;
		// LSD 상에서 zoom 작업을 했을경우 sfm 상에서 Scroll영향받지 않게.( ADD Code )
		m_nZoomFactor = m_rtZoomFactor;
		SetViewWindowSize();
		// LSD와 SFM보기 전환시 전환전의 화면유지.(ADD code)
		SetScrollPos( SB_HORZ, m_DesCX );
		SetScrollPos( SB_VERT, m_DesCY );
	}
	else {
		m_nViewMode = VIEWMODE_SCREEN;
		// LSD 상에서 zoom 작업을 했을경우 sfm 상에서 Scroll영향받지 않게.( ADD Code )
		m_nZoomFactor = 100;
		m_DesCX = nCX;
		m_DesCY = nCY;
		CSize size;
		CLSDDoc *pDoc = GetDocument();
		size.cx = pDoc->m_DotInfo.nViewX;
		size.cy = pDoc->m_DotInfo.nViewY;

		SetScrollSizes(MM_TEXT, size);
		m_bCopyZI = FALSE;	// SFM보기일때 ZoomIn, Out 비활성화 
		m_bCopyZO = FALSE;	// SFM보기일때 ZoomIn, Out 비활성화 
		m_bCopyPrint = FALSE;
		m_bCopyPreview = FALSE;
		m_bCopyOpen = FALSE;
		m_bCopyNew = FALSE;
	}
	Invalidate();
}
void CLSDView::SetViewWindowSize()
{
	m_rtZoomFactor = m_nZoomFactor;

	CClientDC dc(NULL);
	CSize size = GetDocument()->GetSize();
	DocSize = size;
	size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX), DV) * ZOOM_NORMAL / m_nZoomFactor;
	size.cy = MulDiv(size.cy, dc.GetDeviceCaps(LOGPIXELSX), DV) * ZOOM_NORMAL / m_nZoomFactor;

	SetScrollSizes(MM_TEXT, size);

	CLSDDoc *pDocument = GetDocument();
	// SFM 의 X 좌표는 실 좌표의 2 배로 되어있음( LSD 와 동일, LSD 의 좌표도 화면 크기 X 2 임. )
	m_nSFMViewXSize = pDocument->m_DotInfo.nViewX;
	m_nSFMViewYSize = pDocument->m_DotInfo.nViewY;
	OFFSET_X = DocSize.cx / 2 + pDocument->m_DotInfo.nMoveX;
	OFFSET_Y = DocSize.cy / 2 + pDocument->m_DotInfo.nMoveY;
	DWORD nStatus =	pDocument->m_DotInfo.nDesInfoStatus;
	if ( nStatus & DESINFO_REVERSE ) {
		REVERSE = -1;
	}
	else {
		REVERSE = 1;
	}

}

void CLSDView::OnZoomin()  // Zoomin의 경우 m_nZoomFactor == 6이상 Zoomin이 되지 않는다. 그런 경우 메뉴 비활성화.
{
	m_nZoomFactor /= 2;	
	if( m_nZoomFactor <= 12 ) { 
		m_bCopyZI = FALSE;
		m_bCopyZO = TRUE;
	} else {
		m_bCopyZI = TRUE;
		m_bCopyZO = TRUE;	
	}
	int nCurrentX = GetScrollPos( SB_HORZ );
	int nCurrentY = GetScrollPos( SB_VERT );
	CSize sizeClient;
	CSize sizeSb;
	GetTrueClientSize(sizeClient, sizeSb);
	int nSX = sizeClient.cx - sizeSb.cx;
	int nSY = sizeClient.cy - sizeSb.cy;
	SetViewWindowSize();
	SetScrollPos( SB_HORZ, nCurrentX  * 2 + nSX / 2 );
	SetScrollPos( SB_VERT, nCurrentY  * 2 + nSY / 2 );
	Invalidate();
}

void CLSDView::OnZoomout() 
{
	m_nZoomFactor *= 2;	

	if( m_nZoomFactor >= 200 ) { 
		m_bCopyZI = TRUE;
		m_bCopyZO = FALSE;
	} else {
		m_bCopyZI = TRUE;
		m_bCopyZO = TRUE;	
	}

	int nCurrentX = GetScrollPos( SB_HORZ );
	int nCurrentY = GetScrollPos( SB_VERT );
	CSize sizeClient;
	CSize sizeSb;
	GetTrueClientSize(sizeClient, sizeSb);
	int nSX = sizeClient.cx - sizeSb.cx;
	int nSY = sizeClient.cy - sizeSb.cy;
	SetViewWindowSize();
	SetScrollPos( SB_HORZ, (nCurrentX - nSX / 2)  / 2 );
	SetScrollPos( SB_VERT, (nCurrentY - nSY / 2)  / 2 );
	Invalidate();
}

void CLSDView::OnUndo() 
{
	CLSDDoc *pDoc = GetDocument();
	m_selection.RemoveAll();
	pDoc->Undo();
	Invalidate();
}

void CLSDView::OnCopy() 
{
	CLSDDoc		* pDoc = GetDocument();
	CGraphObject* pObj;
	POSITION	  pos;
	int			  iCnt,i;

	GetDocument()->m_objbak3.RemoveAll();
	iCnt = m_selection.GetCount();
	pos = m_selection.GetHeadPosition();	

	for ( i = 0 ; i < iCnt ; i++ ){
		pObj = (CGraphObject*)m_selection.GetNext(pos);
		pDoc->SaveClipBoard( pObj );
	}
	
	pDoc->Copy();
}

void CLSDView::OnCut()
{
	CLSDDoc		* pDoc = GetDocument();
	CGraphObject* pObj;
	POSITION	  pos;
	int			  iCnt,i;

	GetDocument()->m_objbak3.RemoveAll();
	iCnt = m_selection.GetCount();
	pos  = m_selection.GetHeadPosition();	

	for ( i = 0 ; i < iCnt ; i++ ){
		pObj = (CGraphObject*)m_selection.GetNext(pos);
		pDoc->SaveClipBoard( pObj );
	}

	OnTrackdel();
	pDoc->Cut();
}

void CLSDView::OnPaste() 
{
	CLSDDoc *pDoc = GetDocument();
	pDoc->Paste();
	Invalidate();
}

void CLSDView::OnDevelop() 
{
	Develop dlg;
	dlg.DoModal();
	Invalidate();
}


BOOL CLSDView::PreTranslateMessage(MSG* pMsg) 
{
	if((pMsg->wParam == VK_DELETE)) {OnTrackdel();}
	return CScrollView::PreTranslateMessage(pMsg);
}


BOOL CLSDView::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
	return CScrollView::OnEraseBkgnd(pDC);
}




