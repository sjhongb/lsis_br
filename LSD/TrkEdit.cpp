//=======================================================
//==              TrkEdit.cpp
//=======================================================
//	파 일 명 :  TrkEdit.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "TrkEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrackEdit dialog


CTrackEdit::CTrackEdit(CString &name, CMyStrList *trk, CWnd* pParent /*=NULL*/)
	: CDialog(CTrackEdit::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrackEdit)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pName = &name;
	m_pTrack = trk;
}

void CTrackEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrackEdit)
	DDX_Control(pDX, IDC_LIST_TRACK, m_ListTrack);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTrackEdit, CDialog)
	//{{AFX_MSG_MAP(CTrackEdit)
	ON_BN_CLICKED(IDC_TRACKDEL, OnTrackdel)
	ON_BN_CLICKED(IDC_TRACKINS, OnTrackins)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrackEdit message handlers
BOOL CTrackEdit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(*m_pName);

	POSITION pos;
	CString* str;
	for( pos = m_pTrack->GetHeadPosition(); pos != NULL; )
	{
        str = (CString*)m_pTrack->GetNext( pos );
		m_ListTrack.AddString(*str);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MyStrAdd(CComboBox &combo);
void MyStrDel(CComboBox &combo);

void CTrackEdit::OnTrackdel()
{
	// TODO: Add your control notification handler code here
	MyStrDel(m_ListTrack);
}

void CTrackEdit::OnTrackins() 
{
	// TODO: Add your control notification handler code here
	MyStrAdd(m_ListTrack);
}

void CTrackEdit::OnOK() 
{
	// TODO: Add extra validation here
	m_pTrack->Purge();

	int i,n;
	CString str;
	n = m_ListTrack.GetCount();
	for (i=0; i<n; i++) {
		m_ListTrack.GetLBText(i,str);
		m_pTrack->AddTail(str);
	}
	CDialog::OnOK();
}
