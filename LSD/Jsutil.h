//=======================================================
//==              Jsutil.h
//=======================================================
//	파 일 명 :  Jsutil.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef PNTTYPE
#define PNTTYPE
typedef struct {
   double x,y;
} point;
#endif

void swap(int &a, int &b);
void swap(short &a, short &b);
void swap(unsigned &a, unsigned &b);
void swap(long &a, long &b);
void pntswap(point &a, point &b);
int pntcmp(point &p1,point &p2);
char *stripp(char *s);
char *gettoken(char *&p , char *szDel );
char *gettoken(char *&p);
char *gettoken1(char *&p);
void itoszRACK(int r, char *bf);
char cLOCH(int x);
void MoveStr(char *, const char *, int);

class CRelay;
class CONNECTOR;
class CABLE;
class CRackComp;
class NET;
class COMP;

