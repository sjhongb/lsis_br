//=======================================================
//==              LSDView.h
//=======================================================
//	파 일 명 :  LSDView.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#define HINT_UPDATE_WINDOW     0
#define HINT_UPDATE_DRAWOBJ    1
#define HINT_UPDATE_SELECTION  2
#define HINT_DELETE_SELECTION  3

#define VIEWMODE_NORMAL		0
#define VIEWMODE_ROUTE		1
#define VIEWMODE_SCREEN		2

#include "LSDDoc.h"

class CTrack;
class CTableView;
class CTableDoc;

#include "BmpTrackCon.h"
#include "RouteDialog.h"

#define ZOOM_NORMAL 100

class CLSDView : public CScrollView
{
protected: // create from serialization only
	CSize DocSize;
	BOOL m_bDragMode;
	CPoint m_pOrgPoint, m_pLastPoint, m_pMenuPoint;
	CDC *m_pDragDC;
	CLSDView();
	DECLARE_DYNCREATE(CLSDView)

// Attributes
public:
	int m_DesCY;
	int m_DesCX;
	int m_iModiFlag;
	BOOL m_bCopyZO;
	BOOL m_bCopyZI;
	BOOL m_bCopyPrint;
	BOOL m_bCopyOpen;
	BOOL m_bCopyPreview;
	BOOL m_bCopyNew;
	int m_nPathNo, m_nLastPathNo;
	CRouteDialog *m_pRouteDialog;
	int m_nCheckPathNo;

	CBmpTrackCon  *m_pTrackDoc;
	UINT type;	
	int  m_nXSize;
	int  m_nYSize;

	void SetViewWindowSize();
	int m_iSelectObjCnt;
	int m_nZoomFactor;
	int m_rtZoomFactor;  // ZOOM시에 m_nZoomFactor 값
	CObList m_PageInfo;
	CObList m_CheckPath;
	CGraphObject *m_CheckTrack;
	int m_nCheckCNR;
	CPoint m_TablePos;
	CRect m_Area;

	int CalcPageAlign(int *sz);
	void DrawPage(CDC *pDC, CPrintInfo *pInfo = NULL);
	void PurgePageInfo();
	void CreateCheckPath();
	void RemoveCheckPath();

	int	 m_nViewMode;	// m_bTestMode;

	void OnTestDraw(CDC *pDC);

	virtual BOOL IsSelected(const CObject* pDocItem) const;
	void CloneSelection();
	CPoint c_down, c_last;
	UINT c_nDownFlags;
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	BOOL m_bActive;
	CObList m_selection;
	int  m_nSFMViewXSize;
	int  m_nSFMViewYSize;
	int  m_nSystemYPos;

	void Remove(CGraphObject *pObj);
	void InvalObj(CGraphObject *pObj);
	void UpdateActiveItem();
	void Deselect(CGraphObject *pObj);
	void SelectWithinRect(CRect, BOOL bAdd = FALSE);
	void Select(CGraphObject *pObj, BOOL bAdd = FALSE);
	void DocToClient(CRect &rect);
	void DocToClient(CPoint &point);
	void ClientToDoc(CRect &rect);
	void ClientToDoc(CPoint &point);
	void SetPageSize(CSize size);
	void ViewRouteCheck(CPoint  local);
	void DrawGrid(CDC *pDC);
	CLSDDoc* GetDocument();
	void GridPoint(CPoint &point);
	BOOL CheckObectSizeChange(CPoint  local);
	BOOL CheckObectMoveChange(CPoint  local , UINT nFlags);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLSDView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLSDView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLSDView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTrackins();
	afx_msg void OnSignalins();
	afx_msg void OnButtonins();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnTrackdel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnStationinfo();
	afx_msg void OnTestmode();
	afx_msg void OnSelectall();
	afx_msg void OnExport();
	afx_msg void OnViewSystemObj();
	afx_msg void OnUpdateViewSystemObj(CCmdUI* pCmdUI);
	afx_msg void OnChangeDot();
	afx_msg void OnRunMstview();
	afx_msg void OnSfmview();
	afx_msg void OnUpdateZoomin(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomout(CCmdUI* pCmdUI);
	afx_msg void OnZoomin();
	afx_msg void OnZoomout();
	afx_msg void OnDevelop(); 
	afx_msg void OnUndo();
	afx_msg void OnCopy();
	afx_msg void OnPaste(); 
	afx_msg void OnCut();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnUpdatePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePreview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bGrid;
	int m_nGridSize;
};

#ifndef _DEBUG  // debug version in LSDView.cpp
inline CLSDDoc* CLSDView::GetDocument()
   { return (CLSDDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
