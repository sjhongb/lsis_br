//=======================================================
//==              LSDDoc.cpp
//=======================================================
//	파 일 명 :  LSDDoc.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "LSDDoc.h"
#include "LSDView.h"
#include "Track.h"
#include "Signal.h"
#include "MyObject.h"
#include "Develop.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLSDDoc

IMPLEMENT_DYNCREATE(CLSDDoc, CDocument)

BEGIN_MESSAGE_MAP(CLSDDoc, CDocument)
	//{{AFX_MSG_MAP(CLSDDoc)
	ON_COMMAND(ID_MAKETABLE, OnMaketable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLSDDoc construction/destruction

extern CString _szUser, _szCompany, _szSerial, _szCode, _szVolume;
extern int STATIONUPDN;

CString __editdev1,__editdev2,__editdev3,__editdev4,__editdev5,__editdev6,__editdev7;
CString __editdev8,__editdev9,__editdev10,__editdev11,__editdev12,__editdev13,__editdev14;
CString __editdev15,__editdev16,__editdev17,__editdev18,__editdev19,__editdev20,__editdev21;

CLSDDoc::CLSDDoc()
{
	// TODO: add one-time construction code here
	m_SelectedObject = NULL;
	m_TableType = NULL;
	m_bOnViewSystemButton = FALSE;
m_testFlag = 1;
	m_DotInfo.nDot   = 10;
	m_DotInfo.nMoveX = 0;
	m_DotInfo.nMoveY = 0;
	m_DotInfo.nViewX = 2048;
	m_DotInfo.nViewY = 768;
	m_DotInfo.nDesInfoStatus = 0L;

	ComputePageSize();
	m_bSFMMode = FALSE;
}

CLSDDoc::~CLSDDoc()
{
	POSITION pos;
	CGraphObject *item;
	for( pos = m_objects.GetHeadPosition(); pos != NULL; )
	{
        item = (CGraphObject *)m_objects.GetNext( pos );
		delete item;
	}
	m_objects.RemoveAll();
}

BOOL CLSDDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
		
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CLSDDoc serialization

const char DESDOCHEADER[] = "ILT DESIGN FILE V1.0\n";

#define DES_VERSION_100		100	// version 1.00
#define DES_VERSION_110		110	// version 1.1a
#define LSD_VERSION_20B		202	// version 2.0 방글라 데시

void CLSDDoc::Serialize(CArchive& ar)
{
	int n;
	int version, docsizex =0, docsizey =0;
	
	version = LSD_VERSION_20B;
	CString ttl = "INTER LOCK DESIGN FILE";

	if (ar.IsStoring())
	{
		// TODO: add storing code here
		n = m_objects.GetCount();
		ar << ttl << version << docsizex << docsizey << n;
		ar << m_LineName << m_StationName << m_FromStation << m_ToStation << m_PrevStation << m_NextStation;
		ar << m_Kilo;
		ar << m_nUpDn << m_nType;
		ar << __editdev1 << __editdev2 << __editdev3 << __editdev4 << __editdev5;
		ar << __editdev6 << __editdev7 << __editdev8 << __editdev9 << __editdev10;
		ar << __editdev11 << __editdev12 << __editdev13 << __editdev14 << __editdev15;
		ar << __editdev16 << __editdev17 << __editdev18 << __editdev19 << __editdev20 << __editdev21;
		if (version == LSD_VERSION_20B) {
			ar  << m_DotInfo.nDot 
				<< m_DotInfo.nMoveX 
				<< m_DotInfo.nMoveY 
				<< m_DotInfo.nViewX 
				<< m_DotInfo.nViewY 
				<< m_DotInfo.nSystemY
				<< m_DotInfo.nDesInfoStatus;
		}
		m_objects.Serialize(ar);

		POSITION pos = GetFirstViewPosition();  // View 가 SFM 모드이면 화일 생성.
		CLSDView* pView = (CLSDView*)GetNextView(pos);
		if ( pView->m_nViewMode == VIEWMODE_SCREEN ) {
			pView->m_pTrackDoc->Save();
		}

	}
	else
	{
		// TODO: add loading code here
		ar >> ttl >> version >> docsizex >> docsizey >> n;
		ar >> m_LineName >> m_StationName >> m_FromStation >> m_ToStation >> m_PrevStation >> m_NextStation;
		ar >> m_Kilo;
		ar >> m_nUpDn >> m_nType;
		ar >> __editdev1 >> __editdev2 >> __editdev3 >> __editdev4 >> __editdev5;
		ar >> __editdev6 >> __editdev7 >> __editdev8 >> __editdev9 >> __editdev10;
		ar >> __editdev11 >> __editdev12 >> __editdev13 >> __editdev14 >> __editdev15;
		ar >> __editdev16 >> __editdev17 >> __editdev18 >> __editdev19 >> __editdev20 >> __editdev21;

		if (version == LSD_VERSION_20B) {
			ar	>> m_DotInfo.nDot 
				>> m_DotInfo.nMoveX 
				>> m_DotInfo.nMoveY 
				>> m_DotInfo.nViewX 
				>> m_DotInfo.nViewY 
				>> m_DotInfo.nSystemY
				>> m_DotInfo.nDesInfoStatus;
			ComputePageSize();
		} else {
		   AfxMessageBox ( " Check LSD File Vision !! ");
		   return;
		}
		m_objects.Serialize(ar);

		POSITION pos;
		CGraphObject *item;

		for( pos = m_objects.GetHeadPosition(); pos != NULL; )
		{
			item = (CGraphObject *)m_objects.GetNext( pos );
			item->m_pDocument = this;
			item->Init();
			if (isTableType(item)) m_TableType = (CMyObject*)item;
			item->Active( TRUE );
		}
		TrackMatchToDoubleSlip();
		CreateToTrackOrders();

		STATIONUPDN = m_nUpDn;
	}
}

void CLSDDoc::TrackMatchToDoubleSlip()
{
	POSITION pos1, pos2;
	CTrack *t1, *t2;
	CGraphObject *item1, *item2;

	pos1 = m_objects.GetHeadPosition();
	while (pos1 != NULL) {
		item1 = (CGraphObject*)m_objects.GetNext(pos1);
		if (item1->IsKindOf(RUNTIME_CLASS(CTrack)) && item1->isValid()) {
			t1 = (CTrack*)item1;
			if ( t1->isDoubleSlip() ) {
				pos2 = m_objects.GetHeadPosition();
				while (pos2 != NULL) {
					item2 = (CGraphObject*)m_objects.GetNext(pos2);
					if (item2->IsKindOf(RUNTIME_CLASS(CTrack)) && item2->isValid() ) {
						t2 = (CTrack*)item2;
						if ( t2->isDoubleSlip() ) {
							int len = t2->Name.GetLength();
							if (t2->Name.Find(".") >= 0) len -= 2;
							if ((t2 != t1) && !strncmp(t1->Name, t2->Name, len)) {
								t1->m_pDoubleSlip = t2;
								t2->m_pDoubleSlip = t1;
								t1->Init();
								t2->Init();
								break;
							}
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLSDDoc diagnostics

#ifdef _DEBUG
void CLSDDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLSDDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLSDDoc commands

void CLSDDoc::Set_ModifiedFlag()
{
	SetModifiedFlag();
}

void CLSDDoc::Copy()
{
}

void CLSDDoc::Cut()
{
}

void CLSDDoc::Paste()
{
	int iCnt,i;
	CGraphObject * item1;
	CGraphObject * item2;
	CPoint pOint;

	pOint.x = 20;
	pOint.y = 20;
	POSITION pos;

	iCnt = m_objbak3.GetCount();
	pos = m_objbak3.GetHeadPosition();	

	for ( i = 0 ; i < iCnt ; i++ ){
		item1 = (CGraphObject*)m_objbak3.GetNext(pos);	
  	    for (int j=0; j<item1->m_nHandle; j++) {
		   CBlip *bl = item1->m_pBlips[j];
		   if (bl != NULL) bl->Offset(pOint);
		}
		SaveClipBoard( item1 );	
		item2 = (CGraphObject*)m_objbak3.GetTail();
		Add( item2 , iCnt);
		m_objbak3.RemoveTail();
	}
}

void CLSDDoc::Undo()
{
	int iSavePos,i;
	BOOL bSame;
	bSame = TRUE;

	iSavePos = m_iUnDoCnt[0][0];
	
	if ( iSavePos <= 0 ) return;

	CGraphObject * item1;
	CGraphObject * item2;

	POSITION pos;
	POSITION pos1;
    POSITION pos2;

	for ( i = 0 ; i < m_iUnDoCnt[0][1] ; i ++ ){ 


	item1 = (CGraphObject *)m_objbak1.GetTail();
	item2 = (CGraphObject *)m_objbak2.GetTail();


	pos = m_objects.Find( item1 );

	if ( m_iUnDoCnt[iSavePos][0] == 1 ) {
		if (pos != NULL) {
			m_objects.RemoveAt( pos );
		}
	} else if ( m_iUnDoCnt[iSavePos][0] == 2 ) {
		m_objects.AddTail(item1);
	} else if ( m_iUnDoCnt[iSavePos][0] == 3 ) {
		if (pos != NULL) {
			m_objects.SetAt(pos,item2);
		}
		pos1 = m_objbak1.GetHeadPosition();
	    while ( bSame ) {
	        pos2 = m_objbak1.Find( item1 , pos1 );
			pos1 = pos2;
			if (pos2 != NULL ) {
				m_objbak1.SetAt(pos2,item2);
			} else {
				if ( m_objbak1.GetHead() == item1) m_objbak1.SetAt(m_objbak1.GetHeadPosition(),item2);
				bSame = FALSE;
			}
		}
	}

	//m_objbak1 의 카운트와  m_objbak2 의 카운트가 다를경우 처리 방안 -> 처리 완

	bSame = TRUE;

	m_objbak1.RemoveTail();
	m_objbak2.RemoveTail();
	iSavePos--;

	m_iUnDoCnt[0][0] = iSavePos;
	}
	m_iUnDoCnt[0][1] = m_iUnDoCnt[iSavePos][1];

}

void CLSDDoc::Redo()
{

}

void CLSDDoc::SaveItem(CGraphObject *pObj , int iVal , int iSelectObjCnt)
{


	CString strObjName;
	strObjName = pObj->NameOf();
	POSITION pos = m_objects.Find( pObj );
	
	int iSavePos,i;
	iSavePos = m_iUnDoCnt[0][0];
	iSavePos ++;
	if ( iSavePos > 300 ) {
		iSavePos = 300;
		for ( i = 1 ; i < 300 ; i ++ ) m_iUnDoCnt[i][0] = m_iUnDoCnt[i+1][0];
		m_objbak1.RemoveHead();
		m_objbak2.RemoveHead();
	}
	if ( iVal == 3 ) {
			TRACE("11");
		pObj->SaveClone(pObj->m_pDocument);
		m_objbak1.AddTail(pObj);  // 원본 
		m_objbak2.AddTail(m_objects.GetTail());  // 새로만들어진놈 -> 예전의 좌표 
		m_objects.RemoveTail();                  // 새로 만들어진놈 지움
	} else {
			TRACE("22");
		m_objbak1.AddTail(pObj);
		m_objbak2.AddTail(pObj);
	}

	if ( m_objbak1.GetCount() != m_objbak2.GetCount() ) {
		if ( m_objbak1.GetCount() > m_objbak2.GetCount() ){
			TRACE("1");
			m_objbak1.RemoveTail();
		}
		if ( m_objbak1.GetCount() < m_objbak2.GetCount() ){
			TRACE("2");
			m_objbak2.RemoveTail();
		}
	}

	m_iUnDoCnt[iSavePos][0] = iVal;
	m_iUnDoCnt[iSavePos][1] = iSelectObjCnt;
	m_iUnDoCnt[0][0] = iSavePos;
	m_iUnDoCnt[0][1] = iSelectObjCnt;

	TRACE("Save Item - m_objbak1 Count -> %d , m_objbak1 Count -> %d , iSavePos-> %d Val -> %d \n",m_objbak1.GetCount(),m_objbak2.GetCount(), iSavePos , iVal);
}

void CLSDDoc::SaveClipBoard(CGraphObject *pObj)
{
	pObj->SaveClone(pObj->m_pDocument);
	m_objbak3.AddTail(m_objects.GetTail());  // 새로만들어진놈 -> 예전의 좌표 
	m_objects.RemoveTail();                  // 새로 만들어진놈 지움
	TRACE ( "ClipBoard\n");
}

void CLSDDoc::SetDeleteCnt(int iCnt)
{
	m_iUnDoCnt[0][1] = iCnt;
}

void CLSDDoc::DeleteItem()
{
	int iSavePos,i;

	iSavePos = m_iUnDoCnt[0][0];

	for ( i = 0 ; i < m_iUnDoCnt[0][1] ; i ++ ) {
		m_objbak1.RemoveTail();
		m_objbak2.RemoveTail();
		iSavePos--;
		Sleep(10);
		TRACE("Delete SavePos %d\n",iSavePos);
	}


	m_iUnDoCnt[0][0] = iSavePos;
	TRACE("Delete Item - m_objbak1 Count -> %d , m_objbak1 Count -> %d , iSavePos-> %d\n",m_objbak1.GetCount(),m_objbak2.GetCount(), iSavePos);
}

CGraphObject* CLSDDoc::ObjectAt(const CPoint &point)
{
	CRect rect(point, CSize(1,1));

	POSITION pos = m_objects.GetTailPosition();
	while (pos != NULL)
	{
		CGraphObject* item = (CGraphObject *)m_objects.GetPrev( pos );
		if (item->IsKindOf(RUNTIME_CLASS(CTrack))) {
			CTrack *trk = (CTrack*)item;
			CRgn *pRgn = trk->GetRgn();
			if ( pRgn->PtInRegion( point ) ) {
				delete pRgn;
				return item;
			}
			delete pRgn;
		}
		else {
			if (item->Intersects(rect)) return item;
		}
	}
	return NULL;
}

void CLSDDoc::Add(CGraphObject *pObj , int iAddCnt)
{
	Set_ModifiedFlag();
	SaveItem( pObj , 1 , iAddCnt );

	m_objects.AddTail(pObj);
	pObj->m_pDocument = this;
	if (isTableType(pObj)) {
		m_TableType = (CMyObject*)pObj;
	}
}

void CLSDDoc::CloneAdd(CGraphObject *pObj)
{
	Set_ModifiedFlag();
	m_objects.AddTail(pObj);
	pObj->m_pDocument = this;
	if (isTableType(pObj)) {
		m_TableType = (CMyObject*)pObj;
	}
}

void CLSDDoc::Remove(CGraphObject *pObj , int iDelCnt)
{
	Set_ModifiedFlag();
	
	POSITION pos = m_objects.Find( pObj );
	if (pos != NULL) {
		POSITION pos1 = pos;
        CGraphObject * item = (CGraphObject *)m_objects.GetAt( pos );
		SaveItem( item , 2 , iDelCnt );
		m_objects.RemoveAt( pos );
		if (item == m_TableType) m_TableType = NULL;
	}

	pos = GetFirstViewPosition();
	while (pos != NULL) ((CLSDView*)GetNextView(pos))->Remove(pObj);
}


void CLSDDoc::BlipMoveTo(CPoint point)
{
	if (m_SelectedObject && m_SelectedBlip) {
		m_SelectedBlip->MoveTo(CPoint(m_SelectedBlip->x+point.x,m_SelectedBlip->y+point.y));
		m_SelectedObject->UpdateRect();
	}
}

void CLSDDoc::PropertyChange(CWnd *wnd)
{
	if (m_SelectedObject) {
		m_SelectedBlip->PropertyChange(wnd);
	}
}

void DrawArrow(CDC *pDC, CPoint a1, CPoint a2, int w, int ed);

void DrawDirection(CDC *pDC, CPoint p, CString& n1, CString& n2,int dir) {
	pDC->SetTextAlign( TA_CENTER );
	pDC->TextOut(p.x, p.y, n1);
	pDC->TextOut(p.x, p.y-25, n2);
	p.y -= 20;
	p.x -= dir * 30;
	CPoint p1,p2;
	p1 = CPoint(p.x + dir * 80, p.y);
	p2 = CPoint(p.x + dir * 60, p.y);
	pDC->MoveTo(p);
	pDC->LineTo(p1);
	DrawArrow(pDC,p2,p1,5,0);
}


void DrawRect(CDC *pDC, CRect &rect) {
	pDC->MoveTo(rect.left,rect.top);
	pDC->LineTo(rect.right,rect.top);
	pDC->LineTo(rect.right,rect.bottom);
	pDC->LineTo(rect.left,rect.bottom);
	pDC->LineTo(rect.left,rect.top);
}

void DrawCircle(CDC *pDC, CRect &rect) { // omani rect 영역 만큼 라인을 그린다. 
	rect.top = rect.top + 3 ;
	rect.left = rect.left + 3;
	rect.bottom = rect.bottom - 3;
	rect.right = rect.right - 3;
	pDC->Ellipse(rect);
}

void DrawHLine(CDC *pDC, CPoint &p, int l) {
	pDC->MoveTo(p.x,p.y);
	pDC->LineTo(p.x + l, p.y);
}

void DrawVLine(CDC *pDC, CPoint &p, int l) {
	pDC->MoveTo(p.x,p.y);
	pDC->LineTo(p.x, p.y + l);
}

BOOL extern CheckMode;
void CLSDDoc::Draw(CDC *pDC, CLSDView *pView)
{
	// TODO: add draw code for native data here
	if (pDC->IsPrinting() && m_TableType) {
		m_TableType->Active(FALSE);
	}

	int hx = m_size.cx / 2;
	int hy = m_size.cy / 2;

	CFont textfont;
	textfont.CreateFont(12,8,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	CFont* pOldFont = pDC->SelectObject(&textfont);

	int y1 = 220;

	CRect rect(-300, hy-85, 300, hy-115);

	CPoint cp(-1000,hy-170);
	rect.SetRect(cp.x-100, cp.y+15, cp.x+100, cp.y-15);
	POSITION pos = m_objects.GetHeadPosition();

	while ( pos != NULL )
	{

		int savedc = pDC->SaveDC();
		CGraphObject *item = (CGraphObject *)m_objects.GetNext( pos );
		item->Draw(pDC);
		pDC->RestoreDC( savedc );

	}

	if (!CheckMode) {
		pos = m_objects.GetHeadPosition();
		while ( pos != NULL )
		{
			CGraphObject *item = (CGraphObject *)m_objects.GetNext( pos );

			if (pView->m_bActive && !pDC->IsPrinting() && pView->IsSelected(item)) {
				pDC->SetTextColor(RGB(0,255,0));
				item->Draw(pDC , RGB(0,255,0));  //omani

				if (item->IsKindOf(RUNTIME_CLASS(CTrack))) {
					CTrack *trk = (CTrack*)item;   //omani
				}
				item->DrawTracker(pDC, CGraphObject::selected); //omani
				pDC->SetTextColor(RGB(0,0,0));
			}						
			if (item->IsKindOf(RUNTIME_CLASS(CMyObject)) && item->Name == "연동장치명") {
				pView->m_TablePos = CPoint(item->m_position.left, item->m_position.top - 5);
			}
		}
	}

	if (pDC->IsPrinting() && m_TableType) {
		m_TableType->Active(TRUE);
	}
	pDC->SelectObject( pOldFont );
}

void CLSDDoc::ComputePageSize()
{
	double x = 1.0, y = 1.0;
	short dot = m_DotInfo.nDot;
	if (dot == 0) dot = 10;
	if( dot == 10) {
		x = 0.5f;
	}
	else if( dot == 8 )	{
		x = 0.4f;
	}
	else 
		x = 0.4f;

	y = double( dot ) / 10.0f;

	m_size.cx = (int)( m_DotInfo.nViewX / x );
	m_size.cy = (int)( m_DotInfo.nViewY / y );

}

CString MakeTable(CLSDDoc *pDoc, char *tname = "", char *btn = "", CObList *path = NULL);
void CLSDDoc::OnMaketable() 
{
	// TODO: Add your command handler code here
	CString docname = MakeTable(this);
}

Develop Dlg;
void CLSDDoc::DrawOutline(CDC *pDC, CPrintInfo *pInfo) 
{
	int hx = m_size.cx / 2;
	int hy = m_size.cy / 2;

	int gap = 30;
	int w = 570, w1 = 342, w2 = 114, h = 40;		// 570 = 100 mm

	CRect rect(-(hx - gap), hy-gap, hx - gap, -(hy*3 - gap*4));
	int nPage = pInfo->m_nCurPage;
	if(nPage == 1){
		CRect rect(-(hx-gap), hy-gap, hx-gap, -(hy-gap));	
	}

    CPen penLine; // Construct it, then initialize
    penLine.CreatePen( PS_SOLID, 3, RGB(0,0,0) );
    CPen* pOldPen = pDC->SelectObject( &penLine );
	DrawRect(pDC,rect);
	CFont smallfont;
	smallfont.CreateFont(8,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	CFont* pOldFont = pDC->SelectObject(&smallfont);
	pDC->SetTextAlign(TA_LEFT);
	pDC->SelectObject(pOldFont);

	int nMaxPage = pInfo->GetMaxPage();
	if (nPage == nMaxPage) {
		CFont smallfont;
		smallfont.CreateFont(16,12,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
		CFont* pOldFont = pDC->SelectObject(&smallfont);

		rect.left = rect.right - w;
		rect.top = rect.bottom + h * 7;
		DrawRect(pDC,rect);
		pOldFont = pDC->SelectObject(&smallfont);

		pDC->SelectObject( pOldPen );
		CPoint tl(rect.left, rect.top - h);
		DrawHLine(pDC,tl,w);
		int tx1 = rect.left + 15;
		int tx2 = rect.left + w1 + 15;
		int dy = 30;
		int length = 0;
		CString temp;
		pDC->SetTextAlign(TA_LEFT);

		CString s = "";
		pDC->TextOut(tx1, tl.y+dy, s);
		s = __editdev1;
		length = s.GetLength();
		if(length <10) pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		
		tl.y -= h;
		tl.x += w1;
		DrawHLine(pDC,tl,w2*2);
		s = "작성사유";
		pDC->TextOut(tx1, tl.y+dy, s);
		s = __editdev2;
		length = s.GetLength();
		if( length < 10 ) pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		tl.y -= h;
		DrawHLine(pDC,tl,w2*2);
		s = __editdev3;
		length = s.GetLength();
		if ( length < 10 )pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		tl.y -= h;
		tl.x -= w1;
		DrawHLine(pDC,tl,w);
		s = __editdev4;
		length = s.GetLength();
		if( length < 10 ) pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		tl.y -= h;
		DrawHLine(pDC,tl,w);
		s = "년 월 일 제 차 변경승인";
		pDC->TextOut(tx1, tl.y+dy, s);
		s = __editdev5;
		length = s.GetLength();
		if( length < 10 ) pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		tl.y -= h;
		DrawHLine(pDC,tl,w);
		s = "년 월 일 제 차 최종승인";
		pDC->TextOut(tx1, tl.y+dy, s);
		s = __editdev6;
		length = s.GetLength();
		if(length<10) pDC->TextOut(tx2, tl.y+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y+dy-12, temp);
		}
		s = "년 월 일 제 차 최초승인";
		pDC->TextOut(tx1, tl.y-h+dy, s);
		s = __editdev7;
		length = s.GetLength();
		if(length<10) pDC->TextOut(tx2, tl.y-h+dy, s);
		else{
			temp = s.Left(8);
			pDC->TextOut(tx2, tl.y-h+dy+9, temp);
			temp = s.Mid(8,length);
			pDC->TextOut(tx2, tl.y-h+dy-12, temp);
		}

		tl.x += w1;
		tl.y = rect.bottom;
		DrawVLine(pDC,tl,h*7);
		tl.x += w2;
		DrawVLine(pDC,tl,h*7);

		rect.right = rect.left;
		pDC->SelectObject( &penLine );

		pDC->SelectObject(pOldFont);				
	}
	rect.left = rect.right - w / 2;
	rect.top = rect.bottom + h * 2;
	DrawRect(pDC,rect);
	pDC->SelectObject( pOldPen );
	pDC->SetTextAlign(TA_LEFT);
	CFont font;
	font.CreateFont(36,20,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	pOldFont = pDC->SelectObject(&font);
	pDC->TextOut(rect.left + 20, rect.top - h / 2, m_StationName);
	CString s;
	s.Format("%d/%d",nPage,nMaxPage);
	pDC->TextOut(rect.left + 200, rect.top - h / 2, s);
	pDC->SelectObject(&smallfont);
	pDC->SelectObject(pOldFont);
	font.DeleteObject();
	smallfont.DeleteObject();
}

void CLSDDoc::DrawPageOff(CDC *pDC, CPrintInfo *pInfo) 
{
	int hx = m_size.cx / 2;
	int hy = m_size.cy / 2;

	int gap = 10;
	int w = 570, w1 = 342, w2 = 114, h = 40;		// 570 = 100 mm
	CRect rect(-(hx-gap), hy-gap, hx-gap, -(hy-gap));
    CPen penLine; // Construct it, then initialize
    penLine.CreatePen( PS_SOLID, 5, RGB(0,0,0) );
    CPen* pOldPen = pDC->SelectObject( &penLine );
	DrawRect(pDC,rect);
	pDC->SelectObject( pOldPen );

}

void CLSDDoc::StrWrite(int x, int y, CString str , CDC* pDC , int iSzie , int iDevid)
{
   	CFont m_Font;
	CFont * oldfont;

	if ( iDevid == 0 ) m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	else			   m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"Arial Black");

	oldfont = pDC->SelectObject(&m_Font);

	pDC->SelectObject(&m_Font);				
	pDC->TextOut( x, y, str );

	pDC->SelectObject(oldfont);
	m_Font.DeleteObject();
}

CRect CLSDDoc::GetObjArea()
{
	POSITION pos = m_objects.GetHeadPosition();
	CRect r;
	BOOL first = TRUE;
	while ( pos != NULL )
	{
		CGraphObject *item = (CGraphObject *)m_objects.GetNext( pos );
		if (!item->IsKindOf(RUNTIME_CLASS(CMyObject))) {
			if (item->m_position.left < 10000 && item->m_position.right < 10000 && item->m_position.left > -10000 && item->m_position.right > -10000) {
				if (first) r = item->m_position;
				else r |= item->m_position;
				first = FALSE;
			}
			else {

				Remove( item );
			}
		}
	}
	return r;
}

CGraphObject * CLSDDoc::ObjectAtButton(CPoint point, int &cnr)
{
	CRect rect(point, CSize(1,1));

	POSITION pos = m_objects.GetTailPosition();
	while (pos != NULL)
	{
		CGraphObject* item = (CGraphObject *)m_objects.GetPrev( pos );
		if (item->Intersects(rect) && item->IsKindOf(RUNTIME_CLASS(CTrack))) {
			cnr = ((CTrack*)item)->AtSignal(point);
			if (cnr>=0) return item;
		}
	}
	return NULL;
}

BOOL CLSDDoc::isTableType(CGraphObject* pObj)
{
	if (pObj->IsKindOf(RUNTIME_CLASS(CMyObject))) {
		if (pObj->Name == "INTERLOCKING-NAME") return TRUE;
	}
	return FALSE;
}

void CLSDDoc::DrawTableType(CDC* pDC, BOOL org)
{
	if (m_TableType) {
		if (org) {
			m_TableType->Active(TRUE);
			m_TableType->Draw(pDC);
		}
		else {
			CMyObject obj("INTERLOCKING-NAME", -m_size.cx / 2 + 300, m_size.cy / 2 - 60);
			obj.m_pDocument = this;
			obj.GetSize(*m_TableType);
			obj.Active(TRUE);
			obj.Draw(pDC);
		}
	}
}

void CLSDDoc::Export()
{
	CString docname = GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "SFM" );
	docname.ReleaseBuffer( );
	CFile f;
	CFileException e;
	if( f.Open( docname, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
		CDumpContext dc( &f );
#ifdef _DEBUG
		Dump(dc); 
#endif
		CString ttl = "INTER LOCK SCREEN DESIGN FILE V3.0";

		dc << ttl << "\n";
		dc << "선명:" << m_LineName << "\n";
		dc << "역명:" << m_StationName << "\n";
		dc << "기점역:" << m_FromStation << "\n";
		dc << "종점역:" << m_ToStation << "\n";
		dc << "전역(기점):" << m_PrevStation << "\n";
		dc << "후역(종점):" << m_NextStation << "\n";

		dc << "거리:" << m_Kilo << "\n";
		dc << m_objects << "\n";
	
		POSITION pos;
		CGraphObject *item;
		for( pos = m_objects.GetHeadPosition(); pos != NULL; )
		{
			item = (CGraphObject *)m_objects.GetNext( pos );
			item->Dump(dc);
		}

		f.Close();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
#define  MAX_TRACK_ARRAY	50
/////////////////////////////////////////////////////////////////////////////////////////////////////////
class OrderTrackArray {
	int m_nMax;
	CTrack *m_TArray[MAX_TRACK_ARRAY];

public:
	OrderTrackArray()
	{
		Init();
	}
	void Init();
	void Add(CTrack *T);
	void ExtOrderMarking();
	int  Find(const char *s);
	int  ExtFind(const char *s);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
void OrderTrackArray::Init()
{
	for(int i=0; i<MAX_TRACK_ARRAY; i++) {
		m_TArray[i] = NULL;
	}
	m_nMax = 0;
}

void OrderTrackArray::Add(CTrack *T)
{
	if (m_nMax < MAX_TRACK_ARRAY) {
		m_TArray[m_nMax] = T;
		m_nMax++;
	}
}

void OrderTrackArray::ExtOrderMarking()
{
	int i, n1, n2;
	CTrack *T;
	CString sName;

	if ( !m_nMax ) return;
	else if (m_nMax == 1) {
		n1 = m_TArray[0]->Name.Find('.');
		if (n1 >= 0) {
			sName = m_TArray[0]->Name.Left(n1);
			m_TArray[0]->Name = sName;
		}
		return;
	}

	for(i=0; i<m_nMax; i++) {  // Double Slip Check & Set
		n1 = m_TArray[i]->Name.Find('.');
		if (n1 >= 0) n1 = atoi(m_TArray[i]->Name.GetBuffer(1) + n1 + 1);
		else n1 = 0;
		if (!m_TArray[i]->isDoubleSlip() || (((n1 != 1) || (i != 0)) && ((n1 != 2) || (i != 1)))) {
			for(int j=i+1; j<m_nMax; j++) {
				n2 = m_TArray[j]->Name.Find('.');
				if (n2 >= 0) n2 = atoi(m_TArray[j]->Name.GetBuffer(1) + n2 + 1);
				if ( m_TArray[j]->isDoubleSlip() ) {
					if (((i == 0) && (n2 == 1)) || ((i == 1) && (n2 == 2))) {
						T = m_TArray[i];
						m_TArray[i] = m_TArray[j];
						m_TArray[j] = T;
						break;
					}
				}
			}
		}
	}

	for(i=0; i<m_nMax; i++) { // Order Marking
		if ( !m_TArray[i]->isDoubleSlip() ) {
			n1 = m_TArray[i]->Name.Find('.');
			if (n1 >= 0) sName = m_TArray[i]->Name.Left(n1);
			else sName = m_TArray[i]->Name;
			m_TArray[i]->Name.Format("%s.%d", sName, i+1);
		}
	}
}

int  OrderTrackArray::Find(const char *s)
{
	for(int i=0; i<m_nMax; i++) {
		if ( !strcmp(m_TArray[i]->Name.GetBuffer(1), s) ) 
			return i;
	}
	return -1;
}

int  OrderTrackArray::ExtFind(const char *s)
{
	for(int i=0; i<m_nMax; i++) {
		if ( !strcmpTName(m_TArray[i]->Name.GetBuffer(1), s) ) 
			return i;
	}
	return -1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
void CLSDDoc::CreateToTrackOrders()
{
	POSITION pos1, pos2;
	CTrack *T;
	CGraphObject *item;
	const char *sName;
	OrderTrackArray TItems;
	OrderTrackArray TArray;

	pos1 = m_objects.GetHeadPosition();
	while (pos1 != NULL) {
		item = (CGraphObject*)m_objects.GetNext(pos1);
		if (item->IsKindOf(RUNTIME_CLASS(CTrack)) && item->isValid()) {
			T = (CTrack*)item;
			if (TItems.ExtFind(T->Name.GetBuffer(1)) == -1) {
				TItems.Add( T );
				sName = T->Name.GetBuffer(1);
				TArray.Init();
				pos2 = m_objects.GetHeadPosition();
				while (pos2 != NULL) {
					item = (CGraphObject*)m_objects.GetNext(pos2);
					if (item->IsKindOf(RUNTIME_CLASS(CTrack)) && item->isValid()) {
						T = (CTrack*)item;
						if ( !strcmpTName(T->Name.GetBuffer(1), sName) )
							TArray.Add(T);
					}
				}
				TArray.ExtOrderMarking();
			}
		}
	}
}

void CLSDDoc::MarkingToTrackOrders(const char *s)
{
	if (s == NULL) return;
	POSITION pos;
	CTrack *T;
	CGraphObject *item;
	OrderTrackArray TArray;

	pos = m_objects.GetHeadPosition();
	while (pos != NULL) {
		item = (CGraphObject*)m_objects.GetNext(pos);
		if (item->IsKindOf(RUNTIME_CLASS(CTrack)) && item->isValid()) {
			T = (CTrack*)item;
			if ( !strcmpTName(T->Name, s) ) TArray.Add(T);
		}
	}
	TArray.ExtOrderMarking();
}


BOOL CLSDDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	return CDocument::OnSaveDocument(lpszPathName);
}
