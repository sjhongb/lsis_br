//=======================================================
//==              LSDDoc.h
//=======================================================
//	파 일 명 :  LSDDoc.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _LSDDOC_H
#define _LSDDOC_H

#include "GraphObj.h"

/////////////////////////////////////////////////////////////////////////////
// Defined Dot Info.
typedef struct {
	int  nDot;
	int  nMoveX;
	int  nMoveY;
	int  nViewX;
	int  nViewY;
	int  nSystemY;
	DWORD  nDesInfoStatus;
} SFMDotInfoType;


#define DESINFO_REVERSE		0001

/////////////////////////////////////////////////////////////////////////////
class StationDB;
class CMyObject;
class CLSDDoc : public CDocument
{
protected: // create from serialization only
	CSize m_size;
	DECLARE_DYNCREATE(CLSDDoc)
	int m_nMapMode;
public:
	int m_testFlag;
	CLSDDoc();
	BOOL m_bSFMMode;
	void TrackMatchToDoubleSlip();
	void CreateToTrackOrders();
	void MarkingToTrackOrders(const char *s);
	void Set_ModifiedFlag();
	void Copy();
	void Cut();
	void Undo();
	void Paste();
	void Redo();
	void SetDeleteCnt(int iCnt);
	void SaveItem(CGraphObject *pObj , int iVal , int iSelectObjCnt);
	void SaveClipBoard(CGraphObject *pObj);
	void DeleteItem();
	void StrWrite(int x, int y, CString str , CDC* pDC ,int iSzie = 10 , int iDevid = 0 );
	CObList m_objects;

	CObList m_objbak1;
	CObList m_objbak2;
	CObList m_objbak3;
	CObList m_objbak4;
	CObList m_objbak5;

	int m_iUnDoCnt[1000][2]; // [x][0]  백업의 종류 1: ADD 2: REMOVE 3: MOVE [x][1] 동일하게 선택된 객체의 카운트 

	CString Month;
public:
	void Export();
	void DrawTableType(CDC* pDC, BOOL org);
	BOOL isTableType(CGraphObject* pObj);
	BOOL isViewAll() {
		return m_nType & 0x80;
	}
	CGraphObject * ObjectAtButton(CPoint point, int &cnr);
	CRect GetObjArea();
	void DrawOutline(CDC *pDC, CPrintInfo *pInfo);
	void DrawPageOff(CDC *pDC, CPrintInfo *pInfo);
	void ComputePageSize();
	CGraphObject* m_SelectedObject;
	CBlip * m_SelectedBlip;

	CMyObject* m_TableType;

	SFMDotInfoType m_DotInfo;
	CString m_LineName, m_StationName, m_FromStation, m_ToStation;
	CString m_PrevStation, m_NextStation, m_Kilo;
	int  m_nUpDn, m_nType;
	BOOL m_bOnViewSystemButton;

// Attributes
public:
	void Draw(CDC *pDC, CLSDView *pView);
	void PropertyChange(CWnd *wnd);
	void BlipMoveTo(CPoint point);
	CGraphObject* ObjectAt(const CPoint &point);
	CObList *GetObjects() { return &m_objects; }
	const CSize& GetSize() const { return m_size; }

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLSDDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLSDDoc();
	void Add(CGraphObject *pObj , int iAddCnt = 1);
	void CloneAdd(CGraphObject *pObj);	
	void Remove(CGraphObject *pObj , int iDelCnt = 1);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLSDDoc)
	afx_msg void OnMaketable();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

friend StationDB;
};

/////////////////////////////////////////////////////////////////////////////
#endif
extern CString __editdev1,__editdev2,__editdev3,__editdev4,__editdev5,__editdev6,__editdev7;
extern CString __editdev8,__editdev9,__editdev10,__editdev11,__editdev12,__editdev13,__editdev14;
extern CString __editdev15,__editdev16,__editdev17,__editdev18,__editdev19,__editdev20,__editdev21;
