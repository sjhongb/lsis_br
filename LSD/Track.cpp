//=======================================================
//==              Track.cpp
//=======================================================
//	파 일 명 :  Track.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include <iostream.h>
#include <fstream.h>

#include "Track.h"
#include "TrackDlg.h"

#include "LSDDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(CTrack, CGraphObject, 0)

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

#define INVALID_POS	(CPoint(10000,0))
extern int REVERSE;
extern int _nDotPerCell;

/*
	5	6	7
	  \ | /
	4 -	p1-	0		X 2
	  / | \
	3	2	1
*/
int Vector(CPoint p1, CPoint p2) {	// 0,1,2...,7
	CPoint p = p1 - p2;
	if (p.x == 0) {
		if (p.y == 0) {
			return -1;
		}
		else if (p.y > 0) {
			return 2;
		}
		else {
			return 6;
		}
	}
	else if (p.x > 0) {
		if (p.y == 0) {
			return 0;
		}
		else if (p.y > 0) {
			return 1;
		}
		else {
			return 7;
		}
	}
	else {
		if (p.y == 0) {
			return 4;
		}
		else if (p.y > 0) {
			return 3;
		}
		else {
			return 5;
		}
	}
}

#include <math.h>
int isBranchVect(CPoint pS, CPoint pN, CPoint pR)
{

	double nVect = atan2(double(pN.y - pS.y), double(pN.x - pS.x));
	double rVect = atan2(double(pR.y - pS.y), double(pR.x - pS.x));

	
	if (( nVect >= +0. && nVect <= +1.58 ) || ( nVect <= -0. && nVect >= -1.56 )) {
		if ( rVect >= +0. && rVect <= +1.58 )  return 1;
		else if ( rVect >= +1.56 && rVect <= +3.15 ) return 2;
		else if ( rVect >= -3.15 && rVect <= -1.56 ) return 3;
		else if ( rVect >= -1.58 && rVect <= -0. ) return 4;
	} else if (( nVect >= +1.56 && nVect <= +3.15 ) || ( nVect >= -3.15 && nVect <= -1.58 ))  {
		if ( rVect >= +0. && rVect <= +1.58 )  return 5;
		else if ( rVect >= +1.56 && rVect <= +3.15 ) return 6;
		else if ( rVect >= -3.15 && rVect <= -1.56 ) return 7;
		else if ( rVect >= -1.58 && rVect <= -0. ) return 8;
	} else {
		return 0;
	}
    return 0;
}

int strcmpTName(const char *s1, const char *s2)
{
	CString t1, t2;

	t1 = s1;
	t2 = s2;
	int ispariod = t1.Find('.');
	if (ispariod >= 0) t1.SetAt(ispariod, 0x00);
	ispariod = t2.Find('.');
	if (ispariod >= 0) t2.SetAt(ispariod, 0x00);
	return ( strcmp(t1.GetBuffer(1), t2.GetBuffer(1)) );
}

//=======================================================
//
//  함 수 명 :  DelStringMultiNo
//  함수출력 :  CString &s
//  함수입력 :  CString &s
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  둘로 나뉜 동일 트랙명의 구분자를 삭제 한다. 1T.1 -> 1T
//
//=======================================================
void DelStringMultiNo(CString &s , BOOL bPOINT)
{
	CString t;
	// BR 전용 옵션 : BR 의 전철기 트랙은 모두 W- 의 명칭을 갖는다. 
	if ( bPOINT == TRUE ) {
		if ( s.Find("W")>=0 ) return;
	}
	if ( s.Find("HT-",0 ) != -1 ) return;
	int n = s.Find('.');
	if (n > 0) {
		t = s.Left(n);
		s = t;
	}
}

void DelStringLastSpace(CString &str)
{
	CString s = str;
	int n = s.Find(' ');
	if (n > 0) str = s.Left(n);
	else str = s;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CTrack::CTrack(int t, int cx, int cy) : CGraphObject(CRect(CPoint(cx,cy), CSize(5,5)),"t") 
{
	m_iBranchVect = 0;
	m_sSpare = "";
	// For database construct only	
	m_pDoubleSlip = NULL;
	m_pConnect[0] = m_pConnect[1] = m_pConnect[2] = NULL;
	m_nConnect[0] = m_nConnect[1] = m_nConnect[2] = 0;
	m_bKeyLock = FALSE;
	Init();
	m_nShape = B_NAME_C;

	x = cx;
	y = cy;
	SetName("NEW");

	CPoint dr(GridSize,0);
	CPoint dl(-GridSize,0);
	m_cButton[0].Offset(dr); 
	m_cButton[1].Offset(dr); 
	m_cLevelCross.Offset(dr); 

	c = CBlip(CPoint(-GridSizeH * 6,0));
	p = CBlip(CPoint( GridSizeH * 6,0));
	n = CBlip(CPoint(GridSizeH * 6,0));
	r = CBlip(CPoint(GridSizeH * 6,0));
	l = CBlip(CPoint(GridSizeH * 6,0));

	m_cSignalC[0].Offset(c);
	m_cSignalC[1].Offset(c);
	m_cSignalN[0].Offset(n);
	m_cSignalN[1].Offset(n);
	m_cSignalR[0].Offset(n);
	m_cSignalR[1].Offset(n);

	CPoint base(cx,cy);
	for (int i=0; i<m_nHandle; i++) {
		CBlip *bl = m_pBlips[i];
		if (bl) bl->Offset(base);
	}

	Init();
	m_pBmpInfo = new CBmpTrack();
}

int GetTokenTrackName(const char *s, CString *obj)
{
	int i = 0;
	int n = 0;

	if (obj != NULL) {
		obj[0] = obj[1] = obj[2] = obj[3] = "";
	}
	if ( s ) {
		while(*s) {
			if (*s == ',') {
				if ( i ) {
					n++;
					i = 0;
				}
			}
			else {
				if ((obj != NULL) && (n < 4)) obj[n] += *s;
				i++;
			}
			s++;
		}
		if ( i ) n++;
	}
	return n;
}

#define GRID_DOUBLESLIP_DEFSIZE		8
void CTrack::Init() {
	if ((m_nShape & B_DOUBLE_SLIP) && m_pDoubleSlip) { // D-S add code
		m_nShape |= B_OPEN_C;
		CString name =GetName();
		int dir = (name.Find(".2") >= 0) ? 1 : -1;
		if (dir == 1) p = m_pDoubleSlip->p;
		c = p;
		if (n.x == p.x) {
			n.x = p.x + (GridSizeH * GRID_DOUBLESLIP_DEFSIZE * dir);
			n.y = p.y;
		}
		if ((r.x == p.x) || (r.y == p.y)) {
			r.x = p.x + (GridSizeH * GRID_DOUBLESLIP_DEFSIZE * dir);
			r.y = p.y + (GridSizeH * GRID_DOUBLESLIP_DEFSIZE * dir);
		}
	}
	m_pBlips[0] = &c;
	m_pBlips[1] = &n;
	m_pBlips[2] = &r;
	m_pBlips[3] = &p;
	m_pBlips[4] = &nC;
	m_pBlips[5] = &nN;
	m_pBlips[6] = &nR;
	m_pBlips[7] = &m_cButton[0];
	m_pBlips[8] = &m_cButton[1];
	m_pBlips[9] = &m_cSignalC[0];
	m_pBlips[10] = &m_cSignalC[1];
	m_pBlips[11] = &m_cSignalN[0];
	m_pBlips[12] = &m_cSignalN[1];
	m_pBlips[13] = &m_cSignalR[0];
	m_pBlips[14] = &m_cSignalR[1];
	m_pBlips[15] = &m_cLevelCross;
	m_pBlips[16] = NULL;
	m_nHandle = 16;
	c.Visible( TRUE );
	c.SetName("c");
	n.Visible( TRUE );
	n.SetName("n");
	p.Visible( TRUE );
	p.SetName("s");
	if (m_sBranchName != "") {
		m_iBranchVect = isBranchVect(p, n, r);
		r.Visible( TRUE );
	}
	else {
		m_iBranchVect = TRUE;
		r.Visible( FALSE );
	}
	r.SetName("r");
	if (m_sLevelCross != "") {
		l.Visible( TRUE );
	}
	else {
		l.Visible( FALSE );
	}
	l.SetName("l");
	nC.Visible(m_nShape & B_NAME_C);
	nN.Visible(m_nShape & B_NAME_N);
	nR.Visible(m_nShape & B_NAME_R);

	if (m_nShape & B_DOUBLE_SLIP) 
		GetTokenTrackName(m_sBranchName.GetBuffer(1), &m_sBranchNameItems[0]);

	UpdateRect();
	if ( m_bKeyLock != 1 ) m_bKeyLock = 0;
	m_bIsBmpMode=0;
}

BOOL CTrack::isNoGrid(CBlip *pBlip)
{
	if ( ((CMyButton*)m_pBlips[7])->isValid() && ((CMyButton*)m_pBlips[7])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CMyButton*)m_pBlips[8])->isValid() && ((CMyButton*)m_pBlips[8])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[9])->isValid() && ((CSignal*)m_pBlips[9])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[10])->isValid() && ((CSignal*)m_pBlips[10])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[11])->isValid() && ((CSignal*)m_pBlips[11])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[12])->isValid() && ((CSignal*)m_pBlips[12])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[13])->isValid() && ((CSignal*)m_pBlips[13])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[14])->isValid() && ((CSignal*)m_pBlips[14])->isNoGrid(pBlip) ) return TRUE;
	else if ( ((CSignal*)m_pBlips[15])->isValid() && ((CSignal*)m_pBlips[15])->isNoGrid(pBlip) ) return TRUE;
	else 
		return FALSE;
}

BOOL CTrack::isMainLine() { 
	if (!(m_nShape & B_MAIN)) return FALSE; 
	if ((!m_cButton[0].isValid() && !m_cButton[1].isValid())) return TRUE;
	if (( m_cButton[0].isValid() && m_cButton[0].m_nType != 1) || 
		( m_cButton[1].isValid() && m_cButton[1].m_nType != 1)) return TRUE;
	return FALSE;
}

#define GRID_DOUBLESLIP_GAPX	4
#define GRID_DOUBLESLIP_GAPY	4
void CTrack::DrawDoubleSlip(CDC* pDC)
{
	int dirx, diry;
	CBlip m1, e1, e2;
	CBlip text1, text2;
	CSize sz1, sz2;

	DrawSegment(pDC, c, p, 1);			// 1,2,4 -> C,N,R
	DrawSegment(pDC, n, p, 2, TRUE);	// 1,2,4 -> C,N,R
	DrawSegment(pDC, r, p, 4, TRUE);
	e1 = p;
	if (n.x == p.x) dirx = 0;
	else { dirx = (n.x < p.x) ? -1 : 1; }
	if (n.y == p.y) diry = 0;
	else { diry = (n.y < p.y) ? -1 : 1; }
	e1.x += (GridSizeH * GRID_DOUBLESLIP_GAPX) * dirx;
	e1.y += (GridSizeH * GRID_DOUBLESLIP_GAPY) * diry;
	e2 = m_pDoubleSlip->p;
	if (m_pDoubleSlip->r.x == m_pDoubleSlip->p.x) dirx = 0;
	else { dirx = (m_pDoubleSlip->r.x < m_pDoubleSlip->p.x) ? -1 : 1; }
	if (m_pDoubleSlip->r.y == m_pDoubleSlip->p.y) diry = 0;
	else { diry = (m_pDoubleSlip->r.y < m_pDoubleSlip->p.y) ? -1 : 1; }
	e2.x += (GridSizeH * GRID_DOUBLESLIP_GAPX) * dirx;
	e2.y += (GridSizeH * GRID_DOUBLESLIP_GAPY) * diry;
	pDC->MoveTo(e1);
	pDC->LineTo(e2);
	AddLines(new CLine(e1,e2));			

	m1 = p;
	DrawHatch(pDC, FALSE);					// 빗금처리 
	p = e1;
	pC = e1;								//
	pN = m1;									//    	
	pR = e2;								//
	DrawHatch(pDC, FALSE);					// 빗금처리 
	p = e2;
	pC = e2;								//  
	pN = e1;								//  
	pR = m1;									//  
	DrawHatch(pDC, FALSE);					// 빗금처리 
	p = m1;

	// 이름 표시
	CString n1 = m_pDoubleSlip->m_sBranchNameItems[0];
	CString n2 = m_pDoubleSlip->m_sBranchNameItems[1];
	sz1 = pDC->GetTextExtent(n1, n1.GetLength());
	sz2 = pDC->GetTextExtent(n2, n2.GetLength());
	if (n.x > p.x) {
		text1.x = e1.x + (sz1.cx / 2);
		text2.x = e1.x + (sz2.cx / 2);
	}
	else {
		text1.x = e1.x - (sz1.cx / 2);
		text2.x = e1.x - (sz2.cx / 2);
	}
	if (r.y > p.y) {
		text1.y = e1.y + (GridSizeH * 4);
		text2.y = e1.y - (GridSizeH / 2);
	}
	else {
		text1.y = e1.y - (GridSizeH / 2);
		text2.y = e1.y + (GridSizeH * 4);
	}
	text1.y -= 8;	
	if ( !m_pDocument || !m_pDocument->m_bSFMMode )		// 2000.8.25.IM
	{
		TextOutAtPointCenter( pDC, text1, n1 );
		TextOutAtPointCenter( pDC, text2, n2 );
	}
	short nDotPerCell = 10;
	if ( m_pDocument ) {
		nDotPerCell = m_pDocument->m_DotInfo.nDot;
	}
	if (n.x > p.x) {
		text1.x += 2;		
		if( nDotPerCell < 10 )
			text1.x += 15;
	}
	else {
		text1.x -= 2;
		if( nDotPerCell < 10)
			text1.x -= 5;	
	}

	if (r.y > p.y) {
		text1.y += 1;
		if(_nDotPerCell < 10)
			text1.y += 1;		
	}
	else {
		text1.y -= 1;
		if( nDotPerCell < 10 )
			text1.y -= 1;	
	}

	CString strSlipName = m_sBranchNameItems[0].Left(2);
	char s1,s2;
	s1 = m_sBranchNameItems[0].GetAt(2);

	s2 = n2.GetAt(2);
	strSlipName += s1;

	strSlipName += s2;

	CPoint posName = text1;		// 명칭 표시 위치 계산
	sz1 = pDC->GetTextExtent( strSlipName, strSlipName.GetLength() );

	if (r.y > p.y) {

		if(_nDotPerCell < 10)
			posName.y += 1;		
	}
	else {

		if( nDotPerCell < 10 )
			posName.y -= 1;	
	}
	if (n.x > p.x) {
		posName.x += 22;
		posName.x += sz1.cx/2;
		if( nDotPerCell < 10)
			posName.x += 20;
	}
	else {
		posName.x -= 22;
		posName.x -= sz1.cx/2;
		if( nDotPerCell < 10)
			posName.x -= 20;	
	}
	ChangeMapping(text1,text2);
	ChangeMapping( posName );	

	((CBmpTrack*)m_pBmpInfo)->SetSwitch(text1, (char*)(LPCTSTR)strSlipName, posName , m_bKeyLock);

}

BOOL CTrack::isDoubleSlip()
{
	return BOOL(m_nShape & B_DOUBLE_SLIP);
}

void CTrack::Draw(CDC* pDC , COLORREF cColor)
 {
((CBmpTrack*)m_pBmpInfo)->Init();
((CBmpTrack*)m_pBmpInfo)->SetName( (char*)(LPCTSTR)Name );
if ( m_sBranchPower.GetLength() >= MAXBRENCHPOWER ) {
	if ( m_sBranchPower.Mid(20,1) == "P" ) {
		((CBmpTrack*)m_pBmpInfo)->SetAdjacencyPoint(m_sBranchPower.Mid(21,4));
	}
	else
	{
		((CBmpTrack*)m_pBmpInfo)->SetAdjacencyPoint("0");
	}

	if ( m_sBranchPower.Mid(220,1) == "P" ) {
		((CBmpTrack*)m_pBmpInfo)->SetAdjacencyPoint2(m_sBranchPower.Mid(221,4));
	}
	else
	{
		((CBmpTrack*)m_pBmpInfo)->SetAdjacencyPoint2("0");
	}

	int nBufferLength = 150;
	CString strBuffer = m_sBranchPower.Mid(60,150);
	for( int nCount = 150; nCount > 0; nCount--)
	{
		if(strBuffer.Right(1) == ' ')
		{
			strBuffer = strBuffer.Left(nCount-1);
		}
	}
	if ( m_sBranchPower.Mid(60,1) == " " ) {
		((CBmpTrack*)m_pBmpInfo)->SetOverlapRoute("0");
	}
	else
	{
		((CBmpTrack*)m_pBmpInfo)->SetOverlapRoute(strBuffer);
	}
///////////////////////////////////////////////////////////////////////////////////////// 20130903 Overlap 표현방식 변경.
// 	if ( m_sBranchPower.Mid(60,1) == "U" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS(m_sBranchPower.Mid(61,4), 0);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS(m_sBranchPower.Mid(66,4), 0);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS("0", 0);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS("0", 0);
// 	}
// 	if ( m_sBranchPower.Mid(70,1) == "U" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS(m_sBranchPower.Mid(71,4), 1);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS(m_sBranchPower.Mid(76,4), 1);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS("0", 1);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS("0", 1);
// 	}
// 	if ( m_sBranchPower.Mid(80,1) == "U" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS(m_sBranchPower.Mid(81,4), 2);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS(m_sBranchPower.Mid(86,4), 2);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackUS("0", 2);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointUS("0", 2);
// 	}
// 	
// 	if ( m_sBranchPower.Mid(90,1) == "D" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS(m_sBranchPower.Mid(91,4), 0);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS(m_sBranchPower.Mid(96,4), 0);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS("0", 0);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS("0", 0);
// 	}
// 	if ( m_sBranchPower.Mid(100,1) == "D" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS(m_sBranchPower.Mid(101,4), 1);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS(m_sBranchPower.Mid(106,4), 1);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS("0", 1);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS("0", 1);
// 	}
// 	if ( m_sBranchPower.Mid(110,1) == "D" ) {
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS(m_sBranchPower.Mid(111,4), 2);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS(m_sBranchPower.Mid(116,4), 2);
// 	}
// 	else
// 	{
// 		((CBmpTrack*)m_pBmpInfo)->SetDestinationTrackDS("0", 2);
// 		((CBmpTrack*)m_pBmpInfo)->SetReferencePointDS("0", 2);
// 	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
}
	CPen newPen;
	COLORREF color = cColor;

	if (!isValid()) color = RGB(192,128,128);
    if( newPen.CreatePen( PS_SOLID, 1, color ) )
    {
        CPen* pOldPen = pDC->SelectObject( &newPen );
		RemoveLines();

		if ( m_nShape & B_IN_C ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 1 );
		if ( m_nShape & B_OUT_C ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 2 );
		if ( m_nShape & B_IN_N ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 3 );
		if ( m_nShape & B_OUT_N ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 4 );
		if ( m_nShape & B_IN_R ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 5 );
		if ( m_nShape & B_OUT_R ) ((CBmpTrack*)m_pBmpInfo)->SetArrow( 6 );

		if ((m_nShape & B_DOUBLE_SLIP) && m_pDoubleSlip) {		// D-S Add Code
((CBmpTrack*)m_pBmpInfo)->SetCNRPntDes(c,n,r);
			m_iBranchVect = isBranchVect(p, n, r);
			DrawDoubleSlip(pDC);
((CBmpTrack*)m_pBmpInfo)->SetDoubleSlip();
		}
		else {
((CBmpTrack*)m_pBmpInfo)->SetCNRPntDes(c,n,r);
			if (m_sLevelCross != "") {		// 분기 
				DrawSegment(pDC, l, p, 8 ,TRUE );		// 1,2,4 -> C,N,R
			}
			DrawSegment(pDC, c, p, 1);		// 1,2,4 -> C,N,R
			if (m_sBranchName != "") {		// 분기 
				m_iBranchVect = isBranchVect(p, n, r);
				DrawSegment(pDC, r, p, 4, TRUE);
				DrawSegment(pDC, n, p, 2, TRUE);
				DrawHatch(pDC);// 빗금처리 
			}
			else {
				DrawSegment(pDC, n, p, 2, TRUE);// 직선 
			}

			if (m_nShape & B_MAIN) {
				if (Name != "" && Name.GetAt(0) != '?') {
					CPoint b = m_cButton[0] + m_cButton[1];
					b.x /= 2;
					b.y /= 2;
					pDC->MoveTo(b.x, b.y + GridSize);
					pDC->LineTo(b.x, b.y - GridSize);
				}
			}
		}

		((CBmpTrack*)m_pBmpInfo)->m_bLineLimit = (( m_nShape & B_TEND_R )||( m_nShape & B_TEND_N )||( m_nShape & B_TEND_C ));
		if ( (m_nShape & B_DIF_TRACK) != 0 ) {
			((CBmpTrack*)m_pBmpInfo)->SetDifTrack();
		}

		if ( !m_pDocument || !m_pDocument->m_bSFMMode )		// 2000.8.25.IM
			CGraphObject::Draw(pDC , color);			//시그날 그리기 

		char name[16];   										
		for (int nHandle = 14; nHandle > 6 ; nHandle--) {			
			if (m_pBlips[nHandle]->isValid())		{				
				if(nHandle == 7 || nHandle == 8)	{					// 버튼 
					CMyButton *but = (CMyButton*)m_pBlips[nHandle];
					strcpy(name,but->Name);								// 압구명
					but->Draw(pDC , color);//omani 추가 
					BOOL bIsMainTrack = TRUE;
					char *p = (char*)(LPCTSTR)(but->Name);
					while (*p) {
						if ( !isdigit(*p) ) {
							bIsMainTrack = FALSE;
							break;
						}
						p++;
					}
					CPoint cp;	
					cp.x = but->x;
					cp.y = but->y;
					//int nLR = ( bIsMainTrack ) ? but->m_nLR+1 : 0;
					int nLR =  but->m_nLR+1;
					if ( nLR ) {
						if( nLR == 2 ) {			
							//strcat(name,"UP");
							cp.x -= ( 12 )*2;
						}
						else { 
							//strcat(name,"DN");
							cp.x += ( 12 )*2;
						}
					}

					ChangeMapping(cp);
					((CBmpTrack*)m_pBmpInfo)->SetButton(cp, name, nLR, but->m_nType + 1 );
				}
				else	{												// signal					
					short namecheck=0;
					CSignal *m_cSignal = (CSignal*)m_pBlips[nHandle];
					strcpy(name,m_cSignal->Name);						// 신호기, 압구 명 
					if (m_cSignal->m_nShape & B_NAME_B)					// 압구명 일때 	nB
						namecheck |= 0x02;
					if (m_cSignal->m_nShape & B_NAME_S)					// 신호기명 일때 nS
						namecheck |= 0x01;					
					short HasRoute = 0;
//					if(m_cSignal->m_nType <= 1 && m_cSignal->m_nRoute > 1) {
					if ((m_cSignal->m_nShape & B_NOROUTE) != 0 )
							HasRoute = 1;				
//					}
					
					CPoint sigpos = m_cSignal->s;										

					CPoint np1 = m_cSignal->nS;
					CPoint np2 = m_cSignal->nB;
					int gap=0;

					if( _nDotPerCell < 10 )	{
						if(np1.x > sigpos.x)
							np1.x += 15;
						else np1.x -= 15 ;
						if(np2.x > sigpos.x)
							np2.x += 15;
						else np2.x -= 15;
						gap =4;
					}
					// AKA BYPASS 구간의 뒤집어진 신호시들을 위해 신호기의 좌우가 뒤바껴 있을경우 바로 잡아준다.
					int iLR;
					iLR = m_cSignal->m_nLR;
					if ( m_cSignal->m_nUD == 1 ) {
						if ( iLR == 0 ) iLR = 1;
						else if ( iLR == 1 ) iLR = 0;
					}
					((CBmpTrack*)m_pBmpInfo)->SetSignalDes(
						sigpos, np1 , name, np2, namecheck,
						m_cSignal->m_nType, m_cSignal->m_nLamp, m_cSignal->m_nILR,m_cSignal->m_iDiry,
						/*m_cSignal->m_nLR*/iLR, m_cSignal->m_bF1, HasRoute, (m_cSignal->m_nShape >> 4) & 3);  // m_cSignal->m_bF1 -> 유도신호기

				}
			}
		}				

        pDC->SelectObject( pOldPen );
    }
    else
    {
        // Alert the user that resources are low
    }

}

void DrawArrow(CDC *pDC, CPoint a1, CPoint a2, int w, int ed) {
	POINT p[3];
	p[0] = a2;
	p[1] = p[2] = a1;
	if (!ed) {
		p[1].y += w;
		p[2].y -= w;
	}
	else {
		w = w * 2 / 3;
		p[1].y += w;
		p[2].y -= w;
		if (ed < 0) {
			p[1].x += w;
			p[2].x -= w;
		}
		else {
			p[1].x -= w;
			p[2].x += w;
		}
	}
	CBrush br;
	CBrush *old = pDC->SelectObject(&br);
	pDC->Polygon(p,3);
	pDC->SelectObject(old);
	br.DeleteObject();
}

void CTrack::DrawSegment(CDC *pDC, CPoint a, CPoint p, int wh, BOOL horiz) {
	int ed = 0;
	CPoint tp = a;
	int dir = 1;
	if (a != p) {
		if (a.x < p.x) dir = 1;
		else dir = -1;
		if (m_nShape & wh) {
			pDC->MoveTo(a);
			CPoint np = a;
			switch (wh) {
			case 1 : np = nC; break;
			case 2 : np = nN; break;
			case 4 : np = nR; break;
			}
			CPoint tl, tr;

			CSize sz = pDC->GetTextExtent(Name);
			sz.cx /= 2;
			sz.cx += GridSizeH;

			tl = CPoint(np.x - sz.cx * dir, np.y);
			tr = CPoint(np.x + sz.cx * dir, np.y);

			CRect tlr(CPoint(np.x - sz.cx * dir, np.y - 11),
				      CPoint(np.x + sz.cx * dir, np.y + 11));
			tlr.NormalizeRect();
			m_position |= tlr;

			tp = ClipPoint(tl,a);
			if (tp.y < a.y) ed = -dir;
			else if (tp.y > a.y) ed = dir;
			pDC->LineTo(tp);
			pDC->LineTo(tl);
			AddLines(new CLine(a,tp));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(a,tp,wh);
			AddLines(new CLine(tp,tl));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(tp,np,wh);
			DrawParen(pDC,tl,dir);
			DrawParen(pDC,tr,-dir);
			tp = ClipPoint(tr,p);
			pDC->MoveTo(tr);
			pDC->LineTo(tp);
			pDC->LineTo(p);
			AddLines(new CLine(tr,tp));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(np,tp,wh);
			AddLines(new CLine(tp,p));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(tp,p,wh);

	if ( !m_pDocument || !m_pDocument->m_bSFMMode )
		TextOutAtPointCenter( pDC, np, Name );

	CPoint mp1, mp2;
	mp1 = np;
	if (m_nShape & B_MAIN) {	// 도착선 궤도 ?
		CPoint b = m_cButton[0] + m_cButton[1];
		mp2.x = b.x - mp1.x;
		mp2.y = b.y - mp1.y;
		ChangeMapping(mp2);
	}
	else mp2 = INVALID_POS; 
	ChangeMapping(mp1);

	((CBmpTrack*)m_pBmpInfo)->SetNamePos( mp1, mp2 );
			if (tp==p) tp = tr;			
		}
		else {
			pDC->MoveTo(a);
			if (horiz) tp = ClipPoint(a,p);
			else tp = ClipPoint(p,a);
			CPoint pp = tp;
			if (a == tp) pp = p;
			if (pp.y < a.y) ed = -dir;
			else if (pp.y > a.y) ed = dir;
			pDC->LineTo(tp);
			pDC->LineTo(p);
			AddLines(new CLine(a,tp));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(a,tp,wh);
			AddLines(new CLine(tp,p));
	((CBmpTrack*)m_pBmpInfo)->AddPosDes(tp,p,wh);
			if (p == tp) tp = a;
		}
	}
//  선 끝처리및 빗금 처리 
	CPoint p1,p2,dp(GridSize * dir, 0);
	p1 = CPoint(a.x,a.y + GridSizeH);
	p2 = CPoint(a.x,a.y - GridSizeH);
	int d = 3;
	if (ed) {
		p1.y -= 2;
		p2.y += 2;
		if (ed == 1) {
			p1.x -= d;
			p2.x += d;
			dp.y = GridSize * dir;
		}
		else {
			p1.x += d;
			p2.x -= d;
			dp.y = -GridSize * dir;
		}
	}

// OUT - IN - TEND - CEND
// n7    n6   n5     n4    - m_nShape, High word
//	if (m_nShape & 0x00f00000) {
	if (m_nShape & (wh << 20)) {		// 궤도끝
		if (m_nShape & (wh << 20)) {		// 궤도끝
			CPen newPen;
			if( newPen.CreatePen( PS_SOLID, 2, RGB(0,0,0) ) )
			{
				CPen* pOldPen = pDC->SelectObject( &newPen );
				pDC->MoveTo(p1);
				pDC->LineTo(p2);
				pDC->SelectObject( pOldPen );
			}
			p1 += dp;
			p2 += dp;
		}
		else if (!(m_nShape & (wh << 4))) {		// 절연
			pDC->MoveTo(p1);
			pDC->LineTo(p2);
		}
		else   // 절연 없을때
			((CBmpTrack*)m_pBmpInfo)->SetNoJeulyunDes(a);
	}
	else {
		if (m_nShape & (wh << 24) || 
			m_nShape & (wh << 28)) {		// IN,OUT Arrow
			CPoint a1, a2;
			a1 = a+dp;
			a2 = a+dp+dp;
			if (m_nShape & (wh << 24)) {		// IN Arrow
				DrawArrow(pDC, a1, a2, GridSizeH, ed);
			}
			if (m_nShape & (wh << 28)) {		// OUT Arrow
				DrawArrow(pDC, a2, a1, GridSizeH, ed);
			}
		}
		else if (!(m_nShape & (wh << 4))) {		// 절연
			pDC->MoveTo(p1);
			pDC->LineTo(p2);
		}
		else  // 절연 없을때			
			((CBmpTrack*)m_pBmpInfo)->SetNoJeulyunDes(a);			
	}
	if (m_nShape & (wh << 16)) {		// 궤도회로끝
		pDC->MoveTo(p1.x+dp.x/2,p1.y+dp.y/2);
		pDC->LineTo(p1);
		pDC->LineTo(p2);
		pDC->LineTo(p2.x+dp.x/2,p2.y+dp.y/2);
	}

	switch (wh) {
	case 1 : pC = tp; break;
	case 2 : pN = tp; break;
	case 4 : pR = tp; break;
	}

}

CPoint CTrack::ClipPoint(CPoint &a, CPoint &b)
{
	CPoint p;

	int dy = b.y-a.y, dx = b.x-a.x;
	p.y = a.y;
	if (dx * dy < 0) {
		dy = -dy;
		dx = -dx;
	}
	p.x = b.x - dy;

	if (abs(dx) < abs(dy)) {	// b  ----- a
		p.x = b.x;
		p.y = a.y + dx;
	}
	return p;
}

void CTrack::PropertyChange(CWnd *wnd) {
	CString branchname = m_sBranchName;
	int index, issecond;
	int shape = m_nShape;
	CTrack *track;
	CString  sN1, sN2;

	sN1 = Name;
	CTrackDlg dlg(this);
	dlg.DoModal();
	DelStringLastSpace(Name);
	DelStringLastSpace(m_sBranchName);
	DelStringLastSpace(m_cSignalC[0].Name);
	DelStringLastSpace(m_cSignalC[1].Name);
	DelStringLastSpace(m_cSignalN[0].Name);
	DelStringLastSpace(m_cSignalN[1].Name);
	DelStringLastSpace(m_cSignalR[0].Name);
	DelStringLastSpace(m_cSignalR[1].Name);
	DelStringLastSpace(m_cButton[0].Name);
	DelStringLastSpace(m_cButton[1].Name);

	if (m_nShape & B_DOUBLE_SLIP) { // D-S add code
		if (m_pDoubleSlip == NULL) {
			track = new CTrack(0, p.x, p.y);
			index = Name.Find('.');
			if (index >= 0) sN2 = Name.Left(index);
			else sN2 = Name;
			Name  = sN2;
			Name += ".1";
			track->Name  = sN2;
			track->Name += ".2";
			m_pDocument->Add( track );
			if (track->m_sBranchName == "") track->m_sBranchName = m_sBranchName;
			if (track->m_sLevelCross == "") track->m_sLevelCross = m_sLevelCross;
			if (track->m_sBranchPower == "") track->m_sBranchPower = m_sBranchPower;
			track->m_nShape |= B_DOUBLE_SLIP | B_OPEN_C;
			m_pDoubleSlip = track;
			track->m_pDoubleSlip = this;
			track->Init();
		}
		else if ( strcmp(sN1.GetBuffer(1), Name.GetBuffer(1)) ) {
			char c1, c2; 
			index = sN1.Find('.');
			if ((index >= 0) && (sN1.GetAt(index+1) != '1')) { 
				c1 = '2';
				c2 = '1';
			}
			else {
				c1 = '1';
				c2 = '2';
			}
			index = Name.Find('.');
			if (index >= 0) sN2 = Name.Left(index);
			else sN2 = Name;
			sN2 += '.';
			Name = sN2;
			Name += c1;
			m_pDoubleSlip->Name = sN2;
			m_pDoubleSlip->Name += c2;
		}
	}
	else if (shape & B_DOUBLE_SLIP) { 
		m_pDoubleSlip->m_nShape &= ~B_DOUBLE_SLIP;
		m_pDoubleSlip->m_pDoubleSlip = NULL;
		index = Name.Find('.');
		issecond =(Name.GetAt(index+1) == '2') ? 1 : 0;
		if (index >= 0) {
			sN2  = Name.Left(index);
			Name = sN2;
		}
		if ( issecond ) {	
			RemoveLines();
			sN2 = m_pDoubleSlip->Name.Left(index);
			m_pDocument->Remove(this);
			m_pDocument->MarkingToTrackOrders( sN2.GetBuffer(1) );	
			return;
		}
		else {
			m_pDoubleSlip->RemoveLines();
			m_pDocument->Remove(m_pDoubleSlip);
			m_pDoubleSlip = NULL;
		}
	}
	if ( strcmp(sN1.GetBuffer(1), Name.GetBuffer(1)) ) {
		if ( strcmp(sN1.GetBuffer(1), "NEW") )
			m_pDocument->MarkingToTrackOrders( sN1.GetBuffer(1) );
		m_pDocument->MarkingToTrackOrders( Name.GetBuffer(1) );
	}
	Init();
}

void CTrack::DrawParen( CDC *pDC, CPoint p, int dir )
{
	int x1, y1, x2, y2, x3, y3, x4, y4, x5;
	if (dir>0) {
		x1 = p.x;
		y1 = p.y + GridSize;
		x2 = p.x + GridSize * 3;
		y2 = p.y - GridSize;
		x3 = x4 = p.x - GridSize;
		// 사각형에 내접하는 호.
		x5 = p.x + 5;
		y3 = p.y + 5; 
		y4 = p.y - 5;
	}
	else {
		x1 = p.x;
		y1 = p.y - GridSize;
		x2 = p.x - GridSize * 3;
		y2 = p.y + GridSize;
		x3 = x4 = p.x + GridSize;
		// 사각형에 내접하는 호.
		x5 = p.x - 5;
		y3 = p.y- 5;
		y4 = p.y+ 5;
	}
	pDC->Arc(x1, y1, x2, y2, x5, y3, x5, y4);
}

void CTrack::Read(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar >> m_nShape >> m_sBranchName >> m_bKeyLock >> m_sBranchPower;
	if (m_nShape & B_DOUBLE_SLIP) {
		m_nShape |= B_OPEN_C;
	}
	Init();
}

void CTrack::Write(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar << m_nShape << m_sBranchName << m_bKeyLock << m_sBranchPower;
}

void CTrack::Serialize(CArchive &ar) {
	if (ar.IsStoring()) {
		Write(ar);
	}
	else {
		Read(ar);
	}
}

CGraphObject* CTrack::Clone(CLSDDoc* pDoc)
{
	ASSERT_VALID(this);
	CTrack* pClone = (CTrack*)CGraphObject::Clone(pDoc); 
	pClone->SetName("NEW"); //omani

	ASSERT_VALID(pClone);
	return pClone;
}

CGraphObject* CTrack::SaveClone(CLSDDoc* pDoc)
{
	ASSERT_VALID(this);
	CTrack* pClone = (CTrack*)CGraphObject::SaveClone(pDoc); 
	ASSERT_VALID(pClone);
	return pClone;
}

///////////////////////////////////////////////////////////////////////////////
extern int _UseSmallText;

///////////////////////////////////////////////////////////////////////////////

POINT UnitVector[16] = {
	{1,0},
	{1,1},
	{0,1},
	{-1,1},
	{-1,0},
	{-1,-1},
	{0,-1},
	{1,-1},
	{1,0},
	{1,1},
	{0,1},
	{-1,1},
	{-1,0},
	{-1,-1},
	{0,-1},
	{1,-1}
};

//
//     8     4     2
//           |   
//    10   - * -   1
//           | 
//    20    40    80 
//

///////////////////////////////////////////////////////////////////////////////

CRect CTrack::DrawHatch(CDC *pDC, BOOL bIsDrawText)
{
	int vC, vN, vR;
	vC = Vector(pC,p);
	vN = Vector(pN,p);
	vR = Vector(pR,p);
	
	if (pC.x == 0 && pC.y == 0) vC = vN;

	CRect rect(p.x,p.y,p.x,p.y);
	if (vN >= 0) {
		if (vC < vN) vC += 8;
		if (vR < vN) vR += 8;
		CPoint o = p, dp, hp;
		dp = UnitVector[vN];
		if (vC <= vR) {		// N C R
			hp = UnitVector[vN+1];
		}
		else {
			hp = UnitVector[(vN+7) % 8];
		}

		if ((m_nShape & B_DOUBLE_SLIP) && dp.y) { 
			dp.x *= 5;
			dp.y *= 2;
			hp.x *= GridSize;
			hp.y *= GridSize;
		}
		else {
			dp.x *= 3;
			dp.y *= 3;
			hp.x *= GridSize * 2 / 3;
			hp.y *= GridSize * 2 / 3;
		}
		pDC->MoveTo(o);
		pDC->LineTo(o+hp);
		o += dp;
		pDC->MoveTo(o);
		pDC->LineTo(o+hp);
		o += dp;
		pDC->MoveTo(o);
		pDC->LineTo(o+hp);
		hp.x = 0;
		hp.y *= 2;
		o += hp;
		CSize sz = pDC->GetTextExtent("A",1);
		if ( bIsDrawText ) {		
			if ( !m_pDocument || !m_pDocument->m_bSFMMode )
				TextOutAtPointCenter( pDC, o, m_sBranchName );
				// 스위치정보 입력 
				o = p;
				CPoint posName = p;
				ChangeMapping( o, posName );
				// mapping 후 처리는 device 좌표(상하 바뀜)
				double dx=0.0, dy = 0.0 , adx = 0.0 , ady = 0.0, adpx = 2.0, adpy = 0.1;
				if( _nDotPerCell < 10)	{
					adx = 4.0;
					ady = 1.0;
				}
				UINT nVectpor = ((CBmpTrack*)m_pBmpInfo)->mJoinCtypeVector | ((CBmpTrack*)m_pBmpInfo)->mJoinNtypeVector | ((CBmpTrack*)m_pBmpInfo)->mJoinRtypeVector;				
				switch (nVectpor) {
				case 0x11 :
				case 0x13 :
				case 0x19 :			// H,UPPER
					dy = 11+ady; break;
				case 0x91 :
				case 0x31 :			// H,LOWER 
					dy = -(11+ady);	break;
				case 0x23 :			// /, right 
					if(_nDotPerCell < 10) { 
						adx += 2;
						if ( _UseSmallText ) { adpx = 1.6; }
						else { 
							adpx = 1.8; 
						}
					}
					dx = -(13+adx); dy = -(11+ady); break;
				case 0x92 :			// Y, right open 
					if(_nDotPerCell < 10) { 
						adx += 2;
						if ( _UseSmallText ) { adpx = 1.6; }
						else { 
							adpx = 1.8; 
						}
					}
					dx = ( 25 + adx );
					adpx = 1.0; break;
				case 0x89 :			// \, right 
					if(_nDotPerCell < 10) { 
						adx += 2;  
						if ( _UseSmallText ) { adpx = 1.6; }
						else { 
							adpx = 1.8; 
						}
					}
					dx = -(23+adx); dy = 11+ady; break;
				case 0x32 : 		// /, left
					if(_nDotPerCell < 10) { 
						if ( _UseSmallText ) { adpx = 2.0; }
						else { 
						}
					}
					dx = 28+adx; dy = 11+ady; break;
				case 0x98 : 		// \, left
					if(_nDotPerCell < 10) { 
						if ( _UseSmallText ) { adpx = 2.0; }
						else { 
						}
					}
					dx = 13+adx; dy = -(11+ady); break;
				}
				o.x += (int)(dx);
				o.y += (int)(dy);

				if (dx == 0) dx = 11+adx;
				// 2000.7.15 추가, 분기명 위치를 지정
				int r;
				if (REVERSE != 1) r = -1;
				else r = 1;

				if ( m_nShape & B_BRANCH_POS_RIGHT ) {
				}
				if ( m_nShape & B_BRANCH_POS_UP ) {
					dx = 0;
					dy = -100 - ady;
					dy *= r;
				}
				if ( m_nShape & B_BRANCH_POS_LEFT ) {
					dx = -11 - adx;
					dx *= r;
				}
				if ( m_nShape & B_BRANCH_POS_DOWN ) {
					dx = 0;
					dy = 100 + ady;
					dy *= r;
				}

				if (m_sBranchName.GetLength() >= 3) {
					if ( _UseSmallText ) { 
						dx = (int)(dx * 0.9);
					}
					else { 
						dx = (int)(dx * (10 + m_sBranchName.GetLength()) / 8.0 );
					}
				}
				posName.x = o.x + (int)(dx * adpx);
				posName.y = o.y + (int)(dy * adpy);
				((CBmpTrack*)m_pBmpInfo)->SetSwitch(o, m_sBranchName.GetBuffer(16), posName , m_bKeyLock);
		}
		rect.left = o.x - sz.cx;
		rect.right = o.x + sz.cx;
		rect.top = o.y;
		rect.bottom = o.y - sz.cy;
		br = rect;
		br.NormalizeRect();
	}
	return rect;
}

void CTrack::UpdateRect()
{
	CGraphObject::UpdateRect();
	if (m_sBranchName != "") m_position |= br; 
	CreateDefaultSignalPos();
}

char * CTrack::GetButtonName(int cnr2)
{
	switch (cnr2) {
	case 0 :	return 	m_cSignalC[0].GetName(); break;
	case 1 :	return 	m_cSignalC[1].GetName(); break;
	case 2 :	return 	m_cSignalN[0].GetName(); break;
	case 3 :	return 	m_cSignalN[1].GetName(); break;
	case 4 :	return 	m_cSignalR[0].GetName(); break;
	case 5 :	return 	m_cSignalR[1].GetName(); break;
	}
	return "";
}

int CTrack::AtSignal(CPoint point)
{
	if (m_cSignalC[0].isValid() && m_cSignalC[0].ButtonAt(point)) return 0;
	else if (m_cSignalC[1].isValid() && m_cSignalC[1].ButtonAt(point)) return 1;
	else if (m_cSignalN[0].isValid() && m_cSignalN[0].ButtonAt(point)) return 2;
	else if (m_cSignalN[1].isValid() && m_cSignalN[1].ButtonAt(point)) return 3;
	else if (m_cSignalR[0].isValid() && m_cSignalR[0].ButtonAt(point)) return 4;
	else if (m_cSignalR[1].isValid() && m_cSignalR[1].ButtonAt(point)) return 5;
	return -1;
}

BOOL CTrack::Connect(CTrack* t, int &n1, int &n2)
{
	if (!t) return FALSE;
	if ((CPoint)c == (CPoint)t->c) {
		n1 = 0; n2 = 0; return TRUE;
	} else if ((CPoint)c == (CPoint)t->n) {
		n1 = 0; n2 = 1; return TRUE;
	} else if ((CPoint)c == (CPoint)t->r) {
		n1 = 0; n2 = 2; return TRUE;
	} else if ((CPoint)n == (CPoint)t->c) {
		n1 = 1; n2 = 0; return TRUE;
	} else if ((CPoint)n == (CPoint)t->n) {
		n1 = 1; n2 = 1; return TRUE;
	} else if ((CPoint)n == (CPoint)t->r) {
		n1 = 1; n2 = 2; return TRUE;
	} else if ((CPoint)r == (CPoint)t->c) {
		n1 = 2; n2 = 0; return TRUE;
	} else if ((CPoint)r == (CPoint)t->n) {
		n1 = 2; n2 = 1; return TRUE;
	} else if ((CPoint)r == (CPoint)t->r) {
		n1 = 2; n2 = 2; return TRUE;
	}
	return FALSE;
}

void AddPointNode(CPoint &p);

BOOL CTrack::GetClipPoint( int node, CPoint a, CPoint b, CPoint &cp1, CPoint &cp2, CPoint &cp3 )
{
	BOOL bNode = FALSE;

	if (((node == 0) && (m_nShape & B_NAME_C)) || ((node == 1) && (m_nShape & B_NAME_N)) 
											   || ((node == 2) && (m_nShape & B_NAME_R))) {
		if (node == 1) cp2 = nN;		// Name N 좌표
		else if (node == 2) cp2 = nR;	// Name R 좌표
		else cp2 = nC;					// Name C 좌표
		cp1 = ClipPoint(cp2,a);			// Name 왼쪽 중간 좌표.
		cp3 = ClipPoint(cp2,b);			// Name 오른쪽 중간 좌표.
		bNode = TRUE;
	}
	else {
		cp1 = a;
		cp3 = b;
		cp2 = ClipPoint(a,b);
		if ((cp2.x != b.x) || (cp2.y != b.y))
			bNode = TRUE;
	}
	return bNode;
}

void CTrack::DrawVector(int n1, int n2)
{
		BOOL   bAlter1 = FALSE, bAlter2 = FALSE;
		CPoint p1, p2, tp1[3], tp2[3];
		if (n1 == 0) {	// In C
			bAlter1 = GetClipPoint(0, p, c, tp1[2], tp1[1], tp1[0]);
			if (n2 == 1) {
				bAlter2 = GetClipPoint(1, n, p, tp2[2], tp2[1], tp2[0]);
				p2 = n;
			}
			else {
				bAlter2 = GetClipPoint(2, r, p, tp2[2], tp2[1], tp2[0]);
				p2 = r;
			}
			p1 = c;
		}
		else if (n1 == 1) {	// In N
			bAlter1 = GetClipPoint(1, n, p, tp1[0], tp1[1], tp1[2]);
			bAlter2 = GetClipPoint(0, p, c, tp2[0], tp2[1], tp2[2]);
			p1 = n;
			p2 = c;
		}
		else {				// In R
			bAlter1 = GetClipPoint(2, r, p, tp1[0], tp1[1], tp1[2]);
			bAlter2 = GetClipPoint(0, p, c, tp2[0], tp2[1], tp2[2]);
			p1 = r;
			p2 = c;
		}
		AddPointNode(p1);
		if ( bAlter1 ) {
			AddPointNode(tp1[0]);
			AddPointNode(tp1[1]);
			AddPointNode(tp1[2]);
		}
			AddPointNode(p);
		if ( bAlter2 ) {
			AddPointNode(tp2[0]);
			AddPointNode(tp2[1]);
			AddPointNode(tp2[2]);
		}
		AddPointNode(p2);
}

void CTrack::RemoveLines()
{
	POSITION pos;
	for (pos = m_Lines.GetHeadPosition();pos != NULL;) {
		CLine *line = (CLine*)m_Lines.GetNext(pos);
		delete line;
	}
	m_Lines.RemoveAll();
}

void CTrack::AddLines(CLine *line)
{
	m_Lines.AddTail((CObject*)line);
}

void CLine::Draw(CDC *pDC) {
	pDC->MoveTo(a);
	pDC->LineTo(b);
}

void CTrack::DrawLines(CDC *pDC)
{
	CRgn rgn;
	POSITION pos;
	BOOL first = TRUE;
	for (pos = m_Lines.GetHeadPosition(); pos != NULL;) {
		CLine *line = (CLine*)m_Lines.GetNext(pos);
		CRgn irgn;
		POINT *pPoint = line->GetRgnPoint();
		if (first) {
			rgn.CreatePolygonRgn( pPoint, 4, WINDING );
			first = FALSE;
		}
		else {
			irgn.CreatePolygonRgn( pPoint, 4, WINDING );
			rgn.CombineRgn( &rgn, &irgn, RGN_OR );
		}
	}
	CBrush Brush(RGB(128, 128, 196));
	pDC->FrameRgn( &rgn, &Brush, 1, 1 );  //omani
}

BOOL CTrack::isValid()
{
	return (m_nShape & B_NOTRACK) == 0;
}

POINT * CLine::GetRgnPoint()
{
	static POINT p[4];
	
	short dx = 0, dy = 0;

	if (a.x == b.x) {		// 수직선
		dx = 5;
	}
	else if (a.y == b.y) {		// 수평선
		dy = 5;
	}
	else if (a.x > b.x) {
		if (a.y > b.y) {
			dx = -5;
			dy = 5;
		}
		else {
			dx = 5;
			dy = 5;
		}
	}
	else {
		if (a.y > b.y) {
			dx = -5;
			dy = -5;
		}
		else {
			dx = 5;
			dy = -5;
		}
	}
	p[0].x = a.x - dx;
	p[0].y = a.y - dy;
	p[1].x = a.x + dx;
	p[1].y = a.y + dy;
	p[2].x = b.x + dx;
	p[2].y = b.y + dy;
	p[3].x = b.x - dx;
	p[3].y = b.y - dy;
	return p;
}

CRgn * CTrack::GetRgn()
{
	CRgn *pRgn = new CRgn;
	POSITION pos;
	BOOL first = TRUE;
	for (pos = m_Lines.GetHeadPosition(); pos != NULL;) {
		CLine *line = (CLine*)m_Lines.GetNext(pos);
		CRgn irgn;
		POINT *pPoint = line->GetRgnPoint();
		if (first) {
			pRgn->CreatePolygonRgn( pPoint, 4, WINDING );
			first = FALSE;
		}
		else {
			irgn.CreatePolygonRgn( pPoint, 4, WINDING );
			pRgn->CombineRgn( pRgn, &irgn, RGN_OR );
		}
	}
	return pRgn;
}

void CTrack::CreateDefaultSignalPos()
{
	CreateDefaultSignalPosDo(m_cSignalC[0], c);
	CreateDefaultSignalPosDo(m_cSignalC[1], c);
	CreateDefaultSignalPosDo(m_cSignalN[0], n);
	CreateDefaultSignalPosDo(m_cSignalN[1], n);
	CreateDefaultSignalPosDo(m_cSignalR[0], r);
	CreateDefaultSignalPosDo(m_cSignalR[1], r);
}

void CTrack::CreateDefaultSignalPosDo(CSignal &Sig, CBlip &ref)
{
	if (!Sig.isValid()) {
		int xofs;
		xofs = (ref.x - p.x) < 0 ? GridSize * 2 : -GridSize * 2;
		int nx = ref.x + xofs;
		int ny = ref.y;
		Sig.Offset( CPoint( nx - Sig.x, ny - Sig.y ) );
		if (xofs > 0) Sig.m_nLR = 0;
		else Sig.m_nLR = 1;
	}
}

char *szCNR[] = {
	"@",
	"@C",
	"@N",
	"@R"
};

void CTrack::NodeDump( ostream &os, int nNode )
{
	if (m_pConnect[nNode]) {
		os << m_pConnect[nNode]->Name;
		os << szCNR[m_nConnect[nNode]];
	}
	else os << "_@X";
}

// static  function
// LSD 좌표에서 SFM 좌표로 변환.
//
// LSD							SFM
//                             //offset
// (-hx,hy)		(hx,hy)			(0,0)			(2hx,0)
//
//
//			(0,0)						(hx,hy)
//
//
// (-hx,-hy)	(hx,-hy)		(0,2hy)			(2hx,2hy)
//
extern int   OFFSET_X;
extern int   OFFSET_Y;
extern int   REVERSE;

void ChangeMapping(CPoint &p1,CPoint &p2)
{	
	if (REVERSE != 1) {
		p1.x = -p1.x;
		p1.y = -p1.y;
		p2.x = -p2.x;
		p2.y = -p2.y;
	}
	p1.x = ( p1.x + OFFSET_X);
	p1.y = ( -p1.y + OFFSET_Y);
	p2.x = ( p2.x + OFFSET_X);
	p2.y = ( -p2.y + OFFSET_Y);	
}

