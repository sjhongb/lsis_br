//=======================================================
//==              Station.cpp
//=======================================================
//	파 일 명 :  Station.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"

#include "LSDDoc.h"
#include "Station.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern int STATIONUPDN;
/////////////////////////////////////////////////////////////////////////////
// CStationInfo dialog


CStationInfo::CStationInfo(CLSDDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CStationInfo::IDD, pParent)
{
	m_nType = pDoc->m_nType & 0xf;
	m_bViewAll = ((pDoc->m_nType & 0x80) != 0);
	//{{AFX_DATA_INIT(CStationInfo)
	m_FromStation = _T("");
	m_LineName = _T("");
	m_NextStation = _T("");
	m_PrevStation = _T("");
	m_nDir = -1;
	m_StationName = _T("");
	m_ToStation = _T("");
	m_Type = -1;
	m_bOverlapCK = FALSE;
	//}}AFX_DATA_INIT
	pDocument = pDoc;
}


void CStationInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStationInfo)
	DDX_Radio(pDX, IDC_RADIO_TYPE, m_nType);
	DDX_Check(pDX, IDC_VIEWALL, m_bViewAll);
	DDX_Check(pDX, IDC_CHK_OVERLAP, m_bOverlapCK);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_FROMST, pDocument->m_FromStation);
	DDV_MaxChars(pDX, m_FromStation, 20);
	DDX_Text(pDX, IDC_LINENAME, pDocument->m_LineName);
	DDV_MaxChars(pDX, m_LineName, 20);
	DDX_Text(pDX, IDC_NEXTST, pDocument->m_NextStation);
	DDV_MaxChars(pDX, m_NextStation, 20);
	DDX_Text(pDX, IDC_PREVST, pDocument->m_PrevStation);
	DDX_Radio(pDX, IDC_RADIO_DIR, pDocument->m_nUpDn);
	DDX_Text(pDX, IDC_STNAME, pDocument->m_StationName);
	DDV_MaxChars(pDX, m_StationName, 30);
	DDX_Text(pDX, IDC_TOST, pDocument->m_ToStation);
	DDV_MaxChars(pDX, m_ToStation, 20);
	DDX_Text(pDX, IDC_KILO, pDocument->m_Kilo);
	DDV_MaxChars(pDX, m_Kilo, 20);
	STATIONUPDN = pDocument->m_nUpDn;
}


BEGIN_MESSAGE_MAP(CStationInfo, CDialog)
	//{{AFX_MSG_MAP(CStationInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStationInfo message handlers

void CStationInfo::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	pDocument->m_nType = m_nType + (m_bViewAll ? 0x80 : 0);
	if ( m_bOverlapCK == TRUE ) {
		__editdev21 = "1"; 	
	} else {
		__editdev21 = "0"; 	
	}
	CDialog::OnOK();
}

BOOL CStationInfo::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if ( __editdev21 == "1" ) {
		m_bOverlapCK = TRUE;
	} else {
		m_bOverlapCK = FALSE;
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
