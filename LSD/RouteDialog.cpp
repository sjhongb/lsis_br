//=======================================================
//==              RouteDialog.cpp
//=======================================================
//	파 일 명 :  RouteDialog.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "RouteDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef BOOL (WINAPI *SetLayer)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);
#define LWA_COLORKEY            0x01
#define LWA_ALPHA               0x02

/////////////////////////////////////////////////////////////////////////////
// CRouteDialog dialog


CRouteDialog::CRouteDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CRouteDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRouteDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CRouteDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRouteDialog)
	DDX_Control(pDX, IDC_LIST_ROUTE, m_ListRoute);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRouteDialog, CDialog)
	//{{AFX_MSG_MAP(CRouteDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRouteDialog message handlers

void CRouteDialog::UpdateRoute( CString &pStrRoute )
{
	SetDlgItemText( IDC_EDIT_ROUTE, pStrRoute );
}

void CRouteDialog::OnOK() 
{
	// TODO: Add extra validation here
	CDialog::OnOK();
	ShowWindow( SW_SHOW );	
}

void CRouteDialog::OnCancel() 
{
	// TODO: Add extra validation here
	CDialog::OnCancel();
	ShowWindow( SW_SHOW );	
}

BOOL CRouteDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
static _TCHAR *_gszColumnLabel[]= {	_T("TYPE"),  _T("진로방향"), _T("출발점"), _T("도착점"), _T("쇄정"),  _T("신호제어 철사쇄정"), _T("진로(구분)쇄정"), _T("접근 보류쇄정") };
static int _gnColumnFmt[] =		  {	LVCFMT_CENTER, LVCFMT_CENTER,    LVCFMT_CENTER,  LVCFMT_CENTER,  LVCFMT_CENTER, LVCFMT_CENTER,             LVCFMT_CENTER,          LVCFMT_CENTER };
static int _gnColumnWidth[] =	  {	80,			   80,             70,           70,           250,         250,                     250,                  100 };

	int i;
	LV_COLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	for(i = 0; i<sizeof(_gnColumnWidth)/sizeof(int); i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabel[i];
		lvc.cx = _gnColumnWidth[i];
		lvc.fmt = _gnColumnFmt[i];
		m_ListRoute.InsertColumn(i,&lvc);
	}
	
	SetTrans(180);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CRouteDialog::SetTrans(int iVal)
{
	HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
	SetLayer pSetLayer = (SetLayer)GetProcAddress(hUser32, "SetLayeredWindowAttributes");
	if(pSetLayer == NULL)
	{
		MessageBox("win2000 이상");
		return TRUE;
	}
	char chAlpha = iVal; //투명도 설정 0 ~ 255
	SetWindowLong(this->m_hWnd, GWL_EXSTYLE, GetWindowLong(this->m_hWnd, GWL_EXSTYLE) | 0x80000);
	pSetLayer(this->m_hWnd, 0,chAlpha, LWA_ALPHA);
	return TRUE;
}