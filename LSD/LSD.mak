# Microsoft Developer Studio Generated NMAKE File, Format Version 4.20
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

!IF "$(CFG)" == ""
CFG=LSD - Win32 DebugET
!MESSAGE No configuration specified.  Defaulting to LSD - Win32 DebugET.
!ENDIF 

!IF "$(CFG)" != "LSD - Win32 Release" && "$(CFG)" != "LSD - Win32 Debug" &&\
 "$(CFG)" != "LSD - Win32 DebugET"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "LSD.mak" CFG="LSD - Win32 DebugET"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "LSD - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "LSD - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "LSD - Win32 DebugET" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "LSD - Win32 Debug"
F90=fl32.exe
CPP=cl.exe
RSC=rc.exe
MTL=mktyplib.exe

!IF  "$(CFG)" == "LSD - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\LSD.exe"

CLEAN : 
	-@erase "$(INTDIR)\blip.obj"
	-@erase "$(INTDIR)\Bmpdraw.obj"
	-@erase "$(INTDIR)\bmpinfo.obj"
	-@erase "$(INTDIR)\BmpTrackCon.obj"
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\ButtonDl.obj"
	-@erase "$(INTDIR)\Cell.obj"
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\LSD.obj"
	-@erase "$(INTDIR)\LSD.pch"
	-@erase "$(INTDIR)\LSD.res"
	-@erase "$(INTDIR)\LSDDoc.obj"
	-@erase "$(INTDIR)\LSDView.obj"
	-@erase "$(INTDIR)\Develop.obj"
	-@erase "$(INTDIR)\DotChange.obj"
	-@erase "$(INTDIR)\DriveDlg.obj"
	-@erase "$(INTDIR)\GraphObj.obj"
	-@erase "$(INTDIR)\IOInfo.obj"
	-@erase "$(INTDIR)\IOList.obj"
	-@erase "$(INTDIR)\iorack.obj"
	-@erase "$(INTDIR)\KeyCheck.obj"
	-@erase "$(INTDIR)\LockEdit.obj"
	-@erase "$(INTDIR)\Logic.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MyObject.obj"
	-@erase "$(INTDIR)\ObjDlg.obj"
	-@erase "$(INTDIR)\ObjEditDlg.obj"
	-@erase "$(INTDIR)\ObjSetTextDlg.obj"
	-@erase "$(INTDIR)\Parser.obj"
	-@erase "$(INTDIR)\pwdlg.obj"
	-@erase "$(INTDIR)\RouteDialog.obj"
	-@erase "$(INTDIR)\RouteLine.obj"
	-@erase "$(INTDIR)\Rparse.obj"
	-@erase "$(INTDIR)\scrobj.obj"
	-@erase "$(INTDIR)\SetMar.obj"
	-@erase "$(INTDIR)\Signal.obj"
	-@erase "$(INTDIR)\SignalDl.obj"
	-@erase "$(INTDIR)\Station.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StDirDlg.obj"
	-@erase "$(INTDIR)\stobject.obj"
	-@erase "$(INTDIR)\StOpDlg.obj"
	-@erase "$(INTDIR)\strlist.obj"
	-@erase "$(INTDIR)\TableDoc.obj"
	-@erase "$(INTDIR)\TableFrm.obj"
	-@erase "$(INTDIR)\TableRow.obj"
	-@erase "$(INTDIR)\TablView.obj"
	-@erase "$(INTDIR)\Track.obj"
	-@erase "$(INTDIR)\TrackDlg.obj"
	-@erase "$(INTDIR)\TrkEdit.obj"
	-@erase "$(OUTDIR)\LSD.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /I "Release/"
# ADD F90 /I "Release/"
F90_OBJS=.\Release/
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D\
 "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /Fp"$(INTDIR)/LSD.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\Release/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG"
RSC_PROJ=/l 0x412 /fo"$(INTDIR)/LSD.res" /d "NDEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/LSD.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)/LSD.pdb" /machine:I386 /out:"$(OUTDIR)/LSD.exe" 
LINK32_OBJS= \
	"$(INTDIR)\blip.obj" \
	"$(INTDIR)\Bmpdraw.obj" \
	"$(INTDIR)\bmpinfo.obj" \
	"$(INTDIR)\BmpTrackCon.obj" \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\ButtonDl.obj" \
	"$(INTDIR)\Cell.obj" \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\LSD.obj" \
	"$(INTDIR)\LSD.res" \
	"$(INTDIR)\LSDDoc.obj" \
	"$(INTDIR)\LSDView.obj" \
	"$(INTDIR)\Develop.obj" \
	"$(INTDIR)\DotChange.obj" \
	"$(INTDIR)\DriveDlg.obj" \
	"$(INTDIR)\GraphObj.obj" \
	"$(INTDIR)\IOInfo.obj" \
	"$(INTDIR)\IOList.obj" \
	"$(INTDIR)\iorack.obj" \
	"$(INTDIR)\KeyCheck.obj" \
	"$(INTDIR)\LockEdit.obj" \
	"$(INTDIR)\Logic.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MyObject.obj" \
	"$(INTDIR)\ObjDlg.obj" \
	"$(INTDIR)\ObjEditDlg.obj" \
	"$(INTDIR)\ObjSetTextDlg.obj" \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\pwdlg.obj" \
	"$(INTDIR)\RouteDialog.obj" \
	"$(INTDIR)\RouteLine.obj" \
	"$(INTDIR)\Rparse.obj" \
	"$(INTDIR)\scrobj.obj" \
	"$(INTDIR)\SetMar.obj" \
	"$(INTDIR)\Signal.obj" \
	"$(INTDIR)\SignalDl.obj" \
	"$(INTDIR)\Station.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\StDirDlg.obj" \
	"$(INTDIR)\stobject.obj" \
	"$(INTDIR)\StOpDlg.obj" \
	"$(INTDIR)\strlist.obj" \
	"$(INTDIR)\TableDoc.obj" \
	"$(INTDIR)\TableFrm.obj" \
	"$(INTDIR)\TableRow.obj" \
	"$(INTDIR)\TablView.obj" \
	"$(INTDIR)\Track.obj" \
	"$(INTDIR)\TrackDlg.obj" \
	"$(INTDIR)\TrkEdit.obj"

"$(OUTDIR)\LSD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\LSD.exe" "$(OUTDIR)\LSD.bsc"

CLEAN : 
	-@erase "$(INTDIR)\blip.obj"
	-@erase "$(INTDIR)\blip.sbr"
	-@erase "$(INTDIR)\Bmpdraw.obj"
	-@erase "$(INTDIR)\Bmpdraw.sbr"
	-@erase "$(INTDIR)\bmpinfo.obj"
	-@erase "$(INTDIR)\bmpinfo.sbr"
	-@erase "$(INTDIR)\BmpTrackCon.obj"
	-@erase "$(INTDIR)\BmpTrackCon.sbr"
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Button.sbr"
	-@erase "$(INTDIR)\ButtonDl.obj"
	-@erase "$(INTDIR)\ButtonDl.sbr"
	-@erase "$(INTDIR)\Cell.obj"
	-@erase "$(INTDIR)\Cell.sbr"
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ChildFrm.sbr"
	-@erase "$(INTDIR)\LSD.obj"
	-@erase "$(INTDIR)\LSD.pch"
	-@erase "$(INTDIR)\LSD.res"
	-@erase "$(INTDIR)\LSD.sbr"
	-@erase "$(INTDIR)\LSDDoc.obj"
	-@erase "$(INTDIR)\LSDDoc.sbr"
	-@erase "$(INTDIR)\LSDView.obj"
	-@erase "$(INTDIR)\LSDView.sbr"
	-@erase "$(INTDIR)\Develop.obj"
	-@erase "$(INTDIR)\Develop.sbr"
	-@erase "$(INTDIR)\DotChange.obj"
	-@erase "$(INTDIR)\DotChange.sbr"
	-@erase "$(INTDIR)\DriveDlg.obj"
	-@erase "$(INTDIR)\DriveDlg.sbr"
	-@erase "$(INTDIR)\GraphObj.obj"
	-@erase "$(INTDIR)\GraphObj.sbr"
	-@erase "$(INTDIR)\IOInfo.obj"
	-@erase "$(INTDIR)\IOInfo.sbr"
	-@erase "$(INTDIR)\IOList.obj"
	-@erase "$(INTDIR)\IOList.sbr"
	-@erase "$(INTDIR)\iorack.obj"
	-@erase "$(INTDIR)\iorack.sbr"
	-@erase "$(INTDIR)\KeyCheck.obj"
	-@erase "$(INTDIR)\KeyCheck.sbr"
	-@erase "$(INTDIR)\LockEdit.obj"
	-@erase "$(INTDIR)\LockEdit.sbr"
	-@erase "$(INTDIR)\Logic.obj"
	-@erase "$(INTDIR)\Logic.sbr"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainFrm.sbr"
	-@erase "$(INTDIR)\MyObject.obj"
	-@erase "$(INTDIR)\MyObject.sbr"
	-@erase "$(INTDIR)\ObjDlg.obj"
	-@erase "$(INTDIR)\ObjDlg.sbr"
	-@erase "$(INTDIR)\ObjEditDlg.obj"
	-@erase "$(INTDIR)\ObjEditDlg.sbr"
	-@erase "$(INTDIR)\ObjSetTextDlg.obj"
	-@erase "$(INTDIR)\ObjSetTextDlg.sbr"
	-@erase "$(INTDIR)\Parser.obj"
	-@erase "$(INTDIR)\Parser.sbr"
	-@erase "$(INTDIR)\pwdlg.obj"
	-@erase "$(INTDIR)\pwdlg.sbr"
	-@erase "$(INTDIR)\RouteDialog.obj"
	-@erase "$(INTDIR)\RouteDialog.sbr"
	-@erase "$(INTDIR)\RouteLine.obj"
	-@erase "$(INTDIR)\RouteLine.sbr"
	-@erase "$(INTDIR)\Rparse.obj"
	-@erase "$(INTDIR)\Rparse.sbr"
	-@erase "$(INTDIR)\scrobj.obj"
	-@erase "$(INTDIR)\scrobj.sbr"
	-@erase "$(INTDIR)\SetMar.obj"
	-@erase "$(INTDIR)\SetMar.sbr"
	-@erase "$(INTDIR)\Signal.obj"
	-@erase "$(INTDIR)\Signal.sbr"
	-@erase "$(INTDIR)\SignalDl.obj"
	-@erase "$(INTDIR)\SignalDl.sbr"
	-@erase "$(INTDIR)\Station.obj"
	-@erase "$(INTDIR)\Station.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\StDirDlg.obj"
	-@erase "$(INTDIR)\StDirDlg.sbr"
	-@erase "$(INTDIR)\stobject.obj"
	-@erase "$(INTDIR)\stobject.sbr"
	-@erase "$(INTDIR)\StOpDlg.obj"
	-@erase "$(INTDIR)\StOpDlg.sbr"
	-@erase "$(INTDIR)\strlist.obj"
	-@erase "$(INTDIR)\strlist.sbr"
	-@erase "$(INTDIR)\TableDoc.obj"
	-@erase "$(INTDIR)\TableDoc.sbr"
	-@erase "$(INTDIR)\TableFrm.obj"
	-@erase "$(INTDIR)\TableFrm.sbr"
	-@erase "$(INTDIR)\TableRow.obj"
	-@erase "$(INTDIR)\TableRow.sbr"
	-@erase "$(INTDIR)\TablView.obj"
	-@erase "$(INTDIR)\TablView.sbr"
	-@erase "$(INTDIR)\Track.obj"
	-@erase "$(INTDIR)\Track.sbr"
	-@erase "$(INTDIR)\TrackDlg.obj"
	-@erase "$(INTDIR)\TrackDlg.sbr"
	-@erase "$(INTDIR)\TrkEdit.obj"
	-@erase "$(INTDIR)\TrkEdit.sbr"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(OUTDIR)\LSD.bsc"
	-@erase "$(OUTDIR)\LSD.exe"
	-@erase "$(OUTDIR)\LSD.ilk"
	-@erase "$(OUTDIR)\LSD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /I "Debug/"
# ADD F90 /I "Debug/"
F90_OBJS=.\Debug/
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /D "_HSSWITCH_" /FR /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS"\
 /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /D "_HSSWITCH_"\
 /FR"$(INTDIR)/" /Fp"$(INTDIR)/LSD.pch" /Yu"stdafx.h" /Fo"$(INTDIR)/"\
 /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG"
RSC_PROJ=/l 0x412 /fo"$(INTDIR)/LSD.res" /d "_DEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/LSD.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\blip.sbr" \
	"$(INTDIR)\Bmpdraw.sbr" \
	"$(INTDIR)\bmpinfo.sbr" \
	"$(INTDIR)\BmpTrackCon.sbr" \
	"$(INTDIR)\Button.sbr" \
	"$(INTDIR)\ButtonDl.sbr" \
	"$(INTDIR)\Cell.sbr" \
	"$(INTDIR)\ChildFrm.sbr" \
	"$(INTDIR)\LSD.sbr" \
	"$(INTDIR)\LSDDoc.sbr" \
	"$(INTDIR)\LSDView.sbr" \
	"$(INTDIR)\Develop.sbr" \
	"$(INTDIR)\DotChange.sbr" \
	"$(INTDIR)\DriveDlg.sbr" \
	"$(INTDIR)\GraphObj.sbr" \
	"$(INTDIR)\IOInfo.sbr" \
	"$(INTDIR)\IOList.sbr" \
	"$(INTDIR)\iorack.sbr" \
	"$(INTDIR)\KeyCheck.sbr" \
	"$(INTDIR)\LockEdit.sbr" \
	"$(INTDIR)\Logic.sbr" \
	"$(INTDIR)\MainFrm.sbr" \
	"$(INTDIR)\MyObject.sbr" \
	"$(INTDIR)\ObjDlg.sbr" \
	"$(INTDIR)\ObjEditDlg.sbr" \
	"$(INTDIR)\ObjSetTextDlg.sbr" \
	"$(INTDIR)\Parser.sbr" \
	"$(INTDIR)\pwdlg.sbr" \
	"$(INTDIR)\RouteDialog.sbr" \
	"$(INTDIR)\RouteLine.sbr" \
	"$(INTDIR)\Rparse.sbr" \
	"$(INTDIR)\scrobj.sbr" \
	"$(INTDIR)\SetMar.sbr" \
	"$(INTDIR)\Signal.sbr" \
	"$(INTDIR)\SignalDl.sbr" \
	"$(INTDIR)\Station.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\StDirDlg.sbr" \
	"$(INTDIR)\stobject.sbr" \
	"$(INTDIR)\StOpDlg.sbr" \
	"$(INTDIR)\strlist.sbr" \
	"$(INTDIR)\TableDoc.sbr" \
	"$(INTDIR)\TableFrm.sbr" \
	"$(INTDIR)\TableRow.sbr" \
	"$(INTDIR)\TablView.sbr" \
	"$(INTDIR)\Track.sbr" \
	"$(INTDIR)\TrackDlg.sbr" \
	"$(INTDIR)\TrkEdit.sbr"

"$(OUTDIR)\LSD.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)/LSD.pdb" /debug /machine:I386 /out:"$(OUTDIR)/LSD.exe" 
LINK32_OBJS= \
	"$(INTDIR)\blip.obj" \
	"$(INTDIR)\Bmpdraw.obj" \
	"$(INTDIR)\bmpinfo.obj" \
	"$(INTDIR)\BmpTrackCon.obj" \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\ButtonDl.obj" \
	"$(INTDIR)\Cell.obj" \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\LSD.obj" \
	"$(INTDIR)\LSD.res" \
	"$(INTDIR)\LSDDoc.obj" \
	"$(INTDIR)\LSDView.obj" \
	"$(INTDIR)\Develop.obj" \
	"$(INTDIR)\DotChange.obj" \
	"$(INTDIR)\DriveDlg.obj" \
	"$(INTDIR)\GraphObj.obj" \
	"$(INTDIR)\IOInfo.obj" \
	"$(INTDIR)\IOList.obj" \
	"$(INTDIR)\iorack.obj" \
	"$(INTDIR)\KeyCheck.obj" \
	"$(INTDIR)\LockEdit.obj" \
	"$(INTDIR)\Logic.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MyObject.obj" \
	"$(INTDIR)\ObjDlg.obj" \
	"$(INTDIR)\ObjEditDlg.obj" \
	"$(INTDIR)\ObjSetTextDlg.obj" \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\pwdlg.obj" \
	"$(INTDIR)\RouteDialog.obj" \
	"$(INTDIR)\RouteLine.obj" \
	"$(INTDIR)\Rparse.obj" \
	"$(INTDIR)\scrobj.obj" \
	"$(INTDIR)\SetMar.obj" \
	"$(INTDIR)\Signal.obj" \
	"$(INTDIR)\SignalDl.obj" \
	"$(INTDIR)\Station.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\StDirDlg.obj" \
	"$(INTDIR)\stobject.obj" \
	"$(INTDIR)\StOpDlg.obj" \
	"$(INTDIR)\strlist.obj" \
	"$(INTDIR)\TableDoc.obj" \
	"$(INTDIR)\TableFrm.obj" \
	"$(INTDIR)\TableRow.obj" \
	"$(INTDIR)\TablView.obj" \
	"$(INTDIR)\Track.obj" \
	"$(INTDIR)\TrackDlg.obj" \
	"$(INTDIR)\TrkEdit.obj"

"$(OUTDIR)\LSD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "LSD___Wi"
# PROP BASE Intermediate_Dir "LSD___Wi"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DebugET"
# PROP Intermediate_Dir "DebugET"
# PROP Target_Dir ""
OUTDIR=.\DebugET
INTDIR=.\DebugET

ALL : "$(OUTDIR)\LSD.exe" "$(OUTDIR)\LSD.bsc"

CLEAN : 
	-@erase "$(INTDIR)\blip.obj"
	-@erase "$(INTDIR)\blip.sbr"
	-@erase "$(INTDIR)\Bmpdraw.obj"
	-@erase "$(INTDIR)\Bmpdraw.sbr"
	-@erase "$(INTDIR)\bmpinfo.obj"
	-@erase "$(INTDIR)\bmpinfo.sbr"
	-@erase "$(INTDIR)\BmpTrackCon.obj"
	-@erase "$(INTDIR)\BmpTrackCon.sbr"
	-@erase "$(INTDIR)\Button.obj"
	-@erase "$(INTDIR)\Button.sbr"
	-@erase "$(INTDIR)\ButtonDl.obj"
	-@erase "$(INTDIR)\ButtonDl.sbr"
	-@erase "$(INTDIR)\Cell.obj"
	-@erase "$(INTDIR)\Cell.sbr"
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ChildFrm.sbr"
	-@erase "$(INTDIR)\LSD.obj"
	-@erase "$(INTDIR)\LSD.pch"
	-@erase "$(INTDIR)\LSD.sbr"
	-@erase "$(INTDIR)\LSDDoc.obj"
	-@erase "$(INTDIR)\LSDDoc.sbr"
	-@erase "$(INTDIR)\LSDet.res"
	-@erase "$(INTDIR)\LSDView.obj"
	-@erase "$(INTDIR)\LSDView.sbr"
	-@erase "$(INTDIR)\Develop.obj"
	-@erase "$(INTDIR)\Develop.sbr"
	-@erase "$(INTDIR)\DotChange.obj"
	-@erase "$(INTDIR)\DotChange.sbr"
	-@erase "$(INTDIR)\DriveDlg.obj"
	-@erase "$(INTDIR)\DriveDlg.sbr"
	-@erase "$(INTDIR)\GraphObj.obj"
	-@erase "$(INTDIR)\GraphObj.sbr"
	-@erase "$(INTDIR)\IOInfo.obj"
	-@erase "$(INTDIR)\IOInfo.sbr"
	-@erase "$(INTDIR)\IOList.obj"
	-@erase "$(INTDIR)\IOList.sbr"
	-@erase "$(INTDIR)\iorack.obj"
	-@erase "$(INTDIR)\iorack.sbr"
	-@erase "$(INTDIR)\KeyCheck.obj"
	-@erase "$(INTDIR)\KeyCheck.sbr"
	-@erase "$(INTDIR)\LockEdit.obj"
	-@erase "$(INTDIR)\LockEdit.sbr"
	-@erase "$(INTDIR)\Logic.obj"
	-@erase "$(INTDIR)\Logic.sbr"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainFrm.sbr"
	-@erase "$(INTDIR)\MyObject.obj"
	-@erase "$(INTDIR)\MyObject.sbr"
	-@erase "$(INTDIR)\ObjDlg.obj"
	-@erase "$(INTDIR)\ObjDlg.sbr"
	-@erase "$(INTDIR)\ObjEditDlg.obj"
	-@erase "$(INTDIR)\ObjEditDlg.sbr"
	-@erase "$(INTDIR)\ObjSetTextDlg.obj"
	-@erase "$(INTDIR)\ObjSetTextDlg.sbr"
	-@erase "$(INTDIR)\Parser.obj"
	-@erase "$(INTDIR)\Parser.sbr"
	-@erase "$(INTDIR)\pwdlg.obj"
	-@erase "$(INTDIR)\pwdlg.sbr"
	-@erase "$(INTDIR)\RouteDialog.obj"
	-@erase "$(INTDIR)\RouteDialog.sbr"
	-@erase "$(INTDIR)\RouteLine.obj"
	-@erase "$(INTDIR)\RouteLine.sbr"
	-@erase "$(INTDIR)\Rparse.obj"
	-@erase "$(INTDIR)\Rparse.sbr"
	-@erase "$(INTDIR)\scrobj.obj"
	-@erase "$(INTDIR)\scrobj.sbr"
	-@erase "$(INTDIR)\SetMar.obj"
	-@erase "$(INTDIR)\SetMar.sbr"
	-@erase "$(INTDIR)\Signal.obj"
	-@erase "$(INTDIR)\Signal.sbr"
	-@erase "$(INTDIR)\SignalDl.obj"
	-@erase "$(INTDIR)\SignalDl.sbr"
	-@erase "$(INTDIR)\Station.obj"
	-@erase "$(INTDIR)\Station.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\StDirDlg.obj"
	-@erase "$(INTDIR)\StDirDlg.sbr"
	-@erase "$(INTDIR)\stobject.obj"
	-@erase "$(INTDIR)\stobject.sbr"
	-@erase "$(INTDIR)\StOpDlg.obj"
	-@erase "$(INTDIR)\StOpDlg.sbr"
	-@erase "$(INTDIR)\strlist.obj"
	-@erase "$(INTDIR)\strlist.sbr"
	-@erase "$(INTDIR)\TableDoc.obj"
	-@erase "$(INTDIR)\TableDoc.sbr"
	-@erase "$(INTDIR)\TableFrm.obj"
	-@erase "$(INTDIR)\TableFrm.sbr"
	-@erase "$(INTDIR)\TableRow.obj"
	-@erase "$(INTDIR)\TableRow.sbr"
	-@erase "$(INTDIR)\TablView.obj"
	-@erase "$(INTDIR)\TablView.sbr"
	-@erase "$(INTDIR)\Track.obj"
	-@erase "$(INTDIR)\Track.sbr"
	-@erase "$(INTDIR)\TrackDlg.obj"
	-@erase "$(INTDIR)\TrackDlg.sbr"
	-@erase "$(INTDIR)\TrkEdit.obj"
	-@erase "$(INTDIR)\TrkEdit.sbr"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(OUTDIR)\LSD.bsc"
	-@erase "$(OUTDIR)\LSD.exe"
	-@erase "$(OUTDIR)\LSD.ilk"
	-@erase "$(OUTDIR)\LSD.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /I "LSD___Wi/"
# ADD F90 /I "DebugET/"
F90_OBJS=.\DebugET/
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /FR /Yu"stdafx.h" /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_THAI" /D "_MBCS" /FR /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS"\
 /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_THAI" /D "_MBCS" /FR"$(INTDIR)/"\
 /Fp"$(INTDIR)/LSD.pch" /Yu"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\DebugET/
CPP_SBRS=.\DebugET/
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0x412 /d "_DEBUG"
# ADD RSC /l 0x409 /x /d "_DEBUG"
RSC_PROJ=/l 0x409 /x /fo"$(INTDIR)/LSDet.res" /d "_DEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/LSD.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\blip.sbr" \
	"$(INTDIR)\Bmpdraw.sbr" \
	"$(INTDIR)\bmpinfo.sbr" \
	"$(INTDIR)\BmpTrackCon.sbr" \
	"$(INTDIR)\Button.sbr" \
	"$(INTDIR)\ButtonDl.sbr" \
	"$(INTDIR)\Cell.sbr" \
	"$(INTDIR)\ChildFrm.sbr" \
	"$(INTDIR)\LSD.sbr" \
	"$(INTDIR)\LSDDoc.sbr" \
	"$(INTDIR)\LSDView.sbr" \
	"$(INTDIR)\Develop.sbr" \
	"$(INTDIR)\DotChange.sbr" \
	"$(INTDIR)\DriveDlg.sbr" \
	"$(INTDIR)\GraphObj.sbr" \
	"$(INTDIR)\IOInfo.sbr" \
	"$(INTDIR)\IOList.sbr" \
	"$(INTDIR)\iorack.sbr" \
	"$(INTDIR)\KeyCheck.sbr" \
	"$(INTDIR)\LockEdit.sbr" \
	"$(INTDIR)\Logic.sbr" \
	"$(INTDIR)\MainFrm.sbr" \
	"$(INTDIR)\MyObject.sbr" \
	"$(INTDIR)\ObjDlg.sbr" \
	"$(INTDIR)\ObjEditDlg.sbr" \
	"$(INTDIR)\ObjSetTextDlg.sbr" \
	"$(INTDIR)\Parser.sbr" \
	"$(INTDIR)\pwdlg.sbr" \
	"$(INTDIR)\RouteDialog.sbr" \
	"$(INTDIR)\RouteLine.sbr" \
	"$(INTDIR)\Rparse.sbr" \
	"$(INTDIR)\scrobj.sbr" \
	"$(INTDIR)\SetMar.sbr" \
	"$(INTDIR)\Signal.sbr" \
	"$(INTDIR)\SignalDl.sbr" \
	"$(INTDIR)\Station.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\StDirDlg.sbr" \
	"$(INTDIR)\stobject.sbr" \
	"$(INTDIR)\StOpDlg.sbr" \
	"$(INTDIR)\strlist.sbr" \
	"$(INTDIR)\TableDoc.sbr" \
	"$(INTDIR)\TableFrm.sbr" \
	"$(INTDIR)\TableRow.sbr" \
	"$(INTDIR)\TablView.sbr" \
	"$(INTDIR)\Track.sbr" \
	"$(INTDIR)\TrackDlg.sbr" \
	"$(INTDIR)\TrkEdit.sbr"

"$(OUTDIR)\LSD.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)/LSD.pdb" /debug /machine:I386 /out:"$(OUTDIR)/LSD.exe" 
LINK32_OBJS= \
	"$(INTDIR)\blip.obj" \
	"$(INTDIR)\Bmpdraw.obj" \
	"$(INTDIR)\bmpinfo.obj" \
	"$(INTDIR)\BmpTrackCon.obj" \
	"$(INTDIR)\Button.obj" \
	"$(INTDIR)\ButtonDl.obj" \
	"$(INTDIR)\Cell.obj" \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\LSD.obj" \
	"$(INTDIR)\LSDDoc.obj" \
	"$(INTDIR)\LSDet.res" \
	"$(INTDIR)\LSDView.obj" \
	"$(INTDIR)\Develop.obj" \
	"$(INTDIR)\DotChange.obj" \
	"$(INTDIR)\DriveDlg.obj" \
	"$(INTDIR)\GraphObj.obj" \
	"$(INTDIR)\IOInfo.obj" \
	"$(INTDIR)\IOList.obj" \
	"$(INTDIR)\iorack.obj" \
	"$(INTDIR)\KeyCheck.obj" \
	"$(INTDIR)\LockEdit.obj" \
	"$(INTDIR)\Logic.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MyObject.obj" \
	"$(INTDIR)\ObjDlg.obj" \
	"$(INTDIR)\ObjEditDlg.obj" \
	"$(INTDIR)\ObjSetTextDlg.obj" \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\pwdlg.obj" \
	"$(INTDIR)\RouteDialog.obj" \
	"$(INTDIR)\RouteLine.obj" \
	"$(INTDIR)\Rparse.obj" \
	"$(INTDIR)\scrobj.obj" \
	"$(INTDIR)\SetMar.obj" \
	"$(INTDIR)\Signal.obj" \
	"$(INTDIR)\SignalDl.obj" \
	"$(INTDIR)\Station.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\StDirDlg.obj" \
	"$(INTDIR)\stobject.obj" \
	"$(INTDIR)\StOpDlg.obj" \
	"$(INTDIR)\strlist.obj" \
	"$(INTDIR)\TableDoc.obj" \
	"$(INTDIR)\TableFrm.obj" \
	"$(INTDIR)\TableRow.obj" \
	"$(INTDIR)\TablView.obj" \
	"$(INTDIR)\Track.obj" \
	"$(INTDIR)\TrackDlg.obj" \
	"$(INTDIR)\TrkEdit.obj"

"$(OUTDIR)\LSD.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

F90_PROJ=/I "Release/" /Fo"Release/" 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "LSD - Win32 Release"
# Name "LSD - Win32 Debug"
# Name "LSD - Win32 DebugET"

!IF  "$(CFG)" == "LSD - Win32 Release"

!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\ReadMe.txt

!IF  "$(CFG)" == "LSD - Win32 Release"

!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LSD.cpp
DEP_CPP_LSD_C=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\Cell.h"\
	".\ChildFrm.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\GraphObj.h"\
	".\MainFrm.h"\
	".\Pwdlg.h"\
	".\RouteDialog.h"\
	".\SetMar.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableFrm.h"\
	".\TableRow.h"\
	".\TablView.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\LSD.obj" : $(SOURCE) $(DEP_CPP_LSD_C) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\LSD.obj" : $(SOURCE) $(DEP_CPP_LSD_C) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSD.sbr" : $(SOURCE) $(DEP_CPP_LSD_C) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\LSD.obj" : $(SOURCE) $(DEP_CPP_LSD_C) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSD.sbr" : $(SOURCE) $(DEP_CPP_LSD_C) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\StdAfx.cpp
DEP_CPP_STDAF=\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D\
 "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /Fp"$(INTDIR)/LSD.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\LSD.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS"\
 /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_MBCS" /D "_HSSWITCH_"\
 /FR"$(INTDIR)/" /Fp"$(INTDIR)/LSD.pch" /Yc"stdafx.h" /Fo"$(INTDIR)/"\
 /Fd"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\StdAfx.sbr" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\LSD.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MTd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS"\
 /D "_DEBUG_TRACK_" /D "_DEBUG_SIGNAL_" /D "_THAI" /D "_MBCS" /FR"$(INTDIR)/"\
 /Fp"$(INTDIR)/LSD.pch" /Yc"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c\
 $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\StdAfx.sbr" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\LSD.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MainFrm.cpp
DEP_CPP_MAINF=\
	".\LSD.h"\
	".\MainFrm.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\MainFrm.sbr" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\MainFrm.sbr" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ChildFrm.cpp
DEP_CPP_CHILD=\
	".\ChildFrm.h"\
	".\LSD.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\ChildFrm.obj" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\ChildFrm.obj" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ChildFrm.sbr" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\ChildFrm.obj" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ChildFrm.sbr" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LSDDoc.cpp
DEP_CPP_LSDDO=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\Button.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\Develop.h"\
	".\GraphObj.h"\
	".\MyObject.h"\
	".\RouteDialog.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\Track.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\LSDDoc.obj" : $(SOURCE) $(DEP_CPP_LSDDO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\LSDDoc.obj" : $(SOURCE) $(DEP_CPP_LSDDO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSDDoc.sbr" : $(SOURCE) $(DEP_CPP_LSDDO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\LSDDoc.obj" : $(SOURCE) $(DEP_CPP_LSDDO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSDDoc.sbr" : $(SOURCE) $(DEP_CPP_LSDDO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LSDView.cpp
DEP_CPP_LSDVI=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\Button.h"\
	".\Cell.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\Develop.h"\
	".\dotchange.h"\
	".\GraphObj.h"\
	".\MyObject.h"\
	".\ObjDlg.h"\
	".\RouteDialog.h"\
	".\Signal.h"\
	".\Station.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableRow.h"\
	".\TablView.h"\
	".\Track.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Parser.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\LSDView.obj" : $(SOURCE) $(DEP_CPP_LSDVI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\LSDView.obj" : $(SOURCE) $(DEP_CPP_LSDVI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSDView.sbr" : $(SOURCE) $(DEP_CPP_LSDVI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\LSDView.obj" : $(SOURCE) $(DEP_CPP_LSDVI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LSDView.sbr" : $(SOURCE) $(DEP_CPP_LSDVI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TrackDlg.cpp
DEP_CPP_TRACK=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\Button.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\Track.h"\
	".\TrackDlg.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\TrackDlg.obj" : $(SOURCE) $(DEP_CPP_TRACK) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\TrackDlg.obj" : $(SOURCE) $(DEP_CPP_TRACK) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TrackDlg.sbr" : $(SOURCE) $(DEP_CPP_TRACK) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\TrackDlg.obj" : $(SOURCE) $(DEP_CPP_TRACK) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TrackDlg.sbr" : $(SOURCE) $(DEP_CPP_TRACK) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\SignalDl.cpp
DEP_CPP_SIGNA=\
	".\Blip.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\Signal.h"\
	".\SignalDl.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\SignalDl.obj" : $(SOURCE) $(DEP_CPP_SIGNA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\SignalDl.obj" : $(SOURCE) $(DEP_CPP_SIGNA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\SignalDl.sbr" : $(SOURCE) $(DEP_CPP_SIGNA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\SignalDl.obj" : $(SOURCE) $(DEP_CPP_SIGNA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\SignalDl.sbr" : $(SOURCE) $(DEP_CPP_SIGNA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rparse.cpp
DEP_CPP_RPARS=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\Button.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\GraphObj.h"\
	".\MyObject.h"\
	".\RouteDialog.h"\
	".\RouteLine.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\Track.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Rparse.obj" : $(SOURCE) $(DEP_CPP_RPARS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Rparse.obj" : $(SOURCE) $(DEP_CPP_RPARS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Rparse.sbr" : $(SOURCE) $(DEP_CPP_RPARS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Rparse.obj" : $(SOURCE) $(DEP_CPP_RPARS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Rparse.sbr" : $(SOURCE) $(DEP_CPP_RPARS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TablView.cpp

!IF  "$(CFG)" == "LSD - Win32 Release"

DEP_CPP_TABLV=\
	".\Cell.h"\
	".\LSD.h"\
	".\IOInfo.h"\
	".\IOList.h"\
	".\Iorack.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableFrm.h"\
	".\TableRow.h"\
	".\TablView.h"\
	

"$(INTDIR)\TablView.obj" : $(SOURCE) $(DEP_CPP_TABLV) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

DEP_CPP_TABLV=\
	".\Cell.h"\
	".\LSD.h"\
	".\IOInfo.h"\
	".\IOList.h"\
	".\Iorack.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableFrm.h"\
	".\TableRow.h"\
	".\TablView.h"\
	

"$(INTDIR)\TablView.obj" : $(SOURCE) $(DEP_CPP_TABLV) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TablView.sbr" : $(SOURCE) $(DEP_CPP_TABLV) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

DEP_CPP_TABLV=\
	".\Cell.h"\
	".\LSD.h"\
	".\IOInfo.h"\
	".\IOList.h"\
	".\Iorack.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableFrm.h"\
	".\TableRow.h"\
	".\TablView.h"\
	

"$(INTDIR)\TablView.obj" : $(SOURCE) $(DEP_CPP_TABLV) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TablView.sbr" : $(SOURCE) $(DEP_CPP_TABLV) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TableDoc.cpp
DEP_CPP_TABLE=\
	".\Blip.h"\
	".\Cell.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\GraphObj.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableRow.h"\
	".\TablView.h"\
	{$(INCLUDE)}"\Parser.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\TableDoc.obj" : $(SOURCE) $(DEP_CPP_TABLE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\TableDoc.obj" : $(SOURCE) $(DEP_CPP_TABLE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableDoc.sbr" : $(SOURCE) $(DEP_CPP_TABLE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\TableDoc.obj" : $(SOURCE) $(DEP_CPP_TABLE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableDoc.sbr" : $(SOURCE) $(DEP_CPP_TABLE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TableRow.cpp
DEP_CPP_TABLER=\
	".\Cell.h"\
	".\LSD.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\TableDoc.h"\
	".\TableRow.h"\
	{$(INCLUDE)}"\Parser.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\TableRow.obj" : $(SOURCE) $(DEP_CPP_TABLER) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\TableRow.obj" : $(SOURCE) $(DEP_CPP_TABLER) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableRow.sbr" : $(SOURCE) $(DEP_CPP_TABLER) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\TableRow.obj" : $(SOURCE) $(DEP_CPP_TABLER) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableRow.sbr" : $(SOURCE) $(DEP_CPP_TABLER) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Station.cpp
DEP_CPP_STATI=\
	".\Blip.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\GraphObj.h"\
	".\Station.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Station.obj" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Station.obj" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Station.sbr" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Station.obj" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Station.sbr" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Cell.cpp
DEP_CPP_CELL_=\
	".\Cell.h"\
	".\LSD.h"\
	".\LockEdit.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	".\TrkEdit.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Cell.obj" : $(SOURCE) $(DEP_CPP_CELL_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Cell.obj" : $(SOURCE) $(DEP_CPP_CELL_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Cell.sbr" : $(SOURCE) $(DEP_CPP_CELL_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Cell.obj" : $(SOURCE) $(DEP_CPP_CELL_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Cell.sbr" : $(SOURCE) $(DEP_CPP_CELL_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MyObject.cpp
DEP_CPP_MYOBJ=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\GraphObj.h"\
	".\MyObject.h"\
	".\ObjEditDlg.h"\
	".\ObjSetTextDlg.h"\
	".\StdAfx.h"\
	".\StDirDlg.h"\
	".\StOpDlg.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\MyObject.obj" : $(SOURCE) $(DEP_CPP_MYOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\MyObject.obj" : $(SOURCE) $(DEP_CPP_MYOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\MyObject.sbr" : $(SOURCE) $(DEP_CPP_MYOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\MyObject.obj" : $(SOURCE) $(DEP_CPP_MYOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\MyObject.sbr" : $(SOURCE) $(DEP_CPP_MYOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LockEdit.cpp
DEP_CPP_LOCKE=\
	".\LSD.h"\
	".\LockEdit.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\LockEdit.obj" : $(SOURCE) $(DEP_CPP_LOCKE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\LockEdit.obj" : $(SOURCE) $(DEP_CPP_LOCKE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LockEdit.sbr" : $(SOURCE) $(DEP_CPP_LOCKE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\LockEdit.obj" : $(SOURCE) $(DEP_CPP_LOCKE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\LockEdit.sbr" : $(SOURCE) $(DEP_CPP_LOCKE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TrkEdit.cpp
DEP_CPP_TRKED=\
	".\LSD.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	".\TrkEdit.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\TrkEdit.obj" : $(SOURCE) $(DEP_CPP_TRKED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\TrkEdit.obj" : $(SOURCE) $(DEP_CPP_TRKED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TrkEdit.sbr" : $(SOURCE) $(DEP_CPP_TRKED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\TrkEdit.obj" : $(SOURCE) $(DEP_CPP_TRKED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TrkEdit.sbr" : $(SOURCE) $(DEP_CPP_TRKED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ObjDlg.cpp
DEP_CPP_OBJDL=\
	".\LSD.h"\
	".\ObjDlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\ObjDlg.obj" : $(SOURCE) $(DEP_CPP_OBJDL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\ObjDlg.obj" : $(SOURCE) $(DEP_CPP_OBJDL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJDL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\ObjDlg.obj" : $(SOURCE) $(DEP_CPP_OBJDL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJDL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Signal.cpp
DEP_CPP_SIGNAL=\
	".\Blip.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\Signal.h"\
	".\SignalDl.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Signal.obj" : $(SOURCE) $(DEP_CPP_SIGNAL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Signal.obj" : $(SOURCE) $(DEP_CPP_SIGNAL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Signal.sbr" : $(SOURCE) $(DEP_CPP_SIGNAL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Signal.obj" : $(SOURCE) $(DEP_CPP_SIGNAL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Signal.sbr" : $(SOURCE) $(DEP_CPP_SIGNAL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Button.cpp
DEP_CPP_BUTTO=\
	".\Blip.h"\
	".\Button.h"\
	".\ButtonDl.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Button.obj" : $(SOURCE) $(DEP_CPP_BUTTO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Button.obj" : $(SOURCE) $(DEP_CPP_BUTTO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Button.sbr" : $(SOURCE) $(DEP_CPP_BUTTO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Button.obj" : $(SOURCE) $(DEP_CPP_BUTTO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Button.sbr" : $(SOURCE) $(DEP_CPP_BUTTO) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\GraphObj.cpp
DEP_CPP_GRAPH=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\Button.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\GraphObj.h"\
	".\RouteDialog.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\Track.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\GraphObj.obj" : $(SOURCE) $(DEP_CPP_GRAPH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\GraphObj.obj" : $(SOURCE) $(DEP_CPP_GRAPH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\GraphObj.sbr" : $(SOURCE) $(DEP_CPP_GRAPH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\GraphObj.obj" : $(SOURCE) $(DEP_CPP_GRAPH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\GraphObj.sbr" : $(SOURCE) $(DEP_CPP_GRAPH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\blip.cpp
DEP_CPP_BLIP_=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\GraphObj.h"\
	".\RouteDialog.h"\
	".\StdAfx.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\blip.obj" : $(SOURCE) $(DEP_CPP_BLIP_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\blip.obj" : $(SOURCE) $(DEP_CPP_BLIP_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\blip.sbr" : $(SOURCE) $(DEP_CPP_BLIP_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\blip.obj" : $(SOURCE) $(DEP_CPP_BLIP_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\blip.sbr" : $(SOURCE) $(DEP_CPP_BLIP_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ButtonDl.cpp
DEP_CPP_BUTTON=\
	".\Blip.h"\
	".\Button.h"\
	".\ButtonDl.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\ButtonDl.obj" : $(SOURCE) $(DEP_CPP_BUTTON) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\ButtonDl.obj" : $(SOURCE) $(DEP_CPP_BUTTON) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ButtonDl.sbr" : $(SOURCE) $(DEP_CPP_BUTTON) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\ButtonDl.obj" : $(SOURCE) $(DEP_CPP_BUTTON) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ButtonDl.sbr" : $(SOURCE) $(DEP_CPP_BUTTON) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Track.cpp
DEP_CPP_TRACK_=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\Button.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\GraphObj.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\Track.h"\
	".\TrackDlg.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Track.obj" : $(SOURCE) $(DEP_CPP_TRACK_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Track.obj" : $(SOURCE) $(DEP_CPP_TRACK_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Track.sbr" : $(SOURCE) $(DEP_CPP_TRACK_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Track.obj" : $(SOURCE) $(DEP_CPP_TRACK_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Track.sbr" : $(SOURCE) $(DEP_CPP_TRACK_) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LSD.rc

!IF  "$(CFG)" == "LSD - Win32 Release"

DEP_RSC_LSD_R=\
	".\Res\Cursor1.cur"\
	".\Res\LSD.rc2"\
	".\Res\Sheettoo.bmp"\
	".\Res\Symbol.bmp"\
	".\Res\Symbol7.bmp"\
	".\Res\Symbol8.bmp"\
	".\Res\Syminfo1.bmp"\
	{$(INCLUDE)}"\Bmp128.bmp"\
	{$(INCLUDE)}"\Bmp135.bmp"\
	{$(INCLUDE)}"\Ico128.ico"\
	{$(INCLUDE)}"\Ico129.ico"\
	{$(INCLUDE)}"\Toolbar1.bmp"\
	

"$(INTDIR)\LSD.res" : $(SOURCE) $(DEP_RSC_LSD_R) "$(INTDIR)"
   $(RSC) /l 0x412 /fo"$(INTDIR)/LSD.res" /d "NDEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

DEP_RSC_LSD_R=\
	".\Res\Cursor1.cur"\
	".\Res\LSD.rc2"\
	".\Res\Sheettoo.bmp"\
	".\Res\Symbol.bmp"\
	".\Res\Symbol7.bmp"\
	".\Res\Symbol8.bmp"\
	".\Res\Syminfo1.bmp"\
	{$(INCLUDE)}"\Bmp128.bmp"\
	{$(INCLUDE)}"\Bmp135.bmp"\
	{$(INCLUDE)}"\Ico128.ico"\
	{$(INCLUDE)}"\Ico129.ico"\
	{$(INCLUDE)}"\Toolbar1.bmp"\
	

"$(INTDIR)\LSD.res" : $(SOURCE) $(DEP_RSC_LSD_R) "$(INTDIR)"
   $(RSC) /l 0x412 /fo"$(INTDIR)/LSD.res" /d "_DEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

# PROP Exclude_From_Build 1
# ADD BASE RSC /l 0x412
# ADD RSC /l 0x412
# SUBTRACT RSC /x

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\SetMar.cpp
DEP_CPP_SETMA=\
	".\LSD.h"\
	".\SetMar.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\SetMar.obj" : $(SOURCE) $(DEP_CPP_SETMA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\SetMar.obj" : $(SOURCE) $(DEP_CPP_SETMA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\SetMar.sbr" : $(SOURCE) $(DEP_CPP_SETMA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\SetMar.obj" : $(SOURCE) $(DEP_CPP_SETMA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\SetMar.sbr" : $(SOURCE) $(DEP_CPP_SETMA) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\StDirDlg.cpp
DEP_CPP_STDIR=\
	".\LSD.h"\
	".\StdAfx.h"\
	".\StDirDlg.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\StDirDlg.obj" : $(SOURCE) $(DEP_CPP_STDIR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\StDirDlg.obj" : $(SOURCE) $(DEP_CPP_STDIR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\StDirDlg.sbr" : $(SOURCE) $(DEP_CPP_STDIR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\StDirDlg.obj" : $(SOURCE) $(DEP_CPP_STDIR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\StDirDlg.sbr" : $(SOURCE) $(DEP_CPP_STDIR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\StOpDlg.cpp
DEP_CPP_STOPD=\
	".\LSD.h"\
	".\StdAfx.h"\
	".\StOpDlg.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\StOpDlg.obj" : $(SOURCE) $(DEP_CPP_STOPD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\StOpDlg.obj" : $(SOURCE) $(DEP_CPP_STOPD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\StOpDlg.sbr" : $(SOURCE) $(DEP_CPP_STOPD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\StOpDlg.obj" : $(SOURCE) $(DEP_CPP_STOPD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\StOpDlg.sbr" : $(SOURCE) $(DEP_CPP_STOPD) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\TableFrm.cpp
DEP_CPP_TABLEF=\
	".\LSD.h"\
	".\StdAfx.h"\
	".\TableFrm.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\TableFrm.obj" : $(SOURCE) $(DEP_CPP_TABLEF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\TableFrm.obj" : $(SOURCE) $(DEP_CPP_TABLEF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableFrm.sbr" : $(SOURCE) $(DEP_CPP_TABLEF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\TableFrm.obj" : $(SOURCE) $(DEP_CPP_TABLEF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\TableFrm.sbr" : $(SOURCE) $(DEP_CPP_TABLEF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\pwdlg.cpp
DEP_CPP_PWDLG=\
	".\Pwdlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\pwdlg.obj" : $(SOURCE) $(DEP_CPP_PWDLG) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\pwdlg.obj" : $(SOURCE) $(DEP_CPP_PWDLG) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\pwdlg.sbr" : $(SOURCE) $(DEP_CPP_PWDLG) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\pwdlg.obj" : $(SOURCE) $(DEP_CPP_PWDLG) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\pwdlg.sbr" : $(SOURCE) $(DEP_CPP_PWDLG) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\KeyCheck.cpp
DEP_CPP_KEYCH=\
	".\LSD.h"\
	".\DriveDlg.h"\
	".\KeyCheck.h"\
	".\Pwdlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\KeyCheck.obj" : $(SOURCE) $(DEP_CPP_KEYCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\KeyCheck.obj" : $(SOURCE) $(DEP_CPP_KEYCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\KeyCheck.sbr" : $(SOURCE) $(DEP_CPP_KEYCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\KeyCheck.obj" : $(SOURCE) $(DEP_CPP_KEYCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\KeyCheck.sbr" : $(SOURCE) $(DEP_CPP_KEYCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DriveDlg.cpp
DEP_CPP_DRIVE=\
	".\LSD.h"\
	".\DriveDlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\DriveDlg.obj" : $(SOURCE) $(DEP_CPP_DRIVE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\DriveDlg.obj" : $(SOURCE) $(DEP_CPP_DRIVE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\DriveDlg.sbr" : $(SOURCE) $(DEP_CPP_DRIVE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\DriveDlg.obj" : $(SOURCE) $(DEP_CPP_DRIVE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\DriveDlg.sbr" : $(SOURCE) $(DEP_CPP_DRIVE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\IOList.cpp
DEP_CPP_IOLIS=\
	"..\Eipsim\StdAfx.h"\
	".\LSD.h"\
	".\IOInfo.h"\
	".\IOList.h"\
	".\Iorack.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	{$(INCLUDE)}"\Parser.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\IOList.obj" : $(SOURCE) $(DEP_CPP_IOLIS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\IOList.obj" : $(SOURCE) $(DEP_CPP_IOLIS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\IOList.sbr" : $(SOURCE) $(DEP_CPP_IOLIS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\IOList.obj" : $(SOURCE) $(DEP_CPP_IOLIS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\IOList.sbr" : $(SOURCE) $(DEP_CPP_IOLIS) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\stobject.cpp
DEP_CPP_STOBJ=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\Button.h"\
	".\LSD.h"\
	".\GraphObj.h"\
	".\Signal.h"\
	".\StdAfx.h"\
	".\StObject.h"\
	".\StrList.h"\
	".\Track.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\stobject.obj" : $(SOURCE) $(DEP_CPP_STOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\stobject.obj" : $(SOURCE) $(DEP_CPP_STOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\stobject.sbr" : $(SOURCE) $(DEP_CPP_STOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\stobject.obj" : $(SOURCE) $(DEP_CPP_STOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\stobject.sbr" : $(SOURCE) $(DEP_CPP_STOBJ) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\strlist.cpp
DEP_CPP_STRLI=\
	".\StdAfx.h"\
	".\StrList.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\strlist.obj" : $(SOURCE) $(DEP_CPP_STRLI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\strlist.obj" : $(SOURCE) $(DEP_CPP_STRLI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\strlist.sbr" : $(SOURCE) $(DEP_CPP_STRLI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\strlist.obj" : $(SOURCE) $(DEP_CPP_STRLI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\strlist.sbr" : $(SOURCE) $(DEP_CPP_STRLI) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\IOInfo.cpp
DEP_CPP_IOINF=\
	".\IOInfo.h"\
	".\Iorack.h"\
	".\StdAfx.h"\
	".\StrList.h"\
	{$(INCLUDE)}"\Parser.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\IOInfo.obj" : $(SOURCE) $(DEP_CPP_IOINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\IOInfo.obj" : $(SOURCE) $(DEP_CPP_IOINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\IOInfo.sbr" : $(SOURCE) $(DEP_CPP_IOINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\IOInfo.obj" : $(SOURCE) $(DEP_CPP_IOINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\IOInfo.sbr" : $(SOURCE) $(DEP_CPP_IOINF) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\iorack.cpp
DEP_CPP_IORAC=\
	".\Iorack.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\iorack.obj" : $(SOURCE) $(DEP_CPP_IORAC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\iorack.obj" : $(SOURCE) $(DEP_CPP_IORAC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\iorack.sbr" : $(SOURCE) $(DEP_CPP_IORAC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\iorack.obj" : $(SOURCE) $(DEP_CPP_IORAC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\iorack.sbr" : $(SOURCE) $(DEP_CPP_IORAC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DotChange.cpp
DEP_CPP_DOTCH=\
	".\LSD.h"\
	".\dotchange.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\DotChange.obj" : $(SOURCE) $(DEP_CPP_DOTCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\DotChange.obj" : $(SOURCE) $(DEP_CPP_DOTCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\DotChange.sbr" : $(SOURCE) $(DEP_CPP_DOTCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\DotChange.obj" : $(SOURCE) $(DEP_CPP_DOTCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\DotChange.sbr" : $(SOURCE) $(DEP_CPP_DOTCH) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BmpTrackCon.cpp
DEP_CPP_BMPTR=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	".\Blip.h"\
	".\BmpTrackCon.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\LSDView.h"\
	".\GraphObj.h"\
	".\RouteDialog.h"\
	".\StdAfx.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Parser.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\BmpTrackCon.obj" : $(SOURCE) $(DEP_CPP_BMPTR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\BmpTrackCon.obj" : $(SOURCE) $(DEP_CPP_BMPTR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\BmpTrackCon.sbr" : $(SOURCE) $(DEP_CPP_BMPTR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\BmpTrackCon.obj" : $(SOURCE) $(DEP_CPP_BMPTR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\BmpTrackCon.sbr" : $(SOURCE) $(DEP_CPP_BMPTR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RouteLine.cpp
DEP_CPP_ROUTE=\
	".\RouteLine.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\RouteLine.obj" : $(SOURCE) $(DEP_CPP_ROUTE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\RouteLine.obj" : $(SOURCE) $(DEP_CPP_ROUTE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\RouteLine.sbr" : $(SOURCE) $(DEP_CPP_ROUTE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\RouteLine.obj" : $(SOURCE) $(DEP_CPP_ROUTE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\RouteLine.sbr" : $(SOURCE) $(DEP_CPP_ROUTE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ObjSetTextDlg.cpp
DEP_CPP_OBJSE=\
	".\LSD.h"\
	".\ObjSetTextDlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\ObjSetTextDlg.obj" : $(SOURCE) $(DEP_CPP_OBJSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\ObjSetTextDlg.obj" : $(SOURCE) $(DEP_CPP_OBJSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjSetTextDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\ObjSetTextDlg.obj" : $(SOURCE) $(DEP_CPP_OBJSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjSetTextDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RouteDialog.cpp
DEP_CPP_ROUTED=\
	".\LSD.h"\
	".\RouteDialog.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\RouteDialog.obj" : $(SOURCE) $(DEP_CPP_ROUTED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\RouteDialog.obj" : $(SOURCE) $(DEP_CPP_ROUTED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\RouteDialog.sbr" : $(SOURCE) $(DEP_CPP_ROUTED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\RouteDialog.obj" : $(SOURCE) $(DEP_CPP_ROUTED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\RouteDialog.sbr" : $(SOURCE) $(DEP_CPP_ROUTED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\l.Et\LSDet.rc

!IF  "$(CFG)" == "LSD - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"

DEP_RSC_LSDET=\
	".\l.Et\bmp128.bmp"\
	".\l.Et\res\cursor1.cur"\
	".\l.Et\res\LSD.rc2"\
	".\l.Et\res\sheettoo.bmp"\
	".\l.Et\res\symbol.bmp"\
	".\l.Et\res\symbol7.bmp"\
	".\l.Et\res\symbol8.bmp"\
	".\l.Et\res\syminfo1.bmp"\
	

"$(INTDIR)\LSDet.res" : $(SOURCE) $(DEP_RSC_LSDET) "$(INTDIR)"
   $(RSC) /l 0x409 /x /fo"$(INTDIR)/LSDet.res" /i "l.Et" /d "_DEBUG" $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ObjEditDlg.cpp
DEP_CPP_OBJED=\
	".\LSD.h"\
	".\ObjEditDlg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\ObjEditDlg.obj" : $(SOURCE) $(DEP_CPP_OBJED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\ObjEditDlg.obj" : $(SOURCE) $(DEP_CPP_OBJED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjEditDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\ObjEditDlg.obj" : $(SOURCE) $(DEP_CPP_OBJED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\ObjEditDlg.sbr" : $(SOURCE) $(DEP_CPP_OBJED) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=\Eip3\Mscr\Bmpdraw.cpp
DEP_CPP_BMPDR=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	"..\Mscr\stdafx.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Bmpdraw.obj" : $(SOURCE) $(DEP_CPP_BMPDR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Bmpdraw.obj" : $(SOURCE) $(DEP_CPP_BMPDR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Bmpdraw.sbr" : $(SOURCE) $(DEP_CPP_BMPDR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Bmpdraw.obj" : $(SOURCE) $(DEP_CPP_BMPDR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Bmpdraw.sbr" : $(SOURCE) $(DEP_CPP_BMPDR) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=\Eip3\Mscr\Parser.cpp
DEP_CPP_PARSE=\
	"..\Mscr\stdafx.h"\
	{$(INCLUDE)}"\Parser.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Parser.obj" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Parser.obj" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Parser.sbr" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Parser.obj" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Parser.sbr" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=\Eip3\Mscr\scrobj.cpp
DEP_CPP_SCROB=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	"..\Mscr\stdafx.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\scrobj.obj" : $(SOURCE) $(DEP_CPP_SCROB) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\scrobj.obj" : $(SOURCE) $(DEP_CPP_SCROB) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\scrobj.sbr" : $(SOURCE) $(DEP_CPP_SCROB) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\scrobj.obj" : $(SOURCE) $(DEP_CPP_SCROB) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\scrobj.sbr" : $(SOURCE) $(DEP_CPP_SCROB) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=\Eip3\Mscr\Logic.cpp
DEP_CPP_LOGIC=\
	"..\Mscr\logic.h"\
	"..\Mscr\stdafx.h"\
	{$(INCLUDE)}"\Parser.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Logic.obj" : $(SOURCE) $(DEP_CPP_LOGIC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Logic.obj" : $(SOURCE) $(DEP_CPP_LOGIC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Logic.sbr" : $(SOURCE) $(DEP_CPP_LOGIC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\Logic.obj" : $(SOURCE) $(DEP_CPP_LOGIC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\Logic.sbr" : $(SOURCE) $(DEP_CPP_LOGIC) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=\Eip3\Mscr\bmpinfo.cpp
DEP_CPP_BMPIN=\
	"..\Eipsim\StdAfx.h"\
	"..\Mscr\Bmpdraw.h"\
	"..\Mscr\logic.h"\
	"..\Mscr\MScrOpt.h"\
	"..\Mscr\Scrobj.h"\
	"..\Mscr\stdafx.h"\
	{$(INCLUDE)}"\Bmpinfo.h"\
	{$(INCLUDE)}"\Scrinfo.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\bmpinfo.obj" : $(SOURCE) $(DEP_CPP_BMPIN) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\bmpinfo.obj" : $(SOURCE) $(DEP_CPP_BMPIN) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\bmpinfo.sbr" : $(SOURCE) $(DEP_CPP_BMPIN) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


BuildCmds= \
	$(CPP) $(CPP_PROJ) $(SOURCE) \
	

"$(INTDIR)\bmpinfo.obj" : $(SOURCE) $(DEP_CPP_BMPIN) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

"$(INTDIR)\bmpinfo.sbr" : $(SOURCE) $(DEP_CPP_BMPIN) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Develop.cpp
DEP_CPP_DEVEL=\
	".\Blip.h"\
	".\LSD.h"\
	".\LSDDoc.h"\
	".\Develop.h"\
	".\GraphObj.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "LSD - Win32 Release"


"$(INTDIR)\Develop.obj" : $(SOURCE) $(DEP_CPP_DEVEL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 Debug"


"$(INTDIR)\Develop.obj" : $(SOURCE) $(DEP_CPP_DEVEL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Develop.sbr" : $(SOURCE) $(DEP_CPP_DEVEL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ELSEIF  "$(CFG)" == "LSD - Win32 DebugET"


"$(INTDIR)\Develop.obj" : $(SOURCE) $(DEP_CPP_DEVEL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"

"$(INTDIR)\Develop.sbr" : $(SOURCE) $(DEP_CPP_DEVEL) "$(INTDIR)"\
 "$(INTDIR)\LSD.pch"


!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
