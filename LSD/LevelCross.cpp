//=======================================================
//==              LevelCross.cpp
//=======================================================
//	파 일 명 :  LevelCross.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  1.01
//	설    명 :  건널목에 관한 정보및 처리를 담당한다.
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "LevelCross.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(CLevelCross, CGraphObject, 0)

//=======================================================
//
//  함 수 명 :  CLevelCross
//  함수출력 :  없음
//  함수입력 :  int t, int cx, int cy
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  생성자
//
//=======================================================
CLevelCross::CLevelCross( CString strName, int cx, int cy) : CGraphObject(CRect(CPoint(cx,cy), CSize(5,5)),"l") 
{
	Name      = strName;
	m_bUnique = TRUE;
	m_nShape = B_NAME_B;
	m_nLR = 0;
	m_nType = 0;
	nL.y = GridSize * 2;
	UpdateRect();
	Init();	
}

//=======================================================
//
//  함 수 명 :  Draw
//  함수출력 :  없음
//  함수입력 :  CDC* pDC , COLORREF cColor
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  화면에 건널목을 그린다.
//
//=======================================================
void CLevelCross::Draw(CDC* pDC , COLORREF cColor) {
	if (!isValid()) return;
	CPen newPen;
	CBrush black(cColor);
    if( newPen.CreatePen( PS_SOLID, 1, cColor ) )
    {
        CPen* pOldPen = pDC->SelectObject( &newPen );

		int r = GridSize - 1;
		CRect cir(x-r,y-r,x+r,y+r);
		pDC->MoveTo(cir.left , cir.top);
		pDC->LineTo(cir.left+r-2, cir.top);
		pDC->LineTo(cir.left+r-2, cir.bottom);
		pDC->LineTo(cir.left, cir.bottom);

		pDC->MoveTo(cir.right , cir.top);
		pDC->LineTo(cir.right-r+2, cir.top);
		pDC->LineTo(cir.right-r+2, cir.bottom);
		pDC->LineTo(cir.right, cir.bottom);

		if (m_nShape & B_NAME_B)
			TextOutAtPointCenter( pDC, nL, Name );
        pDC->SelectObject( pOldPen );
    }
}

//=======================================================
//
//  함 수 명 :  isNoGrid
//  함수출력 :  BOOL
//  함수입력 :  CBlip *pBlip
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :
//
//=======================================================
BOOL CLevelCross::isNoGrid(CBlip *pBlip)
{
	if (m_pBlips[0] == pBlip) return TRUE;
	else return FALSE;
}

//=======================================================
//
//  함 수 명 :  Init
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  변수 초기화
//
//=======================================================
void CLevelCross::Init() {
	m_pBlips[0] = &nL;
	m_pBlips[1] = NULL;
	m_nHandle = 1;
	nL.Visible(m_nShape & B_NAME_B);
}

//=======================================================
//
//  함 수 명 :  Read
//  함수출력 :  없음
//  함수입력 :  CArchive &ar
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  시리얼라이즈 담당 함수
//
//=======================================================
void CLevelCross::Read(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar >> m_nShape >> m_nType >> m_nLR >> m_nLine;
}

//=======================================================
//
//  함 수 명 :  Write
//  함수출력 :  없음
//  함수입력 :  CArchive &ar
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  시리얼라이즈를 담당
//
//=======================================================
void CLevelCross::Write(CArchive &ar) {
	CGraphObject::Serialize(ar);
	ar << m_nShape << m_nType << m_nLR << m_nLine;
}

