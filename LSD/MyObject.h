//=======================================================
//==              MyObject.h
//=======================================================
//	파 일 명 :  MyObject.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#ifndef _MyObject_H
#define _MyObject_H

#include "GraphObj.h"

class CMyObjectDlg;
class CLSDView;

#define MYOBJ_REVERSE 0x80000000
#define MYOBJ_NOINFO  0x40000000

#define ETCOBJTYPE_STATIONNAME		1   // 역명
#define ETCOBJTYPE_LEFTARROWTL		2	// 인접역명 좌
#define ETCOBJTYPE_RIGHTARROWTL		3	// 인접역명 우
#define ETCOBJTYPE_LGCI         	4	// LG로고
#define ETCOBJTYPE_COUNTER       	5	// 계수기
#define ETCOBJTYPE_OPERATPANNEL    	6	// 조작반
#define ETCOBJTYPE_TEXT         	7	// Text
#define ETCOBJTYPE_MINISTATIONNAME  8	// 미니역
#define ETCOBJTYPE_PLATFORM     	9	// 조작반
#define ETCOBJTYPE_WARNINGL   		11	// LG로고
#define ETCOBJTYPE_WARNINGR   		12	// LG로고
#define ETCOBJTYPE_LEFTARROWTEXT	13	// 인접역명 좌
#define ETCOBJTYPE_RIGHTARROWTEXT	14	// 인접역명 우

#define ETCOBJTYPE_SYSBUTTON		20  // System 버튼
#define ETCOBJTYPE_OPTIONBUTTON	    21  // Option 버튼
#define ETCOBJTYPE_BLOCKBUTTON	    22  // Block 버튼
#define ETCOBJTYPE_PAS              23  // PAS 버튼
#define ETCOBJTYPE_TOGGLE          	24  // Toggle 버튼
#define ETCOBJTYPE_DIMMING          25  // Dimming 버튼
#define ETCOBJTYPE_RGB		        26	// RGB
#define ETCOBJTYPE_OSS		        27	// OSS 버튼
#define ETCOBJTYPE_EPK		        28	// APK 버튼   Hand Point 제어 버튼 남/북
#define ETCOBJTYPE_LEVELCROSS     	29	// 건널목
#define ETCOBJTYPE_LCA		     	30	// 건널목
#define ETCOBJTYPE_BCBL		        31	// Block Clear Button Left
#define ETCOBJTYPE_BCBR		        32	// Block Clear Button Right
#define ETCOBJTYPE_FLEVELCROSS     	33	// 건널목
#define ETCOBJTYPE_BRIDGE     	    34	// 다리(교량)
#define ETCOBJTYPE_LAMP_RT			35	// LDK, Block Signal Fail
#define ETCOBJTYPE_AXLE_COUNTER		36	// AXLE COUNTER
#define ETCOBJTYPE_ROAD_WARNER		37	// ROAD WARNER
#define ETCOBJTYPE_GATE_LODGE		38	// Gate Lodge
#define ETCOBJTYPE_X_MARK			39	// X MARK

class CMyObject : public CGraphObject {
	int m_nShape;
public:
	int GetOutStringILD( CString &str );
	CBlip m_Front, m_Rear;
	int m_nType;

	CString m_strText1, m_strText2;

protected:
	DECLARE_SERIAL(CMyObject);
public:
	void MakeDrawingText();
	virtual void Serialize(CArchive &ar);
	virtual void PropertyChange(CWnd *wnd);
	void GetSize(CMyObject& obj);
	void UpdateRect();
	void DrawAngle(CDC *pDC, CPoint p, int dir);
	int  GetID(); 

public:
	void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));
	CMyObject(CString name = "", int cx = 0, int cy = 0, int index = 0);
	virtual const char *NameOf() { return "MyObject"; }
	virtual BOOL isValid(){ return Name != ""; }
	void Init();
friend class CLSDView;
};

#endif