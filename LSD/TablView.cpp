//=======================================================
//==              TablView.cpp
//=======================================================
//	파 일 명 :  TablView.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================
#include "stdafx.h"
#include "LSD.h"
#include "TablView.h"
#include "TableDoc.h"
#include "TableRow.h"
#include "TableFrm.h"
//#include "jsdxf.h"
#include "../include/iolist.h"

// DXFile MyDxf("");

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LEFTSPACE	200
#define ZOOM_NORMAL 100
#define DV			100
#define LINESPERPAGE 20

/////////////////////////////////////////////////////////////////////////////
// CTableView
IMPLEMENT_DYNCREATE(CTableView, CScrollView)

CTableView::CTableView()
{
	m_bViewAll		= TRUE;
	m_iPageNo		= 0;
	m_iZoomFactor	= ZOOM_NORMAL;
}

CTableView::~CTableView()
{
	PurgePageInfo();
}
																		
BEGIN_MESSAGE_MAP(CTableView, CScrollView)
	//{{AFX_MSG_MAP(CTableView)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_REGEN, OnRegen)
	ON_COMMAND(IDC_HIDE, OnHide)
	ON_COMMAND(ID_EDIT_IOLIST, OnEditIolist)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableView drawing
extern int mLogX, mLogY;
CString MakeFileName(CDocument *pDoc, char *ext);

void CTableView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	
	CSize sSize  = CSize(SCREENX,SCREENY);	// 연동 도표를 화면에 출력할때 화면의 크기 
	CSize pSize	 = CSize(PAPERX, PAPERY );	// 연동 도표를 프린터로 출력할때 용지 크기 
	CClientDC dc(NULL);

	m_cDocSize = sSize;

	CTableDoc* pDoc = (CTableDoc*)GetDocument();

	if (pDoc) {
		pDoc->m_PaperSize = pSize;
		pDoc->m_ScreenSize = sSize;
	}

	SetViewWindowSize();

	CTableFrame* pWnd = (CTableFrame*)GetParent();
	if (pWnd && *pWnd) {
		CTableToolBar* TableToolBar = &pWnd->m_TableToolBar;
		if (::IsWindow(TableToolBar->m_hWnd)) {
			m_pHide = &TableToolBar->m_btnHide;
			if (m_pHide && m_pHide->m_hWnd ) {
				m_pHide->SetCheck(!m_bViewAll);
			}
		}
	}
}

void CTableView::OnDraw(CDC* pDC) // 화면에 연동 도표를 그린다. 
{
	CPoint Base( 40 - m_cDocSize.cx/2, m_cDocSize.cy/2 - 80 );
	CTableDoc* pDoc = (CTableDoc*)GetDocument();

	CDC		dc;
	CDC*	pDrawDC = pDC;
	CRect	rectClient;
	int		iSaveDC = pDC->SaveDC();

	pDC->GetClipBox(rectClient);
	CRect rect = rectClient;

	CFont	Font;
	CFont *	oldFont;
	CBrush	Brush;
	CPen	newPen;
	CRow *	item;
	//15,7
	Font.CreateFont(12,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEVICE_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,"돋움체");
	oldFont = pDC->SelectObject(&Font);

	if (!Brush.CreateSolidBrush(RGB(255,255,255))) return;
	Brush.UnrealizeObject();
	pDrawDC->FillRect(rectClient, &Brush);

	m_iHeight = 17;

    if( newPen.CreatePen( PS_SOLID, 1, RGB(0,0,0) ) )
	{
        CPen* pOldPen = pDrawDC->SelectObject( &newPen );
		CRow::SetPos(Base.x,Base.y,m_iHeight);
		CObList *m_objects = &(pDoc->m_Items);
		POSITION pos = m_objects->GetHeadPosition();
		pDrawDC->SetBkMode(TRANSPARENT);

		int loc = 0;
		PageInfo page;
		page.sy = 0;
		page.hy = 10000;
		pDoc->AllocArea(&page);

		int nRows = 0;

		CRow::RowType oldtype = CRow::ROWNONE;
		CRow::SetPos(Base.x,Base.y,m_iHeight);
		while ( pos != NULL )
		{
			item = (CRow*)m_objects->GetNext( pos );
			if (item->m_RouteName == "") continue;
			if ( item->m_nRowType != oldtype ) {
				loc = 0;
			}
			if (!loc) { 
				if ( oldtype == CRow::ROWNONE ) {
					item->DrawHeader(pDrawDC,CRow::m_Org);
					CRow::m_Org.y -= (CRow::m_nTextH*4);  // 실제 내용 처음 출력 부분
				} else {
					CRow::m_Org.y -= (CRow::m_nTextH);
					item->DrawHeader(pDrawDC,CRow::m_Org);
					CRow::m_Org.y -= (CRow::m_nTextH*4);
				}
				nRows += 5;
			}
			oldtype = (CRow::RowType)item->m_nRowType;
			loc++;
			item->Locate();

			if ( item->m_Org.y <= rectClient.top + 100 && item->m_Org.y >= rectClient.bottom - 200 ) {
				item->Draw(pDrawDC, m_bViewAll);
			}
			nRows += item->GetHeight();
		} // while ( pos != NULL )
		nRows += 8;
		if ( nRows * CRow::m_nTextH > m_cDocSize.cy ) {
			m_cDocSize.cy = nRows * CRow::m_nTextH ;
		}
	pDrawDC->SelectObject( pOldPen );
	} // if( newPen.CreatePen( PS_SOLID, 1, RGB(0,0,0) ) )
	pDC->SelectObject(oldFont);
	Font.DeleteObject();
	pDC->RestoreDC(iSaveDC);
}

/////////////////////////////////////////////////////////////////////////////
// CTableView diagnostics

#ifdef _DEBUG
void CTableView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CTableView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTableView message handlers
void CTableView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	CScrollView::OnBeginPrinting(pDC, pInfo);
}

BOOL CTableView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	m_iTotPage = CalcPageAlign();
//	m_iTotPage = m_iTotPage + ((CLSDApp*)AfxGetApp())->m_iTotPage;
	m_iTotPage =((CLSDApp*)AfxGetApp())->m_iTotPage;
	pInfo->SetMaxPage( m_iTotPage );
	return DoPreparePrinting(pInfo);
}

void CTableView::DrawItem(CDC *pDC, int c, int r, int cx, int cy)
{
	if (r == m_iCursorRow) {
		pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkColor(RGB(128,0,255));
	}
	else {
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SetBkColor(RGB(255,255,255));
	}
}

//=======================================================
//
//  함 수 명 :  OnLButtonDblClk
//  함수출력 :  없음
//  함수입력 :  UINT nFlags, CPoint point
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자가 마우스 왼쪽 버튼을 더블클릭 했을때
//
//=======================================================
void CTableView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CTableDoc* pDoc = (CTableDoc*)GetDocument();
	// TODO: Add your message handler code here and/or call default
	CClientDC objDC(this);
	OnPrepareDC(&objDC);
	objDC.DPtoLP(&point);
	CObList *m_objects = &(pDoc->m_Items);
	POSITION pos = m_objects->GetHeadPosition();
	CRow* item;
	while ( pos != NULL )
	{
		item = (CRow*)m_objects->GetNext( pos );
		CString dif;
		if ((CRow::RowType)item->m_nType != CRow::_TOKENLESSC) {
			if (item->CheckAreaAndEdit(pDoc,point,dif)) {
				pDoc->UpdateModFile( dif );
				Invalidate();
				break;
			}
		}
	}
	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CTableView::DocToClient(CRect &rect)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&rect);
}

void CTableView::DocToClient(CPoint &point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&point);
}


void CTableView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CScrollView::OnPrepareDC(pDC, pInfo);

	CTableDoc* pDoc = (CTableDoc*)GetDocument();

	pDoc->m_bTotalPage = ((CLSDApp*)AfxGetApp())->m_TotPage;
	pDoc->m_iStartPage = ((CLSDApp*)AfxGetApp())->m_iStartPage;
	pDoc->m_iTableType = ((CLSDApp*)AfxGetApp())->m_iTableType;

	if (!pDC->IsPrinting()) { // 화면에 표시할때의 화면 사이즈 
		CSize size = m_cDocSize;
		CClientDC dc(NULL);
		size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX), DV);
		SetViewWindowSize();

		pDC->SetMapMode(MM_ANISOTROPIC);
		pDC->SetViewportExt(pDC->GetDeviceCaps(LOGPIXELSX),pDC->GetDeviceCaps(LOGPIXELSY));
		pDC->SetWindowExt( m_iZoomFactor, -m_iZoomFactor );
		CPoint ptOrg;
		ptOrg.x = m_cDocSize.cx / 2;
		ptOrg.y = m_cDocSize.cy / 2;
		pDC->OffsetWindowOrg(-ptOrg.x,ptOrg.y);
	} else { // for print // 프린팅 할때의 화면 사이즈 
		pDC->SetMapMode(MM_ANISOTROPIC);
		pDC->SetViewportOrg(0,0);
		int prscale = ((CLSDApp*)AfxGetApp())->CalcPrintScaleTable(pDC);
		pDC->SetViewportExt( prscale, prscale );
		pDC->SetWindowExt( 100, -100 );
		pDC->SetWindowOrg( -m_cDocSize.cx/2 - mLogX, m_cDocSize.cy/2 + mLogY);
	}
}

BOOL CTableView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	// TODO: Add your specialized code here and/or call the base class
	int xOrig, x;
	int yOrig, y;
	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) || (pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) || (pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);

	x += sizeScroll.cx;
	if (x < 0) x = 0;
	else if (x > xMax) x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0) y = 0;
	else if (y > yMax) y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig) return FALSE;

	if (bDoScroll)
	{
		CRect rect;
		GetClientRect(&rect);
		CRect clip = rect;

		ScrollWindow(-(x-xOrig), -(y-yOrig), rect, clip);
		if (x != xOrig) {
			SetScrollPos(SB_HORZ, x);
			if (x>xOrig) {	// Left scroll
				rect.left = rect.right - (x - xOrig + 1);
				rect.right++;
			} else {
				rect.right = rect.left + (-x + xOrig + 1); 
				rect.left--;
			}
			InvalidateRect(rect, FALSE);
		}
		if (y != yOrig) {			// y inc. down
			SetScrollPos(SB_VERT, y);
			if (y>yOrig) {		
				rect.top = rect.bottom - (y - yOrig + 1);
				rect.bottom++;
			} else {
				rect.bottom = rect.top + (-y + yOrig + 1);
				rect.top--;
			}
			InvalidateRect(rect, FALSE);
		}
	}
	return TRUE;
//	return CScrollView::OnScrollBy(sizeScroll, bDoScroll);
}

void CTableView::OnRegen() 
{
	// TODO: Add your command handler code here
	CTableDoc* pDoc = (CTableDoc*)GetDocument();
	pDoc->TableRegen(!m_bViewAll);
	Invalidate(); 
}

//=======================================================
//
//  함 수 명 :  OnHide
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자가 삭제한 내용을 화면에 보이지 않게 한다.
//
//=======================================================
void CTableView::OnHide() 
{
	// TODO: Add your command handler code here
	m_bViewAll = !m_pHide->GetCheck();
	OnRegen();
}

//=======================================================
//
//  함 수 명 :  OnEditIolist
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  에디트 리스트를 띄운다.
//
//=======================================================
void CTableView::OnEditIolist() 
{
	// TODO: Add your command handler code here
	CTableDoc* pDoc = (CTableDoc*)GetDocument();

	CString docname = pDoc->GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l+1, "IOD" );
	docname.ReleaseBuffer( );

	
	char *s;  // IOLIST 호출
	int ch = '\\';
	s = strrchr( docname, ch );
	CString str = s;
	CString path;
	int len = strlen(docname) - strlen(str);
	path = docname.Left(len);

	CString Path;
	CString Arg = docname.Left(l);
	Path.Format("IOlist.exe %s", Arg);
	
	int result = WinExec(Path, SW_HIDE);
	if ( result == ERROR_FILE_NOT_FOUND ) {
		AfxMessageBox("Can't find IOlist.exe");
		return;
	}
	if ( result == ERROR_PATH_NOT_FOUND) {
		AfxMessageBox("Can't find IOlist.exe path");
		return;
	}
}

//=======================================================
//
//  함 수 명 :  SetViewWindowSize
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  윈도우 사이즈를 설정한다.
//
//=======================================================
void CTableView::SetViewWindowSize()
{
	CClientDC dc(NULL);
	CSize size = m_cDocSize;	
	size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX), DV) * ZOOM_NORMAL / m_iZoomFactor;
	size.cy = MulDiv(size.cy, dc.GetDeviceCaps(LOGPIXELSX), DV) * ZOOM_NORMAL / m_iZoomFactor;
	SetScrollSizes(MM_TEXT, size);
}

void CTableView::PurgePageInfo()
{
	POSITION pos;
	for (pos = m_PageInfo.GetHeadPosition(); pos != NULL; ) {
		PageInfo *page = (PageInfo*)m_PageInfo.GetNext(pos);
		delete page;
	}
	m_PageInfo.RemoveAll();
}

//=======================================================
//
//  함 수 명 :  CalcPageAlign
//  함수출력 :  int - 페이지수
//  함수입력 :  없음
//  작 성 자 :  김도영
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성된 연동 도표를 토대로 출력시 전체 페이지수를 계산한다.
//
//=======================================================
int CTableView::CalcPageAlign()
{
	PurgePageInfo();

	CTableDoc* pDoc = (CTableDoc*)GetDocument();
	CObList *m_objects = &(pDoc->m_Items);
	CRow::RowType type, oldtype = CRow::ROWNONE;
	CRow *item;

	char oldUD = 'U';

	int nTextH = CRow::m_nTextH;
	int pages = 0;
	int nMaxLines = PAPERY / nTextH - 33;	// 2002.4.3. IM 20->36
	int nLines = 0;
	int nRowCount = 0;
	int nRowNo = 0;
	int nHeight;
	int nPageNo = 0;

	POSITION pos = m_objects->GetHeadPosition();
	while ( pos != NULL )
	{
		item = (CRow*)m_objects->GetNext( pos );
		type = (CRow::RowType)item->m_nRowType;
		if ( ( /*oldtype != CRow::ROWNONE && */oldtype != type ) || ( item->m_cUD != oldUD  ) ) {
			PageInfo *page = new PageInfo;
			page->m_nType = oldtype;
			page->m_iPageNo = ++nPageNo;
			//nPageNo = 0;
			page->m_nActivePageNo = ++pages;
			page->sy = nRowNo;
			page->hy = nRowCount;
			m_PageInfo.AddTail((CObject*)page);
			nRowNo += nRowCount;
			nLines = 0;
			nRowCount = 0;
			oldUD = item->m_cUD;
		}
		oldtype = type;
		nHeight = item->GetHeight();
		if ( nLines + nHeight >= nMaxLines ) {
			PageInfo *page = new PageInfo;
			page->m_nType = type;
			page->m_iPageNo = ++nPageNo;
			page->m_nActivePageNo = ++pages;
			page->sy = nRowNo;
			page->hy = nRowCount;
			m_PageInfo.AddTail((CObject*)page);
			nRowNo += nRowCount;
			nLines = nHeight;
			nRowCount = 1;
		} else {
			nLines += nHeight;
			nRowCount++;
		}
	}

	PageInfo *page = new PageInfo;
	page->m_nType = type;
	page->m_iPageNo = ++nPageNo;
	page->m_nActivePageNo = ++pages;
	page->sy = nRowNo;
	page->hy = nRowCount;
	m_PageInfo.AddTail((CObject*)page);
	
	pages--;
//	pages++;
//	pages++;
	return pages;
}

void CTableView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	CTableDoc* pDoc = (CTableDoc*)GetDocument();
	int m_nPage;
	CRect client;
	CString strTemp;
	pDC->GetClipBox(client);
	CRect rect = client;
	int gap = 10;
	int iFirstCheck = 0;
	int hx = client.right-gap;
	int hy = client.bottom+gap;
	rect.top = rect.top - gap;
	rect.left = rect.left + gap;
	rect.right = rect.right - gap;
	rect.bottom = rect.bottom + gap;

	int y = rect.bottom;
	int x = rect.left+80; // 시작점
	CPoint Base( rect.left+70, rect.top-40 );
	m_nPage = pInfo->m_nCurPage;

	CDC dc;
	CDC* pDrawDC = pDC;

	OnPrepareDC( pDrawDC );


	CBitmap bitmap;

	int iSaveDC = pDC->SaveDC();

	CFont font;
	CFont *oldfont;

	font.CreateFont(12,6,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEVICE_PRECIS ,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FIXED_PITCH,
						"돋움체");
	oldfont = pDC->SelectObject(&font);

	CBrush brush;
	if (!brush.CreateSolidBrush(RGB(255,255,255))) return;
	brush.UnrealizeObject();
	pDrawDC->FillRect(client, &brush);

//	pDC->DrawRect(pDrawDC,rect);

	m_iHeight = 17;
	CRow *item;

	CPen newPen;

	CString docname = pDoc->GetPathName();
	int l = docname.Find('.');
	LPTSTR p = docname.GetBuffer(l+3);
	strcpy( p+l, "" );
	docname.ReleaseBuffer( );

	char szFileName[128];
	char szDwgName[128];
	char *szDwgType = "";
	char *szDwgNo = "";

	char szNote[1024] = "";
					
	char *dwgnostr = "@ABCDEFGHJKLMNPQRSTUVWXYZ";
	szDwgNo[3] = dwgnostr[ pInfo->m_nCurPage ];
	sprintf( szFileName, "%s-%d", docname, pInfo->m_nCurPage ); 
	sprintf( szDwgName, "%s CONTROL TABLE %d", szDwgType, pInfo->m_nCurPage ); 

	POSITION pos;
    if( newPen.CreatePen( PS_SOLID, 1, RGB(0,0,0) ) )
	{
        CPen* pOldPen = pDrawDC->SelectObject( &newPen );
		CRow::SetPos(Base.x,Base.y,m_iHeight);
		CObList *m_objects = &(pDoc->m_Items);
		if ( pInfo->m_nCurPage == 3 )  pos = m_objects->GetHeadPosition();
		else pos = m_posPage;

		pDrawDC->SetBkMode(TRANSPARENT);

		iFirstCheck = 0;
		
		
		pDoc->DrawPageOff(pDrawDC , m_nPage , m_iTotPage+1 ); // 외곽선을 그린다. 

		if ( m_nPage == 1 ) { //if (m_iPageNo) {
		
			pDoc->DrawCover(pDrawDC); // 외곽선을 그린다. 

		} else if ( m_nPage == 2 ){
			pDoc->DrawCover(pDrawDC); // 외곽선을 그린다. 
		}else {
			int loc = 0;
			PageInfo page;
			page.sy = 0;
			page.hy = 10000;	// RT1
			pDoc->AllocArea(&page);

			int nRows = 0;
			char oldUD = 'U';
			CRow::RowType oldtype = CRow::ROWNONE;
			CRow::SetPos(Base.x,Base.y,m_iHeight);
			while ( pos != NULL )
			{
				m_posPage = pos;
				item = (CRow*)m_objects->GetNext( pos );
				if (item->m_RouteName == "") continue;
				if ( item->m_nRowType != oldtype && iFirstCheck == 1) {  // 행의 타입이 바뀔경우 페이지를 바꾼다. 
					loc = 0;
					oldtype = (CRow::RowType)item->m_nRowType;
					break;
				}

				if ( item->m_cUD != oldUD &&  iFirstCheck == 1) { // 행의 업 다운이 바뀔경우 페이지를 바꾼다. 
					loc = 0;
					oldtype = (CRow::RowType)item->m_nRowType;
					break;
				}

				if ( nRows >= 68 ) { // 행이 페이지 길이를 초과할경우 페이지를 바꾼다.
					loc = 0;
					break;
				}

				if (!loc) { 
					if ( oldtype == CRow::ROWNONE ) {
						item->DrawHeader(pDrawDC,CRow::m_Org);
						CRow::m_Org.y -= (CRow::m_nTextH*4);  // 실제 내용 처음 출력 부분
						iFirstCheck = 1;
					}
					else {
						CRow::m_Org.y -= (CRow::m_nTextH);
						item->DrawHeader(pDrawDC,CRow::m_Org);
						CRow::m_Org.y -= (CRow::m_nTextH*4);
						iFirstCheck = 1;
					}
					nRows += 5;				}
				oldtype = (CRow::RowType)item->m_nRowType;
				oldUD = item->m_cUD;
				loc++;
				item->Locate();
				if ( item->m_Org.y <= client.top + 100 && item->m_Org.y >= client.bottom - 200 )
					item->Draw(pDrawDC, m_bViewAll);
				nRows += item->GetHeight();
			} // while ( pos != NULL )
			nRows += 8;

			if ( nRows * CRow::m_nTextH > m_cDocSize.cy ) {
				m_cDocSize.cy = nRows * CRow::m_nTextH;
			}

			y = y - 10;

			// NOTE 출력 
			strTemp = " - LEGEND - ";
			StrWrite( x, y+370,  strTemp , pDC , 13);
			strTemp = "* TYPE  ";
			StrWrite( x, y+350,  strTemp , pDC , 13);
			strTemp = " *   : Depend Signal ";
			StrWrite( x, y+330,  strTemp , pDC , 13);
			strTemp = "";
			StrWrite( x, y+310,  strTemp , pDC , 13);
			strTemp = "";
			StrWrite( x, y+290,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = "* ROUTE NO  ";
			StrWrite( x+200, y+350,  strTemp , pDC , 13);
			strTemp = " (M) : Main route ";
			StrWrite( x+200, y+330,  strTemp , pDC , 13);			
			strTemp = " (C) : Callon route ";
			StrWrite( x+200, y+310,  strTemp , pDC , 13);			
			strTemp = " (S) : Shunt route ";
			StrWrite( x+200, y+290,  strTemp , pDC , 13);			
            // 옆 라인 
			strTemp = " * SIGNAL CONTROL ";
			StrWrite( x+400, y+350,  strTemp , pDC , 13);
			strTemp = " L  : Loop indicator ";
			StrWrite( x+400, y+330,  strTemp , pDC , 13);
			strTemp = " M  : Main indicator ";
			StrWrite( x+400, y+310,  strTemp , pDC , 13);
			strTemp = " YY : Y + Bottom Y Aspect";
			StrWrite( x+400, y+290,  strTemp , pDC , 13);
			strTemp = " SY : Single Y Aspect";
			StrWrite( x+400, y+270,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = " C  : Callon Aspect";
			StrWrite( x+600, y+330,  strTemp , pDC , 13);			
			strTemp = " S  : Shunt Aspect";
			StrWrite( x+600, y+310,  strTemp , pDC , 13);			
			strTemp = " F  : Free Shunt Aspect";
			StrWrite( x+600, y+290,  strTemp , pDC , 13);			
            // 옆 라인 
			strTemp = " * POINT SET LOCKED DETECTED ";
			StrWrite( x+800, y+350,  strTemp , pDC , 13);
			strTemp = " xR  : Reverse point ";
			StrWrite( x+800, y+330,  strTemp , pDC , 13);
			strTemp = " xN  : Normal point";
			StrWrite( x+800, y+310,  strTemp , pDC , 13);
			strTemp = " [x] : Overlap point ";
			StrWrite( x+800, y+290,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = " * TRACK CIRCUIT ";
			StrWrite( x+1000, y+350,  strTemp , pDC , 13);
			strTemp = " $   : only at the time of ";
			StrWrite( x+1000, y+330,  strTemp , pDC , 13);
			strTemp = "       cleaning the signal ";
			StrWrite( x+1000, y+310,  strTemp , pDC , 13);
			strTemp = " =   : flank protection track ";
			StrWrite( x+1000, y+290,  strTemp , pDC , 13);
			strTemp = " @[N|R] : when point is  [Normal|Reverse]";
			StrWrite( x+1000, y+270,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = " * ROUTE NORMALIZE ";
			StrWrite( x+1200, y+350,  strTemp , pDC , 13);
			strTemp = " %S    : after route track and";
			StrWrite( x+1200, y+330,  strTemp , pDC , 13);
			strTemp = "         free in sequence";
			StrWrite( x+1200, y+310,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = " * ADDITIONAL CONDITION ";
			StrWrite( x+1400, y+350,  strTemp , pDC , 13);
			strTemp = " %H    : Signal cleared when ahead Home signal cleared ";
			StrWrite( x+1400, y+330,  strTemp , pDC , 13);
			strTemp = " %C    : Crank Handle locked, Detected ";
			StrWrite( x+1400, y+310,  strTemp , pDC , 13);
			strTemp = " %B    : Block Granted";
			StrWrite( x+1400, y+290,  strTemp , pDC , 13);
			strTemp = " %LCx  : Levelcrossing Granted ";
			StrWrite( x+1400, y+270,  strTemp , pDC , 13);
            // 옆 라인 
			strTemp = " %Dxxx : Signal cleared when xxx signal cleared";
			StrWrite( x+1750, y+330,  strTemp , pDC , 13);			
			strTemp = " %A    : route is seted when backward start signal route is set";
			StrWrite( x+1750, y+310,  strTemp , pDC , 13);			
			strTemp = " %G    : Signal set clear";
			StrWrite( x+1750, y+290,  strTemp , pDC , 13);	
			
			y = y + 10;
		} 
		pDrawDC->SelectObject( pOldPen );
	} else { // if( newPen.CreatePen( PS_SOLID, 1, RGB(0,0,0) ) )
	
	} // if( newPen.CreatePen( PS_SOLID, 1, RGB(0,0,0) ) )
//	MyDxf.fMakeOff();
	pDC->SelectObject(oldfont);
	font.DeleteObject();
	pDC->RestoreDC(iSaveDC);
}

void CTableView::StrWrite(int x, int y, CString str , CDC* pDC , int iSzie , int iDevid)
{
   	CFont m_Font;
	CFont * oldfont;

	if ( iDevid == 0 ) m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"굴림체");
	else			   m_Font.CreateFont(iSzie,iSzie/2,0,0,0,FALSE,FALSE,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,"Arial Black");

	oldfont = pDC->SelectObject(&m_Font);

	pDC->SelectObject(&m_Font);				
	pDC->TextOut( x, y, str );

	pDC->SelectObject(oldfont);

	m_Font.DeleteObject();
}

int	CTableView::GetHeight()
{ 
	return m_iHeight;
}
