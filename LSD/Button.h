//=======================================================
//====               Button.h                        ==== 
//=======================================================
//  파  일  명: Button.h
//  작  성  자: 김도영
//  작성  날짜: 2004.5.20
//  버      전: 2.01
//  설      명: 압구에 관한 정보 및 처리를 담당한다.
//              
//=======================================================

#ifndef _Button_H
#define _Button_H

/*
       a ------c------ b	c = m_Base
			    |
				 |
		 	      d
*/

#include "GraphObj.h"

#define B_NAME_B	0x1
#define B_NAME_S	0x2
#define B_CONT		0x4
#define B_NOCALLON  0x8

class CMyButtonDlg;
class StationDB;

class CMyButton : public CGraphObject {
	int m_nShape;
	CBlip nB;
protected:
	DECLARE_SERIAL(CMyButton);
public:
	void DrawAngle(CDC *pDC, CPoint p, int dir);
	int m_nType, m_nLR, m_nLine;
	
public:
	void Draw(CDC* pDC , COLORREF cColor = RGB(0,0,0));
	virtual void PropertyChange(CWnd *wnd);
	CMyButton(int t = 0, int cx = 0, int cy = 0);
	virtual void Read(CArchive &ar);
	virtual void Write(CArchive &ar);
	virtual const char *NameOf() { return "Button"; }
	virtual BOOL isValid(){ return Name != ""; }
	BOOL isNoGrid(CBlip *pBlip);
	void Init();
	friend CMyButtonDlg;
friend StationDB;
};

#endif