//=======================================================
//==              LockEdit.h
//=======================================================
//	파 일 명 :  LockEdit.h
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "../include/StrList.h"

class CLockEdit : public CDialog
{
// Construction
public:
	CLockEdit(CString &name, CMyStrList *sw, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLockEdit)
	enum { IDD = IDD_LOCKEDIT };
	CComboBox	m_ListSwitch;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLockEdit)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLockEdit)
	virtual BOOL OnInitDialog();
	afx_msg void OnLockSwitchdel();
	afx_msg void OnLockSwitchins();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString* m_pName;
	CMyStrList* m_pSwitch;
};
