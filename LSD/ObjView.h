#if !defined(AFX_OBJVIEW_H__64209E4E_D9E5_4D20_AD1A_CD7763FB2E78__INCLUDED_)
#define AFX_OBJVIEW_H__64209E4E_D9E5_4D20_AD1A_CD7763FB2E78__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ObjView.h : header file
//
#include "Track.h"
/////////////////////////////////////////////////////////////////////////////
// CObjView view

class CObjView : public CScrollView
{
protected:
	CObjView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CObjView)

// Attributes
public:
	CTrack *pObj;
	int m_nZoomFactor;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CObjView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CObjView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJVIEW_H__64209E4E_D9E5_4D20_AD1A_CD7763FB2E78__INCLUDED_)
