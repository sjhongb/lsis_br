//=======================================================
//==              StOpDlg.cpp
//=======================================================
//	파 일 명 :  StOpDlg.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :
//	설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "StOpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjOperateDlg dialog


CObjOperateDlg::CObjOperateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjOperateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CObjOperateDlg)
	m_nBlockLeft = -1;
	m_nBlockRight = -1;
	m_nType = -1;
	m_nRC = -1;
	m_bRCLeft = FALSE;
	m_bRCRight = FALSE;
	//}}AFX_DATA_INIT
}


void CObjOperateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CObjOperateDlg)
	DDX_Radio(pDX, IDC_BLOCK_LEFT, m_nBlockLeft);
	DDX_Radio(pDX, IDC_BLOCK_RIGHT, m_nBlockRight);
	DDX_Radio(pDX, IDC_IL_TYPE, m_nType);
	DDX_Radio(pDX, IDC_RC, m_nRC);
	DDX_Check(pDX, IDC_RC_LEFT, m_bRCLeft);
	DDX_Check(pDX, IDC_RC_RIGHT, m_bRCRight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CObjOperateDlg, CDialog)
	//{{AFX_MSG_MAP(CObjOperateDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjOperateDlg message handlers
