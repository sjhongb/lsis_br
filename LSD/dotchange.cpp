//=======================================================
//==              dotchange.cpp
//=======================================================
//	파 일 명 :  dotchange.cpp
//  작 성 자 :  김도영
//	작성날짜 :  2004-10-11
//	버    전 :  2.01
//	설    명 :  SVI 화면의 표시 단위를 변경한다.
//
//=======================================================

#include "stdafx.h"
#include "LSD.h"
#include "DotChange.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDotChange dialog


CDotChange::CDotChange(CWnd* pParent /*=NULL*/)
	: CDialog(CDotChange::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDotChange)
	mdot = -1;
	m_iOffSetX = 0;
	m_iOffSetY = 0;
	m_bReverse = FALSE;
	m_nXSize = 1024;
	m_nYSize = 768;
	m_nSystemY = 0;
	m_bSmallFont = FALSE;
	//}}AFX_DATA_INIT
}


void CDotChange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDotChange)
	DDX_Radio(pDX, IDC_RADIO1, mdot);
	DDX_Text(pDX, IDC_EDIT_X, m_iOffSetX);
	DDX_Text(pDX, IDC_EDIT_Y, m_iOffSetY);
	DDX_Check(pDX, IDC_REVERSE, m_bReverse);
	DDX_Text(pDX, IDC_EDIT_VIEW_XSIZE, m_nXSize);
	DDX_Text(pDX, IDC_EDIT_VIEW_YSIZE, m_nYSize);
	DDX_Text(pDX, IDC_EDIT_SYSTEM_YPOS, m_nSystemY);
	DDX_Check(pDX, IDC_CHECK_FONT, m_bSmallFont);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDotChange, CDialog)
	//{{AFX_MSG_MAP(CDotChange)
	ON_BN_CLICKED(IDC_REVERSE, OnReverse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDotChange message handlers


void CDotChange::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	CDialog::OnOK();
}

void CDotChange::OnReverse() 
{
}
