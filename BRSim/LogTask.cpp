#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timers.h"

#include "tickLib.h"
#include "taskLib.h"
#include "kernelLib.h"
#include "sysLib.h"
#include "ioLib.h"
#include "fioLib.h"
#include "msgQLib.h"
#include "sockLib.h"
#include "inetLib.h"
#include "semLib.h"
#include "errnoLib.h"

// Include BSP header
#include "kvme402a.h"
#include "ds1646.h"

#include "projdef.h"
#include "LogTask.h"

/* ----------------------------------------------------------------------- */
//#define NEW_LOG_SYSTEM

/* ----------------------------------------------------------------------- */

#define MAX_LOG_QUEUE_QTY	1024
#define MAX_LOG_TEXT_LEN	2048
#define LOG_SERVER_ADDR		"192.168.1.1"
#define LOG_CLIENT_ADDR		"192.168.1.2"
#define LOG_UDP_PORT		4080

#define LOG_MODE_CONSOLE	0
#define LOG_MODE_LAN		1

/* ----------------------------------------------------------------------- */
// Log Initialize
BYTE	g_LogCtrl[256] = {	LOG_BIT_HEX | LOG_BIT_IND | LOG_BIT_ERR, 
							0,
							0,
							0,
							0,//LOG_BIT_IND | LOG_BIT_ERR | LOG_BIT_DBG,
							0,
							0, };

char		g_bRecvLogs	= 1;
char		g_bSendLogs	= 1;
char		g_bAllLogs 	= 1;
char		g_cLastType = LOG_TYPE_EXT_ALL;

SEM_ID		g_LogSemID 	= 0;
MSG_Q_ID	g_LogMsgQID	= NULL;
int			g_LogTaskID	= 0;
char		g_LogMode	= LOG_MODE_CONSOLE;

typedef	struct 
{
	BYTE	bit;
	BYTE	type;
	char	date[10];
	char	file[18];
	USHORT	line;
	union 
	{
		char	text[MAX_LOG_TEXT_LEN - 32];
		struct 
		{
			char 	text[94];
			USHORT	hexLen;
			BYTE	hexBuffer[MAX_LOG_TEXT_LEN - 128];
		} hex;
	} data;
} LOG_MSG;

/* ----------------------------------------------------------------------- */
void cfg()
{
	int fdConsole = open("/tyCo/1", O_CREAT|O_RDWR, 0);

	int fdNULL = open("/null", O_CREAT|O_RDWR, 0);
	int fdSTDOUT = ioGlobalStdGet(1);

	int	 option = 0;
	char ch = 0;
	int  i;

	ioGlobalStdSet(1, fdNULL);

	while(1)
	{
		fdprintf(fdConsole, "\x1b[2J\x1b[3;0H");
		fdprintf(fdConsole,
			"\t+===============================================================+\n"
			"\t|     Application Log Configuration     [D]BG,[H]EX,[I]ND,[E]RR |\n"
			"\t+---------------------------------------------------------------+\n"
			"\t|%s1) General Log ....................................... [%c%c%c%c]%s|\n"
			"\t|%s2) Comm. Interface Task Log .......................... [%c%c%c%c]%s|\n"
			"\t|%s3) I/O Board Control Task Log ........................ [%c%c%c%c]%s|\n"
			"\t|                                                               |\n"
			"\t|%sA) All Logs ........................................... [%3s]%s|\n"
			"\t| x) Exit.                                                      |\n"              
			"\t+===============================================================+\n"
			"\t Input Command : ",
			(g_cLastType == LOG_TYPE_GEN) ? " \x1b[30m\x1b[47m" : " ", 
			g_LogCtrl[LOG_TYPE_GEN] & LOG_BIT_DBG ? 'D' : '.', 
			g_LogCtrl[LOG_TYPE_GEN] & LOG_BIT_HEX ? 'H' : '.', 
			g_LogCtrl[LOG_TYPE_GEN] & LOG_BIT_IND ? 'I' : '.', 
			g_LogCtrl[LOG_TYPE_GEN] & LOG_BIT_ERR ? 'E' : '.',
			(g_cLastType == LOG_TYPE_GEN) ? "\x1b[0m " : " ",
			(g_cLastType == LOG_TYPE_IF) ? " \x1b[30m\x1b[47m" : " ",
			g_LogCtrl[LOG_TYPE_IF] & LOG_BIT_DBG ? 'D' : '.', 
			g_LogCtrl[LOG_TYPE_IF] & LOG_BIT_HEX ? 'H' : '.', 
			g_LogCtrl[LOG_TYPE_IF] & LOG_BIT_IND ? 'I' : '.', 
			g_LogCtrl[LOG_TYPE_IF] & LOG_BIT_ERR ? 'E' : '.',
			(g_cLastType == LOG_TYPE_IF) ? "\x1b[0m " : " ",
			(g_cLastType == LOG_TYPE_IO) ? " \x1b[30m\x1b[47m" : " ",
			g_LogCtrl[LOG_TYPE_IO] & LOG_BIT_DBG ? 'D' : '.', 
			g_LogCtrl[LOG_TYPE_IO] & LOG_BIT_HEX ? 'H' : '.', 
			g_LogCtrl[LOG_TYPE_IO] & LOG_BIT_IND ? 'I' : '.', 
			g_LogCtrl[LOG_TYPE_IO] & LOG_BIT_ERR ? 'E' : '.',
			(g_cLastType == LOG_TYPE_IO) ? "\x1b[0m " : " ",
			(g_cLastType == LOG_TYPE_EXT_ALL) ? " \x1b[30m\x1b[47m" : " ",
			g_bAllLogs ? "ON" : "OFF",
			(g_cLastType == LOG_TYPE_EXT_ALL) ? "\x1b[0m " : " "
		);

		option = ioctl(fdConsole, FIOGETOPTIONS, 0);
		ioctl (fdConsole, FIOSETOPTIONS, option & ~OPT_LINE);
    	ch = getchar();
		ioctl (fdConsole, FIOSETOPTIONS, option | OPT_LINE);
		switch(ch)
		{
		case '1':
			g_cLastType = LOG_TYPE_GEN;
			break;
		case '2':
			g_cLastType = LOG_TYPE_IF;
			break;
		case '3':
			g_cLastType = LOG_TYPE_IO;
			break;
		case 'a':
		case 'A':
			g_bAllLogs = !g_bAllLogs;
			g_cLastType = LOG_TYPE_EXT_ALL;
			break;
		case 'd':
		case 'D':
			g_LogCtrl[g_cLastType] ^= LOG_BIT_DBG;
			break;
		case 'h':
		case 'H':
			g_LogCtrl[g_cLastType] ^= LOG_BIT_HEX;
			break;
		case 'i':
		case 'I':
			g_LogCtrl[g_cLastType] ^= LOG_BIT_IND;
			break;
		case 'e':
		case 'E':
			g_LogCtrl[g_cLastType] ^= LOG_BIT_ERR;
			break;
		case 'x':
		case 'X':
			fdprintf(fdConsole, "\x1b[2J\x1b[0;0H");
			ioGlobalStdSet(1, fdSTDOUT);
			close(fdNULL);
			return;

		default:
			break;
		}
	}
}

int LogTypePrint(int bit, int type, const char *file, const int line, const char *format,...)
{
    va_list 	ap;
    struct tm 	curTime;

	char		TextBuffer[MAX_LOG_TEXT_LEN] = {0,};
	int			nHeadLen = 0, nBodyLen = 0, nTextLen = 0;
	int			error = 0;

    if(!g_bAllLogs)
    {
		return 0;
	}

    if(g_LogCtrl[type] & bit)
    {
#ifdef NEW_LOG_SYSTEM
		LOG_MSG	msg;

		memset(&msg, 0, sizeof(msg));

		// 현재 시간을 구한다.
		sysRtcGet(&curTime);

		msg.bit 	= (BYTE)bit;
		msg.type	= (BYTE)type;
        sprintf(msg.date, "%02d:%02d:%02d", curTime.tm_hour, curTime.tm_min, curTime.tm_sec);
        strncpy(msg.file, file, 17);
		msg.line	= (USHORT)line;

		va_start(ap, format);
        vsprintf(msg.data.text, format, ap);
        va_end(ap);

		if(msgQSend(g_LogMsgQID, 
					(char*)&msg, 
					sizeof(msg),
					NO_WAIT,
					MSG_PRI_NORMAL) == ERROR)
#else	// NEW_LOG_SYSTEM
		char szBit[4] = {0,};
		char szType[4] = {0,};

		switch(bit)
		{
		case LOG_BIT_ERR:
			strcpy(szBit, "ERR");
			break;
		case LOG_BIT_IND:
			strcpy(szBit, "IND");
			break;
		case LOG_BIT_DBG:
			strcpy(szBit, "DBG");
			break;
		default:
			return 0;
		}

		sysRtcGet(&curTime);

        nHeadLen = sprintf(	TextBuffer, "[%3s][%02d:%02d:%02d][%s:%d] ",
							szBit, curTime.tm_hour, curTime.tm_min, curTime.tm_sec, file, line );

		//semTake(g_LogSemID, WAIT_FOREVER);
        va_start(ap,format);
        nBodyLen = vsprintf( TextBuffer + nHeadLen, format,ap );
        va_end(ap);
		//semGive(g_LogSemID);

		nTextLen = nHeadLen + nBodyLen;

		if(msgQSend(g_LogMsgQID, 
					TextBuffer, 
					(nTextLen < MAX_LOG_TEXT_LEN) ? nTextLen : MAX_LOG_TEXT_LEN,
					NO_WAIT,
					MSG_PRI_NORMAL) == ERROR)
#endif	// NEW_LOG_SYSTEM
		{
#if 0
			error = errnoGet();
			switch(error)
			{
			case S_objLib_OBJ_ID_ERROR:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_ID_ERROR\n");
				break;
			case S_objLib_OBJ_DELETED:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_DELETED\n");
				break;
			case S_objLib_OBJ_UNAVAILABLE:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_UNAVAILABLE\n");
				break;
			case S_objLib_OBJ_TIMEOUT:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_TIMEOUT\n");
				break;
			case S_msgQLib_INVALID_MSG_LENGTH:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_INVALID_MSG_LENGTH\n");
				break;
			case S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL\n");
				break;
			default:
				printf("[LOG] msgQSend Error. errno = UNKNOWN (%d)\n", error);
				break;
			}
#endif
			return 0;
		}
    }

    //return charsNo;
	return nTextLen;
}

int LogHexPrint(int type, unsigned char *pBuffer, unsigned short len, const char *file, const int line, const char *format,...)
{
    int     	charsNo;
    va_list 	ap;
    struct tm 	curTime;

	char		TextBuffer[MAX_LOG_TEXT_LEN] = {0,}, *pHexText = NULL;
	int			nHeadLen = 0, nBodyLen = 0, nHexLen = 0, nTextLen = 0;
	int			error = 0;

    if(!g_bAllLogs)
    {
		return 0;
	}

	if(g_LogCtrl[type] & LOG_BIT_HEX)
	{
#ifdef NEW_LOG_SYSTEM
		LOG_MSG	msg;

		memset(&msg, 0, sizeof(msg));

		// 현재 시간을 구한다.
		sysRtcGet(&curTime);

		msg.bit 	= LOG_BIT_HEX;
		msg.type	= (BYTE)type;
        sprintf(msg.date, "%02d:%02d:%02d", curTime.tm_hour, curTime.tm_min, curTime.tm_sec);
        strncpy(msg.file, file, 17);
		msg.line	= (USHORT)line;

		va_start(ap, format);
        vsprintf(msg.data.hex.text, format, ap);
        va_end(ap);

		msg.data.hex.hexLen = (USHORT)len;
		memcpy( msg.data.hex.hexBuffer, 
				pBuffer, 
				(MAX_LOG_TEXT_LEN - 128) > len ? len : (MAX_LOG_TEXT_LEN - 128) );

		if(msgQSend(g_LogMsgQID, 
					(char*)&msg, 
					sizeof(msg),
					NO_WAIT,
					MSG_PRI_NORMAL) == ERROR)
#else
		if(len == 0 || len > MAX_LOG_TEXT_LEN)
		{
			return 0;
		}

		sysRtcGet(&curTime);

   		nHeadLen = sprintf(	TextBuffer, "[HEX][%02d:%02d:%02d][%s:%d] ",
							curTime.tm_hour, curTime.tm_min, curTime.tm_sec, file, line );

		//semTake(g_LogSemID, WAIT_FOREVER);
       	va_start(ap,format);
   		nBodyLen = vsprintf( TextBuffer + nHeadLen, format, ap );
       	va_end(ap);
		//semGive(g_LogSemID);

		pHexText = TextBuffer + nHeadLen + nBodyLen;
		sprintf(pHexText, 
				"\n------------------------------------\n    |  0  1  2  3  4  5  6  7  8  9 \n====+===============================");
		nHexLen = strlen(pHexText);
	
		for(int i = 0 ; i < len ; i++)
		{
			if(i % 10 == 0)
			{
				sprintf(pHexText + nHexLen, "\n %2d | ", i);
				nHexLen += 7;
			}

			sprintf(pHexText + nHexLen, "%02X ", pBuffer[i]);
			nHexLen += 3;
		}

		sprintf(pHexText + nHexLen, "\n------------------------------------\n");

		nTextLen = strlen(TextBuffer);

		if(msgQSend(g_LogMsgQID, 
					TextBuffer, 
					(nTextLen < MAX_LOG_TEXT_LEN) ? nTextLen : MAX_LOG_TEXT_LEN,
					NO_WAIT,
					MSG_PRI_NORMAL) == ERROR)
#endif	// NEW_LOG_SYSTEM
		{
#if 0
			error = errnoGet();
			switch(error)
			{
			case S_objLib_OBJ_ID_ERROR:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_ID_ERROR\n");
				break;
			case S_objLib_OBJ_DELETED:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_DELETED\n");
				break;
			case S_objLib_OBJ_UNAVAILABLE:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_UNAVAILABLE\n");
				break;
			case S_objLib_OBJ_TIMEOUT:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_TIMEOUT\n");
				break;
			case S_msgQLib_INVALID_MSG_LENGTH:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_INVALID_MSG_LENGTH\n");
				break;
			case S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL\n");
				break;
			default:
				printf("[LOG] msgQSend Error. errno = UNKNOWN (%d)\n", error);
				break;
			}
#endif

			return 0;
		}
	}
	else if(g_LogCtrl[type] & LOG_BIT_IND)
	{
		sysRtcGet(&curTime);

   		nHeadLen = sprintf(	TextBuffer, "[IND][%02d:%02d:%02d][%s:%d] ",
						curTime.tm_hour, curTime.tm_min, curTime.tm_sec, file, line );

		//semTake(g_LogSemID, WAIT_FOREVER);
       	va_start(ap,format);
   		nBodyLen = vsprintf( TextBuffer + nHeadLen, format,ap );
       	va_end(ap);
		//semGive(g_LogSemID);

		nTextLen = nHeadLen + nBodyLen;

		if(msgQSend(g_LogMsgQID, 
					TextBuffer, 
					(nTextLen < MAX_LOG_TEXT_LEN) ? nTextLen : MAX_LOG_TEXT_LEN,
					NO_WAIT,
					MSG_PRI_NORMAL) == ERROR)
		{
#if 0
			error = errnoGet();
			switch(error)
			{
			case S_objLib_OBJ_ID_ERROR:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_ID_ERROR\n");
				break;
			case S_objLib_OBJ_DELETED:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_DELETED\n");
				break;
			case S_objLib_OBJ_UNAVAILABLE:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_UNAVAILABLE\n");
				break;
			case S_objLib_OBJ_TIMEOUT:
				printf("[LOG] msgQSend Error. errno = S_objLib_OBJ_TIMEOUT\n");
				break;
			case S_msgQLib_INVALID_MSG_LENGTH:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_INVALID_MSG_LENGTH\n");
				break;
			case S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL:
				printf("[LOG] msgQSend Error. errno = S_msgQLib_NON_ZERO_TIMEOUT_AT_INT_LEVEL\n");
				break;
			default:
				printf("[LOG] msgQSend Error. errno = UNKNOWN (%d)\n", error);
				break;
			}
#endif

			return 0;
		}
	}

	return nTextLen;
}

PROCESS IOLOG_PrintTask()
{
	int	nfd = 0;
	struct sockaddr_in  clientAddr, serverAddr;

#ifdef NEW_LOG_SYSTEM
	LOG_MSG	msg;
#else
	char	LogBuffer[MAX_LOG_TEXT_LEN] = {0,};
	int		nLogLen = 0;
#endif

	int		error = 0;

#ifdef NEW_LOG_SYSTEM
	bzero((char*)&clientAddr, sizeof(clientAddr));
	clientAddr.sin_family		= AF_INET;
	clientAddr.sin_port			= htons(LOG_UDP_PORT);
	clientAddr.sin_addr.s_addr	= inet_addr(LOG_CLIENT_ADDR);  

	bzero((char*)&serverAddr, sizeof(serverAddr));
	serverAddr.sin_family		= AF_INET;
	serverAddr.sin_port			= htons(LOG_UDP_PORT);
	serverAddr.sin_addr.s_addr	= inet_addr(LOG_SERVER_ADDR); 

	if((nfd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		printf("[LOG] Socket Create Error!!\n");
		return (ERROR);
	}

	if(bind(nfd, (struct sockaddr *)&serverAddr, (int)(sizeof(serverAddr))) == ERROR)
	{
		printf("[LOG] Socket Bind Error!!\n");
		return (ERROR);
	}
#endif

	while(1)
	{		
#ifdef NEW_LOG_SYSTEM
		memset(&msg, 0, sizeof(msg));

		if(msgQReceive(g_LogMsgQID, (char*)&msg, sizeof(msg), WAIT_FOREVER) == ERROR)
#else
		memset(LogBuffer, 0, MAX_LOG_TEXT_LEN);

		if((nLogLen = msgQReceive(g_LogMsgQID, LogBuffer, MAX_LOG_TEXT_LEN, WAIT_FOREVER)) == ERROR)
#endif
		{
			error = errnoGet();
			switch(error)
			{
			case S_objLib_OBJ_ID_ERROR:
				printf("[LOG] msgQReceive Error. errno = S_objLib_OBJ_ID_ERROR\n");
				break;
			case S_objLib_OBJ_DELETED:
				printf("[LOG] msgQReceive Error. errno = S_objLib_OBJ_DELETED\n");
				break;
			case S_objLib_OBJ_UNAVAILABLE:
				printf("[LOG] msgQReceive Error. errno = S_objLib_OBJ_UNAVAILABLE\n");
				break;
			case S_objLib_OBJ_TIMEOUT:
				printf("[LOG] msgQReceive Error. errno = S_objLib_OBJ_TIMEOUT\n");
				break;
			case S_msgQLib_INVALID_MSG_LENGTH:
				printf("[LOG] msgQReceive Error. errno = S_msgQLib_INVALID_MSG_LENGTH\n");
				break;
			default:
				printf("[LOG] msgQReceive Error. errno = UNKNOWN (%d)\n", error);
				break;
			}

			continue;
		}

		if(g_LogMode == LOG_MODE_CONSOLE)
		{
			printf("%s\n", LogBuffer);
		}
		else if(g_LogMode == LOG_MODE_LAN)
		{

		}
	}

	close(nfd);
}

int myLogInit()
{
	g_LogSemID = semBCreate(SEM_Q_FIFO, SEM_FULL);
	if(g_LogSemID == NULL)
	{
		printf("[LOG] semBCreate Error.\n");

		return 0;
	}

	if(g_LogMsgQID == NULL)
	{
		g_LogMsgQID = msgQCreate(MAX_LOG_QUEUE_QTY, MAX_LOG_TEXT_LEN, MSG_Q_FIFO);
	}

	if(g_LogMsgQID == NULL)
	{
		printf("[LOG] msgQCreate Error.\n");
		return 0;
	}

	if(g_LogMsgQID != NULL)
	{
		g_LogTaskID = taskSpawn("IOLOG_PrintTask", TASKPR_LOG, 0, 0x5000, (FUNCPTR)IOLOG_PrintTask, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
}	

