/* -------------------------------------------------------------------------+
|  version.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                              
|                                                                           
|  2. FileName.                                                             
|     version.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     Define Software Version.
|                                                                           
|  4. Project Name.                                                         
|     Bangladesh Project - Tongi - Bhairab Bazar Project.                        
|                                                                           
|  5. Maker.                                                                
|                                                                                                    
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402A and KVME314                               
|      - Target O.S		- VxWorks 5.4                                                         
|                                                                                             
|  7. Development History.                                                    
|                                                                           
|    Ver      	Date      	Producer		Remark                    
|   -------------------------------------------------------------------------------------------   
|	 0.1.0		20130306	Seokju Hong		Written new source codes for BR
|	 0.1.4		20130321	Seokju Hong		Correct the problem that the Block communication timeout is very long. (1 min. over) 
|                                                                           
+------------------------------------------------------------------------- */ 

#ifndef __VERSION_H__
#define __VERSION_H__

// VERSION INFORMATION
#define SW_VER_MAJ	00
#define SW_VER_MIN	01
#define	SW_VER_DEV	07
#define SW_VER_YY	13
#define SW_VER_MM	06
#define	SW_VER_DD	17

#define SW_VERSION_SIZE	6
#endif
