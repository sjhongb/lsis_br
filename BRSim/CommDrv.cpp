/* -------------------------------------------------------------------------+
|  CommDrv.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommDrv.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver.
|                                                                           
|  4. Project Name.                                                         
|     Tongi,Laksam,13station by BR.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- LS402A and LS314                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
/* vxWorks library */
#include "tyLib.h"
#include "taskLib.h"
#include "stdio.h"
//#include "fioLib.h"
// #include "msgQLib.h"

/* standard c library */
#include "string.h"

/* user c library */
#include "is.h"			/* 308 board driver header */

#include "LogTask.h"
#include "CommDrv.h"

/* ------------------------------------------------------------------------- */
struct      fd_set  readFds_Io    , tmpFds_Io;
struct      fd_set  readFds_LCC1  , tmpFds_LCC1;		/* for IPM 1 in the MCCR */
struct      fd_set  readFds_LCC2  , tmpFds_LCC2;		/* for IPM 2 in the MCCR */
struct      fd_set  readFds_MMCR  , tmpFds_MMCR;		/* for I/O simulation */
struct      fd_set  readFds_SIM   , tmpFds_SIM;			/* for I/O simulation */
struct      fd_set  readFds_IF    , tmpFds_IF;			/* for other CBI */
struct      fd_set  readFds_Modem[4], tmpFds_Modem[4];	/* for modem 1-4 */
struct      fd_set  readFds_Port1    , tmpFds_Port1;		
struct      fd_set  readFds_Port2    , tmpFds_Port2;		

struct      timeval selTimeOut_Io;
struct      timeval selTimeOut_LCC1;
struct      timeval selTimeOut_LCC2;
struct      timeval selTimeOut_MMCR;
struct      timeval selTimeOut_SIM;
struct      timeval selTimeOut_IF;
struct      timeval selTimeOut_Modem[4];
struct      timeval selTimeOut_Port1;
struct      timeval selTimeOut_Port2;

/* ------------------------------------------------------------------------- */
STATUS InitCommSerial();					/* Initialize to serial ports */
void setIoCfg_(int nfd,int item,int arg);	/* Set configure of open port */

/* ------------------------------------------------------------------------- */
int _bComOpened = 0;

/* file descriptor of serial ports */
int _SerialPort[ PORTNUM_MAX ] = {	
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR,
	ERROR
};


/* ------------------------------------------------------------------------- */
#define COM_OPTION_MODEM 1

int _SerialPortOption[ PORTNUM_MAX ] = { 0, };

/* ----------------------------------------------------------------------- */
#define IOCTRL_RTS_ON			0xf30
#define IOCTRL_RTS_OFF			0xf31
#define IOCTRL_CTS_STATUS		0xf34
#define IOCTRL_308TXQ_STATUS	0x048

/* ----------------------------------------------------------------------- */
void SetRTS( int nfd )	/* RTS signal on */
{
	ioctl( nfd, IOCTRL_RTS_ON, 0 );	// RTS ON : 0xf30
}

void ClearRTS( int nfd )	/* RTS signal off */
{
	ioctl( nfd, IOCTRL_RTS_OFF, 0 );	// RTS OFF : 0xf31
}

int GetCTS( int nfd )	// CTS status.
{
	int result;

	result = ioctl( nfd, IOCTRL_CTS_STATUS, 0 );	// READ CTS : 0x34
	result = (result & 0x20) ? 1 : 0;

	return result;	// 1/cts ON, 0/cts off.
}

int IsEmpty308TXQ( int nfd )	/* Empty status of 308 Board TX buffer. */
{
	int result = 1;

	result = ioctl( nfd, IOCTRL_308TXQ_STATUS, 0 );	// READ 308TX BUFFER : 0x048
	result = (result == 0) ? 1 : 0;

	return result;		// 1/empty, 0/exist.
}

int WaitEmpty308TXQ( int nfd, int nTicks )	/* Empty status of 308 Board TX buffer. */
{
	int n, result = -1;

	n = 0;
	do { 
		result = ioctl( nfd, IOCTRL_308TXQ_STATUS, 0 );	// READ 308TX BUFFER : 0x048
		if ( result != -1 ) 
			break;				// clear 308 TX que buffer.
		else 
			taskDelay(1);		// exist buffer data.
		n++;
	} while(n < nTicks);

	result = (result != -1) ? 1 : 0;
	return result;		// 1/clear buffer, 0/exist.
}

/* ------------------------------------------------------------------------- */
CommDriver::CommDriver( int nBuffSize ) 
{
	m_bOpenFailed = 1;
	m_nPortNo = -1;
	m_nRxQID = NULL;
	m_nTxQID = NULL;
	
	m_bUsedCtrlRTS = 0;	/* set Modem Non-Used */
	m_bUsedIOFlush = 0;	/* set IO flush Non-Used */
	m_nRxPtr = 0;    
	m_nTxPtr = 0;    
	m_nBufferSize = nBuffSize;

	m_pRxBuffer = new unsigned char [nBuffSize];
	m_pTxBuffer = new unsigned char [nBuffSize];

	if ( m_pRxBuffer ) {
		memset( &m_pRxBuffer[0], 0x00, m_nBufferSize );
	}
	if ( m_pTxBuffer ) {
		memset( &m_pTxBuffer[0], 0x00, m_nBufferSize );
	}
}

CommDriver::~CommDriver() 
{
}

void CommDriver::ClearRX()
{
	ioctl( m_nfd, FIORFLUSH, 0 );	// clear to io read buffer of 308 board driver.
}

void CommDriver::ClearTX()
{
	ioctl( m_nfd, FIOWFLUSH, 0 );	// clear to io write buffer of 308 board driver.
}

short CommDriver::Initialize( int nPort, int bUsed, int bUsedMsgQ/* =1 */ )
{
	m_nPortNo = nPort;
	m_bUsedCtrlRTS = bUsed & 1;	/* set Modem Used flag */
	m_bUsedIOFlush = bUsed & 2;	/* set IO flush Used flag */
	m_nRxPtr = 0;    
	m_nTxPtr = 0;    

	if ( !_bComOpened ) 
    {
		if ( InitCommSerial() == OK ) {
			LogInd(LOG_TYPE_GEN, "commOpenOK(%d)", nPort );
		}
		else {
			LogErr(LOG_TYPE_GEN, "commOpenFAIL(%d)", nPort );
		}
	}
	
	//	m_nfd = GetOpenSerialPort( nPort );
	if((nPort < 1) || (nPort >= PORTNUM_MAX))
	{
		m_nfd = ERROR;
	}
	else 
	{
		m_nfd = _SerialPort[nPort];
	}

	if(bUsedMsgQ & 0x01) 
    {	// first, 1th
   		m_nRxQID = msgQCreate( (m_nBufferSize*2), 1, MSG_Q_FIFO );
  	 	if ( m_nRxQID == NULL ){
			LogErr(LOG_TYPE_GEN, "RxQID ERROR...");
   	    	return(ERROR);
		}
	}
	if(bUsedMsgQ & 0x02) 
    {	// second, 2nd
		m_nTxQID = msgQCreate( (m_nBufferSize*2), 1, MSG_Q_FIFO );
		if ( m_nTxQID == NULL ) {
			LogErr(LOG_TYPE_GEN, "TxQID ERROR...");
   		   	return(ERROR);
		}
	}

	if(m_nfd != ERROR) 
    { 
		m_bOpenFailed = 0;
		switch(m_nPortNo)
		{
		case PORTNUM_BLOCK1:
		case PORTNUM_BLOCK2:
		case PORTNUM_BLOCK3:
		case PORTNUM_BLOCK4:
			if(m_bUsedCtrlRTS) 
			{
				//(void)ioctl (m_nfd, 0xF37, TRUE); 
			}
			break;
		default:
			break;
		}
	}
	
	return OK;
}

/* ------------------------------------------------------------------------- */
STATUS uioctl(
    int fd,       /* file descriptor */
    int function, /* function code */
    int arg       /* arbitrary argument */
   	) 
{
	int nStatus;
	int nCount = 0;

	do {
		nStatus = ioctl( fd, function, arg );
		if ( nStatus == ERROR ) {
			nCount++;
			if ( nCount > 2 )	return	ERROR;
			taskDelay(1);
		}
	} while(nStatus == ERROR);

	return nStatus;
}

/* ------------------------------------------------------------------------- */
short CommDriver::WriteBlock( char *pBuffer, short length )
{
	STATUS nStatus;
	short nSendByte = ERROR;
	short nDelay = 0;
	if ( m_nfd != ERROR ) 
	{
		if ( m_bUsedCtrlRTS ) 
		{
#if 0
			SetRTS(m_nfd);

	        for(nDelay=0; nDelay<100; nDelay++) // 500ms loop
			{
				nStatus = GetCTS(m_nfd);
				if(nStatus != 0) 
				{
					LogDbg(LOG_TYPE_BLOCK, "nStatus = %d\n", nStatus);
					break;
				}		
				taskDelay(1);
			}	
#endif

#if 0
			if(ioctl( m_nfd, IOCTRL_CTS_STATUS, 0 ) == 1){
				uioctl( m_nfd, IOCTRL_RTS_OFF, 0 );
				taskDelay(10);
				LogErr(LOG_TYPE_GEN, "IOCTL_CTS_STATUS ERROR!!");
				return(ERROR);
			}

	        nStatus = ioctl( m_nfd, IOCTRL_CTS_STATUS, 0 );			// READ CTS : 0x107

			nStatus = uioctl( m_nfd, IOCTRL_RTS_ON, 0 );	// RTS ON : 0x105
			if( nStatus == ERROR ){
				LogErr(LOG_TYPE_GEN, "IOCTL_RTS_ON ERROR!!!");			// RTS ON : 0x105
				return(ERROR);
			}

			taskDelay(80);	// 33ms

/*
	        for(nDelay=0; nDelay<100; nDelay++) {	// 500ms loop
		        nStatus = ioctl( m_nfd, IOCTRL_CTS_STATUS, 0 );	// READ CTS : 0x107
				if (nStatus != 0) break;
				taskDelay(1);	
			}
*/

		    nStatus = ioctl( m_nfd, IOCTRL_CTS_STATUS, 0 );	// READ CTS : 0x107
        	if (nStatus == 0) {
				nStatus = uioctl( m_nfd, IOCTRL_RTS_OFF, 0 );	// RTS off : 0x106
//				taskDelay(60);	// 100ms delay.
				taskDelay(10);	// 100ms delay.
				LogErr(LOG_TYPE_GEN, "GET-ISIO CTS not response fd= [%d]!!!",m_nfd);
				return(ERROR);
 			}
        	if (nStatus == ERROR) {
				nStatus = uioctl( m_nfd, IOCTRL_RTS_OFF, 0 );	// RTS off : 0x106
//				taskDelay(60);	// 100ms delay.
				LogErr(LOG_TYPE_GEN, "GET CTS ERROR!!!");
        		return(ERROR);
			}
#endif

            nSendByte = write( m_nfd, pBuffer, length );

#if 0
			ClearRTS(m_nfd);

	        for(nDelay=0; nDelay<100; nDelay++) // 500ms loop
			{
				nStatus = GetCTS(m_nfd);
				if(nStatus == 0) 
				{
					LogDbg(LOG_TYPE_BLOCK, "nStatus = %d\n", nStatus);
					break;
				}		
				taskDelay(1);
			}	
#endif
#if 0
			int nWait = 0;
			do {
				taskDelay(40);	// 50ms delay.
				if ( IsEmpty308TXQ( m_nfd ) ) break;
			} while ( nWait++ < 15 );

			nStatus = uioctl( m_nfd, IOCTRL_RTS_OFF, 0 );	// RTS off : 0x106
#endif

/*
			nStatus = uioctl( m_nfd, IOCTRL_RTS_OFF, 0 );	// RTS off : 0x106
			if( nStatus == ERROR ){
				taskDelay(60);	// 100ms delay.
				return(ERROR);
			}
			return nSendByte;
*/
		}		
		else 
		{
			if ( m_bUsedIOFlush ) 
			{
			    nSendByte = 0;
			    uioctl( m_nfd, FIONWRITE, (int)&nSendByte );
			    if ( nSendByte != 0 ) {
			        uioctl( m_nfd, FIOWFLUSH, 0 );
				}
				uioctl( m_nfd, FIOWFLUSH, 0 );
			}

			if (pBuffer[2] == 0xED)
			{
				taskDelay(10);
			}

   		 	nSendByte = write( m_nfd, pBuffer, length );

			//printf("nSendByte[%d] Length[%d] [%x][%x][%x][%x]", nSendByte, length, pBuffer[0],pBuffer[1],pBuffer[2],pBuffer[3]); 
		}
	}
	else {
		LogErr(LOG_TYPE_GEN, "BAD DESCRIPTOR");
	}
	return nSendByte;
}

int CommDriver::ReadBlock( char *pBuffer, int nReadSize, int tmBreak )
{
	int nBytes = 0;
	int result;
	do {
		result = msgQReceive( m_nRxQID, &pBuffer[nBytes], 1, NO_WAIT );
		if ( result != ERROR ) nBytes++;

	} while ( result != ERROR && nBytes < nReadSize );
	return nBytes;
}

/* ------------------------------------------------------------------------- */
void setIoCfg_(int nfd,int item,int arg)
{
	ISIO_IO_CONFIG ioconfig;
	typedef enum {        
		HANDSHAKE   = 1,
		CHARlENGTH  = 2,
		STOPBITS    = 3,
		PARITY      = 4,
		BAUD        = 5
	 } ITEM;


	ioctl(nfd, ISIO_F_GETCFG, (int)&ioconfig);

	switch(item){
		case HANDSHAKE:
			ioconfig.handshake = arg;
			ioctl(nfd, ISIO_F_SETCFG, (int)&ioconfig);
			break;
		case CHARlENGTH:
			ioconfig.charLength = arg;
			ioctl(nfd, ISIO_F_SETCFG, (int)&ioconfig);
			break;
		case STOPBITS:
			ioconfig.stopBits = arg;
			ioctl(nfd, ISIO_F_SETCFG, (int)&ioconfig);
			break;
		case PARITY:
			ioconfig.parity = arg;
			ioctl(nfd, ISIO_F_SETCFG, (int)&ioconfig);
			break;
		case BAUD:
			ioconfig.baud = arg;
			ioctl(nfd, ISIO_F_SETCFG, (int)&ioconfig);
			break;
	}
}

/* ------------------------------------------------------------------------- */
STATUS InitCommSerial()
{
	char buf[256];
	int  i, chan;

#if 0	// reserved for future.
	for(i = PORTNUM_EXT1 ; i <= PORTNUM_EXT14 ; i++)	// PORTNUM_EXT1=8, PORTNUM_EXT14=21	// LKV_314
	{
		if(_SerialPort[i] == ERROR) 
		{
			chan = i - PORTNUM_EXT1;
			sprintf(buf, "/isio/%d", chan);	// chan 0 1 2 3 4 5 6 7 8 9 (0~9 BRD = 9600)

			_SerialPort[i] = open(buf, O_RDWR, 0);
			if( _SerialPort[i] == ERROR )
			{
				LogErr(LOG_TYPE_GEN, "Port OPEN Error! %d", i );
				return( ERROR );
			}

			switch(i)
			{
			case PORTNUM_EXT1:
			case PORTNUM_EXT2:
			case PORTNUM_EXT3:
			case PORTNUM_EXT4:
			case PORTNUM_EXT5:
			case PORTNUM_EXT6:
			case PORTNUM_EXT7:
			case PORTNUM_EXT8:
			case PORTNUM_EXT9:
			case PORTNUM_EXT10:
			case PORTNUM_EXT11:
			case PORTNUM_EXT12:
			case PORTNUM_EXT13:
			case PORTNUM_EXT14:
				LogInd(LOG_TYPE_GEN, "Port[%d] = %d (%s) - %dbps", i, _SerialPort[i], buf, 9600);
				ioctl(_SerialPort[i], FIOBAUDRATE, 9600);		// set baud rate of KVME314 Board's P0-P9 port.
				taskDelay(1);
				setIoCfg_(_SerialPort[i], 1, 2);				// CTC - not RTS control //
				break;
			default:
				break;
			}

			taskDelay( 10 );
		}

		ioctl(_SerialPort[i], FIOFLUSH, 0);		
	}

	taskDelay( 10 );
#endif

	for(i = PORTNUM_CC1 ; i <= PORTNUM_BLOCK4 ; i++ ) 
	{
		// PORTNUM_CC1 = 1, PORTNUM_BLOCK4 = 7	//LKV_402A	
		if(_SerialPort[i] == ERROR) 
		{
			switch(i)
			{
			case PORTNUM_CC1:
			case PORTNUM_CC2:
				chan = i;
				sprintf(buf, "/ScctyCo/%d", chan);	// CC(1), CC(2)
				break;

			case PORTNUM_MC:
			case PORTNUM_BLOCK1:
			case PORTNUM_BLOCK2:
			case PORTNUM_BLOCK3:
				chan = i - 1;
				sprintf(buf, "/tyCo/%d", chan);	// MC, BLOCK(1), BLOCK(2), BLOCK(3)
				break;

			case PORTNUM_BLOCK4:
				sprintf(buf, "/tyCo/0");	// BLOCK(4)
				break;
			default:
				return(ERROR);
			}
			
			_SerialPort[i] = open( buf, O_RDWR , 0 );
			if(_SerialPort[i] == ERROR)
			{
				LogErr(LOG_TYPE_GEN, "Port OPEN Error! %d", i);
				return(ERROR);
			}

			switch(i)
			{
			case PORTNUM_CC1:
			case PORTNUM_CC2:
			case PORTNUM_MC:
				LogInd(LOG_TYPE_GEN, "Port[%d] = %d (%s) - 38400bps", i, _SerialPort[i], buf);
				ioctl(_SerialPort[i], FIOBAUDRATE, 38400);	// set baud rate of LKV402A CPU's P0 ~ P2 port.
				break;
			case PORTNUM_BLOCK1:
			case PORTNUM_BLOCK2:
			case PORTNUM_BLOCK3:
			case PORTNUM_BLOCK4:
				LogInd(LOG_TYPE_GEN, "Port[%d] = %d (%s) - 2400", i, _SerialPort[i], buf);
				ioctl(_SerialPort[i], FIOBAUDRATE, 2400);	// set baud rate of LKV402A CPU's P3 ~ P6 port.
				taskDelay(1);
				break;
			default:
				break;
			}

			taskDelay( 10 );
		}
		ioctl(_SerialPort[i], FIOFLUSH, 0);		
	}

	_bComOpened = 1;
	return( OK );
}

/* ------------------------------------------------------------------------- */
//CRC CCITT
unsigned short CRCtable[256] = {
	0x0000,	0x1189,	0x2312,	0x329b,	0x4624,	0x57ad,	0x6536,	0x74bf,
	0x8c48,	0x9dc1,	0xaf5a,	0xbed3,	0xca6c,	0xdbe5,	0xe97e,	0xf8f7,
	0x1081,	0x0108,	0x3393,	0x221a,	0x56a5,	0x472c,	0x75b7,	0x643e,
	0x9cc9,	0x8d40,	0xbfdb,	0xae52,	0xdaed,	0xcb64,	0xf9ff,	0xe876,
	0x2102,	0x308b,	0x0210,	0x1399,	0x6726,	0x76af,	0x4434,	0x55bd,
	0xad4a,	0xbcc3,	0x8e58,	0x9fd1,	0xeb6e,	0xfae7,	0xc87c,	0xd9f5,
	0x3183,	0x200a,	0x1291,	0x0318,	0x77a7,	0x662e,	0x54b5,	0x453c,
	0xbdcb,	0xac42,	0x9ed9,	0x8f50,	0xfbef,	0xea66,	0xd8fd,	0xc974,
	0x4204,	0x538d,	0x6116,	0x709f,	0x0420,	0x15a9,	0x2732,	0x36bb,
	0xce4c,	0xdfc5,	0xed5e,	0xfcd7,	0x8868,	0x99e1,	0xab7a,	0xbaf3,
	0x5285,	0x430c,	0x7197,	0x601e,	0x14a1,	0x0528,	0x37b3,	0x263a,
	0xdecd,	0xcf44,	0xfddf,	0xec56,	0x98e9,	0x8960,	0xbbfb,	0xaa72,
	0x6306,	0x728f,	0x4014,	0x519d,	0x2522,	0x34ab,	0x0630,	0x17b9,
	0xef4e,	0xfec7,	0xcc5c,	0xddd5,	0xa96a,	0xb8e3,	0x8a78,	0x9bf1,
	0x7387,	0x620e,	0x5095,	0x411c,	0x35a3,	0x242a,	0x16b1,	0x0738,
	0xffcf,	0xee46,	0xdcdd,	0xcd54,	0xb9eb,	0xa862,	0x9af9,	0x8b70,
	0x8408,	0x9581,	0xa71a,	0xb693,	0xc22c,	0xd3a5,	0xe13e,	0xf0b7,
	0x0840,	0x19c9,	0x2b52,	0x3adb,	0x4e64,	0x5fed,	0x6d76,	0x7cff,
	0x9489,	0x8500,	0xb79b,	0xa612,	0xd2ad,	0xc324,	0xf1bf,	0xe036,
	0x18c1,	0x0948,	0x3bd3,	0x2a5a,	0x5ee5,	0x4f6c,	0x7df7,	0x6c7e,
	0xa50a,	0xb483,	0x8618,	0x9791,	0xe32e,	0xf2a7,	0xc03c,	0xd1b5,
	0x2942,	0x38cb,	0x0a50,	0x1bd9,	0x6f66,	0x7eef,	0x4c74,	0x5dfd,
	0xb58b,	0xa402,	0x9699,	0x8710,	0xf3af,	0xe226,	0xd0bd,	0xc134,
	0x39c3,	0x284a,	0x1ad1,	0x0b58,	0x7fe7,	0x6e6e,	0x5cf5,	0x4d7c,
	0xc60c,	0xd785,	0xe51e,	0xf497,	0x8028,	0x91a1,	0xa33a,	0xb2b3,
	0x4a44,	0x5bcd,	0x6956,	0x78df,	0x0c60,	0x1de9,	0x2f72,	0x3efb,
	0xd68d,	0xc704,	0xf59f,	0xe416,	0x90a9,	0x8120,	0xb3bb,	0xa232,
	0x5ac5,	0x4b4c,	0x79d7,	0x685e,	0x1ce1,	0x0d68,	0x3ff3,	0x2e7a,
	0xe70e,	0xf687,	0xc41c,	0xd595,	0xa12a,	0xb0a3,	0x8238,	0x93b1,
	0x6b46,	0x7acf,	0x4854,	0x59dd,	0x2d62,	0x3ceb,	0x0e70,	0x1ff9,
	0xf78f,	0xe606,	0xd49d,	0xc514,	0xb1ab,	0xa022,	0x92b9,	0x8330,
	0x7bc7,	0x6a4e,	0x58d5,	0x495c,	0x3de3,	0x2c6a,	0x1ef1,	0x0f78
};
/* ------------------------------------------------------------------------- */
