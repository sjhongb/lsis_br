/* ------------------------------------------------------------------ */
// MainTask.cpp
//KUL, SYT station are not included. - 3 changed at MainTask.cpp, CommDrv.cpp
//
/* ------------------------------------------------------------------ */

// Include SYSTEM header.
#include "msgQLib.h"
#include "wdLib.h"
#include "tickLib.h"
#include "taskLib.h"
#include "kernelLib.h"
#include "sysLib.h"
#include "timers.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// Include BSP header
#include "kvme402a.h"

// Include user header
#include "projdef.h"
#include "LogTask.h"
#include "brdctrl.h"
#include "commif.h"
#include "MainTask.h"

/* ------------------------------------------------------------------ */
#undef PROCDEB

/* ------------------------------------------------------------------ */
typedef unsigned short WORD;

/* ------------------------------------------------------------------ */
#define CPU_ROTARYSW_ADDR	0xF7000000		// rotary switch address.
#define CPU_DIPSW_ADDR		0xF7000002 		// dip switch adderess.

#define FAILLEDOFF		((unsigned char *)0xF5000000)
#define FAILLEDON		((unsigned char *)0xF9000000)

unsigned char app_tlbit_swap[256] = {
0x00,0x80,0x40,0xc0,0x20,0xa0,0x60,0xe0,
0x10,0x90,0x50,0xd0,0x30,0xb0,0x70,0xf0,
0x08,0x88,0x48,0xc8,0x28,0xa8,0x68,0xe8,
0x18,0x98,0x58,0xd8,0x38,0xb8,0x78,0xf8,
0x04,0x84,0x44,0xc4,0x24,0xa4,0x64,0xe4,
0x14,0x94,0x54,0xd4,0x34,0xb4,0x74,0xf4,
0x0c,0x8c,0x4c,0xcc,0x2c,0xac,0x6c,0xec,
0x1c,0x9c,0x5c,0xdc,0x3c,0xbc,0x7c,0xfc,
0x02,0x82,0x42,0xc2,0x22,0xa2,0x62,0xe2,
0x12,0x92,0x52,0xd2,0x32,0xb2,0x72,0xf2,
0x0a,0x8a,0x4a,0xca,0x2a,0xaa,0x6a,0xea,
0x1a,0x9a,0x5a,0xda,0x3a,0xba,0x7a,0xfa,
0x06,0x86,0x46,0xc6,0x26,0xa6,0x66,0xe6,
0x16,0x96,0x56,0xd6,0x36,0xb6,0x76,0xf6,
0x0e,0x8e,0x4e,0xce,0x2e,0xae,0x6e,0xee,
0x1e,0x9e,0x5e,0xde,0x3e,0xbe,0x7e,0xfe,
0x01,0x81,0x41,0xc1,0x21,0xa1,0x61,0xe1,
0x11,0x91,0x51,0xd1,0x31,0xb1,0x71,0xf1,
0x09,0x89,0x49,0xc9,0x29,0xa9,0x69,0xe9,
0x19,0x99,0x59,0xd9,0x39,0xb9,0x79,0xf9,
0x05,0x85,0x45,0xc5,0x25,0xa5,0x65,0xe5,
0x15,0x95,0x55,0xd5,0x35,0xb5,0x75,0xf5,
0x0d,0x8d,0x4d,0xcd,0x2d,0xad,0x6d,0xed,
0x1d,0x9d,0x5d,0xdd,0x3d,0xbd,0x7d,0xfd,
0x03,0x83,0x43,0xc3,0x23,0xa3,0x63,0xe3,
0x13,0x93,0x53,0xd3,0x33,0xb3,0x73,0xf3,
0x0b,0x8b,0x4b,0xcb,0x2b,0xab,0x6b,0xeb,
0x1b,0x9b,0x5b,0xdb,0x3b,0xbb,0x7b,0xfb,
0x07,0x87,0x47,0xc7,0x27,0xa7,0x67,0xe7,
0x17,0x97,0x57,0xd7,0x37,0xb7,0x77,0xf7,
0x0f,0x8f,0x4f,0xcf,0x2f,0xaf,0x6f,0xef,
0x1f,0x9f,0x5f,0xdf,0x3f,0xbf,0x7f,0xff};


/* ------------------------------------------------------------------ */

/* ------------------------------------------------------------------ */
extern "C" void ifm();
extern "C" STATUS ledCon(char led);
extern "C" STATUS EnetAttach(int unit, char *ipBuf, int netmask);

/* ------------------------------------------------------------------ */
// setting to interlocking table in the flash ROM address.

//2003.5.20unsigned char *_cmosram = (unsigned char *)0xF1000000;		// SRAM //
unsigned char *_cmosram = (unsigned char *)0xF2000300;		// NV RAM //


unsigned char _pIOMapTable[0x900] = {0,};
/* ------------------------------------------------------------------ */
unsigned char _pOutBuffer[256];	// 256 byte (2048 IO port = 256 * 8) //
unsigned char _pInBuffer[256];	// 256 byte (2048 IO port = 256 * 8) //
char  g_nSysNo = 0;				// input data is inversed.

char  g_nBlockType	= 0;		// Tokenless Block Interface Type from CPU Rotary Switch

char  g_Active;
int   g_nOldSysRun = 0;

BYTE  g_ndipSwitch3=0;		// IOSIM DipSwitch

BYTE  g_StationClosed=0;	// This has to write in ROM.
BYTE  g_dnBuffer[16]; // B2 --> B3 when station closed.
BYTE  g_upBuffer[16]; // B4 --> B1 when station closed.
BYTE  g_midBuffer[16];

BYTE  g_nCheckTime=0;
BYTE  g_CardNo[20]={0};

/* ----------------------------------------------------------------------- */
int  g_nMCCRTimeTicked = 0;		/* time tick for MCCR */
int  g_nMMCRTimeTicked = 0;		/* time tick for MMCR */
int	 g_nMainRunTimeTicked = 1;	/* time tick for main run function */

/* ----------------------------------------------------------------------- */
WDOG_ID		g_wid_ComIF;

/* ------------------------------------------------------------------ */
MainTask 	Eis;

/* ------------------------------------------------------------------ */
char _msgBuffer[128];

/* 
 * Function Defines
 */
// ram control functions.
int RamBackup();
int RamReload();
int CheckPrgCode();

void tScan();
int  rClearComIFTimeTick();

/* ------------------------------------------------------------------ */

/*
;======================================================================
; TABLE DEFINE
;======================================================================

m_TableBase 	        = $00   ;00 0100
m_pInfoMem              = $01   ;00 4800
m_pInfoGeneral 	        = $49   ;00 0200
m_pRouteAddTable        = $4B   ;00 0400
m_pRouteCrcTable        = $4F   ;00 0400
m_pRouteSizeTable       = $53   ;00 0200
m_pSwitchOnTrackTable 	= $55   ;00 0100
m_pSignalInfoTable      = $56   ;00 1000
m_pTrackInfoTable       = $66   ;00 0100            ; from $80, TNI Ptr
m_pCommonTrack 	        = $67   ;00 0100
m_pIoAssTable           = $68   ;00 1000
m_pDtsAssTable          = $78   ;00 0800


m_VMEM 	                = $00; 0300	1024 BYTE -> 768 byte

m_pCtcCmd  = $2F0

m_pTrainID              = $03; 0100
m_pSignalTimer 	        = $04; 0100
m_pTrackTimer 	        = $05; 0100                 ; from $80, DTS Out Area
m_pSwitchTimer 	        = $06; 0100                 ; from $80, BackRouteSignal
m_pSwitchRouteTrack     = $07; 0100 [128 * 2];
m_pSwitchOverTrack1     = $08; 0100 [128];
m_pSwitchOverTrack2     = $09; 0100 [128];
m_pTrackPrev            = $0A; 0100
m_pLastTrack            = $0B; 0100
m_pRouteQue             = $0C; 0200,    WORD X 100
m_pOutBuffer 	        = $0E; 0100
m_pInBuffer 	        = $0F; 0100

*/

/*
	VMEM Memory Map

	m_nInBufferEnd = m_nScrEnd + nGapSize;
	m_nRamSize = pEmu->m_nRamSize;
	m_nRamEnd = m_nInBufferEnd + m_nRamSize;

	<<< VMEM >>>														Exchange Buffer ( Comm_IF )

	VMEM		+--------------------+
				|                    |
				|                    |
				|                    |
	48(0x30)	+---Object Start-----+
				|                    |
				|                    |
				|   Objects          |
				|                    |
				|                    |
				|                    |
				|                    |
				|                    |
  ScrEndPos(SEP)+-EMU->m_pMsgBuffer--+									=m_nScrEnd
				|                    |
				|   MsgBuffer        |
				|                    |
	SEP+0x38    +---g_nCtcCmdQuePtr--+ 
				|                    |
				|   CtcCmdQue        |
				|                    |									]
	SEP+0x50	+--EMU->m_nTNIBase---+									] nGapSize = 0xB0 + g_nSIZE_INBUFFER
				|                    |									]
				|   TNI              |
				|                    |
	SEP+0xB0	+--g_nPTR_INBUFFER----+	<<< copy for TxVMEM
				|                    |	]
				|   IN_BUFFER        |	] g_nSIZE_INBUFFER
				|                    |	]
	SEP+0x120	+--PTR_EMUBUFFER-----+  <<< copy EMU->RouteQue			=m_nInBufferEnd
g_nSIZE_INBUFFER|                    |  ]								]
				|   EMU Internal     |  ] m_pMyEngine->m_nRamSize		] m_nRamSize
				|                    |  ]								]
				+--------------------+									=m_nRamEnd

*/

//;=====================================================================
//; Program start
//;=====================================================================

int WaitOprLow() 
{
	return 0;
}


void WaitOnSyncForMainRun() 
{
	int n;
	
	for(n=0; n<MAX_WAITTICK_MAINRUN; n++) {
		if ( g_nMainRunTimeTicked ) break;
		taskDelay(1);
	}
}

/* ----------------------------------------------------------------------- */
int rSetMainRunTimeTick() {
	g_nMainRunTimeTicked = 1;
	return 0;
}

int rClearComIFTimeTick() {
	return 0;
}

/* ----------------------------------------------------------------------- */
MainTask::MainTask()
{
}

MainTask::~MainTask()
{
}


int MainTask::SystemInit( int bRamRoad ) 
{
// Initialize class member.
	m_bIllegalFault = 0;
    m_FaultCount = 0; 
    m_pMsgLastCyc = 0;
	m_bErrEvg = 0;
	m_bOtherErrEvg = 0;
	
	return 0;
}

//;=====================================================================
//; Sys 1,2 Setup
//;=====================================================================
//BYTE bActiveOn1		:1;	// 0  input singal of CBI 1 running status. 
//BYTE bActiveOn2		:1;	// 1  input signal of CBI 2 running status.
//BYTE bVPOR1			:1;	// 2  input signal of system 1 VPOR.
//BYTE bVPOR2			:1;	// 3  input singal of system 2 VPOR.


/****************************************************************************/
int RamBackup() 
{
	return 0;
}

int RamReload() 
{
	return 1;
}

/****************************************************************************/
/****************************************************************************/

int MainTask::Run() 
{
    WDOG_ID wid_MainRun;

	g_ndipSwitch3 = *(uchar *)CPU_DIPSW_ADDR;

    wid_MainRun = wdCreate();
	g_wid_ComIF = wdCreate();

	memset( _pInBuffer, 0, sizeof(_pInBuffer) );
	memset( _pOutBuffer, 0, sizeof(_pOutBuffer) );
	
	ComIF.Initialize();

// initialize IO engin
	EIS_IO.SetBoardInfo(/* pInterlockTable */);

	taskSpawn("tScan", TASKPR_IO/*5*/,0,6000,(FUNCPTR)tScan,0,0,0,0,0,0,0,0,0,0);

	g_nMainRunTimeTicked = 0;
    wdStart(wid_MainRun, 30, (FUNCPTR)rSetMainRunTimeTick, 0);
	WaitOnSyncForMainRun();

	EIS_IO.InputAll();

    SystemInit( 0 );

	ledCon(DIAG0_ON);			// active lamp ON.
	ledCon(DIAG1_ON);			// active lamp ON.

	g_nMainRunTimeTicked = 1;

	while (1) 
	{
		unsigned char temp = 0;

		WaitOnSyncForMainRun();
		
		g_nMainRunTimeTicked = 0;
	    wdStart (wid_MainRun, 250, (FUNCPTR)rSetMainRunTimeTick, 0);	//60/3	// 20->200

		EIS_IO.InputAll();		// All read Input card.

		EIS_IO.OutputAll();
	} // end of while

	wdCancel( wid_MainRun );
	wdCancel( g_wid_ComIF );

	return 0;
}	// end of Run()


/* ------------------------------------------------------------------------ */

void tMainTask() 
{
	//printf( "\n\n\n LGEIS520-III Version %s, create at %s %s\n\n", _pEmuVersion, _pEmuDate, _pEmuTime ); 
	Eis.Run();
}

void ifm() 
{
	// LOG Task Initialize & Start
	myLogInit();

	sysClkRateSet( MAX_TASKTICK_BASE );
	
	kernelTimeSlice( 3 );	// priority 가 같은 task에 대한 task switch 하기전의 task 수행시간 설정.
	
    taskSpawn("tMainTask", TASKPR_MAIN,0,0x8000,(FUNCPTR)tMainTask,0,0,0,0,0,0,0,0,0,0);
}

void fdom(BYTE board, BYTE channel)
{
	if(board < 1 || board > 8)
	{
		printf("Invalid Board Number. (1 ~ 8)\n");
		return;
	}

	if(channel < 1 || channel > 32)
	{
		printf("Invalid Channel Number. (1 ~ 32)\n");
		return;
	}

	_pOutBuffer[(board - 1) * 4 + (channel - 1) / 8] ^= 1 << (channel - 1) % 8;
}

void fdomall()
{
	int i;
	for(i = 0 ; i < FDOM_MAX_QTY ; i++)
	{
		_pOutBuffer[i * 4] = 0xFF;
		_pOutBuffer[i * 4 + 1] = 0xFF;
		_pOutBuffer[i * 4 + 2] = 0xFF;
		_pOutBuffer[i * 4 + 3] = 0xFF;
	}
}

