/* isio.c - HanA SYS68K Intelligent SIO Driver */


static char *copyright = "Copyright 1995, HanA SYSTEMS,INC.";


/*
modification history
--------------------
01c,30apr92,mli	 Added support for blocking read calls.
		 Modified isioClose to make a graceful close-
		 down of the channel (clear pending requests etc.).
		 Modified isioOpen to make sure that the channel
		 always is in a stable and well-defined state, once
		 it has been opened, and also to check that at most
		 one file descriptor is opened per channel.
		 In setOptions: change board echo flags only when
		 necessary.
		 In isioUseTyLib: don't start up receiver until
		 the channel is opened.
		 lockRx() and lockTx() now return a status indicating
		 if the channel could be successfully locked - all
		 calls to these functions have been modified accordingly
01b,14apr92,mli	 Added tyLib support (selectable per channel),
		 through usage of "modifier flags".
		 Removed <> on include files; use "" instead.
01a,27feb92,mli  initial release
*/


/*
DESCRIPTION

This is the driver for the HanA Intelligent serial I/O board.
The driver is able of controlling multiple ISIO boards
simultaneously.

USER CALLABLE ROUTINES

Most of the routines in this driver are accessible only through the I/O
system. Four routines, however, must be called directly: isioDrv() to
initialize the driver, isioBrd() to install ISIO-2 boards,
isioDevCreate() to create devices, and optionally isioUseTyLib() to
select full tyLib compatibility.

ISIODRV

Before using the driver, it must be created by calling the routine:
.CS
  STATUS isioDrv ()
.CE

This routine should be called exactly once, before any other calls
to the ISIO driver.

INSTALLING ISIO BOARDS

Each ISIO board that is to be controlled by the driver must be
installed by calling this routine:
.CS
  int  isioBrd (address, intVec,
		 l0, l1, l2, l3,
		 usrIoConfig, usrBufConfig, options)
  UINT32	   address;	  * board address	      *
  UINT		   intVec;	  * (base) interrupt vector  *
  UINT		   intLvl;    * interrupt levels	      *
  ISIO_IO_CONFIG  *usrIoConfig;   * (optional) I/O config.   *
  ISIOI_BUF_CONFIG *usrBufConfig; * (optional) buffer config *
  UINT		   options;	  * additional options       *
.CE

This routine should be called exactly once for each board, after
isioDrv has been called.

For instance, to install an ISIO board at address 0x90000000, with
base interrupt vector 200, using VME interrupt level 4 for all
interrupts, and using default I/O and buffer configuration, the proper
call would be:
.CS
	index = isioBrd(0x90000000, 200, 4, NULL,NULL, 0);
.CE

CREATING SERIAL DEVICES

Before a serial port can be used, it must be created.  This is done
with the isioDevCreate call.  Each port to be used should have exactly
one device associated with it, by calling this routine.
.CS
	STATUS isioDevCreate (name, channel, board)
	char *name;     * Name to use for this device *
	UINT channel    * Physical channel (0..7)     *
	UINT board;	    * Board index		   *
.CE

For instance, to create the device "/isio/0", for physical channel 0,
on an ISIO board with index 2, the proper call would be:
.CS
   isioDevCreate ("/isio/0", 0, 2);

.CE

SELECTING tyLib COMPATIBILITY

Sometimes an application will require an I/O channel to be fully
tyLib compatible. This is normally not the case with ISIO-2
channels, since some of the tyLib ioctl codes are not supported
by the ISIO-2 driver.

If tyLib compatibilty is needed, the following function should
be used:
.CS
STATUS	isioUseTyLib (channel, board, rdBufSize, wrtBufSize)
	UINT	channel;    * Physical channel (0..7)	   *
	UINT	board;	    * Board index		   *
	UINT    rdBufSize;  * Read buffer size, in bytes  *
	UINT    wrtBufSize; * Write buffer size, in bytes *
.CE

This function sets up the channel to use tyLib, and makes a tyDevInit
call with the read and write buffer sizes supplied as parameters.

This function should be called after isioDevCreate, and before any
descriptor is opened to the channel.

All subsequent I/O requests directed to this this channel will be
routed to tyLib.

After a channel has been set up to use tyLib, it will not respond to
any of the ISIO-specific ioctl codes/options.

IOCTL

The driver responds to the following standard ioctl codes:

	ioctl(fd, FIOBAUDRATE, baudRate);
	set baud rate for the device. The baud rate is set
	for both the receiver and the transmitter, i.e.
	split speed is not supported by this call (see
	"Baud Rates" and ioctl(fd, ISIO_F_SETCFG)).

	ioctl(fd, FIOGETOPTIONS)
	ioctl(fd, FIOSETOPTIONS, options)
	get/set device options. See "Option Codes".

	ioctl(fd, FIOCANCEL)
	cancel read/write request(s). This is the only call
	that may be used from interrupt level code.

	ioctl(fd, FIOFLUSH)
	ioctl(fd, FIORFLUSH)
	flush input buffer

	ioctl(fd, FIONREAD, &nBytes)
	return in nBytes the number of characters
	available in the input buffer

	ioctl(fd, FIOSELECT, nodePtr)
	ioctl(fd, FIOUNSELECT, nodePtr)
	add/delete node from select list. Note - these calls
	are not used directly by the application - they are
	used by the select support library.

	ioctl(fd, FIOISATTY)
		returns TRUE

The driver also responds to the following ISIO-specific ioctl codes:

	ioctl(fd, ISIO_F_GETDLM)
	ioctl(fd, ISIO_F_SETDLM, delimChar)
		get/set delimiter character. The delimiter is only
	used by the driver if the option ISIO_O_USEDLM is
	set.

	ioctl(fd, ISIO_F_GETCFG, &ioConfig)
	ioctl(fd, ISIO_F_SETCFG, &ioConfig)
	get/set device I/O configuration. This call is used
	to set various I/O configuration parameters.
	This call can be used to enable split speed
	configurations.

	The I/O configuration structure (defined in isio.h)
	has 6 configuration items:

	typedef struct {
	    UINT8      handshake;
	    UINT8      charLength;
	    UINT8      stopBits;
	    UINT8      parity;
	    UINT16     baud;
	} ISIO_IO_CONFIG;

	These configuration items can be set to the
	following symbolic values:

	handshake - handshake mode

	    NO_HANDSHAKE    - no handshake
	    HW_HANDSHAKE    - hardware (RTS/CTS) handshake
	    SW_HANDSHAKE    - software (XON/XOFF) handshake
	    HW_SW_HANDSHAKE - both above

	charLength - number of bits per character

	    SIX_CHAR_BITS
	    SEVEN_CHAR_BITS
	    EIGHT_CHAR_BITS

	stopBits - number of stop bits per character

	    ONE_STOP_BIT
	    TWO_STOP_BITS

	parity - parity mode

	    NO_PARITY
	    EVEN_PARITY
	    ODD_PARITY

	rxBaud - receiver baud rate
	txBaud - transmitter baud rate

	    BAUD_50
	    BAUD_75
	    BAUD_110
	    ...
	    BAUD_9600
	    BAUD_19200
	    BAUD_38400

	ioctl(fd, ISIO_F_ABORT)
		abort ISIO command. This call can be used to abort
	ISIO firmware command execution (after FIOCANCEL
	has been used to cancel a read/write request).

BAUD RATES

The baud rates available are 50, 75, 110, 134.5, 150, 200, 300, 600,
1050, 1200, 2000, 2400, 4800, 9600, and 38400 baud.

OPTION CODES

The following standard option codes are supported:

	OPT_LINE		Select line mode.
	OPT_ECHO		Echo input characters.
	OPT_CRMOD		Translate RETURN characters.
	OPT_7_BIT		Strip most significant bit on input.
	OPT_TERMINAL	Set all of the above option bits.
	OPT_RAW		Set none of the above option bits.

The following ISIO-specific option codes are supported:

	ISIO_O_USEDLM	Use delimiter for receiver - the
			read call will block until a
			delimiter character is received.
			The delimiter is included in the
			read data.

	ISIO_O_WAITTX	Wait for transmit completion - the
			write call will block until all
			characters have been transmitted.
			Without this option, the write call
			will just start a transmit operation
			and then return immediately.

	ISIO_O_BLOCKRX	Block read call until the specified
			number of bytes have been received.
			Without this option, the read call
			may return to the application even if
			less than "maxbytes" data bytes have
			been read (which is the normal
			behaviour of the read call in the
			VxWorks I/O system).

SEE ALSO: "I/O System"

INCLUDE FILE: isio.h
*/

#include "vxWorks.h"
#include "ioLib.h"
#include "iosLib.h"
#include "selectLib.h"
#include "iv.h"
#include "logLib.h"
#include "semLib.h"
#include "strLib.h"
#include "taskLib.h"
#include "tyLib.h"
#include "is.h"
#include "intLib.h"
#include "sysLib.h"
#include "errnoLib.h"
#include "vxLib.h"
#include "stdio.h"

int call=0,ack=0;
/*
#define TRACE_CMD  
*/

/*------------------------------------------------------------------
 Local Constants (configurable):

 MAX_BOARDS	- max number of boards (1-16) handled by the driver

 dfltIoConfig	- default I/O configuration for ISIO channels
 dfltBufConfig	- default configuration for I/O buffers
		  (sizes in kBytes, sum <= 94 kBytes)

		  NOTE - default buffer and I/O configuration may
		  be overridden by parameters to isioBrd.
		  The I/O configuration may also be set separately
		  for each I/O channel, by ioctl calls.
*/
#define MAX_BOARDS    16

LOCAL ISIO_IO_CONFIG	dfltIoConfig[ISIO_NUM_CHAN] =
{
/*
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},	
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		
//	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	}		
*/
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #0 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #1 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #2 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #3 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #4 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #5 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	},		/* PORT #6 */
	{	NO_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_2400	}		/* PORT #7 */
};

LOCAL ISIO_BUF_CONFIG	dfltBufConfig =
{
 /* ch0 - rx/tx */  2, 2,	/* NOTE - ch0/rx must ALWAYS be 2 kBytes */
 /* ch1 - rx/tx */  2, 2,
 /* ch2 - rx/tx */  2, 2,
 /* ch3 - rx/tx */  2, 2,
 /* ch4 - rx/tx */  2, 2,
 /* ch5 - rx/tx */  2, 2,
 /* ch6 - rx/tx */  2, 2,
 /* ch7 - rx/tx */  2, 2
};


/*------------------------------------------------------------------
 Local Constants (NON-configurable):

 RESET_TIMEOUT  - timeout (in seconds) for ISIO board reset
 SYNCH_TIMEOUT	- timeout (in seconds) for "synchronous" commands

 ABORT_DELAY	- delay (in fractions of a second) to wait for an
		  abortion of an ISIO command to be acknowledged
 ABORT_RETRIES  - number of retries for aborting ISIO commands

 baudTable	- baud rate conversion table

 RX_CHAN	- receive channel id
 TX_CHAN	- transmit channel id
*/
#define RESET_TIMEOUT 8
#define SYNCH_TIMEOUT 5

#define ABORT_DELAY 10
#define ABORT_RETRIES 3

LOCAL struct {
	int    baudRate;	/* baud rate	    */
	int	   code;		/* ISIO baud code */
} baudTable[] =
{
	50,    BAUD_50,
	75,    BAUD_75,
	110,   BAUD_110,
	134,   BAUD_134,	/* 134.5 */
	150,   BAUD_150,
	200,   BAUD_200,
	300,   BAUD_300,
	600,   BAUD_600,
	1050,  BAUD_1050,
	1200,  BAUD_1200,
	2000,  BAUD_2000,
	2400,  BAUD_2400,
	4800,  BAUD_4800,
	9600,  BAUD_9600,
	19200, BAUD_19200,
	38400, BAUD_38400,
	57600, BAUD_57600
};

#define RX_CHAN 0
#define TX_CHAN 1


#define MAXRX 2048       /* 2kbytes RX buffer */

int PortHandle[8];


/*------------------------------------------------------------------
 Local Variables:

 brdDesc	  - array of ISIO board descriptor structures
 nbrBoards	  - number of installed boards
 isioDrvNum	  - ISIO driver number
*/
LOCAL ISIO_BRD_DESC	brdDesc[ MAX_BOARDS ];
LOCAL int		nbrBoards;
LOCAL int		isioDrvNum;

IMPORT i_Stop(void );


/*------------------------------------------------------------------
 Code macros...
*/

#ifdef TRACE_ERR
# define traceErr1(arg)   logMsg(arg)
# define traceErr2(arg1,arg2)   logMsg(arg1,arg2)
# define traceErr3(arg1,arg2,arg3)   logMsg(arg1,arg2,arg3)
# define traceErr4(arg1,arg2,arg3,arg4)   logMsg(arg1,arg2,arg3,arg4)
#else
# define traceErr1(arg) /*nop*/
# define traceErr2(arg1,arg2) /*nop*/
# define traceErr3(arg1,arg2,arg3) /*nop*/
# define traceErr4(arg1,arg2,arg3,arg4) /*nop*/
#endif


#ifdef TRACE_CMD
# define traceCmd(a1,a2,a3) logMsg(a1,a2,a3)  
#else
# define traceCmd(a1,a2,a3)  /*nop*/
#endif


#ifdef TRACE_RESET
# define traceReset(arg) printf(arg)
/*
# define traceReset(arg) logMsg(arg)
*/
#else
# define traceReset(arg) /*nop*/
#endif

#ifdef TRACE_REQ
# define traceReq(arg) logMsg(arg)
#else
# define traceReq(arg) /*nop*/
#endif



/****************************/
/*	skhong debug 20020216	*/
/****************************/
#define	USER_WAIT		(sysClkRateGet() * 2)	/*	WAIT_FOREVER	*/
#define	ASYNCHCMD_WAIT	(sysClkRateGet() * 2)
#define	PROVE_MEM
/****************************/
/*	skhong debug 20020216	*/
/****************************/

/* Macros for providing mutual exclusion to
   individual receive/transmit channels:
*/
/* lockRx and lockTx are now implemeted as functions instead of macros */
/* //-#define lockRx(devDescp)   semTake(devDescp->rx.lock, USER_WAIT)-// */
/* //-#define lockTx(devDescp)   semTake(devDescp->tx.lock, USER_WAIT)-// */
#define unlockRx(devDescp)	semGive(devDescp->rx.lock)
#define unlockTx(devDescp)		semGive(devDescp->tx.lock)

/*	Macros for providing mutual exclusion to individual ISIO boards:	*/
#define lockBoard(brdDescp)	semTake(brdDescp->lock, USER_WAIT)
#define unlockBoard(brdDescp)	semGive(brdDescp->lock)




/********************************/
/*	skhong debug 20020216	*/
/********************************/
#define	PROVE308ADDR	0x10000000

STATUS	Prove308(int *acc_addr)
{
#ifdef	PROVE_MEM
	char 	testR;
	int		cnt = 10000;
	
	while(cnt--){
		if (vxMemProbe ((char*)&acc_addr, VX_READ, 1, &testR) == OK)		return(OK);
		else{
			taskDelay(1);
			printf("$");
		}
	}
	printf("Prove308 Board Error!!\n");
#endif	/*	PROVE_MEM	*/

	return	ERROR;
}

int	systemClkRate;

void	GetsystemClkRate()
{
	systemClkRate = sysClkRateGet();	
}


/********************************/
/*	skhong debug 20020216	*/
/********************************/

/*==========================================================================*/
/*========		    Private Functions			    ========*/
/*==========================================================================*/

/* //01c + -->// */
/* lockRx and lockTx are now implemeted as functions instead of macros */

/*****************************************************************************
*
* lockRx - lock the receiver part of an ISIO-2 channel
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS  lockRx(ISIO_DEV_DESC *devDescp)
#else
LOCAL STATUS  lockRx(devDescp)
ISIO_DEV_DESC *devDescp;
#endif
{
	semTake(devDescp->rx.lock, USER_WAIT);

	if (devDescp->open)		return OK;
	else{
		traceErr2("(ISIO)lockRx: channel %d closed\n", devDescp->chan);
		errnoSet(S_iosLib_INVALID_FILE_DESCRIPTOR);
		semGive(devDescp->rx.lock);
		return ERROR;
	}
}

/*****************************************************************************
*
* lockTx - lock the transmitter part of an ISIO channel
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS  lockTx(ISIO_DEV_DESC *devDescp)
#else
LOCAL STATUS  lockTx(devDescp)
ISIO_DEV_DESC *devDescp;
#endif
{
	semTake(devDescp->tx.lock, /*sysClkRateGet()*/USER_WAIT);

	if (devDescp->open)		return OK;
	else{
		traceErr2("(ISIO)lockTx: channel %d closed\n", devDescp->chan);
		errnoSet(S_iosLib_INVALID_FILE_DESCRIPTOR);
		semGive(devDescp->tx.lock);
		return ERROR;
	}
}
/* //<-- + 01c// */


/*****************************************************************************
*
* printFlags - help function for printing channel flags
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL VOID  printFlags(int flags)
#else
LOCAL VOID  printFlags(flags)
int   flags;
#endif
{
	int  n;

	for (n = ISIO_NUM_CHAN-1; n >= 0; n--){
		if (flags & (1 << n*2))			putchar(n+'0');
		else							putchar('.');
	}
	putchar('\n');
}


/*****************************************************************************
*
* probeBoard - probe for ISIO board
*
* RETURNS:
*    OK     if ISIO board has been probed successfully
*    ERROR  if failed to probe board
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS  probeBoard (ISIO_BOARD *isio)
#else
LOCAL STATUS  probeBoard (isio)
ISIO_BOARD *isio;
#endif
{
	int  dummy;

	if ((vxMemProbe((char*)&isio->status, READ, 2, (char*)&dummy) == ERROR) ||
		(vxMemProbe((char*)&isio->dpr[0], READ, 2, (char*)&dummy) == ERROR))	{
		traceErr1("probeBoard: failed to probe ISIO board\n");
		errnoSet(S_ioLib_DEVICE_ERROR);
		return ERROR;
	}
	else		return OK;
}


/*****************************************************************************
*
* resetBoard - reset ISIO board
*
* RETURNS:
*    OK     if board has been reset, and is operable
*    ERROR  if failed to reset board
*
* NOMANUAL
*/
#ifdef __STDC__
STATUS  resetBoard (ISIO_BOARD *isio)
#else
STATUS  resetBoard (isio)
ISIO_BOARD  *isio;
#endif
{
	int	 clkRate;
	int	 timeout;
	char *isio_reset;

	clkRate = sysClkRateGet();
	timeout = RESET_TIMEOUT * clkRate;	/* secs */

	/* ISIO Board CPU reset */
	isio_reset = (char *) ((int) isio + ISIO_CRST);

	*isio_reset = 0xff;

	/* wait for target board restarting */
	for (; timeout > 0; timeout--)	taskDelay(1);

	traceReset("ISIO RESET completed\n");

	printf("ISIO RESET completed\n");

	taskDelay(2 * clkRate);

	return OK;
}

/*****************************************************************************
*
* doSynchCmd - issue an ISIO command, and wait for completion
*
* Any input parameters must have been set up by the caller!
*
* This function is only used during installation of
* an ISIO board (interrupts might not be enabled at
* that time). During normal operation - serving read/write
* requests - all commands are completed asynchronously,
* with interrupts.
*
* RETURNS:
*    OK    if command completed OK
*    ERROR if command is not completed by ISIO firmware, or
*	   if command returned error...
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS doSynchCmd (ISIO_RXTX *p, UINT16 command)
#else
LOCAL STATUS doSynchCmd (p, command)
ISIO_RXTX *p;
UINT16	 command;
#endif
{
	int	status;
	int	n;
	
	p->cmd->cmdStat = command;

	for (n=0; n<(SYNCH_TIMEOUT*sysClkRateGet())/2; n++){
		if ((p->cmd->cmdStat & ISIO_CMD_DONE) != 0)	break;
		taskDelay( 2 );	/* number of ticks, 16msec*2 */
	}

	Prove308((int *)PROVE308ADDR);
	status = p->cmd->cmdStat;
	
	Prove308((int *)PROVE308ADDR);
	p->stat = status & ISIO_CMD_STAT;

	if ((status & ISIO_CMD_DONE) == 0){
		traceErr2("doSynchCmd: ISIO timeout (status=0x%04hX)\n", status);
		/* printf("doSynchCmd: ISIO timeout (status=0x%04hX)\n", status); */
		errnoSet(S_ioLib_DEVICE_TIMEOUT);
		return ERROR;
	}

	if ((status & 0xf) == OK)	return OK;
	else{
		traceErr3("doSynchCmd: ISIO BAD STATUS (cmd: 0x%hX status: 0x%X)\n", command, status);
		printf("doSynchCmd: ISIO BAD STATUS (cmd: 0x%hX status: 0x%X)\n", command, status);
		errnoSet(M_isio | (status & 0xf));
		return ERROR;
	}
}


/*****************************************************************************
*
* startAsynchCmd - issue an ISIO command, which will generate
*		   interrupt on completion
*
* Any input parameters must have been set up by the caller!
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL VOID startAsynchCmd (ISIO_RXTX *p, UINT16 command)
#else
LOCAL VOID startAsynchCmd (p, command)
ISIO_RXTX *p;
UINT16	 command;
#endif
{
	int loop=0;

	Prove308((int *)PROVE308ADDR);
	
	while( (p->cmd->cmdStat & ISIO_INT_CMD) && (loop<10000) ){
		Prove308((int *)PROVE308ADDR);
		loop++;
	}


	Prove308((int *)PROVE308ADDR);

	p->pendCmd = command;
	
	Prove308((int *)PROVE308ADDR);

	p->cmd->cmdStat = (command | ISIO_INT_CMD);
}


/*****************************************************************************
*
* waitAsynchCmd - wait for an ISIO "asynchronous" command to complete.
*
* Use for commands that generate an interrupt on completion.
* Call only from TASK code.
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS   waitAsynchCmd (ISIO_RXTX *p,int tick)
#else
LOCAL STATUS   waitAsynchCmd (p,tick)
	  ISIO_RXTX  *p;
	  int tick;
#endif
{
	STATUS status;

	Prove308((int *)PROVE308ADDR);

	status=semTake(p->rdy, tick);

	Prove308((int *)PROVE308ADDR);

	if (p->stat != ISIO_E_CANCEL){
		Prove308((int *)PROVE308ADDR);
		p->pendCmd = ISIO_C_NONE;
	}
	
	return status;
}


/*****************************************************************************
*
* doAsynchCmd - issue an ISIO command, and wait for
*		(interrupt) completion
*
* Any input parameters must have been set up by the caller!
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS doAsynchCmd (ISIO_RXTX *p, UINT16 command)
#else
LOCAL STATUS doAsynchCmd (p, command)
ISIO_RXTX *p;
UINT16	 command;
#endif
{
	STATUS status;

	call++;

	startAsynchCmd(p, command);
	status=waitAsynchCmd(p,ASYNCHCMD_WAIT);

	return	OK;
#ifdef	ORG
	/*do{*/
		startAsynchCmd(p, command);
		status=waitAsynchCmd(p,ASYNCHCMD_WAIT);
	/*}while(status == ERROR);*/
#endif
}


/*****************************************************************************
*
* waitRxCmd - wait for an ISIO (asynchronous) receive
*	      command to complete.
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL VOID   waitRxCmd (ISIO_DEV_DESC *devDescp)
#else
LOCAL VOID   waitRxCmd (devDescp)
ISIO_DEV_DESC  *devDescp;
#endif
{
	char    *datap;
	int	    delim;
	int	    n;
	UINT16  *p1;
	UINT16  *p2;
	
	Prove308((int *)PROVE308ADDR);
	semTake(devDescp->rx.rdy, USER_WAIT);

	Prove308((int *)PROVE308ADDR);

	if (devDescp->rx.stat == OK)
		switch (devDescp->rx.pendCmd)	{
			case ISIO_C_GETCH:
				Prove308((int *)PROVE308ADDR);
				devDescp->rxDataCnt = 1;

				p1 = (UINT16 *)&devDescp->rxDatap;
				p2 = (UINT16 *)&devDescp->rx.cmd->lParam1;
	
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
	
				p1++;
				p2++;
	
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
				break;
	
			case ISIO_C_GETLINE:
				p1 = (UINT16 *)&devDescp->rx.cmd->lParam1;
				p2 = (UINT16 *)&devDescp->board->isio;
				
				Prove308((int *)PROVE308ADDR);
				if (*p1 == *p2){
					p1++;
					p2++;
					Prove308((int *)PROVE308ADDR);
					if( *p1 == *p2)		devDescp->rxDataCnt = 0;
				}
				else{
					Prove308((int *)PROVE308ADDR);
					devDescp->rxDataCnt = devDescp->rx.cmd->wParam1;
			 
					p1 = (UINT16 *)&devDescp->rxDatap;
					p2 = (UINT16 *)&devDescp->rx.cmd->lParam1;
					
					Prove308((int *)PROVE308ADDR);
					*p1 = *p2;
					
					p1++;
					p2++;
					
					Prove308((int *)PROVE308ADDR);
					*p1 = *p2;
	
					/* Add CarriageReturn character to input: */
	
					Prove308((int *)PROVE308ADDR);
					if (devDescp->rxDataCnt < devDescp->rx.buf){
						Prove308((int *)PROVE308ADDR);
						devDescp->rxDatap[devDescp->rxDataCnt++] = '\r';
					}
					else{
						Prove308((int *)PROVE308ADDR);
						devDescp->rx.stat = ISIO_E_OVERFL;
					}
				}
				break;
	
			case ISIO_C_GETBUF:
			
			/* Support blocking read calls */
			case ISIO_C_GETCNT:
				Prove308((int *)PROVE308ADDR);
				devDescp->rxDataCnt = devDescp->rx.cmd->wParam1;
				p1 = (UINT16 *)&devDescp->rxDatap;
				p2 = (UINT16 *)&devDescp->rx.cmd->lParam1;
				
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
				p1++;
				p2++;
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
				break;
	
			case ISIO_C_GETSTR:
				/* ISIO firmware does not return the number
				   of characters received by a GETSTR command!
				 */
				p1 = (UINT16 *)&devDescp->rxDatap;
				p2 = (UINT16 *)&devDescp->rx.cmd->lParam1;
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
				p1++;
				p2++;
				Prove308((int *)PROVE308ADDR);
				*p1 = *p2;
				
				Prove308((int *)PROVE308ADDR);
				datap = devDescp->rxDatap;
				
				Prove308((int *)PROVE308ADDR);
				delim = devDescp->delim;
				
				for (n = 0; *datap != delim; n++, datap++)
					/*NOP*/;
	
				/* Include delimiter in count: */
				Prove308((int *)PROVE308ADDR);
				devDescp->rxDataCnt = n + 1;
				break;
	
			default:
#ifdef DEBUG
				printf("waitRxCmd: unexpected ISIO command (%x)\n",
				devDescp->rx.pendCmd);
				/* logMsg("waitRxCmd: unexpected ISIO command (%x)\n",
				devDescp->rx.pendCmd); */
#endif
				break;
		}

		Prove308((int *)PROVE308ADDR);
		if (devDescp->rx.stat != ISIO_E_CANCEL){
			Prove308((int *)PROVE308ADDR);
			devDescp->rx.pendCmd = ISIO_C_NONE;
		}
}


/*****************************************************************************
*
* startRxCmd - issue an (asynchronous) ISIO receive command,
*	       according to the selected channel options.
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL VOID startRxCmd (ISIO_DEV_DESC *devDescp)
#else
LOCAL VOID startRxCmd (devDescp)
ISIO_DEV_DESC  *devDescp;
#endif
{
	/* There are four different "modes" for read:

	   Blocking	 - read exactly n bytes
	   Line	     - line edit input, auto-echo, read until CR
	   Delimited - read until delimiter character
	   Normal    - read 1..n bytes
	*/

	/* Support blocking read calls */
	Prove308((int *)PROVE308ADDR);

	if (devDescp->options & ISIO_O_BLOCKRX)	startAsynchCmd(&devDescp->rx, ISIO_C_GETCNT);
	else										startAsynchCmd(&devDescp->rx, ISIO_C_GETBUF);
}


/*****************************************************************************
*
* abortCmd - abort an on-going ISIO command.
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS abortCmd (ISIO_DEV_DESC *devDescp, int rxTxId)
#else
LOCAL STATUS abortCmd (devDescp, rxTxId)
ISIO_DEV_DESC  *devDescp;
int	      rxTxId;
#endif
{
	return OK;
}


/*****************************************************************************
*
* setBaud - set baud rate (for Rx and Tx)
*
* RETURNS:
*    OK	    if new baud rate set
*    ERROR  if unsupported baud rate, or
*	    if ISIO firmware returned error
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL STATUS  setBaud (ISIO_DEV_DESC *devDescp, int baudRate)
#else
LOCAL STATUS  setBaud (devDescp, baudRate)
ISIO_DEV_DESC  *devDescp;
int	      baudRate;
#endif
{
	ISIO_IO_CONFIG   *ioConfigp;
	int		       n;
	int		       baudCode;
	UINT16     	*p1;
	UINT16     	*p2;

	for (n = 0; n < sizeof(baudTable)/sizeof(baudTable[0]); n++)
		if (baudTable[n].baudRate == baudRate)	break;

	if (n == sizeof(baudTable)/sizeof(baudTable[0])){
		traceErr1("setBaud: ISIO unsupported baud rate\n");
		errnoSet(S_isio_PARAM);
		return ERROR;
	}

	baudCode = baudTable[n].code;

	ioConfigp = (ISIO_IO_CONFIG *) devDescp->rx.dpr;

	Prove308((int *)PROVE308ADDR);	p1 = (UINT16 *)devDescp->rx.dpr;
	Prove308((int *)PROVE308ADDR);	p2 = (UINT16 *)&devDescp->ioConfig;
	Prove308((int *)PROVE308ADDR);	*p1++ = *p2++;
	Prove308((int *)PROVE308ADDR);	*p1++ = *p2++;
	Prove308((int *)PROVE308ADDR);	*p1 = *p2;

	ioConfigp->baud = baudCode;

	p1 = (UINT16 *)&(devDescp->rx.cmd->lParam1);
	p2 = (UINT16 *)&ioConfigp;
	Prove308((int *)PROVE308ADDR);	*p1++ = *p2++;
	Prove308((int *)PROVE308ADDR);	*p1 = *p2;

	doAsynchCmd(&devDescp->rx, ISIO_C_ASYNINI);

	if (devDescp->rx.stat != OK){
		errnoSet(M_isio | devDescp->rx.stat);
		return ERROR;
	}

	/* Update info in device descriptor: */

	devDescp->ioConfig.baud = baudCode;

	return OK;
}


/*****************************************************************************
*
* setOptions - set channel options
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL VOID  setOptions (ISIO_DEV_DESC *devDescp, int options)
#else
LOCAL VOID  setOptions (devDescp, options)
ISIO_DEV_DESC	*devDescp;
int		options;
#endif
{
	int  echoMask, echoFlag;

	/* If turning on "WaitForTxCompletion", make sure
	   any previous Tx command is done:
	*/
	if (((devDescp->options & ISIO_O_WAITTX) == 0) && ((options & ISIO_O_WAITTX) != 0))
		waitAsynchCmd(&devDescp->tx,sysClkRateGet()*5);

	/* If turning off "WaitForTxCompletion", make sure
	   the Tx ready semaphore is set:
	*/
	if (((devDescp->options & ISIO_O_WAITTX) != 0) && ((options & ISIO_O_WAITTX) == 0))
		semGive(devDescp->tx.rdy);

	Prove308((int *)PROVE308ADDR);
	devDescp->options = options;

	Prove308((int *)PROVE308ADDR);
	echoMask = ~(1 << (devDescp->chan * 2));
	
	Prove308((int *)PROVE308ADDR);
	echoFlag = (options & OPT_ECHO ? 1:0) << (devDescp->chan * 2);

	/* Modify board echo flags only if necessary */
	
	Prove308((int *)PROVE308ADDR);
	if ((devDescp->board->isio->echoFlags & ~echoMask) != echoFlag){
		lockBoard(devDescp->board);
		
		Prove308((int *)PROVE308ADDR);
		devDescp->board->isio->echoFlags &= echoMask;
		
		Prove308((int *)PROVE308ADDR);
		devDescp->board->isio->echoFlags |= echoFlag;
		
		unlockBoard(devDescp->board);
	}
}


/*****************************************************************************
*
* isioOpen - open file to an ISIO device
*
* Called from VxWorks I/O system.
*
* RETURNS:
*    ptr to ISIO device descriptor
*
* NOMANUAL
*/
LOCAL int  isioOpen (devDescp, name, mode)
ISIO_DEV_DESC	 *devDescp;
char             *name;
int              mode;
{
	int  status;

	traceReq("isioOpen\n");

	/* Make sure there is at most one fd open per device. */
	taskLock();

	Prove308((int *)PROVE308ADDR);
	if (devDescp->open){
		taskUnlock();
		return ERROR;
	}
	else		devDescp->open = TRUE;

	taskUnlock();

	/* Make sure there are no pending ISIO commands */

	lockBoard(devDescp->board);

	status  = abortCmd(devDescp, RX_CHAN);
	status |= abortCmd(devDescp, TX_CHAN);

	unlockBoard(devDescp->board);

	if (status != OK){
		devDescp->open = FALSE;
		traceErr1("isioOpen: abortCmd failed\n");
		errnoSet(S_ioLib_DEVICE_TIMEOUT);
		return ERROR;
	}

	/* Clear receive data buffer */

	Prove308((int *)PROVE308ADDR);
	devDescp->rxDataCnt = 0;

	/* Make sure mutual exclusion semaphores are properly initialized:	*/
	unlockRx(devDescp);

	unlockTx(devDescp);

	/* Start up receiver if channel is configured to use tyLib */
	if (devDescp->modFlags & ISIO_M_TYLIB)	startAsynchCmd(&devDescp->rx, ISIO_C_GETCH);

	/* Simulate a completed Tx command: */
	devDescp->tx.pendCmd = ISIO_C_PUTCNT;
	devDescp->tx.stat    = OK;
	semGive(devDescp->tx.rdy);


	/* Reset options (if this is a re-open of the device): */

	if (mode & O_NDELAY)	(VOID) setOptions(devDescp, 0);
	else					(VOID) setOptions(devDescp, ISIO_O_BLOCKRX);

	return ((int) devDescp);
}


/*****************************************************************************
*
* isioClose - close file to an ISIO device
*
* Called from VxWorks I/O system.
*
* RETURNS:
*    ptr to ISIO device descriptor
*
* NOMANUAL
*/
LOCAL int  isioClose (devDescp)
ISIO_DEV_DESC	 *devDescp;
{
	traceReq("isioClose\n");

/* //01c + -->// */

	taskLock();

	if (! devDescp->open)
	{
		taskUnlock();
		return OK;
	}

	devDescp->open = FALSE;

	/* Release pending tasks */

	unlockRx(devDescp);
	unlockTx(devDescp);

	taskSafe();

	taskUnlock();

	/* Cancel any pending requests */

	if (!(devDescp->rx.cmd->cmdStat & ISIO_CMD_DONE))
	{
		lockBoard(devDescp->board);
		abortCmd(devDescp, RX_CHAN);
		unlockBoard(devDescp->board);
	}

/*******
	devDescp->rx.stat = ISIO_E_ABORT;
	semGive(devDescp->rx.rdy);

	if (!(devDescp->tx.cmd->cmdStat & ISIO_CMD_DONE))
	{
		lockBoard(devDescp->board);
		abortCmd(devDescp, TX_CHAN);
		unlockBoard(devDescp->board);
	}
****/

	devDescp->tx.stat = ISIO_E_ABORT;
	semGive(devDescp->tx.rdy);

	taskUnsafe();

	return OK;
}


/*****************************************************************************
*
* isioIoctl - device control
*
* This routine handles device control requests; baud rate etc.
*
* Called from VxWorks I/O system.
*
* RETURNS:
*    X     depends on control request...
*
* NOMANUAL
*/

int  vme_io_addr[ISIO_NUM_CHAN] = {
	0x020,		/* Control register of Port #0 : port B */
	0x000,		/* Control register of Port #1 : port A */
	0x120,		/* Control register of Port #2 */
	0x100,		/* Control register of Port #3 */
	0x220,		/* Control register of Port #4 */
	0x200,		/* Control register of Port #5 */
	0x320,		/* Control register of Port #6 */
	0x300		/* Control register of Port #7 */
};

LOCAL int  isioIoctl (devDescp, request, arg)
ISIO_DEV_DESC    *devDescp;
int              request;
int              arg;
{
ISIO_IO_CONFIG   *ioConfigp;
int	             retVal = OK;
int              err;
UINT16	   *p1;
UINT16	   *p2;


	traceReq("isioIoctl\n");

	if (devDescp->modFlags & ISIO_M_TYLIB)
		switch (request){
			case FIOBAUDRATE:		/* set baud rate */
				retVal = setBaud(devDescp, arg);
				break;

		    default:
				retVal = tyIoctl(&(devDescp->tyDevp->tyDev), request, arg);
				break;
		}
	else
		switch (request)
		{
		/* ---- Standard ioctl codes ---- */

		case FIOBAUDRATE:	/* set baud rate */
			if ( (lockRx(devDescp) != OK) || (lockTx(devDescp) != OK) )	return ERROR;

			retVal = setBaud(devDescp, arg);

			unlockTx(devDescp);
			unlockRx(devDescp);
			break;

		case FIOGETOPTIONS:	/* get channel options */
			retVal = devDescp->options;
			break;

		case FIOSETOPTIONS:	/* set channel options */
			if ( (lockRx(devDescp) != OK) || (lockTx(devDescp) != OK) )	return ERROR;

			setOptions(devDescp, arg);

			unlockTx(devDescp);
			unlockRx(devDescp);
			break;

		case FIOCANCEL:		/* cancel read/write request */
		/* This function can be used by task and interrupt code. */

			Prove308((int *)PROVE308ADDR);
			if (devDescp->rx.pendCmd != ISIO_C_NONE){
				Prove308((int *)PROVE308ADDR);
				devDescp->rx.stat = ISIO_E_CANCEL;
				
				Prove308((int *)PROVE308ADDR);
				semGive(devDescp->rx.rdy);
			}

			if (devDescp->tx.pendCmd != ISIO_C_NONE){
				Prove308((int *)PROVE308ADDR);
				devDescp->tx.stat = ISIO_E_CANCEL;
				
				Prove308((int *)PROVE308ADDR);
				semGive(devDescp->tx.rdy);
			}
			break;

		case FIOFLUSH:		/* flush input buffer */
		case FIORFLUSH:
			if (lockRx(devDescp) != OK)				return ERROR;

			Prove308((int *)PROVE308ADDR);
			devDescp->rxDataCnt = 0;
			
			doAsynchCmd(&devDescp->rx, ISIO_C_CLRBUF);
			
			Prove308((int *)PROVE308ADDR);
			if (devDescp->rx.stat != OK){
				retVal = ERROR;
				traceErr2("isioIoctl: FIOFLUSH failed (0x%X)\n",devDescp->rx.stat);
				errnoSet(M_isio | devDescp->rx.stat);
			}
			
			unlockRx(devDescp);
			break;

		case FIOWFLUSH:
			if (lockRx(devDescp) != OK)				return ERROR;

			Prove308((int *)PROVE308ADDR);
			devDescp->rxDataCnt = 0;
			
			doAsynchCmd(&devDescp->tx, ISIO_C_TX_CLRBUF);
			
			unlockRx(devDescp);
			break;
		
		case FIONREAD:		/* get number of chars in read buffer */
			*((int *) arg) = 0;

			Prove308((int *)PROVE308ADDR);
			if (devDescp->rxDataCnt > 0){
				Prove308((int *)PROVE308ADDR);
				*((int *) arg) = devDescp->rxDataCnt;
			}
			else{
				if (lockRx(devDescp) != OK)			return ERROR;

				doAsynchCmd(&devDescp->rx, ISIO_C_INSTAT);
				
				Prove308((int *)PROVE308ADDR);
				printf("%d\r",(devDescp->rx.cmd->wParam1));
				
				Prove308((int *)PROVE308ADDR);
				if ((devDescp->rx.stat == OK) && ((devDescp->rx.cmd->wParam1 & 0x1000) == 0)){
					Prove308((int *)PROVE308ADDR);
					*((int *) arg) = 1;
				}
				else if (devDescp->rx.stat != OK){
					retVal = ERROR;
					traceErr2("isioIoctl: FIONREAD failed (0x%X)\n",devDescp->rx.stat);
					errnoSet(M_isio | devDescp->rx.stat);
				}

				unlockRx(devDescp);
			}
			break;

		case FIOSELECT:		/* add node to SelectList */
			Prove308((int *)PROVE308ADDR);
			selNodeAdd(&devDescp->wupList, (SEL_WAKEUP_NODE *) arg);

			/* check reads: */
			if (selWakeupType((SEL_WAKEUP_NODE *) arg) == SELREAD){
				Prove308((int *)PROVE308ADDR);
				if (devDescp->rxDataCnt > 0)	selWakeup((SEL_WAKEUP_NODE *) arg);
				else{
					Prove308((int *)PROVE308ADDR);
					err = semTake(devDescp->rx.lock, NO_WAIT);
					if (err == OK){
						Prove308((int *)PROVE308ADDR);
						if (devDescp->rx.pendCmd == ISIO_C_NONE)		startRxCmd(devDescp);
						
						Prove308((int *)PROVE308ADDR);
						semGive(devDescp->rx.lock);
					}
				}
			}

		    /* check writes: */
			if (selWakeupType((SEL_WAKEUP_NODE *) arg) == SELWRITE){
				Prove308((int *)PROVE308ADDR);
				if (devDescp->tx.pendCmd == ISIO_C_NONE)	selWakeup((SEL_WAKEUP_NODE *) arg);
			}
		    break;

		case FIOUNSELECT:	/* delete node from select list */
			Prove308((int *)PROVE308ADDR);
			selNodeDelete(&devDescp->wupList, (SEL_WAKEUP_NODE *) arg);
			break;

		case FIOISATTY:
		    retVal = TRUE;
		    break;

	/* ---- Special ISIO ioctl codes ---- */
		case ISIO_F_GETDLM:	/* get delimiter character */
			Prove308((int *)PROVE308ADDR);
			retVal = devDescp->delim;
			break;
		case ISIO_F_SETDLM:	/* set delimiter character */
			Prove308((int *)PROVE308ADDR);
			devDescp->delim = arg;
			break;

		case ISIO_F_GETCFG:	/* get channel I/O configuration */
			Prove308((int *)PROVE308ADDR);
			*((ISIO_IO_CONFIG *) arg) = devDescp->ioConfig;
			break;
		case ISIO_F_SETCFG:	/* set channel I/O configuration */
			ioConfigp = (ISIO_IO_CONFIG *) arg;
			if ( (ioConfigp->charLength <  SIX_CHAR_BITS)   ||
				(ioConfigp->charLength >  EIGHT_CHAR_BITS) ||
				( (ioConfigp->stopBits != ONE_STOP_BIT) &&
				(ioConfigp->stopBits != TWO_STOP_BITS) ) ||
				( (ioConfigp->parity   != NO_PARITY) &&
				(ioConfigp->parity   != EVEN_PARITY) &&
				(ioConfigp->parity   != ODD_PARITY) )    ||
				(ioConfigp->baud	<  BAUD_50)	    ||
				(ioConfigp->baud	>  BAUD_38400)
			)
			{
				retVal = ERROR;
				traceErr1("isioIoctl: bad I/O config parameter\n");
				errnoSet( S_isio_PARAM );
				break;
			}

			if ( (lockRx(devDescp) != OK) || (lockTx(devDescp) != OK) )		return ERROR;

			Prove308((int *)PROVE308ADDR);
			p1 = ((UINT16 *) devDescp->rx.dpr);
			Prove308((int *)PROVE308ADDR);
			p2 = (UINT16 *) arg;
			Prove308((int *)PROVE308ADDR);
			*p1++ = *p2++;
			Prove308((int *)PROVE308ADDR);
			*p1++ = *p2++;
			Prove308((int *)PROVE308ADDR);
			*p1 = *p2;

			Prove308((int *)PROVE308ADDR);
			p1 = (UINT16 *) &(devDescp->rx.cmd->lParam1);
			Prove308((int *)PROVE308ADDR);
			p2 = (UINT16 *) &devDescp->rx.dpr;
			Prove308((int *)PROVE308ADDR);
			*p1++ = *p2++;
			Prove308((int *)PROVE308ADDR);
			*p1 = *p2;

			doAsynchCmd(&devDescp->rx, ISIO_C_ASYNINI);
			if (devDescp->rx.stat == OK)	devDescp->ioConfig = *((ISIO_IO_CONFIG *) arg);
	     	else{
				retVal = ERROR;
				traceErr2("isioIoctl: SETCFG failed (0x%X)\n",
				devDescp->rx.stat);
				errnoSet(M_isio | devDescp->rx.stat);
			}

			unlockTx(devDescp);
			unlockRx(devDescp);
			break;




/*******************************/
/* 		ISIO_C_RESET		   */
/*******************************/
		case ISIO_RESET:
		{
			if ( (lockTx(devDescp) != OK)  ){
				printf("RX lock fail\n");
				return ERROR;
			}

			Prove308((int *)PROVE308ADDR);
			doAsynchCmd(&devDescp->tx, ISIO_C_RESET);

			Prove308((int *)PROVE308ADDR);

			unlockTx(devDescp);

			{
				int i;
				for(i=2;i<100;i+=2)
					i--;
			}

			break;
		}
		


/*******************************/
/* RTS signal assert: optional */
/*******************************/
		case  ISIO_ASS_RTS:
		{
			if ( (lockTx(devDescp) != OK)  ){
				printf("RX lock fail\n");
				return ERROR;
			}

			Prove308((int *)PROVE308ADDR);
			doAsynchCmd(&devDescp->tx, ISIO_C_ASSRTS);

			Prove308((int *)PROVE308ADDR);
			if (devDescp->tx.stat != OK){
				retVal = ERROR;
				traceErr2("isioIoctl: ISIO_ASS_RTS failed (0x%X)\n", devDescp->tx.stat);
				errnoSet(M_isio | devDescp->tx.stat);
			}

			Prove308((int *)PROVE308ADDR);
			unlockTx(devDescp);

			{
				int i;
				for(i=2;i<100;i+=2)
					i--;
			}

			break;
		}

/************************************/
/* RTS signal deassert: optional	*/
/************************************/
		case  ISIO_DASS_RTS:
		{
			if ( (lockTx(devDescp) != OK) ){
				printf("TX lock fail\n");
				return ERROR;
			}
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd(&devDescp->tx, ISIO_C_DASSRTS);
			
			Prove308((int *)PROVE308ADDR);
			if (devDescp->tx.stat != OK){
				retVal = ERROR;
				traceErr2("isioIoctl: ISIO_C_DASSRTS failed (0x%X)\n",
					devDescp->tx.stat);
				errnoSet(M_isio | devDescp->tx.stat);
			}
			
			Prove308((int *)PROVE308ADDR);
			unlockTx(devDescp);
			
			break;
		}


/************************************/
/* RTS signal deassert: optional		*
		case  ISIO_AUTO_RTS :
		{
			if ( (lockTx(devDescp) != OK) ){
				printf("TX lock fail\n");
				return ERROR;
			}
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd(&devDescp->tx, ISIO_C_DASSRTS);
			
			Prove308((int *)PROVE308ADDR);
			if (devDescp->tx.stat != OK){
				retVal = ERROR;
				traceErr2("isioIoctl: ISIO_C_DASSRTS failed (0x%X)\n",
					devDescp->tx.stat);
				errnoSet(M_isio | devDescp->tx.stat);
			}
			
			Prove308((int *)PROVE308ADDR);
			unlockTx(devDescp);
			
			break;
		}
************************************/

		case ISIO_CHK_CTS:
		{
			int	i;
			
			if ( (lockTx(devDescp) != OK) ){
				printf("TX lock fail\n");
				return ERROR;
			}
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_C_CHKCTS);
#if 0
			for(i=2;i<300;i+=2)			i--;
#endif			
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_C_CHKCTS);
			
			Prove308((int *)PROVE308ADDR);
			retVal= ( devDescp->tx.cmd->wParam1 & 0x20 ) >> 5;
			
			unlockTx(devDescp);
			break;
		}

		case ISIO_CHK_RTS:
		{
			int	i;
			
			if ( (lockTx(devDescp) != OK) ){
				printf("TX lock fail\n");
				return ERROR;
			}
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_C_CHKRTS);
/*
			for(i=2;i<300;i+=2)			i--;
			
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_C_CHKRTS);
*/			
			Prove308((int *)PROVE308ADDR);
			retVal= devDescp->tx.cmd->wParam1;
			
			unlockTx(devDescp);
			break;
		}
		
		case ISIO_WR_WR15:
		{
			char *sio_addr;
			sio_addr = 	(char *) (0xf4000000 | 
						(int) (((int)devDescp->board->isio>>16) & 0xf800) |
						(int) vme_io_addr[devDescp->chan]);
			
			Prove308((int *)PROVE308ADDR);
			*sio_addr = 15;		/* write register #15 */
			Prove308((int *)PROVE308ADDR);
			*sio_addr = 0;		/* CTS IE clear */
			break;
		}
		
		case ISIO_CHECK_TX : 
#if 0
		{
			Prove308((int *)PROVE308ADDR);
			retVal=devDescp->tx.stat;
			break;
		}
#endif
		{
			int i;
			
			if ( (lockTx(devDescp) != OK) ){
				printf("TX lock fail\n");
				return ERROR;
			}
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_CHECK_TX);

			for(i=2;i<300;i+=2)			i--;
			
			Prove308((int *)PROVE308ADDR);
			doAsynchCmd (&devDescp->tx, ISIO_CHECK_TX);
			
			Prove308((int *)PROVE308ADDR);
			retVal= devDescp->tx.cmd->wParam1;
			
			unlockTx(devDescp);
			break;
		}
			

		case 0xffff : /*20020325 by bigfire for debugging*/
		{
			printf("txStatus:0x%x\n",devDescp->tx.stat);
			break;
		}

		default:
			traceErr2("isioIoctl: unsupported ioctl code (%d)\n", request);
			errnoSet(S_ioLib_UNKNOWN_REQUEST);
			retVal = ERROR;
			break;
		}	/* end of switch */

	return retVal;
}


/*****************************************************************************
*
* isioWrite
*
* This routine handles write (transmit) requests.
*
* Called from VxWorks I/O system.
*
* RETURNS:
*    1..n    number of bytes written
*    ERROR   if error during write
*
* NOMANUAL
*/
LOCAL int  isioWrite (devDescp, buffer, nBytes)
ISIO_DEV_DESC  *devDescp;
char           *buffer;
int        	   nBytes;
{
	STATUS	status = OK;
	int		bytesToTx;
	int		n;
	char	*txBufp;
	int		bytesInTxBuf;

	traceReq("isioWrite\n");

	if (devDescp->modFlags & ISIO_M_TYLIB){
		return tyWrite(&(devDescp->tyDevp->tyDev), buffer, nBytes);
	}

	if (nBytes == 0)	return 0;

	if (lockTx(devDescp) != OK)		return ERROR;

	Prove308((int *)PROVE308ADDR);
	devDescp->tx.stat=ERROR;

	bytesToTx = nBytes;

	while (bytesToTx > 0){
		/* If option OPT_CRMOD is set - translate NewLine
		   characters to CarriageReturn-NewLine sequences:
		*/

		if (devDescp->options & OPT_CRMOD){
			Prove308((int *)PROVE308ADDR);
			txBufp = (char *) devDescp->tx.dpr;
			bytesInTxBuf = 0;
			n = 0;
			
			Prove308((int *)PROVE308ADDR);
			while ((bytesInTxBuf < devDescp->tx.buf-1) && (n < bytesToTx)){
				if (buffer[n] == '\n'){
					Prove308((int *)PROVE308ADDR);
					*txBufp++ = '\r';
					bytesInTxBuf++;
				}

				Prove308((int *)PROVE308ADDR);
				*txBufp++ = buffer[n++];
				bytesInTxBuf++;
	  		}
		}
		else{
			n = bytesToTx;
			Prove308((int *)PROVE308ADDR);
			
			if (n > devDescp->tx.buf)	n = devDescp->tx.buf;
			
			Prove308((int *)PROVE308ADDR);
			bcopyBytes((char*)buffer, (char*)devDescp->tx.dpr, n);
			bytesInTxBuf = n;
		}
		
		Prove308((int *)PROVE308ADDR);
		devDescp->tx.cmd->wParam1 = bytesInTxBuf;
	 
		startAsynchCmd(&devDescp->tx, ISIO_C_PUTCNT);
		/*devDescp->txFlag=0;*/

		bytesToTx -= n;
		buffer += n;

		if (bytesToTx > 0){
			Prove308((int *)PROVE308ADDR);
			waitAsynchCmd(&devDescp->tx,/*sysClkRateGet()*5*/USER_WAIT);
			Prove308((int *)PROVE308ADDR);
			status = devDescp->tx.stat;
			
			if (status != OK)			break;
		}
	}	/* while () */
#if 0
	if ((status == OK) && ((devDescp->options & ISIO_O_WAITTX) != 0)){
#endif
		Prove308((int *)PROVE308ADDR);
		waitAsynchCmd(&devDescp->tx,USER_WAIT);
		Prove308((int *)PROVE308ADDR);
		status = devDescp->tx.stat;
#if 0
	}
#endif
	unlockTx(devDescp);

	if (status != OK)
	{
		traceErr2("isiowrite: bad Tx [%.2X] status  \n", status);
		logMsg("isiowrite: bad Tx [%.2X] status  \n", status,0,0,0,0,0);
		errnoSet(M_isio | status);
		return ERROR;
	}
	else

		return nBytes;
	
}

/*****************************************************************************
*
* isioRead
*
* This routine handles read (receive) requests.
*
* Called from VxWorks I/O system.
*
* RETURNS:
*    1..n   number of bytes read, on successful completion
*    ERROR  if error during read
*
* NOMANUAL
*/
#ifdef __STDC__
LOCAL int  isioRead (ISIO_DEV_DESC *devDescp,char *buffer,int maxBytes)
#else
LOCAL int  isioRead (devDescp, buffer, maxBytes)
ISIO_DEV_DESC	*devDescp;
char			*buffer;
int				maxBytes;
#endif
{
	STATUS	status = OK;
	int		nBytes, n;
	unsigned char *dp, *sp, *ep;

	traceReq("isioRead\n");

	Prove308((int *)PROVE308ADDR);
	if (devDescp->modFlags & ISIO_M_TYLIB) {
		Prove308((int *)PROVE308ADDR);
		return tyRead(&(devDescp->tyDevp->tyDev), buffer, maxBytes);
	}

	if (maxBytes == 0)		return 0;

	if (lockRx(devDescp) != OK)		return ERROR;

	/* Check if receiver buffer is empty,
	   issue new read request if there is
	   none pending:
	*/
	Prove308((int *)PROVE308ADDR);
	if (devDescp->rxDataCnt == 0){
		Prove308((int *)PROVE308ADDR);
		if (devDescp->rx.pendCmd == ISIO_C_NONE)	    startRxCmd(devDescp);

		waitRxCmd(devDescp);

		Prove308((int *)PROVE308ADDR);
		status = devDescp->rx.stat;
	}

	if (status != OK){
		traceErr2("isioread: bad Rx status (0x%X)\n", devDescp->rx.stat);
		errnoSet(M_isio | devDescp->rx.stat);
		unlockRx(devDescp);
		printf(" Status = ERROR \n\n");
		return ERROR;
	}

	/* Copy data from I/O buffer to user buffer: */
	Prove308((int *)PROVE308ADDR);
	nBytes = devDescp->rxDataCnt;  

	if (nBytes > maxBytes)		nBytes = maxBytes;

	dp = buffer;
	Prove308((int *)PROVE308ADDR);
	sp = devDescp->rxDatap;
	
	Prove308((int *)PROVE308ADDR);
	ep = (unsigned char *)((int)devDescp->rx.dpr + devDescp->rx.buf -1);

	n = nBytes;

	while(n--){
		Prove308((int *)PROVE308ADDR);
		*dp++ = *sp++;

		if (sp > ep){
			Prove308((int *)PROVE308ADDR);
			sp = devDescp->rx.dpr;
		}
	}

	Prove308((int *)PROVE308ADDR);
	devDescp->rxDatap = sp;
	
	Prove308((int *)PROVE308ADDR);
	devDescp->rxDataCnt -= nBytes;

	/* If "normal" mode, check if there are more
	   characters available (the first read command
	   is always a "read one character" command):
	*/
	unlockRx(devDescp);

	if (status != OK){
		traceErr2("isioread: bad Rx status (0x%X)\n", status);
		errnoSet(M_isio | status);
		return ERROR;
	}

	/* If option OPT_CRMOD is set - translate any
	   CarriageReturn characters to LineFeed characters:
	*/
	Prove308((int *)PROVE308ADDR);
	if (devDescp->options & OPT_CRMOD){
		for (n = 0; n < nBytes; n++)
		    if (buffer[n] == '\r')			buffer[n] = '\n';
	}
	
	Prove308((int *)PROVE308ADDR);
	devDescp->rx.cmd->wParam1-=nBytes;

	return nBytes;
}

/*****************************************************************************
*
* isioIsr - ISIO Interrupt Service Routine
*
* This routine is triggered by ISIO (command completion)
* interrupts.
*
* The ISR will not do much work, just signal to task code
* that the command has completed, and also that the device
* is ready for read or write request (select support).
*
* NOMANUAL
*/
LOCAL VOID  isioIsr (board)
int  board;
{
	ISIO_BRD_DESC  *brdDescp;
	ISIO_DEV_DESC  *devDescp;
	int		    chan;

	ack++;
	/* The board parameter contains the board number
	   (index to brdDesc)

	   MSB     LSB
	   -----------
	   00 00 00 0b
	             |
	             +-------- board number (one nibble) value = 0..15
	*/

	brdDescp = &brdDesc[ board & 0xff ];

	/* Check receiver and transmitter for two I/O channels
	   (break immediately if a completed command is detected):
	*/
	for (chan = 0; chan < ISIO_NUM_CHAN; chan++){
		Prove308((int *)PROVE308ADDR);
		devDescp = &brdDescp->devDesc[chan];
		
		/*****************************************
		*			Check receiver			*
		*****************************************/
		if (	(devDescp->rx.cmd->cmdStat & (ISIO_CMD_DONE | ISIO_INT_CMD)) == 
		   	(ISIO_CMD_DONE | ISIO_INT_CMD)){
			
			/* reset interrupt flag, and serve interrupt: */
			Prove308((int *)PROVE308ADDR);
			devDescp->rx.cmd->cmdStat &= ~ISIO_INT_CMD;

			if (devDescp->modFlags & ISIO_M_TYLIB){
				(void) tyIRd (&(devDescp->tyDevp->tyDev), devDescp->rx.cmd->wParam1 & 0xFF);

				/* Start new receive command */
				startAsynchCmd(&devDescp->rx, ISIO_C_GETCH);
			}
			else {
				Prove308((int *)PROVE308ADDR);
				devDescp->rx.stat = devDescp->rx.cmd->cmdStat & ISIO_CMD_STAT;
				
				Prove308((int *)PROVE308ADDR);
				semGive(devDescp->rx.rdy);
				
				Prove308((int *)PROVE308ADDR);
				selWakeupAll(&devDescp->wupList, SELREAD);
			}
			/*break;*/ /*20020327 by bigfire*/
		}

		/*****************************************
		*			Check transmitter			*
		*****************************************/
		if (	(devDescp->tx.cmd->cmdStat & (ISIO_CMD_DONE | ISIO_INT_CMD) )	== 
			(ISIO_CMD_DONE | ISIO_INT_CMD))
		{
			/* reset interrupt flag, and serve interrupt: */

			Prove308((int *)PROVE308ADDR);
			devDescp->tx.cmd->cmdStat &= ~ISIO_INT_CMD;

			if (devDescp->modFlags & ISIO_M_TYLIB){
				char  outChar;

				Prove308((int *)PROVE308ADDR);
				if (tyITx(&(devDescp->tyDevp->tyDev), &outChar) == OK){
					/* 
					    The PUTCH command accepts one or two bytes to
					   be transmitted. Therefore we must make sure
					   that the most sign. byte is zero (and not an
					   expansion of the sign bit in outChar).
					*/

					Prove308((int *)PROVE308ADDR);
					devDescp->tx.cmd->wParam1 = (unsigned char) outChar;
					Prove308((int *)PROVE308ADDR);
					startAsynchCmd(&devDescp->tx, ISIO_C_PUTCH);
				}
			}
			else{
				Prove308((int *)PROVE308ADDR);
				devDescp->tx.stat = devDescp->tx.cmd->cmdStat & ISIO_CMD_STAT;
				Prove308((int *)PROVE308ADDR);
				semGive(devDescp->tx.rdy);
				Prove308((int *)PROVE308ADDR);
				selWakeupAll(&devDescp->wupList, SELWRITE);
				/*break;*/ /*20020327 by bigfire*/
			}
		}
	}
}


/*****************************************************************************
*
* isioStartup - start up transmitter
*
* This function is called from tyLib.
*
* NOMANUAL
*/
LOCAL VOID  isioStartup(tyDevp)
ISIO_TY_DEV *tyDevp;
{
	char  outChar;

	Prove308((int *)PROVE308ADDR);
	if (tyITx(&(tyDevp->tyDev), &outChar) == OK){
		/* The PUTCH command accepts one or two bytes to
		   be transmitted. Therefore we must make sure
		   that the most sign. byte is zero (and not an
		   expansion of the sign bit in outChar).
		*/

		Prove308((int *)PROVE308ADDR);
		tyDevp->devDescp->tx.cmd->wParam1 = (unsigned char) outChar;
		Prove308((int *)PROVE308ADDR);
		startAsynchCmd(&tyDevp->devDescp->tx, ISIO_C_PUTCH);
	}
}

/*==========================================================================*/
/*========		    Public Functions			    ========*/
/*==========================================================================*/


/*****************************************************************************
*
* isioDrv	 - install ISIO driver
*
* This routine initializes the ISIO driver. It must be called
* before ANY other routine in this driver.
*
* RETURNS:
* OK, or ERROR if I/O system was unable to install driver
*
* SEE ALSO: isioRemove(2)
*/
STATUS	isioDrv ()
{
	if (isioDrvNum > 0)		return OK;	/* driver already installed */

	printf ("isioDrvNum < 0\n");

	isioDrvNum = iosDrvInstall(isioOpen,	/* create */
				(FUNCPTR) NULL,	/* delete */
				isioOpen,	/* open	  */
				isioClose,	/* close  */
				isioRead,	/* read	  */
				isioWrite,	/* write  */
				isioIoctl);	/* ioctl  */

	return (isioDrvNum == ERROR ? ERROR : OK);
}


/*****************************************************************************
*
* isioRemove	 - remove ISIO driver
*
* This function removes the ISIO driver from the
* VxWorks I/O system. Any open files will be closed.
*
* RETURNS:
* OK (always)
*
* SEE ALSO: isioDrv(2)
*/
STATUS	isioRemove ()
{
	if (isioDrvNum <= 0)	return OK;

	iosDrvRemove(isioDrvNum, TRUE);

	isioDrvNum = 0;
	nbrBoards = 0;

	return OK;
}

/*****************************************************************************
*
* isioBrd	 - install ISIO board
*
* This routine installs an ISIO board that is to be
* controlled by the ISIO driver. This routine should be called
* exactly once for each  ISIO board.
*
* NOTE
*
* No mutual exclusion mechanisms are provided by this
* function. Therefore it must either be used by one
* task only, or the application must provide mutual
* exclusion mechanisms, if it is used by multiple tasks.
*
* ARGUMENTS
*
* The usrIoConfig parameter should be a pointer to an
* ISIO I/O configuration structure, or NULL to use
* the default I/O configuration provided by the driver.
*
* The usrBufConfig parameter should be a pointer to an
* ISIO buffer configuration array, or NULL to use
* the default buffer configuration provided by the
* driver.
*
* The options parameter may be set to zero for normal
* operation, or to any combination of the following
* options:
*
*    ISIO_O_RESET  - reset ISIO board before installation
*
* RETURNS:
* index of installed board (0..n), or ERROR if unable to install board
*/
int	isioBrd (address, intVec, intLvl,
		 usrIoConfig, usrBufConfig, options)
UINT32	     address;			/* board address		 */
UINT	     intVec;  			/* (base) interrupt vector */
UINT	     intLvl;     		/* interrupt levels , must know*/
ISIO_IO_CONFIG  *usrIoConfig;	/* (opt.) I/O config  */
ISIO_BUF_CONFIG *usrBufConfig;	/* (opt.) buffer cfg  */
UINT	     options;	   		/* additional options */
{
ISIO_BRD_DESC    *brdDescp;
ISIO_DEV_DESC    *devDescp;
ISIO_BOARD	     *isio;
ISIO_IO_CONFIG   *ioConfig;
ISIO_BUF_CONFIG  *bufConfig;

	ULONG      dprOffs;
	STATUS     err;
	int        chan;
	UINT16	  *p;
	UINT16	  *p1;
	UINT16	  *p2;
	UINT16	  *p8;
	UINT16	  *p9;

	GetsystemClkRate();
	
	if (options)		i_Stop();

	if (isioDrvNum <= 0){
		errnoSet(S_ioLib_NO_DRIVER);
		traceErr1("isioBrd: driver not installed\n");
		return ERROR;
	}
	 printf("isioDrvNum Check OK \n");

	if (nbrBoards == MAX_BOARDS){
		traceErr1("isioBrd: too many boards\n");
		return ERROR;
	}


	/* Take care of optional parameters: */
	if (usrIoConfig != NULL)		ioConfig = usrIoConfig;
	else						ioConfig = &dfltIoConfig[0];

	if (usrBufConfig != NULL)	bufConfig = usrBufConfig;
	else						bufConfig = &dfltBufConfig;

	/* Check that ISIO board can be accessed: */
	isio = (ISIO_BOARD *) address;

	if (probeBoard(isio) != OK){
		printf("Set Board Base Address Properly.\n");
		return ERROR;
	}
	else	printf("probeBoard OK \n");

	/* Program BIM interrupt vectors, and interrupt levels: */
	isio->vmeVec = intVec;
	p = (UINT16 *)&isio->baseaddr;
	p1 = (UINT16 *)&address;
	*p = *p1;
	p++ ;	p1++;
	*p = *p1;

	/* Initialize the board descriptor: */
	brdDescp = &brdDesc[ nbrBoards ];			/* local memory area */
	brdDescp->isio	= (ISIO_BOARD *) address;
	brdDescp->intVec	= intVec;
	brdDescp->intLvl	= intLvl;
	printf("board Vec IntLevel setting OK \n");

	/* Initialize device descriptors: */
	dprOffs = 0;

	for (chan = 0; chan < ISIO_NUM_CHAN; chan++){
		Prove308((int *)PROVE308ADDR);	devDescp = &brdDescp->devDesc[chan];	/* local memory area */
		Prove308((int *)PROVE308ADDR);	devDescp->created = FALSE;
		Prove308((int *)PROVE308ADDR);	devDescp->open = FALSE;
		Prove308((int *)PROVE308ADDR);	devDescp->chan = chan;

		Prove308((int *)PROVE308ADDR);	devDescp->modFlags = 0;
		Prove308((int *)PROVE308ADDR);	devDescp->rx.pendCmd = ISIO_C_NONE;
		Prove308((int *)PROVE308ADDR);	devDescp->tx.pendCmd = ISIO_C_NONE;

		Prove308((int *)PROVE308ADDR);	devDescp->rx.cmd = &isio->cmd[chan*2];
		Prove308((int *)PROVE308ADDR);	devDescp->tx.cmd = &isio->cmd[chan*2 + 1];

		Prove308((int *)PROVE308ADDR);	devDescp->rx.dpr = &isio->dpr[dprOffs];
		Prove308((int *)PROVE308ADDR);	devDescp->rx.buf = ((*bufConfig)[ chan*2 ] * 1024);	/* buf size */		
		Prove308((int *)PROVE308ADDR);	dprOffs += devDescp->rx.buf;

		Prove308((int *)PROVE308ADDR);	devDescp->tx.dpr = &isio->dpr[dprOffs];
		Prove308((int *)PROVE308ADDR);	devDescp->tx.buf = ((*bufConfig)[ (chan*2)+1 ] * 1024);
		Prove308((int *)PROVE308ADDR);	dprOffs += devDescp->tx.buf;

		Prove308((int *)PROVE308ADDR);	devDescp->ioConfig = *ioConfig++;

		Prove308((int *)PROVE308ADDR);	devDescp->board = brdDescp;			/* local memory area */
	}

	/* Fix parameter RAM address */
	/* Program I/O buffer sizes: */

	devDescp = &brdDescp->devDesc[0];
	
	Prove308((int *)PROVE308ADDR);	bcopyBytes((char *)bufConfig, (char*)devDescp->rx.dpr, sizeof(*bufConfig)); 
	Prove308((int *)PROVE308ADDR);	p = (UINT16 *)&(devDescp->rx.cmd->lParam1);
	Prove308((int *)PROVE308ADDR);	p1 = (UINT16 *)&address;
	Prove308((int *)PROVE308ADDR);	p2 = (UINT16 *)&devDescp->rx.dpr;
	Prove308((int *)PROVE308ADDR);	*p = *p2 - *p1;
	p++ ;	p1++;	p2++;
	Prove308((int *)PROVE308ADDR);	*p = *p2 - *p1;
	printf("Fix parameter Pointer write OK \n");

	if (doSynchCmd(&devDescp->rx, ISIO_C_GLOBINI) != OK)	return ERROR;
	printf("Fix parameter doSyncCmd write OK \n");


	/* From now on, all address parameters given to ISIO
	   commands are true VME adresses (and not offsets from
	   base of ISIO board).
	*/

	/* Program default I/O configuration (an initialization
	   command given to a receiver will initialize both
	   receiver and transmitter):
	*/

	for (chan = 0; chan < ISIO_NUM_CHAN; chan++){
		devDescp = &brdDescp->devDesc[chan];
		p8 = (UINT16 *)((ISIO_IO_CONFIG *) devDescp->rx.dpr);
		p9 = (UINT16 *) &devDescp->ioConfig;

		*p8 = *p9;
		p8++;	p9++;
		*p8 = *p9;
		p8++;	p9++;
		*p8 = *p9;

		p = (UINT16 *)&(devDescp->rx.cmd->lParam1);
		p1 = (UINT16 *)&address;
		p2 = (UINT16 *)&devDescp->rx.dpr;

		*p = *p2 - *p1;
		p++ ;	p1++;	p2++;
		*p = *p2 - *p1;
		
		if (doSynchCmd(&devDescp->rx, ISIO_C_ASYNINI) != OK)		return ERROR;
	}
	printf("pass Channel Init!!\n");
	
	/* Connect interrupt handlers for VME interrupt of ISIO channel */
	err = intConnect(INUM_TO_IVEC(intVec), isioIsr, nbrBoards);
	if (err == ERROR){
		traceErr1("isioBrd: intConnect failed\n");
		return ERROR;
	}

	printf ("intrrupt Service routine OK\n");

	/* Create and initialize board lock semaphore: */
	Prove308((int *)PROVE308ADDR);
	brdDescp->lock = semBCreate(SEM_Q_FIFO, SEM_FULL);

	/* ISIO board successfully installed; return board index: */
	printf ("nbrBoards : 0x%x \n", nbrBoards);
	nbrBoards += 1;

	return (nbrBoards - 1);
}


/*****************************************************************************
*
* isioDevCreate - create ISIO I/O device
*
* This routine creates a device on one of the serial ports.
* Each port to be used should have exactly one device associated
* with it, by calling this routine.
*
* RETURNS:
* OK, or ERROR if failed to create device
*/
STATUS	isioDevCreate (name, channel, board)
char	*name;		/* Name to use for this device */
UINT	channel;	/* Physical channel (0..7)	   */
UINT	board;		/* Board index		   */
{
ISIO_DEV_DESC	*devDescp;

	if (isioDrvNum <= 0){
		traceErr1("isioDevCreate: driver not installed\n");
		errnoSet(S_ioLib_NO_DRIVER);
		return ERROR;
	}

	if ((channel < 0) || (channel >= ISIO_NUM_CHAN)){
		traceErr1("isioDevCreate: bad channel number\n");
		errnoSet(S_isio_PARAM);
		return ERROR;
	}

	if ((board < 0) || (board > nbrBoards-1)){
		traceErr1("isioDevCreate: bad board index\n");
		errnoSet(S_isio_PARAM);
		return ERROR;
	}

	lockBoard( (&brdDesc[board]) );

	devDescp = &brdDesc[board].devDesc[channel];
	if (devDescp->created){
		traceErr1("isioDevCreate: device already created\n");
		unlockBoard( (&brdDesc[board]) );
		return ERROR;
	}


	Prove308((int *)PROVE308ADDR);
	if (iosDevAdd((DEV_HDR *) &devDescp->devHdr, name, isioDrvNum) == ERROR){
		traceErr1("isioDevCreate: iosDevAdd failed\n");
		unlockBoard( (&brdDesc[board]) );
		return ERROR;
	}

	/* Initialize for select support */
	selWakeupListInit(&devDescp->wupList);

	/* Create and initialize semaphores: */
	Prove308((int *)PROVE308ADDR);
	devDescp->rx.lock = semBCreate(SEM_Q_FIFO, SEM_FULL);
	Prove308((int *)PROVE308ADDR);
	devDescp->tx.lock = semBCreate(SEM_Q_FIFO, SEM_FULL);

	Prove308((int *)PROVE308ADDR);
	devDescp->rx.rdy  = semBCreate(SEM_Q_FIFO, SEM_EMPTY);  /* not ready */
	Prove308((int *)PROVE308ADDR);
	devDescp->tx.rdy  = semBCreate(SEM_Q_FIFO, SEM_FULL);  /* ready	 */


	/* Initialize misc. fields in descriptor: */

	Prove308((int *)PROVE308ADDR);
	devDescp->rx.pendCmd = ISIO_C_NONE;
	Prove308((int *)PROVE308ADDR);
	devDescp->tx.pendCmd = ISIO_C_PUTCNT; /* simulate a completed tx cmd */

	Prove308((int *)PROVE308ADDR);
	devDescp->rxDataCnt = 0;

	Prove308((int *)PROVE308ADDR);
	devDescp->created = TRUE;

	unlockBoard( (&brdDesc[board]) );

	return OK;
}


/*****************************************************************************
*
* isioUseTyLib - set up a channel to use tyLib
*
* This function sets up the channel to use tyLib, and makes a
* tyDevInit call with the read and write buffer sizes supplied as
* parameters.
*
* This function should be called after isioDevCreate, and
* before any descriptor is opened to the channel.
*
* All subsequent I/O requests directed to this this channel will be
* routed to tyLib.
*
* After a channel has been set up to use tyLib, it will not
* respond to any of the ISIO-specific ioctl codes.
*
* RETURNS:
* OK, or ERROR if device not created, or memory allocation error
*/
STATUS	isioUseTyLib (channel, board, rdBufSize, wrtBufSize)
UINT	channel;	/* Physical channel (0..7)	   */
UINT	board;		/* Board index		   */
UINT	rdBufSize;  /* Read buffer size, in bytes  */
UINT	wrtBufSize; /* Write buffer size, in bytes */
{
ISIO_DEV_DESC	*devDescp;

	if (isioDrvNum <= 0){
		traceErr1("isioUseTyLib: driver not installed\n");
		errnoSet(S_ioLib_NO_DRIVER);
		return ERROR;
	}

	if ((channel < 0) || (channel >= ISIO_NUM_CHAN)){
		traceErr1("isioUseTyLib: bad channel number\n");
		errnoSet(S_isio_PARAM);
		return ERROR;
	}

	if ((board < 0) || (board > nbrBoards-1)){
		traceErr1("isioUseTyLib: bad board index\n");
		errnoSet(S_isio_PARAM);
		return ERROR;
	}


	devDescp = &brdDesc[board].devDesc[channel];
	if (! devDescp->created){
		traceErr1("isioUseTyLib: device not created\n");
		return ERROR;
	}


	Prove308((int *)PROVE308ADDR);
	devDescp->tyDevp = (ISIO_TY_DEV *) calloc( sizeof(ISIO_TY_DEV), 1 );
	Prove308((int *)PROVE308ADDR);
	if (devDescp->tyDevp == NULL){
		traceErr1("isioUseTyLib: calloc failed\n");
		return ERROR;
	}

	Prove308((int *)PROVE308ADDR);
	devDescp->tyDevp->devDescp = devDescp;
	Prove308((int *)PROVE308ADDR);
	devDescp->tyDevp->tyDev.devHdr = devDescp->devHdr;

	Prove308((int *)PROVE308ADDR);
	if (tyDevInit(&(devDescp->tyDevp->tyDev), rdBufSize, wrtBufSize, (FUNCPTR) isioStartup ) != OK)	{
		Prove308((int *)PROVE308ADDR);
		free(devDescp->tyDevp);
		traceErr1("isioUseTyLib: tyDevInit failed\n");
		return ERROR;
	}

	Prove308((int *)PROVE308ADDR);
	devDescp->modFlags |= ISIO_M_TYLIB;
/* Don't start up receiver until the channel is opened... */
/* //- startAsynchCmd(&devDescp->rx, ISIO_C_GETCH); -// */
/* //<-- - 01c// */

	return OK;
}
/* //<-- + 01b// */


/*==========================================================================*/
/*			Utility Routines					*/
/*==========================================================================*/

/*
 These routines can be used from the VxWorks command shell
 to perform various diagnostics etc. on ISIO boards.
*/

/*****************************************************************************
*
* isioShow	 - show installed ISIO boards etc.
*
* This routine shows information about the ISIO driver,
* and about installed ISIO boards. For each installed
* board the following information is showed: board address,
* interrupt vectors used, VME interrupt levels used, and
* created devices. For example:
*
*	 -> isioShow
*	 driver number: 8
*
*	 board  address   vectors	  levels   devices
*	 -----  --------  ---------   -------  --------
*	 0      80000000  200         5        0123....
*	 1      90000000  204         4        01..45..
*
*	 (2 free board descriptors(s))
*
* RETURNS:
* OK, or ERROR if driver not installed
*
* SEE ALSO: isioProbe(2)
*/
STATUS	isioShow ()
{
	int  brdI;
	int  n;

	if (isioDrvNum <= 0)
	{
		printf("isioShow: driver not installed\n");
		return ERROR;
	}

	printf("\ndriver number: %d\n", isioDrvNum);

	if (nbrBoards == 0)
		printf("no boards installed\n");
	else
	{
		printf("board  address   vectors   levels   devices\n");
		printf("-----  --------  --------  -------  --------\n");
		/*		xx   xxxxxxxx     xxx  x  xxxxxxxx*/

		for (brdI = 0; brdI < nbrBoards; brdI++)
		{
			printf("%5d  %08X ", brdI, (UINT32)brdDesc[brdI].isio);
			printf("%7d", brdDesc[brdI].intVec);

			printf(" ");
			printf("%6d", brdDesc[brdI].intLvl);

			printf("      ");
			for (n = 0; n < ISIO_NUM_CHAN; n++)
			{
				if (brdDesc[brdI].devDesc[n].created)
					printf("%d", n);
				else
					printf(".");
			}

			printf("\n\n");
		}
	}

	printf("(%d free board descriptor(s))\n", MAX_BOARDS - nbrBoards);

	return OK;
}


/*****************************************************************************
*
* isioProbe	 - probe ISIO board
*
* This is an ISIO inspection routine, to be used for
* test and verification purposes; a check is made that
* the ISIO board can be accessed, and if so, a print-
* out of the ISIO registers is made.
*
* Please refer to the ISIO documentation for a
* description of the ISIO registers.
*
* RETURNS:
* OK, or ERROR if failed to access board
*
* SEE ALSO: isioShow(2)
*/
STATUS	isioProbe (address)
	UINT32 	address;	/* (local) address of ISIO-2 board */
{
	ISIO_BOARD	  *isio;
	int		  chan;

	isio = (ISIO_BOARD *) address;

	if (probeBoard(isio) == ERROR)
	{
		printf("isioProbe: can not access board at 0x%X\n", address);
		return ERROR;
	}

	printf("ISIO at 0x%X -\n", address);

	printf("  status:   0x%04hX", isio->status & 0x700);

	if (!(isio->status & 0x400))		printf(" RESET");
	if (!(isio->status & 0x200))		printf(" HALT");
	if (!(isio->status & 0x100))		printf(" WATCHDOG");
	printf("\n");

	printf("  version:  0x%04hX\n", isio->version);
	printf("  abortCh:  0x%04hX\n", isio->abortChan);
	printf("  sysfail:  0x%04hX\n", isio->sysfail);
	printf("  revision: 0x%04hX\n", isio->revision);

	printf("  echo:	 0x%04hX  ", isio->echoFlags);
	printFlags(isio->echoFlags);

	printf("  parity:   0x%04hX  ", isio->parityFlags);
	printFlags(isio->parityFlags);

/***********
	printf("  break:	0x%04hX  ", isio->breakFlags);
	printFlags(isio->breakFlags);

	printf("  framing:  0x%04hX  ", isio->framingFlags);
	printFlags(isio->framingFlags);
***********/

	printf("\n	   Rx	   Tx\n");

	for (chan = 0; chan < ISIO_NUM_CHAN; chan++){
		printf("  ch [%d]:  0x%04hX   0x%04hX   (",
		   chan,
		   (isio->cmd[chan*2].cmdStat & 0xFFFF),
		   (isio->cmd[(chan*2)+1].cmdStat & 0xFFFF) );

		if (isio->cmd[chan*2].cmdStat & ISIO_CMD_DONE)
			printf("idle");
		else
			printf("command in progress");

		printf(" / ");

		if (isio->cmd[(chan*2)+1].cmdStat & ISIO_CMD_DONE)	printf("idle");
		else			printf("command in progress");

		printf(")\n");
	}

	return OK;
}


