
#ifndef _LOGTASK_H_
#define _LOGTASK_H_

#define	LOG_TYPE_GEN		0
#define LOG_TYPE_IF			1
#define LOG_TYPE_IO			2
#define LOG_TYPE_MAX		2

#define LOG_TYPE_EXT_ALL		130	// ALL TYPE

#define LOG_BIT_ERR		0x01
#define LOG_BIT_IND		0x02
#define LOG_BIT_HEX		0x04
#define LOG_BIT_DBG		0x08
#define LOG_BIT_MAX		0x0F

#define __FILENAME__ (strrchr(__FILE__,'/')+1)
#define Log(fmt, args...) LogTypePrint(LOG_BIT_IND, LOG_TYPE_GEN, __FILENAME__, __LINE__, fmt, ## args)
#define LogErr(type, fmt, args...) LogTypePrint(LOG_BIT_ERR, type, __FILENAME__, __LINE__, fmt, ## args)
#define LogInd(type, fmt, args...) LogTypePrint(LOG_BIT_IND, type, __FILENAME__, __LINE__, fmt, ## args)
#define LogDbg(type, fmt, args...) LogTypePrint(LOG_BIT_DBG, type, __FILENAME__, __LINE__, fmt, ## args)
#define LogHex(type, pBuffer, len, fmt, args...) LogHexPrint(type, pBuffer, len, __FILENAME__, __LINE__, fmt, ## args)

int LogTypePrint(int bit, int type, const char *file, const int line, const char *format,...);
int LogHexPrint(int type, unsigned char *pBuffer, unsigned short len, const char *file, const int line, const char *format,...);
int myLogInit();

extern "C" cfg();
#endif
