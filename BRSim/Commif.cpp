/* -------------------------------------------------------------------------+
|  CommIf.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     Commif.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */

/* standard c library */
#include <stdio.h>
#include <string.h>

/* vxWorks library */
#include "selectLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "msgQLib.h"
#include "sockLib.h"

/* user c library */
#include "projdef.h"
#include "LogTask.h"
#include "brdctrl.h"
#include "commif.h"

/* ----------------------------------------------------------------------- */

/* ----------------------------------------------------------------------- */
#define	PROCESS		STATUS
#define	DATA_SIZE	42
#define BODY_SIZE	34
#define IODATA_SIZE	32
#define DLE			0x10
#define STX			0x02
#define ETX			0x03
#define OPCODE_IO_SCAN	0x82
#define OPCODE_IO_CTRL	0x83


#define LOCAL_IF_ADDR		"192.168.10.10"
#define REMOTE_IF_ADDR		"192.168.10.11"
#define IF_UDP_PORT			5100


/* ----------------------------------------------------------------------- */
extern "C" STATUS EnetAttach(int unit, char *ipBuf, int netmask);
extern unsigned char _pOutBuffer[];	// 256 byte (2048 IO port = 256 * 8) //
extern unsigned char _pInBuffer[];	// 256 byte (2048 IO port = 256 * 8) //

struct      timeval selTimeOut_IF;
struct      fd_set  readFds_IF, tmpFds_IF;			/* for other CBI */

PROCESS ComIF_SendDrv(void);
PROCESS ComIF_RecvDrv(void);

/* ----------------------------------------------------------------------- */

Comm_IF ComIF;

/* ----------------------------------------------------------------------- */
Comm_IF::Comm_IF()
{
	m_nTxSeq 	= 0x01;

    m_CRC.GenCrcTable();    
}

Comm_IF::~Comm_IF() 
{
}

USHORT Comm_IF::Initialize() 
{
	EnetAttach(1, LOCAL_IF_ADDR, 24);

	bzero((char*)&m_localAddr, sizeof(m_localAddr));
	bzero((char*)&m_remoteAddr, sizeof(m_remoteAddr));

	m_localAddr.sin_family	= AF_INET;
	m_remoteAddr.sin_family	= AF_INET;
	m_localAddr.sin_port	= htons(IF_UDP_PORT);
	m_remoteAddr.sin_port	= htons(IF_UDP_PORT);
	m_localAddr.sin_addr.s_addr		= inet_addr(LOCAL_IF_ADDR); 
	m_remoteAddr.sin_addr.s_addr	= inet_addr(REMOTE_IF_ADDR);  

	if((m_nfd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_IF, "Socket Create Error!!");
		return (ERROR);
	}

	if(bind(m_nfd, (struct sockaddr *)&m_localAddr, (int)(sizeof(m_localAddr))) == ERROR)
	{
		LogErr(LOG_TYPE_IF, "Socket Bind Error!!");
		return (ERROR);
	}

    taskSpawn("ComIF_RecvDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComIF_RecvDrv,0,0,0,0,0,0,0,0,0,0);
    taskSpawn("ComIF_SendDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComIF_SendDrv,0,0,0,0,0,0,0,0,0,0);

	return (OK);
}

BYTE Comm_IF::ProcessData(BYTE *pRxData, USHORT nRxSize) 
{
    BYTE low_crc=0, high_crc=0;
 	USHORT	nBodyLen;

	if(nRxSize != DATA_SIZE) 
    {
		LogErr(LOG_TYPE_IF, "Data Size Missmatched.");
		return 0;
	}

	if((pRxData[0] != DLE) || (pRxData[1] != STX))
	{
		LogErr(LOG_TYPE_IF, "DLE/STX Check Error.");
		return 0;
	}

	if((pRxData[nRxSize-2] != DLE) || (pRxData[nRxSize-1] != ETX))
	{
		LogErr(LOG_TYPE_IF, "DLE/ETX Check Error.");
		return 0;
	}

	nBodyLen = *(USHORT*)&pRxData[2];
	if(nBodyLen != BODY_SIZE)
	{
		LogErr(LOG_TYPE_IF, "Invalid Body Length Data!! nBodyLen = %d", nBodyLen);
		return 0;
	}

	LogHex(LOG_TYPE_IF, pRxData, nRxSize, "Recv [%d] <--- PC", nRxSize);

	m_CRC.CrcGen(nRxSize-4, pRxData, &low_crc, &high_crc);

	// Message trailer check
	if((low_crc != pRxData[nRxSize-4]) || (high_crc != pRxData[nRxSize-3]))
	{
		LogErr(LOG_TYPE_IF, "CRC Check Error. CALC_CRC[%02X%02X] != RECV_CRC[%02X%02X]", 
				low_crc, high_crc, pRxData[nRxSize-4], pRxData[nRxSize-3]);
		return 0;
	}

	if(pRxData[5] == OPCODE_IO_CTRL)
	{
		// Memory Copy
		memcpy(_pOutBuffer, &pRxData[6], FDOM_MAX_QTY * 4); 
	}

	return 1;
}

USHORT Comm_IF::MakeData(BYTE *pTxBuf, USHORT nTxBufSize)
{
	USHORT 	nPos = 0;
	BYTE 	low_crc=0, high_crc=0;

	memset(pTxBuf, 0x00, DATA_SIZE);

	pTxBuf[nPos++] = DLE;
	pTxBuf[nPos++] = STX;
	*(USHORT*)&pTxBuf[nPos] = (USHORT)BODY_SIZE;
	nPos += 2;

	// Tx Sequence Initialize
	if(m_nTxSeq == 0xFF)
	{
		m_nTxSeq = 0x01;
	}

	pTxBuf[nPos++] = m_nTxSeq++; 
	pTxBuf[nPos++] = OPCODE_IO_SCAN; 

	// Memory Copy
	memcpy(&pTxBuf[nPos], _pInBuffer, FDIM_MAX_QTY * 4); 
	nPos += IODATA_SIZE;

    m_CRC.CrcGen(nPos, pTxBuf, &low_crc, &high_crc);

	pTxBuf[ nPos++ ] = low_crc;		// crc low byte
	pTxBuf[ nPos++ ] = high_crc;		// crc high byte
	pTxBuf[ nPos++ ] = DLE;
	pTxBuf[ nPos++ ] = ETX;

	return nPos;
}

/* ----------------------------------------------------------------------- */
#define MAX_BUFFER_SIZE		4096

PROCESS ComIF_RecvDrv(void)
{
	int 	nfd;
    int     width;
    STATUS  status;

	USHORT	nRcvdSize;
    BYTE	result;
    BYTE    readbuf[MAX_BUFFER_SIZE];

	width = 0;
	nfd = ComIF.m_nfd;

    selTimeOut_IF.tv_sec  = 1;
    selTimeOut_IF.tv_usec = 0;

    FD_ZERO(&readFds_IF);

    FD_SET(nfd, &readFds_IF);
    width = max(nfd, width);
	width++;

    while( 1 ) 
	{
        tmpFds_IF = readFds_IF;

        status = select ( width, &tmpFds_IF, NULL, NULL, &selTimeOut_IF);
		if (status == ERROR) 
		{
			LogErr(LOG_TYPE_IF, "Select Error.");
		}
		else if (status == 0) 
		{
			LogErr(LOG_TYPE_IF, "Select TIMEOUT!!!!!");
			continue;
		}
		else 
		{
	      	if(FD_ISSET(nfd, &readFds_IF)) 
			{
				nRcvdSize = recvfrom(nfd, (char*)readbuf, (size_t)MAX_BUFFER_SIZE, 0, 
								 (struct sockaddr *)&ComIF.m_remoteAddr, (int*)(sizeof(ComIF.m_remoteAddr)));

				result = ComIF.ProcessData(readbuf, nRcvdSize);
				if(result) 
				{
					LogDbg(LOG_TYPE_IF, "Receive Success.!!!!!!!!");
				}
          	}
          	else 
			{
				LogErr(LOG_TYPE_IF, "Cannot Read Data.");
          	}
		}
	}
}

PROCESS ComIF_SendDrv(void)
{
	int 	nfd;
	USHORT	nSendSize, nSentSize;
    BYTE    sendbuf[MAX_BUFFER_SIZE];

	nfd = ComIF.m_nfd;

    while( 1 ) 
	{
		nSendSize = ComIF.MakeData(sendbuf, MAX_BUFFER_SIZE);

		nSentSize = sendto(nfd, (char*)sendbuf, nSendSize, 0, 
							(struct sockaddr *)&ComIF.m_remoteAddr, (int)(sizeof(ComIF.m_remoteAddr)));

		LogHex(LOG_TYPE_IF, sendbuf, nSentSize, "Sent [%d] ---> PC", nSentSize);

		taskDelay(150);
	}
}
/* ----------------------------------------------------------------------- */

