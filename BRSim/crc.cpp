/* -------------------------------------------------------------------------+
|  CRC.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CRC.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#include "crc.h"

unsigned int Ccrc::GenCrcTable()
{
	int	 temp;

	union {
		int	i;
		struct {
                        unsigned extra : 24;
			unsigned i8 : 1;
			unsigned i7 : 1;
			unsigned i6 : 1;
			unsigned i5 : 1;
			unsigned i4 : 1;
			unsigned i3 : 1;
			unsigned i2 : 1;
			unsigned i1 : 1;
		} bit;
	} iun;

	union {
		unsigned int entry_num;
		struct {
			unsigned reserved : 16;
			unsigned b16: 1;
			unsigned b15: 1;
			unsigned b14: 1;
			unsigned b13: 1;
			unsigned b12: 1;
			unsigned b11: 1;
			unsigned b10: 1;
			unsigned b9 : 1;
			unsigned b8 : 1;
			unsigned b7 : 1;
			unsigned b6 : 1;
			unsigned b5 : 1;
			unsigned b4 : 1;
			unsigned b3 : 1;
			unsigned b2 : 1;
			unsigned b1 : 1;
		} entrybit;
	} entryun;

	for (iun.i = 0; iun.i < 256; iun.i++) {
		entryun.entry_num = 0;
		temp = (iun.bit.i7 ^ iun.bit.i6 ^iun.bit.i5 ^
				iun.bit.i4 ^iun.bit.i3 ^iun.bit.i2 ^iun.bit.i1);
		entryun.entrybit.b16 = (iun.bit.i8 ^ temp);
		entryun.entrybit.b15 = (temp);
		entryun.entrybit.b14 = (iun.bit.i8 ^ iun.bit.i7);
		entryun.entrybit.b13 = (iun.bit.i7 ^ iun.bit.i6);
		entryun.entrybit.b12 = (iun.bit.i6 ^ iun.bit.i5);
		entryun.entrybit.b11 = (iun.bit.i5 ^ iun.bit.i4);
		entryun.entrybit.b10 = (iun.bit.i4 ^ iun.bit.i3);
		entryun.entrybit.b9  = (iun.bit.i3 ^ iun.bit.i2);
		entryun.entrybit.b8  = (iun.bit.i2 ^ iun.bit.i1);
		entryun.entrybit.b7  = (iun.bit.i1);
		entryun.entrybit.b1  = (iun.bit.i8 ^ temp);
		crc_table[iun.i] = entryun.entry_num;
	} /* for End */
	return (1);
} /* CRCtable() */

unsigned int Ccrc::CrcGen(int len, unsigned char *ptr, unsigned char *low_crc, unsigned char *high_crc)
{
	register  int cnt;
	unsigned crcc;

	for (cnt = crcc = 0 ; cnt < len ; cnt++,ptr++)
		crcc = (crcc >> 8) ^ crc_table[((crcc ^ *ptr) & 0x00ff)];

	// crc2 상위 바이트
	*high_crc = crc2 = (crcc >> 8) & 0x00ff;
	//crc1 하위바이트
	*low_crc  = crc1 = crcc & 0x00ff;

	return (1);
}