#ifndef _FLASH402_H
#define _FLASH402_H

#define WORD unsigned short

class CFlash {

public:
	WORD m_pData[ 0x8000 ];	// 64K BYTE
	unsigned long m_lBaseAddress;
	int m_nReceived;

	CFlash();
	int Read();
	int Write();
	WORD FullStatusCheck();
	int BlockErase( int nBlock );
};

extern CFlash MyFlash;

#endif

