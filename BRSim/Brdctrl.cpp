
#include <stdio.h>
#include "taskLib.h"
#include "vxLib.h"
#include "string.h"
#include "wdLib.h"
#include "tickLib.h"
#include "signal.h"
#include "setjmp.h"

#include "projdef.h"
#include "LogTask.h"
#include "MainTask.h"
#include "brdctrl.h"

/*******************************************************************/
/* type define */
typedef unsigned char uchar;
typedef unsigned int uint;

//////////////////////////////////////////////////////////////////////////////
/// VME IO Board control class
//////////////////////////////////////////////////////////////////////////////

#define IO_CONF_ADRS	0xf4000070
#define IO_DATA_LOW		0xf4000080
#define IO_DATA_HIGH	0xf4000082
#define NEXT_IO_ADRS	0x00000100
#define AMCODE_WRITE 	0xfd000000	// 0xff

/*---------------------------------------------------------------------- 
  MACRO fuctions
----------------------------------------------------------------------*/
#define getConf(x) 				(*(unsigned short*)(IO_CONF_ADRS + x*NEXT_IO_ADRS))
#define getHighdata(x) 			(*(unsigned short*)(IO_DATA_HIGH + x*NEXT_IO_ADRS))
#define getLowdata(x) 			(*(unsigned short*)(IO_DATA_LOW  + x*NEXT_IO_ADRS))
#define writeHighData(x, data)	(*(unsigned short*)(IO_DATA_HIGH + x*NEXT_IO_ADRS) = (unsigned short)data)
#define writeLowData(x, data)	(*(unsigned short*)(IO_DATA_LOW  + x*NEXT_IO_ADRS) = (unsigned short)data)

/*******************************************************************/
#define OK		0
#define ERROR		(-1)
#define FOREVER	for (;;)

/*******************************************************************/
/* defined extern */
extern unsigned char _pOutBuffer[];	// 256 byte (2048 IO port = 256 * 8) //
extern unsigned char _pInBuffer[];	// 256 byte (2048 IO port = 256 * 8) //

/*******************************************************************/
int ScanTask_GO;

unsigned short EXD_low, EXD_high;
int PortErrCnt, CardErrCount = 0;

CBoardCtrl EIS_IO;
jmp_buf location;  	/* BUS ERROR를 처리하기위해 사용됨 */

/***********************************************************************/

CBoardCtrl::CBoardCtrl() 
{   
	m_bError = 0;

	memset(m_pInBuffer, 0x00, MAX_IOBUFFER_SIZE);			//
	memset(m_pOutBuffer, 0x00, MAX_IOBUFFER_SIZE);			// interlocking result output data buffer.
}

CBoardCtrl::~CBoardCtrl() 
{
}



short int CBoardCtrl::OutputAll() 
{
	memcpy(m_pOutBuffer + (FDOM_START_INDEX * 4), _pOutBuffer, (FDOM_MAX_QTY * 4));
}



short int CBoardCtrl::InputAll() 
{
	memcpy(_pInBuffer, m_pInBuffer + (FDIM_START_INDEX * 4), (FDIM_MAX_QTY * 4));
}



/*---------------------------------------------------------------------- 
* 1. prototype : void sigHandle(int sigNum)
*
* 2. 함수 설명 :
*         BUS ERROR발생시 원인을 제공한 함수로 되돌아가서
*         프로그램을 계속 수행하도록 한다.
----------------------------------------------------------------------*/

void sigHandle(int sigNum)
{
	longjmp(location, ERROR);
}

// 입력 오류 시험용 코드... 2003.3.18.
#if 0
BYTE	_In0Card = 0xFF;
BYTE    _CmpData = 0x00;
int 	_ddd=0;
int 	_In0Count = 0;
#endif // test code, <<E.


short int CBoardCtrl::ScanIO() 
{
	int whereFrom; 
	short int i, byteloc, nRealID;
	unsigned short int nIllegalCard;
	unsigned short int nIOdata_low, nIOdata_high;

   	char *addr;
	char data[4];

	char *pd0 = ((char*)&nIOdata_low)+1;
	char *pd1 = (char*)&nIOdata_low;
	char *pd2 = ((char*)&nIOdata_high)+1;
	char *pd3 = (char*)&nIOdata_high;

	m_bError = 0;

// Processing for FDIM Board. 
// Read FDIM Board. 
	for(i = MAX_BOARD_NUM - 1 ; i >=0 ; i--) 
	{
		// input board checking.
		if(m_pBoardInfo[i] & IOTYPE_FDIM) 
		{
			byteloc = i * 4;
			nRealID = m_pRealID[i] & 0x7F;

	 		addr = (char*)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar)nRealID);

	 		// check to a FDIM baord existing.
			whereFrom = vxMemProbe(addr, VX_READ, 2, data);
			if (whereFrom != OK)
			{
				m_pBoardInfo[i] = 0x10;	// Error

				// Is not empty I/O slot ?
				if(!(m_pRealID[i] & 0x80)) 
				{
					m_bError = 1;
				}
			}
			else 
			{
				m_pBoardInfo[i] = 0x11; // Normal

				nIOdata_low  = ~getLowdata( nRealID );	// read 16 bit low-data of a input board.
				nIOdata_high = ~getHighdata( nRealID );	// read 16 bit high-data of a input board.

				LogErr(LOG_TYPE_IO, "FDIM[%d] = [%02X,%02X,%02X,%02X]", i, *pd0, *pd1, *pd2, *pd3);
				
				m_pInBuffer[byteloc]   = *pd0;			// set buffer to read data(bit0  ~ bit7)
				m_pInBuffer[byteloc+1] = *pd1;			// set buffer to read data(bit8  ~ bit15)
				m_pInBuffer[byteloc+2] = *pd2;			// set buffer to read data(bit16 ~ bit23)
				m_pInBuffer[byteloc+3] = *pd3;			// set buffer to read data(bit24 ~ bit31)
			}
		}
	}
	

// Write FDOM Board. 
	for(i = 0; i < MAX_BOARD_NUM ; i++) 
	{
		if(m_pBoardInfo[i] & IOTYPE_FDOM)
		{
			byteloc = i * 4;
			nRealID = m_pRealID[i] & 0x7F;

	 		addr = (char *)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar)nRealID);

			whereFrom = vxMemProbe(addr,VX_READ,2,data);
			if (whereFrom != OK)
			{
				m_pBoardInfo[i] = 0x20;	// Error

				// Is not empty I/O slot ?
				if(!(m_pRealID[i] & 0x80)) 
				{
					m_bError = 1;
				}
			}
			else 
			{
				m_pBoardInfo[i] = 0x21; // Normal

				// write output port.
				*pd0 = m_pOutBuffer[byteloc];
				*pd1 = m_pOutBuffer[byteloc+1];
				*pd2 = m_pOutBuffer[byteloc+2];
				*pd3 = m_pOutBuffer[byteloc+3];

				LogErr(LOG_TYPE_IO, "FDOM[%d] = [%02X,%02X,%02X,%02X]", i, *pd0, *pd1, *pd2, *pd3);

				writeLowData( nRealID, nIOdata_low );
				writeHighData( nRealID, nIOdata_high );		
			}
		}
	}

// Write FDBM Board. 
	for(i = 0; i < MAX_BOARD_NUM ; i++) 
	{
		if(m_pBoardInfo[i] & IOTYPE_FDBM)
		{
			byteloc = i * 4;
			nRealID = m_pRealID[i] & 0x7F;

	 		addr = (char *)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar)nRealID);

			whereFrom = vxMemProbe(addr,VX_READ,2,data);
			if (whereFrom != OK)
			{
				m_pBoardInfo[i] = 0x40;	// Error

				// Is not empty I/O slot ?
				if(!(m_pRealID[i] & 0x80)) 
				{
					m_bError = 1;
				}
			}
			else 
			{
				m_pBoardInfo[i] = 0x41; // Normal

				// write output port.
				*pd0 = m_pOutBuffer[byteloc];
				*pd1 = m_pOutBuffer[byteloc+1];
				*pd2 = m_pOutBuffer[byteloc+2];
				*pd3 = m_pOutBuffer[byteloc+3];

				LogErr(LOG_TYPE_IO, "FDOM[%d] = [%02X,%02X,%02X,%02X]", i, *pd0, *pd1, *pd2, *pd3);

				writeLowData( nRealID, nIOdata_low );
				writeHighData( nRealID, nIOdata_high );		
			}
		}
	}

	return m_bError;
}

/* ------------------------------------------------------------------------- */

int	 g_nIOScanRunTimeTicked = 0;	/* time tick for io_scan run function */

void rSetIOScanRunTimeTick() 
{
	g_nIOScanRunTimeTicked = 1;
}

void rClearIOScanRunTimeTick() 
{
	g_nIOScanRunTimeTicked = 0;
}

void WaitOnSyncForIOScanRun() 
{
	int n;
	
	for(n=0; n<MAX_WAITTICK_IOSCAN; n++) {
		if ( g_nIOScanRunTimeTicked ) break;
		taskDelay(1);
	}
}

void tScan()
{
//	ULONG ulStart, ulEnd, ulGap;

	char  *fn = "tScan";

    WDOG_ID wid_IOScanRun;

    wid_IOScanRun = wdCreate();

#ifndef _NOT_AMCODE
//	(*(unsigned char*)AMCODE_WRITE) = 0xff;
	printf("\nAMCODE");
#endif // _NOT_AMCODE

	FOREVER
	{
//		ulStart = tickSet(0);

		rClearIOScanRunTimeTick();
	    wdStart (wid_IOScanRun, MAX_WAITTICK_IOSCAN, (FUNCPTR)rSetIOScanRunTimeTick, 0);	//600 => 1 sec

		EIS_IO.ScanIO();
		taskDelay(1);

		WaitOnSyncForIOScanRun();
		
//		ulEnd = tickGet();
//		ulGap = ulEnd - ulStart;

	} /* FOREVER */

	wdCancel( wid_IOScanRun );

}


void CBoardCtrl::SetBoardInfo()
{
	//unsigned char *pInfoTable = pInterlockTable + 0x2A;	// offset of IO info
	int i;

	memset(m_pRealID, 0xFF, sizeof(m_pRealID));

	// FDIM
	for(i = 0 ; i < FDIM_MAX_QTY ; i++)
	{
		m_pRealID[FDIM_START_INDEX + i]	= i + FDIM_START_VALUE;
		printf("FDIM[%d] = 0x%X\n", i, i + FDIM_START_VALUE);
	}

	// FDOM
	for(i = 0 ; i < FDOM_MAX_QTY ; i++)
	{
		m_pRealID[FDOM_START_INDEX + i]	= i + FDOM_START_VALUE;
		printf("FDOM[%d] = 0x%X\n", i, i + FDOM_START_VALUE);
	}

	// FDBM
	for(i = 0 ; i < FDBM_MAX_QTY ; i++)
	{
		m_pRealID[FDBM_START_INDEX + i]	= i + FDBM_START_VALUE;
		printf("FDBM[%d] = 0x%X\n", i, i + FDBM_START_VALUE);
	}

	memset(m_pBoardInfo, 0x00, sizeof(m_pBoardInfo));

	// FDIM
	for(i = 0 ; i < FDIM_MAX_QTY ; i++)
	{
		m_pBoardInfo[FDIM_START_INDEX + i]	= 0x11;
	}

	// FDOM
	for(i = 0 ; i < FDOM_MAX_QTY ; i++)
	{
		m_pBoardInfo[FDOM_START_INDEX + i]	= 0x21;
	}

	// FDBM
	for(i = 0 ; i < FDBM_MAX_QTY ; i++)
	{
		m_pBoardInfo[FDBM_START_INDEX + i]	= 0x41;
	}

	memset(m_pOutBuffer, 0, MAX_IOBUFFER_SIZE);
}

