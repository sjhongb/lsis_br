/* -------------------------------------------------------------------------+
|  CommIf.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CommIf.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _COMMIF_H
#define _COMMIF_H

/* vxWorks library */
#include "inetLib.h"

/* user c library */
#include "crc.h"

/* ------------------------------------------------------------------------- */
class Comm_IF 
{
public:
	int m_nfd;				/* file discriptor */
	int m_nTxSeq;

	Ccrc 	m_CRC;

public:
	Comm_IF();
	~Comm_IF();
	
	USHORT Initialize();
	BYTE ProcessData(BYTE *pRxData, USHORT nRxSize);
	USHORT MakeData(BYTE *pTxBuf, USHORT nTxBufSize);

	struct sockaddr_in  m_localAddr;
	struct sockaddr_in  m_remoteAddr;
};

/* ------------------------------------------------------------------------- */
extern Comm_IF ComIF;

/* ------------------------------------------------------------------------- */

#endif // _COMMIF_H
