/* -------------------------------------------------------------------------+
|  Brdctrl.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     Brdctrl.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _BRDCTRL_H
#define _BRDCTRL_H

/* ------------------------------------------------------------------------- */
#define MAX_BOARD_NUM 		64
#define MAX_IOBUFFER_SIZE 	(MAX_BOARD_NUM * 4)		// 64

#define IOTYPE_FDIM			0x10
#define IOTYPE_FDOM			0x20
#define IOTYPE_FDBM			0x40

#define FDIM_MAX_QTY		17
#define FDOM_MAX_QTY		13
#define FDBM_MAX_QTY		12

#define FDIM_START_INDEX	0
#define FDOM_START_INDEX	FDIM_MAX_QTY
#define FDBM_START_INDEX	FDIM_MAX_QTY + FDOM_MAX_QTY

#define FDIM_START_VALUE	0x00
#define FDOM_START_VALUE	0x40
#define FDBM_START_VALUE	0x50

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

class CBoardCtrl 
{
protected:
	int m_bIsSub;
	int m_bError;
	//
public:	
	unsigned char m_pBoardInfo[MAX_BOARD_NUM];		// current status of I/O board
	unsigned char m_pRealID[MAX_BOARD_NUM];		// accessing ID of I/O board

protected:
	unsigned char m_pInBuffer[MAX_IOBUFFER_SIZE];
	unsigned char m_pOutBuffer[MAX_IOBUFFER_SIZE];			// interlocking result output data buffer.

public:
	CBoardCtrl();		
	~CBoardCtrl();

	void SetBoardInfo(/* unsigned char *pInfoTable */);

	short int OutputAll();
	short int InputAll();
	short int ScanIO();
};

/* ------------------------------------------------------------------------- */
extern unsigned char _pInBuffer[];
extern unsigned char _pOutBuffer[];
extern CBoardCtrl EIS_IO;
#endif

/* ------------------------------------------------------------------------- */
