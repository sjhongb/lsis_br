/* hwfuncdef.h : header file.
 *
 */
 
#ifndef  _HWFUNCDEF_H_
#define  _HWFUNCDEF_H_

/****************************************************************************
*	Version Information														*
*	1. BOARD_NAME : Board Name												*
*	2. BOARD_REV_NUM : Board Revision Num - Must be match with PCB NUM		*
*	3. MONITOR_VERSION : Monitor Program Version							*
****************************************************************************/

#define	BOARD_NAME			"KVME402"
#define	BOARD_REV_NUM		"1.0"
#define MONITOR_VERSION		"1.0.0"


/****************************************************************************
*	ETC Define																*
****************************************************************************/
#define	MAX_DIP_SW_NUM	4				/* dip switch count */

/*
#define FORMAT_BYTE 	1 
#define FORMAT_HALF 	2
#define FORMAT_WORD 	3
*/

#define XR      		0xFFFFFFFF		/* define for test board */
#define TEST_RAM_NORMAL 1
#define TEST_RAM_SLOW   2

#define OPSET 			0x8000			/* 32KByte */

#define FLASH_0  		0
#define FLASH_1  		1
#define FLASH_MAC_ID  	31				/* */


/********************************************************
* 	kvme402 rev 1.0	Memory Map							*
********************************************************/
/*	1. SDRAM AREA	*/
#define ST_DRAM 			0x00400000
#define EN_DRAM 			0x01FFFF00     /* 8MByte */

/*	2. VMEbus Extened AREA	*/
#define ST_VME32_ADDR 		0x10000000
#define EN_VME32_ADDR		0xEFFFFF00     /* 3GByte */

/*	3. VMEbus Standard AREA	*/
#define ST_VME24_ADDR 		0xF0000000
#define EN_VME24_ADDR		0xF0FFFF00     /* 16MByte */

/*	4. SRAM AREA	*/
#define ST_SRAM 			0xF1000000
#define EN_SRAM 			0xF11FFFF0     /* 2MByte */

#ifdef	NVRAM_128KBIT
/*	5. NVRAM AREA	*/
#define ST_NVRAM 			0xF2000000
#define EN_NVRAM 			0xF201FBF0  	/* 128KByte :1F400 */

#define	NVRAM_VME_EPT_VALUE	0x4321
#define	NVRAM_VME_EPT_ADDR	0xF201FF00
#endif

/*	5. NVRAM AREA	*/
#define ST_NVRAM 			0xF2000000
#define EN_NVRAM 			0xF207FBF0  	/* 128KByte :1F400 */

#define	NVRAM_VME_EPT_VALUE	0x4321
#define	NVRAM_VME_EPT_ADDR	0xF207FF00

/*	6. VMEbus SHORT AREA	*/
#define ST_VME16_ADDR 		0xF4000000
#define EN_VME16_ADDR		0xF400FF00     /* 64KByte */

/*	7. Error LED Off AREA	*/
#define	ERROR_LED_OFF_ADDR	0xf5000000

/*	8. VMEBus Enable AREA	*/
#define	VMEBUS_ENABLE_ADDR	0xf6000000

/*	9. Error LED On AREA	*/
#define	ERROR_LED_ON_ADDR	0xf7000000

/*	10. VMEbus Intrrupt Ack AREA	*/
#define	VMEBUS_INT_ACK_ADDR	0xf8000000

/*	11. Dip SW Read AREA	*/
#define	DIP_SW_READ_ADDR	0xf9000000

/*	12. FLASH AREA	*/
#define ST_FLASH 			0xFA000000
#define EN_FLASH			0xFA3FFFFF     /* 4MByte */
#define	FLASH_BASE			ST_FLASH

/*	13. VMEBus Interrupt Vector AREA	*/
#define VMEBUS_INT_VECTOR_ADDR	0xFB000000

/*	14. VMEBus Interrupt Request AREA	*/
#define VMEBUS_INT_Req_ADDR	0xFC000000

/*	15. IMMR AREA
#define IMMR	 			0xFF000000
*/

/*	16. EPROM AREA	*/
#define ST_EPROM 			0xFFF00000
#define EN_EPROM			0xFFFFFFFF     /* 1MByte */

/*****************************************************************************/

#endif // _HWFUNCDEF_H_
