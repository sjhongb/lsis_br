#include "vxWorks.h"
#include "stdio.h"
#include "fioLib.h"
#include "ioLib.h"
#include "iosLib.h"
#include "sysLib.h"
#include "stdlib.h"
#include "memLib.h"
#include "taskLib.h"
/* Created by gpande (2011/8/9 - 14:34:1) (Start) */
#include <semLib.h>		/* Semaphore를 사용하기 위해 include 함 */
/* Created by gpande (2011/8/9 - 14:34:4) (End) */

#include "serialMaster.h"

int vmeSioDrvNum=0;

int drvInstallFlag=0;

STATUS serialDrvMemInit(void);
void vmeSerialDrvInstall(void);
STATUS serialDrv(void);
STATUS vmeSioDevCreate(char *name, int num_chan);

VME_DRV_CHAN* vmeSioOpen(VME_DRV_CHAN *sioDevHeader, char *remainder, int mode);
STATUS vmeSioClose(VME_DRV_CHAN *sioDevHeader);
VME_DRV_CHAN* vmeSioWrite(register VME_DRV_CHAN *sioDevHeader, char *buffer, int nBytes);
int vmeSioRead(VME_DRV_CHAN *sioDevHeader, char *buffer, int maxBytes);
int vmeSioIoctl(VME_DRV_CHAN *sioDevHeader, int ioctlCode, int *arg);

/* Created by gpande (2011/7/26 - 12:21:19) (Start) */
/* 함수 선언이 없어서 compile시 warning 뜨는 부분을 위해 추가함 */
STATUS vxMemProbe(char * adrs, int mode, int length, char * pVal);
void bcopyBytes(char * source, char * destination, int nbytes);
int sp(FUNCPTR func, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9);
/* Created by gpande (2011/7/26 - 12:21:21) (End) */

/* Created by gpande (2011/8/9 - 14:31:33) (Start) */
/* Ioctl에서 사용할 각 serial channel semaphore array */
/*#define SERIALMASTER_ADD_SEMAPHORE 1*/

#ifdef SERIALMASTER_ADD_SEMAPHORE
static SEM_ID s_IoctlSemIDArr[NUM_OF_VMESERIAL_CHAN];
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
/* Created by gpande (2011/8/9 - 14:31:41) (End) */

STATUS 
serialDrvMemInit()
{
#if 0
	int i;
	unsigned short *adrs;
	adrs = (unsigned short*)VME_BASE_ADRS;
	
	for(i=0; i<0x80000; i++){
		*adrs = 0;
		adrs++;
	}
	adrs = (unsigned short*)VME_BASE_ADRS;
	for(i=0; i<0x80000; i++){
		if( *adrs != 0){
			return(ERROR);
		}else{
			adrs++;
		}
	}

	memset(adrs, 0x00, 0x100000);	
#endif
	return(OK);

}

void 
vmeSerialDrvInstall(void)
{
	int  num_chan;
	char name[20];
	int  status;
	
	char *adrs;
    char testW = 1;
    char testR;    
    
    unsigned short *drvSetAdrs;
    
    adrs = (unsigned char*)VME_BASE_ADRS+0x500;;
    drvSetAdrs = (unsigned short*)((VME_BASE_ADRS) + 0x510);
	
    if (vxMemProbe (adrs, VX_WRITE, 1, &testW) == OK)
        printf ("value %d written to adrs %x\n", testW, (int)adrs);
	else{
		printf ("value %d written to adrs %x vxMemProbe error!!\n", testW, (int)adrs);
	 	return;
	}
	
	
    if (vxMemProbe (adrs, VX_READ, 1, &testR) == OK)
        printf ("value %d read from adrs %x\n", testR, (int)adrs);
    else{
		printf ("value %d read from adrs  %x vxMemProbe error!!\n", testR, (int)adrs);
	 	return;
	}
	
	if (*drvSetAdrs == INSTALL_SET_VALUE){
	
		if(drvInstallFlag == 0){
			if(serialDrv() == ERROR){
    				printf("can not install driver\n");
    				drvInstallFlag = 0;
    			    return;
			}
			
			for(num_chan = 0; num_chan < NUM_OF_VMESERIAL_CHAN; num_chan++){
/* Modified by gpande (2011/7/11 - 16:38:26) (Start) */
/* Hard coding되어 있던 serial driver를 macro로 변경 */
#if 0
				sprintf(name, "/vme_xrsio/%d", num_chan);
#else
				sprintf(name, "%s%d", SERIALMASTER_SIO_DRIVER_NAME, num_chan);
#endif
/* Modified by gpande (2011/7/11 - 16:38:31) (End) */

				status = vmeSioDevCreate(name, num_chan);
				if(status == ERROR){
					drvInstallFlag = 0;
					return;
				}
			}
			drvInstallFlag = 1;
		}
	}else{
		printf("slave board serial task not install!!\n");
	}

	/* Created by gpande (2011/8/9 - 14:34:57) (Start) */
	/* 각 serial channel semaphore 초기화 */
	#ifdef SERIALMASTER_ADD_SEMAPHORE
	{
		int iInx = 0;
		
		for(; iInx < NUM_OF_VMESERIAL_CHAN; iInx++) {
			s_IoctlSemIDArr[iInx] = semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
		}
	}
	#endif	/* SERIALMASTER_ADD_SEMAPHORE */
	/* Created by gpande (2011/8/9 - 14:34:58) (End) */

}

STATUS 
serialDrv(void)
{
	int num_chan;
	
	if( serialDrvMemInit() != OK){
		return(ERROR);
	}
	
	if( (VME_DRV_DEV.channel = (VME_DRV_CHAN *)calloc(NUM_OF_VMESERIAL_CHAN, sizeof(VME_DRV_CHAN)))== NULL){
		printf("channel structure malloc failed\n");
		return(ERROR);
	}
	taskDelay(60);
	for(num_chan = 0; num_chan < NUM_OF_VMESERIAL_CHAN; num_chan++){
		VME_DRV_DEV.channel[num_chan].num_chan     	= num_chan;
		VME_DRV_DEV.channel[num_chan].chanOpen     	= FALSE;
	}
	
	if((vmeSioDrvNum = iosDrvInstall( 	(FUNCPTR)NULL, 
										(FUNCPTR)NULL, 
										(FUNCPTR)vmeSioOpen, 
										(FUNCPTR)vmeSioClose,
										(FUNCPTR)vmeSioRead, 
										(FUNCPTR)vmeSioWrite, 
										(FUNCPTR)vmeSioIoctl)) < 0){
		printf("vmeSioDrvInstall Fail\n");
		return(ERROR); /* error number set by iosDrvInstall */
	}
	return(OK);	
}

STATUS
vmeSioDevCreate(char *name, int num_chan)
{
	/* Add the device to the vxWorks IO system */
	if(iosDevAdd(&(VME_DRV_DEV.channel[num_chan].devHdr), name, vmeSioDrvNum) != OK){
		printf("vmeSioDevCreate: error adding %s to I/O system.\n", name);
		return(ERROR);
	}
	return(OK);
}


VME_DRV_CHAN* 
vmeSioOpen(VME_DRV_CHAN *sioDevHeader, char *remainder, int mode)
{
	int	chan;
	unsigned short *cmdAdrs;
	unsigned short *flagAdrs;
	
	chan = sioDevHeader->num_chan;
	
	printf("open chan %d\n", chan);
	
	cmdAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * chan));
	flagAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * chan)+ RW_OFFSET + OPEN_FLAG_OFFSET);
	
	*cmdAdrs = OPEN_CMD;
	
	while(1){
		if (*flagAdrs == OPEN_RETURN_VALUE){
			*cmdAdrs = 0;
			VME_DRV_DEV.channel[sioDevHeader->num_chan].chanOpen = TRUE;
			printf("openFlag ------------------> %d\n", VME_DRV_DEV.channel[sioDevHeader->num_chan].chanOpen);
			return(sioDevHeader);
		}
	}
	
	return (VME_DRV_CHAN*)(ERROR);
}

STATUS 
vmeSioClose(VME_DRV_CHAN *sioDevHeader)
{
	int chan;
	unsigned short *cmdAdrs;
	unsigned short *flagAdrs;
	
	chan = sioDevHeader->num_chan;
	
	printf("close chan %d\n", chan);
	printf("struct open flag %d\n", sioDevHeader->chanOpen); 
	
	cmdAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan));
	flagAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+ RW_OFFSET + OPEN_FLAG_OFFSET);
	
	if(*flagAdrs == OPEN_RETURN_VALUE){
	
		printf("open flag ------> 0x%x\n", (unsigned int)OPEN_RETURN_VALUE);
	}
	if( (sioDevHeader->chanOpen == TRUE) && *flagAdrs == OPEN_RETURN_VALUE ){
		*cmdAdrs = CLOSE_CMD;
		printf("pass close cmd\n");
		while(1){
			flagAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+ RW_OFFSET + CLOSE_FLAG_OFFSET);
		
			if (*flagAdrs == CLOSE_RETURN_VALUE) break;
			taskDelay(3);
		}
		
		sioDevHeader->chanOpen = FALSE;
		*cmdAdrs = 0;
		*flagAdrs = 0;
		
	}
	return(OK);
}

VME_DRV_CHAN* 
vmeSioWrite(VME_DRV_CHAN *sioDevHeader, char *buffer, int nBytes)
{
	/*int txbytesPut;*/	/* unused variable */
	
	unsigned short *txSemAdrs;
	unsigned short *txLength;
	unsigned char  *txPtr;
	
	int chan;
	
	chan = sioDevHeader->num_chan;
#if 0	
	printf("open chan %d\n", chan);
	
	printf("vmeSioWrite = %d\n", sioDevHeader->chanOpen); 
#endif	
	if (VME_DRV_DEV.channel[sioDevHeader->num_chan].chanOpen == TRUE){
		if (nBytes <= 0) return (VME_DRV_CHAN*)(ERROR);

		/* Created by gpande (2011/8/9 - 14:39:29) (Start) */
		/* ioctl을 위해 semaphore를 설정함 */
#ifdef SERIALMASTER_ADD_SEMAPHORE
		if(semTake(s_IoctlSemIDArr[sioDevHeader->num_chan], WAIT_FOREVER) == ERROR) return (VME_DRV_CHAN*)(ERROR);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
		/* Created by gpande (2011/8/9 - 14:39:34) (End) */
		
		txSemAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+SEM_OFFSET );
		txLength  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+LENGTH_OFFSET);
		txPtr     = (unsigned char*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + TRX_DATA_OFFSET);
		
#if 0		
		semTake(VME_DRV_DEV.channel[sioDevHeader->num_chan].mutexSemWr, WAIT_FOREVER);
#endif
	
		if (*txSemAdrs == SEM_EMPTY){
			bcopyBytes(buffer, txPtr, nBytes);
			*txLength = nBytes;
			*txSemAdrs = SEM_FULL;
			while(1){
				if(*txSemAdrs == SEM_EMPTY) break;
				taskDelay(1);
			}

			/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
			semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
			/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			
			return (VME_DRV_CHAN*)(nBytes);
				
		}else{
			printf("tx buffer not empty!!\n");

			/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
			semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
			/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			
			return (VME_DRV_CHAN*)(ERROR);
		}
#if 0	
		semGive(VME_DRV_DEV.channel[sioDevHeader->num_chan].mutexSemWr);
#endif		
	}else{
		/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
		semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
		/* Created by gpande (2011/8/9 - 14:41:47) (End) */
		
		return (VME_DRV_CHAN*)(ERROR);
	}	

	/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
	semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
	/* Created by gpande (2011/8/9 - 14:41:47) (End) */
}

int 
vmeSioRead(VME_DRV_CHAN *sioDevHeader, char *buffer, int maxBytes)
{

	unsigned short *rxSemAdrs;
	unsigned short *rxLength;
 	unsigned char  *rxPtr;
	/* Created by gpande (2011/7/21 - 11:18:8) (Start) */
	static unsigned int uiRBuffInx = 0;
	/* Created by gpande (2011/7/21 - 11:18:13) (End) */
 	
 	int nBytes;
 	
	nBytes       = 0;
#if 0
	semTake(VME_DRV_CHANDEV.channel[sioDevHeader->num_chan].mutexSemRd, WAIT_FOREVER) == ERROR)	return(0);
#endif

	/* Created by gpande (2011/8/9 - 14:39:29) (Start) */
	/* ioctl을 위해 semaphore를 설정함 */
#ifdef SERIALMASTER_ADD_SEMAPHORE
	if(semTake(s_IoctlSemIDArr[sioDevHeader->num_chan], WAIT_FOREVER) == ERROR) return (ERROR);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
	/* Created by gpande (2011/8/9 - 14:39:34) (End) */

	rxSemAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+SEM_OFFSET+RW_OFFSET );
	rxLength  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan)+LENGTH_OFFSET+RW_OFFSET);
	rxPtr     = (unsigned char*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + TRX_DATA_OFFSET+RW_OFFSET);
	
	if(*rxSemAdrs == SEM_FULL && *rxLength > 0){
		/* Created by gpande (2011/7/21 - 11:15:32) (Start) */
		/*
			기존 vmeSioRead함수의 리턴값이 사용자가 지정한 buffer size를 확인하여 해당값보다 작거나 같은 값이 리턴되지 않고
			무조건 VME buffer에 쌓인 값이 리턴되기 때문에 일반적인 read 함수와 달라서 사용자들이 헷깔리는 경우가 발생한다.
			해당 문제를 수정하기 위해 VME buffer에 쌓인 데이터가 사용자가 지정한 buffer size보다 작은 경우VME buffer에 쌓인 데이터 길이를 리턴하고
			VME buffer에 쌓인 데이터가 사용자가 지정한 buffer size보다 큰 경우 사용자가 지정한 buffer size를 리턴하도록 수정함.
		*/
#if 0
		nBytes = *rxLength;
		bcopyBytes(rxPtr, buffer, nBytes);
		*rxSemAdrs = SEM_EMPTY;
		return(nBytes);
#else
		if(uiRBuffInx >= *rxLength) {	/* Buffer index가 잘못 처리됨. */
			uiRBuffInx = 0;
		}
		
		if(maxBytes >= (*rxLength - uiRBuffInx)) {
			nBytes = (*rxLength - uiRBuffInx);
			bcopyBytes(rxPtr + uiRBuffInx, buffer, nBytes);
			*rxSemAdrs = SEM_EMPTY;
			uiRBuffInx = 0;	/* Read buffer index를 초기화 */

			/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
			semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
			/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			
			return(nBytes);
		}else if(maxBytes < (*rxLength - uiRBuffInx)) {
			nBytes = maxBytes;
			bcopyBytes(rxPtr + uiRBuffInx, buffer, nBytes);
			uiRBuffInx += maxBytes;

			/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
			semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
			/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			
			return(nBytes);
		}
#endif
		/* Created by gpande (2011/7/21 - 11:15:39) (End) */
	}

	/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
	semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
	/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			
	return(ERROR);
	
	taskDelay(1);
#if 0
	semGive(VME_DRV_CHANDEV.channel[sioDevHeader->num_chan].mutexSemRd);
#endif	
		
}

void 
vmeSioIoctlAdrsMap(int chan)
{
	unsigned short *cmdAdrs;
	unsigned short *ioCmdAdrs;
	unsigned short *flagAdrs;
	
	unsigned int *ioArgAdrs;
	
	cmdAdrs   = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * chan));
	flagAdrs  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * chan) + RW_OFFSET + IOCTL_FLAG_OFFSET);
	ioCmdAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * chan) + IOCTL_IOCMD_OFFSET);
	ioArgAdrs = (unsigned int*)  ((VME_BASE_ADRS) + (CHAN_OFFSET * chan) + IOCTL_ARG_OFFSET);
	
	printf("ioctl num_chan ---------> %d\n", chan);
	
	printf("cmdAdrs   = 0x%x\n", (int)cmdAdrs);
	printf("flagAdrs  = 0x%x\n", (int)flagAdrs);
	printf("ioCmdAdrs = 0x%x\n", (int)ioCmdAdrs);
	printf("ioArgAdrs = 0x%x\n", (int)ioArgAdrs);
	
}


int 
vmeSioIoctl(VME_DRV_CHAN *sioDevHeader, int ioctlCode, int *arg)
{
	int	ret_value;
	int	num_chan;
	
	unsigned short *cmdAdrs;
	unsigned short *ioCmdAdrs;
	unsigned short *flagAdrs;
	
	unsigned int *ioArgAdrs;
	
	num_chan = sioDevHeader->num_chan;

	/* Modified by gpande (2011/7/12 - 18:22:2) (Start) */
	/*printf("ioctl num_chan ---------> %d\n", num_chan);*/
	/* Modified by gpande (2011/7/12 - 18:22:4) (End) */
	ret_value = 0;
	
	/* ioclt(fd, *ioCmdAdrs, *ioArgAdrs); */
	
	cmdAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * num_chan));
	flagAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * num_chan) + RW_OFFSET + IOCTL_FLAG_OFFSET);
	ioCmdAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * num_chan) + IOCTL_IOCMD_OFFSET);
	ioArgAdrs = (unsigned int*)((VME_BASE_ADRS) + (CHAN_OFFSET * num_chan) + IOCTL_ARG_OFFSET);
	*flagAdrs = 0;
	
	
	
	
	switch(ioctlCode){
		/*******************************************************
		*			VME Serial Drv Auto Control ftn			   *
		*******************************************************/
		case FIONREAD:
			printf("NOT Yet\n");
			break;
		
		case FIONWRITE:
			printf("NOT Yet\n");
			break;

		case FIO_SET_TIMEDELAY:
			break;

		case FIO_UNSET_TIMEDELAY:
			break;

		case FIO_SET_TX_TIMEOUT:
			break;
			
       	case FIO_SET_RX_TIMEOUT:
			break;

		case FIO_SET_SLEEP:
			break;

		case FIO_UNSET_SLEEP:
			break;

		case FIO_SET_INFRARED_CTR:
			break;

		case FIO_UNSET_INFRARED_CTR:
			break;

		case FIO_SET_LOOPBACK:
			break;

		case FIO_UNSET_LOOPBACK:
			break;

		case FIOBAUDRATE:
		case FIO_SET_BAUDRATE:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0;
					*cmdAdrs=0; 
					break;
				}
				taskDelay(5);
			}
			printf("ioCtl baudRate Change Command OK!!\n"); 
			break;

		case FIO_GET_BAUDRATE:
			break;

		case FIOFLUSH:     
			break;

		case FIORFLUSH:
		case FIO_R_FLUSH:
			{
				*ioArgAdrs = (int)(int *)arg;
				*ioCmdAdrs = ioctlCode;
			
				printf("arg [%d]\n", *ioArgAdrs);
			
				*cmdAdrs = IOCTL_CMD;
				while(1){
					if(*flagAdrs == IOCTL_RETURN_VALUE){
						*flagAdrs = 0;
						*cmdAdrs=0; 
						break;
					}
					taskDelay(5);
				}
				printf("ioCtl RX flush Command OK!!\n");
			}
			break;

		case FIOWFLUSH:
		case FIO_W_FLUSH:
			/* Created by gpande (2011/7/26 - 11:37:0) (Start) */
			{
				unsigned short* lpusSemAddr = 0x0000;
				unsigned short* lpusBuffLen = 0x0000;

				/* Created by gpande (2011/8/9 - 14:39:29) (Start) */
				/* ioctl을 위해 semaphore를 설정함 */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				if(semTake(s_IoctlSemIDArr[sioDevHeader->num_chan], WAIT_FOREVER) == ERROR) return (ERROR);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:39:34) (End) */

				lpusSemAddr = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + SEM_OFFSET);
				lpusBuffLen  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + LENGTH_OFFSET);

				if((*lpusSemAddr != SEM_EMPTY) && *lpusBuffLen > 0) {
					*lpusSemAddr = SEM_EMPTY;
					*lpusBuffLen = 0x0000;

					/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
					semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
					/* Created by gpande (2011/8/9 - 14:41:47) (End) */
					
					return OK;
				}

				/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			}
			/* Created by gpande (2011/7/26 - 11:37:2) (End) */
			break;

		/* Created by gpande (2011/7/26 - 11:42:17) (Start) */
		/* 사용자가 지정한 timeout값 만큼 buffer empty 값을 확인하는 code */
		case FIO_CHANNEL_RX_SELECT:
			{
				unsigned short* lpusSemAddr = 0x0000;
				unsigned short* lpusBuffLen = 0x0000;

				/* Created by gpande (2011/8/9 - 14:39:29) (Start) */
				/* ioctl을 위해 semaphore를 설정함 */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				if(semTake(s_IoctlSemIDArr[sioDevHeader->num_chan], WAIT_FOREVER) == ERROR) return (ERROR);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:39:34) (End) */

				lpusSemAddr = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + SEM_OFFSET + RW_OFFSET);
				lpusBuffLen  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + LENGTH_OFFSET + RW_OFFSET);

				*ioArgAdrs = (int)(int *)arg;

				if((*ioArgAdrs < 0) || (*ioArgAdrs > FIO_CHANNEL_SELECT_CHECK_MAXVAL)) {
					/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
					semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
					/* Created by gpande (2011/8/9 - 14:41:47) (End) */
					
					return ERROR;
				}

				printf("\nFIO_CHANNEL_RX_SELECT -> ioArgAdrs : %X\n", *ioArgAdrs);

				{
					int iInx = 0;
					
					for(; iInx < (*ioArgAdrs); iInx++) {
						if((*lpusSemAddr == SEM_EMPTY) && (*lpusBuffLen == 0)) return OK;
						taskDelay(FIO_CHANNEL_SELECT_DEF_DELAY_VAL);
					}

					/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
					semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
					/* Created by gpande (2011/8/9 - 14:41:47) (End) */

					return ERROR;
				}

				/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			}
			break;
		case FIO_CHANNEL_TX_SELECT:
			{
				unsigned short* lpusSemAddr = 0x0000;
				unsigned short* lpusBuffLen = 0x0000;

				/* Created by gpande (2011/8/9 - 14:39:29) (Start) */
				/* ioctl을 위해 semaphore를 설정함 */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				if(semTake(s_IoctlSemIDArr[sioDevHeader->num_chan], WAIT_FOREVER) == ERROR) return (ERROR);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:39:34) (End) */

				lpusSemAddr = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + SEM_OFFSET);
				lpusBuffLen  = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * sioDevHeader->num_chan) + LENGTH_OFFSET);

				*ioArgAdrs = (int)(int *)arg;

				if((*ioArgAdrs < 0) || (*ioArgAdrs > FIO_CHANNEL_SELECT_CHECK_MAXVAL)) return ERROR;

				printf("\nFIO_CHANNEL_TX_SELECT -> ioArgAdrs : %X\n", *ioArgAdrs);

				{
					int iInx = 0;
					
					for(; iInx < (*ioArgAdrs); iInx++) {
						if((*lpusSemAddr == SEM_EMPTY) && (*lpusBuffLen == 0)) return OK;
						taskDelay(FIO_CHANNEL_SELECT_DEF_DELAY_VAL);
					}

					/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
					semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
					/* Created by gpande (2011/8/9 - 14:41:47) (End) */

					return ERROR;
				}

				/* Created by gpande (2011/8/9 - 14:41:45) (Start) */
#ifdef SERIALMASTER_ADD_SEMAPHORE
				semGive(s_IoctlSemIDArr[sioDevHeader->num_chan]);
#endif	/* SERIALMASTER_ADD_SEMAPHORE */
				/* Created by gpande (2011/8/9 - 14:41:47) (End) */
			}
			break;
		/* Created by gpande (2011/7/26 - 11:42:23) (End) */

		case FIO_CHAN_RESET:
			break;

		case FIO_GET_DVID:
			break;

		case FIO_GET_DREV:
			break;

		case FIOSELECT:
			break;
		
		case FIOUNSELECT:
			break;

		case FIO_RTSCTSCTL: 
			break;
        	
		/*******************************************************
		*			XR16C788 Auto Control ftn				*
		*******************************************************/
		case FIO_SET_SW_FLOW_CTR:
			break;

		case FIO_SET_AUTO_RS485CTR:
			break;

		case FIO_UNSET_AUTO_RS485CTR:
			break;

		case FIO_SET_AUTO_RTSDTR:
			break;

		case FIO_UNSET_AUTO_RTSDTR:
			break;

		case FIO_SET_AUTO_CTSDSR:
			break;

		case FIO_UNSET_AUTO_CTSDSR:
			break;

		case FIO_SET_DATA_LEN:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				taskDelay(5);
			}
			printf("ioCtl Data Length Change Command OK!!\n");
			break;

		case FIO_SET_STOP_BIT:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				taskDelay(5);
			}
			printf("ioCtl Stop Bit Change Command OK!!\n");
			break;

		case FIO_SET_PARITY:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				
				taskDelay(5);
			}
			printf("ioCtl Parity Set Change Command OK!!\n");
			break;

		case FIO_SET_RTS_ON:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				
				taskDelay(5);
			}
			printf("ioCtl RTS ON Command OK!!\n");
			break;

		case FIO_SET_RTS_OFF:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				
				taskDelay(5);
			}
			printf("ioCtl RTS OFF Command OK!!\n");
			break;

		case FIO_SET_DTR_ON	:
			break;

		case FIO_SET_DTR_OFF:
			break;

		case FIO_GET_CTS_STATUS	:
			*ioArgAdrs = (int)(int *)arg;
			*ioCmdAdrs = ioctlCode;
			
			printf("arg [%d]\n", *ioArgAdrs);
			
			
			*cmdAdrs = IOCTL_CMD;
			while(1){
				if(*flagAdrs == IOCTL_RETURN_VALUE){
					*flagAdrs = 0; 
					break;
				}
				
				taskDelay(5);
			}
			printf("ioCtl CTS Status Command OK!!\n");
			ret_value = *ioArgAdrs;
			break;

		case FIO_GET_DSR_STATUS:
			break;

		case FIO_GET_CD_STATUS:
			break;

		case FIO_RX_OVER_RUN_ERR:
			break;
			
		case FIO_RX_PARITY_ERR:
			break;

		case FIO_RX_FRAMING_ERR:			
			break;
			
		case FIO_RX_BREAK_ERR:			
			break;

		case FIO_RX_FIFO_ERR:			
			break;

		/* Created by gpande (2011/7/12 - 17:54:7) (Start) */
		case IOCTRL_308TXQ_STATUS:
			{
				unsigned short* lpustxSemAdrs = 0x00000000;
				
				lpustxSemAdrs = (unsigned short*)((VME_BASE_ADRS) + (CHAN_OFFSET * num_chan)+SEM_OFFSET );

				if(*(lpustxSemAdrs) == SEM_EMPTY) {
					return OK;
				}
			}
			break;
		case ISIO_F_SETCFG:
			{
				return OK;
			}
			break;
		/* Created by gpande (2011/7/12 - 17:54:14) (End) */

		default:
			
			return(ERROR);
	} 
	
	return (ret_value);
}


/**************************************************************/
/* vme serial driver test function                            */

/* vmeSerialDrvInstall */

/* Modified by gpande (2011/7/11 - 16:34:4) (Start) */
#if 0
int vmeFd[16];
#else
int vmeFd[NUM_OF_VMESERIAL_CHAN];
#endif
/* Modified by gpande (2011/7/11 - 16:34:8) (End) */

void 
vmeOpenPort(int chan)
{
	char vmeFdName[20];
	if(drvInstallFlag == 1){
/* Modified by gpande (2011/7/11 - 16:39:21) (Start) */
/* Hard coding되어 있던 serial driver를 macro로 변경 */
#if 0
		sprintf(vmeFdName, "/vme_xrsio/%d\n", chan);
#else
		sprintf(vmeFdName, "%s%d\n", SERIALMASTER_SIO_DRIVER_NAME, chan);
#endif
/* Modified by gpande (2011/7/11 - 16:39:25) (End) */
		vmeFd[chan] = open(vmeFdName, 2, 0);
		printf("Open %s -----------> %d\n", vmeFdName, vmeFd[chan]);
	}else{
		printf("Device Not install!!\n");
	}
	
#if 1	
	ioctl(vmeFd[chan], FIOBAUDRATE, /*1152*/38400);
	taskDelay(5);
#endif	
#if 1
	ioctl(vmeFd[chan], FIO_SET_STOP_BIT, STOP_BIT_2);
	taskDelay(5);
#endif
#if 0	
	ioctl(vmeFd[chan], FIO_SET_DATA_LEN, CHAR_LEN_6);
	taskDelay(5);
#endif
#if 1	
	ioctl(vmeFd[chan], FIO_SET_PARITY, SET_ODD_PARITY);
	taskDelay(5); 
#endif	
}

void
vmeClosePort(int chan)
{
	close(vmeFd[chan]);
}


void vmeOpenPortAll()
{
	int i;
	for(i=0; i<NUM_OF_VMESERIAL_CHAN; i++){
		vmeOpenPort(i);
		taskDelay(10);
		
	}
}

void
vmeClosePortAll()
{
	int i;
	for(i=0; i<NUM_OF_VMESERIAL_CHAN; i++){
		vmeClosePort(i);
		taskDelay(3);
	}	
}

void
drvInstall()
{
	vmeSerialDrvInstall();
	taskDelay(100);
	vmeOpenPortAll();
}


void 
writeChanTest(int chan)
{
/*	
	write(vmeFd[chan], "012345678901234567890123456789", 30);
*/	
	write(vmeFd[chan], "0123456789abcdefghiijklmnopqrs0123456789", 40);
	taskDelay(100);
	
	
}

void 
writeAllPort()
{
	int i;
	while(1){
		
		for(i=0; i<NUM_OF_VMESERIAL_CHAN; i++){
			writeChanTest(i);
			taskDelay(5);
		}
		taskDelay(10);
	}
	
	
}

void 
writeTest()
{
	int i;
	for(i=0; i<NUM_OF_VMESERIAL_CHAN; i++){
		write(vmeFd[i], "012345678901234567890123456789", 30);
	}
}

void 
readChan(int chan)
{
	int rdByte;
	char buffer[/*1024*/39];
	int i;
	
	while(1){
		taskDelay(10);
		rdByte = read(vmeFd[chan], buffer, /*50*/39);
		if(rdByte > 0){
			#if 1
			printf(" %d : %d \n", rdByte,chan);
			for(i=0; i<rdByte; i++){
				printf("[%c]", buffer[i]);
				if( !((i+1)%16) ) printf("\n");
			}

			/* Created by gpande (2011/7/21 - 10:44:41) (Start) */
			printf("\nRead size : %d\n", rdByte);
			/* Created by gpande (2011/7/21 - 10:44:43) (End) */
			
			printf("\n\n\n");
			#endif
		}	 
	}
}


void 
rw_Task()
{
	sp((FUNCPTR)readChan,0,0,0,0,0,0,0,0,0);taskDelay(5);
	
#if 1	
	sp((FUNCPTR)readChan,1,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,2,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,3,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,4,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,5,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,6,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,7,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,8,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,9,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,10,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,11,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,12,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,13,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,14,0,0,0,0,0,0,0,0);taskDelay(5);
	sp((FUNCPTR)readChan,15,0,0,0,0,0,0,0,0);taskDelay(5);

	/* Modified by gpande (2011/7/21 - 10:46:49) (Start) */
#if 1
	sp((FUNCPTR)writeAllPort,0,0,0,0,0,0,0,0,0);taskDelay(5);
#else
	sp((FUNCPTR)writeChanTest,0,0,0,0,0,0,0,0,0);taskDelay(5);
#endif
	/* Modified by gpande (2011/7/21 - 10:46:51) (End) */
#endif
}

#if 0 
STATUS 
recvMsg(int xrPd, char *sData, int len, int timevalue)
{
	char *rptr;
	int rxLen, rxRLen, rxTLen;
	int timeoutCnt;
	int dataNum;
	
	rptr = sData;
	rxRLen = len;
	
	rxLen=0;
	rxTLen=0;
	timeoutCnt=0;
	dataNum=0;
		
	sysClkRateSet(1000);
	
	
		
	do{
		ioctl(xrPd,FIONREAD,(int)&dataNum);
		if(dataNum <= 0){
			
			if(timevalue > timeoutCnt){
				taskDelay(1);
				timeoutCnt += 1;
				continue;
			}else{
				return(rxTLen);
			}
		}
		/*
		else{
			printf("dataNum = %d\n",dataNum);
		}
		*/
		rxLen = read(xrPd, rptr, rxRLen);
		
		if(rxLen >0){
			rxRLen -= rxLen;
			rptr += rxLen;
			rxTLen += rxLen;
		}
		
	}while(rxRLen>0);	
	return(rxTLen);
}
#endif

void 
rtsOn(int chan)
{
	ioctl(vmeFd[chan], 0xF30, 0); /* RTS ON */
}

void 
rtsOff(int chan)
{
	ioctl(vmeFd[chan], 0xF31, 0); /* RTS Off */
}

void rtsOnOff(int chan)
{
	
	
	while(1){
		rtsOn(chan);
		taskDelay(sysClkRateGet()/10);
		rtsOff(chan);
		taskDelay(sysClkRateGet()/10);
	}
	
}

void 
ctsStat(int chan)
{
	
	unsigned int ctsStatus;
	
	
	ctsStatus=0;
	ctsStatus = ioctl(vmeFd[chan], 0xF34, 0);
	printf("cts status = %d\n", ctsStatus);
}
