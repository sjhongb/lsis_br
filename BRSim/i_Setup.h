/* i_Setup.h - HanA SYS68K Intelligent SIO board header file */

/* Copyright 1995, HanA SYSTMES,INC. */

/*
Modification History
--------------------
01a,23Dec99,khs  made file.
*/

/*==========================================================================*/
/*========            Public Functions                ========*/
/*==========================================================================*/

#ifdef    __STDC__

IMPORT  int  i_Start (
    int       doReset 	/* board reset flag */
    );

IMPORT  int  i_Stop (VOID);
#else

IMPORT    int    isioBrd ();
IMPORT  int  i_Stop ();

#endif  /* __STDC__ */

/* EOF */
