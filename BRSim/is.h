/* isio.h - HanA SYS68K Intelligent SIO board header file */

/* Copyright 1995, HanA SYSTMES,INC. */

/*
Modification History
--------------------
01c,30apr92,mli     Added support for blocking read calls.
         Added "open" flag in device descriptor.
01b,14apr92,mli     Added tyLib support (selectable per channel),
         through usage of "modifier flags".
         Removed <> on include files; use "" instead.
01a,27feb92,mli  initial release
*/

#ifndef    INCisioh
#define    INCisioh

/*******************************************************
*			User Defined Parameter					*
*******************************************************/
#define	ALL_HW_HANDSHAKE

#define	ALL_SW_HANDSHAKE
#undef	ALL_SW_HANDSHAKE

#define	USER_DEFINE_HANDSHAKE
#undef	USER_DEFINE_HANDSHAKE

#define	ISIO_BASE_ADDRESS		0x90000000
#define	ISIO_SHORT_ADDR		0x9000

/*******************************************************
*			User Defined Parameter					*
*******************************************************/


/* (configurable) vxModNum style module number for
   ISIO driver - used for errnoSet (look for
   S_isio_XXX error codes):
*/

#define M_isio    (0x469 << 16)

/* Include files needed by this include file
   (it is assumed that VxWorks.h has already been included):
 */
#include "iosLib.h"
#include "selectLib.h"
#include "semLib.h"
#include "tyLib.h"

/* Miscellaneous: */
#define ISIO_NUM_CHAN     8    /* Number of I/O channels per board    */

/* For isioBrd call: */
#define ISIO_O_RESET    0x0001    /* Reset board when installed */

/* For ioctl( FIOSETOPTIONS ): */
#define ISIO_O_USEDLM   0x0100    /* Use delimiters (Rx)          */
#define ISIO_O_WAITTX   0x0200    /* Wait for Tx completion     */
#define ISIO_O_BLOCKRX  0x0400    /* Blocking receive mode      */

/* Special ioctl function codes: */
#define ISIO_F_GETDLM   0x0100    /* Get delimiter character(s)     */
#define ISIO_F_SETDLM   0x0101    /* Set delimiter character(s)     */
#define ISIO_F_GETCFG   0x0102    /* Get channel I/O configuration */
#define ISIO_F_SETCFG   0x0103    /* Set channel I/O configuration */
#define ISIO_F_ABORT    0x0104    /* Abort ISIO command         */

#define ISIO_ASS_RTS    0x0105    /* Abort ISIO command         */
#define ISIO_DASS_RTS   0x0106    /* Abort ISIO command         */
#define ISIO_CHK_CTS    0x0107    /* Abort ISIO command         */
#define ISIO_WR_WR15    0x0108    /* Abort ISIO command         */
#define ISIO_CHK_RTS    0x0109    /* Abort ISIO command         */
#define ISIO_RESET    0x010A     /* Reset chan - skhong            */

/* Command Codes */

#define ISIO_TX_CMD     0x0100    /* Mask for transmitter commands     */
#define ISIO_INT_CMD    0x1000    /* Mask for interrupt on completion     */
#define ISIO_CMD_DONE   0x8000    /* Mask for command completion         */
#define ISIO_CMD_STAT   0x000F    /* Mask for command status field     */

#define ISIO_C_GLOBINI  0x001     /* Global reconfiguration of I/O buffers */
#define ISIO_C_SETOFFS  0x003     /* Set address offset             */
#define ISIO_C_RESET    0xF04     /* Reset chan - skhong            */
#define ISIO_C_CLRBUF	0x005     /* Clear input buffer             */
#define ISIO_C_ASYNINI  0x006     /* Short asynchronous initialization     */
#define ISIO_C_TX_CLRBUF   0xF07     /* Clear Output buffer - skhong            */
#define ISIO_C_RECOFF   0x008     /* Disable receiver             */
#define ISIO_C_GETCH    0x010     /* Get one character (blocking)         */
#define ISIO_C_GETBUF   0x012     /* Get chars from input buffer (non-bl.) */
#define ISIO_C_GETSTR   0x030     /* Get delimited string             */
#define ISIO_C_PUTCNT   0x135     /* Output a "counted" string         */
#define ISIO_C_GETLINE  0x036     /* Get edited string             */
#define ISIO_C_INSTAT   0x042     /* Return input buffer status         */
#define ISIO_C_PUTCH    0x114     /* Output one character              */
#define ISIO_C_GETCNT   0x033     /* Get fixed-length string         */
#define ISIO_C_ASSRTS   0x043     /* Assert RTS signal */
#define ISIO_C_DASSRTS  0x044     /* De-assert RTS signal */
#define ISIO_C_CHKCTS   0x045     /* Check CTS signal */
#define ISIO_N_GET		0x046
#define ISIO_ENT_DIS	0x047
#define ISIO_CHECK_TX	0x048		/*Check TX status*/
#define ISIO_C_CHKRTS   0x049     /* Check RTS signal */


#define ISIO_C_NONE     0x07F     /* code for "no command"         */


/* Error codes */

#define ISIO_E_PARITY   0x01      /* Parity error         */
#define ISIO_E_FRAME    0x02      /* Framing error        */
#define ISIO_E_READ     0x03      /* Read error        */
#define ISIO_E_BREAK    0x04      /* Break detected        */
#define ISIO_E_WRITE    0x05      /* Write error        */
#define ISIO_E_PARAM    0x06      /* Bad parameter        */
#define ISIO_E_ILLEG    0x07      /* Illegal use (SW FAIL)    */
#define ISIO_E_NOCHR    0x08      /* No character available    */
#define ISIO_E_ABORT    0x09      /* Command aborted        */
#define ISIO_E_OVERFL   0x0a      /* Buffer overflow        */
#define ISIO_E_ADDR     0x0b      /* Address error (SW FAIL)    */
#define ISIO_E_CANCEL   0x0e      /* Request cancelled    */
#define ISIO_E_CMD      0x0f      /* Bad command (SW FAIL)    */


/* Error codes prefixed with module id (for errno) */

#define S_isio_PARITY (M_isio | ISIO_E_PARITY)
#define S_isio_FRAME  (M_isio | ISIO_E_FRAME)
#define S_isio_READ   (M_isio | ISIO_E_READ)
#define S_isio_BREAK  (M_isio | ISIO_E_BREAK)
#define S_isio_WRITE  (M_isio | ISIO_E_WRITE)
#define S_isio_PARAM  (M_isio | ISIO_E_PARAM)
#define S_isio_ILLEG  (M_isio | ISIO_E_ILLEG)
#define S_isio_NOCHR  (M_isio | ISIO_E_NOCHR)
#define S_isio_ABORT  (M_isio | ISIO_E_ABORT)
#define S_isio_OVERFL (M_isio | ISIO_E_OVERFL)
#define S_isio_ADDR   (M_isio | ISIO_E_ADDR)
#define S_isio_CANCEL (M_isio | ISIO_E_CANCEL)
#define S_isio_CMD    (M_isio | ISIO_E_CMD)

#define ISIO_M_TYLIB  0x0001    /* use tyLib        */

/* ISIO Board offset Address */
#define ISIO_CRST	0x730				/* modified by jmy 98.4. */
#define ISIO_VRST	0xC90
#define ISIO_CMDRD	0xCA0
#define ISIO_STSRD	0xCB0
#define ISIO_MSKLAT	0xCD0
#define ISIO_VECLAT	0xCE0
#define ISIO_VMEINT	0xCF0


/* Command structure */
typedef struct {
    UINT16     cmdStat;        /* command/status code    */
    UINT16     wParam1;        /* 1'st word parameter    */
    UINT32     lParam1;      /* 1'st long parameter    */
    UINT32     lParam2;     /* 2'nd long parameter    */
    UINT16     wParam2;        /* 2'nd word parameter    */
    UINT16     _res;        /* (reserved ffu)    */
} ISIO_CMD;


/* I/O buffer reconfiguration structure (GLOBINI) -

   Receive and transmit buffer sizes are set independently
   of each other, hence, the number of buffers that must
   be configured is ISIO_NUM_CHAN * 2.

   Buffers sizes are set in units of 1 kByte.
   The total sum of buffer sizes must be equal (or less)
   to 94 kBytes.

   NOTE: the size of the receiver buffer for channel 0
     must ALWAYS be set to 2 kBytes!
 */

typedef UINT8   ISIO_BUF_CONFIG [ISIO_NUM_CHAN * 2];


/* Asynchronous channel initialization structure (ASYNINI) */

typedef enum {            /* Handshake modes:     */
    NO_HANDSHAKE   = 0,
    HW_HANDSHAKE   = 1,    /* hardware (RTS/CTS)    */
    SW_HANDSHAKE   = 2,    /* software (XON/XOFF)    */
    HW_SW_HANDSHAKE =3    /* both hw and sw    */
} ISIO_HANDSHAKE;

typedef enum {        /* Character length    */
    SIX_CHAR_BITS   = 1,
    SEVEN_CHAR_BITS = 2,
    EIGHT_CHAR_BITS = 3
} ISIO_CHAR_LENGTH;

typedef enum {        /* Stop bits (for 6/7/8 bit chars) */
    ONE_STOP_BIT    = 7,
    TWO_STOP_BITS   = 0xf
} ISIO_STOP_BITS;

typedef enum {        /* Parity modes */
    NO_PARITY   = 0,
    EVEN_PARITY = 2,
    ODD_PARITY  = 6
} ISIO_PARITY;

typedef enum {        /* Baud rate codes */
    BAUD_50     = 50,
    BAUD_75     = 75,
    BAUD_110    = 110,
    BAUD_134    = 134,    /* (= 134.5) */
    BAUD_150    = 150,
    BAUD_200    = 200,
    BAUD_300    = 300,
    BAUD_600    = 600,
    BAUD_1050   = 1050,
    BAUD_1200   = 1200,
    BAUD_2000   = 2000,
    BAUD_2400   = 2400,
    BAUD_4800   = 4800,
    BAUD_9600   = 9600,
    BAUD_19200  = 19200,
    BAUD_38400  = 38400,
    BAUD_57600  = 57600,
    BAUD_115200  = 115200
} ISIO_BAUD;

typedef struct {
    UINT8      handshake;
    UINT8      charLength;
    UINT8      stopBits;
    UINT8      parity;
    UINT16     baud;
} ISIO_IO_CONFIG;


/* ISIO Board */

typedef struct {
 /* 0000 */  UINT16        status;
 /* 0002 */  UINT16        version;
 /* 0004 */  UINT16        abortChan;
 /* 0006 */  UINT16        sysfail;
 /* 0008 */  UINT16        revision;
 /* 000A */  UINT16        echoFlags;
 /* 000C */  UINT16        parityFlags;
 /* 000E */  UINT16        vmeVec;
 /* 0010 */  UINT32        baseaddr;
	     UINT8         dummy[12];
 /* 0020 */  ISIO_CMD      cmd[ISIO_NUM_CHAN * 2];    /* Tx/Rx */
             UCHAR         dpr[ 0x17800 ];
} ISIO_BOARD;


/* ISIO device (I/O channel) descriptor */
typedef struct {
    TY_DEV               tyDev;     /* standard tyLib device descr.  */
    struct ISIO_DEV  *devDescp;     /* pointer to ISIO dev. descr. */
} ISIO_TY_DEV;

typedef struct {
    SEM_ID          lock;     /* mutex lock        */
    int             stat;     /* status            */
    int             pendCmd;  /* pending command   */
    ISIO_CMD        *cmd;     /* ptr to command struct    */
    UINT8           *dpr;     /* ptr to DPR        */
    UINT            buf;      /* size of buffer    */
    SEM_ID          rdy;      /* ready semaphore   */
} ISIO_RXTX;


typedef struct    ISIO_DEV
 {
    DEV_HDR       devHdr;    /* standard device header   */
    BOOL          created;   /* TRUE iff device created  */
    BOOL          open;      /* TRUE iff device is open  */
    UINT          chan;      /* channel number (0..4)    */
    UINT          options;   /* options            */
    UINT          modFlags;  /* driver modifier flags    */
    ISIO_TY_DEV   *tyDevp;   /* ptr to tyLib dev. descr. */
    ISIO_IO_CONFIG   ioConfig;    /* baud rate, handshake etc */
    SEL_WAKEUP_LIST   wupList;    /* wake-up list for select  */
    struct ISIO_BRD_CFG  *board;  /* ref to board        */

    /* Receiver stuff: */
    ISIO_RXTX    rx;          /* receiver Rx/Tx info      */
    UINT          rxDataCnt;   /* nbr of bytes in buffer   */
    char          *rxDatap;    /* ptr to data in buffer    */
    UINT8         delim;       /* (optional) delimiter        */

    /* Transmitter stuff: */
    ISIO_RXTX    tx;          /* transmitter Rx/Tx info   */
} ISIO_DEV_DESC;


/* Board descriptor */

typedef struct ISIO_BRD_CFG {
    ISIO_BOARD	*isio;             /* ptr to board (address)   */
    UINT			intVec;            /* VME interrupt vectors    */
    UINT			intLvl;            /* VME interrupt levels        */
    SEM_ID		lock;              /* per-board  mutex lock    */
    ISIO_DEV_DESC  devDesc[ ISIO_NUM_CHAN ];  /* device (channel) info */
} ISIO_BRD_DESC;



/*==========================================================================*/
/*========            Public Functions                ========*/
/*==========================================================================*/

#ifdef    __STDC__

IMPORT    STATUS isioDrv (VOID);
IMPORT    STATUS isioRemove (VOID);

IMPORT  int  isioBrd (
    UINT32       address, /* board address             */
    UINT         intVec,  /* (base) interrupt vector */
    UINT         intLvl,  /* interrupt levels for..  */
    ISIO_IO_CONFIG  *usrIoConfig,  /* (opt.) I/O config  */
    ISIO_BUF_CONFIG *usrBufConfig, /* (opt.) buffer cfg  */
    UINT         options        /* additional options */
    );

IMPORT  STATUS  isioDevCreate (
    char    *name,    /* Name to use for this device */
    UINT    channel,  /* Physical channel (0..7)     */
    UINT    board     /* Board index               */
    );

/* //01b + -->// */
IMPORT STATUS    isioUseTyLib (
    UINT    channel,    /* Physical channel (0..7)     */
    UINT    board,    /* Board index               */
    UINT    rdBufSize,  /* Read buffer size, in bytes  */
    UINT    wrtBufSize  /* Write buffer size, in bytes */
    );
/* //<-- + 01b// */

IMPORT    STATUS isioShow (VOID);
IMPORT    STATUS isioProbe (UINT32 address);

#else

IMPORT  STATUS isioDrv ();
IMPORT    STATUS isioRemove ();

IMPORT    int    isioBrd ();
IMPORT    STATUS isioDevCreate ();

/* //01b + -->// */
IMPORT STATUS    isioUseTyLib ();
/* //<-- + 01b// */

IMPORT    STATUS isioShow ();
IMPORT    STATUS isioProbe ();

#endif  /* __STDC__ */


#endif    /* INCisioh */

/* EOF */
