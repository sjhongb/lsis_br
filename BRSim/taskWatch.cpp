/* taskWatch.cpp
 *
 */
 
#include "taskLib.h"
#include "stdio.h"
#include "string.h"
//#include "fioLib.h"

// include user header.
#include "projdef.h"
#include "taskWatch.h"

/* ------------------------------------------------------------------------ */
int g_nTaskIDFor308[MAX_WATCH_TASKS] = {
	ERROR ,
	ERROR ,
	ERROR ,
	ERROR
};

/* ------------------------------------------------------------------------ */
int g_nTaskResumeCount[MAX_WATCH_TASKS] = {
//	0,
//	0,
//	0,
//	0,
	0,
	0,
	0,
	0
};

/* ------------------------------------------------------------------------ */
int  writeResumeCountToRAM()
{
	return 0;
}

void readResumeCnt()
{
	int  n;
	
	for(n=0; n<MAX_WATCH_TASKS; n++) {
		printf("\nTASK[%d]: %d", (int)(n+1), (int)g_nTaskResumeCount[n]);
	}
}
	
/* ------------------------------------------------------------------------ */
/*  Return strings of taskStatusString()function.
	String  	Meaning  
	--------------------------------------------------------------------------------
	READY  		Task is not waiting for any resource other than the CPU. 
	PEND  		Task is blocked due to the unavailability of some resource. 
	DELAY  		Task is asleep for some duration. 
	SUSPEND  	Task is unavailable for execution (but not suspended, delayed, or pended). 
	DELAY+S  	Task is both delayed and suspended. 
	PEND+S  	Task is both pended and suspended. 
	PEND+T  	Task is pended with a timeout. 
	PEND+S+T	Task is pended with a timeout, and also suspended. 
	...+I  		Task has inherited priority (+I may be appended to any string above). 
	DEAD  		Task no longer exists. 
	--------------------------------------------------------------------------------
*/

void taskWatchRun()
{
	int  n;
	int  nStatus;
	char pstrTaskStatus[20];

	nStatus = 1;
	while(1) 
    {
		for(n=0; n<MAX_WATCH_TASKS; n++) 
        {
			if ( g_nTaskIDFor308[n] != ERROR ) 
            {
				taskStatusString( g_nTaskIDFor308[n], &pstrTaskStatus[0] );	// get task status.
				if ( !strcmp(pstrTaskStatus, "SUSPEND") ) 
                {	// task status is suspend.
					g_nTaskResumeCount[n]++;		// suspend count is resume count.
					taskResume( g_nTaskIDFor308[n] );
					writeResumeCountToRAM();
				}
			}
			taskDelay(60);	// 100ms
		}
		//short nUpStatus = MyUpModem.ReadFrame( 0 );
		//short nDnStatus = MyDnModem.ReadFrame( 0 );
		
	}
}

void taskWatchInitialize() 
{
	taskSpawn("taskWatchRun", TASKPR_WATCH,0,4000,(FUNCPTR)taskWatchRun,0,0,0,0,0,0,0,0,0,0);
}

/* ------------------------------------------------------------------------ */
