/* -------------------------------------------------------------------------+
|  CommDrv.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommDrv.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver.
|                                                                           
|  4. Project Name.                                                         
|     Tongi,Laksam,13station by BR.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- LS402A and LS314                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _COMDRV_H
#define _COMDRV_H

// user BPS header files.
#include "msgQLib.h"

// user header files.
#include "projdef.h"

/* ----------------------------------------------------------------------- */
#define MAX_COMBUFFER_SIZE		3072						// 3 KByte
#define MAX_QUEBUFFER_SIZE		(MAX_COMBUFFER_SIZE * 2)	// 6 KByte


/* ----------------------------------------------------------------------- */
// for shaistaganj station...
//#define STATION_AZPR
#define STATION_AKA


/* ----------------------------------------------------------------------- */

class CommDriver 
{
public:
	int m_bOpenFailed;		/* comm open status */
	int m_nPortNo;			/* COM port */
	int m_nfd;				/* file discriptor */
    int m_nClientFd_1;
    int m_nClientFd_2;
    int m_nClientFd_3;
    int m_nClientFd_4;
	int m_bUsedCtrlRTS;	
	int m_bUsedIOFlush;
	MSG_Q_ID m_nRxQID;	
	MSG_Q_ID m_nTxQID;
	
	int m_nBufferSize;
	short m_nRxPtr;			/* received pointer counter of the RX buffer. */
	short m_nTxPtr;			/* sending pointer counter of the TX buffer. */    
	unsigned char *m_pRxBuffer;	// RX buffer
	unsigned char *m_pTxBuffer;	// TX Buffer
	BYTE m_CheckTime;
	
public:
	CommDriver( int nBuffSize = MAX_COMBUFFER_SIZE );
	
	unsigned char *GetRXBuffer()
	{
		return m_pRxBuffer;
	}
	unsigned char *GetTXBuffer()
	{
		return m_pTxBuffer;
	}
	
	virtual ~CommDriver();
	virtual void ClearRX();
	virtual void ClearTX();
	virtual int  ReadBlock( char *pBuffer, int nReadSize, int tmBreak=0 );
	virtual short WriteBlock( char *pBuffer, short length );
	virtual short Initialize( int nPort, int bUsed=0, int bUsedMsgQ=1 ); /* create 1/'RX', 2/'TX', 3/'RX and TX' for message Que. */
};

/*---------------------------------------------------------------------- 
|   통신 태스크가 사용하는 포트 번호를 설정한다. 
+----------------------------------------------------------------------*/
// 1 ~ 7 LS_402A, 8 ~ 21 LS_314
#define PORTNUM_SIM     	3
#define PORTNUM_CC1 		1		// KVME402A CPU's P0 - "/ScctyCo/1"
#define PORTNUM_CC2 		2		// KVME402A CPU's P1 - "/ScctyCo/2"
#define PORTNUM_MC  		3		// KVME402A CPU's P2 - "/tyCo/2"
#define PORTNUM_BLOCK1 		4		// KVME402A CPU's P3 - "/tyCo/3"
#define PORTNUM_BLOCK2		5		// KVME402A CPU's P4 - "/tyCo/4"
#define PORTNUM_BLOCK3		6		// KVME402A CPU's P5 - "/tyCo/5"
#define PORTNUM_BLOCK4		7		// KVME402A CPU's P6 - "/tyCo/0"

#define PORTNUM_EXT1 		8		// KVME314 Board's P0 - "/isio/0"
#define PORTNUM_EXT2  		9		// KVME314 Board's P1 - "/isio/1"
#define PORTNUM_EXT3  		10		// KVME314 Board's P2 - "/isio/2"
#define PORTNUM_EXT4 	 	11		// KVME314 Board's P3 - "/isio/3"
#define PORTNUM_EXT5  		12		// KVME314 Board's P4 - "/isio/4"
#define PORTNUM_EXT6  		13		// KVME314 Board's P5 - "/isio/5"
#define PORTNUM_EXT7		14		// KVME314 Board's P6 - "/isio/6"
#define PORTNUM_EXT8		15		// KVME314 Board's P7 - "/isio/7"
#define PORTNUM_EXT9		16		// KVME314 Board's P8 - "/isio/8"
#define PORTNUM_EXT10		17		// KVME314 Board's P9 - "/isio/9"
#define PORTNUM_EXT11		18		// KVME314 Board's P10 - "/isio/10"
#define PORTNUM_EXT12		19		// KVME314 Board's P11 - "/isio/11"
#define PORTNUM_EXT13		20		// KVME314 Board's P12 - "/isio/12"
#define PORTNUM_EXT14		21		// KVME314 Board's P13 - "/isio/13"
#define PORTNUM_MAX			22


/* ----------------------------------------------------------------------- */
// Defined const value of the limit waiting time for 'select' function.
#define	SELTIMEOUT_CONSOLE	1		// 1 second, For CC, MC channel.
#define	SELTIMEOUT_SIM		2		// 2 second, For SIM(I/O simulation) channel.
#define	SELTIMEOUT_IF		1		// 1 second, For IF channel.
#define	SELTIMEOUT_BLOCK	1		// 1 second, For BLOCK channel.
#define	SELTIMEOUT_PORT		2		// 1 second, For PORT channel.

/*----------------------------------------------------------------------------+
|               외부 참조 변수 선언                                           |
+-----------------------------------------------------------------------------*/
extern int	GetOpenSerialPort( int );

/* ----------------------------------------------------------------------- */
extern struct      fd_set  readFds_CC1  , tmpFds_CC1;
extern struct      fd_set  readFds_CC2  , tmpFds_CC2;
extern struct      fd_set  readFds_MC  , tmpFds_MC;
extern struct      fd_set  readFds_SIM   , tmpFds_SIM;
extern struct      fd_set  readFds_IF    ,tmpFds_IF;
extern struct      fd_set  readFds_Port1,	tmpFds_Port1;
extern struct      fd_set  readFds_Port2,	tmpFds_Port2;

/* ----------------------------------------------------------------------- */
extern struct      timeval selTimeOut_CC1;
extern struct      timeval selTimeOut_CC2;
extern struct      timeval selTimeOut_MC;
extern struct      timeval selTimeOut_SIM;
extern struct      timeval selTimeOut_IF;
extern struct      timeval selTimeOut_Port1;
extern struct      timeval selTimeOut_Port2;

/* ----------------------------------------------------------------------- */
/* Defined operation function for the CRC table
 * 
 */
extern unsigned short CRCtable[256];

#define crc_CCITT( c, s )	( CRCtable[(s^c)&0xFF] ^ s>>8 )

/* ----------------------------------------------------------------------- */
/* Defined accessing functions for 308 board.
 * 
 */
extern void SetRTS( int nfd );						/* RTS signal ON for file scriptor */
extern void ClearRTS( int nfd );					/* RTS signal OFF for file scriptor */
extern int  GetCTS( int nfd );						/* Get CTS status for file scriptor */
extern int  IsEmpty308TXQ( int nfd );				/* Get empty status of 308 Board TX buffer. */
extern int  WaitEmpty308TXQ( int nfd, int nTicks );	/* waiting empty status of 308 Board TX buffer. */

/* ----------------------------------------------------------------------- */
/*
// ------------------------------------------------------------------------- //
// ioctl function codes //
// ------------------------------------------------------------------------- //
#define FIONREAD	1		// get num chars available to read //
#define FIOFLUSH	2		// flush any chars in buffers //
#define FIOOPTIONS	3		// set options (FIOSETOPTIONS) //
#define FIOBAUDRATE	4		// set serial baud rate //
#define FIODISKFORMAT	5	// format disk //
#define FIODISKINIT	6		// initialize disk directory //
#define FIOSEEK		7		// set current file char position //
#define FIOWHERE	8		// get current file char position //
#define FIODIRENTRY	9		// return a directory entry (obsolete)//
#define FIORENAME	10		// rename a directory entry //
#define FIOREADYCHANGE	11	// return TRUE if there has been a media change on the device //
#define FIONWRITE	12		// get num chars still to be written //
#define FIODISKCHANGE	13	// set a media change on the device //
#define FIOCANCEL	14		// cancel read or write on the device //
#define FIOSQUEEZE	15		// squeeze out empty holes in rt-11 file system //
#define FIONBIO		16		// set non-blocking I/O; SOCKETS ONLY!//
#define FIONMSGS	17		// return num msgs in pipe //
#define FIOGETNAME	18		// return file name in arg //
#define FIOGETOPTIONS	19	// get options //
#define FIOSETOPTIONS	FIOOPTIONS	// set options //
#define FIOISATTY	20		// is a tty //
#define FIOSYNC		21		// sync to disk //
#define FIOPROTOHOOK	22	// specify protocol hook routine //
#define FIOPROTOARG	23		// specify protocol argument //
#define FIORBUFSET	24		// alter the size of read buffer  //
#define FIOWBUFSET	25		// alter the size of write buffer //
#define FIORFLUSH	26		// flush any chars in read buffers //
#define FIOWFLUSH	27		// flush any chars in write buffers //
#define FIOSELECT	28		// wake up process in select on I/O //
#define FIOUNSELECT	29		// wake up process in select on I/O //
#define FIONFREE        30              // get free byte count on device //
#define FIOMKDIR        31              // create a directory //
#define FIORMDIR        32              // remove a directory //
#define FIOLABELGET     33              // get volume label //
#define FIOLABELSET     34              // set volume label //
#define FIOATTRIBSET    35              // set file attribute //
#define FIOCONTIG       36              // allocate contiguous space //
#define FIOREADDIR      37              // read a directory entry (POSIX) //
#define FIOFSTATGET     38              // get file status info //
#define FIOUNMOUNT      39              // unmount disk volume //
#define FIOSCSICOMMAND  40              // issue a SCSI command //
#define FIONCONTIG      41              // get size of max contig area on dev //
#define FIOTRUNC        42              // truncate file to specified length //
#define FIOGETFL        43		// get file mode, like fcntl(F_GETFL) //
#define FIOTIMESET      44		// change times on a file for utime() //
#define FIOINODETONAME  45		// given inode number, return filename //
#define FIOFSTATFSGET   46              // get file system status info //

// ------------------------------------------------------------------------- //
// ioctl option values //
// ------------------------------------------------------------------------- //
#define OPT_ECHO		0x01		// echo input //
#define OPT_CRMOD		0x02		// lf -> crlf //
#define OPT_TANDEM		0x04		// ^S/^Q flow control protocol //
#define OPT_7_BIT		0x08		// strip parity bit from 8 bit input //
#define OPT_MON_TRAP	0x10		// enable trap to monitor //
#define OPT_ABORT		0x20		// enable shell restart //
#define OPT_LINE		0x40		// enable basic line protocol //
#define OPT_RAW			0			// raw mode //
#define OPT_TERMINAL	(OPT_ECHO | OPT_CRMOD | OPT_TANDEM | OPT_MON_TRAP | OPT_7_BIT | OPT_ABORT | OPT_LINE)

#define CONTIG_MAX	-1		// "count" for FIOCONTIG if requesting maximum contiguous space on dev //
*/
/*
// ------------------------------------------------------------------------- //
// Defined file open mode.
// ------------------------------------------------------------------------- //
#define O_RDONLY 	(0)  (or READ)  	- open for reading only. 
#define O_WRONLY 	(1)  (or WRITE)  	- open for writing only. 
#defien O_RDWR 		(2)  (or UPDATE)  	- open for reading and writing. 
#define O_CREAT 	(0x0200) 			- create a file. 
*/

/* ----------------------------------------------------------------------- */

#endif // _COMDRV_H
