/* -------------------------------------------------------------------------+
|  CommCC.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CommCC.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
// Include system header files.
#include <stdio.h>
#include <string.h>
#include "taskLib.h"
#include "selectLib.h"
#include "ioLib.h"
#include "iosLib.h"
#include "wdLib.h"

// Include user header files.
#include "myque.h"
#include "taskWatch.h"
#include "serialMaster.h"
#include "common/Scrinfo.h"
#include "LogTask.h"
#include "ComModem.h"
#include "CommATO.h"

#include "commCC.h"

/* ------------------------------------------------------------------------ */
#define	TYPE_CC1		0x01
#define	TYPE_CC2		0x02
#define	TYPE_MC			0x04
#define	TYPE_SC1		0x10
#define	TYPE_SC2		0x20
#define TYPE_LOCAL		0x07
#define	TYPE_ALL		0xFF

#define DLE             0x10
#define STX				0x02
#define ETX				0x03

#define FULLUPDATE		0x82
#define SPEEDCODE		0xEE
#define SITECTRL		0x83
#define	SITECTRLRPT		0x34
#define CCSTATUS		0x30
#define ACKMSSTYPE		0xA0
#define NACKMSSTYPE		0xA7
#define AYTMSSTYPE		0xAC

#define RX_TIMEOUT		120		// 200 msec
#define REFRESH_TIME	600		// 1 sec
#define SELECT_TIMEOUT	1		// 1 sec

/* ------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------ */
extern SystemStatusType	*EI_Status;
extern char g_Active;

Comm_CC 	MyCC;
 
WDOG_ID	wid_RetranTimerSC1;
WDOG_ID wid_RetranTimerSC2;

int rRetranTimerSC1();
int rRetranTimerSC2();

BYTE	g_bRetranSC1;
BYTE	g_bRetranSC2;

/* ------------------------------------------------------------------------ */
//;=====================================================================
//; CC Serial Interface
//;=====================================================================
Comm_CC::Comm_CC() {
	memset( CpuCommandQue, 0, sizeof(CpuCommandQue));
	memset( DIM_Buffer, 0, sizeof(DIM_Buffer));

	m_nFailCnt_CC1	= 0;
	m_nFailCnt_CC2	= 0;
	m_nSpeedCodeLimit_SC	= 0;

	m_nTXSEQ		= 0;

	m_nRXPOS_CC1 	= 0;
	m_nRXPOS_CC2 	= 0;
	m_nRXPOS_MC 	= 0;
	m_nRXPOS_SC1 	= 0;
	m_nRXPOS_SC2 	= 0;

	memset(m_pSCDataCH1, 0, sizeof(m_pSCDataCH1));
	memset(m_pSCDataCH2, 0, sizeof(m_pSCDataCH2));

	m_nRetranTxLen = 0;
	memset(m_pRetranTxData, 0, sizeof(m_pRetranTxData));
}

void Comm_CC::SendAck(BYTE ch) 
{    
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;

	nTxLen = MakeMessage(pTxData, GetRxSeqNumber(ch), ACKMSSTYPE, NULL, 0);

	SendData(ch, pTxData, nTxLen);
}

void Comm_CC::SendNack(BYTE ch) 
{
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;

	MakeMessage(pTxData, GetRxSeqNumber(ch), NACKMSSTYPE, NULL, 0);

	SendData(ch, pTxData, nTxLen);
}

USHORT Comm_CC::MakeMessage(BYTE *pTxData, BYTE cSeq, BYTE cOpCode, BYTE *pData, USHORT nDataLen)
{
	USHORT 	nPos = 0;
	BYTE 	low_crc=0, high_crc=0;

#if 0
	if(nDataLen > 120)
	{
		return 0;
	}
#endif

	pTxData[nPos++] = DLE;
	pTxData[nPos++] = STX;
	*(USHORT*)&pTxData[nPos] = nDataLen + 2;	// SEQ(1) + OPCODE(1) + DATA(nDataLen)
	nPos += 2;

	pTxData[nPos++] = cSeq; 
	pTxData[nPos++] = cOpCode; 

	if(nDataLen > 0 && pData != NULL)
	{
		memcpy(&pTxData[nPos], pData, nDataLen);
		nPos += nDataLen;
	}

    m_CRC.CrcGen(nPos, pTxData, &low_crc, &high_crc);

	pTxData[ nPos++ ] = low_crc;		// crc low byte
	pTxData[ nPos++ ] = high_crc;		// crc high byte
	pTxData[ nPos++ ] = DLE;
	pTxData[ nPos++ ] = ETX;

	return nPos;
}

void Comm_CC::ProcessControl(BYTE ch, BYTE *pRxData) 
{
	int nRXSEQ, nPREVSEQ;

	nRXSEQ	= pRxData[4];
	nPREVSEQ = GetRxSeqNumber(ch);

#if 0
	if((nRXSEQ == 0x00) || (nPREVSEQ <= nRXSEQ) || (nPREVSEQ == 0xFF && nRXSEQ <= 0xFF))
#endif
	{
		// RX Sequence Number Update.
		SetRxSeqNumber(ch, nRXSEQ);

		switch (pRxData[5]) 
		{
		case SITECTRL : // 현장 제어
			SendAck(ch);
			LogInd(GetLogType(ch), "Control Message Received.");
#ifndef _759
			if(((ch == TYPE_CC1 || ch == TYPE_CC2) && (EI_Status->bStation == 0)) 
			|| ((ch == TYPE_SC1 || ch == TYPE_SC2) && (EI_Status->bStation == 1))
			|| (ch == TYPE_MC)
			|| (pRxData[8] == 0x99) || (pRxData[8] == 0x98) 
			|| (pRxData[8] == 0x46) || (pRxData[8] == 0x47)
			|| (pRxData[8] == 0x07))
			{
#endif
				LogInd(GetLogType(ch), "Control Message Accepted.");
				EIEControl(pRxData);
				EIEControlRpt(TYPE_ALL, pRxData);	// 취급내역 전송
#ifndef _759
			}
			else
			{
				LogInd(GetLogType(ch), "Control Message Rejected.");
			}
#endif
			break;

		case CCSTATUS:
			SendAck(ch);
			LogInd(GetLogType(ch), "CC Status Message Received.");
			CCStatusRpt(ch, pRxData);
			break;

		case AYTMSSTYPE:
			SendAck(ch);
			LogInd(GetLogType(ch), "AYT Message Received.");
			break;

		case ACKMSSTYPE:
			LogInd(GetLogType(ch), "ACK Message Received.");
			break;

		default: 
			SendNack(ch);
			break;
		}
	}
#if 0
	else
	{
		SendNack(ch);
	}
#endif
}

void Comm_CC::EIEControl(BYTE *pRxData) 
{
	// Insert Data(OpCode(1), AreaNo(1), DeviceID(1), ControlCode(1), ElementNo(2), State(1))
#if 0
	if(*(USHORT*)&pRxData[9] > 0x38)
	{
			*(USHORT*)&pRxData[9] = *(USHORT*)&pRxData[9] - 1;
	}
#endif
	memcpy( &CpuCommandQue[0], &pRxData[5], 7);
}


void Comm_CC::EIEControlRpt(BYTE ch, BYTE *pRxData)
{
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;

	// SPARE(1), CONSOLE_ID(1), CONTROL_TYPE(1), ELEMENT_NO(2), CONTROL_STATE(1)
	nTxLen = MakeMessage(pTxData, m_nTXSEQ++, SITECTRLRPT, &pRxData[6], 6); 

	SendData(ch, pTxData, nTxLen);

	LogInd(GetLogType(ch), "Control Report Message Sent.");
}

void Comm_CC::CCStatusRpt(BYTE ch, BYTE *pRxData)
{
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;
	BYTE 	cPeerCh = 0;

	switch(ch)
	{
	case TYPE_CC1:
		cPeerCh = TYPE_CC2;
		break;
	case TYPE_CC2:
		cPeerCh = TYPE_CC1;
		break;
	default:
		return;
	}

	// STATUS(1)
	nTxLen = MakeMessage(pTxData, m_nTXSEQ++, CCSTATUS, &pRxData[6], 1); 

	SendData(cPeerCh, pTxData, nTxLen);

	LogInd(GetLogType(ch), "%s Status Message Sent To %s", GetChannelName(ch), GetChannelName(cPeerCh));
}

//Master clock update function -- Be required coding continued.
void Comm_CC::MasterClock(BYTE *pRxData) 
{
	//sequential number, count, Message_type, date1, data2, data3, time1, time2, time3.  
	memcpy(	CpuCommandQue, &pRxData[2], 9);		
}

void Comm_CC::FullUpdateIndicationMessage(BYTE ch) 
{
	USHORT nSize = *(USHORT*)&DIM_Buffer[0];	//Element status length

	if(nSize < 20)
	{
		LogErr(GetLogType(ch), "Invalid DIM_Buffer Data. nSize = %d", nSize);
		return;
	}

	BYTE	pMakeData[128] = {0,};
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;

	// BODY Data (exclude OP_CODE);
	pMakeData[0] = 0x04;							// Area No. (756)

#if 0
	*(USHORT*)&pMakeData[1]	= nSize + 1;			// Repetition
	memcpy( &pMakeData[3], &DIM_Buffer[2], 56 );	// Element States.
	memcpy( &pMakeData[3 + 57], &DIM_Buffer[2 + 56], nSize - 56 );

	nSize = nSize + 1;
#else
	*(USHORT*)&pMakeData[1]	= nSize;		// Repetition
	memcpy( &pMakeData[3], &DIM_Buffer[2], nSize );	// Element States.
#endif

	// CC Communication State Update (Always Last Element)
	pMakeData[nSize + 2] = (pMakeData[nSize + 2] & 0x1F) | ((m_nFailCnt_CC1 < 3) ? 0x20 : 0x00) | ((m_nFailCnt_CC2 < 3) ? 0x40 : 0x00);

	pMakeData[5] = (pMakeData[5] & 0x03) | (GetStatusOfATOComm() * 0x04) | (GetStatusOfMODEMComm() * 0x08);

	// AREA_NO(1) + REPETITION(2) + ELEM_STATES(nSize)
	nTxLen = MakeMessage(pTxData, m_nTXSEQ++, FULLUPDATE, pMakeData, nSize + 3); 

	SendData(ch, pTxData, nTxLen);

	LogInd(GetLogType(ch), "Full Update Message Sent.");
}

void Comm_CC::SpeedCodeIndicationMessage(BYTE ch) 
{
	USHORT nRepNo = *(USHORT*)&AF_Buffer[0];	//Element status length

	if(nRepNo > 66)
	{
		LogErr(GetLogType(ch), "Invalid AF_Buffer Data. nRepNo = %d", nRepNo);
		return;
	}

	BYTE	pMakeData[256] = {0,};
	BYTE	pTxData[MAX_CONSOLE_MSG_LEN] = {0,};
	USHORT	nTxLen = 0;

	// BODY Data (exclude OP_CODE);
	*(USHORT*)&pMakeData[0]	= nRepNo;			 		// Repetition
	memcpy( &pMakeData[2], &AF_Buffer[2], nRepNo * 3 );// Elements States. 
														// nRepNo * (ELEMENT_NO(2) + SPEED_CODE(1))
	// REPETITION(2) + nRepNo * ELEM_STATES(4)
	nTxLen = MakeMessage(pTxData, m_nTXSEQ++, SPEEDCODE, pMakeData, 2 + (nRepNo * 3)); 

	SendData(ch, pTxData, nTxLen);

	LogInd(GetLogType(ch), "AF Speed Code Message Sent.");
}

BYTE Comm_CC::GetRxSeqNumber(BYTE ch) 
{
	switch(ch)
	{
	case TYPE_CC1:
		return m_nRXSEQ_CC1;
	case TYPE_CC2:
		return m_nRXSEQ_CC2;
	case TYPE_MC:
		return m_nRXSEQ_MC;
	case TYPE_SC1:
		return m_nRXSEQ_SC1;
	case TYPE_SC2:
		return m_nRXSEQ_SC2;
	default:
		return 0;
	}
}

void Comm_CC::SetRxSeqNumber(BYTE ch, BYTE seq)
{
	switch(ch)
	{
	case TYPE_CC1:
		m_nRXSEQ_CC1 	= seq;
		break;
	case TYPE_CC2:
		m_nRXSEQ_CC2 	= seq;
		break;
	case TYPE_MC:
		m_nRXSEQ_MC 	= seq;
		break;
	case TYPE_SC1:
		m_nRXSEQ_SC1 	= seq;
		break;
	case TYPE_SC2:
		m_nRXSEQ_SC2	= seq;
		break;
	default:
		return;
	}
}

char *Comm_CC::GetChannelName(BYTE ch) 
{
	switch(ch)
	{
	case TYPE_CC1:
		return "CC1";
	case TYPE_CC2:
		return "CC2";
	case TYPE_MC:
		return "MC";
	case TYPE_SC1:
		return "SC1";
	case TYPE_SC2:
		return "SC2";
	case TYPE_CC1 | TYPE_CC2:
		return "CC1,CC2";
	case TYPE_SC1 | TYPE_SC2:
		return "SC1,SC2";
	case TYPE_CC1 | TYPE_CC2 | TYPE_MC:
		return "CC1,CC2,MC";
	case TYPE_CC1 | TYPE_CC2 | TYPE_SC1 | TYPE_SC2:
		return "CC1,CC2,SC1,SC2";
	case TYPE_ALL:
	case TYPE_CC1 | TYPE_CC2 | TYPE_MC | TYPE_SC1 | TYPE_SC2:
		return "CC1,CC2,MC,SC1,SC2";
	default:
		return "UNKNOWN";
	}
}

char Comm_CC::GetLogType(BYTE ch) 
{
	switch(ch)
	{
	case TYPE_CC1:
	case TYPE_CC2:
	case TYPE_CC1|TYPE_CC2:
		return LOG_TYPE_CC;
	case TYPE_MC:
		return LOG_TYPE_MC;
	case TYPE_SC1:
	case TYPE_SC2:
	case TYPE_SC1|TYPE_SC2:
		return LOG_TYPE_SC;
	case TYPE_LOCAL:
		return LOG_TYPE_EXT_LOCAL;
	case TYPE_ALL:
		return LOG_TYPE_EXT_CONSOLE;
	default:
		return 0;
	}
}
	
/////////////////////////////////////////////////////////////////////////////////
// COM I/O 
/////////////////////////////////////////////////////////////////////////////////

USHORT Comm_CC::ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxSize) 
{
    BYTE low_crc=0, high_crc=0, recv_low_crc, recv_high_crc;

	BYTE 	*pRxPeer = NULL;
	BYTE	*pRxDataSts = NULL, *pRxPeerSts = NULL;

	switch(ch)
	{
	case TYPE_SC1:
		pRxPeer	 	= m_pSCDataCH2;
		pRxPeerSts	= &m_bSCStsCH2;
		pRxDataSts	= &m_bSCStsCH1;
		break;
	case TYPE_SC2:
		pRxPeer	 	= m_pSCDataCH1;
		pRxPeerSts	= &m_bSCStsCH1;
		pRxDataSts	= &m_bSCStsCH2;
		break;
	default:
		break;
	}

	*pRxDataSts = 0;

	if(nRxSize > 0) 
    {
		LogHex(GetLogType(ch), pRxData, nRxSize, "Recv [%d] <--- %s", nRxSize, GetChannelName(ch));

		// 다른 채널에서 동일한 Seq번호로 수신 처리 성공하였으면 중복 처리하지 않고 바로 리턴한다.
		if(ch == TYPE_SC1 || ch == TYPE_SC2)
		{
			if(g_Active && (pRxData[5] == 0xA0) && (pRxData[4] == m_pRetranTxData[4]))
			{
				wdCancel((ch == TYPE_SC1) ? wid_RetranTimerSC1 : wid_RetranTimerSC2);
				LogDbg(LOG_TYPE_SC, "wdCancel() call. %s", GetChannelName(ch));
			}

			if((pRxData[4] == pRxPeer[4]) && (*pRxPeerSts == 1))
			{
				LogInd(GetLogType(ch), "%s Recv Data(Seq = 0x%X). But Already Proceeded.", GetChannelName(ch), pRxData[4]);

				return 1;
			}
		}

        m_CRC.CrcGen(nRxSize-4, pRxData, &low_crc, &high_crc);

		// Message trailer check
        recv_low_crc = pRxData[nRxSize-4];
        recv_high_crc = pRxData[nRxSize-3]; 
		if((recv_low_crc == low_crc) && (recv_high_crc == high_crc)
		&& (DLE == pRxData[nRxSize-2]) && (ETX == pRxData[nRxSize-1]))
		{
			*pRxDataSts = 1;

			ProcessControl(ch, pRxData);

			return 1;
		}
		else
		{
			LogErr(GetLogType(ch), "%s Recv Data CRC Check Error.", GetChannelName(ch));

			if(ch == TYPE_SC1 || ch == TYPE_SC2)
			{
				taskDelay(RX_TIMEOUT);

				if((pRxData[4] == pRxPeer[4]) && (*pRxPeerSts == 1))
				{
					LogInd(GetLogType(ch), "%s Recv Invalid Data(Seq = 0x%X). But Already Proceeded.", GetChannelName(ch), pRxData[4]);

					return 1;
				}
			}
		}
	}
	else if(ch == TYPE_SC1 || ch == TYPE_SC2)
	{
		// 내 버퍼를 초기화 하고,
		memset(pRxData, 0, MAX_CONSOLE_MSG_LEN);

		// 반대측 채널에도 데이터가 없는지 확인해 본다. 
		if(memcmp(pRxData, pRxPeer, MAX_CONSOLE_MSG_LEN) != 0)
		{
			return 1;
		}

		taskDelay(RX_TIMEOUT);

		// 200ms 이후에 다시 한번 확인한다.
		if(memcmp(pRxData, pRxPeer, MAX_CONSOLE_MSG_LEN) != 0)
		{
			return 1;
		}
	}

	SendNack(ch);
	return 0;
}

USHORT Comm_CC::ReceiveData(BYTE ch, BYTE *pRxData) 
{
 	USHORT	body_len, recv_len;
 	short 	retval;

	int		nfd;	
	BYTE	*pRxPtr		= NULL;
	BYTE	*pRxBuffer 	= NULL;

	switch(ch)
	{
	case TYPE_CC1:
		m_nFailCnt_CC1 	= 0;	// Reset Fail Counter For CC1
		nfd				= m_ComCC1.m_nfd;
		pRxBuffer		= m_pRxBuffer_CC1;
		pRxPtr			= &m_nRXPOS_CC1;
		break;
	case TYPE_CC2:
		m_nFailCnt_CC2 	= 0;	// Reset Fail Counter For CC2
		nfd				= m_ComCC2.m_nfd;
		pRxBuffer		= m_pRxBuffer_CC2;
		pRxPtr			= &m_nRXPOS_CC2;
		break;
	case TYPE_MC:
		nfd			= m_ComMC.m_nfd;
		pRxBuffer	= m_pRxBuffer_MC;
		pRxPtr		= &m_nRXPOS_MC;
		break;
	case TYPE_SC1:
		nfd			= m_ComSC1.m_nfd;
		pRxBuffer	= m_pRxBuffer_SC1;
		pRxPtr		= &m_nRXPOS_SC1;
		break;
	case TYPE_SC2:
		nfd			= m_ComSC2.m_nfd;
		pRxBuffer	= m_pRxBuffer_SC2;
		pRxPtr		= &m_nRXPOS_SC2;
		break;
	default:
		return 0;
	}

	// RS-422 Channel Process
	if(ch == TYPE_CC1 || ch == TYPE_CC2)
	{
		*pRxPtr = 0;
		memset(pRxBuffer, 0, sizeof(pRxBuffer));

		// read & check DLE
    	retval = read(nfd, (char*)&pRxBuffer[0], 1);
		if(retval < 1) 
		{
        	return 0;
		}
    	else if (pRxBuffer[0] != DLE)
    	{
			LogErr(GetLogType(ch), "DLE Check Error!!");
        	ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		// read & check STX
		retval = read(nfd, (char*)&pRxBuffer[1], 1);
    	if(retval < 1) 
    	{
        	return 0;
    	}
    	else if (pRxBuffer[1] != STX)
    	{ 
			LogErr(GetLogType(ch), "STX Check Error!!");
        	ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

    	// read LENGTH
    	retval = read (nfd, (char*)&pRxBuffer[2], 2);
    	if (retval < 2) 
    	{
        	return 0;
    	}

		body_len = *(USHORT*)&pRxBuffer[2];
		if(body_len > 120)
		{
			LogErr(GetLogType(ch), "Invalid Body Length Data!! body_len = %d", body_len);
        	ioctl(nfd, FIORFLUSH, 0);
			return 0;
		}

		int idx = 0;
    	// read Body Data
    	recv_len = 0;
    	while(1)
    	{
        	retval = read(nfd, (char*)&pRxBuffer[recv_len + 4], body_len - recv_len);
        	if(retval < 1) 
        	{
				LogErr(GetLogType(ch), "Insufficient Body Data!! body_len = %d, recv_len = %d", body_len, recv_len);
        		ioctl(nfd, FIORFLUSH, 0);
            	return 0;
        	}

        	recv_len += retval;
        	if(recv_len >= body_len)  
        	{
            	break;
        	}
    	}

		// read CRC16
		retval = read (nfd, (char*)&pRxBuffer[body_len + 4], 2);
    	if (retval < 2) 
    	{
        	ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		// read & check DLE
    	retval = read(nfd, (char*)&pRxBuffer[body_len + 6], 1);
    	if (retval < 1) 
    	{
        	return 0;
		}
    	else if (pRxBuffer[body_len + 6] != DLE)
    	{
			LogErr(GetLogType(ch), "DLE Check Error!!");
        	ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		// read & check ETX
	    retval = read(nfd, (char*)&pRxBuffer[body_len + 7], 1);
   	 	if (retval < 1) 
   	 	{
        	return 0;
    	}
    	else if (pRxBuffer[body_len + 7] != ETX)
    	{
			LogErr(GetLogType(ch), "ETX Check Error!!");
        	ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		// Copy To RxData Buffer.
		memcpy(pRxData, pRxBuffer, body_len * 8);
	} 
	else	// RS-232C Channel
	{
		// Get Message from Read Buffer
    	retval = read(nfd, (char*)&pRxBuffer[*pRxPtr], MAX_CONSOLE_MSG_LEN - *pRxPtr);
    	if (retval < 1) 
		{
        	LogDbg(GetLogType(ch), "No Read Data...[%d]", retval);
       		//return 0;
		}
		else
		{
			LogDbg(GetLogType(ch), "New Data Received...[%d]", retval);
			*pRxPtr += retval;
		}

		if( *pRxPtr > MAX_CONSOLE_MSG_LEN )
		{
   			ioctl(nfd, FIORFLUSH, 0);
			memset(pRxBuffer, 0, sizeof(pRxBuffer));
			*pRxPtr = 0;

			LogDbg(GetLogType(ch), "Overflow Buffer!!");
       		return 0;
		}

		// Minimum Protocol Size
		// = DLE(1) + STX(1) + LENGTH(2) + SEQNO(1) + OPCODE(1) + CRC(2) + DLE(1) + ETX(1) = 10
		if( *pRxPtr >= 10 )
		{
			LogDbg(GetLogType(ch), "Trying Process Data.");
		}
		else
		{
			LogDbg(GetLogType(ch), "Need More Data. %d bytes", 10 - *pRxPtr);
			return 0;
		}

    	if((pRxBuffer[0] != DLE) || (pRxBuffer[1] != STX))
  	  	{
       		ioctl(nfd, FIORFLUSH, 0);
			memset(pRxBuffer, 0, sizeof(pRxBuffer));
			*pRxPtr = 0;

			LogErr(GetLogType(ch), "DLE/STX Check Error!!");
        	return 0;
    	}

		// Get seq_no and body length. 
    	body_len = *(USHORT*)&pRxBuffer[2];
		if(body_len > 120)
		{
			LogErr(GetLogType(ch), "Invalid Body Length Data!! body_len = %d", body_len);

			ioctl(nfd, FIORFLUSH, 0);
			memset(pRxBuffer, 0, sizeof(pRxBuffer));
			*pRxPtr = 0;

			return 0;
		}

		if(*pRxPtr < body_len + 8)
		{
			LogDbg(GetLogType(ch), "Need More Data. %d bytes", body_len + 8 - *pRxPtr);
			return 0;
		}

		if((pRxBuffer[body_len + 6] != DLE) || (pRxBuffer[body_len + 7] != ETX))
		{
			LogErr(GetLogType(ch), "DLE/ETX Check Error!!");

			memset(pRxBuffer, 0, sizeof(pRxBuffer));
			*pRxPtr = 0;

        	return 0;
		}

		// Copy To RxData Buffer.
		memcpy(pRxData, pRxBuffer, body_len + 8);

		// Move Remain Data To First Position Of RX Buffer.
		*pRxPtr -= (body_len + 8);
		memcpy(pRxBuffer, pRxBuffer + body_len + 8, *pRxPtr);
	}

    return body_len + 8;
}

PROCESS IOCONSOLE_RecvDrv(BYTE ch)
{
    STATUS  status;
    int     width = 0;

	struct	timeval selTimeOut;
	struct	fd_set  readFds, tmpFds;

	int 	nfd;

	USHORT	nRxSize;
	BYTE	*pRxData = NULL;
	BYTE	DataBuffer[MAX_CONSOLE_MSG_LEN] = {0,};

	switch(ch)
	{
	case TYPE_CC1:
		nfd 	= MyCC.m_ComCC1.m_nfd;
		pRxData	= DataBuffer;
		break;
	case TYPE_CC2:
		nfd 	= MyCC.m_ComCC2.m_nfd;
		pRxData	= DataBuffer;
		break;
	case TYPE_MC:
		nfd 	= MyCC.m_ComMC.m_nfd;
		pRxData	= DataBuffer;
		break;
	case TYPE_SC1:
		nfd 	= MyCC.m_ComSC1.m_nfd;
		pRxData	= MyCC.m_pSCDataCH1;
		break;
	case TYPE_SC2:
		nfd 	= MyCC.m_ComSC2.m_nfd;
		pRxData	= MyCC.m_pSCDataCH2;
		break;
	default:
		return 0;
	}

   	selTimeOut.tv_sec  = SELECT_TIMEOUT;
    selTimeOut.tv_usec = 0;

	FD_ZERO(&readFds);
	FD_SET(nfd, &readFds);

    width = max(nfd, 0);
	width++;

	ioctl(nfd, FIORFLUSH, 0);

    while(FORever)
	{
       	tmpFds = readFds;
       	status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
		if (status == ERROR)
		{
        	LogErr(MyCC.GetLogType(ch), "Select Error");
			taskDelay(REFRESH_TIME);
			continue;
		}
		else if(status == 0)	// select의 타임아웃이 1 Sec 소요되었으므로 taskDelay를 넣지 않는다.
		{
			LogErr(MyCC.GetLogType(ch), "%s Channel Receive Timeout. (1 Sec)", MyCC.GetChannelName(ch));

			// SC의 경우에는 데이터를 받지 못한 경우에도 반대측 채널의 데이터를 확인해본다. 
			if(ch == TYPE_SC1 || ch == TYPE_SC2)
			{
				MyCC.ProcessData(ch, pRxData, 0);
			}
			continue;
		}

		if(FD_ISSET(nfd, &readFds))
		{
			LogInd(MyCC.GetLogType(ch), "%s Channel is ready to Receive Data.", MyCC.GetChannelName(ch));

			while((nRxSize = MyCC.ReceiveData(ch, pRxData)) > 0)
			{
				MyCC.ProcessData(ch, pRxData, nRxSize);
			}
		}
	}
}

PROCESS IOCONSOLE_SendDrv()
{
	UINT counter = 0;
 
	while(FORever)
	{
		// Station Total Information Send.
		MyCC.FullUpdateIndicationMessage(TYPE_ALL);	

		taskDelay(REFRESH_TIME/2);

		// AF Speed Code Information Send.
		MyCC.SpeedCodeIndicationMessage(TYPE_ALL);

		taskDelay(REFRESH_TIME/2);

		MyCC.m_nFailCnt_CC1++;
		MyCC.m_nFailCnt_CC2++;

		MyCC.m_nSpeedCodeLimit_SC++;
	}
}

PROCESS IOCONSOLE_RetranDrv()
{
	BYTE ch;

	// Retransmission Watchdog Timer Set.
	wid_RetranTimerSC1 = wdCreate();
	wid_RetranTimerSC2 = wdCreate();

	g_bRetranSC1 = FALSE;
	g_bRetranSC2 = FALSE;

	while(FORever)
	{
		ch = 0;
		
		if(g_bRetranSC1)
		{
			ch |= TYPE_SC1;
			g_bRetranSC1 = 0;
		}
	
		if(g_bRetranSC2)
		{
			ch |= TYPE_SC2;
			g_bRetranSC2 = 0;
		}

		MyCC.RetranData(ch);

		taskDelay(REFRESH_TIME/10);
	}
}
/////////////////////////////////////////////////////////////////////////////////
// COM I/O END
////////////////////////////////////////////////////////////////////////////////

USHORT Comm_CC::SendData(BYTE ch, BYTE *pTxData, BYTE nTxLen)
{
	USHORT nSentLen;

	if(ch & TYPE_CC1)
	{
		nSentLen = write( m_ComCC1.m_nfd, (char*)&pTxData[0], nTxLen );
	}

	if(ch & TYPE_CC2)
	{
    	nSentLen = write( m_ComCC2.m_nfd, (char*)&pTxData[0], nTxLen );
	}

	if(ch & TYPE_MC)
	{
    	nSentLen = write( m_ComMC.m_nfd, (char*)&pTxData[0], nTxLen );
	}

	// Limit Speed Code Transmission via SCC.
	if((pTxData[5] == 0xEE) && (m_nSpeedCodeLimit_SC % 3 != 0))
	{
		LogHex(GetLogType(TYPE_LOCAL), pTxData, nSentLen, "Sent [%d] ---> %s", nSentLen, GetChannelName(TYPE_LOCAL));
		return 0;
	}

	if(pTxData[5] == 0x82)
	{
		memcpy(m_pRetranTxData, pTxData, nTxLen);
		m_nRetranTxLen = nTxLen;
	}

	if(ch & TYPE_SC1)
	{
   		nSentLen = write( m_ComSC1.m_nfd, (char*)&pTxData[0], nTxLen );

		if(g_Active && pTxData[5] == 0x82)
		{
			wdStart(wid_RetranTimerSC1, 240, (FUNCPTR)rRetranTimerSC1, 0);
			LogDbg(LOG_TYPE_SC, "wdStart() call. SC1");
		}
	}

	if(ch & TYPE_SC2)
	{
   		nSentLen = write( m_ComSC2.m_nfd, (char*)&pTxData[0], nTxLen );

		if(g_Active && pTxData[5] == 0x82)
		{
			wdStart(wid_RetranTimerSC2, 240, (FUNCPTR)rRetranTimerSC2, 0);
			LogDbg(LOG_TYPE_SC, "wdStart() call. SC2");
		}
	}

	LogHex(GetLogType(ch), pTxData, nSentLen, "Sent [%d] ---> %s", nSentLen, GetChannelName(ch));
	return 0;
}

USHORT Comm_CC::RetranData(BYTE ch)
{
	int nSentLen;

	if(ch & TYPE_SC1)
	{
   		nSentLen = write( m_ComSC1.m_nfd, (char*)&m_pRetranTxData[0], m_nRetranTxLen );
	}

	if(ch & TYPE_SC2)
	{
   		nSentLen = write( m_ComSC2.m_nfd, (char*)&m_pRetranTxData[0], m_nRetranTxLen );
	}

	LogHex(GetLogType(ch), m_pRetranTxData, nSentLen, "Retransmission [%d] ---> %s", nSentLen, GetChannelName(ch));
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Watchdog Timer Callback
//////////////////////////////////////////////////////////////////////////////

int rRetranTimerSC1()
{	
	LogDbg(LOG_TYPE_SC, "ACK is not arrived. Retransmission excute. SC1");
	g_bRetranSC1 = TRUE;
}

int rRetranTimerSC2()
{
	LogDbg(LOG_TYPE_SC, "ACK is not arrived. Retransmission excute. SC2");
	g_bRetranSC2 = TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
// CC Initalize
////////////////////////////////////////////////////////////////////////////////

short Comm_CC::Initialize()
{
	m_ComCC1.Initialize( PORTNUM_CC1, 0, 0 );
	m_ComCC2.Initialize( PORTNUM_CC2, 0, 0 );
	m_ComMC.Initialize( PORTNUM_MMCR, 0, 0 );
	m_ComSC1.Initialize( PORTNUM_SC1, 0, 0 );
	m_ComSC2.Initialize( PORTNUM_SC2, 0, 0 );

    m_CRC.GenCrcTable();    

	taskSpawn("IOCC1_RecvDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RecvDrv,TYPE_CC1,0,0,0,0,0,0,0,0,0);
   	taskSpawn("IOCC2_RecvDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RecvDrv,TYPE_CC2,0,0,0,0,0,0,0,0,0);
	taskSpawn("IOMC_RecvDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RecvDrv,TYPE_MC,0,0,0,0,0,0,0,0,0);
	taskSpawn("IOSC1_RecvDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RecvDrv,TYPE_SC1,0,0,0,0,0,0,0,0,0);
   	taskSpawn("IOSC2_RecvDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RecvDrv,TYPE_SC2,0,0,0,0,0,0,0,0,0);
   	taskSpawn("IOCONSOLE_SendDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_SendDrv,0,0,0,0,0,0,0,0,0,0);
	taskSpawn("IOCONSOLE_RetranDrv",TASKPR_CONSOLE,0,8192,(FUNCPTR)IOCONSOLE_RetranDrv,0,0,0,0,0,0,0,0,0,0);
}
