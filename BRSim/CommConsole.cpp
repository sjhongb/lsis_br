/* -------------------------------------------------------------------------+
|  CommConsole.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                              
|                                                                           
|  2. FileName.                                                             
|     CommConsole.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for CC1, CC2, MC.
|                                                                           
|  4. Project Name.                                                         
|     Bangladesh Project - Tongi, Laksam, 13 stations.                        
|                                                                           
|  5. Maker.                                                                
|                                                                                                    
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402A and KVME314                               
|      - Target O.S		- VxWorks 5.4                                                         
|                                                                                             
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   -------------------------------------------------------------------------------------------   
|	 1.0    20130201	Seokju Hong		Written new source codes for BR that based on SMG7
|                                                                           
+------------------------------------------------------------------------- */ 

/* vxWorks library */
#include "taskLib.h"
#include "selectLib.h"
#include "ioLib.h"

/* standard c library */
#include "string.h"

/* user c library */
#include "CommConsole.h"
#include "Common/scrinfo.h"
#include "Common/OpMsgCode.h"

#include "version.h"
#include "LogTask.h"

/* ------------------------------------------------------------------------- */
extern unsigned char VMEM[];
extern BYTE g_ndipSwitch3;

#define ERROR		(-1)


/* ------------------------------------------------------------------ */
extern  SystemStatusType	*EI_Status;//  = (SystemStatusType *)&VMEM[8];			// &sysvar[0]
extern  CommStatusType		*EI_CommStatus;//  = (CommStatusType *)&VMEM[13];		// CommStatus
extern  UnitStatusType		*EI_UnitStatus;//  = (UnitStatusType *)&VMEM[14];		// UnitStatus
//extern  AlarmStatusType		*EI_AlarmStatus;// = (AlarmStatusType *)&VMEM[15];		// AlarmStatus
//extern  ExtStatusType		*EI_ExtStatusType;// = (ExtStatusType *)&VMEM[0x32];	// Logical, Physical END

/* ------------------------------------------------------------------------- */
/* 
 *
 */
Comm_CC OprLCC;

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

Comm_CC::Comm_CC() 
{
	m_nFailCnt[TYPE_CC1]	= 0;
	m_nFailCnt[TYPE_CC2]	= 0;

	m_nOnlineSide	= 0;

	m_bIsLogin	= FALSE;
}

Comm_CC::~Comm_CC() 
{
}

#ifdef FAKE_AXLOCC_ON
USHORT Comm_CC::ComExchange(char *pBuffer, int nBlkPos) 
#else
USHORT Comm_CC::ComExchange(char *pBuffer, unsigned char *pLoginBuffer) 
#endif
{
	short nResult = 0;
	short nSendByte;
    unsigned short nCRC = 0;
	DataHeaderType *pHeaderType = NULL;

	// LCC Status Update
	pHeaderType = (DataHeaderType*)VMEM;
	if(pHeaderType != NULL)
	{
		switch(m_nOnlineSide)
		{
		case TYPE_CC1:
			pHeaderType->LCCStatus.bLCC1Active = 1;
			pHeaderType->LCCStatus.bLCC2Active = 0;
			LogDbg(LOG_TYPE_CC, "Set bLCC1Active = 1, bLCC2Active = 0");
			break;
		case TYPE_CC2:
			pHeaderType->LCCStatus.bLCC1Active = 0;
			pHeaderType->LCCStatus.bLCC2Active = 1;
			LogDbg(LOG_TYPE_CC, "Set bLCC1Active = 0, bLCC2Active = 1");
			break;
		default:
			if(pHeaderType->LCCStatus.bLCC1Active)
			{
				m_nOnlineSide = TYPE_CC1;
				LogDbg(LOG_TYPE_CC, "Set m_nOnlineSide = TYPE_CC1");
			}
			
			if(pHeaderType->LCCStatus.bLCC2Active)
			{
				m_nOnlineSide = TYPE_CC2;
				LogDbg(LOG_TYPE_CC, "Set m_nOnlineSide = TYPE_CC2");
			}
			break;
		}

		LogDbg( LOG_TYPE_CC, 
				"bLCC1Alive = %d, bLCC2Alive = %d, bLCC1Active = %d, bLCC2Active = %d", 
				pHeaderType->LCCStatus.bLCC1Alive, pHeaderType->LCCStatus.bLCC2Alive, 
				pHeaderType->LCCStatus.bLCC1Active, pHeaderType->LCCStatus.bLCC2Active);

		if(m_nFailCnt[TYPE_CC1] > COMM_TIMEOUT_CYCLE)
		{
			pHeaderType->LCCStatus.bLCC1Alive = 0;
			pHeaderType->LCCStatus.bLCC1Active = 0;
			m_nOnlineSide &= ~TYPE_CC1;
		}
		else
		{
			pHeaderType->LCCStatus.bLCC1Alive = 1;
		}

		if(m_nFailCnt[TYPE_CC2] > COMM_TIMEOUT_CYCLE)
		{
			pHeaderType->LCCStatus.bLCC2Alive = 0;
			pHeaderType->LCCStatus.bLCC2Active = 0;
			m_nOnlineSide &= ~TYPE_CC2;
		}
		else
		{
			pHeaderType->LCCStatus.bLCC2Alive = 1;
		}

		LogDbg( LOG_TYPE_CC, 
				"bLCC1Alive = %d, bLCC2Alive = %d, bLCC1Active = %d, bLCC2Active = %d", 
				pHeaderType->LCCStatus.bLCC1Alive, pHeaderType->LCCStatus.bLCC2Alive, 
				pHeaderType->LCCStatus.bLCC1Active, pHeaderType->LCCStatus.bLCC2Active);
	}

	// VMEM Data를 m_pTxBuffer로 복사
	m_pTxBuffer[0] = STX;
	memcpy(&m_pTxBuffer[BASESCAN_HEAD_SIZE], VMEM, m_nTxBlockSize);

/*
	printf("Set LoginInfo to Comm. Tx Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
			pLoginBuffer[0],
			pLoginBuffer[1],
			pLoginBuffer[2],
			pLoginBuffer[3],
			pLoginBuffer[4],
			pLoginBuffer[5],
			pLoginBuffer[6],
			pLoginBuffer[7]);
*/

	memcpy(&m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize], pLoginBuffer, LOGIN_INFO_SIZE);
	memcpy(&m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + LOGIN_INFO_SIZE], &m_pRxBuffer[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE], EXTEND_OPMSG_SIZE);

#ifdef FAKE_AXLOCC_ON
	// 폐색장치와 인터페이스 하는 폐색 구간이 있는 경우,
	// 콘솔을 위해 강제로 AXLOCC, AXLDST 값을 세팅하여 보내준다.
	if(nBlkPos > 0)
	{
		m_pTxBuffer[BASESCAN_HEAD_SIZE + nBlkPos] |= 0xC0;
	}
#endif

	// Version 정보를 copy
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE] 		= SW_VER_MAJ;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + 1] 	= SW_VER_MIN;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + 2] 	= SW_VER_DEV;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + 3] 	= SW_VER_YY;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + 4] 	= SW_VER_MM;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + 5] 	= SW_VER_DD;

	for (int i = BASESCAN_HEAD_SIZE ; i < BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + SW_VERSION_SIZE ; i++) 
	{
	    nCRC = crc_CCITT(m_pTxBuffer[i], nCRC);
	}

	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + SW_VERSION_SIZE] 		= nCRC % 256;
	m_pTxBuffer[BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + SW_VERSION_SIZE + 1] 	= nCRC / 256;

	// Send to CC1, CC2, MC 
	SendData(TYPE_ALL, m_pTxBuffer, BASESCAN_HEAD_SIZE + m_nTxBlockSize + EXTEND_MSG_SIZE + SW_VERSION_SIZE + CRC_SIZE);

	if(m_pRxBuffer[0] != 0)
	{
		// RX Command Copy To CBI Logic Buffer
		memcpy(pBuffer, &m_pRxBuffer[1], OPCODE_MSG_SIZE - 1);

		// RX Buffer Clear
		m_pRxBuffer[0] = 0;
	}

	if(m_bIsLogin)
	{
		m_bIsLogin = FALSE;

		for(i = 0 ; i < LOGIN_INFO_SIZE ; i++)
		{
			if(m_pLoginBuffer[i] != pLoginBuffer[i])
			{
				memcpy(pLoginBuffer, m_pLoginBuffer, LOGIN_INFO_SIZE);
				break;
			}
		}
	}

	return (pHeaderType) ? (pHeaderType->LCCStatus.bLCC1Alive | (pHeaderType->LCCStatus.bLCC2Alive << 1)) : (0);
}

USHORT Comm_CC::SendData(BYTE ch, BYTE *pTxData, USHORT nTxLen)
{
	USHORT nSentLen;

	if(ch & TYPE_CC1)
	{
		// write to CC1 Port.
		pTxData[1] = DISP_CC1;	// Display Opcode -- CC1
		nSentLen = write(m_ComCC1.m_nfd, (char*)&pTxData[0], nTxLen);
	}

	if(ch & TYPE_CC2)
	{
		pTxData[1] = DISP_CC2;	// Display Opcode -- CC2
    	nSentLen = write(m_ComCC2.m_nfd, (char*)&pTxData[0], nTxLen);
	}

	if(ch & TYPE_MC)
	{
		pTxData[1] = DISP_MC;	// Display Opcode -- MC
    	nSentLen = write(m_ComMC.m_nfd, (char*)&pTxData[0], nTxLen);
	}

	LogHex(GetLogType(ch), pTxData, nSentLen, "Sent [%d] ---> %s", nSentLen, GetChannelName(ch));
	return 0;
}


USHORT Comm_CC::ReceiveData(BYTE ch, BYTE *pRxData) 
{
 	USHORT	body_len, recv_len;
 	short 	retval;

	int		nfd;	
	BYTE	*pRxPtr		= NULL;
	BYTE	*pRxBuffer 	= NULL;

	switch(ch)
	{
	case TYPE_CC1:
		nfd				= m_ComCC1.m_nfd;
		pRxBuffer		= m_pRxBuffer_CC1;
		pRxPtr			= &m_nRXPOS_CC1;
		break;
	case TYPE_CC2:
		nfd				= m_ComCC2.m_nfd;
		pRxBuffer		= m_pRxBuffer_CC2;
		pRxPtr			= &m_nRXPOS_CC2;
		break;
	case TYPE_MC:
		nfd			= m_ComMC.m_nfd;
		pRxBuffer	= m_pRxBuffer_MC;
		pRxPtr		= &m_nRXPOS_MC;
		break;
	default:
		return 0;
	}

	// RS-422 Channel Process
	if(ch == TYPE_CC1 || ch == TYPE_CC2)
	{
		*pRxPtr = 0;
		memset(pRxBuffer, 0, sizeof(pRxBuffer));

		// read & check STX
		retval = read(nfd, (char*)&pRxBuffer[0], 1);
    	if(retval < 1) 
    	{
        	return 0;
    	}
    	
		if(pRxBuffer[0] != STX) 
    	{ 
			LogErr(GetLogType(ch), "STX Check Error!!, pRxBuffer[0] = 0x%X, STX = 0x%X", pRxBuffer[0], STX);
        	//ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		LogDbg(GetLogType(ch), "pRxBuffer[0] = 0x%X", pRxBuffer[0]);

		// read & check STX
		retval = read(nfd, (char*)&pRxBuffer[1], 1);
    	if(retval < 1) 
    	{
        	return 0;
    	}
    	
		if((pRxBuffer[1] & 0xF0) != SEQ_BASE) 
    	{ 
			LogErr(GetLogType(ch), "SEQ_BASE Check Error!!, pRxBuffer[1] = 0x%X, SEQ_BASE = 0x%X", pRxBuffer[1], SEQ_BASE);
        	//ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		LogDbg(GetLogType(ch), "pRxBuffer[1] = 0x%X", pRxBuffer[1]);

    	// read OpCode
    	retval = read(nfd, (char*)&pRxBuffer[2], 1);
    	if (retval < 1) 
    	{
        	return 0;
    	}
    	
		LogDbg(GetLogType(ch), "pRxBuffer[2] = 0x%X", pRxBuffer[2]);

		int idx = 0;

    	// read Remained Data
    	recv_len = 0;
		body_len = RECV_MSG_SIZE - OPMSG_HEAD_SIZE;

    	while(1)
    	{
        	retval = read(nfd, (char*)&pRxBuffer[OPMSG_HEAD_SIZE + recv_len], body_len - recv_len);
        	if(retval < 1) 
        	{
				LogErr(GetLogType(ch), "Insufficient Body Data!! body_len = %d, recv_len = %d", body_len, recv_len);
        		ioctl(nfd, FIORFLUSH, 0);
            	return 0;
        	}

        	recv_len += retval;
        	if(recv_len >= body_len)  
        	{
            	break;
        	}
    	}

		// Copy To RxData Buffer.
		memcpy(pRxData, pRxBuffer, RECV_MSG_SIZE);
	} 
	else if(ch == TYPE_MC)	// RS-232C Channel
	{
		// Get Message from Read Buffer
    	retval = read(nfd, (char*)&pRxBuffer[*pRxPtr], MAX_COMBUFFER_SIZE - *pRxPtr);
    	if (retval < 1) 
		{
        	LogDbg(GetLogType(ch), "No Read Data...[%d]", retval);
       		//return 0;
		}
		else
		{
			LogDbg(GetLogType(ch), "New Data Received...[%d]", retval);
			*pRxPtr += retval;
		}

		if( *pRxPtr > MAX_COMBUFFER_SIZE )
		{
   			ioctl(nfd, FIORFLUSH, 0);
			memset(pRxBuffer, 0, sizeof(pRxBuffer));
			*pRxPtr = 0;

			LogDbg(GetLogType(ch), "Overflow Buffer!!");
       		return 0;
		}

		// Just fit the Protocol Size
		if( *pRxPtr >= RECV_MSG_SIZE )
		{
			LogDbg(GetLogType(ch), "Trying Process Data.");
		}
		else
		{
			LogDbg(GetLogType(ch), "Need More Data. %d bytes", RECV_MSG_SIZE - *pRxPtr);
			return 0;
		}

		if(pRxBuffer[0] != STX) 
    	{ 
			LogErr(GetLogType(ch), "STX Check Error!!, pRxBuffer[0] = 0x%X, STX = 0x%X", pRxBuffer[0], STX);
        	//ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		LogDbg(GetLogType(ch), "pRxBuffer[0] = 0x%X", pRxBuffer[0]);

		if((pRxBuffer[1] & 0xF0) != SEQ_BASE) 
    	{ 
			LogErr(GetLogType(ch), "SEQ_BASE Check Error!!, pRxBuffer[1] = 0x%X, SEQ_BASE = 0x%X", pRxBuffer[1], SEQ_BASE);
        	//ioctl(nfd, FIORFLUSH, 0);
        	return 0;
    	}

		LogDbg(GetLogType(ch), "pRxBuffer[1] = 0x%X", pRxBuffer[1]);
		LogDbg(GetLogType(ch), "pRxBuffer[2] = 0x%X", pRxBuffer[2]);

		// Copy To RxData Buffer.
		memcpy(pRxData, pRxBuffer, RECV_MSG_SIZE);

		// Move Remain Data To First Position Of RX Buffer.
		*pRxPtr -= RECV_MSG_SIZE;

		if(*pRxPtr > 0)
		{
			memcpy(pRxBuffer, pRxBuffer + RECV_MSG_SIZE, *pRxPtr);
		}
	}
	else
	{
		LogErr(GetLogType(ch), "No target to receive.");
	}

    return RECV_MSG_SIZE;
}

USHORT Comm_CC::ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxSize) 
{
	USHORT	nSendByte = 0;
	USHORT	nCRC;

	if(nRxSize > 0) 
    {
		// Rx Fail Count Reset
		m_nFailCnt[ch] = 0;

		LogHex(GetLogType(ch), pRxData, nRxSize, "Recv [%d] <--- %s", nRxSize, GetChannelName(ch));

		nCRC = 0;
		for(int i = 1 ; i < nRxSize ; i++) 
		{
			nCRC = crc_CCITT(pRxData[i], nCRC);
		}

		if(nCRC != 0)
		{
			LogErr(GetLogType(ch), "%s Recv Data CRC Check Error.", GetChannelName(ch));
			return 0;
		}

		if((pRxData[0] == STX) && ((pRxData[1] & 0xF0) == SEQ_BASE)) 
		{
			// OPCODE Check
			switch(pRxData[2])
			{
				case 0xED:
				{
					nSendByte = SendData(TYPE_ALL, pRxData, nRxSize);
					break;
				}
				case 0x00:
				{
					if(ch == m_nOnlineSide)
					{
						LogDbg(GetLogType(ch), "Copying a RX Message from %s.", GetChannelName(ch));
			    		memcpy(m_pRxBuffer, pRxData, OPCODE_MSG_SIZE);
					}
					break;
				}
				default:
				{
					if((ch == TYPE_MC) && (pRxData[2] != WS_OPMSG_SETTIMER))
					{
						LogDbg(GetLogType(ch), "Block a OpCode(0x%X) from MMCR.", pRxData[2]);
						break;	
					}	

					LogDbg(GetLogType(ch), "RX Command from %s is copied.", GetChannelName(ch));
			    	memcpy(m_pRxBuffer, pRxData, OPCODE_MSG_SIZE);

					LogInd(GetLogType(ch), "USER OPCODE : 0x%X", pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE]);
					// EXTEND OPCODE Check
					switch(pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE])
					{
					case WS_USEROPMSG_USER_CHANGE:
						memcpy(	&m_pRxBuffer[OPCODE_MSG_SIZE], 
								&pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 1],
								LOGIN_INFO_SIZE );
				   		memcpy(	&m_pRxBuffer[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE],
								&pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE],
								EXTEND_OPMSG_SIZE );

/*
						printf("Set LoginInfo to Comm. Rx Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
								m_pRxBuffer[OPCODE_MSG_SIZE],
								m_pRxBuffer[OPCODE_MSG_SIZE+1],
								m_pRxBuffer[OPCODE_MSG_SIZE+2],
								m_pRxBuffer[OPCODE_MSG_SIZE+3],
								m_pRxBuffer[OPCODE_MSG_SIZE+4],
								m_pRxBuffer[OPCODE_MSG_SIZE+5],
								m_pRxBuffer[OPCODE_MSG_SIZE+6],
								m_pRxBuffer[OPCODE_MSG_SIZE+7]);
*/

						// 20130605 Login 정보를 내부 Buffer에 저장 (CBI 계간 동기화를 위해)
						memcpy(m_pLoginBuffer, &m_pRxBuffer[OPCODE_MSG_SIZE], LOGIN_INFO_SIZE);

						m_bIsLogin = TRUE;

						m_nOnlineSide = ch;
						LogDbg( GetLogType(ch), "User Changed to [%c%c%c%c%c%c%c%c] at [%s]",
								m_pRxBuffer[OPCODE_MSG_SIZE], m_pRxBuffer[OPCODE_MSG_SIZE + 1],
								m_pRxBuffer[OPCODE_MSG_SIZE + 2], m_pRxBuffer[OPCODE_MSG_SIZE + 3],
								m_pRxBuffer[OPCODE_MSG_SIZE + 4], m_pRxBuffer[OPCODE_MSG_SIZE + 5],
								m_pRxBuffer[OPCODE_MSG_SIZE + 6], m_pRxBuffer[OPCODE_MSG_SIZE + 7],
							 	GetChannelName(ch) );
						break;
					case WS_USEROPMSG_PASS_CHANGE:
						if(ch != m_nOnlineSide)
						{
							break;
						}

						if((m_pRxBuffer[OPCODE_MSG_SIZE]     == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 1])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 1] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 2])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 2] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 3])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 3] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 4])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 4] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 5])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 5] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 6])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 6] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 7])
						&& (m_pRxBuffer[OPCODE_MSG_SIZE + 7] == pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 8]))
						{
							memcpy(	&m_pRxBuffer[OPCODE_MSG_SIZE], 
									&pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE + 1],
									LOGIN_INFO_SIZE );

							// 20130605 Login 정보를 내부 Buffer에 저장 (CBI 계간 동기화를 위해)
							memcpy(m_pLoginBuffer, &m_pRxBuffer[OPCODE_MSG_SIZE], LOGIN_INFO_SIZE);

							m_bIsLogin = TRUE;

							LogDbg( GetLogType(ch), "Password Changed to [%c%c%c%c%c%c%c%c] at [%s]",
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE], 
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 1],
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 2], 
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 3],
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 4], 
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 5],
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 6], 
									m_pRxBuffer[OPCODE_MSG_SIZE + USERID_SIZE + 7],
							 		GetChannelName(ch) );
						}
						break;
					case WS_USEROPMSG_CREATE:
					case WS_USEROPMSG_DELETE:
					case WS_USEROPMSG_REQUEST_USER:
					default:
						if(ch != m_nOnlineSide)
						{
							break;
						}
				   		memcpy(	&m_pRxBuffer[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE],
								&pRxData[OPCODE_MSG_SIZE + LOGIN_INFO_SIZE],
								EXTEND_OPMSG_SIZE );
						break;
					}

					break;
				}
			}

			return 1;
		}
	}

	return 0;
}
/* ------------------------------------------------------------------------- */

PROCESS CONSOLE_RecvDrv(BYTE ch)
{
    STATUS  status;
    int     width = 0;

	struct	timeval selTimeOut;
	struct	fd_set  readFds, tmpFds;

	int 	nfd;

	USHORT	nRxSize;
	BYTE	*pRxData = NULL;
	BYTE	DataBuffer[MAX_COMBUFFER_SIZE] = {0,};

	switch(ch)
	{
	case TYPE_CC1:
		nfd 	= OprLCC.m_ComCC1.m_nfd;
		pRxData	= DataBuffer;
		break;
	case TYPE_CC2:
		nfd 	= OprLCC.m_ComCC2.m_nfd;
		pRxData	= DataBuffer;
		break;
	case TYPE_MC:
		nfd 	= OprLCC.m_ComMC.m_nfd;
		pRxData	= DataBuffer;
		break;
	default:
		return 0;
	}

    selTimeOut.tv_sec  = SELTIMEOUT_CONSOLE;
    selTimeOut.tv_usec = 0;

    FD_ZERO(&readFds);
    FD_SET(nfd , &readFds);

    width = max(nfd, 0);
	width++;

	ioctl(nfd, FIORFLUSH, 0);

    while(FORever)
	{
        tmpFds = readFds;
        status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
		if(status == ERROR)
		{
        	LogErr(OprLCC.GetLogType(ch), "Select Error");
			taskDelay(SELTIMEOUT_CONSOLE * 600);
			continue;
		}
		else if(status == 0)	// select의 타임아웃이 소요되었으므로 taskDelay를 넣지 않는다.
		{
			continue;
		}

		if(FD_ISSET(nfd, &readFds)) 
		{
			LogInd(	OprLCC.GetLogType(ch), 
					"%s Channel is ready to Receive Data.", 
					OprLCC.GetChannelName(ch) );

			while((nRxSize = OprLCC.ReceiveData(ch, pRxData)) > 0)
			{
				OprLCC.ProcessData(ch, pRxData, nRxSize);
			}
		} 
	}
}

/* ------------------------------------------------------------------------- */

short Comm_CC::Initialize( short nTxSize )
{
	m_nTxBlockSize = nTxSize;
	
	m_ComCC1.Initialize( PORTNUM_CC1, 0, 1 );
	m_ComCC2.Initialize( PORTNUM_CC2, 0, 1 );
	m_ComMC.Initialize( PORTNUM_MC, 0, 1 );
	
    taskSpawn("CC1_RecvDrv", TASKPR_CONSOLE,0,0x8192,(FUNCPTR)CONSOLE_RecvDrv,TYPE_CC1,0,0,0,0,0,0,0,0,0);
    taskSpawn("CC2_RecvDrv", TASKPR_CONSOLE,0,0x8192,(FUNCPTR)CONSOLE_RecvDrv,TYPE_CC2,0,0,0,0,0,0,0,0,0); 
	taskSpawn("MC_RecvDrv", TASKPR_CONSOLE,0,0x8192,(FUNCPTR)CONSOLE_RecvDrv,TYPE_MC,0,0,0,0,0,0,0,0,0);
}

/* ------------------------------------------------------------------------- */


char *Comm_CC::GetChannelName(BYTE ch) 
{
	switch(ch)
	{
	case TYPE_CC1:
		return "CC1";
	case TYPE_CC2:
		return "CC2";
	case TYPE_MC:
		return "MC";
	case TYPE_CC1 | TYPE_CC2:
		return "CC1,CC2";
	case TYPE_ALL:
		return "CC1,CC2,MC";
		return "CC1,CC2,MC";
	default:
		return "UNKNOWN";
	}
}

char Comm_CC::GetLogType(BYTE ch) 
{
	switch(ch)
	{
	case TYPE_CC1:
	case TYPE_CC2:
	case TYPE_CC1|TYPE_CC2:
		return LOG_TYPE_CC;
	case TYPE_MC:
		return LOG_TYPE_MC;
	case TYPE_ALL:
		return LOG_TYPE_EXT_CONSOLE;
	default:
		return 0;
	}
}
