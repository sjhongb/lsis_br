/* -------------------------------------------------------------------------+
|  CommCC.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CommCC.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for CC.
|                                                                           
|  4. Project Name.                                                         
|     Seoul Metro Line 7 Ext.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402A and KVME314                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   -------------------------------------------------------------------------------------------   
|	 1.0    20110804	Seokju Hong		Written new source codes that based on 'class CommCTC'
|                                                                           
+------------------------------------------------------------------------- */ 

#ifndef _COMMCC_H
#define _COMMCC_H
/* ------------------------------------------------------------------------ */
#include "commdrv.h"
#include "crc.h"

#define MAX_CONSOLE_MSG_LEN		256

/* ------------------------------------------------------------------------ */
class Comm_CC 
{
	// About Send Data.
	BYTE	m_nTXSEQ;

	// About Receive Data.
	BYTE	m_nRXPOS_CC1;
	BYTE	m_nRXPOS_CC2;
	BYTE	m_nRXPOS_MC;
	BYTE	m_nRXPOS_SC1;
	BYTE	m_nRXPOS_SC2;

	BYTE	m_nRXSEQ_CC1;
	BYTE	m_nRXSEQ_CC2;
	BYTE	m_nRXSEQ_MC;
	BYTE	m_nRXSEQ_SC1;
	BYTE	m_nRXSEQ_SC2;

	BYTE m_pRxBuffer_CC1[MAX_CONSOLE_MSG_LEN];
	BYTE m_pRxBuffer_CC2[MAX_CONSOLE_MSG_LEN];
	BYTE m_pRxBuffer_MC[MAX_CONSOLE_MSG_LEN];
	BYTE m_pRxBuffer_SC1[MAX_CONSOLE_MSG_LEN];
	BYTE m_pRxBuffer_SC2[MAX_CONSOLE_MSG_LEN];

	BYTE m_pRetranTxData[MAX_CONSOLE_MSG_LEN];
	BYTE m_nRetranTxLen;

public:
	BYTE m_pSCDataCH1[MAX_CONSOLE_MSG_LEN];
	BYTE m_pSCDataCH2[MAX_CONSOLE_MSG_LEN];
	BYTE m_bSCStsCH1;
	BYTE m_bSCStsCH2;

	CommDriver m_ComCC1;
	CommDriver m_ComCC2;
	CommDriver m_ComMC;
	CommDriver m_ComSC1;
	CommDriver m_ComSC2;

	BYTE	m_nFailCnt_CC1;
	BYTE	m_nFailCnt_CC2;
	UINT	m_nSpeedCodeLimit_SC;

	BYTE CpuCommandQue[MAX_CONSOLE_MSG_LEN];
	BYTE DIM_Buffer[MAX_CONSOLE_MSG_LEN];
	BYTE AF_Buffer[MAX_CONSOLE_MSG_LEN];

	Ccrc 	m_CRC;

	Comm_CC();

	USHORT RetranData(BYTE ch);
	USHORT SendData(BYTE ch, BYTE *pTxData, BYTE nTxLen);
	USHORT ReceiveData(BYTE ch, BYTE *pRxData);
	USHORT ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxLen);

	virtual short Initialize();

	void DtsAnswer();
	void AnswerHeader();

	void MakeHead(BYTE *pTxData, BYTE *pTxPtr, USHORT nBodyLen, BYTE cSeq);
	void MakeTail(BYTE *pTxData, BYTE *pTxPtr);

	USHORT MakeMessage(BYTE *pTxData, BYTE cSeq, BYTE cOpCode, BYTE *pBodyData, USHORT nBodyLen);

	void ProcessControl(BYTE ch, BYTE *pRxData);
	void EIEControl(BYTE *pRXData);
	void MasterClock(BYTE *pRxData);

	void FullUpdateIndicationMessage(BYTE ch);
	void SpeedCodeIndicationMessage(BYTE ch);
	void EIEControlRpt(BYTE ch, BYTE *pRXData);
	void CCStatusRpt(BYTE ch, BYTE *pRXData);
	void SendAck(BYTE ch);
	void SendNack(BYTE ch);

	BYTE GetRxSeqNumber(BYTE ch);
	void SetRxSeqNumber(BYTE ch, BYTE seq);
	char *GetChannelName(BYTE ch);
	char GetLogType(BYTE ch);
};

/* ------------------------------------------------------------------------ */
extern Comm_CC MyCC;

/* ------------------------------------------------------------------------ */

#endif // _COMMCC_H
