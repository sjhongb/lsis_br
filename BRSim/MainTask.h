/* MainTask.h
 *
 */
 
#ifndef _MAINTASK_H
#define _MAINTASK_H

/* ------------------------------------------------------------------------- */
//#define  IGNORE_FAULT


/* ------------------------------------------------------------------------- */
#define BYTE unsigned char

/* ------------------------------------------------------------------------- */

class MainTask {
public:
	int  m_bIllegalFault;
	int  m_bErrEvg, m_bOtherErrEvg;
//	BYTE *m_pCardBits;
	BYTE m_FaultCount;
	BYTE m_pMsgLastCyc;

public:
	MainTask();
	~MainTask();
	
	int  Run();
	int  SystemInit( int bRamRoad );
	void GetSysIOBitPosition();
	short DigitalIO();
};

/* ------------------------------------------------------------------------- */
extern MainTask Eis;

/* ------------------------------------------------------------------------- */
#endif // _MAINTASK_H
