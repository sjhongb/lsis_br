// LOGFilter.h : header file
//
#ifndef   _LOGFILTER_H
#define   _LOGFILTER_H
/////////////////////////////////////////////////////////////////////////////
// LOGFilter 

#define LOG_MSG_CONTROL		0x01	// 제어관련 메세지
#define LOG_MSG_ERROR		0x02	// 장애관련 메세지
#define LOG_MSG_ILOCK		0x04	// 연동관련 메세지
#define LOG_MSG_MESSAGE		0x08	// 기타(시스템) 메세지
#define LOG_MSG_MASK		0x0f	//
#define LOG_MSG_ALL			0x0f	//(LOG_MSG_CONTROL | LOG_MSG_ERROR | LOG_MSG_ILOCK | LOG_MSG_MESSAGE)
/*
typedef union tagLOGFilter {
	BYTE filter;
	struct {
		BYTE ctrl:	1;
		BYTE error:	1;
		BYTE ilock:	1;
		BYTE msg:	1;
		BYTE count:	4;
	} f;
} LOGFilter;
*/
/////////////////////////////////////////////////////////////////////////////
#endif // _LOGFILTER_H
