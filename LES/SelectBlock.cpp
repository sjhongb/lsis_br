// SelectBlock.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "SelectBlock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectBlock dialog


CSelectBlock::CSelectBlock(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectBlock::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectBlock)
	m_nIndexOfBlock = 2;
	//}}AFX_DATA_INIT
}


void CSelectBlock::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectBlock)
	DDX_Radio(pDX, IDC_INDEX_B1, m_nIndexOfBlock);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectBlock, CDialog)
	//{{AFX_MSG_MAP(CSelectBlock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectBlock message handlers

void CSelectBlock::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_nRealNumberOfBlock = m_nIndexOfBlock + 1;
	if ( m_nRealNumberOfBlock > 4 )
	{
		m_nRealNumberOfBlock+=6;
	}
	
	CDialog::OnOK();
}

BOOL CSelectBlock::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CLESApp* pApp = (CLESApp*)AfxGetApp();

	if (pApp->m_bLeftSideIsUpDirection == 0)
	{
		GetDlgItem( IDC_INDEX_B1 )->SetWindowText("B1 (Dn Direction TGB)");
		GetDlgItem( IDC_INDEX_B2 )->SetWindowText("B2 (Dn Direction TCB)");
		GetDlgItem( IDC_INDEX_B3 )->SetWindowText("B3 (Up Direction TGB)");
		GetDlgItem( IDC_INDEX_B4 )->SetWindowText("B4 (Up Direction TCB)");
	}

	if (pApp->m_VerifyObjectForMessage.strBlokState[1] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B1 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B1 )->SetWindowText("B1");
	}
	if (pApp->m_VerifyObjectForMessage.strBlokState[2] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B2 )->SetWindowText("B2");
	}
	if (pApp->m_VerifyObjectForMessage.strBlokState[3] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B3 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B3 )->SetWindowText("B3");
	}
	if (pApp->m_VerifyObjectForMessage.strBlokState[4] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B4 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B4 )->SetWindowText("B4");
	}
	if (pApp->m_VerifyObjectForMessage.strBlokState[11] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B11 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B11 )->SetWindowText("B11");
	}
	if (pApp->m_VerifyObjectForMessage.strBlokState[12] == "FALSE")
	{
		GetDlgItem( IDC_INDEX_B12 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_B12 )->SetWindowText("B12");
	}
	
	CenterWindow(AfxGetMainWnd());

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
/////////////////////////////////////////////////////////////////////////////
// CSelectBlockControl dialog


CSelectBlockControl::CSelectBlockControl(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectBlockControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectBlockControl)
		// NOTE: the ClassWizard will add member initialization here
	m_nIndexOfBlockFunction = 0;
	//}}AFX_DATA_INIT
}


void CSelectBlockControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectBlockControl)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Radio(pDX, IDC_INDEX_CA, m_nIndexOfBlockFunction);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectBlockControl, CDialog)
	//{{AFX_MSG_MAP(CSelectBlockControl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectBlockControl message handlers

BOOL CSelectBlockControl::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strBlockName;

	strBlockName.Format("B%d",m_nIndexOfBlock);

	if ( m_nIndexOfBlock%2 == 1 )
	{
		strBlockName += " TGB";
		GetDlgItem( IDC_STATIC_B1 )->SetWindowText(strBlockName);
	}
	else
	{
		strBlockName += " TCB";
		GetDlgItem( IDC_STATIC_B1 )->SetWindowText(strBlockName);
		GetDlgItem( IDC_INDEX_LCR )->SetWindowText("LCG");
		GetDlgItem( IDC_INDEX_BCB )->SetWindowText("BRB");
	}

	UpdateData(FALSE);

	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectBlockControl::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	CDialog::OnOK();
}
