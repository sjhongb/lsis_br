// BasicDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "BasicDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBasicDlg dialog


CBasicDlg::CBasicDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBasicDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBasicDlg)
	//}}AFX_DATA_INIT
	m_DlgTitle = "Confirm";
	m_stState="";
	hIcon = AfxGetApp()->LoadIcon(IDI_INFORM);	

}


void CBasicDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBasicDlg)
	DDX_Control(pDX, IDC_STATIC_ICON, m_stIcon);
	DDX_Text(pDX, IDC_STATIC_STATE, m_stState);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBasicDlg, CDialog)
	//{{AFX_MSG_MAP(CBasicDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBasicDlg message handlers

BOOL CBasicDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(m_DlgTitle);
	m_stIcon.SetIcon(hIcon);

	CenterWindow(AfxGetMainWnd());

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
