#if !defined(AFX_SENDTRAINNUMBER_H__A503A8A8_5D69_425F_A52D_05A17714AA79__INCLUDED_)
#define AFX_SENDTRAINNUMBER_H__A503A8A8_5D69_425F_A52D_05A17714AA79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SendTrainNumber.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSendTrainNumber dialog

class CSendTrainNumber : public CDialog
{
// Construction
public:
	CSendTrainNumber(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendTrainNumber)
	enum { IDD = IDD_DLG_SEND_TRAIN_NUMBER };
	int  m_nIndex;
	CString m_strTrainNumber;
	CEdit m_editTrainNumber;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendTrainNumber)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendTrainNumber)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDTRAINNUMBER_H__A503A8A8_5D69_425F_A52D_05A17714AA79__INCLUDED_)
