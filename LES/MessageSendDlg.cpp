// MessageSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "mainfrm.h"
#include "MessageSendDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMessageSendDlg dialog


CMessageSendDlg::CMessageSendDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageSendDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMessageSendDlg)
	m_strSendMessage = _T("");
	m_strMSgCNT = _T("");
	//}}AFX_DATA_INIT
}


void CMessageSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMessageSendDlg)
	DDX_Control(pDX, IDC_EDT_MESSAGESEND, m_cSendMessage);
	DDX_Text(pDX, IDC_EDT_MESSAGESEND, m_strSendMessage);
	DDX_Text(pDX, IDC_STATIC_MSGCNT, m_strMSgCNT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMessageSendDlg, CDialog)
	//{{AFX_MSG_MAP(CMessageSendDlg)
	ON_EN_CHANGE(IDC_EDT_MESSAGESEND, OnChangeEdtMessagesend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMessageSendDlg message handlers

void CMessageSendDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void CMessageSendDlg::OnChangeEdtMessagesend() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int iMsgLength;
	iMsgLength = m_strSendMessage.GetLength();
	
	if ( iMsgLength > (MAX_MSGCNT*10) - 10 ) {
		m_strSendMessage = m_strSendMessage.Left((MAX_MSGCNT*10) - 10);
	}

	iMsgLength = m_strSendMessage.GetLength();
	m_strMSgCNT.Format("%d/%d",iMsgLength,(MAX_MSGCNT*10) - 10);

	UpdateData(FALSE);
}

BOOL CMessageSendDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
