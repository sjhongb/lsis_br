// LESDoc.cpp : implementation of the CLESDoc class
//

#include "stdafx.h"
#include "LES.h"

#include "LESDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLESDoc

IMPLEMENT_DYNCREATE(CLESDoc, CDocument)

BEGIN_MESSAGE_MAP(CLESDoc, CDocument)
	//{{AFX_MSG_MAP(CLESDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLESDoc construction/destruction

CLESDoc::CLESDoc()
{
	// TODO: add one-time construction code here

}

CLESDoc::~CLESDoc()
{
}

BOOL CLESDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLESDoc serialization

void CLESDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLESDoc diagnostics

#ifdef _DEBUG
void CLESDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLESDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLESDoc commands
