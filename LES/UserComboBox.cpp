// UserComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "UserComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserComboBox
CUserComboBox::CUserComboBox()
{
}

CUserComboBox::~CUserComboBox()
{
}

BEGIN_MESSAGE_MAP(CUserComboBox, CComboBox)
	//{{AFX_MSG_MAP(CUserComboBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserComboBox message handlers
void CUserComboBox::OnInitial(BOOL bLock)
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	m_nLimitCount = pApp->m_SystemValue.nUserNumber;
	m_UserSection = "USER_ID";
	m_PassSection = "USER_PASS";
	m_Entry = "ID";
	CString UserName;

	if(bLock == 1){		//제어 잠금 해제 인 경우 
		UserName= pApp->m_SystemValue.strCurrentOperator;		//현재 취급자.
		EnableWindow( FALSE );

//		if(pApp->m_SystemValue.strLockOperator !=""){
//			UserName= pApp->m_SystemValue.strCurrentOperator;		//현재 취급자.
//			UserName= pApp->m_SystemValue.strLockOperator;
//			EnableWindow( FALSE );		//False이면 Lock한 유저 만이 Lock을 풀수 있다. ( combo box를 disable시킨다.)
//		}
//		else{
//		}
	}
	else{
		UserName = pApp->m_SystemValue.strCurrentOperator;
		if (UserName.GetLength() > 8)
			UserName = UserName.Left(8);
	}
	m_UserName = pApp->m_SystemValue.strCurrentOperator;
	if (m_UserName.GetLength() > 8)
		m_UserName = m_UserName.Left(8);

	OnLoadUserID();
	OnSetUserName();
}

void CUserComboBox::OnLoadUserID()
{
	int  i, n;
	LoginItem *pItem;
	CString strID, strPassword, entry;
	CLESApp *pApp = (CLESApp*)AfxGetApp();

	FlushList();
	m_nLimitCount = pApp->GetProfileInt( m_UserSection, "NUMBER", m_nLimitCount);

	for (i=1; i<=m_nLimitCount; i++) {
		entry.Format("%s%d", m_Entry, i);
		strID = pApp->GetProfileString( m_UserSection, entry, _T("") );
		if (strID != "") {
			strPassword = pApp->GetProfileString( m_PassSection, strID, _T("password") );
			pItem = new LoginItem;			
			if ( pItem ) {
				pItem->strID = strID;
				pItem->strPassword = strPassword;
				n = m_Items.GetSize();
				m_Items.SetAtGrow( n, pItem );
			}
		}
	}
	n = m_Items.GetSize();
	for(i=0; i<n; i++) {
		pItem = (LoginItem*)m_Items.GetAt(i);
		AddString(pItem->strID);
	}
}

void CUserComboBox::FlushList()
{
	int i, n;
	LoginItem *pItem;

	n = m_Items.GetSize();
	for(i=0; i<n; i++) {
		pItem = (LoginItem*)m_Items.GetAt(i);
		if ( pItem ) {
			delete pItem;
		}
	}
	m_Items.RemoveAll();
}

BOOL CUserComboBox::OnSetUser(CString &user)
{	
	OnInitial(FALSE);
	int num = FindString(-1,user);
	if (num != CB_ERR) {
		SetCurSel( num );
		return TRUE;
	}
	else 
	return FALSE;
}

void CUserComboBox::OnSetUserName()
{
	if(m_UserName!=""){
		int num = FindString(-1,m_UserName);
		SetCurSel(num);
	}
}
