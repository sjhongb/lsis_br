// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LES.h"
#include "PasswordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog


CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPasswordDlg)
	m_strPassword = _T("");
	m_strUserID = _T("");
	//}}AFX_DATA_INIT
	m_bIsLock = FALSE;
	m_DlgTitle = "User ID and password confirmation";
	m_stState="User ID and password confirmation.";
	hIcon = AfxGetApp()->LoadIcon(IDI_PASSWORD);	

}


void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPasswordDlg)
	DDX_Control(pDX, IDC_COMBOID, m_ComboUser);
	DDX_Control(pDX, IDC_STATIC_ICON, m_stIcon);
	DDX_Text(pDX, IDC_STATIC_STATE, m_stState);
	DDX_Text(pDX, IDC_EDITPASS, m_strPassword);
	DDX_CBString(pDX, IDC_COMBOID, m_strUserID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPasswordDlg, CDialog)
	//{{AFX_MSG_MAP(CPasswordDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg message handlers

BOOL CPasswordDlg::OnInitDialog() 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	str_RegistryKey = "USER_PASS";
	SetWindowText(m_DlgTitle);
	m_stIcon.SetIcon(hIcon);

	m_strPassword.Empty();
	UpdateData(FALSE);

	if(m_bIsLock)		//제어 잠금 상태 이면 Lock operator
		m_ComboUser.OnInitial(TRUE);
	else		//그렇지 않으면 Current Operator
		m_ComboUser.OnInitial(FALSE);

	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPasswordDlg::OnOK() 
{
	// TODO: Add extra validation here
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	DataHeaderType *pHead = (DataHeaderType *)&pApp->SystemVMEM[0];

	UpdateData(TRUE);
	m_strUserID.MakeUpper();
	UpdateData(FALSE);
	UpdateData(TRUE);

	if( m_strPassword == ""){
		MessageBox("input password.");
	}
	else{
		if((m_strPassword == pApp->GetProfileString(str_RegistryKey,m_strUserID,""))/* || m_strPassword == "LSIS"*/){
/*			if ( m_strUserID == "LSIS")				// LSIS로그인에 대해 complain(유경.)
			{
				m_ComboUser.FlushList();
				m_strUserID = _T("");
				pApp->m_bIsSuperUser = TRUE;
				CDialog::OnOK();	
			}
			else*/ if ((pHead->LCCStatus.bLCC1Alive && pApp->m_SystemValue.nSystemNo == 1)
				|| (pHead->LCCStatus.bLCC2Alive && pApp->m_SystemValue.nSystemNo == 2)
				|| pApp->m_SystemValue.nSystemNo     == 3
				|| m_strUserID == "ADMIN")
			{
				m_ComboUser.FlushList();
				
				if ( m_strUserID == "ADMIN" )
					pApp->m_bIsSuperUser = TRUE;
				else
					pApp->m_bIsSuperUser = FALSE;

				m_strUserID = _T("");
				CDialog::OnOK();	
			}
			else
			{
				MessageBox("Cannot log-in now!", "Login");
			}
		}
// 		else if ( m_strPassword == "BR2012" )
// 		{
// 			m_ComboUser.FlushList();
// 			m_strUserID = _T("");
// 			pApp->m_bIsSuperUser = TRUE;
// 			CDialog::OnOK();	
// 		}
		else{
			MessageBox("you have wrong password. \ncheck password and try again.");
		}
	}	
}

void CPasswordDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
//	CLESApp* pApp = AfxGetApp();

//	if ( pApp->)
	CDialog::OnCancel();
}
