//=======================================================
//==              MyComm.cpp
//=======================================================
//  파 일 명 :  MyComm.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :	2.01
//  설    명 :	시리얼 통신을 담당 한다. 
//
//=======================================================
#include "stdafx.h"
#include "MyComm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_COMM_READ	8192

IMPLEMENT_DYNCREATE(CComm, CObject)

CComm::CComm()
{
	m_idComDev	 = NULL;
	m_bConnected = FALSE;
	m_port		 = 1;
	m_rate		 = CBR_9600;
	m_bytesize	 = 8;
	m_stop		 = ONESTOPBIT;
	m_parity	 = NOPARITY;
}

CComm::~CComm()
{
	DestroyComm();
}

DWORD CommWatchProc( LPVOID lpData )
{
	DWORD       dwEvtMask ;
	OVERLAPPED  os ;
	CComm*      npComm = (CComm*) lpData;
	int			nLength ;
	BYTE		InData[4098] ;

	if( npComm == NULL ||
	   !npComm->IsKindOf( RUNTIME_CLASS(CComm) ) )
		return (DWORD)(-1);

	memset( &os, 0, sizeof( OVERLAPPED ) ) ;

	os.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	if (os.hEvent == NULL) {
		MessageBox( NULL, "Failed to create event for thread!", "Com Error!",MB_ICONEXCLAMATION | MB_OK ) ;
		return ( FALSE ) ;
	}

	if (!SetCommMask( npComm->m_idComDev, EV_RXCHAR )) return ( FALSE ) ;

	while ( npComm->m_bConnected )
	{
		dwEvtMask = 0 ;

		WaitCommEvent( npComm->m_idComDev, &dwEvtMask, NULL );

		if ((dwEvtMask & EV_RXCHAR) == EV_RXCHAR)
		{
			do
		    {
				if (nLength = npComm->ReadCommBlock( (LPSTR)InData, 256 ))
				{
					npComm->SetReadData((LPSTR)InData, nLength);
		      	}
		    }
		    while ( nLength > 0 ) ;
		}
   }

   CloseHandle( os.hEvent ) ;
   return( TRUE ) ;
} 


void CComm::SetReadData(LPSTR data, int size)
{

}

void CComm::SetComPort(int port, DWORD rate, BYTE bytesize, BYTE stop, BYTE parity)
{
	m_port = port;
	m_rate = rate;
	m_bytesize = bytesize;
	m_stop = stop;
	m_parity = parity;
}

BOOL CComm::CreateCommInfo()
{
	osWrite.Offset = 0;
	osWrite.OffsetHigh = 0;
	osRead.Offset = 0;
	osRead.OffsetHigh = 0;

    osRead.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL ); 
    if (osRead.hEvent == NULL)
    {
		return FALSE;
    }
    osWrite.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL ); 
    if (osWrite.hEvent == NULL)
    {
		return FALSE;
    }

	return TRUE;

}

BOOL CComm::OpenComPort()
{            
   char       szPort[ 15 ];
   BOOL       fRetVal ;
   COMMTIMEOUTS  CommTimeOuts ;

   if (m_port > MAXPORTS) lstrcpy( szPort, "\\\\.\\TELNET" ) ;
   else					  wsprintf( szPort, "\\\\.\\COM%d", m_port ) ;

   if ((m_idComDev = CreateFile(szPort,
								GENERIC_READ | GENERIC_WRITE,
								0,                    // exclusive access
								NULL,                 // no security attrs
								OPEN_EXISTING,
								FILE_FLAG_NO_BUFFERING |
								FILE_ATTRIBUTE_ARCHIVE |
								FILE_ATTRIBUTE_COMPRESSED,
								NULL )) == (HANDLE) -1 ) {
		DestroyComm();
		return ( FALSE ) ;
   } else {
      SetCommMask( m_idComDev, EV_RXCHAR ) ;
      SetupComm( m_idComDev, 16384, 16384 ) ;
      PurgeComm( m_idComDev, PURGE_TXABORT | PURGE_RXABORT |
                           PURGE_TXCLEAR | PURGE_RXCLEAR ) ;
      CommTimeOuts.ReadIntervalTimeout = 1; //5;
      CommTimeOuts.ReadTotalTimeoutMultiplier = 1 ;
      CommTimeOuts.ReadTotalTimeoutConstant = 0 ;
      CommTimeOuts.WriteTotalTimeoutMultiplier = 0 ;
      CommTimeOuts.WriteTotalTimeoutConstant = 0 ;
      SetCommTimeouts( m_idComDev, &CommTimeOuts ) ;
   }

   fRetVal = SetupConnection() ;

   if (fRetVal) {
      m_bConnected = TRUE ;
      AfxBeginThread( (AFX_THREADPROC)CommWatchProc, (LPVOID)this );
   } else {
      m_bConnected = FALSE ;
      CloseHandle( m_idComDev );
   }
   return ( fRetVal ) ;

}

BOOL CComm::SetupConnection()
{
   BOOL       fRetVal ;
   BYTE       bSet ;
   DCB        dcb ;

   dcb.DCBlength = sizeof( DCB ) ;

   GetCommState( m_idComDev, &dcb ) ;

   dcb.BaudRate = m_rate;
   dcb.ByteSize = m_bytesize;
   dcb.Parity   = m_parity;
   dcb.StopBits = m_stop;

   // setup hardware flow control
   bSet = FALSE;
   dcb.fOutxDsrFlow = bSet ;
   dcb.fDtrControl = DTR_CONTROL_ENABLE ;
   bSet = FALSE;
   dcb.fOutxCtsFlow = bSet ;
   dcb.fRtsControl = RTS_CONTROL_HANDSHAKE ;
   dcb.fRtsControl = RTS_CONTROL_ENABLE;

   // setup software flow control
   bSet = FALSE;

   dcb.fInX		= dcb.fOutX = bSet ;
   dcb.XonChar	= 0x11; //ASCII_XON;
   dcb.XoffChar = 0x13; //ASCII_XOFF;
   dcb.XonLim	= 100 ;
   dcb.XoffLim	= 100 ;

   dcb.fBinary = TRUE ;
   dcb.fParity = FALSE ;

   fRetVal = SetCommState( m_idComDev, &dcb ) ;

   return ( fRetVal ) ;

}



int CComm::ReadCommBlock( LPSTR lpszBlock, int nMaxLength )
{
	BOOL       fReadStat ;
	COMSTAT    ComStat ;
	DWORD      dwErrorFlags;
	DWORD      dwLength;
	dwLength = (DWORD) nMaxLength;

	if (dwLength > 0)
	{
		fReadStat = ReadFile( m_idComDev, lpszBlock,
		                      dwLength, &dwLength, &osRead) ;

		if (!fReadStat)
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				OutputDebugString("\n\rIO Pending");
				while(!GetOverlappedResult( m_idComDev, 
					  &osRead, &dwLength, TRUE ))
				{
					DWORD dwError = GetLastError();
					if(dwError == ERROR_IO_INCOMPLETE)
						continue;
					else
					{
						ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
						break;
					}
				}
			} else {
			    dwLength = 0 ;
				ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
			}
		}
	
	}
	int size = dwLength;
	if (size>MAX_COMM_READ) size = MAX_COMM_READ;
    return ( dwLength ) ;
} // end of ReadCommBlock()



BOOL CComm::DestroyComm()
{
	if ( m_bConnected == TRUE ) {
		CloseHandle( osRead.hEvent ) ;
		CloseHandle( osWrite.hEvent ) ;
		CloseConnection() ;
	}

	return TRUE;
} // end of DestroyTTYInfo()

BOOL CComm::CloseConnection()
{
	m_bConnected = FALSE ;
	return ( TRUE ) ;

} // end of CloseConnection()

int CComm::WriteBlock( char *lpBuffer, int dwBytesToWrite )
{
	BOOL        fWriteStat ;
	DWORD       dwBytesWritten ;

	int nWait = 0;
	int size;              
	while ( dwBytesToWrite ) {
		DWORD nModemStatus = 0;
		size = dwBytesToWrite;
		if (size > 64) size = 64;
		fWriteStat = WriteFile( m_idComDev, (unsigned char*)lpBuffer, size,
	                       &dwBytesWritten, NULL);
		size = dwBytesWritten;
		lpBuffer += size;
		dwBytesToWrite -= size;
	}
	return 1;
}

BOOL CComm::WriteCommBlock( LPSTR lpByte , DWORD dwBytesToWrite)
{

	BOOL        fWriteStat ;
	DWORD       dwBytesWritten ;
	DWORD   	dwError;
	DWORD       dwErrorFlags;
	COMSTAT     ComStat ;

	fWriteStat = WriteFile( m_idComDev, lpByte, dwBytesToWrite,
	                        &dwBytesWritten, &osWrite ) ;

	if (!fWriteStat) 
	{
		if(GetLastError() == ERROR_IO_PENDING)
		{
			
			while(!GetOverlappedResult( m_idComDev,
				   &osWrite, &dwBytesWritten, TRUE ))
			{
				dwError = GetLastError();
				if(dwError == ERROR_IO_INCOMPLETE)
					continue;
				else
				{
					ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
					break;
				}
			}
			
			return ( TRUE ) ;
		} else
		{
			ClearCommError( m_idComDev, &dwErrorFlags, &ComStat ) ;
			return ( FALSE );
		}
	}
	return ( TRUE ) ;

}

BOOL CComm::SendData(BYTE* data, int len)
{
#ifndef	  _SIMDEMO_
  	return WriteBlock( (char*)data, len );
}
#else  
	return WriteCommBlock( (LPSTR)data, len );
#endif 
