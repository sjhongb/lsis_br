// MenuWndDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "MainFrm.h"
#include "LESView.h"
#include "MenuWndDlg.h"
#include "ControlDlg.h"

#include "Windowsx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef BOOL (WINAPI *SetLayer)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);
#define LWA_COLORKEY            0x01
#define LWA_ALPHA               0x02

#define MIN_SIZEY		33
#define MAX_SIZEY		2560
#define MAX_LOG_ROWS	1000

/////////////////////////////////////////////////////////////////////////////
// CMenuWndDlg dialog


CMenuWndDlg::CMenuWndDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuWndDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMenuWndDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bIsHidden = FALSE;
	m_bIsAutoScroll = TRUE;
	m_bBlockDevMode = FALSE;

	m_bIsCreate = FALSE;
	m_btnCallonSet.m_bAreUAdminUser = TRUE;
	m_btnControl.m_bAreUAdminUser = TRUE;
	m_btnMainSet.m_bAreUAdminUser = TRUE;
	m_btnShuntSet.m_bAreUAdminUser = TRUE;
	m_btnSigCancel.m_bAreUAdminUser = TRUE;
	m_btnSwitch.m_bAreUAdminUser = TRUE;
	m_btnUser.m_bAreUAdminUser = TRUE;
}


void CMenuWndDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMenuWndDlg)
	DDX_Control(pDX, IDC_BTN_SCROLL, m_btnScroll);
	DDX_Control(pDX, IDC_BTN_MENU, m_cBtnTitle);
	DDX_Control(pDX, IDC_BUSER, m_btnUser);
	DDX_Control(pDX, IDC_BTN_ETC_BUTTON, m_btnControl);
	DDX_Control(pDX, IDC_BSYSTEM, m_btnSystem);
	DDX_Control(pDX, IDC_BSWITCH, m_btnSwitch);
	DDX_Control(pDX, IDC_BSIGNALCANCEL, m_btnSigCancel);
	DDX_Control(pDX, IDC_BSHUNTROUTE, m_btnShuntSet);
	DDX_Control(pDX, IDC_BCALLONROUTE, m_btnCallonSet);
	DDX_Control(pDX, IDC_BMAINROUTE, m_btnMainSet);
	//}}AFX_DATA_MAP

	DDX_GridControl(pDX, IDC_CUSTOM_GRID, m_ctrlLogGrid);
}


BEGIN_MESSAGE_MAP(CMenuWndDlg, CDialog)
	//{{AFX_MSG_MAP(CMenuWndDlg)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BMAINROUTE, OnBmainroute)
	ON_BN_CLICKED(IDC_BSHUNTROUTE, OnBshuntroute)
	ON_BN_CLICKED(IDC_BSIGNALCANCEL, OnBsignalcancel)
	ON_BN_CLICKED(IDC_BSWITCH, OnBswitch)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BCALLONROUTE, OnBcallonroute)
	ON_BN_CLICKED(IDC_BTN_MENU, OnBtnMenu)
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BTN_SCROLL, OnBtnScroll)
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMenuWndDlg message handlers

//=======================================================
//
//  함 수 명 :  OnInitDialog() 
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  다이알로그를 초기화 한다.
//
//=======================================================
BOOL CMenuWndDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//set Button position
	CDC   *pDC = GetDC();
	CSize chExtent = pDC->GetTextExtent( "W", 1 );
	ReleaseDC( pDC );

	SetPosition();
	OnSetXpButton();

	m_ctrlLogGrid.SetLSEventMode(TRUE);
	m_ctrlLogGrid.SetEditable(FALSE);
 	m_ctrlLogGrid.SetListMode(TRUE);
	//m_ctrlLogGrid.SetSingleRowSelection(TRUE);
	m_ctrlLogGrid.SetHeaderSort(FALSE);
	m_ctrlLogGrid.EnableDragAndDrop(FALSE);
	m_ctrlLogGrid.SetTextBkColor(RGB(0xFF, 0xFF, 0xFF));
	m_ctrlLogGrid.SetDoubleBuffering(TRUE);
	m_ctrlLogGrid.EnableTitleTips(FALSE);
	
	m_ctrlLogGrid.SetRowResize(FALSE);
	m_ctrlLogGrid.SetColumnResize(FALSE);
	m_ctrlLogGrid.SetRowCount(1);
	m_ctrlLogGrid.SetColumnCount(4);
	m_ctrlLogGrid.SetFixedRowCount(1);
	m_ctrlLogGrid.SetFixedColumnCount(0);
	
 	m_ctrlLogGrid.SetItemText(0, 0, "TYPE");
	m_ctrlLogGrid.SetItemText(0, 1, "TIME");
 	m_ctrlLogGrid.SetItemText(0, 2, "SIZE");
	m_ctrlLogGrid.SetItemText(0, 3, "DATA");
	
 	RECT rectClient;
 	m_ctrlLogGrid.GetClientRect(&rectClient);
 	m_ctrlLogGrid.SetColumnWidth(0, 50);
	m_ctrlLogGrid.SetColumnWidth(1, 70);
	m_ctrlLogGrid.SetColumnWidth(2, 50);
 	m_ctrlLogGrid.SetColumnWidth(3, rectClient.right - 170);
		
	LOGFONT lf;
	m_ctrlLogGrid.GetFont()->GetLogFont(&lf);
	
	memset(lf.lfFaceName, 0, sizeof(lf.lfFaceName));
	strcpy(lf.lfFaceName, "Verdana");
	
	CFont Font;
	Font.CreateFontIndirect(&lf);
	m_ctrlLogGrid.SetFont(&Font);
	Font.DeleteObject();
	
	m_ctrlLogGrid.GetNewRowsHeight();
	
	m_bIsCreate = TRUE;
	m_bIsHidden = FALSE;

	return TRUE;
}

int CMenuWndDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	CLESApp* pApp = (CLESApp*) AfxGetApp();

	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
	iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMenuStartY;
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
	iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
	SetWindowPlacement(&iWinPlacement);	

	return 0;
}

//=======================================================
//
//  함 수 명 :  OnSetSizeControl()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  버튼의 크기를 설정한다. 
//
//=======================================================
void CMenuWndDlg::SetPosition()
{
	int height = 25;
	CRect rect;
	GetClientRect(&rect);

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);

	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top+20;
	m_cBtnTitle.SetWindowPlacement(&iBoxPlacement);

	//ListBox
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right -3;
	iBoxPlacement.rcNormalPosition.top    = rect.top + height + 28;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom - height /*- 4*/;
	m_ctrlLogGrid.SetWindowPlacement(&iBoxPlacement);
	m_ctrlLogGrid.ShowWindow(SW_SHOW);
	m_ctrlLogGrid.Invalidate();

	//System Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 22 ;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + height + 26;
	m_btnSystem.SetWindowPlacement(&iBoxPlacement);
	m_btnSystem.ShowWindow(SW_SHOW);

	//User Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 22 ;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + height + 26;
	m_btnUser.SetWindowPlacement(&iBoxPlacement);
	m_btnUser.ShowWindow(SW_SHOW);

	//Control Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4 * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 22;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + height + 26;
	m_btnControl.SetWindowPlacement(&iBoxPlacement);
	m_btnControl.ShowWindow(SW_SHOW);

	//Scroll Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4 * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 22;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + height + 26;
	m_btnScroll.SetWindowPlacement(&iBoxPlacement);
	m_btnScroll.ShowWindow(SW_SHOW);

	//Main Set Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom-height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnMainSet.SetWindowPlacement(&iBoxPlacement);
	m_btnMainSet.ShowWindow(SW_SHOW);

	//Callon Set Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom-height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnCallonSet.SetWindowPlacement(&iBoxPlacement);
	m_btnCallonSet.ShowWindow(SW_SHOW);

	//Cancel Signal Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4 * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom-height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnSigCancel.SetWindowPlacement(&iBoxPlacement);
	m_btnSigCancel.ShowWindow(SW_SHOW);

	//Switch Button
	iBoxPlacement.rcNormalPosition.left   = rect.right / 4 * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / 4 * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom-height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnSwitch.SetWindowPlacement(&iBoxPlacement);
	m_btnSwitch.ShowWindow(SW_SHOW);
}

void CMenuWndDlg::SetHidePosition()
{
	
	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);
	
	CRect rect;
	GetClientRect(&rect);

	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top+20;
	m_cBtnTitle.SetWindowPlacement(&iBoxPlacement);

	m_ctrlLogGrid.ShowWindow(SW_HIDE);
	m_btnSystem.ShowWindow(SW_HIDE);
	m_btnUser.ShowWindow(SW_HIDE);
	m_btnControl.ShowWindow(SW_HIDE);
	m_btnScroll.ShowWindow(SW_HIDE);
	m_btnMainSet.ShowWindow(SW_HIDE);
	m_btnCallonSet.ShowWindow(SW_HIDE);
}


//=======================================================
//
//  함 수 명 :  OnSetXpButton()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  XP 버튼의 툴팁을 설정한다. 
//
//=======================================================
void CMenuWndDlg::OnSetXpButton()
{

	m_btnMainSet.SetThemeHelper(&m_ThemeHelper);
	m_btnMainSet.SetTooltipText(_T("Handled Route."));
	m_btnMainSet.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 128, 0));

	m_btnCallonSet.SetThemeHelper(&m_ThemeHelper);
	m_btnCallonSet.SetTooltipText(_T("Handled Emergency Route Release."));
	m_btnCallonSet.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 128, 0));

	m_btnSigCancel.SetThemeHelper(&m_ThemeHelper);
	m_btnSigCancel.SetTooltipText(_T("Cancel Signal"));
	m_btnSigCancel.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 128, 0));

	m_btnSwitch.SetThemeHelper(&m_ThemeHelper);
	m_btnSwitch.SetTooltipText(_T("Handled Point Machine"));
	m_btnSwitch.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 128, 0));

	m_btnScroll.SetThemeHelper(&m_ThemeHelper);
	m_btnScroll.DrawAsToolbar(TRUE);
	m_btnScroll.SetTooltipText(_T("Toggle auto scroll"));
	m_btnScroll.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 100, 200));

	m_btnSystem.SetThemeHelper(&m_ThemeHelper);
	m_btnSystem.DrawAsToolbar(TRUE);
	m_btnSystem.SetIcon(IDI_SYSTEM, (int)BTNST_AUTO_DARKER);
	m_btnSystem.SetTooltipText(_T("System Menu"));
	m_btnSystem.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 100, 200));
	m_btnSystem.SetMenu(IDR_MENU_SYSTEM, m_hWnd, TRUE);//, IDR_TOOLBAR1);

	m_btnUser.SetThemeHelper(&m_ThemeHelper);
	m_btnUser.DrawAsToolbar(TRUE);
	m_btnUser.SetIcon(IDI_USER, (int)BTNST_AUTO_DARKER);
	m_btnUser.SetTooltipText(_T("User Account"));
	m_btnUser.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 100, 200));
	m_btnUser.SetMenu(IDR_MENU_USER, m_hWnd, TRUE);//, IDR_TOOLBAR1);

	m_btnControl.SetThemeHelper(&m_ThemeHelper);
	m_btnControl.DrawAsToolbar(TRUE);
	m_btnControl.SetIcon(IDI_CONTROL, (int)BTNST_AUTO_DARKER);
	m_btnControl.SetTooltipText(_T("Control"));
	m_btnControl.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(0, 100, 200));

	CLESApp* pApp = (CLESApp*)AfxGetApp();
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("LAKSAM")==0
		|| pApp->m_VerifyObjectForMessage.strStationName.Find("CHINKI")==0)
	{
		m_btnControl.SetMenu(IDR_MENU_CONTROL, m_hWnd, TRUE);//, IDR_TOOLBAR1);
	}
	else
	{
		m_btnControl.SetMenu(IDR_MENU_CONTROL1, m_hWnd, TRUE);//, IDR_TOOLBAR1);
	}

	m_cBtnTitle.SetBitmaps(IDB_MENUTITLE , (int)BTNST_AUTO_DARKER);

}

BOOL CMenuWndDlg::PreTranslateMessage(MSG* pMsg) 
{
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();
	CLESApp* pApp = (CLESApp*) AfxGetApp();

	if ((pMsg->message == WM_KEYDOWN) || (pMsg->message == WM_SYSKEYDOWN)) 
	{
		switch(pMsg->wParam) {

		case VK_ESCAPE:
			return TRUE;

		case VK_RETURN:
			return TRUE;

		case VK_F1:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_RUN);
				}
				else{
					m_pMainFrame->OnF1();
					return TRUE;
				}
			}
			break;
		case VK_F2:		// About
//			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {					// PAS상태에서도 키보드 제어만으로 PAS를 풀기위해 주석처리.
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_RESET);
				}
				else{
					m_pMainFrame->OnF2();
					return TRUE;
				}
//			}
			break;
		case VK_F3:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_TIMESET);
				}
				else{
					m_pMainFrame->OnF3();
					return TRUE;
				}
			}
			break;
		case VK_F4:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_EXIT);
				}
				else{
					m_pMainFrame->OnF4();
					return TRUE;
				}
			} else {
				return TRUE;
			}
			break;
		case VK_F5:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_APP_ABOUT);
				}
				else if ( pApp->m_bISCTCMode == FALSE )
				{
					m_pMainFrame->OnF5();
					return TRUE;
				}
			}
			break;
		case VK_F6:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ALLUSER);
				}
				else if ( pApp->m_bISCTCMode == FALSE )
				{
					m_pMainFrame->OnF6();
					return TRUE;
				}
			}
			break;
		case VK_F7:		// About
			if((pMsg->lParam&0x20000000)==0x20000000){
				m_pMainFrame->PostMessage(WM_COMMAND,ID_CHANGE);
			}
			else if ( pApp->m_bISCTCMode == FALSE )
			{
				m_pMainFrame->OnF7();
				return TRUE;
			}
			break;
		case VK_F8:		// About
			if((pMsg->lParam&0x20000000)==0x20000000){
				m_pMainFrame->PostMessage(WM_COMMAND,ID_LOCK);
			}
			else if ( pApp->m_bISCTCMode == FALSE )
			{
				m_pMainFrame->OnF8();
				return TRUE;
			}
			break;
		case VK_F9:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_SLS);
				}
				else{
					m_pMainFrame->OnF9();
					return TRUE;
				}
			}
			break;
		case VK_F10:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ATB);
				}
				else{
					m_pMainFrame->OnF10();
					return TRUE;
				}
			}
			break;
		case VK_F11:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ND);
				}
				else{
					m_pMainFrame->OnF11();
					return TRUE;
				}
			}
			break;
		case 0x31:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On1();
					return TRUE;
				}
			}
			break;
		case 0x32:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On2();
					return TRUE;
				}
			}
			break;
		case 0x33:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On3();
					return TRUE;
				}
			}
			break;
		case 0x34:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On4();
					return TRUE;
				}
			}
			break;
		case 0x35:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On5();
					return TRUE;
				}
			}
			break;
		case 0x36:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On6();
					return TRUE;
				}
			}
			break;
		case 0x37:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On7();
					return TRUE;
				}
			}
			break;
		case 0x38:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On8();
					return TRUE;
				}
			}
			break;
		case 0x39:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On9();
					return TRUE;
				}
			}
			break;
		case 0x30:
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On0();
					return TRUE;
				}
			}
			break;
		case 'e':
		case 'E':
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->OnAltPlusE();
					return TRUE;
				}
			}
			break;
		case 'l':
		case 'L':
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->OnAltPlusL();
					return TRUE;
				}
			}
			break;
		default:	
			break;
		}
	}	
	return CDialog::PreTranslateMessage(pMsg);
}

void CMenuWndDlg::Bmainroute() 
{
	OnBmainroute(); 
}
//=======================================================
//
//  함 수 명 :  OnBmainroute()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메인진로 취급을 눌렀을때처리
//
//=======================================================
void CMenuWndDlg::OnBmainroute() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Handled route";
	dlg.m_stState = "If double-clicking starter signal, destination can be selected.";
	dlg.m_stScript = "Start : ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALON);	
	dlg.m_Type = MENU_MAIN;	//주 신호기 
	dlg.DoModal();			
}

void CMenuWndDlg::Bcallonroute() 
{
	OnBcallonroute();
}

void CMenuWndDlg::OnBcallonroute() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Emergency Route Release";
	dlg.m_stState = "";
	dlg.m_stScript = "Track : ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALON);	
	dlg.m_Type = MENU_ERR;	//유도 신호기 
	dlg.DoModal();
}

void CMenuWndDlg::Bshuntroute() 
{
	OnBshuntroute();
}

void CMenuWndDlg::OnBshuntroute() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Handled shunt route";
	dlg.m_stState = "If do double click, can select reaching.";
	dlg.m_stScript = "Start : ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALON);	
	dlg.m_Type = MENU_SHUNT;	//입환 신호기 
	dlg.DoModal();			
}

void CMenuWndDlg::Bsignalcancel() 
{
	OnBsignalcancel();
}

void CMenuWndDlg::OnBsignalcancel() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Handled signal cancel";
	dlg.m_stState = "If double-clicking starter signal, route can be cancelled";
	dlg.m_stScript = "Signal: ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SIGNALOFF);	
	dlg.m_Type = MENU_SIGNAL;	//신호기 취소
	dlg.DoModal();			
}

void CMenuWndDlg::Bswitch() 
{
	OnBswitch();
}

void CMenuWndDlg::OnBswitch() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Handled point";
	dlg.m_stState = "If double-clikcing point, point can be reversed.";
	dlg.m_stScript = "point : ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_SWITCH);	
	dlg.m_Type = MENU_POINT;	//선로전환기 
	dlg.DoModal();			
}

void CMenuWndDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	m_bIsCreate = FALSE;

}

void CMenuWndDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(m_bIsCreate)
	{
		if(m_bIsHidden)
		{
			SetHidePosition();
		}
		else
		{
			SetPosition();
		}
	}
}

BOOL CMenuWndDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::DestroyWindow();
}

void CMenuWndDlg::OnBtnMenu() 
{
	// TODO: Add your control notification handler code here
	if(m_bIsHidden == TRUE)
	{
		m_bIsHidden = FALSE;
		
		CLESApp* pApp = (CLESApp*) AfxGetApp();
		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		iWinPlacement.rcNormalPosition.left   =  pApp->m_SystemValue.nMenuStartX;
		iWinPlacement.rcNormalPosition.top  = pApp->m_SystemValue.nMenuStartY;
		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
		iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
		SetWindowPlacement(&iWinPlacement);	
	}
	else 
	{
		m_bIsHidden = TRUE;
		
		CLESApp* pApp = (CLESApp*) AfxGetApp();
		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		iWinPlacement.rcNormalPosition.left   =  pApp->m_SystemValue.nMenuStartX;
		iWinPlacement.rcNormalPosition.top  = pApp->m_SystemValue.nMainStartY + pApp->m_SystemValue.nMainSizeY - MIN_SIZEY ;
		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
		iWinPlacement.rcNormalPosition.bottom =  pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMainSizeY;
		SetWindowPlacement(&iWinPlacement);	
	}	
}

void CMenuWndDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
#ifdef _LLR
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
#else
	CLESApp* pApp = (CLESApp*)AfxGetApp();
#endif

	lpMMI->ptMinTrackSize.x = pApp->m_SystemValue.nLogSizeX;	// 최소값
	lpMMI->ptMaxTrackSize.x = pApp->m_SystemValue.nLogSizeX;	// 최대값
	
	if(m_bIsHidden)
	{
		lpMMI->ptMinTrackSize.y = MIN_SIZEY; 
		lpMMI->ptMaxTrackSize.y = MIN_SIZEY; 
	}
	else
	{
		lpMMI->ptMinTrackSize.y = pApp->m_SystemValue.nLogSizeY;
		lpMMI->ptMaxTrackSize.y = MAX_SIZEY;
	}
	
	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CMenuWndDlg::AddDataLog(BOOL bRecv, int nLength, BYTE *pBuffer)
{
	CString strData, strTemp;

	for(int i = 0 ; i < nLength ; i++) 
	{
		strTemp.Format("%02X ", pBuffer[i]);
		strData += strTemp;
	}

	int nRow, nMinRow, nMaxRow;
	CCellRange cellRange = m_ctrlLogGrid.GetSelectedCellRange();

	if(bRecv)
	{
		if(m_strLastRcvData == strData)
		{
			return;
		}

		m_strLastRcvData = strData;

		nRow = m_ctrlLogGrid.InsertRow("RECV");
	}
	else
	{
		if(m_strLastSendData == strData)
		{
			return;
		}

		m_strLastSendData = strData;

		nRow = m_ctrlLogGrid.InsertRow("SEND");
	}

	CJTime curTime = CJTime::GetCurrentTime();

	CString strLength;
	strLength.Format("%d", nLength);

	m_ctrlLogGrid.SetItemText(nRow, 1, curTime.Format("%H:%M:%S"));
	m_ctrlLogGrid.SetItemText(nRow, 2, strLength);
	m_ctrlLogGrid.SetItemText(nRow, 3, strData);

	RECT rectClient;
	m_ctrlLogGrid.GetClientRect(&rectClient);
 	m_ctrlLogGrid.SetColumnWidth(3, rectClient.right - 170);

	if(m_bIsAutoScroll)
	{
		m_ctrlLogGrid.EnsureVisible(nRow, 0);
	}

	m_ctrlLogGrid.Invalidate();

	if(nRow > MAX_LOG_ROWS)
	{
		m_ctrlLogGrid.DeleteRow(1);

		nMinRow = cellRange.GetMinRow();
		nMaxRow = cellRange.GetMaxRow();
		
		if((nMinRow == 1) && (nMaxRow == 1))
		{
			return;
		}

		if(nMinRow > 1)
		{
			nMinRow--;
		}


		if(nMaxRow > 1)
		{
			nMaxRow--;
		}

		cellRange.SetMinRow(nMinRow);
		cellRange.SetMaxRow(nMaxRow);
	}

	m_ctrlLogGrid.SetSelectedRange(cellRange);
}


void CMenuWndDlg::AddErrorLog(CString &strData)
{
	CJTime curTime = CJTime::GetCurrentTime();
	int nRow, nMinRow, nMaxRow;
	CCellRange cellRange = m_ctrlLogGrid.GetSelectedCellRange();

	nRow = m_ctrlLogGrid.InsertRow("ERR");

	m_ctrlLogGrid.SetItemText(nRow, 1, curTime.Format("%H:%M:%S"));
	m_ctrlLogGrid.SetItemText(nRow, 3, strData);

	RECT rectClient;
	m_ctrlLogGrid.GetClientRect(&rectClient);
 	m_ctrlLogGrid.SetColumnWidth(3, rectClient.right - 170);

	if(m_bIsAutoScroll)
	{
		m_ctrlLogGrid.EnsureVisible(nRow, 0);
	}

	m_ctrlLogGrid.Invalidate();

	if(nRow > MAX_LOG_ROWS)
	{
		m_ctrlLogGrid.DeleteRow(1);
		
		nMinRow = cellRange.GetMinRow();
		nMaxRow = cellRange.GetMaxRow();
		
		if((nMinRow == 1) && (nMaxRow == 1))
		{
			return;
		}
		
		if(nMinRow > 1)
		{
			nMinRow--;
		}
		
		
		if(nMaxRow > 1)
		{
			nMaxRow--;
		}
		
		cellRange.SetMinRow(nMinRow);
		cellRange.SetMaxRow(nMaxRow);
	}
	
	m_ctrlLogGrid.SetSelectedRange(cellRange);
}

void CMenuWndDlg::OnBtnScroll() 
{
	// TODO: Add your control notification handler code here
	if(m_bIsAutoScroll)
	{
		m_bIsAutoScroll = FALSE;
		
		m_btnScroll.SetWindowText("Auto Scroll OFF");
	}
	else
	{
		m_bIsAutoScroll = TRUE;
		
		m_btnScroll.SetWindowText("Auto Scroll ON");
	}
}

BOOL CMenuWndDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	::SendMessage(m_ctrlLogGrid.m_hWnd, WM_MOUSEWHEEL, (WPARAM)(((DWORD)zDelta) << 16 | (DWORD)nFlags), (LPARAM)(((DWORD)pt.y) << 16 | (DWORD)pt.x));

	//return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	return TRUE;
}
