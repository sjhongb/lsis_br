// TrackBlockDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "TrackBlockDlg.h"
#include "MainFrm.h"
#include "LESView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrackBlockDlg dialog


CTrackBlockDlg::CTrackBlockDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTrackBlockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrackBlockDlg)
	m_strPassword = _T("");
	m_nMode = -1;
	m_strCmd = _T("");
	m_strUserID = _T("");
	//}}AFX_DATA_INIT
	m_ControlMode = 0;
	m_strTrackName="";
	m_BlockID = 0;
	m_bLock=m_bFree = 0x00;
	m_RegistryBlockNumber = 0;
}


void CTrackBlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrackBlockDlg)
	DDX_Control(pDX, IDC_COMBOUSER, m_ComboUser);
	DDX_Text(pDX, IDC_EDIT_PASS, m_strPassword);
	DDX_Radio(pDX, IDC_RADIO1, m_nMode);
	DDX_Text(pDX, IDC_STATIC_CMD, m_strCmd);
	DDX_CBString(pDX, IDC_COMBOUSER, m_strUserID);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTrackBlockDlg, CDialog)
	//{{AFX_MSG_MAP(CTrackBlockDlg)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrackBlockDlg message handlers

BOOL CTrackBlockDlg::OnInitDialog() 
{
	CLESApp* pApp;
	pApp = (CLESApp*)AfxGetApp();

	CString strID = "";
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	str_RegistryPass = "USER_PASS";
	str_RegistryBlocking = "BLOCKING_TRACK";
	str_RegistryUser = "BLOCKING_USER";
	str_Entry = "Blcok";
	if(!OnSetInit()){
		return TRUE;
	}
	OnSetCmd();
	UpdateData( FALSE );

	//취급자 ID 표시
	//현재 트랙을 복원하는 경우라면 트랙을 차단한 취급자 ID를 가져오고 
	m_RegistryBlockNumber = pApp->GetProfileInt(str_RegistryBlocking,"NUMBER",0);
	strID = pApp->GetProfileString( str_RegistryUser, m_strTrackName, _T("") );
	if(m_bFree && (strID!="")&& m_RegistryBlockNumber){	//복구 취급인  경우 
		if(!m_ComboUser.OnSetUser(strID)){		//궤도 차단 취급을 한 취급자의 ID로 표시하기 없으면 현재 사용자 
			AfxMessageBox("사용자 ID목록에 차단한 취급자의 정보가 없어\n현재 취급자의 ID를 표시합니다.");	
			m_ComboUser.OnInitial(FALSE);
		}
		m_ComboUser.EnableWindow(FALSE);
	}
	else{
		m_ComboUser.OnInitial(FALSE);
	}

	CenterWindow(AfxGetMainWnd());

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CTrackBlockDlg::OnSetInit()
{
	CLESApp* pApp;
	pApp = (CLESApp*)AfxGetApp();

	CWnd *pRadio1 =	GetDlgItem( IDC_RADIO1 );
	CWnd *pRadio2 =	GetDlgItem( IDC_RADIO2 );
	CWnd *pRadio3 =	GetDlgItem( IDC_RADIO3 );

	
	m_bLock = m_BlockID & 0xf0;
	m_bFree = m_BlockID & 0x0f;
	if((m_bLock != 0)&&(m_bFree != 0)){		//이상함 표시 ( 차단 취급과 취소 취급이 같이 옮)
		AfxMessageBox("궤도 차단 비트와 복구 비트가 같이 올라옴");
		return FALSE;
	}
	else if((m_bLock == 0)&&( m_bFree == 0 )){	//현재는 LSM에서 트랙 클릭하면 기본으로 사용자 금지로 들어옮
		AfxMessageBox("궤도 차단 비트와 복구 비트가 모두 0임\n 어떻게 여기 들어왔지?");
		return FALSE;
	}
	else if(m_bLock != 0){	//궤도 차단 취급 
		if(pApp->m_bBlockTrackIng){	//이미 메뉴에서 궤도 차단을 선택하여 디 다이얼로그를 띄운 경우
			m_nMode = (m_bLock>>6)-1 ;
			if(m_nMode == 0){	//단전 작업
				pRadio1->EnableWindow( TRUE );
				pRadio2->EnableWindow( FALSE );
				pRadio3->EnableWindow( FALSE );
			}
			else if(m_nMode == 1){		//모터카 점유
				pRadio1->EnableWindow( FALSE );
				pRadio2->EnableWindow( TRUE );
				pRadio3->EnableWindow( FALSE );
			}
			else if(m_nMode == 2){		//취급자 금지 
				pRadio1->EnableWindow( FALSE );
				pRadio2->EnableWindow( FALSE );
				pRadio3->EnableWindow( TRUE );
			}
			else{	//이상한 모드임 
				AfxMessageBox("단전,모터카,취급자 금지 아무것도 아님");
				return FALSE;
			}
		}
		else{		//LSM에서 궤도를 선택하여 들어온 경우 
			m_nMode = (m_bLock>>6) -1 ;
			pRadio1->ShowWindow( SW_SHOW );
			pRadio2->ShowWindow( SW_SHOW );
			pRadio3->ShowWindow( SW_SHOW );

		}
	}
	else if(m_bFree != 0){		//궤도 차단 복구 취급 
			m_nMode = ((m_bFree >> 2) & 3) - 1;
			if(m_nMode == 0){	//단전 작업 복구
				pRadio1->EnableWindow( TRUE );
				pRadio2->EnableWindow( FALSE );
				pRadio3->EnableWindow( FALSE );
			}
			else if(m_nMode == 1){		//모터카 점유 복구
				pRadio1->EnableWindow( FALSE );
				pRadio2->EnableWindow( TRUE );
				pRadio3->EnableWindow( FALSE );
			}
			else if(m_nMode == 2){		//취급자 금지 복구
				pRadio1->EnableWindow( FALSE );
				pRadio2->EnableWindow( FALSE );
				pRadio3->EnableWindow( TRUE );
			}
			else{	//이상한 모드임 
				AfxMessageBox("단전,모터카,취급자 금지 아무것도 아님");
				return FALSE;
			}
	}
	return TRUE;
}

void CTrackBlockDlg::OnSetCmd()
{
	m_strCmd = "궤도 [" +m_strTrackName+"] - ";
	switch(m_nMode){
		case 0:
			m_strCmd += "단전 작업";
			break;
		case 1:
			m_strCmd += "모터카점유";
			break;
		case 2:
			m_strCmd += "취급자금지";
			break;
		default:
			AfxMessageBox("궤도 차단 명령어 스트링 만드는중 이상함 ");
			break;
	}
		m_strCmd += "에 의한 궤도차단 ";
		
		if(m_bLock){
			m_strCmd += "--> 취급 ";
		}
		if(m_bFree){
			m_strCmd += "--> 복구";
		}
}

void CTrackBlockDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	OnSetCmd();
	UpdateData(FALSE);
}

void CTrackBlockDlg::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	OnSetCmd();
	UpdateData(FALSE);
}

void CTrackBlockDlg::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	OnSetCmd();
	UpdateData(FALSE);
}

void CTrackBlockDlg::OnOK() 
{
	CWinApp* pApp = AfxGetApp();

	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_bLock){		//차단 취급 일때만 값지정 , 복구 일때는 모드 값을 바꿀 수 없음 
		m_BlockID = ((m_nMode+1) & 3) << 6; 
	}

	if( m_strPassword == ""){
		MessageBox("input password.");
	}
	else{
		if(m_strPassword == pApp->GetProfileString(str_RegistryPass,m_strUserID,"")){
			m_ComboUser.FlushList();
			if(m_bLock){
				m_RegistryBlockNumber++;
				OnSaveLockUser();
			}
			else if(m_bFree){
				OnDeleteLockUser();
				m_RegistryBlockNumber--;
			}
			pApp->WriteProfileInt(str_RegistryBlocking,"NUMBER",m_RegistryBlockNumber);
			CDialog::OnOK();	
		}
		else{
			MessageBox("you have wrong password. \ncheck password and try again.");
		}
	}

}

void CTrackBlockDlg::OnSaveLockUser()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	CString entry;
	entry.Format("%s%d", str_Entry, m_RegistryBlockNumber);		//차단 궤도 개수 
	pApp->WriteProfileString( str_RegistryBlocking, entry, m_strTrackName );
	pApp->WriteProfileString( str_RegistryUser, m_strTrackName, m_strUserID );
}

void CTrackBlockDlg::OnDeleteLockUser()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	CString entry,strID;
	int nIndex;

	for(int i=1;i<=m_RegistryBlockNumber;i++){
		entry.Format("%s%d", str_Entry, i);		//차단 궤도 개수 
		if(m_strTrackName == pApp->GetProfileString(str_RegistryBlocking,entry,"")){
			nIndex = i;
			break;
		}
	}

	for(i=nIndex; i<=m_RegistryBlockNumber; i++) {		//땡겨서 저장하면 마지막에 지우려는 데이터만 남는다.
		entry.Format("%s%d", str_Entry, i+1);
		strID = pApp->GetProfileString( str_RegistryBlocking, entry, _T("") );
		entry.Format("%s%d", str_Entry, i);
		pApp->WriteProfileString( str_RegistryBlocking, entry, strID );
	
	}
	HKEY	hResult;		//User 목록과 Password목록 동시에 지우기 
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\BLOCKING_USER",&hResult);
	RegDeleteValue(hResult,m_strTrackName); //지우려는 궤도의 취급자 먼저 지우고
	RegCloseKey(hResult);
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\BLOCKING_TRACK",&hResult);
	CString ttt;
	ttt.Format("%s%d",str_Entry,m_RegistryBlockNumber); //마지막 궤도 (지워진) 지우기 -> 빈칸
	RegDeleteValue(hResult,ttt);
	RegCloseKey(hResult);
}

