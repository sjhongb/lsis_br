// PassChangeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "PassChangeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPassChangeDlg dialog


CPassChangeDlg::CPassChangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPassChangeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPassChangeDlg)
	m_strBefore = _T("");
	m_strPassNew = _T("");
	m_strConfirm = _T("");
	//}}AFX_DATA_INIT
}


void CPassChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPassChangeDlg)
	DDX_Text(pDX, IDC_EDIT1, m_strBefore);
	DDX_Text(pDX, IDC_EDIT2, m_strPassNew);
	DDX_Text(pDX, IDC_EDIT3, m_strConfirm);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPassChangeDlg, CDialog)
	//{{AFX_MSG_MAP(CPassChangeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPassChangeDlg message handlers

BOOL CPassChangeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(m_DlgTitle);
	
	CenterWindow(AfxGetMainWnd());
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPassChangeDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );

	if ( m_strBefore !=m_OriginPass ) {
		MessageBox("Warning!\n\nExisting password is different. \nInput again.");
		return;
	}
	if ( m_strPassNew.IsEmpty() ) {
		MessageBox("Warning!\n\nThere is no inputed password. \nInput password");
		return;
	}
	if ( m_strConfirm.IsEmpty() ) {
		MessageBox("Warning!\n\nThere is no inputed password. \nInput password");
		return;
	}
	if ( m_strPassNew != m_strConfirm ) {
		MessageBox("Warning!\n\nPassword and password that re-enter do not conform.\nPassword input again");
		return;
	}
	CDialog::OnOK();
}

void CPassChangeDlg::OnSetPass(CString &password)
{
	m_OriginPass = password;
}
