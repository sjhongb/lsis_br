// LESView.h : interface of the CLESView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LESVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_)
#define AFX_LESVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//{{AFX_INCLUDES()
#include "LSM.h"
//}}AFX_INCLUDES
#include "stdafx.h"
#include "resource.h"
#include "LES.h"
#include "LESDoc.h"
#include "../INCLUDE/GridCtrl.h"
//#include "ServerSocket.h"		//LAN통신 삭제.
//#include "ClientSocket.h"		//LAN통신 삭제.

#define WM_F1				(WM_USER+1001)
#define WM_F2				(WM_USER+1002)
#define WM_F3				(WM_USER+1003)
#define WM_F4				(WM_USER+1004)
#define WM_F5				(WM_USER+1005)
#define WM_F6				(WM_USER+1006)
#define WM_F7				(WM_USER+1007)
#define WM_F8				(WM_USER+1008)
#define WM_F9				(WM_USER+1009)
#define WM_F10				(WM_USER+1010)
#define WM_F11				(WM_USER+1011)
#define WM_F12				(WM_USER+1012)
#define WM_AUTOEXIT	        (WM_USER+1013)
#define WM_AUTOEXITEX       (WM_USER+1014)
class CLESView : public CFormView
{
protected: // create from serialization only
	CLESView();
	DECLARE_DYNCREATE(CLESView)

public:
	CGridCtrl *m_pCtrl;
	char    m_CmdQue[24];
	CLESApp* pApp;
	CLESDoc* GetDocument();
//	CServerSocket* m_ServerSocket;		//LAN통신 삭제.
//	CClientSocket* m_ClientSocket;		//LAN통신 삭제.
	int m_iSysType;
	int m_iClientCnt;
	//for XP Style 
	//사용자 관리 다이얼로그 

	//{{AFX_DATA(CLESView)
	enum { IDD = IDD_LES_FORM };
	CLSM	m_Screen;
	//}}AFX_DATA

// Attributes
public:
// Operations
public:
	virtual void OnFilePrintPreview();
	char m_strSend[2048];
	void UserIDUpdate(char Buffer[2048]);
	void OnDisconnect();
//	void OnConnect();			//LAN통신 사용안함.
//	void OnSend( int nOP);		//LAN통신 사용안함.
//	void StartTimer();			//LAN통신 사용안함.
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLESView)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLESView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLESView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	afx_msg void OnOleSendMessageLSMctrl(long message, long wParam, long lParam);
	afx_msg void OnOperateLSMctrl(short nFunction, short nArgument);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LESView.cpp
inline CLESDoc* CLESView::GetDocument()
   { return (CLESDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LESVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_)
