#if !defined(AFX_USERCOMBOBOX_H__BE0E4CEE_C7FD_4A47_B855_8306A4884005__INCLUDED_)
#define AFX_USERCOMBOBOX_H__BE0E4CEE_C7FD_4A47_B855_8306A4884005__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserComboBox window

class CUserComboBox : public CComboBox
{
// Construction
public:
	CUserComboBox();
	int m_nLimitCount;
	CString m_UserSection;
	CString m_PassSection;
	CString m_Entry;
	CPtrArray m_Items;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserComboBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnSetUserName();
	BOOL OnSetUser(CString &user);
	CString m_UserName;
	void FlushList();
	void OnLoadUserID();
	void OnInitial(BOOL bLock = FALSE);
	virtual ~CUserComboBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CUserComboBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERCOMBOBOX_H__BE0E4CEE_C7FD_4A47_B855_8306A4884005__INCLUDED_)
