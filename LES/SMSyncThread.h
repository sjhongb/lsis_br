#if !defined(AFX_SMSYNCTHREAD_H__9867B5AA_3CA0_4580_BA82_B03559729C64__INCLUDED_)
#define AFX_SMSYNCTHREAD_H__9867B5AA_3CA0_4580_BA82_B03559729C64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SMSyncThread.h : header file
//

#define WM_SHARED_MEMORY_UPDATED	WM_USER + 5678

/////////////////////////////////////////////////////////////////////////////
// CSMSyncThread thread

class CSMSyncThread : public CWinThread
{
	DECLARE_DYNCREATE(CSMSyncThread)
protected:
	CSMSyncThread();           // protected constructor used by dynamic creation
	
public:
	virtual ~CSMSyncThread();

// Attributes
public:

// Operations
public:
	void End();
	void Start(HWND hParentWnd);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSMSyncThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL	m_bIsRunning;
	HANDLE	m_hEventRun;
	HWND	m_hParentWnd;

	// Generated message map functions
	//{{AFX_MSG(CSMSyncThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMSYNCTHREAD_H__9867B5AA_3CA0_4580_BA82_B03559729C64__INCLUDED_)
