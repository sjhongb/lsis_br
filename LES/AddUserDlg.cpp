//=======================================================
//==              AddUserDlg.cpp
//=======================================================
//  파 일 명 :  AddUserDlg.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  add user
//
//=======================================================

#include "stdafx.h"
#include "LES.h"
#include "AddUserDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddUserDlg dialog

//=======================================================
//
//  함 수 명 :  CAddUserDlg
//  함수출력 :  없음
//  함수입력 :  CWnd* pParent
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CAddUserDlg::CAddUserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddUserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddUserDlg)
	m_strID = _T("");
	m_strPassword = _T("");
	m_strConfirm = _T("");
	//}}AFX_DATA_INIT
}

//=======================================================
//
//  함 수 명 :  DoDataExchange
//  함수출력 :  없음
//  함수입력 :  CDataExchange* pDX
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :
//
//=======================================================
void CAddUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddUserDlg)
	DDX_Text(pDX, IDC_USER, m_strID);
	DDV_MaxChars(pDX, m_strID, 8);
	DDX_Text(pDX, IDC_PASS1, m_strPassword);
	DDV_MaxChars(pDX, m_strPassword, 8);
	DDX_Text(pDX, IDC_PASS2, m_strConfirm);
	DDV_MaxChars(pDX, m_strConfirm, 8);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddUserDlg, CDialog)
	//{{AFX_MSG_MAP(CAddUserDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddUserDlg message handlers

void CAddUserDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );

	m_strID.MakeUpper();
	m_strID = m_strID.Left(_MAX_USER_ID_LENGTH);
	m_strPassword = m_strPassword.Left(_MAX_USER_PW_LENGTH);
	UpdateData( FALSE );
	UpdateData( TRUE );
	if ( m_strID == "" ) {
		MessageBox("Warning!\n\nUser name was not inputed.\nInput again.");
		return;
	}
	if ( (m_strID.GetAt(0) == ' ') && (m_strPassword.GetAt(0) == ' ') ) {
		MessageBox("Warning!\n\nInputed wrong user name or password.\nFirst character can not begin to blank."
				   "\nInput again.");
		return;
	}
	if ( m_strPassword.IsEmpty() ) {
		MessageBox("Warning!\n\nDid not input password.\nInput password");
		return;
	}
	if ( m_strPassword != m_strConfirm ) {
		MessageBox("Warning!\n\nPassword and re-enter password  that input do not conform.\nPassword input again.");
		return;
	}
	
	CDialog::OnOK();
}

BOOL CAddUserDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
