// MenuListBox.cpp : implementation file
//

#include "stdafx.h"
#include "MenuListBox.h"
#include "MainFrm.h"
#include "LESView.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	ID_MENU_ROUTE	150
#define MAX_DEST_NUM	50
#define ID_MENU_BLOCKING  300
#define	ID_MENU_CROUTE	450
#define	ID_MENU_SROUTE	600

/////////////////////////////////////////////////////////////////////////////
// CMenuListBox

CMenuListBox::CMenuListBox()
{
	m_strDestName = new CString[MAX_DEST_NUM];
	m_nType = 0;
	m_bSelect = TRUE;
}

CMenuListBox::~CMenuListBox()
{
	delete [] m_strDestName;
	m_nType = 0;
	m_bSelect = TRUE;
}


BEGIN_MESSAGE_MAP(CMenuListBox, CListBox)
	//{{AFX_MSG_MAP(CMenuListBox)
	ON_WM_CREATE()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND_RANGE(ID_MENU_ROUTE, ID_MENU_ROUTE+MAX_DEST_NUM, OnSelectDest)
	ON_COMMAND_RANGE(ID_MENU_BLOCKING, ID_MENU_BLOCKING+2, OnSelectBlocking)
	//}}AFX_MSG_MAP
	END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMenuListBox message handlers

void CMenuListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
	CDC* pDC    = CDC::FromHandle(lpDrawItemStruct->hDC);
	if ((int)lpDrawItemStruct->itemID < 0)
	{
		if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && 
			(lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			pDC->DrawFocusRect(&lpDrawItemStruct->rcItem);
		}
		else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) &&	
			!(lpDrawItemStruct->itemState & ODS_FOCUS)) 
		{
			pDC->DrawFocusRect(&lpDrawItemStruct->rcItem); 
		}
		return;
	}

    CRect  rcItem(lpDrawItemStruct->rcItem); // To draw the focus rect.
    CRect  rClient(rcItem); // Rect to highlight the Item
    CRect  rText(rcItem); // Rect To display the Text
    CPoint Pt( rcItem.left , rcItem.top ); // Point To draw the Image

	rText.left += 2;
	rText.top += 2;

	COLORREF crText;
	CString strText;

	if ((lpDrawItemStruct->itemState & ODS_SELECTED) &&
		 (lpDrawItemStruct->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		CBrush br(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->FillRect(&rClient, &br);
	}
	else if (!(lpDrawItemStruct->itemState & ODS_SELECTED) && 
		(lpDrawItemStruct->itemAction & ODA_SELECT)) 
	{
		CBrush br(::GetSysColor(COLOR_WINDOW));
		pDC->FillRect(&rClient, &br);
	}

	if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && 
		(lpDrawItemStruct->itemState  & ODS_FOCUS))
	{
		pDC->DrawFocusRect(&rcItem); 
	}
	else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) &&	
		!(lpDrawItemStruct->itemState & ODS_FOCUS))
	{
		pDC->DrawFocusRect(&rcItem); 
	}

	// To draw the Text set the background mode to Transparent.
	int iBkMode = pDC->SetBkMode(TRANSPARENT);

	if (lpDrawItemStruct->itemState & ODS_SELECTED)
		crText = pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
	else if (lpDrawItemStruct->itemState & ODS_DISABLED)
		crText = pDC->SetTextColor(::GetSysColor(COLOR_GRAYTEXT));
	else
		crText = pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));

	// Get the item text.
	GetText(lpDrawItemStruct->itemID, strText);

	// Setup the text format.
	UINT nFormat = DT_LEFT | DT_SINGLELINE | DT_VCENTER;
	if (GetStyle() & LBS_USETABSTOPS)
		nFormat |= DT_EXPANDTABS;
	

	// if the ImageList is Existing and there is an associated Image
	HPEN MyPen,OldPen;			
	if(strText !=""){
		MyPen = CreatePen(PS_SOLID,1,RGB(128,128,128));	
		OldPen = (HPEN)SelectObject(lpDrawItemStruct->hDC,MyPen);	
		MoveToEx(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top,NULL);
		LineTo(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.top);
		LineTo(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom);
		LineTo(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
		LineTo(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
		SelectObject(lpDrawItemStruct->hDC,OldPen);	
		DeleteObject(MyPen);	
	}
	pDC->DrawText(strText, -1, &rText, nFormat | DT_CALCRECT);
	pDC->DrawText(strText, -1, &rText, nFormat);

	pDC->SetTextColor(crText); 
	pDC->SetBkMode(iBkMode);	


}

void CMenuListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	lpMeasureItemStruct->itemHeight = 20;
}

int CMenuListBox::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListBox::OnCreate(lpCreateStruct) == -1)
		return -1;
	ResetContent();
	return 0;
}


int CMenuListBox::AddString(LPCTSTR lpszItem)
{
	int i,iCount;
	CString strString;
	iCount = CListBox::GetCount();
	for ( i = 0 ; i < iCount ; i++ ) {
		CListBox::GetText( i , strString );
		if ( lpszItem == strString ) return 0;
	}

	int iRet = CListBox::AddString(lpszItem);
	if (iRet >= 0)
		SetItemData(iRet, -1);
	return iRet;
}

//=======================================================
//
//  함 수 명 :  LoadMenuItem()
//  함수출력 :  int
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  메뉴를 로드한다.
//
//=======================================================
int CMenuListBox::LoadMenuItem()
{	
	CLESApp* pApp;
	CMainFrame* pWnd;
	pApp = (CLESApp*)AfxGetApp();
	pWnd = (CMainFrame*)AfxGetMainWnd();

	LPCTSTR MenuName;
	LPCTSTR CompareName="";
	int		i,j,nMax,Item_num;
	BYTE	mode,SignalID;
	CString str;

	Item_num = 1;
	ResetContent();		//초기화 하고 로드하기 

	switch(m_nType){
	case MENU_MAIN:	//주진로 취급 ( 주신호기만 취급) 
		if((pWnd->m_pMenuRoute[0].nMax ==0) && (pWnd->m_pMenuRoute[1].nMax ==0) && pWnd->m_pMenuRoute[2].nMax ==0){
			AddString("no item");
			return 0;
		}
		for(i = 0; i<pWnd->m_pMenuRoute[0].nMax;i++){		//출발점 신호기는 모두 로드 
			SignalID = pWnd->m_pMenuRoute[0].pRMenu[i].nSignalID;
			MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
//			if((pWnd->m_pMenuRoute[0].pRMenu[i].nDisable) == 0x00 ){	//& 0x80)){		//Disable이 안되어 있으면Load
				if(MenuName != CompareName){
					AddString(MenuName);
					Item_num++;
					CompareName = MenuName;
				}
//			SignalID = pWnd->m_pMenuRoute[0].pRMenu[i].nSignalID;
//			MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
//			if(MenuName != CompareName){
//				AddString(MenuName);
//				Item_num++;
//				CompareName = MenuName;
//			}
		}
		for(i = 0; i<pWnd->m_pMenuRoute[1].nMax;i++)
		{		//출발점 신호기는 모두 로드 
			SignalID = pWnd->m_pMenuRoute[1].pRMenu[i].nSignalID;
			MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
			if(MenuName != CompareName){
				AddString(MenuName);
				Item_num++;
				CompareName = MenuName;
			}
		}
		for(i = 0; i<pWnd->m_pMenuRoute[2].nMax;i++)
		{		//출발점 신호기는 모두 로드 
			SignalID = pWnd->m_pMenuRoute[2].pRMenu[i].nSignalID;
			MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
			if(MenuName != CompareName){
				AddString(MenuName);
				Item_num++;
				CompareName = MenuName;
			}
		}
		if(GetCount() == 0)
		AddString("no item");
		break;
	case MENU_CALLON:	//입환진로 취급
		if((pWnd->m_pMenuRoute[1].nMax ==0)){
			AddString("no item");
			return 0;
		}
		for(i = 0; i<pWnd->m_pMenuRoute[1].nMax;i++){		//출발점 신호기는 모두 로드 
			if(!(pWnd->m_pMenuRoute[1].pIMenu[i].nDisable & 0x80)){		//Disable이 안되어 있으면Load
				SignalID = pWnd->m_pMenuRoute[1].pRMenu[i].nSignalID;
				MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
				if(MenuName != CompareName){
					AddString(MenuName);
					Item_num++;
					CompareName = MenuName;
				}
			}
		}
		if(GetCount() == 0)
		AddString("no item");
	break;
	case MENU_SHUNT:	//입환진로 취급
		if((pWnd->m_pMenuRoute[2].nMax ==0)){
			AddString("no item");
			return 0;
		}
		for(i = 0; i<pWnd->m_pMenuRoute[2].nMax;i++){		//출발점 신호기는 모두 로드 
			if(!(pWnd->m_pMenuRoute[2].pIMenu[i].nDisable & 0x80)){		//Disable이 안되어 있으면Load
				SignalID = pWnd->m_pMenuRoute[2].pRMenu[i].nSignalID;
				MenuName = pApp->Info_pScrMessageInfo[pApp->nMaxTrack + SignalID-1].m_strName;
				if(MenuName != CompareName){
					AddString(MenuName);
					Item_num++;
					CompareName = MenuName;
				}
			}
		}
		if(GetCount() == 0)
		AddString("no item");
	break;
	case MENU_SIGNAL:	//신호기 취소
		if((pWnd->m_pMenuRoute[0].nMax ==0)||(pWnd->m_pMenuRoute[1].nMax ==0)){
			return 0;
		}

		for(i=0; i<3; i++) {
			nMax = pWnd->m_pMenuRoute[i].nMax;
			for(j=0; j<nMax; j++) {
				if ( pWnd->m_pMenuRoute[i].pRMenu[j].nDisable & 3 
					&& !((pWnd->m_pMenuRoute[i].pRMenu[j].nDisable & 0x04) == 0x04 && (pWnd->m_pMenuRoute[i].pRMenu[j].nType == 0xc0))) {
					MenuName = LPCTSTR(pWnd->m_pMenuRoute[i].pRMenu[j].Name);
					str = (CString)MenuName;
					if ( str.Find( "TTB" ) < 0 ) {		//TTB진로는 취소 불가능 메뉴에서 제거
						AddString(MenuName);
						Item_num++;
					}
				}
			}
		}
		if(GetCount() == 0)
		AddString("no item");
		break;
	case MENU_POINT:	//선로전환기
		if((pWnd->m_pMenuSwitch.nMax ==0)){
			return 0;
		}
		for(i = 0; i<pWnd->m_pMenuSwitch.nMax;i++){
			MenuName = (LPCTSTR)(pWnd->m_pMenuSwitch.pIMenu[i].Name);
			if ( /*!pWnd->m_pMenuSwitch.pIMenu[i].nDisable &&*/ !pWnd->m_bOperateDisable && !pApp->m_SystemValue.bCtrlLock ) {
					AddString(MenuName);
					Item_num++;
			}
		}		
		if(GetCount() == 0)
		AddString("no item");		
		break;
	case 6:	//구분진로 해정
		if((pWnd->m_pMenuTrack.nMax ==0)){
			return 0;
		}
		for( i=0; i<pWnd->m_pMenuTrack.nMax; i++) {
				mode = pWnd->m_pMenuTrack.pIMenu[i].nDisable & CHECK_TRACK_ALLDISABLE;		//0x0f
				if ( (mode & CHECK_TRACK_FREEROUTE) && !pWnd->m_bOperateDisable && !pApp->m_SystemValue.bCtrlLock ) {
					MenuName = (LPCTSTR)(pWnd->m_pMenuTrack.pIMenu[i].Name);
					AddString(MenuName);
					Item_num++;
				}
		}
		if(GetCount() == 0)
		AddString("no item");
		break;
	case 7:	//궤도 차단
		if((pWnd->m_pMenuTrack.nMax ==0)){
			return 0;
		}
		for( i=0; i<pWnd->m_pMenuTrack.nMax; i++) {
			if(!(pWnd->m_pMenuTrack.pIMenu[i].nDisable & CHECK_TRACK_FREEROUTE) && !pWnd->m_bOperateDisable && !pApp->m_SystemValue.bCtrlLock ){	//^^
				MenuName = (LPCTSTR)(pWnd->m_pMenuTrack.pIMenu[i].Name);
				AddString(MenuName);
				Item_num++;
			}
		}
		if(GetCount() == 0)
		AddString("");
		break;
	default:
			return 0;
	}
	return Item_num;
}

void CMenuListBox::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CLESApp* pApp;
	CMainFrame* pWnd;
	CLESView* pView;
	pApp = (CLESApp*)AfxGetApp();
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();
	int    i, j,nMax,SignalID=0;
	int    ichkbtn = 0;
	CString MenuName,ButtonName,RouteName;
	ParamType Data;
	CMenu menu;
	menu.CreatePopupMenu();

	CString strBufSignalName[50];
	BOOL bHaveSameSignalName = FALSE;
	int iSigCount = 0;

	ClientToScreen( &point );
	int position = GetCurSel();
	if(position == -1) return;
	CString text ;
	GetText(position,text);
	if(text == "no item") return;	//메뉴가 없으면 리턴
		
	switch(m_nType){
	case MENU_MAIN:	//주진로 취급 ( 주신호기만 취급) 
	case MENU_SHUNT:	//입환진로 취급
	case MENU_CALLON:	//입환진로 취급
		for (i=pApp->nMaxTrack;i<pApp->nMaxTrack + pApp->nMaxSignal;i++){
			if(text== pApp->Info_pScrMessageInfo[i].m_strName){
				SignalID = pApp->Info_pScrMessageInfo[i].m_nID;
				Data.ndl = pApp->Info_pScrMessageInfo[i].m_nID;
				Data.ndh = 0;
				
				pView->SetFocus();
				pView->m_Screen.OlePostMessage( WS_OPMSG_SIGNALDEST, Data.wParam, 0 );
				break;
			}
		}
		break;
	case MENU_POINT:	//선로전환기
		for(i = 0; i<pWnd->m_pMenuSwitch.nMax;i++){
//			if((LPCTSTR)text == pWnd->m_pMenuSwitch.pIMenu[i].Name){
			if(!strcmp((LPCTSTR)text, pWnd->m_pMenuSwitch.pIMenu[i].Name)){
				Data.ndl = pWnd->m_pMenuSwitch.pIMenu[i].nID;
				Data.ndh = 0;
				m_pParent->PostMessage(WM_COMMAND,IDOK);
				pView->m_Screen.OlePostMessage( WS_OPMSG_SWTOGGLE, Data.wParam, 0 );
			}
		}
		break;
	case MENU_SIGNAL:	//신호기 취소
		for ( ; iSigCount < 50; iSigCount++)
		{
			strBufSignalName[iSigCount] = "";
		}
		iSigCount = 0;
		for(i=0; i<2; i++) {
			nMax = pWnd->m_pMenuRoute[i].nMax;
			for(j=0; j<nMax; j++) {
				if((LPCTSTR)text == pWnd->m_pMenuRoute[i].pRMenu[j].Name){
					for ( int k = 0; k < iSigCount; k++)
					{
						if ( !memcmp(strBufSignalName[iSigCount-1], pWnd->m_pMenuRoute[i].pRMenu[j].Name, sizeof(pWnd->m_pMenuRoute[i].pRMenu[j])) )
						{ 
							bHaveSameSignalName = TRUE;
							break;
						}
					}
					if ( bHaveSameSignalName == FALSE )
					{
						Data.ndl = pWnd->m_pMenuRoute[i].pRMenu[j].nSignalID;
						Data.ndh = pWnd->m_pMenuRoute[i].pRMenu[j].nButtonID;
						m_pParent->PostMessage(WM_COMMAND,IDOK);
						pView->m_Screen.OlePostMessage( WS_OPMSG_SIGNALCANCEL, Data.wParam, 0 );
						strBufSignalName[iSigCount++] = pWnd->m_pMenuRoute[i].pRMenu[j].Name;
					}
				}
			}
		}
		break;
	case 6:	//구분진로 해정
		for(i = 0; i<pWnd->m_pMenuTrack.nMax;i++){
			if((LPCTSTR)text == pWnd->m_pMenuTrack.pIMenu[i].Name){
				Data.ndl = pWnd->m_pMenuTrack.pIMenu[i].nID;
				Data.ndh = 0;
				m_pParent->PostMessage(WM_COMMAND,IDOK);
				pView->m_Screen.OlePostMessage( WS_OPMSG_FREESUBTRACK, Data.wParam, 0 );				
			}
		}
		break;
	default:
		break;
	}

	CListBox::OnLButtonDblClk(nFlags, point);
}

BOOL CMenuListBox::PreTranslateMessage(MSG* pMsg) 
{
	CLESApp* pApp;
	CMainFrame* pWnd;
	CLESView* pView;
	pApp = (CLESApp*)AfxGetApp();
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();
    int    i,j,nMax,SignalID=0;
	int    ichkbtn = 0;
	CString MenuName,ButtonName,RouteName;
	ParamType Data;
	CMenu menu;
	menu.CreatePopupMenu();

	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN){
		int position = GetCurSel();
		CString text ;
		GetText(position,text);
		CRect rect,rc;
		GetWindowRect( &rect );
		GetItemRect(position,&rc);

		switch(m_nType){
		case MENU_MAIN:	//주진로 취급 ( 주신호기만 취급) 
		case MENU_SHUNT:	//입환진로 취급
		case MENU_CALLON:	//입환진로 취급
			for (i=pApp->nMaxTrack;i<pApp->nMaxTrack + pApp->nMaxSignal;i++){
				if(text == pApp->Info_pScrMessageInfo[i].m_strName){
					SignalID = pApp->Info_pScrMessageInfo[i].m_nID;
					Data.ndl = pApp->Info_pScrMessageInfo[i].m_nID;
					Data.ndh = 0;
					pView->SetFocus();
					pView->m_Screen.OlePostMessage( WS_OPMSG_SIGNALDEST, Data.wParam, 0 );
					break;
				}
			}
			break;
		case MENU_POINT:	//선로전환기
			for(i = 0; i<pWnd->m_pMenuSwitch.nMax;i++){
// 				if((LPCTSTR)text == pWnd->m_pMenuSwitch.pIMenu[i].Name){
				if(!strcmp((LPCTSTR)text, pWnd->m_pMenuSwitch.pIMenu[i].Name)){
				Data.ndl = pWnd->m_pMenuSwitch.pIMenu[i].nID;
					Data.ndh = 0;
					pView->m_Screen.OlePostMessage( WS_OPMSG_SWTOGGLE, Data.wParam, 0 );
				}
			}
			break;
		case MENU_SIGNAL:	//신호기 취소
			for(i=0; i<2; i++) {
				nMax = pWnd->m_pMenuRoute[i].nMax;
				for(j=0; j<nMax; j++) {
					if(!strcmp((LPCTSTR)text, pWnd->m_pMenuRoute[i].pRMenu[j].Name)){
						Data.ndl = pWnd->m_pMenuRoute[i].pRMenu[j].nSignalID;
						Data.ndh = pWnd->m_pMenuRoute[i].pRMenu[j].nButtonID;
						pView->m_Screen.OlePostMessage( WS_OPMSG_SIGNALCANCEL, Data.wParam, 0 );
					}
				}
			}
			break;
		case 6:	//구분진로 해정
			for(i = 0; i<pWnd->m_pMenuTrack.nMax;i++){
				if(!strcmp((LPCTSTR)text, pWnd->m_pMenuTrack.pIMenu[i].Name)){
					Data.ndl = pWnd->m_pMenuTrack.pIMenu[i].nID;
					Data.ndh = 0;
					pView->m_Screen.OlePostMessage( WS_OPMSG_FREESUBTRACK, Data.wParam, 0 );
					
				}
			}
			break;
		default:
			break;
		}


		pMsg->wParam = NULL;
	}	
	return CListBox::PreTranslateMessage(pMsg);
}




void CMenuListBox::OnSelectDest( UINT nID )
{	
	CLESApp* pApp;
	CMainFrame* pWnd;
	CLESView* pView;
	pApp = (CLESApp*)AfxGetApp();
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();
	CString RouteName;
	BYTE nSignalID;
	BYTE nButtonID;
	ParamType Data;
	int i,RouteId;

	RouteId = m_nType-1;		//주진로?  입환진로?
	int position = GetCurSel();
	if(position == -1) 
		return;
	CString text ;
	GetText(position,text);
	RouteName = text;
	RouteName+= ":";
	RouteName+= m_strDestName[nID-150];
	
	for(i = 0; i<pWnd->m_pMenuRoute[RouteId].nMax;i++){
		if(RouteName == pWnd->m_pMenuRoute[RouteId].pRMenu[i].Name){
			nSignalID = pWnd->m_pMenuRoute[RouteId].pRMenu[i].nSignalID;
			nButtonID = pWnd->m_pMenuRoute[RouteId].pRMenu[i].nButtonID;
			Data.ndl = nSignalID;
			Data.ndh = nButtonID;
			break;
		}
	}
	m_pParent->PostMessage(WM_COMMAND,IDOK);
	pView->m_Screen.OlePostMessage( WS_OPMSG_SIGNALDEST, Data.wParam, 0 );
}

void CMenuListBox::OnSelectBlocking( UINT nID )
{
	CLESApp* pApp;
	CMainFrame* pWnd;
	CLESView* pView;
	pApp = (CLESApp*)AfxGetApp();
	pWnd = (CMainFrame*)AfxGetMainWnd();
	pView = (CLESView*)pWnd->GetActiveView();
	int i;
	BYTE mode;

	long lParam = 0;
	ParamType wData;
	wData.wParam = 0;

	int position = GetCurSel();
	if(position == -1) 
		return;
	CString text ;
	GetText(position,text);
	
	for(i = 0; i<pWnd->m_pMenuTrack.nMax;i++){
		if((LPCTSTR)text == pWnd->m_pMenuTrack.pIMenu[i].Name){
				mode = pWnd->m_pMenuTrack.pIMenu[i].nDisable & CHECK_TRACK_CTRLDISABLE;	//0x07
				wData.d1 = (BYTE)(pWnd->m_pMenuTrack.pIMenu[i].nID);
				break;
		}
	}
	switch(nID-300){
	case 0: //단전
		if(mode){	//&&mode<=3
			wData.d2 = TRACKLOCK_PWRDOWN >> 4;	// 해정
		}
		else{
			wData.d2 = TRACKLOCK_PWRDOWN;		// 쇄정
		}
		break;
	case 1:	//모터카
		if(mode){	//&&mode<=3
			wData.d2 = TRACKLOCK_MOTORCAR >> 4;	// 해정
		}
		else{
			wData.d2 = TRACKLOCK_MOTORCAR;		// 쇄정
		}		
		break;
	case 2://사용자금지
		if(mode){ //&&mode<=3
			wData.d2 = TRACKLOCK_USING >> 4;	// 해정
		}
		else{
			wData.d2 = TRACKLOCK_USING;			// 쇄정
		}				
		break;
	default:
		break;
	}
	pApp->m_bBlockTrackIng = TRUE;	//궤도 블럭 시작
	m_pParent->PostMessage(WM_COMMAND,IDOK);
	pView->m_Screen.OlePostMessage( WS_OPMSG_MARKINGTRACK, wData.wParam, lParam );
}

