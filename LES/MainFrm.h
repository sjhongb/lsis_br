// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_)
#define AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MyCommCRC16.h"
#include "LESView.h"
#include "LESDoc.h"
#include "MenuWndDlg.h"
#include "../include/LOGMessage.h"
#include "DirectSound.h"
#include "resource.h"
#include "Splash.h"
#include "../include/JTime.h"
#include "../include/MenuItem.h"
#include "../include/EipEmu.h"
#include "SharedMemory.h"
#include "PasswordDlg.h"
#include "SerialComm.h"
#include "SelectEPK.h"
#include "SelectLC.h"
#include "SelectBlock.h"
#include "CommEth.h"
#include "EventDlg.h"
#include "SMSyncThread.h"	// Added by ClassView

typedef BOOL (WINAPI *PFN_InstallHook)(
	HWND hWndServer,
	UINT nOSVersionType
	);

typedef BOOL (WINAPI *PFN_UnInstallHook)();


typedef void (WINAPI *PFN_SetHookOption)(
					  int nWaitTime, 
					  BOOL bTraceEnabled, 
					  BOOL bSendDebugText
					  );

typedef void (WINAPI *PFN_GetHookOption)(
					  int &nWaitTime, 
					  BOOL &bTraceEnabled, 
					  BOOL &bSendDebugText
					  );

typedef char* (WINAPI *PFN_GetResultString)();

typedef void (WINAPI *PFN_GetIniFilePath)(char *szIniFile);

typedef void (WINAPI *PFN_SetEnabledRestart)(BOOL bEnabled);

#define ID_TIMER			1
#define TIMER_MAX_INTRCOUNT	200		// x ms
#define MAX_MSGCNT			25
#define _MAX_USER_ID_LENGTH           8    // 15
#define _MAX_USER_PW_LENGTH           8    // 15
#define _MAX_CTRL_COM_LENGTH          14

#define BIT_SOUND_APPROACH	0x01
#define BIT_SOUND_SP		0x02
#define BIT_SOUND_POWER		0x04
#define BIT_SOUND_BLOCKADE1	0x08
// #define BIT_SOUND_BLOCKADE2	0x10
// #define BIT_SOUND_BLOCKADE3	0x20
// #define BIT_SOUND_BLOCKADE4	0x40
// #define BIT_SOUND_BLOCKADE5	0x80

#define WM_F1				(WM_USER+1001)
#define WM_F2				(WM_USER+1002)
#define WM_F3				(WM_USER+1003)
#define WM_F4				(WM_USER+1004)
#define WM_F5				(WM_USER+1005)
#define WM_F6				(WM_USER+1006)
#define WM_F7				(WM_USER+1007)
#define WM_F8				(WM_USER+1008)
#define WM_F9				(WM_USER+1009)
#define WM_F10				(WM_USER+1010)
#define WM_F11				(WM_USER+1011)
#define WM_F12				(WM_USER+1012)
#define WM_AUTOEXIT	        (WM_USER+1013)
#define WM_AUTOEXITEX       (WM_USER+1014)

#define AEPK 1
#define BEPK 2
#define CEPK 3
#define DEPK 4
#define EEPK 5
#define FEPK 6
#define GEPK 7
#define UPEPK 8
#define DNEPK 9

#define LC1 0
#define LC2 1
#define LC3 2
#define LC4 3
#define LC5 4

class CMainFrame : public CFrameWnd
{
	
protected: 
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

	SharedMemory shared;

public:
	CPasswordDlg m_LogInDlg;
	CMyCommCRC16 _classComm1;
	CMyCommCRC16 _classComm2;
	CSerialComm *m_pSerialComm;
	CCommEth m_pEthComm1;
	CCommEth m_pEthComm2;
	UINT m_nTimerID;
	BYTE CommVMemData[ MAX_MSGBLOCK_SIZE ];
	CLESApp* pApp;
	CStatusBar  m_wndStatusBar;
	CString m_strSendMessage;
	CString m_strReceiveMessage;
// 로그 표시 화면 오브젝트
	BOOL m_bIsCreateLOGView;
	CEventDlg	m_EventDlg;
// 메뉴 표시 화면 오브젝트
	BOOL m_bIsCreateMenuView;
	CMenuWndDlg	 m_MenuView;
	BOOL   m_bSoundOpen   ;
	BOOL   m_bSystemRunOn  ;
	BOOL   m_bCBIRun  ;
	BOOL   m_bPrint  ;
	BOOL   m_bMessageSendCk;
	BOOL   m_bSystemRunOff ;
	BOOL m_bExitProgram;	//exit?
	BOOL m_bLogWindowDisable;	//exit?
	BOOL m_bOperateDisable;
	int m_iConsoleRunCounter;		// 10,9,8,.... 최초 running으로부터 안정화까지.
	CJTime m_PCTime;
	int m_iStartBit;
	int m_iResetTimer;
	//메뉴
	MenuItems m_pMenuRoute[3];	// signal, shunt
	MenuItems m_pMenuSwitch;	// switch
	MenuItems m_pMenuTrack;		// track
	CLOGMessage	m_RealLOGMessage;
	//DirectX Sound
	CDirectSound m_PlaySound[8];
	BYTE m_CurrentSound;
	BYTE m_OldSound;
	char    m_CmdQue[24];
	BOOL StartUp();
	void OnCommErrDisplay();
	HANDLE  m_hSubWinMap;
	LPVOID	m_pCmdXBuffer;
	LPVOID	m_pWinXBuffer;
	CSelectEPK m_SelectEPK;
	CSelectLC m_SelectLC;
	CSelectBlock m_SelectBlock;
	CSelectBlockControl m_SelectBlockFunction;

	HMODULE					m_hmodHookTool;
	PFN_InstallHook			m_pfnInstallHook;
	PFN_UnInstallHook		m_pfnUnInstallHook;
	PFN_SetHookOption		m_pfnSetHookOption;
	PFN_GetHookOption		m_pfnGetHookOption;
	PFN_GetResultString		m_pfnGetResultString;
	PFN_GetIniFilePath		m_pfnGetIniFilePath;
	PFN_SetEnabledRestart	m_pfnSetEnabledRestart;
	BOOL			m_bStartHook;
	UINT			m_nOSVersionType;
	int GetTrackID(LPCSTR lpszName);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	CSMSyncThread *m_pSMSyncThread;
	int m_iCountForSendGPStime;
	int m_iCountForLogInDlg;
	void OnAllStopSound();
	void OnCheckSoundState(BYTE* pData);
	void OnPlaySound();
	BOOL OnSoundLoad();
	void SetLSMFocus();
	void GetLogPath(CJTime &destT, CString &strFileName, int nMode);
	void OnDeleteLogFile(/*CJTime *destT, int nMode*/);
	int SystemTimeUpdate(CJTime *destT, CJTime *scrT);
	BOOL UpdateMenuByField(void *pVMem);
	void OnMenuLockUpdate();
	int CreateObj();			// initialize Class or Object
	BOOL SystemTimeBCDToDecimal(DataHeaderType* R);
	BOOL m_bCloseWindow;		//Window Close or not?
	BOOL m_bDlgDisplaed;
	BYTE m_RecieveCycle;
	void OnSetVMem(BYTE *pVMem,int length,BOOL bWrite = TRUE);
	BYTE m_DataCycle;
	CommStatusType m_CommStatus;
	LCCCommStatusType m_LccCommStatus;
	SystemStatusType m_SystemStatus;
	LCCCommDataType m_CommCmd;
	BYTE m_CmdCycle;
	char m_cMessage[500]; // 아카우라 SB 에서 바이패스 기계실로 메세지를 보낸다.
	int m_iMenuViewExitCk;
	int m_iLogViewExitCk;
	int m_iMessageSendCnt; // 메세지를 잘라서 보내는 카운트 처음 보낼때 0 
	int m_iMessageReceiveCnt;
	UINT m_nCmdTick;
	void ShowLockMessage();
	BOOL ConfirmOperation(short* pData,BYTE Cmd);
	void OnSetVMemCommFail();
	void CommCommand(BYTE *pData);
	void CommWatch(BYTE *pData,int nPort);
	BOOL SetCommQue(BYTE Cmd,BYTE *pData,int len);
	void SendCurrentTime();
	int m_nBaudRate;
	void CommClose();
	BOOL CommOpen();
	BOOL CommEthOpen();
	BOOL GPSCommOpen();			//GPS추가.
	void PosiotionFix();
	virtual ~CMainFrame();
	void OnAutoExit();
	void OnAutoExitEx();
	void OnF1();
	void OnF2();
	void OnF3();
	void OnF4();
	void OnF5();
	void OnF6();
	void OnF7();
	void OnF8();
	void OnF9();
	void OnF10();
	void OnF11();
	void OnF12();
	void On0();
	void On1();
	void On2();
	void On3();
	void On4();
	void On5();
	void On6();
	void On7();
	void On8();
	void On9();
	void OnControlBlock();
	void OnAltPlusE();
	void OnAltPlusL();
	void OnAEPK();
	void OnBEPK();
	void OnCEPK();
	void OnDEPK();
	void OnEEPK();
	void OnFEPK();
	void OnGEPK();
	void OnUpEPK();
	void OnDnEPK();
	void OnLC1();
	void OnLC2();
	void OnLC3();
	void OnLC4();
	void OnLC5();
	void ChangeUser();
	int MessageSend(); // 아카우라역 SB 에서 바이패스 기계실로 메세지를 보낸다.
	void CheckLCCHaveCurrentUserIDOrNot( LCCCommDataType *LCCCommdata);
	void CreateUser( RealUserDataType *UserData);
	void DeleteUser( RealUserDataType *UserData);
	void ChangePass( RealUserDataType *UserData);
	void ChangeToAnotherUser( RealUserDataType *UserData);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnRun();
	afx_msg void OnReset();
	afx_msg void OnTimeset();
	afx_msg void OnMessageSend();
	afx_msg void OnExit();
	afx_msg void OnAlluser();
	afx_msg void OnChangeUser();
	afx_msg int OnLockUnLock(int iVal = 0);
	afx_msg void OnSectionRelease();
	afx_msg void OnBlockingTrack();
	afx_msg int OnLogInOut();
	afx_msg void OnSls();
	afx_msg void OnAtb();
	afx_msg void OnNd();
	afx_msg void OnAlarm();
	afx_msg void Onbuzzer();
	afx_msg void OnSendTrainNumber();
	afx_msg void OnSelectEPK();
	afx_msg void OnSelectLC();
	afx_msg void OnSelectBlock();
	afx_msg void OnBtnScroll();
	//}}AFX_MSG
	afx_msg void OnReceiveGPSMsg(USHORT nMsgLen, BYTE *pMsg);		//GPS추가.
	afx_msg void OnSharedMemoryUpdated(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_)
