#if !defined(AFX_TIMESETDLG_H__0E75A5D2_4E49_4D4E_988E_36B165D15E74__INCLUDED_)
#define AFX_TIMESETDLG_H__0E75A5D2_4E49_4D4E_988E_36B165D15E74__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimesetDlg.h : header file
//
#include "../include/JTime.h"
/////////////////////////////////////////////////////////////////////////////
// CTimesetDlg dialog

class CTimesetDlg : public CDialog
{
// Construction
public:
	CTimesetDlg(CWnd* pParent = NULL);   // standard constructor
	CJTime m_Time;
// Dialog Data
	//{{AFX_DATA(CTimesetDlg)
	enum { IDD = IDD_DLG_TIMESET };
	int		m_iDay;
	int		m_iMonth;
	int		m_iYear;
	int		m_iHour;
	int		m_iMin;
	int		m_iSec;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimesetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTimesetDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMESETDLG_H__0E75A5D2_4E49_4D4E_988E_36B165D15E74__INCLUDED_)
