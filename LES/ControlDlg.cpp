// ControlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "ControlDlg.h"
#include "LES.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CControlDlg dialog


CControlDlg::CControlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CControlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CControlDlg)
	//}}AFX_DATA_INIT
	m_Type = 0;
	m_DlgTitle = "";
	m_stState="";
	m_stScript = "";
}


void CControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CControlDlg)
	DDX_Control(pDX, IDC_STATIC_ICON, m_stIcon);
	DDX_Control(pDX, IDC_LIST_ITEM, m_ListItem);
	DDX_Text(pDX, IDC_STATIC_SCRIPT, m_stScript);
	DDX_Text(pDX, IDC_STATIC_STATE, m_stState);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CControlDlg, CDialog)
	//{{AFX_MSG_MAP(CControlDlg)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlDlg message handlers

BOOL CControlDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(m_DlgTitle);
	m_stIcon.SetIcon(hIcon);
	m_ListItem.m_nType = m_Type;
	m_ListItem.m_pParent = this;
	NumberOfItem = m_ListItem.LoadMenuItem();
	if( NumberOfItem== 0){
		return FALSE;
	}
	m_ListItem.SetCurSel(0);
	OnResizeWindow();

	CenterWindow(AfxGetMainWnd());

	return TRUE;  // return TRUE unless you set the focus to a control	              
}

void CControlDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	m_Type = 0;
	
}

int CControlDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1) return -1;
	else return 0;
}

void CControlDlg::OnResizeWindow()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	CDC   *pDC = GetDC();
	CSize chExtent = pDC->GetTextExtent( "W", 1 );
	ReleaseDC( pDC );
	int width,extent;

	m_ListItem.SetItemHeight(-1,20);		//현재 새굴림 10폰트에 리스트 박스 아이템 높이가 20으로 셋팅 된경우 
	if(NumberOfItem<=30){					//아이템이 30개 이하이면 기본 사이즈 아니면 right 확장 
		extent = chExtent.cx*8;
		m_ListItem.SetColumnWidth(extent);
	}
	else{
		extent = chExtent.cx*4;
		m_ListItem.SetColumnWidth(extent);
	}
	
	width = NumberOfItem/10+1;
}
