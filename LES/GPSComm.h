// DTSComm.h: interface for the CDTSComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DTSCOMM_H__EFEA55B3_EE3A_483F_A3A2_AA24A6F79EA2__INCLUDED_)
#define AFX_DTSCOMM_H__EFEA55B3_EE3A_483F_A3A2_AA24A6F79EA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SerialComm.h"

#define GPS_STX				'$'
#define	GPS_ETX				'*'

#define WM_GPS_COMM_RECEIVE_MSG		WM_USER+10000

class CGPSComm : public CSerialComm  
{
	DECLARE_DYNCREATE(CGPSComm)
public:
	virtual void ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen);
	virtual BOOL SendMsg(BYTE *pBuffer, USHORT nMsgLen);
	CGPSComm();
	virtual ~CGPSComm();
};

#endif // !defined(AFX_DTSCOMM_H__EFEA55B3_EE3A_483F_A3A2_AA24A6F79EA2__INCLUDED_)
