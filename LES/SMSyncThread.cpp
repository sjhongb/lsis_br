// SMSyncThread.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "SMSyncThread.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSMSyncThread

IMPLEMENT_DYNCREATE(CSMSyncThread, CWinThread)

CSMSyncThread::CSMSyncThread()
{

}

CSMSyncThread::~CSMSyncThread()
{
}

BOOL CSMSyncThread::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	// kill event starts out in the signaled state
	return TRUE;
}

int CSMSyncThread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSMSyncThread, CWinThread)
	//{{AFX_MSG_MAP(CSMSyncThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMSyncThread message handlers

int CSMSyncThread::Run() 
{
	// TODO: Add your specialized code here and/or call the base class

	while(m_bIsRunning)
	{
		if(WaitForSingleObject(m_hEventRun, 1000) == WAIT_OBJECT_0)
		{
			PostMessage(m_hParentWnd, WM_SHARED_MEMORY_UPDATED, 0, 0);
		}
		else
		{
			continue;
		}
	}

//	return CWinThread::Run();
	return TRUE;
}

void CSMSyncThread::Start(HWND hParentWnd)
{
	m_hParentWnd = hParentWnd;
	m_bIsRunning = TRUE;

	m_hEventRun = CreateEvent(NULL, TRUE, FALSE, "LES_LIS_SHM_SYNC");

	ResumeThread();
}

void CSMSyncThread::End()
{
	ResumeThread();

	m_bIsRunning = FALSE;
	WaitForSingleObject(m_hThread, INFINITE);
}
