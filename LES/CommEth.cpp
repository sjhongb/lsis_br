// CommEth.cpp : implementation file
//

#include "stdafx.h"
#include "LES.h"
#include "CommEth.h"
#include "CRCCCITT.h"
#include "LESView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommEth

CCommEth::CCommEth()
{
	m_hParentWnd = 0;
	m_nRxCRC = 0;
	m_nTxCRC = 0;
	m_nInRxMax = 0;
	m_nTxPtr = 0;
	m_bTxReady = FALSE;
	m_nCmdCounter = 0;
	m_bSendToTx = 0;
}

CCommEth::~CCommEth()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CCommEth, CAsyncSocket)
	//{{AFX_MSG_MAP(CCommEth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CCommEth member functions

void CCommEth::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString strRcvIP;
	UINT nRcvPort;
	UINT nSendPort;

	BYTE	szBuffer[4096] = {0,};
	UINT	nRecvSize = 0;

	nRecvSize = ReceiveFrom(szBuffer, 4096, strRcvIP, nRcvPort, 0);

// 	if ( !((nRcvPort == m_nCBI1Port && strRcvIP == m_strCBI1IP) || ( nRcvPort == m_nCBI2Port && strRcvIP == m_strCBI2IP)))
 	if ( /*( nRcvPort != m_nEthPort ) || */!(strRcvIP == m_strCBI1IP || strRcvIP == m_strCBI2IP))
	{
		return;
	}

	CLESApp *pApp = (CLESApp*)AfxGetApp();
	BYTE *p = (BYTE*)szBuffer;
	BOOL bCallParent = FALSE;
	unsigned short m_nTestRxCRC = 0;

	if ( szBuffer[0] == COM_STX )
	{
		if ( (szBuffer[1] == 0x1D) || (szBuffer[1] == 0x2D) || (szBuffer[1] == 0x3D) || (szBuffer[1] >= 0x1F && szBuffer[1] <= 0x6F) )
		{
			if ( m_bTxReady && !m_bSendToTx )
			{
				m_bSendToTx = 1;
			}

			m_nRxCRC = 0;

			for ( int loc = 2; loc < pApp->nComRecordSize + 2; loc++ )
			{
				m_nRxCRC = crc_CCITT( szBuffer[loc], m_nRxCRC );
				m_nTestRxCRC = crc_CCITT( szBuffer[loc], m_nTestRxCRC);
			}

			if ( (m_nTestRxCRC%256 == szBuffer[pApp->nComRecordSize+2] ) && ( szBuffer[pApp->nComRecordSize+3] == m_nTestRxCRC/256))
			{
				if ( m_hParentWnd )
				{
					if(szBuffer[2] != 0x0A)
					{
						bCallParent = FALSE;
						pApp->m_iCommError = 2;
						PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_ERRMSG, NULL);
					}
					else
					{
						bCallParent = TRUE;
						pApp->m_iCommError = 0;
					}
					memcpy(&m_pDataQ[0], &szBuffer[1], pApp->nComRecordSize+1);
					if ( pApp->m_SystemValue.nMode == 2)
					{
						m_PostDB.Port = pApp->m_SystemValue.nTRCID+10;
					}
					else
					{
						m_PostDB.Port = pApp->m_SystemValue.nSystemNo;
					}
					m_PostDB.Length = nRecvSize;
					m_PostDB.pData = &m_pDataQ[0];
					if (bCallParent)
					{
						PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_WATCH, (LPARAM)&m_PostDB);
					}					
				}

				BYTE bRealTxQ[4096];
				memset(bRealTxQ, 0, sizeof(bRealTxQ));
				bRealTxQ[0] = 0x7E;

				memcpy(&bRealTxQ[1], &m_pTxQ[0], LCC_COM_SIZE);

				nSendPort = nRcvPort;

				if ( pApp->m_SystemValue.nMode == TRC_COMMUNICATION )
				{
					nSendPort += pApp->m_SystemValue.nTRCID;
				}

				if ( pApp->m_SystemValue.nSystemType == 0 ) 
				{
					SendTo(&bRealTxQ[0], LCC_COM_SIZE + 1, nSendPort, m_strCBI1IP);
					SendTo(&bRealTxQ[0], LCC_COM_SIZE + 1, nSendPort, m_strCBI2IP);
				}
// 				SendTo(&bRealTxQ[0], LCC_COM_SIZE + 1, m_nCBI1Port, m_strCBI1IP);
// 				SendTo(&bRealTxQ[0], LCC_COM_SIZE + 1, m_nCBI2Port, m_strCBI2IP);

				pApp->m_bCommSuccess = TRUE;
				pApp->m_iCommError = 0;
				m_nCmdCounter++;
			}
			else
			{
				szBuffer[0] = 0;
				pApp->m_iCommError = 1;
				PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_ERRMSG, NULL);
			}
		}
		else
		{
			szBuffer[0] = 0;
			pApp->m_iCommError = 3;
			PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_ERRMSG, NULL);
		}
	}
	else
	{
		szBuffer[0] = 0;
		pApp->m_iCommError = 3;
		PostMessage(m_hParentWnd, WM_COMMAND, ID_COMM_ERRMSG, NULL);
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

BOOL CCommEth::CreateSocket(HWND hWnd, CString strEthIP, UINT nEthPort, CString strCBI1IP, CString strCBI2IP)
{
	m_hParentWnd = hWnd;
// 	m_strTRCIP	= strTRCIP;
 	m_nEthPort	= nEthPort;
	m_strCBI1IP	= strCBI1IP;
	m_strCBI2IP = strCBI2IP;
// 	m_nCBI1Port	= nCBI1Port;
// 	m_nCBI2Port = nCBI2Port;
// 
	return Create(m_nEthPort, SOCK_DGRAM, FD_READ|FD_WRITE, strEthIP);
}

BOOL CCommEth::SendData(BYTE* data, int len, BYTE stx /* = COM_STX */, BOOL bCmd /* = FALSE */)
{
	int n;
	BYTE c;

	BYTE *p = &m_pTxQ[0];

	m_bTxReady = FALSE;

	m_nTxCRC = 0;

	memset(m_pTxQ, 0, LCC_COM_SIZE );
	m_pTxQ[0] = stx;
	*data++;
	if ( stx != COM_STX )
	{
		GetTxCRC16(stx);
	}
	n = 1;

	for(; n < LCC_COM_SIZE - 2;)
	{
		c = *data++;
		GetTxCRC16(c);
		m_pTxQ[n++] = c;
	}

	// CRC1
	c = m_TxCRCLow;
	m_pTxQ[n++] = c;
	*data++;
	// CRC2
	c = m_TxCRCHigh;
	m_pTxQ[n++] = c;
	*data++;
	// ETX
	m_nTxPtr = n;

	if ( stx != COM_STX )
	{
		m_bTxReady = TRUE;
	}

	if ( bCmd == FALSE )
	{
		m_nCmdCounter = 0;
	}
	
	return TRUE;
}

void CCommEth::GetRxCRC16(BYTE c)
{
	unsigned short data = c;
	m_nRxCRC = crc_CCITT( c, m_nRxCRC );
}

void CCommEth::GetTxCRC16(BYTE c)
{
	unsigned short data = c;
	m_nTxCRC = crc_CCITT( c, m_nTxCRC );
}
