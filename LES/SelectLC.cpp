// SelectLC.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "SelectLC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectLC dialog


CSelectLC::CSelectLC(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectLC::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectLC)
	m_nIndexOfLC = 0;
	pApp = (CLESApp*)AfxGetApp();
	//}}AFX_DATA_INIT
}


void CSelectLC::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectLC)
	DDX_Radio(pDX, IDC_INDEX_LC1, m_nIndexOfLC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectLC, CDialog)
	//{{AFX_MSG_MAP(CSelectLC)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectLC message handlers

void CSelectLC::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 0)
	{
		CDialog::OnCancel();
	}
	else
	{
		CDialog::OnOK();
	}

}

BOOL CSelectLC::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 5)
	{
		GetDlgItem( IDC_INDEX_LC1 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[1]);
		GetDlgItem( IDC_INDEX_LC2 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[2]);
		GetDlgItem( IDC_INDEX_LC3 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[3]);
		GetDlgItem( IDC_INDEX_LC4 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[4]);
		GetDlgItem( IDC_INDEX_LC5 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[5]);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 4)
	{
		GetDlgItem( IDC_INDEX_LC1 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[1]);
		GetDlgItem( IDC_INDEX_LC2 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[2]);
		GetDlgItem( IDC_INDEX_LC3 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[3]);
		GetDlgItem( IDC_INDEX_LC4 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[4]);

		GetDlgItem( IDC_INDEX_LC5)->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 3)
	{
		GetDlgItem( IDC_INDEX_LC1 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[1]);
		GetDlgItem( IDC_INDEX_LC2 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[2]);
		GetDlgItem( IDC_INDEX_LC3 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[3]);

		GetDlgItem( IDC_INDEX_LC4)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC5)->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 2)
	{
		GetDlgItem( IDC_INDEX_LC1 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[1]);
		GetDlgItem( IDC_INDEX_LC2 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[2]);

		GetDlgItem( IDC_INDEX_LC3)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC4)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC5)->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfLC == 1)
	{
		GetDlgItem( IDC_INDEX_LC1 )->SetWindowText(pApp->m_VerifyObjectForMessage.strLCName[1]);

		GetDlgItem( IDC_INDEX_LC2)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC3)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC4)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC5)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem( IDC_INDEX_LC1)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC2)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC3)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC4)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_LC5)->EnableWindow(FALSE);
	}
	
	CenterWindow(AfxGetMainWnd());
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
