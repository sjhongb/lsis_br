//=======================================================
//==              recordmask.h
//=======================================================
//  파 일 명 :  recordmask.h
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================

#ifndef   __RECORDMASK_H
#define   __RECORDMASK_H

typedef struct tagRecordMaskType {
	BOOL bLogDay;
	BOOL bRecDay;
	BOOL bSystem;
	BOOL bTrack;
	BOOL bSignal;
	BOOL bSwitch;
	BOOL bButton;
	BOOL bOpMsg;
	BOOL bUnitMsg;
	BOOL bDetailSignal;
	BOOL bDetailSwitch;
	BOOL bDetailTrack;
	int  nTrack;
	int  nSignal;
	int  nSwitch;
	int  nButton;
	union {
		BYTE nSys;
		struct {
			BYTE bDef		: 1;
			BYTE bSysbar	: 1;
			BYTE bOpbar		: 1;
			BYTE bUnit		: 1;
			BYTE bOther		: 1;
			BYTE bReserved	: 1;
			BYTE bIOC1		: 1;
			BYTE bIOC2		: 1;
		} fSys;
	};
	char *pTrack;
	char *pSignal;
	char *pSwitch;
	char *pButton;

public:
	void Init() {
		// Check state
		bLogDay =
		bRecDay =
		bSystem =
		bTrack  =
		bSignal =
		bSwitch =
		bButton = 
		bOpMsg  =
		bUnitMsg = FALSE;
		// Detail View state
		bDetailSignal = 
		bDetailSwitch = 
		bDetailTrack  = 0;
		// Object length
		nTrack  =
		nSignal =
		nSwitch =
		nButton = 0;
		// System mask data
		nSys = 0x00;
		// Pointer for object mask data
		pTrack = 
		pSignal =
		pSwitch =
		pButton = NULL;
	}
	void Destroy() {
		if ( pTrack ) delete [] pTrack;
		if ( pSignal ) delete [] pSignal;
		if ( pSwitch ) delete [] pSwitch;
		if ( pButton ) delete [] pButton;
		pTrack = pSignal = pSwitch = pButton = NULL;
	}
} RecordMaskType;



#endif // __RECORDMASK_H
