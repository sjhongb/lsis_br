// CLESDoc.h : interface of the CLESDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LESDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_)
#define AFX_LESDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CLESDoc : public CDocument
{
protected: // create from serialization only
	CLESDoc();
	DECLARE_DYNCREATE(CLESDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLESDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:

	virtual ~CLESDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLESDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LESDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_)
