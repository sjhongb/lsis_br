// DTSComm.cpp: implementation of the CDTSComm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GPSComm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CGPSComm, CSerialComm)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGPSComm::CGPSComm()
{

}

CGPSComm::~CGPSComm()
{

}

BOOL CGPSComm::SendMsg(BYTE *pBuffer, USHORT nMsgLen)
{
#if 0
	// Add additional process for Message send.
	WORD	wCrc		= 0x00;
	USHORT	nFinalLen	= 0;

	BYTE	RawMsgBuffer[MAX_BUFFER_LEN] = {0,};	// DLE Encoding 이전 메시지 버퍼
	BYTE	DleMsgBuffer[MAX_BUFFER_LEN] = {0,};	// DLE Encoding 결과 메시지 버퍼 (최종 송신 메시지) 

	// STX(1), ETX(1), CRC16(2)를 포함한 크기가 버퍼 크기를 초과하면 안된다.
	if(nMsgLen > MAX_BUFFER_LEN - 4)
	{
		return FALSE;
	}

	// STX 삽입
	RawMsgBuffer[0] = STX;

	// 메시지 삽입
	memcpy(&RawMsgBuffer[1], pBuffer, nMsgLen);

	// CRC16 생성 & 삽입
	wCrc = CalcCRC16(RawMsgBuffer, nMsgLen + 1);
	RawMsgBuffer[nMsgLen + 1] = (BYTE)(wCrc >> 8);
	RawMsgBuffer[nMsgLen + 2] = (BYTE)(wCrc & 0xFF);

	// ETX 삽입
	RawMsgBuffer[nMsgLen + 3] = ETX;
	
	// Raw 메시지를 DLE 인코딩
	EncodeDLE(RawMsgBuffer, nMsgLen + 4, DleMsgBuffer, &nFinalLen);
	if(nFinalLen > MAX_BUFFER_LEN)
	{
		return FALSE;
	}
	
	return CSerialComm::SendMsg(DleMsgBuffer, nFinalLen);
#endif

	return CSerialComm::SendMsg(pBuffer, nMsgLen);
}

void CGPSComm::ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen)
{
	UINT	nPos = 0;
	
	USHORT	nDleLen = 0, nRawLen = 0;

	BYTE	*pStx			= NULL;
	BYTE	RawMsgBuffer[MAX_BUFFER_LEN] = {0,};	// DLE Decoding 결과 메시지 버퍼

	// 메시지 버퍼의 내용을 탐색하여 STX - ETX 단위로 메시지 처리한다.
	for(nPos = 0 ; nPos < nMsgLen ; nPos++)
	{
		switch(pBuffer[nPos])
		{
		case GPS_STX:
			if( pBuffer[nPos+1] == 'G' &&
				pBuffer[nPos+2] == 'P' &&
				pBuffer[nPos+3] == 'R' &&
				pBuffer[nPos+4] == 'M' &&
				pBuffer[nPos+5] == 'C')
			{
				pStx	= &pBuffer[nPos];
			}
			break;

		case GPS_ETX:
			if(pStx == NULL)			{
			}
			else
			{
				TRACE("Message Received.\n");

				// 메시지를 처리한다.
				nDleLen = &pBuffer[nPos] - pStx + 3;

				DecodeDLE(pStx, nDleLen, RawMsgBuffer, &nRawLen);


				// 여기서 OpCode별 처리를 담당하는 Event Handler가 호출되게 한다.
				// Window Message 송신 or Thread Event Set.

				::SendMessage(m_pMainWnd->m_hWnd, WM_GPS_COMM_RECEIVE_MSG, (WPARAM)nRawLen, (LPARAM)RawMsgBuffer);
			}

			pStx = NULL;
			break;
		}
	}

	if(pStx == NULL)
	{
		// STX가 남아있지 않으므로 남은 메시지 크기를 0으로 하고, 버퍼를 비운다.
		nMsgLen = 0;
		memset(pBuffer, 0x00, MAX_BUFFER_LEN);
	}
	else
	{
		// 마지막 STX 위치 이전의 데이터는 모두 처리되었으므로 남은 메시지 길이에서 제한다.
		nMsgLen = nMsgLen - (pStx - pBuffer);

		// 마지막 STX 위치에서부터 버퍼의 끝까지 내용을 버퍼 맨 앞으로 옮기고 나머지 영역은 클리어한다.
		memcpy(pBuffer, pStx, nMsgLen);
		memset(pBuffer + nMsgLen, 0x00, MAX_BUFFER_LEN - nMsgLen);
	}
}