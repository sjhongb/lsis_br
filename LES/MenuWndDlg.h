//=======================================================
//==              MenuWndDlg.h
//=======================================================
//  파 일 명 :  MenuWndDlg.h
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메뉴 위도우 화면을 생성하고 각 버튼 입력을
//              처리한다.
//
//=======================================================

#if !defined(AFX_MENUWNDDLG_H__1E931B0B_C915_49B1_86A4_E12FB68776D8__INCLUDED_)
#define AFX_MENUWNDDLG_H__1E931B0B_C915_49B1_86A4_E12FB68776D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MenuListBox.h"
#include "XPStyleButtonST.h"
#include "../Include/GridCtrl.h"

#define WM_F1				(WM_USER+1001)
#define WM_F2				(WM_USER+1002)
#define WM_F3				(WM_USER+1003)
#define WM_F4				(WM_USER+1004)
#define WM_F5				(WM_USER+1005)
#define WM_F6				(WM_USER+1006)
#define WM_F7				(WM_USER+1007)
#define WM_F8				(WM_USER+1008)
#define WM_F9				(WM_USER+1009)
#define WM_F10				(WM_USER+1010)
#define WM_F11				(WM_USER+1011)
#define WM_F12				(WM_USER+1012)
#define WM_AUTOEXIT	        (WM_USER+1013)
#define WM_AUTOEXITEX       (WM_USER+1014)

class CMenuWndDlg : public CDialog
{
// Construction
public:
	void AddDataLog(BOOL bRecv, int nLength, BYTE *pBuffer);
	void AddErrorLog(CString &strData);
	void OnSetXpButton();
	void SetPosition();
	void SetHidePosition();
	CMenuWndDlg(CWnd* pParent = NULL);   // standard constructor
	CWnd* m_pParentWnd;
	CThemeHelperST		m_ThemeHelper;	//for XP Style button
	BOOL m_bIsCreate;
	BOOL m_bBlockDevMode;
//	CMenuListBox	m_listBox;
	CGridCtrl		m_ctrlLogGrid;
	int m_iTrans;
	void Bmainroute();
	void Bcallonroute();
	void Bshuntroute();
	void Bsignalcancel();
	void Bswitch();

// Dialog Data
	//{{AFX_DATA(CMenuWndDlg)
	enum { IDD = IDD_DIALOG_MENU };
	CXPStyleButtonST	m_btnScroll;
	CXPStyleButtonST	m_cBtnTitle;
	CXPStyleButtonST	m_btnUser;
	CXPStyleButtonST	m_btnControl;
	CXPStyleButtonST	m_btnSystem;
	CXPStyleButtonST	m_btnSwitch;
	CXPStyleButtonST	m_btnSigCancel;
	CXPStyleButtonST	m_btnShuntSet;
	CXPStyleButtonST	m_btnCallonSet;
	CXPStyleButtonST	m_btnMainSet;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMenuWndDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMenuWndDlg)
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBmainroute();
	afx_msg void OnBshuntroute();
	afx_msg void OnBsignalcancel();
	afx_msg void OnBswitch();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBcallonroute();
	afx_msg void OnBtnMenu();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnBtnScroll();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString m_strLastSendData;
	CString m_strLastRcvData;
	BOOL m_bIsHidden;
	BOOL m_bIsAutoScroll;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MENUWNDDLG_H__1E931B0B_C915_49B1_86A4_E12FB68776D8__INCLUDED_)
