// AllUserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LES.h"
#include "AllUserDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAllUserDlg dialog

CAllUserDlg::CAllUserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAllUserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAllUserDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAllUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllUserDlg)
	DDX_Control(pDX, IDC_LIST_USER, m_ListUser);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAllUserDlg, CDialog)
	//{{AFX_MSG_MAP(CAllUserDlg)
	ON_BN_CLICKED(IDC_ADD_USER, OnAddUser)
	ON_BN_CLICKED(IDC_CHANGE_PASS, OnChangePass)
	ON_BN_CLICKED(IDC_USER_DELETE, OnUserDelete)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAllUserDlg message handlers

BOOL CAllUserDlg::OnInitDialog() 
{
	CLESApp*	pApp = (CLESApp*)AfxGetApp();
	CRect		rect;
	CDC*		pDC = GetDC();
	CDialog::OnInitDialog();
	m_ListUser.OnInitial();
	CSize chExtent = pDC->GetTextExtent( "W", 1 );
	ReleaseDC( pDC );
	chExtent.cx *= 50;
	m_ListUser.SetHorizontalExtent( chExtent.cx );

	CenterWindow(AfxGetMainWnd());
	return TRUE;  
}

void CAllUserDlg::OnAddUser() 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	m_ListUser.OnAddUser();
//	pApp->OnLoadUserID();
//	pApp->OnSaveUserID();
}

void CAllUserDlg::OnChangePass() 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	m_ListUser.OnChange();
//	pApp->OnLoadUserID();
//	pApp->OnSaveUserID();
}

void CAllUserDlg::OnUserDelete() 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	m_ListUser.OnDelete();
//	pApp->OnLoadUserID();
//	pApp->OnSaveUserID();
}

void CAllUserDlg::OnDestroy() 
{
	CDialog::OnDestroy();
}


void CAllUserDlg::OnOK() 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	pApp->OnLoadUserID();
	pApp->OnSaveUserID();

	m_ListUser.FlushList();
	CDialog::OnOK();
}

