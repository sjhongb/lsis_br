#if !defined(AFX_SELECTLC_H__DE47972B_9715_4845_B807_20FFE435EEAC__INCLUDED_)
#define AFX_SELECTLC_H__DE47972B_9715_4845_B807_20FFE435EEAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectLC.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectLC dialog

class CSelectLC : public CDialog
{
// Construction
public:
	CLESApp* pApp;
	CSelectLC(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectLC)
	enum { IDD = IDD_DLG_SELECT_LC };
	int		m_nIndexOfLC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectLC)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectLC)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTLC_H__DE47972B_9715_4845_B807_20FFE435EEAC__INCLUDED_)
