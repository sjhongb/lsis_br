#if !defined(AFX_MENULISTBOX_H__7697FD1B_2456_4519_A51C_4DE759FA54F3__INCLUDED_)
#define AFX_MENULISTBOX_H__7697FD1B_2456_4519_A51C_4DE759FA54F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MENU_MAIN		  1
#define MENU_CALLON		  2
#define MENU_SHUNT		  3
#define MENU_SIGNAL		  4
#define MENU_POINT		  5
#define MENU_ERR		  6
// MenuListBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMenuListBox window
#include "les.h"

class CMenuListBox : public CListBox
{
// Construction
public:
	CMenuListBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMenuListBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	CWnd* m_pParent;
	int LoadMenuItem();
	BYTE m_nType;	//1:주진로 2:입환진로 3:선로전환기 4:신호기취소 5:구분진로해정 
	int AddString(LPCTSTR lpszItem);
	virtual ~CMenuListBox();
	void OnSelectDest( UINT nID );
	void OnSelectBlocking( UINT nID );
	CString *m_strDestName;
	BOOL m_bSelect;
	// Generated message map functions
protected:
	//{{AFX_MSG(CMenuListBox)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MENULISTBOX_H__7697FD1B_2456_4519_A51C_4DE759FA54F3__INCLUDED_)
