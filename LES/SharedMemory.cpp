// SharedMemory.cpp : implementation file
//

#include "stdafx.h"
#include "../lis/lis.h"
#include "SharedMemory.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SharedMemory

IMPLEMENT_DYNCREATE(SharedMemory, CDocument)

SharedMemory::SharedMemory()
{
	strcpy (SharedMemoryFile, "SharedMemory.dat");
	strcpy (SharedMemoryName, "SharedM");
	SharedMemoryMapFile = 0;

	strcpy (SharedControlFile, "SharedControl.dat");
	strcpy (SharedControlName, "SharedC");
	SharedControlMapFile = 0;

	m_creater = 1;
}

BOOL SharedMemory::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

SharedMemory::~SharedMemory()
{
	UnmapViewOfFile(SharedMemoryAddress);
	UnmapViewOfFile(SharedControlAddress);

	if (m_creater == 1)
	{
	    CloseHandle(SharedMemoryMapFile);
	    CloseHandle(SharedControlMapFile);
	}
}


BEGIN_MESSAGE_MAP(SharedMemory, CDocument)
	//{{AFX_MSG_MAP(SharedMemory)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SharedMemory diagnostics

#ifdef _DEBUG
void SharedMemory::AssertValid() const
{
	CDocument::AssertValid();
}

void SharedMemory::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// SharedMemory serialization
void SharedMemory::Serialize(CArchive &ar)
{
}

void SharedMemory::Serialize()
{
	HANDLE hFile;
	DWORD dvalue;
	char Data[MAX_MSGBLOCK_SIZE];

    m_creater = 1;

	memset(Data, NULL, sizeof(Data));
	hFile = CreateFile(SharedMemoryFile, GENERIC_WRITE | GENERIC_READ, 
		FILE_SHARE_DELETE |FILE_SHARE_WRITE|FILE_SHARE_READ, NULL, CREATE_ALWAYS , FILE_ATTRIBUTE_NORMAL, NULL);
	
	WriteFile(hFile, Data, (DWORD)sizeof(Data), &dvalue, NULL);


	SharedMemoryMapFile = CreateFileMapping(hFile,NULL, PAGE_READWRITE, 0, 0, SharedMemoryName);
	if ( SharedMemoryMapFile == NULL) 
	{
		AfxMessageBox("Shared  MemoryFile을 만들수 없습니다.");
	}


	memset(Data, NULL, sizeof(Data));
	hFile = CreateFile(SharedControlFile, GENERIC_WRITE | GENERIC_READ, 
		FILE_SHARE_DELETE |FILE_SHARE_WRITE|FILE_SHARE_READ, NULL, CREATE_ALWAYS , FILE_ATTRIBUTE_NORMAL, NULL);
	
	WriteFile(hFile, Data, (DWORD)sizeof(Data), &dvalue, NULL);


	SharedControlMapFile = CreateFileMapping(hFile,NULL, PAGE_READWRITE, 0, 0, SharedControlName);
	if ( SharedControlMapFile == NULL) 
	{
		AfxMessageBox("Shared  ControlFile을 만들수 없습니다.");
	}


	SharedMemoryInit();
}

/////////////////////////////////////////////////////////////////////////////
// SharedMemory commands

SharedMemory *_pShared=NULL;

void SharedMemory::SharedMemoryInit()
{

	if (!SharedMemoryMapFile)
	{

	    SharedMemoryMapFile  =  OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, SharedMemoryName);
    }
	SharedMemoryAddress  =  MapViewOfFile(SharedMemoryMapFile, FILE_MAP_ALL_ACCESS,0, 0, 0);


	if (!SharedControlMapFile)
	{

	    SharedControlMapFile  =  OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, SharedControlName);
    }
	SharedControlAddress  =  MapViewOfFile(SharedControlMapFile, FILE_MAP_ALL_ACCESS,0, 0, 0);


	_pShared = this;
}
