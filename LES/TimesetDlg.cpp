// TimesetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LES.h"
#include "TimesetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimesetDlg dialog


CTimesetDlg::CTimesetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimesetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTimesetDlg)
	m_iDay = 0;
	m_iMonth = 0;
	m_iYear = 0;
	m_iHour = 0;
	m_iMin = 0;
	m_iSec = 0;
	//}}AFX_DATA_INIT
}


void CTimesetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimesetDlg)
	DDX_Text(pDX, IDC_EDT_DAY, m_iDay);
	DDX_Text(pDX, IDC_EDT_MONTH, m_iMonth);
	DDX_Text(pDX, IDC_EDT_YEAR, m_iYear);
	DDX_Text(pDX, IDC_EDT_HOUR, m_iHour);
	DDX_Text(pDX, IDC_EDT_MIN, m_iMin);
	DDX_Text(pDX, IDC_EDT_SEC, m_iSec);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTimesetDlg, CDialog)
	//{{AFX_MSG_MAP(CTimesetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimesetDlg message handlers

void CTimesetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );

	if(m_iYear < 2000){
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iYear > 2090 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iMonth < 1 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iMonth > 12 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iDay < 1 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iDay > 31 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iHour < 0 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iHour > 23 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iMin < 0 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iMin > 59 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iSec < 0 ) {
		MessageBox("Time Set Error \n Check Time");
	} else if ( m_iSec > 59 ) {
		MessageBox("Time Set Error \n Check Time");
	} else {
		CJTime t( m_iYear, m_iMonth , m_iDay, m_iHour, m_iMin, m_iSec);
		m_Time = t;

		CDialog::OnOK();
	}
}

BOOL CTimesetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CTime time;

	time = CTime::GetCurrentTime();

	if ( time != -1 ) {

		m_iYear = time.GetYear();
		m_iMonth = time.GetMonth();
		m_iDay = time.GetDay();
		m_iHour = time.GetHour();
		m_iMin = time.GetMinute();
		m_iSec = time.GetSecond();
	
		UpdateData(FALSE);
	}

	CenterWindow(AfxGetMainWnd());
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

