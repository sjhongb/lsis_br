#if !defined(AFX_BASICDLG_H__951E4C03_861E_412C_AFDE_2AD54947C5C8__INCLUDED_)
#define AFX_BASICDLG_H__951E4C03_861E_412C_AFDE_2AD54947C5C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BasicDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBasicDlg dialog

class CBasicDlg : public CDialog
{
// Construction
public:
	CBasicDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_DlgTitle;
	HICON hIcon;

// Dialog Data
	//{{AFX_DATA(CBasicDlg)
	enum { IDD = IDD_DLG_BASIC };
	CStatic	m_stIcon;
	CString	m_stState;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBasicDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBasicDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASICDLG_H__951E4C03_861E_412C_AFDE_2AD54947C5C8__INCLUDED_)
