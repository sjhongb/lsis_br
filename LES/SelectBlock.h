#if !defined(AFX_SELECTBLOCK_H__84BD0EE9_6091_41E0_91AD_0628CC8FDB16__INCLUDED_)
#define AFX_SELECTBLOCK_H__84BD0EE9_6091_41E0_91AD_0628CC8FDB16__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectBlock.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectBlock dialog

class CSelectBlock : public CDialog
{
// Construction
public:
	int m_nRealNumberOfBlock;
	CSelectBlock(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectBlock)
	enum { IDD = IDD_DLG_SELECT_BLCOK };
	int		m_nIndexOfBlock;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectBlock)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectBlock)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CSelectBlockControl dialog

class CSelectBlockControl : public CDialog
{
// Construction
public:
	int m_nIndexOfBlock;
	int m_nIndexOfBlockFunction;
	CSelectBlockControl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectBlockControl)
	enum { IDD = IDD_DLG_CONTROL_BLCOK };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectBlockControl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectBlockControl)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTBLOCK_H__84BD0EE9_6091_41E0_91AD_0628CC8FDB16__INCLUDED_)
