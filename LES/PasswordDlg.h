#if !defined(AFX_PASSWORDDLG_H__E9E12CBC_6F42_49BD_A070_4C529FA54CD0__INCLUDED_)
#define AFX_PASSWORDDLG_H__E9E12CBC_6F42_49BD_A070_4C529FA54CD0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PasswordDlg.h : header file
//
#include "UserComboBox.h"
/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog

class CPasswordDlg : public CDialog
{
// Construction
public:
	BOOL m_bIsLock;
	CString str_RegistryKey;
	CPasswordDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_DlgTitle;
	HICON hIcon;

// Dialog Data
	//{{AFX_DATA(CPasswordDlg)
	enum { IDD = IDD_DLG_PASS };
	CUserComboBox	m_ComboUser;
	CStatic	m_stIcon;
	CString	m_stState;
	CString	m_strPassword;
	CString	m_strUserID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPasswordDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPasswordDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PASSWORDDLG_H__E9E12CBC_6F42_49BD_A070_4C529FA54CD0__INCLUDED_)
