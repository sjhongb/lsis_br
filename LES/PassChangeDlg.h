#if !defined(AFX_PASSCHANGEDLG_H__230E9955_5231_42CD_B4CC_819F823E1E23__INCLUDED_)
#define AFX_PASSCHANGEDLG_H__230E9955_5231_42CD_B4CC_819F823E1E23__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PassChangeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPassChangeDlg dialog

class CPassChangeDlg : public CDialog
{
// Construction
public:
	void OnSetPass(CString &password);
	CString m_DlgTitle;
	CPassChangeDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_OriginPass;
// Dialog Data
	//{{AFX_DATA(CPassChangeDlg)
	enum { IDD = IDD_DLG_CHANGE };
	CString	m_strBefore;
	CString	m_strPassNew;
	CString	m_strConfirm;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPassChangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPassChangeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PASSCHANGEDLG_H__230E9955_5231_42CD_B4CC_819F823E1E23__INCLUDED_)
