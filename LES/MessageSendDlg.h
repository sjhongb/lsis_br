#if !defined(AFX_MESSAGESENDDLG_H__1FFFCF41_E4C1_43AC_BF7C_5138206AC5DE__INCLUDED_)
#define AFX_MESSAGESENDDLG_H__1FFFCF41_E4C1_43AC_BF7C_5138206AC5DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MessageSendDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMessageSendDlg dialog

class CMessageSendDlg : public CDialog
{
// Construction
public:
	CMessageSendDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMessageSendDlg)
	enum { IDD = IDD_DLG_MESSAGE_SEND };
	CEdit	m_cSendMessage;
	CString	m_strSendMessage;
	CString	m_strMSgCNT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMessageSendDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMessageSendDlg)
	virtual void OnOK();
	afx_msg void OnChangeEdtMessagesend();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MESSAGESENDDLG_H__1FFFCF41_E4C1_43AC_BF7C_5138206AC5DE__INCLUDED_)
