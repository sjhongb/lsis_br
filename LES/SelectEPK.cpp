// SelectEPK.cpp : implementation file
//

#include "stdafx.h"
#include "les.h"
#include "SelectEPK.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectEPK dialog


CSelectEPK::CSelectEPK(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectEPK::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectEPK)
		// NOTE: the ClassWizard will add member initialization here
	m_nIndexOfEPK = 0;
	m_nIndexOfGroupA = 0;
	m_nIndexOfGroupB = 0;
	pApp = (CLESApp*)AfxGetApp();
	//}}AFX_DATA_INIT
}


void CSelectEPK::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectEPK)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Radio(pDX,IDC_INDEX_AEPK,m_nIndexOfGroupA);
	DDX_Radio(pDX,IDC_INDEX_UPEPK,m_nIndexOfGroupB);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectEPK, CDialog)
	//{{AFX_MSG_MAP(CSelectEPK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectEPK message handlers

void CSelectEPK::OnOK()
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	if (pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2)
	{
		m_nIndexOfEPK = m_nIndexOfGroupB + 8;
	}
	else if ( pApp->m_VerifyObjectForMessage.iNumberOfEPK == 0 )
	{
		CDialog::OnCancel();
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK == 3 && pApp->m_VerifyObjectForMessage.strProjectName == "StandardOfLC")
	{
		if ( m_nIndexOfGroupB == 2 )
		{
			m_nIndexOfEPK = 5;
		}
		else
		{
			m_nIndexOfEPK = m_nIndexOfGroupB + 8;
		}
	}
	else
	{
		m_nIndexOfEPK = m_nIndexOfGroupA + 1;
	}
	CDialog::OnOK();
}

BOOL CSelectEPK::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	if (pApp->m_VerifyObjectForMessage.iNumberOfEPK == 2)
	{
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_AEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_BEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_CEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK ==3 && pApp->m_VerifyObjectForMessage.strProjectName == "StandardOfLC")
	{
		GetDlgItem( IDC_INDEX_AEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_BEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_CEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK ==3 )
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK==4)
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK==5)
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK==6)
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	else if (pApp->m_VerifyObjectForMessage.iNumberOfEPK==7)
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem( IDC_INDEX_UPEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DNEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK2 )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_AEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_BEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_CEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_DEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_EEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_FEPK )->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_GEPK )->EnableWindow(FALSE);
	}
	
	CenterWindow(AfxGetMainWnd());
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
