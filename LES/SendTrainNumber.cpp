// SendTrainNumber.cpp : implementation file
//

#include "stdafx.h"
#include "LES.h"
#include "SendTrainNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendTrainNumber dialog


CSendTrainNumber::CSendTrainNumber(CWnd* pParent /*=NULL*/)
	: CDialog(CSendTrainNumber::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendTrainNumber)

	m_nIndex = -1;
	m_strTrainNumber = _T("");

//}}AFX_DATA_INIT
}


void CSendTrainNumber::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendTrainNumber)
	DDX_Radio(pDX,IDC_INDEX_STATION1,m_nIndex);
	DDX_Text(pDX,IDC_EDT_TRAIN_NUMBER,m_strTrainNumber);
	DDV_MaxChars(pDX, m_strTrainNumber, 5);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendTrainNumber, CDialog)
	//{{AFX_MSG_MAP(CSendTrainNumber)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendTrainNumber message handlers

int CSendTrainNumber::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	// TODO: Add your specialized creation code here
	m_nIndex = 0;
	m_strTrainNumber = _T("");
	
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("LAKSAM")==0)
	{
		GetDlgItem( IDC_INDEX_STATION1 )->SetWindowText("ALISHAHAR");
		GetDlgItem( IDC_INDEX_STATION2 )->SetWindowText("DAULATGANJ");
		GetDlgItem( IDC_INDEX_STATION3 )->SetWindowText("CHITOSE ROAD");
		
		GetDlgItem( IDC_INDEX_STATION2)->EnableWindow(TRUE);
		GetDlgItem( IDC_INDEX_STATION3)->EnableWindow(TRUE);
	}
	else if (pApp->m_VerifyObjectForMessage.strStationName.Find("CHINKI")==0)
	{
		GetDlgItem( IDC_INDEX_STATION1 )->SetWindowText("MASTAN NAGAR");
		GetDlgItem( IDC_INDEX_STATION2 )->SetWindowText("None");
		GetDlgItem( IDC_INDEX_STATION3 )->SetWindowText("None");
		
		GetDlgItem( IDC_INDEX_STATION2)->EnableWindow(FALSE);
		GetDlgItem( IDC_INDEX_STATION3)->EnableWindow(FALSE);
	}

	CenterWindow(AfxGetMainWnd());

	return TRUE;
}

void CSendTrainNumber::OnOK()
{
	UpdateData(TRUE);

	CDialog::OnOK();
}
