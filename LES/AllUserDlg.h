#if !defined(AFX_ALLUSERDLG_H__0B7F5AC5_7425_45C8_8E93_7830D1FF2676__INCLUDED_)
#define AFX_ALLUSERDLG_H__0B7F5AC5_7425_45C8_8E93_7830D1FF2676__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AllUserDlg.h : header file
//
#include "UserListBox.h "

/////////////////////////////////////////////////////////////////////////////
// CAllUserDlg dialog

class CAllUserDlg : public CDialog
{
// Construction
public:
	CAllUserDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAllUserDlg)
	enum { IDD = IDD_DLG_OUSER };
	CUserListBox	m_ListUser;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllUserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAllUserDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddUser();
	afx_msg void OnChangePass();
	afx_msg void OnUserDelete();
	afx_msg void OnDestroy();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALLUSERDLG_H__0B7F5AC5_7425_45C8_8E93_7830D1FF2676__INCLUDED_)
