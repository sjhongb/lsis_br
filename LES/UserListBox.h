#if !defined(AFX_USERLISTBOX_H__8C22564F_2DFF_4D97_BB5F_4261F51CD048__INCLUDED_)
#define AFX_USERLISTBOX_H__8C22564F_2DFF_4D97_BB5F_4261F51CD048__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserListBox.h : header file

#include "PassChangeDlg.h"
#include "AddUserDlg.h"
#include "BasicDlg.h"


#define ID_CHANGE_PASS	99
#define ID_DELETE_USER	100
#define MAX_USER_CNT    10
/////////////////////////////////////////////////////////////////////////////
// CUserListBox window

class CUserListBox : public CListBox
{
// Construction
public:
	int m_nLimitCount;
	CString m_UserSection;
	CString m_PassSection;
	CString m_Entry;
	CPtrArray m_Items;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserListBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnDeleteReg(int position,CString &text);
	void OnSaveUserID();
	void OnLoadUserID();
	void OnInitial();
	void FlushList();
	int AddString(LPCTSTR lpszItem);
	void OnAddUser();
	CString GetPassword( CString strID );
	void DisplayMessageBox ( int iStringID , int iStringTitle = 0 );
	void DisplayMessageBox ( CString strStringID , CString strStringTitle = "");
	CString GetString ( int iStringID );
	afx_msg void OnDelete();
	afx_msg void OnChange();

protected:
	//{{AFX_MSG(CUserListBox)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnSelchange();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERLISTBOX_H__8C22564F_2DFF_4D97_BB5F_4261F51CD048__INCLUDED_)
