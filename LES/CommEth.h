#if !defined(AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_)
#define AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommEth.h : header file
//

/////////////////////////////////////////////////////////////////////////////
#define ID_COMM_WATCH	 200
#define ID_COMM_ERRMSG   201
#define ID_COMM_PRINTMSG 202

/////////////////////////////////////////////////////////////////////////////
#define COM_EXT ((BYTE)0x70) 
#define COM_STX ((BYTE)0x7E) 
#define COM_ETX ((BYTE)0xFE)

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CCommEth command target

class CCommEth : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CCommEth();
	virtual ~CCommEth();
	BOOL SendData(BYTE* data, int len, BYTE stx = COM_STX, BOOL bCmd = FALSE);

	int  m_nCmdCounter;
	BOOL m_bSendToTx;
// Overrides
public:
	BOOL CreateSocket(HWND hWnd, CString strEthIP, UINT nEthPort, CString strCBI1IP, CString strCBI2IP);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommEth)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CCommEth)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:

	void GetRxCRC16(BYTE c);
	void GetTxCRC16(BYTE c);

private:
	union {
		unsigned short m_nRxCRC;
		struct {
			BYTE m_RxCRCLow;
			BYTE m_RxCRCHigh;
		};
	};
	union {
		unsigned short m_nTxCRC;
		struct {
			BYTE m_TxCRCLow;
			BYTE m_TxCRCHigh;
		};
	};
	union {
		unsigned short m_nInCRC;
		struct {
			BYTE m_InCRCLow;
			BYTE m_InCRCHigh;
		};
	};
	union {
		unsigned short  m_nInRxMax;
		struct {
			BYTE m_nInRxLow;
			BYTE m_nInRxHigh;
		};
	};
	COMCommandType m_PostDB;
	HWND	m_hParentWnd;

	CString m_strEthIP;
	CString m_strCBI1IP;
	CString m_strCBI2IP;

	UINT	m_nEthPort;
// 	UINT	m_nCBI1Port;
// 	UINT	m_nCBI2Port;

	int  m_nTxPtr;
	BOOL m_bTxReady;
	BYTE m_pDataQ[4096];
	BYTE m_pTxQ[4096];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMETH_H__8E6147F4_29B3_4466_B100_757AC8676966__INCLUDED_)
