#if !defined(AFX_TRACKBLOCKDLG_H__110F8B7E_D8F5_4ED2_8E83_1DB8F1723BF5__INCLUDED_)
#define AFX_TRACKBLOCKDLG_H__110F8B7E_D8F5_4ED2_8E83_1DB8F1723BF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrackBlockDlg.h : header file
//
#include "UserComboBox.h"

/////////////////////////////////////////////////////////////////////////////
// CTrackBlockDlg dialog

class CTrackBlockDlg : public CDialog
{
// Construction
public:
	CString str_Entry;
	CString str_RegistryUser;
	void OnDeleteLockUser();
	void OnSaveLockUser();
	void OnSetCmd();
	BOOL OnSetInit();
	CString m_strTrackName;
	CTrackBlockDlg(CWnd* pParent = NULL);   // standard constructor
	CString str_RegistryPass;
	CString str_RegistryBlocking;
	int m_ControlMode;	//0;차단 취급 선택 요구  1:차단 취급 선택 완료 2: 해제 취급 선택 완료 
	BYTE m_BlockID;
	BYTE m_bLock, m_bFree;
	int m_RegistryBlockNumber; //차단된 궤도 개수 
// Dialog Data
	//{{AFX_DATA(CTrackBlockDlg)
	enum { IDD = IDD_DLG_BLOCK };
	CUserComboBox	m_ComboUser;
	CString	m_strPassword;
	int		m_nMode;
	CString	m_strCmd;
	CString	m_strUserID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrackBlockDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTrackBlockDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRACKBLOCKDLG_H__110F8B7E_D8F5_4ED2_8E83_1DB8F1723BF5__INCLUDED_)
