//=======================================================
//  파 일 명 :  UserListBox.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
#include "stdafx.h"
#include "les.h"
#include "UserListBox.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CUserListBox, CListBox)
	//{{AFX_MSG_MAP(CUserListBox)
	ON_WM_LBUTTONDBLCLK()
	ON_CONTROL_REFLECT(LBN_SELCHANGE, OnSelchange)
	ON_COMMAND(ID_CHANGE_PASS,OnChange)
	ON_COMMAND(ID_DELETE_USER,OnDelete)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CUserListBox::OnAddUser()
{
	CAddUserDlg		m_AddUserDlg;
	LoginItem		*pItem;
	CString			strPassword;
	CWinApp *pApp = AfxGetApp();

	if ( m_nLimitCount >= MAX_USER_CNT ) {
		AfxMessageBox ( "At maximum 10 users can register");
		return;
	}

	if(m_AddUserDlg.DoModal() == IDOK) 
	{
		CString strEntry, strUserName, strForCmpUserName;
		BOOL bExistSameUserName = FALSE;
		
		strUserName = m_AddUserDlg.m_strID;
		
		for ( int k = 1; k <= m_nLimitCount; k++ )
		{
			strEntry.Format("%s%d", "ID", k);
			strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);
			
			if ( strUserName == strForCmpUserName )
			{
				bExistSameUserName = TRUE;
				break;
			}
		}

		if ( bExistSameUserName ) 
		{
			DisplayMessageBox ( "Warning!\n\nUser exists already.. Input new user" );
			return;
		}
		pItem = new LoginItem;			
		if ( pItem ) {
			pItem->strID		= m_AddUserDlg.m_strID;
			pItem->strPassword	= m_AddUserDlg.m_strPassword;
			m_Items.InsertAt( 0, pItem );
			InsertString(0,m_AddUserDlg.m_strID);
		}
		m_nLimitCount++;
		OnSaveUserID();

		BYTE Buf[17];
		
		for ( int i = 0; i < 17; i++)
		{
			Buf[i] = '\0';
		}
		//	Buf[0]=Buf[1]=Buf[2]=Buf[3]=Buf[4]=Buf[5]='\0';
		
		Buf[0] = WS_USEROPMSG_CREATE;

		memcpy( &Buf[1] , m_AddUserDlg.m_strID , m_AddUserDlg.m_strID.GetLength());
		
		memcpy( &Buf[9], m_AddUserDlg.m_strPassword, m_AddUserDlg.m_strPassword.GetLength());
		
		((CMainFrame*)AfxGetMainWnd())->SetCommQue( WS_OPMSG_USERID, &Buf[0], 0 );
	}
}

void CUserListBox::OnChange()
{
	CPassChangeDlg		ChangeDlg; 
	CString				strUserID,strPassword;
	LoginItem			*pItem;
	CWinApp				*pApp		= AfxGetApp();
	int					position	= GetCurSel();

	if(position != -1){
		GetText(position,strUserID);
	}
	if((position != -1)&&(strUserID != "Add User ID")){
		strPassword = GetPassword( strUserID );
		ChangeDlg.OnSetPass(strPassword);
		ChangeDlg.m_DlgTitle = strUserID + " Change user ID and Password";
		if(ChangeDlg.DoModal() == IDOK){
			pItem = (LoginItem*)m_Items.GetAt(position);
			pItem->strPassword = ChangeDlg.m_strPassNew;
			OnSaveUserID();

			BYTE Buf[17];
			
			for ( int i = 0; i < 17; i++)
			{
				Buf[i] = '\0';
			}
			
			Buf[0] = WS_USEROPMSG_PASS_CHANGE;
			
			memcpy( &Buf[1] , strUserID , strUserID.GetLength());
			memcpy( &Buf[9] , strPassword , strPassword.GetLength());
			
			((CMainFrame*)AfxGetMainWnd())->SetCommQue( WS_OPMSG_USERID, &Buf[0], 0 );
		}
	} else {
		DisplayMessageBox("Select user ID to change."," Information");
	}
}

void CUserListBox::OnDelete()
{
	CLESApp*		pApp = (CLESApp*)AfxGetApp();
    CBasicDlg		m_BasicDlg;
	CString			strUserID,strMessage,strCurOperator;
	LoginItem*		pItem;
	int				position = GetCurSel();

	if(position != -1) GetText(position,strUserID);

	if((position != -1)&&(strUserID != "Add User ID")){
		m_BasicDlg.m_DlgTitle = "Delete user ID \" " + strUserID + " \"";
		m_BasicDlg.m_stState = "Delete really?";
		m_BasicDlg.hIcon= AfxGetApp()->LoadIcon(IDI_DELETE);
		if(m_BasicDlg.DoModal()==IDOK){
			if(strUserID == pApp->m_SystemValue.strCurrentOperator){
			//	GetText(position+1,strCurOperator);
				AfxMessageBox("Impossible to delete user. \nCurrently logged-in user can not be deleted. \n Log-in with other user's ID and then try deletion of user.");	
			//	pApp->m_SystemValue.strCurrentOperator = strCurOperator;
				return;
			}
			DeleteString( position );
			OnDeleteReg(position,strUserID);
			pItem = (LoginItem*)m_Items.GetAt(position);
			if ( pItem ) delete pItem;
			m_Items.RemoveAt(position);
			m_nLimitCount--;
			pApp->m_SystemValue.nUserNumber = m_nLimitCount;
			pApp->WriteProfileInt("USER_ID","NUMBER",pApp->m_SystemValue.nUserNumber);

			BYTE Buf[17];
			
			for ( int i = 0; i < 17; i++)
			{
				Buf[i] = '\0';
			}
			//	Buf[0]=Buf[1]=Buf[2]=Buf[3]=Buf[4]=Buf[5]='\0';
			
			Buf[0] = WS_USEROPMSG_DELETE;
			
			memcpy( &Buf[1] , strUserID , strUserID.GetLength());
			
			((CMainFrame*)AfxGetMainWnd())->SetCommQue( WS_OPMSG_USERID, &Buf[0], 0 );
		}
	} else{
		DisplayMessageBox("Select user ID to Delete"," Information");
	}
}

void CUserListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CBitmap bmp;
	HBITMAP hOldB;
	HDC hMemDC;
	bmp.LoadBitmap(IDB_USER);
	hMemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
	hOldB = (HBITMAP)SelectObject(hMemDC,bmp);
	CDC* pDC    = CDC::FromHandle(lpDrawItemStruct->hDC);

	if ((int)lpDrawItemStruct->itemID < 0)
	{
		if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && 
			(lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			pDC->DrawFocusRect(&lpDrawItemStruct->rcItem);
		} else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) &&	
			!(lpDrawItemStruct->itemState & ODS_FOCUS)) 
		{
			pDC->DrawFocusRect(&lpDrawItemStruct->rcItem); 
		}
		return;
	}

    CRect  rcItem(lpDrawItemStruct->rcItem);
    CRect  rClient(rcItem);
    CRect  rText(rcItem);

	rText.left += 22;
	rText.top += 3;

	COLORREF crText;
	CString strText;

	if ((lpDrawItemStruct->itemState & ODS_SELECTED) &&
		 (lpDrawItemStruct->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		CBrush br(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->FillRect(&rClient, &br);
	}
	else if (!(lpDrawItemStruct->itemState & ODS_SELECTED) && 
		(lpDrawItemStruct->itemAction & ODA_SELECT)) 
	{
		CBrush br(::GetSysColor(COLOR_WINDOW));
		pDC->FillRect(&rClient, &br);
	}

	if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && 
		(lpDrawItemStruct->itemState & ODS_FOCUS))
	{
		pDC->DrawFocusRect(&rcItem); 
	}
	else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) &&	
		!(lpDrawItemStruct->itemState & ODS_FOCUS))
	{
		pDC->DrawFocusRect(&rcItem); 
	}

	int iBkMode = pDC->SetBkMode(TRANSPARENT);

	if (lpDrawItemStruct->itemState & ODS_SELECTED)
		crText = pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
	else if (lpDrawItemStruct->itemState & ODS_DISABLED)
		crText = pDC->SetTextColor(::GetSysColor(COLOR_GRAYTEXT));
	else
		crText = pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));

	GetText(lpDrawItemStruct->itemID, strText);
	if(strText == "Add User ID")
	{
		crText = pDC->SetTextColor(RGB(0,128,0));
	}
	else{
			BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left+2,lpDrawItemStruct->rcItem.top+2,16,16,hMemDC,0,0,SRCCOPY);
	}

	UINT nFormat = DT_LEFT | DT_SINGLELINE | DT_VCENTER;
	if (GetStyle() & LBS_USETABSTOPS) nFormat |= DT_EXPANDTABS;
	
	pDC->DrawText(strText, -1, &rText, nFormat | DT_CALCRECT);
	pDC->DrawText(strText, -1, &rText, nFormat);

	pDC->SetTextColor(crText); 
	pDC->SetBkMode(iBkMode);	

	SelectObject(hMemDC,hOldB);
	DeleteDC(hMemDC);
}

void CUserListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	lpMeasureItemStruct->itemHeight =20;
}

void CUserListBox::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	ClientToScreen( &point );
	int position = GetCurSel();
	CString text ;

	if(position == -1) return;
	GetText(position,text);
			
	if(text == "Add User ID") {
		OnAddUser();
	} else {
		CMenu menu;
		menu.CreatePopupMenu();
		menu.AppendMenu(MF_STRING, ID_CHANGE_PASS, "Change Password");
		menu.AppendMenu(MF_STRING, ID_DELETE_USER, "Delete ID");
		menu.TrackPopupMenu(TPM_LEFTALIGN + TPM_RIGHTBUTTON,  point.x,  point.y, this);
	}
	CListBox::OnLButtonDblClk(nFlags, point);
}

int CUserListBox::AddString(LPCTSTR lpszItem)
{
	int iRet = CListBox::AddString(lpszItem);
	if (iRet >= 0) SetItemData(iRet, -1);
	return iRet;
}

void CUserListBox::FlushList()
{
	int i,iItemSize;
	LoginItem *pItem;

	iItemSize = m_Items.GetSize();
	for(i=0; i<iItemSize ; i++) {
		pItem = (LoginItem*)m_Items.GetAt(i);
		if ( pItem ) delete pItem;
	}
	m_Items.RemoveAll();
}

void CUserListBox::OnInitial()
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	m_nLimitCount = pApp->m_SystemValue.nUserNumber;
	m_UserSection = "USER_ID";
	m_PassSection = "USER_PASS";
	m_Entry		  = "ID";
	OnLoadUserID();
}

void CUserListBox::OnLoadUserID()
{
	int  i, n;
	LoginItem *pItem;
	CString strID, strPassword, entry;
	CWinApp *pApp = AfxGetApp();

	FlushList();
	m_nLimitCount = pApp->GetProfileInt( m_UserSection, "NUMBER", m_nLimitCount);
	for (i=1; i<=m_nLimitCount; i++) {
		entry.Format("%s%d", m_Entry, i);
		strID = pApp->GetProfileString( m_UserSection, entry, _T("") );
		if (strID != "") {
			strPassword = pApp->GetProfileString( m_PassSection, strID, _T("password") );
			pItem = new LoginItem;			
			if ( pItem ) {
				pItem->strID = strID;
				pItem->strPassword = strPassword;
				n = m_Items.GetSize();
				m_Items.SetAtGrow( n, pItem );
			}
		}
	}
	n = m_Items.GetSize();
	for(i=0; i<n; i++) {
		pItem = (LoginItem*)m_Items.GetAt(i);
		AddString(pItem->strID);
	}
	AddString("Add User ID");

}

void CUserListBox::OnSaveUserID()
{
	if ( m_nLimitCount == 0 ) return; // 유저가 한명도 없을 경우는 잘못된 경우이다.
	
	int  i, n;
	LoginItem *pItem;
	CString strID, strPassword, entry;
	CLESApp* pApp = (CLESApp*)AfxGetApp();

	n = m_Items.GetSize();
	for(i=1; i<=n; i++) {
		entry.Format("%s%d", m_Entry, i);
		pItem = (LoginItem*)m_Items.GetAt(i-1);
		pApp->WriteProfileString( m_UserSection, entry, pItem->strID );
		pApp->WriteProfileString( m_PassSection, pItem->strID, pItem->strPassword );
	}
	for (; i<=m_nLimitCount; i++) {
		entry.Format("%s%d", m_Entry, i);
		pApp->WriteProfileString( m_UserSection, entry, "" );
	}
	pApp->m_SystemValue.nUserNumber = m_nLimitCount;
	pApp->WriteProfileInt("USER_ID","NUMBER",pApp->m_SystemValue.nUserNumber);
}

void CUserListBox::OnDeleteReg(int position,CString& text)		//Registry에서 그 항목 삭제 
{
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	CString strID, entry;

	for(int i=position+1; i<=m_nLimitCount; i++) {		//땡겨서 저장하면 마지막에 지우려는 데이터만 남는다.
		entry.Format("%s%d", m_Entry, i+1);
		strID = pApp->GetProfileString( m_UserSection, entry, _T("") );
		entry.Format("%s%d", m_Entry, i);
		pApp->WriteProfileString( m_UserSection, entry, strID );
	
	}
	HKEY	hResult;		//User 목록과 Password목록 동시에 지우기 
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\USER_PASS",&hResult);
	RegDeleteValue(hResult,text);
	RegCloseKey(hResult);
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\USER_ID",&hResult);
	CString ttt;
	ttt.Format("%s%d",m_Entry,m_nLimitCount);
	RegDeleteValue(hResult,ttt);
	RegCloseKey(hResult);

}

BOOL CUserListBox::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN){
			int position = GetCurSel();
			CString text ;
			GetText(position,text);
			CRect rect,rc;
			GetWindowRect( &rect );
			GetItemRect(position,&rc);

			if(text == "Add User ID"){
				OnAddUser();
				return 1;
			}
			else{
				CMenu menu;
				menu.CreatePopupMenu();
				menu.AppendMenu(MF_STRING, ID_CHANGE_PASS, "Change Password");
				menu.AppendMenu(MF_STRING, ID_DELETE_USER, "Delete ID");
				menu.TrackPopupMenu(TPM_LEFTALIGN + TPM_RIGHTBUTTON, rect.left + rc.left+20, rect.top + rc.top+10, this);
			}
		pMsg->wParam = NULL;
	}
	return CListBox::PreTranslateMessage(pMsg);
}


void CUserListBox::OnSelchange() 
{
	RedrawWindow();
}

int CUserListBox::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListBox::OnCreate(lpCreateStruct) == -1)	return -1;
	else											return  0;
}

CString CUserListBox::GetPassword( CString strID ) 
{
	CWinApp *pApp		= AfxGetApp();
	CString strPassword;
	strPassword = pApp->GetProfileString( m_PassSection, strID, _T("password") );
	return strPassword;
}

void CUserListBox::DisplayMessageBox ( int iStringID , int iStringTitle)
{
    CBasicDlg		BasicDlg;
	CString			strMessage;
	if ( iStringTitle == 0 ) {
		strMessage.LoadString(iStringID);
		MessageBox(strMessage);
	} else {
		BasicDlg.m_DlgTitle = GetString (iStringTitle);
		BasicDlg.m_stState = GetString (iStringID);
		BasicDlg.DoModal();	
	}
}

void CUserListBox::DisplayMessageBox ( CString strStringID , CString strStringTitle)
{
    CBasicDlg		BasicDlg;
	if ( strStringTitle == "" ) {
		MessageBox(strStringID);
	} else {
		BasicDlg.m_DlgTitle = strStringID;
		BasicDlg.m_stState = strStringTitle;
		BasicDlg.DoModal();	
	}
}

CString CUserListBox::GetString ( int iStringID )
{
	CString			strMessage;
	strMessage.LoadString(iStringID);
	return strMessage;
}