//=======================================================
//==              MainFrm.cpp
//=======================================================
//  파 일 명 :  MainFrm.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LES 의 메인 프레임 을 생성 한다. 
//
//=======================================================

#include "stdafx.h"
#include "LES.h"
#include "MainFrm.h"
#include <direct.h>
#include "PasswordDlg.h"
#include "TrackBlockDlg.h"
#include "BasicDlg.h"
#include "LESView.h"
#include "TimesetDlg.h"
#include "SendTrainNumber.h"
#include "AllUserDlg.h"
#include "ControlDlg.h"
#include "MessageSendDlg.h"
#include "Splash1.h"
#include "../include/JTime.h"
#include "../Include/OPMsgCode.h"
#include "GPSComm.h"				//GPS추가.

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int _nOldAlter = 0;
extern int _iLCCNo;

//=======================================================
//
//  함 수 명 :  GetOSVersionType
//  함수출력 :  int 
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  OS 의 버전을 리턴한다.
//
//=======================================================
static int WINAPI GetOSVersionType() 
{
	UINT nOSVersion;
	OSVERSIONINFOEX osvi;
	BOOL bOsVersionInfoEx;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	
	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
	{
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) return -1;
	}
	
	switch (osvi.dwPlatformId)
	{
	case VER_PLATFORM_WIN32_NT:
		if ( osvi.dwMajorVersion <= 4 )
			nOSVersion = 4;
		else if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
			nOSVersion = 5;
		else if( bOsVersionInfoEx )  
			nOSVersion = 6;
		break;
	case VER_PLATFORM_WIN32_WINDOWS:
		if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
			nOSVersion = 1;
		else if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
			nOSVersion = 2;
		else if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
			nOSVersion = 3;
		break;
	}
	
	return nOSVersion; 
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_COMMAND(ID_RUN, OnRun)
	ON_COMMAND(ID_RESET, OnReset)
	ON_COMMAND(ID_TIMESET, OnTimeset)
	ON_COMMAND(ID_MESSAGESEND,OnMessageSend)
	ON_COMMAND(ID_EXIT, OnExit)
	ON_COMMAND(ID_ALLUSER, OnAlluser)
	ON_COMMAND(ID_CHANGE, OnChangeUser)
	ON_COMMAND(ID_LOCK, OnLockUnLock)
	ON_COMMAND(ID_SECTION, OnSectionRelease)
	ON_COMMAND(ID_BLOCKING, OnBlockingTrack)
	ON_COMMAND(ID_SLS, OnSls)
	ON_COMMAND(ID_ATB, OnAtb)
	ON_COMMAND(ID_ND, OnNd)
	ON_COMMAND(ID_ALARM, OnAlarm)
	ON_COMMAND(ID_BUZZER, Onbuzzer)
	ON_COMMAND(ID_TRAINNUMBER, OnSendTrainNumber)
	ON_COMMAND(ID_EPK_CONTROL, OnSelectEPK)
	ON_COMMAND(ID_LC_CONTROL, OnSelectLC)
	ON_COMMAND(ID_TOKENLESS_BLOCK, OnSelectBlock)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_BTN_SCROLL, OnBtnScroll)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_F1,  OnF1)
	ON_MESSAGE(WM_F2,  OnF2)
	ON_MESSAGE(WM_F3,  OnF3)
	ON_MESSAGE(WM_F4,  OnF4)
	ON_MESSAGE(WM_F5,  OnF5)
	ON_MESSAGE(WM_F6,  OnF6)
	ON_MESSAGE(WM_F7,  OnF7)
	ON_MESSAGE(WM_F8,  OnF8)
	ON_MESSAGE(WM_F9,  OnF9)
	ON_MESSAGE(WM_F10, OnF10)
	ON_MESSAGE(WM_F11, OnF11)
	ON_MESSAGE(WM_F12, OnF12)
	ON_MESSAGE(WM_AUTOEXIT,OnAutoExit)
	ON_MESSAGE(WM_AUTOEXITEX,OnAutoExitEx)
	ON_MESSAGE(WM_GPS_COMM_RECEIVE_MSG, OnReceiveGPSMsg)		//GPS추가.
	ON_MESSAGE(WM_SHARED_MEMORY_UPDATED, OnSharedMemoryUpdated)
END_MESSAGE_MAP()

//=======================================================
//
//  함 수 명 :  CMainFrame()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CMainFrame::CMainFrame()
{
	m_bCloseWindow			= TRUE;
	pApp					= (CLESApp*) AfxGetApp();
	m_bSoundOpen			= FALSE;
	m_bSystemRunOn			= FALSE;
	m_bSystemRunOff			= FALSE;
	m_bExitProgram			= FALSE;
	m_bLogWindowDisable		= FALSE;
	m_bOperateDisable		= FALSE;
	m_bIsCreateLOGView		= FALSE;
	m_bIsCreateMenuView		= FALSE;
	m_iMenuViewExitCk		= 1;
	m_iLogViewExitCk		= 1;
	m_bDlgDisplaed			= FALSE;
	m_iConsoleRunCounter	= 10;
	m_iCountForLogInDlg		= 0;
	m_iCountForSendGPStime	= 0;

	m_CommStatus.pVar[0]	= 0x00;
	m_CurrentSound			= 0x00;
	m_OldSound				= 0x00;

	m_pMenuRoute[0].pRMenu	= NULL;
	m_pMenuRoute[1].pRMenu	= NULL;
	m_pMenuRoute[2].pRMenu	= NULL;
	m_pMenuSwitch.pIMenu	= NULL;
	m_pMenuTrack.pIMenu		= NULL;

	m_pMenuRoute[0].nMax	= 0;
	m_pMenuRoute[1].nMax	= 0;
	m_pMenuRoute[2].nMax	= 0;
	m_pMenuSwitch.nMax		= 0;
	m_pMenuTrack.nMax		= 0;

	m_hmodHookTool			= NULL;
	m_hSubWinMap			= NULL;
	m_pCmdXBuffer			= NULL;
	m_pWinXBuffer			= NULL;
	m_bMessageSendCk        = TRUE;
	m_iMessageReceiveCnt	= MAX_MSGCNT+100;

	m_iResetTimer = 0;
	m_pSMSyncThread			= NULL;
}

//=======================================================
//
//  함 수 명 :  ~CMainFrame()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CMainFrame::~CMainFrame()
{
}

//=======================================================
//
//  함 수 명 :  OnCreate
//  함수출력 :  int
//  함수입력 :  LPCREATESTRUCT lpCreateStruct
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  초기 생성시 프레임 을 설정 한다.
//
//=======================================================
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon;; 

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1) return -1;
	//Get Current  Time
	m_PCTime = CJTime::GetCurrentTime();

	//Set Window Maintitle.
	SetWindowText( pApp->psz_MainTitle );	// set title string to the caption bar.
	
	//Set Icon of application instead of default icon.
	hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	
	SetIcon(hIcon, FALSE);		// Set small icon
	
	//PosiotionFix();

//	SetTimer(1100,10000,NULL); // 10 초후 후킹을 시작한다. ( 후킹 시작시 에러가 너무 많이 발생함 안전을 위해서 10초뒤 후킹 시작 )

//	if(StartUp() == FALSE)
//	{
//		::MessageBox(GetSafeHwnd(), "HookTool.Dll or DLL internal function loading Failed!", "DLL loading failed", MB_OK);
//		MoveWindow(0, 0, 0, 0);
//		PostQuitMessage(0);
//	}

//	m_bStartHook = TRUE;
//	m_nOSVersionType = GetOSVersionType();
//	m_pfnInstallHook(this->m_hWnd, m_nOSVersionType);

//	m_pfnSetHookOption( 1000 , TRUE , 1);	
	m_iStartBit = 0;
	return 0;
}

//=======================================================
//
//  함 수 명 :  PreCreateWindow
//  함수출력 :  BOOL
//  함수입력 :  CREATESTRUCT& cs
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 생성전 전처리 함수 
//
//=======================================================
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) ) return FALSE;

	if ( pApp->m_SystemValue.bTestMode != TRUE ) cs.dwExStyle = WS_EX_TOPMOST;
	cs.style = WS_BORDER | WS_VISIBLE | WS_POPUP;
    cs.x = pApp->m_SystemValue.nMainStartX;
	cs.y = pApp->m_SystemValue.nMainStartY;
	cs.cx = pApp->m_SystemValue.nMainSizeX;
	cs.cy = pApp->m_SystemValue.nMainSizeY;

	if( cs.hMenu ) 
	{
		DestroyMenu( cs.hMenu );
		cs.hMenu = NULL;
	}
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

//=======================================================
//
//  함 수 명 :  PosiotionFix()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LES 의 윈도우 위치를 고정한다.
//
//=======================================================
void CMainFrame::PosiotionFix()
{
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left   = pApp->m_SystemValue.nMainStartX;	
	iWinPlacement.rcNormalPosition.top    = pApp->m_SystemValue.nMainStartY;	
	iWinPlacement.rcNormalPosition.right  = pApp->m_SystemValue.nMainSizeX;	
	iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMainSizeY;	
	SetWindowPlacement(&iWinPlacement);	
}

//=======================================================
//
//  함 수 명 :  OnSize
//  함수출력 :  없음
//  함수입력 :  UINT nType, int cx, int cy
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 사이즈 변화시 처리 함수 
//
//=======================================================
void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
}

//=======================================================
//
//  함 수 명 :  OnMove
//  함수출력 :  없음
//  함수입력 :  int x, int y
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 이동시 처리함수 
//
//=======================================================
void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);
}

//=======================================================
//
//  함 수 명 :  OnDestroy()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  종료시 처리를 한다.
//
//=======================================================
void CMainFrame::OnDestroy() 
{
	if(m_pSMSyncThread != NULL)
	{
		m_pSMSyncThread->End();

		delete m_pSMSyncThread;
	}

	OnAllStopSound();
	m_bExitProgram = TRUE;
	m_bSystemRunOff = TRUE;
	CommClose();
	
	CFrameWnd::OnDestroy();
	
	if ( m_pMenuRoute[0].pRMenu ) 
	{
		delete [] m_pMenuRoute[0].pRMenu;
		m_pMenuRoute[0].pRMenu = NULL;
	}

	if ( m_pMenuRoute[1].pRMenu ) 
	{
		delete [] m_pMenuRoute[1].pRMenu;
		m_pMenuRoute[1].pRMenu = NULL;
	}

	if ( m_pMenuRoute[2].pRMenu ) 
	{
		delete [] m_pMenuRoute[2].pRMenu;
		m_pMenuRoute[2].pRMenu = NULL;
	}
	
	if ( m_pMenuSwitch.pIMenu ) 
	{
		delete [] m_pMenuSwitch.pIMenu;
		m_pMenuSwitch.pIMenu = NULL;
	}
	
	if ( m_pMenuTrack.pIMenu ) 
	{
		delete [] m_pMenuTrack.pIMenu;
		m_pMenuTrack.pIMenu = NULL;
	}
	
	m_pMenuRoute[0].nMax  = 0;
	m_pMenuRoute[1].nMax  = 0;
	m_pMenuRoute[2].nMax  = 0;
	m_pMenuSwitch.nMax	  = 0;
	m_pMenuTrack.nMax	  = 0;

// 	if ( m_LOGView ) 
// 	{
// 		m_LOGView.DestroyWindow();
// 	}

	if ( m_MenuView ) 
	{
		m_MenuView.DestroyWindow();
	}

	pApp->m_RecordMaskInfo.Destroy();

	pApp->SystemValueSave();

	m_pfnUnInstallHook();	

	if (m_hmodHookTool) ::FreeLibrary( m_hmodHookTool );

	if ( m_pCmdXBuffer ) // 시뮬레이션 프로그램 처리
	{		
		UnmapViewOfFile( m_pCmdXBuffer );
	}

	if ( pApp->m_SystemValue.bTestMode != TRUE ) 
	{
/*		HANDLE hToken;
		TOKEN_PRIVILEGES tkp;
		if ( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
		{        
			LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
			tkp.PrivilegeCount = 1;
			tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
			AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);
			::ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, NULL);
		}*/
	}
}

//=======================================================
//
//  함 수 명 :  OnCommand
//  함수출력 :  BOOL
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  외부로부터 메세지를 받았을때의 처리
//
//=======================================================
BOOL CMainFrame::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	COMCommandType *DestTable;
	UINT commandID = (UINT)wParam;
	
	switch (commandID) 
	{
	case ID_COMM_ERRMSG:
		OnCommErrDisplay();
		return TRUE;
	case ID_COMM_WATCH:			
		SetTimer(600,5000,NULL);
		DestTable = (COMCommandType *)lParam;
		CommWatch( DestTable->pData, DestTable->Port );
		return TRUE;
		
	case ID_COMM_CMD_WRITE:	
		CommCommand( (BYTE*)lParam );
		if ( pApp->m_SystemValue.bSharedMemory == TRUE ) {
			OnSetVMem( &CommVMemData[0], pApp->nComRecordSize);
		}
		*((BYTE*)lParam) = 0x00;
		return TRUE;
		
	case ID_COMM_FAIL:		
		OnSetVMemCommFail();
		return TRUE;

	case ID_COMM_PRINTMSG :
// 		DestTable = (COMCommandType *)lParam;
// 		OnMessagePrint( DestTable->pData, DestTable->Port );
		return TRUE;

	default:
		break;
	}
	
	return CFrameWnd::OnCommand(wParam, lParam);
}

//=======================================================
//
//  함 수 명 :  CommOpen
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  통신포트를 연다. ^^
//
//=======================================================
BOOL CMainFrame::CommOpen()
{
	CommClose();

	int nRet = 0;

	int nPort1	 = pApp->m_SystemValue.nComport1;
	int nPort2	 = pApp->m_SystemValue.nComport2;
	BOOL bPort1Ok =TRUE;
	BOOL bPort2Ok =TRUE;

	m_nBaudRate = pApp->m_SystemValue.nBaudrate;

	if ( nPort1 ) 
	{
		_classComm1.SetComPort(nPort1, m_nBaudRate, _classComm1.m_bytesize, _classComm1.m_stop, _classComm1.m_parity);
		_classComm1.CreateCommInfo();
		if ( _classComm1.OpenComPort((CView*)this) ) 
		{
			nRet |= 1;
			_classComm1.SetLimitTick( LIMIT_COM_FAILTICK, LIMIT_COM_PASTICK );	
		}
		else 
		{
			bPort1Ok = FALSE;
		}
	}

	if ( nPort2 ) 
	{
		_classComm2.SetComPort(nPort2, m_nBaudRate, _classComm2.m_bytesize, _classComm2.m_stop, _classComm2.m_parity);
		_classComm2.CreateCommInfo();
		if ( _classComm2.OpenComPort((CView*)this) ) 
		{
			nRet |= 2;
			_classComm2.SetLimitTick( LIMIT_COM_FAILTICK, LIMIT_COM_PASTICK ); 
		}
		else 
		{
			bPort2Ok = FALSE;
		}
	}

	if ( bPort1Ok == FALSE && bPort2Ok == FALSE ) {
			CString Msg;
			Msg = "COM Port Error!\nCheck Wiring status of communication Port and Device!";
			AfxMessageBox(Msg,MB_ICONSTOP);	
	}

	if ( nRet ) {
		m_nTimerID = SetTimer(ID_TIMER, TIMER_MAX_INTRCOUNT, NULL);		// 200 ms
	}

	SetCommQue(0x00, NULL, 0);

	return((nRet == 0) ? FALSE : TRUE);
}

//=======================================================
//
//  함 수 명 :  CommEthOpen
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :
//  작성날짜 :
//  버    전 :
//  설    명 :  Ethernet 통신포트를 연다.
//
//=======================================================
BOOL CMainFrame::CommEthOpen()
{

	int nEthPort = 1312;

// 	if ( pApp->m_SystemValue.nMode == TRC_COMMUNICATION )
// 		nEthPort += pApp->m_SystemValue.nTRCID;
// 	if ( pApp->m_SystemValue.nMode == 1)
// 	{
// 		int n;
// 	}
// 	else if ( pApp->m_SystemValue.nMode == 2)
// 	{
// 		int nEthPort = 1312;
// 	}

	m_pEthComm1.CreateSocket(m_hWnd, pApp->m_SystemValue.strNet1Local, nEthPort, pApp->m_SystemValue.strNet1PriCBI, pApp->m_SystemValue.strNet1SecCBI);
	m_pEthComm2.CreateSocket(m_hWnd, pApp->m_SystemValue.strNet2Local, nEthPort, pApp->m_SystemValue.strNet2PriCBI, pApp->m_SystemValue.strNet2SecCBI);

	// 	return Create(m_nTRCPort, SOCK_DGRAM, FD_READ|FD_WRITE, m_strTRCIP);

	SetCommQue(0x00, NULL, 0);

	return TRUE;
}

//=======================================================
//
//  함 수 명 :  GPSCommOpen
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  
//  작성날짜 :  2012-08-31
//  버    전 :  
//  설    명 :  GPS 통신포트를 연다.
//
//=======================================================
BOOL CMainFrame::GPSCommOpen()								//GPS추가.
{
	m_pSerialComm = NULL;
	CString	strPort = pApp->m_SystemValue.strGPSComport;
	UINT	nBaudRate, nParity, nDataBit, nStopBit;
	BOOL	bFlowCtrl;
	int		nGPSPortOK;
	
	nBaudRate = 9600;
	nParity = 0;
	nDataBit = 8;
	nStopBit = 1;
	bFlowCtrl = FALSE;
	
	if(m_pSerialComm != NULL)
	{
		m_pSerialComm->End();	
	}
	
	m_pSerialComm = (CGPSComm*)AfxBeginThread(RUNTIME_CLASS(CGPSComm), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	
	if(m_pSerialComm->Start(strPort, nBaudRate, nParity, nDataBit, nStopBit, bFlowCtrl) == FALSE)
	{
		m_pSerialComm->End();
		m_pSerialComm = NULL;
		nGPSPortOK = FALSE;
		
		AfxMessageBox("Connection with serial device for GPS is failed.");
		//		AfxMessageBox("시리얼 장치 연결에 실패하였습니다.");
	}
	else
	{
		nGPSPortOK = TRUE;
	}
	
	
	UpdateData(TRUE);
	
	return nGPSPortOK;
}

//=======================================================
//
//  함 수 명 :  CommClose
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  통신 포트를 닫는다.
//
//=======================================================
void CMainFrame::CommClose()
{
	if ( m_nTimerID ) 
	{
		KillTimer(ID_TIMER);
		m_nTimerID = 0;
	}

	if ( _classComm1.m_bConnected )
	{
		_classComm1.DestroyComm();
		Sleep(200);
	}

	if ( _classComm2.m_bConnected )
	{
		_classComm2.DestroyComm();
		Sleep(200);
	}
}

//=======================================================
//
//  함 수 명 :  SetCommQue
//  함수출력 :  BOOL
//  함수입력 :  BYTE Cmd, BYTE *pData, int len
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  
//
//=======================================================
BOOL CMainFrame::SetCommQue(BYTE Cmd, BYTE *pData, int len)
{

	
	CLESApp* pApp = (CLESApp*)AfxGetApp();
	BOOL bCmd;

	int  i;
	int  n = 1;
	BOOL bComState = FALSE;

	m_nCmdTick = ( Cmd ) ? 1 : 0;
	if ( Cmd ) 
	{
		m_CmdCycle++;
		bCmd = TRUE;
	} else {
	    bCmd = FALSE;
	}

	m_CmdCycle &= 0x0f;
	BYTE stx = 0x40 + m_CmdCycle;	// '@'
	m_CommCmd.OperateData[n++] = Cmd;
	
	if ( Cmd == WS_OPMSG_USERID )
		memcpy(&m_CommCmd.RealUserData.UserData[0],pData,USER_CMD_SIZE);
	else
		memset(&m_CommCmd.RealUserData.UserData[0], 0, USER_CMD_SIZE);

	if ( Cmd ) // Exist Cmd
	{	
		for(i=0; i<len; i++) 
		{
			if ( pData )	m_CommCmd.OperateData[n++] = *pData++;			// cmd info- data.
			else			m_CommCmd.OperateData[n++] = 0x00; 
		}
	} 
	else // None Cmd
	{			
		m_CommCmd.OperateData[n++] = m_CommStatus.pVar[0];	// Opt status.
		len = 1;
	}
	for(; len < MAX_OPERATE_CMD_SIZE; len++)
	{
		m_CommCmd.OperateData[n++] = 0x00;
	}
	if (Cmd == 'D') stx = COM_STX;

	memset(m_CommCmd.CurrentUserName, 0, MAX_USER_SIZE);
	memset(m_CommCmd.CurrentUserPass, 0, MAX_PASS_SIZE);

	if ( pApp->m_SystemValue.nMode /*pApp->m_SystemValue.nTRCID*/ )
	{
// 		if ( m_pEthComm1.m_nCmdCounter > 3 /*|| bCmd == TRUE*/ )
		bComState = m_pEthComm1.SendData( &m_CommCmd.pSendData[0], MAX_OPERATE_CMD_SIZE, stx, bCmd);
		bComState = m_pEthComm2.SendData( &m_CommCmd.pSendData[0], MAX_OPERATE_CMD_SIZE, stx, bCmd);
		
		if(pApp->m_bCommDisplay == TRUE)
		{
			m_MenuView.AddDataLog(FALSE, MAX_OPERATE_CMD_SIZE, (BYTE*)&m_CommCmd.pSendData[0]);
		}
	}
	else
	{
		if ( _classComm1.m_bConnected ) 
		{
// 			if ( _classComm1.m_nCmdCounter > 3 /*|| bCmd == TRUE*/ )
				bComState = _classComm1.SendData( &m_CommCmd.pSendData[0], MAX_OPERATE_CMD_SIZE, stx ,bCmd);			
		} 
		if ( _classComm2.m_bConnected ) 
		{
// 			if ( _classComm2.m_nCmdCounter > 3 /*|| bCmd == TRUE*/ )
				bComState = _classComm2.SendData( &m_CommCmd.pSendData[0], MAX_OPERATE_CMD_SIZE, stx ,bCmd);
		}

		if((_classComm1.m_bConnected || _classComm2.m_bConnected) && (pApp->m_bCommDisplay == TRUE))
		{
			m_MenuView.AddDataLog(FALSE, MAX_OPERATE_CMD_SIZE, (BYTE*)&m_CommCmd.pSendData[0]);
		}
	}


	if ( pApp->m_SystemValue.bSharedMemory == TRUE ) 
	{
		if (shared.SharedControlAddress)
		{
			memcpy ((LPSTR)shared.SharedControlAddress, &m_CommCmd.pSendData[0], MAX_OPERATE_CMD_SIZE);
		}
	}

	
	return bComState;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::CommWatch(BYTE *pData, int nPort)
{
	DataHeaderType		*pHeader;
	SystemStatusType	*pSysVar;
//	SystemStatusType    *sysInfo;
	short length = pApp->nComRecordSize;
	BYTE  nOpCycle, sendCycle;
	BYTE  func = *pData++;
//	CString strStatus;

	switch (func) {
	case 'D':		// data block
	case 0x1D:		// data block
	case 0x2D:		// data block
	case 0x3D:		// data block
	case 0x1F:
	case 0x2F:
	case 0x3F:
	case 0x4F:
	case 0x5F:
	case 0x6F:
		pHeader = (DataHeaderType*)pData;
		pSysVar = &pHeader->nSysVar;
//	sysInfo = (SystemStatusType *)&(pHeader->nSysVar);
 		switch ( nPort ) {
		case 1 : 
		case 11 :
		case 12 :
		case 13 :
		case 14 :
		case 15 :
		case 16 :
				m_CommStatus.bLCCCom1Fail = 0;
				break;
		case 2 : m_CommStatus.bLCCCom2Fail = 0;
				break;
		}

		if (m_DataCycle != pHeader->Cycle) 
		{
			memcpy(CommVMemData, pData, length ); 

			nOpCycle = CommVMemData[pApp->nSfmRecordSize];
			if ( (nOpCycle >= 0x40) && (nOpCycle <= 0x4f) ) // Cmd STX
			{
				if ( nOpCycle == m_RecieveCycle) 
				{
					CommVMemData[pApp->nSfmRecordSize]   = 0x00;		//	   cycle
					CommVMemData[pApp->nSfmRecordSize+1] = 0x00;		//	   Func
				}
				else 
				{
					m_RecieveCycle = nOpCycle;
				}
				
				sendCycle = 0x40 + m_CmdCycle;
				
				if (!m_nCmdTick || (pData[pApp->nSfmRecordSize+1] && (pData[pApp->nSfmRecordSize] == sendCycle))) SetCommQue(0x00, NULL, 0);
			}
			
			OnSetVMem( &CommVMemData[0], length );

//			if ( pApp->m_SystemValue.bTestMode == TRUE ) {			
//				strStatus.Format("Up : %d , Mid : %d , Dn %d",sysInfo->bUpModem,sysInfo->bMidModem,sysInfo->bDnModem);
//				m_LOGView.m_listBox.AddString(strStatus);
//			}


		}
		else 
		{ 
			if ( !m_nCmdTick ) 
			{
				SetCommQue(0x00, NULL, 0);	// Clear Cmd.
			}
		}

		m_DataCycle = pHeader->Cycle; // 
		break;
	case 'P':
		break;
	default:
		break;
	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::CommCommand(BYTE *pData)
{
	BYTE  func = *pData++;
	short len  = *(short*)pData;
	pData += sizeof(short);

	if ( pApp->m_SystemValue.nSystemNo == 3 )
		return;
	if ( !ConfirmOperation( (short*)pData, func ) )  return;

	if ( func == WS_OPMSG_PAS_IN || func == WS_OPMSG_PAS_OUT ) func = WS_OPMSG_PAS;
	if ( func == WS_OPMSG_LOS_ON || func == WS_OPMSG_LOS_OFF ) func = WS_OPMSG_LOS;
	if ( func == WS_OPMSG_POS_ON || func == WS_OPMSG_POS_OFF ) func = WS_OPMSG_NOUSE;
	if ( func == WS_OPMSG_CBBOPEMER ) func = WS_OPMSG_CBBOP;
	if ( func == WS_OPMSG_UTG_CA || func == WS_OPMSG_UTC_CA || func == WS_OPMSG_DTG_CA || func == WS_OPMSG_DTC_CA ) func = WS_OPMSG_CAON;
	if ( func == WS_OPMSG_UTG_LCR || func == WS_OPMSG_UTC_LCR || func == WS_OPMSG_DTG_LCR || func == WS_OPMSG_DTC_LCR ) func = WS_OPMSG_LCROP;
	if ( func == WS_OPMSG_UTG_CBB || func == WS_OPMSG_UTC_CBB || func == WS_OPMSG_DTG_CBB || func == WS_OPMSG_DTC_CBB ) func = WS_OPMSG_CBBOP;
	SetCommQue(func, pData, len);
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnSetVMemCommFail()
{
	CLESView* pView = (CLESView*) GetActiveView();
	union 
	{
		BYTE d;
		struct 
		{
			BYTE dl : 4;
			BYTE dh : 4;
		};
	};

	CJTime T;
	DataHeaderType *poldHeader = (DataHeaderType*)&(pApp->SystemVMEM[0]);
	DataHeaderType *pnewHeader = (DataHeaderType*)&CommVMemData[0];

	memcpy(pnewHeader, poldHeader, pApp->nComRecordSize );

	T = CJTime::GetCurrentTime();
	dl = T.nSec % 10;
	dh = T.nSec / 10;
	pnewHeader->nSec   = d;
	dl = T.nMin % 10;
	dh = T.nMin / 10;
	pnewHeader->nMin   = d;
	dl = T.nHour % 10;
	dh = T.nHour / 10;
	pnewHeader->nHour  = d;
	dl = T.nDate % 10;
	dh = T.nDate / 10;
	pnewHeader->nDate  = d;
	dl = T.nMonth % 10;
	dh = T.nMonth / 10;
	pnewHeader->nMonth = d;
	dl = T.nYear % 10;
	dh = T.nYear / 10;
	pnewHeader->nYear  = d;

	pnewHeader->Cycle = ++m_DataCycle;
	OnSetVMem( &CommVMemData[0], pApp->nComRecordSize, FALSE );
	SetCommQue(0x00, NULL, 0);	// Cmd Clear.
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnTimer(UINT nIDEvent) 
{
	CLESView* pView = (CLESView*) GetActiveView();
	DataHeaderType *pHeader = (DataHeaderType*)&(pApp->SystemVMEM[0]);
	
	if ( nIDEvent == m_nTimerID ) 
	{
		if ( pView ) 
		{
			BOOL b1Fail = _classComm1.CheckCommTick();
			BOOL b2Fail = _classComm2.CheckCommTick();

			// 통신 상태 
			if ( _classComm1.GetCommFail() ) m_CommStatus.bLCCCom1Fail = 1;
			m_LccCommStatus.bLCC1CtrlLock = pApp->m_SystemValue.bCtrlLock;
			
			if ( b1Fail && b2Fail ) 
			{
				m_LccCommStatus.bLCC1CtrlLock = 1;
			}
		} 

		if ( m_nCmdTick ) 
		{
			m_nCmdTick++;
			if (m_nCmdTick > 20) // 제어 명령 시간 검사
			{				
				SetCommQue(0x00, NULL, 0); // 제어 명령 CLEAR.
			}
		}

	} 
	else if ( nIDEvent == 100 )
	{
	} 
	else if ( nIDEvent == 200 )
	{
	} else if ( nIDEvent == 300 )
	{
	} 
	else if ( nIDEvent == 400 )
	{
	} 
	else if ( nIDEvent == 500 ) // 로그 디스플레이 테스트 
	{ 
	} 
	else if ( nIDEvent == 600 ) // 통신 끊겼을때 
	{ 
			KillTimer(600);
			memset(pApp->SystemVMEM, 0 , sizeof(pApp->SystemVMEM));
			pApp->m_bCommOk = FALSE;
			pApp->m_bCommSuccess = FALSE;
			pView->m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, 0, (long)(&pApp->SystemVMEM[0]) );

			if ( pApp->m_SystemValue.nSystemNo != 3)
				PostMessage(WM_COMMAND,ID_CHANGE);
} 
/*	else if ( nIDEvent == 700 ) // 통신 끊겼을때 
	{ 
//		if ( (pHeader->nSysVar.bLcc1Online == 1 && pHeader->nSysVar.bLcc2Online == 1) ||  pApp->m_SystemValue.bTestMode == TRUE ) 
		if ( (pHeader->LCCStatus.bLCC1Alive == 1 && pHeader->LCCStatus.bLCC2Alive == 1) ||  pApp->m_SystemValue.bTestMode == TRUE ) 
		{
				pApp->m_SystemValue.bLanUse = TRUE;
				pView->StartTimer();
		} 
		else 
		{
			pApp->m_SystemValue.bLanUse = FALSE;		
		}
	} */																//LAN통신 사용안함.
	else if ( nIDEvent == 800 ) // 로그 디스플레이 테스트 
	{ 
		//OnSetVMem( &CommVMemData[0], pApp->nComRecordSize);
	}
	else if ( nIDEvent == 900 ) // 로그 디스플레이 테스트 
	{ 
		MessageSend();
	}
	else if ( nIDEvent == 1000 ) // 메세지 전송후 1초간 메세지 전송 금지
	{
		KillTimer ( 1000 );
		m_bMessageSendCk = TRUE;
	}
// 	else if ( nIDEvent == 1100 ) // 후킹 DLL 로딩 및 후킹 시작
// 	{
// 		KillTimer (1100);
// 		if(StartUp() == FALSE)
// 		{
// 			::MessageBox(GetSafeHwnd(), "HookTool.Dll or DLL internal function loading Failed!", "DLL loading failed", MB_OK);
// 			MoveWindow(0, 0, 0, 0);
// 			PostQuitMessage(0);
// 		}
// 
// 		m_bStartHook = TRUE;
// 		m_nOSVersionType = GetOSVersionType();
// //		m_pfnInstallHook(this->m_hWnd, m_nOSVersionType);
// 
// //		m_pfnSetHookOption( 2000 , TRUE , 1);	
// 	}
	CFrameWnd::OnTimer(nIDEvent);
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  LSM 에서 받았을때 
//
//=======================================================
BOOL CMainFrame::ConfirmOperation(short *pData, BYTE Cmd)
{
//	CPasswordDlg pDlg;			//멤버변수로 대체.
	CTrackBlockDlg	tDlg;
	CBasicDlg dlg;
	m_LogInDlg.m_bIsLock = TRUE;

	CString TrackComment;
	short x, y;
	int i,nCmd;
	short *pd = pData;
	
	union 
	{
		short tID;
		struct 
		{
			BYTE tdl;
			BYTE tdh;
		};
	};

	nCmd	= Cmd;
	tID		= *pData++;
	x		= *pData++;
	y		= *pData;
	
	switch ( Cmd ) {
	case WS_OPMSG_BUTTON:			//10	// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
		return TRUE;
		break;

	case WS_OPMSG_PAS_IN:
		return OnLockUnLock( 1 );
		break;

	case WS_OPMSG_PAS_OUT:
		return OnLockUnLock( 2 );
		break;

	case WS_OPMSG_CBBOPEMER:
		m_LogInDlg.m_DlgTitle = "CBB ON";
		m_LogInDlg.m_stState	= "For CBB On \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;

	case WS_OPMSG_LOS_OFF:
		if ( pApp->m_VerifyObjectForMessage.strStationName == "StandardOf13")
		{
			m_LogInDlg.m_DlgTitle = "LOS OFF";
		}
		else
		{
			m_LogInDlg.m_DlgTitle = "COS OFF";
		}
		m_LogInDlg.m_stState	= "For Loop track On \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;

	case WS_OPMSG_LOS_ON:
		if ( pApp->m_VerifyObjectForMessage.strStationName == "StandardOf13")
		{
			m_LogInDlg.m_DlgTitle = "LOS ON";
		}
		else
		{
			m_LogInDlg.m_DlgTitle = "COS ON";
		}
		m_LogInDlg.m_stState = "For Loop track off \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;
		
	case WS_OPMSG_AXL_RESET:
		m_LogInDlg.m_DlgTitle = "AXLE COUNTER RESET";
		m_LogInDlg.m_stState = "For Axle Counter Reset \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;

	case WS_OPMSG_TRACK_RESET:
		m_LogInDlg.m_DlgTitle = "AXLE COUNTER RESET";
		m_LogInDlg.m_stState = "For Axle Counter Reset \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;

	case WS_OPMSG_POS_OFF:
		m_LogInDlg.m_DlgTitle = "POS OFF";
		m_LogInDlg.m_stState = "Unuse route that flow point. \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK){
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;
	case WS_OPMSG_POS_ON:
		m_LogInDlg.m_DlgTitle = "POS ON";
		m_LogInDlg.m_stState = "Use route that flow point. \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK){
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;
	case WS_OPMSG_RESET:			//2			// 시스템 재기동
		m_LogInDlg.m_DlgTitle = "System  Initialization.";
		m_LogInDlg.m_stState = "For Initialization \nConfirm User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_RESET);	
		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() != IDOK){
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		dlg.hIcon = AfxGetApp()->LoadIcon(IDI_WARN);	
		dlg.m_DlgTitle = "Initialization Reconfirm";
		dlg.m_stState = "Do you really want to initialize?";
		m_bDlgDisplaed = TRUE;
		if(dlg.DoModal() !=IDOK)
		{
			m_bDlgDisplaed = FALSE;
			return FALSE;
		}
		m_bDlgDisplaed = FALSE;
		break;
	case WS_OPMSG_MARKINGTRACK:		//14		// 궤도 사용금지/해제(모타카, 사용금지, 단선) [mode(byte), track ID(byte)]
		for( i=0; i<m_pMenuTrack.nMax; i++) 
		{
			if (tdl == m_pMenuTrack.pIMenu[i].nID )
			{ 
				TrackComment = (m_pMenuTrack.pIMenu[i].Name);
				break;
			}
		}
		tDlg.m_BlockID = tdh;
		tDlg.m_strTrackName = TrackComment;
		m_bDlgDisplaed = TRUE;
		if(tDlg.DoModal() == IDOK)
		{
			m_bDlgDisplaed = FALSE;
			tdh = tDlg.m_BlockID;
			*pd = tID;
		}
		else
		{
			m_bDlgDisplaed = FALSE;
			pApp->m_bBlockTrackIng = FALSE;		
			return FALSE;
		}
		pApp->m_bBlockTrackIng = FALSE;			//궤도 차단 명령 끝남
		break;
	case WS_OPMSG_FREESUBTRACK:		//15		// 구분진로비상해정 [track ID(byte)]
		for( i=0; i<m_pMenuTrack.nMax; i++) 
		{
			if (tdl == m_pMenuTrack.pIMenu[i].nID )
			{ 
				TrackComment = (m_pMenuTrack.pIMenu[i].Name);
				break;
			}
		}
			m_LogInDlg.m_DlgTitle = TrackComment + " Emergency Route Release Control";
			m_LogInDlg.m_stState =  TrackComment + "  \n- Input User ID and Password!"  ;
			m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_QUEST);	
			m_bDlgDisplaed = TRUE;

			if(m_LogInDlg.DoModal() != IDOK)
			{
				m_bDlgDisplaed = FALSE;
				return FALSE;
			}
			m_bDlgDisplaed = FALSE;

		break;
	default:
		return TRUE;
	}
	
	return TRUE;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::ShowLockMessage()
{
	AfxMessageBox("Warning !\nNow, Control Locking Status. Make Unlock,Please!",MB_OK,MB_ICONSTOP);
}

void CMainFrame::OnCommErrDisplay ()
{
	CString strRcvData;
	CLESView* pView = (CLESView*)GetActiveView();

	if ( m_MenuView && pApp->m_bCommDisplay == TRUE) {
		if ( pApp->m_iCommError == 1 ) {
			strRcvData = "CRC Error";
			m_MenuView.AddErrorLog(strRcvData);
		} else if ( pApp->m_iCommError == 2 ) {
			strRcvData = "CBI is now running for standby ( 0x05 )";
			m_MenuView.AddErrorLog(strRcvData);
		}
	}

	if ( pApp->m_iCommError == 2 && pView->m_iSysType != 2) {
		memset(pApp->SystemVMEM, 0 , sizeof(pApp->SystemVMEM));
	}

}

void CMainFrame::SendCurrentTime()
{
	CTime time;
	int iYear,iMonth,iDay,iHour,iMin,iSec;
	time = CTime::GetCurrentTime();

	union 
	{
		BYTE t;
		struct 
		{
			BYTE bl : 4;
			BYTE bh : 4;
		};
	};

	if ( time != -1 ) 
	{
		iYear	= time.GetYear();
		iMonth	= time.GetMonth();
		iDay	= time.GetDay();
		iHour	= time.GetHour();
		iMin	= time.GetMinute();
		iSec	= time.GetSecond();	
	}

	CJTime T( iYear, iMonth , iDay, iHour, iMin, iSec);
	BYTE Buf[16];
	bl = T.nSec % 10;
	bh = T.nSec / 10;
	Buf[0] = t;
	bl = T.nMin % 10;
	bh = T.nMin / 10;
	Buf[1] = t;
	bl = T.nHour % 10;
	bh = T.nHour / 10;
	Buf[2] = t;
	bl = T.nDate % 10;
	bh = T.nDate / 10;
	Buf[3] = t;
	bl = T.nMonth % 10;
	bh = T.nMonth / 10;
	Buf[4] = t;
	bl = T.nYear % 10;
	bh = T.nYear / 10;
	Buf[5] = t;
		
	SetCommQue( WS_OPMSG_SETTIMER, &Buf[0], 6 );
}

//=======================================================
//
//  함 수 명 :  OnSetVMem
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  로깅의 제왕 5 !!
//              로깅... 그 머나먼 여정의 시작점...
//              태초에 하나님이 로깅을 만드셨나니... 
//              한번들어가면 다시나오지 못하는 그곳으로의 여정을...
//              사람들은 로깅어드벤처라 불렀다....
//
//=======================================================
void CMainFrame::OnSetVMem(BYTE *pVMem, int length, BOOL bWrite)
{
	CLESView* pView = (CLESView*)GetActiveView();
	CString strRcvData,strTemp;

	BOOL bTrack, bSignal, bSwitch , bReplayFirstCK;
	BYTE *pSetBuf = NULL;
	BYTE ReplayOption[8];

	union 
	{
		long lParam;
		struct 
		{
			short ndl;
			short ndh;
		};
	};

	if ( m_bExitProgram ) 
	{
		return;
	}

	bReplayFirstCK = FALSE;
	
	// 공유메모리를 사용할경우 공유메모리 의 값을 쓴다. 
	if ( pApp->m_SystemValue.bSharedMemory == TRUE ) {
		// SharedMemory debug...
		if (shared.SharedMemoryAddress != 0)
		{
			memcpy (pVMem, (LPSTR)shared.SharedMemoryAddress, length);
			memcpy (ReplayOption, (LPSTR)shared.SharedMemoryAddress+2039, 4); //2045
			if ( ReplayOption[1] == 1 ) {
				bReplayFirstCK = TRUE;
//				m_LOGView.m_listBox.m_bReplayFirstCK = TRUE;
//				ReplayOption[4] = 1;
				memcpy ((LPSTR)shared.SharedMemoryAddress+2043, &ReplayOption[4], 4);
			} else {
				bReplayFirstCK = FALSE;
//				m_LOGView.m_listBox.m_bReplayFirstCK = FALSE;
//				ReplayOption[4] = 0;
				memcpy ((LPSTR)shared.SharedMemoryAddress+2043, &ReplayOption[4], 4);
			}

//			m_LOGView.m_listBox.AttachFileRecord( pVMem, length);
//			m_EventDlg.AddNewRecord(pVMem, length);
			//memcpy (pVMem, (LPSTR)shared.SharedMemoryAddress, _nComRecordSize);
		}
	}

	pSetBuf = pVMem;
	length += sizeof(CJTime);	//시간 데이터 만큼 데이터 연장.
	DataHeaderType* pHead = (DataHeaderType*)pSetBuf;

	pApp->m_bISCTCMode = pHead->nSysVar.bMode;

	// 공유메모리 모드가 아닌 경우에만 연동장치에서 수신한 시간값에 대한 BCD -> Decimal 변환을 수행한다.
	BOOL bFailTime = 0;
	if ( pApp->m_SystemValue.bSharedMemory == FALSE ) 
	{
		bFailTime = SystemTimeBCDToDecimal( pHead );
	}

	CJTime T = CJTime::GetCurrentTime();
	CJTime *destT = (CJTime*)&pSetBuf[pApp->nSfmRecordSize + LCC_COM_SIZE + IPM_VERSION_SIZE];		// 2->연동장치에서 받을때는 2BYTE 더받음.
	CJTime *pT = (CJTime*)&pHead->nSec;

	if ( !bFailTime ) {
		*destT = T;		// 컴퓨터 시간을 설정 
	}
	else {
		*destT = *pT;	// 시간 데이타가 잘못 왔으므로 
		*pT = T;		// 현재 컴퓨터의 시간으로 설정 
	}

	if ( pHead->nSysVar.bN1 == 1 ) { // bN1 = 1 이면 Lock
		m_bOperateDisable = 1; // PAS 값이 1이면 잠금 상태 
		pApp->m_SystemValue.bCtrlLock = 1;
	} else {
		m_bOperateDisable = 0;
		pApp->m_SystemValue.bCtrlLock = 0;
	}
	OnMenuLockUpdate();

	if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPSYSTEM) ) {
		OnMenuLockUpdate();		//메뉴 Enable or Disable
	}
	
	if ( _iLCCNo == 1 ) m_LccCommStatus.bLCC1CtrlLock = pApp->m_SystemValue.bCtrlLock;
	if ( _iLCCNo == 2 ) m_LccCommStatus.bLCC2CtrlLock = pApp->m_SystemValue.bCtrlLock;

	memcpy(pApp->SystemVMEM, pSetBuf, length);

	int nAlter = 0;

	/*
	if ( pHead->nSysVar.bLcc1Online == 0 && pHead->nSysVar.bLcc2Online == 0 ) {
		if ( pApp->m_SystemValue.bLanUse == FALSE ) {
			pApp->m_SystemValue.bLanUse = TRUE;
			pView->StartTimer();
		}
	} else {
		pApp->m_SystemValue.bLanUse = FALSE;		
	}
	*/

	if ( bWrite ) 
	{
		if ( _iLCCNo == 1 ) m_LccCommStatus.bCom1Online = (m_bSystemRunOn) ? 1 : 0;
		if ( _iLCCNo == 2 ) m_LccCommStatus.bCom2Online = (m_bSystemRunOn) ? 1 : 0;		
		m_bSystemRunOn = m_bSystemRunOff = FALSE;

		// 매시지 및 로깅 데이터 갱신
		nAlter = m_EventDlg.AddNewRecord(pSetBuf, length, bFailTime);

		if(pApp->m_bCommDisplay == TRUE) 
		{
			m_MenuView.AddDataLog(TRUE, length, pApp->SystemVMEM);
		}
	}

	// 기타 처리
	int nXORAlter = _nOldAlter ^ nAlter;

	if ( nAlter || nXORAlter) {
		// 시스템 메뉴 갱신
		BOOL bSysAlter = UpdateMenuByField( pSetBuf );
		if ( m_bIsCreateMenuView ) {
			bTrack  = BOOL((nAlter & 2) || (nXORAlter & 2) || bSysAlter);
			bSignal = BOOL((nAlter & 4) || (nXORAlter & 4) || bSysAlter);
			bSwitch = BOOL((nAlter & 8) || (nXORAlter & 8) || bSysAlter);
			if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPSIGNAL) ) {
				if (bSignal || bTrack) {	// Signal Alternation.
					m_RealLOGMessage.ClearDestTrackID();
					m_RealLOGMessage.RouteItemCheck( m_pMenuRoute[0].pRMenu, m_pMenuRoute[0].nMax );
					m_RealLOGMessage.RouteItemCheck( m_pMenuRoute[1].pRMenu, m_pMenuRoute[1].nMax );
					m_RealLOGMessage.RouteItemCheck( m_pMenuRoute[2].pRMenu, m_pMenuRoute[2].nMax );
				}
			}
			if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPTRACK) ) {
				if ( bTrack ) {	// Track Alternation.
					m_RealLOGMessage.TrackItemCheck( m_pMenuTrack.pIMenu, m_pMenuTrack.nMax );
					if ( pApp->m_SystemValue.bOperate ) {
					}
				}
			}
			if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPSWITCH) ) {
				if ( bSwitch || bTrack ) {	// Switch Alternation.
					m_RealLOGMessage.SwitchItemCheck( m_pMenuSwitch.pIMenu, m_pMenuSwitch.nMax );
				}
			}
		}
		// 변화상태 저장
		_nOldAlter = nAlter;
	}
	// 사운드 처리
	if ( pApp->m_SystemValue.nSystemType == 0 ) 
	{
		if ( pApp->m_SystemValue.bSound ) 
		{
			OnCheckSoundState( pSetBuf );
		}
	}

	// 화면 표시 갱신

	if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPSCREEN) ) 
	{
		pView->PostMessage( WM_COMMAND, ID_COMMAND_UPDATE_VIEW_1, (LPARAM)(length) );
		ndl = (m_bOperateDisable) ? 1 : 0;
		ndh = (pApp->m_SystemValue.bCtrlLock) ? 1 : 0;

		if ( pApp->m_SystemValue.nSystemType == 1 || pApp->m_SystemValue.nSystemType == 2 ) 
		{
			ndl = 1;
			ndh = 1;
		}

		pView->m_Screen.OlePostMessage(WS_OPMSG_OPERATESTATE, 0, lParam);
	}

	// 나중에 한번 확인해 봅시다.... bFailTime // omani 
	if ( !bFailTime  ) // 시간이 잘못되었을경우 CBI 로 MMCR 의 시간을 전송한다. 
	{
		SystemTimeUpdate( &m_PCTime, pT );
	}

	LCCCommDataType *ReceivedDataByCBI = (LCCCommDataType *)&pSetBuf[pApp->nSfmRecordSize];

	if ( m_iConsoleRunCounter && pApp->m_SystemValue.nSystemNo == 3 )
	{
		CheckLCCHaveCurrentUserIDOrNot( (LCCCommDataType *)&ReceivedDataByCBI->pSendData[0] );
// 		if( m_iConsoleRunCounter >= 0 )
// 			m_iConsoleRunCounter--;
// 
// 		if ( m_iConsoleRunCounter == 0)
// 			GPSCommOpen();				//GPS추가.
	}

	pView->m_Screen.SetTNI (0,(LPCSTR) ReceivedDataByCBI->CurrentUserName);

	switch ( ReceivedDataByCBI->RealUserData.UserMsgType )
	{
	case WS_USEROPMSG_CREATE:
		CreateUser( (RealUserDataType *)&ReceivedDataByCBI->RealUserData.UserData[0] );
		break;
	case WS_USEROPMSG_DELETE:
		DeleteUser( (RealUserDataType *)&ReceivedDataByCBI->RealUserData.UserData[0] );
		break;
	case WS_USEROPMSG_USER_CHANGE:
		ChangeToAnotherUser( (RealUserDataType *)&ReceivedDataByCBI->RealUserData.UserData[0] );
		break;
	case WS_USEROPMSG_PASS_CHANGE:
		ChangePass( (RealUserDataType *)&ReceivedDataByCBI->RealUserData.UserData[0] );
		break;
	default:
		break;
	}
	
	if( (!pHead->LCCStatus.bLCC1Active && pApp->m_SystemValue.nSystemNo == 1)
		|| (!pHead->LCCStatus.bLCC2Active && pApp->m_SystemValue.nSystemNo == 2) )
	{
		if ( !m_iCountForLogInDlg)
		{
			if ( pApp->m_bIsSuperUser == FALSE || ReceivedDataByCBI->RealUserData.UserMsgType == WS_USEROPMSG_USER_CHANGE )
				OnChangeUser();

			m_iCountForLogInDlg = 5;
		}
		else
		{
			m_iCountForLogInDlg--;
		}
	}
	//현재 Activate 정보 안주니까 일단 막고 나중에 줄때 풀자..

	// USER ID 전송
//	int iCnt;
/*
	CString strUSERID = "LSIS";//pHead->UserID;
	strUSERID.TrimLeft();
	strUSERID.TrimRight();
	if ( strUSERID != "" ) 
	{
		pApp->m_SystemValue.strCurrentOperator = strUSERID.Left(5);
	} 
	else 
	{
		iCnt = pApp->m_SystemValue.strCurrentOperator.GetLength();
		if ( iCnt > 5 ) iCnt = 5;
		memcpy( &Buf[0] , pApp->m_SystemValue.strCurrentOperator , iCnt );
		SetCommQue( WS_OPMSG_USERID, &Buf[0], 5 );		
	} 
*///////////////////////// 2013.01.28 UserID관리방식 변경.
	
	BYTE Buf[5];
	Buf[0]=Buf[1]=Buf[2]=Buf[3]=Buf[4]='\0';
	
	unsigned short nCount = 0;
	unsigned short nRCount = 0;
	unsigned short nB2AXLCount = 0;
	unsigned short nB4AXLCount = 0;
	
	nCount = (unsigned short) pHead->Counter[0];
	BYTE n1 = nCount % 256;
	BYTE n2 = nCount / 256;
	nRCount = (unsigned short) pHead->Counter[1];
	BYTE n3 = nRCount % 256;
	BYTE n4 = nRCount / 256;
	
	nCount = n1*256 + n2;
	nRCount = n3*256 + n4;

	nB2AXLCount = (unsigned short) pHead->Counter[2];
	BYTE n5 = nB2AXLCount % 256;
	BYTE n6 = nB2AXLCount / 256;
	nB2AXLCount = n5*256 + n6;

	nB4AXLCount = (unsigned short) pHead->Counter[3];
	BYTE n7 = nB4AXLCount % 256;
	BYTE n8 = nB4AXLCount / 256;
	nB4AXLCount = n7*256 + n8;
	
	if (( nCount < pApp->m_SystemValue.nCallonCounter || nRCount < pApp->m_SystemValue.nRouteRelease) 
		&& ( pApp->m_SystemValue.nCallonCounter < 9999 && pApp->m_SystemValue.nRouteRelease < 9999)) 
	{
		if (( nCount < pApp->m_SystemValue.nCallonCounter ) && ( pApp->m_SystemValue.nCallonCounter < 9999 )) 
		{
			memcpy ( &Buf[0] , &pApp->m_SystemValue.nCallonCounter , 2);
		} 
		else 
		{
			memcpy ( &Buf[0] , &nCount , 2);
		}
		
		if (( nRCount < pApp->m_SystemValue.nRouteRelease) && ( pApp->m_SystemValue.nRouteRelease < 9999 )) 
		{
			memcpy ( &Buf[2] , &pApp->m_SystemValue.nRouteRelease , 2);
		} 
		else 
		{
			memcpy ( &Buf[2] , &nRCount , 2);
		}
		
		SetCommQue( WS_OPMSG_COUNTER, &Buf[0], 4 );
	}
	else 
	{
		pApp->m_SystemValue.nCallonCounter = nCount;
		pApp->m_SystemValue.nRouteRelease = nRCount;
		pApp->WriteProfileInt("Counter","Callon",pApp->m_SystemValue.nCallonCounter);
		pApp->WriteProfileInt("Counter","RouteRelease",pApp->m_SystemValue.nRouteRelease);
	}

	if (( nB2AXLCount < pApp->m_SystemValue.nB2AXLCounter || nB4AXLCount < pApp->m_SystemValue.nB4AXLCounter) 
		&& ( pApp->m_SystemValue.nB2AXLCounter < 9999 && pApp->m_SystemValue.nB4AXLCounter < 9999)) 
	{
		if (( nB2AXLCount < pApp->m_SystemValue.nB2AXLCounter) && ( pApp->m_SystemValue.nB2AXLCounter < 9999 )) 
		{
			memcpy ( &Buf[0] , &pApp->m_SystemValue.nB2AXLCounter , 2);
		} 
		else 
		{
			memcpy ( &Buf[0] , &nB2AXLCount , 2);
		}

		if (( nB4AXLCount < pApp->m_SystemValue.nB4AXLCounter) && ( pApp->m_SystemValue.nB4AXLCounter < 9999 )) 
		{
			memcpy ( &Buf[2] , &pApp->m_SystemValue.nB4AXLCounter , 2);
		} 
		else 
		{
			memcpy ( &Buf[2] , &nB4AXLCount , 2);
		}

		SetCommQue( WS_OPMSG_AXL_COUNTER, &Buf[0], 4 );
	}
	else 
	{
		pApp->m_SystemValue.nB2AXLCounter = nB2AXLCount;
		pApp->m_SystemValue.nB4AXLCounter = nB4AXLCount;
		pApp->WriteProfileInt("Counter","B2AXLreset",pApp->m_SystemValue.nB2AXLCounter);
		pApp->WriteProfileInt("Counter","B4AXLreset",pApp->m_SystemValue.nB4AXLCounter);
	}

/////////////////////////////////////////////////////
/////		Counter Reset 기능은 CBI에 이전.	/////
/////////////////////////////////////////////////////
// 	if ( pApp->m_SystemValue.nCallonCounter == 99999 ) // 초기화 
// 	{ 
// 		pApp->m_SystemValue.nCallonCounter = 9999;
// 		pApp->m_SystemValue.nRouteRelease = 9999;
// 		pApp->WriteProfileInt("Counter","Callon",pApp->m_SystemValue.nCallonCounter);
// 		pApp->WriteProfileInt("Counter","RouteRelease",pApp->m_SystemValue.nRouteRelease);			
// 		memcpy ( &Buf[0] , &pApp->m_SystemValue.nCallonCounter , 2);
// 		memcpy ( &Buf[2] , &pApp->m_SystemValue.nRouteRelease , 2);
// 		SetCommQue( WS_OPMSG_COUNTER, &Buf[0], 4 );
// 			
// 	}
// 	else if ( pApp->m_SystemValue.nCallonCounter > 9999 || pApp->m_SystemValue.nRouteRelease > 9999 || nCount > 9999 || nRCount > 9999 )
// 	{
// 		if ( pApp->m_SystemValue.nCallonCounter > 9999 || nCount > 9999) 
// 		{
// 			pApp->m_SystemValue.nCallonCounter = 9999;
// 			memcpy ( &Buf[0] , &pApp->m_SystemValue.nCallonCounter , 2);
// 			memcpy ( &Buf[2] , &pApp->m_SystemValue.nRouteRelease , 2);
// 			SetCommQue( WS_OPMSG_COUNTER, &Buf[0], 4 );		
// 		}
// 
// 		if ( pApp->m_SystemValue.nRouteRelease > 9999 || nRCount > 9999) 
// 		{
// 			pApp->m_SystemValue.nRouteRelease = 9999;
// 			memcpy ( &Buf[0] , &pApp->m_SystemValue.nCallonCounter , 2);
// 			memcpy ( &Buf[2] , &pApp->m_SystemValue.nRouteRelease , 2);
// 			SetCommQue( WS_OPMSG_COUNTER, &Buf[0], 4 );		
// 		}
// 	} 
// 	else 
// 	{
// 		if (( pApp->m_SystemValue.nCallonCounter == 9999 && nCount == 0 ) 
// 			|| ( pApp->m_SystemValue.nRouteRelease == 9999  && nRCount == 0)
// 			|| ( pApp->m_SystemValue.nCallonCounter == 99999 && nCount == 0 && nRCount == 0)) 
// 		{
// 			pApp->m_SystemValue.nCallonCounter = nCount;
// 			pApp->m_SystemValue.nRouteRelease = nRCount;
// 			pApp->WriteProfileInt("Counter","Callon",pApp->m_SystemValue.nCallonCounter);
// 			pApp->WriteProfileInt("Counter","RouteRelease",pApp->m_SystemValue.nRouteRelease);
// 		}
// 		else if (( nCount < pApp->m_SystemValue.nCallonCounter || nRCount < pApp->m_SystemValue.nRouteRelease) 
// 			&& ( pApp->m_SystemValue.nCallonCounter < 9999 && pApp->m_SystemValue.nRouteRelease < 9999)) 
// 		{
// 			if (( nCount < pApp->m_SystemValue.nCallonCounter ) && ( pApp->m_SystemValue.nCallonCounter < 9999 )) 
// 			{
// 				memcpy ( &Buf[0] , &pApp->m_SystemValue.nCallonCounter , 2);
// 			} 
// 			else 
// 			{
// 				memcpy ( &Buf[0] , &nCount , 2);
// 			}
// 			
// 			if (( nRCount < pApp->m_SystemValue.nRouteRelease) && ( pApp->m_SystemValue.nRouteRelease < 9999 )) 
// 			{
// 				memcpy ( &Buf[2] , &pApp->m_SystemValue.nRouteRelease , 2);
// 			} 
// 			else 
// 			{
// 				memcpy ( &Buf[2] , &nRCount , 2);
// 			}
// 			
// 			SetCommQue( WS_OPMSG_COUNTER, &Buf[0], 4 );
// 		}
// 		else 
// 		{
// 			pApp->m_SystemValue.nCallonCounter = nCount;
// 			pApp->m_SystemValue.nRouteRelease = nRCount;
// 			pApp->WriteProfileInt("Counter","Callon",pApp->m_SystemValue.nCallonCounter);
// 			pApp->WriteProfileInt("Counter","RouteRelease",pApp->m_SystemValue.nRouteRelease);
// 		}
// 	}
// 
// 	if ( pApp->m_SystemValue.nB2AXLCounter == 99999 ) // 초기화 
// 	{ 
// 			pApp->m_SystemValue.nB2AXLCounter = 9999;
// 			pApp->m_SystemValue.nB4AXLCounter = 9999;
// 			pApp->WriteProfileInt("Counter","B2AXLreset",pApp->m_SystemValue.nB2AXLCounter);			
// 			pApp->WriteProfileInt("Counter","B4AXLreset",pApp->m_SystemValue.nB4AXLCounter);			
// 			memcpy ( &Buf[0] , &pApp->m_SystemValue.nB2AXLCounter , 2);
// 			memcpy ( &Buf[2] , &pApp->m_SystemValue.nB4AXLCounter , 2);
// 			SetCommQue( WS_OPMSG_AXL_COUNTER, &Buf[0], 4 );		
// 	}
// 
// 	if ( pApp->m_SystemValue.nB2AXLCounter > 9999 || pApp->m_SystemValue.nB4AXLCounter > 9999 || nB2AXLCount > 9999 || nB4AXLCount > 9999 )
// 	{
// 		if ( pApp->m_SystemValue.nB2AXLCounter > 9999 || nB2AXLCount > 9999) 
// 		{
// 			pApp->m_SystemValue.nB2AXLCounter = 9999;
// 			memcpy ( &Buf[0] , &pApp->m_SystemValue.nB2AXLCounter , 2);
// 			memcpy ( &Buf[2] , &pApp->m_SystemValue.nB4AXLCounter , 2);
// 			SetCommQue( WS_OPMSG_AXL_COUNTER, &Buf[0], 4 );		
// 		}
// 
// 		if ( pApp->m_SystemValue.nB4AXLCounter > 9999 || nB4AXLCount > 9999) 
// 		{
// 			pApp->m_SystemValue.nB4AXLCounter = 9999;
// 			memcpy ( &Buf[0] , &pApp->m_SystemValue.nB2AXLCounter , 2);
// 			memcpy ( &Buf[2] , &pApp->m_SystemValue.nB4AXLCounter , 2);
// 			SetCommQue( WS_OPMSG_AXL_COUNTER, &Buf[0], 4 );		
// 		}
// 	} 
// 	else 
// 	{
// 		if (( pApp->m_SystemValue.nB2AXLCounter == 9999 && nB2AXLCount == 0)
// 			|| ( pApp->m_SystemValue.nB4AXLCounter == 9999 && nB4AXLCount == 0)) 
// 		{
// 			pApp->m_SystemValue.nB2AXLCounter = nB2AXLCount;
// 			pApp->m_SystemValue.nB4AXLCounter = nB4AXLCount;
// 			pApp->WriteProfileInt("Counter","B2AXLreset",pApp->m_SystemValue.nB2AXLCounter);
// 			pApp->WriteProfileInt("Counter","B4AXLreset",pApp->m_SystemValue.nB4AXLCounter);
// 		}
// 		else if (( nB2AXLCount < pApp->m_SystemValue.nB2AXLCounter || nB4AXLCount < pApp->m_SystemValue.nB4AXLCounter) 
// 			&& ( pApp->m_SystemValue.nB2AXLCounter < 9999 && pApp->m_SystemValue.nB4AXLCounter < 9999)) 
// 		{
// 			if (( nB2AXLCount < pApp->m_SystemValue.nB2AXLCounter) && ( pApp->m_SystemValue.nB2AXLCounter < 9999 )) 
// 			{
// 				memcpy ( &Buf[0] , &pApp->m_SystemValue.nB2AXLCounter , 2);
// 			} 
// 			else 
// 			{
// 				memcpy ( &Buf[0] , &nB2AXLCount , 2);
// 			}
// 
// 			if (( nB4AXLCount < pApp->m_SystemValue.nB4AXLCounter) && ( pApp->m_SystemValue.nB4AXLCounter < 9999 )) 
// 			{
// 				memcpy ( &Buf[2] , &pApp->m_SystemValue.nB4AXLCounter , 2);
// 			} 
// 			else 
// 			{
// 				memcpy ( &Buf[2] , &nB4AXLCount , 2);
// 			}
// 
// 			SetCommQue( WS_OPMSG_AXL_COUNTER, &Buf[0], 4 );
// 		}
// 		else 
// 		{
// 			pApp->m_SystemValue.nB2AXLCounter = nB2AXLCount;
// 			pApp->m_SystemValue.nB4AXLCounter = nB4AXLCount;
// 			pApp->WriteProfileInt("Counter","B2AXLreset",pApp->m_SystemValue.nB2AXLCounter);
// 			pApp->WriteProfileInt("Counter","B4AXLreset",pApp->m_SystemValue.nB4AXLCounter);
// 		}
// 	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	if(m_bCloseWindow)
	{
		CFrameWnd::OnClose();
	}
	else
	{
		m_bCloseWindow = TRUE;
		return;
	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
BOOL CMainFrame::SystemTimeBCDToDecimal(DataHeaderType *R)
{
	union 
	{
		BYTE d;
		struct 
		{
			BYTE d_l : 4;
			BYTE d_h : 4;
		};
	};

	BOOL bFailTimer = FALSE;
	d = R->nYear;
	R->nYear  = d_h * 10;
	R->nYear += d_l;
	d = R->nMonth;
	R->nMonth  = d_h * 10;
	R->nMonth += d_l;
	d = R->nDate;
	R->nDate  = d_h * 10;
	R->nDate += d_l;
	d = R->nHour;
	R->nHour  = d_h * 10;
	R->nHour += d_l;
	d = R->nMin;
	R->nMin  = d_h * 10;
	R->nMin += d_l;
	d = R->nSec;
	R->nSec  = d_h * 10;
	R->nSec += d_l;

	if ((R->nMonth < 1) || (R->nMonth >12)) 
		bFailTimer = TRUE;
	if ((R->nDate < 1) || (R->nDate >31)) 
		bFailTimer = TRUE;
	if (R->nHour > 23) 
		bFailTimer = TRUE;
	if (R->nMin > 59) 
		bFailTimer = TRUE;
	if (R->nSec > 59) 
		bFailTimer = TRUE;
	
	return bFailTimer;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int CMainFrame::CreateObj()
{
	CLESView* pView = (CLESView*) GetActiveView();
	int nRet = 0;

	//CString m_strmem;
	//m_strmem = _T("SharedMessage"); 
// 	HWND m_hwnd;
// 	m_hwnd = ::FindWindow(NULL, "LES - Observer" );
// 	if ( m_hwnd == NULL ) 
// 	{
// 		WinExec("Observer.exe", SW_NORMAL);
// 	}

	// 공유메모리를 사용할경우 공유메모리 의 값을 쓴다. 
	if ( pApp->m_SystemValue.bSharedMemory == TRUE ) 
	{
		// For debug-- 통신이 되지 않으면, SharedMemory를 이용하여 Data를 전송한다...
		//m_nTimerID = SetTimer(ID_TIMER, 100, NULL);		// 2 sec
		//SetTimer (800,100,NULL);
        //shared.SharedMemoryInit();
        shared.Serialize();
	}

	CLESApp *pApp = (CLESApp *)AfxGetApp();
	if ( pApp->m_SystemValue.bOperate ) 
	{
		// 메뉴 데이터 생성
		m_RealLOGMessage.CreateMenuInfo( &m_pMenuRoute[0], MenuSignalType );
		m_RealLOGMessage.CreateMenuInfo( &m_pMenuSwitch, MenuSwitchType );
		m_RealLOGMessage.CreateMenuInfo( &m_pMenuTrack, MenuTrackType );
	}

	// 로그 윈도우 생성
	if ( pApp->m_SystemValue.nSystemType != 2 ) { // 월마운트 용이 아닐때 
//		m_bIsCreateLOGView = m_LOGView.Create(IDD_DIALOG_LOG, NULL);
		m_bIsCreateLOGView = m_EventDlg.Create(IDD_DIALOG_EVENT, NULL);
		m_EventDlg.ShowWindow(SW_SHOW);
	}
	
	// 로그 화면 생성
//	m_LOGView.m_pParentWnd = this;
// 	if ( m_bIsCreateLOGView ) 
// 	{
// 		WinExec("LogWnd.exe", SW_NORMAL);
// 		m_LOGView.ShowWindow(SW_SHOW);
// 	}

	// 메뉴 윈도우 생성
	m_bIsCreateMenuView = m_MenuView.Create(IDD_DIALOG_MENU, NULL);
	// 메뉴 화면 생성	
	m_MenuView.m_pParentWnd = this;
	if ( pApp->m_SystemValue.nSystemType == 0 ) 
	{
		m_MenuView.ShowWindow(SW_SHOW);
	} 
	else 
	{
		m_MenuView.ShowWindow(SW_HIDE);	
	}

	// 통신 초기화
	SetFocus();
	switch ( pApp->m_SystemValue.nMode)
	{
	case LAN_COMMUNICATION:
	case TRC_COMMUNICATION:
		CommEthOpen();
		break;
	default:
		CommOpen();
		break;
	}

// 	if ( pApp->m_SystemValue.nTRCID )
// 	{
// 		CommEthOpen();
// 	}
// 	else
// 	{
// 		if(!CommOpen()) // 통신을 임시로 막았다. 
// 		{  
// 			// 오픈 안된거당..
// 		}
// 	}

	// 사운드 초기화
	OnSoundLoad();
	OnPlaySound();
	//SetTimer ( 500 , 1000 , NULL ); // 로그 테스트 

	if ( pApp->m_SystemValue.bSharedMemory == TRUE ) 
	{
		AfxMessageBox ("LES now running shared memory mode.");
		//SetTimer (800,1,NULL);
		//m_nTimerID = SetTimer(ID_TIMER, 100, NULL);	
		
		
		m_pSMSyncThread = (CSMSyncThread*)AfxBeginThread(RUNTIME_CLASS(CSMSyncThread), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		if(m_pSMSyncThread != NULL)
		{
			m_pSMSyncThread->Start(m_hWnd);
		}
	}

	if ( pApp->m_SystemValue.nSystemType == 0 ) 
	{
		m_MenuView.ShowWindow(SW_SHOW);
	} 
	else 
	{
		m_MenuView.ShowWindow(SW_HIDE);	
	}

//	SetTimer ( 700 , 2000 , NULL );

	return nRet;
}

//=======================================================
//
//  함 수 명 :  OnMenuLockUpdate()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메뉴를 활성화/비활성화 한다.
//
//=======================================================
void CMainFrame::OnMenuLockUpdate()
{

	if((pApp->m_SystemValue.bCtrlLock || m_bOperateDisable ) && pApp->m_SystemValue.bTestMode == FALSE)
	{
		if ( m_MenuView ) {
			m_MenuView.m_btnMainSet.EnableWindow(FALSE);
			m_MenuView.m_btnCallonSet.EnableWindow(FALSE);
			m_MenuView.m_btnShuntSet.EnableWindow(FALSE);
			m_MenuView.m_btnSigCancel.EnableWindow(FALSE);
			m_MenuView.m_btnSwitch.EnableWindow(FALSE);
			m_MenuView.m_btnSystem.EnableWindow(FALSE);
			m_MenuView.m_btnControl.EnableWindow(FALSE);
		}

// 		if ( m_LOGView ) {
// 			m_LOGView.m_btnFind.EnableWindow(FALSE);
// 			m_LOGView.m_btnLast.EnableWindow(FALSE);
// 			m_LOGView.m_btnLogset.EnableWindow(FALSE);
// 			m_LOGView.m_btnNext.EnableWindow(FALSE);
// 			m_LOGView.m_btnPrev.EnableWindow(FALSE);
// 			m_LOGView.m_btnScroll.EnableWindow(FALSE);
// 		}
	}
	else if( pApp->m_bISCTCMode )
	{
		if ( m_MenuView )
		{
			m_MenuView.m_btnMainSet.EnableWindow(FALSE);
			m_MenuView.m_btnCallonSet.EnableWindow(FALSE);
			m_MenuView.m_btnShuntSet.EnableWindow(FALSE);
			m_MenuView.m_btnSigCancel.EnableWindow(FALSE);
			m_MenuView.m_btnSwitch.EnableWindow(FALSE);
			m_MenuView.m_btnSystem.EnableWindow(TRUE);
			m_MenuView.m_btnControl.EnableWindow(FALSE);
			m_MenuView.m_btnSystem.m_bTimeSetDisable = TRUE;
		}
	}
	else
	{
		if ( m_MenuView ) 
		{
			if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
			{
				m_MenuView.m_btnMainSet.EnableWindow(FALSE);
				m_MenuView.m_btnShuntSet.EnableWindow(FALSE);
				m_MenuView.m_btnCallonSet.EnableWindow(FALSE);
				m_MenuView.m_btnSigCancel.EnableWindow(FALSE);
				m_MenuView.m_btnSwitch.EnableWindow(FALSE);
			}
			else
			{
				m_MenuView.m_btnMainSet.EnableWindow(TRUE);
				m_MenuView.m_btnShuntSet.EnableWindow(TRUE);
				m_MenuView.m_btnCallonSet.EnableWindow(TRUE);
				m_MenuView.m_btnSigCancel.EnableWindow(TRUE);
				m_MenuView.m_btnSwitch.EnableWindow(TRUE);
			}

			m_MenuView.m_btnSystem.EnableWindow(TRUE);
			m_MenuView.m_btnUser.EnableWindow(TRUE);
			m_MenuView.m_btnControl.EnableWindow(TRUE);
		}
	}

	m_MenuView.m_btnSystem.m_bAreUAdminUser = pApp->m_bIsSuperUser;

	if ( !pApp->m_bISCTCMode )
	{
		m_MenuView.m_btnSystem.m_bTimeSetDisable = FALSE;
	}
	else
	{
		m_MenuView.m_btnSystem.m_bTimeSetDisable = TRUE;
	}

	if ( pApp->m_VerifyObjectForMessage.strProjectName.Find("StandardOf13") == 0 || pApp->m_VerifyObjectForMessage.strStationName.Find("MYMENSINGH_JN") == 0 )
	{
		m_MenuView.m_btnSystem.m_bTimeSetDisable = TRUE;
	}
	else if((pApp->m_VerifyObjectForMessage.strStationName.Find("BHAIRAB_BAZAR") == 0) || (pApp->m_VerifyObjectForMessage.strStationName.Find("AKHAURA",0) == 0))
	{
		m_MenuView.m_btnSystem.m_bTimeSetDisable = FALSE;
	}
	
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		m_MenuView.m_btnControl.m_bControlMenuDisableForBlockDev = TRUE;
		m_MenuView.m_btnUser.m_bUserMenuDisableForBLockDev = TRUE;
	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
BOOL CMainFrame::UpdateMenuByField(void *pVMem)
{
	BOOL bAlter = FALSE;
	DataHeaderType* pHead = (DataHeaderType*)pVMem;
	UnitStatusType *sysInfo = (UnitStatusType *)&(pHead->UnitState);


	BOOL bOperateDisable;

	if ( sysInfo->bSysRun == TRUE ) 
	{
		m_bCBIRun = TRUE;
	} else {
		m_bCBIRun = FALSE;
	}

	if ( sysInfo->bComFail || !sysInfo->bSysRun) bOperateDisable = TRUE;
	else										 bOperateDisable = FALSE;

	if (m_bOperateDisable != bOperateDisable) bAlter = TRUE;
		m_bOperateDisable = bOperateDisable;

	if ( !(pApp->m_SystemValue.nProcOption & PROC_NO_UPSYSTEM) ) 
	{
	//	if ( bAlter ) 
			// 임시 OnMenuLockUpdate();
	}
	
	return bAlter;
}

//=======================================================
//
//  함 수 명 :  SystemTimeUpdate
//  함수출력 :  없음
//  함수입력 :  CJTime *destT, CJTime *scrT
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  시스템 시간을 설정한다.
//
//=======================================================
int CMainFrame::SystemTimeUpdate(CJTime *destT, CJTime *scrT)
{
	int  nMode = -1;

	if ( destT->nYear != scrT->nYear ) 
		nMode = -1;
	else if ( destT->nMonth != scrT->nMonth ) {
		if ( scrT->nMonth == 1 ) nMode = 2;
		else nMode = 1;
	}
	else if ( destT->nDate != scrT->nDate ) 
		nMode = 0;
	else if ( destT->nHour != scrT->nHour ) 
		nMode = -1;
	else if ( destT->nMin != scrT->nMin ) 
		nMode = -1;
	else if ( destT->nSec != scrT->nSec ) 
		nMode = -1;
	else 
		return FALSE;

// 갱신 시간 저장
	destT->nYear = scrT->nYear;
	destT->nMonth = scrT->nMonth; 
	destT->nDate = scrT->nDate; 
	destT->nHour = scrT->nHour;  
	destT->nMin = scrT->nMin; 
	destT->nSec = scrT->nSec;

// 갱신 파라메터 생성
	SYSTEMTIME T;
	T.wYear = destT->GetYear();
	T.wMonth = destT->nMonth;
	T.wDay = destT->nDate;
	T.wHour = destT->nHour;
	T.wMinute = destT->nMin;
	T.wSecond = destT->nSec;
	T.wMilliseconds = 0;
	T.wDayOfWeek = 0;
	if ( T.wYear > 2090 ) return FALSE;
// 시스템 시간 갱신
	if (!pApp->m_SystemValue.bSharedMemory)
	{
		SetLocalTime( &T );		//2012.08.20 임시로 PC시간 변경안함!!(디버깅시 너무 번거러움)
	}
// 저장 로그 파일 삭제(1년 보존)
//	if ( (nMode >= 0) && destT->nYear) {
//		OnDeleteLogFile( destT, nMode );
//	}
	if ( T.wHour == 3 && T.wMinute == 1 ) OnDeleteLogFile();
	return TRUE;
}

//=======================================================
//
//  함 수 명 :  일년이 지난 로그는 삭제 한다.
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 : 3달 보존으로 수정 해야함.
//
//=======================================================
void CMainFrame::OnDeleteLogFile(/*CJTime *destT, int nMode*/)
{
	CString strPath, strFile;
//	CJTime oldT = *destT;

	strPath = "D:\\LOG\\";

	CTime time;
	int iYear,iMonth,iDay,iHour,iMin,iSec;
	time = CTime::GetCurrentTime();

	if ( time != -1 ) {
		iYear = time.GetYear();
		iMonth = time.GetMonth();
		iDay = time.GetDay();
		iHour = time.GetHour();
		iMin = time.GetMinute();
		iSec = time.GetSecond();	
	}

	if ( iMonth < 4 ) {
		iMonth = iMonth + 9;
		iYear = iYear - 1;
	} else {
		iMonth = iMonth - 3;
	}
	strFile.Format("%04d\\%02d",iYear,iMonth);
	strPath = strPath + strFile;
	for ( int i = 1 ; i < 32 ; i++ ) {
		strFile.Format("\\%02d.LOG",i);
		strFile = strPath + strFile;
		_unlink( (LPCSTR)(strFile) );
	}
	_rmdir( (LPCSTR)(strPath) );
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::GetLogPath(CJTime &destT, CString &strFileName, int nMode)
{
	strFileName = (const char *)&pApp->m_LOGDirInfo[0];
	if ( nMode < 0 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%Y");
	if ( nMode == 2 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%m");
	if ( nMode == 1 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%d.LOG");

	return;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnRun() 
{
	// TODO: Add your command handler code here
	// LSM 을 통해  RUN 명령을 보낸다. 	
/*
	CLGEISView* pView;
	pView = (CLGEISView*)GetActiveView();
	ParamType lData;

	union {
		long wParam;
		struct {
			BYTE d1;
			BYTE d2;
			BYTE d3;
			BYTE d4;
		};
	};

	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight-100;
	d1 = (BYTE)BtnSysRun;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, wParam, lData.lParam );
*/

	// LSM 상관없이 단독으로 LES 에서 RUN 명령을 보낸다. 	
	long message = WS_OPMSG_START;
	long wParam = 0;
	long lParam = 0;

	short n = 0;
	BYTE *p = (BYTE*)&m_CmdQue[0];

	*p++ = (BYTE)message;
	p += sizeof(short);
	*(short*)p = (short)wParam;
	n = sizeof( short );
	p += n;
	*(long*)p = lParam;

	*(short*)&m_CmdQue[1] = n;
	lParam = (long)&m_CmdQue[0];

	pApp->m_pMainWnd->PostMessage(WM_COMMAND, ID_COMM_CMD_WRITE, (LPARAM)lParam);
	
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnReset() 
{
	// TODO: Add your command handler code here
	if ( m_bCBIRun == FALSE ) {
		AfxMessageBox ( " now CBI System is not running " );
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	pView->m_Screen.OlePostMessage( WS_OPMSG_RESET, 0, lData.lParam );		
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnTimeset() 
{
	// TODO: Add your command handler code here
	union {
		BYTE t;
		struct {
			BYTE bl : 4;
			BYTE bh : 4;
		};
	};
	CTimesetDlg Dlg;
	m_bDlgDisplaed = TRUE;
	if ( Dlg.DoModal() == IDOK ) {

		m_bDlgDisplaed = FALSE;
		BYTE Buf[16];
		CJTime T = Dlg.m_Time;
		bl = T.nSec % 10;
		bh = T.nSec / 10;
		Buf[0] = t;
		bl = T.nMin % 10;
		bh = T.nMin / 10;
		Buf[1] = t;
		bl = T.nHour % 10;
		bh = T.nHour / 10;
		Buf[2] = t;
		bl = T.nDate % 10;
		bh = T.nDate / 10;
		Buf[3] = t;
		bl = T.nMonth % 10;
		bh = T.nMonth / 10;
		Buf[4] = t;
		bl = T.nYear % 10;
		bh = T.nYear / 10;
		Buf[5] = t;
		
		SetCommQue( WS_OPMSG_SETTIMER, &Buf[0], 6 );

	}
	m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnMessageSend() 
{
	CMessageSendDlg Dlg;
	CString strMessage;

	if ( m_bMessageSendCk == FALSE ){
		strMessage.Format("Now Message sending %d/%d",m_iMessageSendCnt,MAX_MSGCNT) ;
		AfxMessageBox(strMessage);
		return;
	}

	m_bDlgDisplaed = TRUE;
	if ( Dlg.DoModal() == IDOK ) {
		m_strSendMessage = Dlg.m_strSendMessage;		
	}
	m_bDlgDisplaed = FALSE;

	m_strSendMessage.TrimLeft();
	m_strSendMessage.TrimRight();
	
	memset( &m_cMessage[0] , ' ' , 500 );

	memcpy( &m_cMessage[0] , m_strSendMessage , m_strSendMessage.GetLength() );

	m_cMessage[m_strSendMessage.GetLength()] = '\0';
	m_iMessageSendCnt = 0;

	m_bMessageSendCk = FALSE;
	SetTimer (900,500,NULL); // 16 바이트씩 끊어서 보내기 위해 타이머를 이용한다.

	// WS_OPMSG_MESSAGE - MESSAGE SEND FUNCTION
}
//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnExit() 
{
	// TODO: Add your command handler code here

	if ( pApp->m_SystemValue.nSystemType != 0 ) {
		
		if ( pApp->m_SystemValue.bTestMode != TRUE ) {
/*			HANDLE hToken;
			TOKEN_PRIVILEGES tkp;
			if ( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
			{        
				LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
				tkp.PrivilegeCount = 1;
				tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
				AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);
				::ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, NULL);
			}*/
		}
		PostMessage(WM_CLOSE);

	} else {

//		CPasswordDlg dlg;			//멤버변수 선으로 대체.
		m_LogInDlg.m_bIsLock = TRUE;
		m_LogInDlg.m_DlgTitle = "Exit Program";
		m_LogInDlg.m_stState = "For Exit Program, \nInput User ID and Password.";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_EXIT);	

		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() == IDOK){
			m_bDlgDisplaed = FALSE;
			if ( m_LogInDlg.m_strPassword != "BR2012" ) {
				if ( pApp->m_SystemValue.bTestMode != TRUE ) {
/*					HANDLE hToken;
					TOKEN_PRIVILEGES tkp;
					if ( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
					{        
						LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
						tkp.PrivilegeCount = 1;
						tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
						AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);
						::ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, NULL);
					}*/
				}
			}
			PostMessage(WM_CLOSE);
		}	
		m_bDlgDisplaed = FALSE;
	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnAlluser() 
{
	// TODO: Add your command handler code here
	CAllUserDlg		dlg;
	m_bDlgDisplaed = TRUE;
	dlg.DoModal();	
	m_bDlgDisplaed = FALSE;
	pApp->OnLoadUserID();
//	CLESView* pView;
//	pView = (CLESView*)GetActiveView();
//	pView->OnSend( 0x01 );					//LAN통신으로 유저 동기화부분.

}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnChangeUser() 
{
	// TODO: Add your command handler code here
	HWND m_hwnd;
	m_hwnd = ::FindWindow(NULL, "Login" );
	if ( m_hwnd != NULL ) return;
	if ( m_bLogWindowDisable == TRUE ) return;
	
//	CPasswordDlg dlg;			//멤버변수 선언으로 대체.
	m_LogInDlg.m_bIsLock = FALSE;

	CLESView* pView;
	pView = (CLESView*)GetActiveView();

	BYTE Buf[17];
	int iCnt;
	DataHeaderType *pHead = (DataHeaderType *)&pApp->SystemVMEM[0];

	for ( int i = 0; i < 17; i++)
	{
		Buf[i] = '\0';
	}

	m_LogInDlg.m_DlgTitle = "Login";
	m_LogInDlg.m_stState = "\nInput New User ID And Password.";
	m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_OPERATOR);	
	m_bDlgDisplaed = TRUE;
	
	m_LogInDlg.m_strUserID = pApp->GetProfileString( "Operator", "strCurrentOperator");

	switch (m_LogInDlg.DoModal() )
	{
	case IDOK:
//		if ( (pHead->LCCStatus.bLCC1Alive && pApp->m_SystemValue.nSystemNo == 1)
//			|| (pHead->LCCStatus.bLCC2Alive && pApp->m_SystemValue.nSystemNo == 2)
//			|| pApp->m_SystemValue.nSystemNo == 3 )
//		{
			m_bDlgDisplaed = FALSE;
			m_LogInDlg.m_strUserID.MakeUpper();
			pApp->m_SystemValue.strCurrentOperator = m_LogInDlg.m_strUserID;
			break;
//		}
//		else
//		{
//			MessageBox("Cannot log-in now!", "Login");
//			PostMessage(WM_COMMAND,ID_CHANGE);
//			return;
//		}
//		pView->OnSend( 0x02 );										//LAN통신으로 유저 동기화부분.
	case IDCANCEL:
		if ( (pHead->LCCStatus.bLCC1Active && pApp->m_SystemValue.nSystemNo == 1)
			|| (pHead->LCCStatus.bLCC2Active && pApp->m_SystemValue.nSystemNo == 2) )
		{
			return;
		}
		else
		{
			PostMessage(WM_COMMAND,ID_CHANGE);
			return;
		}
	default:	
		PostMessage(WM_COMMAND,ID_CHANGE);
		return;
	}
	m_bDlgDisplaed = FALSE;

	Buf[0] = WS_USEROPMSG_USER_CHANGE;
	pApp->WriteProfileString("OPERATOR","strCurrentOperator",pApp->m_SystemValue.strCurrentOperator );
	iCnt = pApp->m_SystemValue.strCurrentOperator.GetLength();
	if ( iCnt > 8 ) iCnt = 8;
	memcpy( &Buf[1] , pApp->m_SystemValue.strCurrentOperator , iCnt );

	m_CommCmd.RealUserData.UserMsgType = WS_USEROPMSG_USER_CHANGE;
	CString strCurrentUserPassword = pApp->GetProfileString( "USER_PASS", pApp->m_SystemValue.strCurrentOperator, _T(""));

	memcpy( &Buf[9], strCurrentUserPassword, strCurrentUserPassword.GetLength());

	SetCommQue( WS_OPMSG_USERID, &Buf[0], 0 );		

	OnDeleteLogFile();
}

//=======================================================
//
//  함 수 명 :  OnLockUnLock
//  함수출력 :  void
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  제어 잠금 및 잠금 해제 관련 루틴 
//
//=======================================================
int CMainFrame::OnLockUnLock(int iVal) 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return FALSE;
	}

	// TODO: Add your command handler code here
//	CPasswordDlg dlg;			//멤버변수 선언으로 대체.
	m_LogInDlg.m_bIsLock = TRUE;

	if ( iVal != 1 && iVal != 2 ) {
		CLESView* pView;
		pView = (CLESView*)GetActiveView();
		ParamType lData;
		lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
		lData.ndh =pApp->m_SystemValue.nLSMHeight;
		pView->m_Screen.OlePostMessage( WS_OPMSG_PAS, 0, lData.lParam );		
		return TRUE;
	}

	if( iVal == 1 ){
		m_LogInDlg.m_bIsLock = TRUE;
		m_LogInDlg.m_DlgTitle = "PAS IN";
		m_LogInDlg.m_stState = "System UnLock... \nInput User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_LOCK);	
	}
	else{
		m_LogInDlg.m_bIsLock = TRUE;
		m_LogInDlg.m_DlgTitle = "PAS OUT";
		m_LogInDlg.m_stState = "System lock... \nInput User ID and Password!";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_UNLOCK);	
	}
	m_bDlgDisplaed = TRUE;
	if(m_LogInDlg.DoModal() == IDOK){
		m_bDlgDisplaed = FALSE;
		if ( iVal == 1 ) {
			(BOOL)pApp->m_SystemValue.bCtrlLock = 0;
			m_bOperateDisable = 0;
//			m_LOGView.m_listBox.m_bStopScroll = FALSE;
		} else if ( iVal == 2 ) {
			(BOOL)pApp->m_SystemValue.bCtrlLock = 1;
			m_bOperateDisable = 1;		
		}

		if(pApp->m_SystemValue.bCtrlLock){		//Lock상태가 되면 
			pApp->m_SystemValue.strLockOperator = m_LogInDlg.m_strUserID;	//Lock ID를 현재 유저 ID로 
		}
		else
			pApp->m_SystemValue.strLockOperator = "";			//Unlock상태가되면 Lock ID를 ""로 
	} else {
		m_bDlgDisplaed = FALSE;
		return FALSE;
	}
	pApp->WriteProfileInt("OPERATOR","bCtrlLock",pApp->m_SystemValue.bCtrlLock);
	pApp->WriteProfileString("OPERATOR","strLockOperator",pApp->m_SystemValue.strLockOperator);

	OnMenuLockUpdate();	
	return TRUE;
}

int CMainFrame::OnLogInOut() 
{
	// TODO: Add your command handler code here
//	CPasswordDlg dlg;			//멤버변수 선언으로 대체. 
	int iLoginout;

	iLoginout = 0;
	m_LogInDlg.m_bIsLock = TRUE;

	if ( pApp->m_SystemValue.strCurrentOperator == "" ) {
		m_LogInDlg.m_bIsLock = FALSE;
		m_LogInDlg.m_DlgTitle = "Login ";
		m_LogInDlg.m_stState = "Input User ID and Password!";
		iLoginout = 1;
	} 
	else {
		m_LogInDlg.m_bIsLock = TRUE;
		m_LogInDlg.m_DlgTitle = "Logout ";
		m_LogInDlg.m_stState = "Input User ID and Password!";
		iLoginout = 2;
	}

	m_bDlgDisplaed = TRUE;
	if(m_LogInDlg.DoModal() == IDOK){
		m_bDlgDisplaed = FALSE;
		if ( iLoginout == 1 ) {
			pApp->m_SystemValue.strCurrentOperator = m_LogInDlg.m_strUserID;	//Lock ID를 현재 유저 ID로 
		} else {
			pApp->m_SystemValue.strCurrentOperator = "";	//Lock ID를 현재 유저 ID로 		
		}
	} else {
		m_bDlgDisplaed = FALSE;
		return FALSE;
	}
	return TRUE;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnSectionRelease() 
{
	// TODO: Add your command handler code here
	if ( pApp->m_SystemValue.bCtrlLock ) {
		ShowLockMessage();
		return;
	}

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Emergency Route Release";
	dlg.m_stState  = "Select the Track is released!";
	dlg.m_stScript = "Track: ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_TRACK);	
	dlg.m_Type = 5;	
	m_bDlgDisplaed = TRUE;
	dlg.DoModal();		
	m_bDlgDisplaed = FALSE;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnBlockingTrack() 
{
	// TODO: Add your command handler code here
	if ( pApp->m_SystemValue.bCtrlLock ) {
		ShowLockMessage();
		return;
	}

	CControlDlg	dlg;
	dlg.m_DlgTitle = "Track Blocking ";
	dlg.m_stState  = "Select Track!";
	dlg.m_stScript = "Track: ";
	dlg.hIcon = AfxGetApp()->LoadIcon(IDI_BLOCK);	
	dlg.m_Type = 6;	
	m_bDlgDisplaed = TRUE;
	dlg.DoModal();		
	m_bDlgDisplaed = FALSE;
}

//=======================================================
//
//  함 수 명 :  OnSoundLoad
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드 파일을 로딩한다.
//
//=======================================================
BOOL CMainFrame::OnSoundLoad()
{
	m_PlaySound[0].Create(IDR_WAVE1);		//접근
	m_PlaySound[1].Create(IDR_WAVE2);		//신호기 전철기
	m_PlaySound[2].Create(IDR_WAVE3);		//파워
	m_PlaySound[3].Create(IDR_WAVE4);		//폐색
	m_PlaySound[4].Create(IDR_WAVE5);		//폐색
	m_PlaySound[5].Create(IDR_WAVE6);		//폐색
	m_PlaySound[6].Create(IDR_WAVE7);		//폐색
	m_PlaySound[7].Create(IDR_WAVE8);		//폐색

	return TRUE;
}


//=======================================================
//
//  함 수 명 :  OnPlaySound
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드를 발생혹은 정지한다.
//
//=======================================================
void CMainFrame::OnPlaySound()
{
	if(m_CurrentSound &BIT_SOUND_BLOCKADE1){		//폐색1 
		m_PlaySound[3].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[3].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_POWER){		//파워
		m_PlaySound[2].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[2].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_SP){			//신호기 및 전철기
		m_PlaySound[1].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[1].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_APPROACH){	//접근
		m_PlaySound[0].Play(0,TRUE);
//		m_PlaySound[0].Play(0,TRUE); // 2007년 6월 22일 바이패스역 알람 막음
	}
	else
	{
		m_PlaySound[0].Stop();
	}
}

//=======================================================
//
//  함 수 명 :  OnCheckSoundState
//  함수출력 :  없음 
//  함수입력 :  BYTE* pData
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드 정보가 달라진것이 있으면 사운드를 발생한다.
//
//=======================================================
void CMainFrame::OnCheckSoundState(BYTE* pData)
{
		BYTE nSoundAlter = 0x00;
		DataHeaderType* pHead = (DataHeaderType*)pData;
		SystemStatusType *pSysInfo = (SystemStatusType *)&(pHead->nSysVar);
		UnitStatusType *pUniInfo = (UnitStatusType *)&(pHead->UnitState);
		SystemStatusTypeExt *pSysVarExt = (SystemStatusTypeExt *)&(pHead->nSysVarExt);

		if ( pSysVarExt->bSound1 ) nSoundAlter |= BIT_SOUND_APPROACH;	//0x01
		if ( pSysVarExt->bSound2 ) nSoundAlter |= BIT_SOUND_SP;		//0x02	
		if ( pSysVarExt->bSound3 ) nSoundAlter |= BIT_SOUND_POWER;	//0x04
		if ( pSysVarExt->bSound4 ) nSoundAlter |= BIT_SOUND_BLOCKADE1;	//0x08
// 		if ( pSysVarExt->bSound5 ) nSoundAlter |= BIT_SOUND_BLOCKADE2;	//0x10
// 		if ( pSysVarExt->bSound6 ) nSoundAlter |= BIT_SOUND_BLOCKADE3;	//0x20
// 		if ( pSysVarExt->bSound7 ) nSoundAlter |= BIT_SOUND_BLOCKADE4;	//0x40
// 		if ( pSysVarExt->bSound8 ) nSoundAlter |= BIT_SOUND_BLOCKADE5;	//0x80
		if ( m_CurrentSound != nSoundAlter) {
			m_CurrentSound = nSoundAlter;
		}

		if(pApp->m_SystemValue.bSound){
			if(m_OldSound ^ m_CurrentSound){	//사운드 정보가 달라진것이 있으면
				OnPlaySound();					//사운드 출력하고 정보 업데이트 
				m_OldSound = m_CurrentSound;
			}
		}
}

//=======================================================
//
//  함 수 명 :  OnAllStopSound()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드를 멈춘다.
//
//=======================================================
void CMainFrame::OnAllStopSound()
{
		m_PlaySound[7].Stop();
		m_PlaySound[6].Stop();
		m_PlaySound[5].Stop();
		m_PlaySound[4].Stop();
		m_PlaySound[3].Stop();
		m_PlaySound[2].Stop();
		m_PlaySound[1].Stop();
		m_PlaySound[0].Stop();
}

//=======================================================
//
//  함 수 명 :  StartUp
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  마우스의 후킹을 시작한다.
//
//=======================================================
BOOL CMainFrame::StartUp()
{
	if (NULL == m_hmodHookTool)
	{
		m_hmodHookTool = ::LoadLibrary( "HookTool.Dll" );
		Sleep (500);
		if(m_hmodHookTool == NULL)
			return FALSE;
		
		m_pfnInstallHook = (PFN_InstallHook)::GetProcAddress(m_hmodHookTool, "InstallHook");
		if(m_pfnInstallHook == NULL)
			return FALSE;
		m_pfnUnInstallHook = (PFN_UnInstallHook)::GetProcAddress(m_hmodHookTool, "UnInstallHook");
		if(m_pfnUnInstallHook == NULL)
			return FALSE;
		m_pfnSetHookOption = (PFN_SetHookOption)::GetProcAddress(m_hmodHookTool, "SetHookOption");
		if(m_pfnSetHookOption == NULL)	
			return FALSE;
		m_pfnGetHookOption = (PFN_GetHookOption)::GetProcAddress(m_hmodHookTool, "GetHookOption");
		if(m_pfnGetHookOption == NULL)
			return FALSE;
		m_pfnGetResultString = (PFN_GetResultString)::GetProcAddress(m_hmodHookTool, "GetResultString");
		if(m_pfnGetResultString == NULL)
			return FALSE;
		m_pfnGetIniFilePath	= (PFN_GetIniFilePath)::GetProcAddress(m_hmodHookTool, "GetIniFilePath");
		if(m_pfnGetIniFilePath == NULL)
			return FALSE;
		m_pfnSetEnabledRestart = (PFN_SetEnabledRestart)::GetProcAddress(m_hmodHookTool, "SetEnabledRestart");
		if(m_pfnSetEnabledRestart == NULL)
			return FALSE;
	}
	
	return TRUE;
}

void CMainFrame::ChangeUser()
{
	OnChangeUser();
}

void CMainFrame::OnAutoExit()
{
	   HWND m_hwnd;
	   m_bLogWindowDisable = TRUE;
	   m_hwnd = ::FindWindow(NULL, "Login" );
	   if ( m_hwnd != NULL ) {
			::SendMessage(m_hwnd, WM_CLOSE, 0, 0);
	   }

	   m_hwnd = ::FindWindow(NULL, "Exit Program" );
	   if ( m_hwnd != NULL ) {
			::SendMessage(m_hwnd, WM_CLOSE, 0, 0);
	   }

//	    CPasswordDlg dlg;			//멤버변수 선언으로 대체.
		m_LogInDlg.m_bIsLock = TRUE;
		m_LogInDlg.m_DlgTitle = "Auto System Exit";
		m_LogInDlg.m_stState = "For Exit Program, \nInput User ID and Password.";
		m_LogInDlg.hIcon = AfxGetApp()->LoadIcon(IDI_EXIT);	

		m_bDlgDisplaed = TRUE;
		if(m_LogInDlg.DoModal() == IDOK){
			m_bDlgDisplaed = FALSE;
			PostMessage(WM_CLOSE);
		}	
		m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnAutoExitEx()
{
	   HWND m_hwnd;
	   m_bLogWindowDisable = TRUE;
	   m_hwnd = ::FindWindow(NULL, "Login" );
	   if ( m_hwnd != NULL ) {
			::SendMessage(m_hwnd, WM_CLOSE, 0, 0);
	   }

	   m_hwnd = ::FindWindow(NULL, "Exit Program" );
	   if ( m_hwnd != NULL ) {
			::SendMessage(m_hwnd, WM_CLOSE, 0, 0);
	   }

	   PostMessage(WM_CLOSE);
	   m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnF1()
{
	m_MenuView.SetFocus();
	m_MenuView.m_btnSystem.Click();
}

void CMainFrame::OnF2()
{
	m_MenuView.SetFocus();
	m_MenuView.m_btnUser.Click();
}

void CMainFrame::OnF3()
{
	m_MenuView.SetFocus();
	m_MenuView.m_btnControl.Click();
}

void CMainFrame::OnF4()
{
}

void CMainFrame::OnF5()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	OnAlarm();
}

void CMainFrame::OnF6()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	Onbuzzer();
}

void CMainFrame::OnF7()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("LAKSAM")==0
		|| pApp->m_VerifyObjectForMessage.strStationName.Find("CHINKI")==0)
	{
		OnSendTrainNumber();
	}
}

void CMainFrame::OnF8()
{
	OnSelectBlock();
//	MessageBox("F8");
}

void CMainFrame::OnF9()
{
//	MessageBox("F9");
}

void CMainFrame::OnF10()
{
//	MessageBox("F10");
}

void CMainFrame::OnF11()
{
//	MessageBox("F11");
}

void CMainFrame::OnF12()
{
//	MessageBox("F12");
}

void CMainFrame::On1()
{
// 	m_LOGView.SetFocus();
// 	m_LOGView.m_iTrans = 255;
// 	m_LOGView.SetTrans(255);
// 	m_LOGView.m_btnFind.Click();
// 	m_LOGView.ButtonFind();
}

void CMainFrame::On2()
{
	/*
	m_LOGView.SetFocus();
	m_LOGView.m_iTrans = 255;
	m_LOGView.SetTrans(255);
	m_LOGView.m_btnLogset.Click();
	m_LOGView.ButtonLogset();
	*/
}

void CMainFrame::On3()
{
// 	m_LOGView.SetFocus();
// 	m_LOGView.m_iTrans = 255;
// 	m_LOGView.SetTrans(255);
// 	m_LOGView.m_btnPrev.Click();
// 	m_LOGView.PrevMessage();
}

void CMainFrame::On4()
{
// 	m_LOGView.SetFocus();
// 	m_LOGView.m_iTrans = 255;
// 	m_LOGView.SetTrans(255);
// 	m_LOGView.m_btnLast.Click();
// 	m_LOGView.LastMessage();
}

void CMainFrame::On5()
{
// 	m_LOGView.SetFocus();
// 	m_LOGView.m_iTrans = 255;
// 	m_LOGView.SetTrans(255);
// 	m_LOGView.m_btnScroll.Click();
// 	m_LOGView.ButtonScroll();
}

void CMainFrame::On6()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	m_MenuView.SetFocus();
	m_MenuView.m_btnMainSet.Click();
	m_MenuView.Bmainroute();
}

void CMainFrame::On7()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	m_MenuView.SetFocus();
	m_MenuView.m_btnCallonSet.Click();
	m_MenuView.Bcallonroute();
}

void CMainFrame::On8()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	m_MenuView.SetFocus();
	m_MenuView.m_btnSigCancel.Click();
	m_MenuView.Bsignalcancel();
}

void CMainFrame::On9()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	m_MenuView.SetFocus();
	m_MenuView.m_btnSwitch.Click();
	m_MenuView.Bswitch();
}

void CMainFrame::On0()
{
//	m_MenuView.SetFocus();
//	m_MenuView.m_btnSwitch.Click();
//	m_MenuView.Bswitch();
}

void CMainFrame::OnAltPlusE()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	OnSelectEPK();
}

void CMainFrame::OnAltPlusL()
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	OnSelectLC();
}

void CMainFrame::SetLSMFocus() 
{
}

// void CMainFrame::OnDtgbca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 1;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtgblcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 1;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtgbcbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 1;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtcbca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 2;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtcblcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 2;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnDtcbcbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 2;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnUtgbca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 3;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtgblcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 3;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtgbcbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 3;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcbca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 4;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcblcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 4;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcbcbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 4;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// //
// 
// void CMainFrame::OnDtgb2ca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 11;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtgb2lcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 11;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtgb2cbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 11;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtcb2ca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 12;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnDtcb2lcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 12;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnDtcb2cbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 12;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );				
// }
// 
// void CMainFrame::OnUtgb2ca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 13;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtgb2lcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 13;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtgb2cbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 13;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcb2ca() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 14;
// 	lData.ndh = 1;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcb2lcr() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 14;
// 	lData.ndh = 2;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }
// 
// void CMainFrame::OnUtcb2cbb() 
// {
// 	CLESView* pView;
// 	pView = (CLESView*)GetActiveView();
// 	ParamType lData;
// 	lData.ndl = 14;
// 	lData.ndh = 3;
// 	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );			
// }

void CMainFrame::OnControlBlock()
{
	int nBlockNumber = 0;
	int nOperateIndex = 0;
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = m_SelectBlock.m_nRealNumberOfBlock;
	lData.ndh = m_SelectBlockFunction.m_nIndexOfBlockFunction + 1;

	pView->m_Screen.OlePostMessage( WS_OPMSG_BLOCK, lData.lParam, NULL );
}
void CMainFrame::OnSls() 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnSls;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnAtb() 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAtb;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnNd() 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnDay;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnAlarm() 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAck;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::Onbuzzer() 
{
	if (pApp->m_VerifyObjectForMessage.strStationName.Find("ASHUGANJ") == 0 )
	{
		return;
	}

	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnPDB;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnSendTrainNumber() 
{
	// TODO: Add your command handler code here
	BYTE bIndexPlusTrainNumber[6];
	CSendTrainNumber tnDlg;
	m_bDlgDisplaed = TRUE;
	if ( tnDlg.DoModal() == IDOK ) {
		
		m_bDlgDisplaed = FALSE;

		bIndexPlusTrainNumber[0] = tnDlg.m_nIndex;
		memcpy(&bIndexPlusTrainNumber[1],tnDlg.m_strTrainNumber.Left(5),5);

		
		SetCommQue( WS_OPMSG_TRAIN_NUMBER, &bIndexPlusTrainNumber[0], 6 );
		
	}
	m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnSelectEPK()
{
	m_bDlgDisplaed = TRUE;
	if ( m_SelectEPK.DoModal() == IDOK )
	{
		switch ( m_SelectEPK.m_nIndexOfEPK )
		{
		case AEPK:
			OnAEPK();
			break;
		case BEPK:
			OnBEPK();
			break;
		case CEPK:
			OnCEPK();
			break;
		case DEPK:
			OnDEPK();
			break;
		case EEPK:
			OnEEPK();
			break;
		case FEPK:
			OnFEPK();
			break;
		case GEPK:
			OnGEPK();
			break;
		case UPEPK:
			OnUpEPK();
			break;
		case DNEPK:
			OnDnEPK();
			break;
		default:
			break;
		}
	}
	m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnSelectLC()
{
	m_bDlgDisplaed = TRUE;
	if ( m_SelectLC.DoModal() == IDOK )
	{
		switch ( m_SelectLC.m_nIndexOfLC )
		{
		case LC1:
			OnLC1();
			break;
		case LC2:
			OnLC2();
			break;
		case LC3:
			OnLC3();
			break;
		case LC4:
			OnLC4();
			break;
		case LC5:
			OnLC5();
			break;
		default:
			break;
		}
	}
	m_bDlgDisplaed = FALSE;
}

void CMainFrame::OnSelectBlock()
{
	m_bDlgDisplaed = TRUE;
	if ( m_SelectBlock.DoModal() == IDOK )
	{
		m_SelectBlockFunction.m_nIndexOfBlock = m_SelectBlock.m_nRealNumberOfBlock;

		if ( m_SelectBlockFunction.DoModal() == IDOK )
		{
			OnControlBlock();
		}
	}
}

void CMainFrame::OnUpEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKup;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnDnEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKdn;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnAEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKA;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnBEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKB;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnCEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKC;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnDEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKD;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnEEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKE;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnFEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKF;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnGEPK() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnAPKG;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnLC1() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnLCB;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnLC2() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnLC2B;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnLC3() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnLC3B;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnLC4() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnLC4B;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

void CMainFrame::OnLC5() 
{
	CLESView* pView;
	pView = (CLESView*)GetActiveView();
	ParamType lData;
	lData.ndl = pApp->m_SystemValue.nMainSizeX/2;
	lData.ndh =pApp->m_SystemValue.nLSMHeight;
	lData.d1 = (BYTE)BtnLC5B;
	pView->m_Screen.OlePostMessage( WS_OPMSG_BUTTON, lData.lParam, NULL );		
}

int CMainFrame::GetTrackID(LPCSTR lpszName) // 2006년 6월 22일 아카우라 오픈 : 바이패스역 알람1 계속 발생 때문에 함수 추가 
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strTrack.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strTrack.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

int CMainFrame::MessageSend() // 아카우라역 SB 에서 바이패스 기계실로 메세지를 보낸다.
{

	if ( m_iMessageSendCnt > MAX_MSGCNT ) {
		KillTimer(900);
//		SetTimer (1000,2000,NULL);
//		m_bMessageSendCk = TRUE;
	}

	BYTE stx;
	stx = COM_STX;

	BYTE Buf[12];
	Buf[0]=Buf[1]=Buf[2]=Buf[3]=Buf[4]=Buf[5]=Buf[6]=Buf[7]=Buf[8]=Buf[9]=Buf[10]=Buf[11]='\0';

	Buf[0] = 0x3D;
	Buf[1] = WS_OPMSG_MESSAGE;
	Buf[2] = m_iMessageSendCnt;
	memcpy( &Buf[3] , &m_cMessage[m_iMessageSendCnt*10] , 10 );

	if ( _classComm1.m_bConnected ) 
	{
		_classComm1.SendData( &Buf[0], 13, COM_STX ,TRUE);
	} 
	if ( _classComm2.m_bConnected ) 
	{
		_classComm2.SendData( &Buf[0], 13, COM_STX ,TRUE);
	}

	m_iMessageSendCnt++;
	return 1;
}

void CMainFrame::CreateUser( RealUserDataType *UserData )
{
	CString strEntry, strUserName, strPassWord, strForCmpUserName, strBufUserName, strBufPassWord;
	BOOL bExistSameUserName = FALSE;
	int iUserCount;

	strUserName = UserData->UserName;
	strPassWord = UserData->UserPassword;

	if ( strUserName.GetLength() > 8 )
		strUserName = strUserName.Left(8);
	if ( strPassWord.GetLength() > 8 )
		strPassWord = strPassWord.Left(8);

	iUserCount = pApp->GetProfileInt( "USER_ID", "NUMBER", 0);

	for ( int i = 1; i <= iUserCount; i++ )
	{
		strEntry.Format("%s%d", "ID", i);
		strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);

		if ( strUserName == strForCmpUserName )
		{
			bExistSameUserName = TRUE;
			break;
		}
	}

	if ( !bExistSameUserName )
	{
		iUserCount++;

		for ( int i = 1; i <= iUserCount; i++)
		{
			strEntry.Format("%s%d", "ID", i);
			strBufUserName = pApp->GetProfileString( "USER_ID", strEntry);
			strBufPassWord = pApp->GetProfileString( "USER_PASS", strBufUserName);

			pApp->WriteProfileString( "USER_ID", strEntry, strUserName);
			pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);

			if ( strBufUserName.Left(1) == '0')
				break;
			strUserName = strBufUserName;
			strPassWord = strBufPassWord;
		}

		pApp->m_SystemValue.nUserNumber = iUserCount;
		pApp->WriteProfileInt( "USER_ID", "NUMBER", iUserCount);

		CString strTempID = pApp->GetProfileString( "Operator", "strCurrentOperator");

		if(m_LogInDlg.m_ComboUser.GetSafeHwnd() != NULL)
		{
			m_LogInDlg.m_ComboUser.ResetContent();
			m_LogInDlg.m_ComboUser.OnInitial(FALSE);
			m_LogInDlg.m_ComboUser.SelectString(0, strTempID);
		}

//		m_LogInDlg.m_ComboUser.OnInitial();
	}
	else
	{
		pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
	}


//	pApp->WriteProfileString( "USER_ID", strEntry, strUserName );
//	pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord );
/*
	m_nLimitCount = m_SystemValue.nUserNumber;
	if ( m_nLimitCount == 0 ) {
		return; // 유저가 한명도 없을경우는 잘못된 것이다.
	}
	m_UserSection = "USER_ID";
	m_PassSection = "USER_PASS";
	m_Entry = "ID";
	
	char UserID[6];
	
	int  i, n;
	CString strID, strPassword, entry;
	
	HKEY	hResult;		//User 목록과 Password목록 동시에 지우기 
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES",&hResult);
	RegDeleteKey(hResult,"USER_PASS");
	RegCloseKey(hResult);
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES",&hResult);
	CString ttt;
	ttt.Format("%s%d",m_Entry,m_nLimitCount);
	RegDeleteKey(hResult,"USER_ID");
	RegCloseKey(hResult);
	
	n = m_UserValue.DataCounter;
	for(i=1; i <= n; i++) {		//1부터 시작
		entry.Format("%s%d", m_Entry, i);
		memcpy ( UserID , m_UserValue.UserData[i-1].UserID , 5 );
		UserID[5] = '\0';
		WriteProfileString( m_UserSection, entry, UserID );
		WriteProfileString( m_PassSection, UserID, m_UserValue.UserData[i-1].UserPassword );
	}
	m_SystemValue.nUserNumber = m_UserValue.DataCounter;
	WriteProfileInt("USER_ID","NUMBER",m_UserValue.DataCounter);
	
	CTime time = CTime::GetCurrentTime();
	m_UserValue.lDate = GetSecond( time );
	WriteProfileInt( "Common", "DATE", m_UserValue.lDate );*/
}
void CMainFrame::DeleteUser( RealUserDataType *UserData )
{
	CString strEntry, strUserName, strPassWord, strForCmpUserName, strBufUserName, strBufPassWord;
	BOOL bExistSameUserName = FALSE;
	int iUserCount, iDeleteUserID;
	
	strUserName = UserData->UserName;
	strPassWord = UserData->UserPassword;
	
	if ( strUserName.GetLength() > 8 )
		strUserName = strUserName.Left(8);
	if ( strPassWord.GetLength() > 8 )
		strPassWord = strPassWord.Left(8);
	
	iUserCount = pApp->GetProfileInt( "USER_ID", "NUMBER", 0);
	
	for ( int i = 1; i <= iUserCount; i++ )
	{
		strEntry.Format("%s%d", "ID", i);
		strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);
		
		if ( strUserName == strForCmpUserName )
		{
			bExistSameUserName = TRUE;
			iDeleteUserID = i;
			break;
		}
	}
	
	if ( bExistSameUserName )
	{
		for ( int i = iUserCount; i >= iDeleteUserID; i--)
		{
			strEntry.Format("%s%d", "ID", i);
			strBufUserName = pApp->GetProfileString( "USER_ID", strEntry);
			strBufPassWord = pApp->GetProfileString( "USER_PASS", strBufUserName);
			
			if ( i == iUserCount )
			{
				HKEY	hResult;		//User 목록과 Password목록 동시에 지우기 
				RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\USER_PASS",&hResult);
				RegDeleteValue(hResult,strUserName);
				RegCloseKey(hResult);
				RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LES\\USER_ID",&hResult);
				RegDeleteValue(hResult,strEntry);
				RegCloseKey(hResult);
			}
			else
			{
				pApp->WriteProfileString( "USER_ID", strEntry, strUserName);
				pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
			}
			
			strUserName = strBufUserName;
			strPassWord = strBufPassWord;
		}

		iUserCount--;
		pApp->m_SystemValue.nUserNumber = iUserCount;
		pApp->WriteProfileInt( "USER_ID", "NUMBER", iUserCount);
		
		CString strTempID = pApp->GetProfileString( "Operator", "strCurrentOperator");
		
		if(m_LogInDlg.m_ComboUser.GetSafeHwnd() != NULL)
		{
			m_LogInDlg.m_ComboUser.ResetContent();
			m_LogInDlg.m_ComboUser.OnInitial(FALSE);
			m_LogInDlg.m_ComboUser.SelectString(0, strTempID);
		}
	}
}
void CMainFrame::ChangePass( RealUserDataType *UserData )
{
	CString strEntry, strUserName, strPassWord, strForCmpUserName, strBufUserName, strBufPassWord;
	BOOL bExistSameUserName = FALSE;
	int iUserCount, iUserID;
	
	strUserName = UserData->UserName;
	strPassWord = UserData->UserPassword;
	
	if ( strUserName.GetLength() > 8 )
		strUserName = strUserName.Left(8);
	if ( strPassWord.GetLength() > 8 )
		strPassWord = strPassWord.Left(8);
	
	iUserCount = pApp->GetProfileInt( "USER_ID", "NUMBER", 0);
	
	for ( int i = 1; i <= iUserCount; i++ )
	{
		strEntry.Format("%s%d", "ID", i);
		strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);
		
		if ( strUserName == strForCmpUserName )
		{
			bExistSameUserName = TRUE;
			iUserID = i;
			break;
		}
	}
	
	if ( !bExistSameUserName )
	{
		iUserCount++;
		
		for ( int i = 1; i <= iUserCount; i++)
		{
			strEntry.Format("%s%d", "ID", i);
			strBufUserName = pApp->GetProfileString( "USER_ID", strEntry);
			strBufPassWord = pApp->GetProfileString( "USER_PASS", strBufUserName);
			
			pApp->WriteProfileString( "USER_ID", strEntry, strUserName);
			pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
			
			if ( strBufUserName.Left(1) == '0')
				break;
			strUserName = strBufUserName;
			strPassWord = strBufPassWord;
		}

		pApp->m_SystemValue.nUserNumber = iUserCount;
		pApp->WriteProfileInt( "USER_ID", "NUMBER", iUserCount);
	}
	else
	{
		pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
	}
}

void CMainFrame::ChangeToAnotherUser( RealUserDataType *UserData )
{
	CString strEntry, strUserName, strPassWord, strForCmpUserName, strBufUserName, strBufPassWord;
	BOOL bExistSameUserName = FALSE;
	int iUserCount;
	
	strUserName = UserData->UserName;
	strPassWord = UserData->UserPassword;
	
	if ( strUserName.GetLength() > 8 )
		strUserName = strUserName.Left(8);
	if ( strPassWord.GetLength() > 8 )
		strPassWord = strPassWord.Left(8);
	
	iUserCount = pApp->GetProfileInt( "USER_ID", "NUMBER", 0);
	
	for ( int i = 1; i <= iUserCount; i++ )
	{
		strEntry.Format("%s%d", "ID", i);
		strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);
		
		if ( strUserName == strForCmpUserName )
		{
			bExistSameUserName = TRUE;
			break;
		}
	}
	
	if ( !bExistSameUserName )
	{
		iUserCount++;
		
		for ( int i = 1; i <= iUserCount; i++)
		{
			strEntry.Format("%s%d", "ID", i);
			strBufUserName = pApp->GetProfileString( "USER_ID", strEntry);
			strBufPassWord = pApp->GetProfileString( "USER_PASS", strBufUserName);
			
			pApp->WriteProfileString( "USER_ID", strEntry, strUserName);
			pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
			
			if ( strBufUserName.Left(1) == '0')
				break;
			strUserName = strBufUserName;
			strPassWord = strBufPassWord;
		}

		pApp->m_SystemValue.nUserNumber = iUserCount;
		pApp->WriteProfileInt( "USER_ID", "NUMBER", iUserCount);
		
		pApp->m_SystemValue.strCurrentOperator = UserData->UserName;
		pApp->WriteProfileString( "Operator", "strCurrentOperator", (LPCSTR)UserData->UserName);

		if(m_LogInDlg.m_ComboUser.GetSafeHwnd() != NULL)
		{
			m_LogInDlg.m_ComboUser.ResetContent();
			m_LogInDlg.m_ComboUser.OnInitial(FALSE);
			m_LogInDlg.m_ComboUser.SelectString(0, (LPCSTR)UserData->UserName);
		}
	}
	else
	{
		pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
	}
}

void CMainFrame::CheckLCCHaveCurrentUserIDOrNot( LCCCommDataType *LCCCommdata)
{
	CString strEntry, strUserName, strPassWord, strForCmpUserName, strBufUserName, strBufPassWord;
	BOOL bExistSameUserName = FALSE;
	int iUserCount;
	
	strUserName = LCCCommdata->CurrentUserName;
	strPassWord = LCCCommdata->CurrentUserPass;
	
	if ( strUserName.GetLength() > 8 )
		strUserName = strUserName.Left(8);
	if ( strPassWord.GetLength() > 8 )
		strPassWord = strPassWord.Left(8);
	
	iUserCount = pApp->GetProfileInt( "USER_ID", "NUMBER", 0);
	
	for ( int i = 1; i <= iUserCount; i++ )
	{
		strEntry.Format("%s%d", "ID", i);
		strForCmpUserName = pApp->GetProfileString( "USER_ID", strEntry);
		
		if ( strUserName == strForCmpUserName )
		{
			bExistSameUserName = TRUE;
			break;
		}
	}
	
	if ( !bExistSameUserName && strUserName != "" )
	{
		iUserCount++;
		
		for ( int i = 1; i <= iUserCount; i++)
		{
			strEntry.Format("%s%d", "ID", i);
			strBufUserName = pApp->GetProfileString( "USER_ID", strEntry);
			strBufPassWord = pApp->GetProfileString( "USER_PASS", strBufUserName);
			
			pApp->WriteProfileString( "USER_ID", strEntry, strUserName);
			pApp->WriteProfileString( "USER_PASS", strUserName, strPassWord);
			
			if ( strBufUserName.Left(1) == '0')
				break;
			strUserName = strBufUserName;
			strPassWord = strBufPassWord;
		}
		
		pApp->WriteProfileInt( "USER_ID", "NUMBER", iUserCount);
	}

	CString strTempID = pApp->GetProfileString( "Operator", "strCurrentOperator");
	pApp->m_SystemValue.strCurrentOperator = strUserName;

	if ( memcmp( strTempID, strUserName, strTempID.GetLength() ) || strTempID.GetLength() != strUserName.GetLength() )
	{
		pApp->WriteProfileString( "Operator", "strCurrentOperator", strUserName);
	
		if(m_LogInDlg.m_ComboUser.GetSafeHwnd() != NULL && strUserName != "")
		{
			m_LogInDlg.m_ComboUser.ResetContent();
			m_LogInDlg.m_ComboUser.OnInitial(FALSE);
			m_LogInDlg.m_ComboUser.SelectString(0, strTempID);
		}
	}
}

void CMainFrame::OnReceiveGPSMsg(USHORT nMsgLen, BYTE *pMsg)	//GPS추가.
{
	union {
		BYTE t;
		struct {
			BYTE bl : 4;
			BYTE bh : 4;
		};
	};

	BYTE Buf[6];
	SYSTEMTIME	GPSTime;
	SYSTEMTIME	PCTime;
	SYSTEMTIME  NewTime;
	TIME_ZONE_INFORMATION info;
	memset(&Buf[0], 0, sizeof(Buf));
	memset(&GPSTime, 0, sizeof(GPSTime));
	memset(&NewTime, 0, sizeof(NewTime));
	memset(&PCTime , 0, sizeof(PCTime ));
	memset(&info, 0, sizeof(info));

	GetLocalTime(&PCTime);
	GetTimeZoneInformation((LPTIME_ZONE_INFORMATION)&info);

	CString strTime;
	CString strDate;

	CString strYear, strMonth, strDay, strHour, strMinute, strSecond;
	int nFindComma;
	int nDateLen = nMsgLen;
	int nIsValid = FALSE;

	strTime = strDate = pMsg;

	nFindComma = strTime.Find(',')+1;
	strTime = strTime.Right(nMsgLen-nFindComma);
	strTime = strTime.Left(6);

	for(int i = 0; i < 9; i++)
	{
		nFindComma = strDate.Find(',')+1;
		strDate = strDate.Right(nDateLen-nFindComma);
		nDateLen-=nFindComma;

		if ( i == 1 )
		{
			CString strFirstChar = strDate.Left(1);

			if ( strFirstChar == "A")
				nIsValid = TRUE;
			else
				nIsValid = FALSE;
		}
	}
	strDate = strDate.Left(6);

	strDay = strDate.Left(2);
	strDate = strDate.Right(4);
	strMonth = strDate.Left(2);
	strYear = strDate.Right(2);

	strHour = strTime.Left(2);
	strTime = strTime.Right(4);
	strMinute = strTime.Left(2);
	strSecond = strTime.Right(2);

	GPSTime.wYear = atoi(strYear) + 2000;
	GPSTime.wMonth = atoi(strMonth);
	GPSTime.wDay = atoi(strDay);
	GPSTime.wHour = atoi(strHour);
	GPSTime.wMinute = atoi(strMinute);
	GPSTime.wSecond = atoi(strSecond);
	GPSTime.wMilliseconds = 0;

	SystemTimeToTzSpecificLocalTime(&info, &GPSTime, &NewTime);

	bl = NewTime.wSecond % 10;
	bh = NewTime.wSecond / 10;
	Buf[0] = t;
	bl = NewTime.wMinute % 10;
	bh = NewTime.wMinute / 10;
	Buf[1] = t;
	bl = NewTime.wHour % 10;
	bh = NewTime.wHour / 10;
	Buf[2] = t;
	bl = NewTime.wDay % 10;
	bh = NewTime.wDay / 10;
	Buf[3] = t;
	bl = NewTime.wMonth % 10;
	bh = NewTime.wMonth / 10;
	Buf[4] = t;
	bl = (NewTime.wYear - 2000) % 10;
	bh = (NewTime.wYear - 2000) / 10;
	Buf[5] = t;
		
	if(nIsValid == TRUE)
	{
		int NewTotalSec = NewTime.wHour*3600 + NewTime.wMinute*60 + NewTime.wSecond;
		int PCTotalSec = PCTime.wHour*3600 + PCTime.wMinute*60 + PCTime.wSecond;
		int DiffSecBetweenNewAndPC = 0;

		if( NewTotalSec > PCTotalSec )
			DiffSecBetweenNewAndPC = NewTotalSec - PCTotalSec;
		else
			DiffSecBetweenNewAndPC = PCTotalSec - NewTotalSec;

		if ( m_iCountForSendGPStime == 0 )
		{
			if( (NewTime.wYear != PCTime.wYear) || (NewTime.wMonth != PCTime.wMonth) ||
				(NewTime.wDay != PCTime.wDay) || DiffSecBetweenNewAndPC > 60 )
			{
				SetCommQue( WS_OPMSG_SETTIMER, &Buf[0], 6 );
				m_iCountForSendGPStime = 1;
			}
		}
		else
		{
			m_iCountForSendGPStime--;
		}
	}
}

void CMainFrame::OnSharedMemoryUpdated(WPARAM wParam, LPARAM lParam)
{
	OnSetVMem( &CommVMemData[0], pApp->nComRecordSize);
}


void CMainFrame::OnBtnScroll() 
{
	// TODO: Add your control notification handler code here
	
}
