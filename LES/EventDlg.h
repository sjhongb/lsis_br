#if !defined(AFX_EVENTDLG_H__8A8BF589_E93A_4E28_A938_46AEBE4C2CD9__INCLUDED_)
#define AFX_EVENTDLG_H__8A8BF589_E93A_4E28_A938_46AEBE4C2CD9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EventDlg.h : header file
//

#include "../INCLUDE/LOGFile.h"
#include "../INCLUDE/EventFile.h"
#include "../INCLUDE/GridCtrl.h"
#include "XPStyleButtonST.h"

/////////////////////////////////////////////////////////////////////////////
// CEventDlg dialog

class CEventDlg : public CDialog
{
// Construction
public:
	CEventDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEventDlg)
	enum { IDD = IDD_DIALOG_EVENT };
	CXPStyleButtonST	m_ctrlTitleBtn;
	CXPStyleButtonST	m_ctrlSearchBtn;
	CXPStyleButtonST	m_ctrlPrintBtn;
	CXPStyleButtonST	m_ctrlSaveBtn;
	CXPStyleButtonST	m_ctrlScrollBtn;
	//}}AFX_DATA
	CThemeHelperST		m_ThemeHelper;	//for XP Style button
	CGridCtrl			m_ctrlEventGrid;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL RefreshFilePath();
	void SetHidePosition();
	void RestoreEventRecords();
	int  AddNewRecord(BYTE *pVMem, int length, BOOL bFailTime = FALSE);

protected:

private:
	CObList m_EventList;
	void RemoveEventList();
	void SetButtonTooltip();
	void SetPosition();

	CLOGFile	m_LOGRecordFile;
	CEventFile  m_EventRecordFile;

	CString m_strLOGDir;
	CString m_strStnName;
	CString m_strFilePath;
	
	int m_nEventCount;
	BOOL m_bIsStarted;
	BOOL m_bIsCreated;
	BOOL m_bIsHidden;
	BOOL m_bIsAutoScroll;
	BYTE m_ucOrderInSec;
	LONG m_nLastTopPos;

	// Generated message map functions
	//{{AFX_MSG(CEventDlg)
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDoubleclickedButtonTitle();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnButtonSearch();
	afx_msg void OnButtonPrint();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonScroll();
	afx_msg void OnButtonTitle();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTDLG_H__8A8BF589_E93A_4E28_A938_46AEBE4C2CD9__INCLUDED_)
