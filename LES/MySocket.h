/////////////////////////////////////////////////////////////////////////////
// MySocket.h : header file
//

#ifndef	_MYSOCK_H_
#define	_MYSOCK_H_

#include <winsock.h>

// 소켓에 관련한 Notify들
#define	WSM_BASE		(WM_USER + 200)
#define	WSM_NOTIFY		(WSM_BASE + 0)
#define	WSM_ASYNC_EVENT	(WSM_BASE + 1)
#define	WSM_GOT_HOST	(WSM_BASE + 2)

/////////////////////////////////////////////////////////////////////////////
// Global function
// 다음에는 이 부분을 내부적으로 자동으로 수행되도록 고칠 것이다
BOOL MySockStart();
void MySockEnd();

/////////////////////////////////////////////////////////////////////////////
// CMySocket window

class CMySocket : public CWnd
{
// Construction
public:
	CMySocket(CWnd* pParentWnd, SOCKET s = INVALID_SOCKET);
	virtual ~CMySocket();

// Attributes
public:
	enum {
		SN_GOT_HOST = 1,
		SN_CONNECTED,
		SN_ACCEPTED,
		SN_CLOSED,
		SN_DATA_READY,
		SN_ERROR
	};
	int		GetLastError() { return	m_nLastError; }
	BOOL	IsConnected() { return	m_bConnected; }

protected:
	void	SetAsyncFlag(int nMode) { m_nAsyncFlag = nMode; }
	CWnd*	GetNotifyWnd() { return m_pParentWnd; }

private:
	SOCKADDR_IN		m_SockAddr;
	char	m_HostEnt[MAXGETHOSTSTRUCT];
	BOOL 	m_bConnected;
	int		m_nAsyncFlag;
	SOCKET	m_Socket;
	CWnd*	m_pParentWnd;
	int		m_nLastError;
	CObList	m_listSend;
	CObList	m_listReceive;

// Operations
public:
	BOOL Connect(CString strPeerName, int nPort);
	BOOL Bind(int nPort);
	BOOL Listen(int nBackLog = 1);
	BOOL Accept(SOCKET &s);

	BOOL WriteBlock(LPCSTR pBlock, int nLength);
	BOOL ReadBlock(LPSTR pBlock, int &nLength);
private:
	BOOL CreateSocket();

    // Socket Misc. operation
	BOOL AsyncSelect(int nMode);
	BOOL AsyncSelect();
	BOOL SetOption(int nOption, int nValue = 1);
	BOOL IsError(int nError);
	BOOL IsError();

protected:
    // Socket Main operation
	BOOL CloseSocket();
	void PostNotify(WORD nNotify, WORD nParam = 0);

	virtual BOOL OnConnect(int nErrorCode);
	virtual BOOL OnAccept(int nErrorCode);
	virtual BOOL OnCloseSocket(int nErrorCode);
	virtual BOOL OnDataReady(int nErrorCode);
	virtual BOOL OnSendAvailable(int nErrorCode);
	virtual BOOL OnOutOfBand(int nErrorCode);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySocket)
	//}}AFX_VIRTUAL

// Implementation
public:

	// Generated message map functions
protected:
	//{{AFX_MSG(CMySocket)
	afx_msg LONG OnGotHost(UINT wSocket, LONG lParam);
	afx_msg LONG OnAsyncEvent(UINT wSocket, LONG lParam);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CSockBlock
// 주고받는 데이터 블럭을 담아두기 위한 클래스
// 내부적으로만 사용된다.

class CSockBlock : public CObject
{
// Construction
public:
	CSockBlock(char* pBlock, int nLength);
	virtual ~CSockBlock();

// Attributes
public:
	operator int() { return m_nLength; }
	operator LPCSTR() { return (LPCSTR)m_pBlock; }
	int RemoveHead(int nLength);

protected:
	char* 	m_pBlock;
	int	    m_nLength;
};

#endif
/////////////////////////////////////////////////////////////////////////////
