// TextSet.cpp : implementation file
//

#include "stdafx.h"
#include "TextSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTextSet

IMPLEMENT_DYNAMIC(CTextSet, CDaoRecordset)

CTextSet::CTextSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTextSet)
	m_Station = _T("");
	m_StartDate = _T("");
	m_EndDate = _T("");
	m_Type = _T("");
	m_User = _T("");
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTextSet::GetDefaultDBName()
{
//	return _T("C:\\EISIII PACKAGE\\REPORT.MDB");
	return _T("REPORT.MDB");
}

CString CTextSet::GetDefaultSQL()
{
	return _T("[UserInfo]");
}

void CTextSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTextSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Station]"), m_Station);
	DFX_Text(pFX, _T("[StartDate]"), m_StartDate);
	DFX_Text(pFX, _T("[EndDate]"), m_EndDate);
	DFX_Text(pFX, _T("[Type]"), m_Type);
	DFX_Text(pFX, _T("[User]"), m_User);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTextSet diagnostics

#ifdef _DEBUG
void CTextSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTextSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
