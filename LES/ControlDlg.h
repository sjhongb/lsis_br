#if !defined(AFX_CONTROLDLG_H__448B7652_A1DA_405F_B8CC_72A12A857745__INCLUDED_)
#define AFX_CONTROLDLG_H__448B7652_A1DA_405F_B8CC_72A12A857745__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ControlDlg.h : header file
//
#include "MenuListBox.h"
/////////////////////////////////////////////////////////////////////////////
// CControlDlg dialog

class CControlDlg : public CDialog
{
// Construction
public:
	void OnResizeWindow();
	BYTE m_Type;	//1: 주진로 2:입환진로 3:선로전환기 4:신호기취소 5:구분진로해정 
	CControlDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_DlgTitle;
	HICON hIcon;
	int NumberOfItem;

// Dialog Data
	//{{AFX_DATA(CControlDlg)
	enum { IDD = IDD_DLG_CONTROL };
	CStatic	m_stIcon;
	CMenuListBox	m_ListItem;
	CString	m_stScript;
	CString	m_stState;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CControlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLDLG_H__448B7652_A1DA_405F_B8CC_72A12A857745__INCLUDED_)
