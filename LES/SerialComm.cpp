// SerialComm.cpp : implementation file
//

#include "stdafx.h"
#include "SerialComm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const BYTE abCrcHi[] =
{
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
	0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
	0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
	0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
	0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
	0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
	0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
	0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};

const BYTE abCrcLo[] =
{
	0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
	0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
	0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
	0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
	0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
	0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
	0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
	0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
	0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
	0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
	0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
	0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
	0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
	0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
	0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
	0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
	0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
	0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
	0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
	0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
	0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
	0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
	0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
	0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
	0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
	0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};


/////////////////////////////////////////////////////////////////////////////
// CSerialComm

IMPLEMENT_DYNCREATE(CSerialComm, CWinThread)

CSerialComm::CSerialComm()
{
	m_bIsRunning = FALSE;

	this->m_bAutoDelete = TRUE;
}

CSerialComm::~CSerialComm()
{
}

BOOL CSerialComm::InitInstance()
{
	// TODO:  perform and per-thread initialization here

	return TRUE;
}

int CSerialComm::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here

	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSerialComm, CWinThread)
	//{{AFX_MSG_MAP(CSerialComm)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialComm message handlers
BOOL CSerialComm::Start(CString strPort, UINT nBaudRate, UINT nParity, UINT nDataBit, UINT nStopBit, UINT bFlowCtrl)
{
	// COM10 이상일 경우 포트명의 앞에 "\\\\.\\"를 삽입하지 않으면 정상적으로 열리지 않는다.
	strPort = "\\\\.\\COM" + strPort;

	// Serial Port 오픈
	m_hComDev = CreateFile(strPort, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED, NULL);
	if(m_hComDev != INVALID_HANDLE_VALUE)
	{
		m_bIsRunning = TRUE;
	}
	else
	{
		return FALSE;
	}

	// 시리얼 포트를 설정값대로 초기화
	DWORD			err;
	COMMTIMEOUTS	CommTimeOuts;

	// 통신포트의 모든 에러를 리셋
	ClearCommError(m_hComDev, &err, NULL);
	// 통신포트의Input/Output MsgBuffer 사이즈를 설정
	//SetupComm(m_hComDev,InBufSize,OutBufSize);
	// 모든 Rx/Tx 동작을 제한하고 또한 MsgBuffer의 내용을 버림
	PurgeComm(m_hComDev, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);

	// set up for overlapped I/O (MSDN 참조)
	// 통신 선로상에서 한바이트가 전송되고 또한 바이트가 전송되기까지의 시간
	CommTimeOuts.ReadIntervalTimeout = MAXDWORD;
	// Read doperation 에서 TimeOut을 사용하지 않음
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	// CBR_9600 is approximately 1byte/ms. For our purposes, allow
	// double the expected time per character for a fudge factor.
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 1000;
	// 통신포트의 TimeOut을 설정
	SetCommTimeouts(m_hComDev, &CommTimeOuts);        

	DCB dcb;

	memset(&dcb, 0, sizeof(DCB));
	dcb.DCBlength = sizeof(DCB);

	// 통신포트의 DCB를 얻음
	GetCommState(m_hComDev, &dcb);

	// DCB를 설정(DCB: 시리얼통신의 제어 파라메터, MSDN 참조)
	dcb.fBinary = TRUE;
	dcb.fParity = TRUE;

	switch(nBaudRate)
	{
	case 110:
		dcb.BaudRate = CBR_110;
		break;
	case 300:
		dcb.BaudRate = CBR_300;
		break;
	case 600:
		dcb.BaudRate = CBR_600;
		break;
	case 1200:
		dcb.BaudRate = CBR_1200;
		break;
	case 2400:
		dcb.BaudRate = CBR_2400;
		break;
	case 4800:
		dcb.BaudRate = CBR_4800;
		break;
	case 9600:
		dcb.BaudRate = CBR_9600;
		break;
	case 14400:
		dcb.BaudRate = CBR_14400;
		break;
	case 19200:
		dcb.BaudRate = CBR_19200;
		break;
	case 38400:
		dcb.BaudRate = CBR_38400;
		break;
	case 57600:
		dcb.BaudRate = CBR_57600;
		break;
	case 115200:
		dcb.BaudRate = CBR_115200;
		break;
	case 128000:	
		dcb.BaudRate = CBR_128000;
		break;
	case 256000:
		dcb.BaudRate = CBR_256000;
		break;
	default:
		dcb.BaudRate = CBR_9600;
		break;
	}
	
	switch(nParity)
	{
	case 0:
		dcb.Parity = NOPARITY;
		break;
	case 1:
		dcb.Parity = ODDPARITY;
		break;
	case 2:
		dcb.Parity = EVENPARITY;
		break;
	default:
		dcb.Parity = NOPARITY;
	}

	dcb.ByteSize = nDataBit;
	if(dcb.ByteSize != 7 && dcb.ByteSize != 8)
	{
		dcb.ByteSize = 8;
	}

	switch(nStopBit)
	{
	case 1:
		dcb.StopBits = ONESTOPBIT;
		break;
	case 2:
		dcb.StopBits = TWOSTOPBITS;
		break;
	default:
		dcb.StopBits = ONESTOPBIT;
		break;
	}

#if 0
	dcb.fRtsControl = RTS_CONTROL_ENABLE;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	dcb.fOutxDsrFlow = FALSE;

	if(bFlowCtrl) 
	{
		dcb.fOutX = FALSE;
		dcb.fInX = FALSE;
		dcb.XonLim = 2048;
		dcb.XoffLim = 1024;
	}
	else
	{
		dcb.fOutxCtsFlow = TRUE;
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	}
#endif

	// 설정된 DCB로 통신포트의 제어 파라메터를 설정
	SetCommState(m_hComDev, &dcb);
	// Input MsgBuffer에 데이터가 들어왔을 때 이벤트가 발생하도록 설정
	SetCommMask(m_hComDev, EV_RXCHAR);

	// Read OVERLAPPED structure 초기화
	m_OLR.Offset = 0;
	m_OLR.OffsetHigh = 0;

	// Write OVERLAPPED structure 초기화
	m_OLW.Offset = 0;
	m_OLW.OffsetHigh = 0;
	
	// Overlapped 구조체의 이벤트를 생성
	m_OLR.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(m_OLR.hEvent == NULL) 
	{
		CloseHandle(m_OLR.hEvent);
		return FALSE;
	}

	m_OLW.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(m_OLW.hEvent == NULL) 
	{
		CloseHandle(m_OLW.hEvent);
		return FALSE;
	}

	// DTR (Data Terminal Ready) signal 을 보냄
	EscapeCommFunction(m_hComDev, SETDTR);

	ResumeThread();

	return TRUE;
}

int CSerialComm::Run() 
{
	// TODO: Add your specialized code here and/or call the base class
	DWORD			errCode;
	DWORD			EvtMask;
	DWORD			ErrorFlags;
	COMSTAT			ComStat;

	DWORD			dwRecvLen		= 0;
	DWORD			dwRecvReqLen	= 0;

	USHORT			nMsgLen			= 0;

	BYTE			MsgBuffer[MAX_BUFFER_LEN];

	memset(MsgBuffer, 0x00, MAX_BUFFER_LEN);

	while(m_bIsRunning)
	{
		EvtMask			= 0;
		dwRecvLen		= 0;
		dwRecvReqLen	= 0;

		// 이벤트를 기다림
		WaitCommEvent(m_hComDev, &EvtMask, NULL);
		ClearCommError(m_hComDev, &ErrorFlags, &ComStat);

		// EV_RXCHAR에서 이벤트가 발생하면
		if((EvtMask & EV_RXCHAR) && ComStat.cbInQue)
		{
			// 현재 남은 메시지 버퍼의 크기와 I/O 버퍼에 수신된 데이터의 크기를 비교하여
			// 수신 요청할 데이터 크기를 결정한다.
			if(ComStat.cbInQue > (USHORT)(MAX_BUFFER_LEN - nMsgLen))
			{
				dwRecvReqLen = MAX_BUFFER_LEN - nMsgLen;
			}
			else
			{
				dwRecvReqLen = ComStat.cbInQue;
			}

			ClearCommError(m_hComDev, &ErrorFlags, &ComStat);

			if(ReadFile(m_hComDev, &MsgBuffer[nMsgLen], dwRecvReqLen, &dwRecvLen, &m_OLR) == 0) 
			{
				errCode = GetLastError();
				if(errCode == ERROR_IO_PENDING)
				{
					if(WaitForSingleObject(m_OLR.hEvent, 1000) != WAIT_OBJECT_0)
					{
						dwRecvLen = 0;
					}
					else
					{
						GetOverlappedResult(m_hComDev, &m_OLR, &dwRecvLen, FALSE);
					}
				}
				else
				{
					TRACE("ReadFile Error. errCode = %d\n", errCode);	// 진짜 에러
					dwRecvLen = 0;
				}
			}
			else
			{
				nMsgLen = nMsgLen + (USHORT)dwRecvLen;

				// 가상 함수 호출
				ReceiveMsg(MsgBuffer, nMsgLen);
			}

			ClearCommError(m_hComDev, &ErrorFlags, &ComStat);
		}
	}

	TRACE("Exit Thread Loop.\n");

	CloseHandle(m_hComDev);
//	CloseHandle(m_OLR.hEvent);
//	CloseHandle(m_OLW.hEvent);

	return TRUE;
	//return CWinThread::Run();
}

void CSerialComm::End()
{
	// SUSPENDED 상태일 경우를 고려해서 Resume을 한번 수행한다.
	ResumeThread();

	SetCommMask(m_hComDev, 0);
	EscapeCommFunction(m_hComDev, CLRDTR);
	PurgeComm(m_hComDev, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);

	m_bIsRunning = FALSE;

	WaitForSingleObject(m_hThread, INFINITE);

	TRACE("Thread Terminated.\n");
}

BOOL CSerialComm::SendMsg(BYTE *pBuffer, USHORT nMsgLen)
{
	BOOL	bRet = TRUE;

	DWORD	errCode;
	DWORD	ErrorFlags;
	COMSTAT	ComStat;

	DWORD	dwSentLen	= 0;

	if(!m_bIsRunning)
	{
		return FALSE;
	}

	// 통신 포트의 모든 에러를 리셋
	ClearCommError(m_hComDev, &ErrorFlags, &ComStat);

	// overlapped I/O를 통하여 outbuf의 내용을 len길이 만큼 전송
	if(WriteFile(m_hComDev, pBuffer, nMsgLen, &dwSentLen, &m_OLW) == 0) 
	{
		errCode = GetLastError();
		if(errCode == ERROR_IO_PENDING)
		{
			if(WaitForSingleObject(m_OLW.hEvent, 1000) != WAIT_OBJECT_0)
			{
				bRet = FALSE;
			}
			else
			{
				GetOverlappedResult(m_hComDev, &m_OLW, &dwSentLen, FALSE);
			}
		}
		else /* I/O error */
		{
			bRet = FALSE;
		}
	}

	//FlushFileBuffers(m_hComDev);
	// 다시 한번 통신포트의 모든 에러를 리셋
	ClearCommError(m_hComDev, &ErrorFlags, &ComStat);

	return bRet;
}

// 수신 메시지 처리는 프로토콜별로 상이하므로 Base Class의 Method에서 처리할 내용은 없다.
void CSerialComm::ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen)
{
}

WORD CSerialComm::CalcCRC16(BYTE *pMsg, USHORT nLen)
{
	BYTE bCrcHi = 0xFF;
	BYTE bCrcLo = 0xFF;
	USHORT nIndex;

    while( nLen-- )
    {
		nIndex = bCrcHi ^ *pMsg++;
		bCrcHi = bCrcLo ^ abCrcHi[nIndex];
		bCrcLo = abCrcLo[nIndex];
    }

    return (bCrcHi << 8 | bCrcLo);
}

void CSerialComm::EncodeDLE(BYTE *pRawMsg, USHORT nRawLen, BYTE *pDleMsg, USHORT *pnDleLen)
{
	USHORT	nRawPos = 0, nDlePos = 0;
	
	// STX는 그대로 복사한다.
	pDleMsg[0] = pRawMsg[0];

	for(nRawPos = 1, nDlePos = 1; nRawPos < nRawLen - 1 ; nRawPos++, nDlePos++)
	{
		switch(pRawMsg[nRawPos])
		{
		case 0x02:
			pDleMsg[nDlePos++] = 0x80;
			pDleMsg[nDlePos] = 0x82;
			break;

		case 0x03:
			pDleMsg[nDlePos++] = 0x80;
			pDleMsg[nDlePos] = 0x83;
			break;

		case 0x80:
			pDleMsg[nDlePos++] = 0x80;
			pDleMsg[nDlePos] = 0x80;
			break;
		
		default:
			pDleMsg[nDlePos] = pRawMsg[nRawPos];
			break;
		}
	}

	// ETX는 그대로 복사한다.
	pDleMsg[nDlePos++] = pRawMsg[nRawPos];

	// DLE Encoding된 메시지의 길이를 돌려준다.
	*pnDleLen = nDlePos;
}

void CSerialComm::DecodeDLE(BYTE *pDleMsg, USHORT nDleLen, BYTE *pRawMsg, USHORT *pnRawLen)
{
	USHORT	nRawPos = 0, nDlePos = 0;

	// STX는 그대로 복사한다.
	pRawMsg[0] = pDleMsg[0];

	for(nDlePos = 1, nRawPos = 1 ; nDlePos < nDleLen - 1 ; nDlePos++, nRawPos++)
	{
		if(pDleMsg[nDlePos] == 0x80)
		{
			switch(pDleMsg[nDlePos + 1])
			{
			case 0x80:
				nDlePos++;
				pRawMsg[nRawPos] = 0x80;
				break;
			
			case 0x82:
				nDlePos++;
				pRawMsg[nRawPos] = 0x02;
				break;

			case 0x83:
				nDlePos++;
				pRawMsg[nRawPos] = 0x03;
				break;

			default:
				TRACE("Single 0x80 detected in DLE Encoded Message. Unknown Error!!\n");
				pRawMsg[nRawPos] = 0x80;
				break;
			}
		}
		else
		{
			pRawMsg[nRawPos] = pDleMsg[nDlePos];
		}
	}

	// ETX는 그대로 복사한다.
	pRawMsg[nRawPos++] = pDleMsg[nDlePos];

	// DLE Encoding된 메시지의 길이를 돌려준다.
	*pnRawLen = nRawPos;
}
