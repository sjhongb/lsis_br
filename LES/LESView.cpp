//=======================================================
//==              LESView.cpp
//=======================================================
//  파 일 명 :  LESView.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LESView.h"
#include "MainFrm.h"
#include "../include/UserDataType.h"
#include "../include/MessageInfo.h"
#define PORT 5001
#define _UNKNOWN 0
#define _SERVER 1
#define _CLIENT 2

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////
// CLESView

IMPLEMENT_DYNCREATE(CLESView, CFormView)

BEGIN_MESSAGE_MAP(CLESView, CFormView)
	//{{AFX_MSG_MAP(CLESView)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLESView construction/destruction

BEGIN_EVENTSINK_MAP(CLESView, CFormView)
    //{{AFX_EVENTSINK_MAP(CEIDView)
	ON_EVENT(CLESView, IDC_LSMCTRL, 2 /* OleSendMessage */, OnOleSendMessageLSMctrl, VTS_I4 VTS_I4 VTS_I4)
	ON_EVENT(CLESView, IDC_LSMCTRL, 1 /* Operate */, OnOperateLSMctrl, VTS_I2 VTS_I2)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

//=======================================================
//
//  함 수 명 :  CLESView
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CLESView::CLESView()
	: CFormView(CLESView::IDD)
{
	//{{AFX_DATA_INIT(CLESView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	pApp = (CLESApp*)AfxGetApp();
//	m_ServerSocket = NULL;
//	m_ClientSocket = NULL;		//LAN통신 삭제.
}

//=======================================================
//
//  함 수 명 :  ~CLESView
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CLESView::~CLESView()
{
//	OnDisconnect();	
}

//=======================================================
//
//  함 수 명 :  DoDataExchange
//  함수출력 :  없음
//  함수입력 :  CDataExchange* pDX
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  화면 객체와의 데이터 교환 리스트
//
//=======================================================
void CLESView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLESView)

	//}}AFX_DATA_MAP
}

//=======================================================
//
//  함 수 명 :  OnInitialUpdate()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  View 를 초기화 한다. 
//
//=======================================================
void CLESView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	CLESDoc* pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	pWnd->m_bDlgDisplaed = TRUE;

	BOOL bLoadOK = TRUE;		
	DWORD dwStyle = WS_CHILD;
    CRect rect;
	GetClientRect( &rect );
	m_Screen.Create( (LPCTSTR)(pApp->m_SystemValue.strStationFileName), dwStyle, rect, this, IDC_LSMCTRL ); 

	if ( pApp->m_SystemValue.nSystemType == 2 && pApp->m_SystemValue.strStationFileName == "AKB" ) { 
		m_Screen.DisplayClock(FALSE); // AKB WALL MOUNT 용 일경우 시계를 표시하지 않는다.
	}
	if ( pApp->m_SystemValue.nSystemType == 2 && pApp->m_SystemValue.strStationFileName == "AKA" ) { 
		m_Screen.DisplayBG(1);  //WALL MOUNT 용일 경우만 무정보 궤도를 표시한다.
	} else if ( pApp->m_SystemValue.nSystemType == 2 && pApp->m_SystemValue.strStationFileName == "AKB" ) { 
		m_Screen.DisplayBG(2);  //WALL MOUNT 용일 경우만 무정보 궤도를 표시한다.
	} else {
		m_Screen.DisplayBG(0);  //WALL MOUNT 용일 경우만 무정보 궤도를 표시한다.
	}
	if ( IsWindow(m_Screen.m_hWnd) ){ 
		m_Screen.ShowWindow( SW_SHOW );
		long wParam = (long)(LPCTSTR)(pApp->m_SystemValue.strStationFileName);
		long lParam = (long)(LPCTSTR)(pApp->m_SystemValue.strLSMResolution);	
		
		m_Screen.MoveWindow( 0, 0, pApp->m_SystemValue.nMainSizeX, pApp->m_SystemValue.nMainSizeY);

		char *errstr = (char*)m_Screen.OlePostMessage(WS_OPMSG_LOADFILE, wParam, lParam );

		if ( errstr ) {
			MessageBox( errstr );
		}
		else {
			pApp->m_pEipEmu = (CEipEmu*)m_Screen.OlePostMessage( WS_OPMSG_GETEMUPTR, 0, 0 );		//get EipEmp class ptr
			CMessageInfo *pScrInfo = (CMessageInfo*)m_Screen.OlePostMessage( WS_OPMSG_GETSCRINFO, 0, 0 );

			if(pApp->m_pEipEmu && pScrInfo){
				int nTrack  = pApp->m_pEipEmu->m_strTrack.GetCount();		
				int nSignal = pApp->m_pEipEmu->m_strSignal.GetCount();		
				int nSwitch = pApp->m_pEipEmu->m_strSwitch.GetCount();		
				int nButton = pApp->m_pEipEmu->m_strButton.GetCount();		

				CString strTemp;
				if (!nTrack || (nTrack > 255)) {
					strTemp.Format("%d",nTrack);
					AfxMessageBox(strTemp);
					MessageBox("Warning!!\n\ntrack count error. check data file or LSM.OCX's version", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (!nSignal || (nSignal > 255)) {
					MessageBox("Warning!!\n\nsignal count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (nSwitch > 255) {
					MessageBox("Warning!!\n\npoint count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (nButton > 255) {
					MessageBox("Warning!!\n\nbutton count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}

				if ( bLoadOK == TRUE ) {		//OK 
					int nMaxInfo = nTrack + nSignal + nSwitch + nButton ;		//total number of unit 
					int nSizeRecord = (nTrack * SIZE_INFO_TRACK) + (nSignal * SIZE_INFO_SIGNAL) 
															 + (nSwitch * SIZE_INFO_SWITCH);
					
					pApp->nMaxTrack  = nTrack;
					pApp->nMaxSignal = nSignal;
					pApp->nMaxSwitch = nSwitch;
					pApp->nMaxButton = nButton;
					pApp->nSfmRecordSize = nSizeRecord + sizeof(DataHeaderType);
					pApp->nComRecordSize = pApp->nSfmRecordSize + MAX_OPERATE_CMD_SIZE + MAX_USER_SIZE + MAX_PASS_SIZE + USER_CMD_SIZE + IPM_VERSION_SIZE;		// _nSfmRecordSize + LCCCommand(8) + IPMMessage(8) + User관련 (44)
					pApp->nVerRecordSize = pApp->nComRecordSize + sizeof(CJTime);							

					pApp->m_RecordMaskInfo.nTrack = nTrack;
					pApp->m_RecordMaskInfo.nSignal= nSignal;
					pApp->m_RecordMaskInfo.nSwitch= nSwitch;
					pApp->m_RecordMaskInfo.nButton= nButton;

					//데이터 블럭 할당
					if ( nTrack ) 
						pApp->m_RecordMaskInfo.pTrack  = new char [nTrack];
					if ( nSignal )
						pApp->m_RecordMaskInfo.pSignal = new char [nSignal];
					if ( nSwitch )
						pApp->m_RecordMaskInfo.pSwitch = new char [nSwitch];
					if ( nButton )
						pApp->m_RecordMaskInfo.pButton = new char [nButton];

					// 데이터 COPY
					if ( pApp->m_RecordMaskInfo.pTrack )
						memset(pApp->m_RecordMaskInfo.pTrack, 1, nTrack);
					if ( pApp->m_RecordMaskInfo.pSignal )
						memset(pApp->m_RecordMaskInfo.pSignal, 1, nSignal);
					if ( pApp->m_RecordMaskInfo.pSwitch )
						memset(pApp->m_RecordMaskInfo.pSwitch, 1, nSwitch);
					if ( pApp->m_RecordMaskInfo.pButton )
						memset(pApp->m_RecordMaskInfo.pButton, 1, nButton);
					
					pWnd->m_RealLOGMessage.CreateInfo( pScrInfo, nMaxInfo );	// 전역변수 Info_pScrMessageInfo에 set ( m_strName, m_pEipData, m_nMemOffset, m_nType, m_nID)
					pWnd->m_RealLOGMessage.CreateCardInfo();

					ParamType wData;
					wData.ndl = pApp->m_SystemValue.nOption;
					wData.ndh = 0;
					m_Screen.OlePostMessage( WS_OPMSG_SETLSMOPTION, wData.wParam, 0);		//LSM Option
				}
			} //end of if
			
			else {
				MessageBox( "EIPEMU or SCREEN INFO FAILED for LSM.OCX", MB_OK,MB_ICONSTOP );
			}

		}//end of else
	}//end of if
	else MessageBox( "Creation of main screen is failed.", MB_OK,MB_ICONSTOP);

	m_iClientCnt = 0;
	pApp->m_iConCnt = m_iClientCnt;
	SetTimer ( 1, 1000,NULL);
	SetTimer ( 3, 1000, NULL );

	CString strFlagOfSyncWithLSM = "";
	while(1)
	{
		strFlagOfSyncWithLSM = pApp->GetFlagOfSyncWithLSM();
		
		if ( strFlagOfSyncWithLSM == "TRUE")
		{
			pApp->GetVerifyObject();
			break;
		}
		for (int i; i<100; i++)
		{
			int abc = 0;
		}
	}
	
	CSplashWnd::ShowSplashScreen(AfxGetMainWnd());
}

#ifdef _DEBUG
void CLESView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLESView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CLESDoc* CLESView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLESDoc)));
	return (CLESDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLESView message handlers

//=======================================================
//
//  함 수 명 :  OnOperateLSMctrl
//  함수출력 :  없음
//  함수입력 :  short nFunction, short nArgument
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :
//
//=======================================================
void CLESView::OnOperateLSMctrl(short nFunction, short nArgument) 
{
	// TODO: Add your control notification handler code herehere
	BYTE *p = (BYTE*)&m_CmdQue[0];
	*p++ = (char)nFunction;
	*(short*)p = sizeof(short);
	p += sizeof(short);
	*(short*)p = nArgument;

	if ( nFunction == (short)WS_OPMSG_TRKSEL ) {
		if ( !(pApp->m_SystemValue.nProcOption & PROC_ON_SETTRACK) ) {
			return;
		}
	}

	LPARAM lParam = (LPARAM)&m_CmdQue[0];
	pApp->m_pMainWnd->PostMessage(WM_COMMAND, ID_COMM_CMD_WRITE, lParam);
}

//=======================================================
//
//  함 수 명 :  OnOleSendMessageLSMctrl
//  함수출력 :  없음
//  함수입력 :  long message, long wParam, long lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LSM 로 메세지를 전송한다. 
//
//=======================================================
void CLESView::OnOleSendMessageLSMctrl(long message, long wParam, long lParam) 
{
	short n = 0;
	BYTE *s;
	BYTE *p = (BYTE*)&m_CmdQue[0];

	*p++ = (BYTE)message;
	p += sizeof(short);
	switch ( message ) {
	case WS_OPMSG_SETTRAINNAME	:	//4		// 열차 번호 지정 [track ID(BYTE), train no(5byte)]
									*p++ = (BYTE)wParam;
									s = (BYTE*)lParam;
									for(n=1; n<6; n++) {
									*p++ = *s++;
									}
									break;
	case WS_OPMSG_SIGNALDEST	:	//5		// 신호 (폐색 포함)취급 [signal ID(byte), button ID(byte)]
	case WS_OPMSG_SIGNALCANCEL	:	//6		// 신호 (폐색 포함)취소 [signal ID(byte)]
	case WS_OPMSG_SWTOGGLE		:	//9		// 선로전환기 토글(toggle) [switch ID(byte)]
	case WS_OPMSG_TRKSEL		:	//37		// 궤도 선택
									if ( pApp->m_SystemValue.nProcOption & PROC_ON_SETTRACK ) {
										*(short*)p = (short)wParam;
										n = sizeof( short );
									}
									break;
	case WS_OPMSG_LOS			:
	case WS_OPMSG_LOS_ON		:   //message = WS_OPMSG_LOS;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_LOS_OFF		:   //message = WS_OPMSG_LOS;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_POS_ON		:   //message = WS_OPMSG_NOUSE;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_POS_OFF		:   //message = WS_OPMSG_NOUSE;		//2013.01.30 의미없는 짓을 왜했을까??
//	case WS_OPMSG_PAS			:
	case WS_OPMSG_PAS_IN		:
	case WS_OPMSG_PAS_OUT		:
	case WS_OPMSG_OSS			:
	case WS_OPMSG_BUTTON		:	//10	// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
	case WS_OPMSG_SIGNALSTOP	:
	case WS_OPMSG_SLS			:
	case WS_OPMSG_ATB			:
	case WS_OPMSG_ND			:
	case WS_OPMSG_ALARMCLR		:
	case WS_OPMSG_NOUSE         :
	case WS_OPMSG_BUZZERCLR		:
	case WS_OPMSG_UTG_CA		:
	case WS_OPMSG_UTG_LCR		:
	case WS_OPMSG_UTG_CBB		:
	case WS_OPMSG_UTC_CA		:
	case WS_OPMSG_UTC_LCR		:
	case WS_OPMSG_UTC_CBB		:
	case WS_OPMSG_DTG_CA		:
	case WS_OPMSG_DTG_LCR		:
	case WS_OPMSG_DTG_CBB		:
	case WS_OPMSG_DTC_CA		:
	case WS_OPMSG_DTC_LCR		:
	case WS_OPMSG_DTC_CBB		:
	case WS_OPMSG_HSW			:
	case WS_OPMSG_CAON			:
	case WS_OPMSG_LCROP			:
	case WS_OPMSG_CBBOPEMER		:   //message = WS_OPMSG_CBBOP;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_CBBOP		    :
	case WS_OPMSG_BLOCK_CLEAR	:
	case WS_OPMSG_START			:	//1		// 시스템 기동
	case WS_OPMSG_RESET			:	//2		// 시스템 재기동
	case WS_OPMSG_MARKINGTRACK	:	//14	// 궤도 사용금지/해제(모타카, 사용금지, 단선) [mode(byte), track ID(byte)]
	case WS_OPMSG_FREESUBTRACK	:	//15	// 구분진로비상해정 [track ID(byte)]
	case WS_OPMSG_AXL_RESET		:	//73	// AXL_reset제어.
	case WS_OPMSG_TRACK_RESET	:
									*(short*)p = (short)wParam;
									n = sizeof( short );
									p += n;
									*(long*)p = lParam;
									break;
									// Function Call Message
	case WS_OPMSG_SETLSMSIZE	:	//35		// LSM SIZE (resizing) [x(short), y(short)]
									*(long*)p = (long)wParam;
									n = sizeof( long );
									break;
	case WS_OPMSG_CMDCLEAR		:
									*(short*)p = 0;
									n = sizeof( short );
									break;
	default						:	return;
	}

	*(short*)&m_CmdQue[1] = n;
	lParam = (long)&m_CmdQue[0];

	pApp->m_pMainWnd->PostMessage(WM_COMMAND, ID_COMM_CMD_WRITE, (LPARAM)lParam);
}

//=======================================================
//
//  함 수 명 :  PreTranslateMessage
//  함수출력 :	없음
//  함수입력 :  MSG* pMsg
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사용자의 단축키 입력에 대응한다. 
//
//=======================================================
BOOL CLESView::PreTranslateMessage(MSG* pMsg) 
{
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	
	if ((pMsg->message == WM_KEYDOWN) || (pMsg->message == WM_SYSKEYDOWN)) {
		switch(pMsg->wParam) {
		case VK_F1:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_RUN);
				}
				else{
					m_pMainFrame->OnF1();
					return TRUE;
				}
			}
			break;
		case VK_F2:		// About
//			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {					// PAS상태에서도 키보드 제어만으로 PAS를 풀기위해 주석처리.
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_RESET);
				}
				else{
					m_pMainFrame->OnF2();
					return TRUE;
				}
//			}
			break;
		case VK_F3:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_TIMESET);
				}
				else{
					m_pMainFrame->OnF3();
					return TRUE;
				}
			}
			break;
		case VK_F4:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_EXIT);
				}
				else{
					m_pMainFrame->OnF4();
					return TRUE;
				}
			} else {
				return TRUE;
			}
			break;
		case VK_F5:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_APP_ABOUT);
				}
				else if ( pApp->m_bISCTCMode == FALSE )
				{
					m_pMainFrame->OnF5();
					return TRUE;
				}
			}
			break;
		case VK_F6:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ALLUSER);
				}
				else if ( pApp->m_bISCTCMode == FALSE )
				{
					m_pMainFrame->OnF6();
					return TRUE;
				}
			}
			break;
		case VK_F7:		// About
			if((pMsg->lParam&0x20000000)==0x20000000){
				m_pMainFrame->PostMessage(WM_COMMAND,ID_CHANGE);
			}
			else if ( pApp->m_bISCTCMode == FALSE )
			{
				m_pMainFrame->OnF7();
				return TRUE;
			}
			break;
		case VK_F8:		// About
			if((pMsg->lParam&0x20000000)==0x20000000){
				m_pMainFrame->PostMessage(WM_COMMAND,ID_LOCK);
			}
			else if ( pApp->m_bISCTCMode == FALSE )
			{
				m_pMainFrame->OnF8();
				return TRUE;
			}
			break;
		case VK_F9:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_SLS);
				}
				else{
					m_pMainFrame->OnF9();
					return TRUE;
				}
			}
			break;
		case VK_F10:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ATB);
				}
				else{
					m_pMainFrame->OnF10();
					return TRUE;
				}
			}
			break;
		case VK_F11:		// About
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->PostMessage(WM_COMMAND,ID_ND);
				}
				else{
					m_pMainFrame->OnF11();
					return TRUE;
				}
			}
			break;
		case 0x31:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On1();
					return TRUE;
				}
			}
			break;
		case 0x32:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On2();
					return TRUE;
				}
			}
			break;
		case 0x33:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On3();
					return TRUE;
				}
			}
			break;
		case 0x34:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On4();
					return TRUE;
				}
			}
			break;
		case 0x35:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On5();
					return TRUE;
				}
			}
			break;
		case 0x36:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On6();
					return TRUE;
				}
			}
			break;
		case 0x37:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On7();
					return TRUE;
				}
			}
			break;
		case 0x38:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On8();
					return TRUE;
				}
			}
			break;
		case 0x39:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On9();
					return TRUE;
				}
			}
			break;
		case 0x30:		 //1
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->On0();
					return TRUE;
				}
			}
			break;
		case 'e':
		case 'E':
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->OnAltPlusE();
					return TRUE;
				}
			}
			break;
		case 'l':
		case 'L':
			if ( pApp->m_SystemValue.bCtrlLock == 0 && pApp->m_bISCTCMode == FALSE ) {
				if((pMsg->lParam&0x20000000)==0x20000000){
					m_pMainFrame->OnAltPlusL();
					return TRUE;
				}
			}
			break;
		default:	
			break;
		}
	}	
	return CFormView::PreTranslateMessage(pMsg);
}

//=======================================================
//
//  함 수 명 :  OnCommand
//  함수출력 :  BOOL
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메세지의 수신에 대응한다. 
//
//=======================================================
BOOL CLESView::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	UINT commandID = (UINT)wParam;
	switch (commandID) {
	case ID_COMMAND_UPDATE_VIEW_1:
		m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, (long)(lParam), (long)(&pApp->SystemVMEM[0]) );
		return TRUE;
	}	
	return CFormView::OnCommand(wParam, lParam);
}
/*void CLESView::StartTimer() 
{
	pApp->m_SystemValue.bLanUse = TRUE;
//	OnConnect();			//LAN통신 사용안함.
}
*/							//LAN통신 사용안함.
 
void CLESView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handher code here and/or call default
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	if ( nIDEvent == 1 ) {
//		OnSend ( 0x03 );			//LAN통신 사용안함.
	} else if ( nIDEvent == 2 ) {
		KillTimer(2);
//		OnConnect();				//LAN통신 사용안함.
	} else if ( nIDEvent == 3 ) {
		KillTimer(nIDEvent);
		pWnd->CreateObj();
		if ( pApp->m_SystemValue.nSystemType == 0 ) {
			SetTimer (4,1000,NULL);
			pWnd->m_MenuView.ShowWindow(SW_SHOW);
		} else {
			pWnd->m_MenuView.ShowWindow(SW_HIDE);			
		}
	} else if ( nIDEvent == 4 ) {
		KillTimer(4);
		pWnd->OnMenuLockUpdate();
		pWnd->ChangeUser(); // LOGIN
	}
	CFormView::OnTimer(nIDEvent);
}

void CLESView::UserIDUpdate(char Buffer[2048]) 
{
	// TODO: Add your control notification handler code here
	UserDataType		tempUserValue;
	memcpy ( &tempUserValue , Buffer , sizeof(tempUserValue) );
	memcpy ( &pApp->m_UserValue , Buffer , sizeof(pApp->m_UserValue) );
	pApp->OnSaveUserID();
}
/*		//LAN통신 삭제.
void CLESView::OnSend( int nOP) 
{
	// TODO: Add your control notification handler code here
	memcpy ( &m_strSend , &pApp->m_UserValue , sizeof(pApp->m_UserValue) );
	m_strSend[0] = (char)0x0A;
	m_strSend[1] = (char) nOP;

	if ( m_iSysType == _SERVER ) {
		if (m_ServerSocket)	{
			m_ServerSocket->SendClientAll((LPSTR(LPCSTR(m_strSend))),sizeof(pApp->m_UserValue));
		}
	} else if ( m_iSysType == _CLIENT ) {
		if (m_ClientSocket)	{
			m_ClientSocket->Send((LPSTR(LPCSTR(m_strSend))),sizeof(pApp->m_UserValue));
		}
	}
}

//////////////////////////////////////////////////////////////////
void CLESView::OnConnect() 
{

	if ( pApp->m_SystemValue.nSystemType != 0 ) return;
	if ( pApp->m_SystemValue.bLanUse == FALSE ) {
		m_iSysType = _UNKNOWN;
		return;
	}

	CString strIP;
	UINT iPort;
	iPort = 5001;
	if ( pApp->m_SystemValue.nSystemNo == 1 ) {
		strIP = "165.244.150.82";
	} else if ( pApp->m_SystemValue.nSystemNo == 2 ) {
		strIP = "165.244.150.81";
	}
	if ( pApp->m_SystemValue.nSystemNo == 1 && m_ServerSocket == NULL) {
		m_ServerSocket = new CServerSocket;
		if (!m_ServerSocket->Create(this, iPort)) {
			delete m_ServerSocket;
			m_ServerSocket = NULL;
			SetTimer (2,5000,NULL);
			m_iClientCnt++;
			pApp->m_iConCnt = m_iClientCnt;
			m_iSysType = _UNKNOWN;
		} else {
			m_iSysType = _SERVER;
			pApp->m_iConType = _SERVER;
		}
	} else if ( pApp->m_SystemValue.nSystemNo == 2 && m_ClientSocket == NULL ) {
		m_ClientSocket = new CClientSocket;
		if (!m_ClientSocket->Create((LPSTR(LPCSTR(strIP))), iPort, this)) {
			delete m_ClientSocket;
			m_ClientSocket = NULL;
			SetTimer (2,5000,NULL);
			m_iClientCnt++;
			pApp->m_iConCnt = m_iClientCnt;
			m_iSysType = _UNKNOWN;
		} else {
			m_iSysType = _CLIENT;
			pApp->m_iConType = _CLIENT;
		}
	}

}

LRESULT CLESView::OnClientClose(WPARAM wParam, LPARAM lClient)
{
	OnDisconnect();
	return 0;
}

void CLESView::OnDisconnect() 
{
	if ( m_iSysType == _SERVER ) {
		delete m_ServerSocket;
		m_ServerSocket = NULL;
		SetTimer (2,5000,NULL);
	} else if ( m_iSysType == _CLIENT ) {
		delete m_ClientSocket;
		m_ClientSocket = NULL;
		SetTimer (2,5000,NULL);
	}
}

LRESULT CLESView::OnClientAccept(WPARAM wParam, LPARAM lClient)
{
	return 0;
}

// Client로 부터 수신되는 데이터
LRESULT CLESView::OnClientReceive(WPARAM wParam, LPARAM lClient)
{
	char buf[2048];
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	if ( m_iSysType == _SERVER ) {
		CServerItem* ServerItem = (CServerItem*)lClient;
		memcpy(buf, ServerItem->m_byteReceive, 2048);
		buf[ServerItem->m_nReceiveLen] = 0;
//		SetTimer (5,60000,NULL);
	} else if ( m_iSysType == _CLIENT ) {
		memcpy(buf, m_ClientSocket->m_byteReceive, 2048);
		buf[m_ClientSocket->m_nReceiveLen] = 0;
	}

	if ( buf[0] == 0x0A ) {
		switch ( buf[1] ) {
		case  0x01 : UserIDUpdate( buf ); // USER 관련 업데이트
					 pWnd->ChangeUser();  // LOGIN 창 띄움
			         break;
		case  0x02 : pWnd->ChangeUser();  // LOGIN 창 띄움
			         break;
		case  0x03 : break;				  // AYT
		case  0x04 : break;				  // TimeSet
		}
	}
	return 0;
}
*/		//LAN통신 삭제.

void CLESView::OnFilePrintPreview()
{
	CView::OnFilePrintPreview();
}

BOOL CLESView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	return DoPreparePrinting(pInfo);
}

void CLESView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCtrl->OnBeginPrinting(pDC, pInfo);
}

void CLESView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCtrl->OnPrint(pDC, pInfo);
}

void CLESView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pCtrl->OnEndPrinting(pDC, pInfo);
}

void CLESView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	// TODO: Add your specialized code here and/or call the base class
	CFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
// 	// Show the original frame
// 	m_pOldFrame->ShowWindow(SW_SHOW);
// 	// Restore main frame pointer
// 	AfxGetApp()->m_pMainWnd=m_pOldFrame;
 	m_pCtrl->m_bPrintPreview=FALSE;
// 	// Kill parent frame and itself
// 	GetParentFrame()->DestroyWindow();
}

void CLESView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	CLESApp *pApp = (CLESApp*)AfxGetApp();

	delete pApp->m_RecordMaskInfo.pTrack;
	delete pApp->m_RecordMaskInfo.pSignal;
	delete pApp->m_RecordMaskInfo.pSwitch;
	delete pApp->m_RecordMaskInfo.pButton;
}
