// EventMgrDoc.h : interface of the CEventMgrDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EVENTMGRDOC_H__3AEB6759_97DD_4679_9994_85B1139D2D21__INCLUDED_)
#define AFX_EVENTMGRDOC_H__3AEB6759_97DD_4679_9994_85B1139D2D21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CEventMgrDoc : public CDocument
{
protected: // create from serialization only
	CEventMgrDoc();
	DECLARE_DYNCREATE(CEventMgrDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventMgrDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEventMgrDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CEventMgrDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTMGRDOC_H__3AEB6759_97DD_4679_9994_85B1139D2D21__INCLUDED_)
