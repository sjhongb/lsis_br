// EventMgrView.h : interface of the CEventMgrView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EVENTMGRVIEW_H__E1230B0A_6E4B_4B22_ABD2_C552F575FF60__INCLUDED_)
#define AFX_EVENTMGRVIEW_H__E1230B0A_6E4B_4B22_ABD2_C552F575FF60__INCLUDED_

#include "../INCLUDE/GridCtrl.h"	// Added by ClassView
#include "../INCLUDE/EventFile.h"	// Added by ClassView
#include "FilterDlg.h"	// Added by ClassView
#include "../LES/XPStyleButtonST.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEventMgrView : public CFormView
{
protected: // create from serialization only
	CEventMgrView();
	DECLARE_DYNCREATE(CEventMgrView)

public:
	//{{AFX_DATA(CEventMgrView)
	enum { IDD = IDD_EVENTMGR_FORM };
	CXPStyleButtonST	m_ctrlQueryBtn;
	CXPStyleButtonST	m_ctrlFilterBtn;
	CXPStyleButtonST	m_ctrlSaveBtn;
	CXPStyleButtonST	m_ctrlPreviewBtn;
	CXPStyleButtonST	m_ctrlPrintBtn;
	CDateTimeCtrl		m_ctrlStartDT;
	CDateTimeCtrl		m_ctrlEndDT;
	//}}AFX_DATA
	CThemeHelperST		m_ThemeHelper;	//for XP Style button

// Attributes
public:
	CGridCtrl m_ctrlEventGrid;
	CEventMgrDoc* GetDocument();

// Operations
public:
	void Initialize(BOOL bMode, CString &strLOGPath, CString &strStnName);
	void RefreshGridCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventMgrView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	CFilterDlg m_FilterDlg;
	virtual ~CEventMgrView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CEventMgrView)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonFilter();
	afx_msg void OnFileSaveAs();
	afx_msg void OnButtonQuery();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	DECLARE_MESSAGE_MAP()
private:
	void RemoveEventList();
	CJTime m_StartTime;
	CJTime m_EndTime;

	BOOL m_bPrintMode;
	CFont *m_pDateFont;
	void SetXPButton();
	void SetPosition();
	CObList m_EventList;
	CEventFile m_EventRecordFile;
	CFMSGList m_FMSGList;
	
	int m_nEventCount;
	CString m_strDefFileName;
	CString m_strFilePath;
	CString m_strStnName;
	CString m_strLOGPath;
};

#ifndef _DEBUG  // debug version in EventMgrView.cpp
inline CEventMgrDoc* CEventMgrView::GetDocument()
   { return (CEventMgrDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTMGRVIEW_H__E1230B0A_6E4B_4B22_ABD2_C552F575FF60__INCLUDED_)
