#if !defined(AFX_FILTERDLG_H__44C0E6AA_1E6B_4A63_AB1B_AD339ECCA7B9__INCLUDED_)
#define AFX_FILTERDLG_H__44C0E6AA_1E6B_4A63_AB1B_AD339ECCA7B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterDlg dialog

class CFilterDlg : public CDialog
{
// Construction
public:
	CFilterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFilterDlg)
	enum { IDD = IDD_DIALOG_FILTER };
	CEdit	m_ctrlCodeEdit;
	BOOL	m_bCheckControl;
	BOOL	m_bCheckError;
	BOOL	m_bCheckEtc;
	BOOL	m_bCheckStatus;
	CString	m_strFilterText;
	CString	m_strCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFilterDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFilterDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTERDLG_H__44C0E6AA_1E6B_4A63_AB1B_AD339ECCA7B9__INCLUDED_)
