// EventMgr.h : main header file for the EVENTMGR application
//

#if !defined(AFX_EVENTMGR_H__2CCE67CC_10F3_4FBF_ADCB_7163DA4E54E3__INCLUDED_)
#define AFX_EVENTMGR_H__2CCE67CC_10F3_4FBF_ADCB_7163DA4E54E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CEventMgrApp:
// See EventMgr.cpp for the implementation of this class
//

class CEventMgrApp : public CWinApp
{
public:
	CEventMgrApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventMgrApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CEventMgrApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	HANDLE m_hMutex;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTMGR_H__2CCE67CC_10F3_4FBF_ADCB_7163DA4E54E3__INCLUDED_)
