//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EventMgr.rc
//
#define IDD_ABOUTBOX                    100
#define IDC_CUSTOM_GRIDCTRL             100
#define IDD_EVENTMGR_FORM               101
#define IDR_MAINFRAME                   128
#define IDR_EVENTMTYPE                  129
#define IDD_DIALOG_FILTER               130
#define IDD_DIALOG_WAIT                 131
#define IDD_DIALOG_LOADING              131
#define IDC_EDIT_TEXT                   1000
#define IDC_CHECK_CONTROL               1001
#define IDC_CHECK_ERROR                 1002
#define IDC_CHECK_STATUS                1003
#define IDC_CHECK_ETC                   1004
#define IDC_EDIT_CODE                   1005
#define IDC_STATIC_ALERT                1006
#define IDC_EDIT_END_DATE               1007
#define IDC_DATETIME_START              1008
#define IDC_EDIT_START_DATE             1009
#define IDC_DATETIME_END                1009
#define IDC_STATIC_TILT                 1010
#define IDC_STATIC_ERRMSG               1011
#define IDC_BUTTON_FILTER               57605
#define IDC_BUTTON_QUERY                57608

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
