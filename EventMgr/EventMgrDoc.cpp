// EventMgrDoc.cpp : implementation of the CEventMgrDoc class
//

#include "stdafx.h"
#include "EventMgr.h"

#include "EventMgrDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEventMgrDoc

IMPLEMENT_DYNCREATE(CEventMgrDoc, CDocument)

BEGIN_MESSAGE_MAP(CEventMgrDoc, CDocument)
	//{{AFX_MSG_MAP(CEventMgrDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventMgrDoc construction/destruction

CEventMgrDoc::CEventMgrDoc()
{
	// TODO: add one-time construction code here

}

CEventMgrDoc::~CEventMgrDoc()
{
}

BOOL CEventMgrDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CEventMgrDoc serialization

void CEventMgrDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CEventMgrDoc diagnostics

#ifdef _DEBUG
void CEventMgrDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEventMgrDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEventMgrDoc commands
