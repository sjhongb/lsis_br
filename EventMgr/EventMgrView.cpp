// EventMgrView.cpp : implementation of the CEventMgrView class
//

#include "stdafx.h"
#include "EventMgr.h"

#include "EventMgrDoc.h"
#include "EventMgrView.h"

#include "LoadingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEventMgrView

IMPLEMENT_DYNCREATE(CEventMgrView, CFormView)

BEGIN_MESSAGE_MAP(CEventMgrView, CFormView)
	//{{AFX_MSG_MAP(CEventMgrView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_FILTER, OnButtonFilter)
	ON_BN_CLICKED(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_BN_CLICKED(IDC_BUTTON_QUERY, OnButtonQuery)
	ON_WM_DELETEITEM()
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventMgrView construction/destruction

CEventMgrView::CEventMgrView()
	: CFormView(CEventMgrView::IDD)
{
	//{{AFX_DATA_INIT(CEventMgrView)
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_nEventCount = 0;
	m_pDateFont = NULL;
	m_bPrintMode = FALSE;
}

CEventMgrView::~CEventMgrView()
{
	if(m_pDateFont != NULL)
	{
		delete m_pDateFont;
	}
}

void CEventMgrView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventMgrView)
	DDX_Control(pDX, IDC_BUTTON_QUERY, m_ctrlQueryBtn);
	DDX_Control(pDX, IDC_BUTTON_FILTER, m_ctrlFilterBtn);
	DDX_Control(pDX, ID_FILE_SAVE_AS, m_ctrlSaveBtn);
	DDX_Control(pDX, ID_FILE_PRINT_PREVIEW, m_ctrlPreviewBtn);
	DDX_Control(pDX, ID_FILE_PRINT, m_ctrlPrintBtn);
	DDX_Control(pDX, IDC_DATETIME_START, m_ctrlStartDT);
	DDX_Control(pDX, IDC_DATETIME_END, m_ctrlEndDT);
	//}}AFX_DATA_MAP

	DDX_GridControl(pDX, IDC_CUSTOM_GRIDCTRL, m_ctrlEventGrid);
}

BOOL CEventMgrView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CEventMgrView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	// TODO: Add extra initialization here
	SetXPButton();
	SetPosition();

	m_FMSGList.Create();

	m_ctrlEventGrid.SetLSEventMode(TRUE);
	m_ctrlEventGrid.SetEditable(FALSE);
	m_ctrlEventGrid.SetListMode(TRUE);
	m_ctrlEventGrid.SetSingleRowSelection(TRUE);
	m_ctrlEventGrid.SetHeaderSort(FALSE);
	m_ctrlEventGrid.EnableDragAndDrop(FALSE);
	m_ctrlEventGrid.SetTextBkColor(RGB(0xFF, 0xFF, 0xFF));
	m_ctrlEventGrid.SetDoubleBuffering(TRUE);
	
	m_ctrlEventGrid.SetRowResize(FALSE);
	m_ctrlEventGrid.SetColumnResize(FALSE);
	m_ctrlEventGrid.SetRowCount(1);
	m_ctrlEventGrid.SetColumnCount(5);
	m_ctrlEventGrid.SetFixedRowCount(1);
	m_ctrlEventGrid.SetFixedColumnCount(0);
	
	m_ctrlEventGrid.SetItemText(0, 0, "NO.");
	m_ctrlEventGrid.SetItemText(0, 1, "TYPE");
	m_ctrlEventGrid.SetItemText(0, 2, "DATE");
	m_ctrlEventGrid.SetItemText(0, 3, "CODE");
	m_ctrlEventGrid.SetItemText(0, 4, "MESSAGE");
	
	RECT rectClient;
	m_ctrlEventGrid.GetClientRect(&rectClient);
	
	m_ctrlEventGrid.SetColumnWidth(0, 50);
	m_ctrlEventGrid.SetColumnWidth(1, 70);
	m_ctrlEventGrid.SetColumnWidth(2, 170);
	m_ctrlEventGrid.SetColumnWidth(3, 50);
	m_ctrlEventGrid.SetColumnWidth(4, rectClient.right - 340);

	LOGFONT lf;
	m_ctrlEventGrid.GetFont()->GetLogFont(&lf);
	
	memset(lf.lfFaceName, 0, sizeof(lf.lfFaceName));
	strcpy(lf.lfFaceName, "Verdana");
	
	CFont Font;
	Font.CreateFontIndirect(&lf);
	m_ctrlEventGrid.SetFont(&Font);
	Font.DeleteObject();
	
	m_ctrlStartDT.SetFormat("yyyy-MM-dd HH:mm:ss");
	m_ctrlEndDT.SetFormat("yyyy-MM-dd HH:mm:ss");

	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = 17;
	strcpy(lf.lfFaceName, "Verdana");

	m_pDateFont = new CFont;
	if(m_pDateFont->CreateFontIndirect(&lf))
	{
		m_ctrlStartDT.SetFont(m_pDateFont);
		m_ctrlEndDT.SetFont(m_pDateFont);
	}
	else
	{
		delete m_pDateFont;
		m_pDateFont = NULL;
	}

	m_ctrlEventGrid.GetNewRowsHeight();
}

/////////////////////////////////////////////////////////////////////////////
// CEventMgrView printing

BOOL CEventMgrView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CEventMgrView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CEventMgrView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CEventMgrView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CEventMgrView diagnostics

#ifdef _DEBUG
void CEventMgrView::AssertValid() const
{
	CFormView::AssertValid();
}

void CEventMgrView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CEventMgrDoc* CEventMgrView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEventMgrDoc)));
	return (CEventMgrDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEventMgrView message handlers

void CEventMgrView::RefreshGridCtrl()
{
	CLoadingDlg dlg;
	dlg.Create(IDD_DIALOG_LOADING, this);
	dlg.ShowWindow(SW_SHOWNORMAL);

	RECT rectClient;

	m_nEventCount = 0;
	m_ctrlEventGrid.DeleteNonFixedRows();
	m_ctrlEventGrid.Invalidate();

	// 리스트에 메시지 삽입
	if(UINT nCount = m_EventList.GetCount())
	{
		int nRow = m_ctrlEventGrid.GetRowCount() - 1;
		CString strIndex;
		MessageEvent* pEvent = NULL;
		
		POSITION pos = m_EventList.GetHeadPosition();
		while( pos != NULL ) 
		{
			pEvent = (MessageEvent*)m_EventList.GetNext(pos);
			if(pEvent)
			{
				COLORREF	color = RGB(0,0,0);
				MsgTimeStrings msgs;
				m_FMSGList.GetMessage(msgs, pEvent);
				
				msgs.strType.TrimRight(' ');
				msgs.strTime.TrimRight(' ');
				msgs.strMsgNo.TrimRight(' ');
				msgs.strMessage.TrimRight(' ');
				
				strIndex.Format("%d", ++m_nEventCount);

				if(msgs.strType == "CONTROL")
				{
					if(!m_FilterDlg.m_bCheckControl)
					{
						continue;
					}

					color = RGB(0, 0, 255);
				}
				else if(msgs.strType == "ERROR")
				{
					if(!m_FilterDlg.m_bCheckError)
					{
						continue;
					}

					color = RGB(255, 0, 0);
				}
				else if(msgs.strType == "STATUS")
				{
					if(!m_FilterDlg.m_bCheckStatus)
					{
						continue;
					}
				}
				else if(msgs.strType == "ETC")
				{
					if(!m_FilterDlg.m_bCheckEtc)
					{
						continue;
					}
				}

				if(!m_FilterDlg.m_strCode.IsEmpty())
				{
					if(msgs.strMsgNo != m_FilterDlg.m_strCode)
					{
						continue;
					}
				}

				if(!m_FilterDlg.m_strFilterText.IsEmpty())
				{
					if(msgs.strMessage.Find(m_FilterDlg.m_strFilterText, 0) == -1)
					{
						continue;
					}
				}

				nRow = m_ctrlEventGrid.InsertRow(strIndex);
				m_ctrlEventGrid.SetItemText(nRow, 1, msgs.strType);
				m_ctrlEventGrid.SetItemText(nRow, 2, msgs.strTime.Left(msgs.strTime.GetLength() - 3));
				m_ctrlEventGrid.SetItemText(nRow, 3, msgs.strMsgNo);
				m_ctrlEventGrid.SetItemText(nRow, 4, msgs.strMessage);
				
				m_ctrlEventGrid.SetItemFgColour(nRow, 0, color);
				m_ctrlEventGrid.SetItemFgColour(nRow, 1, color);
				m_ctrlEventGrid.SetItemFgColour(nRow, 2, color);
				m_ctrlEventGrid.SetItemFgColour(nRow, 3, color);
				m_ctrlEventGrid.SetItemFgColour(nRow, 4, color);

				m_ctrlEventGrid.GetClientRect(&rectClient);
				m_ctrlEventGrid.SetColumnWidth(4, rectClient.right - 340);
			}
		}

		if(m_ctrlEventGrid.GetRowCount() == 1)
		{
			SetDlgItemText(IDC_STATIC_ERRMSG, "No event that meets the condition");
		}

		m_ctrlEventGrid.EnsureVisible(nRow, 0);
		m_ctrlEventGrid.GetNewRowsHeight();
	}
	else
	{
		SetDlgItemText(IDC_STATIC_ERRMSG, "No event of query");
	}
	
	m_ctrlEventGrid.GetClientRect(&rectClient);
	m_ctrlEventGrid.SetColumnWidth(4, rectClient.right - 340);
	m_ctrlEventGrid.Invalidate();

	dlg.DestroyWindow();
}

void CEventMgrView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	m_EventRecordFile.Close();	

	RemoveEventList();
}

void CEventMgrView::OnFilePrintPreview()
{
	if(m_ctrlEventGrid.GetRowCount() > 1)
	{
		m_ctrlEventGrid.PrintPreview();
	}
}

void CEventMgrView::OnFilePrint()
{
	if(m_ctrlEventGrid.GetRowCount() > 1)
	{
		m_ctrlEventGrid.Print();
	}
}

void CEventMgrView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(IsWindowVisible())
	{
		SetPosition();
	}
}

void CEventMgrView::OnButtonFilter() 
{
	// TODO: Add your control notification handler code here
	if(m_FilterDlg.DoModal() == IDOK)
	{
		RefreshGridCtrl();
	}
}

void CEventMgrView::Initialize(BOOL bMode, CString &strLOGPath, CString &strStnName)
{
	m_bPrintMode = bMode;
	m_strLOGPath = strLOGPath;
	m_strStnName = strStnName;

	CJTime curTime = CJTime::GetCurrentTime();

	memset(&m_StartTime, 0, sizeof(m_StartTime));
	m_StartTime = curTime;
	m_StartTime.nHour	= 0;
	m_StartTime.nMin	= 0;
	m_StartTime.nSec	= 0;

	memset(&m_EndTime, 0, sizeof(m_EndTime));
	m_EndTime = curTime;

	if(!m_bPrintMode)
	{
		m_EndTime.nHour	= 23;
		m_EndTime.nMin	= 59;
		m_EndTime.nSec	= 59;
	}
	
	m_strDefFileName.Format("%04d%02d%02d_%02d%02d%02d_~_%04d%02d%02d_%02d%02d%02d_Event_Log", 
		m_StartTime.GetYear(), m_StartTime.nMonth, m_StartTime.nDate, m_StartTime.nHour, m_StartTime.nMin, m_StartTime.nSec,
		m_EndTime.GetYear(), m_EndTime.nMonth, m_EndTime.nDate, m_EndTime.nHour, m_EndTime.nMin, m_EndTime.nSec);
	AfxGetMainWnd()->SetWindowText(m_strDefFileName);

	SYSTEMTIME sysTime;
	memset(&sysTime, 0, sizeof(sysTime));
	sysTime.wYear	= m_StartTime.GetYear();
	sysTime.wMonth	= m_StartTime.nMonth;
	sysTime.wDay	= m_StartTime.nDate;
	sysTime.wHour	= m_StartTime.nHour;
	sysTime.wMinute = m_StartTime.nMin;
	sysTime.wSecond = m_StartTime.nSec;
	m_ctrlStartDT.SetTime(&sysTime);
	
	memset(&sysTime, 0, sizeof(sysTime));
	sysTime.wYear	= m_EndTime.GetYear();
	sysTime.wMonth	= m_EndTime.nMonth;
	sysTime.wDay	= m_EndTime.nDate;
	sysTime.wHour	= m_EndTime.nHour;
	sysTime.wMinute = m_EndTime.nMin;
	sysTime.wSecond = m_EndTime.nSec;
	m_ctrlEndDT.SetTime(&sysTime);

	SetPosition();

	if(m_bPrintMode)
	{
		OnButtonQuery();
	}
}

void CEventMgrView::SetPosition()
{	
	int button_height = 30;
	int divide = 4;
	int top_divide = 5;

	CRect rect;
	GetClientRect(&rect);
	
	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);

	if(m_bPrintMode)
	{
		m_ctrlQueryBtn.ShowWindow(SW_HIDE);

		// Error Text
		iBoxPlacement.rcNormalPosition.left   = rect.right / top_divide * 2 + 5;
		iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide * 5;
		iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
		iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height;
		GetDlgItem(IDC_STATIC_ERRMSG)->SetWindowPlacement(&iBoxPlacement);
	}
	else
	{
		// Error Text
		iBoxPlacement.rcNormalPosition.left   = rect.right / top_divide * 2 + 5;
		iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide * 4 - 5;
		iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
		iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height;
		GetDlgItem(IDC_STATIC_ERRMSG)->SetWindowPlacement(&iBoxPlacement);
		
		//Query button
		iBoxPlacement.rcNormalPosition.left   = rect.right / top_divide * 4;
		iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide * 5;
		iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
		iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height ;
		m_ctrlQueryBtn.SetWindowPlacement(&iBoxPlacement);
	}

	//Start DateTimePicker
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide - 10;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height;
	m_ctrlStartDT.SetWindowPlacement(&iBoxPlacement);
	
	// Tilt Text
	iBoxPlacement.rcNormalPosition.left   = rect.right / top_divide - 10;
	iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide + 10;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height;
	GetDlgItem(IDC_STATIC_TILT)->SetWindowPlacement(&iBoxPlacement);
	
	//End DateTimePicker
	iBoxPlacement.rcNormalPosition.left   = rect.right / top_divide + 10;
	iBoxPlacement.rcNormalPosition.right  = rect.right / top_divide * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 3;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + 3 + button_height;
	m_ctrlEndDT.SetWindowPlacement(&iBoxPlacement);

	
	//List Box
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right - 3;
	iBoxPlacement.rcNormalPosition.top    = rect.top + 3 + button_height + 3;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom - button_height;
	m_ctrlEventGrid.SetWindowPlacement(&iBoxPlacement);

	//Print button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - button_height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
	m_ctrlPrintBtn.SetWindowPlacement(&iBoxPlacement);
	
	//Print button
	iBoxPlacement.rcNormalPosition.left   =  rect.right / divide;
	iBoxPlacement.rcNormalPosition.right  =  rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - button_height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlPreviewBtn.SetWindowPlacement(&iBoxPlacement);
	
	//Save button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - button_height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlSaveBtn.SetWindowPlacement(&iBoxPlacement);
	
	//Filter button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - button_height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlFilterBtn.SetWindowPlacement(&iBoxPlacement);
}

void CEventMgrView::OnFileSaveAs() 
{
	// TODO: Add your control notification handler code here
	
	CFileDialog fileDlg(FALSE, ".CSV", m_strDefFileName, 0, "CSV Files (*.csv)|*.csv||", this);
	
	if(fileDlg.DoModal() == IDOK)
	{
		CString strFileName = fileDlg.GetFileName();
		CString strFilePath = fileDlg.GetPathName();
		
		if(m_ctrlEventGrid.Save(strFilePath))
		{
			MessageBox(strFileName + " file is saved.", "Notice", MB_OK|MB_ICONINFORMATION|MB_APPLMODAL);
		}
	}
}

void CEventMgrView::SetXPButton()
{
	m_ctrlQueryBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlQueryBtn.SetTooltipText(_T("Print event logs now."));
	m_ctrlQueryBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	
	m_ctrlPrintBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlPrintBtn.SetTooltipText(_T("Print event logs now."));
	m_ctrlPrintBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_ctrlPreviewBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlPreviewBtn.SetTooltipText(_T("Print event logs now."));
	m_ctrlPreviewBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

		
	m_ctrlSaveBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlSaveBtn.SetTooltipText(_T("Save today event logs to .csv file."));
	m_ctrlSaveBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
		
	m_ctrlFilterBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlFilterBtn.SetTooltipText(_T("Toggle auto scroll."));
	m_ctrlFilterBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
}

void CEventMgrView::OnButtonQuery() 
{
	// TODO: Add your control notification handler code here
	SYSTEMTIME startTime, endTime;
	memset(&startTime, 0, sizeof(startTime));
	memset(&endTime, 0, sizeof(endTime));

	m_ctrlStartDT.GetTime(&startTime);
	m_ctrlEndDT.GetTime(&endTime);

	BOOL bIsOK = TRUE;

	CString strErrMsg = "Successful Query";

	m_StartTime = CJTime(startTime.wYear, startTime.wMonth, startTime.wDay, startTime.wHour, startTime.wMinute, startTime.wSecond);
	m_EndTime = CJTime(endTime.wYear, endTime.wMonth, endTime.wDay, endTime.wHour, endTime.wMinute, endTime.wSecond);

	if(m_StartTime.GetDays() > m_EndTime.GetDays())
	{
		strErrMsg = "End date should be bigger than begin it";
		bIsOK = FALSE;
	}
	else if((m_EndTime.GetDays() - m_StartTime.GetDays()) > 30)
	{
		strErrMsg = "Can't search event logs over 30 days";
		bIsOK = FALSE;
	}
	else if(m_StartTime.GetDays() == m_EndTime.GetDays())
	{
		if(m_StartTime.GetSecInDay() >= m_EndTime.GetSecInDay())
		{
			strErrMsg = "End time should be bigger than begin it";
			bIsOK = FALSE;
		}
	}

	SetDlgItemText(IDC_STATIC_ERRMSG, strErrMsg);

	if(!bIsOK)
	{
		return;
	}

	m_strDefFileName.Format("%04d%02d%02d_%02d%02d%02d_~_%04d%02d%02d_%02d%02d%02d_Event_Log", 
		m_StartTime.GetYear(), m_StartTime.nMonth, m_StartTime.nDate, m_StartTime.nHour, m_StartTime.nMin, m_StartTime.nSec,
		m_EndTime.GetYear(), m_EndTime.nMonth, m_EndTime.nDate, m_EndTime.nHour, m_EndTime.nMin, m_EndTime.nSec);
	AfxGetMainWnd()->SetWindowText(m_strDefFileName);

	RemoveEventList();

	CString strFilePath;
	int year = m_StartTime.GetYear();
	int month = m_StartTime.nMonth;
	int day = m_StartTime.nDate;

	while(1)
	{
		strFilePath.Format("%s\\%4d\\%02d\\%02d.EVT", m_strLOGPath, year, month, day);

		if(m_EventRecordFile.Open(strFilePath, m_strStnName, TRUE))
		{
			//m_EventRecordFile.GetAllRecords(m_EventList);
			m_EventRecordFile.GetPeriodRecords(m_EventList, m_StartTime, m_EndTime);
		}

		if((day == m_EndTime.nDate) && (month == m_EndTime.nMonth) && (year == m_EndTime.GetYear()))
		{
			break;
		}
		
		day++;

		if(day > 31)
		{
			day = 1;

			month++;

			if(month > 12)
			{
				month = 1;

				year++;
			}
		}
	}

	RefreshGridCtrl();
}

BOOL CEventMgrView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_bPrintMode)
	{
		if(((pMsg->hwnd == m_ctrlStartDT.m_hWnd) || (pMsg->hwnd == m_ctrlEndDT.m_hWnd))
		&& ((pMsg->message == WM_KEYDOWN) || (pMsg->message == WM_LBUTTONDOWN) || (pMsg->message == WM_LBUTTONDBLCLK) || (pMsg->message == WM_RBUTTONDOWN)))
		{
			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

void CEventMgrView::RemoveEventList()
{
	POSITION pos = m_EventList.GetHeadPosition();
	MessageEvent *pEvent = NULL;

	while(pos != NULL)
	{
		pEvent = (MessageEvent*)m_EventList.GetNext(pos);
		if(pEvent != NULL)
		{
			delete pEvent;
			pEvent = NULL;
		}
	}
	
	m_EventList.RemoveAll();
}

BOOL CEventMgrView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	::SendMessage(m_ctrlEventGrid.m_hWnd, WM_MOUSEWHEEL, (WPARAM)(((DWORD)zDelta) << 16 | (DWORD)nFlags), (LPARAM)(((DWORD)pt.y) << 16 | (DWORD)pt.x));
	
	//return CFormView::OnMouseWheel(nFlags, zDelta, pt);
	return TRUE;
}
