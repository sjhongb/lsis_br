// FilterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EventMgr.h"
#include "FilterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterDlg dialog


CFilterDlg::CFilterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFilterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFilterDlg)
	m_bCheckControl = TRUE;
	m_bCheckError = TRUE;
	m_bCheckEtc = TRUE;
	m_bCheckStatus = TRUE;
	m_strFilterText = _T("");
	m_strCode = _T("");
	//}}AFX_DATA_INIT
}


void CFilterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterDlg)
	DDX_Control(pDX, IDC_EDIT_CODE, m_ctrlCodeEdit);
	DDX_Check(pDX, IDC_CHECK_CONTROL, m_bCheckControl);
	DDX_Check(pDX, IDC_CHECK_ERROR, m_bCheckError);
	DDX_Check(pDX, IDC_CHECK_ETC, m_bCheckEtc);
	DDX_Check(pDX, IDC_CHECK_STATUS, m_bCheckStatus);
	DDX_Text(pDX, IDC_EDIT_TEXT, m_strFilterText);
	DDV_MaxChars(pDX, m_strFilterText, 48);
	DDX_Text(pDX, IDC_EDIT_CODE, m_strCode);
	DDV_MaxChars(pDX, m_strCode, 3);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterDlg, CDialog)
	//{{AFX_MSG_MAP(CFilterDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterDlg message handlers

void CFilterDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString strCode;
	GetDlgItemText(IDC_EDIT_CODE, strCode);

	if(!strCode.IsEmpty() && ((strCode.GetLength() != 3) || (strCode[0] == '0')))
	{
		SetDlgItemText(IDC_STATIC_ALERT, "Invalid Code");
		((CEdit*)GetDlgItem(IDC_EDIT_CODE))->SetSel(-1, -1);

		return;
	}

	UpdateData(TRUE);

	CDialog::OnOK();
}

BOOL CFilterDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if((pMsg->hwnd == GetDlgItem(IDC_EDIT_CODE)->m_hWnd) && (pMsg->message == WM_KEYDOWN))
	{
		if((pMsg->wParam != VK_RETURN) 
		&& (pMsg->wParam != VK_BACK)
		&& (pMsg->wParam < 0x30 || pMsg->wParam > 0x39))
		{
			return TRUE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
