#if !defined(AFX_LOADINGDLG_H__1820E55A_8798_42FF_9054_5597A3D38A95__INCLUDED_)
#define AFX_LOADINGDLG_H__1820E55A_8798_42FF_9054_5597A3D38A95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoadingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoadingDlg dialog

class CLoadingDlg : public CDialog
{
// Construction
public:
	CLoadingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLoadingDlg)
	enum { IDD = IDD_DIALOG_LOADING };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoadingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoadingDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOADINGDLG_H__1820E55A_8798_42FF_9054_5597A3D38A95__INCLUDED_)
