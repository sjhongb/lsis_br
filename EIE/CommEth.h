/* -------------------------------------------------------------------------+
|  CommEth.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommEth.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver.
|                                                                           
|  4. Project Name.                                                         
|     Tongi,Laksam,13station by BR.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- LS402A and LS314                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _COMETH_H
#define _COMETH_H

#define UDP_CONSOLE_BASE	1312
#define UDP_GPS_PORT		9500
#define UDP_UP_BLOCK_PORT	8487	
#define UDP_DN_BLOCK_PORT	8488	
#define UDP_MD_BLOCK_PORT	8489
#define NEW_BLK_UDP_PORT	11500

#define UDP_NEW_CBI_BLOCK_PORT	

#include "inetLib.h"

// user header files.
#include "projdef.h"

/* ----------------------------------------------------------------------- */

class CommEth 
{
public:
	int m_nfd;				/* File Discriptor */
	BYTE m_bIsDualDest;

	struct sockaddr_in  m_LocalAddr;
	struct sockaddr_in  m_DestAddr;
	struct sockaddr_in	m_DestAddr2;
	struct sockaddr_in  m_RecvAddr;

public:
	CommEth();
	virtual ~CommEth();

	USHORT Send(BYTE *pBuffer, USHORT nLength);
	USHORT SendForNewIfc(BYTE *pBuffer, USHORT nLength, BYTE c1stDevID, BYTE c2ndDevID);
	USHORT Recv(BYTE *pBuffer, USHORT nLength);

	BYTE Initialize(const char *strLocalAddr, const USHORT nLocalPort, 
					const char *strDestAddr, const USHORT nDestPort);

	BYTE Add2ndDest(const char *strDestAddr, const USHORT nDestPort);
};

/* ----------------------------------------------------------------------- */

#endif // _COMETH_H
