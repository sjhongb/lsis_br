/* -------------------------------------------------------------------------+
|  CommGPS.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommGPS.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     Communication Block for GPS.
|                                                                           
|  4. Project Name.                                                         
|     BR Tongi/13Stations PJT.                                       
|                                                                           
|  5. Maker.                                                                
|	  LS Industrial System.
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Targe3 Board	- LS-402A                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */

// Include system header files.
#include "stdio.h"
#include "string.h"
#include "taskLib.h"
#include "selectLib.h"
#include "sockLIb.h"
#include "ioLib.h"
#include "wdLib.h"

// Include user header files.
#include "../INCLUDE/scrinfo.h"
#include "../INCLUDE/LogTask.h"

#include "crc.h"
#include "CommEth.h"
#include "CommGPS.h"

/* ------------------------------------------------------------------------ */

#define DLE             0x10
#define STX				0x02
#define ETX				0x03
#define OP_AYT			0xAC
#define OP_ACK			0xA0
#define OP_STD_TIME		0x91
#define OP_REQ_TIME		0x90

#define MAX_BUFFER_SIZE	2048

/* ------------------------------------------------------------------------ */
extern char g_nSysNo;
extern SystemStatusType	*EI_Status;

/* ------------------------------------------------------------------------ */
Comm_GPS 	ComGPS;

/* ------------------------------------------------------------------------ */
//;=====================================================================
//; GPS Serial Interface
//;=====================================================================
Comm_GPS::Comm_GPS() {
	memset( StdTime_Buffer, 0, sizeof(StdTime_Buffer));

	m_cTxSeqNo = 0;
	m_cRxSeqNo = 0;
}

USHORT Comm_GPS::MakeMessage(BYTE cSeq, BYTE cOpCode, BYTE *pBody, USHORT nBodyLen, BYTE *pTxData)
{
	USHORT 	nPos = 0;
	BYTE 	low_crc=0, high_crc=0;

	if(pTxData == NULL)
	{
		return 0;
	}

	pTxData[nPos++] = DLE;
	pTxData[nPos++] = STX;
	pTxData[nPos++] = (nBodyLen + 3) & 0xFF;		// STATION_NO(1) + SEQ(1) + OPCODE(1) + DATA(nBodyLen)
	pTxData[nPos++] = ((nBodyLen + 3) >> 8) & 0xFF;	// STATION_NO(1) + SEQ(1) + OPCODE(1) + DATA(nBodyLen)
	pTxData[nPos++] = 0;
	pTxData[nPos++] = cSeq;
	pTxData[nPos++] = cOpCode; 

	if(nBodyLen > 0 && pBody != NULL)
	{
		memcpy(&pTxData[nPos], pBody, nBodyLen);
		nPos += nBodyLen;
	}

    m_CRC.CrcGen(nPos, pTxData, &low_crc, &high_crc);

	pTxData[ nPos++ ] = low_crc;		// crc low byte
	pTxData[ nPos++ ] = high_crc;		// crc high byte
	pTxData[ nPos++ ] = DLE;
	pTxData[ nPos++ ] = ETX;

	return nPos;
}


USHORT Comm_GPS::ProcessData(BYTE *pRxData, USHORT nRxSize) 
{
    BYTE low_crc=0, high_crc=0, recv_low_crc, recv_high_crc;
	BYTE 	cStnNo, cSeqNo, cOpCode;

	if(nRxSize > 0) 
    {
		LogHex(LOG_TYPE_GPS, pRxData, nRxSize, "Recv [%d] <--- GPS", nRxSize);

        m_CRC.CrcGen(nRxSize-4, pRxData, &low_crc, &high_crc);

		// Message trailer check
        recv_low_crc = pRxData[nRxSize-4];
        recv_high_crc = pRxData[nRxSize-3]; 

		LogDbg(LOG_TYPE_GPS, "recv_low[0x%X] : calc_low[0x%x], recv_high[0x%X] : calc_high[0x%X]",
				recv_low_crc, low_crc, recv_high_crc, high_crc);	
		
		if((recv_low_crc != low_crc) || (recv_high_crc != high_crc))
		{
			LogErr(LOG_TYPE_GPS, "Recv Data CRC Check Error.");

			cSeqNo 		= pRxData[5];

			return 0;
		}

		if((DLE != pRxData[0]) || (STX != pRxData[1]))
		{
			LogErr(LOG_TYPE_GPS, "Invalid DLE/STX Value.");
			return 0;
		}

		if((DLE != pRxData[nRxSize-2]) || (ETX != pRxData[nRxSize-1]))
		{
			LogErr(LOG_TYPE_GPS, "Invalid DLE/ETX Value.");
			return 0;
		}

		cStnNo 		= pRxData[4];
		cSeqNo 		= pRxData[5];
		cOpCode 	= pRxData[6];

		switch(cOpCode)
		{
		case OP_AYT:
			LogInd(LOG_TYPE_GPS, "AYT Received. LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", m_cRxSeqNo, cSeqNo);
			m_cRxSeqNo = cSeqNo;
			SendAck(m_cRxSeqNo);
			break;

		case OP_STD_TIME:
			if((!g_nSysNo && EI_Status->bIActiveOn1) || (g_nSysNo && EI_Status->bIActiveOn2))
			{
				LogInd(LOG_TYPE_GPS, "STD Time Setting. [%02X %02X %02X %02X %02X %02X] LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", 
						pRxData[7], pRxData[8], pRxData[9], pRxData[10], pRxData[11], pRxData[12], m_cRxSeqNo, cSeqNo);
				memcpy(StdTime_Buffer, &pRxData[7], 6);
			}
			else
			{
				LogInd(LOG_TYPE_GPS, "STD Time Received. but Standby Side is not setting.");
			}

			m_cRxSeqNo = cSeqNo;
			SendAck(m_cRxSeqNo);
			break;

		default:
			break;
		}

		return nRxSize;
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// COM I/O 
/////////////////////////////////////////////////////////////////////////////////

void Comm_GPS::SendAck(BYTE cSeq) 
{    
	BYTE	pTxData[MAX_BUFFER_SIZE] = {0,};
	USHORT	nTxLen = 0;

	nTxLen = MakeMessage(cSeq, OP_ACK, NULL, 0, pTxData);

	LogDbg(LOG_TYPE_GPS, "nTxLen = %d, pTxData[0] = %X", nTxLen, pTxData[0]);

	SendData(pTxData, nTxLen);
}

void Comm_GPS::SendReqTime() 
{    
	BYTE	pTxData[MAX_BUFFER_SIZE] = {0,};
	USHORT	nTxLen = 0;

	m_cTxSeqNo++;
	if(m_cTxSeqNo == 0)
	{
		m_cTxSeqNo++;
	}

	nTxLen = MakeMessage(m_cTxSeqNo, OP_REQ_TIME, NULL, 0, pTxData);

	LogDbg(LOG_TYPE_GPS, "nTxLen = %d, pTxData[0] = %X", nTxLen, pTxData[0]);

	SendData(pTxData, nTxLen);
}

USHORT Comm_GPS::SendData(BYTE *pTxData, USHORT nTxLen)
{
	USHORT nSentLen;

	LogDbg(LOG_TYPE_GPS, "fd[%d], Send to %d.%d.%d.%d:%d", 
					m_nfd,
					(m_remoteAddr.sin_addr.s_addr >> 24) & 0xFF,
					(m_remoteAddr.sin_addr.s_addr >> 16) & 0xFF,
					(m_remoteAddr.sin_addr.s_addr >> 8) & 0xFF,
					(m_remoteAddr.sin_addr.s_addr) & 0xFF,
					m_remoteAddr.sin_port);

	nSentLen = sendto( m_nfd, (char*)pTxData, nTxLen, 0,  
						(struct sockaddr *)&m_remoteAddr, (int)(sizeof(m_remoteAddr)) );

	LogHex(LOG_TYPE_GPS, pTxData, nSentLen, "Sent [%d] ---> GPS", nSentLen);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Task Function
//////////////////////////////////////////////////////////////////////////////

PROCESS ComGPS_RecvDrv()
{
    int     width;
    STATUS  status;
	int 	nfd = ComGPS.m_nfd;

	struct timeval selTimeOut;
	struct fd_set  readFds, tmpFds;

	struct sockaddr_in remoteAddr;
	int remoteAddrSize;

	USHORT	nRcvdSize;
    BYTE	result;
    BYTE    readbuf[MAX_BUFFER_SIZE];

	BYTE	bFirst = TRUE;

	width = 0;

    selTimeOut.tv_sec  = 2;
    selTimeOut.tv_usec = 0;

    FD_ZERO(&readFds);

    FD_SET(nfd, &readFds);
    width = max(nfd, width);
	width++;

    while(1) 
	{
        tmpFds = readFds;

        status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
		if(status == ERROR) 
		{
			LogErr(LOG_TYPE_GPS, "Select Error.");

			taskDelay(600);
			continue;
		}
		else if(status == 0) 
		{
			LogErr(LOG_TYPE_GPS, "Select TIMEOUT!!!!!");

			// 추후 GPS 통신 이상에 대한 코드를 추가한다. (3회 연속 실패 시.. 등등)
		}
		else 
		{
	      	if(FD_ISSET(nfd, &readFds)) 
			{
				nRcvdSize = recvfrom(nfd, (char*)readbuf, (size_t)MAX_BUFFER_SIZE, 0, 
									(struct sockaddr *)&remoteAddr, (int*)&remoteAddrSize);

				result = ComGPS.ProcessData(readbuf, nRcvdSize);
				if(result) 
				{
					LogInd(LOG_TYPE_GPS, "Receive Success.!!!!!!!!");
					continue;
				}
          	}
          	else 
			{
				LogErr(LOG_TYPE_GPS, "Cannot Read Data.");
          	}
		}

		if(bFirst)
		{
			ComGPS.SendReqTime();

			bFirst = FALSE;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
// GPS Initalize
////////////////////////////////////////////////////////////////////////////////

short Comm_GPS::Initialize(const char *MY_ADDR, const char *GPS_ADDR)
{
	m_nTxBlockSize 	= 0;

    m_CRC.GenCrcTable();    

	bzero((char*)&m_localAddr, sizeof(m_localAddr));
	bzero((char*)&m_remoteAddr, sizeof(m_remoteAddr));

	m_localAddr.sin_family		= AF_INET;
	m_localAddr.sin_port		= htons(UDP_GPS_PORT);
	m_localAddr.sin_addr.s_addr	= inet_addr(MY_ADDR); 

	m_remoteAddr.sin_family		= AF_INET;
	m_remoteAddr.sin_port		= htons(UDP_GPS_PORT);
	m_remoteAddr.sin_addr.s_addr	= inet_addr(GPS_ADDR);  

	if((m_nfd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_GPS, "Socket Create Error!!");
		return (ERROR);
	}

	if(bind(m_nfd, (struct sockaddr *)&m_localAddr, (int)(sizeof(m_localAddr))) == ERROR)
	{
		LogErr(LOG_TYPE_GPS, "Socket Bind Error!!");
		return (ERROR);
	}

    taskSpawn("ComGPS_RecvDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComGPS_RecvDrv,0,0,0,0,0,0,0,0,0,0);
}
