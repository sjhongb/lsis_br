/* -------------------------------------------------------------------------+
|  CRC.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CRC.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _CRC_H_
#define _CRC_H_

#define LENBYTE_CRC32	4

typedef unsigned char           Uint8;
typedef char                    Int8;
typedef signed char             Sint8;

typedef unsigned short          Uint16;
typedef signed short            Int16;

typedef unsigned int            Uint32;
typedef signed int              Int32;

typedef unsigned long long int  Uint64;
typedef signed long long int    Int64;

typedef unsigned long 			Ulong32;
typedef signed long				Long32;

void InitialCrc32(void);
Uint32 checkCrc32(const Uint8 p[], Int32 iLen);

class Ccrc {
public:
	unsigned short int crc_table[256];	// crc���� Table
	unsigned    crc1;//CL
	unsigned    crc2;//CH
	unsigned int GenCrcTable();
	unsigned int CrcGen(int, unsigned char *, unsigned char *, unsigned char *);
};
#endif
