/* -------------------------------------------------------------------------+
|  ComModem.cpp                                                            
|  -------------                                                            
+------------------------------------------------------------------------- */
/* vxWorks library */
#include "vxWorks.h"
#include "msgQLib.h"
#include "selectLib.h"
#include "taskLib.h"
#include "wdLib.h"
#include "sysLib.h"
#include "ioLib.h"

/* standard c library */
#include "stdio.h"
#include "string.h"

/* user c library */
#include "../INCLUDE/scrinfo.h"
#include "../INCLUDE/LogTask.h"
#include "taskWatch.h"
#include "Crc.h"
#include "ComModem.h"

#define MAX_DATA_SIZE	56

/* ----------------------------------------------------------------------- */
/* Defined STX and ETX */ 
#define BLOCK_STX		0xE2
#define BLOCK_ETX		0xE1

/* ----------------------------------------------------------------------- */
#define MAX_RXBUFFER_SIZE	256

/* ----------------------------------------------------------------------- */
/* Defiend telegram block size : telegram header size + message data size  */
#define MAX_TELEGRAM_REC_SIZE	(5 + MAX_TELEGRAM_MSG_SIZE)		

#define BLOCK_INFO_POS	4 + MAX_BCDTIME_SIZE + 3
/* ----------------------------------------------------------------------- */
/* Defined Frame configure code */
#define FC_NEXTFRAME		0x40
#define FC_SHORTMSG			0x20
#define FC_NEEDANSWER		0x10
#define FC_FRAMEFORSECOND	0x04
#define FC_CONSTVALUE		FC_FRAMEFORSECOND

/* ----------------------------------------------------------------------- */
/* Defined mask bit for Frame CYC */
#define CYC_COUNTMASK		0x7F

/* ----------------------------------------------------------------------- */
/* Defined Frame configure code */
#define	TELEGRAM_TYPE		0x0A
#define TA_BLOCK_UPDIR		0x10
#define TA_BLOCK_DOWNDIR	0x20
#define TA_UP_CONSTVALUE	(TA_BLOCK_UPDIR + TELEGRAM_TYPE)
#define TA_DOWN_CONSTVALUE	(TA_BLOCK_DOWNDIR + TELEGRAM_TYPE)

/* ----------------------------------------------------------------------- */
#define MSGNO_UP_COMFAIL	51	// communication failure of left modem.
#define MSGNO_UP_RECOVERY	52	// 
#define MSGNO_DOWN_COMFAIL	53	// 
#define MSGNO_DOWN_RECOVERY	54	// 

/*------------------------------------------------------------------------ */

/* ----------------------------------------------------------------------- */
/* Defined funciton prototype */
WORD GetTableCRC( BYTE *p, int nSize );			/* Get CRC of block data */
BYTE *MakeBCDTime( BYTE *pTime );				/* make BCD time and date */

/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
/* Defined converting table of Hamming code */
/* HexDecimal code : 0 1 2 3 4 5 6 7 8 9 A B C D E F */
/* Hemming    code : 0 7 B C D A 6 1 E 9 5 2 3 4 8 F */
/* ----------------------------------------------------------------------- */
unsigned char HemmingCodeTable[16] = { 
	0x0, 0x7, 0xB, 0xC, 0xD, 0xA, 0x6, 0x1, 0xE, 0x9, 0x5, 0x2, 0x3, 0x4, 0x8, 0xF 
};
/* ----------------------------------------------------------------------- */
extern char g_nSysNo;
extern SystemStatusType	*EI_Status;

extern BYTE  g_dnBuffer[16], g_upBuffer[16], g_midBuffer[16], g_StationClosed;
extern char	g_nBlockType;

/* common functions */
BYTE HammingToBin( BYTE d )
{
	BYTE d1, d2;
	
	d1 = d % 16;
	d2 = d / 16;
	d  = HemmingCodeTable[d1];
	d += HemmingCodeTable[d2] * 16;
	
	return d;
}

BYTE *HammingToBlock( BYTE *pBlock, int nSize )
{
	int  i;
	BYTE d, d1, d2;
	
	for(i=0; i<nSize; i++) {
		d = *pBlock;
		d1 = d % 16;
		d2 = d / 16;
		d  = HemmingCodeTable[d1];
		d += HemmingCodeTable[d2] * 16;
		*pBlock = d;
		pBlock++;
	}
	
	return pBlock;
}

WORD GetTableCRC( BYTE *p, int nSize )
{
	int  i;
	WORD nCRC = 0;
	
	for (i=0; i<nSize; i++) {
		nCRC = crc_CCITT( *p, nCRC );
		p++;
	}
	
	return nCRC;
}

BYTE *MakeBCDTime( BYTE *pTime )
{

	*pTime++ = 0x02;	
	*pTime++ = 0x20;	
	*pTime++ = 0x12;	
	*pTime++ = 0x13;	
	*pTime++ = 0x12;	
	*pTime++ = 0x20;	
	*pTime++ = 0x10;	
	return pTime;
}

/* ----------------------------------------------------------------------- */
/* realization to Implimentation functions of CComModem class */
/* ----------------------------------------------------------------------- */
/* Construction */
CComModemDriver::CComModemDriver()
{
	m_nRXFCYC = 0x00;
	m_nRXTCYC = 0x00;
	
	m_bUpdate = 0;
//	m_nErrorNo = 0;
//	m_nComFailCount = 0;
//	m_nCRCFailCount = 0;
//	m_nOtherFailCount = 0;

	memset( &m_pIOBuffer[0], 0x00, MAX_TELEGRAM_MSG_SIZE+1 );
	memset( &m_pTimeBuffer[0], 0x00, MAX_BCDTIME_SIZE+1 );
	
	m_cRxPtr = 0;

	m_nDirection = BLOCK_DIR_UNDEF;
	m_bReverse = 0;
	m_bUseSAXL = 0;
}
	
CComModemDriver::~CComModemDriver()
{
}

/* ------------------------------------------------------------------------ */
short CComModemDriver::Initialize(short nDirection, BYTE bReverse, BYTE bUseSAXL, int nPortNo, int bUsed, int bUsedMsgQ )
{
	m_nDirection = nDirection;
	m_bReverse = bReverse;
	m_bUseSAXL = bUseSAXL;

	CommDriver::Initialize( nPortNo, bUsed, bUsedMsgQ );
}
	
void CComModemDriver::UpdateIO( BYTE *pBuffer )
{
	if ( m_bUpdate ) {
		memcpy( pBuffer, m_pIOBuffer, MAX_TELEGRAM_MSG_SIZE );
		m_bUpdate = 0;
	}
}

void CComModemDriver::ClearIO()
{
	memset( m_pIOBuffer, 0x00, MAX_TELEGRAM_MSG_SIZE );
}

/* ----------------------------------------------------------------------- */
int CComModemDriver::CheckTelegramBlock( BYTE *pBlock ) /* 0/normal, 1/frame error. */
{
	BYTE nTA;
	BYTE nCheckTA;
	BYTE nTCA;
	BYTE nTCB;
	WORD nLen;
	WORD nLoc;
	WORD nCRC;
	BYTE *pTelegramBlk;
	BYTE *pTABlock, *pCopyBlock;
	BYTE *pTBBlock;

// BCD time.
	pBlock +=	MAX_BCDTIME_SIZE;	// BCD Time length.
	
	LogDbg(LOG_TYPE_BLOCK, "Check A Telegram.\n");
// check A Telegram 
	pTelegramBlk = pBlock;
	nTA  = *pBlock++;		// TA
	nTCA = *pBlock++;		// TC
	nLen = *pBlock++;		// LEN
	pCopyBlock = pTABlock = pBlock;

	// MIDDLE의 경우 LEFT와 동일하게 DOWN으로 처리한다.
	nCheckTA = (m_nDirection == BLOCK_DIR_UP) ? TA_DOWN_CONSTVALUE : TA_UP_CONSTVALUE;
	//if (nTA != nCheckTA) 
	//{
	//	return 1;
	//}

	if ( nLen > 0x7f ) {
		return 3;
	}
	nCRC = 0;
	for (nLoc=0; nLoc<(nLen+5); nLoc++ ) {		/* MAX_TELEGRAM_REC_SIZE + 2 */
		nCRC = crc_CCITT( *pTelegramBlk, nCRC );
		pTelegramBlk++;
	}
	if ( nCRC ) { // A Telegram's CRC-16 error.
		return 4; 
	}
// check B Telegram 
	pBlock = pTelegramBlk;
	nTA  = *pBlock++;		// TA
	nTCB = *pBlock++;		// TC
	nLen = *pBlock++;		// LEN
	pTBBlock = pBlock;		
	
	if (nTCA != (BYTE)(~nTCB)) {
		return 2;
	}
	if ( nLen > 0x7f ) {
		return 3;
	}
	
	nCRC = 0;
	for (nLoc=0; nLoc<(nLen+5); nLoc++ ) {	
		nCRC = crc_CCITT( *pTelegramBlk, nCRC );
		pTelegramBlk++;
	}
	if ( nCRC ) { // B Telegram's CRC-16
		return 4;
	}
// check hamming code of B Telegram.
	for (nLoc=0; nLoc<nLen; nLoc++ ) {	
		nTCB = *pTBBlock;						// telegram B
		nTCA = HammingToBin( *pTABlock );		// converting : telegram A -> Hamming code. 
		if (nTCA != nTCB) {
			return 5;	// discordance.
		}
		else {
			pTABlock++;	// telegram A block.
			pTBBlock++;	// telegram B block.
		}
	}
	
	return 0;	// normal.
}

short CComModemDriver::WriteBlock(char *pBuffer, short length)
{
	if(m_nDirection == BLOCK_DIR_UNDEF)
	{
		return 0;
	}
		
	return CommDriver::WriteBlock(pBuffer, length);
}

int CComModemDriver::GetRxFrame() 
{
    STATUS  status;
	BYTE pRxData[MAX_BUFFER_SIZE];

	int		i, loop = 0;
	int		retval = 0;
    BYTE 	cRxLen = 0, cBodyLen = 0;

    int     width = 0;

	struct	timeval selTimeOut;
	struct	fd_set  readFds, tmpFds;

	// INITIALIZE 되지 않은 채널에 대해서는 인터럽트를 기다리지 않도록 수정함
	if(m_nDirection == BLOCK_DIR_UNDEF)
	{
		return 0;
	}

    selTimeOut.tv_sec  = SELTIMEOUT_BLOCK;
    selTimeOut.tv_usec = 0;

    FD_ZERO(&readFds);
    FD_SET(m_nfd , &readFds);

    width = max(m_nfd, 0);
	width++;

    tmpFds = readFds;
    status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
	if(status == ERROR)
	{
       	LogErr(LOG_TYPE_BLOCK, "Select Error");
		taskDelay(SELTIMEOUT_BLOCK * 600);
		return 0;
	}
	else if(status == 0)	// select의 타임아웃이 소요되었으므로 taskDelay를 넣지 않는다.
	{
		LogErr(LOG_TYPE_BLOCK, "Select Timeout...[%d] <--- %s", retval, GetBlockChName(m_nPortNo));
		return 0;
	}

	if(FD_ISSET(m_nfd, &readFds)) 
	{
		retval = read(m_nfd, (char*)&m_pRxBuffer[m_cRxPtr], MAX_BUFFER_SIZE - m_cRxPtr);
		if(retval < 0) 
		{
       		LogErr(LOG_TYPE_BLOCK, "No Read Data...[%d]", retval);

			return 0;
		}
		else
		{
			LogInd(LOG_TYPE_BLOCK, "New Data Received...[%d] <--- %s", retval, GetBlockChName(m_nPortNo));

			if((int)m_cRxPtr + retval > MAX_BUFFER_SIZE)
			{
				LogErr(LOG_TYPE_BLOCK, "Protect Buffer Overflow. Clear Buffer Data.");

				memset(m_pRxBuffer, 0, sizeof(m_pRxBuffer));
				m_cRxPtr = 0;
				return 0;
			}

			m_cRxPtr += retval;
		}

		// Minimum Protocol Size
		// STX, FC, CYC, LEN, DataMessage(Include Time) = 0, FCS(2), ETX.
		if( m_cRxPtr >= MAX_DATA_SIZE )
		{
			LogInd(LOG_TYPE_BLOCK, "Trying Process Data.");

			for(i = 0 ; i < m_cRxPtr - 1 ; i++)
			{
				if((m_pRxBuffer[i] == BLOCK_STX) && (m_pRxBuffer[i + 1] == FC_CONSTVALUE))
				{
					LogDbg(LOG_TYPE_BLOCK, "Find STX and FC at [%d], [%d].", i, i + 1);
					memcpy(m_pRxBuffer, &m_pRxBuffer[i], m_cRxPtr - i);
					m_cRxPtr = m_cRxPtr - i;
					break;	// for() loop escape
				}
			}

			if(i == m_cRxPtr - 1)
			{
				memset(m_pRxBuffer, 0, sizeof(m_pRxBuffer));
				m_cRxPtr = 0;

				LogErr(LOG_TYPE_BLOCK, "No STX or FC. Clear Buffer Data = [%d].", m_cRxPtr);
				return 0;
			}

		}
	
		if(m_cRxPtr < MAX_DATA_SIZE)
		{
			LogDbg(LOG_TYPE_BLOCK, "Pending Process. m_cRxPtr = %d", m_cRxPtr);
			return 0;
		}

		cBodyLen = m_pRxBuffer[3];

		if(cBodyLen > m_cRxPtr - 7)
		{
			LogErr(LOG_TYPE_BLOCK, "Invalid Length Value!! cBodyLen = %d. Clear Buffer", cBodyLen);

			memset(m_pRxBuffer, 0, sizeof(m_pRxBuffer));
			m_cRxPtr = 0;
			return 0;
		}

		cRxLen = cBodyLen + 7;

		if(m_pRxBuffer[cRxLen - 1] != BLOCK_ETX)
		{
			LogErr(LOG_TYPE_BLOCK, "ETX Check Error!! Clear Buffer.");

			LogHex(LOG_TYPE_BLOCK, m_pRxBuffer, cRxLen, "ETX Error Data [%d] <--- %s", cRxLen, GetBlockChName(m_nPortNo));

			memset(m_pRxBuffer, 0, sizeof(m_pRxBuffer));
			m_cRxPtr = 0;
			return 0;
		}

		// Copy To RxData Buffer.
		memcpy(pRxData, m_pRxBuffer, cRxLen);
		// Move Remain Data To First Position Of RX Buffer.
		m_cRxPtr -= cRxLen;

		if(m_cRxPtr > 0)
		{
			memcpy(m_pRxBuffer, m_pRxBuffer + cRxLen, m_cRxPtr);
		}
		else
		{
			memset(m_pRxBuffer, 0, sizeof(m_pRxBuffer));
		}

		LogHex( LOG_TYPE_BLOCK, pRxData, cRxLen, "Recv [%d] <--- %s", cRxLen, GetBlockChName(m_nPortNo));

		if(CheckFrame(cRxLen, pRxData) == 1 )
		{
		   	return 1;	// OK
		}
	}

	return 0;	// FAIL
}

int CComModemDriver::CheckFrame( int nSize, BYTE *pRcvBuf ) 
{
	int  nResult;
	WORD nLoc;
    WORD nRXCRC;

	nResult = 0;
	
	if ( !nSize ) {
		return -1;	/* not RX data */
	}
	
	nRXCRC = 0;
	for (nLoc = 0 ; nLoc < nSize - 1 ; nLoc++ ) { // STX ~ FCS(2).
		nRXCRC = crc_CCITT( pRcvBuf[nLoc], nRXCRC );
	}

	LogDbg(LOG_TYPE_BLOCK, "CRC Result. nRXCRC = %d", nRXCRC);

	if(nRXCRC == 0) 
	{ // Fraim's CRC-16
		nResult = CheckTelegramBlock( &pRcvBuf[4] );	// include time structure.

		if(!nResult) 
		{	// 정상 처리.
			memcpy( m_pTimeBuffer, &pRcvBuf[4], MAX_BCDTIME_SIZE );		// frame time.
			memcpy( m_pIOBuffer, &pRcvBuf[BLOCK_INFO_POS], MAX_TELEGRAM_MSG_SIZE );	// A Telegram message block(16 BYTE).

			//LogHex(LOG_TYPE_BLOCK, m_pIOBuffer, MAX_TELEGRAM_MSG_SIZE, "<--- m_pIOBuffer = 0x%X", m_pIOBuffer);

			if (g_StationClosed)
			{
				switch(m_nDirection)
				{
				case BLOCK_DIR_DOWN:
				    memcpy(g_dnBuffer, m_pIOBuffer, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_MID:
		  			memcpy(g_midBuffer, m_pIOBuffer, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_UP:
					memcpy(g_upBuffer, m_pIOBuffer, MAX_TELEGRAM_MSG_SIZE);
					break;
				default:
					break;
				}
			}
			else
			{
				switch(m_nDirection)
				{
				case BLOCK_DIR_DOWN:
		   			memset(g_dnBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_MID:
		    		memset(g_midBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_UP:
					memset(g_upBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				default:
					break;
				}
			}

			m_bUpdate = 1;
			nResult = 1;
		}
	}

	return nResult;	/* 0/receiveing RX and not done, 1/received RX, -1/not receive RX */ 
}


/* ----------------------------------------------------------------------- */
/* -------------------------------------------------------------------------+
|  CComModem class.
|  
+------------------------------------------------------------------------- */
/* Construction */
CComModem::CComModem()
{
	m_pInBlkTable  = NULL;
	m_pOutBlkTable = NULL;
	m_pTimeTable = NULL;
	
	m_nTXFCYC = 0x00;
	m_nTXTCYC = 0x00;
	m_nRXFCYC = 0x00;
	
	m_nStatus = 3;
//	m_nComFailCount = 0;
//	m_nCRCFailCount = 0;
//	m_nOtherFailCount = 0;

	m_nDirection = BLOCK_DIR_UNDEF;

	m_bReverse = 0;
	m_bUseSAXL = 0;
	m_bNewIfc  = 0;

	m_bIsNetwork 	= 0;
	m_bIsDualNet 	= 0;
	m_bIsBlkDev		= 0;		

	m_bLANCommFail = 0;
	m_bUseBkup = 0;

	m_cMyStnID 		= 0;
	m_cPeerStnID	= 0;
}
	
CComModem::~CComModem()
{
}

/* ------------------------------------------------------------------------ */
/*  Telegram DataMessage  								 					*/
/*  ATelegram = TA, TC, A Length, Data(16 Byte)				, CRC-L, CRC-H	*/
/*  BTelegram = TA,~TC, B Length, Data(16 Byte-Hamming code), CRC-L, CRC-H	*/
/* ------------------------------------------------------------------------ */
BYTE *CComModem::MakeTelegram( BYTE *pTXBlock, BYTE *pOutBlk, int bBSide/* =0 */ )
{
	WORD nCRC;

	// MIDDLE의 경우 LEFT와 동일하게 UP으로 설정하여 보낸다.
	// (송신하는 TELEGRAM이므로 반대로 설정함)
	BYTE nTA = ((m_nDirection == BLOCK_DIR_UP) && (m_bIsBlkDev == FALSE)) ? TA_UP_CONSTVALUE : TA_DOWN_CONSTVALUE;

// setting telegram type & address.	
//	nTA |= TELEGRAM_TYPE;
	
	if ( !bBSide ) {
// telegram cycle No.
		m_nTXTCYC++;
		m_nTXTCYC &= CYC_COUNTMASK;
		if ( !m_nTXTCYC ) m_nTXTCYC++;
	}
	
// m_nTXTCYC = 1;

// setting telegram data.		
	pTXBlock[0] = nTA;								// TA.
	pTXBlock[1] = (bBSide) ? ~m_nTXTCYC : m_nTXTCYC;	// Cycle No.
	pTXBlock[2] = MAX_TELEGRAM_MSG_SIZE;				// message Length

	if (g_StationClosed)
	{
		// 송신하는 TELEGRAM이므로 반대로 설정함.
		switch(m_nDirection)
		{
		case BLOCK_DIR_DOWN:
			memcpy( &pTXBlock[3], g_upBuffer, MAX_TELEGRAM_MSG_SIZE );
			break;
		case BLOCK_DIR_UP:
			memcpy( &pTXBlock[3], g_dnBuffer, MAX_TELEGRAM_MSG_SIZE );
			break;
		case BLOCK_DIR_MID:
			memcpy( &pTXBlock[3], g_midBuffer, MAX_TELEGRAM_MSG_SIZE );
			break;
		default:
			break;
		}

		// SUPER BLOCK 설정이 된 역의 경우 SUPER BLOCK BIT를 SET하여 보낸다.
		if(m_bUseSAXL)
		{
			LogDbg(LOG_TYPE_BLOCK, "Before [%02X%02X%02X%02X%02X]",
					pTXBlock[3],
					pTXBlock[3 + 1],
					pTXBlock[3 + 2],
					pTXBlock[3 + 3],
					pTXBlock[3 + 4]);

			pTXBlock[3 + 3] |= 0x03;	// SAXLACTOUT 값을 넣는다.

			LogDbg(LOG_TYPE_BLOCK, "After [%02X%02X%02X%02X%02X]",
					pTXBlock[3],
					pTXBlock[3 + 1],
					pTXBlock[3 + 2],
					pTXBlock[3 + 3],
					pTXBlock[3 + 4]);
		}
		else // SUPER BLOCK 설정이 안된 역의 경우 받은 AXLOCC, AXLDST 정보를 릴레이하지 않고, 자신의 AXLOCC, AXLDST를 그대로 보낸다.
		{
			LogDbg(LOG_TYPE_BLOCK, "Before [%02X%02X%02X%02X%02X]",
					pTXBlock[3],
					pTXBlock[3 + 1],
					pTXBlock[3 + 2],
					pTXBlock[3 + 3],
					pTXBlock[3 + 4]);

			pTXBlock[3] &= 0x3F;				// 받은 AXLOCC, AXLDST 정보 제거
			pTXBlock[3] |= (pOutBlk[0] & 0xC0);	// 내 AXLOCC, AXLDST 정보를 삽입한다.

			LogDbg(LOG_TYPE_BLOCK, "After [%02X%02X%02X%02X%02X]",
					pTXBlock[3],
					pTXBlock[3 + 1],
					pTXBlock[3 + 2],
					pTXBlock[3 + 3],
					pTXBlock[3 + 4]);
		}
	}
	else
	{
		memcpy( &pTXBlock[3], pOutBlk, MAX_TELEGRAM_MSG_SIZE );	// message setting(16 BYTE).
		//LogHex(LOG_TYPE_GEN, &pTXBlock[3], MAX_TELEGRAM_MSG_SIZE, "------ TX_TELEGRAM -----");
	}

	if ( bBSide ) 
	{
		HammingToBlock( &pTXBlock[3], MAX_TELEGRAM_MSG_SIZE );	// conver hamming code to message(16 BYTE).
	}
	nCRC = GetTableCRC( pTXBlock, (3 + MAX_TELEGRAM_MSG_SIZE) );	// computing telegram table CRC.
 	pTXBlock[3+MAX_TELEGRAM_MSG_SIZE] = nCRC % 256;	// CRC low.
	pTXBlock[4+MAX_TELEGRAM_MSG_SIZE] = nCRC / 256;	// CRC high.
	
// next BYTE pointer of telegram data.		
	return &pTXBlock[5+MAX_TELEGRAM_MSG_SIZE];
}

/* --------------------------------------------------------------------------+
|  Telegram Communication Frame						 							
|  STX, FC, CYC, Length, DATA Message( Times + ATelegram + BTelegram ), FCS, ETX   
+-------------------------------------------------------------------------- */
int CComModem::MakeCommFrame( BYTE *pTXBlock, BYTE *pOutBlk )
{
	WORD nCRC;
	BYTE *pScr = pTXBlock;
	BYTE nMsgSize = MAX_BCDTIME_SIZE + (MAX_TELEGRAM_REC_SIZE * 2); 	// time length + (telegram header + telegram data block) * 2.

// frame cycle.
	m_nTXFCYC++;
	m_nTXFCYC &= CYC_COUNTMASK;
	if ( !m_nTXFCYC ) m_nTXFCYC++;
	
// setting header.
	*pTXBlock = BLOCK_STX;					// STX
	 pTXBlock++;
	*pTXBlock = FC_FRAMEFORSECOND;		// frame configure
 	 pTXBlock++;
	*pTXBlock = m_nTXFCYC;				// frame cycle No.
	 pTXBlock++;
	*pTXBlock = nMsgSize;					// frame data message length with size of telegram A and telegram B.
	 pTXBlock++;
// set BCD time
//	 pTXBlock = MakeBCDTime( pTXBlock );		// frame time data(7byte) with BCD code.
	*pTXBlock = m_pTimeTable[0];		// frame time data(7byte) with BCD code. sec
	 pTXBlock++;
	*pTXBlock = m_pTimeTable[1];		// frame time data(7byte) with BCD code. min
	 pTXBlock++;
	*pTXBlock = m_pTimeTable[2];		// frame time data(7byte) with BCD code. hour
	 pTXBlock++;
	*pTXBlock = m_pTimeTable[3];		// frame time data(7byte) with BCD code. date
	 pTXBlock++;
	*pTXBlock = m_pTimeTable[4];		// frame time data(7byte) with BCD code. mon
	 pTXBlock++;
	*pTXBlock = m_pTimeTable[5];		// frame time data(7byte) with BCD code. year-low
	 pTXBlock++;
//	*pTXBlock = m_pTimeTable[6];		// frame time data(7byte) with BCD code. yea-hi
	*pTXBlock = 0x20;					// frame time data(7byte) with BCD code. yea-hi (2002-20)
	 pTXBlock++;
// set telegram.
	 pTXBlock = MakeTelegram( pTXBlock, pOutBlk, 0 );	// make Telegram side A.
	 pTXBlock = MakeTelegram( pTXBlock, pOutBlk, 1 );	// make Telegram side B with hamming code.
	nCRC = GetTableCRC( pScr, (4 + nMsgSize) ); 
// printf(".%d.", nMsgSize);
// printf("(%4.4X)", nCRC);	
	*pTXBlock = nCRC % 256;				// CRC low.
	 pTXBlock++;
	*pTXBlock = nCRC / 256;				// CRC high.
	 pTXBlock++;
	*pTXBlock = BLOCK_ETX;
	
	return (nMsgSize + 7);		// STX, FC, CTC, Length, TIMES(7Byte), Telegram A, Telegram B, CRC(2Byte), ETX.
}

void CComModem::ClearIO()
{
	// clear to input block table.
	memset( m_pInBlkTable, 0x00, MAX_TELEGRAM_MSG_SIZE );
	// clear to modem block table.
	m_Modem1.ClearIO();
	m_Modem2.ClearIO();
}

/* --------------------------------------------------------------------------+
// write processing.							 				
// 1) RTS ON. (IO func:0x105)
// 2) Write DATA Frame. (IO func:write())
// 3) Check empty of 308 TX buffer or Delay time over. (IO func:0x48)
// 4) RTS OFF. (IO func:0xf31)
+-------------------------------------------------------------------------- */
int CComModem::WriteFrame()
{
	int nResult1;
	int nResult2;
	int nSendByte;
	BYTE *pTXBlock;

	if (m_pOutBlkTable == NULL) 
		return 0;
	
	nResult1 = nResult2 = ERROR;
// make TX data frame.
	pTXBlock = &m_TxFrame[0];
	nSendByte = MakeCommFrame( pTXBlock, m_pOutBlkTable );
	
// send TX data frame.	
	nResult1 = m_Modem1.WriteBlock( (char*)pTXBlock, (short)nSendByte );
	nResult2 = m_Modem2.WriteBlock( (char*)pTXBlock, (short)nSendByte );
	
	if(nResult1 && nResult2)
	{
		LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent [%d] ---> %s, %s", 
				nSendByte, GetBlockChName(m_Modem1.m_nPortNo), GetBlockChName(m_Modem2.m_nPortNo));
	}
	else if(nResult1)
	{
		LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent [%d] ---> %s", 
				nSendByte, GetBlockChName(m_Modem1.m_nPortNo));
	}
	else if(nResult2)
	{
		LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent [%d] ---> %s", 
				nSendByte, GetBlockChName(m_Modem2.m_nPortNo));
	}
	else
	{
		LogErr( LOG_TYPE_BLOCK, "WriteFrame Fail.. nSendByte = %d", nSendByte );
		nSendByte = ERROR;
	}

	return nSendByte;
}

/* --------------------------------------------------------------------------+
// read processing.							 				
// 1) RTS ON. (IO func:0x105)
// 2) Write DATA Frame. (IO func:write())
// 3) Check empty of 308 TX buffer or Delay time over. (IO func:0x48)
// 4) RTS OFF. (IO func:0xf31)
+-------------------------------------------------------------------------- */

int CComModem::ReadFrame(BYTE bUpdate)
{
	int nCount = 0;
	int nResult1, nResult2 = 0;
	
	nResult1 = nResult2 = 0;
	if (m_pInBlkTable == NULL) return -1;

	taskDelay( 6 );
	nResult1 = m_Modem1.GetRxFrame();
	nResult2 = m_Modem2.GetRxFrame();

	LogDbg(LOG_TYPE_BLOCK, "m_nDirection = %d, nResult1 = %d, nResult2 = %d\n", m_nDirection, nResult1, nResult2);

	if (nResult1 == 1) 
	{
		if(bUpdate)
		{
			m_Modem1.UpdateIO( m_pInBlkTable );
			LogHex(LOG_TYPE_BLOCK, m_pInBlkTable, MAX_TELEGRAM_MSG_SIZE, "Telegram [%d] from %s",  MAX_TELEGRAM_MSG_SIZE, GetBlockChName(m_Modem1.m_nPortNo));
		}
	}
	else if (nResult2 == 1) 
	{
		if(bUpdate)
		{
			m_Modem2.UpdateIO( m_pInBlkTable );
			LogHex(LOG_TYPE_BLOCK, m_pInBlkTable, MAX_TELEGRAM_MSG_SIZE, "Telegram [%d] from %s",  MAX_TELEGRAM_MSG_SIZE, GetBlockChName(m_Modem2.m_nPortNo));
		}
	}

	nCount = nResult1 | (nResult2 << 1);
	return nCount;
}

int CComModem::RecvFromNet(BYTE nNetCh)
{
	int nCount = 0;
	int nResult1, nResult2 = 0;
	
	BYTE pRxBuffer[MAX_BUFFER_SIZE] = {0,};
	BYTE pTelegram[MAX_TELEGRAM_MSG_SIZE + 1] = {0,};
	USHORT nRxSize = 0;

	BYTE nRXFCYC = 0;	

	if(nNetCh == 1)
	{
		nRxSize = m_NetPRI.Recv(pRxBuffer, MAX_BUFFER_SIZE);

		LogHex(LOG_TYPE_BLOCK, pRxBuffer, nRxSize, "<--- Recv[%d] from NET[%d] DIR[%d] ADDR[%08X:%d]", 
					nRxSize, nNetCh, m_nDirection, m_NetPRI.m_RecvAddr.sin_addr.s_addr, m_NetPRI.m_RecvAddr.sin_port); 
	}
	else if(nNetCh == 2)
	{
		nRxSize = m_NetSEC.Recv(pRxBuffer, MAX_BUFFER_SIZE);

		LogHex(LOG_TYPE_BLOCK, pRxBuffer, nRxSize, "<--- Recv[%d] from NET[%d] DIR[%d] ADDR[%08X:%d]", 
					nRxSize, nNetCh, m_nDirection, m_NetSEC.m_RecvAddr.sin_addr.s_addr, m_NetSEC.m_RecvAddr.sin_port); 
	}

	if(m_bNewIfc)
	{
		UINT32 uiCalcCRC32 = 0;
		UINT32 uiRcvdCRC32 = 0;

		if(nRxSize > 16)
		{
			memcpy(&uiRcvdCRC32, &pRxBuffer[nRxSize - LENBYTE_CRC32], LENBYTE_CRC32);
			uiCalcCRC32 = checkCrc32(pRxBuffer, nRxSize - LENBYTE_CRC32);

			if(uiCalcCRC32 != uiRcvdCRC32)
			{
				LogErr(LOG_TYPE_BLOCK, "CRC32 missmatched. Calc[%04X], Rcvd[%04X]", uiCalcCRC32, uiRcvdCRC32);
				return 0;
			}

			if(pRxBuffer[NEW_BLK_STX_POS] != NEW_BLK_STX)
			{
				LogErr(LOG_TYPE_BLOCK, "STX Error.");
				return 0;
			}

			nRXFCYC	= pRxBuffer[NEW_BLK_SEQ_POS];
			if(((nRXFCYC != 1) && (nRXFCYC < m_nRXFCYC)) || ((nRXFCYC != 0) && (nRXFCYC == m_nRXFCYC)))
			{
				LogDbg(LOG_TYPE_BLOCK, "Already Received Data. Ignored It.");
				return 1;
			}

			m_nRXFCYC = nRXFCYC;

			// Source Station ID Check
			if(pRxBuffer[NEW_BLK_SRC_POS + 1] != m_cPeerStnID)
			{
				LogErr(LOG_TYPE_BLOCK, "Source Station ID Error. %0X", pRxBuffer[NEW_BLK_SRC_POS + 1]);
				return 0;
			}

			// Source ID Check
			if((pRxBuffer[NEW_BLK_SRC_POS + 2] != 0x11) 
			&& (pRxBuffer[NEW_BLK_SRC_POS + 2] != 0x12) 
			&& (pRxBuffer[NEW_BLK_SRC_POS + 2] != 0x21) 
			&& (pRxBuffer[NEW_BLK_SRC_POS + 2] != 0x22))
			{
				LogErr(LOG_TYPE_BLOCK, "Source Device ID Error. %0X", pRxBuffer[NEW_BLK_SRC_POS + 2]);
				return 0;
			}

			// Destination Station ID Check
			if(pRxBuffer[NEW_BLK_DST_POS + 1] != m_cMyStnID)
			{
				LogErr(LOG_TYPE_BLOCK, "Destination Station ID Error. %0X", pRxBuffer[NEW_BLK_DST_POS + 1]);
				return 0;
			}

			// Destination ID Check
#if 0
			if(!g_nSysNo && (pRxBuffer[NEW_BLK_DST_POS + 2] != 0x11))
			{
				LogErr(LOG_TYPE_BLOCK, "Destination Device ID Error. %0X", pRxBuffer[NEW_BLK_DST_POS + 2]);
				return 0;
			}
			else if(g_nSysNo && (pRxBuffer[NEW_BLK_DST_POS + 2] != 0x))
			{
				LogErr(LOG_TYPE_BLOCK, "Destination Device ID Error. %0X", pRxBuffer[NEW_BLK_DST_POS + 2]);
				return 0;
			}
#endif

			memcpy(m_pInBlkTable, &pRxBuffer[NEW_BLK_DATA_POS], MAX_TELEGRAM_MSG_SIZE);	// A Telegram message block(16 BYTE).			

			return 1;
		}		
	}
	else
	{
		int  nResult;
		WORD nLoc;
	    WORD nRXCRC;

		nRXCRC = 0;
		for(nLoc = 0 ; nLoc < nRxSize - 1 ; nLoc++) 
		{ // STX ~ FCS(2).
			nRXCRC = crc_CCITT(pRxBuffer[nLoc], nRXCRC);
		}

		LogDbg(LOG_TYPE_BLOCK, "CRC Result. nRXCRC = %d", nRXCRC);

		if(nRXCRC == 0) 
		{
			// 정상 처리.
			nRXFCYC = pRxBuffer[2];
			if(((nRXFCYC != 1) && (nRXFCYC < m_nRXFCYC)) || (nRXFCYC == m_nRXFCYC))
			{
				LogDbg(LOG_TYPE_BLOCK, "Already Received Data. Ignored It.");
				return 1;
			}

			m_nRXFCYC = nRXFCYC;
			
			memcpy(pTelegram, &pRxBuffer[BLOCK_INFO_POS], MAX_TELEGRAM_MSG_SIZE);	// A Telegram message block(16 BYTE).

			LogHex(LOG_TYPE_BLOCK, pTelegram, MAX_TELEGRAM_MSG_SIZE, "<--- pTelegram = 0x%X", pTelegram);

			if (g_StationClosed)
			{
				switch(m_nDirection)
				{
				case BLOCK_DIR_DOWN:
				    memcpy(g_dnBuffer, pTelegram, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_MID:
		  			memcpy(g_midBuffer, pTelegram, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_UP:
					memcpy(g_upBuffer, pTelegram, MAX_TELEGRAM_MSG_SIZE);
					break;
				default:
					break;
				}
			}
			else
			{
				switch(m_nDirection)
				{
				case BLOCK_DIR_DOWN:
		   			memset(g_dnBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_MID:
		    		memset(g_midBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				case BLOCK_DIR_UP:
					memset(g_upBuffer, 0, MAX_TELEGRAM_MSG_SIZE);
					break;
				default:
					break;
				}
			}

			memcpy(m_pInBlkTable, pTelegram, MAX_TELEGRAM_MSG_SIZE);

			return 1;
		}
	}

	return 0;
}

int CComModem::SendToNet()
{
	USHORT nResult1;
	USHORT nResult2;
	int nSendByte;
	BYTE *pTXBlock;

	if (m_pOutBlkTable == NULL) 
		return 0;
	
	nResult1 = nResult2 = 0;

	// make TX data frame.
	pTXBlock = &m_TxFrame[0];

	if(m_bNewIfc)
	{
		memset(pTXBlock, 0, MAX_NEW_BLK_PACKET_SIZE);
		
		pTXBlock[NEW_BLK_STX_POS] 	= NEW_BLK_STX;
		pTXBlock[NEW_BLK_SEQ_POS]	= m_nTXFCYC++;

		if(m_nTXFCYC == 0)
		{
			m_nTXFCYC++;
		}

		*((USHORT*)&pTXBlock[NEW_BLK_LEN_POS]) = MAX_NEW_BLK_PACKET_SIZE;
		
		// Source Station ID
		pTXBlock[NEW_BLK_SRC_POS]		= 0x01;
		pTXBlock[NEW_BLK_SRC_POS + 1] 	= m_cMyStnID;
		if(g_nSysNo == 0)
		{
			pTXBlock[NEW_BLK_SRC_POS + 2] 	= 0x10;
		}
		else
		{
			pTXBlock[NEW_BLK_SRC_POS + 2] 	= 0x20;
		}
			
		// Destination Station ID
		pTXBlock[NEW_BLK_DST_POS]		= 0x01;
		pTXBlock[NEW_BLK_DST_POS + 1] 	= m_cPeerStnID;

		memcpy(&pTXBlock[NEW_BLK_DATA_POS], m_pOutBlkTable, MAX_TELEGRAM_MSG_SIZE);	// copy Telegram message block(16 BYTE).

		nSendByte = MAX_NEW_BLK_PACKET_SIZE;
	}
	else
	{
		nSendByte = MakeCommFrame( pTXBlock, m_pOutBlkTable );
	}
	
// send TX data frame.	
//
	if((!g_nSysNo && EI_Status->bIActiveOn1) || (g_nSysNo && EI_Status->bIActiveOn2))
	{
		if(m_bNewIfc)
		{
			nResult1 = m_NetPRI.SendForNewIfc(pTXBlock, nSendByte, 0x11, 0x12);

			if(m_bIsDualNet)
			{
				nResult2 = m_NetSEC.SendForNewIfc(pTXBlock, nSendByte, 0x21, 0x22);
			}
		}
		else
		{
			nResult1 = m_NetPRI.Send(pTXBlock, nSendByte);

			if(m_bIsDualNet)
			{
				nResult2 = m_NetSEC.Send(pTXBlock, nSendByte);
			}
		}
	
		if(nResult1 && nResult2)
		{
			LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent[%d] to NET[1, 2] DIR[%d] --->", nSendByte, m_nDirection);
		}
		else if(nResult1)
		{
			LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent[%d] to NET[1] DIR[%d] --->", nSendByte, m_nDirection);
		}
		else if(nResult2)
		{
			LogHex( LOG_TYPE_BLOCK, pTXBlock, nSendByte, "Sent[%d] to NET[2] DIR[%d] --->", nSendByte, m_nDirection);
		}
		else
		{
			LogErr( LOG_TYPE_BLOCK, "SendToNet() Fail.. nSendByte = %d", nSendByte );
			nSendByte = 0;
		}
	}
	else
	{
		LogDbg(LOG_TYPE_BLOCK, "This side is running to standby mode. Do not send block telegram.");
	}

	return nSendByte;
}

short CComModem::Initialize( BYTE *pInBlkBuf, BYTE *pOutBlkBuf, BYTE *pTimeBuf, short nDirection, BYTE bReverse, BYTE bUseSAXL, BYTE bUseBkup, BYTE *ComPorts)
{
	m_nStatus = 0;
	m_pInBlkTable  = pInBlkBuf;
	m_pOutBlkTable = pOutBlkBuf;
	m_pTimeTable = pTimeBuf;
	m_nDirection = nDirection;
	m_bReverse	= bReverse;
	m_bUseSAXL = bUseSAXL;
	m_bUseBkup = bUseBkup;
	m_bIsNetwork = FALSE;
	m_bIsBlkDev = FALSE;

	switch(ComPorts[0])
	{
	case 1:	
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK1, 0, 0 );
		break;
	case 2:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK2, 1, 0 );
		break;
	case 3:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK3, 0, 0 );
		break;
	case 4:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK4, 1, 0 );
		break;
	case 5:	
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK1, 0, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 6:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK2, 1, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 7:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK3, 0, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 8:
		m_Modem1.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK4, 1, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 0:
		break;
	}

	switch(ComPorts[1])
	{
	case 1:	
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK1, 0, 0 );
		break;
	case 2:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK2, 1, 0 );
		break;
	case 3:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK3, 0, 0 );
		break;
	case 4:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK4, 1, 0 );
		break;
	case 5:	
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK1, 0, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 6:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK2, 1, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 7:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK3, 0, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 8:
		m_Modem2.Initialize( m_nDirection, m_bReverse, m_bUseSAXL, PORTNUM_BLOCK4, 1, 0 );
		m_bIsBlkDev = TRUE;
		break;
	case 0:
		break;
	}

	return 1;
}

short CComModem::Initialize( BYTE *pInBlkBuf, BYTE *pOutBlkBuf, BYTE *pTimeBuf, short nDirection, BYTE bReverse, BYTE bUseSAXL, BYTE bNewIfc, BLOCK_NET_ADDR *pNetAddr)
{
	m_nStatus = 0;
	m_pInBlkTable  = pInBlkBuf;
	m_pOutBlkTable = pOutBlkBuf;
	m_pTimeTable = pTimeBuf;
	m_nDirection = nDirection;
	m_bReverse	= bReverse;
	m_bUseSAXL = bUseSAXL;
	m_bNewIfc	= bNewIfc;
	m_bUseBkup = pNetAddr->bUseModem;
	m_bIsNetwork = TRUE;
	m_bIsDualNet = pNetAddr->bIsDualNet;
	m_cMyStnID		= pNetAddr->cMyStnID;

	switch(m_nDirection)
	{
	case BLOCK_DIR_UP:
		m_cPeerStnID	= pNetAddr->cUPStnID;
		if(m_bNewIfc)
		{
			m_NetPRI.Initialize(pNetAddr->MY_ADDR_PRI, NEW_BLK_UDP_PORT, pNetAddr->UP_CBI1_ADDR_PRI, NEW_BLK_UDP_PORT);
			m_NetPRI.Add2ndDest(pNetAddr->UP_CBI2_ADDR_PRI, NEW_BLK_UDP_PORT);
		}
		else
		{
			m_NetPRI.Initialize(pNetAddr->MY_ADDR_PRI, UDP_UP_BLOCK_PORT, pNetAddr->UP_CBI1_ADDR_PRI, UDP_DN_BLOCK_PORT);
			m_NetPRI.Add2ndDest(pNetAddr->UP_CBI2_ADDR_PRI, UDP_DN_BLOCK_PORT);
		}
		

		if(m_bIsDualNet)
		{
			if(m_bNewIfc)
			{
				m_NetSEC.Initialize(pNetAddr->MY_ADDR_SEC, NEW_BLK_UDP_PORT, pNetAddr->UP_CBI1_ADDR_SEC, NEW_BLK_UDP_PORT);
				m_NetSEC.Add2ndDest(pNetAddr->UP_CBI2_ADDR_SEC, NEW_BLK_UDP_PORT);
			}
			else
			{
				m_NetSEC.Initialize(pNetAddr->MY_ADDR_SEC, UDP_UP_BLOCK_PORT, pNetAddr->UP_CBI1_ADDR_SEC, UDP_DN_BLOCK_PORT);
				m_NetSEC.Add2ndDest(pNetAddr->UP_CBI2_ADDR_SEC, UDP_DN_BLOCK_PORT);
			}
		}
		break;
	case BLOCK_DIR_DOWN:
		m_cPeerStnID	= pNetAddr->cDNStnID;
		if(m_bNewIfc)
		{
			m_NetPRI.Initialize(pNetAddr->MY_ADDR_PRI, NEW_BLK_UDP_PORT, pNetAddr->DN_CBI1_ADDR_PRI, NEW_BLK_UDP_PORT);
			m_NetPRI.Add2ndDest(pNetAddr->DN_CBI2_ADDR_PRI, NEW_BLK_UDP_PORT);
		}
		else
		{
			m_NetPRI.Initialize(pNetAddr->MY_ADDR_PRI, UDP_DN_BLOCK_PORT, pNetAddr->DN_CBI1_ADDR_PRI, UDP_UP_BLOCK_PORT);
			m_NetPRI.Add2ndDest(pNetAddr->DN_CBI2_ADDR_PRI, UDP_UP_BLOCK_PORT);
		}

		if(m_bIsDualNet)
		{
			if(m_bNewIfc)
			{
				m_NetSEC.Initialize(pNetAddr->MY_ADDR_SEC, NEW_BLK_UDP_PORT, pNetAddr->DN_CBI1_ADDR_SEC, NEW_BLK_UDP_PORT);
				m_NetSEC.Add2ndDest(pNetAddr->DN_CBI2_ADDR_SEC, NEW_BLK_UDP_PORT);
			}
			else
			{
				m_NetSEC.Initialize(pNetAddr->MY_ADDR_SEC, UDP_DN_BLOCK_PORT, pNetAddr->DN_CBI1_ADDR_SEC, UDP_UP_BLOCK_PORT);
				m_NetSEC.Add2ndDest(pNetAddr->DN_CBI2_ADDR_SEC, UDP_UP_BLOCK_PORT);
			}
		}
		break;
	case BLOCK_DIR_MID:
		m_cPeerStnID	= pNetAddr->cMDStnID;
		m_NetPRI.Initialize(pNetAddr->MY_ADDR_PRI, UDP_MD_BLOCK_PORT, pNetAddr->MD_CBI1_ADDR_PRI, UDP_MD_BLOCK_PORT);
		m_NetPRI.Add2ndDest(pNetAddr->MD_CBI2_ADDR_PRI, UDP_MD_BLOCK_PORT);

		if(m_bIsDualNet)
		{
			m_NetSEC.Initialize(pNetAddr->MY_ADDR_SEC, UDP_MD_BLOCK_PORT, pNetAddr->MD_CBI1_ADDR_SEC, UDP_MD_BLOCK_PORT);
			m_NetSEC.Add2ndDest(pNetAddr->MD_CBI2_ADDR_SEC, UDP_MD_BLOCK_PORT);
		}
		break;
	}

	return 1;
}
	
/* ----------------------------------------------------------------------- */
CComModem _ComBlockLeft;	/* BLOCK 1, BLOCK 3 */
CComModem _ComBlockRight;	/* BLOCK 2, BLOCK 4 */
CComModem _ComBlockMid;		/* BLOCK 2, BLOCK 4 */
CComModem _ComBkupLeft;
CComModem _ComBkupRight;
CComModem _ComBkupMid;

/* -------------------------------------------------------------------------+
//
//          In Buffer                           Out buffer
//
//  00	+----------------+              00	+----------------+         
//  	|                |              	|                |         
//  	|   ........     |              	|   ........     |         
//  	|                |              	|                |          
//  C0	+----------------+ -------      C0	+----------------+ ------- 
//  	| COM1 IN        | 16 Byte      	| COM1 IN        | 16 Byte 
//  	| (LEFT)         |     use      	| (LEFT)         | not use 
//  D0	+----------------+ -------      D0	+----------------+ ------- 
//  	| COM2 IN        | 16 Byte      	| COM2 IN        | 16 Byte 
//  	| (RIGHT)        |     use      	| (RIGHT)        | not use 
//  E0	+----------------+ -------      E0	+----------------+ ------- 
//  	| COM1 OUT       | 16 Byte      	| COM1 OUT       | 16 Byte 
//  	| (LEFT)         | not use      	| (LEFT)         |     use     
//  F0	+----------------+ -------      F0	+----------------+ ------- 
//  	| COM2 OUT       | 16 Byte      	| COM2 OUT       | 16 Byte 
//  	| (RIGHT)        | not use      	| (RIGHT)        |     use     
//  100	+----------------+ -------      100	+----------------+ ------- 
//  
+------------------------------------------------------------------------- */
/* Defined buffer size */
#define SIZE_IO_INBUFFER		0x120
#define SIZE_IO_OUTBUFFER		0x120
/* Defined ABS block data size */
#define SIZE_ABS_INBLK			16
#define SIZE_ABS_OUTBLK			16
/* Defined ABS block data table address */
#define ADDR_ABS_OUTBLK_MID		(SIZE_IO_OUTBUFFER - SIZE_ABS_OUTBLK)
#define ADDR_ABS_OUTBLK_RIGHT	(SIZE_IO_OUTBUFFER - (SIZE_ABS_OUTBLK * 2))
#define ADDR_ABS_OUTBLK_LEFT	(SIZE_IO_OUTBUFFER - (SIZE_ABS_OUTBLK * 3))
#define ADDR_ABS_INBLK_MID		(SIZE_IO_INBUFFER - (SIZE_ABS_INBLK * 4))
#define ADDR_ABS_INBLK_RIGHT	(SIZE_IO_INBUFFER - (SIZE_ABS_INBLK * 5))
#define ADDR_ABS_INBLK_LEFT		(SIZE_IO_INBUFFER - (SIZE_ABS_INBLK * 6))

/* ----------------------------------------------------------------------- */
extern BYTE _pInBuffer[];				/* 256 */
extern BYTE _pOutBuffer[];   			/* 256 */
extern unsigned char VMEM[];	
/* ----------------------------------------------------------------------- */

// initialize Buffer pointer.
BYTE *_pRightInBlkBuf	= &_pInBuffer[ ADDR_ABS_INBLK_RIGHT ];
BYTE *_pLeftInBlkBuf	= &_pInBuffer[ ADDR_ABS_INBLK_LEFT ];
BYTE *_pMidInBlkBuf		= &_pInBuffer[ ADDR_ABS_INBLK_MID ];

BYTE *_pRightOutBlkBuf	= &_pOutBuffer[ ADDR_ABS_OUTBLK_RIGHT ];
BYTE *_pLeftOutBlkBuf	= &_pOutBuffer[ ADDR_ABS_OUTBLK_LEFT ];
BYTE *_pMidOutBlkBuf	= &_pOutBuffer[ ADDR_ABS_OUTBLK_MID ];

BYTE *_pTimeBuf = &VMEM[OFFSET_TIME_TABLE]; 	

/* ----------------------------------------------------------------------- */
WDOG_ID g_widLeftModem = NULL;
WDOG_ID g_widRightModem = NULL;
WDOG_ID g_widMidModem = NULL;


int  g_nLeftModemTimeTicked = 0;
int  g_nRightModemTimeTicked = 0;
int  g_nMidModemTimeTicked = 0;

/* ----------------------------------------------------------------------- */
int  rSetLeftModemTimeTick() 
{
	g_nLeftModemTimeTicked = 1;
}
int  rSetRightModemTimeTick() 
{
	g_nRightModemTimeTicked = 1;
}
int  rSetMidModemTimeTick() 
{
	g_nMidModemTimeTicked = 1;
}

void rClearLeftModemTimeTick() 
{
	g_nLeftModemTimeTicked = 0;
}
void rClearRightModemTimeTick() 
{
	g_nRightModemTimeTicked = 0;
}
void rClearMidModemTimeTick() 
{
	g_nMidModemTimeTicked = 0;
}

/* ----------------------------------------------------------------------- */
void CommLeftDone( CComModem *pModem, CComModem *pEth )
{
	int nStatus;
	BYTE LeftFailCnt = 0;

	if ( g_widLeftModem != NULL ) {
		wdCancel( g_widLeftModem );
	}
	rClearLeftModemTimeTick();	// clear time_over flag.
    g_widLeftModem = wdCreate();
    wdStart( g_widLeftModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetLeftModemTimeTick, 0 );
   
	nStatus = 1;

	if((pModem->m_nDirection == BLOCK_DIR_DOWN) || (pModem->m_bIsBlkDev == TRUE))
	{
		pModem->WriteFrame();
	} 

	while(1) 
	{
		if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
		{
			nStatus = pModem->ReadFrame(TRUE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}
		else
		{
			nStatus = pModem->ReadFrame(FALSE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}

		if(nStatus > 0)
		{
		// normal status at received RX.
			pModem->SetStatus(3);
			LeftFailCnt = 0;

			pModem->WriteFrame();

			// reset tick.
			rClearLeftModemTimeTick();	// clear time_over flag.
		    wdStart( g_widLeftModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetLeftModemTimeTick, 0 );
		}
		else 
		{
			// none RX data
			if(g_nLeftModemTimeTicked) 
			{	// time over.
				if (LeftFailCnt >= 0x80)		// 2013.3.21   0x80 -> 0xf0 -> 0x80
				{
					LogErr(LOG_TYPE_BLOCK, "Left Communication is Down.");

					pModem->SetStatus(0);	// all modems are failed.
					if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
					{
			    		pModem->ClearIO();		// clear io buffer of block.
					}
					//TMDout3(0x02);
					//taskDelay(100);
					//TMDout3(0x0C);
				}
				else
				{
					LeftFailCnt += 0x10;
					LogDbg(LOG_TYPE_BLOCK, "Left Modem Fail Count = 0x%X", LeftFailCnt);
				}

				if((pModem->m_nDirection == BLOCK_DIR_DOWN) || (pModem->m_bIsBlkDev == TRUE))
				{
					pModem->WriteFrame();
				} 

				// reset tick.
				rClearLeftModemTimeTick();	// clear time_over flag.
			    wdStart( g_widLeftModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetLeftModemTimeTick, 0 );
			}
		}
		taskDelay(300);
	}
	wdCancel( g_widLeftModem );
}

void CommRightDone( CComModem *pModem, CComModem *pEth )
{
	int nStatus;
	BYTE RightFailCnt=0;

	if ( g_widRightModem != NULL ) 
	{
		wdCancel( g_widRightModem );
	}
	rClearRightModemTimeTick();	// clear time_over flag.
    g_widRightModem = wdCreate();
    wdStart( g_widRightModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetRightModemTimeTick, 0 );
    
	nStatus = 1;

	if((pModem->m_nDirection == BLOCK_DIR_DOWN) || (pModem->m_bIsBlkDev == TRUE))
	{
		pModem->WriteFrame();
	}

	while(1) 
	{
		if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
		{
			nStatus = pModem->ReadFrame(TRUE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}
		else
		{
			nStatus = pModem->ReadFrame(FALSE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}

		if(nStatus > 0) 
		{
		// normal status at recieved RX.
			pModem->SetStatus(3);
			RightFailCnt = 0;

			pModem->WriteFrame();

			// reset tick.
			rClearRightModemTimeTick();	// clear time_over flag.
		    wdStart( g_widRightModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetRightModemTimeTick, 0 );
		}
		else 
		{
			if(g_nRightModemTimeTicked) 
			{	// time over.
				if (RightFailCnt >= 0x80)      //2013.3.21   0x80 -> 0xf0 -> 0x80
				{
					LogErr(LOG_TYPE_BLOCK, "Right Modem is Down.");

					pModem->SetStatus(0);	// all modems are failed.
					if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
					{
			    		pModem->ClearIO();		// clear io buffer of block.
					}
					//TMDout3(0x01);
					//taskDelay(100);
					//TMDout3(0x0C);
				}
				else
				{
					RightFailCnt += 0x10;
					LogDbg(LOG_TYPE_BLOCK, "Right Modem Fail Count = 0x%X", RightFailCnt);
				}

				if((pModem->m_nDirection == BLOCK_DIR_DOWN) || (pModem->m_bIsBlkDev == TRUE))
				{
					pModem->WriteFrame();
				}

				// reset tick.
				rClearRightModemTimeTick();	// clear time_over flag.
			    wdStart( g_widRightModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetRightModemTimeTick, 0 );
			}
		}
		taskDelay(300);
	}
	wdCancel( g_widRightModem );
}

void CommMidDone( CComModem *pModem, CComModem *pEth )
{
	int nStatus;
	BYTE MidFailCnt = 0;

	if ( g_widMidModem != NULL ) {
		wdCancel( g_widMidModem );
	}
	rClearMidModemTimeTick();	// clear time_over flag.
    g_widMidModem = wdCreate();
    wdStart( g_widMidModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetMidModemTimeTick, 0 );
    
	nStatus = 1;

	pModem->WriteFrame();

	while(1) 
	{
		if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
		{
			nStatus = pModem->ReadFrame(TRUE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}
		else
		{
			nStatus = pModem->ReadFrame(FALSE);	// -1/not fail, 0/read, 1 or 2 or 3/recceive done.
		}

		if(nStatus > 0)
		{
		// normal status at received RX.
			pModem->SetStatus(3);
			MidFailCnt = 0;

			pModem->WriteFrame();

			// reset tick.
			rClearMidModemTimeTick();	// clear time_over flag.
		    wdStart( g_widMidModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetMidModemTimeTick, 0 );
		}
		else 
		{
			// none RX data
			if(g_nMidModemTimeTicked) 
			{	// time over.
				if (MidFailCnt >= 0x80)		// 2013.3.21   0x80 -> 0xf0 -> 0x80
				{
					LogErr(LOG_TYPE_BLOCK, "Mid Communication is Down.");

					pModem->SetStatus(0);	// all modems are failed.
					if((pEth == NULL) || ((pEth != NULL) && pEth->m_bLANCommFail))
					{
			    		pModem->ClearIO();		// clear io buffer of block.
					}
					//TMDout3(0x02);
					//taskDelay(100);
					//TMDout3(0x0C);
				}
				else
				{
					MidFailCnt += 0x10;
					LogDbg(LOG_TYPE_BLOCK, "Mid Modem Fail Count = 0x%X", MidFailCnt);
				}

				pModem->WriteFrame();

				// reset tick.
				rClearMidModemTimeTick();	// clear time_over flag.
			    wdStart( g_widMidModem, MAX_WAITTICK_BLOCK, (FUNCPTR)rSetMidModemTimeTick, 0 );
			}
		}
		taskDelay(300);
	}
	wdCancel( g_widMidModem );
}

void CommEthDone( CComModem *pModem )
{
	BYTE nFailCnt = 0;
	int nResult = 0;

    STATUS  status;
    int     width = 0;

	struct	timeval selTimeOut;
	struct	fd_set  readFds, tmpFds;

	int 	nfd;

    selTimeOut.tv_sec  = SELTIMEOUT_BLOCK;
    selTimeOut.tv_usec = 0;

    FD_ZERO(&readFds);
    FD_SET(pModem->m_NetPRI.m_nfd, &readFds);
	if(pModem->m_bIsDualNet)
	{
    	FD_SET(pModem->m_NetSEC.m_nfd, &readFds);
    	width = max(pModem->m_NetPRI.m_nfd, pModem->m_NetSEC.m_nfd);
	}
	else
	{
    	width = max(pModem->m_NetPRI.m_nfd, 0);
	}

	width++;

    while(FORever)
	{
        tmpFds = readFds;
        status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
		if(status == ERROR)
		{
        	LogErr(LOG_TYPE_BLOCK, "Select Error");
			taskDelay(SELTIMEOUT_BLOCK * 600);

			if (nFailCnt >= 0x30)		// 2013.3.21   0x80 -> 0xf0 -> 0x80
			{
				if(pModem->m_nDirection == BLOCK_DIR_UP)
				{
					LogErr(LOG_TYPE_BLOCK, "Up Direction Eth Communication is Down.");
				}
				else if(pModem->m_nDirection == BLOCK_DIR_DOWN)
				{
					LogErr(LOG_TYPE_BLOCK, "Down Direction Eth Communication is Down.");
				}

				pModem->SetStatus(0);	// all modems are failed.

				// Backup Modem 사용 중이면 버퍼 초기화 하지 않는다.
				if(pModem->m_bUseBkup)
				{
					pModem->m_bLANCommFail = 1;
				}
				else
				{
			    	pModem->ClearIO();		// clear io buffer of block.
				}
			}
			else
			{
				// timeout
				nFailCnt += 0x10;
				LogDbg(LOG_TYPE_BLOCK, "Eth Modem Fail Count = 0x%X", nFailCnt);
			}

			continue;
		}
		else if(status == 0)	// select의 타임아웃이 소요되었으므로 taskDelay를 넣지 않는다.
		{
			if(nFailCnt >= 0x30)
			{
				if(pModem->m_nDirection == BLOCK_DIR_UP)
				{
					LogErr(LOG_TYPE_BLOCK, "Up Direction Eth Communication is Down.");
				}
				else if(pModem->m_nDirection == BLOCK_DIR_DOWN)
				{
					LogErr(LOG_TYPE_BLOCK, "Down Direction Eth Communication is Down.");
				}

				pModem->SetStatus(0);	// all modems are failed.

				// Backup Modem 사용 중이면 버퍼 초기화 하지 않는다.
				if(pModem->m_bUseBkup)
				{
					pModem->m_bLANCommFail = 1;
				}
				else
				{
			    	pModem->ClearIO();		// clear io buffer of block.
				}
			}
			else
			{
				// timeout
				nFailCnt += 0x10;
				LogDbg(LOG_TYPE_BLOCK, "Eth Modem Fail Count = 0x%X", nFailCnt);
			}

			continue;
		}

		nResult = 0;

	
		if(FD_ISSET(pModem->m_NetPRI.m_nfd, &readFds)) 
		{
			nResult = pModem->RecvFromNet(1);
		}

		if(pModem->m_bIsDualNet)
		{
			if(FD_ISSET(pModem->m_NetSEC.m_nfd, &readFds)) 
			{
				nResult = pModem->RecvFromNet(2);
			}
		}

		if(nResult > 0)
		{
			// normal status at received RX.
			pModem->SetStatus(3);
			nFailCnt = 0;

			if(pModem->m_bUseBkup)
			{
				pModem->m_bLANCommFail = 0;
			}
		}

		taskDelay(SELTIMEOUT_BLOCK * 300);
	}
}

void CommEthSend()
{
    while(FORever)
	{
		if(_ComBlockRight.m_bIsNetwork)
		{
			_ComBlockRight.SendToNet();
		}

		if(_ComBlockLeft.m_bIsNetwork)
		{
			_ComBlockLeft.SendToNet();
		}

		if(_ComBlockMid.m_bIsNetwork)
		{
			_ComBlockMid.SendToNet();
		}

		taskDelay(SELTIMEOUT_BLOCK * 600);
	}
}

/* ----------------------------------------------------------------------- */

PROCESS ModemRightDrv(void)	// Right - master(client-auto write)
{
	CommRightDone( &_ComBlockRight, NULL );
}

PROCESS ModemLeftDrv(void)	// Left - slave(answer)
{
	CommLeftDone( &_ComBlockLeft, NULL );
}

PROCESS ModemMidDrv(void)	// Mid - slave(answer)
{
	CommMidDone( &_ComBlockMid, NULL );
}

PROCESS BkupRightDrv(void)	// Right - master(client-auto write)
{
	CommRightDone( &_ComBkupRight, &_ComBlockRight );
}

PROCESS BkupLeftDrv(void)	// Left - slave(answer)
{
	CommLeftDone( &_ComBkupLeft, &_ComBlockLeft );
}

PROCESS BkupMidDrv(void)	// Mid - slave(answer)
{
	CommMidDone( &_ComBkupMid, &_ComBlockMid );
}

PROCESS EthRightDrv(void)
{
	CommEthDone( &_ComBlockRight );
}

PROCESS EthLeftDrv(void)
{
	CommEthDone( &_ComBlockLeft );
}

PROCESS EthMidDrv(void)
{
	CommEthDone( &_ComBlockMid );
}

PROCESS EthSendDrv(void)
{
	CommEthSend();
}


/* ----------------------------------------------------------------------- */
BYTE GetStatusOfDownBLOCK()
{
	LogDbg(LOG_TYPE_BLOCK, "BLOCK_DIR_DOWN[%d], _ComBlockLeft Dir[%d] = %d, _ComBlockRight Dir[%d] = %d", BLOCK_DIR_DOWN, _ComBlockLeft.m_nDirection, _ComBlockLeft.GetStatus(), _ComBlockRight.m_nDirection, _ComBlockRight.GetStatus());

	return _ComBlockLeft.GetStatus() || (_ComBlockLeft.m_bUseBkup && _ComBkupLeft.GetStatus());	// 3/normal, 0/error.
}

BYTE GetStatusOfUpBLOCK()
{
	LogDbg(LOG_TYPE_BLOCK, "BLOCK_DIR_UP[%d], _ComBlockLeft Dir[%d] = %d, _ComBlockRight Dir[%d] = %d", BLOCK_DIR_UP, _ComBlockLeft.m_nDirection, _ComBlockLeft.GetStatus(), _ComBlockRight.m_nDirection, _ComBlockRight.GetStatus());

	return _ComBlockRight.GetStatus() || (_ComBlockRight.m_bUseBkup && _ComBkupRight.GetStatus());	// 3/normal, 0/error.
}

BYTE GetStatusOfMidBLOCK()
{
	return _ComBlockMid.GetStatus() || (_ComBlockMid.m_bUseBkup && _ComBkupMid.GetStatus());	// 3/normal, 0/error.
}

void ModemTaskRun(BYTE bReverse, BYTE bUseSAXL, BYTE bUPNewIfc, BYTE bDNNewIfc, BYTE *LeftPorts, BYTE *RightPorts, BYTE *MidPorts, BLOCK_NET_ADDR *pNetAddr)
{
	int nRet;

	if(bReverse)
	{
		if(pNetAddr->bUseDNNet)
		{
			_ComBlockLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, bDNNewIfc, pNetAddr );	// Left direction
			g_nTaskIDFor308[ TASKID_LEFT_BLOCK ] = taskSpawn("EthLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthLeftDrv), 0,0,0,0,0,0,0,0,0,0);

			if(pNetAddr->bUseModem)
			{
				_ComBkupLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, TRUE, LeftPorts );	// Left direction
				if((LeftPorts[0] != 0) || (LeftPorts[1] != 0))
				{
					g_nTaskIDFor308[ TASKID_LEFT_BKUP ] = taskSpawn("BkupLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(BkupLeftDrv), 0,0,0,0,0,0,0,0,0,0);
				}
			}
		}
		else
		{
			_ComBlockLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, FALSE, LeftPorts );	// Left direction
			if((LeftPorts[0] != 0) || (LeftPorts[1] != 0))
			{
				g_nTaskIDFor308[ TASKID_LEFT_BLOCK ] = taskSpawn("ModemLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(ModemLeftDrv), 0,0,0,0,0,0,0,0,0,0);
			}
		}

		if(pNetAddr->bUseUPNet)
		{
			_ComBlockRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, bUPNewIfc, pNetAddr );	// Right direction
			g_nTaskIDFor308[ TASKID_RIGHT_BLOCK ]  = taskSpawn("EthRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthRightDrv), 0,0,0,0,0,0,0,0,0,0);

			if(pNetAddr->bUseModem)
			{
				_ComBkupRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, TRUE, RightPorts );	// Right direction
				if((RightPorts[0] != 0) || (RightPorts[1] != 0))
				{
					g_nTaskIDFor308[ TASKID_RIGHT_BKUP ]  = taskSpawn("BkupRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(BkupRightDrv), 0,0,0,0,0,0,0,0,0,0);
				}
			}
		}
		else
		{
			_ComBlockRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, FALSE, RightPorts );	// Right direction
			if((RightPorts[0] != 0) || (RightPorts[1] != 0))
			{
				g_nTaskIDFor308[ TASKID_RIGHT_BLOCK ]  = taskSpawn("ModemRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(ModemRightDrv), 0,0,0,0,0,0,0,0,0,0);
			}
		}
	}
	else
	{
		if(pNetAddr->bUseUPNet)
		{
			_ComBlockLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, bUPNewIfc, pNetAddr );		// Left direction
			g_nTaskIDFor308[ TASKID_LEFT_BLOCK ]  = taskSpawn("EthLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthLeftDrv), 0,0,0,0,0,0,0,0,0,0);

			if(pNetAddr->bUseModem)
			{
				_ComBkupLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, TRUE, LeftPorts );	// Left direction
				if((LeftPorts[0] != 0) || (LeftPorts[1] != 0))
				{
					g_nTaskIDFor308[ TASKID_LEFT_BKUP ] = taskSpawn("BkupLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(BkupLeftDrv), 0,0,0,0,0,0,0,0,0,0);
				}
			}
		}
		else
		{
			_ComBlockLeft.Initialize( _pLeftInBlkBuf, _pLeftOutBlkBuf, _pTimeBuf, BLOCK_DIR_UP, bReverse, bUseSAXL, FALSE, LeftPorts );		// Left direction
			if((LeftPorts[0] != 0) || (LeftPorts[1] != 0))
			{
				g_nTaskIDFor308[ TASKID_LEFT_BLOCK ]  = taskSpawn("ModemLeftDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(ModemLeftDrv), 0,0,0,0,0,0,0,0,0,0);
			}
		}

		if(pNetAddr->bUseDNNet)
		{
			_ComBlockRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, bDNNewIfc, pNetAddr );	// Right direction
			g_nTaskIDFor308[ TASKID_RIGHT_BLOCK ]  = taskSpawn("EthRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthRightDrv), 0,0,0,0,0,0,0,0,0,0);

			if(pNetAddr->bUseModem)
			{
				_ComBkupRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, TRUE, RightPorts );	// Right direction
				if((RightPorts[0] != 0) || (RightPorts[1] != 0))
				{
					g_nTaskIDFor308[ TASKID_RIGHT_BKUP ]  = taskSpawn("BkupRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(BkupRightDrv), 0,0,0,0,0,0,0,0,0,0);
				}
			}
		}
		else
		{
			_ComBlockRight.Initialize( _pRightInBlkBuf, _pRightOutBlkBuf, _pTimeBuf, BLOCK_DIR_DOWN, bReverse, bUseSAXL, FALSE, RightPorts );	// Right direction
			if((RightPorts[0] != 0) || (RightPorts[1] != 0))
			{
				g_nTaskIDFor308[ TASKID_RIGHT_BLOCK ] = taskSpawn("ModemRightDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(ModemRightDrv), 0,0,0,0,0,0,0,0,0,0);
			}
		}

	}

	if(pNetAddr->bUseMDNet)
	{
		_ComBlockMid.Initialize( _pMidInBlkBuf, _pMidOutBlkBuf, _pTimeBuf, BLOCK_DIR_MID, bReverse, bUseSAXL, FALSE, pNetAddr );	// Mid direction
		g_nTaskIDFor308[ TASKID_MID_BLOCK ] = taskSpawn("EthMidDrv", TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthMidDrv), 0,0,0,0,0,0,0,0,0,0);

		if(pNetAddr->bUseModem)
		{
			_ComBkupMid.Initialize( _pMidInBlkBuf, _pMidOutBlkBuf, _pTimeBuf, BLOCK_DIR_MID, bReverse, bUseSAXL, TRUE, MidPorts );	// Mid direction
			if((MidPorts[0] != 0) || (MidPorts[1] != 0))
			{
				g_nTaskIDFor308[ TASKID_MID_BKUP ] = taskSpawn("BkupMidDrv", TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(BkupMidDrv), 0,0,0,0,0,0,0,0,0,0);
			}
		}
	}
	else if(MidPorts[0] > 0)
	{
		_ComBlockMid.Initialize( _pMidInBlkBuf, _pMidOutBlkBuf, _pTimeBuf, BLOCK_DIR_MID, bReverse, bUseSAXL, FALSE, MidPorts );	// Mid direction
		if((MidPorts[0] != 0) || (MidPorts[1] != 0))
		{
			g_nTaskIDFor308[ TASKID_MID_BLOCK ] = taskSpawn("ModemMidDrv", TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(ModemMidDrv), 0,0,0,0,0,0,0,0,0,0);
		}
	}

	if(pNetAddr->bUseUPNet || pNetAddr->bUseDNNet || pNetAddr->bUseMDNet)
	{
		g_nTaskIDFor308[ TASKID_SEND_BLOCK ] = taskSpawn("EthSendDrv" , TASKPR_BLOCK, 0, 0x3000, (FUNCPTR)(EthSendDrv), 0,0,0,0,0,0,0,0,0,0);
	}
}

char *GetBlockChName(int nCh)
{
	switch(nCh)
	{
	case PORTNUM_BLOCK1:
		return "BLOCK1";
	case PORTNUM_BLOCK2:
		return "BLOCK2";
	case PORTNUM_BLOCK3:
		return "BLOCK3";
	case PORTNUM_BLOCK4:
		return "BLOCK4";
	default:
		return " ";
		break;
	}
}
