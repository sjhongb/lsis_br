/* -------------------------------------------------------------------------+
|  ComModeom.h                                                              
|  -----------                                                              
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _COMMODEM_H
#define _COMMODEM_H

#include "CommDrv.h"
#include "CommEth.h"

/* ----------------------------------------------------------------------- */
#define MAX_TELEGRAM_MSG_SIZE	16
#define MAX_BCDTIME_SIZE		7
#define MAX_BUFFER_SIZE			256

#define NEW_BLK_STX_POS		0
#define NEW_BLK_SEQ_POS		1
#define NEW_BLK_LEN_POS		2
#define NEW_BLK_SRC_POS		4
#define NEW_BLK_DST_POS		8
#define NEW_BLK_DATA_POS	12

#define NEW_BLK_STX			0xFE
#define NEW_BLK_SEQ_INIT	0x00
#define NEW_BLK_SEQ_MIN		0x01
#define NEW_BLK_SEQ_MAX		0xFF

#define MAX_NEW_BLK_PACKET_SIZE	56

/* TOKENLESS BLOCK DIRECTION */
#define BLOCK_DIR_UNDEF	-1
#define BLOCK_DIR_DOWN	1
#define BLOCK_DIR_UP	2
#define BLOCK_DIR_MID	3

#define UDP_BLOCK_PORT 8487

/* ----------------------------------------------------------------------- */
/* Defined Telegram Structure */
typedef struct tagComTelegramType {
	BYTE nTA;
	BYTE nTC;
	BYTE nLength;
	BYTE pData[ MAX_TELEGRAM_MSG_SIZE ];
	BYTE nCRC_L;
	BYTE nCRC_H;
} ComTelegramType;

/* Defined Frame Structure */
typedef struct tagComFreamBlkType {
	BYTE nSTX;
	BYTE nFC;
	BYTE nCYC;
	BYTE nLength;
	BYTE nYH;		// YY
	BYTE nYL;		// YY
	BYTE nMon;		// mm
	BYTE nDate;		// DD
	BYTE nHour;		// HH
	BYTE nMin;		// MM
	BYTE nSec;		// SS
	ComTelegramType	ATelegram;
	ComTelegramType	BTelegram;
	BYTE nCRC_L;
	BYTE nCRC_H;
	BYTE nETX;
} ComBlkFreamType;

/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
/* CComModem class */

class CComModemDriver : public CommDriver 
{
/* Attributes */
public:

	BYTE m_nRXFCYC;			// RX Frame Cycle No.
	BYTE m_nRXTCYC;			// RX Telegrame Cycle No.

	BYTE m_pIOBuffer[MAX_TELEGRAM_MSG_SIZE+1];	// I/O block.
	BYTE m_pTimeBuffer[MAX_BCDTIME_SIZE+1];		// time in RX frame.
	
//	WORD m_nComFailCount;
//	WORD m_nCRCFailCount;
//	WORD m_nOtherFailCount;
	short m_bUpdate;
//	short m_nErrorNo;
	short m_nDirection;			// block direction.
	BYTE m_bReverse;
	BYTE m_bUseSAXL;

	BYTE 	m_cRxPtr;
	
/* Construction */
	CComModemDriver();
	
/* Implementation */
public:
	~CComModemDriver();
	
	short Initialize( short nDirection, BYTE bReverse, BYTE bUseSAXL, int nPortNo, int bUsed, int bUsedMsgQ );
	int GetRxFrame();
	int CheckFrame( int nSize, BYTE *pRcvBuf );
	void UpdateIO( BYTE *pBuffer );
	void ClearIO();
	
	BYTE *GetTimeBuffer()
		{
		return &m_pTimeBuffer[0];
		}
	
	BYTE *GetIOBuffer()
		{
		return &m_pIOBuffer[0];
		}
	
//	short GetErrorNo()
//		{
//		return m_nErrorNo;
//		}
	
/* Overrides */
	virtual short WriteBlock( char *pBuffer, short length );
	
protected:
	int CheckTelegramBlock( BYTE *pBlk );
};

/* ----------------------------------------------------------------------- */
/* CComModem class */

class CComModem
{
/* Attributes */
public:
	BYTE *m_pInBlkTable;	// Input block table.
	BYTE *m_pOutBlkTable;	// Output block table.
	BYTE *m_pTimeTable;		// Output Time and Date structure table(BCD 7 Byte).
	
	BYTE m_nTXFCYC;			// TX Frame Cycle No.
	BYTE m_nTXTCYC;			// TX Telegram Cycle No.
	BYTE m_nRXFCYC;			// RX Frame Cycle No.

	short m_nStatus;
	short m_nDirection;		// block direction.
	BYTE m_bReverse;
	BYTE m_bUseSAXL;
	BYTE m_bNewIfc;

	BYTE m_bIsNetwork;
	BYTE m_bIsDualNet;
	BYTE m_bIsBlkDev;

	BYTE m_bUseBkup;
	BYTE m_bLANCommFail;

	BYTE m_cMyStnID;
	BYTE m_cPeerStnID;
	

	CComModemDriver m_Modem1;
	CComModemDriver m_Modem2;

	CommEth	m_NetPRI;
	CommEth m_NetSEC;

	BYTE m_TxFrame[MAX_BUFFER_SIZE];	// write buffer of TX data.

/* Construction */
	CComModem();
	
/* Implementation */
public:
	~CComModem();
	
	void ClearIO();
	int WriteFrame();
	int ReadFrame(BYTE bUpdate);

	int SendToNet();
	int RecvFromNet(BYTE nNetCh);
	
	void SetStatus(short nStatus)
	{
		m_nStatus = nStatus;
	}
	short GetStatus()
	{
		return m_nStatus;
	}
	short Initialize( BYTE *pInBlkBuf, BYTE *pOutBlkBuf, BYTE *pTimeBuf, short nDirection, BYTE bReverse, BYTE bUseSAXL, BYTE bUseBkup, BYTE *ComPorts);
	short Initialize( BYTE *pInBlkBuf, BYTE *pOutBlkBuf, BYTE *pTimeBuf, short nDirection, BYTE bReverse, BYTE bUseSAXL, BYTE bNewIfc, BLOCK_NET_ADDR *pNetAddr);
	
protected:
	BYTE*MakeTelegram( BYTE *pTXBlock, BYTE *pOutBlk, int bBSide=0 );
	int MakeCommFrame( BYTE *pTXBlock, BYTE *pOutBlk );
};

/* ----------------------------------------------------------------------- */

char *GetBlockChName(int nCh);
#endif // _COMMODEM_H
