/* -------------------------------------------------------------------------+
|  Brdctrl.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     Brdctrl.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _BRDCTRL_H
#define _BRDCTRL_H

/* ------------------------------------------------------------------------- */
#define MAX_BOARD_NUM 		64
#define MAX_IOBUFFER_SIZE 	(64*4)		// 256
#define MAX_MIOBUFFER_SIZE 	((64*4)+4)	// 260
#define IOTYPE_FDIM			1
#define IOTYPE_FDOM			2

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

class CBoardCtrl 
{
protected:
	int m_bIsSub;
	int m_bError;
	short int m_nIllegalIOFailure;
	//
public:	
	unsigned short int m_pBoardInfo[MAX_BOARD_NUM];		// current status of I/O board
	unsigned short int m_pRealID[MAX_BOARD_NUM];		// accessing ID of I/O board
	unsigned short int m_pFaultID[MAX_BOARD_NUM];
	//
	unsigned char m_pBoardImage[8];		// Max 64 boards

protected:
	unsigned char m_pMaxInBuffer[MAX_MIOBUFFER_SIZE];		//
	unsigned char m_pInBuffer[MAX_IOBUFFER_SIZE];
	unsigned char m_pOutBufferSave[MAX_MIOBUFFER_SIZE];		//
	unsigned char m_pOutOldBuffer[MAX_IOBUFFER_SIZE];		// 
	unsigned char m_pOutBuffer[MAX_IOBUFFER_SIZE];			// interlocking result output data buffer.
	unsigned char m_pOut1thFailBuffer[MAX_IOBUFFER_SIZE];	// output failure data new buffer.
	unsigned char m_pOut2ndFailBuffer[MAX_IOBUFFER_SIZE];	// output failure data old1 buffer.
	unsigned char m_pOut3rdFailBuffer[MAX_IOBUFFER_SIZE];	// output failure data old2 buffer.
	unsigned char m_pOutSetFailBuffer[MAX_IOBUFFER_SIZE];	// output failure data (old2 AND old1 AND new) buffer.

public:
	CBoardCtrl();		
	~CBoardCtrl();

	void dumyInOut();
	void SetBoardInfo(unsigned char *pInfoTable, unsigned char *pRealID);
	int  MakeBoardImage(/*int bSysBRD*/);
	short int IsIllegalIOFailure()
	{
		return m_nIllegalIOFailure;
	}
//	void ClearIOBoardImage();
	short int OutputAll( short isSub = 0 );
	short int InputAll();
	short int ScanIO_InputBoard();
	short int ScanIO_OutputBoard();
	short int ScanIO();
	short int CheckErrPort();
};

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

#include "commdrv.h"
class Comm_IOSIM {
public:
	CommDriver m_ComSim;
	short m_nTxBlockSize;
	short m_nRxBlockSize;
	int m_nPortNo;

	Comm_IOSIM();
	~Comm_IOSIM();
	short Initialize();
	short ReciveSimdata( unsigned char * pBuffer );
};

extern Comm_IOSIM SimCom;


/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
extern unsigned char _pInBuffer[];
extern unsigned char _pOutBuffer[];
extern CBoardCtrl EIS_IO;

/* ------------------------------------------------------------------------- */

#endif
