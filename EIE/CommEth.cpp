/* -------------------------------------------------------------------------+
|  CommEth.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommEth.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver.
|                                                                           
|  4. Project Name.                                                         
|     Tongi,Laksam,13station by BR.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- LS402A and LS314                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
/* standard c library */
#include <stdio.h>
#include <string.h>

/* vxWorks library */
#include "taskLib.h"
#include "sockLib.h"

#include "../INCLUDE/LogTask.h"
#include "Crc.h"
#include "ComModem.h"
#include "CommEth.h"

extern unsigned short CRCtable[];

struct      fd_set  readFds_CC1, tmpFds_CC1;	/* for Training MCCR */
struct      fd_set  readFds_CC2, tmpFds_CC2;	/* for Training MCCR */
struct      fd_set  readFds_CC3, tmpFds_CC3;	/* for Training MCCR */
struct      fd_set  readFds_CC4, tmpFds_CC4;	/* for Training MCCR */
struct      fd_set  readFds_CC5, tmpFds_CC5;	/* for Training MCCR */
struct      fd_set  readFds_CC6, tmpFds_CC6;	/* for Training MCCR */

struct      timeval selTimeOut_CC1;
struct      timeval selTimeOut_CC2;
struct      timeval selTimeOut_CC3;
struct      timeval selTimeOut_CC4;
struct      timeval selTimeOut_CC5;
struct      timeval selTimeOut_CC6;

/* ------------------------------------------------------------------------- */
CommEth::CommEth() 
{
	m_bIsDualDest = FALSE;
}

CommEth::~CommEth() 
{
}

BYTE CommEth::Initialize(const char *strLocalAddr, const USHORT nLocalPort, 
						 const char *strDestAddr, const USHORT nDestPort)
{
	bzero((char*)&m_LocalAddr, sizeof(m_LocalAddr));
	m_LocalAddr.sin_family		= AF_INET;
	m_LocalAddr.sin_port		= htons(nLocalPort);
	m_LocalAddr.sin_addr.s_addr	= inet_addr(strLocalAddr); 

	bzero((char*)&m_DestAddr, sizeof(m_DestAddr));
	m_DestAddr.sin_family		= AF_INET;
	m_DestAddr.sin_port			= htons(nDestPort);
	m_DestAddr.sin_addr.s_addr	= inet_addr(strDestAddr);  

	if((m_nfd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_CC, "Socket Create Error!!");
		return (ERROR);
	}

	if(bind(m_nfd, (struct sockaddr *)&m_LocalAddr, (int)(sizeof(m_LocalAddr))) == ERROR)
	{
		LogErr(LOG_TYPE_CC, "Socket Bind Error!!");
		return (ERROR);
	}

	return OK;
}

BYTE CommEth::Add2ndDest(const char *strDestAddr, const USHORT nDestPort)
{
	bzero((char*)&m_DestAddr2, sizeof(m_DestAddr));
	m_DestAddr2.sin_family		= AF_INET;
	m_DestAddr2.sin_port		= htons(nDestPort);
	m_DestAddr2.sin_addr.s_addr	= inet_addr(strDestAddr);  

	m_bIsDualDest = TRUE;
}	

USHORT CommEth::Send(BYTE *pBuffer, USHORT nLength)
{
	USHORT nSendSize = 0;

	nSendSize = sendto(m_nfd, (char*)pBuffer, (size_t)nLength, 0, (struct sockaddr *)&m_DestAddr, (int)(sizeof(m_DestAddr)));

	if(m_bIsDualDest)
	{
		nSendSize += sendto(m_nfd, (char*)pBuffer, (size_t)nLength, 0, (struct sockaddr *)&m_DestAddr2, (int)(sizeof(m_DestAddr2)));
	}

	return nSendSize;
}

USHORT CommEth::SendForNewIfc(BYTE *pBuffer, USHORT nLength, BYTE c1stDevID, BYTE c2ndDevID)
{
	USHORT nSendSize = 0;
	UINT32	uiCalcCRC32;

	pBuffer[NEW_BLK_DST_POS + 2] = c1stDevID;

	// CRC
	uiCalcCRC32 = checkCrc32(pBuffer, nLength - LENBYTE_CRC32);
	memcpy(&pBuffer[nLength - LENBYTE_CRC32], &uiCalcCRC32, LENBYTE_CRC32);
	
	nSendSize = sendto(m_nfd, (char*)pBuffer, (size_t)nLength, 0, (struct sockaddr *)&m_DestAddr, (int)(sizeof(m_DestAddr)));

	if(m_bIsDualDest)
	{
		pBuffer[NEW_BLK_DST_POS + 2] = c2ndDevID;
		
		// CRC
		uiCalcCRC32 = checkCrc32(pBuffer, nLength - LENBYTE_CRC32);
		memcpy(&pBuffer[nLength - LENBYTE_CRC32], &uiCalcCRC32, LENBYTE_CRC32);
		
		nSendSize += sendto(m_nfd, (char*)pBuffer, (size_t)nLength, 0, (struct sockaddr *)&m_DestAddr2, (int)(sizeof(m_DestAddr2)));
	}

	return nSendSize;
}


USHORT CommEth::Recv(BYTE *pBuffer, USHORT nLength)
{
	bzero((char*)&m_RecvAddr, sizeof(m_RecvAddr));
	int nAddrLen = sizeof(m_RecvAddr);
	
	short iRcvdSize = 0;
	short iRecvSize = 0;

	while(1)
	{
		iRecvSize = recvfrom(m_nfd, (char*)pBuffer, (size_t)nLength, MSG_DONTWAIT, (struct sockaddr *)&m_RecvAddr, (int*)&nAddrLen);
		if(iRecvSize > 0)
		{
			iRcvdSize = iRecvSize;
		}
		else
		{
			break;
		}
	}

	return (USHORT)iRcvdSize;
}

