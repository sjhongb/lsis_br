/* -------------------------------------------------------------------------+
|  CommGPS.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommGPS.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for GPS Server.
|                                                                           
|  4. Project Name.                                                         
|     Tongi/13Stations PJT. of BR                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- LS-402A                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */ 
#ifndef _COMMGPS_H
#define _COMMGPS_H

/* ------------------------------------------------------------------------ */
#include "inetLib.h"

#include "projdef.h"
#include "crc.h"

/* ------------------------------------------------------------------------ */
class Comm_GPS 
{
	Ccrc 	m_CRC;

	BYTE m_cTxSeqNo;
	BYTE m_cRxSeqNo;
	BYTE m_nTXPOS;

	BYTE m_pTxMakeBuffer[256];
	BYTE m_pTxSendBuffer[256];

public:
	int m_nfd;				/* file discriptor */

	struct sockaddr_in	m_localAddr;
	struct sockaddr_in	m_remoteAddr;

	BYTE StdTime_Buffer[8];

	Comm_GPS();

	short OutputCtc();
	short m_nTxBlockSize;
	short m_nRxBlockSize;

	short Initialize(const char *MY_ADDR, const char *GPS_ADDR);
	short ReceiveCtcData();

	USHORT ProcessData(BYTE *pRxData, USHORT nRxSize);
	USHORT SendData(BYTE *pTxData, USHORT nTxLen);
	void SendAck(BYTE cSeq);
	void SendReqTime();
	USHORT MakeMessage(BYTE cSeq, BYTE cOpCode, BYTE *pBody, USHORT nBodyLen, BYTE *pTxData);
};

/* ------------------------------------------------------------------------ */
extern Comm_GPS ComGPS;

/* ------------------------------------------------------------------------ */

#endif // _COMMGPS_H
