/* hwfunctest.c : function doing.
 *
 */

/************************************************************
*			Make for Etinsystem KVME402 Uart Test			*
************************************************************/

#include "vxWorks.h"
#include "vme.h"
#include "memLib.h"
#include "cacheLib.h"
#include "sysLib.h"
#include "string.h"
#include "intLib.h"
#include "esf.h"
#include "excLib.h"
#include "logLib.h"
#include "taskLib.h"
#include "vxLib.h"
#include "tyLib.h"
#include "arch/ppc/vxPpcLib.h"
#include "private/vmLibP.h"
#include "stdio.h"
#include "ctype.h"
#include "fioLib.h"
#include "msgQLib.h"

#include "drv/multi/ppc860Siu.h"
#include "drv/sio/ppc860Sio.h"

/* #include "kvme402.h" */
#include "hwfuncdef.h"
#include "hwfunctest.h"

/* ------------------------------------------------------------------------- */

struct fd_set readFds_pc,tmpFds_pc;
struct timeval selTimeOut_pc;
int fp_port[12];
int	LOOP_FLAG;
extern PC_DATA send_data;
extern MSG_Q_ID   rx_qid;


/* ------------------------------------------------------------------------- */

int openPort(int channel)
{
int i;
char port_name[10];

	if(channel)
	{
		i_Start(1);
		for(i=0;i < 8;i++)
		{
			sprintf(port_name,"/isio/%d",i);
			fp_port[i+4]=open(port_name,2,0);
			if ( fp_port[i+4] == ERROR )
			return 1;
		}
	}
	else
	{
		fp_port[0]=open("/ScctyCo/1",2,0);
		if ( fp_port[0] == ERROR )
			return 2;
		fp_port[1]=open("/ScctyCo/2",2,0);
		if ( fp_port[1] == ERROR )
			return 3;
		fp_port[2]=open("/ScctyCo/3",2,0);
		if ( fp_port[2] == ERROR )
			return 4;
		fp_port[3]=open("/tyCo/1",2,0);
		if ( fp_port[3] == ERROR )
			return 5;
	}			
	return 0;

}       

void	NVRam_Test() 
{
	int rc=0;
	
	rc = NVRam_Test_1();	/* integer test */
	rc |= NVRam_Test_2(); 	/* byte test */
	send_msg(0x20,(unsigned char)rc);	/* poll nack */
} 

int	NVRam_Test_1() 
{
	int count, index, er_cnt = 0;
	int result;
	int tmp;

	INT32 *sa, *ea;
	
	sa = ST_NVRAM;
	ea = EN_NVRAM;
	count = 1;

	while(sa < ea) {
	
		*sa = (INT32)sa;
		sa++;
	}
	
	sa = ST_NVRAM;
	result = 0;
	while(sa < ea) 
	{
		if((tmp = *sa) != (INT32)sa) 
			result++;		
		sa++;
	}

	return(result);

} 

int	NVRam_Test_2()
{
	int count, index, er_cnt = 0;
	unsigned char  *sa, *ea, tmp;
	int result,i;

	ea = EN_NVRAM;
	result =0;	
	count = 1;

	for(i=0; i<count; i++)
	{
		sa = ST_NVRAM;
		while(sa < ea)
		{
            *sa = 0xaa;
		
			if((tmp = (unsigned char)*sa) != (unsigned char)0xaa)
				result++;
			 
			sa++;
		}

	}
	return(result);

}

void	Diag_LED_Test() 
{
	unsigned char i=0;
	/****************************************
	*			Port Initialize				*
	****************************************/
	*PBPAR(vxImmrGet()) &= ~(0x18);			/* set port B -> RTS1 */
	*PBDIR(vxImmrGet()) |= 0x18;			/* set port B -> RTS1 */
	*PBODR(vxImmrGet()) |= 0x18;			/* set port B -> RTS1 */
	
	/****************************************
	*			LED Low Active				*
	****************************************/
	while(i<1)
	{
		taskDelay(150);
		/* printf("Diag0 LED ON!!     ¡ܡڜr\n");*/
		*PBDAT(vxImmrGet()) &= ~(0x8);			/* set port B -> RTS1 */
		taskDelay(120);
	
		/* printf("Diag1 LED ON!!     ¡ܡۜr\n");*/
		*PBDAT(vxImmrGet()) &= ~(0x10);			/* set port B -> RTS1 */	
		taskDelay(120);

		/* printf("Diag0, 1 LED OFF!! ¡ۡڜr\n"); */
		*PBDAT(vxImmrGet()) |= 0x18;			/* set port B -> RTS1 */	
		taskDelay(120);

		/* printf("Diag0, 1 LED ON!!  ¡ܡۜr\n"); */
		*PBDAT(vxImmrGet()) &= ~(0x18);			/* set port B -> RTS1 */	
		taskDelay(120);
	
		/* printf("Diag0, 1 LED OFF!! ¡ۡڜr\n"); */
		*PBDAT(vxImmrGet()) |= 0x18;			/* set port B -> RTS1 */	
		taskDelay(20);
		i++;
	}	
	
}

void	Dip_SW_Test()
{
	int	cnt;
	uchar	dip_value;
	
	dip_value = *(char *)DIP_SW_READ_ADDR;
	
    send_msg(0x50,dip_value);
}

void	SRam_Test() 
{	
	int count, index, er_cnt=0;

	count = 1;

	for(index = 1; index <=count; index++) {
		
		er_cnt = byte_tst(ST_SRAM, EN_SRAM, 1, 1);
		
	}
	send_msg(0x90,(unsigned char)er_cnt);	/* poll nack */
} 

int byte_tst(char *start, char *end, int count,int loop)
{
	int err_cnt=0;	
		
		while(start < end){
			*start = 0xab;
			if (*start != 0xab)
			{
				err_cnt++;
				return(err_cnt);
			}
			else
				start++;
		}
		return(err_cnt);
}

