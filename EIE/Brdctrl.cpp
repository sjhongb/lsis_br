
#include <stdio.h>
#include "string.h"
#include "signal.h"
#include "setjmp.h"

#include "taskLib.h"
#include "vxLib.h"
#include "wdLib.h"
#include "tickLib.h"
#include "selectLib.h"
#include "ioLib.h"

#include "projdef.h"
#include "../INCLUDE/LogTask.h"
#include "../INCLUDE/eipemu.h"
#include "commDrv.h"
#include "MainTask.h"
#include "brdctrl.h"


/*******************************************************************/
/* type define */
typedef unsigned char uchar;
typedef unsigned int uint;

//////////////////////////////////////////////////////////////////////////////
/// VME IO Board control class
//////////////////////////////////////////////////////////////////////////////

#define IO_CONF_ADRS	0xf4000070
#define IO_DATA_LOW		0xf4000080
#define IO_DATA_HIGH	0xf4000082
#define NEXT_IO_ADRS	0x00000100
#define AMCODE_WRITE 	0xfd000000	// 0xff

/*---------------------------------------------------------------------- 
  MACRO fuctions
----------------------------------------------------------------------*/
#define getConf(x) 				(*(unsigned short*)(IO_CONF_ADRS + x*NEXT_IO_ADRS))
#define getHighdata(x) 			(*(unsigned short*)(IO_DATA_HIGH + x*NEXT_IO_ADRS))
#define getLowdata(x) 			(*(unsigned short*)(IO_DATA_LOW  + x*NEXT_IO_ADRS))
#define writeHighData(id, data)	(*(unsigned short*)(IO_DATA_HIGH + NEXT_IO_ADRS * id) = (unsigned short)data)
#define writeLowData(id, data)	(*(unsigned short*)(IO_DATA_LOW  + NEXT_IO_ADRS * id) = (unsigned short)data)

/*******************************************************************/
#define OK		0
#define ERROR		(-1)
#define FOREVER	for (;;)

/*******************************************************************/
/* defined extern */
extern unsigned char *g_pSysControlByte;	// Be declared in "MainTask.cpp"
//extern unsigned char *pInterlockTable;
//extern unsigned char VMEM[];
extern BYTE g_ndipSwitch3;

extern char g_nSysNo;
extern int _Time1Sec;						// Be declared in "Eipemu.cpp"

extern SystemStatusType *EI_Status;

/*******************************************************************/
static int _OldTime1Sec = 0;
int ScanTask_GO;

unsigned short EXD_low, EXD_high;
int PortErrCnt, CardErrCount = 0;

CBoardCtrl EIS_IO;
jmp_buf location;  	/* BUS ERROR를 처리하기위해 사용됨 */

/***********************************************************************/

/***********************************************************************/
/* for IO Simulation ***************************************************/
/***********************************************************************/

void IOSIM_RecvDrv();
int SIMfd;

Comm_IOSIM::Comm_IOSIM() 
{
/*
	m_bError = 0;
	m_bIsSub = 1;
	m_nIllegalIOFailure = 0;
	//
//	memset(m_pMaxInBuffer, 0x00, MAX_MIOBUFFER_SIZE);		//
//	memset(m_pInBuffer, 0x00, MAX_IOBUFFER_SIZE);			//
	memset(m_pOutBufferSave, 0x00, MAX_MIOBUFFER_SIZE);		//
	memset(m_pOutOldBuffer, 0x00, MAX_IOBUFFER_SIZE);		// 
	memset(m_pOutBuffer, 0x00, MAX_IOBUFFER_SIZE);			// interlocking result output data buffer.
	memset(m_pOut1thFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data new buffer.
	memset(m_pOut2ndFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data old1 buffer.
	memset(m_pOut3rdFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data old2 buffer.
	memset(m_pOutSetFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data (old2 AND old1 AND new) buffer.
*/
}

Comm_IOSIM::~Comm_IOSIM() 
{
}

/***********************************************************************/

Comm_IOSIM SimCom;


short Comm_IOSIM::Initialize()
{
	m_nTxBlockSize = 0;
	m_nPortNo = PORTNUM_SIM;
	m_ComSim.Initialize(m_nPortNo, 0, 1);
	SIMfd = m_ComSim.m_nfd;
    //printf("\n SIMfd = %d", SIMfd);
	taskSpawn("IOSIM_RecvDrv",100,0,3000,(FUNCPTR)IOSIM_RecvDrv,0,0,0,0,0,0,0,0,0,0);
}



/***********************************************************************/
/***********************************************************************/

void IOSIM_RecvDrv()
{
	int     index;
	int     width=0;
	char    readbuf[256];
	STATUS  status;
	int		count;
	uchar	msg[100]; 

	MSG_Q_ID rx_qid = SimCom.m_ComSim.m_nRxQID;

	//---------------------------------------------------------
    	// select시에 timeout value를 결정해 준다.
	// ---------------------------------------------------------//
	selTimeOut_SIM.tv_sec  = SELTIMEOUT_SIM;
	selTimeOut_SIM.tv_usec = 0;

	FD_ZERO ( &readFds_SIM );

	FD_SET(SIMfd, &readFds_SIM);
	width = max(SIMfd, width);
	width ++;

	//WAIT_TASK_CREATION_FLAG_is_SET
	 while(FORever) 
	{
        tmpFds_SIM = readFds_SIM;
        status = select ( width, &tmpFds_SIM, NULL, NULL, &selTimeOut_SIM);
		if (status == ERROR)
		{
	        	//printf("select error\n");
		}
		else if (status == 0)
		{
		}
		else
		{
			memset (readbuf, 256, 0);
			count = read( SIMfd, readbuf, (size_t)150);
			if (count > 0)
			{
				for (index=0;index<count;index++){
                        //---------------------------------------------------------------//
                        // IP port 3를 통하여 수신된 data를 수신용 ring buffer로 전송한다.//
                        //----------------------------------------------------------------//
					if ( msgQSend( rx_qid, &readbuf[index], 1, NO_WAIT, MSG_PRI_NORMAL ) == ERROR )
					{
						//printf("SimIODrv --> Queueing error...\n");
						while(1)
						{
							if(msgQReceive(rx_qid,&readbuf[index],1,NO_WAIT) != ERROR)
							{
								continue;
							}
							else
							{
								goto Purge_Break;
							}
						}
						Purge_Break:
						int a=a;
					}
				}
			} // End if 		
		} 
	}
}
/***********************************************************************/
/**** End of IO Simulation *********************************************/
/***********************************************************************/


CBoardCtrl::CBoardCtrl() 
{   
	m_bError = 0;
	m_bIsSub = 1;
	m_nIllegalIOFailure = 0;
	//
//	memset(m_pMaxInBuffer, 0x00, MAX_MIOBUFFER_SIZE);		//
//	memset(m_pInBuffer, 0x00, MAX_IOBUFFER_SIZE);			//
	memset(m_pOutBufferSave, 0x00, MAX_MIOBUFFER_SIZE);		//
	memset(m_pOutOldBuffer, 0x00, MAX_IOBUFFER_SIZE);		// 
	memset(m_pOutBuffer, 0x00, MAX_IOBUFFER_SIZE);			// interlocking result output data buffer.
	memset(m_pOut1thFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data new buffer.
	memset(m_pOut2ndFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data old1 buffer.
	memset(m_pOut3rdFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data old2 buffer.
	memset(m_pOutSetFailBuffer, 0x00, MAX_IOBUFFER_SIZE);	// output failure data (old2 AND old1 AND new) buffer.
	memset(m_pFaultID, 0x00, MAX_BOARD_NUM);
}

CBoardCtrl::~CBoardCtrl() 
{
}



short int CBoardCtrl::OutputAll(  short isSub = 0 ) 
{
	if ( isSub ) 
	{
		for (int i=0; i<MAX_IOBUFFER_SIZE; i++) {
			m_pOutBuffer[i] = _pOutBuffer[i] & m_pOutOldBuffer[i];
		}
	}
	else {
		memcpy( m_pOutBuffer, _pOutBuffer, MAX_IOBUFFER_SIZE );
	}

	memcpy( m_pOutOldBuffer, _pOutBuffer, MAX_IOBUFFER_SIZE );

//printf(".....[%d, %d]\n", _pOutBuffer[44],m_pOutBuffer[44]);

	/*
	if (g_ndipSwitch3 & 0x20)
	{
		char pBuffer[300];
		memcpy( pBuffer, "STX", 3);
		memcpy( pBuffer + 3, m_pOutBuffer, 256);
		memcpy( pBuffer + 256 + 3, "END", 3 );
		SimCom.m_ComSim.WriteBlock(pBuffer, 256 + 3 + 3 ); //= m_pMaxOutBuffer;
	}
	*/
}

short int CBoardCtrl::InputAll() 
{
	//if (!(g_ndipSwitch3 & 0x20))
		memcpy( _pInBuffer, m_pInBuffer, MAX_IOBUFFER_SIZE - 64 );
	/*
	else
	{
		char bf[300];
//		int nRcv = SimCom.m_ComSim.ReadBlock( bf, MAX_IOBUFFER_SIZE + 3 , 0 );
		int nRcv = SimCom.m_ComSim.ReadBlock( bf, 56 + 3 , 0 );

		if ( nRcv > 0 ) 
		{
//			for (int ii=0; ii<56+3; ii++)
//				printf ("[%.2x]", bf[ii]);
//  	 	printf ("\n");

			unsigned short nCRC = 0;
//			printf("%d byte read\n",nRcv);//for debugging
			for ( int i=0; i<nRcv; i++ ) {
				nCRC = crc_CCITT( bf[i], nCRC );
			}
			if ( nCRC == 0 ) 
			{
//				memcpy( m_pInBuffer, bf + 1, 256 );
//				memcpy( _pInBuffer, m_pInBuffer, MAX_IOBUFFER_SIZE );
				memcpy( m_pInBuffer, bf + 1, MAX_IOBUFFER_SIZE-64 );
				memcpy( _pInBuffer, m_pInBuffer, MAX_IOBUFFER_SIZE-64 );
			}
			else
			{
				// CRC print
				printf ("[%.2x %.2x] [%.2x %.2x]", bf[57], bf[58], nCRC%256, nCRC/256); 
			}
		}
// Dumy receive...
//		SimCom.m_ComSim.ReadBlock( bf, 256 + 3 , 0 );
//		SimCom.m_ComSim.ReadBlock( bf, 56 + 3 , 0 );
	}
	*/
}



/*---------------------------------------------------------------------- 
* 1. prototype : void sigHandle(int sigNum)
*
* 2. 함수 설명 :
*         BUS ERROR발생시 원인을 제공한 함수로 되돌아가서
*         프로그램을 계속 수행하도록 한다.
----------------------------------------------------------------------*/

void sigHandle(int sigNum)
{
	longjmp(location, ERROR);
}

// 입력 오류 시험용 코드... 2003.3.18.
#if 0
BYTE	_In0Card = 0xFF;
BYTE    _CmpData = 0x00;
int 	_ddd=0;
int 	_In0Count = 0;
#endif // test code, <<E.


short int CBoardCtrl::ScanIO() 
{
	int nIOFaultNo;
	int whereFrom,temp; /*where308,*/ 
	short int i, j, id, byteloc, nRealID, nIllegalByte;
	unsigned short int nIllegalCard;
	unsigned short int nIOdata_low, nIOdata_high;
	unsigned short int nXRdata_low, nXRdata_high;
	unsigned short int nRDdata_low, nRDdata_high;
	unsigned char bMyVPOR=0, bMySYS=0, bOtherSYS=0;
	unsigned char bitmask;
	unsigned char *pInfo;
	char _cSysRInputBit2, _cSysRInputBit1, _nSysOutputOffset, _nSysInputOffset;
   	char *addr;
	char data[4];
	//
	char *pd0 = ((char*)&nIOdata_low)+1;
	char *pd1 = (char*)&nIOdata_low;
	char *pd2 = ((char*)&nIOdata_high)+1;
	char *pd3 = (char*)&nIOdata_high;
	char *pfaild0 = ((char*)&nXRdata_low)+1;
	char *pfaild1 = (char*)&nXRdata_low;
	char *pfaild2 = ((char*)&nXRdata_high)+1;
	char *pfaild3 = (char*)&nXRdata_high;

	m_bError = 0;
	nIOFaultNo = -1;

//
	/*   2010.3.22.... 
	addr = (char*)(COMM_CONF_ADRS);
	where308 = vxMemProbe(addr,VX_READ,2,data);
    if (where308 != OK)
    {
        if (_nIllegalFailure <4)
	 	    _nIllegalFailure++;
        printf("\n 308 Prove Error.....");
    }

	if (_nIllegalFailure > 3)
        return m_bError;
    */
	
// Processing for FDIM Board. 
// Read FDIM Board. 
	pInfo  = (unsigned char *)m_pBoardInfo;
	pInfo += (MAX_BOARD_NUM - 1) * 2;
	for ( i= (MAX_BOARD_NUM - 1); i >=0; i-- ) 
	{
		if ( pInfo[0] == IOTYPE_FDIM ) {	// input board checking.
			byteloc = i * 4;
			id = i;
			nRealID = m_pRealID[ id ] & 0x7f;
	 		addr = (char*)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar) nRealID);
	 	// check to a FDIM baord existing.
			whereFrom = vxMemProbe(addr,VX_READ,2,data);
//		whereFrom = OK;
			if (whereFrom != OK)
			{
				pInfo[1] = 0;	// Error
				if ( !(m_pRealID[ id ] & 0x80) ) {	// Not spare I/O board ?
//test S>> 
//nIOFaultNo = i | 0x2000;
//test <<E
					m_bError = 1;

//printf("BRDERR[IN%02d]", i );  //===========================
				}
			}
			else {
				pInfo[1] = 1;	// No error
//				conf = getConf( nRealID );
				nIOdata_low  = ~getLowdata( nRealID );		// read 16 bit low-data of a input board.
				nIOdata_high = ~getHighdata( nRealID );	// read 16 bit high-data of a input board.
				
//				if ( EI_ExtStatusType->b50VINTR  || (nRealID == 0) ) {
					m_pInBuffer[byteloc]   = *pd0;			// set buffer to read data(bit0  ~ bit7)
					m_pInBuffer[byteloc+1] = *pd1;			// set buffer to read data(bit8  ~ bit15)
					m_pInBuffer[byteloc+2] = *pd2;			// set buffer to read data(bit16 ~ bit23)
					m_pInBuffer[byteloc+3] = *pd3;			// set buffer to read data(bit24 ~ bit31)

//				}
			}
//printf("\n %d[ID:%2.2X  data_L = %4.4X,  data_H = %4.4X]", bMySYS, nRealID,  nIOdata_low,  nIOdata_high); //==========
		}
		pInfo -= 2;
	}

	//LogHex(LOG_TYPE_GEN, m_pInBuffer, MAX_IOBUFFER_SIZE, "---> m_pInBuffer[%d]", MAX_IOBUFFER_SIZE);
	//LogHex(LOG_TYPE_GEN, m_pOutBuffer, MAX_IOBUFFER_SIZE, "---> m_pOutBuffer[%d]", MAX_IOBUFFER_SIZE);
	
// Processing for FDOM Board.v 
	PortErrCnt = EXD_low = EXD_high = 0;

	_nSysInputOffset  = 4;	// active1, active2.
	_nSysOutputOffset = 0;	// physical OK, logical OK

	if ( g_nSysNo ) 
	{	// 2계
		_cSysRInputBit1 = 1;
		bOtherSYS = m_pInBuffer[_nSysInputOffset] & _cSysRInputBit1;
		bMySYS  = *g_pSysControlByte & 0x02;		// SYS IN
		bMyVPOR = *g_pSysControlByte & 0x08;		// VPOR IN
	}
	else 
	{
		_cSysRInputBit2 = 2;
		bOtherSYS = m_pInBuffer[_nSysInputOffset] & _cSysRInputBit2;
		bMySYS  = *g_pSysControlByte & 0x01;		// SYS IN
		bMyVPOR = *g_pSysControlByte & 0x04;		// VPOR IN
	}

	nIllegalCard = m_nIllegalIOFailure >> 8;	// 1 ~ 64.
	
// Write FDOM Board. 
	pInfo = (unsigned char *)m_pBoardInfo;
	for ( i = 0; i < MAX_BOARD_NUM; i++ ) 
	{
		if ( pInfo[0] == IOTYPE_FDOM )
		{
		// check IO failure board 
			byteloc = i * 4;
			id = i;
			nRealID = m_pRealID[ id ] & 0x7f;
			if (nRealID >= 0x40)
			{
	 			addr = (char *)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar)nRealID);
//				whereFrom = setjmp(location);

				whereFrom = vxMemProbe(addr,VX_READ,2,data);
//				whereFrom = OK;
//printf(" //O-%X ",addr);
				if (whereFrom != OK)
				{
					pInfo[1] = 0;	// Error
					if ( !(m_pRealID[ id ] & 0x80) ) 
					{ 	// Not spare I/O board ?
						m_bError = 1;
//test S>> 
//nIOFaultNo = i | 0x4000;
//test <<E
//printf("BRDERR[OUT%02d]", id );
					}
				}
// CARD_SYSTEM_OUTPUT 찾기
				else 
				{
/*
if (nRealID==0x41)
{
m_pOutBufferSave[byteloc] = 1;
}
*/
					pInfo[1] = 1;	// No error
// Out Board Data Compare, 
					*pd0 = m_pOutBufferSave[byteloc];
					*pd1 = m_pOutBufferSave[byteloc+1];
					*pd2 = m_pOutBufferSave[byteloc+2];
					*pd3 = m_pOutBufferSave[byteloc+3];

				// read output port.	
					nRDdata_low  = ~getLowdata( nRealID );
					nRDdata_high = ~getHighdata( nRealID );

//printf("\n %d[ID:%2.2X  byteloc = %d  data_L = %4.4X,  data_H = %4.4X]", bMySYS, nRealID,  byteloc, nIOdata_low,  nIOdata_high); //==========

					if ( EI_Status->bB24PWR && ((nRDdata_low != nIOdata_low) || (nRDdata_high != nIOdata_high)) ) 
					{
//printf("\n ...%d[ID:%2.2X  data_L = %4.4X,  data_H = %4.4X]", bMySYS, nRealID,  nIOdata_low,  nIOdata_high); //==========
//printf("\n [ID:%2.2X Rdata_L = %4.4X, Rdata_H = %4.4X]", nRealID, nRDdata_low, nRDdata_high); //==============
							
						if ( m_pFaultID[id] == 0x08)
						{
							pInfo += 2;
							m_bError = 1;
							//printf ("\n Fault FDOM = %d [%d]", m_pFaultID[id], id); //=======================
   					        continue;
						}
	
						nXRdata_low  = nRDdata_low  ^ nIOdata_low;
						nXRdata_high = nRDdata_high ^ nIOdata_high;
						EXD_low  = nXRdata_low;
						EXD_high = nXRdata_high;
						m_pOut1thFailBuffer[byteloc]   = *pfaild0;
						m_pOut1thFailBuffer[byteloc+1] = *pfaild1;
						m_pOut1thFailBuffer[byteloc+2] = *pfaild2;
						m_pOut1thFailBuffer[byteloc+3] = *pfaild3;
						m_pOutSetFailBuffer[byteloc]   = m_pOut1thFailBuffer[byteloc]   & m_pOut2ndFailBuffer[byteloc]   
																						& m_pOut3rdFailBuffer[byteloc];
						m_pOutSetFailBuffer[byteloc+1] = m_pOut1thFailBuffer[byteloc+1] & m_pOut2ndFailBuffer[byteloc+1] 
																						& m_pOut3rdFailBuffer[byteloc+1];
						m_pOutSetFailBuffer[byteloc+2] = m_pOut1thFailBuffer[byteloc+2] & m_pOut2ndFailBuffer[byteloc+2] 
																						& m_pOut3rdFailBuffer[byteloc+2];
						m_pOutSetFailBuffer[byteloc+3] = m_pOut1thFailBuffer[byteloc+3] & m_pOut2ndFailBuffer[byteloc+3] 
																						& m_pOut3rdFailBuffer[byteloc+3];
						nIllegalByte = -1;	// none error

						if ( m_pOutSetFailBuffer[byteloc] ) 
							nIllegalByte = byteloc;
						else if ( m_pOutSetFailBuffer[byteloc+1] ) 
							nIllegalByte = byteloc + 1;
						else if ( m_pOutSetFailBuffer[byteloc+2] ) 
							nIllegalByte = byteloc + 2;
						else if ( m_pOutSetFailBuffer[byteloc+3] ) 
							nIllegalByte = byteloc + 3;
						
						if ( nIllegalByte >= 0 ) 
						{
							if (m_pFaultID[id]==0x07)  //For logging time -- 0x06
							{
								m_nIllegalIOFailure  = (i + 1) << 8;					// Illegal failure board position.
								m_nIllegalIOFailure += (nIllegalByte - byteloc) * 8;	// base Illegal failure port.
								bitmask = 1;
								for(j=1; j<=8; j++) 
								{
									if (m_pOutSetFailBuffer[nIllegalByte] & bitmask) {
										m_nIllegalIOFailure += j;						// Illegal failure port adderss
										break;
									}
									bitmask <<= 1;
								}

							//m_bError = 1;
							}
							if (m_pFaultID[id] != 0x08)
							{
								//printf ("\n ......bMySYS = [%d:%d]", bMySYS,m_pFaultID[id]); //=============
								m_pFaultID[id]++;
							}

//test S>> 
//nIOFaultNo = i | 0x8000;
//test <<E
						}
						

						CardErrCount++;			
					}
					else
					{
						EXD_low = EXD_high = 0;
						CardErrCount = 0;
						m_pOut1thFailBuffer[byteloc]   = 0x00;
						m_pOut1thFailBuffer[byteloc+1] = 0x00;
						m_pOut1thFailBuffer[byteloc+2] = 0x00;
						m_pOut1thFailBuffer[byteloc+3] = 0x00;
						m_pOutSetFailBuffer[byteloc]   = 0x00;
						m_pOutSetFailBuffer[byteloc+1] = 0x00;
						m_pOutSetFailBuffer[byteloc+2] = 0x00;
						m_pOutSetFailBuffer[byteloc+3] = 0x00;
						if ( nIllegalCard == (i+1) ) {
							m_nIllegalIOFailure = 0;
						}
						m_pFaultID[id] = 0;
					}

					if (CardErrCount >3)
					{
						temp = CheckErrPort();
						PortErrCnt = PortErrCnt + temp;
						CardErrCount = 0;

//printf("[PE:%d]",PortErrCnt);
					}

//printf("[PE:%d %d %d %d %d %d]\n",bMyVPOR, bMySYS, bOtherSYS, i, _nSysOutputOffset, nRealID);
				// wirte output port.
					if( ( bMyVPOR != 0 && ( bMySYS != 0 || bOtherSYS == 0 )) || (i == _nSysOutputOffset) ) 
					{
						*pd0 = m_pOutBuffer[byteloc];
						*pd1 = m_pOutBuffer[byteloc+1];
						*pd2 = m_pOutBuffer[byteloc+2];
						*pd3 = m_pOutBuffer[byteloc+3];
					}										
					else 
					{
						*pd0 = 0;
						*pd1 = 0;
						*pd2 = 0;
						*pd3 = 0;

					}
					
/*
if (nRealID == 0x41)
{
printf("[PF:%d %d %d %d]",*pd0, *pd1, *pd2, *pd3);
}
*/
					writeLowData( nRealID, nIOdata_low );
					writeHighData( nRealID, nIOdata_high );		

					m_pOutBufferSave[byteloc]   = *pd0;
					m_pOutBufferSave[byteloc+1] = *pd1;
					m_pOutBufferSave[byteloc+2] = *pd2;
					m_pOutBufferSave[byteloc+3] = *pd3;
				}
			}
		}
		pInfo += 2;
	}
// store  output board failure buffer.	
	memcpy(m_pOut3rdFailBuffer, m_pOut2ndFailBuffer, MAX_IOBUFFER_SIZE);
	memcpy(m_pOut2ndFailBuffer, m_pOut1thFailBuffer, MAX_IOBUFFER_SIZE);


// Existence error I/O board.
	// 에러가 발생한 경우 계절체 시간 단축을 위해 자기계의 SYSR 을 오프한다.
	if ( m_bError ) 
	{
// printf("m_bError = %d\n", m_bError);
		id = _nSysOutputOffset;		// ID number of 1th FDOM Board.
		nRealID = m_pRealID[ id ] & 0x7f;
		pInfo = (unsigned char *)&m_pBoardInfo[id];
		if ( pInfo[1] ) {	// if sys-out board OK
	 		addr = (char*)(IO_DATA_HIGH + NEXT_IO_ADRS * (uchar)nRealID);

		whereFrom = vxMemProbe(addr,VX_READ,2,data);
//		whereFrom = OK;

			if (whereFrom != OK)
			{
				pInfo[1] = 0;	// Error
				m_bError = 1;
			}
			else 
			{
				nIOdata_low  = 0; // 2012.10.22 fix for system FDOM // (m_pOutBufferSave[0] & 0x0fb);	// physical OK bit clearing.
				nIOdata_high = 0;
//
#ifndef   IGNORE_FAULT
				writeLowData( nRealID, nIOdata_low );
				writeHighData( nRealID, nIOdata_high );
#endif // IGNORE_FAULT
//test S>> 
//if ( nIOFaultNo & 0x8000 ) 
//	printf("\n>>IOERR[%d]", nIOFaultNo & 0x0fff); //======================
//else if ( nIOFaultNo & 0x4000 ) 
//	printf("\n>>OCERR[%d]", nIOFaultNo & 0x0fff);
//else if ( nIOFaultNo & 0x2000 ) 
//	printf("\n>>ICERR[%d]", nIOFaultNo & 0x0fff);
//else
//	printf("\n>>NCERR[%d]", nIOFaultNo & 0x0fff);
//test <<E

			}
		}
	}

	_OldTime1Sec = _Time1Sec;
	
	return m_bError;
}

/* ------------------------------------------------------------------------- */

int	 g_nIOScanRunTimeTicked = 0;	/* time tick for io_scan run function */

void rSetIOScanRunTimeTick() 
{
	g_nIOScanRunTimeTicked = 1;
}

void rClearIOScanRunTimeTick() 
{
	g_nIOScanRunTimeTicked = 0;
}

void WaitOnSyncForIOScanRun() 
{
	int n;
	
	for(n=0; n<MAX_WAITTICK_IOSCAN; n++) {
		if ( g_nIOScanRunTimeTicked ) break;
		taskDelay(1);
	}
}


/* ------------------------------------------------------------------------- */
/*
void tScan()
{
//	ULONG ulStart, ulEnd, ulGap;

	char  *fn = "tScan";

#ifndef _NOT_AMCODE
//	(*(unsigned char*)AMCODE_WRITE) = 0xff;
printf("\nAMCODE");
#endif // _NOT_AMCODE
	FOREVER
	{
//		ulStart = tickSet(0);

		EIS_IO.ScanIO();
		taskDelay(1);

//		ulEnd = tickGet();
//		ulGap = ulEnd - ulStart;

	} // FOREVER //
}
*/

void tScan()
{
//	ULONG ulStart, ulEnd, ulGap;

	char  *fn = "tScan";

    WDOG_ID wid_IOScanRun;

    wid_IOScanRun = wdCreate();

#ifndef _NOT_AMCODE
//	(*(unsigned char*)AMCODE_WRITE) = 0xff;
	printf("\nAMCODE");
#endif // _NOT_AMCODE

	FOREVER
	{
//		ulStart = tickSet(0);

		rClearIOScanRunTimeTick();
	    wdStart (wid_IOScanRun, MAX_WAITTICK_IOSCAN, (FUNCPTR)rSetIOScanRunTimeTick, 0);	//600 => 1 sec

		EIS_IO.ScanIO();
		taskDelay(1);

		WaitOnSyncForIOScanRun();
		
//		ulEnd = tickGet();
//		ulGap = ulEnd - ulStart;

	} /* FOREVER */

	wdCancel( wid_IOScanRun );

}

/* ------------------------------------------------------------------------- */

int CBoardCtrl::MakeBoardImage(/*int bSysBRD*/)
{
	int result, bErrEvg, Port;
	result = bErrEvg = 0;

	unsigned char *pInfo = (unsigned char *)m_pBoardInfo;
	for ( short i = 0; i < MAX_BOARD_NUM; i++ ) 
	{
		int byteloc = i / 8;
		unsigned char bitloc = 1 << ( i % 8 );

// 없는 카드는 0 으로 처리
		if ( pInfo[0] == 0 ) 
		{
			m_pBoardImage[ byteloc ] &= ~bitloc;
		}
		else if ( pInfo[1] & 1 ) 
		{
			m_pBoardImage[ byteloc ] |= bitloc;
		}
		else 
		{

			m_pBoardImage[ byteloc ] &= ~bitloc;
// Physical
			bErrEvg++;
			result = bErrEvg;
/*
			if( i == 0){	// FDOM #0 = RealID = 40
// Test
printf("\n<FDOM#0-> Err>");
				bSysBRD = 1;
			}
*/
// Physical END
printf("\n Board error [%d]", i);
		}
		pInfo += 2;
	}
	result = (result * 32) + PortErrCnt;	// 1Card = 32Port
	return result;
}

void CBoardCtrl::SetBoardInfo(unsigned char *pInfoTable, unsigned char *pRealID)
{
	//unsigned char *pInfoTable = pInterlockTable + 0x2A;	// offset of IO info
	//unsigned char *pRealID = pInterlockTable + 0x3A;
	int i;

	for ( i = 0; i < 64; i++ ) 
	{
		m_pRealID[i] = pRealID[i];
	}

	int bytes = (MAX_BOARD_NUM + 3) / 4;

	short nCardID = 0;
	for (i=0; i<bytes; i++) 
	{
		if (nCardID >= MAX_BOARD_NUM) break;
		unsigned char bInfo = *pInfoTable++;
		for (int b=0; b<4; b++) 
		{
			unsigned short t = (unsigned short)bInfo & 3;
			m_pBoardInfo[ nCardID++ ] = t * 256 + 1;	// for MOTOROLA
			bInfo >>= 2;
		}
	}
	memset( m_pOutBuffer, 0, MAX_IOBUFFER_SIZE );
	memset( m_pOutOldBuffer, 0, MAX_IOBUFFER_SIZE );
}

short int CBoardCtrl::CheckErrPort()
{
	int nResult = 0;

	long d = EXD_low + ( EXD_high << 16 );

	for( int i=0; i<32; i++){
		if ( d & 1 ) {
			nResult++;
		}
		d >>= 1;
	}
	return nResult;
}
