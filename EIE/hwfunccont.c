/* hwfunccont.c : console.
 *
 */

#include "stdio.h"
#include "tyLib.h"
#include "vxWorks.h"
#include "socket.h"
#include "inetLib.h"
#include "fioLib.h"
#include "hwfunctest.h"

/* ------------------------------------------------------------------------- */
int comm_port;
int		newFd;
int		sFd;
 
/* ------------------------------------------------------------------------- */
int 	sockAddrSize,sockAddrSize_Client;
struct sockaddr_in Server_Addr;
struct sockaddr_in Client_Addr;
 
/*----------------------------------------------------------------------
*     tyOctSerDrv();
*     A : tyOctSerPack(0xfff5800, 201, 1,2,1);
*     B : tyOctSerPack(0xfff5810, 202, 3,4,1);
*     sprintf (tyName, "%s%d", "/tyCo/", i);
*     tyOctSerDevCreate( tyName, i-IPSTART, 8192, 8192);
*
----------------------------------------------------------------------*/


/* ------------------------------------------------------------------------- */
/* 포트를 열어준다.                                                          */
/* ------------------------------------------------------------------------- */
int port_Open()
{
	int 	status;
	char    name[20];
	int 	modepair;

	sprintf(name,"/ScctyCo/2");
   	comm_port = open(name,O_RDWR, 0);	
	ioctl(comm_port, FIOBAUDRATE , 9600);

	return 1;
}

int send_Data(int portNum,uchar *writeBuf,int sendCounter)
{
	int status;
	int datalen;
	int loop;
	
	printf("[SND] ");
	status = write(newFd,writeBuf,sendCounter);

	for(loop = 0 ; loop < sendCounter ; loop++)
	{
		/* status = write(newFd,(writeBuf+loop),1); */
		printf("%x ",*(writeBuf + loop));
	}

	printf("\n");
	
	return status;
}

 
int port_Init(int start)
{
	int loop;
	int err_con;
	char reuse_addr;

	while(1)
	{
		
		if(!start)
		{	/*setup local address*/

			sockAddrSize = sizeof (struct sockaddr_in);		
			bzero((char *)&Server_Addr,(int)sockAddrSize);
		
			Server_Addr.sin_family 		= AF_INET;
			Server_Addr.sin_len 		= (uchar)sockAddrSize;
			Server_Addr.sin_port 		= htons(SERVER_PORT_NUM1);
			Server_Addr.sin_addr.s_addr	= htonl(INADDR_ANY);
/*
	Server_Addr.sin_addr 	=   *(struct in_addr *)hostentstruct.h_addr;
*/


 
/*
		//socket 생성
*/
			if((sFd = socket(AF_INET,SOCK_STREAM,0)) == ERROR)
			{
				taskDelay(sysClkRateGet()*1);
				continue;
			}
/*
 			//바인딩
*/
			taskDelay(1);
/*
 			//	reuse_addr = 1;
  	   		//	setsockopt(sFd, SOL_SOCKET, SO_REUSEADDR,(char *)&reuse_addr, sizeof(reuse_addr) );
*/

			if(bind(sFd,(struct sockaddr *)&Server_Addr,sockAddrSize) == ERROR)
			{
 				close(sFd);	
				continue;
			}
  
		
	 
/*
		//client의 request를 위한 queue생성
*/
			if(listen(sFd,SERVER_MAX_CONNECTIONS) == ERROR)
/*
			//if(listen(sFd,1) == ERROR)
*/
			{
				close(sFd);
            			continue;
			}
		}
		if((newFd=accept(sFd,(struct sockaddr *)&Client_Addr,&sockAddrSize)) == ERROR)
		{
             		if (err_con == ERROR)
          		{
					taskDelay(10);
					close(sFd);
            				continue;
			}
		}
		return(1);
	}
	return 0;
}
