/* taskWatch.h
 *
 */

#ifndef _TASKWATCH_H
#define _TASKWATCH_H
 
/* ------------------------------------------------------------------------ */
/*
#define TASKID_DOWNMODEM1	0
#define TASKID_DOWNMODEM2	1
#define TASKID_UPMODEM1		2
#define TASKID_UPMODEM2		3
#define MAX_WATCH_TASKS		4
*/

#define TASKID_LEFT_BLOCK	0
#define TASKID_RIGHT_BLOCK	1
#define TASKID_MID_BLOCK	2
#define TASKID_SEND_BLOCK	3
#define TASKID_LEFT_BKUP	4
#define TASKID_RIGHT_BKUP	5
#define TASKID_MID_BKUP		6
#define MAX_WATCH_TASKS		7

/* ------------------------------------------------------------------------ */
extern int g_nTaskIDFor308[MAX_WATCH_TASKS];
extern void taskWatchInitialize();

/* ------------------------------------------------------------------------ */

#endif // _TASKWATCH_H
