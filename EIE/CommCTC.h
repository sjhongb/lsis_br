/* -------------------------------------------------------------------------+
|  CommCTC.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CommCTC.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */ 
#ifndef _COMMCTC_H
#define _COMMCTC_H

/* ------------------------------------------------------------------------ */
#include "inetLib.h"

#include "projdef.h"
#include "crc.h"

/* ------------------------------------------------------------------------ */
class Comm_CTC 
{
#if 0
	BYTE m_nDTSRXSTEP;
	union {
		BYTE flags;
		struct {
			BYTE bDoAnswer		: 1;
			BYTE bDoNegative	: 1;
		} bits;
	};
	union {
		BYTE DTSRXBUFFER[16];
		struct {				// for RX
            BYTE    DLE;
            BYTE    STX;
			BYTE	RXSEQ;
			BYTE	MSGCOUNT;
			BYTE	RXMSG;
		} rx;
	};
#endif

	Ccrc 	m_CRC;

	BYTE m_cTxSeqNo;
	BYTE m_cRxSeqNo;
	BYTE m_nTXPOS;
//	BYTE m_nRXPOS;

	BYTE m_nPacketID;
	
	BYTE m_pTxMakeBuffer[256];
	BYTE m_pTxSendBuffer[256];

	BYTE m_cZoneNo;
	BYTE m_cStationNo;

//	BYTE m_rx_byte;

public:
	int m_nfdPRI;				/* file discriptor */
	int m_nfdSEC;				/* file discriptor */

	struct sockaddr_in	m_localAddrPRI;
	struct sockaddr_in	m_localAddrSEC;
	struct sockaddr_in	m_remoteAddrPRI;
	struct sockaddr_in	m_remoteAddrSEC;

	BYTE CpuCommandQue[256];
	BYTE StdTime_Buffer[8];
	BYTE DIM_Buffer[2048];			// cts data table.

	Comm_CTC();

	short OutputCtc();
	short m_nTxBlockSize;
	short m_nRxBlockSize;

	short Initialize(BYTE cZoneNo, BYTE cStationNo, const char *MY_ADDR_PRI, const char *MY_ADDR_SEC, const char *CTC_ADDR_PRI, const char *CTC_ADDR_SEC);
	short ReceiveCtcData();

	void DtsAnswer();
	void NegativeAcknowledge();
	void AnswerHeader();
	void Acknowledge();
	void AnswerTail();
    short msg_read(int, char*);
	void DTSControl();
	void MasterClock();

	USHORT ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxSize);
	USHORT SendData(BYTE ch, BYTE *pTxData, USHORT nTxLen);
	void SendAck(BYTE ch, BYTE cSeq);
	void SendNack(BYTE ch, BYTE cSeq);
	USHORT UpdateSiteInfo(BYTE *pTxBuffer);
	USHORT MakeMessage(BYTE cSeq, BYTE cOpCode, BYTE *pBody, USHORT nBodyLen, BYTE *pTxData);
};

/* ------------------------------------------------------------------------ */
extern Comm_CTC ComCTC;

/* ------------------------------------------------------------------------ */

#endif // _COMMCTC_H
