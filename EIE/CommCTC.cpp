/* -------------------------------------------------------------------------+
|  CommCtc.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                               
|                                                                           
|  2. FileName.                                                             
|     CommCtc.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     Communication Block for CTC.
|                                                                           
|  4. Project Name.                                                         
|     BR Tongi/Laksam/13Stations PJT.                                       
|                                                                           
|  5. Maker.                                                                
|	  LS Industrial System.
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Targe3 Board	- LS-402A                               
|      - Target O.S		- VxWorks 5.4                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */

// Include system header files.
#include "stdio.h"
#include "string.h"
#include "taskLib.h"
#include "selectLib.h"
#include "sockLIb.h"
#include "ioLib.h"
#include "wdLib.h"

// Include user header files.
#include "../INCLUDE/scrinfo.h"
#include "../INCLUDE/LogTask.h"

#include "crc.h"
#include "commctc.h"

/* ------------------------------------------------------------------------ */
#define CBI_PORT		9500
#define CTC_PORT_BASE	8500

#define CTC_PRI			1
#define CTC_SEC			2

#define DLE             0x10
#define STX				0x02
#define ETX				0x03
#define OP_AYT			0xAC
#define OP_ACK			0xA0
#define OP_NACK			0xA7
#define OP_TRAIN_NO		0x81
#define OP_SITE_INFO	0x82
#define OP_STD_TIME		0x91
#define OP_SITE_CTRL	0x92

#define ENABLE_DTS_MSG

#define MAX_BUFFER_SIZE	2048

/* ------------------------------------------------------------------------ */
extern char g_nSysNo;
extern SystemStatusType	*EI_Status;


/* ------------------------------------------------------------------------ */
WDOG_ID	wid_PeerCheckTimerPRI;
WDOG_ID wid_PeerCheckTimerSEC;
WDOG_ID	wid_SendSiteInfoTimer;

int ISR_PeerCheckTimeoutPRI();
int ISR_PeerCheckTimeoutSEC();
int ISR_SendSiteInfoTimeout();

BYTE	g_bPeerCheckTriggerPRI = FALSE;
BYTE	g_bPeerCheckTriggerSEC = FALSE;
BYTE	g_bSendSiteInfoTrigger = FALSE;

Comm_CTC 	ComCTC;
Ccrc 		CRC;

/* ------------------------------------------------------------------------ */
//;=====================================================================
//; CTC Serial Interface
//;=====================================================================
Comm_CTC::Comm_CTC() {
	memset( CpuCommandQue, 0, sizeof(CpuCommandQue));
	memset( StdTime_Buffer, 0, sizeof(StdTime_Buffer));
	memset( DIM_Buffer, 0, sizeof(DIM_Buffer));

	m_cTxSeqNo = 0;
	m_cRxSeqNo = 0;
}

USHORT Comm_CTC::UpdateSiteInfo(BYTE *pTxBuffer) 
{
	BYTE cSeq;
	USHORT nSize = *(unsigned short*)&DIM_Buffer[0];	//Element status array buffer
 	BYTE Send_Buffer[2048];

	memset(Send_Buffer, 0, sizeof(Send_Buffer));	

	Send_Buffer[0] = (BYTE)(nSize & 0xFF);
	Send_Buffer[1] = (BYTE)((nSize >> 8) & 0xFF);

	memcpy(Send_Buffer + 2, DIM_Buffer + 2, nSize);

	cSeq = m_cTxSeqNo++;
	if(m_cTxSeqNo == 0)
	{
		m_cTxSeqNo++;
	}
	
	return MakeMessage(cSeq, OP_SITE_INFO, Send_Buffer, nSize + 2, pTxBuffer);
}

USHORT Comm_CTC::MakeMessage(BYTE cSeq, BYTE cOpCode, BYTE *pBody, USHORT nBodyLen, BYTE *pTxData)
{
	USHORT 	nPos = 0;
	BYTE 	low_crc=0, high_crc=0;

	if(pTxData == NULL)
	{
		return 0;
	}

	pTxData[nPos++] = DLE;
	pTxData[nPos++] = STX;
	pTxData[nPos++] = (nBodyLen + 3) & 0xFF;		// STATION_NO(1) + SEQ(1) + OPCODE(1) + DATA(nBodyLen)
	pTxData[nPos++] = ((nBodyLen + 3) >> 8) & 0xFF;	// STATION_NO(1) + SEQ(1) + OPCODE(1) + DATA(nBodyLen)
	pTxData[nPos++] = m_cStationNo;
	pTxData[nPos++] = cSeq;
	pTxData[nPos++] = cOpCode; 

	if(nBodyLen > 0 && pBody != NULL)
	{
		memcpy(&pTxData[nPos], pBody, nBodyLen);
		nPos += nBodyLen;
	}

    m_CRC.CrcGen(nPos, pTxData, &low_crc, &high_crc);

	pTxData[ nPos++ ] = low_crc;		// crc low byte
	pTxData[ nPos++ ] = high_crc;		// crc high byte
	pTxData[ nPos++ ] = DLE;
	pTxData[ nPos++ ] = ETX;

	return nPos;
}


USHORT Comm_CTC::ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxSize) 
{
    BYTE low_crc=0, high_crc=0, recv_low_crc, recv_high_crc;
	BYTE 	cStnNo, cSeqNo, cOpCode;

	if(nRxSize > 0) 
    {
		LogHex(LOG_TYPE_CTC, pRxData, nRxSize, "Recv [%d] <--- CTC", nRxSize);

        m_CRC.CrcGen(nRxSize-4, pRxData, &low_crc, &high_crc);

		// Message trailer check
        recv_low_crc = pRxData[nRxSize-4];
        recv_high_crc = pRxData[nRxSize-3]; 

		LogDbg(LOG_TYPE_CTC, "recv_low[0x%X] : calc_low[0x%x], recv_high[0x%X] : calc_high[0x%X]",
				recv_low_crc, low_crc, recv_high_crc, high_crc);	
		
		if((recv_low_crc != low_crc) || (recv_high_crc != high_crc))
		{
			LogErr(LOG_TYPE_CTC, "Recv Data CRC Check Error.");

			cSeqNo 		= pRxData[5];

			SendNack(ch, cSeqNo);
#if 0
			if(ch == TYPE_SC1 || ch == TYPE_SC2)
			{
				taskDelay(RX_TIMEOUT);

				if((pRxData[4] == pRxPeer[4]) && (*pRxPeerSts == 1))
				{
					LogInd(GetLogType(ch), "%s Recv Invalid Data(Seq = 0x%X). But Already Proceeded.", GetChannelName(ch), pRxData[4]);

					return 1;
				}
			}
#endif
			return 0;
		}

		if((DLE != pRxData[0]) || (STX != pRxData[1]))
		{
			LogErr(LOG_TYPE_CTC, "Invalid DLE/STX Value.");
			return 0;
		}

		if((DLE != pRxData[nRxSize-2]) || (ETX != pRxData[nRxSize-1]))
		{
			LogErr(LOG_TYPE_CTC, "Invalid DLE/ETX Value.");
			return 0;
		}

		cStnNo 		= pRxData[4];
		cSeqNo 		= pRxData[5];
		cOpCode 	= pRxData[6];

		if(m_cStationNo != cStnNo) 	
		{
			LogErr(LOG_TYPE_CTC, "Invalid Station Number. Expected = %d, Received = %d", m_cStationNo, cStnNo);
			return 0;
		}

		if(cOpCode == OP_ACK)
		{
			LogDbg(LOG_TYPE_CTC, "ACK Message Received. cSeqNo = 0x%X", cSeqNo);
			return nRxSize;
		}

		if((cSeqNo != 0x00) && (m_cRxSeqNo != 0xFF) && (m_cRxSeqNo > cSeqNo))
		{
			LogErr(LOG_TYPE_CTC, "Sequence Number Is Too Small. LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", m_cRxSeqNo, cSeqNo);
			return 0;
		}

		switch(cOpCode)
		{
		case OP_AYT:
			LogInd(LOG_TYPE_CTC, "AYT Received. LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", m_cRxSeqNo, cSeqNo);
			m_cRxSeqNo = cSeqNo;
			SendAck(ch, m_cRxSeqNo);
			break;

		case OP_STD_TIME:
			LogInd(LOG_TYPE_CTC, "STD Time Setting. [%02X %02X %02X %02X %02X %02X] LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", 
								pRxData[7], pRxData[8], pRxData[9], pRxData[10], pRxData[11], pRxData[12], m_cRxSeqNo, cSeqNo);
			m_cRxSeqNo = cSeqNo;
			memcpy(StdTime_Buffer, &pRxData[7], 6);
			SendAck(ch, m_cRxSeqNo);
			break;

		case OP_SITE_CTRL:
			LogInd(LOG_TYPE_CTC, "Site Control. [%02X %02X %02X] LastSeqNo = 0x%X, RcvdSeqNo = 0x%X", 
								pRxData[7], pRxData[8], pRxData[9], m_cRxSeqNo, cSeqNo);
			m_cRxSeqNo = cSeqNo;
			memcpy(CpuCommandQue, &pRxData[7], 3);		
			SendAck(ch, m_cRxSeqNo);
			break;

		default:
			break;
		}

		return nRxSize;
	}
#if 0
	else if(ch == TYPE_SC1 || ch == TYPE_SC2)
	{
		// 내 버퍼를 초기화 하고,
		memset(pRxData, 0, MAX_CONSOLE_MSG_LEN);

		// 반대측 채널에도 데이터가 없는지 확인해 본다. 
		if(memcmp(pRxData, pRxPeer, MAX_CONSOLE_MSG_LEN) != 0)
		{
			return 1;
		}

		taskDelay(RX_TIMEOUT);

		// 200ms 이후에 다시 한번 확인한다.
		if(memcmp(pRxData, pRxPeer, MAX_CONSOLE_MSG_LEN) != 0)
		{
			return 1;
		}
	}
#endif

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// COM I/O 
/////////////////////////////////////////////////////////////////////////////////

void Comm_CTC::SendAck(BYTE ch, BYTE cSeq) 
{    
	BYTE	pTxData[MAX_BUFFER_SIZE] = {0,};
	USHORT	nTxLen = 0;

	nTxLen = MakeMessage(cSeq, OP_ACK, NULL, 0, pTxData);

	LogDbg(LOG_TYPE_CTC, "nTxLen = %d, pTxData[0] = %X", nTxLen, pTxData[0]);

	SendData(ch, pTxData, nTxLen);
}

void Comm_CTC::SendNack(BYTE ch, BYTE cSeq) 
{
	BYTE	pTxData[MAX_BUFFER_SIZE] = {0,};
	USHORT	nTxLen = 0;

	nTxLen = MakeMessage(cSeq, OP_NACK, NULL, 0, pTxData);

	LogDbg(LOG_TYPE_CTC, "nTxLen = %d, pTxData[0] = %X", nTxLen, pTxData[0]);

	SendData(ch, pTxData, nTxLen);
}

USHORT Comm_CTC::SendData(BYTE ch, BYTE *pTxData, USHORT nTxLen)
{
	USHORT nSentLen;

	if(ch & CTC_PRI)
	{
		LogDbg(LOG_TYPE_CTC, "fd[%d], Send to %d.%d.%d.%d:%d", 
						m_nfdPRI,
						(m_remoteAddrPRI.sin_addr.s_addr >> 24) & 0xFF,
						(m_remoteAddrPRI.sin_addr.s_addr >> 16) & 0xFF,
						(m_remoteAddrPRI.sin_addr.s_addr >> 8) & 0xFF,
						(m_remoteAddrPRI.sin_addr.s_addr) & 0xFF,
						m_remoteAddrPRI.sin_port);

		nSentLen = sendto( m_nfdPRI, (char*)pTxData, nTxLen, 0,  
							(struct sockaddr *)&m_remoteAddrPRI, (int)(sizeof(m_remoteAddrPRI)) );
	}

	if(ch & CTC_SEC)
	{
		LogDbg(LOG_TYPE_CTC, "fd[%d], Send to %d.%d.%d.%d:%d", 
						m_nfdSEC,
						(m_remoteAddrSEC.sin_addr.s_addr >> 24) & 0xFF,
						(m_remoteAddrSEC.sin_addr.s_addr >> 16) & 0xFF,
						(m_remoteAddrSEC.sin_addr.s_addr >> 8) & 0xFF,
						(m_remoteAddrSEC.sin_addr.s_addr) & 0xFF,
						m_remoteAddrSEC.sin_port);

		nSentLen = sendto( m_nfdSEC, (char*)pTxData, nTxLen, 0,
							(struct sockaddr *)&m_remoteAddrSEC, (int)(sizeof(m_remoteAddrSEC)) );
	}

	LogHex(LOG_TYPE_CTC, pTxData, nSentLen, "Sent [%d] ---> CTC #1,#2", (short)nSentLen);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Watchdog Timer Callback
//////////////////////////////////////////////////////////////////////////////

int ISR_SendSiteInfoTimeout()
{	
	//LogInd(LOG_TYPE_CTC, "Update & Send Site Info.");
	g_bSendSiteInfoTrigger = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Task Function
//////////////////////////////////////////////////////////////////////////////

PROCESS ComCTC_SendDrv()
{
	USHORT nTxLen = 0;
	BYTE pTxBuffer[MAX_BUFFER_SIZE] = {0,};
 
	// Set Watchdog Timer for Site Info
	g_bSendSiteInfoTrigger = FALSE;
	wdStart(wid_SendSiteInfoTimer, 300, (FUNCPTR)ISR_SendSiteInfoTimeout, 0);

	while(FORever)
	{
		if(g_bSendSiteInfoTrigger)	
		{
			// Station Total Information Send.
			nTxLen = ComCTC.UpdateSiteInfo(pTxBuffer);

			LogDbg(LOG_TYPE_CTC, "nTxLen = %d", nTxLen);

			if((!g_nSysNo && EI_Status->bIActiveOn1)
			|| (g_nSysNo && EI_Status->bIActiveOn2))
			{
				ComCTC.SendData(CTC_PRI|CTC_SEC, pTxBuffer, nTxLen);
			}

			// Reset Watchdog Timer for Site Info
			g_bSendSiteInfoTrigger = FALSE;
			wdStart(wid_SendSiteInfoTimer, 300, (FUNCPTR)ISR_SendSiteInfoTimeout, 0);
		}
		else
		{
			taskDelay(10);
		}
	}
}

PROCESS ComCTC_RecvDrv(BYTE ch)
{
    int     width;
    STATUS  status;
	int 	nfd = -1;

	struct timeval selTimeOut;
	struct fd_set  readFds, tmpFds;

	struct sockaddr_in remoteAddr;
	int remoteAddrSize;

	USHORT	nRcvdSize;
    BYTE	result;
    BYTE    readbuf[MAX_BUFFER_SIZE];

	width = 0;

	switch(ch)
	{
	case CTC_PRI:
		nfd = ComCTC.m_nfdPRI;
		break;
	case CTC_SEC:
		nfd = ComCTC.m_nfdSEC;
		break;
	default:
		break;
	}

    selTimeOut.tv_sec  = 2;
    selTimeOut.tv_usec = 0;

    FD_ZERO(&readFds);

    FD_SET(nfd, &readFds);
    width = max(nfd, width);
	width++;

    while(1) 
	{
        tmpFds = readFds;

        status = select(width, &tmpFds, NULL, NULL, &selTimeOut);
		if(status == ERROR) 
		{
			LogErr(LOG_TYPE_CTC, "[%d] Select Error.", ch);

			taskDelay(600);
			continue;
		}
		else if(status == 0) 
		{
			LogErr(LOG_TYPE_CTC, "[%d] Select TIMEOUT!!!!!", ch);

			// 추후 CTC 통신 이상에 대한 코드를 추가한다. (3회 연속 실패 시.. 등등)
		}
		else 
		{
	      	if(FD_ISSET(nfd, &readFds)) 
			{
				nRcvdSize = recvfrom(nfd, (char*)readbuf, (size_t)MAX_BUFFER_SIZE, 0, 
									(struct sockaddr *)&remoteAddr, (int*)&remoteAddrSize);

				if((!g_nSysNo && EI_Status->bIActiveOn1)
				|| (g_nSysNo && EI_Status->bIActiveOn2))
				{
					result = ComCTC.ProcessData(ch, readbuf, nRcvdSize);
					if(result) 
					{
						LogInd(LOG_TYPE_CTC, "[%d] Receive Success.!!!!!!!!", ch);
						continue;
					}
				}
          	}
          	else 
			{
				LogErr(LOG_TYPE_CTC, "[%d] Cannot Read Data.", ch);
          	}
		}
#if 0
		// 반대쪽 채널의 데이터를 체크하기 위한 Watchdog Timer 시작
		if(ch == CTC_PRI)
		{
			wdStart(wid_PeerCheckTimerPRI, 200, (FUNCPTR)ISR_PeerCheckTimeoutPRI, 0);
		}
		else
		{
			wdStart(wid_PeerCheckTimerSEC, 200, (FUNCPTR)ISR_PeerCheckTimeoutSEC, 0);
		}
#endif
	}
}

/////////////////////////////////////////////////////////////////////////////////
// CTC Initalize
////////////////////////////////////////////////////////////////////////////////

short Comm_CTC::Initialize(BYTE cZoneNo, BYTE cStationNo, const char *MY_ADDR_PRI, const char *MY_ADDR_SEC, const char *CTC_ADDR_PRI, const char *CTC_ADDR_SEC)
{
	m_cZoneNo		= cZoneNo;
	m_cStationNo	= cStationNo;
	m_nTxBlockSize 	= 0;

	Log("m_cZoneNo = %d, m_cStationNo = %d\n", m_cZoneNo, m_cStationNo);

    m_CRC.GenCrcTable();    

	// Watchdog Timer Create
	wid_PeerCheckTimerPRI = wdCreate();
	wid_PeerCheckTimerSEC = wdCreate();
	wid_SendSiteInfoTimer = wdCreate();

	bzero((char*)&m_localAddrPRI, sizeof(m_localAddrPRI));
	bzero((char*)&m_localAddrSEC, sizeof(m_localAddrSEC));
	bzero((char*)&m_remoteAddrPRI, sizeof(m_remoteAddrPRI));
	bzero((char*)&m_remoteAddrSEC, sizeof(m_remoteAddrSEC));

	m_localAddrPRI.sin_family		= AF_INET;
	m_localAddrPRI.sin_port			= htons(CBI_PORT);
	m_localAddrPRI.sin_addr.s_addr	= inet_addr(MY_ADDR_PRI); 

	m_localAddrSEC.sin_family		= AF_INET;
	m_localAddrSEC.sin_port			= htons(CBI_PORT);
	m_localAddrSEC.sin_addr.s_addr	= inet_addr(MY_ADDR_SEC); 

	m_remoteAddrPRI.sin_family		= AF_INET;
	m_remoteAddrPRI.sin_port		= htons(CTC_PORT_BASE + m_cZoneNo);
	m_remoteAddrPRI.sin_addr.s_addr	= inet_addr(CTC_ADDR_PRI);  

	m_remoteAddrSEC.sin_family		= AF_INET;
	m_remoteAddrSEC.sin_port		= htons(CTC_PORT_BASE + m_cZoneNo);
	m_remoteAddrSEC.sin_addr.s_addr	= inet_addr(CTC_ADDR_SEC);  

	if((m_nfdPRI = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_CTC, "PRIMARY Socket Create Error!!");
		return (ERROR);
	}

	if(bind(m_nfdPRI, (struct sockaddr *)&m_localAddrPRI, (int)(sizeof(m_localAddrPRI))) == ERROR)
	{
		LogErr(LOG_TYPE_CTC, "PRIMARY Socket Bind Error!!");
		return (ERROR);
	}

	if((m_nfdSEC = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_CTC, "SECONDARY Socket Create Error!!");
		return (ERROR);
	}
	
	if(bind(m_nfdSEC, (struct sockaddr *)&m_localAddrSEC, (int)(sizeof(m_localAddrSEC))) == ERROR)
	{
		LogErr(LOG_TYPE_CTC, "SECONDARY Socket Bind Error!!");
		return (ERROR);
	}

    taskSpawn("ComCTC_RecvDrv1",TASKPR_IF,0,0x5000,(FUNCPTR)ComCTC_RecvDrv,CTC_PRI,0,0,0,0,0,0,0,0,0);
    taskSpawn("ComCTC_RecvDrv2",TASKPR_IF,0,0x5000,(FUNCPTR)ComCTC_RecvDrv,CTC_SEC,0,0,0,0,0,0,0,0,0);
    taskSpawn("ComCTC_SendDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComCTC_SendDrv,0,0,0,0,0,0,0,0,0,0);
}
