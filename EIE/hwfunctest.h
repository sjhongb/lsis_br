/* hwfunctest.h : header file.
 *
 */
 
#ifndef  _HWFUNCTEST_H_
#define  _HWFUNCTEST_H_

/* ------------------------------------------------------------------------- */

#define uchar 	unsigned char
#define uint 	unsigned int
#define ushort 	unsigned short
#define SERVER_MAX_CONNECTIONS 5
#define SERVER_PORT_NUM1 (5001)

/*
#define dip_add 0xF9000000
*/

/* ------------------------------------------------------------------------- */
typedef struct      
{
        ushort         : 8;   
		ushort		i8 : 1;
		ushort		i7 : 1;
		ushort		i6 : 1;
		ushort		i5 : 1;
		ushort		i4 : 1;
		ushort		i3 : 1;
		ushort		i2 : 1;
        ushort      i1 : 1;    
} BITS8BYTE;

typedef struct      
{               
        ushort      b16 : 1;        /* bit 15를 의미 */
		ushort		b15 : 1;
		ushort		b14 : 1;
		ushort		b13 : 1;
		ushort		b12 : 1;
		ushort		b11 : 1;
		ushort		b10 : 1;
		ushort		b9  : 1;
		ushort		b8  : 1;
		ushort		b7  : 1;
		ushort		b6  : 1;
		ushort		b5  : 1;
		ushort		b4  : 1;
		ushort		b3  : 1;
		ushort		b2  : 1;
        ushort      b1  : 1;        /* bit0 를 의미  */
} BITS16;
 
typedef struct
{
        uchar       stx;
        uchar       opcode;
        uchar       subopcode;
        uchar       crc1;
        uchar       crc2;
} PC_DATA;


/* ------------------------------------------------------------------------- */

unsigned int string2Int(char *str);
void	poll_ack();
void	tError();
void	Dram_Test();
void	NVRam_Test();
void	SRam_Test();
void	Dip_SW_Test();
void	Diag_LED_Test();
void	SetMac();
void	Digital_IO_Test();
void	uartClose(int channel);
void	menu_main(void);

char	uartTest(int port_num);
void	fTest();
void makeFlashBoot(char *name);
void send_msg(uchar opcode,uchar sub_op);
void board_test();
void	watchDogTimer_Enable();
STATUS  watchDogTimer_Clear_Set();
STATUS  watchDogTimer_Clear_Free();

/* ------------------------------------------------------------------------- */

int	LOOP_FLAG;
typedef int PROTO;
#define Ok 	0
void recv_drv(int );

/* ------------------------------------------------------------------------- */

#endif // _HWFUNCTEST_H_