/* ------------------------------------------------------------------ */
// MainTask.cpp
//KUL, SYT station are not included. - 3 changed at MainTask.cpp, CommDrv.cpp
//
/* ------------------------------------------------------------------ */

// Include SYSTEM header.
#include "msgQLib.h"
#include "wdLib.h"
#include "tickLib.h"
#include "taskLib.h"
#include "kernelLib.h"
#include "sysLib.h"
#include "routeLib.h"
#include "timers.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// Include BSP header
#include "kvme402a.h"
#include "ds1646.h"

// Include user header
#include "../INCLUDE/LogTask.h"
#include "taskWatch.h"
#include "myque.h"
#include "brdctrl.h"
#include "../INCLUDE/eipemu.h"
#include "ComModem.h"
#include "CommConsole.h"
#include "CommIF.h"
#include "CommCTC.h"
#include "CommGPS.h"
#include "Crc.h"
#include "MainTask.h"

/* ------------------------------------------------------------------ */
#undef PROCDEB

/* ------------------------------------------------------------------ */
typedef unsigned short WORD;

/* ------------------------------------------------------------------ */
#define PTR_EMUBUFFER	( g_nPTR_INBUFFER + g_nSIZE_INBUFFER )	// 	
//#define ADDR_SYSINPUT	(m_nScrEndPos+0x42)	

#define CPU_ROTARYSW_ADDR	0xF7000000		// rotary switch address.
#define CPU_DIPSW_ADDR		0xF7000002 		// dip switch adderess.

#define FAILLEDOFF		((unsigned char *)0xF5000000)
#define FAILLEDON		((unsigned char *)0xF9000000)

unsigned char app_tlbit_swap[256] = {
0x00,0x80,0x40,0xc0,0x20,0xa0,0x60,0xe0,
0x10,0x90,0x50,0xd0,0x30,0xb0,0x70,0xf0,
0x08,0x88,0x48,0xc8,0x28,0xa8,0x68,0xe8,
0x18,0x98,0x58,0xd8,0x38,0xb8,0x78,0xf8,
0x04,0x84,0x44,0xc4,0x24,0xa4,0x64,0xe4,
0x14,0x94,0x54,0xd4,0x34,0xb4,0x74,0xf4,
0x0c,0x8c,0x4c,0xcc,0x2c,0xac,0x6c,0xec,
0x1c,0x9c,0x5c,0xdc,0x3c,0xbc,0x7c,0xfc,
0x02,0x82,0x42,0xc2,0x22,0xa2,0x62,0xe2,
0x12,0x92,0x52,0xd2,0x32,0xb2,0x72,0xf2,
0x0a,0x8a,0x4a,0xca,0x2a,0xaa,0x6a,0xea,
0x1a,0x9a,0x5a,0xda,0x3a,0xba,0x7a,0xfa,
0x06,0x86,0x46,0xc6,0x26,0xa6,0x66,0xe6,
0x16,0x96,0x56,0xd6,0x36,0xb6,0x76,0xf6,
0x0e,0x8e,0x4e,0xce,0x2e,0xae,0x6e,0xee,
0x1e,0x9e,0x5e,0xde,0x3e,0xbe,0x7e,0xfe,
0x01,0x81,0x41,0xc1,0x21,0xa1,0x61,0xe1,
0x11,0x91,0x51,0xd1,0x31,0xb1,0x71,0xf1,
0x09,0x89,0x49,0xc9,0x29,0xa9,0x69,0xe9,
0x19,0x99,0x59,0xd9,0x39,0xb9,0x79,0xf9,
0x05,0x85,0x45,0xc5,0x25,0xa5,0x65,0xe5,
0x15,0x95,0x55,0xd5,0x35,0xb5,0x75,0xf5,
0x0d,0x8d,0x4d,0xcd,0x2d,0xad,0x6d,0xed,
0x1d,0x9d,0x5d,0xdd,0x3d,0xbd,0x7d,0xfd,
0x03,0x83,0x43,0xc3,0x23,0xa3,0x63,0xe3,
0x13,0x93,0x53,0xd3,0x33,0xb3,0x73,0xf3,
0x0b,0x8b,0x4b,0xcb,0x2b,0xab,0x6b,0xeb,
0x1b,0x9b,0x5b,0xdb,0x3b,0xbb,0x7b,0xfb,
0x07,0x87,0x47,0xc7,0x27,0xa7,0x67,0xe7,
0x17,0x97,0x57,0xd7,0x37,0xb7,0x77,0xf7,
0x0f,0x8f,0x4f,0xcf,0x2f,0xaf,0x6f,0xef,
0x1f,0x9f,0x5f,0xdf,0x3f,0xbf,0x7f,0xff};

#define APP_RTC_READ(x) \
	( app_tlbit_swap[*(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x)] )
#define APP_RTC_WRITE(x,y) \
	( *(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x) = app_tlbit_swap[(y)])

#define APP_BIN2BCD(b)	(((b)%10) | (((b)/10)<<4))
#define APP_BCD2BIN(b)	(((b)&0xf) + ((b)>>4)*10)

/* ------------------------------------------------------------------ */
/*
 * Details:
 *
 *	DS1646 consists of 8 8-bit registers for 
 *	control and time keeping. The date and time registers are in BCD format, 
 *	arranged as shown below:
 *
 *                        Data
 *   Address    |  D7 D6 D5 D4 D3 D2 D1 D0 |  Function
 *   ---------+--------------------------+----------------------
 *   0x1ffff    |  -- -- -- -- -- -- -- -- |  Year     (00-0x99)
 *   0x1fffe    |   0  0  0 -- -- -- -- -- |  Month    (01-0x12)
 *   0x1fffd    |   0  0 -- -- -- -- -- -- |  Day      (01-0x31)
 *   0x1fffc    |   0 FT  0  0  0 -- -- -- |  Weekday  (01-0x7)
 *   0x1fffb    |  KS  0 -- -- -- -- -- -- |  Hour     (00-0x23)
 *   0x1fffa    |   0 -- -- -- -- -- -- -- |  Minutes  (00-0x59)
 *   0x1fff9    |  ST -- -- -- -- -- -- -- |  Seconds  (00-0x59)
 *   0x1fff8    |   W  R  S -- -- -- -- -- |  Control
 *   ---------+--------------------------+----------------------
 *   Key:  ST = Stop Bit     R = Read Bit      FT = Frequency Test
 *          W = Write Bit    S = Sign Bit      KS = Kick Start
 *
 *   The addresses are really the offset from the start of the battery-backed
 *   ram base. The DS1646 does not use interrupts in any way.
 */
/* ------------------------------------------------------------------ */
#define	RTC_REG_ADDR_BASE	((unsigned char *)0xf207fff8)	/* NVRAM */
#define RTC_REG_CONTROL		((unsigned char *)0xf207fff8)
#define RTC_REG_SECOND		((unsigned char *)0xf207fff9)
#define RTC_REG_MINUTE		((unsigned char *)0xf207fffa)
#define RTC_REG_HOUR		((unsigned char *)0xf207fffb)
#define RTC_REG_DAY			((unsigned char *)0xf207fffc)
#define RTC_REG_DATE		((unsigned char *)0xf207fffd)
#define RTC_REG_MONTH		((unsigned char *)0xf207fffe)
#define RTC_REG_YEAR		((unsigned char *)0xf207ffff)
#define RTC_REG_STOP		((unsigned char *)0xf207fff9)	/* Bit 7 = STOP */
#define RTC_REG_KSTART		((unsigned char *)0xf207fffb)	/* Bit 7 = KICK START */


/* Defined const vale */ 
#define RTC_VAL_WRITE		(BYTE)0x80			/* Values to be written to the */
#define RTC_VAL_READ		(BYTE)0x40			/* RTC_CONTROL register */
#define RTC_VAL_STOPPED		(BYTE)0x80			/* Values for starting and */
#define RTC_VAL_KICK		(BYTE)0x80			/* stopping the oscillator */

// #define BCD_TO_BIN(bcd_value) (((bcd_value % 16) + ((bcd_value / 16) * 10)))
// #define BIN_TO_BCD(bin_value) (((bin_value % 10) + ((bin_value / 10) * 16)))

/* ------------------------------------------------------------------ */
extern UCHAR binArrayStart[];	/* binary image */
extern UCHAR binArrayEnd;		/* end of binary image */
extern int _Time1Sec;			// Be declared in "Eipemu.cpp"

/* ------------------------------------------------------------------ */
extern "C" void ifm();
extern "C" STATUS ledCon(char led);
extern "C" int	i_Start( int );
extern "C" void menu_main(void);
// VME serial port init function
extern "C" vmeSerialDrvInstall();

extern "C" STATUS EnetAttach(int unit, char *ipBuf, int netmask);

extern "C" void testMemCrack();
extern "C" void testPatchCode();
extern "C" void testMemCrack2();

extern BYTE GetStatusOfUpBLOCK();
extern BYTE GetStatusOfDownBLOCK();
extern BYTE GetStatusOfMidBLOCK();
extern void ModemTaskRun(BYTE bReverse, BYTE bUseSAXL, BYTE bUPNewIfc, BYTE bDNNewIfc, BYTE *LeftPorts, BYTE *RightPorts, BYTE *MidPorts, BLOCK_NET_ADDR *pNetAddr);

extern void EthBlockLoop();
unsigned short int _nIllegalFailure;

/* ------------------------------------------------------------------ */
// setting to interlocking table in the flash ROM address.

//2003.5.20unsigned char *_cmosram = (unsigned char *)0xF1000000;		// SRAM //
unsigned char *_cmosram = (unsigned char *)0xF2000300;		// NV RAM //

unsigned char *pInterlockTable = binArrayStart;
//unsigned char *m_pIoAssTable   = binArrayStart + 0x12404;

/* ------------------------------------------------------------------ */
unsigned char VMEM[MAX_SIZE_VMEMBUFFER];
unsigned char _xpVMEM[MAX_SIZE_VMEMBUFFER];
unsigned char _pOutBuffer[0x120];	// 32 + 256 bytes(2048 IO port = 256 * 8)
unsigned char _xpOutBuffer[0x120];	// 32 + 256 bytes(2048 IO port = 256 * 8)
unsigned char _pInBuffer[0x120];	// 32 + 256 bytes(2048 IO port = 256 * 8)
unsigned char _xpInBuffer[0x120];	// 32 + 256 bytes(2048 IO port = 256 * 8)
unsigned char _pDifferCount[2048];	// 64EA cards * 32 port.
unsigned char _cSysROutputBits;
unsigned char _cSysRInputBit2;
unsigned char _cSysRInputBit1;
unsigned short _nSysOutputOffset;
unsigned short _nSysInputOffset;
unsigned char *g_pSysControlByte;	// used in "Brdctrl.cpp"
unsigned char *g_pTrackImage;
unsigned char *g_pSwitchImage;
unsigned char *g_pSignalImage;
unsigned char *g_pLampImage;
unsigned short g_nIllegalIOFail;
unsigned short g_nLogicalFail;
short g_nCtcCmdQuePtr;
short g_nPTR_INBUFFER;
short g_nSIZE_INBUFFER = 128;	// 0x60->96->128 //
BYTE  g_nSysNo = 0;				// input data is inversed.
BYTE  g_bSimMode = 0;
BYTE  g_bEthBlockLoop = 0;

char  g_Active;
int   g_nOldSysRun = 0;

BYTE  g_ndipSwitch3=0;		// IOSIM DipSwitch

BYTE  g_StationClosed=0;	// This has to write in ROM.
BYTE  g_dnBuffer[16]; // B2 --> B3 when station closed.
BYTE  g_upBuffer[16]; // B4 --> B1 when station closed.
BYTE  g_midBuffer[16];

BYTE  g_nCheckTime=0;
BYTE  g_CardNo[20]={0};

/* ----------------------------------------------------------------------- */
int  g_nMCCRTimeTicked = 0;		/* time tick for MCCR */
int  g_nMMCRTimeTicked = 0;		/* time tick for MMCR */
int	 g_nMainRunTimeTicked = 1;	/* time tick for main run function */

/* ----------------------------------------------------------------------- */
WDOG_ID		g_wid_ComIF;

/* ------------------------------------------------------------------ */
MainTask 	Eis;

/* ------------------------------------------------------------------ */
char _msgBuffer[128];

/* 
 * Function Defines
 */
// clock control functions. 
void InitClock();
void WriteClock( BYTE *pMsg );
void WriteClockCTC( BYTE *pMsg );
void ReadClock();
void ReadClockCTC( BYTE *pCurTime );
void opSetTimerCheck( BYTE *pMsg );
int  opIsCommand( BYTE *pMsg );

// ram control functions.
int RamBackup();
int RamReload();
int CheckPrgCode();

// interface functions.
int  ChangeOver();
void InAreaUpdate();
void OutAreaUpdate( short nIsSub );
int  CheckLoadCtcCmd();
//void DTSCommandProc();
void tScan();
//int  rClearComIFTimeTick();

/* ------------------------------------------------------------------ */
SystemStatusType	*EI_Status  = (SystemStatusType *)&VMEM[OFFSET_SYS_VAR];	// &sysvar[0]
SystemStatusType	*EIx_Status = (SystemStatusType *)&_xpVMEM[OFFSET_SYS_VAR];	// &sysvar[0]

CommStatusType		*EI_CommStatus  = (CommStatusType *)&VMEM[OFFSET_COMM_STATUS];	// CommStatus
UnitStatusType		*EI_UnitStatus  = (UnitStatusType *)&VMEM[OFFSET_UNIT_STATUS];	// UnitStatus
UnitStatusType		*EIx_UnitStatus = (UnitStatusType *)&_xpVMEM[OFFSET_UNIT_STATUS];	// UnitStatus

// Data Version 정보의 위치 변경 0x2D -> 0x01
BYTE  *DataVersion = (BYTE *)&VMEM[OFFSET_DATA_VERSION];
// Logical, Physical END

/*
;======================================================================
; TABLE DEFINE
;======================================================================

m_TableBase 	        = $00   ;00 0100
m_pInfoMem              = $01   ;00 4800
m_pInfoGeneral 	        = $49   ;00 0200
m_pRouteAddTable        = $4B   ;00 0400
m_pRouteCrcTable        = $4F   ;00 0400
m_pRouteSizeTable       = $53   ;00 0200
m_pSwitchOnTrackTable 	= $55   ;00 0100
m_pSignalInfoTable      = $56   ;00 1000
m_pTrackInfoTable       = $66   ;00 0100            ; from $80, TNI Ptr
m_pCommonTrack 	        = $67   ;00 0100
m_pIoAssTable           = $68   ;00 1000
m_pDtsAssTable          = $78   ;00 0800


m_VMEM 	                = $00; 0300	1024 BYTE -> 768 byte

m_pCtcCmd  = $2F0

m_pTrainID              = $03; 0100
m_pSignalTimer 	        = $04; 0100
m_pTrackTimer 	        = $05; 0100                 ; from $80, DTS Out Area
m_pSwitchTimer 	        = $06; 0100                 ; from $80, BackRouteSignal
m_pSwitchRouteTrack     = $07; 0100 [128 * 2];
m_pSwitchOverTrack1     = $08; 0100 [128];
m_pSwitchOverTrack2     = $09; 0100 [128];
m_pTrackPrev            = $0A; 0100
m_pLastTrack            = $0B; 0100
m_pRouteQue             = $0C; 0200,    WORD X 100
m_pOutBuffer 	        = $0E; 0100
m_pInBuffer 	        = $0F; 0100

*/

/*
	VMEM Memory Map

	<<< VMEM >>>														Exchange Buffer ( Comm_IF )

				+--------------------+									
	0(0x00)		|                    |		nMsgHeader	Online(0x0A) / Standby(0x05)
	1(0x01)		|                    |		nDataVer
				+--Date/Time Start---+									
	2(0x02)		|                    |		nSec
	3(0x03)		|                    |		nMin
	4(0x04)		|                    |		nHour
	5(0x05)		|                    |		nDate
	6(0x06)		|                    |		nMonth
	7(0x07)		|                    |		nYear
				+---System Status----+									
	8(0x08)		|                    |		nSysVar	(Sync)	bStation/bTCSR/bGround/bTrackOcc/bSYS1/bSYS1GOOD/bSYS2/bSYS2GOOD
	9(0x09)		|					 |				(Sync)	bN1/bN2/bN3/bB24PWR/bUPS1/bUPS2/bGenLowFuel/bGen
	10(0x0A)	|					 |						bCharge/bSignalFail/bPointFail/bAXLFail////bPowerFail
	11(0x0B)	|					 |						bIActiveOn1/bIActiveOn2/bIVPOR1/bIVPOR2/bVPOR1/bVPOR2/bMode/bCTCRequest
	12(0x0C)	|					 |						bStartup/bGenRun/bATB/bExtPowerS/bUpModem/bDnModem/bFan1/bFan2
	13(0x0D)	|					 |						
	14(0x0E)	|					 |					
	15(0x0F)	|					 |				
				+---System Status----+									

		
	
	48(0x30)	+---Object Start-----+
				|                    |	]
				|                    |	]
				| Objects         	 |	]
				|                    |	]
				|                    |	]
				|                    |	]
				|                    |	]
				|                    |	]
  ScrEndPos(SEP)+-EMU->m_pMsgBuffer--+	]
				| Console Msg.		 |	]
	SEP+0x10	+--------------------+	]	<< If a System is Standby, Synchronize This Section with Active Side.
				| Spare Area		 |	]	
	SEP+0x38    +---g_nCtcCmdQuePtr--+ 	]
				|                    |	]
				| CtcCmdQue			 |	]
				|                    |	]
	SEP+0x50	+--EMU->m_nTNIBase---+	]
				|                    |	]
				| TNI				 |	]
				|                    |	]
	SEP+0xB0	+--g_nPTR_INBUFFER----+	<<< copy for TxVMEM
				|                    |	]
				| INBUFFER			 |	] g_nSIZE_INBUFFER = 128(0x80 = 32EA cards * 4bytes(32Ch))
				|                    |	]
	SEP+0x130	+--PTR_EMUBUFFER-----+  <<< copy EMU->RouteQue
				|                    |  ]					
				|   EMU Internal     |  ] m_pMyEngine->m_nRamSize
				|                    |  ]
				+--------------------+	

*/

//;=====================================================================
//; Program start
//;=====================================================================

int WaitOprLow() 
{
	return 0;
}


void WaitOnSyncForMainRun() 
{
	int n;
	
	for(n=0; n<MAX_WAITTICK_MAINRUN; n++) {
		if ( g_nMainRunTimeTicked ) break;
		taskDelay(1);
	}
}

/* ----------------------------------------------------------------------- */
int rSetMainRunTimeTick() {
	g_nMainRunTimeTicked = 1;
	return 0;
}

//int rClearComIFTimeTick() {
//	Eis.bComState.bRxImageGood = 0;  //2011.2.2 삭제함...
//	return 0;
//}

/* ----------------------------------------------------------------------- */
int  rSetMCCRTimeTick() 
{
	g_nMCCRTimeTicked = 1;
}
void rClearMCCRTimeTick() 
{
	g_nMCCRTimeTicked = 0;
}

int  rSetMMCRTimeTick() 
{
	g_nMMCRTimeTicked = 1;
}
void rClearMMCRTimeTick() 
{
	g_nMMCRTimeTicked = 0;
}

/****************************************************************************/
/*
 * InitClock - Initialize the onboard RTC.
 *
 * This turns on the RTC oscillator if needed, creates a semaphore
 * to control access, and returns OK after making sure the clock
 * is really running.
 *
 */
void InitClock() {
	unsigned char value;
	unsigned char *pClock = RTC_REG_CONTROL;
	
	//sysClkRateSet(60);
	kernelTimeSlice(1);

	value  = *(RTC_REG_CONTROL);
	value &= 0x3f;
	*(RTC_REG_CONTROL) = value;
	
	/*
	 * Suspend updating of registers in the RTC
	 */

	*(RTC_REG_CONTROL) |= RTC_VAL_READ;

	/*
	 * Check if the clock is stopped
	 */

	if (*(RTC_REG_STOP) & RTC_VAL_STOPPED) {
		*(RTC_REG_CONTROL) |= RTC_VAL_WRITE;	/* If stopped, */
		*(RTC_REG_STOP) &= ~RTC_VAL_STOPPED;	/* kick-start the oscillator */
		*(RTC_REG_KSTART) |= RTC_VAL_KICK;		/* stop bit clear */
		*(RTC_REG_CONTROL) &= ~RTC_VAL_WRITE;	/* write bit clear */

		taskDelay(2 * sysClkRateGet());

		*(RTC_REG_CONTROL) |= RTC_VAL_WRITE;
		*(RTC_REG_KSTART) &= ~RTC_VAL_KICK;
		*(RTC_REG_CONTROL) &= ~RTC_VAL_WRITE;
	}

	/*
	 * Resume updating of the RTC registers
	 */

	*(RTC_REG_CONTROL) &= ~RTC_VAL_READ;

	taskDelay(sysClkRateGet() / 2);
}

/***************************************************************************/
/* 
 * TimeStop() - Stop the RTC oscillator.
 *
 * This routine stops the oscillator in the RTC, putting the device
 * into power-save mode (recommended if it is going to spend time on the
 * shelf).
 *
 */

int TimeStop()
{
	*(RTC_REG_CONTROL) |= RTC_VAL_WRITE;
	*(RTC_REG_STOP) |= RTC_VAL_STOPPED;
	*(RTC_REG_CONTROL) &= ~RTC_VAL_WRITE;

	if (*(RTC_REG_STOP) & RTC_VAL_STOPPED) 
	   return 1;
	else 
	   return 0;
}


/***************************************************************************/
/*
 * TimeStart() - Staet the RTC oscillator.
 *
 * This routine starts the oscillator in the RTC,
 *
 */

int TimeStart()
{
	*(RTC_REG_CONTROL) |= RTC_VAL_WRITE;
	*(RTC_REG_STOP) &= ~RTC_VAL_STOPPED;
	*(RTC_REG_CONTROL) &= ~RTC_VAL_WRITE;

	if (*(RTC_REG_STOP) & ~RTC_VAL_STOPPED) 
		return 1;
	else 
		return 0;
}

/****************************************************************************/
/*
 * ReadTime - Read RTC.
 *
 */

void ReadClock() {
#if 0
	*(RTC_REG_CONTROL) |= RTC_VAL_READ;
	
	VMEM[OFFSET_TIME_SEC]	= (BYTE)*(RTC_REG_SECOND) & 0x7F;	// sec
	VMEM[OFFSET_TIME_MIN]	= (BYTE)*(RTC_REG_MINUTE) & 0x7F;	// min
	VMEM[OFFSET_TIME_HOUR]	= (BYTE)*(RTC_REG_HOUR) & 0x3F;		// hour
	VMEM[OFFSET_TIME_DAY]	= (BYTE)*(RTC_REG_DATE) & 0x3F;		// date
	VMEM[OFFSET_TIME_MON]	= (BYTE)*(RTC_REG_MONTH) & 0x1F;	// month
	VMEM[OFFSET_TIME_YEAR]	= (BYTE)*(RTC_REG_YEAR);			// year
	
	*(RTC_REG_CONTROL) &= ~RTC_VAL_READ;
#endif
	struct tm tm;

	sysRtcGet(&tm);

	VMEM[OFFSET_TIME_SEC]	= APP_BIN2BCD(tm.tm_sec);		// sec
	VMEM[OFFSET_TIME_MIN]	= APP_BIN2BCD(tm.tm_min); 		// min
	VMEM[OFFSET_TIME_HOUR]	= APP_BIN2BCD(tm.tm_hour);		// hour
	VMEM[OFFSET_TIME_DAY]	= APP_BIN2BCD(tm.tm_mday);		// date
	VMEM[OFFSET_TIME_MON]	= APP_BIN2BCD(tm.tm_mon + 1);	// month
	VMEM[OFFSET_TIME_YEAR]	= APP_BIN2BCD(tm.tm_year - 100);// year

	LogDbg(LOG_TYPE_GEN, "\nReadClock [%x][%x][%x][%x][%x][%x]\n",VMEM[OFFSET_TIME_SEC],VMEM[OFFSET_TIME_MIN],VMEM[OFFSET_TIME_HOUR],VMEM[OFFSET_TIME_DAY],VMEM[OFFSET_TIME_MON],VMEM[OFFSET_TIME_YEAR]);
	
//	if ((BYTE)*(RTC_REG_SECOND) & RTC_VAL_STOPPED)
//		return 0;
//	return 1;
}

void ReadClockCTC( BYTE *pCurTime ) {
	struct tm tm;

	sysRtcGet(&tm);

	pCurTime[5]	= tm.tm_sec;		// sec
	pCurTime[4] = tm.tm_min; 		// min
	pCurTime[3] = tm.tm_hour;		// hour
	pCurTime[2] = tm.tm_mday;		// date
	pCurTime[1] = tm.tm_mon + 1;	// month
	pCurTime[0] = tm.tm_year - 100;	// year
}

/****************************************************************************/
/*
 *   WriteClock - Set the RTC.
 *
 *   This function tests all the time/date values for legality and if
 *   they are legal shuts off the clock, updates the registers and
 *   then restarts the clock.
 *
 */

void WriteClock( BYTE *pMsg ) {
	/*
	 *	Write values into RTC registers, masking off high bits that are not 
	 *	part of the data.
 	 */
#if 0
	*(RTC_REG_CONTROL) |= RTC_VAL_WRITE;
	
	*(RTC_REG_SECOND) = (*(RTC_REG_SECOND) & 0x80) | (pMsg[2] & 0x7f);	// sec
	*(RTC_REG_MINUTE) = (*(RTC_REG_MINUTE) & 0x80) | (pMsg[3] & 0x7f);	// min
	*(RTC_REG_HOUR)   = (*(RTC_REG_HOUR) & 0xc0)   | (pMsg[4] & 0x3f);	// hour
	*(RTC_REG_DATE)   = (*(RTC_REG_DATE) & 0xc0)   | (pMsg[5] & 0x3f);	// date
	*(RTC_REG_MONTH)  = (*(RTC_REG_MONTH) & 0xe0)  | (pMsg[6] & 0x1f);	// month
	*(RTC_REG_YEAR)   = pMsg[7];	// year
	
	*(RTC_REG_CONTROL) &= ~RTC_VAL_WRITE;
#endif

	LogDbg(LOG_TYPE_GEN, "\nWriteClock [%x][%x][%x][%x][%x][%x]\n",pMsg[2],pMsg[3],pMsg[4],pMsg[5],pMsg[6],pMsg[7]);

	struct tm tm;
	struct timespec tv;
	time_t t;
	unsigned char sec;
	int i;

#if 0
	tm.tm_sec   = *(RTC_REG_SECOND);
	tm.tm_min   = *(RTC_REG_MINUTE);
	tm.tm_hour  = *(RTC_REG_HOUR);
	tm.tm_mday  = *(RTC_REG_DATE);
	tm.tm_mon   = *(RTC_REG_MONTH);
	tm.tm_year  = *(RTC_REG_YEAR);
	tm.tm_wday  = 0;
	tm.tm_yday  = 0;
	tm.tm_isdst = 0;
#endif

	// Convert BCD to BIN
	tm.tm_sec	= APP_BCD2BIN(pMsg[2]);
	tm.tm_min	= APP_BCD2BIN(pMsg[3]);
	tm.tm_hour	= APP_BCD2BIN(pMsg[4]);
	tm.tm_mday	= APP_BCD2BIN(pMsg[5]);
	tm.tm_mon	= APP_BCD2BIN(pMsg[6]) - 1;
	tm.tm_year	= APP_BCD2BIN(pMsg[7]) + 100;

#if 0
	tm.tm_sec   = pMsg[2];
	tm.tm_min   = pMsg[3];
	tm.tm_hour  = pMsg[4];
	tm.tm_mday  = pMsg[5];
	tm.tm_mon   = pMsg[6] - 1;
	tm.tm_year  = pMsg[7] + 100;
#endif

	tm.tm_wday  = 0;
	tm.tm_yday  = 0;
	tm.tm_isdst = 0;
		
	tv.tv_sec = mktime(&tm);

	tv.tv_nsec = 0;

	sysRtcSet(&tm); 

	sec = APP_BIN2BCD(tm.tm_sec);
	
	for (i = 0 ; i < 10000000 ; i++)
		if ((APP_RTC_READ(1) & 0x7f) != sec)
			break;
	
	tv.tv_sec ++;
	clock_settime(CLOCK_REALTIME, &tv);

	Log("WriteClock : %s", asctime(&tm));
 
#if 0
	if (tm.tm_year < 80)
		tm.tm_year += 100;
		
	tv.tv_sec = mktime(&tm);
	tv.tv_nsec = 0;
		
	sec = BIN2BCD(tm.tm_sec);
	
	for (i = 0 ; i < 10000000 ; i++)
		if ((RTC_READ(1) & 0x7f) != sec)
			break;
		
	tv.tv_sec ++;
	clock_settime(CLOCK_REALTIME, &tv);
	
	TimeStart();
#endif
}

void WriteClockCTC( BYTE *pMsg ) {
	/*
	 *	Write values into RTC registers, masking off high bits that are not 
	 *	part of the data.
 	 */
	LogDbg(LOG_TYPE_GEN, "\nWriteClock [%x][%x][%x][%x][%x][%x]\n",pMsg[0],pMsg[1],pMsg[2],pMsg[3],pMsg[4],pMsg[5]);

	struct tm tm;
	struct timespec tv;
	time_t t;
	unsigned char sec;
	int i;

	tm.tm_sec	= pMsg[5];
	tm.tm_min	= pMsg[4];
	tm.tm_hour	= pMsg[3];
	tm.tm_mday	= pMsg[2];
	tm.tm_mon	= pMsg[1] - 1;
	tm.tm_year	= pMsg[0] + 100;

	tm.tm_wday  = 0;
	tm.tm_yday  = 0;
	tm.tm_isdst = 0;
		
	tv.tv_sec = mktime(&tm);

	tv.tv_nsec = 0;

	sysRtcSet(&tm); 

	sec = APP_BIN2BCD(tm.tm_sec);
	
	for (i = 0 ; i < 10000000 ; i++)
		if ((APP_RTC_READ(1) & 0x7f) != sec)
			break;
	
	tv.tv_sec ++;
	clock_settime(CLOCK_REALTIME, &tv);

	Log("WriteClockCTC : %s", asctime(&tm));
}

/****************************************************************************/
int  opIsCommand( BYTE *pMsg )
{
	BYTE cmd = pMsg[0];
    if ( (cmd & 0xF0) != 0x40 ) return 0;
    if ( pMsg[1] ) 
		return 1;
	else 
		return 0;
}

void opSetTimerCheck( BYTE *pMsg ) 
{
	BYTE cmd = pMsg[0];
    if ( (cmd & 0xF0) != 0x40 ) return;
    if ( pMsg[1] == WS_OPMSG_SETTIMER  ) {
		WriteClock( pMsg );
		//pMsg[0] = 0x00;  // Don't logging -> 0x00
	}
}

/****************************************************************************/
int CheckPrgCode() 
{
	unsigned char  c;
	unsigned short nCRC = 0;
	unsigned char *p = pInterlockTable;
	unsigned short nTableCRC = *(unsigned short*)&p[0];
	unsigned int nCount = *(unsigned int*)&p[4];
	unsigned int i;

	printf("\n>>nCount = 0x%X", nCount);
	p += 2;

	for (i = 2; i<nCount; i++) 
	{
		nCRC = crc_CCITT( *p++, nCRC );
	}

	printf("\n>>nCRC = %X, nTableCRC = %X.",nCRC, nTableCRC);

	if ( nCRC != nTableCRC ) return 1;

	return 0;
}


/****************************************************************************/
/****************************************************************************/

MainTask::MainTask()
{
	m_nScrEndPos = 0;

// bSystemStatus;
	bSystemStatus.bOnRun        = 0; //0 0
	bSystemStatus.bLogicalFail  = 0; //1 0
	bSystemStatus.bIoUpdate     = 0; //2 0
	bSystemStatus.bPhysicalGood = 0; //3 1
	bSystemStatus.bBackupPage   = 0; //4 0
	bSystemStatus.bBusLocked	= 0; //5 
	bSystemStatus.bTrackGood    = 0; //6 0
	bSystemStatus.bOtherOK      = 0; //7 0
	bSystemStatus.bSysNo	    = 0; //8 0		;0-1계, 1-2계

// bComState;
	bComState.bOnTxData	     = 0;
	bComState.bOnRxData      = 0;
	bComState.bRxDataReady   = 0;
	bComState.bTxBufferReady = 0;
	bComState.bOnDownload    = 0;
	bComState.bRxImageGood   = 0;
	bComState.bRxAnswer      = 0;
	bComState.bCRCERROR	     = 0;


// ChipStatus;
	ChipStatus.bBRD_RESERVED = 0;
	ChipStatus.bBRD_OTHEROK  = 0; //= ChipStatus.6
	ChipStatus.bBRD_ID0      = 0; //= ChipStatus.5
	ChipStatus.bBRD_OPRPHASE = 0; //= ChipStatus.4
	ChipStatus.bBRD_MAINSW   = 0; //= ChipStatus.3      ; 0 - MAIN, 1 - SUB
	ChipStatus.bBRD_P2       = 0; //= ChipStatus.2
	ChipStatus.bBRD_P1       = 0; //= ChipStatus.1
	ChipStatus.bBRD_P0       = 0; //= ChipStatus.0

	m_pMyEngine = NULL;

	m_nSyncFailCount = 0;
	m_bSyncFail = 0;
}

MainTask::~MainTask()
{
}


int MainTask::SystemInit( int bRamRoad ) 
{
// Initialize class member.
	m_bIllegalFault = 0;
    m_FaultCount = 0; 
    m_pMsgLastCyc = 0;
	m_bErrEvg = 0;
	m_bOtherErrEvg = 0;
	
	
// clear to communication buffer. 
	memset(m_pCTCCmdBuffer, 0x00, 16);
	memset(m_pMsgBuffer, 0x00, 16);
	memset(m_pLoginBuffer, 0x00, 16);
	
//	watchDogTimer_Enable();

	BYTE &SysRunState = VMEM[OFFSET_UNIT_STATUS];	// UnitState
   	SysRunState = 0;

	BYTE *pCardBits = &VMEM[OFFSET_CARD_INFO];		// pointer of card bitmap buffer.
    memset( pCardBits, 0, 8 );

	//2004.11.11
	//VMEM[0x13 + 8] = 0x3;

	EI_Status->bSYS1Good = 1;
	EI_Status->bSYS2Good = 1;

	EI_Status->bSYS1 = 1;
	EI_Status->bSYS2 = 0;


	EI_Status->bVPOR1 = 0;
	EI_Status->bVPOR2 = 0;

	if ( !g_nSysNo ) 			// SYS1 - switch 4 ON, SYS2 - switch 4 OFF.
	{
		EI_Status->bVPOR1 = 1;
	}
	else
	{
		EI_Status->bVPOR2 = 1;
	}



//	EI_Status->GenChkPower 		=1;	// input signal of gen power to checking.(1/normal, 0/error)
//	EI_Status->NorthChkPower	=1;	// input signal of north power to checking.(1/normal, 0/error)
//	EI_Status->SouthChkPower	=1;	// input signal of south power to checking.(1/normal, 0/error)


	//ExtStatusType & ExtState = pHead->ExtState;

//	nSysVar.bGeneratorRun	=1;	// 8  generator running(1/running, 0/standby)
//	nSysVar.bGeneratorGood	=1;	// 9  generator failure(1/good, 0/failure)
//	nSysVar.bChargerGood	=1;	// 10 battery charger running(1/good, 0/fail)
//	nSysVar.bBatteryGood 	=1;	// 11 battery failure(1/good, 0/failure(low-voltage))
//	nSysVar.bMainPower		=1;	// 12 main power status(1/good, 0/failure)
//	nSysVar.bUPS  			=1;	// 13 UPS status.(1/normal, 0/error)
//	nSysVar.bAVR			=1;	// 14 AVR status.(1/normal, 0/error)
//	nSysVar.bDC50V			=1;	// 15 B50 Voltage status.(1/normal, 0/error)
//	nSysVar.bSE			= 1;	// 25 Signalling equipment error(1/normal, 0/error)
//	nSysVar.bSignalLight= 1;	// 27 Dimmer set(1/on, 0/off)
//	nSysVar.bAnyComFail	= 0;	// 28 communication failure (MCCR or MMCR or IF or CTS).
//	nSysVar.bTimeError	= 1;	// 31 unit time error
//
//	EI_Status->bN1 = 0; PAS  lock
	EI_Status->bN2 = 0; 
	EI_Status->bN3 = 0; 
	EI_Status->bATB = 0; 

//	EI_Status->bSound1 = 0;
//	EI_Status->bSound2 = 0;
//	EI_Status->bSound3 = 0;
//	EI_Status->bSound4 = 0;
//m_pMyEngine->FillVersionCode();

	// 20130604 Day/Night Input 정보 누락으로 강제 설정 추가
	EI_Status->bIDayNight = 0;

	// 20140414 sjhong - CTC Mode 정보 초기화 추가
	EI_Status->bMode = 0;

	*DataVersion = m_pMyEngine->m_pCommonTrack[255];

	return 0;
}

//;=====================================================================
//; Sys 1,2 Setup
//;=====================================================================
//BYTE bActiveOn1		:1;	// 0  input singal of CBI 1 running status. 
//BYTE bActiveOn2		:1;	// 1  input signal of CBI 2 running status.
//BYTE bVPOR1			:1;	// 2  input signal of system 1 VPOR.
//BYTE bVPOR2			:1;	// 3  input singal of system 2 VPOR.

int MainTask::BootCheck() 
{
	BYTE cUIO = *(BYTE*)CPU_DIPSW_ADDR;
	if ( cUIO & 0x10 ) 
    {			// SYS1 - switch 4 ON, SYS2 - switch 4 OFF.
printf("\n>>SYS1 - %X.",cUIO);
		bSystemStatus.bSysNo = 0; 	// 8 0		;0-1계, 1-2계
		g_nSysNo = 0;
		ChipStatus.bBRD_OTHEROK	= EI_Status->bIActiveOn2;	// SYSR2 
		ChipStatus.bBRD_ID0 = 0;

		if ( ChipStatus.bBRD_OTHEROK ) {
			EI_UnitStatus->bSysRun = EI_Status->bIVPOR2;		
		}
printf("\n>>OTHERBRDOK1 %d.", ChipStatus.bBRD_OTHEROK);
	}
	else 
    {					// SYS2
printf("\n>>SYS2 - %X.",cUIO);
		bSystemStatus.bSysNo = 1; //8 0		;0-1계, 1-2계
		g_nSysNo = 1;
		ChipStatus.bBRD_OTHEROK	= EI_Status->bIActiveOn1;	// SYSR1	
		ChipStatus.bBRD_ID0 = 1;
		if ( ChipStatus.bBRD_OTHEROK ) 
		{
			EI_UnitStatus->bSysRun = EI_Status->bIVPOR1;		
		}
printf("\n>>OTHERBRDOK2 %d.", ChipStatus.bBRD_OTHEROK);
	}

	return 0;
}

int MainTask::RotarySWCheck()
{
	int nValue;

	nValue =  *(uchar *)CPU_ROTARYSW_ADDR; 	// 전면의 로터리 스위치 address
	nValue = ~(nValue & 0xF0); 				// 1의 보수
	nValue = ((nValue >> 4) & 0x0F);

	LogInd(LOG_TYPE_GEN, "Rotary Switch Value = 0x%X", nValue);

	if(nValue == 0x01)
	{
		g_bSimMode = 1;
	}
	else if(nValue == 0x0A)
	{
		g_bEthBlockLoop = 1;
	}
}


/****************************************************************************/
int RamBackup() 
{
	memcpy( _cmosram, VMEM, sizeof(VMEM) );
	*(short*)_cmosram = 0xAA55;
	return 0;
}

int RamReload() 
{
	if ( *((unsigned short*)_cmosram) == (unsigned short)0xAA55 ) {
		memcpy( VMEM, _cmosram, sizeof(VMEM) );
		return 0;
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/

int MainTask::Run() 
{
	m_pMyEngine = new CEipEmu;
	if ( m_pMyEngine == NULL ) 
		return -1;

	if ( CheckPrgCode() != 0 ) {
		printf(	"\n>>BAD INTERLOCK TABLE." );
		return 1;
	}

    WDOG_ID wid_MainRun;

printf( "\n>>TABLE %2.2X %2.2X.", pInterlockTable[0], pInterlockTable[1] );

	wdtEnable(); // BSP 내장 함수, Watch Dog Enable

    InitClock();
	InitialCrc32();

	g_ndipSwitch3 = *(uchar *)CPU_DIPSW_ADDR;

    wid_MainRun = wdCreate();
	g_wid_ComIF = wdCreate();

// initialize interlocking engin.
	m_pMyEngine->LoadTable( pInterlockTable );
	m_pMyEngine->m_pVMEM = VMEM;	// VMEM->&VMEM[0]
	TableBaseInfoType *pTable = &(m_pMyEngine->m_TableBase);

	m_nScrEndPos = pTable->InfoGeneralIO.nStart + pTable->InfoGeneralIO.nSize;

    m_pMyEngine->m_pMsgBuffer = &VMEM[ m_nScrEndPos ];

	g_nCtcCmdQuePtr = m_nScrEndPos + 0x38;
	g_nPTR_INBUFFER = m_nScrEndPos + 0xB0;
 	g_pSysControlByte = (unsigned char*)&VMEM[OFFSET_SYS_CONTROL];	// 0x0B, 	

	g_pTrackImage = &VMEM[pTable->InfoTrack.nStart];
	g_pSwitchImage = &VMEM[pTable->InfoSwitch.nStart];
	g_pSignalImage = &VMEM[pTable->InfoSignal.nStart];
	g_pLampImage = &VMEM[pTable->InfoGeneralIO.nStart];
//	int nCountGen = pTable->InfoGeneralIO.nSize;

//	SystemStatusType *EI_OldStatus = (SystemStatusType *)&VMEM[ m_nScrEndPos + 0x44 ];
	memset( VMEM, 0, sizeof(VMEM) );
	memset( _pInBuffer, 0, sizeof(_pInBuffer) );
	memset( _pOutBuffer, 0, sizeof(_pOutBuffer) );
	memset( _pDifferCount, 0, sizeof(_pDifferCount) );
	
	short nIfsize = m_nScrEndPos + 0xB0 + g_nSIZE_INBUFFER + m_pMyEngine->m_nRamSize;
	
	printf("\n>>m_nScrEndPos:%d, g_nSIZE_INBUFFER:%d, m_nRamSize:%d, nIfsize:%d.\n", m_nScrEndPos, g_nSIZE_INBUFFER, m_pMyEngine->m_nRamSize, nIfsize);

// Sys 1,2 Setup
    BootCheck();

// 20130307 Block Interface Type Read from Rotary Switch 
	RotarySWCheck();

// initialize IO engin
	//printf("pos1 = 0x%X, pos2 = 0x%X\n", (int)pTable->IOCardInfo - (int)pTable, (int)pTable->RealID - (int)pTable);
	EIS_IO.SetBoardInfo(pTable->IOCardInfo, pTable->RealID);

// Read Config List
	m_pMyEngine->ProcessConfigList();

	CONSOLE_NET_ADDR	conNetAddr;
	BLOCK_NET_ADDR		blkNetAddr;

	memset(&conNetAddr, 0, sizeof(CONSOLE_NET_ADDR));
	memset(&blkNetAddr, 0, sizeof(BLOCK_NET_ADDR));

	char GPS_ADDR[16] = {0,};

	char MY_ADDR_PRI[16] = {0,};
	char MY_ADDR_SEC[16] = {0,};
	char PEER_ADDR_PRI[16] = {0,};
	char PEER_ADDR_SEC[16] = {0,};

	char MY_GW_PRI[16] = {0,};
	char MY_GW_SEC[16] = {0,};

	char PEER_NET_PRI[16] = {0,};
	char PEER_NET_SEC[16] = {0,};
	char CONSOLE_NET_PRI[16] = {0,};
	char CONSOLE_NET_SEC[16] = {0,};

	char UP_CBI1_NET_PRI[16] = {0,};
	char UP_CBI2_NET_PRI[16] = {0,};
	char UP_CBI1_NET_SEC[16] = {0,};
	char UP_CBI2_NET_SEC[16] = {0,};
	char DN_CBI1_NET_PRI[16] = {0,};
	char DN_CBI2_NET_PRI[16] = {0,};
	char DN_CBI1_NET_SEC[16] = {0,};
	char DN_CBI2_NET_SEC[16] = {0,};
	char MD_CBI1_NET_PRI[16] = {0,};
	char MD_CBI2_NET_PRI[16] = {0,};
	char MD_CBI1_NET_SEC[16] = {0,};
	char MD_CBI2_NET_SEC[16] = {0,};
	
	char CTC_ADDR_PRI[] = "192.100.1.10";
	char CTC_ADDR_SEC[] = "192.100.2.10";
	char CTC_NET_PRI[] 	= "192.100.1.0";
	char CTC_NET_SEC[] 	= "192.100.2.0";

	BYTE cCTCZoneNo	= m_pMyEngine->m_ConfigList.CTC_ZONENO;
	BYTE cCTCStnNo 	= m_pMyEngine->m_ConfigList.CTC_STATIONNO;
	BYTE cGPSZoneNo = m_pMyEngine->m_ConfigList.GPS_ZONENO;
	BYTE cGPSStnNo 	= m_pMyEngine->m_ConfigList.GPS_STATIONNO;
	BYTE cGPSSvrNo	= m_pMyEngine->m_ConfigList.GPS_SERVERNO;
	BYTE cUPStnNo	= m_pMyEngine->m_ConfigList.UP_STATIONNO;
	BYTE cDNStnNo	= m_pMyEngine->m_ConfigList.DN_STATIONNO;
	BYTE cMDStnNo	= m_pMyEngine->m_ConfigList.MD_STATIONNO;
	BYTE bUseMCNet	= m_pMyEngine->m_ConfigList.USE_MMCR_NET;
	BYTE bUseCCNet	= m_pMyEngine->m_ConfigList.USE_MCCR_NET;
	BYTE bUseModem	= m_pMyEngine->m_ConfigList.USE_MODEM_BKUP;
	BYTE bUPNewIfc	= m_pMyEngine->m_ConfigList.UP_NEWCBI_IFC;
	BYTE bDNNewIfc	= m_pMyEngine->m_ConfigList.DN_NEWCBI_IFC;

	LogInd(LOG_TYPE_GEN, "cCTCZoneNo[%d], cCTCStnNo[%d], cGPSZoneNo[%d], cGPSStnNo[%d], cGPSSvrNo[%d], cUPStnNo[%d], cDNStnNo[%d], cMDStnNo[%d], bUseMCNet[%d], bUseCCNet[%d], bUseModem[%d]", 
			cCTCZoneNo, cCTCStnNo, cGPSZoneNo, cGPSStnNo, cGPSSvrNo, cUPStnNo, cDNStnNo, cMDStnNo, bUseMCNet, bUseCCNet, bUseModem);

	if(cCTCStnNo && cCTCZoneNo)
	{
		if(!g_nSysNo)
		{
			sprintf(MY_ADDR_PRI, "192.%d.1.1", cCTCStnNo);
			sprintf(MY_ADDR_SEC, "192.%d.2.1", cCTCStnNo);

			sprintf(MY_GW_PRI, "192.%d.1.126", cCTCStnNo);
			sprintf(MY_GW_SEC, "192.%d.2.126", cCTCStnNo);

			sprintf(PEER_ADDR_PRI, "192.%d.1.129", cCTCStnNo);
			sprintf(PEER_ADDR_SEC, "192.%d.2.129", cCTCStnNo);

			sprintf(PEER_NET_PRI, "192.%d.1.128", cCTCStnNo);
			sprintf(PEER_NET_SEC, "192.%d.2.128", cCTCStnNo);
		}
		else
		{
			sprintf(MY_ADDR_PRI, "192.%d.1.129", cCTCStnNo);
			sprintf(MY_ADDR_SEC, "192.%d.2.129", cCTCStnNo);

			sprintf(MY_GW_PRI, "192.%d.1.254", cCTCStnNo);
			sprintf(MY_GW_SEC, "192.%d.2.254", cCTCStnNo);

			sprintf(PEER_ADDR_PRI, "192.%d.1.1", cCTCStnNo);
			sprintf(PEER_ADDR_SEC, "192.%d.2.1", cCTCStnNo);

			sprintf(PEER_NET_PRI, "192.%d.1.0", cCTCStnNo);
			sprintf(PEER_NET_SEC, "192.%d.2.0", cCTCStnNo);
		}

		LogInd(LOG_TYPE_GEN, "My Primary IP        : %s", MY_ADDR_PRI);
		LogInd(LOG_TYPE_GEN, "My Secondary IP      : %s", MY_ADDR_SEC);
		LogInd(LOG_TYPE_GEN, "Peer Primary IP      : %s", PEER_ADDR_PRI);
		LogInd(LOG_TYPE_GEN, "Peer Secondary IP    : %s", PEER_ADDR_SEC);
		LogInd(LOG_TYPE_GEN, "CTC Primary IP       : %s", CTC_ADDR_PRI);
		LogInd(LOG_TYPE_GEN, "CTC Secondary IP     : %s", CTC_ADDR_SEC);

		if(g_bSimMode == 1)
		{
			EnetAttach(1, MY_ADDR_SEC, 0xFF000000);

			ifMaskSet("cpm0", 0xFF000000);
			ifMaskSet("cpm1", 0xFF000000);

			// 각 인터페이스에 IP 추가
			ifAddrAdd("cpm0", MY_ADDR_PRI, NULL, 0xFF000000);
			ifAddrAdd("cpm1", MY_ADDR_SEC, NULL, 0xFF000000);
		}
		else
		{
			// P1 Ethernet Port Link Up
			EnetAttach(1, MY_ADDR_SEC, 0xFFFFFF80);

			ifMaskSet("cpm0", 0xFFFFFF80);
			ifMaskSet("cpm1", 0xFFFFFF80);

			// 각 인터페이스에 IP 추가
			ifAddrAdd("cpm0", MY_ADDR_PRI, NULL, 0xFFFFFF80);
			ifAddrAdd("cpm1", MY_ADDR_SEC, NULL, 0xFFFFFF80);

			// CBI 계간통신을 위한 반대쪽 계의 네트워크 라우팅
			routeNetAdd(PEER_NET_PRI, MY_GW_PRI);
			routeNetAdd(PEER_NET_SEC, MY_GW_SEC);

			// CTC 네트워크에 대한 라우팅
			routeNetAdd(CTC_NET_PRI, MY_GW_PRI);
			routeNetAdd(CTC_NET_SEC, MY_GW_SEC);

			LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", PEER_NET_PRI, MY_GW_PRI);
			LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", PEER_NET_SEC, MY_GW_SEC);
			LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", CTC_NET_PRI, MY_GW_PRI);
			LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", CTC_NET_SEC, MY_GW_SEC);

			conNetAddr.bIsDualNet = 0;
			strncpy(conNetAddr.MY_ADDR_PRI, MY_ADDR_PRI, 16);
			strncpy(conNetAddr.MY_ADDR_SEC, MY_ADDR_SEC, 16);

			if(bUseMCNet)
			{
				conNetAddr.bUseMCNet = 1;

				sprintf(conNetAddr.MC_ADDR_PRI, "192.%d.11.5", cCTCStnNo);
				sprintf(conNetAddr.MC_ADDR_SEC, "192.%d.12.5", cCTCStnNo);

				LogInd(LOG_TYPE_GEN, "MMCR Primary IP   : %s", conNetAddr.MC_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MMCR Secondary IP : %s", conNetAddr.MC_ADDR_SEC);
			}

			if(bUseCCNet)
			{
				conNetAddr.bUseCCNet = 1;

				sprintf(conNetAddr.CC1_ADDR_PRI, "192.%d.11.6", cCTCStnNo);
				sprintf(conNetAddr.CC1_ADDR_SEC, "192.%d.12.6", cCTCStnNo);
				sprintf(conNetAddr.CC2_ADDR_PRI, "192.%d.11.7", cCTCStnNo);
				sprintf(conNetAddr.CC2_ADDR_SEC, "192.%d.12.7", cCTCStnNo);

				LogInd(LOG_TYPE_GEN, "MCCR1 Primary IP   : %s", conNetAddr.CC1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MCCR1 Secondary IP : %s", conNetAddr.CC1_ADDR_SEC);
				LogInd(LOG_TYPE_GEN, "MCCR2 Primary IP   : %s", conNetAddr.CC2_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MCCR2 Secondary IP : %s", conNetAddr.CC2_ADDR_SEC);
			}

			if(bUseMCNet || bUseCCNet)
			{
				sprintf(CONSOLE_NET_PRI, "192.%d.11.0", cCTCStnNo);
				sprintf(CONSOLE_NET_SEC, "192.%d.12.0", cCTCStnNo);

				routeNetAdd(CONSOLE_NET_PRI, MY_GW_PRI);
				routeNetAdd(CONSOLE_NET_SEC, MY_GW_SEC);

				LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", CONSOLE_NET_PRI, MY_GW_PRI);
				LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", CONSOLE_NET_SEC, MY_GW_SEC);
			}

			blkNetAddr.bIsDualNet = 1;
			blkNetAddr.bUseModem = bUseModem;
			strncpy(blkNetAddr.MY_ADDR_PRI, MY_ADDR_PRI, 16);
			strncpy(blkNetAddr.MY_ADDR_SEC, MY_ADDR_SEC, 16);

			blkNetAddr.cMyStnID	= cCTCStnNo;

			if(cUPStnNo)
			{
				blkNetAddr.bUseUPNet = 1;
				blkNetAddr.cUPStnID = cUPStnNo;

				if(g_bEthBlockLoop)
				{
					strncpy(blkNetAddr.UP_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
					strncpy(blkNetAddr.UP_CBI1_ADDR_SEC, MY_ADDR_SEC, 16);
					strncpy(blkNetAddr.UP_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					strncpy(blkNetAddr.UP_CBI2_ADDR_SEC, PEER_ADDR_SEC, 16);
				}
				else
				{
					if(bUPNewIfc)
					{
						sprintf(blkNetAddr.UP_CBI1_ADDR_PRI, "192.%d.1.1", cUPStnNo);
						sprintf(blkNetAddr.UP_CBI2_ADDR_PRI, "192.%d.1.2", cUPStnNo);

						sprintf(blkNetAddr.UP_CBI1_ADDR_SEC, "192.%d.2.1", cUPStnNo);
						sprintf(blkNetAddr.UP_CBI2_ADDR_SEC, "192.%d.2.2", cUPStnNo);

						sprintf(UP_CBI1_NET_PRI, "192.%d.1.0", cUPStnNo);
						sprintf(UP_CBI1_NET_SEC, "192.%d.2.0", cUPStnNo);
					
						routeNetAdd(UP_CBI1_NET_PRI, MY_GW_PRI);
						routeNetAdd(UP_CBI1_NET_SEC, MY_GW_SEC);

						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI1_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI1_NET_SEC, MY_GW_SEC);
						LogInd(LOG_TYPE_GEN, "UP CBI #1 A Channel IP : %s", blkNetAddr.UP_CBI1_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "UP CBI #1 B Channel IP : %s", blkNetAddr.UP_CBI2_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "UP CBI #2 A Channel IP : %s", blkNetAddr.UP_CBI1_ADDR_SEC);						
						LogInd(LOG_TYPE_GEN, "UP CBI #2 B Channel IP : %s", blkNetAddr.UP_CBI2_ADDR_SEC);
					}
					else
					{
						sprintf(blkNetAddr.UP_CBI1_ADDR_PRI, "192.%d.1.1", cUPStnNo);
						sprintf(blkNetAddr.UP_CBI1_ADDR_SEC, "192.%d.2.1", cUPStnNo);
						sprintf(blkNetAddr.UP_CBI2_ADDR_PRI, "192.%d.1.129", cUPStnNo);
						sprintf(blkNetAddr.UP_CBI2_ADDR_SEC, "192.%d.2.129", cUPStnNo);

						sprintf(UP_CBI1_NET_PRI, "192.%d.1.0", cUPStnNo);
						sprintf(UP_CBI2_NET_PRI, "192.%d.1.128", cUPStnNo);
						sprintf(UP_CBI1_NET_SEC, "192.%d.2.0", cUPStnNo);
						sprintf(UP_CBI2_NET_SEC, "192.%d.2.128", cUPStnNo);

						routeNetAdd(UP_CBI1_NET_PRI, MY_GW_PRI);
						routeNetAdd(UP_CBI2_NET_PRI, MY_GW_PRI);
						routeNetAdd(UP_CBI1_NET_SEC, MY_GW_SEC);
						routeNetAdd(UP_CBI2_NET_SEC, MY_GW_SEC);
							
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI1_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI2_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI1_NET_SEC, MY_GW_SEC);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", UP_CBI2_NET_SEC, MY_GW_SEC);
						
						LogInd(LOG_TYPE_GEN, "UP CBI #1 Primary IP   : %s", blkNetAddr.UP_CBI1_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "UP CBI #1 Secondary IP : %s", blkNetAddr.UP_CBI1_ADDR_SEC);
						LogInd(LOG_TYPE_GEN, "UP CBI #2 Primary IP   : %s", blkNetAddr.UP_CBI2_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "UP CBI #2 Secondary IP : %s", blkNetAddr.UP_CBI2_ADDR_SEC);
					}
				}
			}

			if(cDNStnNo)
			{
				blkNetAddr.bUseDNNet = 1;
				blkNetAddr.cDNStnID = cDNStnNo;

				if(g_bEthBlockLoop)
				{
					strncpy(blkNetAddr.DN_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
					strncpy(blkNetAddr.DN_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					strncpy(blkNetAddr.DN_CBI1_ADDR_SEC, MY_ADDR_SEC, 16);
					strncpy(blkNetAddr.DN_CBI2_ADDR_SEC, PEER_ADDR_SEC, 16);
				}
				else
				{
					if(bDNNewIfc)
					{
						sprintf(blkNetAddr.DN_CBI1_ADDR_PRI, "192.%d.1.1", cDNStnNo);
						sprintf(blkNetAddr.DN_CBI2_ADDR_PRI, "192.%d.1.2", cDNStnNo);

						sprintf(blkNetAddr.DN_CBI1_ADDR_SEC, "192.%d.2.1", cDNStnNo);
						sprintf(blkNetAddr.DN_CBI2_ADDR_SEC, "192.%d.2.2", cDNStnNo);

						sprintf(DN_CBI1_NET_PRI, "192.%d.1.0", cDNStnNo);
						sprintf(DN_CBI1_NET_SEC, "192.%d.2.0", cDNStnNo);
					
						routeNetAdd(DN_CBI1_NET_PRI, MY_GW_PRI);
						routeNetAdd(DN_CBI1_NET_SEC, MY_GW_SEC);

						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI1_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI1_NET_SEC, MY_GW_SEC);
						LogInd(LOG_TYPE_GEN, "DN CBI #1 A Channel IP : %s", blkNetAddr.DN_CBI1_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "DN CBI #1 B Channel IP : %s", blkNetAddr.DN_CBI2_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "DN CBI #2 A Channel IP : %s", blkNetAddr.DN_CBI1_ADDR_SEC);						
						LogInd(LOG_TYPE_GEN, "DN CBI #2 B Channel IP : %s", blkNetAddr.DN_CBI2_ADDR_SEC);
					}
					else
					{
						sprintf(blkNetAddr.DN_CBI1_ADDR_PRI, "192.%d.1.1", cDNStnNo);
						sprintf(blkNetAddr.DN_CBI1_ADDR_SEC, "192.%d.2.1", cDNStnNo);
						sprintf(blkNetAddr.DN_CBI2_ADDR_PRI, "192.%d.1.129", cDNStnNo);
						sprintf(blkNetAddr.DN_CBI2_ADDR_SEC, "192.%d.2.129", cDNStnNo);

						sprintf(DN_CBI1_NET_PRI, "192.%d.1.0", cDNStnNo);
						sprintf(DN_CBI2_NET_PRI, "192.%d.1.128", cDNStnNo);
						sprintf(DN_CBI1_NET_SEC, "192.%d.2.0", cDNStnNo);
						sprintf(DN_CBI2_NET_SEC, "192.%d.2.128", cDNStnNo);

						routeNetAdd(DN_CBI1_NET_PRI, MY_GW_PRI);
						routeNetAdd(DN_CBI2_NET_PRI, MY_GW_PRI);
						routeNetAdd(DN_CBI1_NET_SEC, MY_GW_SEC);
						routeNetAdd(DN_CBI2_NET_SEC, MY_GW_SEC);

						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI1_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI2_NET_PRI, MY_GW_PRI);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI1_NET_SEC, MY_GW_SEC);
						LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", DN_CBI2_NET_SEC, MY_GW_SEC);

						LogInd(LOG_TYPE_GEN, "DN CBI #1 Primary IP	 : %s", blkNetAddr.DN_CBI1_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "DN CBI #1 Secondary IP : %s", blkNetAddr.DN_CBI1_ADDR_SEC);
						LogInd(LOG_TYPE_GEN, "DN CBI #2 Primary IP	 : %s", blkNetAddr.DN_CBI2_ADDR_PRI);
						LogInd(LOG_TYPE_GEN, "DN CBI #2 Secondary IP : %s", blkNetAddr.DN_CBI2_ADDR_SEC);
					}
				}
			}

			if(cMDStnNo)
			{
				blkNetAddr.bUseMDNet = 1;
				blkNetAddr.cMDStnID = cMDStnNo;

				if(g_bEthBlockLoop)
				{
					strncpy(blkNetAddr.MD_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
					strncpy(blkNetAddr.MD_CBI1_ADDR_SEC, MY_ADDR_SEC, 16);
					strncpy(blkNetAddr.MD_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					strncpy(blkNetAddr.MD_CBI2_ADDR_SEC, PEER_ADDR_SEC, 16);
				}
				else
				{
					sprintf(blkNetAddr.MD_CBI1_ADDR_PRI, "192.%d.1.1", cMDStnNo);
					sprintf(blkNetAddr.MD_CBI1_ADDR_SEC, "192.%d.2.1", cMDStnNo);
					sprintf(blkNetAddr.MD_CBI2_ADDR_PRI, "192.%d.1.129", cMDStnNo);
					sprintf(blkNetAddr.MD_CBI2_ADDR_SEC, "192.%d.2.129", cMDStnNo);

					sprintf(MD_CBI1_NET_PRI, "192.%d.1.0", cMDStnNo);
					sprintf(MD_CBI2_NET_PRI, "192.%d.1.128", cMDStnNo);
					sprintf(MD_CBI1_NET_SEC, "192.%d.2.0", cMDStnNo);
					sprintf(MD_CBI2_NET_SEC, "192.%d.2.128", cMDStnNo);

					routeNetAdd(MD_CBI1_NET_PRI, MY_GW_PRI);
					routeNetAdd(MD_CBI2_NET_PRI, MY_GW_PRI);
					routeNetAdd(MD_CBI1_NET_SEC, MY_GW_SEC);
					routeNetAdd(MD_CBI2_NET_SEC, MY_GW_SEC);

					LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", MD_CBI1_NET_PRI, MY_GW_PRI);
					LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", MD_CBI2_NET_PRI, MY_GW_PRI);
					LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", MD_CBI1_NET_SEC, MY_GW_SEC);
					LogInd(LOG_TYPE_GEN, "Route Add : %s via %s", MD_CBI2_NET_SEC, MY_GW_SEC);
				}

				LogInd(LOG_TYPE_GEN, "MD CBI #1 Primary IP   : %s", blkNetAddr.MD_CBI1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MD CBI #1 Secondary IP : %s", blkNetAddr.MD_CBI1_ADDR_SEC);
				LogInd(LOG_TYPE_GEN, "MD CBI #2 Primary IP   : %s", blkNetAddr.MD_CBI2_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MD CBI #2 Secondary IP : %s", blkNetAddr.MD_CBI2_ADDR_SEC);
			}
		}
	}	
	else
	{
		if(!g_nSysNo)
		{
			strcpy(MY_ADDR_SEC, "192.168.0.1");
			strcpy(PEER_ADDR_SEC, "192.168.0.2");
		}
		else
		{
			strcpy(MY_ADDR_SEC, "192.168.0.2");
			strcpy(PEER_ADDR_SEC, "192.168.0.1");
		}

		LogInd(LOG_TYPE_GEN, "My Secondary IP   : %s", MY_ADDR_SEC);
		LogInd(LOG_TYPE_GEN, "Peer Secondary IP : %s", PEER_ADDR_SEC);

		EnetAttach(1, MY_ADDR_SEC, 0xFFFFFF00);

		if(cGPSStnNo && cGPSZoneNo)
		{
			if(!g_nSysNo)
			{
				sprintf(MY_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 1));
				sprintf(PEER_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 2));
			}
			else
			{
				sprintf(MY_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 2));
				sprintf(PEER_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 1));
			}

			LogInd(LOG_TYPE_GEN, "My Primary IP     : %s", MY_ADDR_PRI);

			ifAddrAdd("cpm0", MY_ADDR_PRI, NULL, 0xFFFFFF00);

			// IP Address Set-up for Console
			conNetAddr.bIsDualNet = 0;
			strncpy(conNetAddr.MY_ADDR_PRI, MY_ADDR_PRI, 16);

			if(bUseMCNet)
			{
				conNetAddr.bUseMCNet = 1;
				sprintf(conNetAddr.MC_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 5));
				LogInd(LOG_TYPE_GEN, "MMCR IP           : %s", conNetAddr.MC_ADDR_PRI);
			}
			
			if(bUseCCNet)
			{
				conNetAddr.bUseCCNet = 1;
				sprintf(conNetAddr.CC1_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 6));
				sprintf(conNetAddr.CC2_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cGPSStnNo * 10 + 7));
				LogInd(LOG_TYPE_GEN, "MCCR1 IP			: %s", conNetAddr.CC1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MCCR2 IP			: %s", conNetAddr.CC2_ADDR_PRI);
			}

			// IP Address Set-up for Block
			blkNetAddr.bIsDualNet = 0;
			blkNetAddr.bUseModem = bUseModem;
			strncpy(blkNetAddr.MY_ADDR_PRI, MY_ADDR_PRI, 16);
			strncpy(blkNetAddr.PEER_ADDR_PRI, PEER_ADDR_PRI, 16);

			blkNetAddr.cMyStnID	= cGPSStnNo;

			if(cUPStnNo)
			{
				blkNetAddr.bUseUPNet = 1;
				blkNetAddr.cUPStnID = cUPStnNo;

				if(g_bEthBlockLoop)
				{
					if(!g_nSysNo)
					{
						strncpy(blkNetAddr.UP_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
						strncpy(blkNetAddr.UP_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					}
					else
					{
						strncpy(blkNetAddr.UP_CBI1_ADDR_PRI, PEER_ADDR_PRI, 16);
						strncpy(blkNetAddr.UP_CBI2_ADDR_PRI, MY_ADDR_PRI, 16);
					}
				}
				else
				{
					sprintf(blkNetAddr.UP_CBI1_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cUPStnNo * 10 + 1));
					sprintf(blkNetAddr.UP_CBI2_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cUPStnNo * 10 + 2));
				}

				LogInd(LOG_TYPE_GEN, "UP CBI #1 Primary IP   : %s", blkNetAddr.UP_CBI1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "UP CBI #2 Primary IP   : %s", blkNetAddr.UP_CBI2_ADDR_PRI);
			}

			if(cDNStnNo)
			{
				blkNetAddr.bUseDNNet = 1;
				blkNetAddr.cDNStnID = cDNStnNo;

				if(g_bEthBlockLoop)
				{
					if(!g_nSysNo)
					{
						strncpy(blkNetAddr.DN_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
						strncpy(blkNetAddr.DN_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					}
					else
					{
						strncpy(blkNetAddr.DN_CBI1_ADDR_PRI, PEER_ADDR_PRI, 16);
						strncpy(blkNetAddr.DN_CBI2_ADDR_PRI, MY_ADDR_PRI, 16);
					}
				}
				else
				{
					sprintf(blkNetAddr.DN_CBI1_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cDNStnNo * 10 + 1));
					sprintf(blkNetAddr.DN_CBI2_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cDNStnNo * 10 + 2));
				}

				LogInd(LOG_TYPE_GEN, "DN CBI #1 Primary IP   : %s", blkNetAddr.DN_CBI1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "DN CBI #2 Primary IP   : %s", blkNetAddr.DN_CBI2_ADDR_PRI);
			}

			if(cMDStnNo)
			{
				blkNetAddr.bUseMDNet = 1;
				blkNetAddr.cMDStnID = cMDStnNo;

				if(g_bEthBlockLoop)
				{
					if(!g_nSysNo)
					{
						strncpy(blkNetAddr.MD_CBI1_ADDR_PRI, MY_ADDR_PRI, 16);
						strncpy(blkNetAddr.MD_CBI2_ADDR_PRI, PEER_ADDR_PRI, 16);
					}
					else
					{
						strncpy(blkNetAddr.MD_CBI1_ADDR_PRI, PEER_ADDR_PRI, 16);
						strncpy(blkNetAddr.MD_CBI2_ADDR_PRI, MY_ADDR_PRI, 16);
					}
				}
				else
				{
					sprintf(blkNetAddr.MD_CBI1_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cMDStnNo * 10 + 1));
					sprintf(blkNetAddr.MD_CBI2_ADDR_PRI, "192.6.%d.%d", cGPSZoneNo, (cMDStnNo * 10 + 2));
				}

				LogInd(LOG_TYPE_GEN, "MD CBI #1 Primary IP   : %s", blkNetAddr.MD_CBI1_ADDR_PRI);
				LogInd(LOG_TYPE_GEN, "MD CBI #2 Primary IP   : %s", blkNetAddr.MD_CBI2_ADDR_PRI);
			}
		}

		if(cGPSSvrNo)
		{
			sprintf(GPS_ADDR, "192.6.%d.%d", cGPSZoneNo, (cGPSSvrNo * 10 + 5));
			LogInd(LOG_TYPE_GEN, "GPS Server IP     : %s", GPS_ADDR);
		}
	}

// initialize Communication with both sides.
	ComIF.Initialize(nIfsize, MY_ADDR_SEC, PEER_ADDR_SEC);

// initialize COM for the LCC
	// 20140609 sjhong - 초기화 시 TrackInfo의 시작 위치와 개수를 인자로 넘겨주도록 수정.
	OprLCC.Initialize( (short)m_nScrEndPos + 16, pTable->InfoTrack.nStart, pTable->InfoTrack.nCount, &conNetAddr);

// Initialize Communication for CTC.
	if(cCTCStnNo && cCTCZoneNo)
	{
		ComCTC.Initialize(cCTCZoneNo, cCTCStnNo, MY_ADDR_PRI, MY_ADDR_SEC, CTC_ADDR_PRI, CTC_ADDR_SEC);	
	}
	else if(cGPSSvrNo)
	{
		ComGPS.Initialize(MY_ADDR_PRI, GPS_ADDR);	
	}

// initialize COM for the BLOCK.
	BYTE bReverse = m_pMyEngine->m_ConfigList.REVERSE_LAYOUT;
	BYTE bUseSAXL = m_pMyEngine->m_ConfigList.SUPER_BLOCK;	

	LogInd(LOG_TYPE_GEN, "bReverse = %d, bUseSAXL = %d", bReverse, bUseSAXL);

	BYTE LeftPorts[2] = {0,}, RightPorts[2] = {0,}, MidPorts[2] = {0,};
	m_pMyEngine->GetBlockCommPorts(LeftPorts, RightPorts, MidPorts);
	LogInd(LOG_TYPE_GEN, "LeftPorts[0] = %d, LeftPorts[1] = %d, RightPorts[0] = %d, RightPorts[1] = %d", LeftPorts[0], LeftPorts[1], RightPorts[0], RightPorts[1]);

	ModemTaskRun(bReverse, bUseSAXL, bUPNewIfc, bDNNewIfc, LeftPorts, RightPorts, MidPorts, &blkNetAddr);

// IO starting
//	if (g_ndipSwitch3 & 0x20)
//		SimCom.Initialize();
//	else
	taskSpawn("tScan", TASKPR_IO/*5*/,0,6000,(FUNCPTR)tScan,0,0,0,0,0,0,0,0,0,0);

// task watching.
	taskWatchInitialize();
//

	g_nMainRunTimeTicked = 0;
    wdStart(wid_MainRun, 30, (FUNCPTR)rSetMainRunTimeTick, 0);
	WaitOnSyncForMainRun();

	EIS_IO.InputAll();

	InAreaUpdate();

    //BootCheck();
    SystemInit( 0 );

	ledCon(DIAG0_OFF);			// active lamp OFF.

//; if (main CBI) then
	if ( ChipStatus.bBRD_OTHEROK )
	{
		g_nMainRunTimeTicked = 0;
	    wdStart(wid_MainRun, 120, (FUNCPTR)rSetMainRunTimeTick, 0);
		while (!g_nMainRunTimeTicked) {	if ( RcvDataCopy() ) break;	}
	}
	else
	{
		if ( RamReload() == 0 ) {
			SystemInit( 1 );		
//
			memcpy( &m_pMyEngine->m_pRouteQue[0], &VMEM[PTR_EMUBUFFER], m_pMyEngine->m_nRamSize );
		}

		EI_UnitStatus->bSysRun = 0;
		if ( !g_nSysNo ) 
		{	// 1 CBI
			EI_Status->bActive1 = 1;
			EI_Status->bActive2 = 0;
printf("\n>>Active1 ");
		}
		else{
			EI_Status->bActive1 = 0;
			EI_Status->bActive2 = 1;
printf("\n>>Active2 ");
		}
	}

	g_nMainRunTimeTicked = 1;

	BYTE cIPM = 0x10;	// IPM 1 is 0x10, IPM 2 is ox20.
	char cMon = '@';
	int  bMyActive=0, bRcvData=0;
/*
	if (!g_nSysNo)
	    EI_Status->bVPOR1 = 1;
	else 
	    EI_Status->bVPOR2 = 1;
*/
	while (1) 
	{
		unsigned char temp = 0;

		//VMEM[0x01] = 0x55;		// Cmd - 'D' (display)
		VMEM[OFFSET_CYCLE]++;		// Cycle

//del	    watchDogUpdate();

		WaitOnSyncForMainRun();
		
		g_nMainRunTimeTicked = 0;
	    wdStart (wid_MainRun, 250, (FUNCPTR)rSetMainRunTimeTick, 0);	//60/3	// 20->200

	    ReadClock();

//; if (error and other system is normal) then not processing.
ReBoot:
		EIS_IO.InputAll();		// All read Input card.

		InAreaUpdate();			// update Input area.

		if(m_pMyEngine->m_ConfigList.BLK_DEV_MODE)
		{
			EI_Status->bIVPOR1 = 1;
			EI_Status->bIActiveOn1 = 1;
		}
		
		m_pMyEngine->UpdateAXLStatusByConfig();

		if ( !g_nSysNo ){	// IPM 1.
			cIPM = 0x10;
			bMyActive = EI_Status->bIActiveOn1;	
if (bMyActive > 0)
int debug = 1;
		}
		else{				// IPM 2.
			cIPM = 0x20;
			bMyActive = EI_Status->bIActiveOn2;	
if (bMyActive > 0)
int debug = 1;

		}
/*
#ifndef SIMMODE
//2004.11.11
bMyActive = 1;
EI_Status->bIVPOR2 = 1;
#endif
*/
		g_Active = 0;
		if( bMyActive ) 
		{
			VMEM[0] = 0x0A;		// IPM is Main system.   -  0x0A (online)  0x05 (standby)

			if ( (g_nSysNo && EI_Status->bIVPOR2 && EI_Status->bVPOR2 && EI_Status->bIActiveOn2) || 
				 (!g_nSysNo && EI_Status->bIVPOR1 && EI_Status->bVPOR1 && EI_Status->bIActiveOn1) ) 
			{
				ledCon(DIAG0_ON);			// DIAG0 - ACT on
				g_Active = 1;
			}
			else
			{
				ledCon(DIAG0_OFF);			// DIAG0 - ACT off
			}
			unsigned char dumy[MAX_SIZE_VMEMBUFFER];
			unsigned char CompareData[20];
			unsigned char pLoginBuffer[16] = {0,};

			if (ComIF.RcvCheck( dumy, CompareData, pLoginBuffer )) 
			{
				LogDbg(LOG_TYPE_SYNC, 
						"CompareData[0] = %d, CompareData[1] = %d, pInterlockTable[0] = %d, pInterlockTable[1] = %d", CompareData[0], CompareData[1], pInterlockTable[0], pInterlockTable[1]);

				m_bOtherErrEvg = CompareData[3];
				if((CompareData[0] != pInterlockTable[0]) 
				|| (CompareData[1] != pInterlockTable[1]))
				{
					if (!g_nSysNo)
						EI_Status->bVPOR1 = 0;
					else 
						EI_Status->bVPOR2 = 0;
					bRcvData = 0;
printf("\n>>LOGICALFAIL");
				}
				else {
					if (!g_nSysNo)
						EI_Status->bVPOR1 = 1;
					else 
						EI_Status->bVPOR2 = 1;

					bRcvData = 2;
				}

			    //wdStart (g_wid_ComIF, MAX_WAITTICK_COMIF, (FUNCPTR)rClearComIFTimeTick, 0);	// wait tick starting.
				memcpy( &_xpVMEM[0], &dumy[0], SIZE_SCR_HEADER_BYTE );
				memcpy( &_xpVMEM[m_nScrEndPos], &dumy[m_nScrEndPos], 0xB0 );
				memcpy( &_xpVMEM[g_nPTR_INBUFFER], &dumy[g_nPTR_INBUFFER], g_nSIZE_INBUFFER );
				bComState.bRxImageGood = 1;

				m_nSyncFailCount = 0;
				m_bSyncFail = 0;
			}
			else
			{ 
				m_bOtherErrEvg = 0;
				bRcvData = 1;
				bComState.bRxImageGood = 0;  // 2006.2.14 Oter cpu comunication failure 

				if(m_nSyncFailCount < 0xFF)
				{
					m_nSyncFailCount++;
				}

				if(m_nSyncFailCount >= 5)
				{
					m_bSyncFail = 1;
				}
			}
		}
		else 
		{
			VMEM[0] = 0x05;				// IPM is Sub system.

			bRcvData = RcvDataCopy();
			if (bRcvData == 2)
			{
				// Secondary time compare and update  2006.02.13
				if ( (VMEM[3] != _xpVMEM[3]) || (VMEM[4] != _xpVMEM[4]) )
				{
					if (!(VMEM[3]==59 || _xpVMEM[3]==59))
					{
						BYTE pWriteMsg[8];
						memcpy (pWriteMsg, _xpVMEM, 8);
						WriteClock( pWriteMsg );
					}
				}
			}

			ledCon(DIAG0_OFF);			// DIAG0 - ACT off
		}


		
		if ( !EI_UnitStatus->bSysRun ) {
			bSystemStatus.bPhysicalGood = 0;
		}
		if ( !EI_UnitStatus->bSysFail )	
		{
		// normal	
			if ( !EI_UnitStatus->bSysRun ) 
			{
				bSystemStatus.bPhysicalGood = 1;
				m_FaultCount &= 0xE0;
			}
			bSystemStatus.bOnRun = 0;		
		}
		else 
		{
		// system failure.
			if ( bSystemStatus.bOnRun )	
			{
				m_FaultCount++;
				if ( m_FaultCount & 0x08 ) {
					bSystemStatus.bPhysicalGood = 0;
				}
			}
			m_FaultCount += 0x20;
			bSystemStatus.bOnRun = 1;
		}
//Move upper to here end...

		ChangeOver(bRcvData);			// CBI 1, CBI2 compare and change.
		InAreaUpdate();			// inbuffer -> VMEM.

		m_pMyEngine->UpdateAXLStatusByConfig();
		
// MCCR Command Processing 		
		opSetTimerCheck( m_pMsgBuffer );

		if ( m_pMyEngine->Operate( (char*)m_pMsgBuffer ) )	
		{
			short nStatus = OprLCC.ComExchange( (char*)m_pMsgBuffer, m_pLoginBuffer );  // m_pMsgBuffer -> received command buffer
			goto ReBoot;
		}

//printf("{%d%d%d%d}", EI_Status->bIActiveOn1,  EI_Status->bIActiveOn2, EI_UnitStatus->bSysRun, bMyActive );

		_nIllegalFailure = 0;
		
		BYTE *_pRightOutBlkBuf	= &_pOutBuffer[0x120 - (16 * 2)];

		//LogHex(LOG_TYPE_GEN, _pRightOutBlkBuf, 16, "---- Before Interlock ----");
//		ULONG ulStart, ulEnd, ulGap;
//		tickSet(0L);
//		ulStart = tickGet();
//
		m_pMyEngine->RunInterlock( NULL );

		//LogHex(LOG_TYPE_GEN, _pRightOutBlkBuf, 16, "---- After Interlock ----");
//		ulEnd = tickGet();
//		ulGap = ulEnd - ulStart;
//		printf(".%UL.", ulGap);

		m_bIllegalFault = ( _nIllegalFailure ) ? 1 : 0;

if ( m_bIllegalFault ) {
printf("\n>>ILLEGAL FAULT");
m_bIllegalFault = 0;
}
		//printf("bMyActive = %d   VPOR1 = %d   VPOR2 = %d\n",bMyActive, EI_Status->bVPOR1, EI_Status->bVPOR2);
		
		OutAreaUpdate(1/*!bMyActive*/);

		BYTE &SysRunState = VMEM[OFFSET_UNIT_STATUS];	// UnitState

		if(_Time1Sec == 0)
		{
			if(OprLCC.m_nFailCnt[1] != 0xFF)
			{
				OprLCC.m_nFailCnt[1]++;
			}

			if(OprLCC.m_nFailCnt[2] != 0xFF)
			{
				OprLCC.m_nFailCnt[2]++;
			}

			// MCCR Comm. Fail Counter Increased.
			LogDbg(LOG_TYPE_CC, "FailCnt[1] = %d, FailCnt[2] = %d", OprLCC.m_nFailCnt[1], OprLCC.m_nFailCnt[2]);
		}

		// Get to MCCR message.
		short nStatus = OprLCC.ComExchange( (char*)m_pMsgBuffer, m_pLoginBuffer );  // m_pMsgBuffer -> received command buffer

		LogDbg(LOG_TYPE_GEN, "MCCR Comm. Status = 0x%X", nStatus);

		// LCC com normal.
		if (nStatus)
		{
			if ( m_pMsgLastCyc == m_pMsgBuffer[0] ) 
			{
				m_pMsgBuffer[1] = 0;			// ignore command by equal cycle.
			}
			m_pMsgLastCyc = m_pMsgBuffer[0];	// store command cycle.
			m_pMyEngine->m_pMsgBuffer[8] = 0;	// if (normal communication) clear send buffer
		}

		EI_CommStatus->bIFComFail	= !bComState.bRxImageGood;	// 2 IF COM status(1/failure, 0/normal)

		g_StationClosed = (EI_Status->bStation && EI_Status->bN1);


		if(cCTCStnNo && cCTCZoneNo)
		{
			if(ComCTC.CpuCommandQue[2] != 0)
			{
				if(m_pMyEngine->ProcessCtcCommand(&ComCTC.CpuCommandQue[0]))
				{
					ComCTC.CpuCommandQue[2] = 0;
				}
			}

			// CTC 시각 정보 Update
			if(ComCTC.StdTime_Buffer[0] > 0)
			{
				WriteClockCTC(ComCTC.StdTime_Buffer);
				memset(ComCTC.StdTime_Buffer, 0, sizeof(ComCTC.StdTime_Buffer));
			}
		}

		if(cGPSSvrNo)
		{
			// CTC 시각 정보 Update
			if(ComGPS.StdTime_Buffer[0] > 0)
			{
				BYTE pCurTime[6] = {0,};
				ReadClockCTC(pCurTime);

				int nMinGap = (pCurTime[4] - ComGPS.StdTime_Buffer[4]);
				int nSecGap = (pCurTime[5] - ComGPS.StdTime_Buffer[5]);

				if(nMinGap < 0)
				{
					nMinGap = -nMinGap;
				}

				if(nSecGap < 0)
				{
					nSecGap = -nSecGap;
				}

				LogDbg(LOG_TYPE_GPS, "nMinGap = %d, nSecGap = %d", nMinGap, nSecGap);

				if(((nMinGap > 1) && (nMinGap < 59)) || ((nSecGap > 10) && (nSecGap < 50)))
				{
					LogDbg(LOG_TYPE_GPS, "WriteClock Now!!");

					WriteClockCTC(ComGPS.StdTime_Buffer);
				}

				memset(ComGPS.StdTime_Buffer, 0, sizeof(ComGPS.StdTime_Buffer));
			}
		}

		if( GetStatusOfUpBLOCK() ) 
		{
			EI_Status->bUpModem = 1;
		}
		else
		{
			if (_Time1Sec == 0) 
			{
				EI_Status->bUpModem = 0;
			}
		}

		if ( GetStatusOfDownBLOCK() ) 
		{
			EI_Status->bDnModem = 1;
		}
		else
		{
			if (_Time1Sec == 0) 
			{
				EI_Status->bDnModem = 0;

			}
		}

		if ( GetStatusOfMidBLOCK() ) 
		{
			EI_Status->bMidModem = 1;
		}
		else
		{
			if (_Time1Sec == 0) 
			{
				EI_Status->bMidModem = 0;
			}
		}
		
		if(m_pMyEngine->m_ConfigList.BLK_DEV_MODE)
		{
/*
			if(m_pMyEngine->m_ConfigList.REVERSE_LAYOUT)
			{
				if(cUPStnNo)
				{
					EI_Status->bUpModem = 1;
				}
				
				if(cDNStnNo)
				{
					EI_Status->bDnModem = 1;
				}
			}
			else
*/
			{
				if(cUPStnNo)
				{
					EI_Status->bDnModem = 1;
				}
				
				if(cDNStnNo)
				{
					EI_Status->bUpModem = 1;
				}
			}
		}
		
		// All read to input card.
		EIS_IO.InputAll();

		InAreaUpdate();

		m_pMyEngine->UpdateAXLStatusByConfig();

		if(cCTCStnNo && cCTCZoneNo)
		{
			// 2011.3.31.. make to Image of CTS element table.
			m_pMyEngine->MakeCtcElementImage( &ComCTC.DIM_Buffer[0] );
		}

		// make to Image.
		memcpy( &VMEM[g_nPTR_INBUFFER], &_pInBuffer[0], g_nSIZE_INBUFFER );
		memcpy( &VMEM[PTR_EMUBUFFER], &m_pMyEngine->m_pRouteQue[0], m_pMyEngine->m_nRamSize );

// CardErrEvg
unsigned char TxCompareData[20];
// LCC, MC, Modem, CTC
int ComFail = 0, ComState = 0;
		ComFail = EI_CommStatus->bLCCCom1Fail & EI_CommStatus->bLCCCom2Fail;
		ComState |= ComFail << 3; 
//		ComState |= MC << 2;
//		ComState |= Modem << 1;       
//		ComState |= ReceiveCtcData();

		TxCompareData[0] = pInterlockTable[0];
		TxCompareData[1] = pInterlockTable[1];
		TxCompareData[2] = ComState;
		TxCompareData[3] = m_bErrEvg;

		ComIF.TxVMEM(TxCompareData, m_pLoginBuffer);

		OutAreaUpdate(!bMyActive);

	    RamBackup();

//#		WaitOprLow();


#if 0
        printf("%1c", cMon++);
        if (cMon > 'Z') 
	    {
			printf("\n");
		    cMon = '@';
		}
#endif
	} // end of while

	wdCancel( wid_MainRun );
	wdCancel( g_wid_ComIF );

	return 0;
}	// end of Run()


//;=====================================================================
//; CBI <-> CTS Control (CTCComm)
//;=====================================================================
/*
int MainTask::ReadOtherCTCCommand() // a like CheckLoadCtcCmd() function. using Sub data table.
{
	BYTE *pCtcCmd = &_xpVMEM[ g_nCtcCmdQuePtr ];	// other(SUB) CTC Cmd buffer
	if ( pCtcCmd[1] & 0x80 ) {
		memcpy( &m_pCTCCmdBuffer[0], pCtcCmd, 8 );	// sequential number, function, para1, ... para6.
		m_pCTCCmdBuffer[0] = 0x40;
		pCtcCmd[1] = 0;

//printf("\n.Other CTC CMD.");

	}
	return 0;
}


int MainTask::CheckLoadCtcCmd() 	// Check and Receive Other Systems CTC Command
{
	BYTE *pCtcCmd = &VMEM[ g_nCtcCmdQuePtr ];
	if ( pCtcCmd[1] & 0x80 ) {
		memcpy( &m_pCTCCmdBuffer[0], pCtcCmd, 8 );	// sequential number, function, para1, ... para6.
		m_pCTCCmdBuffer[0] = 0x40;
		pCtcCmd[1] = 0;

printf("\n.CTC CMD-COPY.");
//printf("\n>>cts:%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X.",	m_pCTCCmdBuffer[0],	m_pCTCCmdBuffer[1],	m_pCTCCmdBuffer[2],	m_pCTCCmdBuffer[3],	m_pCTCCmdBuffer[4],	m_pCTCCmdBuffer[5],	m_pCTCCmdBuffer[6],	m_pCTCCmdBuffer[7] );

	}
	return 0;
}
*/

/* ------------------------------------------------------------------------ */
BYTE CheckBit_Acc_To_T0( WORD nAdd ) 
{
	nAdd &= 0x1FFF;
	BYTE nBits = 1 << ( nAdd & 7 );
	BYTE nResult = VMEM[ nAdd >> 3 ] & nBits;
	return nResult ? 1 : 0;
}

//;=====================================================================
//; System CompareCopy or Change (IFComm)
//;=====================================================================

BYTE MainTask::RcvDataCopy()
{
	unsigned char dumy[MAX_SIZE_VMEMBUFFER];
	unsigned char CompareData[20];
	unsigned char pLoginBuffer[16] = {0,};

	memcpy( dumy, VMEM, m_nScrEndPos );

	if ( ComIF.RcvCheck( dumy, CompareData, pLoginBuffer ) ) 
	{
		memcpy( &_xpVMEM[0], &dumy[0], SIZE_SCR_HEADER_BYTE );	// 24 -> 48
		memcpy( &_xpVMEM[g_nPTR_INBUFFER], &dumy[g_nPTR_INBUFFER], g_nSIZE_INBUFFER );

		m_bOtherErrEvg = CompareData[3];

		if((CompareData[0] != pInterlockTable[0]) 
		|| (CompareData[1] != pInterlockTable[1]))
		{
			if (!g_nSysNo)
	 		   EI_Status->bVPOR1 = 0;
			else 
	 		   EI_Status->bVPOR2 = 0;

			printf(">>SUB_LOGICALFAIL\n");
		}
		else
		{
			if (!g_nSysNo)
	 		   EI_Status->bVPOR1 = 1;
			else 
	 		   EI_Status->bVPOR2 = 1;

		}

// 20130228 sjhong - Online Side가 송신한 DataHeader부에 대한 백업 루팅 수정
// 20130429 sjhong - DB Version 정보가 상이할 경우 받은 데이터를 덮어쓰지 않도록 하고,
// 					 추가적으로 선로전환기 필드의 데이터에 대한 무결성을 체크하여 데이터 시프트 현상의 발생을 차단한다.
		//if(m_pMyEngine->VerifyInfoSwitchTable(VMEM, dumy) == 1)
		
		if((CompareData[0] == pInterlockTable[0]) && (CompareData[1] == pInterlockTable[1]))
		{
			DataHeaderType *pLocalHeader = (DataHeaderType*)VMEM;
			DataHeaderType *pPeerHeader = (DataHeaderType*)dumy;

			pLocalHeader->nSec		= pPeerHeader->nSec;
			pLocalHeader->nMin		= pPeerHeader->nMin;
			pLocalHeader->nHour		= pPeerHeader->nHour;
			pLocalHeader->nDate		= pPeerHeader->nDate;
			pLocalHeader->nMonth	= pPeerHeader->nMonth;
			pLocalHeader->nYear		= pPeerHeader->nYear;

			memcpy(&pLocalHeader->nSysVar, &pPeerHeader->nSysVar, 2); // SysVar의 0,1번 바이트만 복사한다.
			memcpy(&pLocalHeader->LCCStatus, &pPeerHeader->LCCStatus, sizeof(LCCStatusType));
			memcpy(pLocalHeader->Counter, pPeerHeader->Counter, sizeof(unsigned short) * 4);
			memcpy(&pLocalHeader->nSysVarExt, &pPeerHeader->nSysVarExt, sizeof(SystemStatusTypeExt));

			memcpy( &VMEM[SIZE_SCR_HEADER_BYTE], &dumy[SIZE_SCR_HEADER_BYTE], g_nPTR_INBUFFER - SIZE_SCR_HEADER_BYTE );	// Copy Objects
			memcpy( &m_pMyEngine->m_pRouteQue[0], &dumy[PTR_EMUBUFFER], m_pMyEngine->m_nRamSize );

			// 20130604 Day/Night Input 정보 누락으로 강제 설정 추가
			pLocalHeader->nSysVar.bIDayNight = pLocalHeader->nSysVar.bN2;
			// 20140414 sjhong - CTC 모드 정보 동기화를 위해 추가
			pLocalHeader->nSysVar.bMode = pPeerHeader->nSysVar.bMode;
			pLocalHeader->nSysVar.bCTCRequest = pPeerHeader->nSysVar.bCTCRequest;
/*
			printf("Set LoginInfo to MainTask Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
					pLoginBuffer[0],
					pLoginBuffer[1],
					pLoginBuffer[2],
					pLoginBuffer[3],
					pLoginBuffer[4],
					pLoginBuffer[5],
					pLoginBuffer[6],
					pLoginBuffer[7]);
*/

			// 20130605 Login 정보 동기화 copy 루틴 추가
			memcpy(m_pLoginBuffer, pLoginBuffer, LOGIN_DATA_SIZE);
		}

	    //wdStart (g_wid_ComIF, MAX_WAITTICK_COMIF, (FUNCPTR)rClearComIFTimeTick, 0);

		bComState.bRxImageGood = 1;

		m_nSyncFailCount = 0;
		m_bSyncFail = 0;

		if((!g_nSysNo && EI_Status->bVPOR2) || (g_nSysNo && EI_Status->bVPOR1))
		{
			return 2;
		}
		else
		{
			return 0;
		}
	}
	else 
	{
        m_bOtherErrEvg = 0;		
		bComState.bRxImageGood = 0;

		if(m_nSyncFailCount < 0xFF)
		{
			m_nSyncFailCount++;
		}

		if(m_nSyncFailCount >= 5)
		{
			m_bSyncFail = 1;
		}

		return 1;
	}
}


/*
void MainTask::InDataCompareCopy( short nOtherOK ) 
{
	unsigned long *pMainPtr, *pAuxPtr;
	pMainPtr = (unsigned long*)_pInBuffer;
	pAuxPtr = (unsigned long*)&_xpVMEM[g_nPTR_INBUFFER];

	BYTE *pMainIOBits;
	BYTE *pAuxIOBits;
	if ( g_nSysNo == 0 ) {		// 1계
		pMainIOBits = &VMEM[16];
		pAuxIOBits = &_xpVMEM[16 + 8];
	}
	else {
		pMainIOBits = &VMEM[16 + 8];
		pAuxIOBits = &_xpVMEM[16];
	}
	 
	int nCardNo, j;
	for ( nCardNo = 0; nCardNo < (g_nSIZE_INBUFFER + 1)/4; nCardNo++ ) {
		if ( (EIS_IO.m_pBoardInfo[ nCardNo ] & 0x100 ) == 0 ) continue;	// for INPUT only

		unsigned long &MainData = pMainPtr[ nCardNo ];
		unsigned long &AuxData = pAuxPtr[ nCardNo ];

		int nByteLoc = nCardNo / 8;
		int nBitLoc = 1 << ( nCardNo % 8 );

		unsigned long nDiffer;
		if ( nOtherOK ) nDiffer = MainData ^ AuxData;
		else nDiffer = 0L;
		unsigned char *pDifCnt = _pDifferCount + nCardNo * 32;

		unsigned long nChkBit = 1;
		for ( j = 0; j < 32; j++ ) {
			if ( nChkBit & nDiffer ) {
				if ( pDifCnt[j] < 16 ) {	 // 2002.12.18 YN 16 -> 20
					pDifCnt[j]++;
					nDiffer &= ~nChkBit;
				}
			}
			else {
				pDifCnt[j] = 0;
			}
			nChkBit <<= 1;
		}

		if ( nDiffer ) {
			if (pMainIOBits[ nByteLoc ] & nBitLoc) {	// Main = OK
				if ( pAuxIOBits[ nByteLoc ] & nBitLoc ) {	// Main = OK, Aux = OK
					BYTE *p = (BYTE*)&nDiffer;
					for ( int i = 0; i < 4; i++ ) {
						if ( p[i] ) {
							BYTE *pErr = &VMEM[ m_nScrEndPos + 8 ];
							*pErr++ = 0xff;	// 18
							*pErr++ = 0x05;	// 19
							*pErr++ = (nCardNo << 2) + i;
							*pErr++ = p[i];					// different data
							*pErr++ = ((BYTE*)pMainPtr)[i];	// Main's data
							*pErr++ = p[i];
printf("\n>>CPERR:%d,nDiffer:%8.8X.", nCardNo, nDiffer);
							break;
						}
					}
					unsigned long mask = AuxData;
					mask |= ~nDiffer;
					MainData &= mask ;		// 불일치의 경우 0으로 처리
				}
				else {
					for ( j = 0; j < 32; j++ ) {
						pDifCnt[j] = 0;
					}
				}
			}
			else if ( nOtherOK && ( pAuxIOBits[ nByteLoc ] & nBitLoc ) ) {	// Main = Ba0d, Aux = OK
				MainData = AuxData;			// 부계의 데이터를 그대로 사용
				for ( j = 0; j < 32; j++ ) {
					pDifCnt[j] = 0;
				}
			}
		}
	}
}
*/

//
void MainTask::InDataCompareCopy( short nOtherOK, int bOtherVPOR ) 
{
	if ( !nOtherOK ) {
		return;
	}
// <<E.

	int nCardNo, j;
	int nMaxCard;
	int nByteLoc;
	int nBitLoc;
	BYTE *pMainIOBits;
	BYTE *pAuxIOBits;
	unsigned long *pMainPtr, *pAuxPtr;
	unsigned long mask;
	unsigned long nChkBit;
	unsigned long nDiffer;
	unsigned char *pDifCnt = _pDifferCount + nCardNo * 32;
	
	
	pMainPtr = (unsigned long*)_pInBuffer;
	pAuxPtr  = (unsigned long*)&_xpVMEM[g_nPTR_INBUFFER];
	nMaxCard = (g_nSIZE_INBUFFER + 1) / 4;

	if ( g_nSysNo == 0 ) {		// 1계
		pMainIOBits = &VMEM[OFFSET_CARD_INFO];
		pAuxIOBits  = &_xpVMEM[OFFSET_CARD_INFO + 8];
	}
	else {
		pMainIOBits = &VMEM[OFFSET_CARD_INFO + 8];
		pAuxIOBits  = &_xpVMEM[OFFSET_CARD_INFO];
	}
	 
	nChkBit = 0;
	for ( nCardNo = 0; nCardNo < nMaxCard; nCardNo++ ) 
	{
		if ( (EIS_IO.m_pBoardInfo[ nCardNo ] & 0x100 ) == 0 ) 
			continue;	// for INPUT only

		nByteLoc = nCardNo / 8;
		nBitLoc = 1 << ( nCardNo % 8 );

		nDiffer = pMainPtr[ nCardNo ] ^ pAuxPtr[ nCardNo ];
		if ( nDiffer ) 
		{ 

			if (pMainIOBits[ nByteLoc ] & nBitLoc) 
			{		// Main = OK
				if ( pAuxIOBits[ nByteLoc ] & nBitLoc ) 
				{	// Main = OK, Aux = OK
					mask = 1;
					for ( j = 0; j < 32; j++ ) 
					{
						if ( mask & nDiffer ) {
							nChkBit++;
						}
						mask <<= 1;
					}
				}
			}
		}
	}

//printf("\n>>CPERR:nChkBit:%d.", nChkBit);
	
	if ( nChkBit >= 10 ) {
		return;		// I/O access or reading failure.
	}
	
	// 2006.2.14  Changed 1 from 0.  nCardNo 
	for ( nCardNo = 1; nCardNo < nMaxCard; nCardNo++ ) 
	{
		if ( (EIS_IO.m_pBoardInfo[ nCardNo ] & 0x100 ) == 0 ) 
			continue;	// for INPUT only

		nByteLoc = nCardNo / 8;
		nBitLoc = 1 << ( nCardNo % 8 );

		nChkBit = 1;
		nDiffer = pMainPtr[ nCardNo ] ^ pAuxPtr[ nCardNo ];
		pDifCnt = _pDifferCount + nCardNo * 32;

		for ( j = 0; j < 32; j++ ) 
		{
			if ( nChkBit & nDiffer ) {
				if ( pDifCnt[j] < 16 ) {	 // 2002.12.18 YN 16 -> 20
					pDifCnt[j]++;
					nDiffer &= ~nChkBit;
				}
			}
			else {
				pDifCnt[j] = 0;
			}
			nChkBit <<= 1;
		}

		if ( nDiffer ) 
		{
//printf("\n aa  %d, %d,  %d", nByteLoc, nBitLoc, pMainIOBits[ nByteLoc ]);
			if (pMainIOBits[ nByteLoc ] & nBitLoc) 
			{	// Main = OK

//printf("\n aaa  %d, %d,  %d", nByteLoc, nBitLoc, pAuxIOBits[ nByteLoc ]);
				if ( pAuxIOBits[ nByteLoc ] & nBitLoc ) 
				{	// Main = OK, Aux = OK
					BYTE *p = (BYTE*)&nDiffer;
					for ( int i = 0; i < 4; i++ ) 
					{
						if ( p[i] ) 
						{
							BYTE *pErr = &VMEM[ m_nScrEndPos + 8 ];
							
							*pErr++ = 0xff;	// 18
							*pErr++ = 0x05;	// 19
							*pErr++ = (nCardNo << 2) + i;
							*pErr++ = p[i];					// different data
							*pErr++ = ((BYTE*)pMainPtr)[i];	// Main's data
							*pErr++ = p[i];
//printf("\n>>CPERR:%d,nDiffer:%8.8X.", nCardNo, nDiffer);
							
							break;
						}
					}

			// 2006.3.9  Other CPU IOSCAN task error ->  VPOR down,  It must be VPOR check
			// It is processing while other VPOR is normal.		
					if (bOtherVPOR)
					{
						mask  = pAuxPtr[ nCardNo ];
						mask |= ~nDiffer;
						pMainPtr[ nCardNo ] &= mask ;		// 불일치의 경우 0으로 처리
//printf ("\n aaaa [%.8x,%.8x,%.8x]", mask,nDiffer,pMainPtr[ nCardNo ]);
					}
				}
				else 
				{
					for ( j = 0; j < 32; j++ ) 
					{
						pDifCnt[j] = 0;
					}
				}
			}
			else if ( pAuxIOBits[ nByteLoc ] & nBitLoc ) 
			{	// Main = Ba0d, Aux = OK
				pMainPtr[ nCardNo ] = pAuxPtr[ nCardNo ];			// 부계의 데이터를 그대로 사용
				for ( j = 0; j < 32; j++ ) {
					pDifCnt[j] = 0;
				}
			}
		}
	}
}
//

int MainTask::ChangeOver(BYTE bRcvData) 
{
//; If SUBSYS and Image receive good, copy images
/* 만약 주,부계 모두 PHY Fail인 경우 시스템 주,부계모두 VPOR 이 떨어진다.	    		 
	 PHY1  LOG1  PHY2  LOG2  VPOR1  VPOR2  					    		  
	  0	  1     0     1      0      0    					    		 
	 FDOM_0 시스템 출력 보드의 경우 PHY, LOG 출력이 있으므로 H/W 적으로 VPOR이 죽는다.		 
	 즉, 해당 카드에 대한 PHY Fail 의 프로그램처리 제외.				    		 
	 시스템 보드를 제외한 나머지 모드의 경우 현재 우선순위 처리를 하기 저까지 다음과 같이 처리한다.
	 계간 통신 정상시 
	 	해당계의 보드의 장애가 상대계의 보드 장애 수보다 큰경우 계절체.
	 	해당계 총보드의 절반이상이 장애인경우 이고 계간통신으로 받은 상대계의 보드의 장애수가 
	 	더 적은경우 VPOR Down 후 계절체.
	 계간 통신 장애시 
	 	입출력 보드의 총 보드 수의 절반이상이 장애인 경우 PHY_Fail로 간주 VPOR을 떨어 뜨린다.	 
 
 */
 	short int nIllegalIOFail;
	int bOtherActive, bOtherVPOR, bMyActive, bMyVPOR, bSysBRD;
	int bMaxBoardNum = 4;	//17;

	bOtherActive = bOtherVPOR = bMyActive = bMyVPOR = bSysBRD = 0;

	if ( !g_nSysNo ) 
	{	// 1계
		bOtherActive = EI_Status->bIActiveOn2;
		bOtherVPOR = EI_Status->bIVPOR2;
		bMyActive = EI_Status->bIActiveOn1;

		bMyVPOR = EI_Status->bIVPOR1;
	}
	else 
	{
		bOtherActive = EI_Status->bIActiveOn1;
		bOtherVPOR = EI_Status->bIVPOR1;
		bMyActive = EI_Status->bIActiveOn2;
		bMyVPOR = EI_Status->bIVPOR2;
	}

	m_bErrEvg = EIS_IO.MakeBoardImage();
	nIllegalIOFail = EIS_IO.IsIllegalIOFailure();

//test S>>
if ( nIllegalIOFail ) 
{
printf("\n>>IOERR[%d:%d]", (nIllegalIOFail >> 8), (nIllegalIOFail & 0x0ff));
}
//test <<E.

#ifdef    IGNORE_FAULT
	m_bErrEvg = 0;
	nIllegalIOFail = 0;
#endif // IGNORE_FAULT

	if ( (m_bErrEvg > 0) || nIllegalIOFail || m_bIllegalFault ) 
	{
// PhysicalOK
// other_recvdata		
//printf("\n>>PHY_ERR-ErrEvg:%d -%d.",m_bOtherErrEvg, m_bErrEvg);
		if((m_bErrEvg > bMaxBoardNum) || nIllegalIOFail || m_bIllegalFault) 
		{
			printf("m_bErrEvg = %d, bMaxBoardNum = %d, nIllegalIOFail = 0x%X, m_bIllegalFault = 0x%X\n",
					m_bErrEvg, bMaxBoardNum, nIllegalIOFail, m_bIllegalFault);

			if ( !g_nSysNo ) 
				EI_Status->bVPOR1 = 0;
			else
			{
				EI_Status->bVPOR2 = 0;
			}

printf("\n>>PHY_ERR:%4.4X-%4.4X.", m_bErrEvg, nIllegalIOFail);
		}

// System break fault...
		if((m_bErrEvg > m_bOtherErrEvg) || nIllegalIOFail || m_bIllegalFault) 
		{
printf ("Board error [%d, %d][%X, %X]\n", m_bErrEvg, m_bOtherErrEvg, nIllegalIOFail, m_bIllegalFault);
			EI_UnitStatus->bSysFail = 1;
// PhysicalOK END
			if ( g_nOldSysRun == 0 ) 
			{
				EI_UnitStatus->bSysRun = 0;
			}
			else 
				g_nOldSysRun = 0;
	
			EI_UnitStatus->bSubRequest = 1;
		}		
		else 
		{
			g_nOldSysRun = 1;
			EI_UnitStatus->bSysFail = 0;
			EI_UnitStatus->bSubRequest = 0;
		}
	}
	else 
	{
		if ( (bRcvData==2 && bMyActive) || (bRcvData==1 && (!bMyActive && !bOtherVPOR)) )
		{
			g_nOldSysRun = 1;
			EI_UnitStatus->bSysFail = 0;
			EI_UnitStatus->bSubRequest = 0;

			if ( !g_nSysNo ) 
				EI_Status->bVPOR1 = 1;
			else
				EI_Status->bVPOR2 = 1;

		}
		else if ( bRcvData==2 && (EI_Status->bVPOR1 || EI_Status->bVPOR2) )
		{
			EI_UnitStatus->bSysFail = 0;
		}
	}

	if (bMyActive) 
	{
// is Main
		InDataCompareCopy( (short)(bComState.bRxImageGood), bOtherVPOR );

		if ( bComState.bRxImageGood && bOtherVPOR ) 
		{
			if ( !EIx_UnitStatus->bSubRequest )
			{
				if ( EI_UnitStatus->bSysFail ) 
				{
//					TMDout3(0x6);	// DOUT2 - DOUT3 All Low Request...	
					EI_UnitStatus->bSubRequest = 1;
					if (bOtherActive) {
						if ( !g_nSysNo ){// 1 CBI
							EI_Status->bActive1 = 0;
						}
						else{
							EI_Status->bActive2 = 0;
						}

						ledCon(DIAG0_OFF);
					}
				}
			}
		}
	}
// Now, I am Sub System
	else 
	{
		if ( bComState.bRxImageGood ) 
		{
			if ( EIx_UnitStatus->bSysRun && !EI_UnitStatus->bSysFail ) {
				EI_UnitStatus->bSysRun = 1;
			}

			if ( bSystemStatus.bOnRun ) {
				WriteClock( &_xpVMEM[0] );
			}
//
			if (!EI_UnitStatus->bSysFail)	EI_UnitStatus->bSubRequest = 0;

			if ( EIx_UnitStatus->bSubRequest ) 
			{
				if ( !g_nSysNo ){// 1 CBI
					EI_Status->bActive1 = 1;
					EI_Status->bActive2 = 0;
				}
				else{
					EI_Status->bActive1 = 0;
					EI_Status->bActive2 = 1;
				}

				if ( (g_nSysNo && EI_Status->bIVPOR2 && EI_Status->bVPOR2 && EI_Status->bIActiveOn2) || 
					 (!g_nSysNo && EI_Status->bIVPOR1 && EI_Status->bVPOR1 && EI_Status->bIActiveOn1) ) 
					ledCon(DIAG0_ON);			// DIAG0 - ACT on
			}
			else if ( bOtherActive ) 
			{
				if ( !g_nSysNo ) {	// 1 CBI
					EI_Status->bActive1 = 0;
				}
				else {
					EI_Status->bActive2 = 0;
				}
				ledCon(DIAG0_OFF);
			}
			else {
//; Other not Active
				if (!bOtherActive && !EI_UnitStatus->bSysFail) 
				{
					if ( !g_nSysNo ) {	// 1 CBI
						EI_Status->bActive1 = 1;
						EI_Status->bActive2 = 0;
					}
					else{
						EI_Status->bActive1 = 0;
						EI_Status->bActive2 = 1;
					}

					if ( (g_nSysNo && EI_Status->bIVPOR2 && EI_Status->bVPOR2 && EI_Status->bIActiveOn2) || 
						 (!g_nSysNo && EI_Status->bIVPOR1 && EI_Status->bVPOR1 && EI_Status->bIActiveOn1) ) 
						ledCon(DIAG0_ON);			// DIAG0 - ACT on
				}
			}
		}
		else 
		{
// ComFail
			if ( bSystemStatus.bOnRun || !bOtherActive ) 
			{
 				if (!EI_UnitStatus->bSysFail && bMyVPOR) {
					if ( !g_nSysNo ) {	// 1 CBI
						EI_Status->bActive1 = 1;
						EI_Status->bActive2 = 0;
					}
					else {
						EI_Status->bActive1 = 0;
						EI_Status->bActive2 = 1;
					}

					if ( (g_nSysNo && EI_Status->bIVPOR2 && EI_Status->bVPOR2 && EI_Status->bIActiveOn2) || 
						 (!g_nSysNo && EI_Status->bIVPOR1 && EI_Status->bVPOR1 && EI_Status->bIActiveOn1) ) 
						ledCon(DIAG0_ON);			// DIAG0 - ACT on
				}
			}
			if ( bOtherActive && bOtherVPOR && !EI_UnitStatus->bSysFail ) {
				EI_UnitStatus->bSysRun = 1;
			}
		}
	}

	EI_Status->bSYS1 = (EI_Status->bIActiveOn1) ? 1 : 0;
	EI_Status->bSYS2 = (EI_Status->bIActiveOn2) ? 1 : 0;

	if ( !g_nSysNo ) {
	// 1 CBI
		EI_Status->bSYS1Good = (EI_UnitStatus->bSysFail == 0);
		EI_Status->bSYS2Good = (!EIx_UnitStatus->bSysFail && !m_bSyncFail && EI_Status->bIVPOR2) ? 1 : 0;

		LogDbg(LOG_TYPE_GEN, "bSYS1Good = %d, bSYS2Good = %d, bSysFail = %d, m_bSyncFail = %d, bIVPOR2 = %d",
				EI_Status->bSYS1Good, EI_Status->bSYS2Good, EIx_UnitStatus->bSysFail, m_bSyncFail, EI_Status->bIVPOR2);
		
		if ( bComState.bRxImageGood && EI_Status->bIActiveOn2 && !EIx_UnitStatus->bSubRequest ) 
		{
			EI_Status->bActive1 = 0;
			ledCon(DIAG0_OFF);
		}
		memcpy( &VMEM[OFFSET_CARD_INFO], EIS_IO.m_pBoardImage, 8 );
		memcpy( &VMEM[OFFSET_CARD_INFO + 8], &_xpVMEM[OFFSET_CARD_INFO + 8], 8 );
	}
	else {
	// 2 CBI
		EI_Status->bSYS2Good = (EI_UnitStatus->bSysFail == 0);
		EI_Status->bSYS1Good = (!EIx_UnitStatus->bSysFail && !m_bSyncFail && EI_Status->bIVPOR1) ? 1 : 0;
		
		LogDbg(LOG_TYPE_GEN, "bSYS2Good = %d, bSYS1Good = %d, bSysFail = %d, m_bSyncFail = %d, bIVPOR1 = %d",
				EI_Status->bSYS2Good, EI_Status->bSYS1Good, EIx_UnitStatus->bSysFail, m_bSyncFail, EI_Status->bIVPOR1);

		if ( bComState.bRxImageGood && EI_Status->bIActiveOn1 && !EIx_UnitStatus->bSubRequest ) 
		{
			EI_Status->bActive2 = 0;
			ledCon(DIAG0_OFF);
		}
		memcpy( &VMEM[OFFSET_CARD_INFO + 8], EIS_IO.m_pBoardImage, 8 );
		memcpy( &VMEM[OFFSET_CARD_INFO], &_xpVMEM[OFFSET_CARD_INFO], 8 );
	}

	if ( nIllegalIOFail ) 
	{
		BYTE *pErr = &VMEM[ m_nScrEndPos + 8 ];
		*pErr++ = 0xff;	// 18 cmd
		*pErr++ = 0x49;	// 19 msg
		*pErr++ = (BYTE)(nIllegalIOFail / 256);		// bits[0], illegal failure board position.
		*pErr++ = (BYTE)(nIllegalIOFail & 0x0ff);	// bits[1],  illegal failure port position
		*pErr++ = (g_nSysNo) ? 2 : 1;				// bits[2],  Error CBI No.
		*pErr++ = 0x00;	// Dumy
printf("\n>>ILLEGAL:%4.4X.", nIllegalIOFail);
	}
	else 
	{
		if ( VMEM[m_nScrEndPos + 9] == 0x49)
			VMEM[m_nScrEndPos + 9] = 0;
	}
	
	return nIllegalIOFail;
}

//;=====================================================================
//; AssignTable Connect
//;=====================================================================

/*
void MainTask::GetSysIOBitPosition(){
	WORD nLoopCount = 0;	// 2048 bit I/O
	WORD *pInfo = (WORD*)m_pIoAssTable;

	_nSysInputOffset = 0; 
	_nSysOutputOffset = 0;
	_cSysRInputBit1 = 0;
	_cSysRInputBit2 = 0;
	_cSysROutputBits = 0;				

	WORD nSysAddr = ADDR_SYSINPUT;
	while ( nLoopCount < 0x800 ) {		// DA1 -> 1101_1010_0001
		WORD nVMEMBitAddr = *pInfo++ & 0x1FFF;

		WORD nVMEMByteAddr = nVMEMBitAddr >> 3;	// DA1 -> 1_1011_0100 001
		if( nSysAddr == nVMEMByteAddr )         //1B4.0 ~ 1B4.7
		{
			WORD nAddress = nLoopCount >> 3;	// DA1 -> 1_1011_0100 001
			WORD nBitPos = nLoopCount & 7;         
			BYTE nBits = 1 << nBitPos;             // 
			WORD nVMEMBitPos = nVMEMBitAddr & 7;	// 0 ~ 7 

			if( nVMEMBitPos == 4 ) {
				_cSysRInputBit1 = nBits;
				_nSysInputOffset = nAddress;

printf("(_nSysInputOffset=%X, nBits = %X)",_nSysInputOffset,nBits);
			}
			else if( nVMEMBitPos == 5 ) _cSysRInputBit2 = nBits;
			else if( nVMEMBitPos == 6 || nVMEMBitPos == 7 ) {
				_cSysROutputBits |= nBits;			
				_nSysOutputOffset = nAddress;
printf("(_nSysOutputOffset=%X, nBits = %X)",_nSysOutputOffset,nBits);
			}
		}
		nLoopCount++;
	}
}
*/

void InAreaUpdate() 
{

	WORD nLoopCount = 0;	// 2048 bit I/O
	WORD *pInfo = (WORD*)Eis.m_pMyEngine->m_pAssignTable;

	while ( nLoopCount < 0x900 ) 
	{
		WORD nInfo = *pInfo++;
		if ( nInfo & 0x4000 ) 
		{		// 0100_0000	; if 01, IN
//; Move Bit
//; Dest = $11xx.x, Source = Acc.Temp
			WORD nBitAddress = nLoopCount >> 3;
			BYTE nBits = 1 << (nLoopCount & 7);
			BYTE bResult = _pInBuffer[ nBitAddress ] & nBits;
			if ( nInfo & 0x2000 ) {	// bit13, inverse
				bResult = !bResult;
			}
			nBits = 1 << ( nInfo & 7 );
			nBitAddress = ( nInfo & 0x1FFF ) >> 3;
			if ( bResult != 0 ) {	// set
				VMEM[ nBitAddress ] |= nBits;
			}
			else {				// reset
				VMEM[ nBitAddress ] &= ~nBits;
			}
		}
		nLoopCount++;
	}
}

void OutAreaUpdate( short nIsSub ) 
{
	nIsSub = 0;

	WORD nLoopCount = 0;	// 2048 bit I/O
	WORD *pInfo = (WORD*)Eis.m_pMyEngine->m_pAssignTable;
	
//	LogErr(LOG_TYPE_GEN, "VMEM = %p, pInfo = %p, pInfo - VMEM = %d", Eis.m_pMyEngine->m_pVMEM, pInfo, ((UINT)pInfo - (UINT)VMEM));

	while ( nLoopCount < 0x900 ) 
	{
		WORD nInfo = *pInfo++;
		if ( nInfo & 0x8000 ) 
		{		// 1000_0000	; if 10, OUT
//; Move Bit
///-----------------------------------------
			BYTE bResult = CheckBit_Acc_To_T0( nInfo & 0x1FFF );	
			WORD nBitAddress = nLoopCount >> 3;
			BYTE nBits = 1 << (nLoopCount & 7);
			if ( bResult != 0 ) {	// set
				_pOutBuffer[ nBitAddress ] |= nBits;
//				LogErr(LOG_TYPE_GEN, "nInfo = 0x%X, nLoopCount = %d, nBitAddress = 0x%0X, nBits = 0x%0X\n", nInfo, nLoopCount, nBitAddress, nBits);
			}
			else {				// reset
				_pOutBuffer[ nBitAddress ] &= ~nBits;
			}
		}
		nLoopCount++;
	}

    
	//_pOutBuffer[0] = 1;
	//_pOutBuffer[0x2c] = 1;
    
	EIS_IO.OutputAll( nIsSub );
}

//;=====================================================================
//; MainTask Start
//;=====================================================================

void tickTest(unsigned int n)
{
	unsigned int nLoop;
	ULONG ulStart, ulEnd, ulGap;

	for(nLoop=0; nLoop<n; nLoop++) 
	{
		tickSet(0L);
		ulStart = tickGet();
		taskDelay(5);
		ulEnd = tickGet();
		ulGap = ulEnd - ulStart;
		printf(".%UL.", ulGap);
	}
}

void tMainTask() 
{
	//printf( "\n\n\n LGEIS520-III Version %s, create at %s %s\n\n", _pEmuVersion, _pEmuDate, _pEmuTime ); 
	Eis.Run();
}

void tFuncTestTask() 
{
	menu_main();
}

void ifm() 
{
	uchar temp;

	temp = *(uchar *)CPU_DIPSW_ADDR;
	if(temp & 0x40)		// dipswitch 2 번 ON
	{//cpu comm보드 test mode  
		taskSpawn("tFuncTest",130,0,2000,(FUNCPTR)tFuncTestTask,0,0,0,0,0,0,0,0,0,0);
	}
	else	
	{
		// LOG Task Initialize & Start
		myLogInit();

		sysClkRateSet( MAX_TASKTICK_BASE );
	
		kernelTimeSlice( 3 );	// priority 가 같은 task에 대한 task switch 하기전의 task 수행시간 설정.
	
	    taskSpawn("tMainTask", TASKPR_MAIN,0,0x8000,(FUNCPTR)tMainTask,0,0,0,0,0,0,0,0,0,0);
	
	#ifdef	ORIGIN
	// initialize function for kvme308 driver.
		i_Start( 1 );
	
		kernelTimeSlice( 2 );	// 
	
	// run task
	    taskSpawn("tMainTask", TASKPR_MAIN,0,0x8000,(FUNCPTR)tMainTask,0,0,0,0,0,0,0,0,0,0);
	//    taskSpawn("tMainTask",50,0,0x8000,(void *)tMainTask,0,0,0,0,0,0,0,0,0,0);
	#endif	/*	ORIGIN	*/
	}
}

void openStation()
{
	EI_Status->bStation = 0;
	EI_Status->bN1 = 0;
}

int resetCounter(char *szType)
{
	DataHeaderType *pHeader = (DataHeaderType*)VMEM;

	if((strcmp(szType, "callon") == 0)
	|| (strcmp(szType, "CALLON") == 0))
	{
		pHeader->Counter[0] = 9999;

		printf("CALLON COUNTER RESET. VALUE = %d\n\n", pHeader->Counter[0]);

		return 1;
	}
	else if((strcmp(szType, "err") == 0)
		 || (strcmp(szType, "ERR") == 0))
	{
		pHeader->Counter[1] = 9999;

		printf("EMERGENCY ROUTE RELEASE COUNTER RESET. VALUE = %d\n\n", pHeader->Counter[1]);

		return 1;
	}
	else if((strcmp(szType, "b2axl") == 0)
		 || (strcmp(szType, "B2AXL") == 0))
	{
		pHeader->Counter[2] = 9999;

		printf("BLOCK2 AXL RESET COUNTER RESET. VALUE = %d\n\n", pHeader->Counter[2]);

		return 1;
	}
	else if((strcmp(szType, "b4axl") == 0)
		 || (strcmp(szType, "B4AXL") == 0))
	{
		pHeader->Counter[3] = 9999;

		printf("BLOCK4 AXL RESET COUNTER RESET. VALUE = %d\n\n", pHeader->Counter[3]);

		return 1;
	}
	else if((strcmp(szType, "all") == 0)
		 || (strcmp(szType, "ALL") == 0))
	{
		pHeader->Counter[0] = 9999;
		pHeader->Counter[1] = 9999;
		pHeader->Counter[2] = 9999;
		pHeader->Counter[3] = 9999;

		printf("CALLON COUNTER VALUE = %d\n", pHeader->Counter[0]);
		printf("EMERGENCY ROUTE RELEASE COUNTER VALUE = %d\n", pHeader->Counter[1]);
		printf("BLOCK2 AXLRESET COUNTER VALUE = %d\n", pHeader->Counter[2]);
		printf("BLOCK4 AXLRESET COUNTER VALUE = %d\n\n", pHeader->Counter[3]);

		return 1;
	}
	else
	{
		printf("INVALID COMMAND.\n\n");
	}

	return 0;
}

void setclk(BYTE YY, BYTE MM, BYTE DD, BYTE hh, BYTE mm, BYTE ss)
{
	BYTE pMsg[8] = {0,};

	pMsg[2] = ss;
   	pMsg[3]	= mm;
	pMsg[4] = hh;
	pMsg[5] = DD;
	pMsg[6] = MM;
	pMsg[7] = YY;

	WriteClock(pMsg);
}

void getclk()
{
	BYTE pMsg[8] = {0,};
	struct tm tm;

	sysRtcGet(&tm);

	pMsg[2]	= APP_BIN2BCD(tm.tm_sec);		// sec
	pMsg[3] = APP_BIN2BCD(tm.tm_min); 		// min
	pMsg[4] = APP_BIN2BCD(tm.tm_hour);		// hour
	pMsg[5] = APP_BIN2BCD(tm.tm_mday);		// date
	pMsg[6] = APP_BIN2BCD(tm.tm_mon + 1);	// month
	pMsg[7] = APP_BIN2BCD(tm.tm_year - 100);// year

	printf("[%x][%x][%x][%x][%x][%x]\n",pMsg[2],pMsg[3],pMsg[4],pMsg[5],pMsg[6],pMsg[7]);
}
