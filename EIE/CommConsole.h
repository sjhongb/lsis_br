/* -------------------------------------------------------------------------+
|  CommConsole.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.J.HONG                                                              
|                                                                           
|  2. FileName.                                                             
|     CommConsole.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for CC1, CC2, MC.
|                                                                           
|  4. Project Name.                                                         
|     Bangladesh Project - Tongi, Laksam, 13 stations.                        
|                                                                           
|  5. Maker.                                                                
|                                                                                                    
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402A and KVME314                               
|      - Target O.S		- VxWorks 5.4                                                         
|                                                                                             
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   -------------------------------------------------------------------------------------------   
|	 1.0    20130201	Seokju Hong		Written new source codes for BR that based on SMG7
|                                                                           
+------------------------------------------------------------------------- */ 

#ifndef _COMMCC_H
#define _COMMCC_H

#include "projdef.h"
#include "CommDrv.h"
#include "CommEth.h"

#define	TYPE_CC1		0x01
#define	TYPE_CC2		0x02
#define	TYPE_MC			0x04
#define TYPE_ETH		0x80
#define TYPE_ETH_CC1	0xA0
#define TYPE_ETH_CC2	0xB0
#define TYPE_ETH_CC3	0xC0
#define TYPE_ETH_CC4	0xD0
#define TYPE_ETH_CC5	0xE0
#define TYPE_ETH_CC6	0xF0

#define	TYPE_ALL		0xFF

#define STX				0x7E
#define SEQ_BASE		0x40
#define DISP_CC1		0x1D
#define DISP_CC2		0x2D
#define DISP_MC			0x3D
#define DISP_ETH_CC1	0x1F
#define DISP_ETH_CC2	0x2F
#define DISP_ETH_CC3	0x3F
#define DISP_ETH_CC4	0x4F
#define DISP_ETH_CC5	0x5F
#define DISP_ETH_CC6	0x6F

#define IP_PRI	"192.168.200.100"
#define IP_SEC	"192.168.200.200"
#define IP_CC1	"192.168.200.10"
#define IP_CC2	"192.168.200.20"
#define IP_CC3	"192.168.200.30"
#define IP_CC4	"192.168.200.40"
#define IP_CC5	"192.168.200.50"
#define IP_CC6	"192.168.200.60"

#define OPMSG_HEAD_SIZE		3
#define BASESCAN_HEAD_SIZE	2
#define CRC_SIZE			2
#define OPCODE_MSG_SIZE		OPMSG_HEAD_SIZE + 14
#define USERID_SIZE			8
#define PASSWD_SIZE			8
#define	LOGIN_INFO_SIZE		USERID_SIZE + PASSWD_SIZE						// 16
#define	EXTEND_OPCODE_SIZE	1
#define EXTEND_OPMSG_SIZE	EXTEND_OPCODE_SIZE + LOGIN_INFO_SIZE			// 17	
#define EXTEND_MSG_SIZE		LOGIN_INFO_SIZE + EXTEND_OPMSG_SIZE				// 33
#define RECV_MSG_SIZE		OPCODE_MSG_SIZE + EXTEND_MSG_SIZE + CRC_SIZE	// 52

#define COMM_TIMEOUT_CYCLE	5

/* ------------------------------------------------------------------------- */
class Comm_CC 
{
public:
	CommDriver 	m_ComCC1;
	CommDriver 	m_ComCC2;
	CommDriver 	m_ComMC;
	CommEth		m_ComEth1;
	CommEth		m_ComEth2;
	CommEth		m_ComEth3;
	CommEth		m_ComEth4;
	CommEth		m_ComEth5;
	CommEth		m_ComEth6;
	CommEth		m_ComEthMC1;
	CommEth		m_ComEthMC2;

	BYTE m_bUseMCNet;
	BYTE m_bUseCCNet;
	BYTE m_bIsDualNet;

	CONSOLE_NET_ADDR	m_NetAddr;

	short m_nStatus;	// CC COM status(0 or 1 or 2 is normal, 3/ all port failure).
	
	short m_nTxBlockSize;    
	WORD  m_nTrackStart;
	WORD  m_nTrackCount;

	// Data Exchange Buffer.
	unsigned char m_pTxBuffer[MAX_COMBUFFER_SIZE];	// 3 KByte
	unsigned char m_pRxBuffer[MAX_COMBUFFER_SIZE];	// 3 KByte

	unsigned char m_pLoginBuffer[LOGIN_INFO_SIZE];
	BYTE	m_bIsLogin;

	// Temporary Buffer for Receive Data.
	BYTE	m_nRXPOS_CC1;
	BYTE	m_nRXPOS_CC2;
	BYTE	m_nRXPOS_MC;

	BYTE	m_nRXSEQ_CC1;
	BYTE	m_nRXSEQ_CC2;
	BYTE	m_nRXSEQ_MC;

	BYTE 	m_pRxBuffer_CC1[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_CC2[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_CC3[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_CC4[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_CC5[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_CC6[MAX_COMBUFFER_SIZE];
	BYTE 	m_pRxBuffer_MC[MAX_COMBUFFER_SIZE];
	
	BYTE	m_nFailCnt[3];
	BYTE	m_nOnlineSide;

	MSG_Q_ID	m_nRxQID;

public:
	Comm_CC();
	~Comm_CC();
	
	void SetStatus(short nStatus)
	{
		m_nStatus = nStatus;
	}
	short GetStatus()
	{
		return m_nStatus;
	}

	USHORT 	ComExchange(char *pBuffer, unsigned char *pLoginBuffer);
	USHORT 	SendData(BYTE ch, BYTE *pTxData, USHORT nTxLen);
	USHORT	ReceiveData(BYTE ch, BYTE *pRxData);
	USHORT	ProcessData(BYTE ch, BYTE *pRxData, USHORT nRxLen);

	short Initialize(short nTxSize, WORD nTrackStart, WORD nTrackCount, CONSOLE_NET_ADDR *pNetAddr);

	char *GetChannelName(BYTE ch);
	char GetLogType(BYTE ch);
};

/* ------------------------------------------------------------------------- */
extern Comm_CC OprLCC;

/* ------------------------------------------------------------------------- */

#endif // _COMMCC_H
