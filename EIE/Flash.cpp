
#include "vxWorks.h"

#include "Flash.h"
#include <string.h>

// 1 Block = 128 KBYTE
// 1 M = FA00 0000 - FA0F FFFF

//device = E28F320J5

CFlash::CFlash() {
	m_lBaseAddress = 0xfa000000;
	char *p = (char*)m_lBaseAddress + 0x90;
	if ( strncmp( p, "EIP2", 4 ) ) {
		m_lBaseAddress = 0xfa000000;
	}
	m_nReceived = 0;
}

int CFlash::Read() {
	memcpy( m_pData, (void*)m_lBaseAddress, sizeof( m_pData ) );
}

//#define	FLASH_BASE	0xfa000000
#define FLASH_SIZE	0x400000
/****************************************************************
*					Register Status Control						*
****************************************************************/
void	clear_status(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(MyFlash.m_lBaseAddress + selete * 0x100000);

    *flash=0x0050;
}

void	reset_flash(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(MyFlash.m_lBaseAddress + selete * 0x100000);

    *flash=0x00ff;
}

int CFlash::Write() {		// flash_buff_write

	INT32 addr = m_lBaseAddress;
	UINT16 *buf_pt = &m_pData[0];
	UINT32 buf_cnt = 0x8000;

	UINT32 i;
	UINT16 barf;
	
	if((addr + buf_cnt * 2) > (m_lBaseAddress + FLASH_SIZE)){
		printf("flash_buf_write  : Write Size Error %d OverRun\n",
		       (addr + buf_cnt * 2) - (m_lBaseAddress + FLASH_SIZE));
		
		return(ERROR);
	}
	
	for(i = 0; i < buf_cnt; i++){
	    *(UINT16 *)(addr + i * sizeof(UINT16)) = 0x0040;

        *(UINT16 *)(addr + i * sizeof(UINT16)) = *(buf_pt++);
        
        while(!(*(UINT16 *)addr & 0x0080))    	flash_delay(1);
    
	    barf = 0;
	    
	    if(*(UINT16 *)addr & 0x003A)			barf = *(UINT16 *)addr & 0x003A;
		
		if(barf){
			printf("\nFlash write error at address %lx\n", (unsigned long)addr);
				
			if(barf & 0x0002)			printf("Block locked, not erased.\n");
			if(barf & 0x0010)			printf("Programming error.\n");
			if(barf & 0x0008)			printf("Vpp Low error.\n");
			
			clear_status(0);
			
			reset_flash(0);
			
			return(ERROR);
		}

        if((i < 1000) && (i == (buf_cnt - 1)))
           	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
        else if(i % 1000 == 999)
        	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
        else if(i == (buf_cnt - 1))
        	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
    }
	printf("\n");
	
	printf("Flash Write OK. \n");
	printf("Start Addr = 0x%x, End Addr = 0x%x, Total Size = %d Byte\n", addr, addr + buf_cnt * sizeof(UINT16) - 1, buf_cnt * 2);
	
	/* 반드시 Read Array Command를 전송한 후, Read 해야 한다. */
	/* 그렇지 않으면 내부 Data가 제대로 보이지 않는다.         */
	/* 2002.07.03 BY RYOJIN                                  */
	reset_flash(0);

	m_nReceived = 1;
		
	return(OK);
}

int CFlash::BlockErase( int nBlock ) {
    WORD *pAddress = (WORD*)m_lBaseAddress;
    *pAddress = 0x0020;
    taskDelay(10);
    
    *pAddress = 0x00d0;
	int nWait = 0;
	WORD nStatus;
    do {		/* delay 3 sec */
		nStatus = *pAddress;		/* status */
		if( nStatus & 0x80 ){
			*pAddress = 0x50;	/* status clear */
			*pAddress = 0xFF;
			printf("\nFlash erase block Number %d OK\r", nBlock);
			return(OK);
		}
		taskDelay( 1 );
    }
	while ( ++nWait < 100000 );
	printf("Flash erase timeout: blk#=%d\n", nBlock);
	return( ERROR );
}

CFlash MyFlash;

