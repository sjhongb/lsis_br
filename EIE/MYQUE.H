#ifndef _CBYTEQUE_H
#define _CBYTEQUE_H

class CByteQue {

public:
	unsigned char m_pBuffer[256];

	unsigned char m_nQueInPtr;
	unsigned char m_nQueOutPtr;

	CByteQue() {
		m_nQueInPtr = 0;
		m_nQueOutPtr = 0;
	}
	short GetQue( unsigned char &data );
	short LetQue( unsigned char data );
};

extern CByteQue DimQue[8];

#endif
