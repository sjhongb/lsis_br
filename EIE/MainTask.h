/* MainTask.h
 *
 */
 
#ifndef _MAINTASK_H
#define _MAINTASK_H

/* ------------------------------------------------------------------------- */
//#define  IGNORE_FAULT


/* ------------------------------------------------------------------------- */
#define BYTE unsigned char
class CEipEmu;

/* ------------------------------------------------------------------------- */

class MainTask {
public:
	int  m_nScrEndPos;

	struct {
		BYTE bOnRun          : 1; //0 0
		BYTE bLogicalFail    : 1; //1 0
		BYTE bIoUpdate       : 1; //2 0
		BYTE bPhysicalGood   : 1; //3 1
		BYTE bBackupPage     : 1; //4 0
		BYTE bBusLocked	     : 1; //5 
		BYTE bTrackGood      : 1; //6 0
		BYTE bOtherOK        : 1; //7 0
		BYTE bSysNo	         : 1; //8 0		;0-1��, 1-2��
	} bSystemStatus;

	struct {
		BYTE bOnTxData	    : 1;
		BYTE bOnRxData      : 1;
		BYTE bRxDataReady   : 1;
		BYTE bTxBufferReady : 1;
		BYTE bOnDownload    : 1;
		BYTE bRxImageGood   : 1;
		BYTE bRxAnswer      : 1;
		BYTE bCRCERROR	    : 1;
	} bComState;

	struct {
		BYTE bBRD_RESERVED	: 1;
		BYTE bBRD_OTHEROK       : 1; //= ChipStatus.6
		BYTE bBRD_ID0           : 1; //= ChipStatus.5
		BYTE bBRD_OPRPHASE      : 1; //= ChipStatus.4
		BYTE bBRD_MAINSW        : 1; //= ChipStatus.3      ; 0 - MAIN, 1 - SUB
		BYTE bBRD_P2            : 1; //= ChipStatus.2
		BYTE bBRD_P1            : 1; //= ChipStatus.1
		BYTE bBRD_P0            : 1; //= ChipStatus.0
	} ChipStatus;

	CEipEmu *m_pMyEngine;

	int  m_bIllegalFault;
	int  m_bErrEvg, m_bOtherErrEvg;
//	BYTE *m_pCardBits;
	BYTE m_FaultCount;
	BYTE m_pMsgLastCyc;

	BYTE m_pCTCCmdBuffer[16];
	BYTE m_pMsgBuffer[16];
	BYTE m_pLoginBuffer[16];

	BYTE m_nSyncFailCount;
	BYTE m_bSyncFail;

public:
	MainTask();
	~MainTask();
	
	int  Run();
	int  SystemInit( int bRamRoad );
	int  BootCheck();
	int	 RotarySWCheck();
	int  ChangeOver(BYTE bRcvData);
	BYTE  RcvDataCopy();
	void InDataCompareCopy( short nOtherOK, int bOtherVPOR );
	int  CheckLoadCtcCmd();		// CTC Cmd -> Operate Que
	int  ReadOtherCTCCommand();	// Sub CTC Cmd -> Operate Que
	void GetSysIOBitPosition();
//	void WriteClock( BYTE *pMsg );
	short DigitalIO();
};

/* ------------------------------------------------------------------------- */
extern MainTask Eis;

/* ------------------------------------------------------------------------- */
#endif // _MAINTASK_H
