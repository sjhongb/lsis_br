#define _KVME402_

#ifdef _KVME080_
#define VME_BASE_ADRS 0x82000000
#endif

#ifdef _KVME402_
#define VME_BASE_ADRS 0xf0000000
#endif

#ifdef _KVME043_
#define VME_BASE_ADRS 0xf0900000
#endif

#ifdef _MVME5100_
#define VME_BASE_ADRS 0xfa900000
#endif

#define CHAN_OFFSET			0x10000
#define RW_OFFSET 			0x8000
#define SEM_OFFSET			0x7ffc
#define LENGTH_OFFSET		0x7ffe

#define TRX_DATA_OFFSET		0x600

#define OPEN_FLAG_OFFSET	0
#define CLOSE_FLAG_OFFSET	0x10
#define IOCTL_FLAG_OFFSET   0x20
#define IOCTL_IOCMD_OFFSET	0x30
#define IOCTL_ARG_OFFSET	0x40


#define OPEN_CMD			0x8000
#define CLOSE_CMD			0x4000
#define WRITE_CMD			0x2000
#define READ_CMD			0x1000
#define IOCTL_CMD			0x0800

#define SEM_EMPTY			0x0000
#define SEM_FULL			0xffff

#define OPEN_RETURN_VALUE 	0x4f50
#define CLOSE_RETURN_VALUE  0x434c
#define IOCTL_RETURN_VALUE  0x696f

#define INSTALL_SET_VALUE	0x5355 /*SU*/

/* Modified by gpande (2011/7/11 - 16:41:47) (Start) */
/* LKV314에 적용하기 위해 channel number를 줄임 */
#if 0
#define NUM_OF_VMESERIAL_CHAN	16
#else
#define NUM_OF_VMESERIAL_CHAN	14
#endif
/* Modified by gpande (2011/7/11 - 16:41:51) (End) */

#define FIO_SET_TIMEDELAY				0xF00		/*	Set Internal Time Delay					*/
#define FIO_UNSET_TIMEDELAY				0xF01		/*	Unset Set Internal Time Delay			*/
#define FIO_SET_TX_TIMEOUT				0xF02		/*	Set Tx Time Out							*/
#define FIO_SET_RX_TIMEOUT				0xF03		/*	Set Rx Time Out							*/
#define	FIO_485_TIMEDELAY				0xF04

/*			Special ftn		      			*/
#define FIO_SET_SLEEP					0xF10		/*	Set   Sleep								*/
#define FIO_UNSET_SLEEP					0xF11		/*	UnSet Sleep								*/
#define FIO_SET_INFRARED_CTR			0xF12		/*	Set   Infrared Mode						*/
#define FIO_UNSET_INFRARED_CTR			0xF13		/*	UnSet Infrared Mode						*/
#define FIO_SET_LOOPBACK				0xF14		/*	Set   LoopBack Mode						*/
#define FIO_UNSET_LOOPBACK				0xF15		/*	UnSet LoopBack Mode						*/

/*			General ftn		      			*/
#define FIO_SET_BAUDRATE				0xF20		/*	Set Channel BaudRate					*/
#define FIO_GET_BAUDRATE				0xF21		/*	Get Channel Current BaudRate			*/
#define FIO_R_FLUSH						0xF22		/*	Set Read Channel Flush					*/
#define FIO_W_FLUSH						0xF23		/*	Set Write Channel Flush					*/
#define FIO_CHAN_RESET					0xF24		/*	Set Channel Reset						*/
#define FIO_GET_DREV					0xF25		/*	Set Device Revisiont					*/
#define FIO_GET_DVID					0xF26		/*	Set Device ID							*/
#define FIO_SET_DATA_LEN				0xF27		/*	Set Data Length							*/
#define FIO_SET_STOP_BIT				0xF28		/*	Set Data Stop Bit						*/
#define FIO_SET_PARITY					0xF29		/*	Set Data Parity							*/

/*			Set Flow Control				*/
#define FIO_SET_RTS_ON					0xF30		/*	Set Channsel RTS On						*/
#define FIO_SET_RTS_OFF					0xF31		/*	Set Channsel RTS Off					*/
#define FIO_SET_DTR_ON					0xF32		/*	Set Channsel DTR On						*/
#define FIO_SET_DTR_OFF					0xF33		/*	Set Channsel DTR Off					*/
#define FIO_GET_CTS_STATUS				0xF34		/*	Get Channsel CTS Status ( On/Off )		*/
#define FIO_GET_DSR_STATUS				0xF35		/*	Get Channsel DSR Status ( On/Off )		*/
#define FIO_GET_CD_STATUS				0xF36		/*	Get Channsel DCD Status ( On/Off )		*/
#define FIO_RTSCTSCTL					0xF37		/*	Set RTS On Auto Enable at Data Write	*/

/*			Auto Control ftn					*/
#define FIO_SET_SW_FLOW_CTR				0xF41		/*	Set   Channsel Software Flow control	*/
#define FIO_SET_AUTO_RS485CTR			0xF42		/*	Set   Channsel Auto RS485 Control		*/
#define FIO_UNSET_AUTO_RS485CTR			0xF43		/*	UnSet Channsel Auto RS485 Control		*/
#define FIO_SET_AUTO_RTSDTR				0xF44		/*	Set   Channsel Auto RTS/DTR Control		*/
#define FIO_UNSET_AUTO_RTSDTR			0xF45		/*	UnSet Channsel Auto RTS/DTR Control		*/
#define FIO_SET_AUTO_CTSDSR				0xF46		/*	Set   Channsel Auto CTS/DSR Control		*/
#define FIO_UNSET_AUTO_CTSDSR			0xF47		/*	UnSet Channsel Auto CTS/DSR Control		*/

/*			Err Check ftn					*/
#define FIO_RX_OVER_RUN_ERR				0xF51		/*	Get Channsel Rx Over Run Error Number	*/
#define FIO_RX_PARITY_ERR				0xF52		/*	Get Channsel Rx Parity Error Number		*/
#define FIO_RX_FRAMING_ERR				0xF53		/*	Get Channsel Rx Framing Error Number	*/
#define FIO_RX_BREAK_ERR				0xF54		/*	Get Channsel Rx Break Error Number		*/
#define FIO_RX_FIFO_ERR					0xF55		/*	Get Channsel Rx Fifo Error Number		*/

/* Created by gpande (2011/7/12 - 17:53:19) (Start) */
#define IOCTRL_308TXQ_STATUS			0x048
#define ISIO_F_SETCFG   				0x0103    /* Set channel I/O configuration */
#define FIO_CHANNEL_RX_SELECT			0xF70		/* RX Select */
#define FIO_CHANNEL_TX_SELECT			0xF71		/* TX Select */

#define FIO_CHANNEL_SELECT_CHECK_MAXVAL		100		/* 초 단위 */
#define FIO_CHANNEL_SELECT_DEF_DELAY_VAL	1000	/* millisecond 단위 */
/* Created by gpande (2011/7/12 - 17:53:21) (End) */

/**********************************************
*			Line Control Register			  *
**********************************************/
#define	CHAR_LEN_CLEAR		0xFC
#define CHAR_LEN_5			0x00
#define CHAR_LEN_6			0x01
#define CHAR_LEN_7			0x02
#define CHAR_LEN_8			0x03

#define	CHAR_STOP_BIT_CLEAR	0xFB
#define STOP_BIT_1			0x00
#define STOP_BIT_2			0x04

#define	CHAR_PARITY_CLEAR	0xC7
#define SET_NO_PARITY		0x00
#define SET_ODD_PARITY		0x08
#define SET_EVEN_PARITY		0x18



/****************************************************************************/
/*                   VME Serial Channel Control Structure     			*/
/****************************************************************************/


typedef struct
{
	DEV_HDR devHdr;						/* vxWorks I/O device header		*/
	
	int num_chan;						/* Channel number in the UART		*/
	int baudRate;						/* Baud rate for this channel		*/
	int dataBits;						/* Data Bits for this channel		*/
	int stopBits;						/* Stop Bits for this channel		*/
	int parity;							/* Parity for this channel			*/

	int chanOpen;						/* TRUE if device is currently open	*/

	int rxErrFlag;
	int rxOverRunErr;
	int rxParityErr;
	int rxFramingErr;
	int rxBreak;
	int rxFifoError;

	int TxTimeOut;
	int RxTimeOut;
#if 0
	SEM_ID binSemWr;				/* Binary semaphore for writing             */
	SEM_ID binSemRd;				/* Binary semaphore for reading             */

	SEM_ID mutexSemWr;				/* Mutual exclusion semaphore for writing   */
	SEM_ID mutexSemRd;				/* Mutual exclusion semaphore for reading   */
#endif	
} VME_DRV_CHAN;

static struct
{
	VME_DRV_CHAN	*channel;
} VME_DRV_DEV;

/* Created by gpande (2011/7/11 - 16:36:50) (Start) */
#define SERIALMASTER_SIO_DRIVER_NAME	"/isio/"
/* Created by gpande (2011/7/11 - 16:36:52) (End) */
