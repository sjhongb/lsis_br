/*
modification history
--------------------
01a,08Dec99,khs  changed i_VMEinitLvl : 4,
*/

#include "vxWorks.h"

#include "vme.h"
#include "sysLib.h"
#include "stdio.h"
#include "stdioLib.h"
#include "stdlib.h"
#include "iosLib.h"
#include "errno.h"
#include "is.h"

IMPORT	resetBoard (UINT32 );

/* ISIO interrupt configuration: */
int  i_VMEintLvl = 4;				/* VME interrupt level(s) */
int  i_intVecBase	  = 0x73;


/******************************************************************
			ISIO default I/O configuration.

	This is optional - the ISIO driver also provides a default I/O 
	configuration.

	Furthermore, the I/O configuration can be set on a per-channel 
	basis, by calling ioctl(fd, ISIO_F_SETCFG, &ioConfig)
******************************************************************/
LOCAL ISIO_IO_CONFIG	i_ioConfig[8] =
{
#ifdef	ALL_HW_HANDSHAKE
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT,	NO_PARITY, BAUD_9600	},		/* port #0 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #1 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #2 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #3 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #4 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #5 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #6 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #7 */
#endif

#ifdef	ALL_SW_HANDSHAKE
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT,	NO_PARITY, BAUD_9600	},		/* port #0 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #1 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #2 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #3 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #4 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #5 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #6 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #7 */
#endif

#ifdef	USER_DEFINE_HANDSHAKE
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT,	NO_PARITY, BAUD_9600	},		/* port #0 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #1 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #2 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #3 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #4 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #5 */
	{	HW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #6 */
	{	SW_HANDSHAKE,	EIGHT_CHAR_BITS, ONE_STOP_BIT, NO_PARITY, BAUD_9600	},		/* port #7 */
#endif
};


/******************************************************************
			ISIO buffer configuration.

   This is optional - the ISIO driver also provides a default buffer
   configuration.

   Note that the receive buffer for the channel 0 must always be 2 
   kBytes. The sum of all buffer sizes must equal 94 kBytes.

   Once an ISIO board has been installed by a call to isioBrd, there 
   is no way of changing the buffer configuration for that board.
*******************************************************************/
LOCAL ISIO_BUF_CONFIG	i_bufConfig =
{
 /* ch0 - rx/tx */  1, 1,
 /* ch1 - rx/tx */  1, 1,
 /* ch2 - rx/tx */  1, 1,
 /* ch3 - rx/tx */  1, 1,
 /* ch4 - rx/tx */  1, 1,
 /* ch5 - rx/tx */  1, 1,
 /* ch6 - rx/tx */  1, 1,
 /* ch7 - rx/tx */  1, 1
};


/******************************************************************
			ISIO device names.

   These are the names we will use for ISIO I/O devices.

   The names may be chosen at will, but should conform to the 
   naming convention used for VxWorks devices.
******************************************************************/

LOCAL char	*i_devName[ISIO_NUM_CHAN] =
		{"/isio/0", "/isio/1","/isio/2","/isio/3", "/isio/4", "/isio/5","/isio/6","/isio/7"};
  

/******************************************************************
			i_Stop - stop using ISIO board

	This function removes the ISIO driver from the VxWorks I/O 
	system (forcing any open channels to close), and disables 
	VME interrupts used by the ISIO board.
******************************************************************/
int	 i_Stop(VOID)
{
	UINT32	isiobrdAddr;

	sysBusToLocalAdrs (VME_AM_USR_SHORT_IO, (char *) ISIO_SHORT_ADDR, (char**) &isiobrdAddr);
/*	sysBusToLocalAdrs (VME_AM_EXT_USR_DATA, (char *) ISIO_BASE_ADDRESS, (char**) &isiobrdAddr); /* 2006. 08. 31 : hjahn*/
	printf("localaddr=%x\n", isiobrdAddr);

	/* Remove ISIO driver (and devices): */
	isioRemove();

	/* Disable VME interrupt(s) used by ISIO board: */
	sysIntDisable(i_VMEintLvl);

	resetBoard (isiobrdAddr);

	return 0;
}


/*****************************************************************************
* i_Start - start using ISIO board
*
* If the input parameter (doReset) is non-zero,
* the ISIO board will be reset. This is not
* needed after a system reset, but may be needed
* if the system is restarted manually, or if the
* i_Stop call has been used to "uninstall" the board.
*/

int	 i_Start(doReset)
int	 doReset;
{
	UINT32	isiobrdAddr;
	int		chan;
	int		board;
	int		options;

	/******************************************************************
 		The ISIO board is configured for address $960000, in VME
 		standard (A24) address space.
	 	Now we must get hold of the local address, and put it into variable 
	 	"isio2brdAddr":
	******************************************************************/
	if (doReset)		i_Stop();

	sysBusToLocalAdrs (VME_AM_EXT_USR_DATA, (char *) ISIO_BASE_ADDRESS, (char**) &isiobrdAddr);

	/* Create ISIO driver: */
	isioDrv();

	options = 0;

	printf("isioBoard starting !!! \n");
	
	board = isioBrd(	isiobrdAddr,		/* local address		*/
	         			i_intVecBase,		/* interrupt vector base	*/
		     			i_VMEintLvl,		/* interrupt levels..		*/
		     			&i_ioConfig[0],	/* I/O configuration	*/
		     			&i_bufConfig,		/* buffer configuration	*/
		     			options);			/* (none or "reset")	*/

	if (board == ERROR){
		printf("0x%x.....!! failed to install board\n", board);
		return ERROR;
	}
	else	printf("board installed, index=%d\n", board);

	/* Enable VME interrupt(s) used by ISIO board: */
	{
		int	cnt;
		for(cnt=1;cnt<7;cnt++)	sysIntDisable(cnt);
	}

	sysIntEnable(i_VMEintLvl);

	/* Create devices for all ISIO channels: */
	for (chan = 0; chan < ISIO_NUM_CHAN; chan++){
	    if (isioDevCreate(i_devName[chan], chan, board) == ERROR)
	        printf("!! failed to create device (%d)\n", chan);
	    else
	        printf("created %s\n", i_devName[chan]);
	}

	return OK;
}
/* EOF */
