/* hwfuncmenu.c : console.
 *
 */

#include "copyright_wrs.h"
#include "vxWorks.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "fioLib.h"
#include "taskLib.h"
#include "tyLib.h"
#include "msgQLib.h"
#include "socket.h"
#include "inetLib.h"
#include "fioLib.h"
#include "time.h"
#include "hwfunctest.h"


/* ------------------------------------------------------------------------- */

/*IMPORT void menu_main(void);*/
void read_add(void);
extern int i_Start( int );
 
PC_DATA send_data;
extern int 	newFd;
static short crc_table[256];     /* crc 계산을 하기위한 table */
extern UINT32 string2Hex(UINT8 *ptr);
extern int	fp_port[12];
uchar open_port_value;

/* ------------------------------------------------------------------------- */

void   Lib_SetupCrcTable(void)
{
 ushort     index;          /* crc table의 index */
 ushort     number;         /* 순간 data */
 ushort     word;           /* 16 bit data */
 BITS16     *word_ptr;      /* word를 가르킬 pointer */
 BITS8BYTE  *index_ptr;     /* index를 가르킬 pointer */

        for (index = 0; index < 256; index++) 
        {
        word = 0;
                index_ptr = (BITS8BYTE *) &index;
                word_ptr  = (BITS16 *) &word;
                number = (index_ptr->i7 ^ index_ptr->i6 ^ index_ptr->i5 ^ index_ptr->i4 
                  ^ index_ptr->i3 ^ index_ptr->i2 ^ index_ptr->i1);

                word_ptr->b16 = (index_ptr->i8 ^ number);
                word_ptr->b15 = (number);

                word_ptr->b14 = (index_ptr->i8 ^ index_ptr->i7);
                word_ptr->b13 = (index_ptr->i7 ^ index_ptr->i6);
                word_ptr->b12 = (index_ptr->i6 ^ index_ptr->i5);
                word_ptr->b11 = (index_ptr->i5 ^ index_ptr->i4);
                word_ptr->b10 = (index_ptr->i4 ^ index_ptr->i3);
                word_ptr->b9  = (index_ptr->i3 ^ index_ptr->i2);
                word_ptr->b8  = (index_ptr->i2 ^ index_ptr->i1);
                word_ptr->b7  = (index_ptr->i1);

                word_ptr->b1  = (index_ptr->i8 ^ number);
                crc_table[index] = word;
    } /* End for */

  /*  return(Ok); */

} /* End of setup crc table procedure */

PROTO   Lib_CalCrcAll(ushort length,uchar ch[],ushort *all_crc)
{
 ushort     count;      /* 계산 하는 charactor의 순서 */
 ushort     index;      /* crc table 의 entry no */
 ushort     crc;        /* 일시적인 crc 값 */

    for (count = 0, crc = *all_crc; count < length; count++)
        {
        index = (crc ^ ch[count]) & 0x00ff;     /* crc table의 entry no  */
        crc = (crc >> 8) ^ crc_table[index];    /* crc 갱신 */
    }

    *all_crc = crc;                             /* 새로운 crc */

    return(Ok);

} /* End of calculation crc all procedure */

void send_msg(uchar opcode,uchar sub_op)
{
	uchar send_buffer[5],i;
	ushort send_crc;
	
	send_crc=0;
	
    	send_buffer[0] = 0xed;
    	send_buffer[1] = opcode;
    	send_buffer[2] = sub_op;
 	Lib_CalCrcAll(3,send_buffer,&send_crc);
	send_buffer[4] = (uchar)(send_crc & 0xff);
	send_buffer[3] = (uchar)(send_crc >> 8);
	write(newFd,send_buffer,5);

 
}

void	poll_ack()
{

	send_msg(0x10,0);	/* poll ack */
}


void menu_main()
{
	int	menu_num, i,rc;
	ushort crc;
    char string[40];
	unsigned char temp_buffer[5];
    PC_DATA receive_data;
	unsigned char crc_temp[2],re_val;
   
	unsigned char input_buffer[1];
	unsigned char *ptr;
	int width,index=0,count=0;
	struct fd_set readFds,tempFds;
 	struct timeval tv;
 	struct timeval con_tv;	

/* Initialize com port. */
	int nCOMStatus = 0;
	
	Lib_SetupCrcTable();
	
/*
	rx_qid = msgQCreate(1024,16,MSG_Q_FIFO);
*/
	nCOMStatus = openPort(0); /* if port open ok, return value "0" */

	if( nCOMStatus ) {
		open_port_value = nCOMStatus;
	}

	port_Init(0);

/* menu doing.	 */
	while(1)
	{   
		crc = 0;

	 	FD_ZERO(&readFds);
/* 
		tv.tv_sec = sysClkRateGet()*5;
*/
   	    tv.tv_sec = 1;
	    tv.tv_usec= 0;
	    FD_SET(newFd,&readFds);
	    width = newFd;
	    width++;
		if(index >20)
		{
			shutdown(newFd,2);
			close(newFd);
	        	port_Init(1);
			index =0;
		}
	    rc = select(width,&readFds,NULL,NULL,&tv);
	    if(rc != 1)
	    {
			  index++;
	        continue;  /* 최초 while문으로 돌아감 */
	    }
		if (FD_ISSET(newFd,&readFds))        /* received data */
		{
			
			count = read(newFd,input_buffer,1);
			if(count ==0 || count < 0)
			{			
				index++;
				continue;
			}
			else
				index =0;
				
			if(count)
			{
				ptr = &receive_data.stx;
				if(input_buffer[0]== 0xed)
				{
					*ptr++ = 0xed;
				/*	printf("[Rx] : [%x]",input_buffer[0]); */
				}
				else
					continue;
					
				for(i=0;i<4;i++)
				{	
					if(read(newFd,input_buffer,1))
					{
						*ptr++ = input_buffer[0];	
					/*	printf(" [%x]",input_buffer[0]); */
					
					}
				}
			/*	printf(" \n"); */
				memcpy(temp_buffer,&receive_data.stx,5);
				Lib_CalCrcAll(3,temp_buffer,&crc);
				crc_temp[0] =(uchar)(crc & 0xff);
				crc_temp[1] =(uchar)(crc >> 8);
				if((receive_data.crc2 != crc_temp[0]) || (receive_data.crc1 != crc_temp[1]))
					continue; /* data crc 검증 
				(void)ioctl(newFd,FIOFLUSH,0);
*/
			}

		}
	 

		switch(receive_data.opcode){
			case	0xac :
				if(open_port_value)
				{
					send_msg(0xa7,open_port_value);
					open_port_value=0;
				}	
				else
					poll_ack();
				break;

			case	0x20 :
				NVRam_Test();
				break;
			
			case	0x30 :
				send_msg(0x30,0xff);
				Diag_LED_Test();
				break;
			
		
			case	0x50 :
				Dip_SW_Test();
				break;
			

			case	0x90 :
				SRam_Test();
				break;
	
			case	0xc0 :
				
				watchDogTimer_Clear_Set();
				taskDelay(240);
				send_msg(0xc0,0);
				watchDogTimer_Clear_Free();
				taskDelay(140);
				watchDogTimer_Enable();
				break;
				
			case 0xd0 :/* 308 INIT */
				rc = openPort(1);
				if(rc)
					open_port_value = rc;
				break;
			
	
			default	  :
				break;
		}
	}		
}

