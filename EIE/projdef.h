/* projdef.h
 *
 */
 
#ifndef _PROJECT_DEFINE_H
#define _PROJECT_DEFINE_H

/* ----------------------------------------------------------------------- */
/* Redefined types of data.
 *
 */
typedef unsigned char 	uchar;
typedef unsigned char 	BYTE;
typedef unsigned short 	WORD;

/* ----------------------------------------------------------------------- */
#define FORever		1L
#define PROTO   	error
#define	PROCESS		STATUS

/* ----------------------------------------------------------------------- */
/* priority of Tasks
 *
 */
#define TASKPR_IF	 	100
#define TASKPR_IO	 	100
#define TASKPR_BLOCK 	100
#define TASKPR_CONSOLE	100
#define TASKPR_MAIN  	100
#define TASKPR_LOG		100
#define TASKPR_WATCH 	120
#define TASKPR_CT	 	120
#define TASKPR_AG 	 	120

/* ----------------------------------------------------------------------- */
/* wait limit tick of tasks
 *
 */
#define MAX_TASKTICK_BASE		600						// tick for 1 second.
#define MAX_WAITTICK_MAINSTART	(MAX_TASKTICK_BASE * 2)	// tick is 2 second.
#define MAX_WAITTICK_MAINRUN	(MAX_TASKTICK_BASE / 4)	// tick is 1/4 second.
#define MAX_WAITTICK_COMIF		(MAX_TASKTICK_BASE * 3)	// tick is 1 second.
#define MAX_WAITTICK_MCCR		(MAX_TASKTICK_BASE)		// tick is 1 second.
#define MAX_WAITTICK_MMCR		(MAX_TASKTICK_BASE)		// tick is 1 second.
#define MAX_WAITTICK_BLOCK		(MAX_TASKTICK_BASE * 2)	// tick is 2 second.
#define MAX_WAITTICK_IOSCAN		(MAX_TASKTICK_BASE / 8)	// tick is 1/8 second.

/* ------------------------------------------------------------------ */
// Defined size.
//short MAX_SIZE_INBUFFER = 96;	// 2001.4.24  0x40->96 //
//#define MAX_SIZE_INBUFFER 	96	// (96 * 8) / 32 = 24 (EA boards) 
#define MAX_SIZE_VMEMBUFFER		0x1380	// 4480Bytes for MYN
#define MAX_SIZE_IOBUFFER		0x100

/* ----------------------------------------------------------------------- */

typedef struct _CONSOLE_NET_ADDR
{
	BYTE bIsDualNet;
	BYTE bUseCCNet;
	BYTE bUseMCNet;
	char MY_ADDR_PRI[16];
	char MY_ADDR_SEC[16];
	char MC_ADDR_PRI[16];
	char MC_ADDR_SEC[16];
	char CC1_ADDR_PRI[16];
	char CC1_ADDR_SEC[16];
	char CC2_ADDR_PRI[16];
	char CC2_ADDR_SEC[16];
} CONSOLE_NET_ADDR;

typedef struct _BLOCK_NET_ADDR
{
	BYTE bIsDualNet;
	BYTE bUseUPNet;
	BYTE bUseDNNet;
	BYTE bUseMDNet;
	BYTE bUseModem;
	BYTE cMyStnID;
	BYTE cUPStnID;
	BYTE cDNStnID;
	BYTE cMDStnID;
	char MY_ADDR_PRI[16];
	char MY_ADDR_SEC[16];
	char PEER_ADDR_PRI[16];
	char PEER_ADDR_SEC[16];
	char UP_CBI1_ADDR_PRI[16];
	char UP_CBI1_ADDR_SEC[16];
	char UP_CBI2_ADDR_PRI[16];
	char UP_CBI2_ADDR_SEC[16];
	char DN_CBI1_ADDR_PRI[16];
	char DN_CBI1_ADDR_SEC[16];
	char DN_CBI2_ADDR_PRI[16];
	char DN_CBI2_ADDR_SEC[16];
	char MD_CBI1_ADDR_PRI[16];
	char MD_CBI1_ADDR_SEC[16];
	char MD_CBI2_ADDR_PRI[16];
	char MD_CBI2_ADDR_SEC[16];
} BLOCK_NET_ADDR;

#endif // _PROJECT_DEFINE_H
