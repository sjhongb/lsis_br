/* -------------------------------------------------------------------------+
|  CRC.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CRC.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#include "crc.h"

#define CRC_TABLE_SIZE		256
#define CRC_BITS_PER_BYTE	8
#define CRC_BIT_MASK		0xFFU

//-------------------------------------------------
// CRC parameters (default values are for CRC-32):
//-------------------------------------------------
#define  CRC32_ORDER      32
#define  CRC32_POLYNOM    0x4c11db7
#define  CRC32_CRCINIT    0xffffffff

static Int32 g_iOrder;
static Uint32 g_uiPolynom;

// internal global values:
static Uint32 g_uiCrcmask;
static Uint32 g_uiCrchighbit;
//Uint32 g_uiCrcinit_direct;
static Uint32 g_uiCrcinit_nondirect;
static Uint32 g_auiCrctab[CRC_TABLE_SIZE];

unsigned int Ccrc::GenCrcTable()
{
	int	 temp;

	union {
		int	i;
		struct {
                        unsigned extra : 24;
			unsigned i8 : 1;
			unsigned i7 : 1;
			unsigned i6 : 1;
			unsigned i5 : 1;
			unsigned i4 : 1;
			unsigned i3 : 1;
			unsigned i2 : 1;
			unsigned i1 : 1;
		} bit;
	} iun;

	union {
		unsigned int entry_num;
		struct {
			unsigned reserved : 16;
			unsigned b16: 1;
			unsigned b15: 1;
			unsigned b14: 1;
			unsigned b13: 1;
			unsigned b12: 1;
			unsigned b11: 1;
			unsigned b10: 1;
			unsigned b9 : 1;
			unsigned b8 : 1;
			unsigned b7 : 1;
			unsigned b6 : 1;
			unsigned b5 : 1;
			unsigned b4 : 1;
			unsigned b3 : 1;
			unsigned b2 : 1;
			unsigned b1 : 1;
		} entrybit;
	} entryun;

	for (iun.i = 0; iun.i < 256; iun.i++) {
		entryun.entry_num = 0;
		temp = (iun.bit.i7 ^ iun.bit.i6 ^iun.bit.i5 ^
				iun.bit.i4 ^iun.bit.i3 ^iun.bit.i2 ^iun.bit.i1);
		entryun.entrybit.b16 = (iun.bit.i8 ^ temp);
		entryun.entrybit.b15 = (temp);
		entryun.entrybit.b14 = (iun.bit.i8 ^ iun.bit.i7);
		entryun.entrybit.b13 = (iun.bit.i7 ^ iun.bit.i6);
		entryun.entrybit.b12 = (iun.bit.i6 ^ iun.bit.i5);
		entryun.entrybit.b11 = (iun.bit.i5 ^ iun.bit.i4);
		entryun.entrybit.b10 = (iun.bit.i4 ^ iun.bit.i3);
		entryun.entrybit.b9  = (iun.bit.i3 ^ iun.bit.i2);
		entryun.entrybit.b8  = (iun.bit.i2 ^ iun.bit.i1);
		entryun.entrybit.b7  = (iun.bit.i1);
		entryun.entrybit.b1  = (iun.bit.i8 ^ temp);
		crc_table[iun.i] = entryun.entry_num;
	} /* for End */
	return (1);
} /* CRCtable() */

unsigned int Ccrc::CrcGen(int len, unsigned char *ptr, unsigned char *low_crc, unsigned char *high_crc)
{
	register  int cnt;
	unsigned crcc;

	for (cnt = crcc = 0 ; cnt < len ; cnt++,ptr++)
		crcc = (crcc >> 8) ^ crc_table[((crcc ^ *ptr) & 0x00ff)];

	// crc2 상위 바이트
	*high_crc = crc2 = (crcc >> 8) & 0x00ff;
	//crc1 하위바이트
	*low_crc  = crc1 = crcc & 0x00ff;

	return (1);
}

static void GenerateCrc32Table(void)
{
	// make CRC lookup table used by table algorithms

	Int32 iIndex1, iIndex2;
	Uint32 uiBit, uiCrc;

	for (iIndex1 = 0; iIndex1 < CRC_TABLE_SIZE; iIndex1++)
	{

		uiCrc = (Uint32) iIndex1;

		uiCrc <<= g_iOrder - CRC_BITS_PER_BYTE;

		for (iIndex2 = 0; iIndex2 < CRC_BITS_PER_BYTE; iIndex2++)
		{
			uiBit = uiCrc & g_uiCrchighbit;
			uiCrc <<= 1;
			if (uiBit)
			{
				uiCrc ^= g_uiPolynom;
			}
		}

		uiCrc &= g_uiCrcmask;
		g_auiCrctab[iIndex1] = uiCrc;
	}
}

void InitialCrc32(void)
{
	Int32 iIndex1;
	Uint32 uiBit, uiCrc;
	Uint32 uiCrcinit;
	Uint32 uiCrcinit_direct;

	// CRC parameters (default values are for CRC-32):
	g_iOrder = CRC32_ORDER;
	g_uiPolynom = CRC32_POLYNOM;
	uiCrcinit = CRC32_CRCINIT;

	// at first, compute constant bit masks for whole CRC and CRC high bit
	g_uiCrcmask = ((((Uint32) 1 << (g_iOrder - 1)) - 1) << 1) | 1;
	g_uiCrchighbit = (Uint32) 1 << (g_iOrder - 1);

	// compute missing initial CRC value
	g_uiCrcinit_nondirect = uiCrcinit;
	uiCrc = uiCrcinit;
	for (iIndex1 = 0; iIndex1 < g_iOrder; iIndex1++)
	{
		uiBit = uiCrc & g_uiCrchighbit;
		uiCrc <<= 1;
		if (uiBit)
		{
			uiCrc ^= g_uiPolynom;
		}
	}
	uiCrc &= g_uiCrcmask;
	uiCrcinit_direct = uiCrc;

	// generate lookup table
	GenerateCrc32Table();
}

Uint32 checkCrc32(const Uint8 p[], Int32 iLen)
{
	// normal lookup table algorithm with augmented zero bytes.
	// only usable with g_uiPolynom orders of 8, 16, 24 or 32.

	Uint32 uiCrc = g_uiCrcinit_nondirect;
	Int32 iIndex = 0;

	//if (!g_uiRefin)
	while (iLen--)
	{
		uiCrc = ((uiCrc << CRC_BITS_PER_BYTE) | p[iIndex++]) ^ g_auiCrctab[(uiCrc >> (g_iOrder - CRC_BITS_PER_BYTE)) & CRC_BIT_MASK];
	}

	while (++iLen < (g_iOrder / CRC_BITS_PER_BYTE))
	{
		uiCrc = (uiCrc << CRC_BITS_PER_BYTE) ^ g_auiCrctab[(uiCrc >> (g_iOrder - CRC_BITS_PER_BYTE)) & CRC_BIT_MASK];
	}

	uiCrc &= g_uiCrcmask;

	return uiCrc;
	//return 0;
}