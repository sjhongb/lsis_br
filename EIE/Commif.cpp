/* -------------------------------------------------------------------------+
|  CommIf.cpp                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     Commif.cpp                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
//#define PING_CHECK

/* standard c library */
#include <stdio.h>
#include <string.h>

/* vxWorks library */
#include "selectLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "msgQLib.h"
#include "sockLib.h"
#ifdef PING_CHECK
#include "pingLib.h"
#endif

/* user c library */
#include "projdef.h"
#include "../INCLUDE/LogTask.h"

#include "commif.h"


/* ----------------------------------------------------------------------- */
#define COM_STX 			(unsigned char)0x7E
#define MAX_SIZE_IFBUFFER	MAX_SIZE_VMEMBUFFER	// 4480	for MYN

/* ----------------------------------------------------------------------- */
#define FORever				1L
#define PROTO   			error
#define	PROCESS				STATUS
#define COMPAREDATACNT		4

#define IF_UDP_PORT			4885

#define LOGIN_DATA_SIZE		16


/* ----------------------------------------------------------------------- */
extern unsigned char VMEM[];
extern char  g_nSysNo;

extern int	_pingTxTmo;
extern int	_pingTxInterval;


/* ----------------------------------------------------------------------- */

Comm_IF ComIF( MAX_SIZE_IFBUFFER );	// buffer size is 4k byte.

/* ----------------------------------------------------------------------- */
Comm_IF::Comm_IF(int nSize) : CommDriver( nSize )
{
#ifdef PING_CHECK
	m_bSendFlag = 1;
#endif
	m_nCheckCount = 0;

	memset(m_pLoginBuffer, 0, sizeof(m_pLoginBuffer)); 
}

Comm_IF::~Comm_IF() 
{
}

/* ----------------------------------------------------------------------- */

short Comm_IF::RcvCheck( unsigned char *pRcvBuffer, unsigned char *pRcvCompareData, unsigned char *pLoginBuffer ) 
{
	short nResult;
		
	if(m_nReceived) 
	{
		memcpy( pRcvBuffer, m_pRcvBuffer, m_nTxBlockSize);
		memcpy( pRcvCompareData, m_pRcvCompareData, COMPAREDATACNT);
		memcpy( pLoginBuffer, m_pLoginBuffer, LOGIN_DATA_SIZE);

		/*
		printf("Set LoginInfo to Temp Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
				pLoginBuffer[0],
				pLoginBuffer[1],
				pLoginBuffer[2],
				pLoginBuffer[3],
				pLoginBuffer[4],
				pLoginBuffer[5],
				pLoginBuffer[6],
				pLoginBuffer[7]);
		*/

	}

	nResult = m_nReceived;
	m_nReceived = 0;
	
	return nResult;
}

int Comm_IF::RxFrameCheck( char* data, int size ) 
{
	LogInd(LOG_TYPE_SYNC, "Recv [%d] <--- %s", size, (g_nSysNo == 0) ? "System #2" : "System #1");

	unsigned short m_nRxCRC;
	short result = 0;
	BYTE *p = (BYTE*)data;
	
	while ( size > 0 ) 
	{
		int nCopyCnt = size;
		if ( (nCopyCnt + m_nRxPtr) >= MAX_SIZE_IFBUFFER ) 
		{
			nCopyCnt = MAX_SIZE_IFBUFFER - m_nRxPtr;
		}
		memcpy( &m_pRxBuffer[m_nRxPtr], p, nCopyCnt );
		m_nRxPtr += nCopyCnt;
		p += nCopyCnt;

		//printf("m_nRxPtr = %d\n", m_nRxPtr);
		int pos = 0;
rescan:
		if ( m_pRxBuffer[ pos ] != COM_STX ) 
		{
			while ( pos < m_nRxPtr )
			{
				if ( m_pRxBuffer[ pos ] == COM_STX ) 
				{
					//printf("find STX!!!! pos = %d\n", pos);
					break;
				}

				pos++;
			}
		}
		int nRemain = m_nRxPtr - pos;
		if ( (nRemain > 0) && pos ) 
		{
			//printf("move data!!!! nRemain = %d\n", nRemain);
			memcpy( &m_pRxBuffer[0], &m_pRxBuffer[pos], nRemain );
		}

		m_nRxPtr -= pos;
		if ( m_nRxPtr < 0 ) 
		{
			m_nRxPtr = 0;
		}

		if ( m_nRxPtr > 2 ) 
		{
			if ( m_pRxBuffer[0] == COM_STX ) 
			{
				if (m_pRxBuffer[1] == 'L') 
				{
					if ( m_nRxPtr >= (m_nTxBlockSize + 8 + LOGIN_DATA_SIZE) ) 
					{
						//printf("STX & 'L' & size are OK!!!\n");
						// STX(0x7E), 'L', table CRC high, table CRC low, com status, error evg, data[n], CRC low, CRC high.
						m_nRxCRC = 0;
						for (int loc=1; loc<(m_nTxBlockSize + 8 + LOGIN_DATA_SIZE); loc++ ) 
						{
							m_nRxCRC = crc_CCITT( m_pRxBuffer[loc], m_nRxCRC );
						}

						if ( m_nRxCRC == 0 ) 
						{
							result = m_pRxBuffer[5];	// 2002.12.13 CardErrNo
							result++;
							memcpy( m_pRcvCompareData, &m_pRxBuffer[2], COMPAREDATACNT);
							memcpy( m_pRcvBuffer, &m_pRxBuffer[COMPAREDATACNT+2], m_nTxBlockSize);

							// Login 정보 수신하여 버퍼에 저장한다.
							memcpy( m_pLoginBuffer, &m_pRxBuffer[COMPAREDATACNT+2+m_nTxBlockSize], LOGIN_DATA_SIZE);

/*
							printf("Set LoginInfo to Local Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
									m_pLoginBuffer[0],
									m_pLoginBuffer[1],	
									m_pLoginBuffer[2],
									m_pLoginBuffer[3],
									m_pLoginBuffer[4],
									m_pLoginBuffer[5],
									m_pLoginBuffer[6],
									m_pLoginBuffer[7]);
*/

							LogDbg(LOG_TYPE_SYNC, "RXB[ %2.2X %2.2X] RX[0:%x, 1:%x, 2:%x, 3:%x]",
									m_pRxBuffer[2],m_pRxBuffer[3],
								   	m_pRcvCompareData[0],m_pRcvCompareData[1],m_pRcvCompareData[2],m_pRcvCompareData[3]);

							pos = m_nTxBlockSize + 8 + LOGIN_DATA_SIZE;
							int nRemain = m_nRxPtr - pos;
							if ( nRemain > 0 ) {
								memcpy( m_pRxBuffer, m_pRxBuffer + pos, nRemain );
							}
							m_nRxPtr -= pos;
							result = 1;
						}
						else 
						{
							LogDbg(LOG_TYPE_SYNC, "CRC" );
							//printf("CRC error!!!\n");
							m_pRxBuffer[0] = 0; // 무한루프 방지위해 클리어
							pos = 2;
							goto rescan;
						}
					}
					else
					{
						//printf("Insufficient Data!!!\n");
					}
				}
				else 
				{
					LogErr(LOG_TYPE_SYNC, "IF FAIL" );
					m_pRxBuffer[0] = 0; // 무한루프 방지위해 클리어
					pos = 1;
					goto rescan;
				}
			}
			else 
			{
				pos = 0;
			}
		}

		size -= nCopyCnt;
	}
	return result;
}


/* ----------------------------------------------------------------------- */
#define MAX_SIZE_IFREAD		MAX_SIZE_IFBUFFER

#ifdef PING_CHECK
PROCESS ComIF_PingDrv(void)
{	
	int		error;
	char	peer_ip[16] = {0,};

	if(g_nSysNo == 0)
	{
		strncpy(peer_ip, SECONDARY_IF_ADDR, 16);
	}
	else
	{
		strncpy(peer_ip, PRIMARY_IF_ADDR, 16);
	}

	_pingTxTmo		= 2; // 2Sec;
	_pingTxInterval	= 1; // 1Sec;

    while(1) 
	{
		error = ping(peer_ip, 2, PING_OPT_SILENT|PING_OPT_DONTROUTE);
		if(error == 0) 
		{
            LogInd(LOG_TYPE_SYNC, "Ping Test Success.");
			ComIF.m_bSendFlag = 1;
			continue;
        } 

		LogErr(LOG_TYPE_SYNC, "Ping Test Failure. error = %d", error); 
		ComIF.m_bSendFlag = 0;

		switch(error)
		{
		case EINVAL:
		   	LogErr(LOG_TYPE_SYNC, "Error Status : EINVAL"); 
			break;
		case S_pingLib_NOT_INITIALIZED:
		   	LogErr(LOG_TYPE_SYNC, "Error Status : S_pingLib_NOT_INITIALIZED"); 
			break; 
		case S_pingLib_TIMEOUT:
			LogErr(LOG_TYPE_SYNC, "Error Status : S_pingLib_TIMEOUT");
			break;
        }
	}
}
#endif

PROCESS ComIF_RecvDrv(void)
{
	int 	nfd;
    int     index;
	int		count;
    int     width;
    int		result;
    STATUS  status;
    char    readbuf[MAX_SIZE_IFREAD+16];
    uchar	msg[100]; 

	width = 0;
	nfd = ComIF.m_nfd;
	MSG_Q_ID rx_qid = ComIF.m_nRxQID;

    selTimeOut_IF.tv_sec  = 0;
    selTimeOut_IF.tv_usec = 500 * 1000;

    FD_ZERO ( &readFds_IF );

    FD_SET( nfd, &readFds_IF );
    width = max( nfd, width);
	width ++;

    while( 1 ) 
	{
        tmpFds_IF = readFds_IF;

        status = select ( width, &tmpFds_IF, NULL, NULL, &selTimeOut_IF);
		if (status == ERROR) 
		{
			LogErr(LOG_TYPE_SYNC, "IF:select error");
		}
		else if (status == 0) 
		{
			LogErr(LOG_TYPE_SYNC, "IF:select TIMEOUT!!!!!");
			continue;
		}
		else 
		{
	      	if( FD_ISSET(nfd, &readFds_IF) ) 
			{
				count = recvfrom(nfd, readbuf, (size_t)MAX_SIZE_IFREAD, 0, 
								 (struct sockaddr *)&ComIF.m_remoteAddr, (int*)(sizeof(ComIF.m_remoteAddr)));

				//count = read( nfd, readbuf, (size_t)MAX_SIZE_IFREAD);
				result = ComIF.RxFrameCheck( readbuf, count );
				if ( result ) 
				{
					LogDbg(LOG_TYPE_SYNC, "Receive Success.!!!!!!!!");
					ComIF.m_nReceived = 1;
				}
          	}
          	else 
			{
				LogErr(LOG_TYPE_SYNC, "IF:not read");
          	}
		}

		taskDelay(1); // for task swap !!! important IM.
	}
}

/* ----------------------------------------------------------------------- */

short Comm_IF::Initialize(int nIfsize, char *MyAddr, char *PeerAddr)
{
	m_nTxBlockSize = nIfsize;	
	
	bzero((char*)&m_localAddr, sizeof(m_localAddr));
	bzero((char*)&m_remoteAddr, sizeof(m_remoteAddr));

	m_localAddr.sin_family	= AF_INET;
	m_localAddr.sin_port	= htons(IF_UDP_PORT);
	m_localAddr.sin_addr.s_addr		= inet_addr(MyAddr); 

	m_remoteAddr.sin_family	= AF_INET;
	m_remoteAddr.sin_port	= htons(IF_UDP_PORT);
	m_remoteAddr.sin_addr.s_addr	= inet_addr(PeerAddr);  

	if((m_nfd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
	{
		LogErr(LOG_TYPE_SYNC, "Socket Create Error!!");
		return (ERROR);
	}

	if(bind(m_nfd, (struct sockaddr *)&m_localAddr, (int)(sizeof(m_localAddr))) == ERROR)
	{
		LogErr(LOG_TYPE_SYNC, "Socket Bind Error!!");
		return (ERROR);
	}

#ifdef PING_CHECK
#if 0
	if(pingLibInit() == OK)
	{
		LogInd(LOG_TYPE_SYNC, "Ping Library Intialize Successful.");
	}
	else
	{
		LogErr(LOG_TYPE_SYNC, "Ping Library Intialize Failure.");
	}
#endif
#endif
	
    taskSpawn("ComIF_RecvDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComIF_RecvDrv,0,0,0,0,0,0,0,0,0,0);
#ifdef PING_CHECK
    taskSpawn("ComIF_PingDrv",TASKPR_IF,0,0x5000,(FUNCPTR)ComIF_PingDrv,0,0,0,0,0,0,0,0,0,0);
#endif
}

int Comm_IF::TxVMEM( unsigned char *TxCompareData, unsigned char *pLoginBuffer )
{
	int    i;
	int    send_count;
	short  nSendByte;
    unsigned short nCRC = 0;
		
#ifdef PING_CHECK
	if(m_bSendFlag == 0)
	{
		LogInd(LOG_TYPE_SYNC, "Blocking Dual-Data Send By PING Test Fail.");
		return 0;
	}
#endif

	m_pTxBuffer[0] = 0x7E;	// STX
	m_pTxBuffer[1] = 'L';
	
	memcpy( m_pTxBuffer+2, TxCompareData, COMPAREDATACNT); // crc high, crc low, com status, errer evg.
	memcpy( m_pTxBuffer+COMPAREDATACNT+2, VMEM, m_nTxBlockSize );

	/*
	printf("Set LoginInfo to IF Tx Buffer!!!! Login = [%c%c%c%c%c%c%c%c]\n", 
			pLoginBuffer[0],
			pLoginBuffer[1],
			pLoginBuffer[2],
			pLoginBuffer[3],
			pLoginBuffer[4],
			pLoginBuffer[5],
			pLoginBuffer[6],
			pLoginBuffer[7]);
	*/

	// 20130605 Login 정보 전송을 위해 계간통신 데이터 확장
	memcpy( m_pTxBuffer+COMPAREDATACNT+2+m_nTxBlockSize, pLoginBuffer, LOGIN_DATA_SIZE);

	//LogDbg(LOG_TYPE_SYNC, "0:%x, 1:%x, 2:%x, 3:%x",TxCompareData[0],TxCompareData[1],TxCompareData[2],TxCompareData[3]);

	for (i=1; i<(m_nTxBlockSize + COMPAREDATACNT + 2 + LOGIN_DATA_SIZE); i++) 
	{
	    nCRC = crc_CCITT( m_pTxBuffer[i], nCRC );
	}
	m_pTxBuffer[i++] = nCRC % 256;
	m_pTxBuffer[i++] = nCRC / 256;

#if 0
    send_count = 0;

    ioctl( m_nfd, FIONWRITE, (int)&send_count );
    if ( send_count != 0 )
        ioctl( m_nfd, FIOWFLUSH, 0 );
#endif

	nSendByte = 0;	// test code
	nSendByte = sendto(	m_nfd, (char*)&m_pTxBuffer[0], i, 0, 
						(struct sockaddr *)&m_remoteAddr, (int)(sizeof(m_remoteAddr)) );

    //nSendByte = write( m_nfd, (char*)&m_pTxBuffer[0], i );	// (i = m_nTxBuffer + 8);
	
	LogInd(LOG_TYPE_SYNC, "errno[%d] Send[%d] Sent[%d] ---> %s", errno, i, nSendByte, (g_nSysNo == 0) ? "System #2" : "System #1");

	return nSendByte;
}

