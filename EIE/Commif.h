/* -------------------------------------------------------------------------+
|  CommIf.h                                                              
|  -----------                                                              
|                                                                           
|  1. Author : S.Y.CHO                                                               
|                                                                           
|  2. FileName.                                                             
|     CommIf.h                                                            
|                                                                           
|  3. File Discription.                                                     
|     communication driver for MCCR.
|                                                                           
|  4. Project Name.                                                         
|     ST-4 by Thailand RST.                                                 
|                                                                           
|  5. Maker.                                                                
|                                                                                                                                                      
|  6. Target system.                                                        
|   1) Unit.                                                                
|      - Target Board	- KVME402 and KVME308                               
|      - Target O.S		- VxWorks 5.2                                                                                                           
|                                                                                                                                                      
|  7. Development story.                                                    
|                                                                           
|    Ver      Date      producer                  remark                    
|   ---------------------------------------------------------------------   
|                                                                           
+------------------------------------------------------------------------- */
#ifndef _COMMIF_H
#define _COMMIF_H

#include "inetLib.h"
#include "CommDrv.h"

#define	LOGIN_DATA_SIZE	16

/* ------------------------------------------------------------------------- */
class Comm_IF : public CommDriver 
{
public:
	int m_nRxPtr;
	short m_nReceived;
	int m_nCheckCount;

#ifdef PING_CHECK
	int m_bSendFlag;
#endif
	
public:
	short m_nTxBlockSize;
	short m_nRxBlockSize;
	unsigned char m_pRcvBuffer[MAX_SIZE_VMEMBUFFER];		    
	unsigned char m_pRcvCompareData[20];		

	unsigned char m_pLoginBuffer[16];

	Comm_IF( int nSize );
	~Comm_IF();
	
	int TxVMEM(unsigned char *TxCompareData, unsigned char *pLoginBuffer);
	
	short RcvCheck( unsigned char *pRcvBuffer, unsigned char *pRcvCompareData, unsigned char *pLoginBuffer );
	int RxFrameCheck( char* data, int size );
	short Initialize(int nIfsize, char *MyAddr, char *PeerAddr);

	struct sockaddr_in  m_localAddr;
	struct sockaddr_in  m_remoteAddr;
};

/* ------------------------------------------------------------------------- */
extern Comm_IF ComIF;

/* ------------------------------------------------------------------------- */

#endif // _COMMIF_H
