/* sysSerial.c -  MPC860 SCC UART BSP serial device initialization */

/* Copyright 20028 Etin Systems, Inc., All Rights Reserved */
#include "copyright_wrs.h"

#include "vxWorks.h"
#include "iv.h"
#include "intLib.h"
#include "config.h"
#include "sysLib.h"
#include "logLib.h"
#include "drv/intrCtl/ppc860Intr.h"
#include "kvme402a.h"

#include "tl16c554Sio.h"

#include "./ppc860Sio.h"
#include "drv/multi/ppc860Cpm.h"
#include "./ppc860Sio.c"

#define MPC860_REGB_OFFSET	0x2000		/* offset to internal registers */

#define SMC1_TBD_BA		0x200		/* <- 0x500 */
#define SMC1_RBD_BA		SMC1_TBD_BA + 0x10	
#define SMC1_TBD_NUM		0x01
#define SMC1_RBD_NUM		0x10	/* org 0x01*/
#define SMC1_TX_BUF		0x300		/* <- 0x600 */
#define SMC1_RX_BUF		SMC1_TX_BUF + 0x80
#define SMC1_TX_BUF_SIZE	0x01	/* <- 0x01 */

#define SMC2_TBD_BA		0x400		/* <- 0x500 */
#define SMC2_RBD_BA		SMC2_TBD_BA + 0x10	
#define SMC2_TBD_NUM		0x01
#define SMC2_RBD_NUM		0x10	/* org 0x01*/
#define SMC2_TX_BUF		0x500		/* <- 0x600 */
#define SMC2_RX_BUF		SMC2_TX_BUF + 0x80
#define SMC2_TX_BUF_SIZE	0x01	/* <- 0x01 */

/* device initialization structure */

typedef struct {
		UINT32	smcTbdBa;
		UINT32	smcRbdBa;
		UINT32	smcTbdNum;
		UINT32	smcRbdNum;
		UINT32	smcTxBuf;
		UINT32	smcRxBuf;
		UINT32	smcTxBufSize;
		} PPC860SMC_PARMS;

/* Local data structures */

static PPC860SMC_PARMS	ppc860SmcParms [] =
	{
		{SMC1_TBD_BA, SMC1_RBD_BA, SMC1_TBD_NUM, SMC1_RBD_NUM, 
		 SMC1_TX_BUF, SMC1_RX_BUF, SMC1_TX_BUF_SIZE},
		{SMC2_TBD_BA, SMC2_RBD_BA, SMC2_TBD_NUM, SMC2_RBD_NUM, 
		 SMC2_TX_BUF, SMC2_RX_BUF, SMC2_TX_BUF_SIZE}
	};

static 	PPC860SMC_CHAN ppc860Chan [SMC_N_SIO_CHANNELS];

/******************************************************************************
*
* sysSerialHwInit - initialize the BSP serial devices to a quiesent state
*
* This routine initializes the BSP serial device descriptors and puts the
* devices in a quiesent state.  It is called from sysHwInit() with
* interrupts locked.
*
* Data Parameter Ram layout:
*
*          -----------------------------
*          |                           | DPRAM base
*          |                           |
*          |                           |
*          |                           |
*          |---------------------------|
*          | 8 bytes per descriptor    | SMC1 Tx Buffer Descriptor (0x2200)
*          |---------------------------|
*          |                           |
*          |---------------------------|
*          | 16 descriptors @          | SMC1 Rx Buffer Descriptors (0x2210)
*          | 8 bytes per descriptor    |
*          |                           |
*          |---------------------------|
*          |                           | end SMC1 Rx BDs (0x2290)
*          |                           |
*          |                           |
*          |---------------------------|
*          | 80 bytes allowed          | SMC1 Tx Buffer (0x2300 + DPRAM base )
*          |---------------------------|
*          | one receive char/buffer   | SMC1 Rx Buffer (0x2380 + DPRAM base )
*          |---------------------------|
*          |                           |
*          |                           |
*          |---------------------------|
*          | 8 bytes per descriptor    | SMC2 Tx Buffer Descriptor (0x2400)
*          |---------------------------|
*          |                           |
*          |---------------------------|
*          | 16 descriptors @          | SMC2 Rx Buffer Descriptors (0x2410)
*          | 8 bytes per descriptor    |
*          |                           |
*          |---------------------------|
*          |                           | end SMC2 Rx BDs (0x2490)
*          |                           |
*          |                           |
*          |---------------------------|
*          | 80 bytes allowed          | SMC2 Tx Buffer (0x2500 + DPRAM base )
*          |---------------------------|
*          | one receive char/buffer   | SMC2 Rx Buffer (0x2580 + DPRAM base )
*          |---------------------------|
*          |                           |
*          |                           |
*
*
* RETURNS: N/A
*/ 

void sysSerialHwInit (void)
{
	int i;
	UINT32 regBase;

	for (i=0; i<SMC_N_SIO_CHANNELS; i++) 
	{
		/* BRGCLK freq (Hz) */
		
		ppc860Chan[i].numChannel = i;
		 
		
		ppc860Chan[i].clockRate = CLK6_FREQ;/*7.3728MHz*/	

		/* IMMR reg has base address */
		ppc860Chan[i].regBase = vxImmrGet();  		

		regBase = ppc860Chan [i].regBase;


        /************************************
        *   configure the port lines        *
        ************************************/
        /* CLK 6 Clk Line Set   */
        *MPC860_PAPAR(regBase) |= 0x2000;   /*  0x200c  */
        *MPC860_PADIR(regBase) &= ~(0x2000);

	/* use BRG */
	ppc860Chan[i].bgrNum       = i+1;			

	/* SMC wired for rs232 */
	ppc860Chan[i].uart.smcNum  = i+1;			

	/* use 1 transmit BD */
	ppc860Chan[i].uart.txBdNum = ppc860SmcParms[i].smcTbdNum;

	/* use 16 receive BD */
	ppc860Chan[i].uart.rxBdNum = ppc860SmcParms[i].smcRbdNum;			

	/* setup the transmit buffer descriptor base addr */
	ppc860Chan[i].uart.txBdBase = (SMC_BUF *) (MPC860_REGB_OFFSET + ppc860SmcParms[i].smcTbdBa);

	/* setup the receive buffer descriptor base addr */
	ppc860Chan[i].uart.rxBdBase = (SMC_BUF *) (MPC860_REGB_OFFSET + ppc860SmcParms[i].smcRbdBa);

	/* tx buf base */
	ppc860Chan[i].uart.txBufBase = (u_char *) (MPC860_DPRAM_BASE(regBase) + ppc860SmcParms[i].smcTxBuf); 

	/* rx buf base */
	ppc860Chan[i].uart.rxBufBase = (u_char *) (MPC860_DPRAM_BASE(regBase) + ppc860SmcParms[i].smcRxBuf); 

	/* transmit buffer size */
	ppc860Chan[i].uart.txBufSize = ppc860SmcParms[i].smcTxBufSize;				


	/* SMC1 DPRAM addr params */
	ppc860Chan[i].uart.pSmc = (SMC *) ((UINT32) PPC860_DPR_SMC1(MPC860_DPRAM_BASE(regBase)) + (i*0x100));
			
	/* SMCMR1 for SMC1 */
	ppc860Chan[i].uart.pSmcReg = (SMC_REG *) ((UINT32) MPC860_SMCMR1(regBase) + (i*0x10));

	/* mask interrupts */

	ppc860Chan[i].uart.pSmcReg->smcm = 0x00;
	ppc860Chan[i].pBaud = (UINT32 *)((UINT32) MPC860_BRGC1(regBase) + (i * 4));
	ppc860Chan[i].channelMode = 0;

	/* configure the port lines */

	*MPC860_PBPAR(regBase) |= (0x00c0 << (i*4)); 
	*MPC860_PBDIR(regBase) &= ~(0x00c0 << (i*4));
	*MPC860_PBODR(regBase) &= ~(0x00c0 << (i*4));

	*MPC860_SDCR(regBase) = SDCR_RAID_BR5;

	/*         BRGC routing        */
	*MPC860_SIMODE(regBase) &=0xffff0fff;
	*MPC860_SIMODE(regBase) |= ( MPC860_SIMODE_SMC1_NMSI | MPC860_SIMODE_SMC1_BRG1 );

	*MPC860_SIMODE(regBase) &=0x0fffffff;
	*MPC860_SIMODE(regBase) |=( MPC860_SIMODE_SMC2_NMSI | MPC860_SIMODE_SMC2_BRG2 );

	ppc860DevInit (&(ppc860Chan[i]));
	}
}

/******************************************************************************
*
* sysSerialHwInit2 - connect BSP serial device interrupts
*
* This routine connects the BSP serial device interrupts.  It is called from
* sysHwInit2().  Serial device interrupts could not be connected in
* sysSerialHwInit() because the kernel memory allocator was not initialized
* at that point, and intConnect() calls malloc().
*
* RETURNS: N/A
*/ 

void sysSerialHwInit2 (void)
{

	int i;

    /* connect serial interrupts */

	for (i=0; i<NUM_TTY; i++) 
	{
		switch (i) 
		{
			case 0:
  				(void) intConnect (IV_SMC1, (VOIDFUNCPTR) ppc860Int, 
									(int) &ppc860Chan [i]);
									
				*CIMR(vxImmrGet()) |= (CIMR_SMC1 >> i);
									
				break;
			case 1:
  				(void) intConnect (IV_SMC2_PIP, (VOIDFUNCPTR) ppc860Int, 
  									(int) &ppc860Chan [i]);
  									
  				*CIMR(vxImmrGet()) |= (CIMR_SMC1 >> i);					

				break;
			
			default:
				break;;
				
		}
	}
}

/******************************************************************************
*
* sysSerialChanGet - get the SIO_CHAN device associated with a serial channel
*
* This routine gets the SIO_CHAN device associated with a specified serial
* channel.
*
* RETURNS: A pointer to the SIO_CHAN structure for the channel, or ERROR
* if the channel is invalid.
*/

SIO_CHAN * sysSerialChanGet(    int channel		/* serial channel */    )
{
   
	if(channel == 0 || channel == 1)
	{
		return ((SIO_CHAN *) &ppc860Chan [channel]);
	}
	return ((SIO_CHAN *) ERROR);
}

/*******************************************************************************
*
* sysSerialReset - reset the serial device 
*
* This function calls sysSerialHwInit() to reset the serial device
*
* RETURNS: N/A
*
*/

void sysSerialReset (void)
{
    sysSerialHwInit ();
}

