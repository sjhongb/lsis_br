#include "vxWorks.h"
#include "stdio.h"
#include "ioLib.h"
#include "fioLib.h"


unsigned char inBuf[255];

extern STATUS inetIpSetChange (char *ipStr, int what);


void 
menu()
{
	printf("\n\n\n");
	printf("===========================================================\n");
	printf("                 LKV402A IP SET Menu                       \n");
	printf("===========================================================\n");
	printf("    1.  Ethernet IP 0 Set                                  \n");
	printf("    2.  Ethernet IP 1 Set                                  \n");
	printf("    3.  quit                                               \n");
	printf("===========================================================\n");	
}

int
stringToHex(unsigned char *str)
{
	unsigned int hexValue;
	
	for(;  *str; ++str){
		if( *str >= '0' && *str <= '9')     hexValue = ((hexValue << 4) | (int)(*str - '0'));
		else if(*str >= 'a' && *str <= 'f') hexValue = ((hexValue << 4) | 10 + (int)(*str - 'a'));
		else if(*str >= 'A' && *str <= 'F') hexValue = ((hexValue << 4) | 10 + (int)(*str - 'A'));
		else break;
	}
	return(hexValue);
}

int 
stringToInt(char *ptr)
{
	int i;
	unsigned int value;
	value = 0;
	
	for(i = 0; ptr[i]; i++){
		if(ptr[i] >= '0' && ptr[i] <= '9'){
			value *= 10;
			value += ptr[i]-'0';
		}else break;
		
	}
	return value;
	
	
	printf("stringToInt value = %d\n", value);
	return(value);
}


int
getInputStr(unsigned char *ptr)
{
	unsigned char c, len;
	len=0;
	c=0;
	while(1){
		c = getchar();
		if(c== 0x0A) break;
		*ptr++ = (unsigned char)c;
		len++;
	}
	return(len);
}

void 
ipSet()
{
	int	menu_num, i;
    char string [40];

	while(1){	
		menu();
		for(i=0;i<5;i++){
			printf ("\nselect menu number (1~3) : ");
			fioRdString (STD_IN, string, sizeof (string));
			menu_num = stringToInt(string);
			if(menu_num < 1 || menu_num > 3)	printf("Press Valid Number(1~3) %d\n",menu_num);
			else	break;
		}
		switch(menu_num){
			case 1:
				printf("\n\n===========================================================\n");
				printf("         Ethernet IP 0 Setting                   \n");
				printf("===========================================================\n");
				printf("Target IP0 = ");
				getInputStr(inBuf);
				inetIpSetChange(inBuf, 0);
			break;
			case 2:
				printf("\n\n===========================================================\n");
				printf("         Ethernet IP 1 Setting                   \n");
				printf("===========================================================\n");
				printf("Target IP1 = ");
				getInputStr(inBuf);
				inetIpSetChange(inBuf, 1);
				
			break;
			case 3:
				printf("\nIP Setting End..... \n");
			return;
			break;
		}
	}
}

