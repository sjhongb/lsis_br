/* ctors and dtors arrays -- to be used by runtime system */
/*   to call static constructors and destructors          */
/*                                                        */
/* NOTE: Use a GNU C/C++ compiler to compile this file.   */

__asm__ (".data");
__asm__ (".global _ctors");
__asm__ (".align 4");
__asm__ ("_ctors:");
__asm__ ("    .long _GLOBAL_$I$CardErrCount");
__asm__ ("    .long _GLOBAL_$I$ComGPS");
__asm__ ("    .long _GLOBAL_$I$ComIF");
__asm__ ("    .long _GLOBAL_$I$HemmingCodeTable");
__asm__ ("    .long _GLOBAL_$I$OprLCC");
__asm__ ("    .long _GLOBAL_$I$__6CFlash");
__asm__ ("    .long _GLOBAL_$I$__stdstrbufs_o");
__asm__ ("    .long _GLOBAL_$I$__stlinst_o");
__asm__ ("    .long _GLOBAL_$I$__streambuf_o");
__asm__ ("    .long _GLOBAL_$I$app_tlbit_swap");
__asm__ ("    .long _GLOBAL_$I$g_bPeerCheckTriggerPRI");
__asm__ ("    .long 0");
__asm__ (".global _dtors");
__asm__ (".align 4");
__asm__ ("_dtors:");
__asm__ ("    .long _GLOBAL_$D$app_tlbit_swap");
__asm__ ("    .long _GLOBAL_$D$__streambuf_o");
__asm__ ("    .long _GLOBAL_$D$__stlinst_o");
__asm__ ("    .long _GLOBAL_$D$__stdstrbufs_o");
__asm__ ("    .long _GLOBAL_$D$OprLCC");
__asm__ ("    .long _GLOBAL_$D$HemmingCodeTable");
__asm__ ("    .long _GLOBAL_$D$ComIF");
__asm__ ("    .long _GLOBAL_$D$CardErrCount");
__asm__ ("    .long 0");
