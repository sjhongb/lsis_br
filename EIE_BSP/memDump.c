#include <stdio.h>
#include <string.h>

#include <vxWorks.h>
#include <ioLib.h>
#include <fioLib.h>
#include <ctype.h>
#include <taskLib.h>
#include <kernelLib.h>/*test*/

#include "kvme402a.h"



#define	M_MAX_LINE        160

void	dump(int *adrs)			/* If 0, print 64 or last specified. */
{
	int	value;
	
    adrs = (int *) ((int) adrs & ~1);	/* round adrs down to word boundary */

	value = *(char *)adrs;
	
    printf ("addr : 0x%x:  value : 0x%x\n", (int) adrs & ~0xf, value);
}

void	dShow()
{
	int	adrs, value;
	
	adrs = 0x1200c202;
	*(char *)adrs = 0x8;
	value = *(char *)adrs;
	
	printf("address : 0x%x value : 0x%x\n", adrs, value);
}



/*******************************************************************************
*
* m - modify memory
* mb - modify memory(by Byte)
* mw - modify memory(by Word)
* ml - modify memory(by Long Word)
*
* This routine prompts the user for modifications to memory, starting at the
* specified address.  It prints each address, and the current contents of
* that address, in turn.  The user can respond in one of several ways:
*
*	RETURN   - No change to that address, but continue
*		   prompting at next address.
*	<number> - Set the contents to <number>.
*	. (dot)	 - No change to that address, and quit.
*	<EOF>	 - No change to that address, and quit.
*
* All numbers entered and displayed are in hexadecimal.
* Memory is treated as 16-bit words.
*/

void mb(char *adrs)
{
    char line [M_MAX_LINE + 1];	/* leave room for EOS */
    char *pLine;		/* ptr to current position in line */
    int value;			/* value found in line */
    char excess;

    /* round down to word boundary */
    for (adrs = (char *) ((int) adrs & 0xfffffffe);	/* start on even addr */
         ;											/* FOREVER */
		 adrs = (char *) (((char *) adrs) + 1)){	/* bump as short ptr */

		printf ("%06x:  %02x-", (int) adrs, (*(char *)adrs) & 0x0000ffff);
		if (fioRdString (STD_IN, line, M_MAX_LINE) == EOF)	    break;
		line [M_MAX_LINE] = EOS;		/* make sure input line has EOS */
		for (pLine = line; isspace ((UINT) * pLine); ++pLine)	;
		if (*pLine == EOS)	continue;	/* skip field if just CR */
		/* quit if not number */
		if (sscanf (pLine, "%x%1s", &value, &excess) != 1)	break;	
		* (char *) adrs = value;		/* assign new value */
	}

    printf ("\n");
}

void mw(char *adrs)
{
    char line [M_MAX_LINE + 1];	/* leave room for EOS */
    char *pLine;		/* ptr to current position in line */
    int value;			/* value found in line */
    char excess;

    /* round down to word boundary */
    for (adrs = (char *) ((int) adrs & 0xfffffffe);					/* start on even addr */
         ;															/* FOREVER */
		 adrs = (char *) (((short *) adrs) + 1)){					/* bump as short ptr */

		printf ("%06x:  %04x-", (int) adrs, (*(short *)adrs) & 0x0000ffff);
		if (fioRdString (STD_IN, line, M_MAX_LINE) == EOF)		    break;
		line [M_MAX_LINE] = EOS;									/* make sure input line has EOS */
		for (pLine = line; isspace ((UINT) * pLine); ++pLine)	;
		if (*pLine == EOS)	continue;								/* skip field if just CR */
		if (sscanf (pLine, "%x%1s", &value, &excess) != 1)	break;	/* quit if not number */
		* (short *) adrs = value;		/* assign new value */
	}

    printf ("\n");
}

void ml(char *adrs)
{
    char line [M_MAX_LINE + 1];	/* leave room for EOS */
    char *pLine;		/* ptr to current position in line */
    int value;			/* value found in line */
    char excess;

    /* round down to word boundary */
    for (adrs = (char *) ((int) adrs & 0xfffffffe);					/* start on even addr */
         ;															/* FOREVER */
		 adrs = (char *) (((int *) adrs) + 1)){					/* bump as short ptr */

		printf ("%06x:  %08x-", (int) adrs, (*(int *)adrs) & 0xffffffff);
		if (fioRdString (STD_IN, line, M_MAX_LINE) == EOF)		    break;
		line [M_MAX_LINE] = EOS;									/* make sure input line has EOS */
		for (pLine = line; isspace ((UINT) * pLine); ++pLine)	;
		if (*pLine == EOS)	continue;								/* skip field if just CR */
		if (sscanf (pLine, "%x%1s", &value, &excess) != 1)	break;	/* quit if not number */
		* (int *) adrs = value;		/* assign new value */
	}

    printf ("\n");
}


/*******************************************************************************
*
* db - display memory(by Byte)
* dw - display memory(by Word)
* dl - display memory(by Long Word)
*
* Display contents of memory, starting at adrs.  Memory is displayed in
* words.  The number of words displayed defaults to 64.  If
* nwords is non-zero, that number of words is printed, rounded up to
* the nearest number of full lines.  That number then becomes the default.
*/

void db(FAST char *adrs, int nwords)			/* If 0, print 64 or last specified. */
{
    static char *last_adrs;
    static int dNbytes = 128;
    char ascii [17];
    FAST int nbytes, byte;

    ascii [16] = EOS;			/* put an EOS on the string */
    nbytes = 2 * nwords;

    if (nbytes == 0)	nbytes = dNbytes;	/* no count specified: use current byte count */
    else				dNbytes = nbytes;	/* change current byte count */

    if (adrs == 0)		adrs = last_adrs;	/* no address specified: use last address */

    adrs = (char *) ((int) adrs & ~1);	/* round adrs down to word boundary */

    /* print leading spaces on first line */

    bfill ((char *) ascii, 16, '.');
    printf ("%06x:  ", (int) adrs & ~0xf);
    
    for (byte = 0; byte < ((int) adrs & 0xf); byte++){
		printf ("  ");
		if (byte & 1)	    printf (" ");	/* space between words */
		if (byte == 7)	    printf (" ");	/* extra space between words 3 and 4 */

		ascii [byte] = ' ';
	}

    while (nbytes-- > 0){
		if (byte == 16){
		    printf ("  *%16s*\n%06x:  ", ascii, (int) adrs);

		    bfill ((char *) ascii, 16, '.');	/* clear out ascii buffer */
	    	byte = 0;				/* reset word count */
	    }

		printf ("%02x", *adrs & 0x000000ff);

		if (byte & 1)		printf (" ");	/* space between words */
		if (byte == 7)		printf (" ");	/* extra space between words 3 and 4 */
		if (* adrs == ' ' || (isascii ((UINT) * adrs) && isprint ((UINT) * adrs)))	    ascii [byte] = (UINT) * adrs;
	
		adrs++;
		byte++;
	}

    for (; byte < 16; byte++){
		printf ("  ");
		if (byte & 1)		printf (" ");	/* space between words */
		if (byte == 7)		printf (" ");	/* extra space between words 3 and 4 */
	
		ascii [byte] = ' ';
	}

    printf ("  *%16s*\n", ascii);	/* print out ascii format values */
    last_adrs = adrs;
}

void dw
    (
    FAST char *adrs,	/* address to display */
    int	       nwords	/* number of words to print. */
    )			/* If 0, print 64 or last specified. */
    {
    static char *last_adrs;
    static int dNbytes = 128;
    char ascii [17];
    FAST int nbytes;
    FAST int byte;

    ascii [16] = EOS;			/* put an EOS on the string */

    nbytes = 2 * nwords;

    if (nbytes == 0)
	nbytes = dNbytes;	/* no count specified: use current byte count */
    else
	dNbytes = nbytes;	/* change current byte count */

    if (adrs == 0)
	adrs = last_adrs;	/* no address specified: use last address */

    adrs = (char *) ((int) adrs & ~1);	/* round adrs down to word boundary */


    /* print leading spaces on first line */

    bfill ((char *) ascii, 16, '.');

    printf ("%06x:  ", (int) adrs & ~0xf);

    for (byte = 0; byte < ((int) adrs & 0xf); byte++)
	{
	printf ("  ");
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	ascii [byte] = ' ';
	}


    /* print out all the words */

    while (nbytes-- > 0)
	{
	if (byte == 16)
	    {
	    /* end of line:
	     *   print out ascii format values and address of next line */

	    printf ("  *%16s*\n%06x:  ", ascii, (int) adrs);

	    bfill ((char *) ascii, 16, '.');	/* clear out ascii buffer */
	    byte = 0;				/* reset word count */
	    }

	printf ("%02x", *adrs & 0x000000ff);
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	if (* adrs == ' ' ||
			(isascii ((UINT) * adrs) && isprint ((UINT) * adrs)))
	    ascii [byte] = (UINT) * adrs;

	adrs++;
	byte++;
	}


    /* print remainder of last line */

    for (; byte < 16; byte++)
	{
	printf ("  ");
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	ascii [byte] = ' ';
	}

    printf ("  *%16s*\n", ascii);	/* print out ascii format values */

    last_adrs = adrs;
    }

void dl
    (
    FAST char *adrs,	/* address to display */
    int	       nwords	/* number of words to print. */
    )			/* If 0, print 64 or last specified. */
    {
    static char *last_adrs;
    static int dNbytes = 128;
    char ascii [17];
    FAST int nbytes;
    FAST int byte;

    ascii [16] = EOS;			/* put an EOS on the string */

    nbytes = 2 * nwords;

    if (nbytes == 0)
	nbytes = dNbytes;	/* no count specified: use current byte count */
    else
	dNbytes = nbytes;	/* change current byte count */

    if (adrs == 0)
	adrs = last_adrs;	/* no address specified: use last address */

    adrs = (char *) ((int) adrs & ~1);	/* round adrs down to word boundary */


    /* print leading spaces on first line */

    bfill ((char *) ascii, 16, '.');

    printf ("%06x:  ", (int) adrs & ~0xf);

    for (byte = 0; byte < ((int) adrs & 0xf); byte++)
	{
	printf ("  ");
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	ascii [byte] = ' ';
	}


    /* print out all the words */

    while (nbytes-- > 0)
	{
	if (byte == 16)
	    {
	    /* end of line:
	     *   print out ascii format values and address of next line */

	    printf ("  *%16s*\n%06x:  ", ascii, (int) adrs);

	    bfill ((char *) ascii, 16, '.');	/* clear out ascii buffer */
	    byte = 0;				/* reset word count */
	    }

	printf ("%02x", *adrs & 0x000000ff);
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	if (* adrs == ' ' ||
			(isascii ((UINT) * adrs) && isprint ((UINT) * adrs)))
	    ascii [byte] = (UINT) * adrs;

	adrs++;
	byte++;
	}


    /* print remainder of last line */

    for (; byte < 16; byte++)
	{
	printf ("  ");
	if (byte & 1)
	    printf (" ");	/* space between words */
	if (byte == 7)
	    printf (" ");	/* extra space between words 3 and 4 */

	ascii [byte] = ' ';
	}

    printf ("  *%16s*\n", ascii);	/* print out ascii format values */

    last_adrs = adrs;
    }

void mdumpTest(unsigned int *addrs, unsigned int data)
{
	*(unsigned int*)addrs = data;
	printf("address = 0x%x, data = 0x%x\n", (UINT32) addrs, *addrs);
}


void memWrite(unsigned char *memAdrs, unsigned char data)
{
	*(unsigned char *)memAdrs = data;
}

int memRead(unsigned char *memAdrs)
{
	unsigned char memData;
	memData = *(unsigned char*)memAdrs;
	return(memData);
}


void nvRamDump()
{
	int i;
	unsigned char *nvramAdrs;
	unsigned int nvSize;
	unsigned char nvData;
	
	nvData=0;
	nvSize = NV_RAM_SIZE - 0x500;
	nvramAdrs = (UINT8) (NV_RAM_ADRS + NV_BOOT_OFFSET + 0x100);
	
	printf("Start Address = 0x%x, Size = 0x%x\n", (int) nvramAdrs, nvSize);
	
	
	for(i=0; i<nvSize; i++){
		memWrite(nvramAdrs++, nvData++); 
	}
	
	nvData=0;
	nvramAdrs = (UINT8) (NV_RAM_ADRS + NV_BOOT_OFFSET + 0x100);
	
	for(i=0; i<nvSize; i++){
		if(nvData != memRead(nvramAdrs)){
			printf("NVRAM Read/Write Error!!\n");
			printf("Address [0x%x] Data[0x%x]\n", (int) nvramAdrs, nvData);
			break;
		}else{
			nvData++;
			nvramAdrs++;
		}
	}
}

