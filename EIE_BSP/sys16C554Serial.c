/* sysSerial.c -  Yellowknife serial device initialization */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,30jun05,erc  added sysSerialReset..
01a,29apr99,jkf  merged into T2.
01a,01apr99,jkf  created based from mcp750 BSP sysSerial.c
*/

/*
DESCRIPTION
This file contains the board-specific routines for serial channel
initialization.

*/

#include "vxWorks.h"
#include "config.h"
#include "intLib.h"
#include "iv.h"
#include "sysLib.h"
#include "config.h"
#include "kvme402a.h"

#include "tl16c554Sio.h" 		/* TODO use ns16550Sio driver */

#include "drv/multi/ppc860Cpm.h"
#include "drv/multi/ppc860Siu.h"

/* device INIT structs */

typedef struct
    {
    	USHORT vector;				/* Interrupt vector */
    	ULONG  baseAdrs;			/* Register base address */
    	USHORT regSpace;			/* Address Interval */
    	USHORT intLevel;			/* Interrupt level */
    } TL16C554_CHAN_PARAS;

/* static variables */

static TL16C554_CHAN  tl16c554Chan[N_UART_CHANNELS];

static TL16C554_CHAN_PARAS devParas[] = {
	{(USHORT)IV_SIO_3, COM1_BASE_ADR, UART_REG_ADDR_INTERVAL, (USHORT) IV_IRQ7},
	{(USHORT)IV_SIO_3, COM2_BASE_ADR, UART_REG_ADDR_INTERVAL, (USHORT) IV_IRQ7},
	{(USHORT)IV_SIO_3, COM3_BASE_ADR, UART_REG_ADDR_INTERVAL, (USHORT) IV_IRQ7},
	{(USHORT)IV_SIO_3, COM4_BASE_ADR, UART_REG_ADDR_INTERVAL, (USHORT) IV_IRQ7}
};

/*
 * Array of pointers to all serial channels configured in system.
 
 * See sioChanGet(). It is this array that maps channel pointers
 * to standard device names.  The first entry will become "/tyCo/0",
 * the second "/tyCo/1", and so forth.
 *
 */

SIO_CHAN * sysSioChans [N_SIO_CHANNELS] =
    {
    	(SIO_CHAN *)&tl16c554Chan[0].pDrvFuncs,	/* /tyCo/0 */
    	(SIO_CHAN *)&tl16c554Chan[1].pDrvFuncs,	/* /tyCo/1 */
		(SIO_CHAN *)&tl16c554Chan[2].pDrvFuncs,	/* /tyCo/2 */
    	(SIO_CHAN *)&tl16c554Chan[3].pDrvFuncs,	/* /tyCo/3 */        
    };

/******************************************************************************
*
* sysSerialHwInit - initialize the BSP serial devices to a quiescent state
*
* This routine initializes the BSP serial device descriptors and puts the
* devices in a quiescent state.  It is called from sysHwInit() with
* interrupts locked.   Polled mode serial operations are possible, but not
* interrupt mode operations which are enabled by sysSerialHwInit2().
*
* RETURNS: N/A
*
* SEE ALSO: sysHwInit()
*/ 

void sys16c554SerialHwInit (void)
{
	int i,j;

	for (i = 0; i < N_UART_CHANNELS; i++)
	{
		for(j=0; j<5; j++){
			tl16c554Chan[i].regs		= (UINT8*)devParas[i].baseAdrs; taskDelay(5);
			tl16c554Chan[i].level		= devParas[i].intLevel;         taskDelay(5);
			tl16c554Chan[i].ier			= 0x01;                         taskDelay(5);  
			tl16c554Chan[i].lcr			= 0x83;                         taskDelay(5);
			tl16c554Chan[i].regDelta	= devParas[i].regSpace;         taskDelay(5);
			tl16c554Chan[i].xtal		= 7372800;		                taskDelay(5);
			tl16c554Chan[i].baudRate	= 38400;/*9600*/                taskDelay(5);
			tl16c554Chan[i].chanNum 	= i;                            taskDelay(5);
			
			/* reset the device */
        	
			tl16c554DevInit(&tl16c554Chan[i]);
		}
	}

}

/******************************************************************************
*
* sysSerialHwInit2 - connect BSP serial device interrupts
*
* This routine connects the BSP serial device interrupts.  It is called from
* sysHwInit2().  
*
* Serial device interrupts cannot be connected in sysSerialHwInit() because
* the  kernel memory allocator is not initialized at that point and
* intConnect() calls malloc().
*
* RETURNS: N/A
*
* SEE ALSO: sysHwInit2()
*/
int interruptflag=0; 
extern void tl16c554TxStartup(TL16C554_CHAN * pChan);
void tl16c554IntFunc(int parameter)
{
	int i,j;

		intDisable(IV_SCC3);
		intDisable(IV_SCC4);
		intDisable(IV_SMC1);
		intDisable(IV_SMC2_PIP);
		if(interruptflag == 0){
			interruptflag=1;
			for(i=0; i<N_UART_CHANNELS; i++)
			{	
				tl16c554Int(&tl16c554Chan[i]);		
			}
			interruptflag=0;
		}
		intEnable(IV_SCC3);
		intEnable(IV_SCC4);
		intEnable(IV_SMC1);
		intEnable(IV_SMC2_PIP);
}

void sys16c554SerialHwInit2 (void)
{
	
	intConnect(IV_SIO_3, tl16c554IntFunc, NULL);

	*SIMASK (vxImmrGet()) |= SIMASK_IRM7;
	intEnable((int)IV_SIO_3);

}


/******************************************************************************
*
* sysSerialChanGet - get the SIO_CHAN device associated with a serial channel
*
* This routine returns a pointer to the SIO_CHAN device associated
* with a specified serial channel.  It is called by usrRoot() to obtain 
* pointers when creating the system serial devices, `/tyCo/x'.  It
* is also used by the WDB agent to locate its serial channel.
*
* RETURNS: A pointer to the SIO_CHAN structure for the channel, or ERROR
* if the channel is invalid.
*/

SIO_CHAN * sys16c554SerialChanGet(int channel         /* serial channel */)
{
    if( (channel < 0) || (channel >= NELEMENTS(sysSioChans)) )	return (SIO_CHAN *) ERROR;

    return sysSioChans[channel];
}


/******************************************************************************
 *
 * sysSerialReset - reset the serial device (tl16c554)
 *
 * This function calls sysSerialHwInit(0 to reset the serial device
 * 
 * RETURN : N/A
 * added by ERIC...2005.06.30.
 *
 */

void
sys16c554SerialReset (void)
{

	sys16c554SerialHwInit ();
	
}

void tl16c554RegShow()
{
	int i,j;
	unsigned char *adrs;
	
		
		adrs = 0xf3000000;
		
			for(i=0; i<4; i++){
				printf("RBR_THR_DLL -------> [0x%02x]\n", *(adrs+0 +(i*0x20)));
				printf("DLM_IER     -------> [0x%02x]\n", *(adrs+4 +(i*0x20)));
				printf("FCR_IIR     -------> [0x%02x]\n", *(adrs+8 +(i*0x20)));
				printf("LCR         -------> [0x%02x]\n", *(adrs+12+(i*0x20)));
				printf("MCR         -------> [0x%02x]\n", *(adrs+16+(i*0x20)));
				printf("LSR         -------> [0x%02x]\n", *(adrs+20+(i*0x20)));
				printf("MSR         -------> [0x%02x]\n", *(adrs+24+(i*0x20)));
				printf("SCR         -------> [0x%02x]\n", *(adrs+28+(i*0x20)));
				printf("\n\n");
			}
		taskDelay(500);
	
}



