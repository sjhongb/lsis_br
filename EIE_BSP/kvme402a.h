/************************************************************************
*		kvme408.h - Etin System HTCS CPU board header					*
*																		*
*		Copyright 1984-1998 Wind River Systems, Inc.					*
* 		Copyright 1997,1998 Etin Systems, Inc., All Rights Reserved		*
************************************************************************/

/************************************************************************
Modification history
--------------------
2001.09.22	PDS	boot parameter is saved at bottom of NV_RAM	
************************************************************************/


#ifndef	INCkvme408h
#define	INCkvme408h

#include "drv/mem/memDev.h"
#include "drv/intrCtl/ppc860Intr.h"

/* define target communication board type */
#define	_COMM_LIKE_CCU_		1
								

/**********************************************
*			Debug Tool					*
**********************************************/
#define DEBUG_NON		0
#define DEBUG_IOCTL		1
#define DEBUG_RUNNING		2
#define DEBUG_INTERRUPT		3
#define LOGMSG_NVRAM		4
#define	DEBUG_TIME		5

#define DEBUG_FLAG		DEBUG_NON

/**********************************************
*			Debug Tool					*
**********************************************/

#define	FLOW_CONTROL						/*	For Scc Flow control	*/

#define BUS				VME_BUS				 
#define CPU				PPC860				/* CPU type */
#define	COMPANY_MAC_ID_E1	0x71				/*	Etin System Check ID	*/
#define COMPANY_MAC_ID_E2	0x72

#define	N_SIO_CHANNELS	4
#define N_UART_CHANNELS N_SIO_CHANNELS

#define SMC_N_SIO_CHANNELS 2



/* Serial Ports */

#define COM1_BASE_ADR			0xF3000000
#define COM2_BASE_ADR			0xF3000020
#define COM3_BASE_ADR			0xF3000040
#define COM4_BASE_ADR			0xF3000060
#define UART_REG_ADDR_INTERVAL  4
#define BAUD_CLK_FREQ			7372800

#undef  CONSOLE_BAUD_RATE	
#define CONSOLE_BAUD_RATE		38400



#define ENET_NUM		2

#define	MAC_ADDR_NUMBER	6

/* Macro for all i/o operations to use */
#define SYNC  __asm__("	sync")

/***************************************************************************
 * Crystal Frequency - This macro defines the input oscillator frequency
 * clocking the PPC860. On the ADS board, the CPU is clocked by a crystal
 * running at 4 Mhz.  For the MBX boards, the frequency should never change
 * from 32KHZ
***************************************************************************/
#define CRYSTAL_FREQ		32768           /* 32.768 Khz */
#define SCC_FREQ_CLK		7372800         /* 7,3728 Mhz */
#define CLK6_FREQ			SCC_FREQ_CLK

#define SYS_DELAY			50000		/* loop counter for sysDelay */
#define FREQ_50_MHZ			50000000	

#define PTP_VALUE			MPTPR_PTP_DIV16
#define PTP_DIVISOR			(64 / (PTP_VALUE >> 8))

#define SYS_REF_CLK_FREQ	FREQ_50_MHZ
#define SYS_INT_CLK_SPD		FREQ_50_MHZ
#define SPLL_MUL_FACTOR 	((SPLL_FREQ_REQUESTED / SYS_REF_CLK_FREQ) - 1)
#define SPLL_FREQ			((SPLL_MUL_FACTOR + 1) * SYS_REF_CLK_FREQ)
#define BRGCLK_FREQ			(SPLL_FREQ / (1 << (2 * BRGCLK_DIV_FACTOR)))
#define REFRESH_VALUE		(BRGCLK_FREQ / DRAM_REFRESH_FREQ)
#define PTA_VALUE			(REFRESH_VALUE / PTP_DIVISOR)

#define MAX_MPU_SPEED		50000000
#define MEM_SIZE_8MB		0x00800000
#define MEM_SIZE_32MB		0x02000000
#define MEM_SPEED_10NS		0x00000010

/* PPC Decrementer - used as vxWorks system clock */

#define	DELTA(a, b)			(abs((int) a - (int)b))

/* define the decrementer input clock frequency, using in ppcDecTimer.c */
#define DEC_CLOCK_FREQ		SPLL_FREQ_REQUESTED
#define DEC_CLK_TO_INC  	16

/* define system clock reate */
#define SYS_CPU_FREQ		SPLL_FREQ

/* Internal Memory Map base Address */
#define INTERNAL_MEM_MAP_ADDR	0xFF000000	
#define INTERNAL_MEM_MAP_SIZE	0x00010000		/* org = 0x0c00 */

/* MPC860 Register Offsets */
#define BASE_REG0_OFFSET	0x0100


/********************************************************************
*					Memory/Device Base Addresses					*
********************************************************************/
/* SDRAM Address */
#define SDRAM_BA				0x00000000 		/* sdram base addr */
#define SDRAM_SIZE				MEM_SIZE_32MB	/* dram size : 32MB */

/* VME Extended Address */
#define VME_EXTENDED_ADDR		0x10000000
#define VME_EXTENDED_ADDR_SIZE	0xE0000000		/* 3GB size */

/* VME standard Address */
#define VME_STAND_ADDR			0xF0000000
#define VME_STAND_ADDR_SIZE		0x01000000		/* 16MB size */

/* SRAM Address */
#define SRAM_BA					0xF1000000
#define SRAM_SIZE				0x00200000		/* 2MB addressing size */

/* SERIAL CHIP Address */
#define SERIAL_BA				0xF3000000
#define SERIAL_MEM_SIZE			0x00000100

/* VME short Address */
#define VME_SHORT_ADDR			0xF4000000
#define VME_SHORT_ADDR_SIZE		0x00010000		/* 64KB size */

/* ERROR_LED Address */
#define	ERR_LED_OFF_ADR			0xF5000000

#define	ERR_LED_ON_ADR			0xF9000000		/* new logic */

/*	Watch Dog Timer Address */
#define WATCH_DOG_TIMER_ADR		0xF6000001	

/* ENCODER_SW Address */
#define ENC_SW_BA				0xF7000000
#define GET_ENC_SW_VAL			*(unsigned char *) ENC_SW_BA

/*8-bit DIP_SW Address */

#define DIP_SW_BA				0xF7000002
#define GET_DIP_VAL				*(unsigned char *) DIP_SW_BA

/*	FLASH Address */

#define	FLASH_BA				0xFA000000
#define FLASH_SIZE				0x01000000		/* 4M Byte	*/

/* Digital In / Out */
#if 0
#define	DIG_IN_ADRS				0xFE000000
#else
#define	DIG_IN_ADRS				0xF7000004
#endif

/******************************************************** 
* NVRAM definitions *

	+--------------------+ NV _RAM_BA
	|                    |
	|     NV RAM area    |
	|                    |
	+--------------------+ NV_BOOT_OFFSET
	| Boot parameter area|
	+--------------------+ MV_RAM_BA+NV_RAM_SIZE - 0x8
	|      RTC area      |
	+--------------------+ NV_RAM_BA + NV_RAM_SIZE
***************************************************************/

/* NV_RAM Address */
#define NV_RAM_ADRS			0xF2000000
#define NV_RAM_SIZE			0x00080000	/* 512KB for KVME402 Rev3.0 128KB default for NVRAM */
#define NV_RAM_USER_AREA 	0x00000000	/* start of NVRAM user area */

#define NV_RAM_INTRVL		1

#undef  NV_BOOT_OFFSET

#define NV_BOOT_OFFSET		0x200

#define	BBRAM_SIZE			NV_RAM_SIZE


/************************************************************************
*							Ethernet parameters							*
************************************************************************/
#ifdef  INCLUDE_CPM			/* CPM ethernet driver Include*/
/*	Scc1 Ether Set	*/
#define INCLUDE_IF_USR
#define IF_USR_NAME		"cpm"                   /* device name */
#define IF_USR_ATTACH	cpmattach               /* driver attach routine */
#define IF_USR_ARG1		(char *)(INTERNAL_MEM_MAP_ADDR + 0x3c00)
#define IF_USR_ARG2		(int)(INTERNAL_MEM_MAP_ADDR + 0x0a00)
#define IF_USR_ARG3		(int) IV_SCC1           /* int number for SCC1 */
#define IF_USR_ARG4		(int)(INTERNAL_MEM_MAP_ADDR + 0x2600)	/*	<- 2000	*/
												/* address of transmit BDs */
#define IF_USR_ARG5		(int)(INTERNAL_MEM_MAP_ADDR + 0x2700)	/*	<-2100	*/
												/* address of receive BDs */
#define IF_USR_ARG6		(int) 0x20              /* number of transmit BDs */
#define IF_USR_ARG7		(int) 0x20              /* number of receive BDs */
#define IF_USR_ARG8		(int) NONE              /* allocate mem for buffers */

/*	Scc2 Ether Set	*/
#define INCLUDE_IF_USR1
#define IF_USR1_ARG1	(char *)(INTERNAL_MEM_MAP_ADDR + 0x3d00)
#define IF_USR1_ARG2	(int)(INTERNAL_MEM_MAP_ADDR + 0x0a20)
#define IF_USR1_ARG3	(int) IV_SCC2
#define IF_USR1_ARG4	(int)(INTERNAL_MEM_MAP_ADDR + 0x2800)	/*	<-2800	*/
#define IF_USR1_ARG5	(int)(INTERNAL_MEM_MAP_ADDR + 0x2900)	/*	<-2900	*/
#define IF_USR1_ARG6	(int) 0x20
#define IF_USR1_ARG7	(int) 0x20
#define IF_USR1_ARG8	(int) NONE
#endif  /* INCLUDE_CPM */


#define DPRAM_ADDRESS		(INTERNAL_MEM_MAP_ADDR + 0x2000)

/************************************************************************
*	The following are used to define space in DPRAM that is only		*
*	used during system bootup											*
************************************************************************/
#define DPRAM_STACKFRM_BA	(INTERNAL_MEM_MAP_ADDR + 0x2e00)
#define DPRAM_STACK_ALLOC	-0x0020
#define DPRAM_STACK_DEALLOC	0x0020

/* RTC register values */
#define RTC_KEY_VALUE		0x55CCAA33

/* defines for the default configuration values */
#define DEFAULT_CLOCK_SPEED	FREQ_50_MHZ
#define DEFAULT_DRAM_SIZE	MEM_SIZE_32MB
#define DEFAULT_FLASH_SIZE	MEM_SIZE_2MB
#define DEFAULT_MEM_SPEED	MEM_SPEED_10NS

/* Baud Rate Generator division factors */
#define BRG_DIV_BY_1		0
#define BRG_DIV_BY_4		1
#define BRG_DIV_BY_16		2
#define BRG_DIV_BY_64		3

#undef	NUM_VEC_MAX
#define NUM_VEC_MAX			255	/*	255 	*/

/* Interrupt Vector/Number to Interrupt Level */
#define	IVEC_TO_ILVL(X)		(UINT) IVEC_TO_INUM(X)
#define	INUM_TO_ILVL(X)		(UINT) (X)

/* Interrupt Classes */
#define	INT_CLASS_PPC860	0x1
#define	INT_CLASS_RES		0x2
#define	INT_CLASS_CPM		0x3
#define	INT_CLASS_ISA_PCI	0x4
#define	INT_CLASS_UNDEF		0x5

/* Interrupt Level to Interrupt Class */
#define INT_CLASS(lev)  (lev <= 15 ? INT_CLASS_PPC860 :                      \
                        (lev <= 31 ? INT_CLASS_RES :                         \
                        (lev <= 63 ? INT_CLASS_CPM :                         \
                        (lev <= 79 ? INT_CLASS_ISA_PCI : INT_CLASS_UNDEF))))

/* defines for interrupt sources into the SIU */
#define	IV_POWERFAIL	IV_IRQ0		/* Power Fail */
#define	IV_ZCROSS		IV_IRQ1		

#define	IV_CPM_CTLR		IV_LEVEL4	/* CPM */
#define	IV_COMINT		IV_IRQ6		/* COMMINT_L */

#if 1
#define	IV_SIO_0			IV_IRQ2		
#define	IV_SIO_1			IV_IRQ4		
#define	IV_SIO_2			IV_IRQ6
#define	IV_SIO_3			IV_IRQ7		
#else
#define	IV_SIO			IV_IRQ7		
#endif

#define	IV_RTC			IV_LEVEL0	/* Real-Time Clock */
#define	IV_PIT			IV_LEVEL1	/* Periodic Intr Tmr */
#define	IV_TIMEBASE		IV_LEVEL2	/* Timebase Cntr */

#define IO_SYNC __asm__("      sync")

/*	for Digital In/Out Channel	*/
#define	DI_ON		1
#define	DI_OFF		0
#define	DO_ON		1
#define	DO_OFF		0

/*	for VME AMCODE control	*/
#define	CLEAR_AMCODE_AREA		0x7
#define	NON_PRIVILEGED_DATA		0x1
#define	NON_PRIVILEGED_PROG		0x2
#define	SUPERVISORY_DATA		0x5
#define	SUPERVISORY_PROG		0x6

/*	for led control	*/
#define DIAG0_ON	0
#define DIAG0_OFF	1	
#define DIAG1_ON	2	
#define DIAG1_OFF	3	

#define	SCC_RTS_1	0x2000
#define	SCC_RTS_2	0x4000
#define	SCC_DTR_1	0x0002
#define	SCC_DTR_2	0x8000

#define	RTS_ON		1
#define	RTS_OFF		0
#define	DTR_ON		1
#define	DTR_OFF		0


/* Port A,B,C Defines  */
#define PA15    (0x0001)
#define PA14    (0x0002)
#define PA13    (0x0004)
#define PA12    (0x0008)
#define PA11    (0x0010)
#define PA10    (0x0020)
#define PA9     (0x0040)
#define PA8     (0x0080)
#define PA7     (0x0100)
#define PA6     (0x0200)
#define PA5     (0x0400)
#define PA4     (0x0800)
#define PA3     (0x1000)
#define PA2     (0x2000)
#define PA1     (0x4000)
#define PA0     (0x8000)

#define PB31    (0x00001)
#define PB30    (0x00002)
#define PB29    (0x00004)
#define PB28    (0x00008)
#define PB27    (0x00010)
#define PB26    (0x00020)
#define PB25    (0x00040)
#define PB24    (0x00080)
#define PB23    (0x00100)
#define PB22    (0x00200)
#define PB21    (0x00400)
#define PB20    (0x00800)
#define PB19    (0x01000)
#define PB18    (0x02000)
#define PB17    (0x04000)
#define PB16    (0x08000)
#define PB15    (0x10000)
#define PB14    (0x20000)

#define PC15    (0x0001)
#define PC14    (0x0002)
#define PC13    (0x0004)
#define PC12    (0x0008)
#define PC11    (0x0010)
#define PC10    (0x0020)
#define PC9     (0x0040)
#define PC8     (0x0080)
#define PC7     (0x0100)
#define PC6     (0x0200)
#define PC5     (0x0400)
#define PC4     (0x0800)

#define PD15	(0x0001)
#define PD14	(0x0002)
#define PD13	(0x0004)
#define PD12	(0x0008)
#define PD11	(0x0010)
#define PD10	(0x0020)
#define PD9		(0x0040)
#define PD8		(0x0080)
#define PD7		(0x0100)
#define PD6		(0x0200)
#define PD5		(0x0400)
#define PD4		(0x0800)
#define PD3		(0x1000)




#endif /* INCkvme408h */


