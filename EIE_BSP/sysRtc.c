/*
 * (C) Copyright 2004
 * Sangmoon Kim, ETIN SYSTEMS Co.,Ltd. <dogoil@etinsys.com>
 * VxWorks RTC driver for Debris board
 *
 * Revision History
 * 2004-09-16 : First crated by Sangmoon Kim to replace phantom.c
 * 2004-09-17 : Add sysClkRateAdjust, date by Sangmoon Kim
 * 2004-10-12 : Initialize freq, tlb in sysClkRateAdjust function
 * 		to avoid warnings by Sangmoon Kim
 */

#include <vxWorks.h>
#include <time.h>
#include <dosFsLib.h>
#include "stdio.h"
#include "config.h"
#include "fioLib.h"
#include "sysLib.h"
#include "taskLib.h"
#include "kernelLib.h"

#if 1

unsigned char tlbit_swap[256] = {
0x00,0x80,0x40,0xc0,0x20,0xa0,0x60,0xe0,
0x10,0x90,0x50,0xd0,0x30,0xb0,0x70,0xf0,
0x08,0x88,0x48,0xc8,0x28,0xa8,0x68,0xe8,
0x18,0x98,0x58,0xd8,0x38,0xb8,0x78,0xf8,
0x04,0x84,0x44,0xc4,0x24,0xa4,0x64,0xe4,
0x14,0x94,0x54,0xd4,0x34,0xb4,0x74,0xf4,
0x0c,0x8c,0x4c,0xcc,0x2c,0xac,0x6c,0xec,
0x1c,0x9c,0x5c,0xdc,0x3c,0xbc,0x7c,0xfc,
0x02,0x82,0x42,0xc2,0x22,0xa2,0x62,0xe2,
0x12,0x92,0x52,0xd2,0x32,0xb2,0x72,0xf2,
0x0a,0x8a,0x4a,0xca,0x2a,0xaa,0x6a,0xea,
0x1a,0x9a,0x5a,0xda,0x3a,0xba,0x7a,0xfa,
0x06,0x86,0x46,0xc6,0x26,0xa6,0x66,0xe6,
0x16,0x96,0x56,0xd6,0x36,0xb6,0x76,0xf6,
0x0e,0x8e,0x4e,0xce,0x2e,0xae,0x6e,0xee,
0x1e,0x9e,0x5e,0xde,0x3e,0xbe,0x7e,0xfe,
0x01,0x81,0x41,0xc1,0x21,0xa1,0x61,0xe1,
0x11,0x91,0x51,0xd1,0x31,0xb1,0x71,0xf1,
0x09,0x89,0x49,0xc9,0x29,0xa9,0x69,0xe9,
0x19,0x99,0x59,0xd9,0x39,0xb9,0x79,0xf9,
0x05,0x85,0x45,0xc5,0x25,0xa5,0x65,0xe5,
0x15,0x95,0x55,0xd5,0x35,0xb5,0x75,0xf5,
0x0d,0x8d,0x4d,0xcd,0x2d,0xad,0x6d,0xed,
0x1d,0x9d,0x5d,0xdd,0x3d,0xbd,0x7d,0xfd,
0x03,0x83,0x43,0xc3,0x23,0xa3,0x63,0xe3,
0x13,0x93,0x53,0xd3,0x33,0xb3,0x73,0xf3,
0x0b,0x8b,0x4b,0xcb,0x2b,0xab,0x6b,0xeb,
0x1b,0x9b,0x5b,0xdb,0x3b,0xbb,0x7b,0xfb,
0x07,0x87,0x47,0xc7,0x27,0xa7,0x67,0xe7,
0x17,0x97,0x57,0xd7,0x37,0xb7,0x77,0xf7,
0x0f,0x8f,0x4f,0xcf,0x2f,0xaf,0x6f,0xef,
0x1f,0x9f,0x5f,0xdf,0x3f,0xbf,0x7f,0xff};

#define RTC_READ(x) \
	( tlbit_swap[*(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x)] )
#define RTC_WRITE(x,y) \
	( *(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x) = tlbit_swap[(y)])

#else
#define RTC_READ(x) \
	( *(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x) )
#define RTC_WRITE(x,y) \
	( *(volatile unsigned char *)(NV_RAM_ADRS + NV_RAM_SIZE - 8 + x) = y)
#endif

#define BIN2BCD(b)	(((b)%10) | (((b)/10)<<4))
#define BCD2BIN(b)	(((b)&0xf) + ((b)>>4)*10)

STATUS sysRtcGet(struct tm *tp)
{
	RTC_WRITE(0, RTC_READ(0) | 0x40);

	tp->tm_sec = BCD2BIN(RTC_READ(1) & 0x7f);
	tp->tm_min = BCD2BIN(RTC_READ(2) & 0x7f);
	tp->tm_hour = BCD2BIN(RTC_READ(3) & 0x3f);
	tp->tm_wday = BCD2BIN(RTC_READ(4) & 0x07) - 1;
	tp->tm_mday = BCD2BIN(RTC_READ(5) & 0x3f);
	tp->tm_mon = BCD2BIN(RTC_READ(6) & 0x1f) - 1;
	tp->tm_year = BCD2BIN(RTC_READ(7));

	RTC_WRITE(0, RTC_READ(0) & ~0x40);

	if (tp->tm_year < 80)
		tp->tm_year += 100;

	tp->tm_yday = 0;
	tp->tm_isdst = 0;

	return OK;
}

STATUS sysRtcSet(const struct tm *tp)
{
	RTC_WRITE(0, RTC_READ(0) | 0x80);

	RTC_WRITE(1, BIN2BCD(tp->tm_sec));
	RTC_WRITE(2, BIN2BCD(tp->tm_min));
	RTC_WRITE(3, BIN2BCD(tp->tm_hour));
	RTC_WRITE(4, BIN2BCD(tp->tm_wday + 1));
	RTC_WRITE(5, BIN2BCD(tp->tm_mday));
	RTC_WRITE(6, BIN2BCD(tp->tm_mon + 1));
	RTC_WRITE(7, BIN2BCD(tp->tm_year % 100));
	
	RTC_WRITE(0, (RTC_READ(0) & 0x40) |
			BIN2BCD((tp->tm_year < 100) ? 20 : 21));
	return OK;
}

void sysRtcHook(DOS_DATE_TIME *pDateTime)
{
	struct tm tm;
	
	sysRtcGet(&tm);

	pDateTime->dosdt_year = tm.tm_year + 1900;
	pDateTime->dosdt_month = tm.tm_mon + 1;
	pDateTime->dosdt_day = tm.tm_mday;
	pDateTime->dosdt_hour = tm.tm_hour;
	pDateTime->dosdt_minute = tm.tm_min;
	pDateTime->dosdt_second = tm.tm_sec;
}

#define get_tbl()	({unsigned int rval; \
			__asm__ __volatile__("mfspr %0,268" \
				     : "=r" (rval)); rval;})

#define get_tbu()	({unsigned int rval; \
			__asm__ __volatile__("mfspr %0,269" \
				     : "=r" (rval)); rval;})

void sysClkRateAdjust(int *sysDecClkFrequency)
{
	unsigned long	freq = 0;
	unsigned long	tbl = 0, tbu;
    long	i, loop_count;
    unsigned char	sec;
	struct timespec tv;
	struct tm tm;

	loop_count = 0;

	sec = RTC_READ(1);
	
	if(sec & 0x80) {
		sec &= 0x7f;
		RTC_WRITE(1, sec );
	}

	do {
		tbu = get_tbu();

		for (i = 0 ; i < 10000000 ; i++) {/* may take up to 1 second */
			tbl = get_tbl();

			if ((RTC_READ(1) & 0x7f) != sec) {
				break;
			}
		}

		sec = RTC_READ(1) & 0x7f;

		for (i = 0 ; i < 10000000 ; i++) { /* Should take 1 second */
			freq = get_tbl();

			if ((RTC_READ(1) & 0x7f) != sec) {
				break;
			}
		}

		freq -= tbl;
	} while ((get_tbu() != tbu) && (++loop_count < 2));

	*sysDecClkFrequency = (int)freq;

	sysRtcGet(&tm);
	tv.tv_sec = mktime(&tm);
	tv.tv_nsec = 0;
	clock_settime(CLOCK_REALTIME, &tv);
}

void date(const char *str)
{
	struct tm tm;
	struct timespec tv;
	time_t t;
	unsigned char sec;
	int i;

	if (str) {
		tm.tm_sec   = (str[10]-'0') *10 + str[11] - '0';
		tm.tm_min   = (str[8]-'0') *10 + str[9] - '0';
		tm.tm_hour  = (str[6]-'0') *10 + str[7] - '0';
		tm.tm_mday  = (str[4]-'0') *10 + str[5] - '0';
		tm.tm_mon   = (str[2]-'0') *10 + str[3] - '0' - 1;
		tm.tm_year  = (str[0]-'0') *10 + str[1] - '0';
		tm.tm_wday  = 0;
		tm.tm_yday  = 0;
		tm.tm_isdst = 0;
		
		if (tm.tm_year < 80)
			tm.tm_year += 100;
		
		tv.tv_sec = mktime(&tm);
		tv.tv_nsec = 0;
		
		sysRtcSet(&tm);
		
		sec = BIN2BCD(tm.tm_sec);
		
		for (i = 0 ; i < 10000000 ; i++)
			if ((RTC_READ(1) & 0x7f) != sec)
				break;
		
		tv.tv_sec ++;
		clock_settime(CLOCK_REALTIME, &tv);
		
		printf("%s", asctime(&tm));
	} else {
		t = time(NULL);
		printf("%s", ctime(&t));
	}
}


void timeTest()
{
	struct tm tm;
	sysClkRateSet(60);
	kernelTimeSlice(1);
	while(1)
	{
		sysRtcGet(&tm);
		printf("year   = %d\n",tm.tm_year+1900);
		printf("month  = %d\n",tm.tm_mon);
		printf("day    = %d\n",tm.tm_mday);
		printf("hour   = %d\n",tm.tm_hour);
		printf("minute = %d\n",tm.tm_min);
		printf("second = %d\n",tm.tm_sec);
		taskDelay(60);
		
	}
}

void UpdateClk(unsigned char *pClock)
{
 struct tm tm;	
 struct timespec tv;
/* time_t t;*/
 unsigned char sec;
 int i;
	
	
	
	tm.tm_sec = pClock[5];/*BIN2BCD(pClock[5]);*/
	tm.tm_min = pClock[4];/*BIN2BCD(pClock[4]);*/
	tm.tm_hour = pClock[3];/*BIN2BCD(pClock[3]);*/
	tm.tm_mday = pClock[2];/*BIN2BCD(pClock[2]);*/
	tm.tm_mon = (pClock[1]-1);/*BIN2BCD(pClock[1]);*/
	tm.tm_year = pClock[0];/*BIN2BCD(pClock[0]);*/
	 
	
	tm.tm_wday  = 0;
	tm.tm_yday  = 0;
	tm.tm_isdst = 0;
	
	if (tm.tm_year < 80)
		tm.tm_year += 100;
		
	tv.tv_sec = mktime(&tm);
	tv.tv_nsec = 0;
	
	sysRtcSet(&tm); 
	
	sec = BIN2BCD(tm.tm_sec);
	
	for (i = 0 ; i < 10000000 ; i++)
		if ((RTC_READ(1) & 0x7f) != sec)
			break;
	
	tv.tv_sec ++;
	clock_settime(CLOCK_REALTIME, &tv);
	printf("%s", asctime(&tm));
 
}
