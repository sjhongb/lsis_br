
/* ppc860SccSio.c - Motorola PPC860 SCC UART serial driver */

#include "copyright_wrs.h"

/* includes */
#include <stdio.h>
#include <string.h>
#include <vxWorks.h>
#include <iosLib.h>
#include <semLib.h>
#include <memLib.h>
#include <rngLib.h>
#include <taskLib.h>
#include <vxLib.h>
#include <intLib.h>
#include <errnoLib.h>
#include <iv.h>
#include <sysLib.h>
#include <selectLib.h>
#include <msgQLib.h>
#include <ioLib.h>
#include <time.h>
#include <logLib.h>
#include <tickLib.h>

#include "types.h"
#include "drv/multi/ppc860Siu.h"
#include "drv/multi/ppc860Cpm.h"
#include "./ppc860SccDrv.h"
#include "kvme402a.h"

#define	SIMPLE_TTY_RX_MODE		1
IMPORT UINT KVME402A_RX_PUT_ERR[8];

/* forward declarations */
void 	ppc860SccDevInit (PPC860SCC_CHAN *pChan);
STATUS	ppc860SccDevInstall();
static void ppc860SccResetChannel (PPC860SCC_CHAN *pChan);

PPC860SCC_CHAN* Scc860Open(PPC860SCC_CHAN *pChan, char *remainder, int mode);
int 	Scc860Close(PPC860SCC_CHAN *pChan);
int 	Scc860Write(PPC860SCC_CHAN *pChan, char *buffer, int nBytes);
int 	Scc860Read(PPC860SCC_CHAN *pChan, char *buffer, int maxBytes);
LOCAL STATUS	Scc860Ioctl(PPC860SCC_CHAN *pChan,	int request, int arg);

void	Scc860TxIntSet(PPC860SCC_CHAN *pChan);
void	Scc860TxIntUnSet(PPC860SCC_CHAN *pChan);
void 	Scc860Int (PPC860SCC_CHAN *pChan);

void	Fio_Scc_ModeSet(PPC860SCC_CHAN *pChan, int arg);
int		Fio_Scc_ModeGet(PPC860SCC_CHAN *pChan, int arg);
void	Fio_Scc_SET_TX_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out);
void	Fio_Scc_SET_RX_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out);
void	Fio_Scc_SET_RTS_AUTO_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out);
int		Fio_Scc_SET_BAUDRATE(PPC860SCC_CHAN *pChan, int arg);
int		Fio_Scc_GET_BAUDRATE(PPC860SCC_CHAN *pChan);
void	Fio_Scc_R_FLUSH(PPC860SCC_CHAN *pChan);
void	Fio_Scc_W_FLUSH(PPC860SCC_CHAN *pChan);
void	Fio_Scc_CHAN_RESET(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_SW_FLOW_CTR(PPC860SCC_CHAN *pChan);
void	Fio_Scc_UNSET_SW_FLOW_CTR(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_AUTO_RTSCTS(PPC860SCC_CHAN *pChan);
void	Fio_Scc_UNSET_AUTO_RTSCTS(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_DATA_LEN(PPC860SCC_CHAN *pChan, int char_len);
void	Fio_Scc_SET_STOP_BIT(PPC860SCC_CHAN *pChan, int stop_bit);
void	Fio_Scc_SET_PARITY(PPC860SCC_CHAN *pChan, int parity);
void	Fio_Scc_SET_RTS_ON(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_RTS_OFF(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_DTR_ON(PPC860SCC_CHAN *pChan);
void	Fio_Scc_SET_DTR_OFF(PPC860SCC_CHAN *pChan);
STATUS	Fio_Scc_GET_CTS_STATUS(PPC860SCC_CHAN *pChan);
STATUS	Fio_Scc_GET_DSR_STATUS(PPC860SCC_CHAN *pChan);
STATUS	Fio_Scc_GET_CD_STATUS(PPC860SCC_CHAN *pChan);

void	ShowScc860Reg(PPC860SCC_CHAN *pChan);


extern	void	sysSccSerialFlowConHwInit (int chan);
extern	STATUS	sysRTSCon(int num_chan, int flag);
extern	STATUS	sysDTRCon(int num_chan, int flag);

extern void sysSccSerialHwInit (int i);

int	SccDrvNum = 0;



/*******************************************************************************
*
* ppc860SccDevInit - initialize the SCC
*
* This routine is called to initialize the chip to a quiescent state.
*/
void 
ppc860SccDevInit (PPC860SCC_CHAN *pChan)
{
    * CIMR(pChan->regBase) &= (~(CIMR_SCC4 << (3 - (pChan->uart.sccNum - 1) )));

    pChan->baudRate = DEFAULT_SCC_DRV_BAUD;
}

STATUS	ppc860SccDevInstall()
{
	if((SccDrvNum = iosDrvInstall(	(FUNCPTR)NULL, 
									(FUNCPTR)NULL, 
									(FUNCPTR)Scc860Open, 
									(FUNCPTR)Scc860Close,
									(FUNCPTR)Scc860Read, 
									(FUNCPTR)Scc860Write, 
									(FUNCPTR)Scc860Ioctl)) < 0){
		printf("Scc860DrvInstall Fail\n");
		return(ERROR);
	}
	return(OK);
}

/*******************************************************************************
*
* ppc860SccResetChannel - initialize the SCC
*
*/
static void 
ppc860SccResetChannel (PPC860SCC_CHAN *pChan)
{
    int scc;			/* the SCC number being initialized */
    int baud;			/* the baud rate generator being used */
    int frame;

    int oldlevel = intLock ();			/* LOCK INTERRUPTS */

	sysSccSerialHwInit(pChan->uart.sccNum - 1);
	
    scc = pChan->uart.sccNum - 1;		/* get SCC number */
    baud = pChan->bgrNum - 1;			/* get BRG number */

    pChan->uart.intMask = CIMR_SCC4 << (3 - scc);


	/* set up SCC as NMSI */
    *MPC860_SICR(pChan->regBase) |= (UINT32) (baud << (scc << 3));
    *MPC860_SICR(pChan->regBase) |= (UINT32) ((baud << 3) << (scc << 3));

	*MPC860_SICR(pChan->regBase) &=0xc0ffffff;/*SCC4 routing*/
	*MPC860_SICR(pChan->regBase) |= (SICR_R4CS_BRG4 | SICR_T4CS_BRG4);
	
	*MPC860_SICR(pChan->regBase) &=0xffc0ffff;/*SCC3 routing*/
	*MPC860_SICR(pChan->regBase) |= (SICR_R3CS_BRG3 | SICR_T3CS_BRG3);

	/* reset baud rate generator */
     *pChan->pBaud |= BRGC_RST;
    while (*pChan->pBaud & BRGC_RST);

    Scc860Ioctl (pChan, SIO_BAUD_SET, pChan->baudRate); 

    /* set up transmit buffer descriptors */
    pChan->uart.txBdBase = (SCC_BUF *) (pChan->regBase + ((UINT32) pChan->uart.txBdBase));

    pChan->uart.pScc->param.tbase = (UINT16) ((UINT32) pChan->uart.txBdBase);
    pChan->uart.pScc->param.tbptr = (UINT16) ((UINT32) pChan->uart.txBdBase);
    pChan->uart.txBdNext 		  = 0;
	
    /* initialize each transmit buffer descriptor */
    for (frame=0; frame<pChan->uart.txBdNum; frame++){
        pChan->uart.txBdBase[frame].statusMode  = SCC_UART_TX_BD_INT;
   	    pChan->uart.txBdBase[frame].dataPointer = pChan->uart.txBufBase + (frame * pChan->uart.txBufSize);
        pChan->uart.txBdBase[frame].dataLength  = 1;
	}
		
    /* set the last BD to wrap to the first */
    pChan->uart.txBdBase[(frame - 1)].statusMode |= SCC_UART_TX_BD_WRAP;

    /* set up receive buffer descriptors */
    pChan->uart.rxBdBase = (SCC_BUF *) (pChan->regBase + ((UINT32) pChan->uart.rxBdBase));
	
    pChan->uart.pScc->param.rbase = (UINT16) ((UINT32) pChan->uart.rxBdBase);
    pChan->uart.pScc->param.rbptr = (UINT16) ((UINT32) pChan->uart.rxBdBase);
    pChan->uart.rxBdNext 	  = 0;

    /* initialize each receive buffer descriptor */
    for (frame=0; frame<pChan->uart.rxBdNum; frame++) {
        pChan->uart.rxBdBase[frame].statusMode = SCC_UART_RX_BD_EMPTY | SCC_UART_RX_BD_INT;
       	pChan->uart.rxBdBase[frame].dataLength = 1; /* char oriented */
        pChan->uart.rxBdBase[frame].dataPointer = pChan->uart.rxBufBase + frame;
	}
	
    /* set the last BD to wrap to the first */

    pChan->uart.rxBdBase[(frame - 1)].statusMode |= SCC_UART_TX_BD_WRAP;

    /* set SCC attributes to UART mode */
    pChan->uart.pSccReg->gsmrl    = SCC_GSMRL_RDCR_X16 | SCC_GSMRL_TDCR_X16 | SCC_GSMRL_UART;
    pChan->uart.pSccReg->gsmrh    = SCC_GSMRH_RFW      | SCC_GSMRH_TFL;
    pChan->uart.pSccReg->psmr     = SCC_UART_PSMR_CL_8BIT | SCC_UART_PSMR_SL;

    pChan->uart.pSccReg->dsr	  = 0x7e7e;	/* no fractional stop bits */

    pChan->uart.pScc->param.rfcr  = 0x18;	/* supervisor data access */
    pChan->uart.pScc->param.tfcr  = 0x18;	/* supervisor data access */
    pChan->uart.pScc->param.mrblr = 0x1;	/* one character rx buffers */

    /* initialize parameter the SCC RAM */

    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->maxIdl      = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->brkcr       = 0x1;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->parec       = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->frmer       = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->nosec       = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->brkec       = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->uaddr1      = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->uaddr2      = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->toseq       = 0x0;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character1  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character2  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character3  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character4  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character5  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character6  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character7  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->character8  = 0x8000;
    ((SCC_UART_PROTO *)pChan->uart.pScc->prot)->rccm        = 0xc0ff;

    pChan->uart.pSccReg->scce = 0xffff;	/* clr events */

    /* enables the transmitter and receiver  */
    pChan->uart.pSccReg->gsmrl |= SCC_GSMRL_ENR | SCC_GSMRL_ENT;

    /* unmask interrupt */
    pChan->uart.pSccReg->sccm = SCC_UART_SCCX_RX | SCC_UART_SCCX_TX;
    *MPC860_CIMR(pChan->regBase) |= pChan->uart.intMask;	/* 0x10000000 */
	
    intUnlock (oldlevel);			/* UNLOCK INTERRUPTS */
}


PPC860SCC_CHAN* Scc860Open(PPC860SCC_CHAN *pChan, char *remainder, int mode)
{
	int	num_chan;
	
	num_chan = pChan->uart.num_chan;

	/* use vxTas to implement mutual exclusion 
					to prevent simultaneous channel access */
	if(vxTas(&(pChan->uart.chanOpen)) == FALSE){
		return((PPC860SCC_CHAN *)ERROR);
	} /* if */

	/* 해당 Channel이 Open되었음을 알린다. */
	pChan->uart.chanOpen		= TRUE;
	pChan->uart.baudRate		= DEFAULT_SCC_DRV_BAUD;
    pChan->baudRate				= DEFAULT_SCC_DRV_BAUD;
	pChan->uart.dataBits		= SCC_CHAR_LEN_8;
	pChan->uart.stopBits		= SCC_1_STOP_BIT;
	pChan->uart.parity			= SCC_NO_PARITY;
	pChan->uart.TxTimeOut		= DEFAULT_TX_TIME_OUT;
	pChan->uart.RxTimeOut		= DEFAULT_RX_TIME_OUT;
	pChan->uart.RTS_autoTimeOut	= DEFAULT_RTS_AUTOTIMEOUT;
	pChan->uart.bufcreated      = FALSE;            
	pChan->uart.loopback     	= FALSE;            
	pChan->uart.wrtStateBusy 	= FALSE;            
	pChan->uart.txBufferSize 	= DEFAULT_TX_BUFF_SIZE;     
	pChan->uart.rxBufferSize 	= DEFAULT_RX_BUFF_SIZE;     
	pChan->uart.selWakeup		= TRUE;                 
	pChan->uart.setRTSCTSCtrl	= FALSE;

	pChan->uart.rxFrErr 	= 0;
	pChan->uart.rxPrErr 	= 0;
	pChan->uart.rxOvErr 	= 0;
	pChan->uart.rxCdErr 	= 0;
	pChan->uart.rxIntCnt 	= 0;
	pChan->uart.rxBytes 	= 0;

	pChan->uart.txIntCnt 	= 0;
	pChan->uart.txBytes 	= 0;

	/* Flush input and output ring buffers    */
	Fio_Scc_W_FLUSH(pChan);
	Fio_Scc_R_FLUSH(pChan);

	ppc860SccResetChannel(pChan);

	/* sometimes this flag remains enabled -- can not explain it so far */
	pChan->uart.wrtStateBusy = FALSE;

	/* Set baud rate and line format options  */
	Fio_Scc_SET_BAUDRATE(pChan, pChan->uart.baudRate);
	Fio_Scc_SET_DATA_LEN(pChan, pChan->uart.dataBits);
	Fio_Scc_SET_STOP_BIT(pChan, pChan->uart.stopBits);
	Fio_Scc_SET_PARITY(pChan, pChan->uart.parity);

	Fio_Scc_ModeSet(pChan, SIO_MODE_SET);

	return(pChan);
} /* Scc860Open() */


int Scc860Close(PPC860SCC_CHAN *pChan)
{
	pChan->uart.chanOpen = FALSE;

	return OK;
}

#define	TIME_10MS_DELAY	120000

int Scc860Write(PPC860SCC_CHAN *pChan, char *buffer, int nBytes)
{
	register int txBytes, totalTxBytes, reqTxBytes, freeBytes;
	register int oldlevel;/*, temp, dataLen = 0;*/
	char	txByte, *pBuffer;
			
	if (nBytes <= 0) 	return 0;

	if(pChan->uart.selWakeup == TRUE) {
		if(semTake(pChan->uart.mutexSemWr, WAIT_FOREVER) == ERROR) {
	    	return(0);
		}
	}

	totalTxBytes = 0;
	reqTxBytes = nBytes;
	pBuffer = buffer;
	while (1) {
		oldlevel = intLock();
		if ((freeBytes = rngFreeBytes(pChan->uart.wrtBuf)) < 1) {
			intUnlock (oldlevel);
			taskDelay(1);
			continue;
		}
		if (freeBytes < reqTxBytes)	reqTxBytes = freeBytes;
		txBytes = rngBufPut(pChan->uart.wrtBuf, pBuffer, reqTxBytes);
		totalTxBytes += txBytes;

		if(!pChan->uart.wrtStateBusy){
			if(pChan->uart.setRTSCTSCtrl == TRUE) {
				Fio_Scc_SET_RTS_ON(pChan);
			}
			pChan->uart.wrtStateBusy = TRUE;
			
			intUnlock (oldlevel);

			rngBufGet(pChan->uart.wrtBuf, (char *)&txByte, 1);
			pChan->uart.txBdBase[pChan->uart.txBdNext].dataPointer[0] = txByte;	
			pChan->uart.txBdBase[pChan->uart.txBdNext].dataLength = 1;
			pChan->uart.txBdNext = (pChan->uart.txBdNext+1)%pChan->uart.txBdNum;
			Scc860TxIntSet(pChan);
		} else { /* if */	
			intUnlock(oldlevel);
		}

		if (totalTxBytes >= nBytes) break;

		reqTxBytes = nBytes - totalTxBytes ;
		pBuffer += txBytes;
	} /* while */ 
			
	if(pChan->uart.selWakeup == TRUE) {
		semGive(pChan->uart.mutexSemWr);
	}

	if(pChan->uart.setRTSCTSCtrl == TRUE) {
		taskDelay(sysClkRateGet() * 0.001 * pChan->uart.RTS_autoTimeOut);
		Fio_Scc_SET_RTS_OFF(pChan);
	}

	return(nBytes);
}

int 
Scc860Read(PPC860SCC_CHAN *pChan, char *buffer, int maxBytes)
{
	RING_ID ringId;
	register int	nBytes;
	register int	i, rlen;
	register int	oldlevel;

	if (maxBytes <= 0) return(0);
	
	nBytes  = 0;
	ringId  = pChan->uart.rdBuf;

	if(pChan->uart.selWakeup == TRUE)
		if(semTake(pChan->uart.mutexSemRd, WAIT_FOREVER) == ERROR)	return(0);
	
	oldlevel = intLock();
	rlen = maxBytes;
	while (nBytes < maxBytes) {
		oldlevel = intLock();
		if((i = rngBufGet(ringId, &(buffer[nBytes]), rlen)) == 0) {
			taskDelay(1);
			intUnlock(oldlevel);
		} else {
			nBytes += i;
			rlen = maxBytes - nBytes;
			intUnlock(oldlevel);
		}
	}
	intUnlock(oldlevel);
	
	if(pChan->uart.selWakeup == TRUE)	semGive(pChan->uart.mutexSemRd);
	return(nBytes);
}


LOCAL STATUS 
Scc860Ioctl(PPC860SCC_CHAN *pChan,	int request, int arg)	
{
    STATUS status = OK;

    switch (request) {
    	case FIOBAUDRATE	:
		case SIO_BAUD_SET:
			Fio_Scc_SET_BAUDRATE(pChan, arg);
	    	break;	    			
   
		case FIO_SCC_BAUDRATE_GET:	
		case SIO_BAUD_GET:
	    	*(int *)arg = Fio_Scc_GET_BAUDRATE(pChan);
	    	break;

		case SIO_MODE_SET:
			Fio_Scc_ModeSet(pChan, arg);
            break;

        case SIO_MODE_GET:
            *(int *)arg = Fio_Scc_ModeGet(pChan, arg);
            return (OK);

        case SIO_AVAIL_MODES_GET:
            *(int *)arg = SIO_MODE_INT | SIO_MODE_POLL;
            return (OK);

		case FIOSELECT:
		case FIO_SCC_SELECT:
			selNodeAdd(&(pChan->uart.selWakeupList), (SEL_WAKEUP_NODE *)arg);
			/*
			if(selWakeupType((SEL_WAKEUP_NODE *)arg) == SELREAD && msgQNumMsgs(pChan->uart.mesQId) > 0)
				selWakeup((SEL_WAKEUP_NODE *)arg);
			*/
			
			if(selWakeupType((SEL_WAKEUP_NODE *)arg) == SELREAD && rngIsEmpty(pChan->uart.rdBuf) == FALSE)
				selWakeup((SEL_WAKEUP_NODE *)arg);
				
			if(selWakeupType((SEL_WAKEUP_NODE *)arg) == SELWRITE && pChan->uart.wrtStateBusy == FALSE)
				selWakeup((SEL_WAKEUP_NODE *)arg);
			break;
		
		case FIOUNSELECT:
		case FIO_SCC_UNSELECT:
			selNodeDelete(&(pChan->uart.selWakeupList), (SEL_WAKEUP_NODE *)arg);
			break;

		case FIO_SCC_SET_TX_TIMEOUT :
			Fio_Scc_SET_TX_TIMEOUT(pChan,  arg);
			break;
			
		case FIO_SCC_SET_RX_TIMEOUT :
			Fio_Scc_SET_RX_TIMEOUT(pChan,  arg);
			break;

		case FIO_SCC_SET_RTS_AUTO_TIMEOUT :
			Fio_Scc_SET_RTS_AUTO_TIMEOUT(pChan,  arg);
			break;

		case FIORFLUSH :
		case FIO_SCC_R_FLUSH :
			Fio_Scc_R_FLUSH(pChan);
			break;

		case FIOWFLUSH :
		case FIO_SCC_W_FLUSH :
			Fio_Scc_W_FLUSH(pChan);
			break;
			
		case FIO_SCC_CHAN_RESET :
			Fio_Scc_CHAN_RESET(pChan);
			break;
			
		case FIO_SCC_SET_SW_FLOW_CTR :
			Fio_Scc_SET_SW_FLOW_CTR(pChan);
			break;

		case FIO_SCC_UNSET_SW_FLOW_CTR :
			Fio_Scc_UNSET_SW_FLOW_CTR(pChan);
			break;
			
		case FIO_SCC_SET_AUTO_RTSCTS :
			Fio_Scc_SET_AUTO_RTSCTS(pChan);
			break;
			
		case FIO_SCC_UNSET_AUTO_RTSCTS :
			Fio_Scc_UNSET_AUTO_RTSCTS(pChan);
			break;
			
		case FIO_SCC_SET_DATA_LEN :
			Fio_Scc_SET_DATA_LEN(pChan,  arg);
			break;
			
		case FIO_SCC_SET_STOP_BIT :
			Fio_Scc_SET_STOP_BIT(pChan,  arg);
			break;
			
		case FIO_SCC_SET_PARITY :
			Fio_Scc_SET_PARITY(pChan,  arg);
			break;
			
		case FIO_SCC_SET_RTS_ON :
			Fio_Scc_SET_RTS_ON(pChan);
			break;
			
		case FIO_SCC_SET_RTS_OFF :
			Fio_Scc_SET_RTS_OFF(pChan);
			break;
			
		case FIO_SCC_SET_DTR_ON :
			Fio_Scc_SET_DTR_ON(pChan);
			break;
			
		case FIO_SCC_SET_DTR_OFF :
			Fio_Scc_SET_DTR_OFF(pChan);
			break;
			
		case FIO_SCC_GET_CTS_STATUS :
			Fio_Scc_GET_CTS_STATUS(pChan);
			break;
			
		case FIO_SCC_GET_DSR_STATUS :
			Fio_Scc_GET_DSR_STATUS(pChan);
			break;
			
		case FIO_SCC_GET_CD_STATUS :
			Fio_Scc_GET_CD_STATUS(pChan);
			break;

		default:
	    	status = ENOSYS;
	    	
	}
    return (status);
}


void	Scc860TxIntSet(PPC860SCC_CHAN *pChan)
{
	pChan->uart.txBdBase[pChan->uart.txBdNext].statusMode  
						|= SCC_UART_TX_BD_READY | SCC_UART_TX_BD_INT;
}

void	Scc860TxIntUnSet(PPC860SCC_CHAN *pChan)
{
	pChan->uart.txBdBase[pChan->uart.txBdNext].statusMode 
					&= ~(SCC_UART_TX_BD_READY | SCC_UART_TX_BD_INT);
}

/*******************************************************************************
*
* Scc860Int - handle an SCC interrupt
*
* This routine gets called to handle SCC interrupts.
*/
UINT scc860_rx_err_stat = SCC_UART_RX_BD_FR || SCC_UART_RX_BD_PR || SCC_UART_RX_BD_OV || SCC_UART_RX_BD_CD;

void 
Scc860Int (PPC860SCC_CHAN *pChan)
{
	char	rxByte;			/* character recevied byte	*/
	char	txByte;
	register int	oldlevel;	
	int	cnt, i;
	register int	status, txBdNext, errFlag, selFlag;
	int		portNum = pChan->uart.num_chan + 4;
	
	oldlevel = intLock();
	
	
	
	/*	SCC Rx	*/	
	if ((pChan->uart.pSccReg->scce & SCC_UART_SCCX_RX) &&
		(pChan->channelMode != SIO_MODE_POLL) ) {
		pChan->uart.pSccReg->scce = SCC_UART_SCCX_RX;
		
		selFlag = 0;
		while (1) {

			errFlag = 0;
			status = pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode;

			/* RX Bd Empty */

			if (status & SCC_UART_RX_BD_EMPTY){
				break;
			}
			pChan->uart.rxIntCnt++;
#if 0
			if (status & scc860_rx_err_stat) {
				errFlag = 1;
				if (status & SCC_UART_RX_BD_OV) {
					rngFlush(pChan->uart.rdBuf);
				}
			}
#else	/* NON-SIMPLE_TTY_RX_MODE */
			if (status & scc860_rx_err_stat) {
				if (status & SCC_UART_RX_BD_FR) {
					pChan->uart.rxFrErr++;
					errFlag = 1;
		 		}
				if (status & SCC_UART_RX_BD_PR) {
					pChan->uart.rxPrErr++;
					errFlag = 1;
				}
				if (status & SCC_UART_RX_BD_OV) {
					pChan->uart.rxOvErr++;
					rngFlush(pChan->uart.rdBuf);
					errFlag = 1;
				}
				if (status & SCC_UART_RX_BD_CD) {
					pChan->uart.rxCdErr++;
					errFlag = 1;
				}
#if 1	/* added by shjung, 2007-06-04 */
				if (status & SCC_UART_RX_BD_BR) {
					pChan->uart.rxBreak++;
					errFlag = 1;
				}
#endif				
			}
#endif	/* NON-SIMPLE_TTY_RX_MODE */
			
			if (!errFlag) {
				rxByte = pChan->uart.rxBdBase[pChan->uart.rxBdNext].dataPointer[0];
#if 1	/* added by shjung, 2007-06-04 */
				if (rngIsFull(pChan->uart.rdBuf))	return;
#endif
				if (rngBufPut(pChan->uart.rdBuf, (char *)&rxByte, 1)) {
	                selFlag = 1;		
					pChan->uart.rxBytes++;
				} else {
					KVME402A_RX_PUT_ERR[portNum] += 1;
				}
				if(selWakeupListLen(&(pChan->uart.selWakeupList)) > 0){
					if (rngNBytes(pChan->uart.rdBuf) == 1){
						selWakeupAll(&(pChan->uart.selWakeupList), SELREAD);
					}	
				}
			}


#if 0	/* changed by shjung, 2007-06-04 */
			pChan->uart.rxBdBase[pChan->uart.rxBdNext].dataPointer[0] = 0xff;
			pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode |= SCC_UART_RX_BD_EMPTY;
#else
			pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode &=
				~(SCC_UART_RX_BD_BR | SCC_UART_RX_BD_FR | SCC_UART_RX_BD_PR | 
				  SCC_UART_RX_BD_OV | SCC_UART_RX_BD_ID);
			pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode |= SCC_UART_RX_BD_EMPTY;
#endif			
			/* incr BD count */
			pChan->uart.rxBdNext 
					= (pChan->uart.rxBdNext + 1) % pChan->uart.rxBdNum;
			
			/* acknowledge interrupt */
			pChan->uart.pSccReg->scce = SCC_UART_SCCX_RX;
		}	/* while */
	}
	
	/* taskDelay(1); DELETE IT */
	
	/*	SCC Tx	*/
	if ((pChan->uart.pSccReg->scce & SCC_UART_SCCX_TX) && 
		(pChan->channelMode != SIO_MODE_POLL) ) {
		pChan->uart.pSccReg->scce |= SCC_UART_SCCX_TX;

		/* Modified by Cho, WonYong 2004.03.30 */
		for (i = 0; i < pChan->uart.txBdNum; i++) {
			txBdNext = pChan->uart.txBdNext;
			status = pChan->uart.txBdBase[txBdNext].statusMode 
											& SCC_UART_TX_BD_READY;
			if (!status) break;

			pChan->uart.txBdNext 
					= (pChan->uart.txBdNext+ 1) % pChan->uart.txBdNum;
		}

		if (i < pChan->uart.txBdNum) {

			pChan->uart.txIntCnt++;
			if(rngBufGet(pChan->uart.wrtBuf, (char *)&txByte, 1) != 0){
				pChan->uart.txBdBase[pChan->uart.txBdNext].dataPointer[0] = txByte;
				pChan->uart.txBdBase[pChan->uart.txBdNext].dataLength = 1;
				pChan->uart.txBdNext 
							= (pChan->uart.txBdNext+ 1) % pChan->uart.txBdNum;
				Scc860TxIntSet(pChan);
				pChan->uart.txBytes++;
			} else {
				cnt = 0;
				pChan->uart.wrtStateBusy = FALSE; /* no more chars */
				
				selWakeupAll(&(pChan->uart.selWakeupList), SELWRITE);
				
				Scc860TxIntUnSet(pChan);
			}
		}
	}

	pChan->uart.pSccReg->scce = (pChan->uart.pSccReg->scce & ~(SCC_UART_SCCX_RX | SCC_UART_SCCX_TX));

	*CISR(pChan->regBase) = pChan->uart.intMask;

	intUnlock(oldlevel);
}

/******************************************************************************
*
* ppc860SccPollInput - poll the device for input.
*
* RETURNS: OK if a character arrived, ERROR on device error, EAGAIN
*          if the input buffer is empty.
*/

int 
ppc860SccPollInput (SIO_CHAN *pSioChan, char *thisChar)
{
    PPC860SCC_CHAN * pChan = (PPC860SCC_CHAN *)pSioChan;

    if (!(pChan->uart.pSccReg->scce & SCC_UART_SCCX_RX))
        return (EAGAIN); 

    if (pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode & SCC_UART_RX_BD_EMPTY)
        return (EAGAIN);

    /* get a character */
    *thisChar = pChan->uart.rxBdBase[pChan->uart.rxBdNext].dataPointer[0];

    /* set the empty bit */
    pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode |= SCC_UART_RX_BD_EMPTY;

    /* incr BD count */
    pChan->uart.rxBdNext = (pChan->uart.rxBdNext + 1) % pChan->uart.rxBdNum;

    /* only clear RX event if no more characters are ready */
    if (pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode & SCC_UART_RX_BD_EMPTY)
        pChan->uart.pSccReg->scce = SCC_UART_SCCX_RX;

    return (OK);
}

/******************************************************************************
*
* ppc860SccPollOutput - output a character in polled mode.
*
* RETURNS: OK if a character arrived, ERROR on device error, EAGAIN
*          if the output buffer if full.
*/
int
ppc860SccPollOutput (SIO_CHAN *pSioChan, char outChar)
{
    PPC860SCC_CHAN * pChan = (PPC860SCC_CHAN *)pSioChan;

	printf("ppc860SccPollOutput\n");
	
    /* is the transmitter ready to accept a character? */

    if (pChan->uart.txBdBase[pChan->uart.txBdNext].statusMode & SCC_UART_TX_BD_READY)
		return(EAGAIN);

    /* reset the transmitter status bit */
    pChan->uart.pSccReg->scce = SCC_UART_SCCX_TX;

    /* write out the character */
    pChan->uart.txBdBase[pChan->uart.txBdNext].dataPointer[0] = outChar;
    pChan->uart.txBdBase[pChan->uart.txBdNext].dataLength  = 1;

    /* send transmit buffer */
    pChan->uart.txBdBase[pChan->uart.txBdNext].statusMode |= SCC_UART_TX_BD_READY;
    pChan->uart.txBdNext = (pChan->uart.txBdNext + 1) % pChan->uart.txBdNum;

    return (OK);
}



/****************************************************
*													*
*			Scc860 IOCTL Function					*
*													*
****************************************************/
void	Fio_Scc_ModeSet(PPC860SCC_CHAN *pChan, int arg)
{
    int oldlevel;
    STATUS status = OK;

	if (!((int)arg == SIO_MODE_POLL || (int)arg == SIO_MODE_INT)) {
	    status = EIO;
	    return;
	}
	
	/* lock interrupt  */
	oldlevel = intLock();
	
	/* initialize channel on first MODE_SET */
	
	if (!pChan->channelMode)    ppc860SccResetChannel(pChan);
	
	/*
	 * if switching from POLL to INT mode, wait for all characters to
	 * clear the output pins
	 */
	
	if ((pChan->channelMode == SIO_MODE_POLL) && (arg == SIO_MODE_INT)) {
		int i;
	
	    for (i=0; i < pChan->uart.txBdNum; i++)
	        while (	pChan->uart.txBdBase[(pChan->uart.txBdNext + i) % pChan->uart.txBdNum].statusMode & 
	        		SCC_UART_TX_BD_READY);
	}
	
	if (arg == SIO_MODE_INT) {
		/* reset the SCC's interrupt status bit */
	    *CISR(pChan->regBase) = pChan->uart.intMask;
	
	    /* enable this SCC's interrupt  */
		*CIMR(pChan->regBase) |= pChan->uart.intMask;
	
	    /* reset the receiver status bit */ 
		pChan->uart.pSccReg->scce = SCC_UART_SCCX_RX;
	
	    /* enables the receive and transmit interrupts */
	    pChan->uart.pSccReg->sccm = SCC_UART_SCCX_RX | SCC_UART_SCCX_TX;
	}
	else {
	    /* mask off the receive and transmit interrupts */
	    pChan->uart.pSccReg->sccm = 0;
	
	    /* mask off this SCC's interrupt */ 
		*CIMR(pChan->regBase) &= (~(pChan->uart.intMask));
	
	}
	
	pChan->channelMode = arg;
	
	intUnlock(oldlevel);
}

int	Fio_Scc_ModeGet(PPC860SCC_CHAN *pChan, int arg)
{
	return(pChan->channelMode);
}

void	Fio_Scc_SET_TX_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out)
{
	pChan->uart.TxTimeOut	= time_out;
}

void	Fio_Scc_SET_RX_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out)
{
	pChan->uart.RxTimeOut	= time_out;
}

void	Fio_Scc_SET_RTS_AUTO_TIMEOUT(PPC860SCC_CHAN *pChan, int time_out)
{
	pChan->uart.RTS_autoTimeOut = time_out;
}

int	Fio_Scc_SET_BAUDRATE(PPC860SCC_CHAN *pChan, int arg)
{
	int	oldlevel;
	int	baudRate;

	oldlevel = intLock();

    if (arg >=  50 && arg <= 38400) {
		baudRate = (pChan->clockRate / (16 * arg))/* - 1*/;  			

		if (baudRate > 0xfff)
			*pChan->pBaud = MPC860_BRGC_CLK6_SRC | 
							(BRGC_CD_MSK & ((baudRate / 16 - 1) << 1)) | 
							BRGC_EN | 
							BRGC_DIV16;
		else
			*pChan->pBaud = MPC860_BRGC_CLK6_SRC | 
							(BRGC_CD_MSK & ((baudRate - 1) << 1)) | 
							BRGC_EN;
		
		pChan->baudRate = arg;
	}
	else{
		baudRate = (pChan->clockRate / (16 * arg))/* - 1*/;
		*pChan->pBaud = MPC860_BRGC_CLK6_SRC | (BRGC_CD_MSK &  ((baudRate - 1) << 1)) | BRGC_EN;
  		pChan->baudRate = arg;
  		
  		
	}
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */
	return(OK);
}

int	Fio_Scc_GET_BAUDRATE(PPC860SCC_CHAN *pChan)
{
	int cur_baudrate;

	cur_baudrate = pChan->baudRate;/*org  pChan->uart.baudRate; tjkim modify*/

	return(cur_baudrate);
}

void	Fio_Scc_R_FLUSH(PPC860SCC_CHAN *pChan)
{
	int oldlevel;
	
	semTake(pChan->uart.mutexSemRd, WAIT_FOREVER);
	
	oldlevel = intLock();
		
	rngFlush(pChan->uart.rdBuf);
	
	intUnlock(oldlevel);
	
	semGive(pChan->uart.mutexSemRd);

}

void	Fio_Scc_W_FLUSH(PPC860SCC_CHAN *pChan)
{
	int oldlevel;

	semTake (pChan->uart.mutexSemWr, WAIT_FOREVER);

	oldlevel = intLock();

	rngFlush(pChan->uart.wrtBuf);

	intUnlock(oldlevel);
	
	semGive(pChan->uart.mutexSemWr);

}

void	Fio_Scc_CHAN_RESET(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

}

void	Fio_Scc_SET_SW_FLOW_CTR(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	pChan->uart.setRTSCTSCtrl = TRUE;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

}

void	Fio_Scc_UNSET_SW_FLOW_CTR(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	pChan->uart.setRTSCTSCtrl = FALSE;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

}


void	Fio_Scc_SET_AUTO_RTSCTS(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	switch(pChan->uart.num_chan){
		case	0 :
			/* It's difference between this set and org set:tjkim test*/
			/* make reference to org set*/
			/* port B to enable RTS1 *//*tjkim add*/
			*MPC860_PBPAR(pChan->regBase) |= PB19; 
			*MPC860_PBDIR(pChan->regBase) |= PB19;
			*MPC860_PBODR(pChan->regBase) &= ~PB19;
					
			/* port C to enable CTS1, and DCD1 */
			*MPC860_PCPAR(pChan->regBase) &=~(PC11|PC10);
			*MPC860_PCDIR(pChan->regBase) &=~(PC11|PC10);
			*MPC860_PCSO (pChan->regBase) |= (PC11|PC10);
			break;
			
		case	1 :
			/* port B to enable RTS2 *//*tjkim add*/
			*MPC860_PBPAR(pChan->regBase) |= PB18; 
			*MPC860_PBDIR(pChan->regBase) |= PB18;
			*MPC860_PBODR(pChan->regBase) &= ~PB18;
					
			/* port C to enable CTS2, and DCD2 */
			*MPC860_PCPAR(pChan->regBase) &= ~(PC9|PC8);
			*MPC860_PCDIR(pChan->regBase) &= ~(PC9|PC8);
			*MPC860_PCSO (pChan->regBase) |= (PC9|PC8);
			break;
			
		case	2 :
			/* port D to enable RTS3 *//*tjkim add*/
			*MPC860_PDPAR(pChan->regBase) |= PD7;
			*MPC860_PDDIR(pChan->regBase) &= ~PD7;/*tjkim manual p.34-18*/
					
			/* port C to enable       CTS3, DCD3 */	
			*MPC860_PCPAR(pChan->regBase) &= ~(PC7|PC6);
			*MPC860_PCDIR(pChan->regBase) &= ~(PC7|PC6);
			*MPC860_PCSO (pChan->regBase) |= (PC7|PC6);
			break;
			
		case	3 :
			/* port D to enable RTS4 *//*tjkim add*/
			*MPC860_PDPAR(pChan->regBase) |= PD6;
			*MPC860_PDDIR(pChan->regBase) &= ~PD6;
					
			/* port C to enable        CTS4, DCD4 */
			*MPC860_PCPAR(pChan->regBase) &= ~(PC5|PC4);
			*MPC860_PCDIR(pChan->regBase) &= ~(PC5|PC4);
			*MPC860_PCSO (pChan->regBase) |= (PC5|PC4);
			break;
			
		default : break;
	}
	
	pChan->uart.pSccReg->gsmrh    = SCC_GSMRH_RFW | SCC_GSMRH_TFL | SCC_GSMRH_RTSM; /*tjkim add*/
    	pChan->uart.pSccReg->psmr     = SCC_UART_PSMR_FLC | SCC_UART_PSMR_CL_8BIT | SCC_UART_PSMR_SL;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

}

void	Fio_Scc_UNSET_AUTO_RTSCTS(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	switch(pChan->uart.num_chan){
		case	0 :
			*MPC860_PBPAR(pChan->regBase) &= ~PB19; 
			
			*MPC860_PCSO (pChan->regBase) &= ~(PC11|PC10);
			*MPC860_PCDIR(pChan->regBase) |=(PC11|PC10);
			break;
			
		case	1 :
			*MPC860_PBPAR(pChan->regBase) &= ~PB18; 	SYNC;
			
			*MPC860_PCSO (pChan->regBase) &= ~(PC9|PC8);
			*MPC860_PCDIR(pChan->regBase) |= (PC9|PC8);
			break;
			
		case	2 :
			*MPC860_PDPAR(pChan->regBase) &= ~PD7;		SYNC;
			
			*MPC860_PCSO (pChan->regBase) &= ~(PC7|PC6);
			*MPC860_PCDIR(pChan->regBase) |= (PC7|PC6);
			break;
			
		case	3 :
			*MPC860_PDPAR(pChan->regBase) &= ~PD6;	SYNC;
			
			*MPC860_PCSO (pChan->regBase) &= ~	(PC5|PC4);
			*MPC860_PCDIR(pChan->regBase) |= (PC5|PC4);
			break;
			
		default : break;
	}
	
    pChan->uart.pSccReg->gsmrh    = SCC_GSMRH_RFW | SCC_GSMRH_TFL;
    pChan->uart.pSccReg->psmr     = SCC_UART_PSMR_CL_8BIT | SCC_UART_PSMR_SL;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_UNSET_AUTO_RTSCTS OK\n",pChan->uart.num_chan);
#endif
}

void	Fio_Scc_SET_DATA_LEN(PPC860SCC_CHAN *pChan, int char_len)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

    pChan->uart.pSccReg->gsmrl &= ~(SCC_GSMRL_ENR | SCC_GSMRL_ENT); /*disable rx and tx*/

    pChan->uart.pSccReg->psmr = (pChan->uart.pSccReg->psmr & ~SCC_CHAR_LEN_MASK) | char_len;

   	pChan->uart.pSccReg->gsmrl |= SCC_GSMRL_ENR | SCC_GSMRL_ENT;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_DATA_LEN : %d OK\n",pChan->uart.num_chan, char_len);
#endif
}


void	Fio_Scc_SET_STOP_BIT(PPC860SCC_CHAN *pChan, int stop_bit)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

    pChan->uart.pSccReg->gsmrl &= ~(SCC_GSMRL_ENR | SCC_GSMRL_ENT); /*disable rx and tx*/

    pChan->uart.pSccReg->psmr = (pChan->uart.pSccReg->psmr & ~SCC_STOP_BIT_MASK) | stop_bit;

   	pChan->uart.pSccReg->gsmrl |= SCC_GSMRL_ENR | SCC_GSMRL_ENT;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_STOP_BIT : %d OK\n",pChan->uart.num_chan, stop_bit);
#endif
}


void	Fio_Scc_SET_PARITY(PPC860SCC_CHAN *pChan, int parity)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

    pChan->uart.pSccReg->gsmrl &= ~(SCC_GSMRL_ENR | SCC_GSMRL_ENT); /*disable rx and tx*/

    	pChan->uart.pSccReg->psmr = (pChan->uart.pSccReg->psmr & ~SCC_PARITY_MASK) | parity;

   	pChan->uart.pSccReg->gsmrl |= SCC_GSMRL_ENR | SCC_GSMRL_ENT;

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_PARITY : %d OK\n",pChan->uart.num_chan, parity);
#endif
}

void	Fio_Scc_SET_RTS_ON(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	sysRTSCon(pChan->uart.num_chan,RTS_ON);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_RTS_ON OK\n",pChan->uart.num_chan);
#endif
}

void	Fio_Scc_SET_RTS_OFF(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	sysRTSCon(pChan->uart.num_chan,RTS_OFF);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_RTS_OFF OK\n",pChan->uart.num_chan);
#endif
}

void	Fio_Scc_SET_DTR_ON(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	sysDTRCon(pChan->uart.num_chan,DTR_ON);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_DTR_ON OK\n",pChan->uart.num_chan);
#endif
}

void	Fio_Scc_SET_DTR_OFF(PPC860SCC_CHAN *pChan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	sysDTRCon(pChan->uart.num_chan,DTR_OFF);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_Scc_SET_DTR_OFF OK\n",pChan->uart.num_chan);
#endif
}

STATUS	Fio_Scc_GET_CTS_STATUS(PPC860SCC_CHAN *pChan)
{
	int ret_value = 0;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

	printf("Not Supported\n");

	return(ret_value);
}

STATUS	Fio_Scc_GET_DSR_STATUS(PPC860SCC_CHAN *pChan)
{
	int ret_value = 0;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

	printf("Not Supported\n");
	
	return(ret_value);
}

STATUS	Fio_Scc_GET_CD_STATUS(PPC860SCC_CHAN *pChan)
{
	int ret_value = 0;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

	printf("Not Supported\n");

	return(ret_value);
}


void	ShowScc860Reg(PPC860SCC_CHAN *pChan)
{
	printf("pChan->uart.pSccReg->scce : 0x%x\n",pChan->uart.pSccReg->scce);
	printf("pChan->uart.txBdNext : 0x%x\n",pChan->uart.txBdNext);
	printf("pChan->uart.txBdBase[%d].dataPointer : 0x%x\n",pChan->uart.txBdNext, pChan->uart.txBdBase[pChan->uart.txBdNext].dataPointer[0]);
	printf("pChan->uart.txBdBase[%d].statusMode : 0x%x\n",pChan->uart.txBdNext, pChan->uart.txBdBase[pChan->uart.txBdNext].statusMode);
	printf("pChan->uart.txBdBase[%d].dataLength : 0x%x\n",pChan->uart.txBdNext, pChan->uart.txBdBase[pChan->uart.txBdNext].dataLength);
	printf("pChan->uart.rxBdNext : 0x%x\n",pChan->uart.rxBdNext);
	printf("pChan->uart.rxBdBase[%d].dataPointer : 0x%x\n",pChan->uart.rxBdNext, pChan->uart.rxBdBase[pChan->uart.rxBdNext].dataPointer[0]);
	printf("pChan->uart.rxBdBase[%d].statusMode : 0x%x\n",pChan->uart.rxBdNext, pChan->uart.rxBdBase[pChan->uart.rxBdNext].statusMode);
	printf("pChan->uart.rxBdBase[%d].dataLength : 0x%x\n",pChan->uart.rxBdNext, pChan->uart.rxBdBase[pChan->uart.rxBdNext].dataLength);
	printf("*MPC860_CICR(pChan->regBase) : 0x%x\n",*MPC860_CICR(pChan->regBase));
	printf("*MPC860_CIPR(pChan->regBase) : 0x%x\n",*MPC860_CIPR(pChan->regBase));
	printf("*MPC860_CISR(pChan->regBase) : 0x%x\n",*MPC860_CISR(pChan->regBase));
	printf("*MPC860_CIMR(pChan->regBase) : 0x%x\n",*MPC860_CIMR(pChan->regBase));
	printf("pChan->uart.pSccReg->gsmrh : 0x%x\n",pChan->uart.pSccReg->gsmrh);
	printf("pChan->uart.pSccReg->psmr : 0x%x\n",pChan->uart.pSccReg->psmr);
}




