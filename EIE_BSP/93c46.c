
#include "vxWorks.h"
#include "vme.h"
#include "memLib.h"
#include "cacheLib.h"
#include "sysLib.h"
#include "config.h"
#include "string.h"
#include "intLib.h"
#include "esf.h"
#include "excLib.h"
#include "logLib.h"
#include "taskLib.h"
#include "vxLib.h"
#include "tyLib.h"
#include "arch/ppc/vxPpcLib.h"
#include "private/vmLibP.h"
#include "stdio.h"
#include "ctype.h"
#include "fioLib.h"

#include "kvme402a.h"

/*****************************************************************



                              ---    ---          VCC
                              |   |  |   |          |
               PC0  -----    =|CS  -- VCC|=    -----+
                              |         |
               PC1  -----    =|SK     N.C|=
                              |          |
               PC2  -----    =|DI     N.C|=
                              |          |
               PB6  -----    =|DO     GND|=    -----+
                              |          |          |
                               ----------          ---
                                  93C46             -



      *  user address:    6 bit   // not char but short address //
      *  user data: 16 bit

      FUNCTION: write all
      {
          eInit ();   // boot start 1 call
          eEnable ();
            eAllErase ();
            eAllWrite (val);
            eDisable ();
        }

        FUNCTION: 1 word write
        {
            eInit ();
            eEnable ();
            eErase (user address);
            eWrite (user address, user value);
            eDisable ();
        }

        FUNCTION: 1 word read
        {
            int value;
            eInit ();
            value = eRead (user address);
        }

***************************************************************/
#define PPIB PPI_PORTB
#define PPIC PPI_PORTC

#define LOW         0
#define HIGH        1

#define ALL_MODE    4
#define WRITE_MODE  5
#define READ_MODE   6
#define ERASE_MODE  7

#define EWDS_ALL    0x00
#define EWEN_ALL    0x30
#define WRITE_ALL   0x10
#define ERASE_ALL   0x20

#define	EEP_CS_LOW		0
#define	EEP_CS_HIGH		1
#define	EEP_CLK_LOW		2
#define	EEP_CLK_HIGH	3
#define	EEP_DI_LOW		4
#define	EEP_DI_HIGH		5
#define	EEP_DO_READ		6

#define cs_low()    ctl93c46(EEP_CS_LOW)
#define cs_high()   ctl93c46(EEP_CS_HIGH)
#define clk_low()   ctl93c46(EEP_CLK_LOW)
#define clk_high()  ctl93c46(EEP_CLK_HIGH)
#define di_low()    ctl93c46(EEP_DI_LOW)
#define di_high()   ctl93c46(EEP_DI_HIGH)
#define do_read()   ct93c46Read()
#define clk_pulse() clk_high();\
                    clk_low()


#define MAX93c46	0x7e
#define	MAX_MAC_ADDR	28
#define	MAC_ADDR_1		14

LOCAL void	ct93c46AccPortInit()
{
	int	immrVal = vxImmrGet();

    *PCPAR(immrVal) &= ~(0x0e);
    *PCDIR(immrVal) |= (0x0e);
}

LOCAL void ctl93c46(UCHAR sel)
{
	int	immrVal = vxImmrGet();

    switch (sel){
		case	EEP_CS_LOW :
				*PCDAT(immrVal) &= ~(0x8);
				break;
				
		case	EEP_CS_HIGH :
				*PCDAT(immrVal) |= (0x8);
				break;
				
		case	EEP_CLK_LOW :
				*PCDAT(immrVal) &= ~(0x4);
				break;
				
		case	EEP_CLK_HIGH :
				*PCDAT(immrVal) |= (0x4);
				break;
				
		case	EEP_DI_LOW :
				*PCDAT(immrVal) &= ~(0x2);
				break;
				
		case	EEP_DI_HIGH :
				*PCDAT(immrVal) |= (0x2);
				break;
				
		default:	break;
	}
/*	taskDelay(2);
*/
}

LOCAL char	ct93c46Read()
{
	int	immrVal = vxImmrGet();
	char	ret_value;
	
	ret_value = *PCDAT(immrVal) & 0x01;
	
	return(ret_value);
}

LOCAL void Error (char vt)
{
    /* no Warning */
    vt++;
}

LOCAL void IntShift (unsigned value)
{
    register char i;

    for (i=0; i < 15; i++)
    {
        if (value & 0x8000)
            di_high ();
        else
            di_low ();
        clk_pulse ();
        value <<= 1;
    }
    if (value & 0x8000)
        di_high ();
    else
        di_low ();
    clk_high ();
    di_low ();
}

LOCAL unsigned ReadData (void)
{
    register char i;
    register unsigned ret;

    ret = 0;
    for (i=0; i < 16; i++)
    {
        ret <<= 1;
        clk_pulse ();
        if (do_read ())
            ret |= 1;
    }
    return (ret);
}

LOCAL void Bsy (char ErrorCode)
{
    cs_low ();
    cs_high ();
    if (do_read ())
        Error (ErrorCode);
     while (!do_read ())
        ;
    cs_low ();
    clk_low ();
}

LOCAL void InitMode (char mode, char address)
{
    cs_low ();
    clk_low ();
    cs_high ();
    IntShift ((((int) mode) << 6) | (address & 0x3f));
}

LOCAL void eDisable (void)
{
    InitMode (ALL_MODE, EWDS_ALL);
    cs_low ();
    clk_low ();
}

LOCAL void eEnable (void)
{
    InitMode (ALL_MODE, EWEN_ALL);
    cs_low ();
    clk_low ();
}

LOCAL unsigned eRead (char address)
{
    register int ret;

    InitMode (READ_MODE, address);
    if (do_read ())
        Error (1);
    clk_low ();
    ret = ReadData ();
    cs_low ();
    return (ret);
}

LOCAL char eWrite (char address, unsigned udata)
{
    InitMode (WRITE_MODE, address);
    clk_low ();
    IntShift (udata);
    Bsy (0x20);
    return 0;
}

LOCAL char eErase (char address)
{
    InitMode (ERASE_MODE, address);
    Bsy (0x30);
    return 0;
}

LOCAL void eInit (void)
{
    cs_low();
    di_low();
    clk_low();
}

LOCAL UINT32 string2Hex (UINT8 *ptr)
{
	UINT32 val = 0;

	for (; *ptr; ++ptr) {
		if (*ptr >= '0' && *ptr <= '9')			val = (val << 4) | (*ptr - '0');
		else if (*ptr >= 'a' && *ptr <= 'f')	val = (val << 4) | (0xa + *ptr - 'a');
		else if (*ptr >= 'A' && *ptr <= 'F')	val = (val << 4) | (0xA + *ptr - 'A');
	}
	return (val);
}

LOCAL unsigned short	GetMacData()
{
	static int	cnt;
	char    mac_char[20];
	unsigned short		mac_data;
	
	printf("Put Mac Addr count %d : ", cnt);
	gets(mac_char);
	mac_data = string2Hex(mac_char);

	if(cnt > MAX_MAC_ADDR - 1)	cnt = 0;
	else						cnt++;
	
	return(mac_data);
}


void	GetMac()
{
	int i, cnt;
	int	print_flag = 0;
	
	unsigned short y[MAX_MAC_ADDR];
	
	ct93c46AccPortInit();

	eInit();
	eDisable();

	printf("\nEther 1 MAC Addr : ");
	for(cnt=i=0;i<MAX_MAC_ADDR;i+=2){
		y[i]=eRead(i>>1);

		if(i >= MAC_ADDR_1 && print_flag == 0){
			printf("\nEther 2 MAC Addr : ");
			print_flag = 1;
		}
		printf(" %02x",y[i]);
	}
	printf("\n"); 
}


void SetMac()
{
	int i,cnt;
	int	print_flag = 0;
	unsigned short new_enet[MAX_MAC_ADDR];
	
	ct93c46AccPortInit();

	GetMac();

	printf ("--------------------------------------------------\n");
	printf (" Modify Ethernet Address.\n");
	printf (" You can modify only the unit #0.\n");
	printf ("--------------------------------------------------\n");

	eInit();
	eEnable();

	printf ("Ethernet 1 Mac------------------------------------\n");
	for(i=0, cnt=0;i<MAX_MAC_ADDR;i+=2,cnt++){
		if(i >= MAC_ADDR_1 && print_flag == 0){
			printf ("\nEthernet 2 Mac------------------------------------\n");
			print_flag = 1;
		}
		new_enet[i] = GetMacData();
		eErase(i>>1);
		eWrite(i>>1,new_enet[i]);
		printf("\rWrite Addr %d - Data 0x%x\n",i,new_enet[i]);
	}
	eDisable();
	eInit();

	GetMac();
}


void	GetsysCpmEnetAddr(char *mac, int unit)
{
	int i = 0, cnt = 0;

	ct93c46AccPortInit();

	eInit();
	eDisable();

	for(cnt=i=0;i<MAX_MAC_ADDR;i+=2, cnt++){
		if(unit == 0){
			if(i == MAC_ADDR_1)	break;
			mac[cnt] = eRead(i>>1);
		}
		else if(i >= MAC_ADDR_1){
			mac[cnt - 7] = eRead(i>>1);
		}
	}
}
void eAllRead()
{
	unsigned int i,j,cnt;
	unsigned short y[64];
	printf("\n\n");
	
	ct93c46AccPortInit();

	eInit();
	eDisable();

	cnt=0;
	for(j=i=0;i<128;i+=2,j++){
		y[j]=eRead(i>>1);
		printf("%04x ",y[j]);
		cnt++;
		if(cnt == 16){
			printf("\n");
			cnt=0;
		}
		
	}
	printf("\n");
}
