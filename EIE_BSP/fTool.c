/****************************************************************
*   flash402.c													*
*																*
*	File name 	: flash402.c									*
*	Date      	: 31/1/2002										*
*	Version   	: 1.0											*
*	written by LK11 ,co.,Ltd								*
****************************************************************/
#include "vxWorks.h"
#include "ioLib.h"
#include "iosLib.h"
#include "selectLib.h"
#include "iv.h"
#include "logLib.h"
#include "semLib.h"
#include "strLib.h"
#include "taskLib.h"
#include "tyLib.h"
#include "intLib.h"
#include "sysLib.h"
#include "errnoLib.h"
#include "vxLib.h"
#include "stdio.h"

#include "flash.h"


/********************************************
*			Extern Function					*
********************************************/
IMPORT	STATUS block_erase(INT16 area);
IMPORT	void all_erase();
IMPORT	INT32 flash_write(INT32 addr, UINT16 *buf_pt, UINT32 buf_cnt);
IMPORT	INT32 flash_byte_write(INT32 addr,UINT16 data);
IMPORT	void	reset_flash(UINT16 selete);
IMPORT	STATUS flash_set
    (
    char *string,     /* string to be copied into flash memory */
    int strLen,       /* maximum number of bytes to copy       */
    int offset        /* byte offset into flash memory         */
    );


/********************************************
*			Test Main Function				*
********************************************/
void	fTest()
{
	UINT16	i;
	UINT16	data[TEST_DATA_NUM];
	
	printf("Flash Test Start.... \n");	

	all_erase();
	
	for(i=0;i<TEST_DATA_NUM;i++){
		data[i] = (UINT16)i;
	}

	flash_write(0xfa310000, &data[0], TEST_DATA_NUM);
	
	printf("Flash Write End\n");
	
	reset_flash(0);
}

void makeFlashBoot(char *name)
{
	int fd, readByte;
	char *buf;
	int	clkRate;
	
	clkRate=sysClkRateGet();
	
	sysClkRateSet(100000);
	
	printf("Start binary file to Flash\n");
	
	buf = (char *)malloc(MAX_FILE_SIZE);

	fd = open(name,2,0);

	readByte = read(fd, buf, MAX_FILE_SIZE);

	if(readByte > MAX_FILE_SIZE){
		printf("Load File is over Max boot flash size.");
		return;
	}
	printf("Load File Name : %s - size : %d byte\n",name, readByte);
	printf("Open & read binary file OK\n");
	
/*		sp(flash_set, buf, readByte, FLASH_ROM_OFFSET + ROM_OFFSET);
*/	
	flash_set(buf, readByte, (FLASH_ROM_OFFSET + ROM_OFFSET) );

	free(buf);

	sysClkRateSet(clkRate);
}



/********************************************
*			ETC Test Function				*
********************************************/
void	ww(UINT16 data, UINT32	flash_addr)
{
	*(UINT16 *)(flash_addr) = data;
	
	printf("write : 0x%x - data : 0x%x\n",flash_addr, data);
}

void	rw(UINT32	flash_addr)
{
	UINT16	data;
	
	data = *(UINT16 *)(flash_addr);
	
	printf("Read : 0x%x - data : 0x%x\n",flash_addr, (UINT16)data);
}

void	wa(UINT32 sel, UINT32 data)
{
	UINT32	flash_base = FLASH_ADDR;
	
	sel = sel * 4;
	
	*(UINT32 *)(flash_base + sel) = data;
	printf("write : 0x%x - data : 0x%x\n",flash_base + sel, data);
}

void	ra(UINT32 sel)
{
	UINT32	data;
	UINT32	flash_base = FLASH_ADDR;
	
	sel = sel * 4;
	
	data = *(UINT32 *)(flash_base + sel);
	printf("Read : 0x%x - data : 0x%x\n",flash_base + sel, (UINT32)data);
}
