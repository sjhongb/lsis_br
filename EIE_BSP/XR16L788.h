
#include "kvme402a.h"

/*********************************************************************
*					Version Informaiton							*
*********************************************************************/
#define BSP_NAME						"XR16L788_DUAL"
#define XR16L788_BSP_VERSION			"ver_1.0.0"
#define XR16L788_LAST_UPDATE			"2003-11-10 11:21오전"


/*********************************************************************
*				IOCTL Arg 1 : FIO Ftn Call						*
*********************************************************************/
/********************************************************************
*			사용가능한 VxWorks Standard IOCTL				       *
*********************************************************************
*				1)	FIONREAD         						       *
*				2)	FIONWRITE        						       *
*				6)	FIOBAUDRATE      						       *
*				7)	FIOFLUSH         						       *
*				8)	FIORFLUSH        						       *
*				9)	FIOWFLUSH        						       *
********************************************************************/

/********************************************************************
*				XR16L788 BSP Specific IOCTL				       		*
*********************************************************************/
/*			Time ftn								*/
#define FIO_SET_TIMEDELAY				0xF00		/*	Set XR16L788 Internal Time Delay	*/
#define FIO_UNSET_TIMEDELAY				0xF01		/*	Unset Set XR16L788 Internal Time Delay	*/
#define FIO_SET_TX_TIMEOUT				0xF02		/*	Set Tx Time Out	*/
#define FIO_SET_RX_TIMEOUT				0xF03		/*	Set Rx Time Out	*/
#define	FIO_485_TIMEDELAY				0xF04

/*			XR16C788 Special ftn		      			*/
#define FIO_SET_SLEEP					0xF10		/*	Set XR16L788 Sleep	*/
#define FIO_UNSET_SLEEP					0xF11		/*	UnSet XR16L788 Sleep	*/
#define FIO_SET_INFRARED_CTR			0xF12		/*	Set XR16L788 Infrared Mode	*/
#define FIO_UNSET_INFRARED_CTR			0xF13		/*	UnSet XR16L788 Infrared Mode	*/
#define FIO_SET_LOOPBACK				0xF14		/*	Set XR16L788 LoopBack Mode	*/
#define FIO_UNSET_LOOPBACK				0xF15		/*	UnSet XR16L788 LoopBack Mode	*/

/*			XR16C788 General ftn		      			*/
#define FIO_SET_BAUDRATE				0xF20		/*	Set XR16L788 Channel BaudRate	*/
#define FIO_GET_BAUDRATE				0xF21		/*	Get XR16L788 Channel Current BaudRate	*/
#define FIO_R_FLUSH						0xF22		/*	Set XR16L788 Read Channel Flush	*/
#define FIO_W_FLUSH						0xF23		/*	Set XR16L788 Write Channel Flush	*/
#define FIO_CHAN_RESET					0xF24		/*	Set XR16L788 Channel Reset	*/
#define FIO_GET_DREV					0xF25		/*	Set XR16L788 Device Revisiont	*/
#define FIO_GET_DVID					0xF26		/*	Set XR16L788 Device ID*/
#define FIO_SET_DATA_LEN				0xF27		/*	Set XR16L788 Data Length	*/
#define FIO_SET_STOP_BIT				0xF28		/*	Set XR16L788 Data Stop Bit	*/
#define FIO_SET_PARITY					0xF29		/*	Set XR16L788 Data Parity		*/

/*			XR16C788 Set Flow Control				*/
#define FIO_SET_RTS_ON					0xF30		/*	Set XR16L788 Channsel RTS On	*/
#define FIO_SET_RTS_OFF					0xF31		/*	Set XR16L788 Channsel RTS Off	*/
#define FIO_SET_DTR_ON					0xF32		/*	Set XR16L788 Channsel DTR On	*/
#define FIO_SET_DTR_OFF					0xF33		/*	Set XR16L788 Channsel DTR Off	*/
#define FIO_GET_CTS_STATUS				0xF34		/*	Get XR16L788 Channsel CTS Status ( On/Off )	*/
#define FIO_GET_DSR_STATUS				0xF35		/*	Get XR16L788 Channsel DSR Status ( On/Off )	*/
#define FIO_GET_CD_STATUS				0xF36		/*	Get XR16L788 Channsel DCD Status ( On/Off )	*/
#define FIO_RTSCTSCTL					0xF37		/*	Set RTS On Auto Enable at Data Write	*/

/*			XR16C788 Auto Control ftn					*/
#define FIO_SET_SW_FLOW_CTR				0xF41		/*	Set XR16L788 Channsel Software Flow control	*/
#define FIO_SET_AUTO_RS485CTR			0xF42		/*	Set XR16L788 Channsel Auto RS485 Control	*/
#define FIO_UNSET_AUTO_RS485CTR			0xF43		/*	UnSet XR16L788 Channsel Auto RS485 Control	*/
#define FIO_SET_AUTO_RTSDTR				0xF44		/*	Set XR16L788 Channsel Auto RTS/DTR Control	*/
#define FIO_UNSET_AUTO_RTSDTR			0xF45		/*	UnSet XR16L788 Channsel Auto RTS/DTR Control	*/
#define FIO_SET_AUTO_CTSDSR				0xF46		/*	Set XR16L788 Channsel Auto CTS/DSR Control	*/
#define FIO_UNSET_AUTO_CTSDSR			0xF47		/*	UnSet XR16L788 Channsel Auto CTS/DSR Control	*/

/*			XR16C788 Err Check ftn					*/
#define FIO_RX_OVER_RUN_ERR				0xF51		/*	Get XR16L788 Channsel Rx Over Run Error Number	*/
#define FIO_RX_PARITY_ERR				0xF52		/*	Get XR16L788 Channsel Rx Parity Error Number	*/
#define FIO_RX_FRAMING_ERR				0xF53		/*	Get XR16L788 Channsel Rx Framing Error Number	*/
#define FIO_RX_BREAK_ERR				0xF54		/*	Get XR16L788 Channsel Rx Break Error Number	*/
#define FIO_RX_FIFO_ERR					0xF55		/*	Get XR16L788 Channsel Rx Fifo Error Number	*/


/*********************************************************************
*				Interrupt Vector & ETC Define						*
*********************************************************************/
#define XR16L788_1_BASE_ADDRESS			0xF3000000

#define XR16L788_1_INT_VEC				IV_SIO_3			/* IRQ 2 interrupt : IV_IRQ2*/


#define NUM_OF_XR16L788_CHAN			4
#define	ONE_OF_XR16L788_CHAN			4

#define MAX_XR16L788_TX_FIFO			1
#define MAX_XR16L788_RX_FIFO			1

#define MAX_BAUDRATE					921600
#define MIN_BAUDRATE					100
#define DEFAULT_BAUD_RATE				9600

#define OUTPUT_DATA_RATE_MCR7_1_OR		(0x80)
#define OUTPUT_DATA_RATE_MCR7_0_OR		~(0x80)

#define XTAL							7372800

#define BAUD_LO(baud)					((XTAL/(16*baud)) & 0xff)
#define BAUD_HI(baud)					(((XTAL/(16*baud)) & 0xff00) >> 8)

#define XR16L788_RESET					0xFB000002



/*********************************************************************
* 					Error numbers								*
*********************************************************************/
#define M_GMSSIO			(130 << 16)				/* GMS SIO Serial Board Driver   */
#define ESIO_NO_CARD		(M_GMSSIO | 12)		/* Board hardware is missing     */
#define ESIO_NO_DRIVER		(M_GMSSIO | 13)		/* SIO4 driver not initialzed    */
#define ESIO_BAD_IVEC		(M_GMSSIO | 14)		/* bad Interrupt Vector Base     */
#define ESIO_NO_CHAN		(M_GMSSIO | 15)		/* bad channel number            */
#define ESIO_CEXIST			(M_GMSSIO | 16)		/* channel already exists        */
#define ESIO_MALLOC			(M_GMSSIO | 17)		/* malloc failed - no  memory    */
#define ESIO_EBAUD			(M_GMSSIO | 18)		/* Bad Baud Rate value specified */
#define ESIO_EDBITS			(M_GMSSIO | 19)		/* Bad Data Bits value specified */
#define ESIO_ESBITS			(M_GMSSIO | 20)		/* Bad Stop Bits value specified */
#define ESIO_EPARITY		(M_GMSSIO | 21)		/* Bad Parity value specified    */
#define ESIO_COPEN			(M_GMSSIO | 22)		/* Channel already open          */
#define ESIO_EREQUEST		(M_GMSSIO | 23)		/* IOCTL request not valid       */
#define ESIO_TY_DEV			(M_GMSSIO | 24)		/* Error creating tty device     */
#define ESIO_COLLISION		(M_GMSSIO | 25)		/* Transmit collision detected   */
#define ESIO_NO_TIMER		(M_GMSSIO | 26)		/* No Timers are available       */


/******************************************************************************/
/*                   XR16L788B Channel Control Structure                        			*/
/******************************************************************************/
typedef struct
{
	DEV_HDR devHdr;					/* vxWorks I/O device header		*/
	
	int num_chan;						/* Channel number in the UART		*/
	int baudRate;						/* Baud rate for this channel			*/
	int dataBits;						/* Data Bits for this channel			*/
	int stopBits;						/* Stop Bits for this channel			*/
	int parity;							/* Parity for this channel			*/

	int bufcreated;						/* TRUE if device has been created	*/
	int chanOpen;						/* TRUE if device is currently open	*/
	
	int brgTest;						/* BRG Test Mode 사용 여부 표시	*/
	int loopback;						/* Local loopback enable status		*/
	
	int wrtStateBusy;					/* Transmitter busy flag for TxStartUp	*/
	int setRTSCTSCtrl;

	int rxErrFlag;
	int rxOverRunErr;
	int rxParityErr;
	int rxFramingErr;
	int rxBreak;
	int rxFifoError;

	int TxTimeOut;
	int RxTimeOut;

	int reqTxDataNum;
	int actualTxDataNum;
	int reqRxDataNum;
	int actualRxDataNum;

	RING_ID rdBuf;					/* ring buffer for read				*/
	RING_ID wrtBuf;					/* ring buffer for write				*/

	int rdrngstatus;
	int wrrngstatus;

	int rdintstatus;
	int wrintstatus;

	int txBufferSize;
	int rxBufferSize;

	SEM_ID binSemWr;				/* Binary semaphore for writing             */
	SEM_ID binSemRd;				/* Binary semaphore for reading             */

	char frameEndChar;

	int	selWakeup;					/* select() 기능 사용 여부를 표시한다.       */
	SEL_WAKEUP_LIST selWakeupList;	/* list of tasks pended in select           */

 	MSG_Q_ID mesQId;

	SEM_ID mutexSemWr;				/* Mutual exclusion semaphore for writing   */
	SEM_ID mutexSemRd;				/* Mutual exclusion semaphore for reading   */
	
	unsigned int txIntCnt;
	unsigned int rxIntCnt;
	
} XR16L788_CHAN;

static struct
{
	XR16L788_CHAN	*channel;
} XR16L788_DEV;


/*******************************************************
*												*
*			XR16550 Register Set					*
*												*
*******************************************************/
typedef	struct	COMPATABLE16550{
	char	rbr_thr_dll;	/*	Offset	0x0		*/
	char    dummy_00;
	char    dummy_01;
	char    dummy_02;
	char	dlm_ier;		/*	Offset	0x1		*/
	char    dummy_10;
	char    dummy_11;
	char    dummy_12;
	char	fcr_iir;		/*	Offset	0x2		*/
	char    dummy_20;
	char    dummy_21;
	char    dummy_22;
	char	lcr;			/*	Offset	0x3		*/
	char    dummy_30;
	char    dummy_31;
	char    dummy_32;
	char	mcr;			/*	Offset	0x4		*/
	char    dummy_40;
	char    dummy_41;
	char    dummy_42;
	char	lsr;			/*	Offset	0x5		*/
	char    dummy_50;
	char    dummy_51;
	char    dummy_52;
	char	msr;			/*	Offset	0x6		*/
	char    dummy_60;
	char    dummy_61;
	char    dummy_62;
	char	scr;			/*	Offset	0x7		*/
	char    dummy_70;
	char    dummy_71;
	char    dummy_72;
}COMPATABLE16550;                           	

typedef	struct	XR16L788REGSET{
	COMPATABLE16550	comp16550;
}XR16L788REGSET;

typedef	struct	XR16L788REG{
	XR16L788REGSET		reg[ONE_OF_XR16L788_CHAN];
}XR16L788REG;



#define XR16L788_REG(chan, reg)		( (unsigned char *)(XR16L788_BASE_ADDRESS + (sizeof(XR16L788REGSET) * chan) + reg*4) )




/********************************************
*				Register address			*
********************************************/
/********************************************
*		Register offsets from base address	*
*			16550 Compatible Register		*
********************************************/
#define RBR		0x00	/*	Receiver buffer register	*/
#define THR		0x00	/*	Transmitter holding register	*/
#define DLL		0x00	/*	Divisor latch LSB		*/

#define DLM		0x01	/*	Divisor latch MSB		*/
#define IER		0x01	/*	Interrupt enable register	*/

#define FCR		0x02	/*	FIFO Control Register	*/
#define IIR		0x02	/*	Interrupt identification register	*/

#define LCR		0x03	/*	Line Control Register	*/
#define XR_MCR	0x04	/*	Modem control reigster	*/
#define LSR		0x05	/*	Line status register		*/
#define MSR		0x06	/*	Modem status register	*/
#define SCR		0x07	/*	Scratchpad register		*/








/**********************************************
*			Each Register mean			*
**********************************************/
/**********************************************
*			Interrupt Enable Register			*
**********************************************/
#define IER_ERDAI							0x01
#define RxFIFO_BIT							IER_ERDAI
#define IER_ETHREI							0x02
#define TxFIFO_BIT							IER_ETHREI
#define IER_ELSI							0x04
#define Rx_BIT								IER_ELSI
#define IER_EMSI							0x08

#define XON_XOFF_SP_CHAR_INT_ENABLE		0x20
#define RTS_DTR_INT_ENABLE				0x40
#define CTS_DSR_INT_ENABLE				0x80


/**********************************************
*			FIFO Control Register			*
**********************************************/
#define FCR_EN			0x01			/*	Enables the transmit and receive FIFOs	*/
#define FIFO_ENABLE		FCR_EN
#define FCR_RXCLR		0x02			/*	Clears all bytes in the receiver FIFO and reset its counter	*/
#define RxCLEAR			FCR_RXCLR
#define FCR_TXCLR		0x04			/*	Clears all bytes in the tranasmit FIFO and reset its counter	*/
#define TxCLEAR			FCR_TXCLR
#define FCR_DMA			0x08			/*	Chanegs RxRDY, TXRDY form mode 0 to mode 1 if FCR0 is set	*/
#define FCR_RXTRIG_L	0x40			/*	set Trigger level for the receiver FIFO interrupt and the auto-RTS flow contorl	*/
#define FCR_RXTRIG_H	0x80			/*	set Trigger level for the receiver FIFO interrupt and the auto-RTS flow contorl	*/

/**********************************************
*	Software Flow Control Mode Select		*
**********************************************/
#define	NO_TXRX_FLOW_CON					0x00
#define	NO_TRANSMIT_FLOW_CON				0x00
#define	TRANSMIT_XONOFF1					0x08
#define	TRANSMIT_XONOFF2					0x04
#define	TRANSMIT_ALL						0x0C
#define	NO_RECEIVE_FLOW_CON					0x00
#define	RECEIVER_COMPARES_XONOFF1			0x02
#define	RECEIVER_COMPARES_XONOFF2			0x01
#define	T_XONOFF1_R_COMP_ALL				0x0B
#define	T_XONOFF2_R_COMP_ALL				0x07
#define	T_ALL_R_COMP_ALL					0x0F
#define	NO_T_FLOW_CON_R_COMP_ALL			0x03

/*-----------------------------------------
	 BIT		Receiver fifo trigger level(bytes)
	7, 6 
-------------------------------------------	
	0, 0					01
	0, 1					04
	1, 1					08
	1, 1					14
-----------------------------------------*/

/**********************************************
*		Interrupt Identification Register		*
**********************************************/
#define IIR_IP			0x01
#define IIR_ID			0x0e
#define IIR_RLS			0x06
#define Rx_INT			IIR_RLS
#define IIR_RDA			0x04
#define RxFIFO_INT		IIR_RDA
#define IIR_THRE		0x02
#define TxFIFO_INT		IIR_THRE
#define IIR_MSTAT		0x00
#define IIR_TIMEOUT		0x0c

/**********************************************
*			Line Control Register			*
**********************************************/
#define LCR_STB				0x04
#define LCR_PEN				0x08
#define LCR_EPS				0x10
#define LCR_SP				0x20
#define LCR_SBRK			0x40
#define LCR_DLAB			0x80
#define DLAB				LCR_DLAB

#define	CHAR_LEN_CLEAR		0xFC
#define CHAR_LEN_5			0x00
#define CHAR_LEN_6			0x01
#define CHAR_LEN_7			0x02
#define CHAR_LEN_8			0x03

#define	CHAR_STOP_BIT_CLEAR	0xFB
#define STOP_BIT_1			0x00
#define STOP_BIT_2			0x04

#define	CHAR_PARITY_CLEAR	0xC7
#define SET_NO_PARITY		0x00
#define SET_ODD_PARITY		0x08
#define SET_EVEN_PARITY		0x18
#define SET_PARITY_MARK_1	0x28
#define SET_PARITY_SPACE_0	0x38

/**********************************************
*			Modem Control Register			*
**********************************************/
#define MCR_DTR				0x01
#define DTR					MCR_DTR
#define MCR_RTS				0x02
#define MCR_OUT1			0x04
#define MCR_OUT2			0x08
#define MCR_LOOP			0x10

#define DTR_PIN_CONTROL		0x01
#define RTS_PIN_CONTROL		0x02
#define RTS_DTR_FLOW_SEL	0x04
#define OP2					0x08
#define INTERNAL_LOOPBACK	0x10
#define XON_ANY				0x20
#define IR_ENABLE			0x40
#define BRG_PRESCALER		0x80

#define MCR_DTR_ON			0x01
#define MCR_DTR_OFF			(~0x01)
#define MCR_RTS_ON			0x02
#define MCR_RTS_OFF			(~0x02)

/**********************************************
*			Line Status Register			*
**********************************************/
#define LSR_DR				0x01
#define RxCHAR_AVAIL		LSR_DR
#define LSR_OE				0x02
#define LSR_PE				0x04
#define LSR_FE				0x08
#define LSR_BI				0x10
#define LSR_THRE			0x20
#define LSR_TEMT			0x40
#define LSR_FERR			0x80

/**********************************************
*			Modem Status Register			*
**********************************************/
#define MSR_DCTS			0x01
#define MSR_DDSR			0x02
#define MSR_TERI			0x04
#define MSR_DDCD			0x08
#define MSR_CTS				0x10
#define MSR_DSR				0x20
#define MSR_RI				0x40
#define MSR_DCD				0x80

#define RS485_DLY_0			0x10
#define RS485_DLY_1			0x20
#define RS485_DLY_2			0x40
#define RS485_DLY_3			0x80





/**********************************************
*			EFR Register					*
**********************************************/
#define	SW_FLOW_CON_BIT0			0x01
#define	SW_FLOW_CON_BIT1			0x02
#define	SW_FLOW_CON_BIT2			0x04
#define	SW_FLOW_CON_BIT3			0x08
#define	EN_IER_ISR_FCR_MCR_MSR		0x10
#define	SPECIAL_CHAR_SEL			0x20
#define	AUTO_RTS_DTR_ENABLE			0x40
#define	AUTO_CTS_DSR_ENABLE			0x80

/**********************************************
*			XCHAR Register				*
**********************************************/
#define	XOFF_DET_INDICATOR			0x01
#define	XON_DET_INDICATOR			0x02



/**********************************************
*										*
*			Interrupt source				*
*										*
**********************************************/
#define	NON_IS						0x0
#define	RXRDY_IS					0x1
#define	RXRDY_T_IS					0x2
#define	TXRDY_IS					0x3
#define	MSR_IS						0x4
#define	RESERVED_IS1				0x5
#define	RESERVED_IS2				0x6
#define	TIMER_IS					0x7

/**********************************************
*										*
*		Device Configuration Register		*
*										*
**********************************************/
#define	UART0						0x01
#define	UART1						0x02
#define	UART2						0x04
#define	UART3						0x08
#define	UART4						0x10
#define	UART5						0x20
#define	UART6						0x40
#define	UART7						0x80

#define	UART_INT_S_MASK				0x07

/**********************************************
*			Timer CTRL Register			*
**********************************************/
#define	ENABLE_TIMER_INT			0x01
#define	DISABLE_TIMER_INT			~0x01
#define	START_TIMER					0x02
#define	STOP_TIMER					~0x02
#define	ONE_SHOT_TIMER				0x04
#define	RETRIGGER_TIMER				~0x04
#define	EXTERNAL_CLOCK				0x08
#define	INTERNAL_CLOCK				~0x08

/**********************************************
*			Sample Rate Register			*
**********************************************/
#define	SAMPLE_MODE_16X				0
#define	SAMPLE_MODE_8X				1

/**********************************************
*			RTS/CTS/DCD value			*
**********************************************/
#define	RTS_ON						1
#define	RTS_OFF						0
#define	CTS_ON						1
#define	CTS_OFF						0

#define	DTR_ON						1
#define	DTR_OFF						0
#define	DSR_ON						1
#define	DSR_OFF						0

#define	DCD_ON						1
#define	DCD_OFF						0


/*#define BUFFER_SIZE 5000*/ /* Size of Tx and Rx Ring Buffers in bytes */

#define MAX_MSGS   					100
#define MAX_M_LEN  					2
