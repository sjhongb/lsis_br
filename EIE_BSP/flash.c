/****************************************************************
*   flash402.c													*
*																*
*	File name 	: flash402.c									*
*	Date      	: 31/1/2002										*
*	Version   	: 1.0											*
*	written by LK11 ,co.,Ltd								*
****************************************************************/
#include "copyright_wrs.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "vxWorks.h"
#include "flash.h"
#include "taskLib.h"

/********************************************
*		Declaration of Internal Function 	*
********************************************/
INT32 flash_test(UINT16 area);
INT32 get_manufacturer_code(UINT16 selete);
INT32 get_device_code(UINT16 selete);
INT32 block_erase(INT16 area);
INT32 flash_byte_write(INT32 addr,UINT16 data);

/****************************************************************
*					Register Status Control						*
****************************************************************/
void	clear_status(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(FLASH_BASE + selete * 0x100000);

    *flash=0x0050;
}

void	reset_flash(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(FLASH_BASE + selete * 0x100000);

    *flash=0x00ff;
}


/****************************************************************
*						Check etc inform 						*
****************************************************************/
INT32 flash_code()
{
    int fl_er_cnt = 0;

    printf("Flash Manufacture Code    = 0x%x\n",get_manufacturer_code(0));	/*	FLASH_0 */
    printf("Flash Device Code         = 0x%x\n",get_device_code(0));

    clear_status(FLASH_0);

    reset_flash(FLASH_0);

    return(fl_er_cnt);
}


INT32 get_manufacturer_code(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(FLASH_BASE + selete * 0x100000);

    *flash=0x0090;
    return(*flash & 0x00ff);
}

INT32 get_device_code(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(FLASH_BASE + selete * 0x100000);

    *flash=0x0090;
    return(*(flash+1) & 0x00ff);
}

void	flash_delay(int time)
{
	int	i, j;
	for(i=0;i<time;i++)	j=j+1;
}

/****************************************************************
*					Get Register Status 						*
****************************************************************/
UINT32	get_status(UINT16 selete)
{
	UINT16 *flash = (UINT16 *)(FLASH_BASE + selete * 0x100000);

    *flash=0x0070;
    return(*flash & 0x00ff);
}



/****************************************************************
*						Flash Erase Function					*
****************************************************************/
STATUS 
block_erase(INT16 area)
{
/* area offset  0x10000(64KByte) */
    INT32 i;
    UINT16 status ;
    UINT16 *flash = (UINT16 *)(FLASH_BASE + area * 0x20000);

    *flash = 0x0020;
    taskDelay(10);
    
    *flash = 0x00d0;
	
    for(i=0; i< 8000000; i++){		/* delay 3 sec */
		status = *flash;		/* status */
		
		if(status & 0x0080){
/*			if(status & 0x0020)	clear_status(FLASH_0);
			else				clear_status(FLASH_1);
*/			
			clear_status(0);
			
			printf("Flash erase block Number %d OK\r", area);
			return(OK);
		}
    }

	printf("Flash erase timeout: blk#=%d\n", area);
	return(ERROR);
}

STATUS all_erase()
{
    unsigned char y;
    int i;

    clear_status(FLASH_0);

	for(i=0,y=0;i<MAX_BLOCK_NUM;i++){
		if(block_erase(i) != OK){
			printf("Flash Erase Error!!\n");
			printf("Block Erase Stop\n");
			return(ERROR);
		}
	}
	return(OK);
}

STATUS 
clear_block_lock(INT16 area)
{

    INT32 i;
    UINT16 status ;
    UINT16 *flash = (UINT16 *)(FLASH_BASE + area * 0x20000);

    *flash = 0x0060;
    taskDelay(10);
    
    *flash = 0x00d0;
	
    for(i=0; i< 8000000; i++){		/* delay 3 sec */
		status = *flash;			/* status */
		if(status & 0x0080){

			clear_status(0);
			
			return(OK);
		}
    }

	printf("Flash erase timeout: blk#=%d\n", area);
	return(ERROR);
}

STATUS 
set_block_lock(INT16 area)
{

    INT32 i;
    UINT16 status ;
    UINT16 *flash = (UINT16 *)(FLASH_BASE + area * 0x20000);
	
    *flash = 0x0060;
    taskDelay(10);
    
    *flash = 0x0001;
	
    for(i=0; i< 8000000; i++){		/* delay 3 sec */
		status = *flash;			/* status */
		if(status & 0x0080){

			clear_status(0);
			
			return(OK);
		}
    }

	printf("Flash erase timeout: blk#=%d\n", area);
	return(ERROR);
}

STATUS
unlock_all()
{
	int i;
	printf("Flash Block Unlock Mode :\n");
	for(i=0; i<32; i++){
		if( clear_block_lock(i) !=  OK ){
			printf("block un lock error !! [block : %d]\n", i);
			return(ERROR);
		}else{
			printf("[%02d]", i);
			if( ((i+1)%16) == 0) printf("\n");
		}	
	}
	printf("\n\n\n");
	return(OK);
}
STATUS
lock_all()
{
	int i;
	printf("Flash Block lock Mode :\n");
	for(i=0; i<32; i++){
		if( set_block_lock(i) !=  OK ){
			printf("block lock error !! [block : %d]\n", i);
			return(ERROR);
		}else{
			printf("[%02d]", i);
			if( ((i+1)%16) == 0) printf("\n");
		}	
	}
	printf("\n\n\n");
	return(OK);
}
/****************************************************************
*					Flash Data Write Function					*
****************************************************************/
STATUS flash_buff_write(addr, bp, cnt)
UINT32 addr, cnt;
UINT16 *bp;
{
	UINT32	base_addr, i = 0;
	UINT16	status;

	base_addr = addr;

	/********************************************************
	*	Step 1 : Write to Buffer Command E8H, Block Address	*
	********************************************************/
	*(UINT16 *)base_addr = 0xE8;
    taskDelay(10);

	/********************************************************
	*	Step 2 : Read Extended Status Register				*
	********************************************************/
	while(!(*(UINT16 *)base_addr & 0x80)){
		*(UINT16 *)base_addr = 0xE8;
		taskDelay(1);
	}
	
    /********************************************************
	*	Step 3 : Write Word or Byte Count, Block Address	*
	********************************************************/
    *(UINT16 *)base_addr = cnt - 1;
    taskDelay(10);
	
	/********************************************************
	*	Step 4 : Write Word Data to Address					*
	********************************************************/
	printf("Step 4 \n");
	while(1){
		if(i++ == cnt)	break;
		*(UINT16 *)addr++ = *bp++;
		if(i % 0x1000 == 0)	printf("%d byte Write. addr : 0x%x\r", (addr - FLASH_ADDR), addr);
		taskDelay(10);
	}
	printf("\n");

	/********************************************************
	*	Step 5 : Program Buffer to Flash Confirm D0H		*
	********************************************************/
	*(UINT16 *)addr = 0xD0;
    taskDelay(10);

	/********************************************************
	*	Step 6 : Status Check & Programming Complete		*
	********************************************************/
    for(i=0; i<1000; i++){
		taskDelay(1);
		status = *(UINT16 *)base_addr;

		if(status & 0x80){
			printf("Status Check XSR.7 OK!!\n");

			status = *(UINT16 *)base_addr;
			
			*(UINT16 *)base_addr = 0x50;	/* status clear */
			
			if(status & 0x08){
				printf("Voltage Range Error!!  Value : 0x%x\n",status);
				return(ERROR);
			}
			else	printf("Voltage Range OK!!\n");
			
			if(status & 0x02){
				printf("Device Protect Error!  Value : 0x%x\n",status);
				return(ERROR);
			}
			else	printf("Device Protect OK!!\n");
			
			if(status & 0x10){
				printf("Programming Error!  Value : 0x%x\n",status);
				return(ERROR);
			}
			else	printf("Programming OK!!\n");

#ifdef	skhong_debug
			if(status & 0x7e){
				*(UINT16 *)base_addr = 0x50;	/* status clear */

				printf("Flash write error:addr=0x%x, status=0x%x\n",(unsigned int)addr, status);
				return(ERROR);
			}
#endif
   			return(OK);
		}
	}
	printf("Flash write timeout: addr=0x%x\n", addr);

    return(ERROR);
}


STATUS flash_write(UINT32 addr, UINT16 *buf_pt, UINT32 buf_cnt)
{
	UINT32 i;
	UINT16 barf;
	
	if((addr + buf_cnt * 2) > (FLASH_BASE + FLASH_SIZE)){
		printf("flash_buf_write  : Write Size Error %d OverRun\n",
		       (addr + buf_cnt * 2) - (FLASH_BASE + FLASH_SIZE));
		
		return(ERROR);
	}
	
	for(i = 0; i < buf_cnt; i++){
	    *(UINT16 *)(addr + i * sizeof(UINT16)) = 0x0040;

        *(UINT16 *)(addr + i * sizeof(UINT16)) = *(buf_pt++);
        
        while(!(*(UINT16 *)addr & 0x0080))    	flash_delay(1);
    
	    barf = 0;
	    
	    if(*(UINT16 *)addr & 0x003A)			barf = *(UINT16 *)addr & 0x003A;
		
		if(barf){
			printf("\nFlash write error at address %lx\n", (unsigned long)addr);
				
			if(barf & 0x0002)			printf("Block locked, not erased.\n");
			if(barf & 0x0010)			printf("Programming error.\n");
			if(barf & 0x0008)			printf("Vpp Low error.\n");
			
			clear_status(0);
			
			reset_flash(0);
			
			return(ERROR);
		}

        if((i < 1000) && (i == (buf_cnt - 1)))
           	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
        else if(i % 1000 == 999)
        	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
        else if(i == (buf_cnt - 1))
        	printf("%d Byte Write. addr : 0x%x\r", (i + 1) * 2, addr + i * sizeof(UINT16));
    }
	printf("\n");
	
	printf("Flash Write OK. \n");
	printf("Start Addr = 0x%x, End Addr = 0x%x, Total Size = %d Byte\n", addr, addr + buf_cnt * sizeof(UINT16) - 1, buf_cnt * 2);
	
	/* 반드시 Read Array Command를 전송한 후, Read 해야 한다. */
	/* 그렇지 않으면 내부 Data가 제대로 보이지 않는다.         */
	/* 2002.07.03 BY RYOJIN                                  */
	reset_flash(0);
	
	return(OK);
}


INT32 flash_byte_write(INT32 addr,UINT16 data)
{
	UINT16 status;
	INT32 i;

    *(UINT16 *)addr = 0x0040;
    taskDelay(1);
    
    *(UINT16 *)addr = data;
	
    for(i=0; i< 800000; i++){		/* delay 3 sec */
		taskDelay(1);
		status = *(UINT16 *)addr;		/* status */

		if(status & 0x0080){
/*			printf("Pass 0x80\n");	*/
			taskDelay(1);
			
			if(status & 0x0010){
				printf("Pass 0x10\n");
				
				*(UINT16 *)addr = 0x0050;	/* status clear */
			
				printf("cnt : %i Flash write error:addr=0x%x, status=%d\n", i, addr, status);
				break;
			}
			else{
				printf("Flash write OK:addr=0x%x(deleay =%d)\n", addr, i);
				return(OK);
			}
		}
	}

	printf("Flash write timeout: addr=0x%x\n", addr);

    return(ERROR);
}


/****************************************************************
*						Flash Read Function						*
****************************************************************/
STATUS	flash_read(faddr, dst, cnt)
register unsigned char *faddr;
register unsigned char *dst;
register cnt;
{
	register i;

	for(i=0; i<cnt; i++) {
		/*** TEST
		printf(" 0x%x ", *dst);
		***/
		*dst++ = *faddr++;
	}

	return(OK);
}


/******************************************************************************
*
* sysFlashSet - write to flash memory
*
* This routine copies a specified string into flash memory after calling
* sysFlashErase() and clearing flash memory.
*
* If FLASH_NO_OVERLAY is defined, the parameter <offset> must be
* appropriately aligned for the Flash devices in use (device width,
* sector size etc.).
*
* If the specified string must be overlaid on the contents of flash memory,
* undefine FLASH_NO_OVERLAY.
*
* RETURNS: OK, or ERROR if the write fails or the input parameters are
* out of range.
*
* SEE ALSO: sysFlashErase(), sysFlashGet(), sysFlashTypeGet(), sysFlashWrite()
*
* INTERNAL
* If multiple tasks are calling sysFlashSet() and sysFlashGet(),
* they should use a semaphore to ensure mutually exclusive access to flash
* memory.
*/

STATUS flash_set
(
UINT16 *string,     /* string to be copied into flash memory */
int strLen,       /* maximum number of bytes to copy       */
int offset        /* byte offset into flash memory         */
)
{
	UINT16 *tempBuffer;

	tempBuffer =string;
	
	strLen /= 2;
	
    if ((offset < 0) || (strLen < 0) || ((offset + strLen) > FLASH_SIZE)){
    	printf("strLen Error\n");
        return (ERROR);
	}
	else	printf("Bin File size OK\n");

	flash_code();
	
	unlock_all();
	if( all_erase() != ERROR)	printf("Flash Block Erase OK                       \n\n");
	else{
		printf("Flash Block Erase ERROR\n");
		return(ERROR);
	}


	if(flash_write(FLASH_BASE + offset, tempBuffer, strLen) == ERROR){
		printf("Flash Buf Write Error\n");
		return(ERROR);
	}
	else{
		printf("Flash Buf Write OK!!!\n");
	}

	reset_flash(0);

    if (bcmp ((char *) (FLASH_BASE + offset), (char *)string, strLen) == 0){
    	printf("Data Varify OK!!\n");
    	lock_all();
    	return (OK);
	}
	else{
		printf("Data Varify Error!!\n");
		return(ERROR);
	}

    printf("Flash Write OK!\n");
    
	

    return (OK);
}



