

#include <stdio.h>
#include <string.h>
#include <vxWorks.h>
#include <iosLib.h>
#include <semLib.h>
#include <memLib.h>
#include <rngLib.h>
#include <taskLib.h>
#include <vxLib.h>
#include <intLib.h>
#include <errnoLib.h>
#include <iv.h>
#include <sysLib.h>
#include <selectLib.h>
#include <msgQLib.h>
#include <ioLib.h>
#include <time.h>
#include <logLib.h>

#include "types.h"
#include "XR16L788.h"

#define	SIMPLE_TTY_RX_MODE				1
#define	XR16L_SINGLE_TX_MODE			0


IMPORT UINT KVME402A_RX_PUT_ERR[8];


#define TxBufferSize			5000
#define RxBufferSize			5000
#define DefaultTxTimeOut		(sysClkRateGet() * 5)
#define DefaultRxTimeOut		(-1)			/*	(sysClkRateGet() * 5)	*/

#define CHECK_RNG_ELEM_GET(ringId)		((ringId)->pFromBuf == (ringId)->pToBuf) ? 0 : 1

int sioDrvNum    	= 0;					/* driver index into IO switch table */
int sioIntWasSet 	= FALSE;				/* interrupt was already set         */

XR16L788REG	*xr16l788reg1;


/**************************************************
*			Local Function declaration			*
**************************************************/
void		XR16L788wait(int wait_t);
void		XR16L788Error(int errorNum, char *errorMsg);
void		XR16L788HWReset();

void		XR16L788Install(void);
STATUS		XR16L788Drv(void);
STATUS		XR16L788DevCreate(char *name, int channel);

XR16L788_CHAN* XR16L788Open(XR16L788_CHAN *sioDevHeader, char *remainder, int mode);
int			XR16L788Close(XR16L788_CHAN *sioDevHeader);
int			XR16L788Read(XR16L788_CHAN *sioDevHeader, char *buffer, int nchars);
int			XR16L788Write(XR16L788_CHAN *sioDevHeader, char *buffer, int nchars);
int			XR16L788Ioctl(XR16L788_CHAN *sioDevHeader, int ioctlCode, int *arg);

int			XR16L788IntInit();
void		XR16L788RxIntSet(int num_chan);
void		XR16L788TxIntSet(int num_chan);
void		XR16L788TxIntUnSet(int num_chan);
void		XR16L788_1_Interrupt();



void		Fio_SET_TX_TIMEOUT(int num_chan, int time_out);
void		Fio_SET_RX_TIMEOUT(int num_chan, int time_out);


void		Fio_SET_INFRARED_CTR(int num_chan);

void		Fio_SET_LOOPBACK(int num_chan);
void		Fio_UNSET_LOOPBACK(int num_chan);
int			Fio_SET_BAUDRATE(int num_chan, int baudRate);
int			Fio_GET_BAUDRATE(XR16L788_CHAN *sioDevHeader);
void		Fio_R_FLUSH(XR16L788_CHAN *sioDevHeader, int num_chan);
void		Fio_W_FLUSH(XR16L788_CHAN *sioDevHeader, int num_chan);
void		Fio_CHAN_RESET(int num_chan);



void		Fio_SET_AUTO_RTSDTR(int num_chan);
void		Fio_UNSET_AUTO_RTSDTR(int num_chan);
void		Fio_SET_AUTO_CTSDSR(int num_chan);
void		Fio_UNSET_AUTO_CTSDSR(int num_chan);
void		Fio_SET_DATA_LEN(int num_chan, int char_len);
void		Fio_SET_STOP_BIT(int num_chan, int stop_bit);
void		Fio_SET_PARITY(int num_chan, int parity);
void		Fio_SET_RTS_ON(int num_chan);
void		Fio_SET_RTS_OFF(int num_chan);
void		Fio_SET_DTR_ON(int num_chan);
void		Fio_SET_DTR_OFF(int num_chan);
STATUS		Fio_GET_CTS_STATUS(int num_chan);
STATUS		Fio_GET_DSR_STATUS(int num_chan);
STATUS		Fio_GET_CD_STATUS(int num_chan);

void		RegShow(int num_chan);
/**************************************************
*			Local Function declaration			*
**************************************************/


void	XR16L788wait(int wait_t)
{
	while(wait_t--);
}


/******************************************************************************/
/* Function    : XR16L788Error                                                */
/* Purpose     : error print utility. Prints an error message and sets errno  */
/* Inputs      : error number and error message                               */
/* Outputs     : None                                                         */
/******************************************************************************/
void XR16L788Error(int errorNum, char *errorMsg)
{
#if(DEBUG_FLAG == DEBUG_RUNNING)
    printf("%s", errorMsg);
#endif

    errnoSet(errorNum);
} /* XR16L788Error() */


/****************************************************************************
* Function	: XR16L788HWReset                              					*
* Purpose   : XR16L788 H/W Reset                           					*
* Inputs    : None                                   						*
* Outputs   : None                                    						*
****************************************************************************/
void XR16L788HWReset(void)
{
	*(char *)XR16L788_RESET = 0xff;

	taskDelay(1);
}

void XR16L788DevInit(void)
{
	
	int  num_chan;
	
	xr16l788reg1 = (XR16L788REG *)(XR16L788_1_BASE_ADDRESS);

	for(num_chan = 0; num_chan < NUM_OF_XR16L788_CHAN; num_chan++){
		xr16l788reg1->reg[num_chan].comp16550.lcr		= 0x03;
		xr16l788reg1->reg[num_chan].comp16550.fcr_iir	= 0xC7;
		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	= 0x00;
		xr16l788reg1->reg[num_chan].comp16550.mcr		= 0x00;
	}
}

/******************************************************************************/
/* Function    :  XR16L788Install                                             */
/* Purpose     :  install driver and initialize XR16L788 devices              */
/******************************************************************************/
void XR16L788Install(void)
{
	int  num_chan;
	char name[20];
	int  status;
	
	XR16L788DevInit();	

	if(XR16L788Drv() == ERROR){
    	printf("can not install driver\n");
    	return;
	}

	for(num_chan = 0; num_chan < NUM_OF_XR16L788_CHAN; num_chan++){
		sprintf(name, "/tyCo/%d", num_chan+2);
		status = XR16L788DevCreate(name, num_chan);
	}
}


/******************************************************************************/
/* Function    : XR16L788Drv                                                  */
/* Purpose     : SIO Driver Initialization Function. Installs the driver      */
/* Inputs      : Hardware interrupt vector base and interrupt level           */
/* Outputs     : Reurns OK or ERROR and adds driver to OS driver table        */
/* Note        : allocates data structers, establishes the interrupt service  */
/*               routine for the driver and initializes the SIO hardware.     */
/******************************************************************************/
STATUS XR16L788Drv(void)
{
	int  num_chan;         /* Channel counter */
	
	if(sioDrvNum > 0)	return(OK);

	if( (XR16L788_DEV.channel = (XR16L788_CHAN *)calloc(NUM_OF_XR16L788_CHAN, sizeof(XR16L788_CHAN)))== NULL){
		XR16L788Error(ESIO_MALLOC, "channel structure malloc failed\n");
		return(ERROR);
	}
	
	for(num_chan = 0; num_chan < NUM_OF_XR16L788_CHAN; num_chan++){
		XR16L788_DEV.channel[num_chan].num_chan     	= num_chan;
		XR16L788_DEV.channel[num_chan].bufcreated      	= FALSE;
		XR16L788_DEV.channel[num_chan].chanOpen     	= FALSE;
		XR16L788_DEV.channel[num_chan].loopback     	= FALSE;
		XR16L788_DEV.channel[num_chan].wrtStateBusy 	= FALSE;
		XR16L788_DEV.channel[num_chan].txBufferSize 	= TxBufferSize;
		XR16L788_DEV.channel[num_chan].rxBufferSize 	= RxBufferSize;
		XR16L788_DEV.channel[num_chan].TxTimeOut		= DefaultTxTimeOut;
		XR16L788_DEV.channel[num_chan].RxTimeOut		= DefaultRxTimeOut;
		XR16L788_DEV.channel[num_chan].selWakeup		= TRUE;
		XR16L788_DEV.channel[num_chan].setRTSCTSCtrl	= FALSE;
		XR16L788_DEV.channel[num_chan].txIntCnt         = 0;
		XR16L788_DEV.channel[num_chan].rxIntCnt         = 0;

		Fio_CHAN_RESET(num_chan);
		
		/* Set baud rate 9600 and line format: 8 data, No Parity, 1 stop  */
		Fio_SET_BAUDRATE(num_chan, DEFAULT_BAUD_RATE);
		Fio_SET_DATA_LEN(num_chan, CHAR_LEN_8);
		Fio_SET_STOP_BIT(num_chan, STOP_BIT_1);
		Fio_SET_PARITY(num_chan, SET_NO_PARITY);
	} /* for */

	XR16L788IntInit();
	
	if((sioDrvNum = iosDrvInstall(	(FUNCPTR)NULL, 
									(FUNCPTR)NULL, 
									(FUNCPTR)XR16L788Open, 
									(FUNCPTR)XR16L788Close,
									(FUNCPTR)XR16L788Read, 
									(FUNCPTR)XR16L788Write, 
									(FUNCPTR)XR16L788Ioctl)) < 0){
		printf("XR16L788DrvInstall Fail\n");
		return(ERROR); /* error number set by iosDrvInstall */
	}
	
	return(OK); /* driver successfully installed */
} /* XR16L788Drv() */


/******************************************************************************/
/* Function    : XR16L788DevCreate                                             */
/* Purpose     : Creates an sio device entry for use by open/close etc        */
/* Inputs      : Name, channel, initial line parameters, protocol             */
/* Outputs     : Returns OK or ERROR                                          */
/******************************************************************************/
STATUS XR16L788DevCreate(char *name, int num_chan)
{
	if(sioDrvNum <= 0){
		XR16L788Error(ESIO_NO_DRIVER, "SIO Driver Not Installed\n");
		return(ERROR);
	}

	if(num_chan >= NUM_OF_XR16L788_CHAN){
		XR16L788Error(ESIO_NO_CHAN, "SIO No Channel\n");
		return(ERROR);
	}
	
	/* check that the channel's initialized  */
	if(XR16L788_DEV.channel[num_chan].num_chan != num_chan){
		XR16L788Error(ESIO_NO_CHAN, "XR16L788DevCreate: SIO No Channel\n");
		return(ERROR);
	}

	/* if this device already exists then don't create it. We use the TAS to       */
	/* prevent other tasks from creating this channel simultaneously and also mark */
	/* the channel as having been bufcreated                                          */
	if(vxTas(&(XR16L788_DEV.channel[num_chan].bufcreated)) == FALSE){
		XR16L788Error(ESIO_CEXIST, "XR16L788DevCreate: Channel already created\n");
		return(OK);
	}
	
	/* allocate read and write ring buffers   */
	if((XR16L788_DEV.channel[num_chan].wrtBuf = 
	    rngCreate(XR16L788_DEV.channel[num_chan].txBufferSize)) == NULL)
	{
		XR16L788_DEV.channel[num_chan].bufcreated = FALSE;
		XR16L788Error(ESIO_TY_DEV, "XR16L788DevCreate: Cannot allocate buffers\n");
		return(ERROR);
	} /* if */
	
	if((XR16L788_DEV.channel[num_chan].rdBuf = 
	    rngCreate(XR16L788_DEV.channel[num_chan].rxBufferSize)) == NULL)
	{
		rngDelete(XR16L788_DEV.channel[num_chan].wrtBuf);
		XR16L788_DEV.channel[num_chan].bufcreated = FALSE;
		XR16L788Error(ESIO_TY_DEV, "XR16L788DevCreate: Cannot allocate buffers\n");
	    return(ERROR);
	} /* if */

	XR16L788_DEV.channel[num_chan].mutexSemWr	= semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
	XR16L788_DEV.channel[num_chan].mutexSemRd	= semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
	
	XR16L788_DEV.channel[num_chan].binSemWr		= semBCreate(SEM_Q_FIFO, SEM_EMPTY);
	XR16L788_DEV.channel[num_chan].binSemRd		= semBCreate(SEM_Q_FIFO, SEM_EMPTY);

	selWakeupListInit(&(XR16L788_DEV.channel[num_chan].selWakeupList));
	
	/* Add the device to the vxWorks IO system */
	if(iosDevAdd(&(XR16L788_DEV.channel[num_chan].devHdr), name, sioDrvNum) != OK){
		XR16L788_DEV.channel[num_chan].bufcreated = FALSE;
		printf("XR16L788DevCreate: error adding %s to I/O system.\n", name);
		return(ERROR);
	} /* if */

#if(DEBUG_FLAG == DEBUG_RUNNING)
	printf("XR16L788DevCreate: %s bufcreated\n", name);
#endif
	
	return(OK);
} /* XR16L788DevCreate() */



/******************************************************************************/
/* Function    : XR16L788IntInit                                               */
/* Purpose     : Initialize and enable hardware interrupts                    */
/* Inputs      : device, interrupt vector and interrupt level                 */
/* Outputs     : None                                                         */
/******************************************************************************/
int XR16L788IntInit()
{
	if(sioIntWasSet)    	return(OK);
	
	/* Attach interrupt handlers for hardware */
	if(intConnect(XR16L788_1_INT_VEC, (VOIDFUNCPTR)XR16L788_1_Interrupt,0) == ERROR){
		printf("Failed to connect to vector %d\n", (int)XR16L788_1_INT_VEC);
		return(ERROR);
	}
	else{
		sioIntWasSet	= TRUE;
		

	}
	intEnable((int)XR16L788_1_INT_VEC);

	

	return(OK);
} /* XR16L788IntInit() */


void XR16L788RxIntSet(int num_chan)
{
	int    oldlevel;
	
	oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.dlm_ier	|= RxFIFO_BIT;
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_INTERRUPT)
	logMsg("XR16L788RxIntSet : %d\n",num_chan,0,0,0,0,0);
#endif
} /* XR16L788RxIntSet() */


void XR16L788RxIntUnSet(int num_chan)
{
	int    oldlevel;
	
	oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.dlm_ier	&= ~RxFIFO_BIT;


	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_INTERRUPT)
	logMsg("XR16L788RxIntUnSet : %d\n",num_chan,0,0,0,0,0);
#endif
} /* XR16L788RxIntUnSet() */


void XR16L788TxIntSet(int num_chan)
{
	int    oldlevel;
	
	oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	if(num_chan < 8){	
		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	|= TxFIFO_BIT;
		
	}
	
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */
	
#if(DEBUG_FLAG == DEBUG_INTERRUPT)
	logMsg("XR16L788TxIntSet : %d\n",num_chan,0,0,0,0,0);
#endif	
} /* XR16L788IntSet() */


void XR16L788TxIntUnSet(int num_chan)
{
	int    oldlevel;
	
	oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.dlm_ier	&= ~(TxFIFO_BIT);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_INTERRUPT)
	logMsg("XR16L788TxIntUnSet : %d\n",num_chan,0,0,0,0,0);
#endif
} /* XR16L788IntSet() */

#include "tickLib.h"


#undef	CHAN_SCANNING

#define	CHAN_SCANNING

#define	ISR_INT_LOCK
/*
#undef	ISR_INT_LOCK
*/
/****************************************************************************/
/* Function    : XR16L788Interrupt                                           */
/* Purpose     : IP-Octal Module Interrupt Handler                          */
/* Inputs      : IP Module Identifier                                       */
/* Outputs     : Returns OK or ERROR                                        */
/****************************************************************************/
void	testDelay(int max_cnt)
{
	int	i, j;
	
	for(i=0;i<max_cnt;i++)	j++;
}



#if XR16L_SINGLE_TX_MODE
int xr16l_tx_inx = 0;
typedef struct {
	UINT8				ready_flag;
	UINT8				iir_thre_status;
	UINT8				reserved;
	UINT8				out_ch;
	
	UINT32			s_cnt;
	UINT32			r_cnt;
	UINT32			p_cnt;
	UINT32			c_cnt;
} XR16L_TX_INT;

XR16L_TX_INT xr16l_tx_buf[N_SIO_CHANNELS];

void
xr16lint_tx_proc()
{
	register int	i, inx, find_it;
	XR16L_TX_INT	*txChan;
	
	
	find_it = FALSE;
	inx = xr16l_tx_inx;
	txChan = &(xr16l_tx_buf[xr16l_tx_inx]);
	for (i = 0; i < N_SIO_CHANNELS; i++) { 
		if (txChan->ready_flag && txChan->iir_thre_status) {	/* I am ready */
			find_it = TRUE;
			
			xr16l788reg1->reg[inx].comp16550.rbr_thr_dll = txChan->out_ch;
			XR16L788_DEV.channel[inx].actualTxDataNum++;
			XR16L788TxIntSet(inx);
			
			txChan->ready_flag = FALSE;
			txChan->p_cnt += 1;
			break;
		}
		inx++, txChan++;
		if (inx == N_SIO_CHANNELS) {
			inx = 0;
			txChan = &(xr16l_tx_buf[0]);
		}
	}
	find_it = FALSE;
	for (i = 0; i < N_SIO_CHANNELS; i++) {
		if (txChan->ready_flag)	{
			if (!find_it && !(txChan->iir_thre_status)) {
				/* 준비는 되어 있으나, interrupt 조건이 안됨 */
				XR16L788TxIntSet(inx);
				xr16l_tx_inx = inx;
				find_it = TRUE;
			} else {
				txChan->iir_thre_status = FALSE;
			}
		}
		inx++, txChan++;
		if (inx == N_SIO_CHANNELS) {
			inx = 0;
			txChan = &(xr16l_tx_buf[0]);
		}
	}
}

#endif

#if SIMPLE_TTY_RX_MODE
void XR16L788_1_Interrupt()
{
	register char	intStatus = 0;  /* Interrupt status register (ISR)    */
	char			rxByte;       	/* character recevied byte            */
	register int	num_chan = 0;   /* Device serial channel number       */
	register int	temp;         	/* Temporary Register for Ring Put    */
	register int	status;
	register int	chan;
	int				txByte; 		/* character to send byte */
	XR16L788_CHAN	*channel;
	int				selFlag;
#ifdef	ISR_INT_LOCK
	register int	oldlevel;
#endif
#if XR16L_SINGLE_TX_MODE
	XR16L_TX_INT	*txChan;
#endif	/* XR16L_SINGLE_TX_MODE */
	

#ifdef	ISR_INT_LOCK
	oldlevel = intLock(); /* LOCK INTERRUPTS   */
#endif

	for(chan = 0;chan<ONE_OF_XR16L788_CHAN;chan++) {
		num_chan = chan;
		
		intStatus = xr16l788reg1->reg[num_chan].comp16550.fcr_iir & 0x0f;
		
		channel = &(XR16L788_DEV.channel[num_chan]);
		
/*	Read Function		*/
		if((intStatus == IIR_RDA) || (intStatus == IIR_TIMEOUT)){
			status = xr16l788reg1->reg[num_chan].comp16550.lsr;

			if(status & (LSR_OE | LSR_PE | LSR_FE | LSR_FE | LSR_BI | LSR_FERR)){
				if(status & LSR_OE){
					channel->rxOverRunErr++;
					xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_OE);
				}
				if(status & LSR_PE){
					channel->rxParityErr++;
					xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_PE);
				}
				if(status & LSR_FE){
					channel->rxFramingErr++;
					xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_FE);
				}
				if(status & LSR_BI){
					channel->rxBreak++;
					xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_BI);
				}
				if(status & LSR_FERR){
					channel->rxFifoError++;
					xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_FERR);
				}
				rxByte = xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll;
			
				if(channel->reqRxDataNum > 0)	{
					channel->actualRxDataNum++;
					if(channel->actualRxDataNum == 
						channel->reqRxDataNum)		semGive(channel->binSemRd);
				}
			} /*	Error Processing	*/
			else{
				selFlag = 0;
				while(1){
				
					if( (xr16l788reg1->reg[num_chan].comp16550.lsr & 0x01) == 0x00)	break;
					rxByte = xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll;
				
					status = RNG_ELEM_PUT(channel->rdBuf, rxByte, temp);
				
					if(status == 1) {
						channel->rxIntCnt++;
				
						if (!selFlag && rngNBytes(channel->rdBuf) == 1)	selFlag = TRUE;
					} else						KVME402A_RX_PUT_ERR[(num_chan+2)] += 1;	

					if(channel->reqRxDataNum > 0){
						channel->actualRxDataNum++;
					
						if(	channel->actualRxDataNum == 
							channel->reqRxDataNum)	 
							semGive(channel->binSemRd);
					}
				} /* while */
				if (selFlag && selWakeupListLen(&(channel->selWakeupList) > 0)) {
					selWakeupAll(&(channel->selWakeupList), SELREAD);
				}
			} /* else */
		} /* if */

/*	Write Function		*/
		if(intStatus == IIR_THRE && channel->wrtStateBusy == TRUE){
			channel->txIntCnt++;
		
#if XR16L_SINGLE_TX_MODE
			txChan = &(xr16l_tx_buf[chan]);
			txChan->iir_thre_status = TRUE;
			txChan->s_cnt += 1;
			if (!(txChan->ready_flag)) {
				if(RNG_ELEM_GET(channel->wrtBuf, &txByte, temp)){
					txChan->ready_flag = TRUE;
					txChan->out_ch = txByte;
				} else {
					channel->wrtStateBusy = FALSE; /* no more chars */
					selWakeupAll(&(channel->selWakeupList), SELWRITE);
					if(channel->setRTSCTSCtrl == TRUE)	Fio_SET_RTS_OFF(num_chan);
				}
			}	
			XR16L788TxIntUnSet(num_chan);
#else	/* NON-XR16L_SINGLE_TX_MODE */
	
			if(RNG_ELEM_GET(channel->wrtBuf, &txByte, temp)){
				xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll = txByte;
				channel->actualTxDataNum++;
			} else{
				channel->wrtStateBusy = FALSE; /* no more chars */

				selWakeupAll(&(channel->selWakeupList), SELWRITE);

				if(channel->setRTSCTSCtrl == TRUE)	Fio_SET_RTS_OFF(num_chan);

				XR16L788TxIntUnSet(num_chan);
			} /* if */
#endif	/* NON-XR16L_SINGLE_TX_MODE */
		} /* if */
	}	/* for */


#ifdef	ISR_INT_LOCK
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */
#endif
} /* XR16L788_1_Interrupt() */


#else	/* NON-SIMPLE_TTY_RX_MODE */


void XR16L788_1_Interrupt()
{
	register char	intStatus = 0;  /* Interrupt status register (ISR)    */
	char			rxByte;       	/* character recevied byte            */
	register int	num_chan = 0;   /* Device serial channel number       */
	register int	temp;         	/* Temporary Register for Ring Put    */
	register int	status;
	register int	chan;
	int				txByte; 		/* character to send byte */
	int				selFlag;
#ifdef	ISR_INT_LOCK
	register int	oldlevel;
#endif
	

#ifdef	ISR_INT_LOCK
	oldlevel = intLock(); /* LOCK INTERRUPTS   */
#endif

	for(chan = 0;chan<ONE_OF_XR16L788_CHAN;chan++) {
		num_chan = chan;
		
		intStatus = xr16l788reg1->reg[num_chan].comp16550.fcr_iir & 0x0f;
			
		
/*	Read Function		*/
	if((intStatus == IIR_RDA) || (intStatus == IIR_TIMEOUT)){
		status = xr16l788reg1->reg[num_chan].comp16550.lsr;

		if(status & (LSR_OE | LSR_PE | LSR_FE | LSR_FE | LSR_BI | LSR_FERR)){
			if(status & LSR_OE){
				XR16L788_DEV.channel[num_chan].rxOverRunErr++;
				xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_OE);
			}
			if(status & LSR_PE){
				XR16L788_DEV.channel[num_chan].rxParityErr++;
				xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_PE);
			}
			if(status & LSR_FE){
				XR16L788_DEV.channel[num_chan].rxFramingErr++;
				xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_FE);
			}
			if(status & LSR_BI){
				XR16L788_DEV.channel[num_chan].rxBreak++;
				xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_BI);
			}
			if(status & LSR_FERR){
				XR16L788_DEV.channel[num_chan].rxFifoError++;
				xr16l788reg1->reg[num_chan].comp16550.lsr &= ~(LSR_FERR);
			}
			rxByte = xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll;
			
			if(XR16L788_DEV.channel[num_chan].reqRxDataNum > 0)	{
				XR16L788_DEV.channel[num_chan].actualRxDataNum++;
				if(	XR16L788_DEV.channel[num_chan].actualRxDataNum == 
					XR16L788_DEV.channel[num_chan].reqRxDataNum)		semGive(XR16L788_DEV.channel[num_chan].binSemRd);
			}
		} /*	Error Processing	*/
		else{
			selFlag = FALSE;
			while(1){
				
				if( (xr16l788reg1->reg[num_chan].comp16550.lsr & 0x01) == 0x00)	break;
				rxByte = xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll;
				
				status = RNG_ELEM_PUT(XR16L788_DEV.channel[num_chan].rdBuf, rxByte, temp);
				
				if(status == 1) {
					if (!selFlag && rngNBytes(XR16L788_DEV.channel[num_chan].rdBuf) == 1)	selFlag = TRUE;
					XR16L788_DEV.channel[num_chan].rxIntCnt++;			
				} else						KVME402A_RX_PUT_ERR[(num_chan+2)] += 1;	

				if(XR16L788_DEV.channel[num_chan].reqRxDataNum > 0){
					XR16L788_DEV.channel[num_chan].actualRxDataNum++;
					
					if(	XR16L788_DEV.channel[num_chan].actualRxDataNum == 
						XR16L788_DEV.channel[num_chan].reqRxDataNum)	 
						semGive(XR16L788_DEV.channel[num_chan].binSemRd);
				}
			} /* while */
			if (selFlag && selWakeupListLen(&(XR16L788_DEV.channel[num_chan].selWakeupList) > 0)) {
				selWakeupAll(&(XR16L788_DEV.channel[num_chan].selWakeupList), SELREAD);
			}
		} /* else */
	} /* if */

/*	Write Function		*/
	if(intStatus == IIR_THRE && XR16L788_DEV.channel[num_chan].wrtStateBusy == TRUE){
		XR16L788_DEV.channel[num_chan].txIntCnt++;
	
		if(RNG_ELEM_GET(XR16L788_DEV.channel[num_chan].wrtBuf, &txByte, temp)){
			xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll = txByte;
			XR16L788_DEV.channel[num_chan].actualTxDataNum++;
		} 
#if 1		
		else{
			XR16L788_DEV.channel[num_chan].wrtStateBusy = FALSE; /* no more chars */

			selWakeupAll(&(XR16L788_DEV.channel[num_chan].selWakeupList), SELWRITE);

			if(XR16L788_DEV.channel[num_chan].setRTSCTSCtrl == TRUE)	Fio_SET_RTS_OFF(num_chan);

			XR16L788TxIntUnSet(num_chan);
		} /* if */
#endif		
	} /* if */

}


#ifdef	ISR_INT_LOCK
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */
#endif
} /* XR16L788_1_Interrupt() */
#endif	/* NON-SIMPLE_TTY_RX_MODE */

/******************************************************************************/
/* Function    : XR16L788Open                                                  */
/* Purpose     : opens the specified SIO device (called via IO system)        */
/* Inputs      : device specific data structure, path name, mode              */
/* Outputs     : returns a pointer to the device specific data structure      */
/******************************************************************************/
XR16L788_CHAN* XR16L788Open(XR16L788_CHAN *sioDevHeader, char *remainder, int mode)
{
	int	num_chan;
	
	num_chan = sioDevHeader->num_chan;
	
	/* use vxTas to implement mutual exclusion to prevent simultaneous channel access */
	if(vxTas(&(sioDevHeader->chanOpen)) == FALSE){
	        XR16L788Error(ESIO_COPEN, "XR16L788Open: SIO Channel already open\n");
		return((XR16L788_CHAN *)ERROR);
	} /* if */

	/* 해당 Channel이 Open되었음을 알린다. */
	sioDevHeader->chanOpen		= TRUE;
	sioDevHeader->baudRate		= DEFAULT_BAUD_RATE;
	sioDevHeader->dataBits		= CHAR_LEN_8;
	sioDevHeader->stopBits		= STOP_BIT_1;
	sioDevHeader->parity		= SET_NO_PARITY;

	/* Flush input and output ring buffers    */
	Fio_W_FLUSH(sioDevHeader, num_chan);
	Fio_R_FLUSH(sioDevHeader, num_chan);

	Fio_CHAN_RESET(num_chan);
	XR16L788wait(100);

	/* Set baud rate and line format options  */
	Fio_SET_BAUDRATE(num_chan, sioDevHeader->baudRate);
	Fio_SET_DATA_LEN(num_chan, sioDevHeader->dataBits);
	Fio_SET_STOP_BIT(num_chan, sioDevHeader->stopBits);
	Fio_SET_PARITY(num_chan, sioDevHeader->parity);

	/* sometimes this flag remains enabled -- can not explain it so far */
	sioDevHeader->wrtStateBusy = FALSE;
	
	XR16L788RxIntSet(num_chan);
	
	return(sioDevHeader);
} /* XR16L788Open() */


/******************************************************************************/
/* Function    : XR16L788Close                                                 */
/* Purpose     : closes the specified SIO device (called via IO system)       */
/* Inputs      : device specific data structure                               */
/* Outputs     : returns OK                                                   */
/******************************************************************************/
int XR16L788Close(XR16L788_CHAN *sioDevHeader)
{
	sioDevHeader->chanOpen = FALSE;

	return(OK);
} /* XR16L788Close() */

/******************************************************************************/
/* Function    : XR16L788Read                                                  */
/* Purpose     : reads up to maxchars from the specified SIO device           */
/* Inputs      : device specific data structure, buffer, maxchars             */
/* Outputs     : returns the actual number of chars read or ERROR.            */
/******************************************************************************/
int XR16L788PollRead(XR16L788_CHAN *sioDevHeader, char *buffer, int maxchars)
{
	int	 num_chan, time_limit, get_cnt = 0, time_cnt = 0;
	
	num_chan = sioDevHeader->num_chan;

	time_limit = XR16L788_DEV.channel[num_chan].RxTimeOut;
	
	if(num_chan < 8){
		while(maxchars--){
			while( (xr16l788reg1->reg[num_chan].comp16550.lsr & 0x01) == 0x00){
				taskDelay(1);
				if(time_limit != DefaultRxTimeOut)
					if(time_cnt++ > time_limit){

						return(ERROR);
					}
			}

	
			*buffer++ = xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll;
			 
			 get_cnt++;
		}
	}
	

	return(get_cnt);
} /* XR16L788Read() */

/******************************************************************************/
/* Function    : XR16L788PollWrite                                            */
/* Purpose     : writes nchars to the specified SIO device                    */
/* Inputs      : device specific data structure, buffer, nchars               */
/* Outputs     : returns the actual number of chars written or ERROR.         */
/******************************************************************************/
int XR16L788PollWrite(XR16L788_CHAN *sioDevHeader, char *buffer, int nchars)
{
	int	 num_chan, wchars = 0;
	
	num_chan = sioDevHeader->num_chan;

	if(num_chan < 8){
		while(nchars--){
			while( (xr16l788reg1->reg[num_chan].comp16550.lsr & 0x20) == 0x00)	XR16L788wait(100);
	
			xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll = *(buffer++);

			wchars++;
		}
	}
	

	return(wchars);
} /* XR16L788PollWrite() */

int pollTest(int num_chan, char *buffer, int nchars)
{
	int	 wchars = 0;
	int	loop_cnt;


	
	while(nchars--){


		loop_cnt = 0;
		
		if(num_chan < 8){
			while( (xr16l788reg1->reg[num_chan].comp16550.lsr & 0x20) == 0x00){	loop_cnt++;	XR16L788wait(1);	}

			xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll = *(buffer++);
			wchars++;
		}
		
	}

	return(wchars);
} /* pollTest */


/******************************************************************************/
/* Function    : XR16L788Write                                                */
/* Purpose     : task level write routine for sio device                      */
/* Inputs      : device specific data structure, buffer, number of bytes      */
/* Outputs     : number of bytes actually written to device or ERROR          */
/******************************************************************************/
int XR16L788Write(register XR16L788_CHAN *sioDevHeader, char *buffer, int nBytes)
{
	register int txbytesPut;
	
	if (nBytes <= 0) return(0);

	if(sioDevHeader->selWakeup == TRUE)
		if(semTake(XR16L788_DEV.channel[sioDevHeader->num_chan].mutexSemWr, WAIT_FOREVER) == ERROR)
			return(0);

	while (sioDevHeader->wrtStateBusy == TRUE) taskDelay(1);	/* waiting */
	txbytesPut = rngBufPut(sioDevHeader->wrtBuf, (char *)buffer, nBytes);
	if(sioDevHeader->setRTSCTSCtrl == TRUE)			Fio_SET_RTS_ON(sioDevHeader->num_chan);

	sioDevHeader->wrtStateBusy = TRUE;
	
	XR16L788TxIntSet(sioDevHeader->num_chan);
	
	if(sioDevHeader->selWakeup == TRUE)	
		semGive(XR16L788_DEV.channel[sioDevHeader->num_chan].mutexSemWr);
		
	return (txbytesPut);
} /* XR16L788BufWrite() */


/******************************************************************************/
/* Function    : XR16L788BufRead                                               */
/* Purpose     : task level read routine for sio device                       */
/* Inputs      : device specific data structure, buffer, buffer length        */
/* Outputs     : Returns number of bytes read or ERROR                        */
/******************************************************************************/
int XR16L788Read(register XR16L788_CHAN *sioDevHeader, char *buffer, int maxBytes)
{
	register RING_ID ringId;
	register int     nBytes;
	register int     temp;
	register int     oldlevel;


	char	rxByte;
	
	if (maxBytes <= 0) return(0);
	
	nBytes       = 0;
	ringId       = sioDevHeader->rdBuf;

	if(sioDevHeader->selWakeup == TRUE)
		if(semTake(XR16L788_DEV.channel[sioDevHeader->num_chan].mutexSemRd, WAIT_FOREVER) == ERROR)	return(0);


	
	oldlevel = intLock();
	sioDevHeader->reqRxDataNum = 0;
	intUnlock(oldlevel);
	
	if( CHECK_RNG_ELEM_GET(ringId) != 0){
	
	do{
		
		oldlevel = intLock();
		if(RNG_ELEM_GET(ringId, &rxByte, temp) == 0){
			intUnlock(oldlevel);
			break;
		}
		intUnlock(oldlevel);
		
		buffer[nBytes++] = rxByte;



	} while(nBytes < maxBytes);
	
	} /* if */
	else taskDelay(1);

	if(sioDevHeader->selWakeup == TRUE)	
		semGive(XR16L788_DEV.channel[sioDevHeader->num_chan].mutexSemRd);
	return(nBytes);
	
} /* XR16L788BufRead() */


/******************************************************************************/
/* Function    : XR16L788Ioctl                                                 */
/* Purpose     : provides various device specific functions for sio devices   */
/* Inputs      : device specific data structure, ioctl code, argument         */
/* Outputs     : returns OR or ERROR, Data may be returned                    */
/******************************************************************************/
int XR16L788Ioctl(XR16L788_CHAN *sioDevHeader, int ioctlCode, int *arg)
{
	int	ret_value = OK;
	int	num_chan;
	int	oldlevel;
		
	num_chan = sioDevHeader->num_chan;
	
	switch(ioctlCode){
		/*******************************************************
		*			XR16C788 Auto Control ftn				*
		*******************************************************/
		case FIONREAD:
			ret_value =0;
			oldlevel = intLock();
			ret_value = rngNBytes(sioDevHeader->rdBuf);
			*arg = ret_value;
			intUnlock(oldlevel);
			break;
		
		case FIONWRITE:
			oldlevel = intLock();
			*arg = rngNBytes(sioDevHeader->wrtBuf);
			intUnlock(oldlevel);
			break;



		case FIO_SET_TX_TIMEOUT :
			Fio_SET_TX_TIMEOUT(num_chan, (int)(int *)arg);
			break;
			
       	case FIO_SET_RX_TIMEOUT :
			Fio_SET_RX_TIMEOUT(num_chan, (int)(int *)arg);
			break;



		case FIO_SET_LOOPBACK	:
			Fio_SET_LOOPBACK(num_chan);
			break;

		case FIO_UNSET_LOOPBACK	:
			Fio_UNSET_LOOPBACK(num_chan);
			break;

		case FIOBAUDRATE	:
		case FIO_SET_BAUDRATE	:
			sioDevHeader->baudRate = (int)(int *)arg;
			Fio_SET_BAUDRATE(num_chan,(int)(int *)arg);
			break;

		case FIO_GET_BAUDRATE	:
			return(Fio_GET_BAUDRATE(sioDevHeader));
			break;

		case FIOFLUSH:     /* make a rx/tx ring buffer empty         */
			Fio_R_FLUSH(sioDevHeader, num_chan);
			Fio_W_FLUSH(sioDevHeader, num_chan);
			break;

		case FIORFLUSH :
		case FIO_R_FLUSH	:
			Fio_R_FLUSH(sioDevHeader, num_chan);
			break;

		case FIOWFLUSH :
		case FIO_W_FLUSH	:
			Fio_W_FLUSH(sioDevHeader, num_chan);
			break;

		case FIO_CHAN_RESET	:
			Fio_CHAN_RESET(num_chan);
			break;

		case FIOSELECT:
			selNodeAdd(&(sioDevHeader->selWakeupList), (SEL_WAKEUP_NODE *)arg);
			if(selWakeupType((SEL_WAKEUP_NODE *)arg) == SELREAD && rngNBytes(sioDevHeader->rdBuf) > 0) {
				selWakeup((SEL_WAKEUP_NODE *)arg);
			}
		
			if(selWakeupType((SEL_WAKEUP_NODE *)arg) == SELWRITE && sioDevHeader->wrtStateBusy == FALSE) {
				selWakeup((SEL_WAKEUP_NODE *)arg);
			}
			break;
		
		case FIOUNSELECT:
			selNodeDelete(&(sioDevHeader->selWakeupList), (SEL_WAKEUP_NODE *)arg);
			break;

		case FIO_RTSCTSCTL: /* determine RTS/CTS Control              */
			if((int)(int *)arg == TRUE)	sioDevHeader->setRTSCTSCtrl = TRUE;
			else						sioDevHeader->setRTSCTSCtrl = FALSE;
#if(DEBUG_FLAG == DEBUG_IOCTL)
			if(sioDevHeader->setRTSCTSCtrl == TRUE)	printf("chan : %d FIO_RTSCTSCTL Set\n",num_chan);
			else									printf("chan : %d FIO_RTSCTSCTL Unset\n",num_chan);
#endif
			break;
        	
		/*******************************************************
		*			XR16C788 Auto Control ftn				*
		*******************************************************/
	


		case FIO_SET_AUTO_RTSDTR	:
			Fio_SET_AUTO_RTSDTR(num_chan);
			break;

		case FIO_UNSET_AUTO_RTSDTR	:
			Fio_UNSET_AUTO_RTSDTR(num_chan);
			break;

		case FIO_SET_AUTO_CTSDSR	:
			Fio_SET_AUTO_CTSDSR(num_chan);
			break;

		case FIO_UNSET_AUTO_CTSDSR	:
			Fio_UNSET_AUTO_CTSDSR(num_chan);
			break;

		case FIO_SET_DATA_LEN	:
			Fio_SET_DATA_LEN(num_chan,(int)(int *)arg);
			sioDevHeader->dataBits = (int)(int *)arg;
			break;

		case FIO_SET_STOP_BIT	:
			Fio_SET_STOP_BIT(num_chan,(int)(int *)arg);
			sioDevHeader->stopBits = (int)(int *)arg;
			break;

		case FIO_SET_PARITY	:
			Fio_SET_PARITY(num_chan,(int)(int *)arg);
			sioDevHeader->parity = (int)(int *)arg;
			break;

		case FIO_SET_RTS_ON	:
			Fio_SET_RTS_ON(num_chan);
			break;

		case FIO_SET_RTS_OFF	:
			Fio_SET_RTS_OFF(num_chan);
			break;

		case FIO_SET_DTR_ON	:
			Fio_SET_DTR_ON(num_chan);
			break;

		case FIO_SET_DTR_OFF	:
			Fio_SET_DTR_OFF(num_chan);
			break;

		case FIO_GET_CTS_STATUS	:
			ret_value = Fio_GET_CTS_STATUS(num_chan);
			break;

		case FIO_GET_DSR_STATUS	:
			ret_value = Fio_GET_DSR_STATUS(num_chan);
			break;

		case FIO_GET_CD_STATUS	:
			ret_value = Fio_GET_CD_STATUS(num_chan);
			break;

		case FIO_RX_OVER_RUN_ERR:
			ret_value = sioDevHeader->rxOverRunErr;
			break;
			
		case FIO_RX_PARITY_ERR:
			ret_value = sioDevHeader->rxParityErr;
			break;

		case FIO_RX_FRAMING_ERR:
			ret_value = sioDevHeader->rxFramingErr;
			break;
			
		case FIO_RX_BREAK_ERR:
			ret_value = sioDevHeader->rxBreak;
			break;

		case FIO_RX_FIFO_ERR:
			ret_value = sioDevHeader->rxFifoError;
			break;

		default:
			errnoSet(S_ioLib_UNKNOWN_REQUEST);
			return(ERROR);
	} /* switch */
	
	return (ret_value);
}

















/*******************************************************
*												*
*			XR16L788 IOCTL Function				*
*												*
*******************************************************/




void	Fio_SET_TX_TIMEOUT(int num_chan, int time_out)
{
	XR16L788_DEV.channel[num_chan].TxTimeOut	= time_out;
}

void	Fio_SET_RX_TIMEOUT(int num_chan, int time_out)
{
	XR16L788_DEV.channel[num_chan].RxTimeOut	= time_out;
}






void	Fio_SET_LOOPBACK(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr 		|= INTERNAL_LOOPBACK;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_LOOPBACK OK\n",num_chan);
#endif
}

void	Fio_UNSET_LOOPBACK(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr 		&= ~INTERNAL_LOOPBACK;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_UNSET_LOOPBACK OK\n",num_chan);
#endif
}

int	Fio_SET_BAUDRATE(int num_chan, int baudRate)
{
	int	oldlevel;
	char	dlm_value = 0, dll_value = 0, mcr_value = 0;
#if 0		
	switch (baudRate){
		case 100	:
			dlm_value	= 0x09;
			dll_value		= 0x00;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 400	:
			dlm_value	= 0x09;
			dll_value		= 0x00;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_0_OR;
			break;

		case 600	:
			dlm_value	= 0x01;
			dll_value		= 0x80;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 1200	:
			dlm_value	= 0x00;
			dll_value		= 0xC0;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 2400	:
			dlm_value	= 0x00;
			dll_value		= 0x60;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 4800	:
			dlm_value	= 0x00;
			dll_value		= 0x30;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;
			
		case 9600 :	
			dlm_value	= 0x00;
			dll_value		= 0x18;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 19200 :	
			dlm_value	= 0x00;
			dll_value		= 0x0C;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 38400 :	
			dlm_value	= 0x00;
			dll_value		= 0x06;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 57600 :	
			dlm_value	= 0x00;
			dll_value		= 0x04;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 76800 :	
			dlm_value	= 0x00;
			dll_value		= 0x0C;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_0_OR;
			break;

		case 115200 :	
			dlm_value	= 0x00;
			dll_value		= 0x02;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 153600 :	
			dlm_value	= 0x00;
			dll_value		= 0x06;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_0_OR;
			break;

		case 230400 :	
			dlm_value	= 0x00;
			dll_value		= 0x01;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
			break;

		case 460800 :	
			dlm_value	= 0x00;
			dll_value		= 0x02;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_0_OR;
			break;

		case 921600 :	
			dlm_value	= 0x00;
			dll_value		= 0x01;
			mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_0_OR;
			break;

		default : 
			printf("set baudRate : %d, is not supported!!\n",baudRate);
			return(ERROR);
			break;
	}
#else
		dll_value = 7372800/(16 * baudRate);
    	dlm_value = (7372800/(16 * baudRate)) >> 8;
    	mcr_value	= (char)OUTPUT_DATA_RATE_MCR7_1_OR;
#endif

	oldlevel = intLock(); /* LOCK INTERRUPTS   */

	
	if(mcr_value == OUTPUT_DATA_RATE_MCR7_1_OR){
		if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr		|= mcr_value;
	

	}
	else{
		if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr		&= mcr_value;

	}
	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.lcr		= 0x83;
		xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll	= dll_value;
		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	= dlm_value;
		xr16l788reg1->reg[num_chan].comp16550.lcr		= 0x03;
	}
	
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */


	return(OK);
}

int	Fio_GET_BAUDRATE(XR16L788_CHAN *sioDevHeader)
{
	int cur_baudrate;

	cur_baudrate = sioDevHeader->baudRate;

	return(cur_baudrate);
}

void	Fio_R_FLUSH(XR16L788_CHAN *sioDevHeader, int num_chan)
{
	int oldlevel;
	
	semTake(XR16L788_DEV.channel[num_chan].mutexSemRd, WAIT_FOREVER);
	
	oldlevel = intLock();
	
	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.fcr_iir	|= FCR_RXCLR;

	
	rngFlush(sioDevHeader->rdBuf);
	
	intUnlock(oldlevel);
	
	semGive(XR16L788_DEV.channel[num_chan].mutexSemRd);
	

}

void	Fio_W_FLUSH(XR16L788_CHAN *sioDevHeader, int num_chan)
{
	int oldlevel;

	semTake (XR16L788_DEV.channel[num_chan].mutexSemWr, WAIT_FOREVER);

	oldlevel = intLock();

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.fcr_iir	|= FCR_TXCLR;


	rngFlush(sioDevHeader->wrtBuf);

	intUnlock(oldlevel);
	
	semGive (XR16L788_DEV.channel[num_chan].mutexSemWr);
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */


}

void	Fio_CHAN_RESET(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	xr16l788reg1->reg[num_chan].comp16550.fcr_iir	= 0xc7;
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

}





/********************************************************************
*			XR16C788 Auto Control ftn						      *
********************************************************************/




void	Fio_SET_AUTO_RTSDTR(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.mcr 		|= MCR_RTS;

		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	|= RTS_DTR_INT_ENABLE;
	}
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_AUTO_RTSDTR OK\n",num_chan);
#endif
}

void	Fio_UNSET_AUTO_RTSDTR(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.mcr 		&= ~MCR_RTS;

		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	&= ~RTS_DTR_INT_ENABLE;
	}
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_UNSET_AUTO_RTSDTR OK\n",num_chan);
#endif
}

void	Fio_SET_AUTO_CTSDSR(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.mcr 		|= MCR_RTS;

		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	|= CTS_DSR_INT_ENABLE;
	}
	
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */
                                   
#if(DEBUG_FLAG == DEBUG_IOCTL)     
	printf("chan : %d Fio_SET_AUTO_CTSDSR OK\n",num_chan);
#endif
}

void	Fio_UNSET_AUTO_CTSDSR(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.mcr 		&= ~MCR_RTS;

		xr16l788reg1->reg[num_chan].comp16550.dlm_ier	&= ~CTS_DSR_INT_ENABLE;
	}
	
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_UNSET_AUTO_CTSDSR OK\n",num_chan);
#endif
}


void	Fio_SET_DATA_LEN(int num_chan, int char_len)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.lcr	&= CHAR_LEN_CLEAR;
		xr16l788reg1->reg[num_chan].comp16550.lcr	|= char_len;
	}
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_DATA_LEN : %d OK\n",num_chan, char_len);
#endif
}


void	Fio_SET_STOP_BIT(int num_chan, int stop_bit)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.lcr	&= CHAR_STOP_BIT_CLEAR;
		xr16l788reg1->reg[num_chan].comp16550.lcr	|= stop_bit;
	}
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_STOP_BIT : %d OK\n",num_chan, stop_bit);
#endif
}


void	Fio_SET_PARITY(int num_chan, int parity)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8){
		xr16l788reg1->reg[num_chan].comp16550.lcr	&= CHAR_PARITY_CLEAR;
		xr16l788reg1->reg[num_chan].comp16550.lcr	|= parity;
	}
	

	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_PARITY : %d OK\n",num_chan, parity);
#endif
}

void	Fio_SET_RTS_ON(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr	|= MCR_RTS_ON;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_RTS_ON OK\n",num_chan);
#endif
}

void	Fio_SET_RTS_OFF(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr	&= MCR_RTS_OFF;

		
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_RTS_OFF OK\n",num_chan);
#endif
}

void	Fio_SET_DTR_ON(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	if(num_chan < 8)	xr16l788reg1->reg[num_chan].comp16550.mcr	|= MCR_DTR_ON;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_DTR_ON OK\n",num_chan);
#endif
}

void	Fio_SET_DTR_OFF(int num_chan)
{
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	xr16l788reg1->reg[num_chan].comp16550.mcr	&= MCR_DTR_OFF;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	printf("chan : %d Fio_SET_DTR_OFF OK\n",num_chan);
#endif
}

STATUS	Fio_GET_CTS_STATUS(int num_chan)
{
	int ret_value;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */
	
	
	ret_value = (xr16l788reg1->reg[num_chan].comp16550.msr & MSR_CTS) ?  CTS_ON : CTS_OFF;
	
	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	if(ret_value == CTS_ON)	printf("chan : %d Fio_GET_CTS_STATUS CTS_ON\n",num_chan);
	else					printf("chan : %d Fio_GET_CTS_STATUS CTS_OFF\n",num_chan);
#endif
	
	return(ret_value);
}

STATUS	Fio_GET_DSR_STATUS(int num_chan)
{
	int ret_value;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	ret_value = (xr16l788reg1->reg[num_chan].comp16550.msr & MSR_DSR) ? DSR_ON : DSR_OFF;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	if(ret_value == DSR_ON)	printf("chan : %d Fio_GET_DSR_STATUS DSR_ON\n",num_chan);
	else						printf("chan : %d Fio_GET_DSR_STATUS DSR_OFF\n",num_chan);
#endif

	return(ret_value);
}

STATUS	Fio_GET_CD_STATUS(int num_chan)
{
	int ret_value;
	int oldlevel = intLock(); /* LOCK INTERRUPTS   */

	ret_value = (xr16l788reg1->reg[num_chan].comp16550.msr & MSR_DCD) ? DCD_ON : DCD_OFF;

	
	intUnlock(oldlevel);  /* UNLOCK INTERRUPTS */

#if(DEBUG_FLAG == DEBUG_IOCTL)
	if(ret_value == DCD_ON)	printf("chan : %d Fio_GET_CD_STATUS DCD_ON\n",num_chan);
	else						printf("chan : %d Fio_GET_CD_STATUS DCD_OFF\n",num_chan);
#endif

	return(ret_value);
}





























void	XR16L788_BSP_Version()
{
	printf("===================================================\n");
	printf("             Etin system  Co,. LTD.\n");
	printf("             Board Development Team.\n");	
	printf("      BSP Name  : %s\n", BSP_NAME);
	printf("      Version       : %s\n", XR16L788_BSP_VERSION);
	printf("      Last UpDate : %s\n", XR16L788_LAST_UPDATE);
	printf("===================================================\n");
}


void 
RegShow(int num_chan)
{
	if(num_chan < 8){
		printf("===================================================\n");
		printf("	ST16554 Channel %d Register\n",num_chan);
		printf("===================================================\n");
		printf("	RBR_THR_DLL : 0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll,	(int)&xr16l788reg1->reg[num_chan].comp16550.rbr_thr_dll);
		printf("	DLM_IER :     0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.dlm_ier,		(int)&xr16l788reg1->reg[num_chan].comp16550.dlm_ier);
		printf("	FCR_IIR :     0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.fcr_iir,		(int)&xr16l788reg1->reg[num_chan].comp16550.fcr_iir);
		printf("	LCR :         0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.lcr,			(int)&xr16l788reg1->reg[num_chan].comp16550.lcr);
		printf("	MCR :         0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.mcr,			(int)&xr16l788reg1->reg[num_chan].comp16550.mcr);
		printf("	LSR :         0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.lsr,			(int)&xr16l788reg1->reg[num_chan].comp16550.lsr);
		printf("	MSR :         0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.msr,			(int)&xr16l788reg1->reg[num_chan].comp16550.msr);
		printf("	SCR :         0x%02x - Addr : 0x%08x\n",		xr16l788reg1->reg[num_chan].comp16550.scr,			(int)&xr16l788reg1->reg[num_chan].comp16550.scr);

		printf("===================================================\n");
	}
	
}
void
regShowAll()
{
	int i;
	for(i=0; i<4; i++){
		RegShow(i);
	}
	
}


void showChanStatusTM()
{
	unsigned int i;
	for(i=0; i<4; i++){
		
		printf("/========================================================\n");
		printf("     tm_channel : %2d\n", i);
		printf("/========================================================\n");
		
		printf("Baud Rate          : [%d]\n", XR16L788_DEV.channel[i].baudRate);
		switch(XR16L788_DEV.channel[i].dataBits){
			case 0 : printf("Data Bits          : [%d]\n", XR16L788_DEV.channel[i].dataBits+5); break;
			case 1 : printf("Data Bits          : [%d]\n", XR16L788_DEV.channel[i].dataBits+5); break;
			case 2 : printf("Data Bits          : [%d]\n", XR16L788_DEV.channel[i].dataBits+5); break;
			case 3 : printf("Data Bits          : [%d]\n", XR16L788_DEV.channel[i].dataBits+5); break;
			default : printf("Data Bits          : [%d]\n", XR16L788_DEV.channel[i].dataBits+5); break;
			 
		}
		
		switch(XR16L788_DEV.channel[i].stopBits){
			case 0 : printf("Stop Bits          : [%d]\n", XR16L788_DEV.channel[i].stopBits+1);break;
			case 4 : printf("Stop Bits          : [%d]\n", XR16L788_DEV.channel[i].stopBits-2);break;
			default: printf("Stop Bits          : [%d]\n", XR16L788_DEV.channel[i].stopBits+1);break;
	
		}
		switch(XR16L788_DEV.channel[i].parity){
			case 0 : printf("Parity Bits        : NONE\n");break;
			case 0x08 : printf("Parity Bits        : ODD\n");break;
			case 0x18 : printf("Parity Bits        : EVEN\n");break;
			default : printf("Parity Bits        : NONE\n");break;
		}
			
		
		
		printf("Tx Interrupt Count : [%d]\n", XR16L788_DEV.channel[i].txIntCnt);
		printf("Rx Interrupt Count : [%d]\n", XR16L788_DEV.channel[i].rxIntCnt);
		
	}
	
}
int __fd;
void
openport()
{
	
	__fd=open("/xr_scc/0",2,0);
	
}

void
tsdrv()
{
	write(__fd,"12345",5);
}

