/* tl16c554Sio.c - TI 16C554 UART tty driver */

/* Copyright 1984-1997 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/* includes */

#include "vxWorks.h"
#include "intLib.h"
#include "errnoLib.h"
#include "errno.h"
#include "sioLib.h"
#include "tl16c554Sio.h"
#include "tyLib.h"
#include "stdio.h"

#include "drv/intrCtl/ppc860Intr.h"
#include "drv/multi/ppc860Cpm.h"
#include "./ppc860Sio.h"
#include "./ppc860SccDrv.h"

/* local defines       */

#ifndef SIO_HUP
#   define SIO_OPEN	0x100A
#   define SIO_HUP	0x100B
#endif

#if 0
unsigned char tlbit_swap[256] = {
0x00,0x80,0x40,0xc0,0x20,0xa0,0x60,0xe0,
0x10,0x90,0x50,0xd0,0x30,0xb0,0x70,0xf0,
0x08,0x88,0x48,0xc8,0x28,0xa8,0x68,0xe8,
0x18,0x98,0x58,0xd8,0x38,0xb8,0x78,0xf8,
0x04,0x84,0x44,0xc4,0x24,0xa4,0x64,0xe4,
0x14,0x94,0x54,0xd4,0x34,0xb4,0x74,0xf4,
0x0c,0x8c,0x4c,0xcc,0x2c,0xac,0x6c,0xec,
0x1c,0x9c,0x5c,0xdc,0x3c,0xbc,0x7c,0xfc,
0x02,0x82,0x42,0xc2,0x22,0xa2,0x62,0xe2,
0x12,0x92,0x52,0xd2,0x32,0xb2,0x72,0xf2,
0x0a,0x8a,0x4a,0xca,0x2a,0xaa,0x6a,0xea,
0x1a,0x9a,0x5a,0xda,0x3a,0xba,0x7a,0xfa,
0x06,0x86,0x46,0xc6,0x26,0xa6,0x66,0xe6,
0x16,0x96,0x56,0xd6,0x36,0xb6,0x76,0xf6,
0x0e,0x8e,0x4e,0xce,0x2e,0xae,0x6e,0xee,
0x1e,0x9e,0x5e,0xde,0x3e,0xbe,0x7e,0xfe,
0x01,0x81,0x41,0xc1,0x21,0xa1,0x61,0xe1,
0x11,0x91,0x51,0xd1,0x31,0xb1,0x71,0xf1,
0x09,0x89,0x49,0xc9,0x29,0xa9,0x69,0xe9,
0x19,0x99,0x59,0xd9,0x39,0xb9,0x79,0xf9,
0x05,0x85,0x45,0xc5,0x25,0xa5,0x65,0xe5,
0x15,0x95,0x55,0xd5,0x35,0xb5,0x75,0xf5,
0x0d,0x8d,0x4d,0xcd,0x2d,0xad,0x6d,0xed,
0x1d,0x9d,0x5d,0xdd,0x3d,0xbd,0x7d,0xfd,
0x03,0x83,0x43,0xc3,0x23,0xa3,0x63,0xe3,
0x13,0x93,0x53,0xd3,0x33,0xb3,0x73,0xf3,
0x0b,0x8b,0x4b,0xcb,0x2b,0xab,0x6b,0xeb,
0x1b,0x9b,0x5b,0xdb,0x3b,0xbb,0x7b,0xfb,
0x07,0x87,0x47,0xc7,0x27,0xa7,0x67,0xe7,
0x17,0x97,0x57,0xd7,0x37,0xb7,0x77,0xf7,
0x0f,0x8f,0x4f,0xcf,0x2f,0xaf,0x6f,0xef,
0x1f,0x9f,0x5f,0xdf,0x3f,0xbf,0x7f,0xff};
#endif

extern unsigned char tlbit_swap[256];

/* min/max baud rate */
 
#define TL16C554_MIN_RATE 50
#define TL16C554_MAX_RATE 115200

#if 0
#define REG(reg, pchan) \
 (tlbit_swap[*(volatile UINT8 *)((UINT32)pchan->regs + (reg * pchan->regDelta))])
#define REGPTR(reg, pchan) \
 ((volatile UINT8 *)((UINT32)pchan->regs + (reg * pchan->regDelta)))
#define REG_WRITE(reg, pchan, value) \
 *(volatile UINT8 *)((UINT32)pchan->regs + (reg * pchan->regDelta)) = tlbit_swap[value]

#else

#define REG(reg, pchan) \
 (*(volatile UINT8 *)((UINT32)pchan->regs + (reg * pchan->regDelta)))
#define REGPTR(reg, pchan) \
 ((volatile UINT8 *)((UINT32)pchan->regs + (reg  * pchan->regDelta)))
#define REG_WRITE(reg, pchan, value) \
 *(volatile UINT8 *)((UINT32)pchan->regs + (reg  * pchan->regDelta)) = value
#endif

/* static forward declarations */

LOCAL 	int 	tl16c554CallbackInstall (SIO_CHAN *, int, STATUS (*)(), void *);
LOCAL 	STATUS 	tl16c554DummyCallback ();
LOCAL 	void 	tl16c554InitChannel (TL16C554_CHAN *);
LOCAL   STATUS  tl16c554BaudSet (TL16C554_CHAN *, UINT);
LOCAL 	STATUS  tl16c554ModeSet (TL16C554_CHAN *, UINT);
LOCAL 	STATUS 	tl16c554Ioctl (TL16C554_CHAN *, int, int);
void 	tl16c554TxStartup (TL16C554_CHAN *);
LOCAL 	int 	tl16c554PollOutput (TL16C554_CHAN *, char);
LOCAL 	int 	tl16c554PollInput (TL16C554_CHAN *, char *);
LOCAL 	STATUS 	tl16c554OptsSet (TL16C554_CHAN *, UINT);
LOCAL 	STATUS 	tl16c554Open (TL16C554_CHAN * pChan );
LOCAL 	STATUS 	tl16c554Hup (TL16C554_CHAN * pChan );

/* driver functions */

static SIO_DRV_FUNCS tl16c554SioDrvFuncs =
    {
    (int (*)())tl16c554Ioctl,
    (int (*)())tl16c554TxStartup,
    tl16c554CallbackInstall,
    (int (*)())tl16c554PollInput,
    (int (*)(SIO_CHAN *,char))tl16c554PollOutput
    };

/******************************************************************************
*
* tl16c554DummyCallback - dummy callback routine.
*/

LOCAL STATUS tl16c554DummyCallback (void)
{
    return (ERROR);
}

/******************************************************************************
*
* tl16c554DevInit - intialize an TL16C554 channel
*
* This routine initializes some SIO_CHAN function pointers and then resets
* the chip in a quiescent state.  Before this routine is called, the BSP
* must already have initialized all the device addresses, etc. in the
* TL16C554_CHAN structure.
*
* RETURNS: N/A
*/

void tl16c554DevInit(    TL16C554_CHAN * pChan	/* pointer to channel */    )
{
    int oldlevel = intLock ();

    /* initialize the driver function pointers in the SIO_CHAN's */

    pChan->pDrvFuncs    = &tl16c554SioDrvFuncs;

    /* set the non BSP-specific constants */

    pChan->getTxChar    = tl16c554DummyCallback;	taskDelay(5);
    pChan->putRcvChar   = tl16c554DummyCallback;	taskDelay(5);
    pChan->channelMode  = 0;    /* undefined */		taskDelay(5);
    pChan->options      = (CLOCAL | CREAD | CS8);	taskDelay(5);
    pChan->mcr			= MCR_OUT2;					taskDelay(5);
	pChan->openFlag 	= 0;						taskDelay(5);
    /* reset the chip */

    tl16c554InitChannel (pChan);

    intUnlock (oldlevel);
}

/*******************************************************************************
*
* tl16c554InitChannel - initialize UART
*
* Initialize the number of data bits, parity and set the selected
* baud rate.
* Set the modem control signals if the option is selected.
*
* RETURNS: N/A
*/

LOCAL void tl16c554InitChannel(    TL16C554_CHAN * pChan	/* pointer to channel */)
{

    /* set the requested baud rate */

    tl16c554BaudSet(pChan, pChan->baudRate);

    /* set the options */

    tl16c554OptsSet(pChan, pChan->options);
}

/*******************************************************************************
*
* tl16c554OptsSet - set the serial options
*
* Set the channel operating mode to that specified.  All sioLib options
* are supported: CLOCAL, HUPCL, CREAD, CSIZE, PARENB, and PARODD.
* When the HUPCL option is enabled, a connection is closed on the last
* close() call and opened on each open() call.
*
* Note, this routine disables the transmitter.  The calling routine
* may have to re-enable it.
*
* RETURNS:
* Returns OK to indicate success, otherwise ERROR is returned
*/

LOCAL STATUS tl16c554OptsSet
    (
    TL16C554_CHAN * pChan,	/* pointer to channel */
    UINT options		/* new hardware options */
    )
    {
    FAST int     oldlevel;		/* current interrupt level mask */

    pChan->lcr = 0; 
    pChan->mcr &= (~(MCR_RTS | MCR_DTR)); /* clear RTS and DTR bits */
    
    if (pChan == NULL || options & 0xffffff00)
	return ERROR;

    switch (options & CSIZE)
	{
	case CS5:
	    pChan->lcr = CHAR_LEN_5; break;
	case CS6:
	    pChan->lcr = CHAR_LEN_6; break;
	case CS7:
	    pChan->lcr = CHAR_LEN_7; break;
	default:
	case CS8:
	    pChan->lcr = CHAR_LEN_8; break;
	}

    if (options & STOPB)
	pChan->lcr |= LCR_STB;
    else
	pChan->lcr |= ONE_STOP;
    
    switch (options & (PARENB | PARODD))
	{
	case PARENB|PARODD:
	    pChan->lcr |= LCR_PEN; break;
	case PARENB:
	    pChan->lcr |= (LCR_PEN | LCR_EPS); break;
	default:
	case 0:
	    pChan->lcr |= PARITY_NONE; break;
	}

    REG_WRITE(IER, pChan, 0);

    if (!(options & CLOCAL))
	{
	/* !clocal enables hardware flow control(DTR/DSR) */

	pChan->mcr |= (MCR_DTR | MCR_RTS);
    	pChan->ier &= (~TxFIFO_BIT); 
	pChan->ier |= IER_EMSI;    /* enable modem status interrupt */
	}
    else
        pChan->ier &= ~IER_EMSI; /* disable modem status interrupt */ 

    oldlevel = intLock ();

    REG_WRITE(LCR, pChan, pChan->lcr);
    REG_WRITE(MCREG, pChan, pChan->mcr);

    /* now reset the channel mode registers */

    REG_WRITE(FCR, pChan, (RxCLEAR | TxCLEAR | FIFO_ENABLE));

    if (options & CREAD)  
	pChan->ier |= RxFIFO_BIT;

    if (pChan->channelMode == SIO_MODE_INT)
	{
        REG_WRITE(IER, pChan, pChan->ier);
        }

    intUnlock (oldlevel);

    pChan->options = options;
    
    return OK;
    }

/*******************************************************************************
*
* tl16c554Hup - hang up the modem control lines 
*
* Resets the RTS and DTR signals and clears both the receiver and
* transmitter sections.
*
* RETURNS: OK
*/

LOCAL STATUS tl16c554Hup
    (
    TL16C554_CHAN * pChan 	/* pointer to channel */
    )
    {
    FAST int     oldlevel;	/* current interrupt level mask */

    oldlevel = intLock ();

    pChan->mcr &= (~(MCR_RTS | MCR_DTR));
    REG_WRITE(MCREG, pChan, pChan->mcr);
    REG_WRITE(FCR, pChan,(RxCLEAR | TxCLEAR)); 

    intUnlock (oldlevel);

    return (OK);

    }    

/*******************************************************************************
*
* tl16c554Open - Set the modem control lines 
*
* Set the modem control lines(RTS, DTR) TRUE if not already set.  
* It also clears the receiver, transmitter and enables the fifo. 
*
* RETURNS: OK
*/

LOCAL STATUS tl16c554Open
    (
    TL16C554_CHAN * pChan 	/* pointer to channel */
    )
    {
    FAST int     oldlevel;	/* current interrupt level mask */
    char mask;

    mask = REG(MCREG, pChan) & (MCR_RTS | MCR_DTR);

    if (mask != (MCR_RTS | MCR_DTR)) 
    	{
    	/* RTS and DTR not set yet */

    	oldlevel = intLock ();

	/* set RTS and DTR TRUE */

    	pChan->mcr |= (MCR_DTR | MCR_RTS); 
    	REG_WRITE(MCREG, pChan, pChan->mcr); 

    	/* clear Tx and receive and enable FIFO */
		#if 0
        REG_WRITE(FCR, pChan, (RxCLEAR | TxCLEAR | FIFO_ENABLE));
		#else
		REG_WRITE(FCR, pChan, 0xc7);
		#endif
    	intUnlock (oldlevel);
	}

    return (OK);
    }

/******************************************************************************
*
* tl16c554BaudSet - change baud rate for channel
*
* This routine sets the baud rate for the UART. The interrupts are disabled
* during chip access.
*
* RETURNS: OK
*/

LOCAL STATUS  tl16c554BaudSet
    (
    TL16C554_CHAN * pChan,	/* pointer to channel */
    UINT	   baud		/* requested baud rate */
    )
    {
    int   oldlevel;

    /* disable interrupts during chip access */

    oldlevel = intLock ();

    /* Enable access to the divisor latches by setting DLAB in LCR. */

    REG_WRITE(LCR, pChan, LCR_DLAB | pChan->lcr);

    /* Set divisor latches. */

    REG_WRITE(DLL, pChan, pChan->xtal/(16 * baud));
    REG_WRITE(DLM, pChan, (pChan->xtal/(16 * baud)) >> 8);

    /* Restore line control register */

    REG_WRITE(LCR, pChan, pChan->lcr);

    pChan->baudRate = baud;
 
    intUnlock (oldlevel);

    return (OK);
    }

/*******************************************************************************
*
* tl16c554ModeSet - change channel mode setting
*
* This driver supports both polled and interrupt modes and is capable of
* switching between modes dynamically. 
*
* If interrupt mode is desired this routine enables the channels receiver and 
* transmitter interrupts. If the modem control option is TRUE, the Tx interrupt
* is disabled if the CTS signal is FALSE. It is enabled otherwise. 
*
* If polled mode is desired the device interrupts are disabled. 
*
* RETURNS:
* Returns a status of OK if the mode was set else ERROR.
*/

LOCAL STATUS tl16c554ModeSet
    (
    TL16C554_CHAN * pChan,	/* pointer to channel */
    UINT	newMode		/* mode requested */
    )
    {
    FAST int     oldlevel;	/* current interrupt level mask */
    char mask;
    
    if ((newMode != SIO_MODE_POLL) && (newMode != SIO_MODE_INT))
	return (ERROR);
           
    oldlevel = intLock ();

    if (newMode == SIO_MODE_INT)
	{
        /* Enable appropriate interrupts */
		
	if (pChan->options & CLOCAL) 
               	REG_WRITE(IER, pChan, pChan->ier | RxFIFO_BIT | TxFIFO_BIT);
	else  
		{
		mask = REG(MSR, pChan) & MSR_CTS;

   		/* if the CTS is asserted enable Tx interrupt */

   		if (mask & MSR_CTS)
			pChan->ier |= TxFIFO_BIT;    /* enable Tx interrupt */
		else
           		pChan->ier &= (~TxFIFO_BIT); /* disable Tx interrupt */

		REG_WRITE(IER, pChan, pChan->ier); 
		}	
	}
    else
        {
        /* disable all tl16c554 interrupts */ 

        REG_WRITE(IER, pChan, 0);   
	}

    pChan->channelMode = newMode;

    intUnlock (oldlevel);
    

    return (OK);
   }

/*******************************************************************************
*
* tl16c554Ioctl - special device control
*
* Includes commands to get/set baud rate, mode(INT,POLL), hardware options(
* parity, number of data bits), and modem control(RTS/CTS and DTR/DSR).
* The ioctl command SIO_HUP is sent by ttyDrv when the last close() function 
* call is made. Likewise SIO_OPEN is sent when the first open() function call
* is made.
*
* RETURNS: OK on success, EIO on device error, ENOSYS on unsupported
*          request.
*/

LOCAL STATUS tl16c554Ioctl
    (
    TL16C554_CHAN * 	pChan,		/* pointer to channel */
    int			request,	/* request code */
    int        		arg		/* some argument */
    )
    {
    FAST STATUS  status;

    status = OK;

    switch (request)
	{
	case SIO_BAUD_SET:
	    if (arg < TL16C554_MIN_RATE || arg > TL16C554_MAX_RATE)
		status = EIO;		/* baud rate out of range */
	    else
	        status = tl16c554BaudSet (pChan, arg);
	    break;

        case SIO_BAUD_GET:
            *(int *)arg = pChan->baudRate;
            break; 

        case SIO_MODE_SET:
	    status = (tl16c554ModeSet (pChan, arg) == OK) ? OK : EIO;
            break;          

        case SIO_MODE_GET:
            *(int *)arg = pChan->channelMode;
            break;

        case SIO_AVAIL_MODES_GET:
            *(int *)arg = SIO_MODE_INT | SIO_MODE_POLL;
            break;

        case SIO_HW_OPTS_SET:
    	    status = (tl16c554OptsSet (pChan, arg) == OK) ? OK : EIO;
    	    break;

        case SIO_HW_OPTS_GET:
            *(int *)arg = pChan->options;
            break;

        case SIO_HUP:
            /* check if hupcl option is enabled */

    	    if (pChan->options & HUPCL) 
	    	status = tl16c554Hup (pChan);
            break;
	
	case SIO_OPEN:
            /* check if hupcl option is enabled */

    	    if (pChan->options & HUPCL) 
	    	status = tl16c554Open (pChan);
	    	pChan->openFlag=1;
			printf("pChan->openFlag = %d\n",pChan->openFlag);
	    break;

        default:
            status = ENOSYS;
	}
    return (status);
    }

/*******************************************************************************
*
* tl16c554IntWr - handle a transmitter interrupt 
*
* This routine handles write interrupts from the UART. It reads a character
* and puts it in the transmit holding register of the device for transfer.
*
* If there are no more characters to transmit, transmission is disabled by 
* clearing the transmit interrupt enable bit in the IER(int enable register).
*
* RETURNS: N/A
*
*/

void tl16c554IntWr 
    (
    TL16C554_CHAN * pChan		/* pointer to channel */	
    )
    {
    char           outChar;

    if ((*pChan->getTxChar) (pChan->getTxArg, &outChar) != ERROR)
        REG_WRITE(THR, pChan, (UINT16)outChar);	/* write char to Transmit Holding Reg */
    else
        {
        pChan->ier &= (~TxFIFO_BIT);	/* indicates to disable Tx Int */
        REG_WRITE(IER, pChan, pChan->ier);
        }
    }

/*******************************************************************************
*
* tl16c554IntRd - handle a receiver interrupt 
*
* This routine handles read interrupts from the UART.
*
* RETURNS: N/A
*
*/

void tl16c554IntRd 
    (
    TL16C554_CHAN * pChan	/* pointer to channel */
    )
    {
    char   inchar;

    /* read character from Receive Holding Reg. */
	
    inchar = REG(RBR, pChan);
    if(pChan->openFlag == 1)
    	(*pChan->putRcvChar) (pChan->putRcvArg, inchar);

    }

/*******************************************************************************
*
* tl16c554IntEx - miscellaneous interrupt processing
*
* This routine handles miscellaneous interrupts on the UART.
* Not implemented yet.
*
* RETURNS: N/A
*
*/

void tl16c554IntEx 
    (
    TL16C554_CHAN *pChan		/* pointer to channel */
    )
    {

    /* Nothing for now... */
    }

/********************************************************************************
*
* tl16c554Int - interrupt level processing
*
* This routine handles four sources of interrupts from the UART. They are
* prioritized in the following order by the Interrupt Identification Register:
* Receiver Line Status, Received Data Ready, Transmit Holding Register Empty
* and Modem Status.
*
* When a modem status interrupt occurs, the transmit interrupt is enabled if
* the CTS signal is TRUE.
*
* RETURNS: N/A
*
*/
#if 1
void tl16c554Int 
    (
    TL16C554_CHAN * pChan	/* pointer to channel */
    )
    {
    FAST volatile char        intStatus;
    register int	oldlevel;	

	FAST volatile int fcrStatus;	

    /* read the Interrrupt Status Register (Int. Ident.) */
	
	oldlevel = intLock();
		
    intStatus = (REG(IIR, pChan)) & 0x0f;
	
    /*
     * This UART chip always produces level active interrupts, and the IIR 
     * only indicates the highest priority interrupt.  
     * In the case that receive and transmit interrupts happened at
     * the same time, we must clear both interrupt pending to prevent
     * edge-triggered interrupt(output from interrupt controller) from locking
     * up. One way doing it is to disable all the interrupts at the beginning
     * of the ISR and enable at the end.
     */
	
		
        
    
    REG_WRITE(IER,pChan, 0);    /* disable interrupt */
	
    switch (intStatus)
	{
		
	case IIR_RLS:
            /* overrun,parity error and break interrupt */

            intStatus = REG(LSR, pChan); /* read LSR to reset interrupt */
	    break;

        case IIR_RDA:     		/* received data available */
	case IIR_TIMEOUT: 
	   /*
	    * receiver FIFO interrupt. In some case, IIR_RDA will
            * not be indicated in IIR register when there is more
	    * than one character in FIFO.
	    */
	    
		
			tl16c554IntRd (pChan);  	/* RxChar Avail */		
           
            

            break;

       	case IIR_THRE:  /* transmitter holding register ready */
	    {
            char outChar;
			
            if ((*pChan->getTxChar) (pChan->getTxArg, &outChar) != ERROR){
                REG_WRITE(THR, pChan, (UINT16)outChar);   /* char to Transmit Holding Reg */
                fcrStatus = REG(LSR, pChan)&0x80;
				if (fcrStatus==0x80) {
					REG_WRITE(LSR,pChan, 0x00);
                	REG_WRITE(FCR, pChan, (TxCLEAR | FIFO_ENABLE));
                }
            }else{
                pChan->ier &= (~TxFIFO_BIT); /* indicates to disable Tx Int */
				
        	}
        }
        
        	
            break;

	case IIR_MSTAT: /* modem status register */
	   {
	   char	msr;

	   msr = REG(MSR, pChan);

	   /* if CTS is asserted by modem, enable tx interrupt */

	   if ((msr & MSR_CTS) && (msr & MSR_DCTS)) 
	   	pChan->ier |= TxFIFO_BIT;
           else
           	pChan->ier &= (~TxFIFO_BIT); 
	   }
	   break;

        default:
	    break;
        }
		
    	REG_WRITE(IER, pChan, pChan->ier); /* enable interrupts accordingly */
    	#if 0		
		fcrStatus = REG(LSR, pChan)&0x80;
		if (fcrStatus==0x80) {
			REG_WRITE(LSR,pChan, 0x00);
	    	REG_WRITE(FCR,pChan, 0x00);
	    	REG_WRITE(FCR,pChan, 0x07);
		}
		#endif
			 intUnlock(oldlevel);
    }
#else
void tl16c554Int(TL16C554_CHAN * pChan)	/* pointer to channel */
{
	FAST volatile char        intStatus;

	char ch;
	

	/* read the Interrrupt Status Register (Int. Ident.) */
	
	
		
	intStatus = (REG(IIR, pChan)) & 0x0f;

	/*
	 * This UART chip always produces level active interrupts, and the IIR 
	 * only indicates the highest priority interrupt.  
		 * In the case that receive and transmit interrupts happened at
	 * the same time, we must clear both interrupt pending to prevent
	 * edge-triggered interrupt(output from interrupt controller) from locking
	 * up. One way doing it is to disable all the interrupts at the beginning
	 * of the ISR and enable at the end.
	 */
	
		
	REG_WRITE(IER,pChan, 0);    /* disable interrupt */
	
	if (intStatus == IIR_RDA || intStatus == IIR_TIMEOUT) {
		/*
		 * receiver FIFO interrupt. In some case, IIR_RDA will
		 * not be indicated in IIR register when there is more
		 * than one character in FIFO.
		 */
#if 0
		ch = REG(RBR, pChan);
    /* if(pChan->openFlag == 1) (*pChan->putRcvChar) (pChan->putRcvArg, ch); */
#else
		ch = REG(RBR, pChan);
    if(pChan->openFlag == 1) (*pChan->putRcvChar) (pChan->putRcvArg, ch);
#endif
	} else if (intStatus == IIR_THRE) {
#if 1
		FAST TY_DEV_ID pTyDev;
		FAST RING_ID	ringId;
    FAST int		nn;
    pTyDev = (TY_DEV_ID) pChan->getTxArg;
    ringId = pTyDev->wrtBuf;
    if (RNG_ELEM_GET (ringId, &ch, nn) == 1) {
    	pTyDev->wrtState.busy = TRUE;
    	REG_WRITE(THR, pChan, (UINT16)ch);   /* char to Transmit Holding Reg */
    } else {
    	pTyDev->wrtState.busy = FALSE;
			pChan->ier &= (~TxFIFO_BIT); /* indicates to disable Tx Int */
			taskDelay(5);
		}
#else
		if ((*pChan->getTxChar) (pChan->getTxArg, &ch) != ERROR)
			REG_WRITE(THR, pChan, (UINT16)ch);   /* char to Transmit Holding Reg */
		else
			pChan->ier &= (~TxFIFO_BIT); /* indicates to disable Tx Int */
#endif
	} else if (intStatus == IIR_RLS) {
		intStatus = REG(LSR, pChan); /* read LSR to reset interrupt */
	} else if (intStatus == IIR_MSTAT) {
		char	msr;
		msr = REG(MSR, pChan);

		/* if CTS is asserted by modem, enable tx interrupt */

		if ((msr & MSR_CTS) && (msr & MSR_DCTS)) 
			pChan->ier |= TxFIFO_BIT;
		else
			pChan->ier &= (~TxFIFO_BIT); 
	}	
  REG_WRITE(IER, pChan, pChan->ier); /* enable interrupts accordingly */  
  	
	/* intUnlock(oldlevel); */
}
#endif
/*******************************************************************************
*
* tl16c554TxStartup - transmitter startup routine
*
* Call interrupt level character output routine and enable interrupt if it is
* in interrupt mode with no hardware flow control.
* If the option for hardware flow control is enabled and CTS is set TRUE,
* then the Tx interrupt is enabled.
* 
* RETURNS: N/A
*/

	void tl16c554TxStartup
    (
    TL16C554_CHAN * pChan 	/* pointer to channel */
    )
    {
    char mask;
	
    if (pChan->channelMode == SIO_MODE_INT)
	{
	if (pChan->options & CLOCAL)
		{
		/* No modem control */

		pChan->ier |= TxFIFO_BIT;
 		REG_WRITE(IER,pChan, pChan->ier); 
		}
	else
		{
		mask = REG(MSR, pChan) & MSR_CTS;

   		/* if the CTS is asserted enable Tx interrupt */

   		if (mask & MSR_CTS)
			pChan->ier |= TxFIFO_BIT;    /* enable Tx interrupt */
		else
           		pChan->ier &= (~TxFIFO_BIT); /* disable Tx interrupt */

		REG_WRITE(IER, pChan, pChan->ier); 
		}
	}
    }

/******************************************************************************
*
* tl16c554PollOutput - output a character in polled mode.
*
* RETURNS: OK if a character arrived, EIO on device error, EAGAIN
* if the output buffer if full.
*/

LOCAL int tl16c554PollOutput
    (
    TL16C554_CHAN *  pChan,	/* pointer to channel */
    char            outChar	/* char to send */
    )
    {
    char pollStatus = REG(LSR, pChan);
    char msr = REG(MSR, pChan);

    /* is the transmitter ready to accept a character? */

    if ((pollStatus & LSR_THRE) == 0x00)
        return (EAGAIN);

    if (!(pChan->options & CLOCAL))	 /* modem flow control ? */
    	{
    	if (msr & MSR_CTS)
    		REG_WRITE(THR, pChan, (UINT16)outChar);
	else
		return (EAGAIN);
	}
    else
    	REG_WRITE(THR, pChan, (UINT16)outChar);       /* transmit character */

    return (OK);
    }
/******************************************************************************
*
* tl16c554PollInput - poll the device for input.
*
* RETURNS: OK if a character arrived, EIO on device error, EAGAIN
* if the input buffer if empty.
*/

LOCAL int tl16c554PollInput
    (
    TL16C554_CHAN *  pChan,	/* pointer to channel */
    char *          pChar 	/* pointer to char */
    )
    {

    char pollStatus = REG(LSR, pChan);

    if ((pollStatus & LSR_DR) == 0x00)
        return (EAGAIN);

    /* got a character */

    *pChar = REG(RBR, pChan);

    return (OK);
    }

/******************************************************************************
*
* tl16c554CallbackInstall - install ISR callbacks to get/put chars.
*
* This routine installs the callback functions for the driver
*
* RETURNS: OK on success or ENOSYS on unsupported callback type.
*/

LOCAL int tl16c554CallbackInstall
    (
    SIO_CHAN *  pSioChan,	/* pointer to device to control */
    int         callbackType,	/* callback type(tx or receive) */
    STATUS      (*callback)(),	/* pointer to callback function */
    void *      callbackArg	/* callback function argument */
    )
    {
    TL16C554_CHAN * pChan = (TL16C554_CHAN *)pSioChan;

    switch (callbackType)
        {
        case SIO_CALLBACK_GET_TX_CHAR:
            pChan->getTxChar    = callback;
            pChan->getTxArg     = callbackArg;
            return (OK);
        case SIO_CALLBACK_PUT_RCV_CHAR:
            pChan->putRcvChar   = callback;
            pChan->putRcvArg    = callbackArg;
            return (OK);
        default:
            return (ENOSYS);
        }

    }


