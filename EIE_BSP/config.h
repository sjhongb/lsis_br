/* htcs/config.h - LK11 Kvme402 board configuration header */

/* Copyright 1984-1998 Wind River Systems, Inc. */
/* Copyright 1998,1999 Etin Systems, Inc., All Rights Reserved */

/*
modification history
--------------------
01a,28jun05,erc	changed BSP_VERSION to "1.2" and BSP_REV to "/0"
				added BSP_VER_1_2 macro (Tornado 2.0 and 2.2 release).
01a,01jun05,erc	added support for HDLC Ports (SCC3, SCC4).
01a,26jun97,srr	created from htcs.
*/ 

/*    
This file contains the configuration parameters for the 
LK11 LKV402A board.
*/

#ifndef	INCconfigh
#define	INCconfigh 
 
/* 
 * BSP version/revision identification, should be placed
 * before #include "configAll.h"
 */
 
/*
 LSIS 철도시스템 기술팀 BSP Version

 LSIS 김범렬 SysRtc->UpdateClk()추가 070911   : Version No. 1.0
 LSIS 김범렬 ppcread변경,Intterupt변경 070916 : Version No. 1.1
 LK11 최기철 LKV-402A <-> LKV314 system 적용을 위해 수정 110727 : Version No. 1.2.3
*/

#define BSP_VER_1_1     1
#define BSP_VER_1_2     1
#define BSP_VERSION     "1.2"
#define BSP_REV         "/3"		/* 0 for the first bsp revision */


#include "configAll.h"

#define DEFAULT_BOOT_LINE \
"cpm(0,0)host:/vxWorks h=192.168.1.2 e=192.168.1.1 u=itsme"

#define IP_MAX_UNITS	2

/* Created by gpande (2011/7/27 - 16:33:26) (Start) */
/* LS BSP type macro가 설정되어 있는 경우 LS산전 관련 내용이 주석처리됨 */
#if 1
#define LK11_BSP_TYPE
#endif
/* Created by gpande (2011/7/27 - 16:33:28) (End) */

#if 0
#undef	FLASH_BOOT_MODE	
#else
#define	FLASH_BOOT_MODE
#endif

/* Cache and MMU supported */

#undef USER_I_MMU_ENABLE
#undef USER_D_MMU_ENABLE
#undef INCLUDE_MMU_BASIC
#undef INCLUDE_MMU_FULL

#undef  INCLUDE_CACHE_SUPPORT
#undef  USER_I_CACHE_ENABLE
#undef USER_D_CACHE_ENABLE
#undef USER_I_CACHE_MODE
#define USER_I_CACHE_MODE   CACHE_DISABLED
#undef USER_D_CACHE_MODE
#define USER_D_CACHE_MODE   CACHE_DISABLED
#undef USER_B_CACHE_ENABLE

#define	INCLUDE_SIGNALS
#define	INCLUDE_EXC_HANDLING

#define	INCLUDE_CPM			/* include the CPM ethernet drvier */
#define	INCLUDE_END			/* Enhanced Network Driver (see configNet.h) */

#define	INCLUDE_FLOATING_POINT
#define	INCLUDE_SW_FP		/* undef eric : 050822 */



#undef NUM_TTY
#define NUM_TTY		SMC_N_SIO_CHANNELS		


#undef CONSOLE_TTY
#define CONSOLE_TTY 1


/* additional utility */
#define INCLUDE_DOSFS

#define INCLUDE_NET_SHOW
#define INCLUDE_PING
#define	INCLUDE_TELNET

#undef	INCLUDE_WDB
#define INCLUDE_WDB 
#define INCLUDE_DEBUG
#define INCLUDE_LOADER
#define INCLUDE_NET_SYM_TBL

#define INCLUDE_SHELL

#define INCLUDE_SHOW_ROUTINES
#define INCLUDE_STARTUP_SCRIPT
#define INCLUDE_STAT_SYM_TBL
#define INCLUDE_SYM_TBL
#define INCLUDE_UNLOADER

#ifdef FLASH_BOOT_MODE
#define STANDALONE_NET
#endif


/*
#define INCLUDE_NFS
#define INCLUDE_NFS_MOUNT_ALL
#define INCLUDE_RPC
*/


#define INCLUDE_POSIX_TIMERS

#define	NFS_GROUP_ID	100
#define NFS_USER_ID	100

#undef INCLUDE_VME

/* optional timestamp support */
#define INCLUDE_TIMESTAMP

/* clock rates */

#define SYS_CLK_RATE_MIN	1		/* minumum system clock rate */
#define SYS_CLK_RATE_MAX	8000	/* maximum system clock rate */
#define AUX_CLK_RATE_MIN	1		/* minumum auxiliary clock rate */
#define AUX_CLK_RATE_MAX	8000	/* maximum auxiliary clock rate */

/* 
 * SPLL_FREQ_REQUESTED - This constant defines the expected system PLL
 * frequency divided by 2. The supported frequecies are 25, 40 and 50 MHz.
 * We are defaulting the clock reate to 25Mhz.
 */

#define SPLL_FREQ_REQUESTED	FREQ_50_MHZ

/* DRAM refresh frequency - This macro defines the DRAM refresh frequency.
 * For a DRAM with 1024 rows to refresh in 16ms,
 *			DRAM_REFRESH_FREQ = 1024/ 16E-3 = 64E3 hz
 */

#define	DRAM_REFRESH_FREQ	64000		/* 64 kHz : 15.6us*/

/*
 * Baud Rate Generator division factors --
 *	BRG_DIV_BY_1,  divides by  1
 *	BRG_DIV_BY_4,  divides by  4
 * 	BRG_DIV_BY_16, divides by 16
 * 	BRG_DIV_BY_64, divides by 64
 */

#define	BRGCLK_DIV_FACTOR	BRG_DIV_BY_1

/* add necessary drivers */
#define INCLUDE_STARTUP_SCRIPT

/* remove unnecessary drivers */

#undef INCLUDE_BP
#undef INCLUDE_EX
#undef INCLUDE_ENP
#undef INCLUDE_SM_NET
#undef INCLUDE_SM_SEQ_ADD

/****************************************************************************/
/*                  Shared Memory Networks                                  */
/****************************************************************************/
#if 0
#define INCLUDE_SM_NET


#undef  INCLUDE_SM_SEQ_ADD

#ifdef INCLUDE_SM_NET
#define INCLUDE_BSD /* netif driver support */
#undef  SM_TAS_TYPE
#define SM_TAS_TYPE     SM_TAS_SOFT

#define SM_INT_TYPE     SM_INT_NONE 	/* SM_INT_MAILBOX_1, SM_INT_NONE    */
#define SM_INT_ARG1     VME_AM_EXT_SUP_DATA /* VME_AM_EXT_SUP_DATA, VME_AM_STD_SUP_DATA */
#define SM_INT_ARG2     (char *)(0x10400008 + (sysProcNumGet() * 0x08000000))
#define SM_INT_ARG3     0xFF

#undef  SM_PKTS_SIZE
#define SM_PKTS_SIZE    DEFAULT_PKT_SIZE
#undef  SM_CPUS_MAX
#define SM_CPUS_MAX     DEFAULT_CPUS_MAX


#define	SM_OFF_BOARD	FALSE


#undef  SM_ANCHOR_ADRS
#define SM_ANCHOR_ADRS ((char *)0xF1000000)
#define SM_MEM_ADRS	((char *)SM_ANCHOR_ADRS)
#define SM_OBJ_MEM_ADRS ((char *)(SM_MEM_ADRS+SM_MEM_SIZE))

#define SM_MEM_SIZE	0x000e000	/* 64K - 8k */
#define SM_OBJ_MEM_SIZE 0x10000         /* sh. mem Objects pool size 64K */

#endif
#endif

/* Memory addresses */

#define LOCAL_MEM_LOCAL_ADRS	0x00000000	/* Base of RAM */
#define LOCAL_MEM_SIZE			0x01800000	/* 0x00400000 - 24 Mbyte 	*/
#define	LOCAL_DISP_MEM_SIZE		0x02000000	/* 32 Mbyte is the default */ 


/*
 * The constants ROM_TEXT_ADRS, ROM_SIZE, and RAM_HIGH_ADRS are defined
 * in config.h, and Makefile.
 * All definitions for these constants must be identical.
 */

#define ROM_BASE_ADRS		0xFFF00000	/* base address of ROM */
#define ROM_TEXT_ADRS		(ROM_BASE_ADRS + 0x100)

#define ROM_SIZE			0x00100000 	/* 1MB ROM space */

/* RAM address for ROM boot */
#define RAM_HIGH_ADRS		(LOCAL_MEM_LOCAL_ADRS + LOCAL_MEM_SIZE)
/* RAM address for system image */
#define RAM_LOW_ADRS		(LOCAL_MEM_LOCAL_ADRS + 0x00200000)

#define USER_RESERVED_MEM	0x00000000	/* user reserved memory size */

#define DEFAULT_POWER_MGT_MODE	VX_POWER_MODE_DISABLE




#include "kvme402a.h"				/* include htcs860 params */

#endif	/* INCconfigh */


#if defined(PRJ_BUILD)
#include "prjParams.h"
#endif
