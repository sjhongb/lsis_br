/* configNet.h - network configuration header */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,26jan99,dat changed driver name from motCmpEnd to motCpmEnd
01a,22jun98,cn  written.
*/
 
#ifndef INCconfigNeth
#define INCconfigNeth

#include "vxWorks.h"
#include "end.h"

#define MOT_LOAD_FUNC	motCpmEndLoad

/* motCpmAddr:ivec:sccNum:txBdNum:rxBdNum:txBdBase:rxBdBase:bufBase */
#define MOT_LOAD_STRING		"0xFF000000:0x3e:1:0x20:0x20:0x2600:0x2700:-1"
#define MOT_LOAD_STRING_1	"0xFF000000:0x3d:2:0x20:0x20:0x2800:0x2900:-1"
/*	0x3e 0x3d	Origine	*/


IMPORT END_OBJ* MOT_LOAD_FUNC (char *, void*);

END_TBL_ENTRY endDevTbl [] =
{
    { 0, MOT_LOAD_FUNC, MOT_LOAD_STRING, 1, NULL, FALSE},
    { 1, MOT_LOAD_FUNC, MOT_LOAD_STRING_1, 1, NULL, FALSE},
    { 0, END_TBL_END, NULL, 0, NULL, FALSE},
};

#endif /* INCconfigNeth */
