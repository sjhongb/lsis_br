
#define	FLASH_BASE	0xfa000000
#define FLASH_SIZE	0x400000
#define	FLASH_0		0
#define	FLASH_1		1
#define	FLASH_DELAY_TIME	1000
#define	MAX_BLOCK_NUM	32

#define FLASH_ADDR 			0xfa000000
#define	FLASH_BOOT_BASE		0xFA300000
#define	MAX_FILE_SIZE		0x000FFF00
#define	FLASH_ROM_OFFSET	0x300000
#define	ROM_OFFSET			0x100
#define	TEST_DATA_NUM		10000
