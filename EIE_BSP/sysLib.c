/* sysLib.c - Etin System kvme402a board system-dependent library */

/*
Modification history
--------------------
2006.03.16	hjh		digital out 4ch reverse
*/


#include "copyright_wrs.h"

/* includes */

#include "vxWorks.h"
#include "vme.h"
#include "memLib.h"
#include "cacheLib.h"
#include "inetLib.h"
#include "sysLib.h"
#include "config.h"
#include "string.h"
#include "intLib.h"
#include "esf.h"
#include "excLib.h"
#include "logLib.h"
#include "taskLib.h"
#include "vxLib.h"
#include "tyLib.h"
#include "arch/ppc/vxPpcLib.h"
#include "private/vmLibP.h"
#include "stdio.h"
#include "ctype.h"
#include "fioLib.h"
#include "wdLib.h"
#include "time.h"

#include "drv/multi/ppc860Siu.h"
/*#include "drv/sio/ppc860Sio.h"*/
#include "./ppc860Sio.h"
#include "kvme402a.h"


#ifdef INCLUDE_CPM
#include "drv/netif/if_cpm.h"
#include "ifLib.h"
#include "net/if.h"
#include "hostLib.h"
#include "arch/ppc/ivPpc.h"
#include "routeLib.h"
#endif /* INCLUDE_CPM */

/* externals */
IMPORT int	sysDecGet();
IMPORT int	sysStartType;
IMPORT void sysSccSerialHwInit (int i);

IMPORT STATUS sysRtcGet (struct tm *tp);
IMPORT void memWrite(unsigned char *memAdrs, unsigned char data);

/* Local */
STATUS 	EnetAttach (int, char *, int);
void	sysPhyMemClr();
STATUS	sysAMCodeSet(int am_cmd);
STATUS ledAllOff();

/* Created by gpande (2011/7/12 - 18:13:28) */
/*
LS산전 철도 application에서 사용하는 LKV402 BSP의 함수가
LKV402A BSP와 맞지 않아 정상동작하지 않으므로 임시로 dummy function을 선언함.
*/
void watchDogTimer_Enable()
{
	return;
}
STATUS watchDogTimer_Clear_Set()
{
	return OK;
}
STATUS watchDogTimer_Clear_Free()
{
	return OK;
}
STATUS sysIntEnable(int intNum)
{
	return OK;
}

STATUS sysIntDisable(int intNum)
{
	return OK;
}
/* Created by gpande (2011/7/12 - 18:13:30) */

/* Macro for all i/o operations to use */
#define SYNC  __asm__("	sync")

PHYS_MEM_DESC sysPhysMemDesc [] =
    {
    {
    (void *) LOCAL_MEM_LOCAL_ADRS,
    (void *) LOCAL_MEM_LOCAL_ADRS,
    RAM_LOW_ADRS ,
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE
    },

    {
    (void *) RAM_LOW_ADRS,
    (void *) RAM_LOW_ADRS,
    LOCAL_MEM_SIZE - RAM_LOW_ADRS,
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE
    },

    {
    (void *) SRAM_BA,			/* 0xF1000000 */
    (void *) SRAM_BA,			/* 0xF1000000 */
    SRAM_SIZE,				/* 2M Bytes (0x200000) */
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

    {
    (void *) NV_RAM_ADRS,			/* 0xF2000000 */
    (void *) NV_RAM_ADRS,			/* 0xF2000000 */
    NV_RAM_SIZE,			
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },


	/********************************************************************/

	{
    (void *) SERIAL_BA,			/* 0xF3000000 */
    (void *) SERIAL_BA,			/* 0xF3000000 */
    SERIAL_MEM_SIZE,			/* 4K Bytes (0x1000) */    
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

	/********************************************************************/

    {
    (void *) VME_SHORT_ADDR,		/* 0xF4FF0000 */
    (void *) VME_SHORT_ADDR,		/* 0xF4FF0000 */
    VME_SHORT_ADDR_SIZE,		/* 64K Bytes (0x10000) */	
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

    {
    (void *) VME_STAND_ADDR,		/* 0xF0000000 */
    (void *) VME_STAND_ADDR,		/* 0xF0000000 */
    VME_STAND_ADDR_SIZE,		/* 16M Bytes (0x1000000) */	
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

    /* a32 vme */
    {
    (void *) 0x90000000,
    (void *) 0x90000000,
    0x1000000,                          /* 16 Mbytes */
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },
	
	/* WahchDog Timer Reset Address : 0xF6000000 */
    {
    (void *) WATCH_DOG_TIMER_ADR,
    (void *) WATCH_DOG_TIMER_ADR,
    0x100,                          /* 100 bytes */
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

	/* ENCODER Switch Address : 0xF7000000 */
    {
    (void *) ENC_SW_BA,
    (void *) ENC_SW_BA,
    0x8, 
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
	},
#if 0	
	/* TL16C554 Reset Address : 0xFD000000 */
    {
    (void *) (0xFD000000),
    (void *) (0xFD000000),
    0x100, 
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },
#endif
    {
    (void *) INTERNAL_MEM_MAP_ADDR,
    (void *) INTERNAL_MEM_MAP_ADDR,
    INTERNAL_MEM_MAP_SIZE,		/* 64 k - Internal Memory Map */	
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

    {
    (void *) FLASH_BA,
    (void *) FLASH_BA,
    FLASH_SIZE,		/* 64 k - Internal Memory Map */	
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    },

    {
    (void *) ROM_BASE_ADRS,		/* 0xFFF00000 */
    (void *) ROM_BASE_ADRS,		/* 0xFFF00000 */
    ROM_SIZE,				/* EPROM memory */	
    VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE | VM_STATE_MASK_CACHEABLE,
    VM_STATE_VALID      | VM_STATE_WRITABLE      | VM_STATE_CACHEABLE_NOT
    }

    };

int sysPhysMemDescNumEnt = NELEMENTS (sysPhysMemDesc);

int   sysBus      = BUS;                /* system bus type (VME_BUS, etc) */
int   sysCpu      = CPU;                /* system CPU type (PPC860) */
char *sysBootLine = BOOT_LINE_ADRS;	/* address of boot line */
char *sysExcMsg   = EXC_MSG_ADRS;	/* catastrophic message area */
int   sysProcNum;			/* processor number of this CPU */
int   sysFlags;				/* boot flags */
char  sysBootHost [BOOT_FIELD_LEN];	/* name of host from which we booted */
char  sysBootFile [BOOT_FIELD_LEN];	/* name of file from which we booted */
BOOL  sysVmeEnable = FALSE;		/* by default no VME */
UINT  sysRamSize = DEFAULT_DRAM_SIZE;   /* total RAM size: onboard DRAM+DIMM */


WDOG_ID	_sw_wid;
int	_sw_watchdog_flag;

#ifdef	INCLUDE_CPM

unsigned char sysCpmEnetAddr [ENET_NUM][6] = 
					{
						{0x00, 0x20, 0x74, 0x11, 0x11, 0x15},
						{0x00, 0x20, 0x74, 0x11, 0x11, 0x16},
					};

/* locals */
extern STATUS cpmattach();
void sysCpmEnetDisable (int unit);
void sysCpmEnetIntDisable (int unit);

#endif	/* INCLUDE_CPM */

/* forward declarations */

void	sysDelay( void );
void	sysMsDelay ( UINT );
void	sysUsDelay ( UINT );

STATUS	ledCon(char led);
UCHAR	encRead (int);
UCHAR 	DipSwRead (int);
UCHAR	digInChan (int);
UCHAR	digIn (void);
STATUS	digOutChan (int, char);
STATUS	digOut (char);

void	FailLedOff (void);

/* BSP Drivers */

#include "sysSerial.c"

#include "intrCtl/ppc860Intr.c"
#include "timer/ppc860Timer.c"		/* PPC860 & 821 have on chip timers */
#include "mem/byteNvRam.c"
#include "93c46.c"

/******************************************************************************
*
* sysModel - return the model name of the CPU board
*
* This routine returns the model name of the CPU board.
*
* RETURNS: A pointer to the string.
*/

char * sysModel (void)
    {
    return ("Etin System KVME402a");
    }

/******************************************************************************
*
* sysBspRev - return the bsp version with the revision eg 1.0/<x>
*
* This function returns a pointer to a bsp version with the revision.
* for eg. 1.0/<x>. BSP_REV defined in config.h is concatanated to
* BSP_VERSION and returned.
*
* RETURNS: A pointer to the BSP version/revision string.
*/

char * sysBspRev (void)
    {
    return (BSP_VERSION BSP_REV);
    }


/******************************************************************************
*
* sysHwInit - initialize the system hardware
*
* This routine initializes various feature of the MPC860ADS boards. It sets up
* the control registers, initializes various devices if they are present.
*
* NOTE: This routine should not be called directly by the user.
*
* RETURNS: N/A
*/
void VMESlaveErrReset(void)
{
	/*wdtEnable();*/
	*(char *)WATCH_DOG_TIMER_ADR = 0xff;
	wdtReset();
	return;
}

void sysHwInit (void)
    {
    int	immrVal = vxImmrGet();
	int	cnt;
	
	/* set the SPLL to the value requested */
	* PLPRCR (immrVal) = ( *PLPRCR (immrVal) & ~PLPRCR_MF_MSK) |
					(SPLL_MUL_FACTOR << PLPRCR_MF_SHIFT);

	/* set the BRGCLK division factor */
	*SCCR(immrVal) = (*SCCR(immrVal) & ~SCCR_DFBRG_MSK) |
					 (BRGCLK_DIV_FACTOR << SCCR_DFBRG_SHIFT);

	*SCCR (immrVal) &= ~(SCCR_RTSEL | SCCR_RTDIV);

	*MAMR(immrVal) = (*MAMR(immrVal) & ~MAMR_PTA_MSK) |
					 (PTA_VALUE << MAMR_PTA_SHIFT);

	*MPTPR(immrVal) = PTP_VALUE;

    /* reset the port A */

    *PAPAR(immrVal) = 0x0000;
    *PADIR(immrVal) = 0x0000;
    *PAODR(immrVal) = 0x0000;

    /* reset the port B */

    *PBPAR(immrVal) = 0x0000000;
    *PBDIR(immrVal) = 0x0000000;
    *PBODR(immrVal) = 0x0000000;
     

    /* reset the port C */

    *PCPAR(immrVal) = 0x0000;
    *PCDIR(immrVal) = 0x0000;
    *PCSO(immrVal)  = 0x0000;

    /* reset the port D */

/*  *PDPAR(immrVal) &= 0xe3f0;
	*PDDIR(immrVal) |= 0x1c0f;    
*/
	*PDPAR(immrVal) = 0x0000;
    *PDDIR(immrVal) = 0x000f;    
	*PDDAT(immrVal) = 0x0002;

	

#if 1 	
    *SICR(immrVal) = 0x0;		/* initialize SI/NMSI connections */
	*CICR(immrVal) = 0x0;
	*SIEL(immrVal) = 0x0;
	*SIMASK (immrVal) = 0x0000;
#endif
    /* Initialize interrupts */
    
    ppc860IntrInit (IV_LEVEL4);	/* default vector level */

	/* Created by gpande (2011/9/19 - 16:54:24) (Start) */
	excConnect((VOIDFUNCPTR *)_EXC_OFF_MACH, VMESlaveErrReset);
	/* Created by gpande (2011/9/19 - 16:54:26) (End) */

	FailLedOff ();
	
	ledAllOff();

	/* Reset serial channels */
	sysSerialHwInit();
		
	*(UCHAR*)WATCH_DOG_TIMER_ADR = 0x00;	/*	WatchDog Timer reset	*/

#ifdef INCLUDE_CPM
	
	for (cnt=0; cnt<IP_MAX_UNITS; cnt++) {
		sysCpmEnetDisable (cnt);
		sysCpmEnetIntDisable (cnt);
	}
#endif /* INCLUDE_CPM */
	
	sysAMCodeSet(SUPERVISORY_DATA);
	
    vxPowerModeSet (DEFAULT_POWER_MGT_MODE);

}

void	sysTest()
{
	int	immrVal = vxImmrGet();
	printf("immrVal : 0x%x\n",immrVal);
	printf("SIPEND : 0x%x\n",*SIPEND (immrVal));
	printf("SIMASK : 0x%x\n",*SIMASK (immrVal));
	printf("SIEL : 0x%x\n",*SIEL (immrVal));
	printf("SIVEC : 0x%x\n",*SIVEC (immrVal));
}

void	sysPhyMemClr()
{

}

/*******************************************************************************
*
* sysPhysMemTop - get the address of the top of physical memory
*
* This routine returns the address of the first missing byte of memory,
* which indicates the top of memory.
*
* RETURNS: The address of the top of physical memory.
*
* SEE ALSO: sysMemTop()
*/

char * sysPhysMemTop (void)
{
    static char * physTop = NULL;

    if (physTop == NULL){
#ifdef LOCAL_MEM_AUTOSIZE
	/* use the calculated size of Ram */

	physTop = (char *)(LOCAL_MEM_LOCAL_ADRS + sysRamSize);
#else
	/* use the default size for Ram */

#if 1
	physTop = (char *)(LOCAL_MEM_LOCAL_ADRS + LOCAL_DISP_MEM_SIZE);
#else
	physTop = (char *)(LOCAL_MEM_LOCAL_ADRS + LOCAL_MEM_SIZE);
#endif
	
#endif
	}

    return (physTop) ;
}

/*******************************************************************************
*
* sysMemTop - get the address of the top of VxWorks memory
*
* This routine returns a pointer to the first byte of memory not
* controlled or used by VxWorks.
*
* The user can reserve memory space by defining the macro USER_RESERVED_MEM
* in config.h.  This routine returns the address of the reserved memory
* area.  The value of USER_RESERVED_MEM is in bytes.
*
* RETURNS: The address of the top of VxWorks memory.
*/

char * sysMemTop (void)
    {
    static char * memTop = NULL;

    if (memTop == NULL)
	{
	memTop = sysPhysMemTop () - USER_RESERVED_MEM;
	}

    return memTop;
    }


/******************************************************************************
*
* sysToMonitor - transfer control to the ROM monitor
*
* This routine transfers control to the ROM monitor.  Normally, it is called
* only by reboot()--which services ^X--and bus errors at interrupt level.
* However, in some circumstances, the user may wish to introduce a
* <startType> to enable special boot ROM facilities.
*
* RETURNS: Does not return.
*/

STATUS sysToMonitor
    (
     int startType	/* parameter passed to ROM to tell it how to boot */
    )
    {
    
    
    FUNCPTR pRom = (FUNCPTR) (ROM_TEXT_ADRS + 4);	/* Warm reboot */

	
	*CIMR (vxImmrGet()) = 0;		/* disable all cpm interrupts */
	
	

#ifdef INCLUDE_CPM
   	sysCpmEnetDisable (0);	/* disable the ethernet device */
#endif /* INCLUDE_CPM */

#if 0
    sysSerialReset();		/* reset the serial device */
    sysSccSerialHwInit(2);
    sysSccSerialHwInit(3);
#if 1	/* MSCHOI CHECK_ME */
		memWrite((unsigned char *)0xf7000006, (unsigned char) 0xff);
#else
		memWrite(0xf7000006,0xffff);
#endif
#endif	
	
	
    /*sys16c554SerialReset();*/
    
    (*pRom) (startType);	/* jump to bootrom entry point */

    return (OK);	/* in case we ever continue from ROM monitor */
    }

/******************************************************************************
*
* sysHwInit2 - additional system configuration and initialization
*
* This routine connects system interrupts and does any additional
* configuration necessary.
*
* RETURNS: N/A
*
* NOMANUAL
*/

void sysHwInit2 (void)
    {
    static BOOL configured = FALSE;
    int		immrVal;
	struct tm tm;
	struct timespec tv;
	
	immrVal = vxImmrGet();

    if (!configured){
		/* initialize serial interrupts */
		
		sysSerialHwInit2(); /* SMC INTERRUPT CONNECTION */
		
		*SCCR(immrVal) &= ~SCCR_TBS;

		* TBSCR(immrVal) = TBSCR_TBE | TBSCR_TBF;

		configured = TRUE;
	}
	
	sysRtcGet(&tm);
	tv.tv_sec = mktime(&tm);
	tv.tv_nsec = 0;
	clock_settime(CLOCK_REALTIME, &tv);
}

/******************************************************************************
*
* sysProcNumGet - get the processor number
*
* This routine returns the processor number for the CPU board, which is
* set with sysProcNumSet().
* 
* RETURNS: The processor number for the CPU board.
*
* SEE ALSO: sysProcNumSet()
*/

int sysProcNumGet (void)
    {
    return (sysProcNum);
    }

/******************************************************************************
*
* sysProcNumSet - set the processor number
*
* This routine sets the processor number for the CPU board.  Processor numbers
* should be unique on a single backplane.
*
* Not applicable for the busless 860Ads.
*
* RETURNS: N/A
*
* SEE ALSO: sysProcNumGet()
*
*/

void sysProcNumSet
    (
    int 	procNum			/* processor number */
    )
    {
    sysProcNum = procNum;
    }

/******************************************************************************
*
* sysLocalToBusAdrs - convert a local address to a bus address
*
* This routine gets the VMEbus address that accesses a specified local
* memory address.
*
* Not applicable for the 860Ads
*
* RETURNS: ERROR, always.
*
* SEE ALSO: sysBusToLocalAdrs()
*/
 
STATUS sysLocalToBusAdrs
    (
    int 	adrsSpace,	/* bus address space where busAdrs resides */
    char *	localAdrs,	/* local address to convert */ 
    char **	pBusAdrs	/* where to return bus address */ 
    )
    {
       return (ERROR);
    }

/******************************************************************************
*
* sysBusToLocalAdrs - convert a bus address to a local address
*
* This routine gets the local address that accesses a specified VMEbus
* physical memory address.
*
* Not applicable for the 860Ads
*
* RETURNS: ERROR, always.
*
* SEE ALSO: sysLocalToBusAdrs()
*/

STATUS sysBusToLocalAdrs
    (
    int  	adrsSpace, 	/* bus address space where busAdrs resides */
    char *	busAdrs,   	/* bus address to convert */
    char **	pLocalAdrs 	/* where to return local address */
    )
    {
    switch (adrsSpace)
    {
	case VME_AM_SUP_SHORT_IO:
	case VME_AM_USR_SHORT_IO:
	    if (busAdrs > (char *)0x0000ffff)	/* if (size > 64KB) */
		return (ERROR);

	    *pLocalAdrs = (char *) (0xf4000000 | (int)busAdrs);
	    return (OK);

	case VME_AM_STD_SUP_PGM:
	case VME_AM_STD_SUP_DATA:
	case VME_AM_STD_USR_PGM:
	case VME_AM_STD_USR_DATA:
	    if (busAdrs > (char *)0x00ffffff)	/* if (size > 16MB) */
		return (ERROR);

	    *pLocalAdrs = (char *)(0xf0000000 | (int)busAdrs);
		return (OK);

	case VME_AM_EXT_SUP_PGM:
	case VME_AM_EXT_SUP_DATA:
	case VME_AM_EXT_USR_PGM:
	case VME_AM_EXT_USR_DATA:
	    if (busAdrs < sysPhysMemTop () || busAdrs > (char *)0xf0000000)
		return (ERROR);

	    *pLocalAdrs = (char *) busAdrs;
	    return (OK);

	default:
	    return (ERROR);
	}
}

/******************************************************************************
*
* sysBusTas - test and set a location across the bus
*
* This routine does an atomic test-and-set operation across the backplane.
*
* Not applicable for the 860Ads.
*
* RETURNS: FALSE, always.
*
* SEE ALSO: vxTas()
*/

BOOL sysBusTas
    (
    char *	adrs		/* address to be tested-and-set */
    )
    {
    return (TRUE);
    }

/******************************************************************************
*
* sysBusClearTas - test and clear 
*
* This routine is a null function.
*
* RETURNS: N/A
*/

void sysBusClearTas
    (
    volatile char * address	/* address to be tested-and-cleared */
    )
    {
    } 

#ifdef INCLUDE_CPM
/*******************************************************************************
*
* sysCpmEnetDisable - disable the Ethernet controller
*
* This routine is expected to perform any target specific functions required
* to disable the Ethernet controller.  This usually involves disabling the
* Transmit Enable (TENA) signal.
*
* RETURNS: N/A
*/

void sysCpmEnetDisable
(
    int	unit    
)
{
	switch (unit) {
		case 0:
    		*PBPAR(vxImmrGet()) &= ~(0x1000); SYNC;		/* set port B -> RTS1 */
			break;
		case 1:
    		*PBPAR(vxImmrGet()) &= ~(0x2000); SYNC;
			break;
		default:
    		*PBPAR(vxImmrGet()) &= ~(0x1000); SYNC;
			break;
	}
}



/*******************************************************************************
*
* sysCpmEnetIntDisable - disable the Ethernet interface interrupt
*
* This routine disable the interrupt for the Ethernet interface specified
* by <unit>.
*
* RETURNS: N/A.
*/


void sysCpmEnetIntDisable
(
	int		unit	
)
{
	switch (unit) {
		case 0:
    		*CIMR(vxImmrGet()) &= ~CIMR_SCC1;
			break;
		case 1:
    		*CIMR(vxImmrGet()) &= ~CIMR_SCC2;
			break;
		default:
    		*CIMR(vxImmrGet()) &= ~CIMR_SCC1;
			break;
	}
}

/*******************************************************************************
*
* sysCpmEnetEnable - enable the Ethernet controller
*
* This routine is expected to perform any target specific functions required
* to enable the Ethernet controller.  These functions typically include
* enabling the Transmit Enable signal (TENA) and connecting the transmit
* and receive clocks to the SCC.
*
* RETURNS: OK, or ERROR if the Ethernet controller cannot be enabled.
*/

STATUS sysCpmEnetEnable
(
int		unit    
)
{
    int immrVal = vxImmrGet();


	switch(unit){
		case 0 :
			/****************************************************/
			/* 				SCC1 Ethernet Set 					*/
			/****************************************************/

			/* Port A Pins :  RXD1, TXD1, RxCLK1, TxCLK1 Set	*/
	    	*PAPAR (immrVal) |= 0x0303; SYNC;
	    	*PADIR (immrVal) &= ~0x0303; SYNC;
		    *PAODR (immrVal) &= ~0x0002; SYNC;

		    /* Port B Pins */                 
    		*PBPAR(immrVal) |= 0x1000; SYNC;	/*	RTS1 Set				*/
	    	*PBDIR(immrVal) |= 0x1000; SYNC;
	    	*PBODR(immrVal) &= ~0x1000; SYNC;

		    *PBPAR(immrVal) &= ~0x0020; SYNC;	/*	LPBK1 Set				*/
    		*PBDIR(immrVal) |= 0x0020; SYNC;
	    	*PBODR(immrVal) |= 0x0020; SYNC;

		    *PBDAT(immrVal) &= ~0x0020; SYNC;

		    /* Port C Pins */                 
    		*PCPAR(immrVal) &= ~0x0030; SYNC;
	    	*PCDIR(immrVal) &= ~0x0030; SYNC;
	    	*PCSO(immrVal)  |= 0x0030; SYNC;
    
		    /* Clear SCC1 Clock Sources and set NMSI */
    		*SICR (immrVal) &= ~(SICR_SC1_MUX | SICR_R1CS_MSK | SICR_T1CS_MSK);
	    	SYNC;
 
 		   /* Set SCC1 Clock Sources to Rx: CLK1, and TX: CLK2 */
    		*SICR (immrVal) |= (SICR_R1CS_CLK1 | SICR_T1CS_CLK2);
	    	SYNC;

			break;
	
		case 1 :
			/****************************************************/
			/* 				SCC2 Ethernet Set 					*/
			/****************************************************/

			/* Port A Pins :  RXD2, TXD2, RxCLK2, TxCLK2 Set	*/
			*PAPAR(immrVal) |=  0x0c0c; SYNC;	
			*PADIR(immrVal) &=  ~0x0c0c; SYNC;
			*PAODR(immrVal) &=  ~0x0008; SYNC;

			/* Port B Pins */
			*PBPAR (immrVal) |= 0x2000; SYNC;	/*	RTS2 Set				*/
			*PBDIR (immrVal) |= 0x2000; SYNC;
			*PBODR (immrVal) &= ~0x2000; SYNC;

		    *PBPAR(immrVal) &= ~0x0004; SYNC;	/*	LPBK2 Set				*/
    		*PBDIR(immrVal) |= 0x0004; SYNC;
	    	*PBODR(immrVal) |= 0x0004; SYNC;

		    *PBDAT(immrVal) &= ~0x0004; SYNC;

			/* Port C Pins */
			*PCPAR(immrVal) &= ~0x00c0; SYNC;	/*	LPBK1 Set				*/
			*PCDIR(immrVal) &= ~0x00c0; SYNC;
			*PCSO(immrVal)  |= 0x00c0; SYNC;

			/* Clear SCC2 Clock Sources and set NMSI */
			*SICR (immrVal) &= ~(SICR_SC2_MUX | SICR_R2CS_MSK | SICR_T2CS_MSK);
			SYNC;

			/* Set SCC2 Clock Sources to Rx: CLK3, and TX: CLK4 */
			*SICR (immrVal) |= (SICR_R2CS_CLK3 | SICR_T2CS_CLK4);
			SYNC;
			
			break;
	}

    /* U-BUS arbitration priority 5 (BR5) */
    *MPC860_SDCR(immrVal) = SDCR_RAID_BR5; SYNC;

    return (OK);
}


/*******************************************************************************
*
* sysCpmEnetAddrGet - get the hardware Ethernet address
*
* This routine provides the six byte Ethernet hardware address that will be
* used by each individual Ethernet device unit.  This routine must copy
* the six byte address to the space provided by <addr>.
*
* RETURNS: OK, or ERROR if the Ethernet address cannot be returned.
*/

STATUS sysCpmEnetAddrGet
    (
    int		unit,   /* not used */
    UINT8 *	addr
    )
{
	char	temp_buf[7];
	int		i;

	/********************************/
	/*	Get Mac Address From 93c46	*/
	/********************************/
	GetsysCpmEnetAddr(&temp_buf[0],unit);
#if 0	/* DIFF_IT : rev_1 */
	if(temp_buf[0] != COMPANY_MAC_ID)
		bcopy ((char *) sysCpmEnetAddr, (char *) addr, MAC_ADDR_NUMBER);
   	else
		for(i=0;i<6;i++)			addr[i] = temp_buf[i+1];
    	
	return(OK);
#else	/* DIFF_IT : rev_2 */
	if(unit == 0){
		if(temp_buf[0] != COMPANY_MAC_ID_E1)
			bcopy ((char *) sysCpmEnetAddr, (char *) addr, MAC_ADDR_NUMBER);
   		else
			for(i=0;i<6;i++)			addr[i] = temp_buf[i+1];
    	
		return(OK);
	}else{
		if(temp_buf[0] != COMPANY_MAC_ID_E2)
			bcopy ((char *) sysCpmEnetAddr, (char *) addr, MAC_ADDR_NUMBER);
   		else
			for(i=0;i<6;i++)			addr[i] = temp_buf[i+1];
    	
		return(OK);
	}
#endif	/* DIFF_IT : rev_2 */
}


/*******************************************************************************
*
* sysCpmEnetCommand - issue a command to the Ethernet interface controller
*
* RETURNS: OK, or ERROR if the Ethernet controller could not be restarted.
*/

STATUS sysCpmEnetCommand
    (
    int		unit,
    UINT16	command
    )
    {
    int	immrVal = vxImmrGet();
	int	channel = 0;

	switch (unit) {
		case 0:
			channel = CPM_CR_CHANNEL_SCC1;
			break;
		case 1:
			channel = CPM_CR_CHANNEL_SCC2;
			break;
		default:
			channel = CPM_CR_CHANNEL_SCC1;
			break;
	}

    while (*CPCR(immrVal) & CPM_CR_FLG);
        *CPCR(immrVal) = channel | command | CPM_CR_FLG;

    while (*CPCR(immrVal) & CPM_CR_FLG)
	/* do nothing */;

    return (OK);
}

/*******************************************************************************
*
* sysCpmEnetIntEnable - enable the Ethernet interface interrupt
*
* This routine enable the interrupt for the Ethernet interface specified
* by <unit>.
*
* RETURNS: N/A.
*/

void sysCpmEnetIntEnable
    (
    int		unit
    )
    {
	switch (unit) {
		case 0:
    		*CIMR(vxImmrGet()) |= CIMR_SCC1;	
			break;
		case 1:
    		*CIMR(vxImmrGet()) |= CIMR_SCC2;
			break;
		default :
    		*CIMR(vxImmrGet()) |= CIMR_SCC1;	
			break;
	}
}

/*******************************************************************************
*
* sysCpmEnetIntClear - clear the Ethernet interface interrupt
*
* This routine clears the interrupt for the Ethernet interface specified
* by <unit>.
*
* RETURNS: N/A.
*/

void sysCpmEnetIntClear
    (
    int		unit
    )
    {
	switch (unit) {
		case 0:
    		*CISR(vxImmrGet()) = CISR_SCC1;
			break;
		case 1:
    		*CISR(vxImmrGet()) = CISR_SCC2;
			break;
		default :
    		*CISR(vxImmrGet()) = CISR_SCC1;
			break;
	}
}

/*****************************************************************************
*
* EnetAttach - Ethernet Attach routine
*
* RETURNS : N/A.
*/
IMPORT int	ipAttach();

STATUS EnetAttach (int unit, char *ipBuf, int netmask)
{
	char str[4];

	/* build interface name */
	sprintf (str, "cpm%d", unit);

	if (ipAttach (unit, "cpm") != OK) {
		printErr ("ipAttach failed..\n\n", 0,0,0,0,0,0);
		return (ERROR);
	}

	if (ifMaskSet (str, netmask) != OK) {
		printErr ("ifMaskSet failed..\n\n", 0,0,0,0,0,0);
		return (ERROR);
	}

	ifAddrSet (str, ipBuf);

#if 0
	if (ifMaskSet (str, netmask) != OK) {
		printErr ("ifMaskSet failed..\n\n", 0,0,0,0,0,0);
		return (ERROR);
	}
	if (ipAttach (unit, "cpm") != OK) {
		printErr ("ipAttach failed..\n\n", 0,0,0,0,0,0);
		return (ERROR);
	}

	/* set subnet mask */
	bootNetmaskExtract (ipBuf, &netmask);
	if (netmask != 0)
		ifMaskSet (str, netmask);

	/* set inet address */
	if (ifAddrSet (str, ipBuf) != OK) {
		printf ("Error setting other inet address of %s to %s\n", 
			str, ipBuf);
		return (ERROR);
	}
#endif


	return (OK);
}


#endif	/* INCLUDE_CPM */

void sysMsDelay (UINT delay)
{
	register UINT oldval;		/* decrementer value */
	register UINT newval;		/* decrementer value */
	register UINT totalDelta;	/* Dec. delta for entire delay period */
	register UINT decElapsed;	/* cumulative decrementer ticks */

	totalDelta = ((DEC_CLOCK_FREQ / DEC_CLK_TO_INC) / 1000) * delay;

	decElapsed = 0;

	oldval = vxDecGet ();

	while (decElapsed < totalDelta) {
		newval = vxDecGet ();

		if ( DELTA (oldval, newval) < 1000)
			decElapsed += DELTA (oldval, newval);	/* no rollover */
		else if (newval > oldval)
			decElapsed += abs ((int) oldval);		/* rollover */

		oldval = newval;
	}
}

void sysUsDelay (UINT delay)
{
	register UINT oldval;		/* decrementer value */
	register UINT newval;		/* decrementer value */
	register UINT totalDelta;	/* Dec. delta for entire delay period */
	register UINT decElapsed;	/* cumulative decrementer ticks */

	totalDelta = ((DEC_CLOCK_FREQ / DEC_CLK_TO_INC) / 1000000) * delay;

	decElapsed = 0;

	oldval = vxDecGet ();

	while (decElapsed < totalDelta) {
		newval = vxDecGet ();

		if ( DELTA (oldval, newval) < 1000)
			decElapsed += DELTA (oldval, newval);	/* no rollover */
		else if (newval > oldval)
			decElapsed += abs ((int) oldval);		/* rollover */

		oldval = newval;
	}
}

/*****************************************************
 *
 * Utilities & BSP libraries
 *
 *****************************************************/

#define	LKV402A_EAD		0
#define	LKV402A_OTHER	1

void
IpAdrsExtract (char *ipbuf, char *ext_ip)
{
	int len;
	char *idx;

	idx = index (ipbuf, ':');			/* extract ip adrs dot notation */

	if (idx != NULL) {
		len = idx - ipbuf;
		strncpy (ext_ip, ipbuf, len);
		ext_ip[len] = EOS;
	}
	else {
		strcpy (ext_ip, ipbuf);
	}
}

STATUS
inetIpGetChange (char *bootString, int what)
{
	BOOT_PARAMS	pBoot;
	ULONG	iphex, ipchange;
	int 	netmask, stat, len;
	char 	newEad[BOOT_TARGET_ADDR_LEN]; /* new ether adrs included netmask */
	char	pStr[BOOT_TARGET_ADDR_LEN];		/* org ether string */
	char 	nvIp[BOOT_ADDR_LEN]; 			/* org. only ip adrs */
	char	tmpIp[BOOT_ADDR_LEN];			/* changed only ip adrs */
	char	*ptr;							/* params pointer */
	struct in_addr iaddr;
	UCHAR	enc_val;

	bootStringToStruct (bootString, &pBoot);

	if (what == LKV402A_EAD)	ptr = pBoot.ead;
	else						ptr = pBoot.other;

	strcpy (pStr, ptr);

	if (pStr[0] == EOS)	{
		return ERROR;
	}

	if (what == LKV402A_EAD)
		printf ("---------------Ethernet Address -------------------\n");
	else
		printf ("---------------Other Address -------------------\n");

	printf ("NV enet = %s\n", pStr);

	IpAdrsExtract (pStr, nvIp);

	printf ("nvIp == %s\n", nvIp);

	enc_val = encRead (TRUE);

	if (enc_val == 0xff || enc_val == 0x00) {
		printf ("This IP is not available....\n");
		printf ("----------------------------------------\n");
	}

	iphex = inet_addr (nvIp);
	ipchange = (iphex & 0xffffff00) | enc_val;

#if 0
	if ((iphex & 0x000000ff) == enc_val)	return OK;
#endif
	iaddr.s_addr = ipchange;

	strcpy (tmpIp, (char *) inet_ntoa (iaddr));	/* insert only ip adrs */
	printf ("changing IP : %s\n", tmpIp);

	stat = bootNetmaskExtract (pStr, &netmask);
	if (stat > 0) {
		printf ("net mask = %#x\n", netmask);
		sprintf (newEad, "%s:%x", tmpIp, netmask);
	}
	else {
		netmask = 0xffffff00;
		sprintf (newEad, "%s:%x", tmpIp, netmask);
		printf ("netmask = %#x\n", netmask);
	}

	len = strlen (newEad);
	strcpy (ptr, newEad);					/* insert net mask */

	printf ("final address == %s\n", ptr);
	printf ("struct ead : %s\n", pBoot.ead);
	printf ("----------------------------------------\n");

	bootStructToString ((char*)0x4200, &pBoot);
	bootStructToString ((char*)0xf2000200, &pBoot);
	
	bootParamsShow((char*)0x4200);

	if (what == LKV402A_OTHER) {
		printf ("\nAttached TCP/IP interface to %s%d.\n", "cpm", pBoot.unitNum + 1);
		EnetAttach (pBoot.unitNum + 1, tmpIp, netmask);
	}



	return OK;
}

/* board IP Change function */

STATUS
inetIpSetChange (char *ipStr, int what)
{
	BOOT_PARAMS	pBoot;
	ULONG	iphex, ipchange;
	int 	netmask, stat, len;
	char 	newEad[BOOT_TARGET_ADDR_LEN]; /* new ether adrs included netmask */
	char	pStr[BOOT_TARGET_ADDR_LEN];		/* org ether string */
	char 	nvIp[BOOT_ADDR_LEN]; 			/* org. only ip adrs */
	char	tmpIp[BOOT_ADDR_LEN];			/* changed only ip adrs */
	char	*ptr;							/* params pointer */
	struct in_addr iaddr;
	UCHAR	enc_val;
	
	bootStringToStruct (BOOT_LINE_ADRS, &pBoot);

	ptr = ipStr;

	strcpy (pStr, ptr);

	if (pStr[0] == EOS)	{
		return ERROR;
	}

	if (what == LKV402A_EAD)
		printf ("---------------Ethernet Address -------------------\n");
	else
		printf ("---------------Other Address -------------------\n");

	printf ("NV enet = %s\n", pStr);

	IpAdrsExtract (pStr, nvIp);

	printf ("nvIp == %s\n", nvIp);

	enc_val = encRead (TRUE);

	if (enc_val == 0xff || enc_val == 0x00) {
		printf ("This IP is not available....\n");
		printf ("----------------------------------------\n");
	}

	iphex = inet_addr (nvIp);
	ipchange = (iphex & 0xffffff00) | enc_val;

#if 0
	if ((iphex & 0x000000ff) == enc_val)	return OK;
#endif
	iaddr.s_addr = ipchange;

	strcpy (tmpIp, (char *) inet_ntoa (iaddr));	/* insert only ip adrs */
	printf ("changing IP : %s\n", tmpIp);

	stat = bootNetmaskExtract (pStr, &netmask);
	if (stat > 0) {
		printf ("net mask = %#x\n", netmask);
		sprintf (newEad, "%s:%x", tmpIp, netmask);
	}
	else {
		netmask = 0xffffff00;
		sprintf (newEad, "%s:%x", tmpIp, netmask);
		printf ("netmask = %#x\n", netmask);
	}

	len = strlen (newEad);
	if (what == LKV402A_EAD){
		strcpy (pBoot.ead, newEad);
	}else{
		strcpy (pBoot.other, newEad);
	}
						/* insert net mask */

	printf ("final address == %s\n", ptr);
	printf ("struct ead : %s\n", pBoot.ead);
	printf ("----------------------------------------\n");

	bootStructToString ((char*)0x4200, &pBoot);
	bootStructToString ((char*)0xf2000200, &pBoot);
	
	bootParamsShow((char*)0x4200);

	if (what == LKV402A_OTHER) {
		printf ("\nAttached TCP/IP interface to %s%d.\n", "cpm", pBoot.unitNum + 1);
		EnetAttach (pBoot.unitNum + 1, tmpIp, netmask);
	}



	return OK;
}
/******************************************************************************************/

STATUS
inetIpSecondSet (char *bootString)
{
	BOOT_PARAMS	pBoot;
	ULONG	iphex, ipchange;
	int 	netmask, stat, len;
	char 	newEad[BOOT_TARGET_ADDR_LEN]; /* new ether adrs included netmask */
	char	pStr[BOOT_TARGET_ADDR_LEN];		/* org ether string */
	char 	nvIp[BOOT_ADDR_LEN]; 			/* org. only ip adrs */
	char	tmpIp[BOOT_ADDR_LEN];			/* changed only ip adrs */
	char	*ptr;							/* params pointer */
	struct in_addr iaddr;
	

	bootStringToStruct (bootString, &pBoot);

	ptr = pBoot.other;

	strcpy (pStr, ptr);

	if (pStr[0] == EOS)	{
		return ERROR;
	}

	
	printf ("---------------Other Address -------------------\n");

	printf ("NV enet = %s\n", pStr);

	IpAdrsExtract (pStr, nvIp);

	printf ("nvIp == %s\n", nvIp);

	iphex = inet_addr (nvIp);
	ipchange = (iphex & 0xffffffff);

	iaddr.s_addr = ipchange;

	strcpy (tmpIp, (char *) inet_ntoa (iaddr));	/* insert only ip adrs */
	printf ("changing IP : %s\n", tmpIp);

	stat = bootNetmaskExtract (pStr, &netmask);
	if (stat > 0) {
		printf ("net mask = %#x\n", netmask);
		sprintf (newEad, "%s:%x", tmpIp, netmask);
	}
	else {
		netmask = 0xffffff00;
		sprintf (newEad, "%s:%x", tmpIp, netmask);
		printf ("netmask = %#x\n", netmask);
	}

	len = strlen (newEad);
	strcpy (ptr, newEad);					/* insert net mask */

	printf ("final address == %s\n", ptr);
	printf ("struct other : %s\n", pBoot.other);
	printf ("----------------------------------------\n");

	bootStructToString ((char*)0x4200, &pBoot);
	bootStructToString ((char*)0xf2000200, &pBoot);
	
	bootParamsShow((char*)0x4200);

	
	printf ("\nAttached TCP/IP interface to %s%d.\n", "cpm", pBoot.unitNum + 1);
	EnetAttach (pBoot.unitNum + 1, tmpIp, netmask);
	
	return OK;
}
/******************************************************************************************/


UCHAR
encRead (int prn)
{
	UCHAR	val;

	val = *(UCHAR *) (ENC_SW_BA);
	val = (~val & 0xff)>>4;

	if (prn == TRUE)
		printf ("Encoder rotary switch value = %#x[%d]\n", val, val);

	return (val);
}

UCHAR
get_hdlc_ch_id (void)
{
	return (encRead (FALSE));
}


/***********************************************
*
*	Watch Dog Timer Clear ISR
*
*/
void 
wdtClearISR()
{
	static int	flag;
	
	int immrVal = vxImmrGet();

    *PBPAR(immrVal) &= ~(0x18); 	/* PB27, 28 General Purpos IO Mode(0) Set */
	*PBDIR(immrVal) |= 0x18;		/* PB27, 28 Output(1) Mode Set */
	*PBODR(immrVal) |= 0x18;		/* PB27, 28 3-stated Mode(1) Set */	
	
	if(_sw_wid){
		if(flag == 0){
			flag = 1;
			*PBDAT(immrVal)	&=	~(0x10); /*SYNC;*/
		}
		else{
			flag = 0;
			*PBDAT(immrVal)	|=	(0x10); /*SYNC;*/
			
		}
    	
		if(_sw_watchdog_flag == 1){
			wdStart(_sw_wid, sysClkRateGet(), (FUNCPTR)wdtClearISR,0);
			
		}
	}
}
/***********************************************
*
*	Watch Dog Timer wdtReset
*
*/
void	wdtReset()
{
	ledCon(DIAG1_ON);
	if(_sw_watchdog_flag == 1){
		_sw_watchdog_flag = 0;
		
		if(_sw_wid){
			wdDelete(_sw_wid);
			_sw_wid = 0;
		}
	}	
}

/***********************************************
*
*	Watch Dog Timer Enable
*
*/
void	
wdtEnable()
{
	_sw_watchdog_flag = 1;
	printf("_sw_wid = %d\n", (int) _sw_wid);
	if(!_sw_wid){
		_sw_wid = wdCreate ();
		wdStart(_sw_wid, sysClkRateGet(), (FUNCPTR)wdtClearISR,0);
	}
	taskDelay(2000);
	*(char *)WATCH_DOG_TIMER_ADR = 0xff;
		
}

void 
wdtDisable()
{
	*(char *)WATCH_DOG_TIMER_ADR = 0x00;
	taskDelay(10);
	wdtReset();
	ledCon(DIAG1_OFF);
	
}


/***********************************************
*
*	LED control function
*
*/

STATUS ledCon(char led)
{
	int immrVal = vxImmrGet();


    *PBPAR(immrVal) &= ~(0x18); 	/* PB27, 28 General Purpos IO Mode(0) Set */
	*PBDIR(immrVal) |= 0x18;		/* PB27, 28 Output(1) Mode Set */
	*PBODR(immrVal) |= 0x18;		/* PB27, 28 3-stated Mode(1) Set */	

	switch(led){
		case DIAG0_ON:
			*PBDAT(immrVal)	&= ~(0x08); /*SYNC;*/
			return(OK);

		case DIAG0_OFF:
			*PBDAT(immrVal)	|= (0x08); /*SYNC;*/
			return(OK);

		case DIAG1_ON:
			*PBDAT(immrVal)	&=	~(0x10); /*SYNC;*/
			return(OK);

		case DIAG1_OFF:
			*PBDAT(immrVal)	|=	(0x10); /*SYNC;*/
			return(OK);
		
		default : 	break;
	}
	return OK;
}

STATUS ledAllOff()
{
	int immrVal = vxImmrGet();


    *PBPAR(immrVal) &= ~(0x18); 	/* PB27, 28 General Purpos IO Mode(0) Set */
	*PBDIR(immrVal) |= 0x18;		/* PB27, 28 Output(1) Mode Set */
	*PBODR(immrVal) |= 0x18;		/* PB27, 28 3-stated Mode(1) Set */	
	
	*PBDAT(immrVal)	|= (0x18); /*SYNC;*/
	return(OK);
	
	
}

STATUS ledAllOn()
{
	int immrVal = vxImmrGet();


    *PBPAR(immrVal) &= ~(0x18); 	/* PB27, 28 General Purpos IO Mode(0) Set */
	*PBDIR(immrVal) |= 0x18;		/* PB27, 28 Output(1) Mode Set */
	*PBODR(immrVal) |= 0x18;		/* PB27, 28 3-stated Mode(1) Set */	
	
	*PBDAT(immrVal)	&=	~(0x18); /*SYNC;*/
	return(OK);
	
	
}


void FailLedOff (void)
{
	*(UCHAR *)ERR_LED_OFF_ADR = 0x00;
}

void FailLedOn (void)
{
	*(UCHAR *) ERR_LED_ON_ADR = 0x00;
}


UCHAR 
DipSwRead (int prn)
{
	UCHAR	val;

	val = *(UCHAR *) DIP_SW_BA & 0x7F;

	if (prn == TRUE)
		printf("dip_sw_value : %#x[%d]\n", val, val);

	return (val);
}

UCHAR
CcuDipSel(int prn)
{
	UCHAR	val;

	val = *(UCHAR *) DIP_SW_BA & 0x0F;

	if (prn == TRUE)
		printf("dip_sw_value : %#x[%d]\n", val, val);

	return (val);
}

STATUS	sysAMCodeSet(int am_cmd)
{
	
	int immrVal;

	immrVal = vxImmrGet();

	*PDPAR(immrVal) &= ~(0x1c00); 	/* PD3, 4, 5 General Purpos IO Mode(0) Set */
	*PDDIR(immrVal) |= 0x1c00;		/* PD3, 4, 5 Output(1) Mode Set */

	switch(am_cmd)
	{
		case	NON_PRIVILEGED_DATA :
			*PDDAT(immrVal)	&= ~CLEAR_AMCODE_AREA << 10;
			*PDDAT(immrVal)	|= NON_PRIVILEGED_DATA << 10;
			break;
		case	NON_PRIVILEGED_PROG :
			*PDDAT(immrVal)	&= ~CLEAR_AMCODE_AREA << 10;
			*PDDAT(immrVal)	|= NON_PRIVILEGED_PROG << 10;
			break;
		case	SUPERVISORY_DATA :
			*PDDAT(immrVal)	&= ((~CLEAR_AMCODE_AREA << 10) | 0x02);
			*PDDAT(immrVal)	|= SUPERVISORY_DATA << 10;
			break;
		case	SUPERVISORY_PROG :
			*PDDAT(immrVal)	&= ~CLEAR_AMCODE_AREA << 10;
			*PDDAT(immrVal)	|= SUPERVISORY_PROG << 10;
			break;
		default	:
			*PDDAT(immrVal)	&= ~CLEAR_AMCODE_AREA << 10;
			*PDDAT(immrVal)	|= SUPERVISORY_DATA << 10;
			break;
	}

	return OK;
}

/*******************************************************
*					Digital Input					   *
*******************************************************/
UCHAR digInChan (int chan)
{
	unsigned char di_value;
	di_value=0;
	di_value = *(unsigned char*)DIG_IN_ADRS;
	
	if ((di_value & (0x1 << (chan+4) ) ) != 0x0)
	{
		di_value = DI_ON;
	}		
	else
	{
		di_value = DI_OFF;
	}
	
	return (di_value);
}

UCHAR digIn()
{
	unsigned char inValue;
	inValue = *(unsigned char*)DIG_IN_ADRS;
	inValue &=0xf0;
	return (inValue);
}

STATUS 
digOutChan(int chan, char value)
{
	int immrVal;

	immrVal = vxImmrGet();

	/* PD12, 13, 14, 15 General Purpos IO Mode(0) Set */
	*PDPAR(immrVal) &= ~(0x000F);
	
	
	
	/* PD12, 13, 14, 15 Output(1) Mode Set */
	*PDDIR(immrVal) |= 0x000F;	
#if 0	
	printf("pdpar address = [0x%x]\n", PDPAR(immrVal)); 
	printf("pddir address = [0x%x]\n", PDDIR(immrVal)); 
#endif
		
	if(value == DO_ON)
	{ 	
		*PDDAT(immrVal)	|= 0x8 >> chan;
	}
	else
	{ 				
		*PDDAT(immrVal)	&= ~(0x8 >> chan);
	}
	
	*(unsigned char*)DIG_IN_ADRS = 0;
	
	return OK;
}


STATUS 
digOut(char value)
{
	int immrVal;

	immrVal = vxImmrGet();

	/* PD12, 13, 14, 15 General Purpos IO Mode(0) Set */
	*PDPAR(immrVal) &= ~(0x000F);	

	/* PD12, 13, 14, 15 Output(1) Mode Set */
	*PDDIR(immrVal) |= 0x000F;		

	*PDDAT(immrVal)	= (*PDDAT(immrVal) & 0xFFF0) | value;
/*	
	printf("pdpar address = 0x%x\n", PDPAR(immrVal));
	printf("pddir address = 0x%x\n", PDDIR(immrVal));
	printf("pddat address = 0x%x\n", PDDAT(immrVal)); 
*/
	return OK;
}
/* end of file */



#if 1
void 
digInOutChanLoop()
{
	int i;
	printf("channel out test!!\n");
	digOutChan(0,0);
	digOutChan(1,0);
	digOutChan(2,0);
	digOutChan(3,0);
	taskDelay(sysClkRateGet());
	digOutChan(0,1);
	taskDelay(sysClkRateGet());
	digOutChan(1,1);
	taskDelay(sysClkRateGet());
	digOutChan(2,1);
	taskDelay(sysClkRateGet());
	digOutChan(3,1);
	printf("channel in test!!\n");
	for(i=0;i<4;i++){
		printf("channel %d = %d\n", i, digInChan(i)); 
	}
	
}

#endif

/******************************************************************************
 *
 * board_help - print a synopsis of selected routines
 *
 * RETURNS : N/A
 */

void 
board_help (void)
{
	static char *help_msg[] = {
	"board_help                Print this list",
	"ledCon       led          Set LED ON/OFF",
	"                          0 - Diag 0 On",
	"                          1 - Diag 0 Off",
	"                          2 - Diag 1 On",
	"                          3 - Diag 1 Off",
	"encRead      prn          Read Encoder",
	"                          0 - not printing",
	"                          1 - printing",
	"DipSwRead    prn          Read DIP Switch - lower 4 bits",
	"                          0 - not debug printing",
	"                          1 - debug printing",
	"digIn                     Read Dig. In. All Chan. - higher 4bits",
	"digInChan    chan         Read Dig. In. the Chan.",
	"digOutChan   chan, value  Write to Dig. Out chan.",
	"get_hdlc_ch_id            Read Encoder for HDLC",
	"EnetAttach   unit, ip_adrs, netmask  Ethernet Attach",
	NULL
	};

	FAST int ix;
	char ch;

	printf ("\n");
	for (ix = 0; help_msg[ix] != NULL; ix++) {
		if ((ix+1) % 20 == 0) {
			printf ("\nType <CR> to continue, Q<CR> to stop: ");
			fioRdString (STD_IN, &ch, 1);
			if (ch == 'q' || ch == 'Q')	break;
			else						printf ("\n");
		}
		printf ("%s\n", help_msg[ix]);
	}

	printf ("\n");
}


#define	_swap(a, b)	{ a^=b; b^=a; a^=b; }

UINT32
swap_32 (UINT32 a)
{
	union {
		UINT32	data;
		UCHAR	c[4];
	} swaping;

	/*************************************
	 *  -----------------------------    *
     *  | msb1 | msb2 | lsb1 | lsb2 |    *
	 *  -----------------------------    *
	 *************************************/

	swaping.data = a;

	_swap (swaping.c[0], swaping.c[3]);
	_swap (swaping.c[1], swaping.c[2]);

	return (swaping.data);
}

INT16 
swap_16 (INT8 a)
{
	union {
		INT8	data;
		INT8	c[2];
	} swaping;

	swaping.data = a;
	_swap (swaping.c[0], swaping.c[1]);

	return (swaping.data);
}

void
mWrite(unsigned char *adrs, unsigned char value)
{
	*(unsigned char*)adrs = value;
}
