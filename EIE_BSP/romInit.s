/* romInit.s - Etin System KVME402 ROM initialization module */

/* Copyright 2002 Etin Systems, Inc. All Rights Reserved */

	.data
	.globl  copyright_wind_river
	.long   copyright_wind_river

/*
DESCRIPTION
This module contains the entry code for the VxWorks bootrom.
The entry point romInit, is the first code executed on power-up.
It sets the BOOT_COLD parameter to be passed to the generic
romStart() routine.

The routine sysToMonitor() jumps to the location 4 bytes
past the beginning of romInit, to perform a "warm boot".
This entry point allows a parameter to be passed to romStart().
*/

#define	_ASMLANGUAGE
#include "vxWorks.h"
#include "asm.h"
#include "cacheLib.h"
#include "regs.h"	
#include "sysLib.h"
#include "drv/multi/ppc860Siu.h"
#include "./config.h"
#include "kvme402a.h"

	/* internals */

	.globl	_romInit	/* start of system code */
	.globl	romInit		/* start of system code */
	.globl	LED_SET
	.globl	LED_ON
	.globl	LED_OFF
	
	/* externals */

	.extern romStart	/* system initialization routine */

	.text
	.align 2

/******************************************************************************
*
* romInit - entry point for VxWorks in ROM
*
*
* romInit
*     (
*     int startType	/@ only used by 2nd entry point @/
*     )
*
*/

_romInit:
romInit:
	bl	cold		/* jump to the cold boot initialization */
	bl	start		/* jump to the warm boot initialization */
	bl	warm

	/* copyright notice appears at beginning of ROM (in TEXT segment) */

	.ascii   "Copyright 1990-1997 Etin Systems, Inc."
	.align 2

cold:
	li	r3, BOOT_COLD	/* set cold boot as start type */

	/*
	 * When the PowerPC 860 is powered on, the processor fletch the
	 * instructions located at the address 0x100. We need to jump
	 * from the address 0x100 to the Flash space.
	 */


	lis	r4, HIADJ(start)			/* load r4 with the address */
	addi	r4, r4, LO(start)			/* of start */

	lis	r5, HIADJ(romInit)			/* load r5 with the address */
	addi	r5, r5, LO(romInit)			/* of romInit() */

	lis	r6, HIADJ(ROM_TEXT_ADRS)		/* load r6 with the address */
	addi	r6, r6, LO(ROM_TEXT_ADRS)		/* of ROM_TEXT_ADRS */

	sub	r4, r4, r5
	add	r4, r4, r6

	mtspr	LR, r4					/* save destination address*/
							/* into LR register */
	blr						/* jump to flash mem address */

start:	

	li		r31, 0x00
	lis		r9, HIADJ (ERR_LED_OFF_ADR)		/* new logic */
	addi	r9, r9, LO(ERR_LED_OFF_ADR)
	stb		r31, 0x00(r9)
	
	li		r31, 0x00
	lis		r9, HIADJ (0xf6000001)		/* new logic */
	addi	r9, r9, LO(0xf6000001)
	stb		r31, 0x00(r9)


warm:
	
/*******************************************/
/* digital out chan                        */
/*******************************************/
	
	li		r31, 0xf0
	lis		r9, HIADJ (0xff000972)		
	addi	r9, r9, LO(0xff000972)
	stb		r31, 0x1(r9)
	
	li		r31, 0x0f
	lis		r9, HIADJ (0xff000970)		
	addi	r9, r9, LO(0xff000970)
	stb		r31, 0x1(r9)
	
	li		r31, 0xf2
	lis		r9, HIADJ (0xff000976)		
	addi	r9, r9, LO(0xff000976)
	stb		r31, 0x1(r9)
	
	
/*******************************************/
	
	xor		r0, r0, r0		/* clear out x0 */

	/* set the MSR register to a known state */
	lis		r4, 0x0000			/* enable ME and RI in MSR */
	addi	r4, r4, 0x1002

	mtmsr	r4				/* set MSR */
	mtspr	SRR1, r4			/* set SRR1 the same as MSR */

	/* DER - clear the Debug Enable Register */
	xor		r4, r4, r4		/* clear out r4 */
	mtspr	DER, r4

	/* ICR - clear the Interrupt Cause Register */
	mtspr	ICR, r4

    /* ICTRL - initialize the Intstruction Support Control register */
   	lis 	r5, HIADJ(0x00000007)                                          
   	addi    r5, r5, LO(0x00000007)
   	mtspr   ICTRL, r5
 
 
    /* disable the instruction/data cache */
   	lis 	r4, HIADJ (CACHE_CMD_DISABLE)   	/* load disable cmd */
   	addi    r4, r4, LO (CACHE_CMD_DISABLE)
   	mtspr   IC_CST, r4                  		/* disable I cache */
   	mtspr   DC_CST, r4                  		/* disable D cache */

    /* unlock the instruction/data cache */
   	lis 	r4, HIADJ ( CACHE_CMD_UNLOCK_ALL)   	/* load unlock cmd */
   	addi    r4, r4, LO (CACHE_CMD_UNLOCK_ALL)
   	mtspr   IC_CST, r4         		 	/* unlock all I cache lines */
   	mtspr   DC_CST, r4        		  	/* unlock all D cache lines */
     
    /* invalidate the instruction/data cache */
   	lis 	r4, HIADJ ( CACHE_CMD_INVALIDATE)   	/* load invalidate cmd */
   	addi    r4, r4, LO (CACHE_CMD_INVALIDATE)
   	mtspr   IC_CST, r4      			/* invalidate all I cache lines */
   	mtspr   DC_CST, r4      			/* invalidate all D cache lines */


	/*
	 * initialize the IMMR register before any non-core registers
	 * modification.
	 */
	lis	r4, HIADJ( INTERNAL_MEM_MAP_ADDR)	
	addi	r4, r4, LO(INTERNAL_MEM_MAP_ADDR)
	mtspr	IMMR, r4			/* initialize the IMMR register */

	mfspr	r4, IMMR			/* read it back, to be sure */
	rlwinm  r4, r4, 0, 0, 15	/* only high 16 bits count */


	/* SYPCR - turn off the system protection stuff */

   	lis 	r5, HIADJ(0x00000f80)
   	addi    r5, r5, LO(0x00000f80)
   	stw 	r5, SYPCR(0)(r4)			/* 0xffffff83 : watchdong Enable*/
										/* 0xffffff00 : watchdong disable*/

	/* set the SIUMCR register for important debug port, etc... stuff */

	lwz	r5, SIUMCR(0)(r4)		/* 0x00632000 */
	lis	r6, HIADJ(0x00632000)
	addi	r6, r6, LO(0x00632000)
	or	r5, r5, r6
	stw	r5, SIUMCR(0)(r4)		/* 0x00632000 */

	/* set PIT status and control inti value */
	li		r5, PISCR_PS | PISCR_PITF
	sth		r5, PISCR(0)(r4)

	/* set the SPLL frequency */
	lis	r5, HIADJ ((SPLL_MUL_FACTOR<<PLPRCR_MF_SHIFT) | PLPRCR_TEXPS)
	addi	r5, r5, LO((SPLL_MUL_FACTOR<<PLPRCR_MF_SHIFT) | PLPRCR_TEXPS)
	stw	r5, PLPRCR(0)(r4)


	/**************************************************/
	/* EPROM (CS0) : 0xfff00000 ~ 0xffffffff (1MByte) */
	/**************************************************/	
#ifndef FLASH_BOOT_MODE
	lis	r6, HIADJ (0xfff00401)
	addi	r6, r6, LO(0xfff00401)
#else			/*  == FLASH_BOOT_MODE */
	lis	r6, HIADJ (0xfff00801)
	addi	r6, r6, LO(0xfff00801)
#endif
	stw	r6, BR0(0)(r4)

	lis	r6, HIADJ (0xfff00770)
	addi	r6, r6, LO(0xfff00770)
	stw	r6, OR0(0)(r4)	



	/* FLASH (CS1) */
	lis	r6, HIADJ (0xfa000801)
	addi	r6, r6, LO(0xfa000801)
	stw	r6, BR1(0)(r4)		

	lis	r6, HIADJ (0xff000770)	
	addi	r6, r6, LO(0xff000770)
	stw	r6, OR1(0)(r4)			

	/* SRAM (CS3) */
	lis	r6, HIADJ (0xf1000001)
	addi	r6, r6, LO(0xf1000001)
	stw	r6, BR3(0)(r4)		

	lis	r6, HIADJ (0xffe007f8)	
	addi	r6, r6, LO(0xffe007f8)
	stw	r6, OR3(0)(r4)		


	/* NVRAM (CS4) */
	lis	r6, HIADJ (0xf2000401)
	addi	r6, r6, LO(0xf2000401)
	stw	r6, BR4(0)(r4)		

	lis	r6, HIADJ (0xfff807f0)	
	addi	r6, r6, LO(0xfff807f0)
	stw	r6, OR4(0)(r4)	


	/* VME STANDARD ADDRESS (CS5) */
	lis	r6, HIADJ (0xf0000801)
	addi	r6, r6, LO(0xf0000801)
	stw	r6, BR5(0)(r4)		

	lis	r6, HIADJ (0xff0006f8)
	addi	r6, r6, LO(0xff0006f8)
	stw	r6, OR5(0)(r4)			

	/* VME SHORT ADDRESS (CS6) */

	lis	r6, HIADJ (0xf4000801)
	addi	r6, r6, LO(0xf4000801)
	stw	r6, BR6(0)(r4)			

	lis	r6, HIADJ (0xffff06f8)
	addi	r6, r6, LO(0xffff06f8)
	stw	r6, OR6(0)(r4)
	
#if 1
	/* Digital I/O, Encoder, Dip Switch (CS7)*/		
	
	lis	r6, HIADJ (0xf7000401)			/* here <- 501 */
	addi	r6, r6, LO(0xf7000401)
	stw	r6, BR7(0)(r4)			

	lis	r6, HIADJ (0xfff807f0)			/* <- 7f8 :  08.26 */
	addi	r6, r6, LO(0xfff807f0)
	stw	r6, OR7(0)(r4)	
#else

	lis	r6, 0x0000
	ori	r6, r6,0x0000
	stw	r6, BR7(0)(r4)
	stw	r6, OR7(0)(r4)
#endif


upmInit:
	lis	r6, HIADJ( UpmaTableSdram)
	addi	r6, r6, LO(UpmaTableSdram)

	lis	r7, HIADJ( UpmaTableSdramEnd)
	addi	r7, r7, LO(UpmaTableSdramEnd)

	/* init UPMB for memory access */
	sub	r5, r7, r6		/* compute table size */
	srawi	r5, r5, 2		/* in integer size */

	/* convert UpmTable to ROM based addressing */
	lis	r7, HIADJ(romInit)	
	addi	r7, r7, LO(romInit)

	lis	r8, HIADJ(ROM_TEXT_ADRS)
	addi	r8, r8, LO(ROM_TEXT_ADRS)

	sub	r6, r6, r7		/* subtract romInit base address */
	add	r6, r6, r8 		/* add in ROM_TEXT_ADRS address */

	/* Command: OP=Write, UPMB, MAD=0 */
	lis	r9, HIADJ (MCR_OP_WRITE | MCR_UM_UPMA | MCR_MB_CS0) 
	addi r9, r9, LO(MCR_OP_WRITE | MCR_UM_UPMA | MCR_MB_CS0) 
	or	r5, r5, r9


upmWriteLoop:	
	/* write the UPM table in the UPM */

	lwz	r10, 0(r6)		/* get data from table */
	stw	r10, MDR(0)(r4)		/* store the data to MD register */

	stw	r9, MCR(0)(r4)		/* issue command to MCR register */

	addi	r6, r6, 4		/* next entry in the table */
	addi	r9, r9, 1		/* next MAD address */

	cmpw	r9, r5
	blt	upmWriteLoop


	/* SDRAM (CS2) */	/* OR_ACS_DIV4 -> OR_ACS_DIV1 */
	lis	r6, HIADJ (0xfe000800)		/* 32 MB */
	addi	r6, r6, LO(0xfe000800)
	stw	r6, OR2(0)(r4)

	lis	r6, HIADJ (0x00000081)
	addi	r6, r6, LO(0x00000081)
	stw	r6, BR2(0)(r4)	

	li	r5, 0x0800
	sth	r5, MPTPR(0)(r4)

	lis	r5, HIADJ (0x30904111)
	addi	r5, r5, LO(0x30904111)
	stw	r5, MAMR(0)(r4)

    lis     r5, HIADJ (0x80004212)
    addi    r5, r5, LO(0x80004212)
    stw     r5, MCR(0)(r4)	/* 0x80004212 */	

    lis     r5, HIADJ (0x80004830)
    addi    r5, r5, LO(0x80004830)
    stw	    r5, MCR(0)(r4)	/* 0x80004830 */	

    lis     r5, HIADJ (0x88)
    addi    r5, r5, LO(0x88)
    stw     r5, MAR(0)(r4)

    lis     r5, HIADJ (0x80004114)
    addi    r5, r5, LO(0x80004114)
    stw     r5, MCR(0)(r4)	/* 0x80004114 */	

    /* initialize the stack pointer */
	lis	sp, HIADJ (STACK_ADRS)
	addi	sp, sp, LO(STACK_ADRS)

	/***********************************************************************
	** calculate C entry point: routine - entry point + ROM base 
	** routine		= romStart
	** entry point	= romInit		= R7
	** ROM base		= ROM_TEXT_ADRS = R8
	** C entry point: romStart - R7 + R8 
	***********************************************************************/

	/* go to C entry point */
	addi	sp, sp, -FRAMEBASESZ		/* get frame stack */

	lis		r6, HIADJ (romStart)	
   	addi	r6, r6, LO(romStart)		/* load R6 with C entry point */
	
	lis		r7, HIADJ (romInit)
	addi	r7, r7, LO(romInit)

	lis		r8, HIADJ (ROM_TEXT_ADRS)
	addi	r8, r8, LO(ROM_TEXT_ADRS)

	sub		r6, r6, r7		/* routine - entry point */
	add		r6, r6, r8 		/* + ROM base */

	mtlr	r6				/* move C entry point to LR */
	blr						/* jump to the C entry point */



UpmaTableSdram:
   /* read single beat cycle */
        .long   0xefcebc24, 0x1f0dfc04, 0xeeafbc04, 0x01be6c04	/* 0x00 */
        .long   0x1ffddc00, 0xfffffc47, 0xfffffc04, 0xfffffc04	/* 0x04 */

   /* read burst cycle */
        .long   0xefcebc24, 0x1f0dfc04, 0xeeafbc04, 0x10ae6c04	/* 0x08 */
        .long   0xf0affc00, 0xf0affc08, 0xf1affc08, 0xefbeec08	/* 0x0C */
        .long   0x1ffddc47, 0xfffffc04, 0xeffeec04, 0x1ffddc47	/* 0x10 */
        .long   0xefeeac34, 0x1fbd5c35, 0xfffffc04, 0xfffffc04	/* 0x14 */

   /* write single beat cycle */
        .long   0xefcebc24, 0x1f0dfc04, 0xeeafbc00, 0x01be4c04	/* 0x18 */
        .long   0x1ffddc04, 0xfffffc47, 0xfffffc04, 0xfffffc04	/* 0x1C */

   /* write burst cycle */
        .long   0xefcebc24, 0x1f0dfc04, 0xeeafbc00, 0x10af5c00	/* 0x20 */
        .long   0xf0affc00, 0xf0affc00, 0xe1beec04, 0x1ffddc04	/* 0x24 */
        .long   0xfffffc47, 0xfffffc04, 0xfffffc04, 0xfffffc04	/* 0x28 */
        .long   0xfffffc04, 0xfffffc04, 0xfffffc04, 0xfffffc04	/* 0x2C */

   /* periodic(refresh) timer expired */
        .long   0xeffebc84, 0x1ffd7c04, 0xfffffc04, 0xfffffc04	/* 0x30 */
        .long   0xfffffc04, 0xfffffc84, 0xfffffc47, 0xfffffc04	/* 0x34 */
        .long   0xfffffc04, 0xfffffc04, 0xfffffc04, 0xfffffc04	/* 0x38 */

   /* exception */
        .long   0xfffffc47, 0xfffffc04, 0xfffffc04, 0xfffffc04	/* 0x3C */
UpmaTableSdramEnd:

LED_SET:
 	lis	r9,-256
 	ori	r9,r9,2748
 	lis	r11,-256
 	ori	r11,r11,2748
 	lwz	r0,0(r11)
 	rlwinm	r11,r0,0,29,27
 	stw	r11,0(r9)
 	lis	r9,-256
 	ori	r9,r9,2744
 	lis	r11,-256
 	ori	r11,r11,2744
 	lwz	r0,0(r11)
 	ori	r11,r0,8
 	stw	r11,0(r9)
 	lis	r9,-256
 	ori	r9,r9,2754
 	lis	r11,-256
 	ori	r11,r11,2754
 	lhz	r0,0(r11)
 	ori	r11,r0,8
	mr	r0,r11
 	sth	r0,0(r9)
 	lwz	r11,0(r1)
 	lwz	r31,-4(r11)
 	mr	r1,r11
 	blr

LED_ON:
 	lis	r9,-256
 	ori	r9,r9,2756
 	lis	r11,-256
 	ori	r11,r11,2756
 	lwz	r0,0(r11)
 	rlwinm	r11,r0,0,29,27
 	stw	r11,0(r9)
 	lwz	r11,0(r1)
 	lwz	r31,-4(r11)
 	mr	r1,r11
 	blr

LED_OFF:
 	lis	r9,-256
 	ori	r9,r9,2756
 	lis	r11,-256
 	ori	r11,r11,2756
 	lwz	r0,0(r11)
 	ori	r11,r0,8
 	stw	r11,0(r9)
 	lwz	r11,0(r1)
 	lwz	r31,-4(r11)
 	mr	r1,r11
 	blr
