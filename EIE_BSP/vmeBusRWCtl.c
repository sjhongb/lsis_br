#include "vxWorks.h"
#include "stdio.h"

int vmeLongWordAccess(unsigned int *vmeEadrs, unsigned int vmeEsize)
{
	unsigned int i;
	unsigned int size;
	unsigned int *adrs;
	
	adrs = vmeEadrs;
	
	size = vmeEsize/4;
	
	for(i=0; i<size; i++){
		*adrs++ = (UINT) adrs;	
	}
	for(i=0; i<size; i++){	
		if(*vmeEadrs != (UINT) vmeEadrs){
			printf("vme bus Error!! [address = 0x%x][data = 0x%x]\n", (int) vmeEadrs, (int) *vmeEadrs);
			return(ERROR);
		}else{
			vmeEadrs++;
		}
	}
	printf("vmeLongWordAccess Complete!!\n");
	return(OK);
}

int vmeWordAccess(unsigned short *vmeEadrs, unsigned int vmeEsize)
{
	unsigned int i;
	unsigned int size;
	unsigned short *adrs;
	unsigned short wData;
	
	wData=0;
	adrs = vmeEadrs;
	
	size = vmeEsize/2;
	
	for(i=0; i<size; i++){
		*adrs++ = wData++;
		
	}
	
	wData=0;
	for(i=0; i<size; i++){
		if(*vmeEadrs != wData){
			printf("vme bus Error!! [address = 0x%x][data = 0x%x]\n", (int) vmeEadrs, (int) wData);
			return(ERROR);
		}else{
			vmeEadrs++;
			wData++;
		}
	}
	printf("vmeWordAccess Complete!!\n");	
	return(OK);	
}

int vmeByteAccess(unsigned char *vmeEadrs, unsigned int vmeEsize)
{
	unsigned int i;
	unsigned int size;
	unsigned char *adrs;
	unsigned char bData;
	
	bData=0;
	adrs = vmeEadrs;
	
	size = vmeEsize/2;
	
	for(i=0; i<size; i++){
		*adrs++ = bData++;
		
	}
	
	bData=0;
	for(i=0; i<size; i++){
		if(*vmeEadrs != bData){
			printf("vme bus Error!! [address = 0x%x][data = 0x%x]\n", (int) vmeEadrs, (int) bData);
			return(ERROR);
		}else{
			vmeEadrs++;
			bData++;
		}
	}
	printf("vmeByteAccess Complete!!\n");	
	return(OK);	
}

void vmeExtended()
{
	while(1){
		if( vmeLongWordAccess((UINT32 *) 0x30000000,0x800000) == ERROR) break;
		if( vmeWordAccess((UINT16 *) 0x30000000,0x800000) == ERROR) break;
		if( vmeByteAccess((UINT8 *) 0x30000000,0x800000) == ERROR) break;
	}
	
}

void vmeStandard()
{
	while(1){
		if( vmeLongWordAccess((UINT32 *) 0xf0000000,0x800000) == ERROR) break;
		if( vmeWordAccess((UINT16 *) 0xf0000000,0x800000) == ERROR) break;
		if( vmeByteAccess((UINT8 *) 0xf0000000,0x800000) == ERROR) break;	
	}
	
}

void testVME()
{
	unsigned int vmeAccessCnt;
	vmeAccessCnt=1;
	while(1){
		
		if( vmeLongWordAccess((UINT32 *) 0x30000000,0x800000) == ERROR){
			printf("vmeLongWordAccess Read/Write Error!!\n");
			break;
		} 
		if( vmeWordAccess((UINT16 *) 0x30000000,0x800000) == ERROR){
			printf("vmeWordAccess Read/Write Error!!\n");
			break;
		} 
		if( vmeByteAccess((UINT8 *) 0x30000000,0x800000) == ERROR){
			printf("vmeByteAccess Read/Write Error!!\n");
			break;
		}
		printf("[%d]VME Extended Mode Read/Write Test Complete!!\n", vmeAccessCnt);
		 
		if( vmeLongWordAccess((UINT32 *) 0xf0000000,0x800000) == ERROR){
			printf("vmeLongWordAccess Read/Write Error!!\n");
			break;
		} 
		if( vmeWordAccess((UINT16 *) 0xf0000000,0x800000) == ERROR){
			printf("vmeLongWordAccess Read/Write Error!!\n");
			break;
		}
		if( vmeByteAccess((UINT8 *) 0xf0000000,0x800000) == ERROR){
			printf("vmeLongWordAccess Read/Write Error!!\n");
			break;
		}
		printf("[%d]VME Standard Mode Read/Write Test Complete!!\n", vmeAccessCnt);
		vmeAccessCnt++;
	}
	

}
