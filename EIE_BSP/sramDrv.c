#include "vxWorks.h"
#include "stdio.h"

#define SRAM_BASE_ADDR 0xf1000000
#define SRAM_SIZE 0x200000	  	

int sramClearAll()
{	
	int i;
	unsigned char *sramAddr;
	sramAddr = (UINT8) SRAM_BASE_ADDR;
	for(i=0; i<SRAM_SIZE; i++){
		*(unsigned char*)(sramAddr++) = 0xff; 
	}
	sramAddr = (UINT8) SRAM_BASE_ADDR;
	for(i=0; i<SRAM_SIZE; i++){
		if( *(unsigned char*)(sramAddr++) != 0xff){
			printf("SRAM Clear Error!!\n");
			printf("Error Address = 0x%x Data = 0x%x\n", (int) sramAddr, *(unsigned char*)(sramAddr));
			return(ERROR);
		}
	}
	return(OK);
}

int sramLongWordAccess()
{
	int i;
	unsigned int *sramAddr;
	unsigned int sramSize;
	sramSize = SRAM_SIZE/4;
	sramAddr = (UINT *) SRAM_BASE_ADDR;
	for(i=0; i<sramSize; i++){
		*(unsigned int*)(sramAddr) = (UINT) sramAddr;
		sramAddr++; 
	}
	sramAddr = (UINT *) SRAM_BASE_ADDR;
	for(i=0; i<sramSize; i++){
		if( *(unsigned int*)(sramAddr) != (UINT) sramAddr){
			printf("SRAM Clear Error!!\n");
			printf("Error Address = 0x%x Data = 0x%x\n", (int) sramAddr, *(unsigned int*)(sramAddr));
			return(ERROR);
		}else{
			sramAddr++;
		}
	
	}
	return(OK);
}
int sramWordAccess()
{
	int i;
	unsigned short *sramAddr;
	unsigned int sramSize;
	unsigned short sramData;
	sramData=0;
	sramSize = SRAM_SIZE/2;
	sramAddr = (UINT16) SRAM_BASE_ADDR;
	
	for(i=0; i<sramSize; i++){
		*(unsigned short*)(sramAddr) = (UINT16) sramData;
		sramAddr++; 
		sramData++;
	}
	
	sramData=0;
	sramAddr = (UINT16) SRAM_BASE_ADDR;
	for(i=0; i<sramSize; i++){
		if( *(unsigned short*)(sramAddr) != (UINT16) sramData){
			printf("SRAM Clear Error!!\n");
			printf("Error Address = 0x%x Data = 0x%x\n", (int) sramAddr, *(unsigned short*)(sramAddr));
			return(ERROR);
		}else{
			sramAddr++;
			sramData++;
		}
	
	}
	return(OK);
	
}
int sramByteAccess()
{
	int i;
	unsigned char *sramAddr;
	unsigned char bData;
	bData=0;
	sramAddr = (UCHAR) SRAM_BASE_ADDR;
	for(i=0; i<SRAM_SIZE; i++){
		*(unsigned char*)(sramAddr) = bData;
		sramAddr++;
		bData++; 
	}
	bData=0;
	sramAddr = (UCHAR) SRAM_BASE_ADDR;
	for(i=0; i<SRAM_SIZE; i++){
		if( *(unsigned char*)(sramAddr) != bData){
			printf("SRAM Clear Error!!\n");
			printf("Error Address = 0x%x Data = 0x%x\n", (int) sramAddr, *(unsigned char*)(sramAddr));
			return(ERROR);
		}else{
			sramAddr++;
			bData++;
		}
	
	}
	return(OK);	
}


