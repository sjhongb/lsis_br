#if (TRX_TEST == 1)

typedef struct {
	UINT	rxIntCount;
	UINT	txIntCount;
	int		chan;
	int		drv_chan;
	RING_ID bufId; 
	UINT	loopCnt;
	int		here;
	SEM_ID	ccuSemRd;
	UINT	takeCount;
	UINT	giveCount;
} TRX_TASK_INFO;

IMPORT TRX_TASK_INFO	trx_task_buffer[4];
#endif /* TRX_TEST == 1 */
