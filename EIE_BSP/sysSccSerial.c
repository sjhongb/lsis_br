
/* sysSccSerial.c - MPC860 SCC UART BSP serial device initialization */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,03apr01,jmy	created for the SCC device driver of ITS860 Board.
*/
#include <stdio.h>
#include <string.h>
#include <vxWorks.h>
#include <iosLib.h>
#include <semLib.h>
#include <memLib.h>
#include <rngLib.h>
#include <taskLib.h>
#include <vxLib.h>
#include <intLib.h>
#include <errnoLib.h>
#include <iv.h>
#include <sysLib.h>
#include <selectLib.h>
#include <msgQLib.h>
#include <ioLib.h>
#include <time.h>
#include <logLib.h>
#include <tickLib.h>


#include "types.h"
#include "drv/intrCtl/ppc860Intr.h"
#include "drv/multi/ppc860Cpm.h"
#include "./ppc860SccDrv.h"

#include "./ppc860SccDrv.c"

#define	MPC860_REGB_OFFSET	0x2000
#define N_SCC_SIO_CHAN		4

#define SCC1_TBD_BA		0x600
#define SCC1_RBD_BA		SCC1_TBD_BA + 0x10
#define SCC1_TX_BUF		0x700			
#define SCC1_RX_BUF		SCC1_TX_BUF + 0x80
#define SCC1_TBD_NUM		0x01
#define SCC1_RBD_NUM		0x10				
#define SCC1_TX_BUF_SIZE	0x01

#define SCC2_TBD_BA		0x800
#define SCC2_RBD_BA		SCC2_TBD_BA + 0x10
#define SCC2_TX_BUF		0x900
#define SCC2_RX_BUF		SCC2_TX_BUF + 0x80
#define SCC2_TBD_NUM		0x01
#define SCC2_RBD_NUM		0x10				
#define SCC2_TX_BUF_SIZE	0x01

#define SCC3_TBD_BA		0xA00
#define SCC3_RBD_BA		SCC3_TBD_BA + 0x10
#define SCC3_TX_BUF		0xB00
#define SCC3_RX_BUF		SCC3_TX_BUF + 0x80
#define SCC3_TBD_NUM		0x01
#define SCC3_RBD_NUM		0x10				
#define SCC3_TX_BUF_SIZE	0x01

#define SCC4_TBD_BA		0xC00
#define SCC4_RBD_BA		SCC4_TBD_BA + 0x10
#define SCC4_TX_BUF		0xD00
#define SCC4_RX_BUF		SCC4_TX_BUF + 0x80
#define SCC4_TBD_NUM		0x01
#define SCC4_RBD_NUM		0x10				
#define SCC4_TX_BUF_SIZE	0x01

/* device initialization structure */

typedef struct {
	UINT32	sccTbdBa;
	UINT32	sccRbdBa;
	UINT32	sccTxBuf;
	UINT32	sccRxBuf;
	UINT32	sccTbdNum;
	UINT32	sccRbdNum;
	UINT32	sccTxBufSize;
} PPC860SCC_PARMS;

/* local data structures */

static PPC860SCC_PARMS	ppc860SccParms [] = {
	{SCC1_TBD_BA, SCC1_RBD_BA, SCC1_TX_BUF, SCC1_RX_BUF, 
	 SCC1_TBD_NUM, SCC1_RBD_NUM, SCC1_TX_BUF_SIZE},
	{SCC2_TBD_BA, SCC2_RBD_BA, SCC2_TX_BUF, SCC2_RX_BUF, 
	 SCC2_TBD_NUM, SCC2_RBD_NUM, SCC2_TX_BUF_SIZE},
	{SCC3_TBD_BA, SCC3_RBD_BA, SCC3_TX_BUF, SCC3_RX_BUF, 
	 SCC3_TBD_NUM, SCC3_RBD_NUM, SCC3_TX_BUF_SIZE},
	{SCC4_TBD_BA, SCC4_RBD_BA, SCC4_TX_BUF, SCC4_RX_BUF, 
	 SCC4_TBD_NUM, SCC4_RBD_NUM, SCC4_TX_BUF_SIZE}
};

static	struct{
	PPC860SCC_CHAN	*channel;
}PPC860SCC_CHAN_DEV;

/******************************************************************************
*
* sysSccSerialHwInit - initialize the BSP serial devices 
*
* This routine initializes the BSP serial device descriptors and puts the
* devices in a quiesent state.  It is called from sysHwInit() with
* interrupts locked.
*
* Data Parameter Ram layout:
*
*          ----------------------------- DPRAM base (address = IMMR + 0x2000)
*          |                           | 
*          |                           |
*          |===========================|
*          | 8 bytes per descriptor    | SCC1 Tx Buffer Descriptor (0x2400)
*          |===========================|
*          | 16 descriptors @          | SCC1 Rx Buffer Descriptors (0x2410)
*          | 8 bytes per descriptor    |
*          |===========================| end SCC1 Rx BDs
*          | 80 hex bytes allowed      | SCC1 Tx Buffer (0x2500)
*          |===========================|
*          | one receive char/buffer   | SCC1 Rx Buffer (0x2580)
*          |===========================|
*          | 8 bytes per descriptor    | SCC2 Tx Buffer Descriptor (0x2600)
*          |===========================|
*          | 16 descriptors @          | SCC2 Rx Buffer Descriptors (0x2610)
*          | 8 bytes per descriptor    |
*          |===========================| end SCC2 Rx BDs
*          | 80 hex bytes allowed      | SCC2 Tx Buffer (0x2700)
*          |===========================|
*          | one receive char/buffer   | SCC2 Rx Buffer (0x2780)
*          |===========================|
*          | 8 bytes per descriptor    | SCC3 Tx Buffer Descriptor (0x2800)
*          |===========================|
*          | 16 descriptors @          | SCC3 Rx Buffer Descriptors (0x2810)
*          | 8 bytes per descriptor    |
*          |===========================| end SCC3 Rx BDs
*          | 80 hex bytes allowed      | SCC3 Tx Buffer (0x2900)
*          |===========================|
*          | one receive char/buffer   | SCC3 Rx Buffer (0x2980)
*          |===========================|
*          | 8 bytes per descriptor    | SCC4 Tx Buffer Descriptor (0x2A00)
*          |===========================|
*          | 16 descriptors @          | SCC4 Rx Buffer Descriptors (0x2A10)
*          | 8 bytes per descriptor    |
*          |===========================| end SCC4 Rx BDs
*          | 80 hex bytes allowed      | SCC4 Tx Buffer (0x2B00)
*          |===========================|
*          | one receive char/buffer   | SCC4 Rx Buffer (0x2B80)
*          |===========================|
*          |                           |
*          |                           |
*
* RETURN : OK or ERROR.
*/

extern	void 	ppc860SccDevInit (PPC860SCC_CHAN *pChan);
extern	STATUS	ppc860SccDevInstall();

extern	int	SccDrvNum;


void sysSccSerialHwInit (int i)
{
        
	UINT32	regBase = 0;

	PPC860SCC_CHAN_DEV.channel[i].clockRate = CLK6_FREQ;/*7.3728MHz*/

	PPC860SCC_CHAN_DEV.channel[i].regBase = vxImmrGet ();

	regBase = PPC860SCC_CHAN_DEV.channel[i].regBase;

    	/* for External Scc Clock Setting  */
    	*MPC860_PAPAR(regBase) |= 0x2000;
    	*MPC860_PADIR(regBase) &= ~(0x2000);

	PPC860SCC_CHAN_DEV.channel[i].bgrNum = i+1;
	
	PPC860SCC_CHAN_DEV.channel[i].uart.sccNum = i+1;
	
	PPC860SCC_CHAN_DEV.channel[i].uart.txBdNum = ppc860SccParms[i].sccTbdNum;
	
	PPC860SCC_CHAN_DEV.channel[i].uart.rxBdNum = ppc860SccParms[i].sccRbdNum;
	
	PPC860SCC_CHAN_DEV.channel[i].uart.txBdBase = (SCC_BUF *)(MPC860_REGB_OFFSET+ppc860SccParms[i].sccTbdBa);
	
	PPC860SCC_CHAN_DEV.channel[i].uart.rxBdBase = 
		(SCC_BUF *)(MPC860_REGB_OFFSET+ppc860SccParms[i].sccRbdBa);
	
	PPC860SCC_CHAN_DEV.channel[i].uart.txBufBase = 
		(u_char *)(MPC860_DPRAM_BASE(regBase)+ppc860SccParms[i].sccTxBuf);
	
	PPC860SCC_CHAN_DEV.channel[i].uart.rxBufBase = 
		(u_char *)(MPC860_DPRAM_BASE(regBase)+ppc860SccParms[i].sccRxBuf);
	
	PPC860SCC_CHAN_DEV.channel[i].uart.txBufSize = 
		ppc860SccParms[i].sccTxBufSize;

	PPC860SCC_CHAN_DEV.channel[i].uart.pScc = 
		(SCC *) ((UINT32) PPC860_DPR_SCC1(MPC860_DPRAM_BASE (regBase)) + (i * 0x100));
	
	PPC860SCC_CHAN_DEV.channel[i].uart.pSccReg = 
		(SCC_REG *) (((UINT32) (MPC860_GSMR_L1 (regBase)) + (i*0x20)));

	PPC860SCC_CHAN_DEV.channel[i].uart.pSccReg->sccm = 0x00;

	PPC860SCC_CHAN_DEV.channel[i].pBaud = (UINT32 *)(MPC860_BRGC1(regBase) + i);
				
	PPC860SCC_CHAN_DEV.channel[i].channelMode = 0;
	
	/********************************************************/
	/* 				configure the port lines				*/
	/********************************************************/
	if (i == 0) 
	{	
		/* port A pins to enable TXD1, RXD1 */
		*MPC860_PAPAR(regBase) |= 0x0003;	SYNC;
		*MPC860_PADIR(regBase) &= 0xffff;	SYNC;
		*MPC860_PAODR(regBase) &= 0xfffc;	SYNC;
		
		/* port B to enable RTS1 */
		*MPC860_PBPAR(regBase) &= ~PB19;	
		*MPC860_PBDIR(regBase) |= PB19;		
		*MPC860_PBODR(regBase) &= ~PB19;	
	}
	else if (i == 1) 
	{
		/* port A pins to enable TXD2, RXD2 */
		*MPC860_PAPAR(regBase) |= 0x000c;	SYNC;
		*MPC860_PADIR(regBase) &= ~0x000c;	SYNC;
		*MPC860_PAODR(regBase) &= ~0x0008;	SYNC;
		
		/* port B to enable RTS2 */
	    *MPC860_PBPAR(regBase) &= ~PB18;    
		*MPC860_PBDIR(regBase) |= PB18;     
		*MPC860_PBODR(regBase) &= ~PB18;    
	}
	else if (i == 2) 
	{
		/* port D pins to enable TXD3, RXD3 */
		*MPC860_PDPAR(regBase) |= 0x0030;	SYNC;
		*MPC860_PDDIR(regBase) &= ~0x0020;	SYNC;

		/* port D to enable RTS3 */
	    *MPC860_PDPAR(regBase) &= ~PD7;	    
		*MPC860_PDDIR(regBase) |= PD7;		
	}
	else if (i == 3) 
	{
		/* port D pins to enable TXD4, RXD4 */
		*MPC860_PDPAR(regBase) |= 0x00c0;	SYNC;
		*MPC860_PDDIR(regBase) &= ~0x0080;	SYNC;

	    /* port D to enable RTS4 */
		*MPC860_PDPAR(regBase) &= ~PD6;		
		*MPC860_PDDIR(regBase) |= PD6;		
	}

	else	 return;


	ppc860SccDevInit (&(PPC860SCC_CHAN_DEV.channel[i]));
	
	*MPC860_SICR(regBase) &= 0xffc0ffff;	/*SCC3 routing*/
	SYNC;
	*MPC860_SICR(regBase) |= (SICR_R3CS_BRG3 | SICR_T3CS_BRG3);
	SYNC;
		
	*MPC860_SICR(regBase) &= 0xc0ffffff;	/*SCC4 routing*/
	SYNC;
	*MPC860_SICR(regBase) |= (SICR_R4CS_BRG4 | SICR_T4CS_BRG4);
	SYNC;

	*MPC860_SDCR(regBase) = SDCR_RAID_BR5;
}


/***************************************************************************
*
* sysSccSerialHwInit2 - connect BSP serial device interrupts
*
* This routine connects the BSP serial device interrupts. It is called from
* sysHwinit2(). Serial device interrupts could not be connected in 
* sysSccSerialHwInit() because the kernel memory allocator was not initialized
* at that point, and intConnect() calls malloc().
*
* RETURN: N/A
*/

void
sysSccSerialHwInit2 (int i)
{
    
	switch (i) 
	{
		
		case 2:
			if(intConnect(IV_SCC3, (VOIDFUNCPTR)Scc860Int,(int) &PPC860SCC_CHAN_DEV.channel[i]) == ERROR)
			{
				printf("Failed to connect to vector %d\n", (int)IV_SCC3);
				return;
			}
			break;

		case 3:
			if(intConnect(IV_SCC4, (VOIDFUNCPTR)Scc860Int,(int) &PPC860SCC_CHAN_DEV.channel[i]) == ERROR)
			{
				printf("Failed to connect to vector %d\n", (int)IV_SCC4);
				return;
			}
			break;

		default:
			return;
	}

	if(ppc860SccDevInstall() == ERROR)	return;
}

/******************************************************************************
*
* sysSccSerialChanGet _ get the SIO_CHAN device associated with a serial channel
*
* This routine gets the SIO_CHAN device associated with a specified serial 
* channel.

* RETURN: A pointer to the SIO_CHAN structure for the channel, or ERROR
* if the channel is invalid.
*/

SIO_CHAN *
sysSccSerialChanGet (int channel)
{
	if (channel >= N_SCC_SIO_CHAN)		return ((SIO_CHAN *) ERROR);

	return ((SIO_CHAN *) &PPC860SCC_CHAN_DEV.channel[channel]);
}

/*******************************************************************************
*
* SccSerialInit - initialize SCC Serial IO systems.
*
*/
void
SccSerialInit (void)
{
	int i;
	char 	SccName [20];
	
	if( (PPC860SCC_CHAN_DEV.channel = (PPC860SCC_CHAN *)calloc(N_SCC_SIO_CHAN, sizeof(PPC860SCC_CHAN)))== NULL)
	{
		printf("channel structure malloc failed\n");
		return;
	}
	
	if (N_SCC_SIO_CHAN > 0) 
	{
		for (i=2; i<N_SCC_SIO_CHAN; i++) 
		{

			sysSccSerialHwInit(i);
			sysSccSerialHwInit2(i);
			
			if(vxTas(&(PPC860SCC_CHAN_DEV.channel[i].uart.bufcreated)) == FALSE)
			{
				printf("SIO Driver Not Installed\n");
				return;
			}
			
			PPC860SCC_CHAN_DEV.channel[i].uart.txBufferSize = DEFAULT_TX_BUFF_SIZE;
			PPC860SCC_CHAN_DEV.channel[i].uart.rxBufferSize = DEFAULT_RX_BUFF_SIZE;
			
			if((PPC860SCC_CHAN_DEV.channel[i].uart.wrtBuf = rngCreate(PPC860SCC_CHAN_DEV.channel[i].uart.txBufferSize)) == NULL)
			{
				PPC860SCC_CHAN_DEV.channel[i].uart.bufcreated = FALSE;
				printf("ScctyCoDevCreate: Cannot allocate buffers\n");
				return;
			} /* if */
			
			if((PPC860SCC_CHAN_DEV.channel[i].uart.rdBuf = rngCreate(PPC860SCC_CHAN_DEV.channel[i].uart.rxBufferSize)) == NULL)
			{
				rngDelete(PPC860SCC_CHAN_DEV.channel[i].uart.wrtBuf);
				PPC860SCC_CHAN_DEV.channel[i].uart.bufcreated = FALSE;
				printf("ScctyCoDevCreate: Cannot allocate buffers\n");
			    return;
			} /* if */
		
			PPC860SCC_CHAN_DEV.channel[i].uart.mutexSemWr	= semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
			PPC860SCC_CHAN_DEV.channel[i].uart.mutexSemRd	= semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
			
			PPC860SCC_CHAN_DEV.channel[i].uart.binSemWr	= semBCreate(SEM_Q_FIFO, SEM_EMPTY);
			PPC860SCC_CHAN_DEV.channel[i].uart.binSemRd	= semBCreate(SEM_Q_FIFO, SEM_EMPTY);

			PPC860SCC_CHAN_DEV.channel[i].uart.num_chan	= i;
			
			selWakeupListInit(&PPC860SCC_CHAN_DEV.channel[i].uart.selWakeupList);
	
/* Created by gpande (2011/7/12 - 16:55:49) (Start) */
/*
LS산전 철도에서 사용하는 serial channel들이 모두 LKV402에 맞춰져 있기 때문에
LKV402에 맞게 ScctyCo serial driver의 숫자를 1씩 감소시킴
*/
#if 0	
			sprintf (SccName, "%s%d", "/ScctyCo/", i);
#else
			sprintf (SccName, "%s%d", "/ScctyCo/", i - 1);
#endif
/* Created by gpande (2011/7/12 - 16:55:53) (End) */
	
			/* Add the device to the vxWorks IO system */
			if(iosDevAdd(&(PPC860SCC_CHAN_DEV.channel[i].devHdr), SccName, SccDrvNum) != OK)
			{
				PPC860SCC_CHAN_DEV.channel[i].uart.bufcreated = FALSE;
				printf("XR16L788DevCreate: error adding %s to I/O system.\n", SccName);
				return;
			}/* if */
		}
	}
}
/* end of file */

/************************************************
*				RTS Signal On/Off				*
************************************************/
STATUS	sysRTSCon(int num_chan, int flag)
{
	int immrVal;

	immrVal = vxImmrGet();

	switch(num_chan)
	{
		case	0 :
			/* port B to enable RTS1 */
			*MPC860_PBPAR(immrVal) &= ~PB19; 
			*MPC860_PBDIR(immrVal) |= PB19;
			*MPC860_PBODR(immrVal) &= ~PB19;

			if(flag == RTS_OFF)
			{
				*PBDAT(immrVal)	|= PB19;
			}
			else
			{
				*PBDAT(immrVal)	&= ~PB19;
			}
			break;

		case	1 :
			/* port B to enable RTS2 */
			*MPC860_PBPAR(immrVal) &= ~PB18; 
			*MPC860_PBDIR(immrVal) |= PB18;
			*MPC860_PBODR(immrVal) &= ~PB18;

			if(flag == RTS_OFF)
			{
				*PBDAT(immrVal)	|= PB18;
			}
			else
			{
				*PBDAT(immrVal)	&= ~PB18;
			}
			break;
			
		case	2 :
		    /* port B to enable RTS3 */
			*MPC860_PDPAR(immrVal) &= ~PD7; 	
			*MPC860_PDDIR(immrVal) |= PD7;    

			if(flag == RTS_OFF)
			{
				*PDDAT(immrVal)	|= PD7;
			}
			else
			{
				*PDDAT(immrVal)	&= ~PD7;
			}
			break;

		case	3 :
		    /* port B to enable RTS2 */
			*MPC860_PDPAR(immrVal) &= ~PD6; 	
			*MPC860_PDDIR(immrVal) |= PD6;    

			if(flag == RTS_OFF)
			{
				*PDDAT(immrVal)	|= PD6;
			}
			else
			{
				*PDDAT(immrVal)	&= ~PD6;
			}
			break;

		default	:
			break;
	}
	return OK;
}


/************************************************
*				DTR Signal On/Off				*
************************************************/
STATUS	sysDTRCon(int num_chan, int flag)
{
	int immrVal;

	immrVal = vxImmrGet();

	switch(num_chan)
	{
		case	0 :
		    /* port B to enable DTR1 */
			*MPC860_PBPAR(immrVal) &= ~PB31; 
			*MPC860_PBDIR(immrVal) |= PB31;
			*MPC860_PBODR(immrVal) &= ~PB31;
			
			if(flag == DTR_OFF)
			{
				*PBDAT(immrVal)	|= PB31;
			}
			else
			{
				*PBDAT(immrVal)	&= ~PB31;
			}
			break;

		case	1 :
		    /* port B to enable DTR2 */
			*MPC860_PBPAR(immrVal) &= ~PB30; 
			*MPC860_PBDIR(immrVal) |= PB30;
			*MPC860_PBODR(immrVal) &= ~PB30;

			if(flag == DTR_OFF)
			{
				*PBDAT(immrVal)	|= PB30;
			}
			else
			{
				*PBDAT(immrVal)	&= ~PB30;
			}
			break;

		case	2 :
		    /* port B to enable DTR3 */
			*MPC860_PBPAR(immrVal) &= ~PB17; 
			*MPC860_PBDIR(immrVal) |= PB17;
			*MPC860_PBODR(immrVal) &= ~PB17;

			if(flag == DTR_OFF)
			{
				*PBDAT(immrVal)	|= PB17;
			}
			else
			{
				*PBDAT(immrVal)	&= ~PB17;
			}
			break;

		case	3 :
		    /* port B to enable DTR4 */
			*MPC860_PBPAR(immrVal) &= ~PB16; 
			*MPC860_PBDIR(immrVal) |= PB16;
			*MPC860_PBODR(immrVal) &= ~PB16;

			if(flag == DTR_OFF)
			{
				*PBDAT(immrVal)	|= PB16;

			}
			else
			{
				*PBDAT(immrVal)	&= ~PB16;

			}
			break;
		default	:
			break;
	}
	return OK;
}
