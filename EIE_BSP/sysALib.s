/* sysALib.s - HanA Rustle Router system-dependent assembly routines */

/* Copyright 1984-1998 HanA Systems, Inc. */
/* Copyright 1997,1998 Motorola, Inc., All Rights Reserved */
        .data
	.globl	copyright_wind_river
	.long	copyright_wind_river


/*
DESCRIPTION
This module contains system-dependent routines written in assembly
language.

This module must be the first specified in the \f3ld\f1 command used to
build the system.  The sysInit() routine is the system start-up code.
*/

#define _ASMLANGUAGE
#include "vxWorks.h"
#include "asm.h"
#include "cacheLib.h"
#include "config.h"
#include "regs.h"	
#include "sysLib.h"

	/* globals */

	.globl	_sysInit		/* start of system code */

	/* externals */

	.extern usrInit
	
	.text

/*******************************************************************************
*
* sysInit - start after boot
*
* This is the system start-up entry point for VxWorks in RAM, the
* first code executed after booting.  It disables interrupts, sets up
* the stack, and jumps to the C routine usrInit() in usrConfig.c.
*
* The initial stack is set to grow down from the address of sysInit().  This
* stack is used only by usrInit() and is never used again.  Memory for the
* stack must be accounted for when determining the system load address.
*
* NOTE: This routine should not be called by the user.
*
* RETURNS: N/A
*
* sysInit (void)              /@ THIS IS NOT A CALLABLE ROUTINE @/
*
*/

_sysInit:

	/*
	 * disable external interrupts and Instruction/Data MMU, set
	 * the exception prefix 
	 */

    mfmsr   p0                      /* p0 = msr    */
    INT_MASK(p0, p1)                /* mask EE bit */
	rlwinm	p1, p1, 0, _PPC_MSR_BIT_DR + 1, _PPC_MSR_BIT_IR - 1
	rlwinm  p1, p1, 0, _PPC_MSR_BIT_IP + 1, _PPC_MSR_BIT_IP - 1
    mtmsr   p1                      /* msr = p1, update msr */
	isync				/* ISYNC */

	/* disable instruction and data caches */

	lis	p1, HIADJ ( CACHE_CMD_DISABLE)		/* load disable cmd */
	addi	p1, p1, LO (CACHE_CMD_DISABLE)
	mtspr	IC_CST, p1				/* Disable I cache */
	mtspr	DC_CST, p1				/* Disable D cache */

	/* unlock instruction and data caches */

	lis     p1, HIADJ ( CACHE_CMD_UNLOCK_ALL)	/* load unlock cmd */
	addi	p1, p1, LO (CACHE_CMD_UNLOCK_ALL)
    mtspr   IC_CST, p1				/* Unlock I cache */
    mtspr   DC_CST, p1				/* Unlock D cache */

	/* invalidate instruction and data caches */

    lis     p1, HIADJ ( CACHE_CMD_INVALIDATE)	/* load invalidate cmd*/
	addi	p1, p1, LO (CACHE_CMD_INVALIDATE)
    mtspr   IC_CST, p1				/* Invalidate I cache */
    mtspr   DC_CST, p1				/* Invalidate D cache */

	/* invalidate entries within both TLBs */

	tlbia

	/* initialize Small Data Area (SDA) start address */

/*
	lis     r2, HIADJ( _SDA2_BASE_)
	addi    r2, r2, LO(_SDA2_BASE_)

	lis     r13, HIADJ ( _SDA_BASE_)
	addi    r13, r13, LO(_SDA_BASE_)
*/


	/* initialize the stack pointer */
	
	lis     sp, HIADJ( RAM_LOW_ADRS)
	addi    sp, sp, LO(RAM_LOW_ADRS)

	/* set the default boot code */
	
	lis	r3, HIADJ( BOOT_WARM_AUTOBOOT)
	addi	r3, r3, LO(BOOT_WARM_AUTOBOOT)

	/* jump to usrInit */

	addi	sp, sp, -FRAMEBASESZ	/* get frame stack */
	b	usrInit			/* never returns - starts up kernel */

	.globl	sysInByte
sysInByte:
	eieio			/* Sync I/O Operation */
	sync
	lbzx	p0,r0,p0	/* Read byte from I/O space */
	bclr	20,0		/* Return to caller */

#if 0
#ifdef INCLUDE_CACHE_SUPPORT
#include "cache800LockALib.s"
#endif	/* INCLUDE_CACHE_SUPPORT */
#endif
