#if !defined(AFX_BITSETDLG_H__996F7BCF_E644_4B0A_A64A_0EFCBD29F321__INCLUDED_)
#define AFX_BITSETDLG_H__996F7BCF_E644_4B0A_A64A_0EFCBD29F321__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBitSetDlg dialog

class CBitSetDlg : public CDialog
{
// Construction
public:
	CBitSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBitSetDlg)
	enum { IDD = IDD_DLG_BITSET };
	int		m_iStartBit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBitSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITSETDLG_H__996F7BCF_E644_4B0A_A64A_0EFCBD29F321__INCLUDED_)
