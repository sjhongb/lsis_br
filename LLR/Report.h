#if !defined(AFX_REPORT_H__896F1D41_2E1C_11D4_8CD7_0040440144E3__INCLUDED_)
#define AFX_REPORT_H__896F1D41_2E1C_11D4_8CD7_0040440144E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Report.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CReport DAO recordset

class CReport : public CDaoRecordset
{
public:
	CReport(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CReport)

// Field/Param Data
	//{{AFX_FIELD(CReport, CDaoRecordset)
	CString	m_Station;
	CString	m_StartDate;
	CString	m_EndDate;
	CString	m_SearchType;
	CString	m_User;
	CString	m_Type;
	CString	m_Date;
	CString	m_Time;
	CString	m_Msg;
	CString	m_Contents;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReport)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORT_H__896F1D41_2E1C_11D4_8CD7_0040440144E3__INCLUDED_)
