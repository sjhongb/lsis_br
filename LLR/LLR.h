// LLR.h : main header file for the LLR application
//

#if !defined(AFX_LLR_H__794AEAC8_34ED_4B67_A759_9AAFA190F09C__INCLUDED_)
#define AFX_LLR_H__794AEAC8_34ED_4B67_A759_9AAFA190F09C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"
#include "recordmask.h"
#include "../include/EipEmu.h"
#include "../include/UserDataType.h"
#include "../include/MessageInfo.h"
#include "../include/FMSGList.h"

/////////////////////////////////////////////////////////////////////////////
// typedef struct {
// 	CString strStationName;
// 	int iNumberOfEPK;
// 	int iNumberOfLC;
// 	CString strBlokState[17];
// } VerifyObjectType;
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CLLRApp:
// See LLR.cpp for the implementation of this class
//

class CLLRApp : public CWinApp
{

public:
	BOOL m_bLeftSideIsUpDirection;
	BOOL m_bIsSuperUser;
	BOOL OnLogInit();
	void OnSystemValueInit();
	void CreateObj();
	CLLRApp();
	int nMaxButton;
	int nMaxSwitch;
	int nMaxSignal;
	int nMaxTrack;
	int nVerRecordSize;
	int nComRecordSize;
	int nSfmRecordSize;
	int m_iCallonCnt;
	int m_iRouteReleaseCnt;
	int m_iConCnt;
	char m_cOperator[8];
	char m_WorkPath[256];
	char m_LOGDirInfo[256];	
	int  m_iFDIM_Err_Backup[2];
	int  m_iFDOM_Err_Backup[2];
	CString m_VoiceErr;	
	BOOL m_bCommDisplay;
	BOOL m_bCommOk;
	BOOL m_bCommSuccess;
	int  m_iCommError; // 0 : 이상없음  1 : CRC Error 2: 0x05 
	int  m_iConType;
	// User 관련 변수
	int m_nLimitCount;
	CString m_UserSection;
	CString m_PassSection;
	CString m_Entry;
	CPtrArray m_Items;
	VerifyObjectType m_VerifyObjectForMessage;

	//EipEmu
	CEipEmu *m_pEipEmu;
	//Title Name
	CString psz_MainTitle;
	void LoadFromINI();
	SystemParameterType m_SystemValue;
	UserDataType		m_UserValue;
	void ParameterSave();
	void SystemValueLoad();
	void ParameterLoade();
	void ParameterReset();
	long GetSecond(CTime time);
	void OnLoadUserID();
	void OnSaveUserID();
	CString GetProgramVersionOfIPM();
	CString GetProgramVersionOfVDU();
	void GetVerifyObject();
	void SetFlagOfSyncWithLSM();
	CString GetFlagOfSyncWithLSM();
	BYTE SystemVMEM[ MAX_MSGBLOCK_SIZE ];	
	//Station.ini
	CString pszProfileName;
	BOOL m_bBlockTrackIng;		//궤도차단  명령중이면 TRUE 그렇지 않으면 FALSE..
	CMessageInfo	*Info_pScrMessageInfo;
#ifndef   _NOMASKINFO_
	RecordMaskType	m_RecordMaskInfo;
#endif // _NOMASKINFO_
	CFMSGList* m_pEIPMsgList;
	CString	m_strEventMessage;
	int		m_iMsgChangeFlag;
	MessageEvent* m_pChangeEvent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLLRApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CLLRApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LLR_H__794AEAC8_34ED_4B67_A759_9AAFA190F09C__INCLUDED_)
