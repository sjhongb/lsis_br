// LLRView.h : interface of the CLLRView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LLRVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_)
#define AFX_LLRVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//{{AFX_INCLUDES()
#include "LSM.h"
//}}AFX_INCLUDES
#include "stdafx.h"
#include "resource.h"
#include "LLR.h"
#include "LLRDoc.h"
#include "Sysinitdlg.h"

class CLLRView : public CFormView
{
protected: // create from serialization only
	CLLRView();
	DECLARE_DYNCREATE(CLLRView)

public:
	char    m_CmdQue[24];
	CLLRApp* pApp;
	CLLRDoc* GetDocument();
	int m_iSysType;
	int m_iClientCnt;
	//for XP Style 
	//사용자 관리 다이얼로그 

	//{{AFX_DATA(CLLRView)
 	enum { IDD = IDD_LLR_FORM };
	CLSM	m_Screen;
	//}}AFX_DATA

// Attributes
public:
	void ReleaseLogFile();
	void LogReplayStop(BOOL Condition = TRUE);
	void TimerRunOn();
	void TimerRunOff();
	void SetPlayTime();
	void SetLogFile(CString strLogFile);
	void LogReplayFrontRun(BOOL Condition = TRUE);
	void LogReplaySelectRun (BOOL Condition = TRUE);
	void LogReplaySetTime ();
	void LogReplaySpeedSet (int iSpeed = 60);
	void LogSetFrameSkip (int iSpeed = 1);
	
// Operations
public:
	BOOL m_bSelOn;
	BOOL m_bPlay;
	BOOL m_TimeDo;
	UINT m_nTimerID;
	char m_strSend[2048];
	void UserIDUpdate(char Buffer[2048]);
	void OnDisconnect();
//	void OnConnect();			//LAN통신 사용안함.
//	void OnSend( int nOP);		//LAN통신 사용안함.
//	void StartTimer();			//LAN통신 사용안함.
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLLRView)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLLRView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void OnReplayRun(int nVector);

// Generated message map functions
protected:
	//{{AFX_MSG(CLLRView)
	afx_msg void OnMenuReplayStop();
	afx_msg void OnMenuReplayFrontRun();
	afx_msg void OnMenuSelReplayTime();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnClientClose(WPARAM wParam, LPARAM lClient);	// Client 소켓 해제
	afx_msg LRESULT OnClientReceive(WPARAM wParam, LPARAM lClient);	// Client로 부터 수신되는 데이터
	afx_msg LRESULT OnClientAccept(WPARAM wParam, LPARAM lClient);	// Client로 부터 수신되는 데이터
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	afx_msg void OnOleSendMessageLSMctrl(long message, long wParam, long lParam);
	afx_msg void OnOperateLSMctrl(short nFunction, short nArgument);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LLRView.cpp
inline CLLRDoc* CLLRView::GetDocument()
   { return (CLLRDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LLRVIEW_H__B1B5D51A_AA90_49A8_9BD4_B8768CF5233D__INCLUDED_)
