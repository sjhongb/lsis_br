// Report.cpp : implementation file
//

#include "stdafx.h"
#include "Report.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReport

IMPLEMENT_DYNAMIC(CReport, CDaoRecordset)

CReport::CReport(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CReport)
	m_Station = _T("");
	m_StartDate =_T("");
	m_EndDate = _T("");
	m_SearchType = _T("");
	m_User = _T("");
	m_Type = _T("");
	m_Date = _T("");
	m_Time = _T("");
	m_Msg = _T("");
	m_Contents = _T("");
	m_nFields = 10;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CReport::GetDefaultDBName()
{
	return _T("REPORT.MDB");
//	return _T("C:\\EISIII PACKAGE\\REPORT.MDB");
}

CString CReport::GetDefaultSQL()
{
	return _T("[PrintData]");
}

void CReport::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CReport)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[Station]"), m_Station);
	DFX_Text(pFX, _T("[StartDate]"), m_StartDate);
	DFX_Text(pFX, _T("[EndDate]"), m_EndDate);
	DFX_Text(pFX, _T("[SearchType]"), m_SearchType);
	DFX_Text(pFX, _T("[User]"), m_User);
	DFX_Text(pFX, _T("[Type]"), m_Type);
	DFX_Text(pFX, _T("[Date]"), m_Date);
	DFX_Text(pFX, _T("[Time]"), m_Time);
	DFX_Text(pFX, _T("[Msg]"), m_Msg);
	DFX_Text(pFX, _T("[Contents]"), m_Contents);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CReport diagnostics

#ifdef _DEBUG
void CReport::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CReport::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
