//=======================================================
//==              MainFrm.cpp
//=======================================================
//  파 일 명 :  MainFrm.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LLR 의 메인 프레임 을 생성 한다. 
//
//=======================================================

#include "stdafx.h"
#include "LLR.h"
#include "MainFrm.h"
#include "LLRView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int _nOldAlter = 0;
extern int _iLCCNo;

//=======================================================
//
//  함 수 명 :  GetOSVersionType
//  함수출력 :  int 
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  OS 의 버전을 리턴한다.
//
//=======================================================
static int WINAPI GetOSVersionType() 
{
	UINT nOSVersion;
	OSVERSIONINFOEX osvi;
	BOOL bOsVersionInfoEx;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	
	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
	{
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) return -1;
	}
	
	switch (osvi.dwPlatformId)
	{
	case VER_PLATFORM_WIN32_NT:
		if ( osvi.dwMajorVersion <= 4 )
			nOSVersion = 4;
		else if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
			nOSVersion = 5;
		else if( bOsVersionInfoEx )  
			nOSVersion = 6;
		break;
	case VER_PLATFORM_WIN32_WINDOWS:
		if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
			nOSVersion = 1;
		else if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
			nOSVersion = 2;
		else if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
			nOSVersion = 3;
		break;
	}
	
	return nOSVersion; 
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_COMMAND(ID_EXIT, OnExit)
	ON_WM_ACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//=======================================================
//
//  함 수 명 :  CMainFrame()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CMainFrame::CMainFrame()
{
	pApp = (CLLRApp*)AfxGetApp();
	m_iReplaySpeed = 248;
	m_bIsLOGView = FALSE;
// 	m_bCloseWindow			= TRUE;
// 	pApp					= (CLLRApp*) AfxGetApp();
// 	m_bSoundOpen			= FALSE;
// 	m_bSystemRunOn			= FALSE;
// 	m_bSystemRunOff			= FALSE;
// 	m_bExitProgram			= FALSE;
// 	m_bLogWindowDisable		= FALSE;
// 	m_bOperateDisable		= FALSE;
// 	m_bIsCreateLOGView		= FALSE;
// 	m_bIsCreateMenuView		= FALSE;
// 	m_iMenuViewExitCk		= 1;
// 	m_iLogViewExitCk		= 1;
// 	m_bDlgDisplaed			= FALSE;
// 	m_iConsoleRunCounter	= 10;
// 	m_iCountForLogInDlg		= 0;
// 	m_iCountForSendGPStime	= 0;
// 
// 	m_CommStatus.pVar[0]	= 0x00;
// 	m_CurrentSound			= 0x00;
// 	m_OldSound				= 0x00;
// 
// 	m_pMenuRoute[0].pRMenu	= NULL;
// 	m_pMenuRoute[1].pRMenu	= NULL;
// 	m_pMenuRoute[2].pRMenu	= NULL;
// 	m_pMenuSwitch.pIMenu	= NULL;
// 	m_pMenuTrack.pIMenu		= NULL;
// 
// 	m_pMenuRoute[0].nMax	= 0;
// 	m_pMenuRoute[1].nMax	= 0;
// 	m_pMenuRoute[2].nMax	= 0;
// 	m_pMenuSwitch.nMax		= 0;
// 	m_pMenuTrack.nMax		= 0;
// 
// 	m_hmodHookTool			= NULL;
// 	m_hSubWinMap			= NULL;
// 	m_pCmdXBuffer			= NULL;
// 	m_pWinXBuffer			= NULL;
// 	m_iMessageReceiveCnt	= MAX_MSGCNT+100;
// 
// 	m_iResetTimer = 0;
}

//=======================================================
//
//  함 수 명 :  ~CMainFrame()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CMainFrame::~CMainFrame()
{
}

//=======================================================
//
//  함 수 명 :  OnCreate
//  함수출력 :  int
//  함수입력 :  LPCREATESTRUCT lpCreateStruct
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  초기 생성시 프레임 을 설정 한다.
//
//=======================================================
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon;; 

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1) return -1;
	//Get Current  Time
	m_PCTime = CJTime::GetCurrentTime();

	//Set Window Maintitle.
	SetWindowText( pApp->psz_MainTitle );	// set title string to the caption bar.
	
	//Set Icon of application instead of default icon.
	hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	
	SetIcon(hIcon, FALSE);		// Set small icon
	
	//PosiotionFix();

	SetTimer(1100,10000,NULL); // 10 초후 후킹을 시작한다. ( 후킹 시작시 에러가 너무 많이 발생함 안전을 위해서 10초뒤 후킹 시작 )

//	if(StartUp() == FALSE)
//	{
//		::MessageBox(GetSafeHwnd(), "HookTool.Dll or DLL internal function loading Failed!", "DLL loading failed", MB_OK);
//		MoveWindow(0, 0, 0, 0);
//		PostQuitMessage(0);
//	}

//	m_bStartHook = TRUE;
//	m_nOSVersionType = GetOSVersionType();
//	m_pfnInstallHook(this->m_hWnd, m_nOSVersionType);

//	m_pfnSetHookOption( 1000 , TRUE , 1);	
	m_iStartBit = 0;
	return 0;
}

//=======================================================
//
//  함 수 명 :  PreCreateWindow
//  함수출력 :  BOOL
//  함수입력 :  CREATESTRUCT& cs
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 생성전 전처리 함수 
//
//=======================================================
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) ) return FALSE;

	if ( pApp->m_SystemValue.bTestMode != TRUE ) cs.dwExStyle = WS_EX_TOPMOST;
	cs.style = WS_BORDER | WS_VISIBLE | WS_POPUP;
    cs.x = pApp->m_SystemValue.nMainStartX;
	cs.y = pApp->m_SystemValue.nMainStartY;
	cs.cx = pApp->m_SystemValue.nMainSizeX;
	cs.cy = pApp->m_SystemValue.nMainSizeY;

	if( cs.hMenu ) 
	{
		DestroyMenu( cs.hMenu );
		cs.hMenu = NULL;
	}
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

//=======================================================
//
//  함 수 명 :  PosiotionFix()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LLR 의 윈도우 위치를 고정한다.
//
//=======================================================
void CMainFrame::PosiotionFix()
{
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left   = pApp->m_SystemValue.nMainStartX;	
	iWinPlacement.rcNormalPosition.top    = pApp->m_SystemValue.nMainStartY;	
	iWinPlacement.rcNormalPosition.right  = pApp->m_SystemValue.nMainSizeX;	
	iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMainSizeY;	
	SetWindowPlacement(&iWinPlacement);	
}

//=======================================================
//
//  함 수 명 :  OnSize
//  함수출력 :  없음
//  함수입력 :  UINT nType, int cx, int cy
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 사이즈 변화시 처리 함수 
//
//=======================================================
void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
}

//=======================================================
//
//  함 수 명 :  OnMove
//  함수출력 :  없음
//  함수입력 :  int x, int y
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  화면 이동시 처리함수 
//
//=======================================================
void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);
}

//=======================================================
//
//  함 수 명 :  OnDestroy()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  종료시 처리를 한다.
//
//=======================================================
void CMainFrame::OnDestroy() 
{
	OnAllStopSound();
	m_bExitProgram = TRUE;
	m_bSystemRunOff = TRUE;
	
	CFrameWnd::OnDestroy();
	
// 	if ( m_pMenuRoute[0].pRMenu ) 
// 	{
// 		delete [] m_pMenuRoute[0].pRMenu;
// 		m_pMenuRoute[0].pRMenu = NULL;
// 	}
// 
// 	if ( m_pMenuRoute[1].pRMenu ) 
// 	{
// 		delete [] m_pMenuRoute[1].pRMenu;
// 		m_pMenuRoute[1].pRMenu = NULL;
// 	}
// 
// 	if ( m_pMenuRoute[2].pRMenu ) 
// 	{
// 		delete [] m_pMenuRoute[2].pRMenu;
// 		m_pMenuRoute[2].pRMenu = NULL;
// 	}
// 	
// 	if ( m_pMenuSwitch.pIMenu ) 
// 	{
// 		delete [] m_pMenuSwitch.pIMenu;
// 		m_pMenuSwitch.pIMenu = NULL;
// 	}
// 	
// 	if ( m_pMenuTrack.pIMenu ) 
// 	{
// 		delete [] m_pMenuTrack.pIMenu;
// 		m_pMenuTrack.pIMenu = NULL;
// 	}
// 	
// 	m_pMenuRoute[0].nMax  = 0;
// 	m_pMenuRoute[1].nMax  = 0;
// 	m_pMenuRoute[2].nMax  = 0;
// 	m_pMenuSwitch.nMax	  = 0;
// 	m_pMenuTrack.nMax	  = 0;

	pApp->m_RecordMaskInfo.Destroy();

	m_pfnUnInstallHook();	

	if (m_hmodHookTool) ::FreeLibrary( m_hmodHookTool );

	if ( m_pCmdXBuffer ) // 시뮬레이션 프로그램 처리
	{		
		UnmapViewOfFile( m_pCmdXBuffer );
	}

	if ( pApp->m_SystemValue.bTestMode != TRUE ) 
	{
/*		HANDLE hToken;
		TOKEN_PRIVILEGES tkp;
		if ( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
		{        
			LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
			tkp.PrivilegeCount = 1;
			tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
			AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);
			::ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, NULL);
		}*/
	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::ShowLockMessage()
{
	AfxMessageBox("Warning !\nNow, Control Locking Status. Make Unlock,Please!",MB_OK,MB_ICONSTOP);
}

//=======================================================
//
//  함 수 명 :  OnSetVMem
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  로깅의 제왕 5 !!
//              로깅... 그 머나먼 여정의 시작점...
//              태초에 하나님이 로깅을 만드셨나니... 
//              한번들어가면 다시나오지 못하는 그곳으로의 여정을...
//              사람들은 로깅어드벤처라 불렀다....
//
//=======================================================
void CMainFrame::OnSetVMem(BYTE *pVMem, int length)
{
	CString strRcvData,strTemp;

	BOOL bReplayFirstCK;
	BYTE *pSetBuf;
	CString strUserIDTemp;
	CString strUserPass;
	CString strLogDate;

	bReplayFirstCK = FALSE;
	pSetBuf = pVMem;
	length += sizeof(CJTime);
	DataHeaderType* pHead = (DataHeaderType*)pSetBuf;

//	CJTime *destT = (CJTime*)&pSetBuf[pApp->nSfmRecordSize + LCC_COM_SIZE + IPM_VERSION_SIZE];		// 2->연동장치에서 받을때는 2BYTE 더받음.
	strLogDate.Format(" 2%03d/%02d/%02d %02d:%02d:%02d ",pHead->nYear,pHead->nMonth,pHead->nDate,pHead->nHour,pHead->nMin,pHead->nSec);

	CJTime playTime((int)pHead->nYear + 2000, (int)pHead->nMonth, (int)pHead->nDate, (int)pHead->nHour, (int)pHead->nMin, (int)pHead->nSec);


//	m_ControlPannelDlg.LogDateDisplay( strLogDate );
	m_ControlDlg.UpdatePlayTime(playTime);

	memcpy(pApp->SystemVMEM, pSetBuf, length);

	m_EventDlg.RestoreEventRecords(playTime);
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	if(m_bCloseWindow)
	{
		CFrameWnd::OnClose();
	}
	else
	{
		m_bCloseWindow = TRUE;
		return;
	}
}

//=======================================================
//
//  함 수 명 :  OnCommand
//  함수출력 :  BOOL
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  외부로부터 메세지를 받았을때의 처리
//
//=======================================================
BOOL CMainFrame::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	UINT commandID = (UINT)wParam;
	
	switch (commandID) 
	{
	case ID_COMM_CMD_WRITE:	
		*((BYTE*)lParam) = 0x00;
		return TRUE;
	default:
		break;
	}
	
	return CFrameWnd::OnCommand(wParam, lParam);
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
BOOL CMainFrame::SystemTimeBCDToDecimal(DataHeaderType *R)
{
	union 
	{
		BYTE d;
		struct 
		{
			BYTE d_l : 4;
			BYTE d_h : 4;
		};
	};

	BOOL bFailTimer = FALSE;
	d = R->nYear;
	R->nYear  = d_h * 10;
	R->nYear += d_l;
	d = R->nMonth;
	R->nMonth  = d_h * 10;
	R->nMonth += d_l;
	d = R->nDate;
	R->nDate  = d_h * 10;
	R->nDate += d_l;
	d = R->nHour;
	R->nHour  = d_h * 10;
	R->nHour += d_l;
	d = R->nMin;
	R->nMin  = d_h * 10;
	R->nMin += d_l;
	d = R->nSec;
	R->nSec  = d_h * 10;
	R->nSec += d_l;

	if ((R->nMonth < 1) || (R->nMonth >12)) 
		bFailTimer = TRUE;
	if ((R->nDate < 1) || (R->nDate >31)) 
		bFailTimer = TRUE;
	if (R->nHour > 23) 
		bFailTimer = TRUE;
	if (R->nMin > 59) 
		bFailTimer = TRUE;
	if (R->nSec > 59) 
		bFailTimer = TRUE;
	
	return bFailTimer;
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
int CMainFrame::CreateObj()
{
	CLLRView* pView = (CLLRView*) GetActiveView();
	int nRet = 0;

	//CString m_strmem;
	//m_strmem = _T("SharedMessage"); 
// 	HWND m_hwnd;
// 	m_hwnd = ::FindWindow(NULL, "LLR - Observer" );
// 	if ( m_hwnd == NULL ) 
// 	{
// 		WinExec("Observer.exe", SW_NORMAL);
// 	}

	CLLRApp *pApp = (CLLRApp *)AfxGetApp();

	// 로그 윈도우 생성
	m_bIsCreateLOGView = m_EventDlg.Create(IDD_DIALOG_EVENT, NULL);

	m_bIsLOGView = TRUE;

	// 로그 화면 생성
//	m_LOGView.m_pParentWnd = this;
// 	if ( m_bIsCreateLOGView ) 
// 	{
// 		WinExec("LogWnd.exe", SW_NORMAL);
//		m_LOGView.ShowWindow(SW_SHOW);
//	}

// 	// 사운드 초기화
// 	OnSoundLoad();
// 	OnPlaySound();

	return nRet;
}

//=======================================================
//
//  함 수 명 :  OnMenuLockUpdate()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메뉴를 활성화/비활성화 한다.
//
//=======================================================
void CMainFrame::OnMenuLockUpdate()
{

// 		if ( m_LOGView ) {
// 			m_LOGView.m_btnFind.EnableWindow(FALSE);
// 			m_LOGView.m_btnLast.EnableWindow(FALSE);
// 			m_LOGView.m_btnLogset.EnableWindow(FALSE);
// 			m_LOGView.m_btnNext.EnableWindow(FALSE);
// 			m_LOGView.m_btnPrev.EnableWindow(FALSE);
// 			m_LOGView.m_btnScroll.EnableWindow(FALSE);
// 		}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::GetLogPath(CJTime &destT, CString &strFileName, int nMode)
{
	strFileName = (const char *)&pApp->m_LOGDirInfo[0];
	if ( nMode < 0 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%Y");
	if ( nMode == 2 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%m");
	if ( nMode == 1 ) return;

	strFileName += '\\';
	strFileName += destT.Format("%d.LOG");

	return;
}

void CMainFrame::OnMessagePrint( BYTE *pData, int nPort ) 
{
// 	char cReceiveMessage[11];
// 	char cTemp[11];
// 
// 	CString strReceiveMessage;
// 
// 	memcpy ( &cReceiveMessage,pData,11);
// 	memcpy ( &cTemp[0],&cReceiveMessage[1],10);
// 	cTemp[10] = '\0';
// 
// 	strReceiveMessage = cTemp;
// 
// 	// 같은 번호의 메세지가 두번 이상 오면 버린다.
// 	if ( m_iMessageReceiveCnt == cReceiveMessage[0] ) {
// 		return;
// 	}
// 
// 	m_iMessageReceiveCnt = cReceiveMessage[0];
// 	if ( m_iMessageReceiveCnt < MAX_MSGCNT-2  ) {
// 		m_bPrint = FALSE;
// 	}
// 
// 	// 혹시 메세지를 놓칠 경우에 대비해서 MAX_MSGCNT-1 부터 MAX_MSGCNT까지 두번 확인한다.
// 	if ( m_iMessageReceiveCnt >= MAX_MSGCNT-1 && m_bPrint == FALSE) {
// 		m_LOGView.m_strMessage = m_strReceiveMessage;
// 		m_LOGView.MessagePrint();
// 		m_iMessageReceiveCnt = MAX_MSGCNT+100;
// 		m_bPrint = TRUE;
// 	} else {
// 		m_strReceiveMessage = m_strReceiveMessage + strReceiveMessage;
// 	}
}

//=======================================================
//
//  함 수 명 :
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
void CMainFrame::OnExit() 
{
	// TODO: Add your command handler code here
	PostMessage(WM_CLOSE);
}

//=======================================================
//
//  함 수 명 :  OnSoundLoad
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드 파일을 로딩한다.
//
//=======================================================
BOOL CMainFrame::OnSoundLoad()
{
	m_PlaySound[0].Create(IDR_WAVE1);		//접근
	m_PlaySound[1].Create(IDR_WAVE2);		//신호기 전철기
	m_PlaySound[2].Create(IDR_WAVE3);		//파워
	m_PlaySound[3].Create(IDR_WAVE4);		//폐색
	m_PlaySound[4].Create(IDR_WAVE5);		//폐색
	m_PlaySound[5].Create(IDR_WAVE6);		//폐색
	m_PlaySound[6].Create(IDR_WAVE7);		//폐색
	m_PlaySound[7].Create(IDR_WAVE8);		//폐색

	return TRUE;
}


//=======================================================
//
//  함 수 명 :  OnPlaySound
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드를 발생혹은 정지한다.
//
//=======================================================
void CMainFrame::OnPlaySound()
{
	if(m_CurrentSound &BIT_SOUND_BLOCKADE5){		//폐색 5
		m_PlaySound[7].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[7].Stop();
	}
	if(m_CurrentSound &BIT_SOUND_BLOCKADE4){		//폐색 4
		m_PlaySound[6].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[6].Stop();
	}
	if(m_CurrentSound &BIT_SOUND_BLOCKADE3){		//폐색 3
		m_PlaySound[5].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[5].Stop();
	}
	if(m_CurrentSound &BIT_SOUND_BLOCKADE2){		//폐색 2
		m_PlaySound[4].Play(0,TRUE);
	}
 	else
 	{
 		m_PlaySound[4].Stop();
 	}
	if(m_CurrentSound &BIT_SOUND_BLOCKADE1){		//폐색1 
		m_PlaySound[3].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[3].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_POWER){		//파워
		m_PlaySound[2].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[2].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_SP){			//신호기 및 전철기
		m_PlaySound[1].Play(0,TRUE);
	}
	else
	{
		m_PlaySound[1].Stop();
	}
	if(m_CurrentSound & BIT_SOUND_APPROACH){	//접근
		m_PlaySound[0].Play(0,TRUE);
//		m_PlaySound[0].Play(0,TRUE); // 2007년 6월 22일 바이패스역 알람 막음
	}
	else
	{
		m_PlaySound[0].Stop();
	}
}

//=======================================================
//
//  함 수 명 :  OnCheckSoundState
//  함수출력 :  없음 
//  함수입력 :  BYTE* pData
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드 정보가 달라진것이 있으면 사운드를 발생한다.
//
//=======================================================
void CMainFrame::OnCheckSoundState(BYTE* pData)
{
		BYTE nSoundAlter = 0x00;
		DataHeaderType* pHead = (DataHeaderType*)pData;
		SystemStatusType *pSysInfo = (SystemStatusType *)&(pHead->nSysVar);
		UnitStatusType *pUniInfo = (UnitStatusType *)&(pHead->UnitState);
		AlarmStatusType *AlarmInfo = (AlarmStatusType *)&(pHead->AlarmStatus);

		if ( AlarmInfo->bSound1 ) nSoundAlter |= BIT_SOUND_APPROACH;	//0x01
		if ( AlarmInfo->bSound2 ) nSoundAlter |= BIT_SOUND_SP;		//0x02	
		if ( AlarmInfo->bSound3 ) nSoundAlter |= BIT_SOUND_POWER;	//0x04
		if ( AlarmInfo->bSound4 ) nSoundAlter |= BIT_SOUND_BLOCKADE1;	//0x08
// 		if ( AlarmInfo->bSound5 ) nSoundAlter |= BIT_SOUND_BLOCKADE2;	//0x10
// 		if ( AlarmInfo->bSound6 ) nSoundAlter |= BIT_SOUND_BLOCKADE3;	//0x20
// 		if ( AlarmInfo->bSound7 ) nSoundAlter |= BIT_SOUND_BLOCKADE4;	//0x40
// 		if ( AlarmInfo->bSound8 ) nSoundAlter |= BIT_SOUND_BLOCKADE5;	//0x80
		if ( m_CurrentSound != nSoundAlter) {
			m_CurrentSound = nSoundAlter;
		}

		if(pApp->m_SystemValue.bSound){
			if(m_OldSound ^ m_CurrentSound){	//사운드 정보가 달라진것이 있으면
				OnPlaySound();					//사운드 출력하고 정보 업데이트 
				m_OldSound = m_CurrentSound;
			}
		}
}

//=======================================================
//
//  함 수 명 :  OnAllStopSound()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  사운드를 멈춘다.
//
//=======================================================
void CMainFrame::OnAllStopSound()
{
		m_PlaySound[7].Stop();
		m_PlaySound[6].Stop();
		m_PlaySound[5].Stop();
		m_PlaySound[4].Stop();
		m_PlaySound[3].Stop();
		m_PlaySound[2].Stop();
		m_PlaySound[1].Stop();
		m_PlaySound[0].Stop();
}

//=======================================================
//
//  함 수 명 :  StartUp
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  마우스의 후킹을 시작한다.
//
//=======================================================
BOOL CMainFrame::StartUp()
{
	if (NULL == m_hmodHookTool)
	{
		m_hmodHookTool = ::LoadLibrary( "HookTool.Dll" );
		Sleep (500);
		if(m_hmodHookTool == NULL)
			return FALSE;
		
		m_pfnInstallHook = (PFN_InstallHook)::GetProcAddress(m_hmodHookTool, "InstallHook");
		if(m_pfnInstallHook == NULL)
			return FALSE;
		m_pfnUnInstallHook = (PFN_UnInstallHook)::GetProcAddress(m_hmodHookTool, "UnInstallHook");
		if(m_pfnUnInstallHook == NULL)
			return FALSE;
		m_pfnSetHookOption = (PFN_SetHookOption)::GetProcAddress(m_hmodHookTool, "SetHookOption");
		if(m_pfnSetHookOption == NULL)	
			return FALSE;
		m_pfnGetHookOption = (PFN_GetHookOption)::GetProcAddress(m_hmodHookTool, "GetHookOption");
		if(m_pfnGetHookOption == NULL)
			return FALSE;
		m_pfnGetResultString = (PFN_GetResultString)::GetProcAddress(m_hmodHookTool, "GetResultString");
		if(m_pfnGetResultString == NULL)
			return FALSE;
		m_pfnGetIniFilePath	= (PFN_GetIniFilePath)::GetProcAddress(m_hmodHookTool, "GetIniFilePath");
		if(m_pfnGetIniFilePath == NULL)
			return FALSE;
		m_pfnSetEnabledRestart = (PFN_SetEnabledRestart)::GetProcAddress(m_hmodHookTool, "SetEnabledRestart");
		if(m_pfnSetEnabledRestart == NULL)
			return FALSE;
	}
	
	return TRUE;
}

//=======================================================
//
//  함 수 명 :  OnGet777
//  함수출력 :  없음
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  마우스 이동시의 좌표를 받아서 처리한다.
//
//=======================================================
void CMainFrame::OnGet777(WPARAM wParam, LPARAM lParam)
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();

	CRect Logrect;
	CRect Menurect;

	if ( pApp->m_SystemValue.nSystemType == 2 ) {
		if ( m_bDlgDisplaed == FALSE && pView->m_Screen.GetMenuStatus() == FALSE) {
//			pView->SetFocus();
		}
		return;	
 	}
// 	if ( !m_LOGView ) return;
// 
// 	int iViewCheck = 0;
// 
// 	m_LOGView.GetWindowRect(&Logrect);
// 	
// 	KillTimer (300);
// 	KillTimer (400);
// 
// 	if ( ( (int)wParam > Logrect.left && (int)wParam < Logrect.right  )  && 
// 		( (int)lParam > Logrect.top  && (int)lParam < Logrect.bottom )  ) {
// 
// 		if ( m_LOGView.m_iHideCk == 3 ) {
// 			m_LOGView.SetTransOn(0);
// 		}
// 		m_LOGView.m_iHideCk = 0;
// 		m_LOGView.m_iTrans = 255;
// 		m_LOGView.SetTrans(255);
// 		m_iLogViewExitCk = 1;
// 		iViewCheck = 1;
// 		return;
// 	} else {
// 		if ( m_iLogViewExitCk == 1 ) {
// 			m_iLogViewExitCk = 0;
// 			SetTimer ( 200 , 5000 , NULL );
// 		}
// 	}

// 	if ( pApp->m_SystemValue.nSystemType == 0 ) {
// 		if ( ( (int)wParam > Menurect.left && (int)wParam < Menurect.right  )  && 
// 			( (int)lParam > Menurect.top  && (int)lParam < Menurect.bottom )  ) {
// 
// 			if ( m_MenuView.m_iHideCk == 3 ) {
// 				m_MenuView.SetTransOn(0);
// 			}
// 			m_MenuView.m_iHideCk = 0;
// 			m_MenuView.m_iTrans = 255;
// 			m_MenuView.SetTrans(255);
// 			m_iMenuViewExitCk = 1;
// 			iViewCheck = 1;
// 			if ( m_bDlgDisplaed == FALSE && m_MenuView.m_btnSystem.GetMenuStatus()==FALSE && m_MenuView.m_btnUser.GetMenuStatus()==FALSE && m_MenuView.m_btnControl.GetMenuStatus()==FALSE && pView->m_Screen.GetMenuStatus() == FALSE) {
// //				m_MenuView.SetFocus();
// 			}
// 			return;
// 		} else {
// 			if ( m_iMenuViewExitCk == 1 ) {
// 				m_iMenuViewExitCk = 0;
// 				SetTimer ( 100 , 5000 , NULL );
// 			}
// 		}
// 	}

}

//=======================================================
//
//  함 수 명 :  OnGet775
//  함수출력 :  없음
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  마우스가 멈춰있은후 일정시간이 지난후 발생하는 
//              메세지를 받아서 처리한다.
//
//=======================================================
void CMainFrame::OnGet775(WPARAM wParam, LPARAM lParam)
{
	if ( pApp->m_SystemValue.bAnyMenu == FALSE ) return;

	
// 	if ( pApp->m_SystemValue.nSystemType == 0 ) {
// 		if ( m_iMenuViewExitCk == 2 && m_MenuView.m_iHideCk == 0) {
// 			m_MenuView.SetTransOff(0);	
// 		}
// 	}
// 	if ( m_iLogViewExitCk == 2 && m_LOGView.m_iHideCk == 0) {
// 		m_LOGView.SetTransOff(0);	
// 	}
}

void CMainFrame::SetLSMFocus() 
{
}

void CMainFrame::OnLLRExit ()
{
	PostMessage(WM_CLOSE);
}


int CMainFrame::GetTrackID(LPCSTR lpszName) // 2006년 6월 22일 아카우라 오픈 : 바이패스역 알람1 계속 발생 때문에 함수 추가 
{
	if ( pApp->m_pEipEmu == NULL) return -1;
	int id = 0;
	POSITION pos;
	for(pos = pApp->m_pEipEmu->m_strTrack.GetHeadPosition(); pos != NULL; ) {
		CString *str = (CString*)pApp->m_pEipEmu->m_strTrack.GetNext(pos);
		if (*str == lpszName) return id;
		else id++;
	}
	return -1;
}

void CMainFrame::ReleaseLogFile()
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	pView->ReleaseLogFile();
	pView->m_bSelOn = FALSE;
}

void CMainFrame::LogSelectRun (BOOL bCondition )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	
	if ( bCondition == TRUE ) {
		pView->LogReplaySelectRun(TRUE);
	} else {
		pView->LogReplaySelectRun(TRUE);
	}
}

void CMainFrame::LogPlayStop(BOOL bCondition)
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	CString strLogDate;

	if( bCondition == TRUE )
	{
		pView->LogReplayStop(TRUE);
		memset(CommVMemData, 0, MAX_MSGBLOCK_SIZE);
		DataHeaderType* pHead = (DataHeaderType*)CommVMemData;
		pView->m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, (long)(CommVMemData), (long)(&CommVMemData));
		strLogDate.Format(" 2%03d/%02d/%02d %02d:%02d:%02d ", pHead->nYear, pHead->nMonth, pHead->nDate, pHead->nHour, pHead->nMin, pHead->nSec);
//		m_ControlPannelDlg.LogDateDisplay(strLogDate);
	}
	else
	{
		pView->LogReplayStop (TRUE);
		memset (CommVMemData ,0, MAX_MSGBLOCK_SIZE);
		DataHeaderType* pHead = (DataHeaderType*)CommVMemData;
		pView->m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, (long)(CommVMemData), (long)(&CommVMemData) );
		strLogDate.Format(" 2%03d/%02d/%02d %02d:%02d:%02d ",pHead->nYear,pHead->nMonth,pHead->nDate,pHead->nHour,pHead->nMin,pHead->nSec);
//		m_ControlPannelDlg.LogDateDisplay( strLogDate );
	}
}

void CMainFrame::LogPlayPause (BOOL bCondition )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	
	if ( bCondition == TRUE ) {
		pView->LogReplayStop (TRUE);
	} else {
		pView->LogReplayStop (TRUE);
	}
}

void CMainFrame::LogPlaySpeedSet (int iSpeed )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	
	pView->LogReplaySpeedSet (iSpeed);
}

void CMainFrame::LogSetFrameSkip (int iSpeed )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	pView->LogSetFrameSkip (iSpeed);
}

void CMainFrame::SetPlayTime ()
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	pView->SetPlayTime();
}

void CMainFrame::LogFrontRun (BOOL bCondition )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	
	if ( bCondition == TRUE ) {
		pView->LogReplayFrontRun(TRUE);
	} else {
		pView->LogReplayFrontRun(TRUE);
	}
}

void CMainFrame::UpdateLog()
{
	OnSetVMem( &CommVMemData[0], pApp->nComRecordSize+MAX_OPERATE_CMD_SIZE);
}

void CMainFrame::ShowControlPannel (BOOL bShow )
{
	if ( bShow == TRUE ) {
//		m_ControlPannelDlg.ShowWindow(SW_SHOW);
	} else {
//		m_ControlPannelDlg.ShowWindow(SW_HIDE);
	}
}

void CMainFrame::SetLogFile (CString strLogFile )
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	pView->SetLogFile(strLogFile);
}

void CMainFrame::LogPlaySetTime ()
{
	CLLRView* pView;
	pView = (CLLRView*)GetActiveView();
	pView->LogReplaySetTime();
}

void CMainFrame::ShowLogView (BOOL bShow )
{
	if (m_bIsLOGView)
	{
//		m_LOGView.ShowWindow(SW_HIDE);
//		m_bIsLOGView = FALSE;
	}
	else
	{
//		m_LOGView.ShowWindow(SW_SHOW);
//		m_bIsLOGView = TRUE;
	}
}
