// ControlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "ControlDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MIN_SIZEY		200

/////////////////////////////////////////////////////////////////////////////
// CControlDlg dialog


CControlDlg::CControlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CControlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CControlDlg)
	//}}AFX_DATA_INIT

	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	m_strLOGDir = (const char *)&pApp->m_LOGDirInfo[0];
	m_bIsPlay = FALSE;
	m_bIsMinimized = FALSE;
}


void CControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CControlDlg)
	DDX_Control(pDX, IDC_SLIDER_FRAMESKIP, m_ctrlFrameSkipSlider);
	DDX_Control(pDX, IDC_BUTTON_FORWARD, m_ctrlForwardBtn);
	DDX_Control(pDX, IDC_BUTTON_REWIND, m_ctrlRewindBtn);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_ctrlStopBtn);
	DDX_Control(pDX, IDC_BUTTON_PLAY, m_ctrlPlayBtn);
	DDX_Control(pDX, IDC_BUTTON_FILE, m_ctrlFileBtn);
	DDX_Control(pDX, IDC_BUTTON_LOAD, m_ctrlLoadBtn);
	DDX_Control(pDX, IDC_BUTTON_TITLE, m_ctrlTitleBtn);
	DDX_Control(pDX, IDC_SLIDER_SPEED, m_ctrlSpeedSlider);
	DDX_Control(pDX, IDC_SLIDER_TIME, m_ctrlTimeSlider);
	DDX_Control(pDX, IDC_DATETIME_DATE, m_ctrlDateDT);
	DDX_Control(pDX, IDC_DATETIME_TIME, m_ctrlTimeDT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CControlDlg, CDialog)
	//{{AFX_MSG_MAP(CControlDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_PLAY, OnButtonPlay)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	ON_NOTIFY(NM_KILLFOCUS, IDC_DATETIME_TIME, OnKillfocusDatetimeTime)
	ON_NOTIFY(NM_SETFOCUS, IDC_DATETIME_TIME, OnSetfocusDatetimeTime)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SPEED, OnReleasedcaptureSliderSpeed)
	ON_BN_CLICKED(IDC_BUTTON_TITLE, OnButtonTitle)
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_BITMAPSLIDER_MOVED, OnBitmapSliderMoved)
	ON_MESSAGE(WM_BITMAPSLIDER_MOVING, OnBitmapSliderMoving)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlDlg message handlers

BOOL CControlDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetButtonTooltip();
	SetPosition();

	LOGFONT lf;
	CFont Font;

	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = 16;
	strcpy(lf.lfFaceName, "Verdana");
	
	m_pDateFont = new CFont;
	if(m_pDateFont->CreateFontIndirect(&lf))
	{
		m_ctrlDateDT.SetFont(m_pDateFont);
		m_ctrlTimeDT.SetFont(m_pDateFont);
	}
	else
	{
		delete m_pDateFont;
		m_pDateFont = NULL;
	}

	m_ctrlDateDT.SetFormat("yyyy-MM-dd");
	m_ctrlTimeDT.SetFormat("HH:mm:ss");

	// Initialize DateTimePicker
	m_curDateTime = CJTime::GetCurrentTime();
// 	m_curDateTime.nYear = 15;
// 	m_curDateTime.nMonth = 10;
// 	m_curDateTime.nDate = 27;
// 	m_curDateTime.nHour	= 0;
// 	m_curDateTime.nMin	= 0;
// 	m_curDateTime.nSec	= 0;

	UpdateDate(FALSE);
	UpdateTime(FALSE);
	

	m_ctrlTimeSlider.SetBitmapChannel( IDB_MP_CHANNEL, IDB_MP_CHANNEL_ACTIVE, TRUE );
	m_ctrlTimeSlider.SetBitmapThumb( IDB_MP_THUMB, IDB_MP_THUMB_ACTIVE, TRUE );
	m_ctrlTimeSlider.DrawFocusRect( FALSE );
	m_ctrlTimeSlider.SetMargin( 2, 3, 2, 0 );
	m_ctrlTimeSlider.SetRange(0, 86399);

	m_ctrlSpeedSlider.SetBitmapChannel( IDB_BITMAP_SPEEDBAR, NULL, TRUE );
	m_ctrlSpeedSlider.SetBitmapThumb( IDB_MP_THUMB, IDB_MP_THUMB_ACTIVE, TRUE );
	m_ctrlSpeedSlider.DrawFocusRect( FALSE );
	m_ctrlSpeedSlider.SetMargin( 2, 3, 2, 0 );
	m_ctrlSpeedSlider.SetRange(1, 6);
	m_ctrlSpeedSlider.SetPos(2);
	m_iReplaySpeed = 248;

	m_ctrlFrameSkipSlider.SetBitmapChannel( IDB_BITMAP_FRAMESKIPBAR, NULL, TRUE );
	m_ctrlFrameSkipSlider.SetBitmapThumb( IDB_MP_THUMB, IDB_MP_THUMB_ACTIVE, TRUE );
	m_ctrlFrameSkipSlider.DrawFocusRect( FALSE );
	m_ctrlFrameSkipSlider.SetMargin( 2, 3, 2, 0 );
	m_ctrlFrameSkipSlider.SetPos(0);
	m_ctrlFrameSkipSlider.SetRange(0,8);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CControlDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_pDateFont != NULL)
	{
		delete m_pDateFont;
	}
}

void CControlDlg::UpdateDate(BOOL bSaveAndValidate)
{
	SYSTEMTIME date;
	
	memset(&date, 0, sizeof(date));
	
	if(bSaveAndValidate)
	{
		m_ctrlDateDT.GetTime(&date);

		m_curDateTime.SetYear(date.wYear);
		m_curDateTime.nMonth = (BYTE)date.wMonth;
		m_curDateTime.nDate = (BYTE)date.wDay;
	}
	else
	{
		date.wYear		= m_curDateTime.GetYear();
		date.wMonth		= m_curDateTime.nMonth;
		date.wDay		= m_curDateTime.nDate;

		m_ctrlDateDT.SetTime(&date);
	}
}

void CControlDlg::UpdateTime(BOOL bSaveAndValidate)
{
	SYSTEMTIME time;
	
	memset(&time, 0, sizeof(time));
	
	if(bSaveAndValidate)
	{
		m_ctrlTimeDT.GetTime(&time);
		
		m_curDateTime.nHour = (BYTE)time.wHour;
		m_curDateTime.nMin = (BYTE)time.wMinute;
		m_curDateTime.nSec = (BYTE)time.wSecond;
	}
	else
	{ 
		time.wYear		= m_curDateTime.GetYear();
		time.wMonth		= m_curDateTime.nMonth;
		time.wDay		= m_curDateTime.nDate;
		time.wHour		= m_curDateTime.nHour;
		time.wMinute	= m_curDateTime.nMin;
		time.wSecond	= m_curDateTime.nSec;
		
		m_ctrlTimeDT.SetTime(&time);
	}
}

void CControlDlg::OnButtonLoad() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	UpdateDate(TRUE);
	UpdateTime(TRUE);
	
	CString strDir = m_strLOGDir + m_curDateTime.Format("\\%Y\\%m");
	CString strLogPath = strDir + m_curDateTime.Format("\\%d.LOG");
	CString strEventPath = strDir + m_curDateTime.Format("\\%d.EVT");
	
	HFILE			hLogFile;
	LOGHeaderType	logHead;
	LOGIndexType	logIndex;
	
	BYTE nData[500];
	hLogFile = _lopen(strLogPath, OF_READ);
	_lread(hLogFile, &logIndex, SIZE_LOGINDEX);
	_lread(hLogFile, &logHead, 4);
	_lread(hLogFile, &nData, (short)logHead.Length + sizeof(CJTime));
	
	m_startDateTime.nYear	= nData[9];
	m_startDateTime.nMonth	= nData[8];
	m_startDateTime.nDate	= nData[7];
	m_startDateTime.nHour	= nData[6];
	m_startDateTime.nMin	= nData[5];
	m_startDateTime.nSec	= nData[4];

	long pos = _llseek(hLogFile, -((short)logHead.Length + sizeof(CJTime)), SEEK_END);

	if(pos != -1L)
	{
		_lread(hLogFile, &nData, (short)logHead.Length + sizeof(CJTime));

		m_endDateTime.nYear		= nData[9];
		m_endDateTime.nMonth	= nData[8];
		m_endDateTime.nDate		= nData[7];
		m_endDateTime.nHour		= nData[6];
		m_endDateTime.nMin		= nData[5];
		m_endDateTime.nSec		= nData[4];
	}
	_lclose(hLogFile);

	int gap = m_endDateTime.GetSecInDay() - m_startDateTime.GetSecInDay();
	
	m_ctrlTimeSlider.SetRange(0, gap);

	SetDlgItemText(IDC_STATIC_START, m_startDateTime.Format("%H:%M:%S"));
	SetDlgItemText(IDC_STATIC_END, m_endDateTime.Format("%H:%M:%S"));

	m_curDateTime = m_startDateTime;

	pWnd->LogPlayStop(TRUE);
	pWnd->ReleaseLogFile();
	
	UpdateTime(FALSE);
	m_ctrlTimeSlider.SetPos(0);
	
	pWnd->SetLogFile(strLogPath);
	pWnd->m_EventDlg.RefreshFilePath(strEventPath);
	
	pWnd->m_ReplayTime = m_curDateTime;
	pWnd->SetPlayTime();	
	pWnd->LogPlaySetTime();

//	pWnd->LogFrontRun(TRUE);
	pWnd->LogSelectRun(TRUE);
	pWnd->LogPlayPause(TRUE);
}

void CControlDlg::OnButtonPlay() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	UpdateDate(TRUE);
	UpdateTime(TRUE);

	if(m_bIsPlay)
	{
		pWnd->LogPlayPause();
		m_bIsPlay = FALSE;
		m_ctrlPlayBtn.SetBitmaps(IDB_BITMAP_PLAY, RGB(0, 0, 0));
	}
	else
	{
		pWnd->m_ReplayTime = m_curDateTime;
		pWnd->SetPlayTime();
		pWnd->LogPlaySetTime();
		
		//pWnd->LogFrontRun(TRUE);
		pWnd->LogSelectRun(TRUE);
		m_bIsPlay = TRUE;
		m_ctrlPlayBtn.SetBitmaps(IDB_BITMAP_PAUSE, RGB(0, 0, 0));
	}
}

void CControlDlg::OnButtonStop() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	pWnd->LogPlayStop(TRUE);
	m_bIsPlay = FALSE;
	m_ctrlPlayBtn.SetBitmaps(IDB_BITMAP_PLAY, RGB(0, 0, 0));
}

int CControlDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	
	// TODO: Add your specialized creation code here
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
	iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMenuStartY;
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
	iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
	SetWindowPlacement(&iWinPlacement);	
	
	return 0;
}


void CControlDlg::UpdatePlayTime(CJTime &playTime)
{
	if(GetSafeHwnd() == NULL)
	{
		return;
	}

	SYSTEMTIME time;
	memset(&time, 0, sizeof(time));

	m_curDateTime.nHour = playTime.nHour;
	m_curDateTime.nMin = playTime.nMin;
	m_curDateTime.nSec = playTime.nSec;

	time.wYear		= m_curDateTime.GetYear();
	time.wMonth		= m_curDateTime.nMonth;
	time.wDay		= m_curDateTime.nDate;
	time.wHour		= m_curDateTime.nHour;
	time.wMinute	= m_curDateTime.nMin;
	time.wSecond	= m_curDateTime.nSec;
		
	m_ctrlTimeDT.SetTime(&time);

	int nNewPos = m_curDateTime.GetSecInDay() - m_startDateTime.GetSecInDay();
	m_ctrlTimeSlider.SetPos(nNewPos);
}

LRESULT CControlDlg::OnBitmapSliderMoved(WPARAM wParam, LPARAM lParam)
{
	if(wParam == IDC_SLIDER_TIME)
	{
		CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
		
		int nNewSec = m_ctrlTimeSlider.GetPos() + m_startDateTime.GetSecInDay();
		
		m_curDateTime.nSec	= nNewSec % 60;
		m_curDateTime.nMin	= (nNewSec / 60) % 60;
		m_curDateTime.nHour	= nNewSec / 3600;
		
		UpdateTime(FALSE);

		pWnd->m_ReplayTime = m_curDateTime;
		pWnd->SetPlayTime();	
		pWnd->LogPlaySetTime();

		if(m_bIsPlay)
		{
			pWnd->LogSelectRun(TRUE);
		}
	}

	return 0;
}

LRESULT CControlDlg::OnBitmapSliderMoving(WPARAM wParam, LPARAM lParam)
{
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	switch(wParam)
	{
	case IDC_SLIDER_TIME:
	{
		CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
		pWnd->LogPlayPause(TRUE);

		int nNewSec = m_ctrlTimeSlider.GetPos() + m_startDateTime.GetSecInDay();
		
		m_curDateTime.nSec	= nNewSec % 60;
		m_curDateTime.nMin	= (nNewSec / 60) % 60;
		m_curDateTime.nHour	= nNewSec / 3600;
		
		UpdateTime(FALSE);
		break;
	}
	case IDC_SLIDER_SPEED:
	{
		int iPos = m_ctrlSpeedSlider.GetPos();
		switch(iPos)
		{
		case 1: m_iReplaySpeed = 496;	break;
		case 2: m_iReplaySpeed = 248;	break;
		case 3: m_iReplaySpeed = 120;	break;
		case 4: m_iReplaySpeed = 58;	break;
		case 5: m_iReplaySpeed = 17;	break;
		case 6: m_iReplaySpeed = 8;		break;
		}
		
		pWnd->m_iReplaySpeed = m_iReplaySpeed;
		pWnd->LogPlaySpeedSet(m_iReplaySpeed);
		break;
	}
	case IDC_SLIDER_FRAMESKIP:
	{
		int iPos = m_ctrlFrameSkipSlider.GetPos();
		
		pWnd->m_iFrameSkip = iPos;
		pWnd->LogSetFrameSkip(iPos); 
	}
	}
		
	return 0;
}

void CControlDlg::SetButtonTooltip()
{
 	m_ctrlLoadBtn.SetThemeHelper(&m_ThemeHelper);
 	m_ctrlLoadBtn.SetTooltipText(_T("Load log data at selected day."));
 	m_ctrlLoadBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_ctrlFileBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlFileBtn.SetTooltipText(_T("Load log data from file."));
 	m_ctrlFileBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_ctrlPlayBtn.SetBitmaps(IDB_BITMAP_PLAY, RGB(0, 0, 0));
	m_ctrlStopBtn.SetBitmaps(IDB_BITMAP_STOP, RGB(0, 0, 0));
	m_ctrlRewindBtn.SetBitmaps(IDB_BITMAP_REWIND, RGB(0, 0, 0));
	m_ctrlForwardBtn.SetBitmaps(IDB_BITMAP_FORWARD, RGB(0, 0, 0));
	
// 	
// 	m_ctrlPrintBtn.SetThemeHelper(&m_ThemeHelper);
// 	m_ctrlPrintBtn.SetTooltipText(_T("Print today event logs now."));
// 	m_ctrlPrintBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
// 	
// 	m_ctrlSaveBtn.SetThemeHelper(&m_ThemeHelper);
// 	m_ctrlSaveBtn.SetTooltipText(_T("Save today event logs to .csv file."));
// 	m_ctrlSaveBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
// 	
// 	m_ctrlScrollBtn.SetThemeHelper(&m_ThemeHelper);
// 	m_ctrlScrollBtn.SetTooltipText(_T("Toggle auto scroll."));
// 	m_ctrlScrollBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
// 	
	m_ctrlTitleBtn.SetBitmaps(IDB_MENUTITLE, (int)BTNST_AUTO_DARKER);
}

void CControlDlg::SetPosition()
{
	int height = 25;
	int divide = 10;
	
	CRect rect;
	GetClientRect(&rect);
	
	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);
	
	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + 20;
	m_ctrlTitleBtn.SetWindowPlacement(&iBoxPlacement);

 	//Date Picker
 	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 460;
 	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 300;
 	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 95;
 	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 65;
 	m_ctrlDateDT.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlDateDT.ShowWindow(SW_SHOW);

 	//Load button
 	iBoxPlacement.rcNormalPosition.left   =  rect.left + (rect.right - rect.left) / 2 - 300;
 	iBoxPlacement.rcNormalPosition.right  =  rect.left + (rect.right - rect.left) / 2 - 200;
 	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 95;
 	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 65;
 	m_ctrlLoadBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlLoadBtn.ShowWindow(SW_SHOW);
	
	//Load File button
	iBoxPlacement.rcNormalPosition.left   =  rect.left + (rect.right - rect.left) / 2 - 100;
	iBoxPlacement.rcNormalPosition.right  =  rect.left + (rect.right - rect.left) / 2 + 50;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 95;
 	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 65;
	m_ctrlFileBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlFileBtn.ShowWindow(SW_SHOW);

	//TimeLine Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 460;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 460;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 55;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 25;
	GetDlgItem(IDC_STATIC_TIMELINE_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_TIMELINE_BORDER)->ShowWindow(SW_SHOW);

	//Time Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 400;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 400;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 35;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 15;
	m_ctrlTimeSlider.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlTimeSlider.ShowWindow(SW_SHOW);

	//Start Time
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 430;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 370;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 10;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 15;
	GetDlgItem(IDC_STATIC_START)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);

	//Time Picker
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 50;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 50;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 10;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 15;
	m_ctrlTimeDT.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlTimeDT.ShowWindow(SW_SHOW);

	//End Time
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 370;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 430;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 10;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 15;
	GetDlgItem(IDC_STATIC_END)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_END)->ShowWindow(SW_SHOW);

	//Control Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 460;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 175;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 35;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 120;
	GetDlgItem(IDC_STATIC_CONTROL_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_CONTROL_BORDER)->ShowWindow(SW_SHOW);
	
	//Rewind Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 430;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 385;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 105;
	m_ctrlRewindBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlRewindBtn.ShowWindow(SW_SHOW);

	//Stop Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 370;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 325;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 105;
	m_ctrlStopBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlStopBtn.ShowWindow(SW_SHOW);

	//Play Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 310;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 265;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 105;
	m_ctrlPlayBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlPlayBtn.ShowWindow(SW_SHOW);

	//Forward Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 250;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 205;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 105;
	m_ctrlForwardBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlForwardBtn.ShowWindow(SW_SHOW);

	//Speed/FrameSkip Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 155;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 460;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 35;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 120;
	GetDlgItem(IDC_STATIC_SPEED_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_SPEED_BORDER)->ShowWindow(SW_SHOW);

	//Speed Text
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 135;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 85;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 100;
	GetDlgItem(IDC_STATIC_SPEED)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_SPEED)->ShowWindow(SW_SHOW);

	//Speed Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 85;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 115;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 70;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 100;
	m_ctrlSpeedSlider.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlSpeedSlider.ShowWindow(SW_SHOW);

	//FrameSkip Text
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 150;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 230;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 60;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 100;
	GetDlgItem(IDC_STATIC_FRAMESKIP)->SetWindowPlacement(&iBoxPlacement);
	GetDlgItem(IDC_STATIC_FRAMESKIP)->ShowWindow(SW_SHOW);

	//FrameSkip Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 230;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 430;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 70;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 100;
	m_ctrlFrameSkipSlider.SetWindowPlacement(&iBoxPlacement);
	m_ctrlFrameSkipSlider.ShowWindow(SW_SHOW);
}

void CControlDlg::SetMinPosition()
{
//	int height = 25;
	int divide = 10;
	
	CRect rect;
	GetClientRect(&rect);
	
	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);
	
	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + 20;
	m_ctrlTitleBtn.SetWindowPlacement(&iBoxPlacement);
	
 	//Date Picker
 	m_ctrlDateDT.ShowWindow(SW_HIDE);

 	//Load button
	m_ctrlLoadBtn.ShowWindow(SW_HIDE);
	
	//Load File button
	m_ctrlFileBtn.ShowWindow(SW_HIDE);

	//TimeLine Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 460;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 460;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 65;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 5;
	GetDlgItem(IDC_STATIC_TIMELINE_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_TIMELINE_BORDER)->ShowWindow(SW_SHOW);

	//Time Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 400;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 400;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 45;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 25;
	m_ctrlTimeSlider.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlTimeSlider.ShowWindow(SW_SHOW);

	//Start Time
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 430;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 370;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 25;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 5;
	GetDlgItem(IDC_STATIC_START)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);

	//Time Picker
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 50;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 50;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 5;
	m_ctrlTimeDT.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlTimeDT.ShowWindow(SW_SHOW);

	//End Time
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 370;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 430;
	iBoxPlacement.rcNormalPosition.top    = rect.top + (rect.bottom - rect.top) / 2 - 25;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 - 5;
	GetDlgItem(IDC_STATIC_END)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_END)->ShowWindow(SW_SHOW);

	//Control Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 460;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 175;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 10;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 85;
	GetDlgItem(IDC_STATIC_CONTROL_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_CONTROL_BORDER)->ShowWindow(SW_SHOW);
	
	//Rewind Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 430;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 385;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 75;
	m_ctrlRewindBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlRewindBtn.ShowWindow(SW_SHOW);

	//Stop Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 370;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 325;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 75;
	m_ctrlStopBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlStopBtn.ShowWindow(SW_SHOW);

	//Play Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 310;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 265;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 75;
	m_ctrlPlayBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlPlayBtn.ShowWindow(SW_SHOW);

	//Forward Button
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 250;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 205;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 75;
	m_ctrlForwardBtn.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlForwardBtn.ShowWindow(SW_SHOW);

	//Speed/FrameSkip Border
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 155;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 460;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 10;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 85;
	GetDlgItem(IDC_STATIC_SPEED_BORDER)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_SPEED_BORDER)->ShowWindow(SW_SHOW);

	//Speed Text
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 135;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 - 85;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 70;
	GetDlgItem(IDC_STATIC_SPEED)->SetWindowPlacement(&iBoxPlacement);
 	GetDlgItem(IDC_STATIC_SPEED)->ShowWindow(SW_SHOW);

	//Speed Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 - 85;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 115;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 40;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 80;
	m_ctrlSpeedSlider.SetWindowPlacement(&iBoxPlacement);
 	m_ctrlSpeedSlider.ShowWindow(SW_SHOW);

	//FrameSkip Text
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 150;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 230;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 30;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 70;
	GetDlgItem(IDC_STATIC_FRAMESKIP)->SetWindowPlacement(&iBoxPlacement);
	GetDlgItem(IDC_STATIC_FRAMESKIP)->ShowWindow(SW_SHOW);

	//FrameSkip Silder
	iBoxPlacement.rcNormalPosition.left   = rect.left + (rect.right - rect.left) / 2 + 230;
	iBoxPlacement.rcNormalPosition.right  = rect.left + (rect.right - rect.left) / 2 + 430;
	iBoxPlacement.rcNormalPosition.top	  = rect.top + (rect.bottom - rect.top) / 2 + 40;
	iBoxPlacement.rcNormalPosition.bottom = rect.top + (rect.bottom - rect.top) / 2 + 80;
	m_ctrlFrameSkipSlider.SetWindowPlacement(&iBoxPlacement);
	m_ctrlFrameSkipSlider.ShowWindow(SW_SHOW);
}

void CControlDlg::OnKillfocusDatetimeTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	UpdateTime(TRUE);

	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	int nNewSec = m_curDateTime.GetSecInDay() - m_startDateTime.GetSecInDay();
	m_ctrlTimeSlider.SetPos(nNewSec);
	
	m_curDateTime.nSec	= nNewSec % 60;
	m_curDateTime.nMin	= (nNewSec / 60) % 60;
	m_curDateTime.nHour	= nNewSec / 3600;
	
	UpdateTime(FALSE);
	
	pWnd->m_ReplayTime = m_curDateTime;
	pWnd->SetPlayTime();	
	pWnd->LogPlaySetTime();
	
	if(m_bIsPlay)
	{
		pWnd->LogSelectRun(TRUE);
	}

	*pResult = 0;
}

void CControlDlg::OnSetfocusDatetimeTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	pWnd->LogPlayPause(TRUE);
	
	*pResult = 0;
}

void CControlDlg::OnReleasedcaptureSliderSpeed(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

BOOL CControlDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();
	
	if ((pMsg->message == WM_KEYDOWN) || (pMsg->message == WM_SYSKEYDOWN)) 
	{
		switch(pMsg->wParam) 
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		case VK_F4:		// About
			if((pMsg->lParam&0x20000000)==0x20000000)
			{
				m_pMainFrame->PostMessage(WM_COMMAND,ID_EXIT);
			}
			break;
		default:
			break;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CControlDlg::OnButtonTitle() 
{
	// TODO: Add your control notification handler code here
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();

	if(m_bIsMinimized)
	{
		m_bIsMinimized = FALSE;

		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
		iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMainStartY + pApp->m_SystemValue.nMainSizeY - MIN_SIZEY;
		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
		iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
		SetWindowPlacement(&iWinPlacement);	

		SetMinPosition();
	}
	else
	{
		m_bIsMinimized = TRUE;

		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
		iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMenuStartY;
		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
		iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
		SetWindowPlacement(&iWinPlacement);	

		SetPosition();
	}

	Invalidate();
}

void CControlDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();

	lpMMI->ptMinTrackSize.x = pApp->m_SystemValue.nMenuSizeX;	// 최소값
	lpMMI->ptMaxTrackSize.x = pApp->m_SystemValue.nMenuSizeX;	// 최대값
	
	if(m_bIsMinimized)
	{
		lpMMI->ptMinTrackSize.y = pApp->m_SystemValue.nMenuSizeY;
		lpMMI->ptMaxTrackSize.y = pApp->m_SystemValue.nMenuSizeY;
	}
	else
	{
		lpMMI->ptMinTrackSize.y = MIN_SIZEY; 
		lpMMI->ptMaxTrackSize.y = MIN_SIZEY; 
	}
	
	CDialog::OnGetMinMaxInfo(lpMMI);
}

//DEL void CControlDlg::OnSize(UINT nType, int cx, int cy) 
//DEL {
//DEL 	CDialog::OnSize(nType, cx, cy);
//DEL 	
//DEL 	// TODO: Add your message handler code here
//DEL 	if(m_bIsMinimized)
//DEL 	{
//DEL 		WINDOWPLACEMENT iWinPlacement;
//DEL 		GetWindowPlacement(&iWinPlacement);
//DEL 		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
//DEL 		iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMainStartY + pApp->m_SystemValue.nMainSizeY - MIN_SIZEY;
//DEL 		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
//DEL 		iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
//DEL 		SetWindowPlacement(&iWinPlacement);	
//DEL 		
//DEL 		SetMinPosition();
//DEL 	}
//DEL 	else
//DEL 	{
//DEL 		m_bIsMinimized = TRUE;
//DEL 		
//DEL 		WINDOWPLACEMENT iWinPlacement;
//DEL 		GetWindowPlacement(&iWinPlacement);
//DEL 		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
//DEL 		iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMenuStartY;
//DEL 		iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
//DEL 		iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
//DEL 		SetWindowPlacement(&iWinPlacement);	
//DEL 		
//DEL 		SetPosition();
//DEL 	}
//DEL }
