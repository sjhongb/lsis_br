// MySocket.cpp : implementation of the CMySocket class
//

#include "stdafx.h"
#include "MySocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMySocket
CMySocket::CMySocket(CWnd* pParentWnd, SOCKET s)
{
	m_Socket = s;
	m_nLastError = 0;
	
	ASSERT(pParentWnd);
	m_pParentWnd = pParentWnd;
	if (!Create(NULL, "", WS_CHILD, CRect(0,0,0,0), m_pParentWnd, m_Socket))
	{
		// 일단 window를 만드는데에는 성공하는 것을 전제로 하자
		ASSERT(0);
	}
    
	if (INVALID_SOCKET != m_Socket)
	{
		// Accept를 통해 만들어진 소켓의 경우
		m_nAsyncFlag = (FD_READ | FD_WRITE | FD_OOB | FD_CLOSE);
		m_bConnected = TRUE;
	}
	else
	{
		// 자체적으로 만든 소켓의 경우
		m_nAsyncFlag = 0;
		m_bConnected = FALSE;
		CreateSocket();
	}
}

CMySocket::~CMySocket()
{
	// 아직 처리되지 않은 데이터 블럭이 있으면 지운다
	while (!m_listSend.IsEmpty())
		delete	m_listSend.RemoveHead();
	while (!m_listReceive.IsEmpty())
		delete	m_listReceive.RemoveHead();
}

BEGIN_MESSAGE_MAP(CMySocket, CWnd)
	//{{AFX_MSG_MAP(CMySocket)
		ON_MESSAGE(WSM_GOT_HOST, OnGotHost)
		ON_MESSAGE(WSM_ASYNC_EVENT, OnAsyncEvent)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMySocket Create & Close

BOOL CMySocket::CreateSocket() 
{
	if (!IsWindow(m_hWnd))	return	FALSE;
	if (INVALID_SOCKET == m_Socket)
	{
	    // TCP용 소켓을 만든다
		m_Socket = socket(AF_INET, SOCK_STREAM, 0);
		if (INVALID_SOCKET == m_Socket) 
			return	IsError();
	}
	return	TRUE;
}

BOOL CMySocket::CloseSocket()
{
	PostNotify(SN_CLOSED);
	if (INVALID_SOCKET != m_Socket) {
		DWORD	dw = 1L;

		AsyncSelect(0);
		ioctlsocket(m_Socket, FIONBIO, &dw);
		closesocket(m_Socket);
		m_Socket = INVALID_SOCKET;
	}
	m_bConnected = FALSE;
	SetAsyncFlag(0);
	return	TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Socket Main operation

BOOL CMySocket::Connect(CString strPeerName, int nPort)
{
	if (INVALID_SOCKET == m_Socket && !CreateSocket())	return	FALSE;

    m_SockAddr.sin_port = htons(nPort);
	DWORD	dwPeerAddress = inet_addr(strPeerName);
	if (INADDR_NONE != dwPeerAddress) 
	{
	    m_SockAddr.sin_addr.s_addr = dwPeerAddress;
		OnGotHost(m_Socket, MAKELPARAM(-1, 0));
		return	TRUE;
	}

	// 호스트네임을 입력한 경우, IP Address로 바꾸어야한다
	if (!(WSAAsyncGetHostByName(m_hWnd, WSM_GOT_HOST, (LPSTR)(LPCSTR)strPeerName, 
			(LPSTR)m_HostEnt, MAXGETHOSTSTRUCT)))
		return	IsError();
	return	TRUE;
}

BOOL CMySocket::Bind(int nPort)
{
	if (INVALID_SOCKET == m_Socket && !CreateSocket())	return	FALSE;
    int	nLen = sizeof(m_SockAddr);

	m_SockAddr.sin_port = htons(nPort);
    m_SockAddr.sin_family = AF_INET;
    m_SockAddr.sin_addr.s_addr = INADDR_ANY;

	if (SOCKET_ERROR != bind(m_Socket, (LPSOCKADDR)&m_SockAddr, nLen))
		return TRUE;
	return	IsError();
}

BOOL CMySocket::Listen(int nBackLog)
{
	if (INVALID_SOCKET == m_Socket && !CreateSocket())	return	FALSE;
	if (AsyncSelect(FD_ACCEPT) && SOCKET_ERROR != listen(m_Socket, nBackLog)) 
		return	TRUE;
	return	IsError();
}

BOOL CMySocket::Accept(SOCKET &s)
{
	int		nLen = sizeof(m_SockAddr);
	
	// 현재의 소켓은 계속 Listen상태로 남아 있고
	// 데이터를 주고받기 위한 소켓을 새로 할당받는다
	s = accept(m_Socket, (LPSOCKADDR)&m_SockAddr, (int *)&nLen);
    if (INVALID_SOCKET != s) 
    	return TRUE;
	return	IsError();
}

BOOL CMySocket::WriteBlock(LPCSTR pBlock, int nLength)
{
	if (INVALID_SOCKET == m_Socket || !m_bConnected)	return	FALSE;
	
	// 리스트에만 추가시키고 실제로 FD_WRITE에서 보낸다
	m_listSend.AddTail(new CSockBlock((LPSTR)pBlock, nLength));
	return	AsyncSelect();
}

BOOL CMySocket::ReadBlock(LPSTR lpBlock, int &nLength)
{
	ASSERT(lpBlock);
	ASSERT(0 < nLength);

	int	nRequire = nLength;
	nLength = 0;
	
	if (INVALID_SOCKET == m_Socket || !m_bConnected)	return	FALSE;
	if (m_listReceive.IsEmpty())	return	FALSE;

	// 실제로는, 이미 읽어서 리스트에 넣어둔 내용을 처리한다
	CSockBlock* pBlock;
	int nRead;
	while (nLength < nRequire && !m_listReceive.IsEmpty())
	{
		pBlock = (CSockBlock*)m_listReceive.GetHead();
		nRead = min(nRequire - nLength, (int)*pBlock);
		memcpy(lpBlock, (LPCSTR)*pBlock, nRead);
		if (!pBlock->RemoveHead(nRead))
			delete	m_listReceive.RemoveHead();
		lpBlock += nRead;
		nLength += nRead;
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Socket Misc. operation

BOOL CMySocket::AsyncSelect()
{
	return	AsyncSelect(m_nAsyncFlag);
}

BOOL CMySocket::AsyncSelect(int nMode)
{
	if (INVALID_SOCKET == m_Socket || !IsWindow(m_hWnd))	return	FALSE;
   	if (SOCKET_ERROR != WSAAsyncSelect(m_Socket, m_hWnd, nMode ? WSM_ASYNC_EVENT : 0, nMode))
   		return	TRUE;
   	return	IsError();
}

BOOL CMySocket::SetOption(int nOption, int nValue)
{
	if (INVALID_SOCKET == m_Socket)	return	FALSE;
    return	!setsockopt(m_Socket, SOL_SOCKET, nOption, (LPSTR)&nValue, sizeof(int));
}

/////////////////////////////////////////////////////////////////////////////
// CMySocket Error

BOOL CMySocket::IsError()
{ 
	return IsError(WSAGetLastError()); 
}

BOOL CMySocket::IsError(int nError)
{
	// 정상적인 상태라고 여길 수 있는 Error의 경우 무시하고
	// 그 외의 경우에는 소켓을 닫는다
	switch (nError) {
		case WSAENOBUFS:
		case WSAEWOULDBLOCK:
		case WSAEFAULT:
		case 0:
			return	FALSE;
	}
	m_nLastError = nError;
	PostNotify(SN_ERROR, nError);
	CloseSocket();
	return	TRUE;	
}

/////////////////////////////////////////////////////////////////////////////
// CMySocket Network event

LONG CMySocket::OnGotHost(UINT wSocket, LONG lParam)
{
	if (IsError(WSAGETSELECTERROR(lParam)))	return	0;
	
	PostNotify(SN_GOT_HOST);

    m_SockAddr.sin_family = AF_INET;
	if ((WORD)-1 != WSAGETASYNCBUFLEN(lParam)) {
	    PHOSTENT	ps = (PHOSTENT)m_HostEnt;
	    m_SockAddr.sin_addr = *((IN_ADDR FAR *)ps->h_addr);
	}

	// 실제로 Connect하는 부분
	if (!AsyncSelect(FD_CONNECT) ||
			SOCKET_ERROR != connect(m_Socket, (SOCKADDR *)&m_SockAddr, sizeof (m_SockAddr)))
		IsError();
	return	0;
}

LONG CMySocket::OnAsyncEvent(UINT wSocket, LONG lParam)
{
	// Notify에 대한 각 핸들러를 불러준다
	int nErrorCode = WSAGETSELECTERROR(lParam);
	switch (WSAGETSELECTEVENT(lParam)) 
	{
	case FD_CONNECT:	
		OnConnect(nErrorCode);	
		break;
	case FD_ACCEPT:
		OnAccept(nErrorCode);
		break;
	case FD_CLOSE:		
		OnCloseSocket(nErrorCode);
		break;
	case FD_OOB:
		OnOutOfBand(nErrorCode);
		break;
	case FD_READ:		
		OnDataReady(nErrorCode);	
		break;
	case FD_WRITE:		
		OnSendAvailable(nErrorCode);
	}
	return	0;
}

BOOL CMySocket::OnConnect(int nErrorCode)
{
	if (IsError(nErrorCode))	return	FALSE;
	PostNotify(SN_CONNECTED);
	m_bConnected = TRUE;
	SetAsyncFlag(FD_READ | FD_WRITE);
	return	TRUE;
}

BOOL CMySocket::OnAccept(int nErrorCode)
{
	if (IsError(nErrorCode))	return	FALSE;
	PostNotify(SN_ACCEPTED);
	return	TRUE;
}

BOOL CMySocket::OnCloseSocket(int nErrorCode)
{
	if (IsError(nErrorCode))	return	FALSE;
	CloseSocket();
	return	TRUE;
}

#define	RECV_BLOCK_SIZE	(4*1024)
BOOL CMySocket::OnDataReady(int nErrorCode)
{
	if (IsError(nErrorCode))	return	FALSE;
	char	pBlock[RECV_BLOCK_SIZE];
	
	// 도착한 데이터를 바로바로 읽어서 리스트에 넣어둔다
	int		nLength = recv(m_Socket, pBlock, sizeof(pBlock), 0);
	if (nLength)
	{
		if (SOCKET_ERROR != nLength) 
		{
			m_listReceive.AddTail(new CSockBlock(pBlock, nLength));
			PostNotify(SN_DATA_READY);
			return	TRUE;
		}
		// Notify를 Re-enable시킨다
		if (!IsError())
			AsyncSelect();
	}
	return	FALSE;
}	

BOOL CMySocket::OnSendAvailable(int nErrorCode)
{
	if (IsError(nErrorCode))	return	FALSE;
	// 리스트에 쌓인 내용을 읽어서 한 블럭씩 보낸다
	if (!m_listSend.IsEmpty())
	{
		CSockBlock* pBlock = (CSockBlock*)m_listSend.GetHead();
		int nSend = send(m_Socket, (LPCSTR)*pBlock, (int)*pBlock, 0);
		if (SOCKET_ERROR != nSend) 
		{
			if (!pBlock->RemoveHead(nSend))
				delete	m_listSend.RemoveHead();
		} else {
			if (!IsError())
				AsyncSelect();
			return	FALSE;
		}
	}
	return	TRUE;
}

BOOL CMySocket::OnOutOfBand(int nErrorCode)
{
	return IsError(nErrorCode);
}

void CMySocket::PostNotify(WORD nNotify, WORD nParam)
{
	// CMainFrame으로 Notify를 넘겨준다
	m_pParentWnd->PostMessage(WSM_NOTIFY, m_Socket, MAKELPARAM(nNotify, nParam));
}

/////////////////////////////////////////////////////////////////////////////
// CMySocket message handlers

void CMySocket::OnDestroy()
{
	CloseSocket();
	CWnd::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CSockBlock

CSockBlock::CSockBlock(char *pBlock, int nLength) 
{ 
	ASSERT(pBlock);
	ASSERT(0 < nLength);
	m_pBlock = new char[nLength]; 
	m_nLength = nLength; 
	memcpy(m_pBlock, pBlock, m_nLength); 
}

CSockBlock::~CSockBlock() 
{ 
	delete m_pBlock;
}

int CSockBlock::RemoveHead(int nLength)
{
	if (m_nLength <= nLength)
		m_nLength = 0;
	else
	{
		m_nLength -= nLength;
		char *p = new char[m_nLength];
		memcpy(p, m_pBlock + nLength, m_nLength);
		delete	m_pBlock;
		m_pBlock = p;
	}
	return	m_nLength;
}

/////////////////////////////////////////////////////////////////////////////
// Global function
BOOL MySockStart()
{
	// Winsock을 시작하고, 버젼이 1.1인지 확인한다
	WSADATA WSAData;
    return !WSAStartup(0x0101, &WSAData);
}

void MySockEnd()
{
	WSACleanup();
}
