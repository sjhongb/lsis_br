// MsgTypeSelDLg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "MsgTypeSelDLg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Declaration in EIDApp.cpp 
extern BOOL _bOnMsgSystem;

/////////////////////////////////////////////////////////////////////////////
// CMsgTypeSelDLg dialog

CMsgTypeSelDLg::CMsgTypeSelDLg(CWnd* pParent /*=NULL*/)
	: CDialog(CMsgTypeSelDLg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMsgTypeSelDLg)
	m_bActionInfo = FALSE;
	m_bErrorInfo = FALSE;
	m_bOperateInfo = FALSE;
	m_bSwitchType = FALSE;
	m_bSignalType = FALSE;
	m_bSystemType = FALSE;
	m_bOtherInfo = FALSE;
	m_bTrackType = FALSE;
	//}}AFX_DATA_INIT

	m_ModeType.MsgType = 0xff;
	if ( !_bOnMsgSystem ) {
		m_ModeType.bTypeSystem = 0;		// 시스템 관련 메세지
	}	
}


void CMsgTypeSelDLg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMsgTypeSelDLg)
	DDX_Check(pDX, IDC_CHECK_MSG_ACTIVE, m_bActionInfo);
	DDX_Check(pDX, IDC_CHECK_MSG_ERROR, m_bErrorInfo);
	DDX_Check(pDX, IDC_CHECK_MSG_OPERATE, m_bOperateInfo);
	DDX_Check(pDX, IDC_CHECK_MSG_SWITCH, m_bSwitchType);
	DDX_Check(pDX, IDC_CHECK_MSG_SIGNAL, m_bSignalType);
	DDX_Check(pDX, IDC_CHECK_MSG_SYSTEM_MODULE, m_bSystemType);
	DDX_Check(pDX, IDC_CHECK_MSG_SYSTEM_OTHER, m_bOtherInfo);
	DDX_Check(pDX, IDC_CHECK_MSG_TRACK, m_bTrackType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMsgTypeSelDLg, CDialog)
	//{{AFX_MSG_MAP(CMsgTypeSelDLg)
	ON_BN_CLICKED(IDC_BUTTON_ALL_MODE_SEL, OnButtonAllModeSel)
	ON_BN_CLICKED(IDC_BUTTON_ALL_TYPE_SEL, OnButtonAllTypeSel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgTypeSelDLg message handlers

BOOL CMsgTypeSelDLg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_bErrorInfo = ( m_ModeType.bModeError ) ? TRUE : FALSE;		// 장애관련 메세지
	m_bActionInfo = ( m_ModeType.bModeILocking ) ? TRUE : FALSE;	// 연동관련 메세지
	m_bOperateInfo = ( m_ModeType.bModeOperate ) ? TRUE : FALSE;	// 제어관련 메세지
	m_bOtherInfo = ( m_ModeType.bModeMessage ) ? TRUE : FALSE;		// 기타(시스템) 메세지

	m_bTrackType = ( m_ModeType.bTypeTrack ) ? TRUE : FALSE;		// 궤도 관련 메세지
	m_bSignalType = ( m_ModeType.bTypeSignal ) ? TRUE : FALSE;		// 신호기 관련 메세지
	m_bSwitchType = ( m_ModeType.bTypeSwitch ) ? TRUE : FALSE;		// 선로전환기 관련 메세지
	if ( _bOnMsgSystem )
		m_bSystemType = ( m_ModeType.bTypeSystem ) ? TRUE : FALSE;		// 시스템 관련 메세지
	else {
		GetDlgItem( IDC_CHECK_MSG_SYSTEM_MODULE )->EnableWindow( FALSE );
		m_bSystemType = FALSE;
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CMsgTypeSelDLg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );

	if ( !m_bErrorInfo && !m_bActionInfo && !m_bOperateInfo && !m_bOtherInfo ) {
		AfxMessageBox("Input mistake! \nThere is no selected data. Input again");
		return;
	}

	if ( !m_bTrackType && !m_bSignalType && !m_bSwitchType && !m_bSystemType ) {
		AfxMessageBox("Input mistake! \nThere is no selected data. Input again");
		return;
	}

	m_ModeType.bModeError = ( m_bErrorInfo ) ? 1 : 0;		// 장애관련 메세지
	m_ModeType.bModeILocking = ( m_bActionInfo ) ? 1 : 0;	// 연동관련 메세지
	m_ModeType.bModeOperate = ( m_bOperateInfo ) ? 1 : 0; 	// 제어관련 메세지
	m_ModeType.bModeMessage = ( m_bOtherInfo ) ? 1 : 0;		// 기타(시스템) 메세지

	m_ModeType.bTypeTrack = ( m_bTrackType ) ? 1 : 0;		// 궤도 관련 메세지
	m_ModeType.bTypeSignal = ( m_bSignalType ) ? 1 : 0;		// 신호기 관련 메세지
	m_ModeType.bTypeSwitch = ( m_bSwitchType ) ? 1 : 0;		// 선로전환기 관련 메세지
	if ( _bOnMsgSystem )
		m_ModeType.bTypeSystem = ( m_bSystemType ) ? 1 : 0;		// 시스템 관련 메세지
	else 
		m_ModeType.bTypeSystem = 0;
	
	CDialog::OnOK();
}

void CMsgTypeSelDLg::OnButtonAllModeSel() 
{
	// TODO: Add your control notification handler code here
	
	m_bErrorInfo  = TRUE;		// 장애관련 메세지
	m_bActionInfo = TRUE;		// 연동관련 메세지
	m_bOperateInfo = TRUE;		// 제어관련 메세지
	m_bOtherInfo = TRUE;		// 기타(시스템) 메세지

	UpdateData( FALSE );
}

void CMsgTypeSelDLg::OnButtonAllTypeSel() 
{
	// TODO: Add your control notification handler code here

	m_bTrackType = TRUE;		// 궤도 관련 메세지
	m_bSignalType = TRUE;		// 신호기 관련 메세지
	m_bSwitchType = TRUE;		// 선로전환기 관련 메세지
	if ( _bOnMsgSystem ) {
		m_bSystemType = TRUE;		// 시스템 관련 메세지
	}	

	UpdateData( FALSE );
}
