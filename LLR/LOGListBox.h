// LOGListBox.h : header file
//

#ifndef   _LOGLISTBOX_H
#define   _LOGLISTBOX_H

#include "../include/MessageNo.h"
#include "../include/LOGPrnConfig.h"
#include "../include/LOGFile.h"
#include "../include/LOGMessage.h"
#include "../include/memdll.h"

/////////////////////////////////////////////////////////////////////////////
// CLOGListBox window
/*
#define MAX_MESSAGEQUEUE 100;
typedef struct {
	struct  {
		int  iMsgQNo;           // FTP 를 호출한 부모 프로그램명
		char cMessage[100][255];       
	} log_data;
} MESSAGE_FILE;
*/
#define LOG_MESSAGE             (WM_USER+5678)
#define VOICE_MESSAGE           (WM_USER+6789)

class CLOGListBox : public CListBox
{
	LOGHeaderType	m_AlterHead;
	DataHeaderType	m_AlterSysHead;	// header data
	CLOGRecord		m_crRecord;
	CLOGRecord		m_exRecord;
	int   m_nLastPage;
	int   m_nMaxViewItem;
	BYTE  m_nOrderInSec;
	BOOL  m_bExRecOrder;
	CLOGMessage	m_ViewLOGMessage;

//    CMemDLL*        m_pMapDLL;
//    MESSAGE_FILE*   m_pMapMemory;

public:
	void ResizeWindow();
	void AddListItem( LPCTSTR pStr, int attop = 1 );
	void AddSharedMemory( LPCTSTR pStr, int attop = 1 );
	void AttachMessage( CObList &objlist, BOOL bRealData );
	int  AttachFileRecord( BYTE *pVMem, int length, BOOL bFailTime = FALSE , int iView = 0 );
	int  LastMessage();
	int  QuickLastMessage();
    int  NextMessage(BOOL bFirst = TRUE, BOOL bResearch = FALSE, LOGPrnConfigType *pConfig = NULL);
    int  OnedayMessage(BOOL bFirst = TRUE, BOOL bResearch = FALSE, LOGPrnConfigType *pConfig = NULL);
	int  PrevMessage(BOOL bFirst = TRUE, BOOL bResearch = FALSE, LOGPrnConfigType *pConfig = NULL);
	int  GetNextMsgPage( BOOL bFirst, CJTime &SearchTime, LOGPrnConfigType &Config, CObList &oblist );
	int  LastOneMessage();
	int  m_iControlFlag;
	CLOGListBox();

// Attributes
public:
	BOOL	m_bLastView;
	BOOL	m_bAttachFile;
	BOOL	m_bNoneSaved;
	CJTime	m_SearchTime;
	CLOGFile  m_SaveLOGFile;
	CLOGFile* m_pViewLOGFile;
	CObList   m_ViewList;
	CObList   m_MessageList;
	BOOL  m_bStopScroll;
	BOOL  m_bAttachView;
	MsgModeType	m_MsgFilter;
	CString m_strNewMessage[200];
	int     m_iMsgPoint;
	BOOL  m_bReplayFirstCK;
	
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLOGListBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLOGListBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLOGListBox)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//extern void FlushEventObjLst( CObList &objlist );

/////////////////////////////////////////////////////////////////////////////
#endif // _LOGLISTBOX_H
