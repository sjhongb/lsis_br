// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_)
#define AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LLRView.h"
#include "LLRDoc.h"
#include "../include/LOGMessage.h"
//#include "LogWndDlg.h"
#include "EventDlg.h"
#include "LogTestDlg.h"
#include "DirectSound.h"
#include "resource.h"
#include "Splash.h"
#include "../include/JTime.h"
#include "../include/MenuItem.h"
#include "../include/EipEmu.h"
#include "SharedMemory.h"
#include "ControlPannelDlg.h"
#include "ControlDlg.h"

typedef BOOL (WINAPI *PFN_InstallHook)(
	HWND hWndServer,
	UINT nOSVersionType
	);

typedef BOOL (WINAPI *PFN_UnInstallHook)();


typedef void (WINAPI *PFN_SetHookOption)(
					  int nWaitTime, 
					  BOOL bTraceEnabled, 
					  BOOL bSendDebugText
					  );

typedef void (WINAPI *PFN_GetHookOption)(
					  int &nWaitTime, 
					  BOOL &bTraceEnabled, 
					  BOOL &bSendDebugText
					  );

typedef char* (WINAPI *PFN_GetResultString)();

typedef void (WINAPI *PFN_GetIniFilePath)(char *szIniFile);

typedef void (WINAPI *PFN_SetEnabledRestart)(BOOL bEnabled);

#define ID_TIMER			1
#define TIMER_MAX_INTRCOUNT	200		// x ms
#define MAX_MSGCNT			25
#define _MAX_USER_ID_LENGTH           8    // 15
#define _MAX_USER_PW_LENGTH           8    // 15
#define _MAX_CTRL_COM_LENGTH          14

#define BIT_SOUND_APPROACH	0x01
#define BIT_SOUND_SP		0x02
#define BIT_SOUND_POWER		0x04
#define BIT_SOUND_BLOCKADE1	0x08
#define BIT_SOUND_BLOCKADE2	0x10
#define BIT_SOUND_BLOCKADE3	0x20
#define BIT_SOUND_BLOCKADE4	0x40
#define BIT_SOUND_BLOCKADE5	0x80

#define AEPK 1
#define BEPK 2
#define CEPK 3
#define DEPK 4
#define EEPK 5
#define FEPK 6
#define GEPK 7
#define UPEPK 8
#define DNEPK 9

#define LC1 0
#define LC2 1
#define LC3 2
#define LC4 3
#define LC5 4

class CMainFrame : public CFrameWnd
{
	
protected: 
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

	SharedMemory shared;

public:
	CJTime m_ReplayTime;
	int m_iReplaySpeed;
	int m_iFrameSkip;
	void ReleaseLogFile();
	void SetLogFile(CString strLogFile);
	void LogPlayStop(BOOL bCondition = TRUE);
	void SetPlayTime();
	void LogFrontRun(BOOL bCondition = TRUE);
	void ShowControlPannel(BOOL bShow = TRUE);
	void LogSelectRun (BOOL bCondition = TRUE);
	void LogPlaySetTime ();
	void LogPlaySpeedSet (int iSpeed = 1000 );
	void LogSetFrameSkip (int iSpeed = 1);
	void LogPlayPause (BOOL bCondition = TRUE);
	void ShowLogView (BOOL bShow = TRUE );
	UINT m_nTimerID;
	BYTE CommVMemData[ MAX_MSGBLOCK_SIZE ];
	CLLRApp* pApp;
	CStatusBar  m_wndStatusBar;
	CString m_strSendMessage;
	CString m_strReceiveMessage;
//	CControlPannelDlg m_ControlPannelDlg;
	CControlDlg	m_ControlDlg;
// 로그 표시 화면 오브젝트
	BOOL m_bIsCreateLOGView;
	BOOL m_bIsLOGView;
//	CLogWndDlg  m_LOGView;
	CEventDlg	m_EventDlg;
// 메뉴 표시 화면 오브젝트
	BOOL m_bIsCreateMenuView;
	BOOL   m_bSoundOpen   ;
	BOOL   m_bSystemRunOn  ;
	BOOL   m_bPrint  ;
	BOOL   m_bSystemRunOff ;
	BOOL m_bExitProgram;	//exit?
	BOOL m_bOperateDisable;
	CJTime m_PCTime;
	int m_iStartBit;
	int m_iResetTimer;
	//메뉴
	MenuItems m_pMenuRoute[3];	// signal, shunt
	MenuItems m_pMenuSwitch;	// switch
	MenuItems m_pMenuTrack;		// track
	CLOGMessage	m_RealLOGMessage;
	//DirectX Sound
	CDirectSound m_PlaySound[8];
	BYTE m_CurrentSound;
	BYTE m_OldSound;
	char    m_CmdQue[24];
	BOOL StartUp();
	HANDLE  m_hSubWinMap;
	LPVOID	m_pCmdXBuffer;
	LPVOID	m_pWinXBuffer;

	HMODULE					m_hmodHookTool;
	PFN_InstallHook			m_pfnInstallHook;
	PFN_UnInstallHook		m_pfnUnInstallHook;
	PFN_SetHookOption		m_pfnSetHookOption;
	PFN_GetHookOption		m_pfnGetHookOption;
	PFN_GetResultString		m_pfnGetResultString;
	PFN_GetIniFilePath		m_pfnGetIniFilePath;
	PFN_SetEnabledRestart	m_pfnSetEnabledRestart;
	BOOL			m_bStartHook;
	UINT			m_nOSVersionType;
	int GetTrackID(LPCSTR lpszName);

// Operations
public:
	void UpdateLog();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnAllStopSound();
	void OnCheckSoundState(BYTE* pData);
	void OnPlaySound();
	BOOL OnSoundLoad();
	void SetLSMFocus();
	void GetLogPath(CJTime &destT, CString &strFileName, int nMode);
	void OnMenuLockUpdate();
	int CreateObj();			// initialize Class or Object
	BOOL SystemTimeBCDToDecimal(DataHeaderType* R);
	BOOL m_bCloseWindow;		//Window Close or not?
	BOOL m_bDlgDisplaed;
	BYTE m_RecieveCycle;
	void OnSetVMem(BYTE *pVMem,int length);
	BYTE m_DataCycle;
	CommStatusType m_CommStatus;
	LCCCommStatusType m_LccCommStatus;
	SystemStatusType m_SystemStatus;
	LCCCommDataType m_CommCmd;
	BYTE m_CmdCycle;
	int m_iMenuViewExitCk;
	int m_iLogViewExitCk;
	int m_iMessageSendCnt; // 메세지를 잘라서 보내는 카운트 처음 보낼때 0 
	int m_iMessageReceiveCnt;
	UINT m_nCmdTick;
	void ShowLockMessage();
	void SendCurrentTime();
	int m_nBaudRate;
	void PosiotionFix();
	virtual ~CMainFrame();
	void OnMessagePrint( BYTE *pData, int nPort );
	void OnLLRExit();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnExit();
	//}}AFX_MSG
	afx_msg void OnGet777(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGet775(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__14A34A2B_3ACE_4BC9_84A7_F487B6EEB594__INCLUDED_)
