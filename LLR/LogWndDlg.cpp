// LogWndDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LogWndDlg.h"
#include "LLR.h"
#include "mainfrm.h"
#include "MsgTypeSelDLg.h"
#include "LOGPrnSelDlg.h"

extern class CFMSGList *_pEIPMsgList;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern Eid_AppOptionType _AppOptions;

typedef BOOL (WINAPI *SetLayer)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);
#define LWA_COLORKEY            0x01
#define LWA_ALPHA               0x02

/////////////////////////////////////////////////////////////////////////////
// CLogWndDlg dialog

void FlushEventPtrArray( CPtrArray &objarray )
{
	int i, max = objarray.GetSize();
	for(i =0; i<max; i++ ) {
		MessageEvent* pEvent =(MessageEvent*)objarray.GetAt( i );
		delete pEvent;
	}
	objarray.RemoveAll();
}

CLogWndDlg::CLogWndDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLogWndDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLogWndDlg)
	//}}AFX_DATA_INIT
	m_bIsCreate = FALSE;
	pApp= (CLLRApp*)AfxGetApp();
	m_iHideCk = 0;

}


void CLogWndDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogWndDlg)
	DDX_Control(pDX, IDC_BTN_MENU, m_cBtnTitle);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btnPrev);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_btnNext);
	DDX_Control(pDX, IDC_BUTTON_LOGSET, m_btnLogset);
	DDX_Control(pDX, IDC_BUTTON_LAST, m_btnLast);
	DDX_Control(pDX, IDC_BUTTON_FIND, m_btnFind);
	DDX_Control(pDX, IDC_BUTTON_SCROLL, m_btnScroll);
	DDX_Control(pDX, IDC_LIST_LOG, m_listBox);
	DDX_Control(pDX, IDC_CRYSTALREPORT1, m_Rpt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogWndDlg, CDialog)
	//{{AFX_MSG_MAP(CLogWndDlg)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_LAST, OnLastMessage)
	ON_BN_CLICKED(IDC_BUTTON_PREV, OnPrevMessage)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, OnNextMessage)
	ON_BN_CLICKED(IDC_BUTTON_FIND, OnButtonFind)
	ON_BN_CLICKED(IDC_BUTTON_LOGSET, OnButtonLogset)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_SCROLL, OnButtonScroll)
	ON_BN_CLICKED(IDC_BTN_MENU, OnBtnMenu)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_TEST_MSG_PAGE_LAST, OnLastMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogWndDlg message handlers

BOOL CLogWndDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	
	OnSetSizeControl();
	OnSetXpButton();

	CDC   *pDC = GetDC();
	CSize chExtent = pDC->GetTextExtent( "W", 1 );
	ReleaseDC( pDC );
	chExtent.cx *= 150;
	m_listBox.SetItemHeight(-1,16);		
	m_listBox.SetHorizontalExtent( chExtent.cx );
	ToDayMessage();


	m_bIsCreate = TRUE;

	m_pReport  = new CReport;

	m_iTrans = 255;
	SetTrans(m_iTrans);
	m_bWindowsPos = TRUE;
	m_listBox.m_bStopScroll = FALSE;
	m_listBox.m_bAttachView = TRUE;

	pWaitDlg = new CWaitDlg;
	pWaitDlg->Create(IDD_DLG_WAIT,this);
	pWaitDlg->ShowWindow(SW_HIDE);

	m_listBox.LastMessage();
	m_listBox.SetCurSel( m_listBox.GetCount() - 1 );


	return TRUE;
}

BOOL CLogWndDlg::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CDialog::PreCreateWindow(cs);
}


int CLogWndDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1) return -1;
	
	CLLRApp* pApp = (CLLRApp*) AfxGetApp();

	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nLogStartX;
	iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nLogStartY;

	pApp->m_SystemValue.nLogSizeX = pApp->m_SystemValue.nMainSizeX;	
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;		

	iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY;
	SetWindowPlacement(&iWinPlacement);		

	return 0;
}

void CLogWndDlg::OnSetXpButton()
{
	m_btnFind.SetThemeHelper(&m_ThemeHelper);
	m_btnFind.SetTooltipText(_T("Recording search and print."));
	m_btnFind.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_btnLogset.SetThemeHelper(&m_ThemeHelper);
	m_btnLogset.SetTooltipText(_T("Set recording topology that is marked."));
	m_btnLogset.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_btnPrev.SetThemeHelper(&m_ThemeHelper);
	m_btnPrev.SetTooltipText(_T("Store log in Excel format."));
	m_btnPrev.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_btnNext.SetThemeHelper(&m_ThemeHelper);
	m_btnNext.SetTooltipText(_T("Next Page"));
	m_btnNext.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	m_btnNext.ShowWindow(SW_HIDE);

	m_btnLast.SetThemeHelper(&m_ThemeHelper);
	m_btnLast.SetTooltipText(_T("Last Page"));
	m_btnLast.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_btnScroll.SetThemeHelper(&m_ThemeHelper);
	m_btnScroll.SetTooltipText(_T("Stop or proceed scroll."));
	m_btnScroll.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));

	m_cBtnTitle.SetBitmaps(IDB_LOGTITLE , (int)BTNST_AUTO_DARKER);

}

void CLogWndDlg::OnSetSizeControl()
{
	int height = 25;
	int divide = 5;
	CRect rect;
	GetClientRect(&rect);

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);

	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top+20;
	m_cBtnTitle.SetWindowPlacement(&iBoxPlacement);

	//List Box
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right -3;
	iBoxPlacement.rcNormalPosition.top    = rect.top+20;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom - height ;
	m_listBox.SetWindowPlacement(&iBoxPlacement);

	//Search button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
	m_btnFind.SetWindowPlacement(&iBoxPlacement);

	//Log Set button
	iBoxPlacement.rcNormalPosition.left   =  rect.right / divide;
	iBoxPlacement.rcNormalPosition.right  =  rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnLogset.SetWindowPlacement(&iBoxPlacement);

	//Prev button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnPrev.SetWindowPlacement(&iBoxPlacement);


	//Last button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnLast.SetWindowPlacement(&iBoxPlacement);

	//Scroll button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.right  = rect.right ;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnScroll.SetWindowPlacement(&iBoxPlacement);
}

void CLogWndDlg::OnResizeWindow()
{
	int height = 25;
	int divide = 5;
	CRect rect;
	GetClientRect(&rect);

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);

	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top+20;
	m_cBtnTitle.SetWindowPlacement(&iBoxPlacement);

	//List Box
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right -3;
	iBoxPlacement.rcNormalPosition.top    = rect.top+20;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom - height ;
	m_listBox.SetWindowPlacement(&iBoxPlacement);

	//Search button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
	m_btnFind.SetWindowPlacement(&iBoxPlacement);

	//Log Set button
	iBoxPlacement.rcNormalPosition.left   =  rect.right / divide;
	iBoxPlacement.rcNormalPosition.right  =  rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnLogset.SetWindowPlacement(&iBoxPlacement);

	//Prev button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnPrev.SetWindowPlacement(&iBoxPlacement);

	//Last button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnLast.SetWindowPlacement(&iBoxPlacement);

	//Scroll button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.right  = rect.right ;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_btnScroll.SetWindowPlacement(&iBoxPlacement);

}

void CLogWndDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(m_bIsCreate)OnResizeWindow();
}

void CLogWndDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	m_bIsCreate = FALSE;
//	delete m_pReport;	//?????
}

void CLogWndDlg::LastMessage() 
{
	OnLastMessage();
}

void CLogWndDlg::OnLastMessage() 
{
	m_listBox.m_bStopScroll = FALSE;
	m_listBox.m_bAttachView = TRUE;
	m_listBox.SetFocus();
	m_listBox.LastMessage();
	m_listBox.SetCurSel( m_listBox.GetCount() - 1 );
	GetDlgItem( IDC_BUTTON_SCROLL )->SetWindowText("Scroll");	
}
void CLogWndDlg::PrevMessage()
{
	OnPrevMessage();
}

void CLogWndDlg::OnPrevMessage() 
{
       OnSave();
}

void CLogWndDlg::NextMessage()
{
	OnNextMessage();
}

void CLogWndDlg::OnNextMessage() 
{
	m_listBox.SetFocus();
	m_listBox.NextMessage(FALSE);	
}
void CLogWndDlg::ButtonFind() 
{
	OnButtonFind();
}

void CLogWndDlg::OnButtonFind() 
{
	CTime    time = CTime::GetCurrentTime();
	CLOGPrnSelDlg	Dlg( TRUE );

	m_LogConfig.Type = m_listBox.m_MsgFilter;
	MessageEvent *pEvent = NULL;
	if ( m_listBox.GetCount() ) {
		pEvent = (MessageEvent*)m_listBox.m_ViewList.GetHead();
	}

	m_LogConfig.STime.nSec  = 0;
	m_LogConfig.STime.nMin  = 0;
	m_LogConfig.STime.nHour = 0;
	m_LogConfig.STime.nDate  = time.GetDay();
	m_LogConfig.STime.nMonth  = time.GetMonth();
	m_LogConfig.STime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	m_LogConfig.ETime.nSec  = 59;
	m_LogConfig.ETime.nMin  = 59;
	m_LogConfig.ETime.nHour = 23;
	m_LogConfig.ETime.nDate  = time.GetDay();
	m_LogConfig.ETime.nMonth  = time.GetMonth();
	m_LogConfig.ETime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	Dlg.m_Config = m_LogConfig;
	if ( Dlg.DoModal() == IDOK ) {
		m_LogConfig = Dlg.m_Config;
		m_listBox.m_MsgFilter = m_LogConfig.Type;
		m_listBox.OnedayMessage( FALSE, TRUE, &m_LogConfig );
	}
	m_listBox.m_bAttachView = FALSE;
	m_listBox.m_bStopScroll = TRUE;
}

void CLogWndDlg::ToDayMessage()
{
	CTime    time = CTime::GetCurrentTime();

	m_LogConfig.Type = m_listBox.m_MsgFilter;
	MessageEvent *pEvent = NULL;
	if ( m_listBox.GetCount() ) {
		pEvent = (MessageEvent*)m_listBox.m_ViewList.GetHead();
	}

	m_LogConfig.STime.nSec  = 0;
	m_LogConfig.STime.nMin  = 0;
	m_LogConfig.STime.nHour = 0;
	m_LogConfig.STime.nDate  = time.GetDay();
	m_LogConfig.STime.nMonth  = time.GetMonth();
	m_LogConfig.STime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	m_LogConfig.ETime.nSec  = 59;
	m_LogConfig.ETime.nMin  = 59;
	m_LogConfig.ETime.nHour = 23;
	m_LogConfig.ETime.nDate  = time.GetDay();
	m_LogConfig.ETime.nMonth  = time.GetMonth();
	m_LogConfig.ETime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	m_listBox.m_MsgFilter = m_LogConfig.Type;
	m_listBox.OnedayMessage( FALSE, TRUE, &m_LogConfig );
	m_listBox.m_bAttachView = FALSE;
	m_listBox.m_bStopScroll = TRUE;

	if ( m_listBox.GetCount() > 0 ) {
		m_listBox.SetCurSel( m_listBox.GetCount() - 1 );
	}
}

void CLogWndDlg::ButtonLogset() 
{
	OnButtonLogset();
}

void CLogWndDlg::OnButtonLogset() 
{

	// Message Print Test Code
	// RunMessagePrint(0,0);
	// return;

	CLLRApp* pApp = (CLLRApp*) AfxGetApp();
	CTime    time = CTime::GetCurrentTime();
	
	CLOGPrnSelDlg	Dlg( TRUE );

	m_LogConfig.Type = m_listBox.m_MsgFilter;

	m_LogConfig.STime.nSec  = 0;
	m_LogConfig.STime.nMin  = 0;
	m_LogConfig.STime.nHour = 0;
	m_LogConfig.STime.nDate  = time.GetDay();
	m_LogConfig.STime.nMonth  = time.GetMonth();
	m_LogConfig.STime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	m_LogConfig.ETime.nSec  = 59;
	m_LogConfig.ETime.nMin  = 59;
	m_LogConfig.ETime.nHour = 23;
	m_LogConfig.ETime.nDate  = time.GetDay();
	m_LogConfig.ETime.nMonth  = time.GetMonth();
	m_LogConfig.ETime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	Dlg.m_Config = m_LogConfig;
	Dlg.m_iDlgType = 2;
	if ( Dlg.DoModal() == IDOK ) {
		m_LogConfig = Dlg.m_Config;
	} else {
		return;
	}
	pWaitDlg->ShowWindow(SW_SHOW);

//	RunPrint(0,0);	
	SetTimer(3,1000,NULL);
}

void CLogWndDlg::RunPrint(int iCntList, int iPrintSel)
{
	CLLRApp* pApp = (CLLRApp*) AfxGetApp();	
	CString strTrName,strTemp,strLogData;
	int iCnt;
	if ( m_pReport->IsOpen() ) m_pReport->Close();

	pWaitDlg->m_cProgress.SetRange32(0,0);

	m_pReport->m_strFilter = "";
	m_pReport->Open();


	// 레코드가 완전히 클리어 되었을때 발생하는 에러를 방지
	m_pReport->AddNew();
	m_pReport->m_Type = "     ";
	m_pReport->Update();
	m_pReport->Requery();
	m_pReport->MoveFirst();

	while(!m_pReport->IsEOF()) {
		m_pReport->Delete();
		m_pReport->MoveNext();
	}
	
	CString strTitle;

	int done,count;
	BOOL bFirst = TRUE;
	CJTime SearchTime;
	CObList	mMsgList;
	iCnt = 0;
	count = 0;
	MsgTimeStrings Msgs;
	mMsgList.RemoveAll();
	m_PrintList.RemoveAll();

	do {
		done = m_listBox.GetNextMsgPage( bFirst, SearchTime, m_LogConfig, mMsgList );
		bFirst = FALSE;
		count = mMsgList.GetCount();
		iCnt = iCnt + count;
		if ( count ) {
			POSITION pos = mMsgList.GetHeadPosition();
			for(; pos != NULL; ) {
				MessageEvent* pEvent = (MessageEvent*)mMsgList.GetNext( pos );
				if ( pEvent )
					m_PrintList.SetAtGrow( m_PrintList.GetSize(), pEvent ); 
			}
			mMsgList.RemoveAll();
		}
	} while (done != LOG_SEARCH_OVER);

	pWaitDlg->m_cProgress.SetRange32(0,iCnt);
	CString strCnt;
	
	if ( iCnt == 0 ) {
		AfxMessageBox ( " Log data not found ");
		m_pReport->Close();
		pWaitDlg->ShowWindow(SW_HIDE);
		return;
	}

	for(int i = 0 ; i < iCnt ; i++) {
		strCnt.Format("%d / %d",i,iCnt);
		pWaitDlg->m_cProgress.SetPos(i);
		pWaitDlg->SetCnt(strCnt);
		MessageEvent* pEvent = (MessageEvent*)m_PrintList.GetAt(i);
		pApp->m_pEIPMsgList->GetMessage( Msgs, pEvent );

		m_pReport->AddNew();

		m_pReport->m_Station = pApp->m_SystemValue.strStationRealName;
		strTitle.Format("%d/%d/20%02d   %02d:%02d:%02d" , m_LogConfig.STime.nDate , m_LogConfig.STime.nMonth , m_LogConfig.STime.nYear , m_LogConfig.STime.nHour , m_LogConfig.STime.nMin , m_LogConfig.STime.nSec );
		m_pReport->m_StartDate = strTitle;
		strTitle.Format("%d/%d/20%02d   %02d:%02d:%02d" , m_LogConfig.ETime.nDate , m_LogConfig.ETime.nMonth , m_LogConfig.ETime.nYear , m_LogConfig.ETime.nHour , m_LogConfig.ETime.nMin , m_LogConfig.ETime.nSec );
		m_pReport->m_EndDate = strTitle;
		m_pReport->m_SearchType = "ALL";
		m_pReport->m_User = pApp->m_SystemValue.strCurrentOperator;

		m_pReport->m_Type = Msgs.strType;
		m_pReport->m_Date = Msgs.strTime.Left(10);
		m_pReport->m_Time = Msgs.strTime.Mid(12,8);
		m_pReport->m_Msg = Msgs.strMsgNo;
		m_pReport->m_Contents = Msgs.strMessage;
		m_pReport->Update();
		m_pReport->Requery();
	}

	m_pReport->MoveFirst();
	
	if ( m_pReport->IsOpen() )
		m_pReport->Close();

	Sleep(1000);

	pWaitDlg->ShowWindow(SW_HIDE);

	m_Rpt.SetReportFileName("REPORT.RPT");	
	m_Rpt.SetDataFiles(1, "REPORT.MDB");
	m_Rpt.RetrieveDataFiles();

	Sleep(1000);

	m_Rpt.SetAction(1);
	
	Sleep(1000);
	// 레코드가 완전히 클리어 되었을때 발생하는 에러를 방지
	m_pReport->m_strFilter = "";
	m_pReport->Open();	
	m_pReport->AddNew();
	m_pReport->m_Type = "     ";
	m_pReport->Update();
	m_pReport->Requery();
	m_pReport->MoveFirst();
	m_pReport->Close();
}

void CLogWndDlg::MessagePrint()
{
	SetTimer(4,1000,NULL);
}

void CLogWndDlg::RunMessagePrint(int iCntList, int iPrintSel)
{
	CLLRApp* pApp = (CLLRApp*) AfxGetApp();	
	CString strTrName,strTemp,strLogData;

	int iYear,iMonth,iDay,iHour,iMin,iSec;
	SYSTEMTIME	csTime;
	::GetLocalTime(&csTime);

	iYear = csTime.wYear;
	iMonth = csTime.wMonth; 
	iDay  = csTime.wDay;
	iHour = csTime.wHour;
	iMin = csTime.wMinute;
	iSec = csTime.wSecond;

	if ( m_pReport->IsOpen() ) m_pReport->Close();

	m_pReport->m_strFilter = "";
	m_pReport->Open();

	// 레코드가 완전히 클리어 되었을때 발생하는 에러를 방지
	m_pReport->AddNew();
	m_pReport->m_Type = "     ";
	m_pReport->Update();
	m_pReport->Requery();
	m_pReport->MoveFirst();

	while(!m_pReport->IsEOF()) {
		m_pReport->Delete();
		m_pReport->MoveNext();
	}
	
	CString strTitle;

	if ( m_strMessage.GetLength() > (MAX_MSGCNT*10) ) {
		m_strMessage = m_strMessage.Left( (MAX_MSGCNT*10)-10 );
	}

	m_pReport->AddNew();
	m_pReport->m_Station = pApp->m_SystemValue.strStationRealName;
	strTitle.Format("%d/%d/%04d   %02d:%02d:%02d" , iDay , iMonth , iYear , iHour , iMin , iSec );
	m_pReport->m_StartDate = strTitle;
	m_pReport->m_EndDate = " ";
	m_pReport->m_SearchType = " ";
	m_pReport->m_User = pApp->m_SystemValue.strCurrentOperator;
	m_pReport->m_Type = " ";
	m_pReport->m_Date = " ";
	m_pReport->m_Time = " ";
	m_pReport->m_Msg = " ";
	m_pReport->m_Contents = m_strMessage;
	m_pReport->Update();
	m_pReport->Requery();

	m_pReport->MoveFirst();
	
	if ( m_pReport->IsOpen() )
		m_pReport->Close();

	Sleep(1000);

	m_Rpt.SetReportFileName("MESSAGE.RPT");	
	m_Rpt.SetDataFiles(1, "REPORT.MDB");
	m_Rpt.RetrieveDataFiles();

	Sleep(1000);

	m_Rpt.SetAction(1);
	
	Sleep(1000);
	// 레코드가 완전히 클리어 되었을때 발생하는 에러를 방지
	m_pReport->m_strFilter = "";
	m_pReport->Open();	
	m_pReport->AddNew();
	m_pReport->m_Type = "     ";
	m_pReport->Update();
	m_pReport->Requery();
	m_pReport->MoveFirst();
	m_pReport->Close();
}

BOOL CLogWndDlg::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}

void CLogWndDlg::OnSave() 
{

	CLLRApp* pApp = (CLLRApp*) AfxGetApp();
	CTime    time = CTime::GetCurrentTime();
    int        iCnt = 0;	
	CLOGPrnSelDlg	Dlg( TRUE );

	m_LogConfig.Type = m_listBox.m_MsgFilter;

	m_LogConfig.STime.nSec  = 0;
	m_LogConfig.STime.nMin  = 0;
	m_LogConfig.STime.nHour = 0;
	m_LogConfig.STime.nDate  = time.GetDay();
	m_LogConfig.STime.nMonth  = time.GetMonth();
	m_LogConfig.STime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	m_LogConfig.ETime.nSec  = 59;
	m_LogConfig.ETime.nMin  = 59;
	m_LogConfig.ETime.nHour = 23;
	m_LogConfig.ETime.nDate  = time.GetDay();
	m_LogConfig.ETime.nMonth  = time.GetMonth();
	m_LogConfig.ETime.nYear  = time.GetYear() - CJTIME_BASE_YEAR;

	Dlg.m_Config = m_LogConfig;
	Dlg.m_iDlgType = 2;
	if ( Dlg.DoModal() == IDOK ) {
		m_LogConfig = Dlg.m_Config;
	} else {
		return;
	}

	CString strTitle;

	int done,count;
	BOOL bFirst = TRUE;
	CJTime SearchTime;
	CObList	mMsgList;
	iCnt = 0;
	count = 0;
	MsgTimeStrings Msgs;
	mMsgList.RemoveAll();
	m_PrintList.RemoveAll();
	do {
		done = m_listBox.GetNextMsgPage( bFirst, SearchTime, m_LogConfig, mMsgList );
		bFirst = FALSE;
		count = mMsgList.GetCount();
		iCnt = iCnt + count;
		if ( count ) {
			POSITION pos = mMsgList.GetHeadPosition();
			for(; pos != NULL; ) {
				MessageEvent* pEvent = (MessageEvent*)mMsgList.GetNext( pos );
				if ( pEvent )
					m_PrintList.SetAtGrow( m_PrintList.GetSize(), pEvent ); 
			}
			mMsgList.RemoveAll();
		}
	} while (done != LOG_SEARCH_OVER);

	static char  szFilter[] = "Log data file (*.csv)";
    CString    filename;
	CString    pathName;
	HFILE      hFile;
    CString    temp, errstring;

    CFileDialog  m_filedlg( FALSE, "csv","*.csv",OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
    int ret_code = m_filedlg.DoModal();
    if(ret_code != IDOK) return;
    filename = m_filedlg.GetFileName();
	pathName = m_filedlg.GetPathName();
    if(pathName.IsEmpty()) AfxMessageBox("Data Save error!");
    else {
   
    temp.Format("%s", pathName );
	hFile = _lopen(temp,OF_READWRITE);
	if(hFile < 0) hFile = _lcreat(temp,0);

    temp.Format(" Log data \r\n\r\n\r\n");
    _lwrite(hFile,(LPCSTR) temp , temp.GetLength());
    temp = "Status , Date , Time , Code ,  Contents\r\n";
    _lwrite(hFile,(LPCSTR) temp , temp.GetLength());
    temp = "--------------,----------,----------,-----------------,----------------\r\n";
    _lwrite(hFile,(LPCSTR) temp , temp.GetLength());

	CString strLogData;
	CString strCnt;
	for(int i = 0 ; i < iCnt ; i++) {
		MessageEvent* pEvent = (MessageEvent*)m_PrintList.GetAt(i);
		pApp->m_pEIPMsgList->GetMessage( Msgs, pEvent );
		strLogData = Msgs.strType + ",";
		strLogData = strLogData + Msgs.strTime.Left(10) + ",";
		strLogData = strLogData + Msgs.strTime.Mid(12,8) + ",";
		strLogData = strLogData + Msgs.strMsgNo + ",";
		strLogData = strLogData + Msgs.strMessage + ",";
		strLogData = strLogData + "\r\n";
		_lwrite(hFile,(LPCSTR) strLogData , strLogData.GetLength());
	}
    _lclose( hFile );  
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
}

BOOL CLogWndDlg::SetTrans(int iVal)
{
	HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
	SetLayer pSetLayer = (SetLayer)GetProcAddress(hUser32, "SetLayeredWindowAttributes");
	if(pSetLayer == NULL)
	{
		MessageBox("More than Windows 2000");
		return TRUE;
	}
	char chAlpha = iVal; //투명도 설정 0 ~ 255
	SetWindowLong(this->m_hWnd, GWL_EXSTYLE, GetWindowLong(this->m_hWnd, GWL_EXSTYLE) | 0x80000);
	pSetLayer(this->m_hWnd, 0,chAlpha, LWA_ALPHA);
	return TRUE;
}

BOOL CLogWndDlg::SetTransOn(int iVal)
{
	SetTimer( 1 , 50, NULL);
	return TRUE;
}

BOOL CLogWndDlg::SetTransOff(int iVal)
{
	if ( m_iHideCk == 0 ) {
		SetTimer( 2 , 50, NULL);
		m_iHideCk = 1;
	}
	return TRUE;
}

BOOL CLogWndDlg::MoveHidePos()
{
	CLLRApp* pApp = (CLLRApp*) AfxGetApp();
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left   =  pApp->m_SystemValue.nLogStartX;
	iWinPlacement.rcNormalPosition.top  = pApp->m_SystemValue.nMainSizeY - 27 ;
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;
	iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nMainSizeY - 27 + pApp->m_SystemValue.nLogSizeY;
	SetWindowPlacement(&iWinPlacement);	
	m_bWindowsPos = FALSE;
	return TRUE;
}

BOOL CLogWndDlg::MoveinitPos()
{
	CLLRApp* pApp = (CLLRApp*) AfxGetApp();
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left   =  pApp->m_SystemValue.nLogStartX;
	iWinPlacement.rcNormalPosition.top  = pApp->m_SystemValue.nLogStartY;
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;
	iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY;
	SetWindowPlacement(&iWinPlacement);	
	m_bWindowsPos = TRUE;
	SetTrans(255);
	return TRUE;
}

void CLogWndDlg::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent){
	case 1 :  m_iTrans ++ ; 
		      if ( m_iTrans > 255 ) { 
				  KillTimer(1);
				  MoveHidePos();
				  break;
			  }
		      SetTrans(m_iTrans);
			  break;
	case 2 :  m_iTrans --;
		      if ( m_iTrans < 10 ) { 
				  KillTimer(2);
				  m_iHideCk = 3;
				  MoveHidePos();
				  break;
			  }
		      SetTrans(m_iTrans);
			  break;
	case 3: 
		KillTimer(3);
		RunPrint(0,0);	
		break;
	case 4: 
		KillTimer(4);
		RunMessagePrint(0,0);
		break;
	}	

	CDialog::OnTimer(nIDEvent);
}

void CLogWndDlg::ButtonScroll()
{
	OnButtonScroll();
}

void CLogWndDlg::OnButtonScroll() 
{
//	m_listBox.m_bAttachView = TRUE;
	if ( m_listBox.m_bStopScroll == TRUE ) {
		m_listBox.m_bStopScroll = FALSE;
		GetDlgItem( IDC_BUTTON_SCROLL )->SetWindowText("Scroll");					
		m_listBox.SetFocus();
		//if(!m_listBox.m_bLastView){
			m_listBox.LastMessage();
		//}
		m_listBox.SetCurSel( m_listBox.GetCount() - 1 );
	}
	else if ( m_listBox.m_bStopScroll == FALSE ) {
		m_listBox.m_bStopScroll = TRUE;
		GetDlgItem( IDC_BUTTON_SCROLL )->SetWindowText("Stop Scroll");					
	}
}

void CLogWndDlg::OnBtnMenu() 
{
	// TODO: Add your control notification handler code here
	if ( m_bWindowsPos == TRUE ) 
	{
		MoveHidePos();
	}
	else {
		MoveinitPos();
	}
}
