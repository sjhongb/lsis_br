#if !defined(AFX_LOGTESTDLG_H__4E33E6D5_6589_415A_A722_F0EE37308045__INCLUDED_)
#define AFX_LOGTESTDLG_H__4E33E6D5_6589_415A_A722_F0EE37308045__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogTestDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogTestDlg dialog

class CLogTestDlg : public CDialog
{
// Construction
public:
	CLogTestDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogTestDlg)
	enum { IDD = IDD_DLG_LOGTEST };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogTestDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGTESTDLG_H__4E33E6D5_6589_415A_A722_F0EE37308045__INCLUDED_)
