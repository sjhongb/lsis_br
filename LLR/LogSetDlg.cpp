// LogSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "LogSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg dialog


CLogSetDlg::CLogSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLogSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLogSetDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLogSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogSetDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogSetDlg, CDialog)
	//{{AFX_MSG_MAP(CLogSetDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg message handlers
