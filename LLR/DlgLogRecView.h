//=======================================================
//==              DlgLogRecView.h
//=======================================================
// File Name   : DlgLogRecView.h
// Creator     : OMANI (Do Young Kim)
// Date        : 2011-02-16
// Version     : 0.1.1
// Description :
// History     :
//               2011.02.16 : v0.1.1 Start LLR Project
//
//=======================================================

#ifndef   _DLGLOGRECVIEW_H
#define   _DLGLOGRECVIEW_H

#include "LOGListBox.h"

class CDlgLogRecView : public CDialog
{
public:
	void UpdateListBox();
	CDlgLogRecView(CWnd* pParent = NULL);   // standard constructor

public:
	BOOL	m_bChkButtons;
	BOOL	m_bChkLogDays;
	BOOL	m_bChkOpMsg;
	BOOL	m_bChkOrderNo;
	BOOL	m_bChkRealNo;
	BOOL	m_bChkRecDays;
	BOOL	m_bChkSignals;
	BOOL	m_bChkSwitchs;
	BOOL	m_bChkSystems;
	BOOL	m_bChkTracks;
	BOOL	m_bChkUnitMsg;
	BOOL    m_bOpen;
	CWnd*   m_pParentWnd;
	BOOL    m_bIsCreate;
	int     m_iLogControlAreaX;
	int     m_iLogControlMoveX;
	int     m_iLogControlTempX;
	MsgModeType m_ModeType;
	void OnResizeWindow();
	void ReArrangeWindow();

	// Print
	void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	CPtrArray	m_PrintList;
	LOGPrnConfigType m_PrnConfig;
	CFont m_docFont;
	CFont m_stFont;
	CFont m_headFont;

// Dialog Data
	//{{AFX_DATA(CDlgLogRecView)
	enum { IDD = IDD_DLG_OBJVIEW_FORM };
	CButton	m_cCheckSystemModule;
	CButton	m_cCheckCrossingPoint;
	CButton	m_cCheckSignal;
	CButton	m_cCheckTrack;
	CButton	m_cCheckOthers;
	CButton	m_cCheckOperate;
	CButton	m_cCheckMovement;
	CButton	m_cCheckMsgError;
	CButton	m_cStaticEquipment;
	CButton	m_cStaticEventType;
	CButton	m_cBtnShowControl;
	CLOGListBox	m_listRecView;
	CString	m_strOrder;
	CString	m_strSize;
	CString	m_strTotalNo;
	BOOL	m_bCheckMsgError;
	BOOL	m_bCheckMovement;
	BOOL	m_bCheckOperate;
	BOOL	m_bCheckOthers;
	BOOL	m_bCheckTrack;
	BOOL	m_bCheckSignal;
	BOOL	m_bCheckCrossingPoint;
	BOOL	m_bCheckSystemModule;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgLogRecView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgLogRecView)
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnShowOption();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheckMsgError();
	afx_msg void OnCheckMsgActive();
	afx_msg void OnCheckMsgOperate();
	afx_msg void OnCheckMsgSystemOther();
	afx_msg void OnCheckMsgTrack();
	afx_msg void OnCheckMsgSignal();
	afx_msg void OnCheckMsgSwitch();
	afx_msg void OnCheckMsgSystemModule();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _DLGLOGRECVIEW_H
