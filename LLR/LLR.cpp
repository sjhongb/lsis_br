//=======================================================
//==              LLR.cpp
//=======================================================
//  파 일 명 :  LLR.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================
#include "stdafx.h"
#include "LLR.h"
#include <direct.h>
#include "MainFrm.h"
#include "LLRDoc.h"
#include "LLRView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLLRApp

BEGIN_MESSAGE_MAP(CLLRApp, CWinApp)
	//{{AFX_MSG_MAP(CLLRApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLLRApp construction

CLLRApp::CLLRApp()
{
	// TODO: add construction code here,
	psz_MainTitle = "LLR - LS Log Replay";
	m_pEipEmu = NULL;
	m_bBlockTrackIng = FALSE;
	m_bLeftSideIsUpDirection = TRUE;
	m_pEIPMsgList = new CFMSGList;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CLLRApp object

CLLRApp theApp;
BOOL	_bOnMsgSystem = TRUE;
int		_iLCCNo = 0;
Eid_AppOptionType _AppOptions;

/////////////////////////////////////////////////////////////////////////////
// CLLRApp initialization

//=======================================================
//
//  함 수 명 :  InitInstance()
//  함수출력 :  BOOL
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  EIS 의 초기 설정을 한다. 
//
//=======================================================
BOOL CLLRApp::InitInstance()
{
	// CG: The following block was added by the Splash Screen component.

	{
		CCommandLineInfo cmdInfo;
		ParseCommandLine(cmdInfo);
	}
	TCHAR pszPathName[_MAX_PATH];
	::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH);
	

	pszProfileName = pszPathName;
	pszProfileName.Replace("\\","/");
	for (int k=pszProfileName.GetLength();k>0;k--){
		if (pszProfileName.Mid(k,1)=="/"){ pszProfileName = pszProfileName.Left(k);break;}
	}
	pszProfileName = pszProfileName + "/Station.ini";
	//System Value 초기화 
	OnSystemValueInit();

	//프로그램이 이미 실행중인지 검사 
	HWND pApp = ::FindWindow( NULL, psz_MainTitle );	//find class name and window name. if find child window, use FindWindowEx function.
	if ( pApp ) {
		AfxMessageBox("Warning!\nAnother LLR now running. \nexit program",MB_OK,MB_ICONSTOP);
		return FALSE;
	}

	if (!AfxOleInit()){
		AfxMessageBox(" Wrong LSM version.",MB_OK,MB_ICONSTOP);
		return FALSE;
	}



	AfxEnableControlContainer();

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CLLRDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CLLRView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	
	//After the call, the memory block pointed to by pv is invalid and can no longer be used.
	free( (void*)m_pszProfileName );		
	m_pszProfileName = strdup(pszProfileName);	//Duplicates a string.
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)

	LoadFromINI();

//Load from Registry 
	SetRegistryKey(_T("LSIS"));
	SystemValueLoad();
	m_bLeftSideIsUpDirection = GetProfileInt("LSM","STATION_DIRECTION",1);
	SetFlagOfSyncWithLSM();

	if(!OnLogInit()){		//LOG디렉토리
		return FALSE;
	}

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	
	m_iMsgChangeFlag = FALSE;
	m_bCommOk = FALSE;
	m_bCommSuccess = FALSE;
	m_iFDIM_Err_Backup[0] = -1;
	m_iFDIM_Err_Backup[1] = -1;
	m_iFDOM_Err_Backup[0] = -1;
	m_iFDOM_Err_Backup[1] = -1;
	return TRUE;
}

#include "ScrollStatic.h"
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	void SetDataVer(BYTE dataVer);
	void SetSoftVersionOfIPM(CString strSoftVersionOfIPM);
	void SetSoftVersionOfVDU(CString strSoftVersionOfVDU);
	void SetConnect(CString strConType);
	BYTE m_DataVer;
	CString m_strSoftWareVersionOfVDU;
	CString m_strSoftWareVersionOfIPM;
	CString m_strConType;
    CScrollStatic     m_ScrollStatic;
// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

void CAboutDlg::SetConnect(CString strConType)
{
	m_strConType = strConType;
}

void CAboutDlg::SetDataVer(BYTE dataVer)
{
	m_DataVer = dataVer;
}

void CAboutDlg::SetSoftVersionOfIPM(CString strSoftVersionOfIPM)
{
	m_strSoftWareVersionOfIPM = strSoftVersionOfIPM;
}

void CAboutDlg::SetSoftVersionOfVDU(CString strSoftVersionOfVDU)
{
	m_strSoftWareVersionOfVDU = strSoftVersionOfVDU;
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strDataVer;

	strDataVer.Format("%02X",m_DataVer);
	strDataVer = strDataVer.Left(1) + "." + strDataVer.Right(1);

	GetDlgItem( IDC_STATIC_DATAVER )->SetWindowText(strDataVer);
	GetDlgItem( IDC_STATIC_SOFTVER_IPM )->SetWindowText(m_strSoftWareVersionOfIPM);
	GetDlgItem( IDC_STATIC_SOFTVER_VDU )->SetWindowText(m_strSoftWareVersionOfVDU);
//	GetDlgItem( IDC_STATIC_TYPE )->SetWindowText(m_strConType);
/*
    char szScroll[512];
    memset( szScroll, 0, sizeof(szScroll));
    strcpy( szScroll, "LLR-520III for BR");
    strcat( szScroll, "|"); strcat( szScroll, "|"); 
    strcat( szScroll, "Indoor manager"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "I"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "ShinYoung Cho"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "Software Staff"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "I"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "OMANI"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "Triple J"); 
    strcat( szScroll, "|");
	strcat( szScroll, "|"); 
    strcat( szScroll, "Hardware Staff"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "I"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "HakRae Roh"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "WonHee Lee"); 
    strcat( szScroll, "|"); 
	strcat( szScroll, "|");  
    strcat( szScroll, "( eagle five brothers !! )"); 
    strcat( szScroll, "|"); 
	strcat( szScroll, "|");  
	strcat( szScroll, "|");  
    strcat( szScroll, "Special Thanks"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "I"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "Project Manager : SungMin Cho"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "                              SukJu Jeon "); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "Outdoor staff : YoungBo Kim"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "                       IChang Ha  "); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "                       JaeWoo Yu  "); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "                       Black dog  "); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "LGIS family"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "and"); 
    strcat( szScroll, "|"); 
    strcat( szScroll, "BR"); 
    strcat( szScroll, "|"); 
	strcat( szScroll, "|");  
    strcat( szScroll, "|"); 
	strcat( szScroll, "|");  
    strcat( szScroll, "|"); 
	strcat( szScroll, "|");  

    // delimiter

    m_ScrollStatic.SubclassDlgItem(IDC_ABOUT_STATIC, this);
    m_ScrollStatic.SetCredits(szScroll);
    m_ScrollStatic.SetSpeed(DISPLAY_SLOW);
    m_ScrollStatic.StartScrolling();	
*/		// 스크롤 삭제.

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CLLRApp::OnAppAbout()
{
	DataHeaderType* pHead = (DataHeaderType*)SystemVMEM;
	BYTE dataVer;
	dataVer = pHead->nDataVer;
	CString m_strMsg,strProgramVersionOfIPM,strProgramVersionOfVDU;
	strProgramVersionOfIPM = GetProgramVersionOfIPM();
	strProgramVersionOfVDU = GetProgramVersionOfVDU();
	CAboutDlg aboutDlg;
	aboutDlg.SetDataVer(dataVer);
	aboutDlg.SetSoftVersionOfIPM(strProgramVersionOfIPM);
	aboutDlg.SetSoftVersionOfVDU(strProgramVersionOfVDU);
//	if ( m_iConType == _SERVER ) aboutDlg.SetConnect("SERVER");
//	else if ( m_iConType == _CLIENT ) aboutDlg.SetConnect("CLIENT");
//	else  {
//		m_strMsg.Format("UnKnown %d",m_iConCnt);
//		aboutDlg.SetConnect(m_strMsg);
//	}
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CLLRApp message handlers

CString CLLRApp::GetProgramVersionOfIPM()
{
	BYTE bProgramVersionOfIPM[6];
	CString strSWVersionOfIPM;

	memcpy(&bProgramVersionOfIPM, &SystemVMEM[nSfmRecordSize + MAX_OPERATE_CMD_SIZE + MAX_USER_SIZE + MAX_PASS_SIZE + USER_CMD_SIZE], 6);
	strSWVersionOfIPM.Format("%d.%d.%d.%02d%02d%02d",bProgramVersionOfIPM[0],bProgramVersionOfIPM[1],bProgramVersionOfIPM[2],
														bProgramVersionOfIPM[3],bProgramVersionOfIPM[4],bProgramVersionOfIPM[5]);
	return strSWVersionOfIPM;
}

CString CLLRApp::GetProgramVersionOfVDU()
{
    HRSRC hRsrc = FindResource(NULL, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION);
	CString strVersion;
	strVersion = "0.0.0.0";
    if (hRsrc != NULL)
    {
        HGLOBAL hGlobalMemory = LoadResource(NULL, hRsrc);
        if (hGlobalMemory != NULL)
        {
            void *pVersionResouece = LockResource(hGlobalMemory); 
            void *pVersion;
            UINT uLength;
            // 아래줄에 041204B0는 리소스 파일(*.rc)에서 가져옴.
            // 프로젝트 리소스 파일을 참고하세요(어느부분 참고인지는 밑에 나와있음)
            if( VerQueryValue(pVersionResouece, "StringFileInfo\\041204B0\\FileVersion", &pVersion, &uLength) != 0 )
            {
               // 1, 0, 0, 1로 되어 있는 부분에서 숫자 부분만 가져옴.
                int anVersion[4] = {0,};
                char* pcTemp = strtok( (char *)pVersion, "," );
                for(int inxVersion=0; inxVersion<4; inxVersion++)
                {
                    if(pcTemp  != NULL)
                    {
                        anVersion[inxVersion] = atoi(pcTemp );
                        if(inxVersion != 3)
                        {
                            pcTemp = strtok(NULL, ",");
                        }
                    }
                }
                strVersion.Format("%d.%d.%d.%d", anVersion[0], anVersion[1], anVersion[2], anVersion[3]);
                //AfxMessageBox(strVersion);
            }
            FreeResource(hGlobalMemory);
        }
    }
	return strVersion;
}

void CLLRApp::SystemValueLoad()
{
	m_SystemValue.strCurrentOperator = GetProfileString("Operator","strCurrentOperator","");
	m_SystemValue.strLockOperator = GetProfileString("Operator","strLockOperator","");
	m_SystemValue.nUserNumber = GetProfileInt("USER_ID","NUMBER",0);
	m_SystemValue.nCallonCounter = GetProfileInt("Counter","Callon",0);
	m_SystemValue.nRouteRelease = GetProfileInt("Counter","RouteRelease",0);
	m_SystemValue.nB2AXLCounter = GetProfileInt("Counter","B2AXLreset",0);
	m_SystemValue.nB4AXLCounter = GetProfileInt("Counter","B4AXLreset",0);
	m_SystemValue.nSystemType = GetProfileInt("Common","SYSTYPE",0);
	m_SystemValue.nSystemNo = GetProfileInt("Common","SYSNO",1);
}

void CLLRApp::GetVerifyObject()
{
	m_VerifyObjectForMessage.strStationName = GetProfileString("LSM", "StationName", NULL);
	m_VerifyObjectForMessage.iNumberOfLC = GetProfileInt("LSM", "NumberOfLC", 0);
	m_VerifyObjectForMessage.iNumberOfEPK = GetProfileInt("LSM", "NumberOfEPK", 0);
	
	CString strNameOfBlock;
	m_VerifyObjectForMessage.strBlokState[0] = "FALSE";
	for (int i = 1; i < 17; i++)
	{
		strNameOfBlock.Format("B%d",i);
		
		m_VerifyObjectForMessage.strBlokState[i] = GetProfileString("LSM\\BLOCK", strNameOfBlock, "FALSE");
	}
}

void CLLRApp::SetFlagOfSyncWithLSM()
{
	WriteProfileString("LSM", "FlagOfFinishSync", "FALSE");
}

CString CLLRApp::GetFlagOfSyncWithLSM()
{
	CString strFlag = GetProfileString("LSM", "FlagOfFinishSync", "FALSE");

	return strFlag;
}

void CLLRApp::ParameterLoade()
{
	m_UserValue.lDate = GetProfileInt("Common","DATE",0);
}

void CLLRApp::ParameterReset()
{
	m_UserValue.lDate = GetProfileInt("Common","DATE",0);
}

void CLLRApp::ParameterSave()
{
	WriteProfileInt("Common","DATE",m_UserValue.lDate);
}

void CLLRApp::LoadFromINI()
{	
	char *s;
	CString str;
	//SYSTEM
	str = GetProfileString( _T("SYSTEM"), _T("STATION_FILENAME"), "" );
	if (str != "") {
		m_SystemValue.strStationFileName = str;		//역 화일명
	}
	str = GetProfileString( _T("SYSTEM"), _T("STATION_REALNAME"), "" );
	if (str != "") {
		m_SystemValue.strStationRealName = str;		//역 이름
	}
	//WINDOW
	str = GetProfileString( _T("WINDOW"), _T("LSM_RESOLUTION"), "" );
	if (str != "") {
		m_SystemValue.strLSMResolution = str;		
	}
	str = GetProfileString( _T("WINDOW"), _T("LSM_HEIGHT"), "" );
	if (str != "") {
		m_SystemValue.nLSMHeight = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("MAINWND_STARTX"), "" );
	if (str != "") {
		m_SystemValue.nMainStartX = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("MAINWND_STARTY"), "" );
	if (str != "") {
		m_SystemValue.nMainStartY = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("MAINWND_SIZEX"), "" );
	if (str != "") {
		m_SystemValue.nMainSizeX = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("MAINWND_SIZEY"), "" );
	if (str != "") {
		m_SystemValue.nMainSizeY = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("MENUWND_STARTX"), "" );
	if (str != "") {
		m_SystemValue.nMenuStartX = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nMenuStartX == 0){
			m_SystemValue.nMenuStartX = m_SystemValue.nMainSizeX/2 + m_SystemValue.nMainStartX;
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("MENUWND_STARTY"), "" );
	if (str != "") {
//		m_SystemValue.nMenuStartY = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nMenuStartY == 0){
			m_SystemValue.nMenuStartY = m_SystemValue.nMainSizeY - (int)(m_SystemValue.nMainSizeY/4) + m_SystemValue.nMainStartY;
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("MENUWND_SIZEX"), "" );
	if (str != "") {
		m_SystemValue.nMenuSizeX = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nMenuSizeX == 0){
			m_SystemValue.nMenuSizeX = m_SystemValue.nMainSizeX/2;
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("MENUWND_SIZEY"), "" );
	if (str != "") {
//		m_SystemValue.nMenuSizeY = (short)( strtoul( str, &s, 10 ) );
		if(m_SystemValue.nMenuSizeY == 0){
			m_SystemValue.nMenuSizeY = (int)(m_SystemValue.nMainSizeY/4);
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("LOGWND_STARTX"), "" );
	if (str != "") {
		m_SystemValue.nLogStartX = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("WINDOW"), _T("LOGWND_STARTY"), "" );
	if (str != "") {
//		m_SystemValue.nLogStartY = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nLogStartY == 0){
			m_SystemValue.nLogStartY = m_SystemValue.nLSMHeight-(int)(m_SystemValue.nMainSizeY/4);
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("LOGWND_SIZEX"), "" );
	if (str != "") {
		m_SystemValue.nLogSizeX = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nLogSizeX == 0){
			m_SystemValue.nLogSizeX = m_SystemValue.nMainSizeX/2;
		}
	}
	str = GetProfileString( _T("WINDOW"), _T("LOGWND_SIZEY"), "" );
	if (str != "") {
//		m_SystemValue.nLogSizeY = (short)( strtoul( str, &s, 10 ) );		
		if(m_SystemValue.nLogSizeY == 0){
			m_SystemValue.nLogSizeY = (int)(m_SystemValue.nMainSizeY/4);
		}
	}
	//OPTION
	str = GetProfileString( _T("OPTION"), _T("RUNSIM"), "" );
	if (str != "") {
		m_SystemValue.bSimulate = BOOL( atoi( str ) );
	}

	str = GetProfileString( _T("OPTION"), _T("RUNOPERATE"), "" );
	if (str != "") {
		m_SystemValue.bOperate = BOOL( atoi( str ) );
	}

	str = GetProfileString( _T("OPTION"), _T("RUNSOUND"), "" );
	if (str != "") {
		m_SystemValue.bSound = BOOL( atoi( str ) );
	}

	str = GetProfileString( _T("OPTION"), _T("RUNMSGHELP"), "" );
	if (str != "") {
		m_SystemValue.bMsgHelp = BOOL( atoi( str ) );
	}

	str = GetProfileString( _T("OPTION"), _T("RUNREPLAY"), "" );
	if (str != "") {
		m_SystemValue.bReplay = BOOL( atoi( str ) );
	}
	str = GetProfileString( _T("OPTION"), _T("RUNPROCESS"), "" );
	if (str != "") {
		m_SystemValue.nProcOption = (short)( strtoul( str, &s, 16 ) );	//Convert strings to an unsigned long-integer value.
	}
	str = GetProfileString( _T("OPTION"), _T("LSM_OPTION"), "" );
	if (str != "") {
		m_SystemValue.nOption = (short)( strtoul( str, &s, 16 ) );	//Convert strings to an unsigned long-integer value.
	}
	str = GetProfileString( _T("OPTION"), _T("ANIMENU"), "" );
	if (str == "YES") {
		m_SystemValue.bAnyMenu = TRUE;
	} else m_SystemValue.bAnyMenu = FALSE;
	str = GetProfileString( _T("OPTION"), _T("EXTERNALPOWER"), "" );
	if (str == "2") {
		m_SystemValue.nExternPower = 2;
	} else m_SystemValue.nExternPower = 1;
	str = GetProfileString( _T("OPTION"), _T("TESTMODE"), "FALSE" );
	if (str == "TRUE") {
		m_SystemValue.bTestMode = TRUE;
	} else m_SystemValue.bTestMode = FALSE;
	str = GetProfileString( _T("OPTION"), _T("VOICE"), "" );
	if (str == "YES") {
		m_SystemValue.bVoice = TRUE;
	} else m_SystemValue.bVoice = FALSE;
	str = GetProfileString( _T("OPTION"), _T("LANUSE"), "" );
	if (str == "YES") {
		m_SystemValue.bLanUse = TRUE;
	} else m_SystemValue.bLanUse = FALSE;


	//COM
	str = GetProfileString( _T("COM"), _T("COMPORT1"), "" );
	if (str != "") {
		m_SystemValue.nComport1 = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("COM"), _T("COMPORT2"), "" );
	if (str != "") {
		m_SystemValue.nComport2 = (short)( strtoul( str, &s, 10 ) );		
	}
	str = GetProfileString( _T("COM"), _T("GPSCOMPORT"), "" );
	if (str != "") {
		m_SystemValue.strGPSComport = str;		
	}																//GPS추가.
	str = GetProfileString( _T("COM"), _T("BAUDRATE"), "" );
	if (str != "") {
		m_SystemValue.nBaudrate = (int)( strtoul( str, &s, 10 ) );		
	}

	m_SystemValue.bSharedMemory = FALSE;

	str = GetProfileString( _T("COM"), _T("DISPLAY"), "" );
	if (str == "YES") {
		m_bCommDisplay = TRUE;
	} else  m_bCommDisplay = FALSE;
}

void CLLRApp::OnSystemValueInit()
{
	 m_SystemValue.strStationRealName = "";
	 m_SystemValue.strStationFileName = "";
	 m_SystemValue.strCurrentOperator = "";
	 m_SystemValue.strLockOperator	  = "";
	 m_SystemValue.strLSMResolution	  = "";
	 m_SystemValue.bSimulate		  = FALSE;
	 m_SystemValue.bOperate			  = FALSE;
	 m_SystemValue.bCtrlLock		  = TRUE;
	 m_SystemValue.bSound			  = FALSE;
	 m_SystemValue.bReplay			  = FALSE;
	 m_SystemValue.bMsgHelp			  = FALSE;
	 m_SystemValue.nProcOption		  = 0;
	 m_SystemValue.nOption			  = 0;
	 m_SystemValue.nMainStartX		  = 0;		
	 m_SystemValue.nMainStartY		  = 0;		
	 m_SystemValue.nMainSizeX		  = 0;		
	 m_SystemValue.nMainSizeY		  = 0;	
	 m_SystemValue.nMenuStartX		  = 0;		
	 m_SystemValue.nMenuStartY		  = 0;		
	 m_SystemValue.nMenuSizeX		  = 0;		
	 m_SystemValue.nMenuSizeY		  = 0;	
	 m_SystemValue.nLogStartX		  = 0;		
	 m_SystemValue.nLogStartY		  = 0;		
	 m_SystemValue.nLogSizeX		  = 0;		
	 m_SystemValue.nLogSizeY		  = 0;	
	 m_SystemValue.nLSMHeight		  = 0;	
	 m_SystemValue.nComport1		  = 0;
	 m_SystemValue.nComport2		  = 0;
	 m_SystemValue.strGPSComport	  = "";		//GPS추가.
	 m_SystemValue.nBaudrate		  = 0;
	 m_SystemValue.nUserNumber		  = 0;

}

BOOL CLLRApp::OnLogInit()
{
	if ( !m_pEIPMsgList->Create() ) {	//  Load Msg List and hold.
		return FALSE;
	}
	m_pEIPMsgList->m_bHelpOn = m_SystemValue.bMsgHelp;

 	::GetCurrentDirectory(255, LPTSTR(m_WorkPath));
	CString  Name = "D:\\LOG";

	strcpy(m_LOGDirInfo, Name);

	return TRUE;
}

int CLLRApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	OnSaveUserID();
	delete m_pEIPMsgList;
	return CWinApp::ExitInstance();
}


long CLLRApp::GetSecond(CTime time)
{
	long lTime;
	CTime defTime( 2000, 1, 1, 0, 0, 0 ); // 10:15PM March 19, 1999
	CTimeSpan ts = time - defTime;  // Subtract 2 CTimes
	lTime = ts.GetTotalSeconds();

	return lTime;
}


void CLLRApp::OnLoadUserID()
{
	m_nLimitCount = m_SystemValue.nUserNumber;
	m_UserSection = "USER_ID";
	m_PassSection = "USER_PASS";
	m_Entry = "ID";

	int  i;
	CString strID, strPassword, entry;

	for (i=1; i<=m_nLimitCount; i++) {
		entry.Format("%s%d", m_Entry, i);
		strID = GetProfileString( m_UserSection, entry, _T("") );
		if (strID != "") {
			strPassword = GetProfileString( m_PassSection, strID, _T("password") );
			memcpy ( m_UserValue.UserData[i-1].UserID , strID , 8 );
			memcpy ( m_UserValue.UserData[i-1].UserPassword , strPassword , 8 );
		}
	}
	m_UserValue.DataCounter = m_nLimitCount;
	m_UserValue.lDate = GetProfileInt("Common","DATE",0);
}

void CLLRApp::OnSaveUserID()
{
	m_nLimitCount = m_SystemValue.nUserNumber;
	if ( m_nLimitCount == 0 ) {
		return; // 유저가 한명도 없을경우는 잘못된 것이다.
	}
	m_UserSection = "USER_ID";
	m_PassSection = "USER_PASS";
	m_Entry = "ID";

	char UserID[9];

	int  i, n;
	CString strID, strPassword, entry;

	HKEY	hResult;		//User 목록과 Password목록 동시에 지우기 
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LLR",&hResult);
	RegDeleteKey(hResult,"USER_PASS");
	RegCloseKey(hResult);
	RegOpenKey(HKEY_CURRENT_USER,(LPCTSTR)"Software\\LSIS\\LLR",&hResult);
	CString ttt;
	ttt.Format("%s%d",m_Entry,m_nLimitCount);
	RegDeleteKey(hResult,"USER_ID");
	RegCloseKey(hResult);

	n = m_UserValue.DataCounter;
	for(i=1; i <= n; i++) {		//1부터 시작
		entry.Format("%s%d", m_Entry, i);
		memcpy ( UserID , m_UserValue.UserData[i-1].UserID , 8 );
		UserID[8] = '\0';
		WriteProfileString( m_UserSection, entry, UserID );
		WriteProfileString( m_PassSection, UserID, m_UserValue.UserData[i-1].UserPassword );
	}
	m_SystemValue.nUserNumber = m_UserValue.DataCounter;
	WriteProfileInt("USER_ID","NUMBER",m_UserValue.DataCounter);

	CTime time = CTime::GetCurrentTime();
	m_UserValue.lDate = GetSecond( time );
	WriteProfileInt( "Common", "DATE", m_UserValue.lDate );
}

