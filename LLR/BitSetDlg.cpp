// BitSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "BitSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitSetDlg dialog


CBitSetDlg::CBitSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBitSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBitSetDlg)
	//}}AFX_DATA_INIT
}


void CBitSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitSetDlg)
	DDX_Text(pDX, IDC_EDT_SBIT, m_iStartBit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBitSetDlg, CDialog)
	//{{AFX_MSG_MAP(CBitSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitSetDlg message handlers

BOOL CBitSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBitSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CDialog::OnOK();
}
