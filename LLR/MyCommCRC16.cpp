//=======================================================
//==              MyCommCRC16.cpp
//=======================================================
//  파 일 명 :  MyCommCRC16.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :	2.01
//  설    명 :  시리얼 통신을 처리한다. (CRC 포함)
//
//				이부분 소스 계속적인 Goto 문 사용등 동작은 되나
//				전체적으로 문제의 소지가 있음..
//				개선 가능할경우 검증된 시리얼 소스로 변경 필요.
//=======================================================

#include "stdafx.h"
#include "CRCCCITT.h"
#include "MyCommCRC16.h"
#include "LLRView.h"
#include "../include/ScrInfo.h"

#define COM_CRC	((unsigned short)0x1021

int  CMyCommCRC16::_nPas = 0;

extern int _iLCCNo;

CMyCommCRC16::CMyCommCRC16()
{
	m_PasSetCount = 0;
	m_nRxCRC = 0;
	m_nTxCRC = 0;
	m_nInRxMax = 0;
	m_nTxPtr = 0;
	m_nRxPtr = 0;
	m_nPasBitMask = 0;
	m_bExtRx = FALSE;
	m_bTxReady = FALSE;
	m_ParentWnd = NULL;
	m_nCmdCounter = 0;
	m_bSendToTx = 0;
	m_nPasTick = 0;
	SetCommFail( TRUE );
	SetLimitTick( 5, 19 );
	InitCommTick();
}

CMyCommCRC16::~CMyCommCRC16()
{
	PurgeTxQ();
}

BOOL CMyCommCRC16::CheckCommTick()
{
	if ( m_bConnected == TRUE ) {
		m_nWaitTick++;
		if (m_nWaitTick > m_nFailLimitTick) {
			SetCommFail ( TRUE );
			InitCommTick( );
		}
		m_nPasTick++;
		if (m_nPasTick > m_nPasLimitTick) {
			ClearPas(); 
		}
	}
	
	return m_bCommFail;
}

BOOL CMyCommCRC16::OpenComPort(CView *ParentWnd)
{
	m_ParentWnd = ParentWnd;
	
	InitCommTick();

	m_nPasBitMask = 1 << (m_port - 1);

	if ( CComm::OpenComPort() ) {
		CMyCommCRC16::_nPas |= m_nPasBitMask;
		SetCommFail( FALSE );
		return TRUE;
	}
	else {
		SetCommFail( TRUE );
		return FALSE;
	}
}

void CMyCommCRC16::ClearPas()
{
	CMyCommCRC16::_nPas &= ~m_nPasBitMask; 
	m_nPasTick = 0;
}

void CMyCommCRC16::SetPas()
{
	m_nPasTick = 0;
	CMyCommCRC16::_nPas &= m_nPasBitMask; 
	CMyCommCRC16::_nPas |= m_nPasBitMask; 
}

BOOL CMyCommCRC16::GetPas()
{
	if ( m_bCommFail ) 
		return FALSE;
	else 
		return ((CMyCommCRC16::_nPas & m_nPasBitMask) ? TRUE : FALSE); 
}


void CMyCommCRC16::SetReadData(LPSTR data, int size)
{
	CLLRApp *pApp =  (CLLRApp*)AfxGetApp();
	BYTE *p = (BYTE*)data;
	BOOL bCallParent = FALSE;
	
	static cLastChar = 0;
	int iMessageLength;
	iMessageLength = 0;
	*(p+size) = 0;
	BOOL bMsgFind = FALSE;

	while ( size > 0 ) {
		int nCopyCnt = size;
		if ( nCopyCnt + m_nRxPtr > 1024 ) {
			nCopyCnt = 1024 - m_nRxPtr;
		}
		memcpy( m_pRxBuffer + m_nRxPtr, data, nCopyCnt );
		m_nRxPtr += nCopyCnt;
		data += nCopyCnt;
		int pos = 0;
		int MsgPos = 0;
rescan:
		if ( m_pRxBuffer[ pos ] != COM_STX ) {
			while ( pos < m_nRxPtr ) {
				if ( m_pRxBuffer[ pos ] == COM_STX ) break;
				pos++;
			}
		}
		MsgPos = 0;
		bMsgFind = FALSE;

		int nRemain = m_nRxPtr - pos;
		if ( nRemain > 0 && pos ) {
			memcpy( m_pRxBuffer, m_pRxBuffer + pos, nRemain );
		}

		m_nRxPtr -= pos;
		unsigned short m_nTestRxCRC = 0;
		if ( m_nRxPtr < 0 ) m_nRxPtr = 0;
		if ( m_nRxPtr > 2 ) {
			if ( m_pRxBuffer[0] == COM_STX ) {
				if ( m_pRxBuffer[1] == 0x1D || m_pRxBuffer[1] == 0x2D || m_pRxBuffer[1] == 0x3D || m_pRxBuffer[1] == 'D') {
					if ( m_pRxBuffer[1] == 0x1D ) _iLCCNo = 1;
					if ( m_pRxBuffer[1] == 0x2D ) _iLCCNo = 2;
					if ( m_pRxBuffer[1] == 0x3D ) _iLCCNo = 3;
					if ( m_pRxBuffer[1] == 'D'  ) _iLCCNo = 0;
						// TX Write
					if ( m_bTxReady && !m_bSendToTx ) {
						m_bSendToTx = 1;
					}
					if ( m_nRxPtr >= pApp->nComRecordSize + 4 ) {
						m_nRxCRC = 0; 
						// CRC 체크를 2번째 비트 부터 하기로 했다. 
						for ( int loc = 2; loc < pApp->nComRecordSize + 2; loc++ ) {

							m_nRxCRC = crc_CCITT( m_pRxBuffer[loc], m_nRxCRC );
							m_nTestRxCRC = crc_CCITT( m_pRxBuffer[loc], m_nTestRxCRC );
						}

						if ( ( m_nTestRxCRC%256 == m_pRxBuffer[pApp->nComRecordSize+2] ) && ( m_pRxBuffer[pApp->nComRecordSize+3] == m_nTestRxCRC/256 )) { 
							m_nPasTick = 0;
							SetCommFail ( FALSE );
							InitCommTick();
							if ( m_ParentWnd ) {
								if (m_pRxBuffer[2] != 0x0A) {
									bCallParent = FALSE; //0x05 StanBy
									pApp->m_iCommError = 2;
									m_ParentWnd->PostMessage( WM_COMMAND, ID_COMM_ERRMSG, NULL );
								} else {
									bCallParent = TRUE;  //0x0A Main ( 온라인 상태 ) Vpor 출력 검사 
									pApp->m_iCommError = 0;
								}
								memcpy(&m_pDataQ[0], &m_pRxBuffer[1], pApp->nComRecordSize+1);
								m_PostDB.Port = m_port;
								m_PostDB.Length = pApp->nComRecordSize;
								m_PostDB.pData = &m_pDataQ[0];
								if ( bCallParent ) {
									//-> 메세지 관련 추가 
									m_ParentWnd->PostMessage( WM_COMMAND, ID_COMM_WATCH, (LPARAM)&m_PostDB );
								} else {
									//m_ParentWnd->PostMessage( WM_COMMAND, ID_COMM_ERRMSG, NULL );
								}
							}

							pos = pApp->nComRecordSize + 4;
							int nRemain = m_nRxPtr - pos;
							if ( nRemain > 0 ) {
								memcpy( m_pRxBuffer, m_pRxBuffer + pos, nRemain );
							}
							m_nRxPtr -= pos;
							m_bSendToTx = 0;

							BYTE bRealTxQ[4096];
							memset(&bRealTxQ, 0, sizeof(bRealTxQ));
							bRealTxQ[0] = 0x7E;
							
							memcpy(&bRealTxQ[1], &m_pTxQ[0], LCC_COM_SIZE);
							
							// 실제 COM 포트에 쏘는곳...
							if ( _iLCCNo != 3 ) {
								CComm::SendData(&bRealTxQ[0], LCC_COM_SIZE + 1);
							} else if (_iLCCNo == 3) { // 메세지 출력은 MMCR 에서만 한다.
 								// SB 에서 전송하는 메세지가 어디에 있는지 찾는다.
								if ( bRealTxQ[2] == WS_OPMSG_SETTIMER)
								{
									CComm::SendData(&bRealTxQ[0], LCC_COM_SIZE + 1);
								}
								MsgPos = 0;
								while ( MsgPos < m_nRxPtr ) {
									if ( m_pRxBuffer[ MsgPos ] == COM_STX && m_pRxBuffer[ MsgPos+1 ] == 0x3D && m_pRxBuffer[ MsgPos+2 ] == WS_OPMSG_MESSAGE) {
										bMsgFind = TRUE;
										break;
									}
									MsgPos++;
								}
								// SB에서 보낸 메세지가 있으면..
								if ( bMsgFind == TRUE ) {
									// SB 의 메세지를 받기 위한 로직 --> 여기 부터
									iMessageLength = 12;
									memcpy(&m_pMsgQ[0], &m_pRxBuffer[MsgPos+3], iMessageLength+1);
									m_MsgDB.Port = m_port;
									m_MsgDB.Length = iMessageLength;
									m_MsgDB.pData = &m_pMsgQ[0];

									if ( m_ParentWnd ) {
										m_ParentWnd->PostMessage( WM_COMMAND, ID_COMM_PRINTMSG, (LPARAM)&m_MsgDB );
									}

								}
								//-> 메세지 관련 추가 
							}
							pApp->m_bCommSuccess = TRUE;
							pApp->m_iCommError = 0;
							m_nCmdCounter++;
							// may be
							goto rescan;
						} else {
							m_pRxBuffer[0] = 0; // 무한루프 방지위해 클리어
							pos = 2;
							pApp->m_iCommError = 1;
							m_ParentWnd->PostMessage( WM_COMMAND, ID_COMM_ERRMSG, NULL );
							goto rescan;
						}
					}
				} else if (m_pRxBuffer[1] == 'P' && m_nRxPtr > 3 ) {
					m_pRxBuffer[0] = 0; // 무한루프 방지위해 클리어
					SetPas();
					pos = 2;
					goto rescan;
				} else {
					m_pRxBuffer[0] = 0; // 무한루프 방지위해 클리어
					pos = 1;
					goto rescan;
				}
			} else {
				pos = 0;
			}
		}
		size -= nCopyCnt;
	}
	return;
}

BOOL CMyCommCRC16::SendData(BYTE* data, int len, BYTE stx , BOOL bCmd)
{

	int  n ;
	BYTE c;
	BYTE *p = &m_pTxQ[0];

	m_bTxReady = FALSE;
	// Init CRC
	m_nTxCRC = 0;
	// STX
	memset(m_pTxQ, 0, LCC_COM_SIZE );
	m_pTxQ[0] = stx;	//COM_STX;
	*data++;
	if ( stx != COM_STX ) {
		GetTxCRC16( stx );
	}
	n = 1;

	for(; n < LCC_COM_SIZE - 2;) {
		c = *data++;
		GetTxCRC16( c );
		m_pTxQ[n++] = c;
	}

	// CRC1
	c = m_TxCRCLow;
	m_pTxQ[n++] = c;
	*data++;
	// CRC2
	c = m_TxCRCHigh;
	m_pTxQ[n++] = c;
	*data++;
	// ETX
	m_nTxPtr = n;

	if ( stx != COM_STX ) {
		m_bTxReady = TRUE;
	}

	if ( bCmd == FALSE ) {
		m_nCmdCounter = 0;
	}

	return TRUE;
}

void CMyCommCRC16::PurgeRxQ()
{
	m_nMode		= 0;
	m_nRxPtr	= 0;
	m_nInRxMax	= 0;
	m_nRxCRC	= 0;
}

void CMyCommCRC16::PurgeTxQ()
{
	m_bTxReady	= FALSE;
	m_nTxPtr	= 0;
	m_nTxCRC	= 0;
}

void CMyCommCRC16::Poll()
{
}

void CMyCommCRC16::GetRxCRC16(BYTE c)
{
	unsigned short data = c;
	m_nRxCRC = crc_CCITT( c, m_nRxCRC );
}

void CMyCommCRC16::GetTxCRC16(BYTE c)
{
	unsigned short data = c;
	m_nTxCRC = crc_CCITT( c, m_nTxCRC );
}

