// CLLRDoc.h : interface of the CLLRDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LLRDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_)
#define AFX_LLRDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../Include/LOGFile.h"

class CLLRDoc : public CDocument
{
protected: // create from serialization only
	CLLRDoc();
	DECLARE_DYNCREATE(CLLRDoc)

// Attributes
public:
	CLOGFile*	m_pViewLOGFile;
	CJTime		m_ReplayTime;
	int			m_nVector;
	int			m_nSpeed;
	long		m_lReplayforSeek;
	CMessageInfo *Info_pScrMessageInfo;

// Operations
public:
	void ReleaseLogFile();
	BOOL InitReplayRecord();
	BOOL UpdateReplayRecord(int nVector);

	CLOGRecord *GetNextRecord();
	void CLLRDoc::SetLogFile(CString strLogFile);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLLRDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:

	virtual ~CLLRDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLLRDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LLRDOC_H__8D49CF48_D853_47B3_8009_AAEB0500269D__INCLUDED_)
