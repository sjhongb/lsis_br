#if !defined(AFX_SYSINITDLG_H__068FE309_E59A_4D9A_943F_DB48C3EDAD00__INCLUDED_)
#define AFX_SYSINITDLG_H__068FE309_E59A_4D9A_943F_DB48C3EDAD00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sysinitdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSysinitdlg dialog

class CSysinitdlg : public CDialog
{
// Construction
public:
	CSysinitdlg(CWnd* pParent = NULL);   // standard constructor
	void SetMessage( CString strMsg );
	void OK() ;
// Dialog Data
	//{{AFX_DATA(CSysinitdlg)
	enum { IDD = IDD_DLG_SYSTEMINIT };
	CProgressCtrl	m_Progress;
	CString	m_strMessage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSysinitdlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSysinitdlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSINITDLG_H__068FE309_E59A_4D9A_943F_DB48C3EDAD00__INCLUDED_)
