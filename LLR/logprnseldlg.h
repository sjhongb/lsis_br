// LOGPrnSelDlg.h : header file
//

#ifndef    __LOGPRNSELDLG_H
#define    __LOGPRNSELDLG_H

#include "LLR.h"
#include "../include/LOGPrnConfig.h"

/////////////////////////////////////////////////////////////////////////////
// CLOGPrnSelDlg dialog

class CLOGPrnSelDlg : public CDialog
{
// Construction
public:
	CLOGPrnSelDlg(BOOL bPrnType = TRUE, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLOGPrnSelDlg)
	enum { IDD = IDD_DLG_SELECT_MSG_SEARCHTYPE };
	CEdit	m_editEYear;
	CEdit	m_editESec;
	CEdit	m_editEMonth;
	CEdit	m_editEMin;
	CEdit	m_editEHour;
	CEdit	m_editEDay;
	CSpinButtonCtrl	m_spinSYear;
	CSpinButtonCtrl	m_spinSSec;
	CSpinButtonCtrl	m_spinSMonth;
	CSpinButtonCtrl	m_spinSMin;
	CSpinButtonCtrl	m_spinSHour;
	CSpinButtonCtrl	m_spinSDay;
	CSpinButtonCtrl	m_spinEYear;
	CSpinButtonCtrl	m_spinESec;
	CSpinButtonCtrl	m_spinEMonth;
	CSpinButtonCtrl	m_spinEMin;
	CSpinButtonCtrl	m_spinEHour;
	CSpinButtonCtrl	m_spinEDay;
	UINT	m_nEDay;
	UINT	m_nEHour;
	UINT	m_nEMin;
	UINT	m_nEMonth;
	UINT	m_nESec;
	UINT	m_nEYear;
	UINT	m_nSDay;
	UINT	m_nSHour;
	UINT	m_nSMin;
	UINT	m_nSMonth;
	UINT	m_nSSec;
	UINT	m_nSYear;
	int		m_nPrnFormMode;
	//}}AFX_DATA

// Attribute
public:
	BOOL m_bPrnType;
	LOGPrnConfigType m_Config;
	int  m_iDlgType;

public:
	void ShowPrintModeItem( BOOL bShow );
	void EnablePrintModeItem( BOOL bEnable );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLOGPrnSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLOGPrnSelDlg)
	afx_msg void OnButtonCallSelMsgType();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditPrnSyear();
	afx_msg void OnKillfocusEditPrnSmon();
	afx_msg void OnKillfocusEditPrnEyear();
	afx_msg void OnKillfocusEditPrnEmon();
	afx_msg void OnKillfocusEditPrnSday();
	afx_msg void OnKillfocusEditPrnEday();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // __LOGPRNSELDLG_H
