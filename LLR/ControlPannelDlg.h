#if !defined(AFX_CONTROLPANNELDLG_H__ACC6DF61_3070_4538_BB77_499AC41417D6__INCLUDED_)
#define AFX_CONTROLPANNELDLG_H__ACC6DF61_3070_4538_BB77_499AC41417D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ControlPannelDlg.h : header file
//
#include "XPStyleButtonST.h"
#include "MatrixStatic.h"

/////////////////////////////////////////////////////////////////////////////
// CControlPannelDlg dialog

class CControlPannelDlg : public CDialog
{
// Construction
public:
	CControlPannelDlg(CWnd* pParent = NULL);   // standard constructor
	CJTime m_ReplayTime;
	void LogDateDisplay(CString strDate);

	CString m_strPathName;
	BOOL m_bPlayStatus;
	BOOL m_bDisplayFlag;
	BOOL m_bEditFlag;
	BOOL m_bSpecialPlay;


	int m_iMoveYear;
	int m_iMoveMonth;
	int m_iMoveDay;
	int m_iMoveHour;
	int m_iMoveMin;
	int m_iMoveSec;
// Dialog Data
	//{{AFX_DATA(CControlPannelDlg)
	enum { IDD = IDD_DLG_CONTROL_PANNEL };
	CXPStyleButtonST m_cBtnExit;
	CXPStyleButtonST m_cBtnTimeSet;
	CXPStyleButtonST m_cBtnPause;
	CXPStyleButtonST m_cBtnBackFF;
	CXPStyleButtonST m_cBtnStop;
	CXPStyleButtonST m_cBtnFrontPlay;
	CXPStyleButtonST m_cBtnFrontFF;
	CXPStyleButtonST m_cBtnOpen;
	CXPStyleButtonST m_cBtnLogView;
	CSpinButtonCtrl m_cSpinYear;
	CSpinButtonCtrl m_cSpinMonth;
	CSpinButtonCtrl m_cSpinDay;
	CSpinButtonCtrl m_cSpinHour;
	CSpinButtonCtrl m_cSpinMinute;
	CSpinButtonCtrl m_cSpinSecond;
	CMatrixStatic	m_cStaticCurTime;
	CMatrixStatic	m_cStaticLogTime;
	CSliderCtrl		m_cFrameSkip;
	CSliderCtrl		m_cReplaySpeed;
	CEdit m_cReplayYear;
	CEdit m_cReplayMonth;
	CEdit m_cReplayDay;
	CEdit m_cReplayHour;
	CEdit m_cReplayMinute;
	CEdit m_cReplaySecond;
	int m_iReplayYear;
	int m_iReplayMonth;
	int m_iReplayDay;
	int m_iReplayHour;
	int m_iReplayMinute;
	int m_iReplaySecond;
	int m_iFrameSkip;
	float m_fReplaySpeed;
	int m_iSliderSpeedNumber;
	BOOL m_bPlay;
	int m_iReplaySpeed;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlPannelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CControlPannelDlg)
	afx_msg void OnBtnExit();
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnPlay();
	afx_msg void OnBtnSettime();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnPause();
	afx_msg void OnBtnBack();
	afx_msg void OnBtnFront();
	afx_msg void OnBtnStop();
	afx_msg void OnBtnViewLog();
	afx_msg void OnBtnOpen();
	afx_msg void OnReleasedcaptureSliderFrameSkip(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSliderSpeed(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusEdtYear();
	afx_msg void OnKillfocusEdtYear();
	afx_msg void OnSetfocusEdtMonth();
	afx_msg void OnKillfocusEdtMonth();
	afx_msg void OnSetfocusEdtDay();
	afx_msg void OnKillfocusEdtDay();
	afx_msg void OnSetfocusEdtHour();
	afx_msg void OnKillfocusEdtHour();
	afx_msg void OnSetfocusEdtMinute();
	afx_msg void OnKillfocusEdtMinute();
	afx_msg void OnSetfocusEdtSecond();
	afx_msg void OnKillfocusEdtSecond();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLPANNELDLG_H__ACC6DF61_3070_4538_BB77_499AC41417D6__INCLUDED_)
