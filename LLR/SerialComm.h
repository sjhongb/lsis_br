#if !defined(AFX_SERIALCOMM_H__60027FC8_60A9_418A_B420_099A0CA6596C__INCLUDED_)
#define AFX_SERIALCOMM_H__60027FC8_60A9_418A_B420_099A0CA6596C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerialComm.h : header file
//
#define MAX_BUFFER_LEN	4096

/////////////////////////////////////////////////////////////////////////////
// CSerialComm thread

class CSerialComm : public CWinThread
{
	DECLARE_DYNCREATE(CSerialComm)
protected:
	CSerialComm();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	BOOL Start(CString strPort, UINT nBaudRate, UINT nParity, UINT nDataBit, UINT nStopBit, UINT bFlowCtrl);
	void End();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialComm)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	virtual BOOL SendMsg(BYTE *pBuffer, USHORT nMsgLen);
	virtual	void ReceiveMsg(BYTE *pBuffer, USHORT &nMsgLen);

	// Implementation
protected:
	void DecodeDLE(BYTE *pDleMsg, USHORT nDleLen, BYTE *pRawMsg, USHORT *pnRawLen);
	void EncodeDLE(BYTE *pRawMsg, USHORT nRawLen, BYTE *pDleMsg, USHORT *pnDleLen);
	WORD CalcCRC16(BYTE *pMsg, USHORT nLen);
	virtual ~CSerialComm();

	// Generated message map functions
	//{{AFX_MSG(CSerialComm)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	BOOL	m_bIsRunning;
	HANDLE	m_hComDev;

	OVERLAPPED	m_OLR;
	OVERLAPPED	m_OLW;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCOMM_H__60027FC8_60A9_418A_B420_099A0CA6596C__INCLUDED_)
