//=======================================================
//==              LOGPrnSelDlg.cpp
//=======================================================
//  파 일 명 :  LOGPrnSelDlg.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LLR.h"
#include "MsgTypeSelDLg.h"
#include "LOGPrnSelDlg.h"
#include "../include/LOGPrnConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLOGPrnSelDlg dialog


CLOGPrnSelDlg::CLOGPrnSelDlg(BOOL bPrnType /*= TURE*/, CWnd* pParent /*=NULL*/)
	: CDialog(CLOGPrnSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLOGPrnSelDlg)
	m_nEDay   = 1;
	m_nEHour  = 0;
	m_nEMin   = 0;
	m_nEMonth = 1;
	m_nESec   = 0;
	m_nEYear  = 2000;
	m_nSDay   = 1;
	m_nSHour  = 0;
	m_nSMin   = 0;
	m_nSMonth = 1;
	m_nSSec   = 0;
	m_nSYear  = 2000;
	//}}AFX_DATA_INIT

	m_bPrnType = bPrnType;
}


void CLOGPrnSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLOGPrnSelDlg)
	DDX_Control(pDX, IDC_EDIT_PRN_EYEAR, m_editEYear);
	DDX_Control(pDX, IDC_EDIT_PRN_ESEC, m_editESec);
	DDX_Control(pDX, IDC_EDIT_PRN_EMON, m_editEMonth);
	DDX_Control(pDX, IDC_EDIT_PRN_EMIN, m_editEMin);
	DDX_Control(pDX, IDC_EDIT_PRN_EHOUR, m_editEHour);
	DDX_Control(pDX, IDC_EDIT_PRN_EDAY, m_editEDay);
	DDX_Control(pDX, IDC_SPIN_PRN_SYEAR, m_spinSYear);
	DDX_Control(pDX, IDC_SPIN_PRN_SSEC, m_spinSSec);
	DDX_Control(pDX, IDC_SPIN_PRN_SMON, m_spinSMonth);
	DDX_Control(pDX, IDC_SPIN_PRN_SMIN, m_spinSMin);
	DDX_Control(pDX, IDC_SPIN_PRN_SHOUR, m_spinSHour);
	DDX_Control(pDX, IDC_SPIN_PRN_SDAY, m_spinSDay);
	DDX_Control(pDX, IDC_SPIN_PRN_EYEAR, m_spinEYear);
	DDX_Control(pDX, IDC_SPIN_PRN_ESEC, m_spinESec);
	DDX_Control(pDX, IDC_SPIN_PRN_EMON, m_spinEMonth);
	DDX_Control(pDX, IDC_SPIN_PRN_EMIN, m_spinEMin);
	DDX_Control(pDX, IDC_SPIN_PRN_EHOUR, m_spinEHour);
	DDX_Control(pDX, IDC_SPIN_PRN_EDAY, m_spinEDay);
	DDX_Text(pDX, IDC_EDIT_PRN_EDAY, m_nEDay);
	DDV_MinMaxUInt(pDX, m_nEDay, 1, 31);
	DDX_Text(pDX, IDC_EDIT_PRN_EHOUR, m_nEHour);
	DDV_MinMaxUInt(pDX, m_nEHour, 0, 23);
	DDX_Text(pDX, IDC_EDIT_PRN_EMIN, m_nEMin);
	DDV_MinMaxUInt(pDX, m_nEMin, 0, 59);
	DDX_Text(pDX, IDC_EDIT_PRN_EMON, m_nEMonth);
	DDV_MinMaxUInt(pDX, m_nEMonth, 1, 12);
	DDX_Text(pDX, IDC_EDIT_PRN_ESEC, m_nESec);
	DDV_MinMaxUInt(pDX, m_nESec, 0, 59);
	DDX_Text(pDX, IDC_EDIT_PRN_EYEAR, m_nEYear);
	DDV_MinMaxUInt(pDX, m_nEYear, 2000, 2089);
	DDX_Text(pDX, IDC_EDIT_PRN_SDAY, m_nSDay);
	DDV_MinMaxUInt(pDX, m_nSDay, 1, 31);
	DDX_Text(pDX, IDC_EDIT_PRN_SHOUR, m_nSHour);
	DDV_MinMaxUInt(pDX, m_nSHour, 0, 23);
	DDX_Text(pDX, IDC_EDIT_PRN_SMIN, m_nSMin);
	DDV_MinMaxUInt(pDX, m_nSMin, 0, 59);
	DDX_Text(pDX, IDC_EDIT_PRN_SMON, m_nSMonth);
	DDV_MinMaxUInt(pDX, m_nSMonth, 1, 12);
	DDX_Text(pDX, IDC_EDIT_PRN_SSEC, m_nSSec);
	DDV_MinMaxUInt(pDX, m_nSSec, 0, 59);
	DDX_Text(pDX, IDC_EDIT_PRN_SYEAR, m_nSYear);
	DDV_MinMaxUInt(pDX, m_nSYear, 2000, 2089);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLOGPrnSelDlg, CDialog)
	//{{AFX_MSG_MAP(CLOGPrnSelDlg)
	ON_BN_CLICKED(IDC_BUTTON_CALL_SEL_MSGTYPE, OnButtonCallSelMsgType)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_SYEAR, OnKillfocusEditPrnSyear)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_SMON, OnKillfocusEditPrnSmon)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_EYEAR, OnKillfocusEditPrnEyear)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_EMON, OnKillfocusEditPrnEmon)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_SDAY, OnKillfocusEditPrnSday)
	ON_EN_KILLFOCUS(IDC_EDIT_PRN_EDAY, OnKillfocusEditPrnEday)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLOGPrnSelDlg message handlers

void CLOGPrnSelDlg::OnButtonCallSelMsgType() 
{
	CMsgTypeSelDLg	Dlg;

	Dlg.m_ModeType = m_Config.Type;
	if ( Dlg.DoModal() == IDOK ) 
	{
		m_Config.Type = Dlg.m_ModeType;
	}
}

void CLOGPrnSelDlg::OnOK() 
{
	UpdateData( TRUE );

	CJTime T;

	if ( m_iDlgType == 2 ) {
		// Start time.
		T.nSec  = m_nSSec;
		T.nMin  = m_nSMin;
		T.nHour = m_nSHour;
		T.nDate = m_nSDay;
		T.nMonth = m_nSMonth;
		T.nYear  = m_nSYear - CJTIME_BASE_YEAR;	// 2000 년이 기본 0 값.
		m_Config.STime = T;
		// End Time
		T.nSec  = m_nESec;
		T.nMin  = m_nEMin;
		T.nHour = m_nEHour;
		T.nDate = m_nSDay;
		T.nMonth = m_nSMonth;
		T.nYear  = m_nSYear - CJTIME_BASE_YEAR;	// 2000 년이 기본 0 값.
		m_Config.ETime = T;
	} else {
		// Start time.
		T.nSec  = m_nSSec;
		T.nMin  = m_nSMin;
		T.nHour = m_nSHour;
		T.nDate = m_nSDay;
		T.nMonth = m_nSMonth;
		T.nYear  = m_nSYear - CJTIME_BASE_YEAR;	// 2000 년이 기본 0 값.
		m_Config.STime = T;
		// End Time
		T.nSec  = m_nESec;
		T.nMin  = m_nEMin;
		T.nHour = m_nEHour;
		T.nDate = m_nEDay;
		T.nMonth = m_nEMonth;
		T.nYear  = m_nEYear - CJTIME_BASE_YEAR;	// 2000 년이 기본 0 값.
		m_Config.ETime = T;
	}
	// DB Set Mode
	m_Config.bFirst = FALSE;
	
	CDialog::OnOK();
}

BOOL CLOGPrnSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CJTime T;
	// Start Time.
	T = m_Config.STime;
	m_nSSec  = T.nSec;
	m_nSMin  = T.nMin;
	m_nSHour = T.nHour;
	m_nSDay  = T.nDate;
	m_nSMonth = T.nMonth;
	m_nSYear  = T.GetYear();

	// spin setup
	m_spinSSec.SetRange( 0, 59 );
	m_spinSMin.SetRange( 0, 59 );
	m_spinSHour.SetRange( 0, 23 );
	m_spinSDay.SetRange( 1, 31 );
	m_spinSMonth.SetRange( 1, 12 );
	m_spinSYear.SetRange( 2005, 2089 );
//  	m_spinSYear.SetRange( CJTIME_BASE_YEAR, 2089 );
	// End Time
//	if ( m_bPrnType ) {
	//T = m_Config.ETime;
	T = m_Config.ETime;
	m_nESec  = T.nSec;
	m_nEMin  = T.nMin;
	m_nEHour = T.nHour;
	m_nEDay  = T.nDate;
	m_nEMonth = T.nMonth;
	m_nEYear  = T.GetYear();
	// spin setup
	m_spinESec.SetRange( 0, 59 );
	m_spinEMin.SetRange( 0, 59 );
	m_spinEHour.SetRange( 0, 23 );
	m_spinEDay.SetRange( 1, 31 );
	m_spinEMonth.SetRange( 1, 12 );
	m_spinEYear.SetRange( 2005, 2089 );
//	m_spinEYear.SetRange( CJTIME_BASE_YEAR, 2089 );

	if ( m_iDlgType == 2 ) {
		SetWindowText ( "input print date" );
		GetDlgItem( IDC_STATIC_STIME_BOX )->SetWindowText("Print date");

		ShowPrintModeItem( FALSE );

//		WINDOWPLACEMENT iBoxPlacement;
//		RECT rect;

//		GetWindowRect( &rect );
//		GetWindowPlacement(&iBoxPlacement);	
//		iBoxPlacement.rcNormalPosition.left   = rect.left;
//		iBoxPlacement.rcNormalPosition.right  = rect.right;
//		iBoxPlacement.rcNormalPosition.top    = rect.top;
//		iBoxPlacement.rcNormalPosition.bottom = rect.top + ((rect.bottom - rect.top) / 2) + 20;
//		SetWindowPlacement(&iBoxPlacement);

	}
	// View Title 
	
//	}
/*
	else {
		ShowPrintModeItem( FALSE );
		SetWindowText( "Indication message environment establishment window" );

		WINDOWPLACEMENT iBoxPlacement;
		RECT rect;

		GetWindowRect( &rect );
		GetWindowPlacement(&iBoxPlacement);	
		iBoxPlacement.rcNormalPosition.left   = rect.left;
		iBoxPlacement.rcNormalPosition.right  = rect.right;
		iBoxPlacement.rcNormalPosition.top    = rect.top;
		iBoxPlacement.rcNormalPosition.bottom = rect.top + ((rect.bottom - rect.top) / 2) + 20;
		SetWindowPlacement(&iBoxPlacement);
	}
*/
	m_Config.Type.bModeError = 1;
	m_Config.Type.bModeILocking = 1;
	m_Config.Type.bModeMessage = 1;
	m_Config.Type.bModeOperate = 1;
	m_Config.Type.bTypeSignal = 1;
	m_Config.Type.bTypeSwitch = 1;
	m_Config.Type.bTypeSystem = 1;
	m_Config.Type.bTypeTrack = 1;
	
	EnablePrintModeItem( TRUE );
	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLOGPrnSelDlg::ShowPrintModeItem( BOOL bShow )
{
	// Box View
//	GetDlgItem( IDC_STATIC_ETIME_BOX )->ShowWindow( bShow );
//	GetDlgItem( IDC_STATIC_PRNTYPE_BOX )->ShowWindow( bShow );
	// End Time View/
//	m_editESec.ShowWindow( bShow );
//	m_editEMin.ShowWindow( bShow );
//	m_editEHour.ShowWindow( bShow );
	m_editEDay.ShowWindow( bShow );
	m_editEMonth.ShowWindow( bShow );
	m_editEYear.ShowWindow( bShow );
	// Spin view
//	m_spinESec.ShowWindow( bShow );
//	m_spinEMin.ShowWindow( bShow );
//	m_spinEHour.ShowWindow( bShow );
	m_spinEDay.ShowWindow( bShow );
	m_spinEMonth.ShowWindow( bShow );
	m_spinEYear.ShowWindow( bShow );

//	GetDlgItem( IDC_EDIT_PRN_SHOUR )->ShowWindow( bShow );
//	GetDlgItem( IDC_EDIT_PRN_SMIN )->ShowWindow( bShow );
//	GetDlgItem( IDC_EDIT_PRN_SSEC )->ShowWindow( bShow );

//	GetDlgItem( IDC_SPIN_PRN_SHOUR )->ShowWindow( bShow );
//	GetDlgItem( IDC_SPIN_PRN_SMIN )->ShowWindow( bShow );
//	GetDlgItem( IDC_SPIN_PRN_SSEC )->ShowWindow( bShow );

	GetDlgItem( IDC_STATIC_EY )->ShowWindow( bShow );
	GetDlgItem( IDC_STATIC_EM )->ShowWindow( bShow );
	GetDlgItem( IDC_STATIC_ED )->ShowWindow( bShow );
}

void CLOGPrnSelDlg::EnablePrintModeItem( BOOL bEnable )
{
	if ( bEnable ) {
		GetDlgItem( IDC_STATIC_STIME_BOX )->SetWindowText("Start time");
		GetDlgItem( IDC_STATIC_ETIME_BOX )->SetWindowText("End time");
	}
	else {
		GetDlgItem( IDC_STATIC_STIME_BOX )->SetWindowText("At the basic date");
		GetDlgItem( IDC_STATIC_ETIME_BOX )->SetWindowText("");
	}
	// End Time View
	m_editESec.EnableWindow( bEnable );
	m_editEMin.EnableWindow( bEnable );
	m_editEHour.EnableWindow( bEnable );
	m_editEDay.EnableWindow( bEnable );
	m_editEMonth.EnableWindow( bEnable );
	m_editEYear.EnableWindow( bEnable );
	// Spin view
	m_spinESec.EnableWindow( bEnable );
	m_spinEMin.EnableWindow( bEnable );
	m_spinEHour.EnableWindow( bEnable );
	m_spinEDay.EnableWindow( bEnable );
	m_spinEMonth.EnableWindow( bEnable );
	m_spinEYear.EnableWindow( bEnable );
	// Start Disable
//	if ( m_nPrnFormMode == 0 ) {
//		GetDlgItem( IDC_EDIT_PRN_SSEC )->EnableWindow( TRUE );
//		GetDlgItem( IDC_SPIN_PRN_SSEC )->EnableWindow( TRUE );
//	}
}






void CLOGPrnSelDlg::OnKillfocusEditPrnSyear() 
{
	CString strYear;
	GetDlgItem( IDC_EDIT_PRN_SYEAR )->GetWindowText( strYear );
	GetDlgItem( IDC_EDIT_PRN_EYEAR )->SetWindowText( strYear );	
}


void CLOGPrnSelDlg::OnKillfocusEditPrnEyear() 
{
	// TODO: Add your control notification handler code here
	CString strYear;
	GetDlgItem( IDC_EDIT_PRN_EYEAR )->GetWindowText( strYear );
	GetDlgItem( IDC_EDIT_PRN_SYEAR )->SetWindowText( strYear );		
}

void CLOGPrnSelDlg::OnKillfocusEditPrnSmon() 
{
	// TODO: Add your control notification handler code here
	CString strMon;
	GetDlgItem( IDC_EDIT_PRN_SMON )->GetWindowText( strMon );
	GetDlgItem( IDC_EDIT_PRN_EMON )->SetWindowText( strMon );		
}


void CLOGPrnSelDlg::OnKillfocusEditPrnEmon() 
{
	// TODO: Add your control notification handler code here
	CString strMon;
	GetDlgItem( IDC_EDIT_PRN_EMON )->GetWindowText( strMon );
	GetDlgItem( IDC_EDIT_PRN_SMON )->SetWindowText( strMon );			
}

void CLOGPrnSelDlg::OnKillfocusEditPrnSday() 
{
	// TODO: Add your control notification handler code here
	CString strDay;
	GetDlgItem( IDC_EDIT_PRN_SDAY )->GetWindowText( strDay );
	GetDlgItem( IDC_EDIT_PRN_EDAY )->SetWindowText( strDay );				
}

void CLOGPrnSelDlg::OnKillfocusEditPrnEday() 
{
	// TODO: Add your control notification handler code here
	CString strDay;
	GetDlgItem( IDC_EDIT_PRN_EDAY )->GetWindowText( strDay );
	GetDlgItem( IDC_EDIT_PRN_SDAY )->SetWindowText( strDay );					
}
