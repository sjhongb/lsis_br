//=======================================================
//==              LLRView.cpp
//=======================================================
//  파 일 명 :  LLRView.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :
//
//=======================================================

#include "stdafx.h"
#include "LLR.h"
#include "LLRDoc.h"
#include "LLRView.h"
#include "MainFrm.h"
#include "../include/UserDataType.h"
#include "../include/MessageInfo.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////
// CLLRView

IMPLEMENT_DYNCREATE(CLLRView, CFormView)

BEGIN_MESSAGE_MAP(CLLRView, CFormView)
	//{{AFX_MSG_MAP(CLLRView)
//	ON_MESSAGE(SCM_CLOSE,   OnClientClose)		// Client 소켓 해제						//LAN통신 삭제.
//	ON_MESSAGE(SCM_RECEIVE, OnClientReceive)	// Client로 부터 수신되는 데이터		//LAN통신 삭제.
//	ON_MESSAGE(SCM_ACCEPT, OnClientAccept)												//LAN통신 삭제.
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////

#define REPLAY_TIMER_ID		10

/////////////////////////////////////////////////////////////////////////////
// CLLRView construction/destruction

BEGIN_EVENTSINK_MAP(CLLRView, CFormView)
    //{{AFX_EVENTSINK_MAP(CEIDView)
	ON_EVENT(CLLRView, IDC_LSMCTRL, 2 /* OleSendMessage */, OnOleSendMessageLSMctrl, VTS_I4 VTS_I4 VTS_I4)
	ON_EVENT(CLLRView, IDC_LSMCTRL, 1 /* Operate */, OnOperateLSMctrl, VTS_I2 VTS_I2)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

//=======================================================
//
//  함 수 명 :  CLLRView
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  생성자
//
//=======================================================
CLLRView::CLLRView()
	: CFormView(CLLRView::IDD)
{
	//{{AFX_DATA_INIT(CLLRView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	pApp = (CLLRApp*)AfxGetApp();
//	m_ServerSocket = NULL;
//	m_ClientSocket = NULL;		//LAN통신 삭제.
	m_TimeDo = FALSE;
	m_bSelOn = FALSE;
}

//=======================================================
//
//  함 수 명 :  ~CLLRView
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  소멸자
//
//=======================================================
CLLRView::~CLLRView()
{
//	OnDisconnect();	
}

//=======================================================
//
//  함 수 명 :  DoDataExchange
//  함수출력 :  없음
//  함수입력 :  CDataExchange* pDX
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  1.01
//  설    명 :  화면 객체와의 데이터 교환 리스트
//
//=======================================================
void CLLRView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLLRView)

	//}}AFX_DATA_MAP
}

//=======================================================
//
//  함 수 명 :  OnInitialUpdate()
//  함수출력 :  없음
//  함수입력 :  없음
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  View 를 초기화 한다. 
//
//=======================================================
void CLLRView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	CLLRDoc* pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	pWnd->m_bDlgDisplaed = TRUE;

	BOOL bLoadOK = TRUE;		
	DWORD dwStyle = WS_CHILD;
    CRect rect;
	GetClientRect( &rect );
	m_Screen.Create( (LPCTSTR)(pApp->m_SystemValue.strStationFileName), dwStyle, rect, this, IDC_LSMCTRL ); 

	pWnd->m_ControlDlg.Create(IDD_DIALOG_CONTROL, NULL);
	pWnd->m_ControlDlg.ShowWindow(SW_SHOW);

// 	pWnd->m_ControlPannelDlg.Create(IDD_DLG_CONTROL_PANNEL,NULL);
// 	pWnd->m_ControlPannelDlg.ShowWindow(SW_SHOW);

	m_Screen.DisplayBG(0);  //WALL MOUNT 용일 경우만 무정보 궤도를 표시한다.

	if ( IsWindow(m_Screen.m_hWnd) ){
		m_Screen.ShowWindow( SW_SHOW );
		long wParam = (long)(LPCTSTR)(pApp->m_SystemValue.strStationFileName);
		long lParam = (long)(LPCTSTR)(pApp->m_SystemValue.strLSMResolution);	
		
 		m_Screen.MoveWindow( 0, 0, pApp->m_SystemValue.nMainSizeX, pApp->m_SystemValue.nMainSizeY);

		char *errstr = (char*)m_Screen.OlePostMessage(WS_OPMSG_LOADFILE, wParam, lParam );

		if ( errstr ) {
			MessageBox( errstr );
		}
		else {
			pApp->m_pEipEmu = (CEipEmu*)m_Screen.OlePostMessage( WS_OPMSG_GETEMUPTR, 0, 0 );		//get EipEmp class ptr
			CMessageInfo *pScrInfo = (CMessageInfo*)m_Screen.OlePostMessage( WS_OPMSG_GETSCRINFO, 0, 0 );

			if(pApp->m_pEipEmu && pScrInfo){
				int nTrack  = pApp->m_pEipEmu->m_strTrack.GetCount();		
				int nSignal = pApp->m_pEipEmu->m_strSignal.GetCount();		
				int nSwitch = pApp->m_pEipEmu->m_strSwitch.GetCount();		
				int nButton = pApp->m_pEipEmu->m_strButton.GetCount();		

				CString strTemp;
				if (!nTrack || (nTrack > 255)) {
					strTemp.Format("%d",nTrack);
					AfxMessageBox(strTemp);
					MessageBox("Warning!!\n\ntrack count error. check data file or LSM.OCX's version", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (!nSignal || (nSignal > 255)) {
					MessageBox("Warning!!\n\nsignal count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (nSwitch > 255) {
					MessageBox("Warning!!\n\npoint count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}
				if (nButton > 255) {
					MessageBox("Warning!!\n\nbutton count error. check data file or LSM.OCX's version.", MB_OK,MB_ICONSTOP);
					bLoadOK = FALSE;
				}

				if ( bLoadOK == TRUE ) {		//OK 
					int nMaxInfo = nTrack + nSignal + nSwitch + nButton ;		//total number of unit 
					int nSizeRecord = (nTrack * SIZE_INFO_TRACK) + (nSignal * SIZE_INFO_SIGNAL) 
															 + (nSwitch * SIZE_INFO_SWITCH);
					
					pApp->nMaxTrack  = nTrack;
					pApp->nMaxSignal = nSignal;
					pApp->nMaxSwitch = nSwitch;
					pApp->nMaxButton = nButton;
					pApp->nSfmRecordSize = nSizeRecord + sizeof(DataHeaderType);
					pApp->nComRecordSize = pApp->nSfmRecordSize + MAX_OPERATE_CMD_SIZE + MAX_USER_SIZE + MAX_PASS_SIZE + USER_CMD_SIZE + IPM_VERSION_SIZE;		// _nSfmRecordSize + LCCCommand(8) + IPMMessage(8) + User관련 (44)
					pApp->nVerRecordSize = pApp->nComRecordSize + sizeof(CJTime);							

					pApp->m_RecordMaskInfo.nTrack = nTrack;
					pApp->m_RecordMaskInfo.nSignal= nSignal;
					pApp->m_RecordMaskInfo.nSwitch= nSwitch;
					pApp->m_RecordMaskInfo.nButton= nButton;

					//데이터 블럭 할당
					if ( nTrack ) 
						pApp->m_RecordMaskInfo.pTrack  = new char [nTrack];
					if ( nSignal )
						pApp->m_RecordMaskInfo.pSignal = new char [nSignal];
					if ( nSwitch )
						pApp->m_RecordMaskInfo.pSwitch = new char [nSwitch];
					if ( nButton )
						pApp->m_RecordMaskInfo.pButton = new char [nButton];

					// 데이터 COPY
					if ( pApp->m_RecordMaskInfo.pTrack )
						memset(pApp->m_RecordMaskInfo.pTrack, 1, nTrack);
					if ( pApp->m_RecordMaskInfo.pSignal )
						memset(pApp->m_RecordMaskInfo.pSignal, 1, nSignal);
					if ( pApp->m_RecordMaskInfo.pSwitch )
						memset(pApp->m_RecordMaskInfo.pSwitch, 1, nSwitch);
					if ( pApp->m_RecordMaskInfo.pButton )
						memset(pApp->m_RecordMaskInfo.pButton, 1, nButton);
					
					pWnd->m_RealLOGMessage.CreateInfo( pScrInfo, nMaxInfo );	// 전역변수 Info_pScrMessageInfo에 set ( m_strName, m_pEipData, m_nMemOffset, m_nType, m_nID)
					pWnd->m_RealLOGMessage.CreateCardInfo();

					ParamType wData;
					wData.ndl = pApp->m_SystemValue.nOption;
					wData.ndh = 0;
					m_Screen.OlePostMessage( WS_OPMSG_SETLSMOPTION, wData.wParam, 0);		//LSM Option
				}
			} //end of if
			
			else {
				MessageBox( "EIPEMU or SCREEN INFO FAILED for LSM.OCX", MB_OK,MB_ICONSTOP );
			}

		}//end of else
	}//end of if
	else MessageBox( "Creation of main screen is failed.", MB_OK,MB_ICONSTOP);

	m_iClientCnt = 0;
	pApp->m_iConCnt = m_iClientCnt;
	SetTimer ( 1, 1000,NULL);
	SetTimer ( 3, 1000, NULL );

	CString strFlagOfSyncWithLSM = "";
	while(1)
	{
		strFlagOfSyncWithLSM = pApp->GetFlagOfSyncWithLSM();
		
		if ( strFlagOfSyncWithLSM == "TRUE")
		{
			pApp->GetVerifyObject();
			break;
		}
		for (int i; i<100; i++)
		{
			int abc = 0;
		}
	}
	
	CSplashWnd::ShowSplashScreen(AfxGetMainWnd());
}

#ifdef _DEBUG
void CLLRView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLLRView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CLLRDoc* CLLRView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLLRDoc)));
	return (CLLRDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLLRView message handlers

//=======================================================
//
//  함 수 명 :  OnOperateLSMctrl
//  함수출력 :  없음
//  함수입력 :  short nFunction, short nArgument
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :
//
//=======================================================
void CLLRView::OnOperateLSMctrl(short nFunction, short nArgument) 
{
	// TODO: Add your control notification handler code herehere
	BYTE *p = (BYTE*)&m_CmdQue[0];
	*p++ = (char)nFunction;
	*(short*)p = sizeof(short);
	p += sizeof(short);
	*(short*)p = nArgument;

	if ( nFunction == (short)WS_OPMSG_TRKSEL ) {
		if ( !(pApp->m_SystemValue.nProcOption & PROC_ON_SETTRACK) ) {
			return;
		}
	}

	LPARAM lParam = (LPARAM)&m_CmdQue[0];
	pApp->m_pMainWnd->PostMessage(WM_COMMAND, ID_COMM_CMD_WRITE, lParam);
}

//=======================================================
//
//  함 수 명 :  OnOleSendMessageLSMctrl
//  함수출력 :  없음
//  함수입력 :  long message, long wParam, long lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LSM 로 메세지를 전송한다. 
//
//=======================================================
void CLLRView::OnOleSendMessageLSMctrl(long message, long wParam, long lParam) 
{
	short n = 0;
	BYTE *s;
	BYTE *p = (BYTE*)&m_CmdQue[0];

	*p++ = (BYTE)message;
	p += sizeof(short);
	switch ( message ) {
	case WS_OPMSG_SETTRAINNAME	:	//4		// 열차 번호 지정 [track ID(BYTE), train no(5byte)]
									*p++ = (BYTE)wParam;
									s = (BYTE*)lParam;
									for(n=1; n<6; n++) {
									*p++ = *s++;
									}
									break;
	case WS_OPMSG_SIGNALDEST	:	//5		// 신호 (폐색 포함)취급 [signal ID(byte), button ID(byte)]
	case WS_OPMSG_SIGNALCANCEL	:	//6		// 신호 (폐색 포함)취소 [signal ID(byte)]
	case WS_OPMSG_SWTOGGLE		:	//9		// 선로전환기 토글(toggle) [switch ID(byte)]
	case WS_OPMSG_TRKSEL		:	//37		// 궤도 선택
									if ( pApp->m_SystemValue.nProcOption & PROC_ON_SETTRACK ) {
										*(short*)p = (short)wParam;
										n = sizeof( short );
									}
									break;
	case WS_OPMSG_LOS_ON		:   //message = WS_OPMSG_LOS;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_LOS_OFF		:   //message = WS_OPMSG_LOS;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_POS_ON		:   //message = WS_OPMSG_NOUSE;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_POS_OFF		:   //message = WS_OPMSG_NOUSE;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_PAS_IN		:
	case WS_OPMSG_PAS_OUT		:
	case WS_OPMSG_OSS			:
	case WS_OPMSG_BUTTON		:	//10	// 버튼 메세지(CMD, ID, ON/OFF), CTC, LOCAL, EXTLOCAL, S, P ...
	case WS_OPMSG_SIGNALSTOP	:
	case WS_OPMSG_SLS			:
	case WS_OPMSG_ATB			:
	case WS_OPMSG_ND			:
	case WS_OPMSG_ALARMCLR		:
	case WS_OPMSG_NOUSE         :
	case WS_OPMSG_BUZZERCLR		:
	case WS_OPMSG_UTG_CA		:
	case WS_OPMSG_UTG_LCR		:
	case WS_OPMSG_UTG_CBB		:
	case WS_OPMSG_UTC_CA		:
	case WS_OPMSG_UTC_LCR		:
	case WS_OPMSG_UTC_CBB		:
	case WS_OPMSG_DTG_CA		:
	case WS_OPMSG_DTG_LCR		:
	case WS_OPMSG_DTG_CBB		:
	case WS_OPMSG_DTC_CA		:
	case WS_OPMSG_DTC_LCR		:
	case WS_OPMSG_DTC_CBB		:
	case WS_OPMSG_HSW			:
	case WS_OPMSG_CAON			:
	case WS_OPMSG_LCROP			:
	case WS_OPMSG_CBBOPEMER		:   //message = WS_OPMSG_CBBOP;		//2013.01.30 의미없는 짓을 왜했을까??
	case WS_OPMSG_CBBOP		    :
	case WS_OPMSG_BLOCK_CLEAR	:
	case WS_OPMSG_START			:	//1		// 시스템 기동
	case WS_OPMSG_RESET			:	//2		// 시스템 재기동
	case WS_OPMSG_MARKINGTRACK	:	//14	// 궤도 사용금지/해제(모타카, 사용금지, 단선) [mode(byte), track ID(byte)]
	case WS_OPMSG_FREESUBTRACK	:	//15	// 구분진로비상해정 [track ID(byte)]
	case WS_OPMSG_AXL_RESET		:	//73	// AXL_reset제어.
									*(short*)p = (short)wParam;
									n = sizeof( short );
									p += n;
									*(long*)p = lParam;
									break;
									// Function Call Message
	case WS_OPMSG_SETLSMSIZE	:	//35		// LSM SIZE (resizing) [x(short), y(short)]
									*(long*)p = (long)wParam;
									n = sizeof( long );
									break;
	case WS_OPMSG_CMDCLEAR		:
									*(short*)p = 0;
									n = sizeof( short );
									break;
	default						:	return;
	}

	*(short*)&m_CmdQue[1] = n;
	lParam = (long)&m_CmdQue[0];

	pApp->m_pMainWnd->PostMessage(WM_COMMAND, ID_COMM_CMD_WRITE, (LPARAM)lParam);
}

//=======================================================
//
//  함 수 명 :  OnCommand
//  함수출력 :  BOOL
//  함수입력 :  WPARAM wParam, LPARAM lParam
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  메세지의 수신에 대응한다. 
//
//=======================================================
BOOL CLLRView::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	UINT commandID = (UINT)wParam;
	switch (commandID) {
	case ID_COMMAND_UPDATE_VIEW_1:
		m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, 999, (long)(&pApp->SystemVMEM[0]) );
		return TRUE;
	}	
	return CFormView::OnCommand(wParam, lParam);
}
 
void CLLRView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handher code here and/or call default
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	union
	{
		long lParam;
		struct
		{
			short ndl;
			short ndh;
		};
	};

	ndl = 1;
	ndh = 1;

	m_Screen.OlePostMessage(WS_OPMSG_OPERATESTATE, 0, lParam);

	if ( nIDEvent == REPLAY_TIMER_ID ) {
		m_TimeDo = TRUE;
		CLLRDoc*pDoc = GetDocument();
		CLOGRecord *pRecord = pDoc->GetNextRecord();
		if(pRecord) 
		{
			// pRecord->HexDump();
			memcpy (&pWnd->CommVMemData[0],&pRecord->m_BitBlock[0],pApp->nSfmRecordSize+MAX_OPERATE_CMD_SIZE);
			pWnd->UpdateLog();
			m_Screen.OlePostMessage( WS_OPMSG_UPDATESCREEN, (long)(pRecord->m_Head.Length), (long)(&pRecord->m_BitBlock[0]) );
		} 
		else 
		{
			OnMenuReplayStop();
			// 상태 표시				
			pDoc->InitReplayRecord();
		}
		m_TimeDo = FALSE;
	} else if ( nIDEvent == 1 ) {
		pWnd->ShowControlPannel(TRUE);
	} else if ( nIDEvent == 3 ) {
		KillTimer(nIDEvent);
		pWnd->CreateObj();
	}
	
	CFormView::OnTimer(nIDEvent);
}


void CLLRView::ReleaseLogFile()
{
	CLLRDoc *pDoc = GetDocument();
	pDoc->ReleaseLogFile();
}

void CLLRView::LogReplayStop(BOOL Condition)
{
	OnMenuReplayStop();
}

void CLLRView::OnMenuReplayStop()
{
	CLLRDoc *pDoc = GetDocument();

	pDoc->m_nVector = 0;
	m_bPlay = FALSE;
	TimerRunOff();
}

void CLLRView::OnMenuReplayFrontRun() 
{
	OnReplayRun( 1 );
}

void CLLRView::OnMenuSelReplayTime() 
{
	CLLRDoc *pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	// TODO: Add your command handler code here
	CJTime T = pDoc->m_ReplayTime;
	
	BOOL bOldPlay = m_bPlay;
	
	m_bPlay = FALSE;
	if ( pDoc->InitReplayRecord() ) {
		m_bPlay = bOldPlay;
		m_bSelOn = TRUE;
	} else {
		//		MessageBox( "Warning !\n\nThere is no established time log file or data was wrong.");
		MessageBox( "Warning !\n\nThere is no log data / connection fail.");
		pDoc->m_ReplayTime = T;
		
		if ( m_bSelOn ) {
			pDoc->InitReplayRecord();
		}
	}
}

void CLLRView::LogReplayFrontRun (BOOL Condition) 
{
	CLLRDoc *pDoc = GetDocument();
	OnMenuReplayFrontRun();
}

void CLLRView::LogReplaySelectRun (BOOL Condition) 
{
	CLLRDoc *pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	CJTime T = pDoc->m_ReplayTime;
	
	pDoc->m_ReplayTime = pWnd->m_ReplayTime;
	
	BOOL bOldPlay = m_bPlay;
	m_bPlay = FALSE;
	if ( pDoc->InitReplayRecord() ) {
		m_bPlay = bOldPlay;
		m_bSelOn = TRUE;
	}
	else {
		MessageBox( "Warning !\n\n지정된 시간의 기록 파일을 읽어들일 수가 없거나 데이터가 없습니다. \n재생시각설정을 취소합니다.");
		pDoc->m_ReplayTime = T;
		if ( m_bSelOn ) {
			pDoc->InitReplayRecord();
		}
	}
	
	pDoc->m_nVector = 1;
	
	TimerRunOn();
	m_bPlay = TRUE;
}

void CLLRView::LogReplaySetTime ()
{
	OnMenuSelReplayTime();
}

void CLLRView::TimerRunOn()
{
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	int iSpeed;
	
	iSpeed = pWnd->m_iReplaySpeed;
	TimerRunOff();
	m_nTimerID = SetTimer( REPLAY_TIMER_ID, iSpeed, NULL );
	//m_nTimerID = SetTimer( REPLAY_TIMER_ID, 10, NULL );
}

void CLLRView::LogReplaySpeedSet (int iSpeed)
{
	m_nTimerID = SetTimer( REPLAY_TIMER_ID, iSpeed, NULL );
}

void CLLRView::LogSetFrameSkip (int iSpeed)
{
	CLLRDoc *pDoc = GetDocument();
	pDoc->m_nSpeed = iSpeed+1;
}

void CLLRView::TimerRunOff()
{
	if ( m_nTimerID ) {
		KillTimer( m_nTimerID );
		m_nTimerID = 0;
	}
}

void CLLRView::SetPlayTime ()
{
	CLLRDoc *pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	pDoc->m_ReplayTime = pWnd->m_ReplayTime;
}

void CLLRView::OnReplayRun( int nVector ) 
{
	CLLRDoc *pDoc = GetDocument();
	
	// TODO: Add your command handler code here
	pDoc->m_nVector = nVector;
	
	if ( !m_bSelOn ) {
		OnMenuSelReplayTime();
	}
	if ( m_bSelOn ) {
		TimerRunOn();
		m_bPlay = TRUE;
	}
}

void CLLRView::SetLogFile (CString strLogFile )
{
	
	CLLRDoc *pDoc = GetDocument();
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	pDoc->m_ReplayTime = pWnd->m_ReplayTime;
	pDoc->SetLogFile(strLogFile);
}
