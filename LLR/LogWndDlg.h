#if !defined(AFX_LOGWNDDLG_H__07BC5D8C_E5B6_4305_A56A_4F523FCE6465__INCLUDED_)
#define AFX_LOGWNDDLG_H__07BC5D8C_E5B6_4305_A56A_4F523FCE6465__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogWndDlg.h : header file
//
//{{AFX_INCLUDES()
#include "crystalctrl.h"
//}}AFX_INCLUDES

#include "LOGListBox.h"
#include "XPStyleButtonST.h"
#include "resource.h"
#include "Report.h"
//#include "TextSet.h"
#include "WaitDlg.h"
#include "llr.h"

/////////////////////////////////////////////////////////////////////////////
// CLogWndDlg dialog

class CLogWndDlg : public CDialog
{
// Construction
public:
	BOOL m_bIsCreate;
	void OnResizeWindow();
	void OnSetSizeControl();
	void OnSetXpButton();
	void FlushPrintList();
	void RunPrint(int iCntList, int iPrintSel);
	void OnSave();
	BOOL SetTrans(int iVal);
	BOOL SetTransOn(int iVal);
	BOOL SetTransOff(int iVal);
	BOOL MoveHidePos();
	BOOL MoveinitPos();
	int m_iTrans;
	int m_iHideCk;
	void ButtonFind();
	void ButtonLogset();
	void PrevMessage();
	void NextMessage();
	void LastMessage();
	void ButtonScroll();
	void ToDayMessage();
	void RunMessagePrint(int iCntList, int iPrintSel);
	void MessagePrint();
	BOOL m_bWindowsPos;
	CString m_strMessage;

	CWnd* m_pParentWnd;
	CWaitDlg* pWaitDlg;

	CPtrArray	m_PrintList;
	CLogWndDlg(CWnd* pParent = NULL);   // standard constructor
	CThemeHelperST		m_ThemeHelper;	//for XP Style button
	LOGPrnConfigType m_LogConfig;
	CLLRApp* pApp;

	CReport* m_pReport;
//	CTextSet* m_pTextSet;

// Dialog Data
	//{{AFX_DATA(CLogWndDlg)
	enum { IDD = IDD_DIALOG_LOG };
	CXPStyleButtonST	m_cBtnTitle;
	CXPStyleButtonST	m_btnPrev;
	CXPStyleButtonST	m_btnNext;
	CXPStyleButtonST	m_btnLogset;
	CXPStyleButtonST	m_btnLast;
	CXPStyleButtonST	m_btnFind;
	CXPStyleButtonST	m_btnScroll;
	CLOGListBox			m_listBox;
	CCrystalCtrl		m_Rpt;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogWndDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogWndDlg)
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnLastMessage();
	afx_msg void OnPrevMessage();
	afx_msg void OnNextMessage();
	afx_msg void OnButtonFind();
	afx_msg void OnButtonLogset();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonScroll();
	afx_msg void OnBtnMenu();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGWNDDLG_H__07BC5D8C_E5B6_4305_A56A_4F523FCE6465__INCLUDED_)
