#if !defined(AFX_LOGSETDLG_H__C332C399_6C2C_4C70_B6CD_B951BBD139A5__INCLUDED_)
#define AFX_LOGSETDLG_H__C332C399_6C2C_4C70_B6CD_B951BBD139A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg dialog

class CLogSetDlg : public CDialog
{
// Construction
public:
	CLogSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogSetDlg)
	enum { IDD = IDD_DLG_LOGSET };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogSetDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGSETDLG_H__C332C399_6C2C_4C70_B6CD_B951BBD139A5__INCLUDED_)
