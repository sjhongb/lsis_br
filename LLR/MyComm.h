//=======================================================
//==              MyComm.h
//=======================================================
//  파 일 명 :  MyComm.h
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :	2.01 
//  설    명 :	시리얼 통신을 담당한다.
//
//=======================================================
#ifndef _COMM_H
#define _COMM_H

/////////////////////////////////////////////////////////////////////////////
// constant definitions
#define ID_COMM_RX_INRECORD		999

/////////////////////////////////////////////////////////////////////////////
#define MAX_COMM_BUF_SIZE		0x01ffff
#define MAXPORTS			    30
#define WM_RECVDATA				(WM_USER+1)
#define WM_COM_FAIL				(WM_USER+2)
#define LIMIT_COM_PASTICK		50		// 4.2 Sec [(x+1) * 200 ms]
#define LIMIT_COM_FAILTICK		20		// 1.2 Sec [(x+1) * 200 ms]
/////////////////////////////////////////////////////////////////////////////

DWORD CommWatchProc( LPVOID ) ;

class CComm : public CObject 
{
	DECLARE_DYNCREATE(CComm)
public:
	HANDLE m_idComDev;
	BOOL m_bConnected;
	OVERLAPPED osWrite, osRead;
	int m_port;
	DWORD m_rate;
	BYTE m_bytesize;
	BYTE m_stop;
	BYTE m_parity;
	BYTE m_pRxBuffer[MAX_COMM_BUF_SIZE+1];
	BYTE m_pMsgRxBuffer[MAX_COMM_BUF_SIZE+1];
	BOOL m_bSendToTx;

public:
	CComm();
	~CComm();
	BOOL CreateCommInfo();
	BOOL DestroyComm();
	BOOL OpenComPort();
	BOOL SetupConnection();
	BOOL CloseConnection();
	BOOL WriteCommBlock(LPSTR lpByte , DWORD dwBytesToWrite);
	int  WriteBlock( char *lpBuffer, int dwBytesToWrite );
	int  ReadCommBlock(LPSTR lpszBlock, int nMaxLength );
	void SetComPort(int port, DWORD rate, BYTE bytesize, BYTE stop, BYTE parity);

	virtual void SetReadData(LPSTR data, int size);
	virtual BOOL SendData(BYTE* data, int len);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _COMM_H