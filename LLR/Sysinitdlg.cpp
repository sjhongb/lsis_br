// Sysinitdlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "Sysinitdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSysinitdlg dialog


CSysinitdlg::CSysinitdlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSysinitdlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSysinitdlg)
	m_strMessage = _T("");
	//}}AFX_DATA_INIT
}


void CSysinitdlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSysinitdlg)
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Text(pDX, IDC_STATIC_MESSAGE, m_strMessage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSysinitdlg, CDialog)
	//{{AFX_MSG_MAP(CSysinitdlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSysinitdlg message handlers

BOOL CSysinitdlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowPos(&wndTopMost,100,100,450,100,SWP_NOMOVE);
	return TRUE;
}

void CSysinitdlg::SetMessage( CString strMsg ) 
{
	m_strMessage = strMsg;
	UpdateData( FALSE);
}
void CSysinitdlg::OK() 
{
	CDialog::OnOK();
}

void CSysinitdlg::OnOK() 
{
	CDialog::OnOK();
}
