//=======================================================
//==              DlgLogRecView.cpp
//=======================================================
// File Name   : DlgLogRecView.cpp
// Creator     : OMANI (Do Young Kim)
// Date        : 2009-03-30
// Version     : 3.1.1
// Description : header file
// History     :
//               
//
//=======================================================

#include "stdafx.h"
#include "LLR.h"
#include "MainFrm.h"
#include "DlgLogRecView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgLogRecView dialog


CDlgLogRecView::CDlgLogRecView(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLogRecView::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgLogRecView)
	m_strOrder = _T("");
	m_strSize = _T("");
	m_strTotalNo = _T("");
	m_bCheckMsgError = FALSE;
	m_bCheckMovement = FALSE;
	m_bCheckOperate = FALSE;
	m_bCheckOthers = FALSE;
	m_bCheckTrack = FALSE;
	m_bCheckSignal = FALSE;
	m_bCheckCrossingPoint = FALSE;
	m_bChkButtons = FALSE;
	m_bChkLogDays = FALSE;
	m_bChkOpMsg = FALSE;
	m_bChkOrderNo = FALSE;
	m_bChkRealNo = FALSE;
	m_bChkRecDays = FALSE;
	m_bChkSignals = FALSE;
	m_bChkSwitchs = FALSE;
	m_bChkSystems = FALSE;
	m_bChkTracks = FALSE;
	m_bChkUnitMsg = FALSE;
	m_bCheckSystemModule = FALSE;
	//}}AFX_DATA_INIT
	m_bIsCreate = FALSE;
	m_bOpen = FALSE;
	m_iLogControlAreaX = 20;
}


void CDlgLogRecView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLogRecView)
	DDX_Control(pDX, IDC_CHECK_MSG_SYSTEM_MODULE, m_cCheckSystemModule);
	DDX_Control(pDX, IDC_CHECK_MSG_SWITCH, m_cCheckCrossingPoint);
	DDX_Control(pDX, IDC_CHECK_MSG_SIGNAL, m_cCheckSignal);
	DDX_Control(pDX, IDC_CHECK_MSG_TRACK, m_cCheckTrack);
	DDX_Control(pDX, IDC_CHECK_MSG_SYSTEM_OTHER, m_cCheckOthers);
	DDX_Control(pDX, IDC_CHECK_MSG_OPERATE, m_cCheckOperate);
	DDX_Control(pDX, IDC_CHECK_MSG_ACTIVE, m_cCheckMovement);
	DDX_Control(pDX, IDC_CHECK_MSG_ERROR, m_cCheckMsgError);
	DDX_Control(pDX, IDC_STATIC_EQUIPMENT, m_cStaticEquipment);
	DDX_Control(pDX, IDC_STATIC_EVENTTYPE, m_cStaticEventType);
	DDX_Control(pDX, IDC_BTN_SHOW_OPTION, m_cBtnShowControl);
	DDX_Control(pDX, IDC_LIST_REC_VIEW, m_listRecView);
	DDX_Check(pDX, IDC_CHECK_MSG_ERROR, m_bCheckMsgError);
	DDX_Check(pDX, IDC_CHECK_MSG_ACTIVE, m_bCheckMovement);
	DDX_Check(pDX, IDC_CHECK_MSG_OPERATE, m_bCheckOperate);
	DDX_Check(pDX, IDC_CHECK_MSG_SYSTEM_OTHER, m_bCheckOthers);
	DDX_Check(pDX, IDC_CHECK_MSG_TRACK, m_bCheckTrack);
	DDX_Check(pDX, IDC_CHECK_MSG_SIGNAL, m_bCheckSignal);
	DDX_Check(pDX, IDC_CHECK_MSG_SWITCH, m_bCheckCrossingPoint);
	DDX_Check(pDX, IDC_CHECK_MSG_SYSTEM_MODULE, m_bCheckSystemModule);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgLogRecView, CDialog)
	//{{AFX_MSG_MAP(CDlgLogRecView)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_SHOW_OPTION, OnBtnShowOption)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_MSG_ERROR, OnCheckMsgError)
	ON_BN_CLICKED(IDC_CHECK_MSG_ACTIVE, OnCheckMsgActive)
	ON_BN_CLICKED(IDC_CHECK_MSG_OPERATE, OnCheckMsgOperate)
	ON_BN_CLICKED(IDC_CHECK_MSG_SYSTEM_OTHER, OnCheckMsgSystemOther)
	ON_BN_CLICKED(IDC_CHECK_MSG_TRACK, OnCheckMsgTrack)
	ON_BN_CLICKED(IDC_CHECK_MSG_SIGNAL, OnCheckMsgSignal)
	ON_BN_CLICKED(IDC_CHECK_MSG_SWITCH, OnCheckMsgSwitch)
	ON_BN_CLICKED(IDC_CHECK_MSG_SYSTEM_MODULE, OnCheckMsgSystemModule)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CDlgLogRecView::UpdateListBox()
{

}


void CDlgLogRecView::OnDestroy() 
{
	CDialog::OnDestroy();
	((CMainFrame*)m_pParentWnd)->m_bOnRecObjView = FALSE;
	m_bIsCreate = FALSE;
}

void CDlgLogRecView::OnClose() 
{
	((CMainFrame*)m_pParentWnd)->m_bOnRecObjView = FALSE;	
	CDialog::OnClose();
}

void CDlgLogRecView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(m_bIsCreate)OnResizeWindow();	
}

void CDlgLogRecView::OnResizeWindow()
{
	int height = 25;
	int divide = 5;
	CRect rect;
	CRect btnRect;
	CRect ctrlRect;
	GetClientRect(&rect);
	CString strMessage;
	int iSumRectLeft;

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);
	iSumRectLeft = rect.left;

	height = 3;

	//ListBox
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	if ( m_bOpen == TRUE ) {
		iBoxPlacement.rcNormalPosition.right  = rect.right -290;
	} else {
		iBoxPlacement.rcNormalPosition.right  = rect.right -20;
	}
	iBoxPlacement.rcNormalPosition.top    = rect.top + height; // + 28;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;

	btnRect.left = iBoxPlacement.rcNormalPosition.right;
	btnRect.right = iBoxPlacement.rcNormalPosition.right +20;
	btnRect.top = iBoxPlacement.rcNormalPosition.top;
	btnRect.bottom = iBoxPlacement.rcNormalPosition.bottom;

	m_listRecView.SetWindowPlacement(&iBoxPlacement);
	m_cBtnShowControl.MoveWindow (btnRect,TRUE);


	ctrlRect.left  = rect.right-150;
	ctrlRect.right = rect.right -10;
	ctrlRect.top = rect.top+10;
	ctrlRect.bottom = rect.top + 130;
	
	m_cStaticEquipment.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-140;
	ctrlRect.right = rect.right -20;
	ctrlRect.top = rect.top+40;
	ctrlRect.bottom = rect.top + 50;

	m_cCheckTrack.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-140;
	ctrlRect.right = rect.right -20;
	ctrlRect.top = rect.top+60;
	ctrlRect.bottom = rect.top + 70;

	m_cCheckSignal.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-140;
	ctrlRect.right = rect.right -20;
	ctrlRect.top = rect.top+80;
	ctrlRect.bottom = rect.top + 90;

	m_cCheckCrossingPoint.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-140;
	ctrlRect.right = rect.right -20;
	ctrlRect.top = rect.top+100;
	ctrlRect.bottom = rect.top + 110;

	m_cCheckSystemModule.MoveWindow (ctrlRect,TRUE);

    //------------------------------------------------

	ctrlRect.left  = rect.right-250;
	ctrlRect.right = rect.right -155;
	ctrlRect.top = rect.top+10;
	ctrlRect.bottom = rect.top + 130;

	m_cStaticEventType.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-240;
	ctrlRect.right = rect.right -170;
	ctrlRect.top = rect.top+40;
	ctrlRect.bottom = rect.top + 50;

	m_cCheckMsgError.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-240;
	ctrlRect.right = rect.right -170;
	ctrlRect.top = rect.top+60;
	ctrlRect.bottom = rect.top + 70;

	m_cCheckMovement.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-240;
	ctrlRect.right = rect.right -170;
	ctrlRect.top = rect.top+80;
	ctrlRect.bottom = rect.top + 90;

	m_cCheckOperate.MoveWindow (ctrlRect,TRUE);

	ctrlRect.left  = rect.right-240;
	ctrlRect.right = rect.right -170;
	ctrlRect.top = rect.top+100;
	ctrlRect.bottom = rect.top + 110;

	m_cCheckOthers.MoveWindow (ctrlRect,TRUE);

	this->Invalidate(TRUE);
	return;
}

BOOL CDlgLogRecView::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_bIsCreate = TRUE;

	int x,y;

	x=::GetSystemMetrics(SM_CXSCREEN);
	y=::GetSystemMetrics(SM_CYSCREEN);

	MoveWindow(0, 770, x, y-800, TRUE);
	
	m_cCheckSystemModule.ShowWindow(SW_HIDE);
	m_cCheckCrossingPoint.ShowWindow(SW_HIDE);
	m_cCheckSignal.ShowWindow(SW_HIDE);
	m_cCheckTrack.ShowWindow(SW_HIDE);
	m_cCheckOthers.ShowWindow(SW_HIDE);
	m_cCheckOperate.ShowWindow(SW_HIDE);
	m_cCheckMovement.ShowWindow(SW_HIDE);
	m_cCheckMsgError.ShowWindow(SW_HIDE);
	m_cStaticEquipment.ShowWindow(SW_HIDE);
	m_cStaticEventType.ShowWindow(SW_HIDE);

	m_cCheckSystemModule.SetCheck(TRUE);
	m_cCheckCrossingPoint.SetCheck(TRUE);
	m_cCheckSignal.SetCheck(TRUE);
	m_cCheckTrack.SetCheck(TRUE);
	m_cCheckOthers.SetCheck(TRUE);
	m_cCheckOperate.SetCheck(TRUE);
	m_cCheckMovement.SetCheck(TRUE);
	m_cCheckMsgError.SetCheck(TRUE);

    m_bCheckMsgError      =TRUE;
    m_bCheckMovement      =TRUE;
    m_bCheckOperate       =TRUE;
    m_bCheckOthers        =TRUE;
    m_bCheckTrack         =TRUE;
    m_bCheckSignal        =TRUE;
    m_bCheckCrossingPoint =TRUE;
    m_bCheckSystemModule  =TRUE;	

	m_ModeType.bModeError = TRUE;
	m_ModeType.bModeILocking = TRUE;
	m_ModeType.bModeOperate = TRUE;
	m_ModeType.bModeMessage = TRUE;
	m_ModeType.bTypeTrack = TRUE;
	m_ModeType.bTypeSignal = TRUE;
	m_ModeType.bTypeSwitch = TRUE;
	m_ModeType.bTypeSystem = TRUE;



	
	UpdateData (TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgLogRecView::OnBtnShowOption() 
{
	// TODO: Add your control notification handler code here
	int height = 25;
	int divide = 5;
	CRect rect;
	CRect btnRect;
	GetClientRect(&rect);
	CString strMessage;
	int iSumRectLeft;
	int iControlAreaX,i;

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);
	iSumRectLeft = rect.left;

	height = 3;
	iControlAreaX = 290;
	

	if ( m_iLogControlAreaX != iControlAreaX ) {
		SetTimer ( 10,10,NULL );
	//ListBox
		for ( i = m_iLogControlAreaX ; i < iControlAreaX ; i++ ) {
			iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
			iBoxPlacement.rcNormalPosition.right  = rect.right - i;
			iBoxPlacement.rcNormalPosition.top    = rect.top + height; // + 28;
			iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
			m_listRecView.SetWindowPlacement(&iBoxPlacement);
			btnRect.left = iBoxPlacement.rcNormalPosition.right;
			btnRect.right = iBoxPlacement.rcNormalPosition.right+20;
			btnRect.top = iBoxPlacement.rcNormalPosition.top;
			btnRect.bottom = iBoxPlacement.rcNormalPosition.bottom;
			m_cBtnShowControl.MoveWindow (btnRect,TRUE);
		}
		m_iLogControlAreaX = iControlAreaX;

		m_cCheckSystemModule.ShowWindow(SW_SHOW);
		m_cCheckCrossingPoint.ShowWindow(SW_SHOW);
		m_cCheckSignal.ShowWindow(SW_SHOW);
		m_cCheckTrack.ShowWindow(SW_SHOW);
		m_cCheckOthers.ShowWindow(SW_SHOW);
		m_cCheckOperate.ShowWindow(SW_SHOW);
		m_cCheckMovement.ShowWindow(SW_SHOW);
		m_cCheckMsgError.ShowWindow(SW_SHOW);
		m_cStaticEquipment.ShowWindow(SW_SHOW);
		m_cStaticEventType.ShowWindow(SW_SHOW);
		m_bOpen = TRUE; 
		m_cBtnShowControl.SetWindowText(">");

	} else {
		SetTimer ( 0,10,NULL );
		for ( i = m_iLogControlAreaX ; i > 20 ; i-- ) {
			iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
			iBoxPlacement.rcNormalPosition.right  = rect.right - i;
			iBoxPlacement.rcNormalPosition.top    = rect.top + height; // + 28;
			iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
			m_listRecView.SetWindowPlacement(&iBoxPlacement);	
			btnRect.left = iBoxPlacement.rcNormalPosition.right;
			btnRect.right = iBoxPlacement.rcNormalPosition.right+20;
			btnRect.top = iBoxPlacement.rcNormalPosition.top;
			btnRect.bottom = iBoxPlacement.rcNormalPosition.bottom;
			m_cBtnShowControl.MoveWindow (btnRect,TRUE);

		}
		m_iLogControlAreaX = 20;

		m_cCheckSystemModule.ShowWindow(SW_HIDE);
		m_cCheckCrossingPoint.ShowWindow(SW_HIDE);
		m_cCheckSignal.ShowWindow(SW_HIDE);
		m_cCheckTrack.ShowWindow(SW_HIDE);
		m_cCheckOthers.ShowWindow(SW_HIDE);
		m_cCheckOperate.ShowWindow(SW_HIDE);
		m_cCheckMovement.ShowWindow(SW_HIDE);
		m_cCheckMsgError.ShowWindow(SW_HIDE);
		m_cStaticEquipment.ShowWindow(SW_HIDE);
		m_cStaticEventType.ShowWindow(SW_HIDE);
		m_bOpen = FALSE; 
		m_cBtnShowControl.SetWindowText("<");

	}



}

void CDlgLogRecView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnTimer(nIDEvent);
}

void CDlgLogRecView::OnCheckMsgError() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckMsgError == TRUE ) {
		m_bCheckMsgError = FALSE;
		m_cCheckMsgError.SetCheck(FALSE);
		m_ModeType.bModeError = FALSE;
	} else {
		m_bCheckMsgError = TRUE;
		m_cCheckMsgError.SetCheck(TRUE);
		m_ModeType.bModeError = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgActive() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckMovement == TRUE ) {
		m_bCheckMovement = FALSE;
		m_cCheckMovement.SetCheck(FALSE);
		m_ModeType.bModeILocking = FALSE;
	} else {
		m_bCheckMovement = TRUE;
		m_cCheckMovement.SetCheck(TRUE);
		m_ModeType.bModeILocking = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgOperate() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckOperate == TRUE ) {
		m_bCheckOperate = FALSE;
		m_cCheckOperate.SetCheck(FALSE);
		m_ModeType.bModeOperate = FALSE;
	} else {
		m_bCheckOperate = TRUE;
		m_cCheckOperate.SetCheck(TRUE);
		m_ModeType.bModeOperate = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgSystemOther() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckOthers == TRUE ) {
		m_bCheckOthers = FALSE;
		m_cCheckOthers.SetCheck(FALSE);
		m_ModeType.bModeMessage = FALSE;

	} else {
		m_bCheckOthers = TRUE;
		m_cCheckOthers.SetCheck(TRUE);
		m_ModeType.bModeMessage = TRUE;

	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgTrack() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckTrack == TRUE ) {
		m_bCheckTrack = FALSE;
		m_cCheckTrack.SetCheck(FALSE);
		m_ModeType.bTypeTrack = FALSE;
	} else {
		m_bCheckTrack = TRUE;
		m_cCheckTrack.SetCheck(TRUE);
		m_ModeType.bTypeTrack = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgSignal() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckSignal == TRUE ) {
		m_bCheckSignal = FALSE;
		m_cCheckSignal.SetCheck(FALSE);
		m_ModeType.bTypeSignal = FALSE;
	} else {
		m_bCheckSignal = TRUE;
		m_cCheckSignal.SetCheck(TRUE);
		m_ModeType.bTypeSignal = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgSwitch() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckCrossingPoint == TRUE ) {
		m_bCheckCrossingPoint = FALSE;
		m_cCheckCrossingPoint.SetCheck(FALSE);
		m_ModeType.bTypeSwitch = FALSE;
	} else {
		m_bCheckCrossingPoint = TRUE;
		m_cCheckCrossingPoint.SetCheck(TRUE);
		m_ModeType.bTypeSwitch = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}

void CDlgLogRecView::OnCheckMsgSystemModule() 
{
	// TODO: Add your control notification handler code here
	UpdateData (FALSE);

    if ( m_bCheckSystemModule == TRUE ) {
		m_bCheckSystemModule = FALSE;
		m_cCheckSystemModule.SetCheck(FALSE);
		m_ModeType.bTypeSystem = FALSE;
	} else {
		m_bCheckSystemModule = TRUE;
		m_cCheckSystemModule.SetCheck(TRUE);
		m_ModeType.bTypeSystem = TRUE;
	}

	m_listRecView.m_MsgFilter = m_ModeType;
	UpdateData (TRUE);
}


void CDlgLogRecView::ReArrangeWindow() 
{
	// TODO: Add your control notification handler code here
	int x,y;

	x=::GetSystemMetrics(SM_CXSCREEN);
	y=::GetSystemMetrics(SM_CYSCREEN);

	MoveWindow(0, 770, x, y-800, TRUE);

}

void CDlgLogRecView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
/*
	if ( !m_PrintList.GetSize() ) return;
	int i, n, x, y;
	int page = pInfo->m_nCurPage;
	int nPageLine = m_PrnConfig.nPageLine;

	CFont* poldFont1, *poldFont2, *poldFont3;
	CSize chExtent;
	CString str;

// 메시지 출력 박스 사각점 구하기
	RECT rect;
	rect.top   = pInfo->m_rectDraw.top  + PRN_RECT_TOP;			// 5 Cm spare
	rect.left  = pInfo->m_rectDraw.left + PRN_RECT_LEFT;		// 1.5 Cm spare 
	rect.right = pInfo->m_rectDraw.right - PRN_RECT_RIGHT;		// 1.5 Cm spare 
	rect.bottom = pInfo->m_rectDraw.bottom - PRN_RECT_BOTTOM;	// 2.5 Cm spare 
// redefine page line
	y  = pInfo->m_rectDraw.bottom - pInfo->m_rectDraw.top;
	y -= PRN_RECT_TOP + PRN_RECT_BOTTOM + FONT_N_HEIGHT + 80;	// 20 + 20 + 20 + 20
	y /= (FONT_N_HEIGHT + FONR_N_GAPY);
	if ( y < nPageLine ) nPageLine = y;
// 문서 폰트 변경
	poldFont1 = pDC->SelectObject( &m_docFont );
// title 문자 출력
	str = "[PRINT FORMAT BY TIME]";
	pDC->TextOut( rect.left, PRN_TOP_HEADY, str );
// 페이지 출력
	str.Format("PAGE[%d/%d]", page, pInfo->GetMaxPage());
	chExtent = pDC->GetTextExtent( str );
	x = rect.right - chExtent.cx;
	pDC->TextOut( x, PRN_TOP_HEADY, str );
// 역명 폰트 변경
	poldFont2 = pDC->SelectObject( &m_stFont );
// 역명 및 해더 출력 
	str = _AppOptions.strStationPrintName;
	chExtent = pDC->GetTextExtent( str );
	y = PRN_TOP_BASEY;
	x = (pInfo->m_rectDraw.right / 2) - (chExtent.cx / 2);
	pDC->TextOut( x, y, str );
// 폰트 복귀
	pDC->SelectObject( poldFont2 );
// 
	y += chExtent.cy + FONR_N_GAPY;
	str = "[Electronic Interlocking System]";
	chExtent = pDC->GetTextExtent( str );
	x  = (pInfo->m_rectDraw.right / 2) - (chExtent.cx / 2);
	pDC->TextOut( x, y, str );
// 출력 문자 높이 및 폭 구하기
	chExtent = pDC->GetTextExtent( "A", 1 );
	m_nTextHeight = chExtent.cy;
// 시간 출력
	y = rect.top - ((chExtent.cy+FONR_N_GAPY) * 2);
	x = rect.left;
	str  = "Printing a term: ";
	str += m_PrnConfig.STime.Format("%Y.%m.%d  %H:%M:%S");
	str += m_PrnConfig.ETime.Format("  ∼  %Y.%m.%d  %H:%M:%S");

	pDC->TextOut( x, y, str );
	CJTime NowTime = CJTime::GetCurrentTime();
	str  = "Printing Date & Time: ";
	str += NowTime.Format("%Y.%m.%d  %H:%M:%S");
	y += chExtent.cy + FONR_N_GAPY;
	pDC->TextOut( x, y, str );
// 인쇄형식 그리기
	pDC->Rectangle( &rect );

	// 헤더 문자 출력
	poldFont3 = pDC->SelectObject( &m_headFont );
	CSize headExtent = pDC->GetTextExtent( "A", 1 );
	x = rect.left;
	y = rect.top + 20;
	int x1 = x;

	for (i=0; i<6; i++) {
		x += chExtent.cx * TimesHead[i].nMax;
		if (i != 5) {
			pDC->MoveTo( x, rect.top );
			pDC->LineTo( x, rect.bottom );
		}
		int mx = (x + x1) / 2;
		str = TimesHead[i].strhead;
		mx -= (headExtent.cx * str.GetLength()) / 2;
		pDC->TextOut( mx, y, str );
		x1 = x;
	}
	pDC->SelectObject( poldFont3 );
	// 해더 라인 
	y += headExtent.cy + 20;
	pDC->MoveTo( rect.left, y );
	pDC->LineTo( rect.right, y );
	y += 20;

	MsgTimeStrings Msgs;
	int nPrnLineNo = ( page > 0 ) ? ((page-1) * nPageLine) : 0;
	n = m_PrintList.GetSize();
	i = 0;
	do {
		if ( nPrnLineNo >= n ) break;
		else {
			i++;
			MessageEvent* pEvent = (MessageEvent*)m_PrintList.GetAt(nPrnLineNo);
			pApp->m_pEIPMsgList->GetMessage( Msgs, pEvent );
			x  = rect.left + chExtent.cx;
			y += FONR_N_GAPY;
			pDC->TextOut(x, y, Msgs.strTime);
			x += chExtent.cx * TimesHead[0].nMax; 				
			pDC->TextOut(x, y, Msgs.strType);
			x += chExtent.cx * TimesHead[1].nMax; 				
			pDC->TextOut(x, y, Msgs.strMsgNo);
			x += chExtent.cx * TimesHead[2].nMax; 				
			pDC->TextOut(x, y, Msgs.strMessage);
			x += chExtent.cx * TimesHead[3].nMax; 				
			pDC->TextOut(x, y, Msgs.strHelp);
			y += m_nTextHeight+2;
			nPrnLineNo++;
		}
	} while ( i < nPageLine );

	str = "(c)Copylight 2010 by LS Industrial Systems Co., Ltd. Product Model: LES"; 
	y = rect.bottom + FONR_N_GAPY;
	pDC->TextOut( rect.left, y, str );
// 폰트 복귀
	pDC->SelectObject( poldFont1 );
*/
}
