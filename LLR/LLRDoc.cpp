// LLRDoc.cpp : implementation of the CLLRDoc class
//

#include "stdafx.h"
#include "LLR.h"

#include "LLRDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLLRDoc

IMPLEMENT_DYNCREATE(CLLRDoc, CDocument)

BEGIN_MESSAGE_MAP(CLLRDoc, CDocument)
	//{{AFX_MSG_MAP(CLLRDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

typedef struct tagRecordBufInfo
{
	int nRecMax;
	int nRecCur;
	CLOGRecord pRecord[_MAX_RECORD_MINUTE+1];
}RecordBufInfo;

RecordBufInfo Info_Rep;
RecordBufInfo Info_Prn;

/////////////////////////////////////////////////////////////////////////////
// CLLRDoc construction/destruction

CLLRDoc::CLLRDoc()
{
	// TODO: add one-time construction code here
	m_pViewLOGFile = new CLOGFile("rb");
	m_nSpeed = 1;
	m_nVector = 1;
	m_lReplayforSeek = 0;
	
	Info_pScrMessageInfo = NULL;
	Info_Rep.nRecMax = 0;
	Info_Rep.nRecCur = 0;
	Info_Prn.nRecMax = 0;
	Info_Prn.nRecCur = 0;

	m_ReplayTime = CJTime::GetCurrentTime();
}

CLLRDoc::~CLLRDoc()
{
	if ( Info_pScrMessageInfo ) {
		delete [] Info_pScrMessageInfo;
		Info_pScrMessageInfo = NULL;
	}

	delete m_pViewLOGFile;
}

BOOL CLLRDoc::OnNewDocument()
{
	this->SetTitle("VDU View");
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLLRDoc serialization

void CLLRDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

void CLLRDoc::ReleaseLogFile()
{
	if (m_pViewLOGFile != NULL )
	{
		delete m_pViewLOGFile;
	}
	m_pViewLOGFile = new CLOGFile("rb");
	m_ReplayTime = CJTime::GetCurrentTime();
}

BOOL CLLRDoc::InitReplayRecord()
{
	BOOL bEnd = FALSE;
	long SearchTime = m_ReplayTime.GetTime();

	if ( m_pViewLOGFile->SetFind( SearchTime, TRUE ) ) {
		
		BOOL bFirst = TRUE;
		int  nSpeed = 0;
		m_lReplayforSeek = 0;

		do {
			if ( bEnd ) bFirst = FALSE;
			bEnd = UpdateReplayRecord( nSpeed );
			nSpeed = 1;
		} while ( bFirst && bEnd );
		if ( !bEnd ) {
			Info_Rep.nRecCur  = m_ReplayTime.nSec;
			Info_Rep.nRecCur *= 4;
			Info_Rep.nRecCur -= (m_nSpeed * m_nVector);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CLLRDoc::UpdateReplayRecord( int nVector )
{
	BOOL bEnd = FALSE;
	int  nMode = _LOG_SEARCH_FIND;
	
	if ( nVector < 0 )
		nMode = m_pViewLOGFile->PrevRecord();
	else if ( nVector > 0 )
		nMode = m_pViewLOGFile->NextRecord();
	if ( nMode == _LOG_SEARCH_FIND ) {
		CLOGRecord *pBuf = &Info_Rep.pRecord[0];
		CLOGRecord mRec;
		if (Info_Rep.nRecMax > 0) {
			mRec = Info_Rep.pRecord[ Info_Rep.nRecMax - 1 ];
		}
		int nCount = m_pViewLOGFile->GetRecordInMinute( pBuf, mRec, m_lReplayforSeek, TRUE );
		if ( nCount ) {
			Info_Rep.nRecMax = nCount;
			if ( nVector >= 0 ) Info_Rep.nRecCur = 0;
			else Info_Rep.nRecCur = nCount - 1;
		}
		else {
			bEnd = TRUE;
		}
	}
	else if ( nMode == _LOG_SEARCH_OVER ) 
		bEnd = TRUE;
	return bEnd;
}

CLOGRecord *CLLRDoc::GetNextRecord()
{
	int nOver;
	BOOL bEnd = FALSE;
	CLOGRecord *pRec = NULL;
	
	if ( Info_Rep.nRecMax ) {
		Info_Rep.nRecCur += (m_nSpeed * m_nVector);
		if ((m_nVector < 0 ) && (Info_Rep.nRecCur < 0)) {		// Prev
			do {
				nOver = 0 - Info_Rep.nRecCur;
				bEnd = UpdateReplayRecord( m_nVector );
				Info_Rep.nRecCur -= nOver;
			} while ( !bEnd && (Info_Rep.nRecCur < 0) );
			
		}
		else if ((m_nVector > 0) && (Info_Rep.nRecCur >= Info_Rep.nRecMax)) {		// Next
			do {
				nOver = Info_Rep.nRecCur % Info_Rep.nRecMax;
				bEnd = UpdateReplayRecord( m_nVector );
				Info_Rep.nRecCur += nOver;
			} while ( !bEnd && (Info_Rep.nRecCur >= Info_Rep.nRecMax) );
		}
		if ( !bEnd ) {
			pRec = &Info_Rep.pRecord[ Info_Rep.nRecCur ];
		}
	}
	return pRec;
}

void CLLRDoc::SetLogFile (CString strLogFile )
{
	if ( m_pViewLOGFile != NULL ) {
		delete m_pViewLOGFile;
	}
	m_pViewLOGFile = new CLOGFile(strLogFile);
	
}

/////////////////////////////////////////////////////////////////////////////
// CLLRDoc diagnostics

#ifdef _DEBUG
void CLLRDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLLRDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLLRDoc commands
