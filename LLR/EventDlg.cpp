// EventDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LLR.h"
#include "EventDlg.h"
#include "MainFrm.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MIN_SIZEY	200
#define MAX_SIZEY	2560

extern int 	_iLCCNo;

/////////////////////////////////////////////////////////////////////////////
// CEventDlg dialog


CEventDlg::CEventDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEventDlg::IDD, pParent), m_LOGRecordFile("a+b")
{
	//{{AFX_DATA_INIT(CEventDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bIsStarted = FALSE;
	m_bIsCreated = FALSE;
	m_bIsMinimized = FALSE;
	m_bIsAutoScroll = TRUE;
	m_nEventCount = 0;
	m_posLastEvent = NULL;

	CLLRApp* pApp = (CLLRApp*)AfxGetApp();

	m_strLOGDir = (const char *)&pApp->m_LOGDirInfo[0];
	m_strStnName = pApp->m_SystemValue.strStationFileName;
}


void CEventDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventDlg)
	DDX_Control(pDX, IDC_BUTTON_TITLE, m_ctrlTitleBtn);
	DDX_Control(pDX, IDC_BUTTON_SEARCH, m_ctrlSearchBtn);
	DDX_Control(pDX, IDC_BUTTON_PRINT, m_ctrlPrintBtn);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_ctrlSaveBtn);
	DDX_Control(pDX, IDC_BUTTON_SCROLL, m_ctrlScrollBtn);
	//}}AFX_DATA_MAP

	DDX_GridControl(pDX, IDC_CUSTOM_GRID, m_ctrlEventGrid);
}


BEGIN_MESSAGE_MAP(CEventDlg, CDialog)
	//{{AFX_MSG_MAP(CEventDlg)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_DOUBLECLICKED(IDC_BUTTON_TITLE, OnDoubleclickedButtonTitle)
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, OnButtonSearch)
	ON_BN_CLICKED(IDC_BUTTON_PRINT, OnButtonPrint)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_SCROLL, OnButtonScroll)
	ON_BN_CLICKED(IDC_BUTTON_TITLE, OnButtonTitle)
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventDlg message handlers

BOOL CEventDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetPosition();
	SetButtonTooltip();

	// TODO: Add extra initialization here
	m_ctrlEventGrid.SetLSEventMode(TRUE);
	m_ctrlEventGrid.SetEditable(FALSE);
 	m_ctrlEventGrid.SetListMode(TRUE);
//	m_ctrlEventGrid.SetSingleRowSelection(TRUE);
	m_ctrlEventGrid.SetHeaderSort(FALSE);
	m_ctrlEventGrid.EnableDragAndDrop(FALSE);
 	m_ctrlEventGrid.SetTextBkColor(RGB(0xFF, 0xFF, 0xFF));
	m_ctrlEventGrid.SetDoubleBuffering(TRUE);

	m_ctrlEventGrid.SetRowResize(FALSE);
	m_ctrlEventGrid.SetColumnResize(FALSE);
	m_ctrlEventGrid.SetRowCount(1);
	m_ctrlEventGrid.SetColumnCount(5);
	m_ctrlEventGrid.SetFixedRowCount(1);
	m_ctrlEventGrid.SetFixedColumnCount(0);

	m_ctrlEventGrid.SetItemText(0, 0, "NO.");
	m_ctrlEventGrid.SetItemText(0, 1, "TYPE");
	m_ctrlEventGrid.SetItemText(0, 2, "DATE");
	m_ctrlEventGrid.SetItemText(0, 3, "CODE");
	m_ctrlEventGrid.SetItemText(0, 4, "MESSAGE");

	RECT rectClient;
	m_ctrlEventGrid.GetClientRect(&rectClient);

	m_ctrlEventGrid.SetColumnWidth(0, 50);
	m_ctrlEventGrid.SetColumnWidth(1, 70);
	m_ctrlEventGrid.SetColumnWidth(2, 150);
	m_ctrlEventGrid.SetColumnWidth(3, 50);
	m_ctrlEventGrid.SetColumnWidth(4, rectClient.right - 320);

	LOGFONT lf;
	m_ctrlEventGrid.GetFont()->GetLogFont(&lf);

	memset(lf.lfFaceName, 0, sizeof(lf.lfFaceName));
	strcpy(lf.lfFaceName, "Verdana");

	CFont Font;
	Font.CreateFontIndirect(&lf);
	m_ctrlEventGrid.SetFont(&Font);
	Font.DeleteObject();

	m_ctrlEventGrid.GetNewRowsHeight();
	
	m_bIsCreated = TRUE;

//	RestoreEventRecords();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CEventDlg::SetPosition()
{
	int height = 25;
	int divide = 4;

	CRect rect;
	GetClientRect(&rect);

	WINDOWPLACEMENT iBoxPlacement;
	GetWindowPlacement(&iBoxPlacement);

	//MenuTitle
	iBoxPlacement.rcNormalPosition.left   = rect.left;
	iBoxPlacement.rcNormalPosition.right  = rect.right-3;
	iBoxPlacement.rcNormalPosition.top    = rect.top;
	iBoxPlacement.rcNormalPosition.bottom = rect.top+20;
	m_ctrlTitleBtn.SetWindowPlacement(&iBoxPlacement);
	
	//List Box
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right -3;
	iBoxPlacement.rcNormalPosition.top    = rect.top+20;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom - height ;
	m_ctrlEventGrid.SetWindowPlacement(&iBoxPlacement);
	m_ctrlEventGrid.ShowWindow(SW_SHOW);
	m_ctrlEventGrid.Invalidate();
	
	//Search button
	iBoxPlacement.rcNormalPosition.left   = rect.left + 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom ;
	m_ctrlSearchBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlSearchBtn.ShowWindow(SW_SHOW);

	//Print button
	iBoxPlacement.rcNormalPosition.left   =  rect.right / divide;
	iBoxPlacement.rcNormalPosition.right  =  rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlPrintBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlPrintBtn.ShowWindow(SW_SHOW);
	
	//Save button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 2;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height ;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlSaveBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlSaveBtn.ShowWindow(SW_SHOW);
	
	//Scroll button
	iBoxPlacement.rcNormalPosition.left   = rect.right / divide * 3;
	iBoxPlacement.rcNormalPosition.right  = rect.right / divide * 4;
	iBoxPlacement.rcNormalPosition.top    = rect.bottom - height;
	iBoxPlacement.rcNormalPosition.bottom = rect.bottom;
	m_ctrlScrollBtn.SetWindowPlacement(&iBoxPlacement);
	m_ctrlScrollBtn.ShowWindow(SW_SHOW);
}

int CEventDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here	
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	
	WINDOWPLACEMENT iWinPlacement;
	GetWindowPlacement(&iWinPlacement);
	iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nLogStartX;
	iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nLogStartY;
	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;
	iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY;
	SetWindowPlacement(&iWinPlacement);		

	return 0;
}

void CEventDlg::RestoreEventRecords(CJTime curTime)
{
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	
	// 리스트에 메시지 삽입.
	if(m_EventList.GetCount())
	{
		int nRow = m_ctrlEventGrid.GetRowCount() - 1;
				
		CString strIndex;

		if(m_posLastEvent == NULL)
		{
			m_posLastEvent = m_EventList.GetHeadPosition();
		}

		while( m_posLastEvent ) 
		{
			m_pLastEvent = (MessageEvent*)m_EventList.GetNext(m_posLastEvent);
			if(m_pLastEvent != NULL)
			{
				long lastEventSec = m_pLastEvent->time.GetSecInDay();
				long curSec = curTime.GetSecInDay();

				if(lastEventSec <= curSec)
				{
//				CJTime *pRowTime = (CJTime*)m_ctrlEventGrid.GetItemData(nRow, 0);

					COLORREF	color = RGB(0,0,0);
					MsgTimeStrings msgs;
					pApp->m_pEIPMsgList->GetMessage(msgs, m_pLastEvent);
						
					msgs.strType.TrimRight(' ');
					msgs.strTime.TrimRight(' ');
					msgs.strMsgNo.TrimRight(' ');
					msgs.strMessage.TrimRight(' ');
					
					if(msgs.strType == "CONTROL")
					{
						color = RGB(0, 0, 255);
					}
					else if(msgs.strType == "ERROR")
					{
						color = RGB(255, 0, 0);
					}
					
					strIndex.Format("%d", ++m_nEventCount);
					
					nRow = m_ctrlEventGrid.InsertRow(strIndex);
					m_ctrlEventGrid.SetItemData(nRow, 0, (DWORD)&m_pLastEvent->time);
					m_ctrlEventGrid.SetItemText(nRow, 1, msgs.strType);
					m_ctrlEventGrid.SetItemText(nRow, 2, msgs.strTime.Left(msgs.strTime.GetLength() - 3));
					m_ctrlEventGrid.SetItemText(nRow, 3, msgs.strMsgNo);
					m_ctrlEventGrid.SetItemText(nRow, 4, msgs.strMessage);
					
					m_ctrlEventGrid.SetItemFgColour(nRow, 0, color);
					m_ctrlEventGrid.SetItemFgColour(nRow, 1, color);
					m_ctrlEventGrid.SetItemFgColour(nRow, 2, color);
					m_ctrlEventGrid.SetItemFgColour(nRow, 3, color);
					m_ctrlEventGrid.SetItemFgColour(nRow, 4, color);
					
					RECT rectClient;
					m_ctrlEventGrid.GetClientRect(&rectClient);
					m_ctrlEventGrid.SetColumnWidth(4, rectClient.right - 320);

					m_ctrlEventGrid.EnsureVisible(nRow, 0);
					m_ctrlEventGrid.Invalidate();
				}
				else
				{
					break;
				}
			}
		}
	}
}

void CEventDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	RemoveEventList();
	m_EventRecordFile.Close();
}

void CEventDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(m_bIsCreated)
	{
		SetPosition();
	}
}


void CEventDlg::OnButtonTitle() 
{
	// TODO: Add your control notification handler code here
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	
	if(m_bIsMinimized)
	{
		m_bIsMinimized = FALSE;
		
		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nLogStartX;
		iWinPlacement.rcNormalPosition.top		= m_nLastTopPos;
		
		if (pApp->m_SystemValue.nSystemType == 0 ) {
			iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;
		} else if (pApp->m_SystemValue.nSystemType == 1 ) {
			pApp->m_SystemValue.nLogSizeX = pApp->m_SystemValue.nMainSizeX;	
			iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;		
		}
		
		iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY;
		SetWindowPlacement(&iWinPlacement);	
	}
	else
	{
		m_bIsMinimized = TRUE;
		
		WINDOWPLACEMENT iWinPlacement;
		GetWindowPlacement(&iWinPlacement);
		
		m_nLastTopPos = iWinPlacement.rcNormalPosition.top;
		
		iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nLogStartX;
		iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY - MIN_SIZEY;
		if (pApp->m_SystemValue.nSystemType == 0 ) 
		{
			iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;
		} 
		else if (pApp->m_SystemValue.nSystemType == 1 ) 
		{
			pApp->m_SystemValue.nLogSizeX = pApp->m_SystemValue.nMainSizeX;	
			iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nLogStartX + pApp->m_SystemValue.nLogSizeX;		
		}
		
		iWinPlacement.rcNormalPosition.bottom = pApp->m_SystemValue.nLogStartY + pApp->m_SystemValue.nLogSizeY;
		SetWindowPlacement(&iWinPlacement);
	}
	
	SetPosition();
}

void CEventDlg::OnDoubleclickedButtonTitle() 
{
	// TODO: Add your control notification handler code here
	OnButtonTitle();
}

void CEventDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();

	// TODO: Add your message handler code here and/or call default
	lpMMI->ptMinTrackSize.x = pApp->m_SystemValue.nLogSizeX;	// 최소값
	lpMMI->ptMaxTrackSize.x = pApp->m_SystemValue.nLogSizeX;	// 최대값

	if(m_bIsMinimized)
	{
		lpMMI->ptMinTrackSize.y = MIN_SIZEY; 
		lpMMI->ptMaxTrackSize.y = MIN_SIZEY; 
	}
	else
	{
		lpMMI->ptMinTrackSize.y = pApp->m_SystemValue.nLogSizeY;
		lpMMI->ptMaxTrackSize.y = MAX_SIZEY;
	}
	
	CDialog::OnGetMinMaxInfo(lpMMI);
}


void CEventDlg::SetButtonTooltip()
{
	m_ctrlSearchBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlSearchBtn.SetTooltipText(_T("Search last event logs."));
	m_ctrlSearchBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	
	m_ctrlPrintBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlPrintBtn.SetTooltipText(_T("Print today event logs now."));
	m_ctrlPrintBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	
	m_ctrlSaveBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlSaveBtn.SetTooltipText(_T("Save today event logs to .csv file."));
	m_ctrlSaveBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	
	m_ctrlScrollBtn.SetThemeHelper(&m_ThemeHelper);
	m_ctrlScrollBtn.SetTooltipText(_T("Toggle auto scroll."));
	m_ctrlScrollBtn.SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(128, 0, 128));
	
	m_ctrlTitleBtn.SetBitmaps(IDB_LOGTITLE, (int)BTNST_AUTO_DARKER);
}

void CEventDlg::OnButtonSearch() 
{
	// TODO: Add your control notification handler code here
	CString strArgs;
	strArgs.Format("/S \"%s\" \"%s\"", m_strLOGDir, m_strStnName);

	HINSTANCE hInstance = ShellExecute(m_hWnd, NULL, "EventMgr.exe", strArgs, NULL, SW_SHOWNORMAL);
	if(hInstance < (HINSTANCE)32)
	{
		AfxGetMainWnd()->MessageBox("Can't excute EventMgr program.", "Error", MB_OK|MB_ICONERROR|MB_APPLMODAL);
	}
}

void CEventDlg::OnButtonPrint() 
{
	// TODO: Add your control notification handler code here
	CString strArgs;
	strArgs.Format("/P \"%s\" \"%s\"", m_strLOGDir, m_strStnName);
	
	HINSTANCE hInstance = ShellExecute(m_hWnd, NULL, "EventMgr.exe", strArgs, NULL, SW_SHOWNORMAL);
	if(hInstance < (HINSTANCE)32)
	{
		AfxGetMainWnd()->MessageBox("Can't excute EventMgr program.", "Error", MB_OK|MB_ICONERROR|MB_APPLMODAL);
	}
}

void CEventDlg::OnButtonSave() 
{
	// TODO: Add your control notification handler code here
	CJTime curTime = CJTime::GetCurrentTime();
	CString strDefFileName = curTime.Format("%Y%m%d_000000_~_%Y%m%d_%H%M%S_Event_Log");

	CFileDialog fileDlg(FALSE, ".CSV", strDefFileName, OFN_OVERWRITEPROMPT, "CSV Files (*.csv)|*.csv||", AfxGetMainWnd());
	
	if(fileDlg.DoModal() == IDOK)
	{
		CString strFileName = fileDlg.GetFileName();
		CString strFilePath = fileDlg.GetPathName();
		
		if(m_ctrlEventGrid.Save(strFilePath))
		{
			AfxGetMainWnd()->MessageBox(strFileName + " file is saved.", "Notice", MB_OK|MB_ICONINFORMATION|MB_APPLMODAL);
		}
	}
}

void CEventDlg::OnButtonScroll() 
{
	// TODO: Add your control notification handler code here
	if(m_bIsAutoScroll)
	{
		m_bIsAutoScroll = FALSE;

		m_ctrlScrollBtn.SetWindowText("Auto Scroll OFF");
	}
	else
	{
		m_bIsAutoScroll = TRUE;
		
		m_ctrlScrollBtn.SetWindowText("Auto Scroll ON");
	}
}

BOOL CEventDlg::RefreshFilePath(CString &strEventPath)
{
	if(m_strFilePath != strEventPath)
	{
		m_EventRecordFile.Close();

		m_strFilePath = strEventPath;

		if(m_EventRecordFile.Open(m_strFilePath, m_strStnName, TRUE))
		{
			m_EventRecordFile.GetAllRecords(m_EventList);
			return TRUE;
		}
		else
		{
			AfxMessageBox("Can't Open New File.");
		}
	}

	return FALSE;
}

void CEventDlg::RemoveEventList()
{
	POSITION pos = m_EventList.GetHeadPosition();
	MessageEvent *pEvent = NULL;
	
	while(pos != NULL)
	{
		pEvent = (MessageEvent*)m_EventList.GetNext(pos);
		if(pEvent != NULL)
		{
			delete pEvent;
			pEvent = NULL;
		}
	}
	
	m_EventList.RemoveAll();
}

BOOL CEventDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	::SendMessage(m_ctrlEventGrid.m_hWnd, WM_MOUSEWHEEL, (WPARAM)(((DWORD)zDelta) << 16 | (DWORD)nFlags), (LPARAM)(((DWORD)pt.y) << 16 | (DWORD)pt.x));
	
	//return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	return TRUE;
}

BOOL CEventDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	CMainFrame* m_pMainFrame;
	m_pMainFrame = (CMainFrame*)GetParentFrame();

	if ((pMsg->message == WM_KEYDOWN) || (pMsg->message == WM_SYSKEYDOWN)) 
	{
		switch(pMsg->wParam) 
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		case VK_F4:		// About
			if((pMsg->lParam&0x20000000)==0x20000000)
			{
				m_pMainFrame->PostMessage(WM_COMMAND,ID_EXIT);
			}
			break;
		default:
			break;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
