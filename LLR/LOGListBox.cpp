//=======================================================
//                LOGListBox.cpp
//=======================================================
//  파 일 명 :  LOGListBox.cpp
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  LOG Window 에 로그 메세지를 표시하고 Log 파일로 로그를 저장한다. 
//
//=======================================================

#include "stdafx.h"
#include "LOGListBox.h"
#include "LLR.h"
#include "MainFrm.h"
#include "sapi.h"
#include "../include/LOGMessage.h"
#include "../include/FMSGList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define  MAX_VIEW_EVENT		2000
#define  MAX_WINDOW_EVENT	1000
#define  MAX_RECORD_EVENT	5000

extern int 	_iLCCNo;
int exitflag;
int voiceflag;
char errmsg [2048];

UINT ThreadProc ( LPVOID lParam )
{
	voiceflag = 0;
	CString m_sText;
	while (!exitflag){
		Sleep(0);
		if ( voiceflag == 1 ) {
			voiceflag = 0;
			m_sText = errmsg;
				ISpVoice * pVoice = NULL;

				if (FAILED(CoInitialize(NULL))) {
					AfxMessageBox("Error to intiliaze COM");
					return FALSE;
				}
				HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
				if( SUCCEEDED( hr ) ) {
					hr = pVoice->Speak(m_sText.AllocSysString(), 0, NULL);
        
					pVoice->Release();
					pVoice = NULL;
				}
				CoUninitialize();
				voiceflag = 0;
		}

	}
	voiceflag = 0;
	return TRUE;
}

void FlushEventObjLst( CObList &objlist )
{
	POSITION pos = objlist.GetHeadPosition();
	while( pos != NULL ) {
		MessageEvent* pEvent =(MessageEvent*)objlist.GetNext(pos);
		if ( pEvent ) delete pEvent;
	}
	objlist.RemoveAll();
}

CLOGListBox::CLOGListBox() : m_SaveLOGFile("a+b")
{
	CLLRApp*	pApp	= (CLLRApp  *)AfxGetApp();

	m_bNoneSaved	= TRUE;
	m_bAttachFile	= FALSE;
	m_bExRecOrder	= FALSE;
	m_bLastView		= FALSE;
	m_nMaxViewItem	= 0;
	m_nOrderInSec	= 0;
	m_nLastPage		= 0;
	m_iMsgPoint     = 0;
	m_iControlFlag  = 0;
	m_MsgFilter.MsgType = 0xff;
}

CLOGListBox::~CLOGListBox()
{
	exitflag = TRUE;
	FlushEventObjLst( m_ViewList );
	FlushEventObjLst( m_MessageList );
}


BEGIN_MESSAGE_MAP(CLOGListBox, CListBox)
	//{{AFX_MSG_MAP(CLOGListBox)
	ON_WM_CREATE()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLOGListBox message handlers

int CLOGListBox::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	lpCreateStruct->style |= WS_HSCROLL;
	if (CListBox::OnCreate(lpCreateStruct) == -1) return -1;
	m_bReplayFirstCK = FALSE;
	ResetContent();
	return 0;
}

void CLOGListBox::ResizeWindow()
{
	RECT mRect;
	GetClientRect(&mRect);
	
	int my = mRect.bottom - mRect.top + 1;
	int cy = GetItemHeight( 0 );

	m_nMaxViewItem = (my + (cy / 2)) / cy;
}

//=======================================================
//
//  함 수 명 :  AttachFileRecord
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  로그 메세지 생성 
//
//=======================================================
int CLOGListBox::AttachFileRecord( BYTE *pVMem, int length, BOOL bFailTime /* = FALSE */ , int iView )
{
	CLLRApp*	pApp	= (CLLRApp  *)AfxGetApp();
	CMainFrame*	pWnd	= (CMainFrame *)AfxGetMainWnd();
	int	 nAlter			= 0;
	BOOL bFirst			= FALSE;
	BOOL bSave			= TRUE;
	BOOL bIsAttachView	= FALSE;
	CLOGRecord*	pRec	= NULL;
	DataHeaderType* pHead = (DataHeaderType*)pVMem;
	SystemStatusType *sysInfo = (SystemStatusType *)&(pHead->nSysVar);

	m_bAttachFile = TRUE;

	// 메시지 버퍼 갱신
	m_crRecord.SetRecord( pVMem, length );
	m_crRecord.m_Head.IsVersion = 1;

	if ( m_bNoneSaved ) {
		if ( pWnd->m_RealLOGMessage.m_bNoneBase ) {
			pWnd->m_RealLOGMessage.SetBaseRecord( m_crRecord );
		}
		m_bNoneSaved = FALSE;
		bFirst		 = TRUE;
		nAlter		 = 0x0ff;		// 취소 메뉴 생성 조건 지정
		pRec		 = &pWnd->m_RealLOGMessage.m_OldRecord;
		// 초기 참조될 이전 데이터의 버전 관련 데이터 초기화(데이터가 없을 수도 있기 때문임)
// 		if ( m_SaveLOGFile.IsStrIDVersion() ) {
// 			pRec->m_nVersion = REC_VER_200;
// 			pRec->m_Head.IsVersion = 1;
// 		}
		pRec->m_Head.Type = LOG_RECTYPE_ALL;
	}

	// 같은 시간대(초)의 데이터 순서 지정
	CJTime	*pLHead = (CJTime*)&pWnd->m_RealLOGMessage.m_OldRecord.m_SysHead.nSec;
	CJTime	*pCHead = (CJTime*)&m_crRecord.m_SysHead.nSec;
	long lastlt = pLHead->GetTime();
	long curlt  = pCHead->GetTime();

	if ( !bFailTime ) {
		if (pLHead->nSec != pCHead->nSec) {
			m_nOrderInSec = 0;
		}
		else {
			if ( !bFirst ) m_nOrderInSec++;
		}
		m_crRecord.m_Head.Order = m_nOrderInSec;
	}
	// 메시지 분석 및 생성
	nAlter |= pWnd->m_RealLOGMessage.CreateMessage(m_crRecord, m_MessageList); // -1/All, 0/None, +Other/Part
	// 파일 저장
	if ( pApp->m_SystemValue.nProcOption & PROC_NO_LOGWRITE ) {
		bSave = FALSE;
	}
	
// 	m_SaveLOGFile.AttachRecord( &m_crRecord, bSave, pRec );

	// 실시간 메시지 화면 출력 가능한 상태인지 검사	
	if ( m_bStopScroll == FALSE ) {
		bIsAttachView = TRUE;
		SetCurSel( GetCount() - 1 );
	} else if ( m_bStopScroll == TRUE ) {
		bIsAttachView = TRUE;
	}

	// 리스트에 메시지 삽입.
	if ( pApp->m_SystemValue.nProcOption & PROC_NO_VIEWLIST ) 
		bIsAttachView = FALSE; 
	if ( bIsAttachView ) {		// 리얼 데이터의 메시지를 리스트창에 표시 가능하면  
		m_AlterHead = m_crRecord.m_Head;			// record header data
		m_AlterSysHead = m_crRecord.m_SysHead;		// system header data
	}

	if ( m_MessageList.GetCount() ) {
 		AttachMessage( m_MessageList, bIsAttachView );  // 로깅 에러시 막는부분 임시..
	}

	m_bAttachFile = FALSE;

	if ( bFirst ) {
		nAlter |= 0x1000;	// 메시지 목록 표시 상자 재 검색 표시.
	}
	return nAlter;
}

//=======================================================
//
//  함 수 명 :  AttachMessage
//  함수출력 :  없음
//  함수입력 :  CObList &objlist, BOOL bRealData : 현재 화면의 
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  
//
//=======================================================
void CLOGListBox::AttachMessage( CObList &objlist, BOOL bRealData )
{
	CLLRApp *pApp = (CLLRApp *)AfxGetApp();
	MessageEvent* pEvent;
	CString msg;
	
	if ( !bRealData ) {
		FlushEventObjLst( objlist );
	}
	else {
		int listcount = objlist.GetCount();
		int viewcount = GetCount();
		int  n = 0;
		POSITION pos = objlist.GetHeadPosition();
		while( pos != NULL ) {
			pEvent = (MessageEvent*)objlist.GetNext(pos);
			if ( pEvent ) {
				if ( pEvent->IsAttach( m_MsgFilter ) ) {
					n++;
					msg = "";
					pApp->m_pEIPMsgList->GetMessage(msg, pEvent);
					if ( m_bStopScroll == FALSE ) {
						AddListItem( LPCTSTR(msg) );
					}
					AddSharedMemory ( LPCTSTR(msg) );
					//if ( m_bStopScroll == TRUE ) 
					m_ViewList.AddTail( (CObject*)pEvent );					
					// 제어 의 경우 리스트에 추가하진 않는다. 
					// 그러나 파일에 기록은 한다. 
					//ASSERT( GetCount() == m_ViewList.GetCount() );
					if ( GetCount() > MAX_VIEW_EVENT) {		//200
						DeleteString( 0 );
						POSITION pos1 = m_ViewList.GetHeadPosition();
						if ( pos1 != NULL ) {
							MessageEvent *pdelEvent = (MessageEvent*)m_ViewList.GetAt(pos1);
							m_ViewList.RemoveAt(pos1);
							delete pdelEvent;
						}
					}
				}
				else 
				delete pEvent;
			}
		}
		objlist.RemoveAll();
	}
}

int  CLOGListBox::QuickLastMessage()
{
	ResetContent();
	return TRUE;
}

int  CLOGListBox::LastMessage()
{
	int flag = 0;

	m_SearchTime = CJTime::GetCurrentTime(); // 현재시간을 얻어온다.
	if ( PrevMessage(TRUE, TRUE) ) {
		SetCurSel( GetCount() - 1 );
		flag = 1;
	}
	m_bLastView = TRUE;

	return flag;
}

int  CLOGListBox::LastOneMessage()
{
	CLLRApp *pApp = (CLLRApp *)AfxGetApp();
	CString msg;
	int flag = 0;
	msg = pApp->m_strEventMessage;

	if ( pApp->m_iMsgChangeFlag == TRUE ) {
		if ( msg.Find("CONTROL",0) > 0 )  AddListItem( LPCTSTR(msg) );
		pApp->m_iMsgChangeFlag = FALSE;
		SetCurSel( GetCount() - 1 );
	}
	
	if ( GetCount() > MAX_VIEW_EVENT) {		//200
		DeleteString( 0 );
	}

	return flag;
}

int  CLOGListBox::OnedayMessage(BOOL bFirst , BOOL bResearch , LOGPrnConfigType *pConfig )
{
	CLLRApp *pApp = (CLLRApp *)AfxGetApp();

	BOOL bSetRecord = FALSE;
	long forSeek;
	int  i, n, count;
	int  flag = LOG_SEARCH_NONE;
	int  nSearchCount = MAX_WINDOW_EVENT;
	BOOL bFirstSeek = TRUE;
	MsgModeType	  MsgType;
	MessageEvent* pEvent;
	CObList	MsgList;
	CLOGRecord mRec;
	
	if ( pConfig ) {
		bResearch = TRUE;
		MsgType = pConfig->Type;
		m_SearchTime = pConfig->STime;
	}
	else {
		MsgType = m_MsgFilter;
		if ( !GetCount() ) {
			return flag;
		}
		else {
			if ( bResearch ) 
				pEvent = (MessageEvent*)m_ViewList.GetHead();
			else 
				pEvent = (MessageEvent*)m_ViewList.GetTail();
			m_SearchTime = pEvent->time;
		}
	}
	if ( bResearch ) {
		FlushEventObjLst( m_ViewList );
		FlushEventObjLst( m_MessageList );
		ResetContent();
	}

	m_pViewLOGFile = new CLOGFile("rb");
	if (m_pViewLOGFile ) {

		if ( m_pViewLOGFile->SetFindOneday( m_SearchTime, TRUE ) ) {
			n = 0;
			flag = LOG_SEARCH_FIND;
			bFirstSeek = TRUE;
			do {
				if ( GetCount() >= MAX_WINDOW_EVENT) {
					break;
				}
				BOOL bGetEventBase = TRUE;
				if ( !bFirstSeek ) {
					flag = m_pViewLOGFile->NextRecord();
				}
				forSeek = 0;
				if (flag == LOG_SEARCH_FIND) {
					CLOGRecord *pRec = _pRecordBlk;
					do {
						count = m_pViewLOGFile->GetRecordInMinute( pRec, mRec, forSeek );
						if ( count ) {
							m_ViewLOGMessage.SetBaseRecord( mRec );
							for(i=0; i<count; i++) {
								if ( m_ViewLOGMessage.CreateMessage(pRec[i], MsgList) ) // -1/All, 0/None, +Other/Part
								{
								}
							}
							if ( bFirst ) {
								bSetRecord = TRUE;
							}
							n = MsgList.GetCount();
							if ( n ) {
								MessageEvent* pBaseEvn;
								if ( bGetEventBase ) {
									if (m_ViewList.GetCount() ) 
										pBaseEvn = (MessageEvent*)m_ViewList.GetTail();
									else 
										pBaseEvn = NULL;
									bGetEventBase = FALSE;
								}
								BOOL bSet;
								CString msg;
								POSITION pos = MsgList.GetHeadPosition();
								while( pos != NULL ) {
									pEvent =(MessageEvent*)MsgList.GetNext( pos );
									if ( pEvent ) {
										if ( !pEvent->IsAttach(MsgType) ) 
											bSet = FALSE;
										else if (pConfig && (pEvent->time > pConfig->ETime)) 
											bSet = FALSE;
										else if ( !pBaseEvn ) 
											bSet = TRUE;
										else if (pBaseEvn->time > pEvent->time) 
											bSet = FALSE;
										else if (pBaseEvn->time < pEvent->time) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nRec > pEvent->evn.nRec) 
											bSet = FALSE;
										else if (pBaseEvn->evn.nRec < pEvent->evn.nRec) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nLoc >= pEvent->evn.nLoc) 
											bSet = FALSE;
										else bSet = TRUE;
										if ( bSet && (nSearchCount > 0) ) {
											pApp->m_pEIPMsgList->GetMessage(msg, pEvent);
											AddListItem( LPCTSTR(msg) );
											//if ( m_bStopScroll == TRUE ) 
												m_ViewList.AddTail( (CObject*)pEvent );
											if ( GetCount() >= MAX_WINDOW_EVENT) {
												break;
											}
											nSearchCount--;
										} else {
											delete pEvent;
										}
									}
								}
								MsgList.RemoveAll();
								if ( nSearchCount > 0 ) n =0;
								else SetCurSel( (MAX_WINDOW_EVENT-nSearchCount)-1 );
								flag = LOG_SEARCH_FIND;
							}
							else flag = LOG_SEARCH_NONE;
							if ( forSeek ) {
								mRec = pRec[count-1];
							}
						}
						else break;
					} while ( forSeek );
				} else {
					if (flag == LOG_SEARCH_OVER) {
						m_bLastView = TRUE;
					}
					break;
				}
				bFirstSeek = FALSE;
			} while ( !n );
			if (m_ViewList.GetCount() >= MAX_VIEW_EVENT) {
				POSITION pos1;
				n = MAX_WINDOW_EVENT - nSearchCount;
				for(i=0; i<n; i++) {
					if ( GetCount() )
						DeleteString( 0 );
					pos1 = m_ViewList.GetHeadPosition();
					if ( pos1 != NULL ) {
						pEvent = (MessageEvent*)m_ViewList.GetAt(pos1);
						m_ViewList.RemoveAt(pos1);
						delete pEvent;
					}
				}
				SetCurSel( 0 );		//^^추가 몽땅 다 넘어가던거 방지
			}
		}
		else {
			m_bLastView = TRUE;
		}
		delete m_pViewLOGFile;
		m_pViewLOGFile = NULL;
	}

	if ( bFirst ) {
		if ( bSetRecord ) return 1;
		else return 0;
	}
	else 
		return flag;
}

int  CLOGListBox::NextMessage(BOOL bFirst , BOOL bResearch , LOGPrnConfigType *pConfig )
{
	CLLRApp *pApp = (CLLRApp *)AfxGetApp();

	BOOL bSetRecord = FALSE;
	long forSeek;
	int  i, n, count;
	int  flag = LOG_SEARCH_NONE;
	int  nSearchCount = MAX_WINDOW_EVENT;
	BOOL bFirstSeek = TRUE;
	MsgModeType	  MsgType;
	MessageEvent* pEvent;
	CObList	MsgList;
	CLOGRecord mRec;
	
	if ( pConfig ) {
		bResearch = TRUE;
		MsgType = pConfig->Type;
		m_SearchTime = pConfig->STime;
	}
	else {
		MsgType = m_MsgFilter;
		if ( !GetCount() ) {
			return flag;
		}
		else {
			if ( bResearch ) 
				pEvent = (MessageEvent*)m_ViewList.GetHead();
			else 
				pEvent = (MessageEvent*)m_ViewList.GetTail();
			m_SearchTime = pEvent->time;
		}
	}
	if ( bResearch ) {
		FlushEventObjLst( m_ViewList );
		FlushEventObjLst( m_MessageList );
		ResetContent();
	}

	m_pViewLOGFile = new CLOGFile("rb");
	if (m_pViewLOGFile ) {

		if ( m_pViewLOGFile->SetFind( m_SearchTime, TRUE ) ) {
			n = 0;
			flag = LOG_SEARCH_FIND;
			bFirstSeek = TRUE;
			do {
				BOOL bGetEventBase = TRUE;
				if ( !bFirstSeek ) {
					flag = m_pViewLOGFile->NextRecord();
				}
				forSeek = 0;
				if (flag == LOG_SEARCH_FIND) {
					CLOGRecord *pRec = _pRecordBlk;
					do {
						Sleep(0);
						count = m_pViewLOGFile->GetRecordInMinute( pRec, mRec, forSeek );
						if ( count ) {
							m_ViewLOGMessage.SetBaseRecord( mRec );
							for(i=0; i<count; i++) {
								if ( m_ViewLOGMessage.CreateMessage(pRec[i], MsgList) ) // -1/All, 0/None, +Other/Part
								{
								}
							}
							if ( bFirst ) {
								bSetRecord = TRUE;
							}
							n = MsgList.GetCount();
							if ( n ) {
								MessageEvent* pBaseEvn;
								if ( bGetEventBase ) {
									if (m_ViewList.GetCount() ) 
										pBaseEvn = (MessageEvent*)m_ViewList.GetTail();
									else 
										pBaseEvn = NULL;
									bGetEventBase = FALSE;
								}
								BOOL bSet;
								CString msg;
								POSITION pos = MsgList.GetHeadPosition();
								while( pos != NULL ) {
									pEvent =(MessageEvent*)MsgList.GetNext( pos );
									if ( pEvent ) {
										if ( !pEvent->IsAttach(MsgType) ) 
											bSet = FALSE;
										else if (pConfig && (pEvent->time > pConfig->ETime)) 
											bSet = FALSE;
										else if ( !pBaseEvn ) 
											bSet = TRUE;
										else if (pBaseEvn->time > pEvent->time) 
											bSet = FALSE;
										else if (pBaseEvn->time < pEvent->time) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nRec > pEvent->evn.nRec) 
											bSet = FALSE;
										else if (pBaseEvn->evn.nRec < pEvent->evn.nRec) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nLoc >= pEvent->evn.nLoc) 
											bSet = FALSE;
										else bSet = TRUE;
										if ( bSet && (nSearchCount > 0) ) {
											pApp->m_pEIPMsgList->GetMessage(msg, pEvent);
											AddListItem( LPCTSTR(msg) );
											//if ( m_bStopScroll == TRUE ) 
												m_ViewList.AddTail( (CObject*)pEvent );
											nSearchCount--;
										} else {
											delete pEvent;
										}
									}
								}
								MsgList.RemoveAll();
								if ( nSearchCount > 0 ) n =0;
								else SetCurSel( (MAX_WINDOW_EVENT-nSearchCount)-1 );
								flag = LOG_SEARCH_FIND;
							}
							else flag = LOG_SEARCH_NONE;
							if ( forSeek ) {
								mRec = pRec[count-1];
							}
						}
						else break;
					} while ( forSeek );
				} else {
					if (flag == LOG_SEARCH_OVER) {
						m_bLastView = TRUE;
					}
					break;
				}
				bFirstSeek = FALSE;
			} while ( !n );
			if (m_ViewList.GetCount() >= MAX_VIEW_EVENT) {
				POSITION pos1;
				n = MAX_WINDOW_EVENT - nSearchCount;
				for(i=0; i<n; i++) {
					if ( GetCount() )
						DeleteString( 0 );
					pos1 = m_ViewList.GetHeadPosition();
					if ( pos1 != NULL ) {
						pEvent = (MessageEvent*)m_ViewList.GetAt(pos1);
						m_ViewList.RemoveAt(pos1);
						delete pEvent;
					}
				}
				SetCurSel( 0 );		//^^추가 몽땅 다 넘어가던거 방지
			}
		}
		else {
			m_bLastView = TRUE;
		}
		delete m_pViewLOGFile;
		m_pViewLOGFile = NULL;
	}

	if ( bFirst ) {
		if ( bSetRecord ) return 1;
		else return 0;
	}
	else 
		return flag;
}

int  CLOGListBox::PrevMessage(BOOL bFirst , BOOL bResearch , LOGPrnConfigType *pConfig )
{
	CLLRApp *pApp = (CLLRApp *)AfxGetApp();

	BOOL bSetRecord = FALSE;
	long forSeek;
	int  i, n, count;
	int  flag = LOG_SEARCH_NONE;
	int  nSearchCount = MAX_WINDOW_EVENT;
	BOOL bFirstSeek = TRUE;
	MsgModeType	  MsgType;
	MessageEvent* pEvent;
	CObList	MsgList;
	CLOGRecord mRec;
	
	if ( !bFirst ) {
		if ( pConfig ) {
			m_SearchTime = pConfig->ETime;
			bResearch = TRUE;
		}
		else if ( !GetCount() ) {
			m_SearchTime = CJTime::GetCurrentTime();
		}
		else { 
			if ( bResearch ) 
				pEvent = (MessageEvent*)m_ViewList.GetTail();
			else 
				pEvent = (MessageEvent*)m_ViewList.GetHead();
			m_SearchTime = pEvent->time;
		}
	}
	if ( pConfig ) 	
		MsgType = pConfig->Type;
	else 	
		MsgType = m_MsgFilter;
	if ( bResearch ) {
		FlushEventObjLst( m_ViewList );
		FlushEventObjLst( m_MessageList );
		ResetContent();
	}

	m_pViewLOGFile = new CLOGFile("rb");
	if (m_pViewLOGFile ) {
		if ( m_pViewLOGFile->SetFind( m_SearchTime, FALSE ) ) {
			n = 0;
			flag = LOG_SEARCH_FIND;
			bFirstSeek = TRUE;
			do {
				BOOL bGetEventBase = TRUE;
				if ( !bFirstSeek ) {
					flag = m_pViewLOGFile->PrevRecord();
				}
				forSeek = 0;
				if (flag == LOG_SEARCH_FIND) {
					CLOGRecord *pRec = &_pRecordBlk[0];
					do {
						Sleep(0);
						count = m_pViewLOGFile->GetRecordInMinute( pRec, mRec, forSeek );
						if ( count ) {
							m_ViewLOGMessage.SetBaseRecord( mRec );
							int  alterIndex = 0;
							for(i=0; i<count; i++) {
								if ( m_ViewLOGMessage.CreateMessage(pRec[i], MsgList) )  // -1/All, 0/None, +Other/Part
								{
								}
							}
							if ( bFirstSeek ) {
								bSetRecord = TRUE;
							}
							n = MsgList.GetCount();
							if ( n ) {
								if ( !forSeek ) {
									if ( count > MAX_RECORD_MINUTE ) count = MAX_RECORD_MINUTE;
									MessageEvent* pBaseEvn;
									if ( bGetEventBase ) {
										if ( m_ViewList.GetCount() )
											pBaseEvn = (MessageEvent*)m_ViewList.GetHead();
										else 
											pBaseEvn = NULL;
										bGetEventBase = FALSE;
									}
									BOOL bSet;
									CString msg;
									POSITION pos = MsgList.GetTailPosition();
									while( pos != NULL ) {
										pEvent =(MessageEvent*)MsgList.GetPrev( pos );
										if ( pEvent ) {
											bSet = FALSE;
											if ( !pEvent->IsAttach(MsgType) ) bSet = FALSE;
											else if ( pConfig && (pEvent->time < pConfig->STime)) {
												bSet = FALSE;
											}
											else if ( !pBaseEvn ) bSet = TRUE;
											else if (pBaseEvn->time < pEvent->time) bSet = FALSE;
											else if (pBaseEvn->time > pEvent->time) bSet = TRUE;
											else if (pBaseEvn->evn.nRec < pEvent->evn.nRec) bSet = FALSE;
											else if (pBaseEvn->evn.nRec > pEvent->evn.nRec) bSet = TRUE;
											else if (pBaseEvn->evn.nLoc <= pEvent->evn.nLoc) bSet = FALSE;
											else bSet = TRUE;
											if ( bSet && (nSearchCount > 0) ) {
												pApp->m_pEIPMsgList->GetMessage(msg, pEvent);
												AddListItem( LPCTSTR(msg), 0 );
												m_ViewList.AddHead( (CObject*)pEvent );
												nSearchCount--;
											}
											else {
												delete pEvent;
											}
										}
									}
									MsgList.RemoveAll();
									if ( nSearchCount > 0 ) n =0;
									else SetCurSel( (MAX_WINDOW_EVENT-nSearchCount)-1 );
									flag = LOG_SEARCH_FIND;
								}
							}
							else flag = LOG_SEARCH_NONE;
							if ( forSeek ) {
								mRec = pRec[count-1];
							}
						}
						else break;
					} while ( forSeek );					
				}
				else break;
				bFirstSeek = FALSE;
			} while ( !n );
			if ( m_ViewList.GetCount() >= MAX_VIEW_EVENT ) {
				POSITION pos1;
				n = MAX_WINDOW_EVENT - nSearchCount;
				for(i=0; i<n; i++) {
					if ( GetCount() ) 
						DeleteString( GetCount() - 1 );
					pos1 = m_ViewList.GetTailPosition();
					if ( pos1 != NULL ) {
						pEvent = (MessageEvent*)m_ViewList.GetAt(pos1);
						m_ViewList.RemoveAt(pos1);
						delete pEvent;
					}
				}
				m_bLastView = FALSE;
			}
		}
		delete m_pViewLOGFile;
		m_pViewLOGFile = NULL;
	}
	if ( bFirst ) {
		if ( bSetRecord ) return 1;
		else return 0;
	}
	else 
		return flag;
}

int  CLOGListBox::GetNextMsgPage( BOOL bFirst, CJTime &SearchTime, LOGPrnConfigType &Config, CObList &objlist )
{
	int  find = LOG_SEARCH_NONE;
	long forSeek;

	CLOGFile* pLogFile = new CLOGFile("rb");
	if ( pLogFile ) {
		int  i, n;
		BOOL bFirstSeek = TRUE;
		MsgModeType MsgType = Config.Type;
		MessageEvent* pEvent;
		CObList	MsgList;
		CLOGRecord mRec;
	
		if ( bFirst ) SearchTime = Config.STime;
		if ( pLogFile->SetFind( SearchTime, TRUE, &Config.STime, &Config.ETime ) ) {
			n = 0;
			find = LOG_SEARCH_FIND;
			bFirstSeek = bFirst;
			do {
				BOOL bGetEventBase = TRUE;
				if ( !bFirstSeek ) {
					find = pLogFile->NextRecord();
				}
				forSeek = 0;
				if (find != LOG_SEARCH_FIND) break;
				else {
					CLOGRecord *pRec = _pRecordBlk;
					do {
						int count = pLogFile->GetRecordInMinute( pRec, mRec, forSeek, FALSE, TRUE );
						if ( count ) {
							m_ViewLOGMessage.SetBaseRecord( mRec );
							for(i=0; i<count; i++) {
								m_ViewLOGMessage.CreateMessage(pRec[i], MsgList); // -1/All, 0/None, +Other/Part
							}
							n = MsgList.GetCount();
							if ( !n ) find = LOG_SEARCH_NONE;
							else {
								if ( count > MAX_RECORD_MINUTE ) count = MAX_RECORD_MINUTE;
								MessageEvent* pBaseEvn;
								if ( bGetEventBase ) {
									if ( objlist.GetCount() ) 
										pBaseEvn = (MessageEvent*)objlist.GetTail();
									else 
										pBaseEvn = NULL;
									bGetEventBase = FALSE;
								}
								BOOL bSet;
								CString msg;
								POSITION pos = MsgList.GetHeadPosition();
								while( pos != NULL ) {
									pEvent =(MessageEvent*)MsgList.GetNext( pos );
									if ( pEvent ) {
										if ( !pEvent->IsAttach( MsgType ) ) 
											bSet = FALSE;
										else if (pEvent->time > Config.ETime) 
											bSet = FALSE;
										else if ( !pBaseEvn ) 
											bSet = TRUE;
										else if (pBaseEvn->time > pEvent->time) 
											bSet = FALSE;
										else if (pBaseEvn->time < pEvent->time) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nRec > pEvent->evn.nRec) 
											bSet = FALSE;
										else if (pBaseEvn->evn.nRec < pEvent->evn.nRec) 
											bSet = TRUE;
										else if (pBaseEvn->evn.nLoc >= pEvent->evn.nLoc) 
											bSet = FALSE;
										else bSet = TRUE;
										if ( bSet ) 
											objlist.AddTail( (CObject*)pEvent );
										else  
											delete pEvent;
									}
								}
								MsgList.RemoveAll();
								find = LOG_SEARCH_FIND;
							}
							if ( forSeek ) {
								mRec = pRec[count-1];
								n = 0;
							}
						}
						else break;
					} while ( forSeek );
				}
				bFirstSeek = FALSE;
			} while ( !n );
			pLogFile->GetSearchTime( SearchTime );
		}
		else {
			find = LOG_SEARCH_OVER;
		}
		delete pLogFile;
	}

	return find;
}

//=======================================================
//
//  함 수 명 :  OnKeyDown
//  함수출력 :
//  함수입력 :
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :
//  설    명 :  키 입력에 따른 처리를 한다. 
//
//=======================================================
void CLOGListBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	int sel = GetCurSel();

	switch ( nChar ) {
	case VK_PRIOR	:
	case VK_UP		:	if ( !sel ) PrevMessage(FALSE);
						break;
	case VK_NEXT	:
	case VK_DOWN	:	if (sel == (GetCount() - 1)) {
							NextMessage(FALSE);
							if(!m_bLastView) SetCurSel(0);
						}
						break;
	case VK_END		:	if(!m_bLastView){
							LastMessage();
							SetCurSel( GetCount() - 1 );
						}
						break;
	default			:	break;
	}
	
	CListBox::OnKeyDown(nChar, nRepCnt, nFlags);
}

//=======================================================
//
//  함 수 명 :  DrawItem
//  함수출력 :  없음
//  함수입력 :  LPDRAWITEMSTRUCT lpDrawItemStruct
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  2.01
//  설    명 :  로그리스트에 출력한다. 
//
//=======================================================
void CLOGListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	HBITMAP hOldB;
	HDC hMemDC;

	hMemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
	CBitmap			m_bmpF,m_bmpS,m_bmpC,m_bmpE;
	COLORREF bkcolor;
	COLORREF textcolor;
	COLORREF m_clrText;
	COLORREF m_clrTextBk;
	COLORREF m_clrBkgnd;

	m_bmpF.LoadBitmap(IDB_ERROR);
	m_bmpC.LoadBitmap(IDB_CONTROL);
	m_bmpE.LoadBitmap(IDB_ETC);
	m_bmpS.LoadBitmap(IDB_STATE);

    CRect  rcItem(lpDrawItemStruct->rcItem);	// To draw the focus rect.
    CRect  rClient(rcItem);						// Rect to highlight the Item
    CRect  rText(rcItem);						// Rect To display the Text
	rText.left += 16;
	rText.top += 2;

	m_clrText	= ::GetSysColor(COLOR_WINDOWTEXT);
	m_clrTextBk = ::GetSysColor(COLOR_WINDOW);
	m_clrBkgnd	= ::GetSysColor(COLOR_WINDOW);

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	CString szText = "";
	if ( GetCount() ) {
		GetText( lpDrawItemStruct->itemID,  szText );
	}
	UINT nJustify = DT_LEFT;

	char *pstr = (char*)( LPCTSTR(szText) + 1 );	// add ( 26 -> 29) Cycle 추가되었음.

	if ( szText != "" ) {
		if ( !strncmp(pstr, "ER", 2) && (m_iControlFlag == 0) ){
			textcolor = RGB(200,   0,   0);
			hOldB = (HBITMAP)SelectObject(hMemDC,m_bmpF)  ;
			BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left+1,lpDrawItemStruct->rcItem.top+1,16,16,hMemDC,0,0,SRCCOPY);
		} else if ( !strncmp(pstr, "CO", 2 ) ){
			textcolor = RGB(0, 0, 200);
			hOldB = (HBITMAP)SelectObject(hMemDC,m_bmpC)  ;
			BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left+1,lpDrawItemStruct->rcItem.top+1,16,16,hMemDC,0,0,SRCCOPY);
		} else if ( !strncmp(pstr, "ST", 2 ) && (m_iControlFlag == 0)){
			textcolor = RGB(0,   0,   0);
			hOldB = (HBITMAP)SelectObject(hMemDC,m_bmpS)  ;
			BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left+1,lpDrawItemStruct->rcItem.top+1,16,16,hMemDC,0,0,SRCCOPY);
		} else if ( !strncmp(pstr, "ET", 2) && (m_iControlFlag == 0)){
			textcolor = RGB(0, 200, 0);
			hOldB = (HBITMAP)SelectObject(hMemDC,m_bmpE)  ;
			BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left+1,lpDrawItemStruct->rcItem.top+1,16,16,hMemDC,0,0,SRCCOPY);
		} else textcolor = RGB(0,   0,   0);
	} else {
		textcolor = m_clrText;
	}
	
	if ( lpDrawItemStruct->itemAction & ODA_FOCUS ) {
		bkcolor = textcolor;
		textcolor = m_clrTextBk;
	} else {
		bkcolor = m_clrTextBk;	// default color
	}

	pDC->SetTextColor( textcolor );
	pDC->SetBkColor( bkcolor );		
	pDC->FillSolidRect( &rText, bkcolor );
	pDC->DrawText(pstr, -1, rText,nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
	SelectObject(hMemDC,hOldB);
	DeleteDC(hMemDC);
}

//=======================================================
//
//  함 수 명 :	AddListItem
//  함수출력 :  없음
//  함수입력 :	LPCTSTR pStr : 추가될 문자열  int attop  : 현재 로그상태가 마지막 페이지인지 여부 
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  로그 리스트 박스에 문자열을 추가 한다. 
//  설    명 :
//
//=======================================================
void CLOGListBox::AddListItem( LPCTSTR pStr, int attop )
{
	
	if ( m_bReplayFirstCK == TRUE ){
		return;
	}
	CString strTemp,strTemp1,strTemp2;
	strTemp = pStr;
	strTemp1 = strTemp.Left(31);
	strTemp2 = strTemp.Mid(34,strTemp.GetLength());
	strTemp = strTemp1 + strTemp2;
	pStr = (LPCSTR) strTemp;
	if (!attop) InsertString( 0, pStr );
	else AddString( pStr );
}


//=======================================================
//
//  함 수 명 :	AddListItem
//  함수출력 :  없음
//  함수입력 :	LPCTSTR pStr : 추가될 문자열  int attop  : 현재 로그상태가 마지막 페이지인지 여부 
//  작 성 자 :  OMANI
//  작성날짜 :  2004-10-11
//  버    전 :  로그 리스트 박스에 문자열을 추가 한다. 
//  설    명 :
//
//=======================================================
void CLOGListBox::AddSharedMemory( LPCTSTR pStr, int attop )
{
	strcpy ( &errmsg[0] , pStr );

	CString m_sText;
	m_sText = errmsg;

	if ( m_sText.Left(3) == " ER" || m_sText.Find("Login") > 0 ) {
		m_sText = m_sText.Mid(42,m_sText.GetLength());
		if ( m_sText.Find("Login") > 0 ) m_sText = "Welcome. " + m_sText;
		m_sText = "<voice required='Gender=Female'> <pitch middle='5'>" + m_sText;
		strcpy ( &errmsg[0] , m_sText );
		voiceflag = 1;
	} else {
		voiceflag = 0;
	}

}


