#if !defined(AFX_TEXTSET_H__562ACD83_2F07_11D4_A8B3_005004FC3DE7__INCLUDED_)
#define AFX_TEXTSET_H__562ACD83_2F07_11D4_A8B3_005004FC3DE7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTextSet DAO recordset

class CTextSet : public CDaoRecordset
{
public:
	CTextSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTextSet)

// Field/Param Data
	//{{AFX_FIELD(CTextSet, CDaoRecordset)
	CString	m_Station;
	CString	m_StartDate;
	CString	m_EndDate;
	CString	m_Type;
	CString	m_SearchType;
	CString	m_User;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTSET_H__562ACD83_2F07_11D4_A8B3_005004FC3DE7__INCLUDED_)
