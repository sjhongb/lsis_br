#if !defined(AFX_CONTROLDLG_H__EA01B6DF_9A35_46E4_9ED9_31B552D2F684__INCLUDED_)
#define AFX_CONTROLDLG_H__EA01B6DF_9A35_46E4_9ED9_31B552D2F684__INCLUDED_

#include "..\INCLUDE\JTime.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ControlDlg.h : header file
//

#include "BitmapSlider.h"
#include "XPStyleButtonST.h"

/////////////////////////////////////////////////////////////////////////////
// CControlDlg dialog

class CControlDlg : public CDialog
{
// Construction
public:
	void UpdatePlayTime(CJTime &playTime);
	CControlDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CControlDlg)
	enum { IDD = IDD_DIALOG_CONTROL };
	CXPStyleButtonST	m_ctrlForwardBtn;
	CXPStyleButtonST	m_ctrlRewindBtn;
	CXPStyleButtonST	m_ctrlStopBtn;
	CXPStyleButtonST	m_ctrlPlayBtn;
	CXPStyleButtonST	m_ctrlFileBtn;
	CXPStyleButtonST	m_ctrlLoadBtn;
	CXPStyleButtonST	m_ctrlTitleBtn;
	CBitmapSlider	m_ctrlTimeSlider;
	CBitmapSlider	m_ctrlSpeedSlider;
	CBitmapSlider	m_ctrlFrameSkipSlider;
	CDateTimeCtrl	m_ctrlDateDT;
	CDateTimeCtrl	m_ctrlTimeDT;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CControlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonPlay();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnButtonStop();
	afx_msg void OnKillfocusDatetimeTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusDatetimeTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSliderSpeed(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonTitle();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	//}}AFX_MSG
	afx_msg LRESULT OnBitmapSliderMoved(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBitmapSliderMoving(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	BOOL m_bIsMinimized;
	int m_iReplaySpeed;
	BOOL m_bIsPlay;
	CThemeHelperST		m_ThemeHelper;	//for XP Style button

	CFont *m_pDateFont;
	CJTime m_curDateTime;
	CJTime m_startDateTime;
	CJTime m_endDateTime;
	CString m_strLOGDir;

	void UpdateTime(BOOL bSaveAndValidate);
	void UpdateDate(BOOL bSaveAndValidate);
	void SetButtonTooltip();
	void SetPosition();
	void SetMinPosition();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLDLG_H__EA01B6DF_9A35_46E4_9ED9_31B552D2F684__INCLUDED_)

