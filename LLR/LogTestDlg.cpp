// LogTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "LogTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogTestDlg dialog


CLogTestDlg::CLogTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLogTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLogTestDlg)
		// NOTE : the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLogTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogTestDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogTestDlg, CDialog)
	//{{AFX_MSG_MAP(CLogTestDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogTestDlg message handlers

void CLogTestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
