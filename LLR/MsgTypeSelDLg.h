// MsgTypeSelDLg.h : header file
//
#include "llr.h"
#include "../include/MessageNo.h"

/////////////////////////////////////////////////////////////////////////////
// CMsgTypeSelDLg dialog

class CMsgTypeSelDLg : public CDialog
{
// Construction
public:
	CMsgTypeSelDLg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMsgTypeSelDLg)
	enum { IDD = IDD_DLG_SELECT_MSG_VIEWTYPE };
	BOOL	m_bActionInfo;
	BOOL	m_bErrorInfo;
	BOOL	m_bOperateInfo;
	BOOL	m_bSwitchType;
	BOOL	m_bSignalType;
	BOOL	m_bSystemType;
	BOOL	m_bOtherInfo;
	BOOL	m_bTrackType;
	//}}AFX_DATA

	MsgModeType m_ModeType;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgTypeSelDLg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMsgTypeSelDLg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButtonAllModeSel();
	afx_msg void OnButtonAllTypeSel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
