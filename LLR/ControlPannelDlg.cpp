// ControlPannelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "llr.h"
#include "ControlPannelDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CControlPannelDlg dialog


CControlPannelDlg::CControlPannelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CControlPannelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CControlPannelDlg)
		// NOTE: the ClassWizard will add member initialization here
	m_iFrameSkip = 0;
	m_iReplayYear = 0;
	m_iReplayMonth = 0;
	m_iReplayDay = 0;
	m_iReplayHour = 0;
	m_iReplayMinute = 0;
	m_iReplaySecond = 0;
	m_iSliderSpeedNumber = 0;

	m_bPlay = FALSE;
	//}}AFX_DATA_INIT
	m_bDisplayFlag = FALSE;
	m_bSpecialPlay = FALSE;
	m_bEditFlag = FALSE;
	m_iReplaySpeed = 248;
	m_bPlayStatus = FALSE;
	m_strPathName = "";
}


void CControlPannelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CControlPannelDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_BTN_EXIT, m_cBtnExit);
	DDX_Control(pDX, IDC_BTN_SETTIME, m_cBtnTimeSet);
	DDX_Control(pDX, IDC_BTN_PAUSE, m_cBtnPause);
	DDX_Control(pDX, IDC_BTN_BACK, m_cBtnBackFF);
	DDX_Control(pDX, IDC_BTN_STOP, m_cBtnStop);
	DDX_Control(pDX, IDC_BTN_PLAY, m_cBtnFrontPlay);
	DDX_Control(pDX, IDC_BTN_FRONT, m_cBtnFrontFF);
	DDX_Control(pDX, IDC_BTN_OPEN, m_cBtnOpen);
	DDX_Control(pDX, IDC_BTN_VIEW_LOG, m_cBtnLogView);
	DDX_Control(pDX, IDC_STATIC_CURTIME, m_cStaticCurTime);
	DDX_Control(pDX, IDC_STATIC_LOGTIME, m_cStaticLogTime);
	DDX_Control(pDX, IDC_SLIDER_FRAME_SKIP, m_cFrameSkip);
	DDX_Control(pDX, IDC_SLIDER_SPEED, m_cReplaySpeed);
	DDX_Control(pDX, IDC_EDT_REPLAY_YEAR, m_cReplayYear);
	DDX_Control(pDX, IDC_EDT_REPLAY_MONTH, m_cReplayMonth);
	DDX_Control(pDX, IDC_EDT_REPLAY_DAY, m_cReplayDay);
	DDX_Control(pDX, IDC_EDT_REPLAY_HOUR, m_cReplayHour);
	DDX_Control(pDX, IDC_EDT_REPLAY_MINUTE, m_cReplayMinute);
	DDX_Control(pDX, IDC_EDT_REPLAY_SECOND, m_cReplaySecond);
	DDX_Text(pDX, IDC_EDT_REPLAY_YEAR, m_iReplayYear);
	DDX_Text(pDX, IDC_EDT_REPLAY_MONTH, m_iReplayMonth);
	DDX_Text(pDX, IDC_EDT_REPLAY_DAY, m_iReplayDay);
	DDX_Text(pDX, IDC_EDT_REPLAY_HOUR, m_iReplayHour);
	DDX_Text(pDX, IDC_EDT_REPLAY_MINUTE, m_iReplayMinute);
	DDX_Text(pDX, IDC_EDT_REPLAY_SECOND, m_iReplaySecond);
	DDX_Control(pDX, IDC_SPIN_SEC, m_cSpinSecond);
	DDX_Control(pDX, IDC_SPIN_MIN, m_cSpinMinute);
	DDX_Control(pDX, IDC_SPIN_HOUR, m_cSpinHour);
	DDX_Control(pDX, IDC_SPIN_DAY, m_cSpinDay);
	DDX_Control(pDX, IDC_SPIN_MONTH, m_cSpinMonth);
	DDX_Control(pDX, IDC_SPIN_YEAR, m_cSpinYear);
	DDX_Slider(pDX, IDC_SLIDER_FRAME_SKIP, m_iFrameSkip);
	DDX_Slider(pDX, IDC_SLIDER_SPEED, m_iSliderSpeedNumber);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CControlPannelDlg, CDialog)
	//{{AFX_MSG_MAP(CControlPannelDlg)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBtnExit)
	ON_BN_CLICKED(IDC_BTN_PLAY, OnBtnPlay)
	ON_BN_CLICKED(IDC_BTN_SETTIME, OnBtnSettime)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_PAUSE, OnBtnPause)
	ON_BN_CLICKED(IDC_BTN_BACK, OnBtnBack)
	ON_BN_CLICKED(IDC_BTN_FRONT, OnBtnFront)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBtnStop)
	ON_BN_CLICKED(IDC_BTN_VIEW_LOG, OnBtnViewLog)
	ON_BN_CLICKED(IDC_BTN_OPEN, OnBtnOpen)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_FRAME_SKIP, OnReleasedcaptureSliderFrameSkip)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SPEED, OnReleasedcaptureSliderSpeed)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_YEAR, OnSetfocusEdtYear)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_YEAR, OnKillfocusEdtYear)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_MONTH, OnSetfocusEdtMonth)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_MONTH, OnKillfocusEdtMonth)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_DAY, OnSetfocusEdtDay)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_DAY, OnKillfocusEdtDay)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_HOUR, OnSetfocusEdtHour)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_HOUR, OnKillfocusEdtHour)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_MINUTE, OnSetfocusEdtMinute)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_MINUTE, OnKillfocusEdtMinute)
	ON_EN_SETFOCUS(IDC_EDT_REPLAY_SECOND, OnSetfocusEdtSecond)
	ON_EN_KILLFOCUS(IDC_EDT_REPLAY_SECOND, OnKillfocusEdtSecond)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlPannelDlg message handlers

void CControlPannelDlg::OnBtnExit() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();

	if ( AfxMessageBox("Program will be exit , Are you sure ?",MB_OKCANCEL ) ==  IDOK )
	{
		pWnd->OnLLRExit();
	}
}

BOOL CControlPannelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_cReplaySpeed.SetRange(1,6);
	m_cFrameSkip.SetRange(0,8);
	m_iSliderSpeedNumber = 2;
	m_iFrameSkip = 0;
	m_cReplaySpeed.SetPos(2);
	m_cFrameSkip.SetPos(0);

	m_cStaticCurTime.SetNumberOfLines(1);
	m_cStaticCurTime.SetXCharsPerLine(21);
	m_cStaticCurTime.SetSize(CMatrixStatic::TINY);
	m_cStaticCurTime.AdjustClientXToSize(21);
	m_cStaticCurTime.AdjustClientYToSize(1);
	m_cStaticCurTime.SetText(" 0000/00/00 00:00:00");

	m_cStaticLogTime.SetNumberOfLines(1);
	m_cStaticLogTime.SetXCharsPerLine(21);
	m_cStaticLogTime.SetSize(CMatrixStatic::TINY);
	m_cStaticLogTime.AdjustClientXToSize(21);
	m_cStaticLogTime.AdjustClientYToSize(1);
	m_cStaticLogTime.SetText(" 0000/00/00 00:00:00");

	COLORREF color = RGB(49, 54, 57 );
	m_cBtnExit.SetColor(0, color );
	m_cBtnExit.SetColor(1, color );
	m_cBtnExit.SetColor(2, color );
	m_cBtnExit.SetColor(3, color );
	m_cBtnExit.SetColor(4, color );
	m_cBtnExit.SetBitmaps(IDB_BITMAP_BtnPower,RGB(0, 0, 0));

	m_cBtnTimeSet.SetColor(0, color );
	m_cBtnTimeSet.SetColor(1, color );
	m_cBtnTimeSet.SetColor(2, color );
	m_cBtnTimeSet.SetColor(3, color );
	m_cBtnTimeSet.SetColor(4, color );
	m_cBtnTimeSet.SetBitmaps(IDB_BITMAP_BtnTimeSet,RGB(0, 0, 0));

	m_cBtnPause.SetColor(0, color );
	m_cBtnPause.SetColor(1, color );
	m_cBtnPause.SetColor(2, color );
	m_cBtnPause.SetColor(3, color );
	m_cBtnPause.SetColor(4, color );
	m_cBtnPause.SetBitmaps(IDB_BITMAP_BtnPause,RGB(0, 0, 0));
	
	m_cBtnBackFF.SetColor(0, color );
	m_cBtnBackFF.SetColor(1, color );
	m_cBtnBackFF.SetColor(2, color );
	m_cBtnBackFF.SetColor(3, color );
	m_cBtnBackFF.SetColor(4, color );
	m_cBtnBackFF.SetBitmaps(IDB_BITMAP_BtnBackFF,RGB(0, 0, 0));
	
	m_cBtnStop.SetColor(0, color );
	m_cBtnStop.SetColor(1, color );
	m_cBtnStop.SetColor(2, color );
	m_cBtnStop.SetColor(3, color );
	m_cBtnStop.SetColor(4, color );
	m_cBtnStop.SetBitmaps(IDB_BITMAP_BtnStop,RGB(0, 0, 0));
	
	m_cBtnFrontPlay.SetColor(0, color );
	m_cBtnFrontPlay.SetColor(1, color );
	m_cBtnFrontPlay.SetColor(2, color );
	m_cBtnFrontPlay.SetColor(3, color );
	m_cBtnFrontPlay.SetColor(4, color );
	m_cBtnFrontPlay.SetBitmaps(IDB_BITMAP_BtnFrontPlay,RGB(0, 0, 0));
	
	m_cBtnFrontFF.SetColor(0, color );
	m_cBtnFrontFF.SetColor(1, color );
	m_cBtnFrontFF.SetColor(2, color );
	m_cBtnFrontFF.SetColor(3, color );
	m_cBtnFrontFF.SetColor(4, color );
	m_cBtnFrontFF.SetBitmaps(IDB_BITMAP_BtnFrontFF,RGB(0, 0, 0));
	
	m_cBtnOpen.SetColor(0, color );
	m_cBtnOpen.SetColor(1, color );
	m_cBtnOpen.SetColor(2, color );
	m_cBtnOpen.SetColor(3, color );
	m_cBtnOpen.SetColor(4, color );
	m_cBtnOpen.SetBitmaps(IDB_BITMAP_BtnOpen,RGB(0, 0, 0));
	
	m_cBtnLogView.SetColor(0, color );
	m_cBtnLogView.SetColor(1, color );
	m_cBtnLogView.SetColor(2, color );
	m_cBtnLogView.SetColor(3, color );
	m_cBtnLogView.SetColor(4, color );
	m_cBtnLogView.SetBitmaps(IDB_BITMAP_BtnLogView,RGB(0, 0, 0));

	UpdateData(TRUE);

	m_cSpinYear.SetRange(2010,2099);
	m_cSpinMonth.SetRange(1,12);
	m_cSpinDay.SetRange(1,31);
	m_cSpinHour.SetRange(0,23);
	m_cSpinMinute.SetRange(0,59);
	m_cSpinSecond.SetRange(0,59);
	
	CTime time;
	time = CTime::GetCurrentTime();
	
	m_iReplayYear = time.GetYear();
	m_iReplayMonth = time.GetMonth();
	m_iReplayDay = time.GetDay();
	m_iReplayHour = time.GetHour();
	m_iReplayMinute = time.GetMinute();
	m_iReplaySecond = 0;//time.GetSecond();

	UpdateData(FALSE);
	
	SetTimer ( 10, 200, NULL );
//	SetTimer ( 20, 2000, NULL );
	SetTimer ( 30, 1000, NULL );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CControlPannelDlg::OnBtnPlay() 
{
	// TODO: Add your control notification handler code here

	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	UpdateData(TRUE);

	if(m_bSpecialPlay == FALSE)
	{
		pWnd->m_ReplayTime.nYear = m_iReplayYear-2000;
		pWnd->m_ReplayTime.nMonth = m_iReplayMonth;
		pWnd->m_ReplayTime.nDate = m_iReplayDay;
		pWnd->m_ReplayTime.nHour = m_iReplayHour;
		pWnd->m_ReplayTime.nMin = m_iReplayMinute;
		pWnd->m_ReplayTime.nSec = m_iReplaySecond;
		pWnd->SetPlayTime();
		m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0),RGB(0, 255, 50),RGB(0, 103, 30));
		m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0),RGB(0, 255, 50),RGB(0, 103, 30));		
	}
	else
	{
		m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0),RGB(63, 181, 255),RGB(23, 64, 103));
		m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0),RGB(63, 181, 255),RGB(23, 64, 103));	
	}

	pWnd->LogFrontRun(TRUE);
}

void CControlPannelDlg::OnBtnSettime() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	UpdateData (TRUE);
	
	pWnd->m_ReplayTime.nYear = m_iReplayYear-2000;
	pWnd->m_ReplayTime.nMonth = m_iReplayMonth;
	pWnd->m_ReplayTime.nDate = m_iReplayDay;
	pWnd->m_ReplayTime.nHour = m_iReplayHour;
	pWnd->m_ReplayTime.nMin = m_iReplayMinute;
	pWnd->m_ReplayTime.nSec = m_iReplaySecond;
	pWnd->SetPlayTime();	
	pWnd->LogPlaySetTime();
}

void CControlPannelDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	CString strTime;
	int iYear,iMonth,iDay,iHour,iMin,iSec;
	CTime time;
	time = CTime::GetCurrentTime();
	CString strTemp;
	
	if(m_bDisplayFlag == FALSE)
	{
		if(m_iReplayYear > 2000)
		{
			strTemp.Format("%d",m_iReplayYear);
		}
		else
		{
			strTemp.Format("%d",m_iReplayYear+2000);
		}
		GetDlgItem(IDC_EDT_REPLAY_YEAR)->SetWindowText(strTemp);
		strTemp.Format("%d",m_iReplayMonth);
		GetDlgItem(IDC_EDT_REPLAY_MONTH)->SetWindowText(strTemp);
		strTemp.Format("%d",m_iReplayDay);
		GetDlgItem(IDC_EDT_REPLAY_DAY)->SetWindowText(strTemp);
		strTemp.Format("%d",m_iReplayHour);
		GetDlgItem(IDC_EDT_REPLAY_HOUR)->SetWindowText(strTemp);
		strTemp.Format("%d",m_iReplayMinute);
		GetDlgItem(IDC_EDT_REPLAY_MINUTE)->SetWindowText(strTemp);
		strTemp.Format("%d",m_iReplaySecond);
		GetDlgItem(IDC_EDT_REPLAY_SECOND)->SetWindowText(strTemp);
		m_cFrameSkip.SetFocus();
		m_cReplaySpeed.SetFocus();
		strTemp.Format("%d Frame Skipped",m_iFrameSkip);
		GetDlgItem(IDC_STATIC_FRAME_SKIP)->SetWindowText(strTemp);
		switch(m_iSliderSpeedNumber)
		{
		case 1: m_fReplaySpeed = 0.5; break;
		case 2: m_fReplaySpeed = 1; break;
		case 3: m_fReplaySpeed = 2; break;
		case 4: m_fReplaySpeed = 4; break;
		case 5: m_fReplaySpeed = 8; break;
		case 6: m_fReplaySpeed = 16; break;
		}
		strTemp.Format("Speed x%.1f",m_fReplaySpeed);
		GetDlgItem(IDC_STATIC_SPEED)->SetWindowText(strTemp);
		m_bDisplayFlag = TRUE;		
	}
	if ( nIDEvent == 10 ) 
	{
		iYear = time.GetYear();
		iMonth = time.GetMonth();
		iDay = time.GetDay();
		iHour = time.GetHour();
		iMin = time.GetMinute();
		iSec = time.GetSecond();	
		strTime.Format(" %04d/%02d/%02d %02d:%02d:%02d ",iYear,iMonth,iDay,iHour,iMin,iSec);		
		m_cStaticCurTime.SetText(strTime);
	}
	else if ( nIDEvent == 20 ) 
	{
		if ( m_bEditFlag == FALSE ) {
			if ( m_iReplayYear > 2000 ) {
				strTemp.Format("%d",m_iReplayYear);
			} else {
				strTemp.Format("%d",m_iReplayYear+2000);
			}
			GetDlgItem(IDC_EDT_REPLAY_YEAR)->SetWindowText(strTemp);
			strTemp.Format("%d",m_iReplayMonth);
			GetDlgItem(IDC_EDT_REPLAY_MONTH)->SetWindowText(strTemp);
			strTemp.Format("%d",m_iReplayDay);
			GetDlgItem(IDC_EDT_REPLAY_DAY)->SetWindowText(strTemp);
			strTemp.Format("%d",m_iReplayHour);
			GetDlgItem(IDC_EDT_REPLAY_HOUR)->SetWindowText(strTemp);
			strTemp.Format("%d",m_iReplayMinute);
			GetDlgItem(IDC_EDT_REPLAY_MINUTE)->SetWindowText(strTemp);
			strTemp.Format("%d",m_iReplaySecond);
			GetDlgItem(IDC_EDT_REPLAY_SECOND)->SetWindowText(strTemp);
			m_cFrameSkip.SetFocus();
			m_cReplaySpeed.SetFocus();
			strTemp.Format("%d Frame Skipped",m_iFrameSkip);
			GetDlgItem(IDC_STATIC_FRAME_SKIP)->SetWindowText(strTemp);
			switch(m_iSliderSpeedNumber)
			{
			case 1: m_fReplaySpeed = 0.5; break;
			case 2: m_fReplaySpeed = 1; break;
			case 3: m_fReplaySpeed = 2; break;
			case 4: m_fReplaySpeed = 4; break;
			case 5: m_fReplaySpeed = 8; break;
			case 6: m_fReplaySpeed = 16; break;
			}
			strTemp.Format("Speed x%.1f",m_fReplaySpeed);
			GetDlgItem(IDC_STATIC_SPEED)->SetWindowText(strTemp);
		}
	}
	else if ( nIDEvent == 30 ) 
	{
		m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));
		m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));	
		KillTimer(30);
		OnBtnPlay();
	}

	CDialog::OnTimer(nIDEvent);
}

void CControlPannelDlg::OnBtnPause() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	m_bPlayStatus = FALSE;
	pWnd->LogPlayPause(TRUE);
}

void CControlPannelDlg::OnBtnBack() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	int is_leap_year=0;		//�����̸� 1, ������ �ƴϸ� 0
	is_leap_year = (!(m_iMoveYear%4) && (m_iMoveYear%100) || !(m_iMoveYear%400))? 1 : 0; 
	
	m_iMoveMin -= 10;
	if (m_iMoveMin < 0)
	{
		m_iMoveHour--;
		m_iMoveMin = 60+m_iMoveMin;
	}
	
	if (m_iMoveHour < 0)
	{
		m_iMoveDay--;
		m_iMoveHour = 24+m_iMoveHour;
	}
	
	if (m_iMoveDay <1)
	{
		m_iMoveMonth--;
		
		switch(m_iMoveMonth)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			m_iMoveDay = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			m_iMoveDay = 30;
			break;
		case 2:
			if (is_leap_year == 1) {
				m_iMoveDay = 29;
			} else {
				m_iMoveDay = 28;
			}
			break;
		case 0:
			{
				m_iMoveYear--;
				m_iMoveMonth=12;
			}
		}
	}
	pWnd->m_ReplayTime.nYear = m_iMoveYear-2000;
	pWnd->m_ReplayTime.nMonth = m_iMoveMonth;
	pWnd->m_ReplayTime.nDate = m_iMoveDay;
	pWnd->m_ReplayTime.nHour = m_iMoveHour;
	pWnd->m_ReplayTime.nMin = m_iMoveMin;
	pWnd->m_ReplayTime.nSec = m_iMoveSec;
	pWnd->SetPlayTime();	
	pWnd->LogPlaySetTime();
}

void CControlPannelDlg::OnBtnFront() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	int is_leap_year;		//�����̸� 1, ������ �ƴϸ� 0
	is_leap_year = (!(m_iMoveYear%4) && (m_iMoveYear%100) || !(m_iMoveYear%400))? 1 : 0; 
	
	m_iMoveMin += 10;
	if (m_iMoveMin >= 60)
	{
		m_iMoveHour++;
		m_iMoveMin -= 60;
	}
	
	if (m_iMoveHour >= 24)
	{
		m_iMoveDay++;
		m_iMoveHour -= 24;
	}
	
	if (m_iMoveDay==29)
	{
		switch(m_iMoveMonth)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
		case 4:
		case 6:
		case 9:
		case 11:
			break;
		case 2:
			if (is_leap_year == 0)
			{
				m_iMoveMonth++;
				m_iMoveDay-=28;
			}
			break;
		}
	}
	else if (m_iMoveDay==30)
	{
		switch(m_iMoveMonth)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
		case 4:
		case 6:
		case 9:
		case 11:
			break;
		case 2:
			if (is_leap_year == 1)
			{
				m_iMoveMonth++;
				m_iMoveDay-=29;
			}
			break;
		}
	}
	else if (m_iMoveDay==31)
	{
		switch(m_iMoveMonth)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			m_iMoveMonth++;
			m_iMoveDay-=30;
			break;
		case 2:
			break;
		}
	}
	
	else if (m_iMoveDay==32)
	{
		switch(m_iMoveMonth)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			m_iMoveMonth++;
			m_iMoveDay-=31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
		case 2:
			break;
		}
	}
	
	if (m_iMoveMonth>12)
	{
		m_iMoveYear++;
		m_iMoveMonth-=12;
	}
	
	pWnd->m_ReplayTime.nYear = m_iMoveYear-2000;
	pWnd->m_ReplayTime.nMonth = m_iMoveMonth;
	pWnd->m_ReplayTime.nDate = m_iMoveDay;
	pWnd->m_ReplayTime.nHour = m_iMoveHour;
	pWnd->m_ReplayTime.nMin = m_iMoveMin;
	pWnd->m_ReplayTime.nSec = m_iMoveSec;
	pWnd->SetPlayTime();	
	pWnd->LogPlaySetTime();
}

void CControlPannelDlg::OnBtnStop() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	pWnd->LogPlayStop(TRUE);	
	pWnd->ReleaseLogFile ();
	
	m_strPathName = "";
	
	m_cReplayYear.EnableWindow(TRUE);
	m_cReplayMonth.EnableWindow(TRUE);
	m_cReplayDay.EnableWindow(TRUE);
	
	m_bSpecialPlay = FALSE;
	m_bPlayStatus = FALSE;
	m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));
	m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));	
	
	//---------------------------------------------------------------------------------
	
	CTime time;
	time = CTime::GetCurrentTime();
	
	m_iReplayYear = time.GetYear();
	m_iReplayMonth = time.GetMonth();
	m_iReplayDay = time.GetDay();
	m_iReplayHour = time.GetHour();
	m_iReplayMinute = time.GetMinute();
	m_iReplaySecond = 0;//time.GetSecond();
	
	m_cReplaySpeed.SetPos(2);
	UpdateData (FALSE);
	
	m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));
	m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(103, 30, 0));	
	OnBtnPlay();
}

void CControlPannelDlg::OnBtnViewLog() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
	pWnd->ShowLogView();
}

void CControlPannelDlg::OnBtnOpen() 
{
	// TODO: Add your control notification handler code here
// 	MessageBox("Click the Open");
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	
    static char  szFilter[] = "Log data file (*.log)";
    CString    filename,strCurLogFile;
    CString    temp, errstring;
    int        iCnt = 0;
	
	HFILE           LogFile;

	pWnd->m_ReplayTime.nHour = 0;
	pWnd->m_ReplayTime.nMin = 0;
	
    CFileDialog  m_filedlg( TRUE, "log","*.log",OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
    int ret_code = m_filedlg.DoModal();
    if(ret_code != IDOK) return;
	
	
	UpdateData(TRUE);

	m_bSpecialPlay = TRUE;
	m_bDisplayFlag = FALSE;
	
    filename = m_filedlg.GetFileName();
	m_strPathName = m_filedlg.GetPathName();
 	LOGHeaderType	m_Head;
 	LOGIndexType	m_stIndex;
 	
 	BYTE nData[500];
 	LogFile = _lopen(m_strPathName,OF_READ);
 	_lread( LogFile,&m_stIndex,SIZE_LOGINDEX);
 	_lread( LogFile, &m_Head, 4);
 	_lread( LogFile, &nData, (short)m_Head.Length + sizeof(CJTime));
 	_lclose( LogFile );
 	
 	m_ReplayTime.nYear = nData[9];
 	m_ReplayTime.nMonth = nData[8];
 	m_ReplayTime.nDate = nData[7];
 	m_ReplayTime.nHour = nData[6];
 	m_ReplayTime.nMin = nData[5];
 	m_ReplayTime.nSec = nData[4];
 	
  	pWnd->LogPlayStop(TRUE);	
  	pWnd->ReleaseLogFile ();
 	
 	
 	pWnd->SetLogFile(m_strPathName);
 	pWnd->LogSelectRun(TRUE);
 	
	pWnd->m_ReplayTime = m_ReplayTime;

 	m_iReplayYear = pWnd->m_ReplayTime.nYear;
 	m_iReplayYear = m_iReplayYear + 2000;
 	m_iReplayMonth = pWnd->m_ReplayTime.nMonth;
 	m_iReplayDay = pWnd->m_ReplayTime.nDate;
 	m_iReplayHour = pWnd->m_ReplayTime.nHour;
 	m_iReplayMinute = pWnd->m_ReplayTime.nMin;
 	m_iReplaySecond = pWnd->m_ReplayTime.nSec;
 	
 	UpdateData(FALSE);
 	temp.Format("%04d",m_iReplayYear);
 	m_cReplayYear.SetWindowText(temp);
 	m_cReplayYear.EnableWindow(FALSE);
 	m_cReplayMonth.EnableWindow(FALSE);
 	m_cReplayDay.EnableWindow(FALSE);
 	
 	m_bDisplayFlag = FALSE;	
 	
 	m_cStaticCurTime.SetDisplayColors(RGB(0, 0, 0),RGB(63, 181, 255),RGB(23, 64, 103));
 	m_cStaticLogTime.SetDisplayColors(RGB(0, 0, 0),RGB(63, 181, 255),RGB(23, 64, 103));	
 
}

void CControlPannelDlg::OnReleasedcaptureSliderFrameSkip(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	UpdateData (TRUE);
	
	CString strFrameSkip;
	strFrameSkip.Format("%d Frame Skipped",m_iFrameSkip);
	GetDlgItem(IDC_STATIC_FRAME_SKIP)->SetWindowText(strFrameSkip);
	
	m_cFrameSkip.SetPos(m_iFrameSkip);
	
	pWnd->m_iFrameSkip = m_iFrameSkip;
	pWnd->LogSetFrameSkip ( m_iFrameSkip ); 
	*pResult = 0;
}

void CControlPannelDlg::OnReleasedcaptureSliderSpeed(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pWnd = (CMainFrame*)AfxGetMainWnd();
	UpdateData (TRUE);
	
	switch(m_iSliderSpeedNumber)
	{
	case 1: m_fReplaySpeed = 0.5; m_iReplaySpeed = 496; break;
	case 2: m_fReplaySpeed = 1; m_iReplaySpeed = 248; break;
	case 3: m_fReplaySpeed = 2; m_iReplaySpeed = 120; break;
	case 4: m_fReplaySpeed = 4; m_iReplaySpeed = 58; break;
	case 5: m_fReplaySpeed = 8; m_iReplaySpeed = 17; break;
	case 6: m_fReplaySpeed = 16; m_iReplaySpeed = 8; break;
	}
	CString strReplaySpeed;
	strReplaySpeed.Format("Speed x%.1f",m_fReplaySpeed);
	GetDlgItem(IDC_STATIC_SPEED)->SetWindowText(strReplaySpeed);
	
	m_cReplaySpeed.SetPos(m_iSliderSpeedNumber);

	pWnd->m_iReplaySpeed = m_iReplaySpeed;
	pWnd->LogPlaySpeedSet(m_iReplaySpeed);
	*pResult = 0;
}

void CControlPannelDlg::LogDateDisplay(CString strDate)
{
	m_cStaticLogTime.SetText(strDate);

	m_iMoveYear = atoi(strDate.Left(5));
	m_iMoveMonth = atoi(strDate.Mid(6,2));
	m_iMoveDay = atoi(strDate.Mid(9,2));
	m_iMoveHour = atoi(strDate.Mid(12,2));
	m_iMoveMin = atoi(strDate.Mid(15,2));
	m_iMoveSec = atoi(strDate.Mid(18,2));
	
	m_bPlayStatus = TRUE;
}

void CControlPannelDlg::OnSetfocusEdtYear() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtYear() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplayYear > time.GetYear() ) {
		m_iReplayYear = time.GetYear();
	}
	if ( m_iReplayYear < 2010 ) {
		m_iReplayYear = time.GetYear();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

void CControlPannelDlg::OnSetfocusEdtMonth() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtMonth() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplayMonth > 12 ) {
		m_iReplayMonth = time.GetMonth();
	}
	if ( m_iReplayMonth < 1 ) {
		m_iReplayMonth = time.GetMonth();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

void CControlPannelDlg::OnSetfocusEdtDay() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtDay() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplayDay > 31 ) {
		m_iReplayDay = time.GetDay();
	}
	if ( m_iReplayDay < 1 ) {
		m_iReplayDay = time.GetDay();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

void CControlPannelDlg::OnSetfocusEdtHour() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtHour() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplayHour > 23 ) {
		m_iReplayHour = time.GetHour();
	}
	if ( m_iReplayHour < 0 ) {
		m_iReplayHour = time.GetHour();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

void CControlPannelDlg::OnSetfocusEdtMinute() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtMinute() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplayMinute > 59 ) {
		m_iReplayMinute = time.GetMinute();
	}
	if ( m_iReplayMinute < 0 ) {
		m_iReplayMinute = time.GetMinute();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

void CControlPannelDlg::OnSetfocusEdtSecond() 
{
	// TODO: Add your control notification handler code here
	m_bEditFlag = TRUE;
}

void CControlPannelDlg::OnKillfocusEdtSecond() 
{
	// TODO: Add your control notification handler code here
	CTime time;
	time = CTime::GetCurrentTime();

	UpdateData(TRUE);
	if ( m_iReplaySecond > 59 ) {
		m_iReplaySecond = time.GetSecond();
	}
	if ( m_iReplaySecond < 0 ) {
		m_iReplaySecond = time.GetSecond();
	}
	UpdateData(FALSE);

	m_bEditFlag = FALSE;
}

int CControlPannelDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CLLRApp* pApp = (CLLRApp*)AfxGetApp();
	
	// TODO: Add your specialized creation code here
 	WINDOWPLACEMENT iWinPlacement;
 	GetWindowPlacement(&iWinPlacement);
 	iWinPlacement.rcNormalPosition.left		=  pApp->m_SystemValue.nMenuStartX;
 //	iWinPlacement.rcNormalPosition.top		= pApp->m_SystemValue.nMenuStartY;
 	iWinPlacement.rcNormalPosition.right    = pApp->m_SystemValue.nMenuStartX + pApp->m_SystemValue.nMenuSizeX;
 //	iWinPlacement.rcNormalPosition.bottom	= pApp->m_SystemValue.nMenuStartY + pApp->m_SystemValue.nMenuSizeY;
 	SetWindowPlacement(&iWinPlacement);	
		
	return 0;
}
