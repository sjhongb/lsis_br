// MyCommCRC16.h : header file
//

#ifndef    _MYCOMMCRC16_H
#define    _MYCOMMCRC16_H

#include "MyComm.h"
#include "../include/UserDataType.h"
#include "LLRDoc.h"


/////////////////////////////////////////////////////////////////////////////
#define ID_COMM_WATCH	 200
#define ID_COMM_ERRMSG   201
#define ID_COMM_PRINTMSG 202

/////////////////////////////////////////////////////////////////////////////
#define COM_EXT ((BYTE)0x70) 
#define COM_STX ((BYTE)0x7E) 
#define COM_ETX ((BYTE)0xFE)

/////////////////////////////////////////////////////////////////////////////
// CComm class

class CMyCommCRC16 : public CComm
{
	union {
		unsigned short m_nRxCRC;
		struct {
			BYTE m_RxCRCLow;
			BYTE m_RxCRCHigh;
		};
	};
	union {
		unsigned short m_nTxCRC;
		struct {
			BYTE m_TxCRCLow;
			BYTE m_TxCRCHigh;
		};
	};
	union {
		unsigned short m_nInCRC;
		struct {
			BYTE m_InCRCLow;
			BYTE m_InCRCHigh;
		};
	};
	union {
		unsigned short  m_nInRxMax;
		struct {
			BYTE m_nInRxLow;
			BYTE m_nInRxHigh;
		};
	};
// for test 
	BYTE m_PasSetCount;
//
	BOOL m_bExtRx;
	BOOL m_bTxReady;
	int  m_nTxPtr;
	int  m_nRxPtr;
	int  m_nMode;		// 0/wait STX, 1/func, 2/L-low, 3/L-high, 4/data, 5/crc1, 6/crc2, 7/etx
	COMCommandType m_PostDB;
	COMCommandType m_MsgDB;
	CView *m_ParentWnd;
	BYTE m_CmdBuf[16];
	BYTE m_pDataQ[4096];
	BYTE m_pMsgQ[4096];
	BYTE m_pRxQ[4096];
	BYTE m_pTxQ[4096];

public:
	static int _nPas;
	BOOL m_bCommFail;
	int  m_nPasBitMask;
	int  m_nPasTick;
	int  m_nWaitTick;
	int  m_nFailLimitTick;
	int  m_nPasLimitTick;
	int  m_nCmdCounter;

public:
	CMyCommCRC16();
	~CMyCommCRC16();

	BOOL OpenComPort(CView *ParentWnd);
	virtual void SetReadData(LPSTR data, int size);
	BOOL SendData(BYTE* data, int len, BYTE stx = COM_STX , BOOL bCmd = FALSE);

	void ClearPas();
	void SetPas();
	BOOL GetPas();
	BOOL CheckCommTick();
	BOOL GetCommFail()
	{
		return m_bCommFail;
	}
	void SetCommFail( BOOL bFail )
	{
		m_bCommFail = bFail;
	}
	void InitCommTick()
	{
		m_nWaitTick = 0;
	}
	void SetLimitTick(int nFailLimit, int nPasLimit) 
	{
		m_nFailLimitTick = nFailLimit;
		m_nPasLimitTick  = nPasLimit;
	}

	void PurgeRxQ();
	void PurgeTxQ();
	void Poll();

protected:
	void GetRxCRC16(BYTE c);
	void GetTxCRC16(BYTE c);
};

/////////////////////////////////////////////////////////////////////////////
#endif //  _MYCOMMCRC16_H

